"""Entity class to interface with Shotgun photogrammetry records."""

from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    """Photogrammetry subtypes and their corresponding strings in Shotgun."""

    ENVIRONMENT_PROP = "Environment/Prop"
    TALENT = "Talent"  # To be removed from Shotgun
    TALENT_FACE = "Talent - Face"
    TALENT_BODY = "Talent - Body"
    TALENT_POSE = "Talent - Pose"


class Photogrammetry(baseEntity.BaseEntity):
    """Class that represents a photogrammetry entity."""

    ENTITY_TYPE = const.Entities.PHOTOGRAMMETRY
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.ASSETLINK,
        const.Fields.GLOBAL_ENTITY_PROJECT,
        const.Fields.STATUS,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.PROCESSED_MODEL_PATH,
        const.Fields.GROUP,
        const.Fields.CODE,
        const.Fields.CAPTURE_DATE,
        const.Fields.THUMBNAIL,
        const.Fields.VERSIONS,
        const.Fields.TALENT,
        const.Fields.PERFORCE_PATH,
        const.Fields.PERFORCE_NEUTRAL_PATH
    )
    NAME_FIELD = const.Fields.CODE
    SUBTYPES = SubTypes

    DATETIME_FORMAT = "%Y-%m-%d"

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def project(self):
        """The project code that this task is currently set to.

        Returns:
            str
        """
        return self._getattr(const.Fields.GLOBAL_ENTITY_PROJECT)

    @project.setter
    def project(self, value):
        """Sets the current project code and updates the project name field to reflect the new project code being set.

        Args:
            value (str): project code name to set
        """
        self._setattr(const.Fields.GLOBAL_ENTITY_PROJECT, value)

    @property
    def subtype(self):
        """The subtype for this photogrammetry asset.

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """Sets the current entity subtype code.

        Args:
            value (str): subtype code name to set
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def assetLink(self):
        """The assetLink for this photogrammetry asset.

        Returns:
            object
        """
        return self._getmutable(const.Fields.ASSETLINK, mutableType=list)

    @assetLink.setter
    def assetLink(self, value):
        """Sets the current entity assetLink code.

        Args:
            value (Asset entity): assetLink code name to set
        """
        self._setattr(const.Fields.ASSETLINK, value, isMutable=True)

    @property
    def processedModelPath(self):
        """The processed model path of the record.

        Returns:
            str: processed model path
        """
        return self._getattr(const.Fields.PROCESSED_MODEL_PATH)

    @processedModelPath.setter
    def processedModelPath(self, value):
        """Sets the processed model path.

        Args:
            value (str): Tne new value to set the processed model path to
        """
        self._setattr(const.Fields.PROCESSED_MODEL_PATH, value)

    @property
    def talent(self):
        """The talent that this scan was made from.

        Returns:
            rsg_core.shotgun.entites.talent.Talent
        """
        return self._getattr(const.Fields.TALENT)

    @talent.setter
    def talent(self, value):
        """Sets the talent that this scan was made from.

        Args:
            value (rsg_core.shotgun.entites.talent.Talent): talent that this scan was made from
        """
        self._setattr(const.Fields.TALENT, value)

    @property
    def status(self):
        """The status code that this task is currently set to.

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """Sets the current status code and updates the status name field to reflect the new status code being set.

        Args:
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)

    @property
    def scanId(self):
        """The id of the scan.

        This number is only unique in combination with the shot date and talent whom was scanned.

        Returns:
            int
        """
        return self._getattr(const.Fields.SCAN_ID)

    @scanId.setter
    def scanId(self, value):
        """Sets the value of the scan ID.

        Args:
            value (int): ID of the scan
        """
        self._setattr(const.Fields.SCAN_ID, value)

    @property
    def groups(self):
        """The groups that are allowed to view this entity.

        Returns:
            rsg_core.shotgun.entities.group.Group
        """
        return self._getmutable(const.Fields.GROUP, mutableType=list)

    @groups.setter
    def groups(self, value):
        """Sets the current groups that are allowed to view this entity.

        Args:
            value (str): groups code name to set
        """
        self._setattr(const.Fields.GROUP, value, isMutable=True)

    @property
    def captureDate(self):
        """The date the scan was captured.

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.CAPTURE_DATE)

    @captureDate.setter
    def captureDate(self, value):
        """Sets the current captureDate code and updates the captureDate name field accordingly.

        Args:
            value (datetime.datetime): the date the scan was captured
        """
        self._setattr(const.Fields.CAPTURE_DATE, value)

    @property
    def perforcePath(self):
        """The path to the photogrammetry root in perforce.

        Returns:
            str: The path, as a perforce-formatted string (//path/to/root/)
        """
        return self._getattr(const.Fields.PERFORCE_PATH)

    @perforcePath.setter
    def perforcePath(self, value):
        """Sets the perforce path root.

        Args:
            value (str): The path to the perforce root.
        """
        if len(value) < 3 or value[:2] != "//":
            raise ValueError("{} does not appear to be a Perforce-formatted path. Path must start with `//`.".format(value))
        self._setattr(const.Fields.PERFORCE_PATH, value)

    @property
    def perforceNeutralPath(self):
        """The path to the neutral folder of a photogrammetry scan in perforce.

        Returns:
            str: The path, as a perforce-formatted string (//path/to/root/_Neutral/)
        """
        return self._getattr(const.Fields.PERFORCE_NEUTRAL_PATH)

    @perforceNeutralPath.setter
    def perforceNeutralPath(self, value):
        """Sets the perforce neutral path.

        Args:
            value (str): The path to the neutral folder of the photogrammetry.
        """
        if len(value) < 3 or value[:2] != "//":
            raise ValueError("{} does not appear to be a Perforce-formatted path. Path must start with `//`.".format(value))
        self._setattr(const.Fields.PERFORCE_NEUTRAL_PATH, value)

    @property
    def tags(self):
        """The tags that are allowed to view this entity.

        Returns:
            rsg_core.shotgun.entities.tag.Tag
        """
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        """Sets the current tags that are allowed to view this entity.

        Args:
            value (str): tags code name to set
        """
        self._setattr(const.Fields.TAGS, value, isMutable=True)


class Builder(baseEntity.BaseBuilder):
    """Photogrammetry builder."""

    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Photogrammetry

    @property
    def subtypes(self):
        """Gets subtypes."""
        return SubTypes
