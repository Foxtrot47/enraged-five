from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Brand(baseEntity.BaseEntity):
    """
    Class that represents a Asset entity
    """

    ENTITY_TYPE = const.Entities.BRAND
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.TASKS,
        const.Fields.STATUS
    )

    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        """
        The name of the entity

        Return:
            str
        """
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        """
        Sets the name of the entity

        Arguments:
            value (str): name for the entity
        """
        self._setattr(const.Fields.CODE, value)

    @property
    def tasks(self):
        """
        Tasks associated with this entity

        Returns:
            list
        """
        return self._getattr(const.Fields.TASKS)

    @tasks.setter
    def tasks(self, value):
        """
        Sets the tasks associated with this entity

        Arguments:
            value (list): list of tasks

        Returns:
            list
        """
        self._setattr(const.Fields.TASKS, value)

    @property
    def status(self):
        """
        The status code for current status this entity is set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the status code to use for this entity.
        Also sets the status name based on the code being set.

        Arguments:
            value (str): status code to set
        """
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Brand
