from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    AI = "AI"
    MISSION = "Mission"
    MOCAP = "Mocap"
    IE_TEMPLATE = "IE Template"
    IE_VARIATION = "IE Variation"


class Script(baseEntity.BaseEntity):
    """
    Class that represents a Script entity
    """

    ENTITY_TYPE = const.Entities.SCRIPT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.MISSION,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
        const.Fields.CHILD_ENTITY,
        const.Fields.PARENT_ENTITY,
        const.Fields.ENTITY_TYPE,
        const.Fields.MISSIONS,
        const.Fields.SCRIPT_CHARACTERS,
    )

    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def missions(self):
        return self._getmutable(const.Fields.MISSIONS, mutableType=list)

    @missions.setter
    def missions(self, value):
        self._setattr(const.Fields.MISSIONS, value, isMutable=True)

    @property
    def subtype(self):
        """
        The subtype for this photogrammetry asset

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the current entity subtype code

        Arguments
            value (str): subtype code name to set
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def scriptCharacters(self):
        """Gets the script characters that this script links to.

        Returns:
            list of rsg_clients.shotgun.entities.asset.Asset: The linked to by this script.
        """
        return self._getmutable(const.Fields.SCRIPT_CHARACTERS, mutableType=list)

    @scriptCharacters.setter
    def scriptCharacters(self, value):
        """Sets the script characters.

        Args:
            value (list of rsg_clients.shotgun.entities.asset.Asset): The characters in this scene.
        """
        self._setattr(const.Fields.SCRIPT_CHARACTERS, value, isMutable=True)


class Builder(baseEntity.BaseBuilder):
    """
    Builder for building script entity payloads
    """

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Script

    @property
    def subtypes(self):
        return SubTypes
