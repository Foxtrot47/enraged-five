from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Version(baseEntity.BaseEntity):
    """
    Class that represents a version entity
    """

    ENTITY_TYPE = const.Entities.VERSION
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.BUGSTAR_ID,
        const.Fields.CODE,
        const.Fields.ENTITY,
        const.Fields.TASK,
        const.Fields.THUMBNAIL,
        const.Fields.UPLOADED_MOVIE,
        const.Fields.FRAME_RATE,
        const.Fields.ARCHIVED,
        const.Fields.DISPLAY_AS_THUMBNAIL,
        const.Fields.WATCHSTAR_ID,
        const.Fields.REVISION,
    )
    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        return Builder

    @property
    def entity(self):
        """
        The entity that this version is attached to

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        return self._getattr(const.Fields.ENTITY)

    @entity.setter
    def entity(self, value):
        """
        Sets the entity that this version should be attached to

        Arguments:
            value (rsg_core.shotgun.entities.baseEntity.BaseEntity): the entity to attach this version to
        """
        self._setattr(const.Fields.ENTITY, value)

    @property
    def task(self):
        """
        Gets the task that the version belongs to.

        Returns:
            rsg_core.shotgun.task.Task
        """
        return self._getattr(const.Fields.TASK)

    @task.setter
    def task(self, value):
        """
        Sets the task that this version should be attached to

        Args:
            value (rsg_core.shotgun.task.Task): task this version belongs to
        """
        self._setattr(const.Fields.TASK, value)

    @property
    def bugstarId(self):
        """
        Gets the Bugstar ID that the current item is being tracked by in Bugstar.

        Returns:
            int - ID of the item in Bugstar
        """
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        """
        Sets the bugstar ID that the current item is being tracked by in Bugstar
        Args:
            value (int): number of the ID in Bugstar
        """
        self._setattr(const.Fields.BUGSTAR_ID, value)

    @property
    def watchstarId(self):
        """
        Gets the Watchstar ID that the current item is being tracked by in Watchstar.

        Returns:
            int: ID of the item in Watchstar
        """
        return self._getattr(const.Fields.WATCHSTAR_ID)

    @watchstarId.setter
    def watchstarId(self, value):
        """
        Sets the watchstar ID that the current item is being tracked by in Watchstar.

        Args:
            value (int): number of the ID in Watchstar
        """
        self._setattr(const.Fields.WATCHSTAR_ID, value)

    @property
    def attachment(self):
        """
        Gets the file that was uploaded

        Returns:
            string
        """
        return self._getattr(const.Fields.UPLOADED_MOVIE)

    def uploadAttachment(self, path, name=None, tags=None):
        """Uploads a file to the sg_uploaded_movie field. Automatically triggers the transcoder to update the thumbnail.

        This will overwrite an existing file attachment if one exists.

        Args:
            path (str): Location of the file to upload.

        Returns:
            int: Result of the upload operation.
        """
        return self.upload(path, field=const.Fields.UPLOADED_MOVIE, name=name, tags=tags)

    def downloadAttachment(self, path):
        """
        Downloads the attachment assigned to the Version to the specified path in the file system.

        Args:
            path (str): location where to save the file.

        Returns:
            str - path where the file has been saved.
        """
        if self.attachment:
            return self.attachment.download(path)

    @property
    def archived(self):
        """
        Returns the value of the archived field
        Returns:
            bool
        """
        return self._getattr(const.Fields.ARCHIVED)

    @archived.setter
    def archived(self, value):
        """
        Sets the archived field for the attachments
        Args:
            value (bool): True if the attachment has been archived on bugstar server.
        """
        self._setattr(const.Fields.ARCHIVED, value)

    @property
    def displayAsThumbnail(self):
        """ Gets the checkbox state of the "Display as thumbnail" field """
        return self._getattr(const.Fields.DISPLAY_AS_THUMBNAIL)

    @displayAsThumbnail.setter
    def displayAsThumbnail(self, value):
        """
        Sets the state of "Display as Thumbnail" field
        Args:
            value (bool): True if this versions thumbnails should be the thumbnail of the task/entity.
        """
        self._setattr(const.Fields.DISPLAY_AS_THUMBNAIL, value)        
        
    @property
    def frameRate(self):
        """ Returns the value of the frame rate field """
        return self._getattr(const.Fields.FRAME_RATE)

    @frameRate.setter
    def frameRate(self, value):
        """
        Sets the frame rate of the uploaded movie 
        Args:
            value (float)
        """
        self._setattr(const.Fields.FRAME_RATE, value)
        
    @property
    def thumbnail(self):
        """ Returns the thumbnail of the version """
        return self._getattr(const.Fields.THUMBNAIL)
    
    @thumbnail.setter
    def thumbnail(self, value):
        """
        Sets the thumbnail of the version
        Args:
            value (str)
        """
        self._setattr(const.Fields.THUMBNAIL, value)
        
    @property
    def revision(self):
        """Returns the revision number.

        Returns:
            int: The revision number of the version, if there is one; else, None.
        """
        return self._getattr(const.Fields.REVISION)

    @revision.setter
    def revision(self, value):
        """Sets the revision number.

        Args:
            value (int): The new value for the revision.
        """
        if type(value) != int:
            raise TypeError("Revision number must be an int, and {} is not.".format(value))
        if value < 0:
            raise TypeError("Revision number should be positive, and {} is not.".format(value))

        self._setattr(const.Fields.REVISION, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Version

    def getVersionByTask(self, task, filters=None):
        """
        Gets all the versions on the specific task.

        Args:
            task (rsg_core.shotgun.task.Task):
            filters (list, optional): list of [rsg_core.shotgun.const.Fields, rsg_core.shotgun.const.Operations, value]

        Returns:
            list (rsg_core.shotgun.version.Version) - all version objects that exists in the specific task
        """
        filters = filters or []
        filters.append([const.Fields.task, const.Operators.IS, task])
        entities = self.find(filters)
        return entities

    def getById(self, versionId, filters=None):
        """
        Gets the version object with the specified ID.

        Args:
            versionId (int): id number of the version
            filters (list, optional): list of [rsg_core.shotgun.const.Fields, rsg_core.shotgun.const.Operations, value]

        Returns:
            rsg_core.shotgun.version.Version
        """
        filters = filters or []
        filters.append([const.Fields.ID, const.Operators.IS, versionId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None

    def getByBugstarId(self, bugstarId, filters=None):
        """
        Gets the version object with the specified Bugstar ID.

        Args:
            bugstarId (int): bugstar Id number of the attachment that the version is presenting
            filters (list, optional): list of [rsg_core.shotgun.const.Fields, rsg_core.shotgun.const.Operations, value]

        Returns:
            rsg_core.shotgun.version.Version
        """
        filters = filters or []
        filters.append([const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None

    def getByWatchstarId(self, watchstarId, filters=None):
        """
        Gets the version object with the specified Watchstar ID.

        Args:
            watchstarId (int): watchstar Id number of the attachment that the version is presenting
            filters (list, optional): list of [rsg_core.shotgun.const.Fields, rsg_core.shotgun.const.Operations, value]

        Returns:
            rsg_core.shotgun.version.Version
        """
        filters = filters or []
        filters.append([const.Fields.WATCHSTAR_ID, const.Operators.IS, watchstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None
