"""Entity class to interface with Shotgun environment records."""

from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity
from rsg_core_py.abstracts import abstractConsts


class SubTypes(baseEntity.BaseEntitySubtypes):
    """Location subtypes and their corresponding strings in shotgun."""

    AREA_NAME = "Area Name"
    LOCATION = "Location"
    GRID = "Grid"
    INTERIOR = "Interior"
    MILO = "Milo"
    TERRAIN = "Terrain"


class MapPhases(baseEntity.BaseEntitySubtypes, abstractConsts.AbstractConsts):
    """Map phases and their corresponding strings in shotgun."""
    MAP_PHASE_1 = "Map Phase 1"
    MAP_PHASE_2 = "Map Phase 2"
    MAP_PHASE_3 = "Map Phase 3"
    MAP_PHASE_4 = "Map Phase 4"
    MAP_PHASE_5 = "Map Phase 5"
    MAP_PHASE_6 = "Map Phase 6"


class GridTypes(baseEntity.BaseEntitySubtypes, abstractConsts.AbstractConsts):
    """Grid types and their corresponding shotgun strings."""
    CITY = "City"
    COUNTRYSIDE = "Countryside"
    DISTRICT = "District"
    FREEWAY = "Freeway"
    NEIGHBOURHOOD = "Neighbourhood"
    RAIL = "Rail"
    ROAD = "Road"
    SEA = "Sea"
    TOWN = "Town"


class Location(baseEntity.BaseEntity):
    """Class that represents an environment entity."""

    ENTITY_TYPE = const.Entities.LOCATION
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.PROJECT,
        const.Fields.DESCRIPTION,
        const.Fields.LOCATION_SUBTYPE,
        const.Fields.BUGSTAR_ID,
        const.Fields.MAP_PHASE,
        const.Fields.GRID_TYPE,
    )
    NAME_FIELD = const.Fields.CODE
    SUBTYPES = SubTypes

    DATETIME_FORMAT = "%Y-%m-%d"

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder: The builder for this class.
        """
        return Builder

    @property
    def subtype(self):
        """
        The entity subtype for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the entity subtype for this entity

        Arguments:
            value (str): entity subtype
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def locationSubtype(self):
        """Gets the location subtype.

        Returns:
            str: the location subtype.
        """
        return self._getattr(const.Fields.LOCATION_SUBTYPE)

    @locationSubtype.setter
    def locationSubtype(self, value):
        """Sets the location subtype.

        Args:
            value (str): Value to set the location subtype to.
        """
        self._setattr(const.Fields.LOCATION_SUBTYPE, value)

    @property
    def bugstarId(self):
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        self._setattr(const.Fields.BUGSTAR_ID, value)

    @property
    def mapPhase(self):
        """The map phase, per terrain workflow.

        Returns:
            str: The map phase (one of the values of location.MapPhases).
        """
        return self._getattr(const.Fields.MAP_PHASE)

    @mapPhase.setter
    def mapPhase(self, value):
        """Set the map phase. Value must be an attribute of location.MapPhases, or an integer that corresponds to one.

        Args:
            str or int: If str, the map phase; if int, corresponds to the map phase, as per the conversion below.
        """
        # try converting an integer to the format of a map phase step
        if isinstance(value, int):
            value = "Map Phase {}".format(value)
        if value not in MapPhases:
            raise ValueError("mapPhase value must be in location.MapPhases. {} is not.".format(value))
        self._setattr(const.Fields.MAP_PHASE, value)

    @property
    def gridType(self):
        """The grid type, per terrain workflow.

        Returns:
            str: The grid type (one of the values of location.GridTypes).
        """
        return self._getattr(const.Fields.GRID_TYPE)

    @gridType.setter
    def gridType(self, value):
        """Set the grid type. Value must be an attribute of location.GridTypes.

        Args:
            str: The grid type.
        """
        if value not in GridTypes:
            raise ValueError("gridType value must be in location.GridTypes. {} is not.".format(value))
        self._setattr(const.Fields.GRID_TYPE, value)


class Builder(baseEntity.BaseBuilder):
    """Location builder."""

    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_core_py.abstracts.abstractPayload.AbstractPayload: The object created by the builder.
        """
        return Location

    @property
    def subtypes(cls):
        """Gets subtypes."""
        return SubTypes

    def getLocationByName(self, name, filters=None):
        """Gets the location with the specified name.

        Args:
            name (str): The name of the location to look for.
            filters (list, optional): List of additional filters to apply.

        Returns:
            rsg_clients.shotgun.location.Location: Location with the corresponding name.
        """
        filters = filters or []
        filters.append([const.Fields.CODE, const.Operators.IS, name])
        return self.findOne(filters)

    def getGridByName(self, name, filters=None):
        """Gets the grid with the specified name.

        Grids are subtypes of Location, so this is just a helper function that also tacks on a filter to specify
        grids only.

        Args:
            name (str): The name of the grid to look for.
            filters (list, optional): List of additional filters to apply.

        Returns:
            rsg_clients.shotgun.location.Location: Grid with the corresponding name.
        """
        filters = filters or []
        filters.append([const.Fields.ENTITY_SUBTYPE, const.Operators.IS, SubTypes.GRID])
        return self.getLocationByName(name, filters)

    def getByBugstarId(self, bugstarId, filters=None):
        """
        Get a location from shotgun based on the bugstar id

        Arguments:
            bugstarId (int): the bugstar id of the location

        Returns:
            rockstare.shotgun.entities.location.Location
        """
        filters = filters or []
        filters.append([const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId])
        locations = self.find(filters)
        if locations:
            return locations[0]
