from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    MISSION = const.SubTypes.MISSION
    SCRIPTED_ANIM = const.SubTypes.SCRIPTED_ANIM


class Strand(baseEntity.BaseEntity):
    """
    Class that represents a TaG entity
    """

    ENTITY_TYPE = const.Entities.STRAND
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.BUGSTAR_ID,
        const.Fields.CODE,
        const.Fields.NOTES,
        const.Fields.TAGS,
        const.Fields.TASKS,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.CINEMATIC_ANIMATOR,
        const.Fields.LEVEL_DESIGNER,
        const.Fields.MISSION_ID,
        const.Fields.ACTIVE,
        const.Fields.SINGLE_PLAYER,
        const.Fields.DESCRIPTION,
        const.Fields.STORY_DESCRIPTION,
        const.Fields.VERSION,
        const.Fields.MISSION_ID,
        const.Fields.CHILD_ENTITY,
        const.Fields.PARENT_ENTITY,
    )

    NAME_FIELD = const.Fields.CODE

    SUBTYPES = SubTypes

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def subtype(self):
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def active(self):
        return self._getattr(const.Fields.ACTIVE)

    @active.setter
    def active(self, value):
        self._setattr(const.Fields.ACTIVE, value)

    @property
    def description(self):
        return self._getattr(const.Fields.DESCRIPTION)

    @description.setter
    def description(self, value):
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def missionId(self):
        return self._getattr(const.Fields.MISSION_ID)

    @missionId.setter
    def missionId(self, value):
        self._setattr(const.Fields.MISSION_ID, value)

    @property
    def storyDescription(self):
        return self._getattr(const.Fields.STORY_DESCRIPTION)

    @storyDescription.setter
    def storyDescription(self, value):
        self._setattr(const.Fields.STORY_DESCRIPTION, value)

    @property
    def cinematicAnimator(self):
        return self._getattr(const.Fields.CINEMATIC_ANIMATOR)

    @cinematicAnimator.setter
    def cinematicAnimator(self, value):
        self._setattr(const.Fields.CINEMATIC_ANIMATOR, value)

    @property
    def levelDesigner(self):
        return self._getattr(const.Fields.LEVEL_DESIGNER)

    @levelDesigner.setter
    def levelDesigner(self, value):
        self._setattr(const.Fields.LEVEL_DESIGNER, value)

    @property
    def audioDesigner(self):
        return self._getattr(const.Fields.AUDIO_DESIGNER)

    @audioDesigner.setter
    def audioDesigner(self, value):
        self._setattr(const.Fields.AUDIO_DESIGNER, value)

    @property
    def singlePlayer(self):
        return self._getattr(const.Fields.SINGLE_PLAYER)

    @singlePlayer.setter
    def singlePlayer(self, value):
        self._setattr(const.Fields.SINGLE_PLAYER, value)

    @property
    def version(self):
        return self._getattr(const.Fields.VERSION)

    @version.setter
    def version(self, value):
        self._setattr(const.Fields.VERSION, value)

    @property
    def bugstarId(self):
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        self._setattr(const.Fields.BUGSTAR_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Strand

    @property
    def subtypes(self):
        """
        The subtypes for this entity
        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntitySubtypes
        """
        return SubTypes

    def getByBugstarId(self, bugstarId, filters):
        """
        Get a strand from shotgun based on the bugstar id associated with that strand.

        Arguments:
            bugstarId (int): the bugstar id of the strand

        Returns:
            rockstare.shotgun.entities.project.Project
        """
        filters = filters or []
        filters.append([const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
