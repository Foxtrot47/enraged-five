from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class HumanUser(baseEntity.BaseEntity):
    """
    Class that represents a Human User entity
    """

    ENTITY_TYPE = const.Entities.HUMAN_USER
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.NAME,
        const.Fields.USERNAME,
        const.Fields.STUDIO,
        const.Fields.JOB_TITLE,
        const.Fields.THUMBNAIL,
        const.Fields.GROUPS,
        const.Fields.PROJECTS,
        const.Fields.DEPARTMENT,
        const.Fields.EMAIL,
        const.Fields.STATUS,
        const.Fields.EMPLOYEE_ID,
        const.Fields.BUGSTAR_ID,
        const.Fields.LOGIN,
        const.Fields.VERSIONS,
        const.Fields.LAST_ACTIVE
    )
    NAME_FIELD = const.Fields.NAME

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.NAME)

    @property
    def email(self):
        return self._getattr(const.Fields.EMAIL)

    @email.setter
    def email(self, value):
        """
        Sets the email for this user

        Arguments:
            value (str): email for this user
        """
        self._setattr(const.Fields.EMAIL, value)

    @property
    def username(self):
        return self._getattr(const.Fields.USERNAME)

    @username.setter
    def username(self, value):
        """
        Sets the username for this user

        Arguments:
            value (str): username for this user
        """
        self._setattr(const.Fields.USERNAME, value)

    @property
    def title(self):
        return self._getattr(const.Fields.JOB_TITLE)

    @title.setter
    def title(self, value):
        """
        Sets the title for this user

        Arguments:
            value (str): title for this user
        """
        self._setattr(const.Fields.JOB_TITLE, value)

    @property
    def studio(self):
        return self._getattr(const.Fields.STUDIO)

    @studio.setter
    def studio(self, value):
        """
        Sets the studio for this user

        Arguments:
            value (str): studio for this user
        """
        self._setattr(const.Fields.STUDIO, value)

    @property
    def department(self):
        return self._getattr(const.Fields.DEPARTMENT)

    @department.setter
    def department(self, value):
        """
        Sets the department for this user

        Arguments:
            value (str): department for this user
        """
        self._setattr(const.Fields.DEPARTMENT, value)

    @property
    def projects(self):
        return self._getmutable(const.Fields.PROJECTS, mutableType=list)

    @projects.setter
    def projects(self, value):
        """
        Sets the projects this user should have access to in Shotgun

        Arguments:
            value (str): projects this user should have access to
        """
        self._setattr(const.Fields.PROJECTS, value, isMutable=True)

    @property
    def groups(self):
        return self._getmutable(const.Fields.GROUPS, mutableType=list)

    @groups.setter
    def groups(self, value):
        """
        Sets the groups this user should be a part of in Shotgun

        Arguments:
            value (str): groups this user should be a part of
        """
        self._setattr(const.Fields.GROUPS, value, isMutable=True)

    @property
    def status(self):
        """
        The status code that this task is currently set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the current status code and updates the status name field to reflect the new status code being set.

        Arguments
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)

    @property
    def bugstarId(self):
        """
        Gets the Bugstar ID that the current item is being tracked by in Bugstar.

        Returns:
            int: ID of the item in Bugstar
        """
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        """
        Sets the bugstar ID that the current item is being tracked by in Bugstar.

        Args:
            value (int): number of the ID in Bugstar
        """
        self._setattr(const.Fields.BUGSTAR_ID, value)

    @property
    def employeeId(self):
        """
        Gets the Employee ID that this human user if they have one.

        Returns:
            int: employee id number of the user
        """
        return self._getattr(const.Fields.EMPLOYEE_ID)

    @employeeId.setter
    def employeeId(self, value):
        """Sets the employee ID for this human user.

        Args:
            value (int): the employee id number
        """
        self._setattr(const.Fields.EMPLOYEE_ID, value)

    @property
    def login(self):
        """The identification that Shotgun uses when determining which user is login in.

        Returns:
            str: employee id number of the user
        """
        return self._getattr(const.Fields.LOGIN)

    @login.setter
    def login(self, value):
        """Sets the identification that Shotgun uses when determining which user is login in.

        Args:
            value (str): value to set as the identifier for this individual when they log in.
        """
        self._setattr("login", value)

    @property
    def lastActive(self):
        """
        The datetime this user last logged into Shotgun.

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.LAST_ACTIVE)

    @lastActive.setter
    def lastActive(self, value):
        """
        Sets the date/time this user last logged into Shotgun. Can be a datetime formatted string or datetime object.

        Arguments:
            value (datetime.datetime): the datetime the user logged in, usually the current UTC time.
        """
        self._setattr(const.Fields.LAST_ACTIVE, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return HumanUser

    def getByEmail(self, email):
        """
        Gets the user based on its email

        Arguments:
            email (str): email of the user

        Returns:
            rsg_core.shotgun.entities.humanUser.HumanUser
        """
        entity = self.find([[const.Fields.EMAIL, const.Operators.IS, email]])
        if entity:
            return entity[0]

    def getByUsername(self, name, filters=None):
        """
        Gets the entity based on its username

        Arguments:
            name (str): name of the entity
            filters(list): additional filters to include

        Returns:
            rsg_core.shotgun.entities.humanUser.HumanUser
        """
        filters = filters or []
        filters.append([const.Fields.USERNAME, const.Operators.IS, name])
        entity = self.find(filters)
        if entity:
            return entity[0]
