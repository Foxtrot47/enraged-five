"""Various api/script keys to authenticate against shotgun.

Due to the fact that we use SSO for Shotgun, we can't use our personal login details to authenticate against the server.
"""
from rsg_core import accounts
from rsg_core_py.metaclasses import singleton


class AbstractApiKey(accounts.AbstractAccount):
    """Authenticate as a shotgun script/api key.

    This is intended to only be used for accessing Shotgun. These credentials will not work with other services.
    """
    __metaclass__ = singleton.Singleton
    isApiKey = True


class Core(AbstractApiKey):
    """The default script key for testing shotgun python commands."""

    name = "shotgunCore"

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password="ZTRqaGZxbGZkeW56bGpmYWRuamtpJkR5cQ==", **kwargs)


class UnitTests(AbstractApiKey):
    """The script key to track commands made for unit tests"""

    name = "unitTests"

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password="RWdkZGF4aXB1fmtxaGY5bGduZXVrbGRncA==", **kwargs)


class Backend(AbstractApiKey):
    """The script key for tracking commands made by the shotgun backend."""

    name = "eventDaemon"

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(
            password="ZTAxYjU5ZWM5N2RiNzk2NWY5MTNjZmE3M2Q3MzQzZGMxYThiMTk0MDBkNmU5OGNlZTM3MzlkM2ZhMzk1MzQxMQ==",
            **kwargs
        )


class Mocap(AbstractApiKey):
    """The script key for tracking commands made by the mocap tools."""

    name = "mocap"

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password="Y19ta3J1aVFwZnNqZmNibmtidDR1eWR1eQ==", **kwargs)


class UserActivity(AbstractApiKey):
    """The script key for tracking commands made by the user activity monitoring tool."""

    name = "userActivity"

    def authenticate(self, instance, **kwargs):
        """Reimplemented from the inherited class.

        Passes the password for this account to the authenticate method of the shotgun instance.

        Args:
            instance (rsg_clients.shotgun.core.Shotgun): object with the authenticate method that needs to access the password.
            **kwargs (dictionary): additional keyword arguments that should be passed into the authenticate method
                                   of the given instance.
        Returns:
            bool
        """
        return instance.authenticate(password="YkkmaXZrY21wc2F3Y3NhdWc5dmhndWtweA==", **kwargs)
