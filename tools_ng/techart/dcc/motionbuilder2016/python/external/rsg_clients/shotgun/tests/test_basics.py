"""
Test module for debugging the basic interface for interacting with bugs on Bugstar
"""
import os
import time
import unittest

from rsg_core import accounts as coreAccounts
from rsg_clients.decorators import connection
from rsg_clients.shotgun import servers, const, accounts, exceptions
from rsg_clients.shotgun.entities import baseEntity


class TestConnection(unittest.TestCase):
    """Tests basic connection functionality."""

    SERVER = None  # Server to use for this test
    AMOUNT_OF_RETRIES = 5
    WAIT_TIME = 1

    @classmethod
    def setUpClass(cls):
        """Overrides inherited method.

        Sets up the server connection to the development server when the class is invoked the first time.
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.UnitTests()

    def test_Connection(self):
        """Test if you can connect to the servers."""
        for server in (servers.development, servers.production):
            server.account = accounts.UnitTests()
            server.account = coreAccounts.OtherUser("notreal", "badpassword")
            self.assertFalse(server.shotgun.login())
            server.account = accounts.UnitTests()
            self.assertTrue(server.shotgun.login())

    def test_Token(self):
        """Test that refreshing the token works properly.

        Notes:
            This unit test takes 10 minutes to run as we have to wait for the token to expire.
        """
        self.assertTrue(self.SERVER.shotgun.login())
        time.sleep(600) # This is the amount of time it takes for a token to expire
        self.assertTrue(self.SERVER.shotgun.login())

    def test_ServerUnavailable(self):
        """Test if waiting for the server for the given amount of time works."""
        @connection.waitForServer(waitTime=self.WAIT_TIME, amountOfRetries=self.AMOUNT_OF_RETRIES)
        def raiseError():
            raise exceptions.ServerUnavailable(self.SERVER, connection=self.SERVER.instance, message="Test")
        try:
            startTime = time.time()
            raiseError()
        except exceptions.ServerUnavailable:
            self.assertGreaterEqual((time.time() - startTime), self.AMOUNT_OF_RETRIES * self.WAIT_TIME)


class TestTasks(unittest.TestCase):
    SERVER = None
    TEST_BUG_ID = 5381704  # ID of Bugstar bug used for testing the Python API.

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection when the class is invoked the first time
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.UnitTests()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SERVER.logout()
        cls.SERVER = None

    def tearDown(self):
        self.SERVER.clearCache()

    def test_GetTask(self):
        currentTask = self.SERVER.task.getByBugNumber(self.TEST_BUG_ID)
        self.assertIsInstance(currentTask, self.SERVER.task.cls())

    def test_SetTags(self):
        currentTask = self.SERVER.task.getByBugNumber(self.TEST_BUG_ID)
        tag = self.SERVER.tag.getByName("sg_backend")
        if tag in currentTask.tags:
            currentTask.tags.remove(tag)
            currentTask.updateFields([const.Fields.TAGS])

        currentTask.tags.append(tag)
        currentTask.updateFields([const.Fields.TAGS])
        currentTask.tags.remove(tag)
        currentTask.updateFields([const.Fields.TAGS])

    def test_CopyTask(self):
        currentTask = self.SERVER.task.getByBugNumber(self.TEST_BUG_ID)
        self.assertIsNotNone(currentTask)

        copyTask = currentTask.copy(thumbnail=True)
        copyTask.bugNumber = None
        copyTask.name = "{} Copy".format(currentTask.name)
        copyTask.create()
        self.assertIsNotNone(copyTask)
        self.assertNotEqual(copyTask.id, currentTask.id)

    def test_uploadDownloadThumbnail(self):
        currentTask = self.SERVER.task.getByBugNumber(self.TEST_BUG_ID)
        testImage = os.path.join(const.Paths.TESTS_DIRECTORY, "testImage.jpg")

        currentTask.uploadThumbnail(testImage)
        self.assertIsNotNone(currentTask.thumbnailUrl)

        testDownloadImage = os.path.join(const.Paths.TESTS_DIRECTORY, "testImage1.jpg")
        currentTask.downloadThumbnail(testDownloadImage)
        self.assertTrue(os.path.exists(testDownloadImage))

        os.remove(testDownloadImage)

    def test_mutableData(self):
        """
        Test that mutable data is being edited properly
        """
        currentTask = self.SERVER.task.getByBugNumber(self.TEST_BUG_ID)
        self.assertIsInstance(currentTask, self.SERVER.task.cls())

        user = self.SERVER.humanUser.getByName("Tech Art Support")

        if currentTask.cc:
            # Test that we can edit the value by setting it
            currentTask.cc = []
            currentTask.updateFields([const.Fields.CC])
            currentTask.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(currentTask.cc, [])

            # Test that we can add data by editing it in place
            currentTask.cc.append(user)
            currentTask.updateFields([const.Fields.CC])
            currentTask.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(currentTask.cc, [user])

            # Leave the cc empty for future tests
            currentTask.cc = []
            currentTask.updateFields([const.Fields.CC])

        else:
            # Test that we can edit the value by setting it
            currentTask.cc = [user]
            currentTask.updateFields([const.Fields.CC])
            currentTask.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(currentTask.cc, [user])

            # Test that we can add data by editing it in place
            currentTask.cc.remove(user)
            currentTask.updateFields([const.Fields.CC])
            currentTask.removeEdits()  # verify that no edits are in place and only RAW data
            self.assertEqual(currentTask.cc, [])

    def test_batch(self):
        """
        Perform batch operations on the tasks
        """
        project = self.SERVER.project.getByName("Sandbox")
        task1 = self.SERVER.task()
        task2 = self.SERVER.task()
        task3 = self.SERVER.task()

        task1.name = "Batch Unit Test: Task #1"
        task2.name = "Batch Unit Test: Task #2"
        task3.name = "Batch Unit Test: Task #3"

        task1.project = project
        task2.project = project
        task3.project = project

        self.SERVER.batch(create=[task1, task2, task3])

        self.assertIsNotNone(task1.id)
        self.assertIsNotNone(task2.id)
        self.assertIsNotNone(task3.id)

        task2.name = "Batch Unit Test: Task #2 edit"
        task3.name = "Batch Unit Test: Task #3 edit"

        self.SERVER.batch(update=[task2, task3], delete=[task1])

        task2.removeEdits()
        task3.removeEdits()

        self.assertFalse(task1.isCached())
        self.assertEqual(task2.name, "Batch Unit Test: Task #2 edit")
        self.assertEqual(task3.name, "Batch Unit Test: Task #3 edit")
        self.SERVER.batch(delete=[task2, task3])

        self.assertFalse(task2.isCached())
        self.assertFalse(task3.isCached())


class TestEntities(unittest.TestCase):
    TEST_IMAGE = os.path.join(const.Paths.RESOURCES_DIRECTORY, "tests", "testImage.jpg")

    @classmethod
    def setUpClass(cls):
        """
        Overrides inherited method.

        Sets up the server connection when the class is invoked the first time
        """
        cls.SERVER = servers.development
        cls.SERVER.account = accounts.UnitTests()

    @classmethod
    def tearDownClass(cls):
        """
        Overrides inherited method.

        Closes the server connection when the class is invoked the first time
        """
        cls.SERVER.logout()
        cls.SERVER = None

    def tearDown(self):
        self.SERVER.clearCache()

    def test_Search(self):
        """Test that the search endpoint works."""
        # Simple Filter
        self.SERVER.instance.search(
            const.Endpoints.TASK, filters=[[const.Fields.ID, const.Operators.IS, 177719]], size=1
        )

        # Complex Filter
        self.SERVER.instance.search(
            const.Endpoints.TASK,
            filters={
                "conditions": [
                    [const.Fields.ID, const.Operators.IS, 177719],
                    [const.Fields.ID, const.Operators.IS_NOT, 0],
                ],
                "logical_operator": "and",
            },
            size=1,
        )
        # Nested Filter
        self.SERVER.instance.search(
            const.Endpoints.TASK,
            filters={
                "conditions": [
                    {
                        "conditions": [
                            [const.Fields.ID, const.Operators.IS, 177719],
                            [const.Fields.ID, const.Operators.IS, 177720],
                        ],
                        "logical_operator": "or",
                    },
                    [const.Fields.ID, const.Operators.IS_NOT, 0],
                ],
                "logical_operator": "and",
            },
            size=1,
        )

    def test_Summarize(self):
        """Test that the summarize endpoint works."""
        # Simple Filter
        result = self.SERVER.instance.summarize(
            const.Endpoints.TASK, filters=[[const.Fields.ID, const.Operators.IS, 177719]]
        )
        self.assertIs(result["data"]["summaries"][const.Fields.ID], 1)

        # Complex Filter
        result = self.SERVER.instance.summarize(
            const.Endpoints.TASK,
            filters={
                "conditions": [
                    [const.Fields.ID, const.Operators.IS, 177719],
                    [const.Fields.ID, const.Operators.IS_NOT, 0],
                ],
                "logical_operator": "and",
            },
            size=1,
        )
        self.assertIs(result["data"]["summaries"][const.Fields.ID], 1)

    def test_GetPhotogrammetryEntity(self):
        photogrammetry = self.SERVER.photogrammetry.getByName("000_BScanNeutral")
        self.assertIsInstance(photogrammetry, self.SERVER.photogrammetry.cls())

    def test_GetGoalEntity(self):
        goal = self.SERVER.goal.getByName("TA - Shotgun: Backend Systems")
        self.assertIsInstance(goal, self.SERVER.goal.cls())

    def test_readSchema(self):
        schema = self.SERVER.task.schema.read(const.Fields.BUG_NUMBER)
        self.assertIsNotNone(schema)

    def test_updateSchema(self):
        """Test that the update schema method is working as expected."""
        unitTestString = "Unit Test - DELETE ME"

        # Get the current values from the entity subtype field
        schema = self.SERVER.task.schema.read(const.Fields.ENTITY_SUBTYPE)
        cleanValues = schema[const.Fields.ENTITY_SUBTYPE]["properties"]["valid_values"]["value"]

        # Create a copy of the list to edit
        editedValues = list(cleanValues)
        editedValues.append(unitTestString)

        success = self.SERVER.task.schema.update(const.Fields.ENTITY_SUBTYPE, {"valid_values": editedValues})
        self.assertTrue(success)

        # Grab data from the server to confirm changes went through
        schema = self.SERVER.task.schema.read(const.Fields.ENTITY_SUBTYPE)
        updatedValues = schema[const.Fields.ENTITY_SUBTYPE]["properties"]["valid_values"]["value"]
        self.assertEqual(editedValues, updatedValues)

        success = self.SERVER.task.schema.update(const.Fields.ENTITY_SUBTYPE, {"valid_values": cleanValues})
        self.assertTrue(success)

    def test_ClearData(self):
        """Test that we can clear and remove data from the cache properly while still being able to query it."""
        task = self.SERVER.task(7542)  # Python test task
        self.assertIsInstance(task.qaOwner.name, basestring)
        task._clearDataCache()
        self.assertIsInstance(task.qaOwner.name, basestring)
        task._removeFromCache()
        self.assertIsInstance(task.qaOwner.name, basestring)
        task._removeFromCache(force=True)
        self.assertIsInstance(task.qaOwner.name, basestring)
        baseEntity.BaseEntity.CACHE_LIMIT = 0
        baseEntity.BaseEntity.CACHE_EXPIRY = 1
        baseEntity.BaseEntity.DATA_EXPIRY = 1
        time.sleep(3)
        self.SERVER.instance.cleanLeastAccessedCache()
        self.assertIsInstance(task.qaOwner.name, basestring)

    def test_AssetVersions(self):
        asset = self.SERVER.asset(23999)
        originalVersionCount = len(asset.versions)
        self.assertGreaterEqual(len(asset.versions), 0)
        # Create Version
        version = self.SERVER.version()
        version.name = os.path.splitext("testImage.jpg")[0]
        version.project = asset.project
        version.entity = asset
        version.archived = True
        version.create()
        version.upload(self.TEST_IMAGE, field=const.Fields.UPLOADED_MOVIE, name="testImage.jpg")

        asset.versions.append(version)
        asset.updateFields([const.Fields.VERSIONS])
        asset._clearDataCache()  # clear data cache to confim latest data was added properly to the server

        self.assertEqual(originalVersionCount + 1, len(asset.versions))

        asset.versions.remove(version)
        asset.updateFields([const.Fields.VERSIONS])
        asset._clearDataCache()  # clear data cache to confirm latest data was added properly to the server

        self.assertEqual(originalVersionCount, len(asset.versions))
        version.delete()

    def test_AssetVersions(self):
        asset = self.SERVER.asset(23999)
        asset.uploadThumbnail(self.TEST_IMAGE)
        asset._clearDataCache()
        self.assertEqual(1, len([version for version in asset.versions if version.displayAsThumbnail]))

    def test_TaskSearch(self):
        """Test that the search object returns the expected information."""
        search = self.SERVER.task.search(filters=[[const.Fields.ID, const.Operators.IS, 177719]])
        results = search.search(size=1)
        self.assertIsNotNone(results)
        self.assertEqual(len(results), 1)
        self.assertEqual(search.totalCount, 1)

    def test_FindEntity(self):
        """Test that the search object returns the expected information."""
        results = self.SERVER.instance.find(
            entityType="Task", filters=[[const.Fields.OWNERS, const.Operators.IS, self.SERVER.currentUser]]
        )
        self.assertIsNotNone(results)
