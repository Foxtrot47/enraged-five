from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    # Face Video Subtypes
    CUTSCENE = const.SubTypes.CUTSCENE
    IN_GAME = const.SubTypes.IN_GAME
    RANDOM_EVENT = const.SubTypes.RANDOM_EVENT
    AI = const.SubTypes.AI
    SCRIPTED_SPEECH = const.SubTypes.SCRIPTED_SPEECH
    PED = const.SubTypes.PED
    VFX = const.SubTypes.VFX
    ROM = const.SubTypes.ROM


class FaceCaptureVideo(baseEntity.BaseEntity):
    """
    Class that represents a Face Capture Video entity
    """

    ENTITY_TYPE = const.Entities.FACE_CAPTURE_VIDEO
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.ENTITY_TYPE,
        const.Fields.PROJECT,
        const.Fields.DYNAMIXYZ_PROFILE,
        const.Fields.DIALOGUE_TYPE,
    )
    NAME_FIELD = const.Fields.CODE

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def entityType(self):
        return self._getattr(const.Fields.ENTITY_TYPE)

    @entityType.setter
    def entityType(self, value):
        self._setattr(const.Fields.ENTITY_TYPE, value)

    @property
    def dynamixyzProfile(self):
        return self._getattr(const.Fields.DYNAMIXYZ_PROFILE)

    @dynamixyzProfile.setter
    def dynamixyzProfile(self, value):
        self._setattr(const.Fields.DYNAMIXYZ_PROFILE, value)

    # TODO: Replace with a subtype field
    @property
    def dialogueType(self):
        return self._getattr(const.Fields.DIALOGUE_TYPE)

    @dialogueType.setter
    def dialogueType(self, value):
        self._setattr(const.Fields.DIALOGUE_TYPE, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return FaceCaptureVideo

    @property
    def subtypes(self):
        return SubTypes
