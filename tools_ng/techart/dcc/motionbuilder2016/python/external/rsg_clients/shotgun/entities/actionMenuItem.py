from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class ActionMenuItem(baseEntity.BaseEntity):
    """
    Class that represents a Attachment entity.
    Attachment entities are considered files and are displayed as such in the shotgun interface
    """

    ENTITY_TYPE = const.Entities.ACTION_MENU_ITEM
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    DEFAULT_FIELDS = (const.Fields.ID, const.Fields.TITLE, const.Fields.ENTITY_TYPE)
    NAME_FIELD = const.Fields.TITLE

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def entityType(self):
        """
        EntityType data detailing what changes the event entailed

        Returns:
            dict
        """
        return self._getattr(const.Fields.ENTITY_TYPE)

    @entityType.setter
    def entityType(self, value):
        """
        Sets the entityType data detailing what changes the event entailed

        Arguments:
            value (dict): the details of the changes that emitted this event
        """
        self._setattr(const.Fields.ENTITY_TYPE, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return ActionMenuItem
