"""
Module for utilities relating to shotgun events
"""
from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class EventLogEntry(baseEntity.BaseEntity):
    """
    Class that represents an event log entry
    """

    ENTITY_TYPE = const.Entities.EVENT_LOG_ENTRY
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.PROJECT,
        const.Fields.USER,
        const.Fields.ENTITY,
        const.Fields.EVENT_TYPE,
        const.Fields.DESCRIPTION,
        const.Fields.META,
        const.Fields.ATTRIBUTE_NAME,
        const.Fields.SESSION_UUID,
        const.Fields.ATTRIBUTE_NAME,
        const.Fields.DATE_CREATED,
    )
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    NAME_FIELD = None  # This entity doesn't really have a field that is a name

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def description(self):
        """
        Summary of the event

        Returns:
            str
        """
        return self._getattr(const.Fields.DESCRIPTION)

    @description.setter
    def description(self, value):
        """
        Sets the summary of the event

        Arguments:
            value (str): summary of the event
        """
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def user(self):
        """
        User that authored this event

        Returns:
            rsg_core.shotgun.entities.humanUser
        """
        return self._getattr(const.Fields.USER)

    @user.setter
    def user(self, value):
        """
        Sets the user who authored this the event
    
        Arguments:
            value (rsg_core.shotgun.entities.humanUser): the user who authored this event
        """
        self._setattr(const.Fields.USER, value)

    @property
    def attributeName(self):
        """
        Name of the attribute that triggered the event if there is one

        Returns:
            str
        """
        return self._getattr(const.Fields.ATTRIBUTE_NAME)

    @attributeName.setter
    def attributeName(self, value):
        """
        Sets the attribute that triggered the event

        Arguments:
            value (str): name of the attribute that triggered the event
        """
        self._setattr(const.Fields.ATTRIBUTE_NAME, value)

    @property
    def eventType(self):
        """
        the type of event this is

        Returns:
            str
        """
        value = self._getattr(const.Fields.EVENT_TYPE)
        if isinstance(value, basestring):
            value = const.EventType(value)
            self.eventType = value
        return value

    @eventType.setter
    def eventType(self, value):
        """
        Sets the type of event this is

        Arguments:
            value (str): the type of the event
        """
        self._setattr(const.Fields.EVENT_TYPE, value)

    @property
    def meta(self):
        """
        Metadata detailing what changes the event entailed

        Returns:
            dict
        """
        return self._getattr(const.Fields.META)

    @meta.setter
    def meta(self, value):
        """
        Sets the metadata detailing what changes the event entailed

        Arguments:
            value (dict): the details of the changes that emitted this event
        """
        self._setattr(const.Fields.META, value)

    @property
    def entity(self):
        """
        The entity that triggered this event

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        return self._getattr(const.Fields.ENTITY)

    @entity.setter
    def entity(self, value):
        """
        Sets the entity that triggered this event

        Arguments:
            value (rsg_core.shotgun.entities.baseEntity.BaseEntity): the entity that triggered this event
        """
        if isinstance(value, basestring):
            value = const.EventTypes.getEventType(value)
        self._setattr(const.Fields.ENTITY, value)

    @property
    def createdAt(self):
        """
        The date and time this event was created

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.DATE_CREATED)

    @createdAt.setter
    def createdAt(self, value):
        """
        Sets the date and time this event was created

        Arguments:
            value (dateTime.datetime): the date and time that this event was created
        """
        self._setattr(const.Fields.DATE_CREATED, value)

    @property
    def sessionUuid(self):
        """
        The uuid for the session in which this event took place.
        Multiple events may share a sessions id

        Returns:
            str
        """
        return self._getattr(const.Fields.SessionUuid)

    @sessionUuid.setter
    def sessionUuid(self, value):
        """
        Sets the uuid for the session in which this event took place.

        Arguments:
            value (str): the uuid for the session
        """
        self._setattr(const.Fields.SessionUuid, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return EventLogEntry
