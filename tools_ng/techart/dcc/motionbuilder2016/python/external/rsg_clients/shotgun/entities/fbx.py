from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity
from rsg_core_py.decorators import deprecated


class Fbx(baseEntity.BaseEntity):
    """
    Class that represents a Fbx entity
    """

    ENTITY_TYPE = const.Entities.SHOT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.PROJECT,
        const.Fields.STATUS,
        const.Fields.STATUS_NAME,
        const.Fields.CONCAT,
        const.Fields.TASKS,
        const.Fields.PERFORCE_PATH,
        const.Fields.WATCHSTAR_ID,
        const.Fields.VERSIONS,
        const.Fields.SHOT_TYPE,
    )
    NAME_FIELD = const.Fields.CODE
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def project(self):
        """ The shotgun project associated with this entity """
        return self._getattr(const.Fields.PROJECT)

    @project.setter
    def project(self, value):
        """
        The shotgun project to associate with this entity

        Arguments:
             value (rsg_core.shotgun.entities.project.Project): project to set
        """
        self._setattr(const.Fields.PROJECT, value)

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def status(self):
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        return const.Statuses.getName(self.status)

    @property
    def tasks(self):
        return self._getmutable(const.Fields.TASKS, mutableType=list)

    @tasks.setter
    def tasks(self, value):
        self._setattr(const.Fields.TASKS, value, isMutable=True)

    @property
    def perforcePath(self):
        return self._getattr(const.Fields.PERFORCE_PATH)

    @perforcePath.setter
    def perforcePath(self, value):
        self._setattr(const.Fields.PERFORCE_PATH, value)

    @property
    def feature(self):
        return self._getattr(const.Fields.CONCAT)

    @feature.setter
    def feature(self, value):
        self._setattr(const.Fields.CONCAT, value)

    @property
    def watchstarId(self):
        return self._getattr(const.Fields.WATCHSTAR_ID)

    @watchstarId.setter
    def watchstarId(self, value):
        self._setattr(const.Fields.WATCHSTAR_ID, value)

    @property
    def subtype(self):
        return self._getattr(const.Fields.SHOT_TYPE)

    @subtype.setter
    def subtype(self, value):
        self._setattr(const.Fields.SHOT_TYPE, value)

    # TODO: Deprecated, replace existing usages of this property with feature instead
    @property
    @deprecated.deprecated("Use the feature property instead")
    def concat(self):
        return self._getattr(const.Fields.CONCAT)

    @concat.setter
    @deprecated.deprecated("Use the feature property instead")
    def concat(self, value):
        self._setattr(const.Fields.CONCAT, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Fbx

    def getByWatchstarId(self, watchstarId, filters=None):
        """Gets the entity based on the watchstar id associated with it.

        Args:
            watchstarId (int): the unique watchstar id to look for
            filters (list of list): list of additional filters to include for this search.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.WATCHSTAR_ID, const.Operators.IS, watchstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None
