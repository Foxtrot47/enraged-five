"""Exceptions classes to be used in Shotgun clients implementation."""
import re
from rsg_core_py.exceptions import types
from rsg_clients.shotgun import const
from rsg_clients import exceptions

import shotgun_api3


class ShotgunException(shotgun_api3.ShotgunError, exceptions.RestException):
    """Base Shotgun Error."""


class ShotgunServerError(ShotgunException, exceptions.ServerError):
    """Error from status code 500."""

    ERROR_CODE = 500
    DEFAULT_MESSAGE = "Unhandled Error by the Shotgun Team"


class NotAuthenticated(ShotgunException, types.AuthenticationError):
    """Could not authenticate against the server."""

    ERROR_CODE = 401
    DEFAULT_MESSAGE = "The current user is not authenticated to access the current server."


class EntityExists(ShotgunException):
    """Error thrown as a result of an entity already existing in Shotgun."""

    def __init__(self, message=None):
        """Initializer.

        Args:
            message (str): Error message.
        """
        super(EntityExists, self).__init__(message or "This entity already exists on Shotgun")


class EntityDoesNotExist(ShotgunException):
    """Error thrown as a result of an entity not existing in Shotgun."""

    def __init__(self, message=None):
        """Initializer.

        Args:
            message (str): Error message.
        """
        super(EntityDoesNotExist, self).__init__(message or "This entity does not exist on Shotgun")


class InvalidEventName(ShotgunException):
    """Error thrown as a result of an invalid event name."""

    def __init__(self, eventName):
        """Initializer.

        eventName (str): The event name that led to this exception being raised.
        """
        super(InvalidEventName, self).__init__("{} is not a valid event name".format(eventName))


class InvalidServer(ShotgunException):
    """An invalid server or no server has been set."""

    def __init__(self, message=None):
        """Initializer.

        Args:
            message (str): Error message.
        """
        self.message = message or "A server has not been set for this connection"
        super(InvalidServer, self).__init__(self.message)


class InvalidField(ShotgunException):
    """An error for attempting to access a field that this entity does not support."""

    def __init__(self, message=None, fieldName=None, entityType=None):
        """Initializer.

        Args:
            message (str): Error message.
            fieldName (str): Name of the field that was invalid.
            entityType (str): name of the entity type that doesn't have access to this field.
        """
        if not message:
            message = (
                "{} field is not supported by {} entity type\n"
                "Extend the DEFAULT_LIST property of this entity type to access this data.".format(
                    fieldName or "This", entityType or "this"
                )
            )
        self.message = message
        super(InvalidField, self).__init__(self.message)


class FolderCreationError(ShotgunException, exceptions.FolderCreationError):
    """System errored out during the creation of the folder."""

    def __init__(self, path, message=None):
        """Initializer.

        Args:
            path (str): Path to the folder that could not be created.
            message (str): Error message.
        """
        exceptions.FolderCreationError.__init__(self, path, message=message)
        ShotgunException.__init__(self, self.message)


class ServerUnavailable(ShotgunException, exceptions.ServerUnavailable):
    """Server is unavailable and can't be reached."""

    # TODO: Update regex with more relevant searches
    REGEX = re.compile(
        "(The CacheManager has been shut down)|(Session/EntityManager is closed)|"
        "(Can not read response from server)|(BeanCreationNotAllowedException)"
    )

    def __init__(self, server, message=None, url=None, includeErrorCode=None, connection=None):
        """Overrides inherited method.

        Args:
            server (rsg_clients.shotgun.servers.BaseServer): server that raised

            message (str, optional): error message to set for the error.
            url (str, optional): The url that raised the error
            includeErrorCode (bool, optional): Include the error code in the error message
        """
        exceptions.ServerUnavailable.__init__(
            self,
            client=const.AppInfo.NAME,
            server=server,
            message=message,
            url=url,
            includeErrorCode=includeErrorCode,
            connection=connection,
        )


class NoDiskSpace(ServerUnavailable):
    """Error for when the server is out of disk space."""

    def __init__(self, message=None):
        """Initializer.

        Args:
            message (str): Error message.
        """
        self.message = message or "No space left on device, contact IT to check free disk space on server"
        super(NoDiskSpace, self).__init__(self.message)


class ThumbnailDoesNotExist(ShotgunException):
    """Error for when a thumbnail is attempted to be accessed but does not exist."""

    def __init__(self, message=None):
        """Initializer.

        Args:
            message (str): Error message.
        """
        self.message = message or "Thumbnail does not exist."
        super(ThumbnailDoesNotExist, self).__init__(self.message)
