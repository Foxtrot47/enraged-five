"""Non-project-specific constants to be used in Shotgun clients implementations.
Most of the contants in this file are kept in json data, so we can share them with the C# library
and eventually other tools/environments."""

import os
import collections

from rsg_core.configs import main as config
from rsg_core_py.decorators import classProperty
from rsg_core_py.metaclasses import attributesLazyLoader
from rsg_clients import const


NameKeyPair = collections.namedtuple("NameKeyPair", ["name", "key"])


class AppInfo(object):
    """Info about the clients.shotgun module."""

    NAME = "Shotgun"
    VERSION = "0.41.2"
    TITLE = "{0} (Version {1})".format(NAME, VERSION)
    DATE_TIME_FORMAT = "%Y-%m-%d"


class Servers(object):
    """Shotgun server we use."""

    PRODUCTION = "shotgun"
    DEVELOPMENT = "shotgundev"
    MAPPINGS = {const.Servers.PRODUCTION: PRODUCTION, const.Servers.DEVELOPMENT: DEVELOPMENT}

    @classmethod
    def getRestUrl(cls, server):
        """Builds the base url for running rest commands on the given server.

        Args:
            server (str): name of the server to build the url for.

        Returns:
            str
        """
        url = "{}/api/v1".format(cls.getServerUrl(server))
        return url

    @staticmethod
    def getServerUrl(server, isAPI=False):
        """Gets the url path to the given server.

        Args:
            server (str): server name

        Returns:
            str
        """
        if isAPI:
            return "https://{}-api.rockstargames.com".format(server)
        return "https://{}.rockstargames.com".format(server)

    @classmethod
    def getType(cls, server):
        """The type of server of that is being hit (production or development).

        Args:
            server (str): the name of the shotgun server

        Returns:
            str
        """
        for key, value in cls.MAPPINGS.iteritems():
            if server == value:
                return key


class Paths(object):
    """Paths to resources."""

    ROOT_PATH = os.path.dirname(__file__)
    PLUGIN_DIRECTORY = os.path.join(ROOT_PATH, "plugins")
    ENTITY_DIRECTORY = os.path.join(ROOT_PATH, "entities")
    RESOURCES_DIRECTORY = os.path.join(ROOT_PATH, "resources")
    IMAGES_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "images")
    LOG_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "logs")
    CONFIG_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "configs")
    SEARCHES_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "searches")
    TESTS_DIRECTORY = os.path.join(RESOURCES_DIRECTORY, "tests")
    TEMPLATE_DAEMON_CONFIG = os.path.join(CONFIG_DIRECTORY, "templateEventDaemon.conf")
    DEFAULT_USER_THUMBNAIL = os.path.join(IMAGES_DIRECTORY, "default_thumbnail.png")
    JSON_DATA_DIRECTORY = os.path.join(ROOT_PATH, "resources", "data")
    SSL_CERTIFICATE = config.system.environment.sslCertificate or config.system.environment.techartProject.sslCertificate


class Applications(object):
    """Applications invoking events on the Shotgun Server.

    It may not always be Shotgun itself calling events
    """

    SHOTGUN = "Shotgun"


class Endpoints(object):
    """The enpoints available to the rest api."""

    AUTH = "auth"
    ACCESS_TOKEN = "access_token"
    ENTITY = "entity"

    # Entity endpoints
    TASK = "task"

    # Entity Support Endpoints
    SEARCH = "_search"
    SUMMARIZE = "_summarize"


class ContentType(object):
    """The type of of content that a REST command should expect or return.

    They are included in the header of the rest call.
    """

    FILE = "*/*"
    BASIC_FILTER = "application/vnd+shotgun.api3_array+json"
    COMPLEX_FILTER = "application/vnd+shotgun.api3_hash+json"


class SummaryType(object):
    """The valid types for summarizing data from shotgun using the summarize endpoint."""

    COUNT = "count"


class Entities(object):
    """The available entities on our shotgun server.

    Note:
        This list is currently incomplete.
    """

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunEntities.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    @classProperty.classProperty
    def ACTION_MENU_ITEM(cls):
        return cls.getData("ACTION_MENU_ITEM", None)

    @classProperty.classProperty
    def APP_WELCOME(cls):
        return cls.getData("APP_WELCOME", None)

    @classProperty.classProperty
    def APP_WELCOME_USER_CONNECTION(cls):
        return cls.getData("APP_WELCOME_USER_CONNECTION", None)

    @classProperty.classProperty
    def ASSET(cls):
        return cls.getData("ASSET", None)

    @classProperty.classProperty
    def ATTACHMENT(cls):
        return cls.getData("ATTACHMENT", None)

    @classProperty.classProperty
    def API_USER(cls):
        return cls.getData("API_USER", None)

    @classProperty.classProperty
    def BANNER_USER_CONNECTION(cls):
        return cls.getData("BANNER_USER_CONNECTION", None)

    @classProperty.classProperty
    def BRAND(cls):
        return cls.getData("BRAND", None)

    @classProperty.classProperty
    def CASTING(cls):
        return cls.getData("CASTING", None)

    @classProperty.classProperty
    def COMPONENT(cls):
        return cls.getData("COMPONENT", None)

    @classProperty.classProperty
    def DEPARTMENT(cls):
        return cls.getData("DEPARTMENT", None)

    @classProperty.classProperty
    def DEADLINE(cls):
        return cls.getData("DEADLINE", None)

    @classProperty.classProperty
    def DISPLAY_COLUMN(cls):
        return cls.getData("DISPLAY_COLUMN", None)

    @classProperty.classProperty
    def ENTITY_FIELD_PREF(cls):
        return cls.getData("ENTITY_FIELD_PREF", None)

    @classProperty.classProperty
    def EVENT_LOG_ENTRY(cls):
        return cls.getData("EVENT_LOG_ENTRY", None)

    @classProperty.classProperty
    def FACE_CAPTURE_VIDEO(cls):
        return cls.getData("FACE_CAPTURE_VIDEO", None)

    @classProperty.classProperty
    def GOAL(cls):
        return cls.getData("GOAL", None)

    @classProperty.classProperty
    def GROUP(cls):
        return cls.getData("GROUP", None)

    @classProperty.classProperty
    def HUMAN_USER(cls):
        return cls.getData("HUMAN_USER", None)

    @classProperty.classProperty
    def INGAME(cls):
        return cls.getData("INGAME", None)

    @classProperty.classProperty
    def LEGAL_CLEARANCE(cls):
        return cls.getData("LEGAL_CLEARANCE", None)

    @classProperty.classProperty
    def LOCATION(cls):
        return cls.getData("LOCATION", None)

    @classProperty.classProperty
    def NOTE(cls):
        return cls.getData("NOTE", None)

    @classProperty.classProperty
    def NOTE_LINK(cls):
        return cls.getData("NOTE_LINK", None)

    @classProperty.classProperty
    def PACKAGES(cls):
        return cls.getData("PACKAGES", None)

    @classProperty.classProperty
    def PAGE(cls):
        return cls.getData("PAGE", None)

    @classProperty.classProperty
    def PAGE_SETTING(cls):
        return cls.getData("PAGE_SETTING", None)

    @classProperty.classProperty
    def PHOTOGRAMMETRY(cls):
        return cls.getData("PHOTOGRAMMETRY", None)

    @classProperty.classProperty
    def PRELIM_CS(cls):
        return cls.getData("PRELIM_CS", None)

    @classProperty.classProperty
    def PRELIM_IG(cls):
        return cls.getData("PRELIM_IG", None)

    @classProperty.classProperty
    def PROJECT(cls):
        return cls.getData("PROJECT", None)

    @classProperty.classProperty
    def PROJECT_USER_CONNECTION(cls):
        return cls.getData("PROJECT_USER_CONNECTION", None)

    @classProperty.classProperty
    def READING(cls):
        return cls.getData("READING", None)

    @classProperty.classProperty
    def REPLY(cls):
        return cls.getData("REPLY", None)

    @classProperty.classProperty
    def ROCKSTAR_ASSET(cls):
        return cls.getData("ROCKSTAR_ASSET", None)

    @classProperty.classProperty
    def ROCKSTAR_CHARACTER(cls):
        return cls.getData("ROCKSTAR_CHARACTER", None)

    @classProperty.classProperty
    def ROCKSTAR_PROJECT(cls):
        return cls.getData("ROCKSTAR_PROJECT", None)

    @classProperty.classProperty
    def ROM(cls):
        return cls.getData("ROM", None)

    @classProperty.classProperty
    def SCRIPT(cls):
        return cls.getData("SCRIPT", None)

    @classProperty.classProperty
    def SEQUENCE(cls):
        return cls.getData("SEQUENCE", None)

    @classProperty.classProperty
    def SHOT(cls):
        return cls.getData("SHOT", None)

    @classProperty.classProperty
    def STATUS(cls):
        return cls.getData("STATUS", None)

    @classProperty.classProperty
    def STEP(cls):
        return cls.getData("STEP", None)

    @classProperty.classProperty
    def STRAND(cls):
        return cls.getData("STRAND", None)

    @classProperty.classProperty
    def TAG(cls):
        return cls.getData("TAG", None)

    @classProperty.classProperty
    def TALENT(cls):
        return cls.getData("TALENT", None)

    @classProperty.classProperty
    def TASK(cls):
        return cls.getData("TASK", None)

    @classProperty.classProperty
    def TICKET(cls):
        return cls.getData("TICKET", None)

    @classProperty.classProperty
    def UI_VISIBILITY_RULE(cls):
        return cls.getData("UI_VISIBILITY_RULE", None)

    @classProperty.classProperty
    def USER(cls):
        return cls.getData("USER", None)

    @classProperty.classProperty
    def VERSION(cls):
        return cls.getData("VERSION", None)

    # Renamed Entities

    @classProperty.classProperty
    def CONCAT(cls):
        return cls.getData("CONCAT", None)

    @classProperty.classProperty
    def FBX(cls):
        return cls.getData("FBX", None)


class Events(object):
    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunEvents.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    # Common events

    @classProperty.classProperty
    def NEW(cls):
        return cls.getData("NEW", None)

    @classProperty.classProperty
    def CREATE(cls):
        return cls.getData("CREATE", None)

    @classProperty.classProperty
    def CHANGE(cls):
        return cls.getData("CHANGE", None)

    @classProperty.classProperty
    def RETIREMENT(cls):
        return cls.getData("RETIREMENT", None)

    @classProperty.classProperty
    def REVIVAL(cls):
        return cls.getData("REVIVAL", None)

    # Entity Specific Events

    @classProperty.classProperty
    def LOGIN(cls):
        return cls.getData("LOGIN", None)

    @classProperty.classProperty
    def VIEW(cls):
        return cls.getData("VIEW", None)

    @classProperty.classProperty
    def PASSWORD_CHANGE(cls):
        return cls.getData("PASSWORD_CHANGE", None)

    @classProperty.classProperty
    def TRIGGERED(cls):
        return cls.getData("TRIGGERED", None)

    # Custom Events

    @classProperty.classProperty
    def BUTTON_TRIGGER(cls):
        return cls.getData("BUTTON_TRIGGER", None)


class EventType(object):
    """Event types."""

    __cache__ = {}
    _EXCEPTIONS = ("ClientUser", "Metering")

    def __new__(cls, name):
        """Overrides built-in method.

        It caches the event name so the same instance is returned.

        Args:
            name (str): Name of the event being invoked by shotgun.
        """
        instance = cls.__cache__.get(name, None)
        if instance is None:
            instance = object.__new__(cls)
        return instance

    def __init__(self, name):
        """Constructor.

        Args:
            name (string): name of the event being invoked by shotgun.
        """
        instance = self.__class__.__cache__.get(name, None)
        if instance is not None:
            return
        self.name = name
        parts = name.split("_")
        # There are some valid events that are only two words
        if parts[0] in self._EXCEPTIONS:
            parts[0:0] = ["Shotgun"]
        elif parts[-1] in self._EXCEPTIONS:
            parts[1:1] = ["None"]

        if len(parts) < 3:
            raise ValueError("{} is not a valid event name".format(name))
        self.application = parts[0]
        self.entity = "_".join(parts[1:-1])
        self.event = parts[-1]

        self.__class__.__cache__[name] = self

    def __eq__(self, other):
        """Determines if this object's name string is equal to a given value.

        Args:
            other (str): String to compare to this object's name.
        """
        return self.name == other

    def __ne__(self, other):
        """Determines if this object's name string is not equal to another value.

        Args:
            other (str): String to compare to this object's name.
        """
        return self.name != other


class EventTypes(object):
    """Events triggered by the shotgun api.

    This currently does not cover all the events available in shotgun

    In general shotgun will categorize events in the following format
    Application_Entity_Event

    For example:
        Shotgun_Task_Change

    There are a few custom events that break this paradigm

    For more info check out:
    http://developer.shotgunsoftware.com/python-api/event_types.html
    """

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunEventTypes.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    @classProperty.classProperty
    def ACTION_MENU_ITEM_TRIGGERED(cls):
        return EventType(cls.getData("ACTION_MENU_ITEM_TRIGGERED", None))

    @classProperty.classProperty
    def APP_WELCOME_CHANGE(cls):
        return EventType(cls.getData("APP_WELCOME_CHANGE", None))

    @classProperty.classProperty
    def APP_WELCOME_USER_CONNECTION_NEW(cls):
        return EventType(cls.getData("APP_WELCOME_USER_CONNECTION_NEW", None))

    @classProperty.classProperty
    def ATTACHMENT_VIEW(cls):
        return EventType(cls.getData("ATTACHMENT_VIEW", None))

    @classProperty.classProperty
    def ASSET_CHANGE(cls):
        return EventType(cls.getData("ASSET_CHANGE", None))

    @classProperty.classProperty
    def DISPLAY_COLUMN_NEW(cls):
        return EventType(cls.getData("DISPLAY_COLUMN_NEW", None))

    @classProperty.classProperty
    def DISPLAY_COLUMN_CHANGE(cls):
        return EventType(cls.getData("DISPLAY_COLUMN_CHANGE", None))

    @classProperty.classProperty
    def DISPLAY_COLUMN_DELETE(cls):
        return EventType(cls.getData("DISPLAY_COLUMN_DELETE", None))

    @classProperty.classProperty
    def DISPLAY_COLUMN_RETIREMENT(cls):
        return EventType(cls.getData("DISPLAY_COLUMN_RETIREMENT", None))

    @classProperty.classProperty
    def ENTITY_FIELD_PREF_NEW(cls):
        return EventType(cls.getData("ENTITY_FIELD_PREF_NEW", None))

    @classProperty.classProperty
    def HUMAN_USER_NEW(cls):
        return EventType(cls.getData("HUMAN_USER_NEW", None))

    @classProperty.classProperty
    def HUMAN_USER_CHANGE(cls):
        return EventType(cls.getData("HUMAN_USER_CHANGE", None))

    @classProperty.classProperty
    def HUMAN_USER_RETIREMENT(cls):
        return EventType(cls.getData("HUMAN_USER_RETIREMENT", None))

    @classProperty.classProperty
    def HUMAN_USER_REVIVAL(cls):
        return EventType(cls.getData("HUMAN_USER_REVIVAL", None))

    @classProperty.classProperty
    def NOTE_NEW(cls):
        return EventType(cls.getData("NOTE_NEW", None))

    @classProperty.classProperty
    def NOTE_CHANGE(cls):
        return EventType(cls.getData("NOTE_CHANGE", None))

    @classProperty.classProperty
    def NOTE_LINK_NEW(cls):
        return EventType(cls.getData("NOTE_LINK_NEW", None))

    @classProperty.classProperty
    def PAGE_NEW(cls):
        return EventType(cls.getData("PAGE_NEW", None))

    @classProperty.classProperty
    def PAGE_CHANGE(cls):
        return EventType(cls.getData("PAGE_CHANGE", None))

    @classProperty.classProperty
    def PAGE_SETTING_NEW(cls):
        return EventType(cls.getData("PAGE_SETTING_NEW", None))

    @classProperty.classProperty
    def PAGE_SETTING_CHANGE(cls):
        return EventType(cls.getData("PAGE_SETTING_CHANGE", None))

    @classProperty.classProperty
    def PASSWORD_CHANGE(cls):
        return EventType(cls.getData("PASSWORD_CHANGE", None))

    @classProperty.classProperty
    def PROJECT_CHANGE(cls):
        return EventType(cls.getData("PROJECT_CHANGE", None))

    @classProperty.classProperty
    def PROJECT_USER_CONNECTION_NEW(cls):
        return EventType(cls.getData("PROJECT_USER_CONNECTION_NEW", None))

    @classProperty.classProperty
    def READING_CHANGE(cls):
        return EventType(cls.getData("READING_CHANGE", None))

    @classProperty.classProperty
    def SEQUENCE_CHANGE(cls):
        return EventType(cls.getData("SEQUENCE_CHANGE", None))

    @classProperty.classProperty
    def SHOT_CHANGE(cls):
        return EventType(cls.getData("SHOT_CHANGE", None))

    @classProperty.classProperty
    def STATUS_CHANGE(cls):
        return EventType(cls.getData("STATUS_CHANGE", None))

    @classProperty.classProperty
    def STEP_NEW(cls):
        return EventType(cls.getData("STEP_NEW", None))

    @classProperty.classProperty
    def STEP_CHANGE(cls):
        return EventType(cls.getData("STEP_CHANGE", None))

    @classProperty.classProperty
    def UI_VISIBILITY_RULE_NEW(cls):
        return EventType(cls.getData("UI_VISIBILITY_RULE_NEW", None))

    @classProperty.classProperty
    def UI_VISIBILITY_RULE_CHANGE(cls):
        return EventType(cls.getData("UI_VISIBILITY_RULE_CHANGE", None))

    @classProperty.classProperty
    def UI_VISIBILITY_RULE_RETIREMENT(cls):
        return EventType(cls.getData("UI_VISIBILITY_RULE_RETIREMENT", None))

    @classProperty.classProperty
    def USER_LOGIN(cls):
        return EventType(cls.getData("USER_LOGIN", None))

    @classProperty.classProperty
    def TASK_CHANGE(cls):
        return EventType(cls.getData("TASK_CHANGE", None))

    @classProperty.classProperty
    def VERSION_CHANGE(cls):
        return EventType(cls.getData("VERSION_CHANGE", None))

    @classProperty.classProperty
    def REVIEW_TOOLS_VERSION_VIEW(cls):
        return EventType(cls.getData("REVIEW_TOOLS_VERSION_VIEW", None))

    @classProperty.classProperty
    def CLIENT_USER_LOGOUT(cls):
        return EventType(cls.getData("CLIENT_USER_LOGOUT", None))

    @classProperty.classProperty
    def METERING(cls):
        return EventType(cls.getData("METERING", None))

    @classProperty.classProperty
    def TALENT_NEW(cls):
        return EventType(cls.getData("TALENT_NEW", None))

    @classProperty.classProperty
    def TALENT_CHANGE(cls):
        return EventType(cls.getData("TALENT_CHANGE", None))

    @staticmethod
    def getEventType(name):
        """Gets event type by name.

        Args:
            name (str): The name of the event type to get.
        """
        return EventType(name)


class Statuses(object):
    """List of statuses available in our shotgun server."""

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunStatuses.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader
    _nameDict = None

    @classProperty.classProperty
    def DEV_PLANNED(cls):
        return cls.getData("DEV_PLANNED", None)

    @classProperty.classProperty
    def DEV_FIXING(cls):
        """This is also the default in progress status code."""
        return cls.getData("DEV_FIXING", None)

    @classProperty.classProperty
    def DEV_REOPEN(cls):
        return cls.getData("DEV_REOPEN", None)

    @classProperty.classProperty
    def DEV_FIXED(cls):
        return cls.getData("DEV_FIXED", None)

    @classProperty.classProperty
    def TEST_VERIFIED(cls):
        return cls.getData("TEST_VERIFIED", None)

    @classProperty.classProperty
    def TEST_NEED_MORE_INFO(cls):
        """This is also the default pending status code"""
        return cls.getData("TEST_NEED_MORE_INFO", None)

    @classProperty.classProperty
    def CLOSED_FIXED(cls):
        """This is also the default confirmed status code"""
        return cls.getData("CLOSED_FIXED", None)

    @classProperty.classProperty
    def CLOSED_WAIVED(cls):
        return cls.getData("CLOSED_WAIVED", None)

    @classProperty.classProperty
    def CLOSED_DUPLICATE(cls):
        return cls.getData("CLOSED_DUPLICATE", None)

    @classProperty.classProperty
    def BLOCKED(cls):
        return cls.getData("BLOCKED", None)

    @classProperty.classProperty
    def BLOCKED_PLANNED(cls):
        return cls.getData("BLOCKED_PLANNED", None)

    @classProperty.classProperty
    def ON_HOLD(cls):
        return cls.getData("ON_HOLD", None)

    @classProperty.classProperty
    def READY_TO_START(cls):
        return cls.getData("READY_TO_START", None)

    # Non-Game Project Statuses

    @classProperty.classProperty
    def ACTIVE_CAST(cls):
        return cls.getData("ACTIVE_CAST", None)

    @classProperty.classProperty
    def AWAITING_REVIEW(cls):
        return cls.getData("AWAITING_REVIEW", None)

    @classProperty.classProperty
    def CAST_IN_REVIEW(cls):
        return cls.getData("CAST_IN_REVIEW", None)

    @classProperty.classProperty
    def DECLINED(cls):
        return cls.getData("DECLINED", None)

    @classProperty.classProperty
    def DO_NOT_USE(cls):
        return cls.getData("DO_NOT_USE", None)

    @classProperty.classProperty
    def FUTURE_CONSIDERATION(cls):
        return cls.getData("FUTURE_CONSIDERATION", None)

    @classProperty.classProperty
    def INACTIVE_CAST(cls):
        return cls.getData("INACTIVE_CAST", None)

    # Note Statuses

    @classProperty.classProperty
    def OPEN(cls):
        return cls.getData("OPEN", None)

    @classProperty.classProperty
    def CLOSED(cls):
        return cls.getData("CLOSED", None)

    @classProperty.classProperty
    def READY(cls):
        return cls.getData("READY", None)

    @classProperty.classProperty
    def COMPLETE(cls):
        return cls.getData("COMPLETE", None)

    # Human User Status

    @classProperty.classProperty
    def ACTIVE(cls):
        return cls.getData("ACTIVE", None)

    @classProperty.classProperty
    def DISABLED(cls):
        return cls.getData("DISABLED", None)

    @classProperty.classProperty
    def IN_PROGRESS_LIST(cls):
        """List of statuses that mean a bug is in progress relative to Shotgun.

        Returns:
            tuple
        """
        return (cls.DEV_REOPEN, cls.DEV_FIXING)

    @classProperty.classProperty
    def MAPPINGS(cls):
        """Mapping the UI names of the statuses to their code names.

        Notes:
            This method had to be created to work around an issue with the classProperty decorator.
            The decorator does not resolve properly until the class has been processed by Python.

        Returns:
            dict
        """
        if not cls._nameDict:
            cls._nameDict = {
                "dev (fixing)": cls.DEV_FIXING,
                "dev (reopen)": cls.DEV_REOPEN,
                "dev (fixed, waiting build)": cls.DEV_FIXED,
                "dev (planned)": cls.DEV_PLANNED,
                "test (verifying)": cls.TEST_VERIFIED,
                "test (need more info)": cls.TEST_NEED_MORE_INFO,
                "closed (fixed)": cls.CLOSED_FIXED,
                "closed (waived)": cls.CLOSED_WAIVED,
                "closed (duplicate)": cls.CLOSED_DUPLICATE,
                "blocked (planned)": cls.BLOCKED_PLANNED,
                "blocked": cls.BLOCKED,
                "on hold": cls.ON_HOLD,
                "ready to start": cls.READY_TO_START,
                # Non-Game Project Statuses
                "Active Cast": cls.ACTIVE_CAST,
                "Awaiting": cls.AWAITING_REVIEW,
                "Cast In Review": cls.CAST_IN_REVIEW,
                "Do Not Use": cls.DO_NOT_USE,
                "Inactive Cast": cls.INACTIVE_CAST,
                "Uncast": cls.DECLINED,
            }
        return cls._nameDict

    @classmethod
    def getByName(cls, name):
        """Gets the corresponding status based on the given name.

        Args:
            name (str): String version of the status.

        Returns:
            str: The corresponding Shotgun status code, or None if the name cannot be found in cls._nameDict.
        """
        return cls.MAPPINGS.get(name, None)

    @classmethod
    def getName(cls, status):
        """Gets the corresponding string value based on the given status code.

        Args:
            status (str): Shotgun status code.

        Returns:
            str: Corresponding status name if found, else None.
        """
        statusDict = {value: key for key, value in cls.MAPPINGS.iteritems()}
        return statusDict.get(status, None)

    @classmethod
    def isWorkingStatus(cls, status):
        """Returns whether this status represent a task that is currently in progress (is working).

        cls._inProgressList represents the set of statuses that correspond to a task currently in progress. This
        function determines whether a given status is within that set.

        Args:
            status (str): The code that represents a status.

        Returns:
            bool: Whether this status represents a task that is working.
        """
        return status in cls.IN_PROGRESS_LIST


class FieldTypes(object):
    """The types of fields available on Shotgun."""

    # Fields that can be created by the api
    CALCULATE = "calculated"
    CHECKBOX = "checkbox"
    CURRENCY = "currency"
    DATE = "date"
    DATE_AND_TIME = "date_time"
    DURATION = "duration"
    ENTITY = "entity"
    FLOAT = "float"
    FOOTAGE = "footage"
    LIST = "list"
    MULTI_ENTITY = "multi_entity"
    NUMBER = "number"
    PERCENT = "percent"
    STATUS_LIST = "status list"
    TEXT = "text"
    TIMECODE = "timecode"
    URL_TEMPLATE = "url"
    UUID = "uuid"


class Fields(object):
    """List of commonly accessed fields on our shotgun server.

    Note:
        Entities have access to different fields and the same "fields" may use different names.
    """

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunFields.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    @classProperty.classProperty
    def ID(cls):
        return cls.getData("ID", None)

    # Text Fields

    @classProperty.classProperty
    def NAME(cls):
        return cls.getData("NAME", None)

    @classProperty.classProperty
    def USERNAME(cls):
        return cls.getData("USERNAME", None)

    @classProperty.classProperty
    def JOB_TITLE(cls):
        return cls.getData("JOB_TITLE", None)

    @classProperty.classProperty
    def STUDIO(cls):
        return cls.getData("STUDIO", None)

    @classProperty.classProperty
    def CODE(cls):
        return cls.getData("CODE", None)

    @classProperty.classProperty
    def CONTENT(cls):
        return cls.getData("CONTENT", None)

    @classProperty.classProperty
    def SUBJECT(cls):
        return cls.getData("SUBJECT", None)

    @classProperty.classProperty
    def TITLE(cls):
        return cls.getData("TITLE", None)

    @classProperty.classProperty
    def EMAIL(cls):
        return cls.getData("EMAIL", None)

    @classProperty.classProperty
    def DESCRIPTION(cls):
        return cls.getData("DESCRIPTION", None)

    @classProperty.classProperty
    def NONDEFAULT_DESCRIPTION(cls):
        """description field name for entities that don't have a description field by default"""
        return cls.getData("NONDEFAULT_DESCRIPTION", None)

    @classProperty.classProperty
    def FILENAME(cls):
        return cls.getData("FILENAME", None)

    @classProperty.classProperty
    def STATUS_NAME(cls):
        return cls.getData("STATUS_NAME", None)

    @classProperty.classProperty
    def CAST_STATUS_NAME(cls):
        return cls.getData("CAST_STATUS_NAME", None)

    @classProperty.classProperty
    def SHORT_NAME(cls):
        return cls.getData("SHORT_NAME", None)

    @classProperty.classProperty
    def SHORTNAME(cls):
        return cls.getData("SHORTNAME", None)

    @classProperty.classProperty
    def MISSION_ID(cls):
        return cls.getData("MISSION_ID", None)

    @classProperty.classProperty
    def SCRIPT_NAME(cls):
        return cls.getData("SCRIPT_NAME", None)

    @classProperty.classProperty
    def STORY_DESCRIPTION(cls):
        return cls.getData("STORY_DESCRIPTION", None)

    @classProperty.classProperty
    def DEADLINE(cls):
        return cls.getData("DEADLINE", None)

    @classProperty.classProperty
    def ATTRIBUTE_NAME(cls):
        return cls.getData("ATTRIBUTE_NAME", None)

    @classProperty.classProperty
    def MAX_PATH(cls):
        return cls.getData("MAX_PATH", None)

    @classProperty.classProperty
    def HUB_LINK(cls):
        return cls.getData("HUB_LINK", None)

    @classProperty.classProperty
    def PROCESSED_MODEL_PATH(cls):
        return cls.getData("PROCESSED_MODEL_PATH", None)

    @classProperty.classProperty
    def SHOT_TYPE(cls):
        return cls.getData("SHOT_TYPE", None)

    @classProperty.classProperty
    def ENTITY_TYPE(cls):
        return cls.getData("ENTITY_TYPE", None)

    @classProperty.classProperty
    def ENTITY_SUBTYPE(cls):
        return cls.getData("ENTITY_SUBTYPE", None)

    @classProperty.classProperty
    def NOTE_TYPE(cls):
        return cls.getData("NOTE_TYPE", None)

    @classProperty.classProperty
    def ASSET_TYPE(cls):
        return cls.getData("ASSET_TYPE", None)

    @classProperty.classProperty
    def PRIORITY(cls):
        return cls.getData("PRIORITY", None)

    @classProperty.classProperty
    def TICKET_TYPE(cls):
        return cls.getData("TICKET_TYPE", None)

    @classProperty.classProperty
    def CLASS(cls):
        return cls.getData("CLASS", None)

    @classProperty.classProperty
    def EVENT_TYPE(cls):
        return cls.getData("EVENT_TYPE", None)

    @classProperty.classProperty
    def CONTENT_TYPE(cls):
        """Field on the attachment that tells us what kind of the attachment it is, if it's text/plain, image"""
        return cls.getData("CONTENT_TYPE", None)

    @classProperty.classProperty
    def FIRST_NAME(cls):
        return cls.getData("FIRST_NAME", None)

    @classProperty.classProperty
    def LAST_NAME(cls):
        return cls.getData("LAST_NAME", None)

    @classProperty.classProperty
    def PERFORCE_PATH(cls):
        return cls.getData("PERFORCE_PATH", None)

    @classProperty.classProperty
    def PERFORCE_NEUTRAL_PATH(cls):
        return cls.getData("PERFORCE_NEUTRAL_PATH", None)

    @classProperty.classProperty
    def BUG_SUMMARY(cls):
        return cls.getData("BUG_SUMMARY", None)

    @classProperty.classProperty
    def DYNAMIXYZ_PROFILE(cls):
        return cls.getData("DYNAMIXYZ_PROFILE", None)

    @classProperty.classProperty
    def DIALOGUE_TYPE(cls):
        return cls.getData("DIALOGUE_TYPE", None)

    @classProperty.classProperty
    def ESTIMATED_TIME_VALUE(cls):
        return cls.getData("ESTIMATED_TIME_VALUE", None)

    @classProperty.classProperty
    def SUGGESTED_ESTIMATED_TIME_VALUE(cls):
        return cls.getData("SUGGESTED_ESTIMATED_TIME_VALUE", None)

    @classProperty.classProperty
    def CHARACTER_TYPE(cls):
        return cls.getData("CHARACTER_TYPE", None)

    @classProperty.classProperty
    def GARMENT_TYPE(cls):
        return cls.getData("GARMENT_TYPE", None)

    @classProperty.classProperty
    def GARMENT_SUBTYPE(cls):
        return cls.getData("GARMENT_SUBTYPE", None)

    @classProperty.classProperty
    def GARMENT_FAMILY(cls):
        return cls.getData("GARMENT_FAMILY", None)

    @classProperty.classProperty
    def PARENTS(cls):
        return cls.getData("PARENTS", None)

    @classProperty.classProperty
    def GENDER(cls):
        return cls.getData("GENDER", None)

    @classProperty.classProperty
    def LEGAL_CLEARANCE_TYPE(cls):
        return cls.getData("LEGAL_CLEARANCE_TYPE", None)

    @classProperty.classProperty
    def CATEGORY(cls):
        return cls.getData("CATEGORY", None)

    @classProperty.classProperty
    def SUBCATEGORY(cls):
        return cls.getData("SUBCATEGORY", None)

    @classProperty.classProperty
    def INSPIRATION_INFORMATION(cls):
        return cls.getData("INSPIRATION_INFORMATION", None)

    @classProperty.classProperty
    def HEIGHT(cls):
        return cls.getData("HEIGHT", None)

    @classProperty.classProperty
    def VIRTUAL_HEIGHT(cls):
        return cls.getData("VIRTUAL_HEIGHT", None)
    
    @classProperty.classProperty
    def SPECIES(cls):
        return cls.getData("SPECIES", None)

    @classProperty.classProperty
    def DIALOGUE_STAR_NAME(cls):
        return cls.getData("DIALOGUE_STAR_NAME", None)

    @classProperty.classProperty
    def LOGIN(cls):
        return cls.getData("LOGIN", None)

    @classProperty.classProperty
    def LOCATION_SUBTYPE(cls):
        return cls.getData("LOCATION_SUBTYPE", None)

    @classProperty.classProperty
    def FACIAL_RIG_TYPE(cls):
        return cls.getData("FACIAL_RIG_TYPE", None)

    @classProperty.classProperty
    def ANIMAL_TYPE(cls):
        return cls.getData("ANIMAL_TYPE", None)

    @classProperty.classProperty
    def PROP_TYPE(cls):
        return cls.getData("PROP_TYPE", None)

    @classProperty.classProperty
    def VAULT_PATH(cls):
        return cls.getData("VAULT_PATH", None)

    @classProperty.classProperty
    def VAULT_ID(cls):
        return cls.getData("VAULT_ID", None)

    @classProperty.classProperty
    def INSPIRATION_NAMES(cls):
        return cls.getData("INSPIRATION_NAMES", None)

    @classProperty.classProperty
    def INSPIRATION_ADDRESS(cls):
        return cls.getData("INSPIRATION_ADDRESS", None)

    @classProperty.classProperty
    def INSPIRATION_MAP_LINK(cls):
        return cls.getData("INSPIRATION_MAP_LINK", None)

    @classProperty.classProperty
    def ADDITIONAL_INFORMATION(cls):
        return cls.getData("ADDITIONAL_INFORMATION", None)

    @classProperty.classProperty
    def ARTIST(cls):
        return cls.getData("ARTIST", None)

    # List Fields (behave the same as a text field but it errors out if the string is not in the list of values the field accepts)

    @classProperty.classProperty
    def WORKFLOW(cls):
        return cls.getData("WORKFLOW", None)

    @classProperty.classProperty
    def SIMILAR_IRL_SURROUNDINGS(cls):
        return cls.getData("SIMILAR_IRL_SURROUNDINGS", None)

    @classProperty.classProperty
    def IRL_LOCATION_SIGNAGE(cls):
        return cls.getData("IRL_LOCATION_SIGNAGE", None)

    @classProperty.classProperty
    def MAP_PHASE(cls):
        return cls.getData("MAP_PHASE", None)

    @classProperty.classProperty
    def GRID_TYPE(cls):
        return cls.getData("GRID_TYPE", None)

    # Boolean Fields

    @classProperty.classProperty
    def SINGLE_PLAYER(cls):
        return cls.getData("SINGLE_PLAYER", None)

    @classProperty.classProperty
    def ACTIVE(cls):
        return cls.getData("ACTIVE", None)

    @classProperty.classProperty
    def IN_BUGSTAR(cls):
        return cls.getData("IN_BUGSTAR", None)

    @classProperty.classProperty
    def SG_ARCHIVED(cls):
        return cls.getData("SG_ARCHIVED", None)

    @classProperty.classProperty
    # version uses sg_archived, project uses archived
    def ARCHIVED(cls):
        return cls.getData("ARCHIVED", None)

    @classProperty.classProperty
    def DISPLAY_AS_THUMBNAIL(cls):
        return cls.getData("DISPLAY_AS_THUMBNAIL", None)

    @classProperty.classProperty
    def IS_TEMPLATE(cls):
        return cls.getData("IS_TEMPLATE", None)

    # Int Fields

    @classProperty.classProperty
    def BUGSTAR_ID(cls):
        return cls.getData("BUGSTAR_ID", None)

    @classProperty.classProperty
    def BUGSTAR_ACTUAL_TIME(cls):
        return cls.getData("BUGSTAR_ACTUAL_TIME", None)

    @classProperty.classProperty
    def BUGSTAR_ESTIMATED_TIME(cls):
        return cls.getData("BUGSTAR_ESTIMATED_TIME", None)

    @classProperty.classProperty
    def BUGSTAR_DEADLINE(cls):
        return cls.getData("BUGSTAR_DEADLINE", None)

    @classProperty.classProperty
    def BUG_NUMBER(cls):
        return cls.getData("BUG_NUMBER", None)

    @classProperty.classProperty
    def USAGE(cls):
        return cls.getData("USAGE", None)

    @classProperty.classProperty
    def REVISION(cls):
        return cls.getData("REVISION", None)

    @classProperty.classProperty
    def DIALOGUE_ID(cls):
        return cls.getData("DIALOGUE_ID", None)

    @classProperty.classProperty
    def SHOOT_NUMBER(cls):
        return cls.getData("SHOOT_NUMBER", None)

    @classProperty.classProperty
    def VERSION(cls):
        return cls.getData("VERSION", None)

    @classProperty.classProperty
    def START_FRAME(cls):
        return cls.getData("START_FRAME", None)

    @classProperty.classProperty
    def END_FRAME(cls):
        return cls.getData("END_FRAME", None)
    
    @classProperty.classProperty
    def FRAME_RATE(cls):
        return cls.getData("FRAME_RATE", None)

    @classProperty.classProperty
    def SCAN_ID(cls):
        return cls.getData("SCAN_ID", None)

    @classProperty.classProperty
    def PROGRESS(cls):
        return cls.getData("PROGRESS", None)

    @classProperty.classProperty
    def POSITION(cls):
        return cls.getData("POSITION", None)

    @classProperty.classProperty
    def WATCHSTAR_ID(cls):
        return cls.getData("WATCHSTAR_ID", None)

    @classProperty.classProperty
    def EMPLOYEE_ID(cls):
        return cls.getData("EMPLOYEE_ID", None)

    @classProperty.classProperty
    def BACKEND_STEP_ID(cls):
        return cls.getData("BACKEND_STEP_ID", None)

    # Status Related Fields

    @classProperty.classProperty
    def STATUS(cls):
        return cls.getData("STATUS", None)

    @classProperty.classProperty
    def CAST_STATUS(cls):
        return cls.getData("CAST_STATUS", None)

    # Time Related Fields

    @classProperty.classProperty
    def DUE_DATE(cls):
        return cls.getData("DUE_DATE", None)

    @classProperty.classProperty
    def START_DATE(cls):
        return cls.getData("START_DATE", None)

    @classProperty.classProperty
    def SUBMISSION_DATE(cls):
        return cls.getData("SUBMISSION_DATE", None)

    @classProperty.classProperty
    def SG_DUE_DATE(cls):
        return cls.getData("SG_DUE_DATE", None)

    @classProperty.classProperty
    def SG_START_DATE(cls):
        return cls.getData("SG_START_DATE", None)

    @classProperty.classProperty
    def REMAINING_TIME(cls):
        return cls.getData("REMAINING_TIME", None)

    @classProperty.classProperty
    def ESTIMATED_TIME(cls):
        """default task field"""
        return cls.getData("ESTIMATED_TIME", None)

    @classProperty.classProperty
    def ACTUAL_TIME(cls):
        return cls.getData("ACTUAL_TIME", None)

    @classProperty.classProperty
    def TICKET_ACTUAL_TIME(cls):
        return cls.getData("TICKET_ACTUAL_TIME", None)

    @classProperty.classProperty
    def TICKET_REMAINING_TIME(cls):
        return cls.getData("TICKET_REMAINING_TIME", None)

    @classProperty.classProperty
    def DATE_CREATED(cls):
        return cls.getData("DATE_CREATED", None)

    @classProperty.classProperty
    def DATE_UPDATED(cls):
        return cls.getData("DATE_UPDATED", None)

    @classProperty.classProperty
    def ESTIMATE(cls):
        """Default time field found in other non-task entities."""
        return cls.getData("ESTIMATE", None)

    @classProperty.classProperty
    def SUGGESTED_ESTIMATED_TIME(cls):
        return cls.getData("SUGGESTED_ESTIMATED_TIME", None)

    @classProperty.classProperty
    def CAPTURE_DATE(cls):
        return cls.getData("CAPTURE_DATE", None)

    @classProperty.classProperty
    def BUGSTAR_CREATED_ON(cls):
        """Field to track when something was created on bugstar."""
        return cls.getData("BUGSTAR_CREATED_ON", None)

    @classProperty.classProperty
    def BUGSTAR_LAST_MODIFIED_ON(cls):
        """Field to track when something was created on bugstar."""
        return cls.getData("BUGSTAR_LAST_MODIFIED_ON", None)

    @classProperty.classProperty
    def LAST_ACTIVE(cls):
        """Time that a Human User last logged into shotgun. Set by ShotgunBackend."""
        return cls.getData("LAST_ACTIVE", None)

    # URL Fields

    @classProperty.classProperty
    def BUG_URL(cls):
        return cls.getData("BUG_URL", None)

    @classProperty.classProperty
    def URL(cls):
        """Attachment field that gives us a link from where we can download the attachment."""
        return cls.getData("URL", None)

    @classProperty.classProperty
    def UPLOADED_MOVIE(cls):
        """Version field that gives us information about uploaded file/attachment."""
        return cls.getData("UPLOADED_MOVIE", None)

    @classProperty.classProperty
    def META(cls):
        return cls.getData("META", None)
    
    @classProperty.classProperty
    def FILMSTRIP(cls):
        return cls.getData("FILMSTRIP", None)

    # UUID Field

    @classProperty.classProperty
    def SESSION_UUID(cls):
        return cls.getData("SESSION_UUID", None)

    # Entity Related Fields

    @classProperty.classProperty
    def ENTITY(cls):
        return cls.getData("ENTITY", None)

    @classProperty.classProperty
    def USER(cls):
        return cls.getData("USER", None)

    @classProperty.classProperty
    def USERS(cls):
        return cls.getData("USERS", None)

    @classProperty.classProperty
    def OWNERS(cls):
        """Owner of Tasks."""
        return cls.getData("OWNERS", None)

    @classProperty.classProperty
    def SCHEMA(cls):
        return cls.getData("SCHEMA", None)

    @classProperty.classProperty
    def QA_OWNER(cls):
        return cls.getData("QA_OWNER", None)

    @classProperty.classProperty
    def REVIEWER(cls):
        """reviewer of the task"""
        return cls.getData("REVIEWER", None)

    @classProperty.classProperty
    def ADDRESSINGS_TO(cls):
        return cls.getData("ADDRESSINGS_TO", None)

    @classProperty.classProperty
    def CC(cls):
        return cls.getData("CC", None)

    @classProperty.classProperty
    def CREATED_BY(cls):
        return cls.getData("CREATED_BY", None)

    @classProperty.classProperty
    def UPDATED_BY(cls):
        return cls.getData("UPDATED_BY", None)

    @classProperty.classProperty
    def CINEMATIC_ANIMATOR(cls):
        return cls.getData("CINEMATIC_ANIMATOR", None)

    @classProperty.classProperty
    def LEVEL_DESIGNER(cls):
        return cls.getData("LEVEL_DESIGNER", None)

    @classProperty.classProperty
    def AUDIO_DESIGNER(cls):
        return cls.getData("AUDIO_DESIGNER", None)

    @classProperty.classProperty
    def PIPELINE_STEP(cls):
        return cls.getData("PIPELINE_STEP", None)

    @classProperty.classProperty
    def MISSIONS(cls):
        """Script entity field that can hold multiple missions instead of one (replacing field called sg_mission that is removed from here)."""
        return cls.getData("MISSIONS", None)

    @classProperty.classProperty
    def ASSETLINK(cls):
        return cls.getData("ASSETLINK", None)

    @classProperty.classProperty
    def ASSETS(cls):
        return cls.getData("ASSETS", None)

    @classProperty.classProperty
    def PROJECT(cls):
        return cls.getData("PROJECT", None)

    @classProperty.classProperty
    def PROJECTS(cls):
        return cls.getData("PROJECTS", None)

    @classProperty.classProperty
    def GROUP(cls):
        """Currently only used by photogrammetry, this will be removed in the future.

        url:bugstar:6375515
        """
        return cls.getData("GROUP", None)

    @classProperty.classProperty
    def GROUPS(cls):
        return cls.getData("GROUPS", None)

    @classProperty.classProperty
    def DEPARTMENT(cls):
        return cls.getData("DEPARTMENT", None)

    @classProperty.classProperty
    def SEQUENCE(cls):
        return cls.getData("SEQUENCE", None)

    @classProperty.classProperty
    def CONCAT(cls):
        return cls.getData("CONCAT", None)

    @classProperty.classProperty
    def NOTES(cls):
        return cls.getData("NOTES", None)

    @classProperty.classProperty
    def OPEN_NOTES(cls):
        return cls.getData("OPEN_NOTES", None)

    @classProperty.classProperty
    def ATTACHMENTS(cls):
        return cls.getData("ATTACHMENTS", None)

    @classProperty.classProperty
    def PLAYLIST(cls):
        return cls.getData("PLAYLIST", None)

    @classProperty.classProperty
    def NOTE_LINKS(cls):
        return cls.getData("NOTE_LINKS", None)

    @classProperty.classProperty
    def REPLIES(cls):
        return cls.getData("REPLIES", None)

    @classProperty.classProperty
    def TASKS(cls):
        return cls.getData("TASKS", None)

    @classProperty.classProperty
    def TAGS(cls):
        return cls.getData("TAGS", None)

    @classProperty.classProperty
    def SCRIPT_CHARACTERS(cls):
        return cls.getData("SCRIPT_CHARACTERS", None)

    @classProperty.classProperty
    def TALENT(cls):
        return cls.getData("TALENT", None)

    @classProperty.classProperty
    def MISSION(cls):
        return cls.getData("MISSION", None)

    @classProperty.classProperty
    def ROCKSTAR_PROJECT(cls):
        return cls.getData("ROCKSTAR_PROJECT", None)

    @classProperty.classProperty
    def SELECT_CAST(cls):
        return cls.getData("SELECT_CAST", None)

    @classProperty.classProperty
    def GLOBAL_ENTITY_PROJECT(cls):
        """Non-project entities need to create a project entity."""
        return cls.getData("GLOBAL_ENTITY_PROJECT", None)

    @classProperty.classProperty
    def GOAL(cls):
        return cls.getData("GOAL", None)

    @classProperty.classProperty
    def GOALS(cls):
        return cls.getData("GOALS", None)

    @classProperty.classProperty
    def PARENT_ENTITY(cls):
        """Our custom fields for tracking a parent of an entity."""
        return cls.getData("PARENT_ENTITY", None)

    @classProperty.classProperty
    def CHILD_ENTITY(cls):
        """Our custom field for tracking children of an entity."""
        return cls.getData("CHILD_ENTITY", None)

    @classProperty.classProperty
    def TASK(cls):
        """Used for the version entity, which task it belongs to."""
        return cls.getData("TASK", None)

    @classProperty.classProperty
    def VERSIONS(cls):
        """Saves version entities."""
        return cls.getData("VERSIONS", None)

    @classProperty.classProperty
    def CHANGELISTS(cls):
        return cls.getData("CHANGELISTS", None)

    @classProperty.classProperty
    def BUGSTAR_CHANGELISTS(cls):
        """Used for tracking changelists associated with a bug."""
        return cls.getData("BUGSTAR_CHANGELISTS", None)

    @classProperty.classProperty
    def BUGSTAR_GRID(cls):
        """Used for tracking the grid associated with a bug."""
        return cls.getData("BUGSTAR_GRID", None)

    @classProperty.classProperty
    def BUGSTAR_COMPONENT(cls):
        """Used for tracking component associated with a bug."""
        return cls.getData("BUGSTAR_COMPONENT", None)

    # binary

    @classProperty.classProperty
    def THUMBNAIL(cls):
        return cls.getData("THUMBNAIL", None)

    @classProperty.classProperty
    def GRID_LOCATION(cls):
        return cls.getData("GRID_LOCATION", None)


class Meta(object):
    """List of common keys returned by the meta dictionary of an EventLogEntry."""

    OLD_VALUE = "old_value"
    NEW_VALUE = "new_value"


class Operators(object):
    """List of operators available for the find method of the shotgun api."""

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunOperators.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    @classProperty.classProperty
    def IS(cls):
        return cls.getData("IS", None)

    @classProperty.classProperty
    def IS_NOT(cls):
        return cls.getData("IS_NOT", None)

    @classProperty.classProperty
    def LESS_THAN(cls):
        return cls.getData("LESS_THAN", None)

    @classProperty.classProperty
    def GREATER_THAN(cls):
        return cls.getData("GREATER_THAN", None)

    @classProperty.classProperty
    def CONTAINS(cls):
        return cls.getData("CONTAINS", None)

    @classProperty.classProperty
    def NOT_CONTAINS(cls):
        return cls.getData("NOT_CONTAINS", None)

    @classProperty.classProperty
    def STARTS_WITH(cls):
        return cls.getData("STARTS_WITH", None)

    @classProperty.classProperty
    def ENDS_WITH(cls):
        return cls.getData("ENDS_WITH", None)

    @classProperty.classProperty
    def BETWEEN(cls):
        return cls.getData("BETWEEN", None)

    @classProperty.classProperty
    def NOT_BETWEEN(cls):
        return cls.getData("NOT_BETWEEN", None)

    @classProperty.classProperty
    def IN_LAST(cls):
        return cls.getData("IN_LAST", None)

    @classProperty.classProperty
    def IN_NEXT(cls):
        return cls.getData("IN_NEXT", None)

    @classProperty.classProperty
    def IN(cls):
        return cls.getData("IN", None)

    @classProperty.classProperty
    def TYPE_IS(cls):
        return cls.getData("TYPE_IS", None)

    @classProperty.classProperty
    def TYPE_IS_NOT(cls):
        return cls.getData("TYPE_IS_NOT", None)

    @classProperty.classProperty
    def IN_CALENDAR_DAY(cls):
        return cls.getData("IN_CALENDAR_DAY", None)

    @classProperty.classProperty
    def IN_CALENDAR_WEEK(cls):
        return cls.getData("IN_CALENDAR_WEEK", None)

    @classProperty.classProperty
    def IN_CALENDAR_MONTH(cls):
        return cls.getData("IN_CALENDAR_MONTH", None)

    @classProperty.classProperty
    def NAME_CONTAINS(cls):
        return cls.getData("NAME_CONTAINS", None)

    @classProperty.classProperty
    def NAME_NOT_CONTAINS(cls):
        return cls.getData("NAME_NOT_CONTAINS", None)

    @classProperty.classProperty
    def NAME_ENDS_WITH(cls):
        return cls.getData("NAME_ENDS_WITH", None)


class SubTypes(object):
    """Subtypes for entities that represent multiple type of data."""

    _dataJsonPath = os.path.join(Paths.JSON_DATA_DIRECTORY, "shotgunSubTypes.json")
    __metaclass__ = attributesLazyLoader.AttributesLazyLoader

    # Asset Subtypes

    @classProperty.classProperty
    def ENVIRONMENT(cls):
        return cls.getData("ENVIRONMENT", None)

    @classProperty.classProperty
    def POSE(cls):
        return cls.getData("POSE", None)

    @classProperty.classProperty
    def PROP(cls):
        return cls.getData("PROP", None)

    @classProperty.classProperty
    def CUTSCENE(cls):
        return cls.getData("CUTSCENE", None)

    @classProperty.classProperty
    def IN_GAME(cls):
        return cls.getData("IN_GAME", None)

    @classProperty.classProperty
    def CS_IG(cls):
        return cls.getData("CS_IG", None)

    @classProperty.classProperty
    def VEHICLE(cls):
        return cls.getData("VEHICLE", None)

    @classProperty.classProperty
    def WEAPON(cls):
        return cls.getData("WEAPON", None)

    # Garment Types

    @classProperty.classProperty
    def ACCESSORIES(cls):
        return cls.getData("ACCESSORIES", None)

    @classProperty.classProperty
    def BAGS(cls):
        return cls.getData("BAGS", None)

    @classProperty.classProperty
    def BOTTOMS(cls):
        return cls.getData("BOTTOMS", None)

    @classProperty.classProperty
    def BELTSBUCKLES(cls):
        return cls.getData("BOTTOMS", None)

    @classProperty.classProperty
    def EYEWEAR(cls):
        return cls.getData("EYEWEAR", None)

    @classProperty.classProperty
    def FOOTWEAR(cls):
        return cls.getData("FOOTWEAR", None)

    @classProperty.classProperty
    def HAIRSTYLES(cls):
        return cls.getData("HAIRSTYLES", None)

    @classProperty.classProperty
    def HEAD(cls):
        return cls.getData("HEAD", None)

    @classProperty.classProperty
    def HEADWEAR(cls):
        return cls.getData("HEADWEAR", None)

    @classProperty.classProperty
    def NUDE(cls):
        return cls.getData("NUDE", None)

    @classProperty.classProperty
    def ONEPIECESUIT(cls):
        return cls.getData("ONEPIECESUIT", None)

    @classProperty.classProperty
    def PROXYPEDS(cls):
        return cls.getData("PROXYPEDS", None)

    @classProperty.classProperty
    def TOPS(cls):
        return cls.getData("TOPS", None)

    # Garment Subtypes

    @classProperty.classProperty
    def ARMORBODY(cls):
        return cls.getData("ARMORBODY", None)

    @classProperty.classProperty
    def ARMORLEGS(cls):
        return cls.getData("ARMORLEGS", None)

    @classProperty.classProperty
    def BELTS(cls):
        return cls.getData("BELTS", None)

    @classProperty.classProperty
    def BRACELETS(cls):
        return cls.getData("BRACELETS", None)

    @classProperty.classProperty
    def EARRINGS(cls):
        return cls.getData("EARRINGS", None)

    @classProperty.classProperty
    def GILETS(cls):
        return cls.getData("GILETS", None)

    @classProperty.classProperty
    def GLOVES(cls):
        return cls.getData("GLOVES", None)

    @classProperty.classProperty
    def HEADPHONES(cls):
        return cls.getData("HEADPHONES", None)

    @classProperty.classProperty
    def NECKLACES(cls):
        return cls.getData("NECKLACES", None)

    @classProperty.classProperty
    def NECKWEAR(cls):
        return cls.getData("NECKWEAR", None)

    @classProperty.classProperty
    def PANTS(cls):
        return cls.getData("PANTS", None)

    @classProperty.classProperty
    def RINGS(cls):
        return cls.getData("RINGS", None)

    @classProperty.classProperty
    def SHORTS(cls):
        return cls.getData("SHORTS", None)

    @classProperty.classProperty
    def SKIRTS(cls):
        return cls.getData("SKIRTS", None)

    @classProperty.classProperty
    def WATCHES(cls):
        return cls.getData("WATCHES", None)

    # Garment Family

    @classProperty.classProperty
    def DERIVATIVE(cls):
        return cls.getData("DERIVATIVE", None)

    @classProperty.classProperty
    def ARCHETYPE(cls):
        return cls.getData("ARCHETYPE", None)

    # Gender Types

    @classProperty.classProperty
    def FEMALE(cls):
        return cls.getData("FEMALE", None)

    @classProperty.classProperty
    def MALE(cls):
        return cls.getData("MALE", None)

    # Character Types

    @classProperty.classProperty
    def ANIMAL(cls):
        return cls.getData("ANIMAL", None)

    @classProperty.classProperty
    def GARMENT(cls):
        return cls.getData("GARMENT", None)

    @classProperty.classProperty
    def BOARDCONCEPT(cls):
        return cls.getData("BOARDCONCEPT", None)

    @classProperty.classProperty
    def BOARDPLAYER(cls):
        return cls.getData("BOARDPLAYER", None)

    @classProperty.classProperty
    def BOARDSHOPSTORE(cls):
        return cls.getData("BOARDSHOPSTORE", None)

    @classProperty.classProperty
    def BOARDSTORY(cls):
        return cls.getData("BOARDSTORY", None)

    @classProperty.classProperty
    def BOARDWORLD(cls):
        return cls.getData("BOARDWORLD", None)

    @classProperty.classProperty
    def PEDUNIQUE(cls):
        return cls.getData("PEDUNIQUE", None)

    @classProperty.classProperty
    def PEDWORLD(cls):
        return cls.getData("PEDWORLD", None)

    @classProperty.classProperty
    def ROLEOUTFIT(cls):
        return cls.getData("ROLEOUTFIT", None)

    @classProperty.classProperty
    def SHOP(cls):
        return cls.getData("SHOP", None)

    @classProperty.classProperty
    def ITEM(cls):
        return cls.getData("ITEM", None)

    # Rockstar Asset Subtypes

    @classProperty.classProperty
    def DIALOGUE(cls):
        return cls.getData("DIALOGUE", None)

    @classProperty.classProperty
    def CHARACTER(cls):
        return cls.getData("CHARACTER", None)

    # Strand Subtypes

    @classProperty.classProperty
    def MISSION(cls):
        return cls.getData("MISSION", None)

    @classProperty.classProperty
    def SCRIPTED_ANIM(cls):
        return cls.getData("SCRIPTED_ANIM", None)

    # Face Video Subtypes

    @classProperty.classProperty
    def RANDOM_EVENT(cls):
        return cls.getData("RANDOM_EVENT", None)

    @classProperty.classProperty
    def AI(cls):
        return cls.getData("AI", None)

    @classProperty.classProperty
    def SCRIPTED_SPEECH(cls):
        return cls.getData("SCRIPTED_SPEECH", None)

    @classProperty.classProperty
    def PED(cls):
        return cls.getData("PED", None)

    @classProperty.classProperty
    def VFX(cls):
        return cls.getData("VFX", None)

    @classProperty.classProperty
    def ROM(cls):
        return cls.getData("ROM", None)

    # Note Types

    @classProperty.classProperty
    def NOTE(cls):
        return cls.getData("NOTE", None)

    @classProperty.classProperty
    def FACE(cls):
        return cls.getData("FACE", None)

    @classProperty.classProperty
    def BODY(cls):
        return cls.getData("BODY", None)

    # Task Types

    @classProperty.classProperty
    def ART(cls):
        return cls.getData("ART", None)

    @classProperty.classProperty
    def BUG(cls):
        return cls.getData("BUG", None)

    @classProperty.classProperty
    def LEGAL(cls):
        return cls.getData("LEGAL", None)

    @classProperty.classProperty
    def SCRIPT_RECORDING(cls):
        return cls.getData("SCRIPT_RECORDING", None)

    @classProperty.classProperty
    def SCRIPT_WRITING(cls):
        return cls.getData("SCRIPT_WRITING", None)

    @classProperty.classProperty
    def RIGGING(cls):
        return cls.getData("RIGGING", None)

    # Tag Subtypes

    @classProperty.classProperty
    def TAG(cls):
        return cls.getData("TAG", None)

    @classProperty.classProperty
    def CHANGELIST(cls):
        return cls.getData("CHANGELIST", None)

    # Unordered

    @classProperty.classProperty
    def BACKPACKS(cls):
        return cls.getData("BACKPACKS", None)

    @classProperty.classProperty
    def BAGSTRAPS(cls):
        return cls.getData("BAGSTRAPS", None)

    @classProperty.classProperty
    def BEARD(cls):
        return cls.getData("BEARD", None)

    @classProperty.classProperty
    def BOOTS(cls):
        return cls.getData("BOOTS", None)

    @classProperty.classProperty
    def BOTH(cls):
        return cls.getData("BOTH", None)

    @classProperty.classProperty
    def BRAS(cls):
        return cls.getData("BRAS", None)

    @classProperty.classProperty
    def COATS(cls):
        return cls.getData("COATS", None)

    @classProperty.classProperty
    def COSTUME(cls):
        return cls.getData("COSTUME", None)

    @classProperty.classProperty
    def DRESS(cls):
        return cls.getData("DRESS", None)

    @classProperty.classProperty
    def DUFFLEBAGS(cls):
        return cls.getData("DUFFLEBAGS", None)

    @classProperty.classProperty
    def FULLBODY(cls):
        return cls.getData("FULLBODY", None)

    @classProperty.classProperty
    def GOGGLES(cls):
        return cls.getData("GOGGLES", None)

    @classProperty.classProperty
    def HAIR(cls):
        return cls.getData("HAIR", None)

    @classProperty.classProperty
    def HANDBAGS(cls):
        return cls.getData("HANDBAGS", None)

    @classProperty.classProperty
    def HATS(cls):
        return cls.getData("HATS", None)

    @classProperty.classProperty
    def HELMETS(cls):
        return cls.getData("HELMETS", None)

    @classProperty.classProperty
    def HOODIES(cls):
        return cls.getData("HOODIES", None)

    @classProperty.classProperty
    def JACKETS(cls):
        return cls.getData("JACKETS", None)

    @classProperty.classProperty
    def MASKS(cls):
        return cls.getData("MASKS", None)

    @classProperty.classProperty
    def PARACHUTES(cls):
        return cls.getData("PARACHUTES", None)

    @classProperty.classProperty
    def SHADES(cls):
        return cls.getData("SHADES", None)

    @classProperty.classProperty
    def SHIRTS(cls):
        return cls.getData("SHIRTS", None)

    @classProperty.classProperty
    def SHOES(cls):
        return cls.getData("SHOES", None)

    @classProperty.classProperty
    def SOCKS(cls):
        return cls.getData("SOCKS", None)

    @classProperty.classProperty
    def SPECS(cls):
        return cls.getData("SPECS", None)

    @classProperty.classProperty
    def SWEATSHIRTS(cls):
        return cls.getData("SWEATSHIRTS", None)

    @classProperty.classProperty
    def SWIMSUIT(cls):
        return cls.getData("SWIMSUIT", None)

    @classProperty.classProperty
    def TANKTOPS(cls):
        return cls.getData("TANKTOPS", None)

    @classProperty.classProperty
    def TSHIRTS(cls):
        return cls.getData("TSHIRTS", None)

    @classProperty.classProperty
    def UNDERWEAR(cls):
        return cls.getData("UNDERWEAR", None)

    @classProperty.classProperty
    def VESTS(cls):
        return cls.getData("VESTS", None)

    @classProperty.classProperty
    def WRAPS(cls):
        return cls.getData("WRAPS", None)
