from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    """ Class for holding the subtypes associated with the note entity"""

    NOTE = const.SubTypes.NOTE
    FACE = const.SubTypes.FACE
    BODY = const.SubTypes.BODY
    BUG = const.SubTypes.BUG


class Note(baseEntity.BaseEntity):
    """
    Class that represents a Note entity
    """

    ENTITY_TYPE = const.Entities.NOTE
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.PROJECT,
        const.Fields.CC,
        const.Fields.ATTACHMENTS,
        const.Fields.USER,
        const.Fields.CONTENT,
        const.Fields.SUBJECT,
        const.Fields.NOTE_LINKS,
        const.Fields.TASKS,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.REPLIES,
        const.Fields.START_FRAME,
        const.Fields.END_FRAME,
        const.Fields.ESTIMATE,
        const.Fields.ESTIMATED_TIME_VALUE,
        const.Fields.SUGGESTED_ESTIMATED_TIME,
        const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE,
        const.Fields.STATUS,
    )
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    NAME_FIELD = const.Fields.SUBJECT

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def cc(self):
        """
        List of individuals cc'd to the bug associated with this note

        Returns:
            list
        """
        return self._getmutable(const.Fields.CC, mutableType=list)

    @cc.setter
    def cc(self, value):
        """
        Sets the list of people cc'd to this note

        Arguments:
            value (list): list of people cc'd to the bug associated with this task in bugstar
        """
        self._setattr(const.Fields.CC, value, isMutable=True)

    @property
    def author(self):
        """
        Shotgun user that created the note

        Returns:
            rsg_core.shotgun.entities.humanUser.HumanUser
        """
        return self._getattr(const.Fields.USER)

    @author.setter
    def author(self, value):
        """
        Sets the user that created this note

        Arguments:
            value (rsg_core.shotgun.entities.humanUser.HumanUser): user who authored the note
        """
        self._setattr(const.Fields.USER, value)

    @property
    def body(self):
        """
        The main message of the note

        Returns:
            str
        """
        return self._getattr(const.Fields.CONTENT)

    @body.setter
    def body(self, value):
        """
        Sets the main message for the note

        Arguments:
            value (str): the message to set as the main message of the note
        """
        self._setattr(const.Fields.CONTENT, value)

    @property
    def subject(self):
        """
        The title of the note

        Returns:
            str
        """
        return self.name

    @subject.setter
    def subject(self, value):
        """
        Sets the title fo the note

        Arguments:
            value (str): title for the note
        """
        self.name = value

    @property
    def status(self):
        """
        The status code that this note is currently set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the current status code and updates the status name field to reflect the new status code being set.

        Arguments
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        """
        The status name of the current status set for this entity

        Returns:
            str
        """
        return const.Statuses.getName(self.status)

    @property
    def links(self):
        """
        Entities connected to this note

        Returns:
            list
        """
        return self._getmutable(const.Fields.NOTE_LINKS, mutableType=list)

    @links.setter
    def links(self, value):
        """
        Sets which entities are linked to this note

        Arguments:
            value (list): list of entities to associate with this note
        """
        self._setattr(const.Fields.NOTE_LINKS, value, isMutable=True)

    @property
    def tasks(self):
        """
        Tasks connected to this note

        Returns:
            list
        """
        return self._getmutable(const.Fields.TASKS, mutableType=list)

    @tasks.setter
    def tasks(self, value):
        """
        Sets which tasks are connected to this note

        Arguments:
            value (list): list of tasks to associate with this note
        """
        self._setattr(const.Fields.TASKS, value, isMutable=True)

    @property
    def replies(self):
        """
        The replies/comments made to this note

        Returns:
            list
        """
        return self._getmutable(const.Fields.REPLIES, mutableType=list)

    @property
    def subtype(self):
        """
        The entity subtype for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the entity subtype for this entity

        Arguments:
            value (str): entity subtype
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def startFrame(self):
        """
        The start frame for the the frame range this note is referring to

        Returns:
            int
        """
        return self._getattr(const.Fields.START_FRAME)

    @startFrame.setter
    def startFrame(self, value):
        """
        Sets the start frame for the the frame range this note is referring to

        Arguments:
            value (int): start frame for the frame range the note is referring to
        """
        self._setattr(const.Fields.START_FRAME, value)

    @property
    def endFrame(self):
        """
        The end frame for the the frame range this note is referring to

        Returns:
            int
        """
        return self._getattr(const.Fields.END_FRAME)

    @endFrame.setter
    def endFrame(self, value):
        """
        Sets the end frame for the the frame range this note is referring to

        Arguments:
            value (int): end frame for the frame range the note is referring to
        """
        self._setattr(const.Fields.END_FRAME, value)

    @property
    def estimatedTime(self):
        """
        The estimated time it should take to address the work described in the note

        Returns:
            float
        """
        return self._getattr(const.Fields.ESTIMATE)

    @estimatedTime.setter
    def estimatedTime(self, value):
        """
        Sets the estimated time it should take to address the work described in the note

        Arguments:
            value (float): The estimated time in days that it should take to complete the work described in this note
        """
        self._setattr(const.Fields.ESTIMATE, value)
        self._setEstimatedTimeValue(value, const.Fields.ESTIMATED_TIME_VALUE)

    @property
    def estimatedTimeValue(self):
        """
        The human readable version of the suggested estimated time it should take to address the work
        described in the note based on the frame range provided by the note.

        Returns:
            str
        """
        return self._getattr(const.Fields.ESTIMATED_TIME_VALUE)

    @property
    def suggestedEstimatedTime(self):
        """
        The suggested estimated time it should take to address the work described in the note based on the frame range
        provided by the note

        Returns:
            float
        """
        return self._getattr(const.Fields.SUGGESTED_ESTIMATED_TIME)

    @property
    def suggestedEstimatedTimeValue(self):
        """
        The human readable version of the suggested estimated time it should take to address the work
        described in the note based on the frame range provided by the note

        Returns:
            str
        """
        return self._getattr(const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE)

    def _setSuggestedEstimateTime(self):
        """ Sets the suggested estimated time for the work involved in this note based on the start and end frame """
        frameRange = [self.startFrame, self.endFrame]
        if None in frameRange or frameRange[0] > frameRange[-1]:
            self.__setattr__(const.Fields.SUGGESTED_ESTIMATED_TIME, None)
            self.__setattr__(const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE, None)
            return

        # TODO: Move the multiplier to non-hardcoded location in the future
        multiplier = 0
        if self.subtype == SubTypes.FACE:
            multiplier = 0.0001
        elif self.subtype == SubTypes.BODY:
            multiplier = 1 / 1500.0

        days = (frameRange[-1] - frameRange[0]) * multiplier  # this is a float value
        self._setattr(const.Fields.SUGGESTED_ESTIMATED_TIME, days)
        self._setEstimatedTimeValue(const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE, days)

    def reply(self, text, author=None):
        """
        Creates a reply for this note

        Arguments:
            text (str): text to add to the reply

        Keyword Args:
            author (rsg_core.shotgun.entities.humanUser.HumanUser): user that wrote the note.
                    Defaults to the current user.
        """
        reply = self.server.reply()
        reply.author = author or self.server.currentUser
        reply.body = text
        reply.link = self
        self.replies.append(reply)
        return reply


class Builder(baseEntity.BaseBuilder):
    @property
    def subtypes(self):
        return SubTypes

    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Note
