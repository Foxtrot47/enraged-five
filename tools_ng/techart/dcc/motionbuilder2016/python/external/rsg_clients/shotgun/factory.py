"""
Module for resolving which entities need to be created
"""
from rsg_clients.shotgun.entities import *  # The init has been modified to import all modules in its directory when import * is used
from rsg_clients.shotgun.entities import baseEntity


def resolveEntityClass(entityType):
    """
    Resolves which class to get based on the given entity name

    Arguments:
        entityType (str): the type of entity whose custom class we want to get

    Returns:
        class
    """
    return baseEntity.BaseEntity._getSubclassByType(entityType)


def resolveEntity(shotgun, data):
    """Converts a dictionary generated from shotgun into an instance of the entity it represents.

    Arguments:
        shotgun (rsg_core.shotgun.core.Shotgun): rockstar shotgun instance
        data (dict): dictionary generated from shotgun

    Returns:
        rsg_core.shotgun.entities.baseEntity.BaseEntity
    """
    entityId = data["id"]
    entityType = data["type"]
    builder = shotgun._getBuilder(entityType)
    return builder(index=entityId, data=data)