from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Attachment(baseEntity.BaseEntity):
    """
    Class that represents a Attachment entity.
    Attachment entities are considered files and are displayed as such in the shotgun interface
    """

    ENTITY_TYPE = const.Entities.ATTACHMENT
    DEFAULT_FIELDS = (
        const.Fields.NAME,
        const.Fields.ID,
        const.Fields.URL,
        const.Fields.CONTENT_TYPE,
        const.Fields.FILENAME,
    )
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    NAME_FIELD = const.Fields.NAME

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_clients.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def url(self):
        """
        Gets the path where the attachment has been saved on the server

        Returns:
            str
        """
        return self._getattr(const.Fields.URL)

    @property
    def contentType(self):
        """
        Gets the type of the attachment if it's image, or text.

        Returns:
            str
        """
        return self._getattr(const.Fields.CONTENT_TYPE)

    @property
    def filename(self):
        """Gets the name of the file as stored in shotgun.

        Returns:
            str
        """
        return self._getattr(const.Fields.FILENAME)

    def download(self, path):
        """
        Downloads the attachment to the specified file location with it's own name. If the file already exists,
        adds the number at the end.


        Args:
            path (str): Directory where to save the attachment

        Return:
            str - file system location where the file has been saved.
        """
        fileName = self.name if self.name else self.filename
        downloadedLocation = self.shotgun.download(self, path, fileName)
        return downloadedLocation


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_clients.abstracts.AbstractPayload.abstractPayload
        """
        return Attachment

    def getById(self, attachmentId, filters=None):
        """
        Gets the attachment object with the specified ID.

        Args:
            attachmentId (int): id number of the attachment
            filters (list, optional): list of [rsg_clients.shotgun.const.Fields, rsg_clients.shotgun.const.Operations, value]

        Returns:
            rsg_clients.shotgun.version.Version
        """
        filters = filters or []
        filters.append([const.Fields.ID, const.Operators.IS, attachmentId])
        entity = self.find(filters)
        if entity:
            return entity[0]
