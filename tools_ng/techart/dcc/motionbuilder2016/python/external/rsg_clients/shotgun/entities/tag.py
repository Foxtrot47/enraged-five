from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    TAG = const.SubTypes.TAG
    CHANGELIST = const.SubTypes.CHANGELIST


class Tag(baseEntity.BaseEntity):
    """
    Class that represents a TaG entity
    """

    ENTITY_TYPE = const.Entities.TAG
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.NAME,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
        const.Fields.IN_BUGSTAR,
        const.Fields.USAGE,
        const.Fields.ENTITY_SUBTYPE,
    )

    NAME_FIELD = const.Fields.NAME

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.NAME)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.NAME, value)

    @property
    def inBugstar(self):
        return self._getattr(const.Fields.IN_BUGSTAR)

    @inBugstar.setter
    def inBugstar(self, value):
        self._setattr(const.Fields.IN_BUGSTAR, value)

    @property
    def usageCount(self):
        return self._getattr(const.Fields.USAGE)

    @property
    def subtype(self):
        """
        The entity subtype for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the entity subtype for this entity

        Arguments:
            value (str): entity subtype
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Tag

    @property
    def subtypes(self):
        return SubTypes
