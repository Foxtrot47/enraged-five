from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Concat(baseEntity.BaseEntity):
    """
    Class that represents a Concat/Sequence entity
    """

    ENTITY_TYPE = const.Entities.SEQUENCE
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.PROJECT,
        const.Fields.STATUS,
        const.Fields.STATUS_NAME,
        const.Fields.CONCAT,
        const.Fields.TASKS,
    )
    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def status(self):
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        return const.Statuses.getName(self.status)

    @property
    def tasks(self):
        return self._getmutable(const.Fields.TASKS, mutableType=list)

    @tasks.setter
    def tasks(self, value):
        self._setattr(const.Fields.TASKS, value, isMutable=True)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Concat
