"""Base Shotgun entity to subclass into specific entity types."""

import os
import pprint
import urllib
import urlparse
import datetime
import tempfile

from rsg_core import conversions
from rsg_clients.shotgun import const, exceptions
from rsg_core_py.abstracts import abstractPayload, abstractBuilder
from rsg_core_py.metaclasses import mapping


class BaseEntitySubtypes(object):
    """Base Enum Class to hold subtypes for other classes."""


class Schema(object):
    """Class for holding methods relating to editing the schemas of entity types.

    This mostly relates to the CRUD of fields.

    Data returned by these method cannot be shared between one another (The dictionary returned by the create method
    cannot be used for the update method.)
    """

    def __init__(self, server, entityType):
        """Initializer.

        Args:
            server (rsg_clients.shotgun.core.Shotgun): the server to query information from
            entityType (str): the type of the entity whose fields need to be edited
        """
        self._server = server
        self._entityType = entityType

    def create(self, fieldType, name, properties=None):
        """Creates a field on this entity type.

        Examples:
            # data returned by the method
            {'sg_bug_number': {'custom_metadata': {'editable': True, 'value': ''},
                   'data_type': {'editable': False, 'value': 'number'},
                   'description': {'editable': True,
                                   'value': 'Bug associated with this task'},
                   'editable': {'editable': False, 'value': True},
                   'entity_type': {'editable': False, 'value': 'Task'},
                   'mandatory': {'editable': False, 'value': False},
                   'name': {'editable': True, 'value': 'Bug Number'},
                   'properties': {'default_value': {'editable': False,
                                                    'value': None},
                                  'summary_default': {'editable': True,
                                                      'value': 'none'}},
                   'ui_value_displayable': {'editable': False, 'value': True},
                   'unique': {'editable': False, 'value': False},
                   'visible': {'editable': True, 'value': True}}}
        Args:
            fieldType (str): the type for the field
            name (str): the display name of the field, the generated codename will add an 'sg' prefix and convert any spaces & special characters to underscores.

            properties (dict, optional): properties to set for the schema of the field

        Returns:
            dict
        """
        return self._server.createField(self._entityType, fieldType, name, properties)

    def read(self, field=None, project=None):
        """Gets the schema of a fields of this entity. If no field name specified, schema for all are returned.

        Args:
            field (str, optional): the codename of the the field
            project (rsg_clients.shotgun.entity.project.Project, optional): project to get the schema for

        Returns:
            dict
        """
        return self._server.readField(self._entityType, field, project)

    def update(self, field, properties, project=None):
        """Updates the schema for the given field.

         Args:
            field (str): the codename of the the field
            properties (dict): schema to update

            project (rsg_clients.shotgun.entity.project.Project, optional): project to get the schema for

        Returns:
            dict
        """
        return self._server.updateField(self._entityType, field, properties, project)

    def delete(self, field):
        """Updates the schema for the given field.

        Args:
           field (str): the codename of the the field

        Returns:
           bool
        """
        return self._server.deleteField(self._entityType, field)


class Search(object):
    """Class for editing and performing searches (filters as they are known in Shotgun).

    This class isn't required but it does offer quick access to some search related utility methods.
    Shotgun also doesn't allow the developers to access the filters stored in the website, so these classes are not
    cached or represent existing filters on the database.
    """

    def __init__(self, connection, builder, data=None):
        """Initializer.

        Args:
            connections (rsg_clients.shotgun.core.Shotgun): the connection to use for generating the report
            builder (rsg_clients.shotgun.entities.baseEntity.BaseBuilder): the builder for the entity type of the data to request from shotgun
            data (dict or list): the filter accepted by the shotgun search endpoint
        """
        self._connection = connection
        self._builder = builder
        entityClass = builder.cls()
        self._entityEndpoint = entityClass.ENDPOINT
        self._root = data
        self._fields = [
            field
            for field in entityClass.DEFAULT_FIELDS
            if field in (const.Fields.ID, const.Fields.PROJECT, const.Fields.GLOBAL_ENTITY_PROJECT)
        ]

    @property
    def root(self):
        """The root shotgun filter.

        Returns:
            list or dict
        """
        return self._root

    def search(self, fields=None, size=None, page=None, iterative=True, generator=False):
        """Performs the search and returns raw data from Shotgun.

        Args:
            fields (list, optional): List of fields to return in a query. Default is defined per-entity in that entity
                    class's DEFAULT_FIELDS property.
            size (int, optional): The amount of results to return. Default is 100 and max is 500.
            page (int, optional): The page of results to return. This flag is only respected if a value to limit is passed.
            iterative (bool, optional): Continue getting results if the results span multiple pages
            generator (bool, optional): Return a generator instead of a list

        Returns:
            list of dicts
        """
        results = self._connection.search(
            endpoint=self._entityEndpoint,
            filters=self._root,
            fields=fields or self.fields,
            size=size,
            page=page,
            iterative=iterative,
        )
        if not generator:
            allResults = []
            for result in results:
                allResults.extend(result)
            return allResults
        return results

    def results(self, fields=None, size=None, page=None, iterative=True):
        """Gets the results as python objects

        Args:
            fields (list, optional): List of fields to return in a query. Default is defined per-entity in that entity
                    class's DEFAULT_FIELDS property.
            size (int, optional): The amount of results to return. Default is 100 and max is 500.
            page (int, optional): The page of results to return. This flag is only respected if a value to limit is passed.
            iterative (bool, optional): Continue getting results if the results span multiple pages
            generator (bool, optional): Return a generator instead of a list

        Returns:
            list of objects
        """
        raise NotImplementedError("This method is currently not supported.")

    @property
    def totalCount(self):
        """The amount of unique results for the search.

        Returns:
            int
        """
        results = self._connection.summarize(self._entityEndpoint, self._root)
        return results["data"]["summaries"][const.Fields.ID]

    @property
    def fields(self):
        """Fields that will be grabbed by the search endpoint."""
        return tuple(self._fields)

    @property
    def server(self):
        """"""
        return self._connection

    def addField(self, value):
        """Add fields to include in the search results."""
        if value not in self._fields:
            self._fields.append(value)

    def removeField(self, value):
        """Removes a field so it isn't included in the search results."""
        if value in self._fields:
            self._fields.remove(value)


class BaseBuilder(abstractBuilder.AbstractPayloadBuilder):
    """Base Builder for building shotgun entity payloads."""

    def __init__(self, server):
        """Initializer.

        Args:
            server (rsg_clients.shotgun.servers.BaseServer): server to connect to
        """
        super(BaseBuilder, self).__init__(server)
        self._schema = Schema(server, self.cls().ENTITY_TYPE)

    @property
    def schema(self):
        """The schema of this object."""
        return self._schema

    def search(self, filters=None):
        """Builds a search object to get information from the server.

        Args:
            filters (list or dict): list or dict of filters to query against the server.

        Returns:
            Search
        """
        return Search(self._server, self, data=filters)

    def _clearCache(self, clearAll=False):
        """Clears out the memory caches of the entities that have been queried from Shotgun. STRICTLY FOR DEBUGGING.

        Args:
            clearAll (bool, optional): clear the cache for all classes that subclass the BaseEntity class.
        """
        class_ = self.cls()
        class_._INSTANCES = {}
        if all:
            for subclass in BaseEntity.__subclasses__():
                subclass._INSTANCES = {}

    def find(self, filters, fields=None, order=None, limit=None):
        """Performs a find operation on this type of entity in Shotgun.

        Args:
            filters (list): a list of filter conditions to perform

            fields (list, optional): list of fields to return
            order (list, optional): list of conditions for the order of how the data should be returned
            limit (int, optional): the amount of results to return, default is None which return everything

        Returns:
            list or None
        """
        class_ = self.cls()
        results = self._server.find(class_.ENTITY_TYPE, filters, fields or class_.DEFAULT_FIELDS, order, limit)
        # TODO: Remove parsing from here as it slows down generating the python object.
        return [
            class_(shotgun=self._server, index=data["id"], data=class_._parse(self._server, data)) for data in results
        ]

    def findOne(self, filters, fields=None):
        """Performs a find operation on this entity in Shotgun, limited to one result.

        Args:
            filters (list): A list of filter conditions to perform.

            fields (list, optional): List of fields to return.
        """
        queryResults = self.find(filters, fields, limit=1)
        return queryResults[0] if len(queryResults) else None

    def all(self, filters=None):
        """Gets the all the entities for this type of entity.

        Args:
            filters(list, optional): additional filters to include

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        return self.find(filters) or []

    def duplicates(self, field=None, filters=None, format=None):
        """Gets a list of all the entities whose given field match.

        Args:
            field (str, optional): name of the field to compare and match. Defaults to the name field of the entity type.
            filters (list of lists, optional): filters to limit the scope of duplicates to be returned.
                                    This defaults to returning all the entities of this entity type.
            format (func, optional): function to perform on the returned on the field before comparing values.

        Returns:
            list of BaseEntity
        """
        if not field:
            class_ = self.cls()
            field = class_.NAME_FIELD

        duplicates = {}
        for each in self.all(filters=filters):
            key = each._getattr(field)
            if format:
                key = format(key)
            duplicates.setdefault(key, []).append(each)
        return {key: value for key, value in duplicates.iteritems() if len(value) > 1}

    def getByName(self, name, filters=None, shotgun=None):
        """Gets the entity based on its name.

        Args:
            name (str): name of the entity

            name (str, optional): name of the entity
            filters(list, optional): additional filters to include
            shotgun (rsg_core.shotgun.core.Shotgun, optional): shotgun instance to perform operations with

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        class_ = self.cls()
        if class_.NAME_FIELD is None:
            raise NotImplementedError(
                "Entities may use different fields for displaying names.\n"
                "The NAME_FIELD class property should be reimplemented in each subclass of BaseEntity."
            )
        filters = filters or []
        filters.append([class_.NAME_FIELD, const.Operators.IS, name])
        entity = self.find(filters)
        if entity:
            return entity[0]


class BaseEntity(abstractPayload.AbstractPayload):
    """Base class for shotgun entity types.

    Contains the CRUD operations for interacting with Shotgun.

    Attributes:
        MAPPINGS (dict): mapping of the classes that inherit this class, where the key is the value returned by the key method
        ENTITY_TYPE (str): The codename of the entity on Shotgun; the codenames do not always match the actual name of the entity/class
        DEFAULT_FIELDS (tuple): List of fields that should be grabbed
        READ_ONLY_FIELDS (tuple): List of fields that shouldn't be edited
        NONCONVERTABLE_FIELDS (tuple): List of fields that stay as strings and not converted to other values for now
        NAME_FIELD (str): The field the entity uses for a name. This varies by entity and it may not exist for some.
        EXPIRY (int): time in seconds for how long that instance should wait until pinging the server for new fresh data
        SUPTYPES (rsg_core.shotgun.entities.baseEntity.BaseEntitySubtypes): constants for the various subtypes a given entity may be

        _INSTANCES (dict): dictionary for storing instances of the instance in memory.
                           This should be reimplemented in inherited classes.
        _INVALID_SERVER_ERROR (exception): The exception to raise when an invalid server is used.
    """

    __metaclass__ = mapping.Mapping
    MAPPINGS = {}

    ENTITY_TYPE = None  # This property will be depracted in favor of the endpoint property
    ENDPOINT = None
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.NOTES,
        const.Fields.OPEN_NOTES,
        const.Fields.VERSIONS,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
        const.Fields.THUMBNAIL,
    )
    READ_ONLY_FIELDS = (
        const.Fields.ID,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
    )

    NONCONVERTABLE_FIELDS = (
        const.Fields.PRIORITY,
        const.Fields.LOGIN,
        const.Fields.NONDEFAULT_DESCRIPTION,
    )  # Fields that should not attempt to be converted due to logic limitations
    NAME_FIELD = None

    EXPIRY = 3600  # an hour in seconds
    SUBTYPES = None
    DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    _INDEX_FIELD = const.Fields.ID
    _INVALID_SERVER_ERROR = exceptions.InvalidServer

    def __new__(cls, shotgun, index=None, data=None):
        """Stores & returns the instance of the class if an argument is provided, otherwise creates a new instance.

        Overrides builtin method

        Args:
            shotgun (rsg_core.shotgun.core.Shotgun): shotgun instance to perform operations with
            index (int): the id of the entity on Shotgun
            data (dictionary): data that represents this entity

        Returns:
            object
        """
        return cls._getInstance(shotgun, index)

    def __init__(self, shotgun, index=None, data=None):
        """Constructor.

        Args:
            index (int): id of the entity

            data (dictionary, optional): data that represents this entity
            shotgun (rsg_core.shotgun.core.Shotgun, optional): shotgun instance to perform operations with
        """
        if self._isCached(shotgun, index, self):
            if data:
                # Helps refresh the data when a find operation is performed
                data.pop("type", None)
                fields = list(data.keys())
                if self.NAME_FIELD != const.Fields.NAME and const.Fields.NAME in data:
                    data[self.NAME_FIELD or const.Fields.NAME] = data.pop(
                        const.Fields.NAME, self._fields.get(self.NAME_FIELD, None)
                    )
                    fields.remove(const.Fields.NAME)

                if fields:
                    self._refresh(data)
                    if not all([field in fields for field in self.DEFAULT_FIELDS]):
                        # Not all fields passed int the object, so we set isUpdated to false to query the server again next time
                        self._isUpdated = False
            return

        # properties to store
        self._setup(index, data, shotgun)
        self._cache()

    def _setup(self, index, data, server):
        """Sets the self._isUpdated flag based on whether data exists and is complete.

        Sets the isUpdated flag to false if the data dictionary does not contain all the defaults field.
        Information returned from other entities usually provide incomplete payloads and/or values under the wrong key.
        An example of a 'wrong' key is when a multi-entity or entity field is queried, it will always have a name key
        even if the entity in question does not have a field called name. Overrides inherited method.

        Args:
            index (int): id of the entity
            data (dict): data that represents this entity
            server (rsg_core.shotgun.core.Shotgun): shotgun instance to perform operations with
        """
        super(BaseEntity, self)._setup(index, data, server)
        if not data or not all([field in data for field in self.DEFAULT_FIELDS]):
            self._isUpdated = False

    def _store(self, data):
        """Sets the data for the item and caches it.

        Args:
            data (dict): data to set for the object
        """
        self._setup(data["id"], data, self.shotgun)
        self._cache()

    def _getattr(self, item, refresh=True, itemType=None):
        """Overrides inherited method.

        Adds error for when an unsupported property is accessed.

        Arguments:
            item (str): name of the property being accessed

            refresh (bool, optional): populate the rest of the object with data from the server
            itemType (class, optional): The class that the item should be.

        Raises:
            rsg_clients.shotgun.exceptions.InvalidField

        Returns:
            object
        """
        if item not in self.DEFAULT_FIELDS:
            raise exceptions.InvalidField(fieldName=item, entityType=self.ENTITY_TYPE)
        return super(BaseEntity, self)._getattr(item, refresh, itemType)

    def _getmutable(self, item, mutableType, refresh=True, itemType=None):
        """Overrides inherited method.

        Adds error for when an unsupported property is accessed.

        Arguments:
            item (str): name of the property being accessed
            mutableType (type): the mutable data type for the attribute being requested

            refresh (bool, optional): populate the rest of the object with data from the server
            itemType (object, optional): a callable object to cast the data in a mutable as another type.
                                    This is intented for times when a mutable, particularly a list,
                                    has data that is expensive to discern ahead of time.

        Raises:
            rsg_clients.shotgun.exceptions.InvalidField

        Returns:
            object
        """
        if item not in self.DEFAULT_FIELDS:
            raise exceptions.InvalidField(fieldName=item, entityType=self.ENTITY_TYPE)
        return super(BaseEntity, self)._getmutable(item, mutableType, refresh, itemType)

    @classmethod
    def _getSubclassByType(cls, entityType):
        """Gets the subclass that inherits baseEntity based on the mapping key provided.

        Args:
            entityType (str): the entity type whose matching class we want to get

        Returns:
            class
        """
        subclass = cls.MAPPINGS.get(entityType, None)
        if subclass is not None:
            return subclass

    @classmethod
    def key(cls):
        """Name to use as key for dictionaries returned from Bugstar to find information about this type of data.

        Returns:
            str
        """
        return cls.ENTITY_TYPE or cls.__name__

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        raise NotImplementedError

    def __str__(self):
        """Print out the objects payload when print is used on the object.

        Returns:
            str
        """
        return pprint.pformat(self.toDict())

    def toDict(self, onlyDirty=False, onlyEditableFields=False, specificFields=None, create=False):
        """Converts the local values to the correct data type that shotgun expects as a payload.

        Args:
            onlyDirty (bool, optional): only return data that has been edited locally
            onlyEditableFields (bool, optional): omit read only fields.
            specificFields (list, optional): only return the specific fields
            create (bool, optional): create shotgun entities if they aren't already on shotgun.

        Returns:
            dictionary
        """
        dictionaries = [self._fields, self._edits, self._mutables]
        if onlyDirty:
            dictionaries = [self._edits, self._mutables]

        data = {}
        for dictionary_ in dictionaries:
            data.update(dictionary_)

        if self._id is not None and not onlyEditableFields:
            data[const.Fields.ID] = self._id

        if onlyEditableFields:
            # Remove read only fields
            for readOnlyField in self.READ_ONLY_FIELDS:
                data.pop(readOnlyField, None)

        if specificFields:
            # Only keep specific fields
            data = {field: data[field] for field in specificFields if field in data}

        # Convert the data into data consumable by the base shotgun methods
        dictionary = {}
        for key, value in data.iteritems():
            # If the value on the server is not None, we are intentionally setting the value back to None
            if value is None and self._fields.get(key, None) is None:
                continue
            elif key == self.NAME_FIELD and isinstance(value, basestring):
                dictionary[key] = value.encode("utf-8", "ignore").decode("ascii", "ignore")
            elif key != self.NAME_FIELD and key not in self.NONCONVERTABLE_FIELDS:
                dictionary[key] = self._formatValue(value, create=create)
            else:
                dictionary[key] = value
        return dictionary

    def hasField(self, field):
        """Determines whether this entity supports the given field.

        Args:
            field (str): name of the field

        Returns:
            bool
        """
        return field in self.DEFAULT_FIELDS

    def _formatValue(self, value, create=False):
        """Converts the given value to the correct format that is consumable by Shotgun.

        Args:
            value (object): value to format

            create (bool, optional): create the value if it doesn't already exist on Shotgun

        Returns:
            object: the value unedited if unmutable or the contents formatted if it is.
            dictionary: when a value that is a subclass of BaseEnitity is passed, a dictionary is returned.
            int: if a string that represents an int is passed in then it gets returned as an int.
        """
        if isinstance(value, BaseEntity):
            if not value.id and create:
                value.create()
            if value.id:
                value_ = {"type": value.type, "id": value.id}
                if self.server.attachment.isInstance(value):
                    value_["url"] = value.url
                value = value_
        elif isinstance(value, (list, tuple)):
            value = [self._formatValue(subvalue, create=create) for subvalue in value]
        elif isinstance(value, basestring) and value.isdigit():
            value = int(value)
        elif isinstance(value, const.EventType):
            value = value.name
        elif isinstance(value, datetime.datetime):
            value = value.strftime(self.DATETIME_FORMAT)
        return value

    def _refresh(self, data):
        """Sets the new data as internal data for the instance.

        Args:
            data (dict): dictionary generated from a shotgun command
        """
        # Store if the data was updated prior to this refresh
        wasUpdated = self._isUpdated
        self.parse(data)

        if wasUpdated:
            # Get the current state of the mutable data
            mutableDifferences = self._getMutableDifferences()
            # Take the mutable data and update it with added/removed content from shotgun
            # Examples include people being removed/added to a cc list, new comments, attachments, etc.
            self._refreshMutables(mutableDifferences)
        else:
            # In the case that we are grabbing the data after a first update but mutable data was being used.
            # We go ahead and match the previously mutable data with the latest from the server.
            self._matchMutables()
        self._lastDataUpdate = datetime.datetime.utcnow()
        self._removeStalePayloads(instance=self)

    @property
    def exists(self):
        """Does this entity exist."""
        return self._id is not None

    @property
    def shotgun(self):
        """The shotgun instance it will be using to perform tasks."""
        return self.server

    @property
    def id(self):
        """The id of the entity."""
        return self._id

    @property
    def type(self):
        """The entity type."""
        return self.ENTITY_TYPE

    @property
    def data(self):
        """The raw data currently contained within this entity."""
        return self._fields

    @property
    def project(self):
        """The shotgun project associated with this entity."""
        return self._getattr(const.Fields.PROJECT)

    @project.setter
    def project(self, value):
        """The shotgun project to associate with this entity.

        Args:
             value (rsg_core.shotgun.entities.project.Project): project to set
        """
        self._setattr(const.Fields.PROJECT, value)

    @property
    def name(self):
        """The name of this entity.

        Returns:
            str
        """
        return self._getattr(self.NAME_FIELD)

    @name.setter
    def name(self, value):
        """Sets the name of this entity.

        Args:
            value (str): name for this entity
        """
        if not isinstance(value, basestring):
            raise ValueError("Expected string, not {}".format(type(value)))
        self._setattr(self.NAME_FIELD, value)

    @property
    def notes(self):
        """Notes attached to this entity."""
        return self._getmutable(const.Fields.NOTES, mutableType=list)

    @notes.setter
    def notes(self, value):
        """Notes attached to this entity."""
        return self._setattr(const.Fields.NOTES, value, isMutable=True)

    @property
    def parent(self):
        """Parent attached to the entity."""
        return self._getattr(const.Fields.PARENT_ENTITY)

    @parent.setter
    def parent(self, value):
        """Sets the parent of the entity."""
        self._setattr(const.Fields.PARENT_ENTITY, value)

    @property
    def children(self):
        """Children of the entity."""
        return self._getmutable(const.Fields.CHILD_ENTITY, mutableType=list)

    @children.setter
    def children(self, value):
        """Sets/adds a child to the entity."""
        self._setattr(const.Fields.CHILD_ENTITY, value, isMutable=True)

    @property
    def versions(self):
        """Gets all the versions that exist under the entity."""
        return self._getmutable(const.Fields.VERSIONS, mutableType=list)

    @versions.setter
    def versions(self, value):
        """Sets/adds a version entity to this entity."""
        self._setattr(const.Fields.VERSIONS, value, isMutable=True)

    @property
    def url(self):
        """The URL for the details page of this entity. Useful for debugging."""
        return "{}/detail/{}/{}".format(self.server._shotgun.base_url, self.ENTITY_TYPE, self.id)

    @property
    def openNotes(self):
        """List of open notes (which are different from Notes) on this entity.

        Returns:
            list
        """
        return self._getmutable(const.Fields.OPEN_NOTES, mutableType=list)

    @property
    def thumbnailVersions(self):
        """The version(s) for this entity for which displayAsThumbnail is True.

        Returns:
            list of rsg_clients.shotgun.entities.version.Version: This entity's thumbnail version(s).
        """
        # check all versions, in case multiple have displayAsThumbnail = True
        thumbVersions = []

        for version in self.versions:
            if version.displayAsThumbnail:
                thumbVersions.append(version)

        return thumbVersions

    @property
    def thumbnailUrl(self):
        """URL to get the thumbnail file. Consider further development to provide the image through the version / API.

        Returns:
            str: URL to the thumbnail
        """
        if const.Fields.THUMBNAIL not in self.DEFAULT_FIELDS:
            raise exceptions.InvalidField(const.Fields.THUMBNAIL)
        thumbnailPath = self._getattr(const.Fields.THUMBNAIL)
        if not thumbnailPath:
            raise exceptions.ThumbnailDoesNotExist(
                "{} entity {} {} does not have a thumbnail.".format(self.type, self.id, self.name)
            )
        return thumbnailPath

    @property
    def createdBy(self):
        """Who created this entity.

        Returns:
            rsg_clients.shotgun.entities.humanUser.HumanUser
        """
        return self._getattr(const.Fields.CREATED_BY)

    @property
    def updatedBy(self):
        """Last person who updated this entity.

        Returns:
            rsg_clients.shotgun.entities.humanUser.HumanUser
        """
        return self._getattr(const.Fields.UPDATED_BY)

    @property
    def dateCreated(self):
        """Date that this entity was created.

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.DATE_CREATED)

    @property
    def dateUpdated(self):
        """Date that this entity was last updated.

        Returns:
            datetime.datetime
        """
        return self._getattr(const.Fields.DATE_UPDATED)

    def uploadVersion(self, path, bugstarId = None):
        """Uploads a file as a version linked to this entity, using the sg_uploaded_movie field.

        Args:
            path (str): The path of the file to be uploaded.
            bugstarId (int, optional): The Bugstar ID of the file. This allows us to use an existing version if one can
                be located that has the same bugstar ID.
            overwrite (bool, optional): Whether to update an existing version if one is found (by bugstar ID).

        Returns:
            rsg_clients.shotgun.entities.version.Version: The newly-created (or located, via bugstarId) version.
        """
        # if **this entity** already has a version with this bug number, just give back the existing version. done.
        # TODO: I'm keeping this from the previous code - do we want to keep it? Shouldn't this be handled more at implementation level, rather than here?
        version = (
            self.shotgun.version.getByBugstarId(bugstarId, [[const.Fields.ENTITY, const.Operators.IS, self]])
            if bugstarId
            else None
        )
        if version:
            return version

        # otherwise make a new version on this entity
        version = self.shotgun.version()
        version.name = os.path.splitext(os.path.basename(path))[0]
        version.project = self.project
        version.bugstarId = bugstarId if bugstarId else None

        if isinstance(self, self.shotgun.task.cls()):
            version.task = self
            version.entity = self.entity
        else:
            version.entity = self

        version.create()
        self.versions.append(version)
        self.updateFields([const.Fields.VERSIONS])

        # now that we've created the version, we can actually upload the file to it
        self.shotgun.uploadToVersion(version.id, path)

        self.refresh()
        version.refresh()
        return version

    def uploadThumbnail(self, path, bugstarId=None, overwrite=True):
        """Uploads a thumbnail. By default, adds it as a version.

        It has been decided that shotgun thumbnails should be handled as versions, by default. As such, when they are
        uploaded, thumbnails are uploaded to the sg_uploaded_movie field of a version that is associated with this
        entity. The displayAsThumbnail field of this version is then set to True, and that of all other associated
        versions is set to False.

        By default, if a thumbnail already exists on an entity, uploading a new one will overwrite the existing version
        for which displayAsThumbnail is True. This can be changed by setting the overwrite argument to False.

        Args:
            path (str): Path to the file to upload
            overwrite (bool): Whether to overwrite the file of the existing thumbnail version, if it exists.
            bugstarId (int, optional): ID number by which the attachment is saved in bugstar.

        Returns:
            rsg_clients.entities.version.Version: Version created or updated for the thumbnail.
        """
        thumbVersion = None

        # if len(thumbnailVersions) is 0, there's nothing to overwrite; if it's more than 1, we don't know which
        #   to overwrite, so we don't overwrite at all, but we should throw a warning. is there a good place to log?
        tvs = self.thumbnailVersions
        if overwrite and len(tvs) == 1:
            thumbVersion = tvs[0]
            self.shotgun.uploadToVersion(thumbVersion.id, path, overwrite=overwrite)

            # this should update even if bugstarId is None, in which case it will (and should) clear
            thumbVersion.bugstarId = bugstarId
            thumbVersion.name = os.path.splitext(os.path.basename(path))[0]

            thumbVersion.updateFields([thumbVersion.NAME_FIELD, const.Fields.BUGSTAR_ID])
            thumbVersion.refresh()

        else:
            thumbVersion = self.uploadVersion(path, bugstarId)
            thumbVersion.displayAsThumbnail = True
            thumbVersion.updateFields([const.Fields.DISPLAY_AS_THUMBNAIL])

            # this re-creation of the versions array is only necessary here; if overwriting, the overwritten version is
            #   already the only version with displayAsThumbnail=True
            newVersionsList = [thumbVersion]
            batchUpdateList = []
            for version in self.versions:
                if version != thumbVersion:
                    version.displayAsThumbnail = False
                    newVersionsList.append(version)
                    batchUpdateList.append(version)

            self.server.batch(update=batchUpdateList)
            self.versions = newVersionsList

        self.refresh()
        return thumbVersion

    def downloadThumbnail(self, path=None):
        """Downloads the thumbnail to the given location on disk or gets the thumbnail data with requests.

        Args:
            path (str, optional): path to download the file to. If None, get thumbnail data with requests instead.

        Returns:
            requests.models.Response: Contains the data from the thumbnail.
        """
        if path:
            downloadLocation = os.path.dirname(path)
            if not os.path.exists(downloadLocation):
                try:
                    os.makedirs(downloadLocation)
                except Exception as error:
                    raise exceptions.FolderCreationError(downloadLocation, message=str(error))
            urllib.urlretrieve(self.thumbnailUrl, path)
            return None
        else:
            return self.server.session.get(self.thumbnailUrl, stream=True)

    def uploadToThumbnailField(self, path):
        """Uploads an image to the thumbnail field directly.

        Note that per our usage of shotgun, this should only be done when absolutely necessary; if at all possible,
        upload thumbnails and versions as Version entities, using uploadVersion and uploadThumbnail, defined above.
        However, this is a necessary method to expose to handle cases in which entities are not associated with
        a project, e.g. users, and as such cannot be linked to versions, which require a project. This will overwrite
        any existing thumbnail that this entity had previously.

        Args:
            path (str): Path to the file to upload as the new thumbnail image.

        Returns:
            int: Result of the uploadThumbnail operation in shotgun_api3.
        """
        return self.shotgun.uploadThumbnail(self.ENTITY_TYPE, self.id, path)

    def find(self, filters, fields=None):
        """Performs a find operation on this type of entity on shotgun.

        Args:
            filters (list): a list of filter conditions to perform
            fields (list): list of fields to return

        Returns:
            list of dict or None
        """
        return self.shotgun.find(self.ENTITY_TYPE, filters, fields or self.DEFAULT_FIELDS)

    def foundDuplicates(self, field):
        """Checks if object with this field and same value already exists on the server.

        If we have 2 same operations running at the same time,
        one might create the same object while the second process is in a middle of building it in the memory,
        which will cause a creation of the duplicate objects in Shotgun.

        Args:
            field (const.Fields): field to compare if the entity with value from this entity already exists on the server

        Returns:
            list of dicts
        """
        serverItem = self.find(filters=[[field, const.Operators.IS, self._getattr(field)]])
        return serverItem

    def create(self, data=None, validateField=None):
        """Creates this type of entity on shotgun with the provided data.

        Args:
            data (dict, optional): information to add to shotgun
            validateField(str, optional): Pass a Field to check if there is an object on the server
                                                    with the same value as this entity object.

        Returns:
            dict
        """
        if self._id is not None:
            raise exceptions.EntityExists()

        if validateField:
            duplicateTasks = self.foundDuplicates(validateField)

            if validateField and duplicateTasks:
                duplicateIds = "\n\t ".join([str(task[const.Fields.ID]) for task in duplicateTasks])
                raise exceptions.EntityExists(
                    message="Found duplicate {}(s) whose field {} match. \n\t{}.".format(
                        self.ENTITY_TYPE, validateField, duplicateIds
                    )
                )

        # Get the raw data
        newData = self.shotgun.create(self.ENTITY_TYPE, data or self.toDict(create=True, onlyEditableFields=True))

        self._id = newData[const.Fields.ID]
        self.parse(newData)
        self._cache()

        return self._id

    def update(self, data=None, clear=True):
        """Updates this entity in shotgun with the latest provided data.

        Args:
            data (dict, optional): information to update on shotgun
            clear (bool, optional): clears out the edited data from the entity

        Returns:
            dict
        """
        if self._id is None:
            raise Exception("This instance does not exist on shotgun")
        data = data or self.toDict(onlyDirty=True, onlyEditableFields=True, create=True)
        if not data:
            raise Exception("No changes detected to update shotgun with")
        newData = self.shotgun.update(self.ENTITY_TYPE, index=self._id, data=data)
        self.parse(newData)
        if clear:
            self.removeEdits()
        return newData

    def updateFields(self, fields):
        """Updates specific fields on shotgun.

        Arguments:
            fields (list): list of fields to update
        """
        if isinstance(fields, basestring):
            fields = [fields]

        data = self.toDict(specificFields=fields, create=True)
        if data:
            self.update(data=data, clear=False)
            self.removeEdits(fields=fields)

    def upload(self, path, field=None, name=None, tags=None):
        """Uploads a file as an attachment to this entity.

        Arguments:
            path (str): path on disk to the file that should be uploaded

            field (str, optional): field to add file to
            name (str, optional): short name for the file, it will default to the file name.
            tags (str, optional): tags to include on the attachment.

        Returns:
            rsg_core.shotgun.entities.attachment.Attachment
        """
        return self.shotgun.upload(self.ENTITY_TYPE, self._id, path, field=field, name=name, tags=tags)

    def copy(self, thumbnail=False):
        """Makes a copy in memory of this entity, without the id and notes.

        The user must invoke create on the newly copied entity to add it to Shotgun

        Args:
            thumbnail (bool): Use the thumbnail from the source task (requires IO) for the new destination task.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        copy = self.__class__(self.shotgun)
        values = self._fields.copy()
        values.pop(const.Fields.ID, None)
        values.pop(const.Fields.NOTES, None)
        values.pop(const.Fields.OPEN_NOTES, None)  # Read only Shotgun Field
        values.pop("type", None)  # Extra information returned by Shotgun that doesn't correlate to a field
        values.pop(const.Fields.BUGSTAR_ID, None)  # This id must be unique to the entity being created.

        # Downloading thumbnail and replacing the url of the image with its downloaded location on a disc,
        # because shotgunAPI has a check during upload_thumbnail, which is called during the creation of the task,
        # that checks if the image exists on the disc.
        thumbnailImage = values.get(const.Fields.THUMBNAIL, None)
        if thumbnailImage and urlparse.urlparse(thumbnailImage).scheme:
            if thumbnail:
                imageId = thumbnailImage.split("?")[0].split("/")[-1]
                downloadLocation = os.path.join(tempfile.gettempdir(), "{}.jpg".format(imageId))
                self.downloadThumbnail(downloadLocation)
                if os.path.exists(downloadLocation):
                    values[const.Fields.THUMBNAIL] = downloadLocation
            else:
                values.pop(const.Fields.THUMBNAIL, None)
        copy._fields = values
        return copy

    def delete(self):
        """Deletes this entity in Shotgun.

        Returns:
            bool: Operation result.
        """
        if self._id is None:
            raise exceptions.EntityDoesNotExist("This instance does not exist on shotgun")
        results = self.shotgun.delete(self.ENTITY_TYPE, self._id)
        self._removeFromCache(force=True)
        return results

    def refresh(self):
        """Refreshes the internal cache of the instance."""
        if self._id is None:
            raise exceptions.EntityDoesNotExist("This instance does not exist on shotgun")
        data = self.find([[const.Fields.ID, const.Operators.IS, self._id]])
        if not data:
            raise exceptions.EntityDoesNotExist("This instance does not exist on shotgun")

        data = data[0]
        self._refresh(data)

    def parse(self, data):
        """Resolves data so that data is converted to the expected type.

        This method was created with the intention of receiving the results of a shotgun find function, which is a list of
        dictionaries. It replaces the contents of the list in place with instances of our custom entity classes.

        Arguments:
            data (dict or list): the source mutable iterable that contains data from shotgun
        """
        self._fields.update(self._parse(self.shotgun, data))
        self._isUpdated = True

    @classmethod
    def _parse(cls, shotgun, data, depth=0):
        """
        Resolves data so that data is converted to the expected type.

        This method was created with the intention of receiving the results of a shotgun find function, which is a list of
        dictionaries. It replaces the contents of the list in place with instances of our custom entity classes.

        Arguments:
            shotgun (rsg_core.shotgun.core.Shotgun): rockstar shotgun instance
            data (dict or list): the source mutable iterable that contains data from shotgun

            depth (int): recursion depth

        Examples:
            from rsg_core.shotgun import servers, factory, const

            # Set our default connector

            # Convert this dictionary into an entity
            dataDict = {'id': 171, 'name': 'David Vega', 'type': 'HumanUser'}
            dataList = [dataDict] # resolveData changes the contents of the list/dictionary you pass in
            entityList = resolveData(servers.development.shotgun, dataList, enumerate(dataList))

        Returns:
            dict or list
        """
        if isinstance(data, dict):
            iterable = data.iteritems()
        elif isinstance(data, (tuple, list)):
            iterable = enumerate(data)
        else:
            raise ValueError(
                "Expected a dictionary, list or tuple but received {} instead.".format(data.__class__.__name__)
            )

        for key, value in iterable:
            if isinstance(value, dict) and "type" in value and "id" in value:
                entityType = value["type"]
                entityId = value["id"]
                entityClass = cls._getSubclassByType(entityType) or cls._getSubclassByType("Unknown")
                data[key] = entityClass(
                    shotgun=shotgun, index=entityId, data=value if entityClass.ENTITY_TYPE == "Unknown" else None
                )
            elif isinstance(value, (tuple, list)):
                data[key] = cls._parse(shotgun=shotgun, data=value, depth=depth + 1)
            elif isinstance(value, datetime.datetime):
                data[key] = conversions.Datetime.setTimeZoneToUtc(value)
        return data

    # TODO: Remove url:bugstar:6079258
    def addNote(self, text, author, title=None):
        """Adds a note to this entity.

        Args:
            text (str): text to include in the note
            author (rsg_core.shotgun.entities.humanUser.HumanUser): the person who wrote the note

            title (str, optional): title for the note, defaults to the text if nothing is supplied as a title
        """
        if self._id is None:
            raise exceptions.EntityDoesNotExist("This instance does not exist on shotgun")
        return self.shotgun.addNote(entity=self, text=text, author=author, title=title)

    # TODO: Remove url:bugstar:6079258
    def addReply(self, text, author):
        """Adds a reply to this entity.

        Args:
            text (str): text to include in the reply
            author (rsg_core.shotgun.entities.humanUser.HumanUser): the person who wrote the reply
        """
        if self._id is None:
            raise exceptions.EntityDoesNotExist("This instance does not exist on shotgun")
        return self.shotgun.addReply(entity=self, text=text, author=author)

    def _setEstimatedTimeValue(self, field, minutes):
        """Sets the text value for the given text field so it reads in days hours and minutes.

        Args:
            field (str): the field to set the human readable value to
            minutes (float): the float value in minutes of how long it takes to complete a task
        """
        if minutes is None:
            return self._setattr(field, minutes)

        # Set a human readable text value
        seconds = minutes * 60
        minutes, _ = divmod(seconds, 60)  # get the number of minutes left for the task
        hours, minutes = divmod(minutes, 60)  # get the number of hours and remaining minutes
        days, hours = divmod(hours, 8)

        minutes = int(minutes)
        hours = int(hours)
        days = int(days)

        self._setattr(
            field,
            " ".join(
                [
                    "{} {}".format(value, key)
                    for value, key in ((days, "days"), (hours, "hours"), (minutes, "minutes"))
                    if value
                ]
            ),
        )
