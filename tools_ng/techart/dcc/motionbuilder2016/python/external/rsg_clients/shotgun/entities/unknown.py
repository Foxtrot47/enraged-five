from rsg_clients.shotgun.entities import baseEntity


class Unknown(baseEntity.BaseEntity):
    """
    Class to capture entities that currently don't have a class
    """

    ENTITY_TYPE = "Unknown"
    _INSTANCES = {}
    DEFAULT_FIELDS = ()

    def __init__(self, index=None, data=None, shotgun=None):
        """
        Constructor

        Arguments:
            shotgun (rsg_core.shotgun.core.Shotgun): shotgun instance to perform operations with
            index (int): id of the entity
            data (dictionary): data that represents this entity
        """
        super(Unknown, self).__init__(shotgun=shotgun, index=index, data=data)
        self._type = data.get("type", None)

    @property
    def type(self):
        return self._type

    @classmethod
    def builder(cls):
        return Builder


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Unknown
