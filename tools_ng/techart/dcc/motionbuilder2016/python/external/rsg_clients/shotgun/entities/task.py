import types
import collections

from rsg_clients.shotgun import const, exceptions
from rsg_clients.shotgun.entities import baseEntity


class SubTypes(baseEntity.BaseEntitySubtypes):
    BUG = const.SubTypes.BUG
    ART = const.SubTypes.ART
    LEGAL = const.SubTypes.LEGAL
    SCRIPT_WRITING = const.SubTypes.SCRIPT_WRITING
    SCRIPT_RECORDING = const.SubTypes.SCRIPT_RECORDING
    RIGGING = const.SubTypes.RIGGING


class Task(baseEntity.BaseEntity):
    """
    Class that represents a Task entity
    """

    ENTITY_TYPE = const.Entities.TASK
    ENDPOINT = const.Endpoints.TASK
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.PROJECT,
        const.Fields.BUGSTAR_ACTUAL_TIME,
        const.Fields.BUGSTAR_CREATED_ON,
        const.Fields.BUGSTAR_LAST_MODIFIED_ON,
        const.Fields.BUGSTAR_CHANGELISTS,
        const.Fields.BUGSTAR_COMPONENT,
        const.Fields.BUGSTAR_ESTIMATED_TIME,
        const.Fields.BUGSTAR_DEADLINE,
        const.Fields.BUGSTAR_GRID,
        const.Fields.BUGSTAR_ID,
        const.Fields.BUG_NUMBER,
        const.Fields.BUG_URL,
        const.Fields.BUG_SUMMARY,
        const.Fields.CONTENT,
        const.Fields.WORKFLOW,
        const.Fields.NONDEFAULT_DESCRIPTION,
        const.Fields.STATUS,
        const.Fields.STATUS_NAME,
        const.Fields.OWNERS,
        const.Fields.REVIEWER,
        const.Fields.START_DATE,
        const.Fields.ESTIMATED_TIME,
        const.Fields.ESTIMATED_TIME_VALUE,
        const.Fields.DUE_DATE,
        const.Fields.ENTITY,
        const.Fields.NOTES,
        const.Fields.OPEN_NOTES,
        const.Fields.PIPELINE_STEP,
        const.Fields.TAGS,
        const.Fields.REVISION,
        const.Fields.CC,
        const.Fields.QA_OWNER,
        const.Fields.MISSION,
        const.Fields.IN_BUGSTAR,
        const.Fields.DEADLINE,
        const.Fields.ASSETS,
        const.Fields.SUGGESTED_ESTIMATED_TIME,
        const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.THUMBNAIL,
        const.Fields.GOAL,
        const.Fields.PARENT_ENTITY,
        const.Fields.CHILD_ENTITY,
        const.Fields.PROGRESS,
        const.Fields.VERSIONS,
        const.Fields.CHANGELISTS,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
        const.Fields.GOALS,
    )

    NAME_FIELD = const.Fields.CONTENT

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    def _setup(self, index=None, data=None, server=None):
        """
        Overrides inherited method

        Arguments:
            index (int): id of the object from the server
            data (dictionary): data that represents this object
            server (rsg_core_py.abstracts.abstractServer.AbstractServer): the local server to set for this object
        """
        self._bugNote = None
        return super(Task, self)._setup(index, data, server)

    @property
    def subtype(self):
        """
        The entity subtype for this entity

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """
        Sets the entity subtype for this entity

        Arguments:
            value (str): entity subtype
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def inBugstar(self):
        """
        Is this task in bugstar.
        Used when a task is created to determine if a matching bug needs to be created for this new task.

        Returns:
            bool
        """
        return self._getattr(const.Fields.IN_BUGSTAR)

    @inBugstar.setter
    def inBugstar(self, value):
        """
        Sets if this bug is in bugstar

        Arguments:
            value (bool): is this task in bugstar as a bug
        """
        self._setattr(const.Fields.IN_BUGSTAR, value)

    @property
    def bugSummary(self):
        """
        The summary of the bug associated with this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUG_SUMMARY)

    @bugSummary.setter
    def bugSummary(self, value):
        """
        Sets the bug summary for this task

        Arguments:
            value (str): the summary/title of the bug
        """
        self._setattr(const.Fields.BUG_SUMMARY, value)

    @property
    def bugstarCreatedOn(self):
        """
        The createdOn of the bug associated with this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUGSTAR_CREATED_ON)

    @bugstarCreatedOn.setter
    def bugstarCreatedOn(self, value):
        """
        Sets the bug createdOn for this task

        Arguments:
            value (str): the createdOn/title of the bug
        """
        self._setattr(const.Fields.BUGSTAR_CREATED_ON, value)

    @property
    def bugstarLastModifiedOn(self):
        """
        The bugstarLastModifiedOn of the bug associated with this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUGSTAR_LAST_MODIFIED_ON)

    @bugstarLastModifiedOn.setter
    def bugstarLastModifiedOn(self, value):
        """
        Sets the bug bugstarLastModifiedOn for this task

        Arguments:
            value (str): the bugstarLastModifiedOn/title of the bug
        """
        self._setattr(const.Fields.BUGSTAR_LAST_MODIFIED_ON, value)

    @property
    def bugNumber(self):
        """The bug number associated with this task.

        Note:
            This property is now deprecated

        Returns:
            int
        """
        return self._getattr(const.Fields.BUG_NUMBER)

    @bugNumber.setter
    def bugNumber(self, value):
        """The bug number associated with this task.

        Note:
            This property is now deprecated

        Returns:
            int
        """
        return self._setattr(const.Fields.BUG_NUMBER, value)

    @property
    def bugstarId(self):
        """The bugstar Id associated with this task.

        Note:
            This property is now deprecated

        Returns:
            int
        """
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        """Sets the bugstar id associated with this task.

        Also sets the bugstar url to match the new bugstar Id if applicable, the subtype to bug and the bug number for
        backwards compatibility.

        Arguments:
            value (int): the bugstar Id to set for this task
        """
        url = None
        if not isinstance(value, (int, types.NoneType)):
            typeName = type(value).__name__
            raise ValueError("Expected int or None and got {} instead".format(typeName))

        elif value is not None:
            url = {
                "name": str(value),
                "url": "bugstar://{}".format(value),
                "link_type": "url",
                "content_type": "string",
            }

        self._setattr(const.Fields.BUGSTAR_ID, value)
        self._setattr(const.Fields.BUG_NUMBER, value)
        self._setattr(const.Fields.BUG_URL, url)
        self._setattr(const.Fields.ENTITY_SUBTYPE, SubTypes.BUG)

    @property
    def bugUrl(self):
        """
        The url to open the bug in bugstar associated

        Returns:
            dict
        """
        return self._getattr(const.Fields.BUG_URL)

    @property
    def bugNote(self):
        """
        The note that contains all the comments that appear in Bugstar

        Returns:
            rsg_core.shotgun.entites.note.Note
        """
        if not self.bugNumber:
            return

        if not self._bugNote or self not in self._bugNote.tasks:
            noteName = "Bug {} Comments".format(self.bugNumber)
            for note in self.notes:
                if noteName == note.subject:
                    self._bugNote = note
                    break

        return self._bugNote

    @property
    def bugComments(self):
        """
        Comments from bugstar

        Returns:
            list
        """
        if self._bugNote is None and self.bugstarId:
            for note in self.notes:
                if note.name == "Bug {} Comments".format(self.bugstarId):
                    self._bugNote = note
                    break
        return self._bugNote.replies

    @bugComments.setter
    def bugComments(self, value):
        """
        The list of assets to associate with this task

        Arguments:
            value (list): list of assets
        """
        bugNote = self.bugNote
        if bugNote:
            bugNote.replies = value
        raise exceptions.EntityDoesNotExist("This task does not have a bug type Note to hold the comments of the bug")

    @property
    def assets(self):
        """
        List of assets associated with this task

        Returns:
            list
        """
        return self._getmutable(const.Fields.ASSETS, mutableType=list)

    @assets.setter
    def assets(self, value):
        """
        The list of assets to associate with this task

        Arguments:
            value (list): list of assets
        """
        self._setattr(const.Fields.ASSETS, value, isMutable=True)

    @property
    def status(self):
        """
        The status code that this task is currently set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the current status code and updates the status name field to reflect the new status code being set.

        Arguments
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        """
        The status name of the current status set for this entity

        Returns:
            str
        """
        return const.Statuses.getName(self.status)

    @property
    def description(self):
        """
        The description of this task

        Returns:
            str
        """
        return self._getattr(const.Fields.NONDEFAULT_DESCRIPTION)

    @description.setter
    def description(self, value):
        """
        Sets the description of this task

        Args:
            value (str): description to set
        """
        self._setattr(const.Fields.NONDEFAULT_DESCRIPTION, value)

    @property
    def workflow(self):
        """
        The workflow of this task

        Returns:
            str
        """
        return self._getattr(const.Fields.WORKFLOW)

    @workflow.setter
    def workflow(self, value):
        """
        Sets the workflow of this task

        Args:
            value (str): workflow to set
        """
        self._setattr(const.Fields.WORKFLOW, value)

    @property
    def owners(self):
        """
        The current owners of this field.
        This uses a default field in shotgun that can accept a list of owners.

        Returns:
            list
        """
        return self._getmutable(const.Fields.OWNERS, mutableType=list)

    @owners.setter
    def owners(self, value):
        """
        Sets the owners for this task

        Args:
            value (list): list of owners for this task
        """
        if not isinstance(value, (list, tuple)):
            raise ValueError("Excepted a list or tuple, received {} instead".format(value))
        self._setattr(const.Fields.OWNERS, value, isMutable=True)

    @property
    def qaOwner(self):
        """
        The QA owner for this task

        Returns:
            rsg_core.shotgun.entity.humanUser.HumanUser or rsg_core.shotgun.entity.team.Team
        """
        return self._getattr(const.Fields.QA_OWNER)

    @qaOwner.setter
    def qaOwner(self, value):
        """
        Sets the QA owner for this task

        Returns:
            rsg_core.shotgun.entity.humanUser.HumanUser or rsg_core.shotgun.entity.team.Team
        """
        self._setattr(const.Fields.QA_OWNER, value)

    @property
    def reviewer(self):
        """
        The current reviewer of this field.
        This uses a default field in shotgun that can accept a list of reviewer.

        Returns:
            list
        """
        return self._getmutable(const.Fields.REVIEWER, mutableType=list)

    @reviewer.setter
    def reviewer(self, value):
        """
        Sets the reviewer for this task

        Args:
            value (list): list of reviewer for this task
        """
        if not isinstance(value, (list, tuple)):
            raise ValueError("Excepted a list or tuple, received {} instead".format(value))
        self._setattr(const.Fields.REVIEWER, value, isMutable=True)

    @property
    def cinematicAnimator(self):
        """
        The cinematic animator from the mission associated with this task.

        Returns:
            rsg_core.shotgun.entity.humanUser.HumanUser or rsg_core.shotgun.entity.team.Team
        """
        if self.mission:
            return self.mission.cinematicAnimator

    @cinematicAnimator.setter
    def cinematicAnimator(self, value):
        """
        Sets the QA owner for this task

        Returns:
            rsg_core.shotgun.entity.humanUser.HumanUser or rsg_core.shotgun.entity.team.Team
        """
        if self.mission:
            self.mission.cinematicAnimator = value

    @property
    def changelists(self):
        """
        The current changelists of this task.

        Returns:
            list
        """
        return self._getmutable(const.Fields.CHANGELISTS, mutableType=list)

    @changelists.setter
    def changelists(self, value):
        """
        Sets the changelists for this task

        Args:
            value (list): list of changelists for this task
        """
        if not isinstance(value, collections.Iterable):
            raise ValueError("Excepted a list or tuple, received {} instead".format(value))
        self._setattr(const.Fields.CHANGELISTS, value, isMutable=True)

    @property
    def dueDate(self):
        """
        The due date set for this task

        Returns:
            str
        """
        return self._getattr(const.Fields.DUE_DATE)

    @dueDate.setter
    def dueDate(self, value):
        """
        Sets the due date for this task

        Arguments:
            value(str): the new due date for this task
        """
        self._setattr(const.Fields.DUE_DATE, value)

    @property
    def startDate(self):
        """
        The start date set for this task

        Returns:
            str
        """
        return self._getattr(const.Fields.START_DATE)

    @startDate.setter
    def startDate(self, value):
        """
        Sets the start date for this task

        Arguments:
            value(str): the new start date for this task
        """
        self._setattr(const.Fields.START_DATE, value)

    @property
    def bugstarChangelists(self):
        """
        The bugstarChangelists associated with this task

        Returns:
            list
        """
        return self._getmutable(const.Fields.BUGSTAR_CHANGELISTS, mutableType=list)

    @bugstarChangelists.setter
    def bugstarChangelists(self, value):
        """
        Sets the bugstarChangelists associated with this task

        Arguments:
            value (list): list of bugstarChangelists to associate with this bug

        Returns:
            list
        """
        self._setattr(const.Fields.BUGSTAR_CHANGELISTS, value, isMutable=True)

    @property
    def bugstarComponent(self):
        """
        The bugstarComponent associated with this task

        Returns:
            rsg_clients.shotgun.entities.component.Component
        """
        return self._getattr(const.Fields.BUGSTAR_COMPONENT)

    @bugstarComponent.setter
    def bugstarComponent(self, value):
        """
        Sets the bugstarComponent associated with this task

        Arguments:
            value (list): list of bugstarComponent to associate with this bug

        Returns:
            list
        """
        self._setattr(const.Fields.BUGSTAR_COMPONENT, value, isMutable=True)

    @property
    def bugstarGrid(self):
        """
        The bugstarGrid associated with this task

        Returns:
            rsg_clients.shotgun.entities.location.Location
        """
        return self._getattr(const.Fields.BUGSTAR_GRID)

    @bugstarGrid.setter
    def bugstarGrid(self, value):
        """
        Sets the bugstarGrid associated with this task

        Arguments:
            value (rsg_clients.shotgun.entities.location.Location): list of bugstarGrid to associate with this bug

        Returns:
            list
        """
        self._setattr(const.Fields.BUGSTAR_GRID, value)

    @property
    def bugstarActualTime(self):
        """
        The estimated time set for this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUGSTAR_ACTUAL_TIME)

    @bugstarActualTime.setter
    def bugstarActualTime(self, value):
        """
        Sets the amount of time it will take this task to be finished

        Arguments:
            value(str): the new start date for this task
        """
        self._setattr(const.Fields.BUGSTAR_ACTUAL_TIME, value)

    @property
    def bugstarEstimatedTime(self):
        """
        The estimated time set for this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUGSTAR_ESTIMATED_TIME)

    @bugstarEstimatedTime.setter
    def bugstarEstimatedTime(self, value):
        """
        Sets the amount of time it will take this task to be finished

        Arguments:
            value(str): the new start date for this task
        """
        self._setattr(const.Fields.BUGSTAR_ESTIMATED_TIME, value)
        self._setEstimatedTimeValue(const.Fields.ESTIMATED_TIME_VALUE, value)

    @property
    def estimatedTimeValue(self):
        """
        The human readable version of the suggested estimated time it should take to address the work
        described in the note based on the frame range provided by the note
        """
        return self._getattr(const.Fields.ESTIMATED_TIME_VALUE)

    @property
    def entity(self):
        """
        The entity that this task is attached to

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        return self._getattr(const.Fields.ENTITY)

    @entity.setter
    def entity(self, value):
        """
        Sets the entity that this task should be attached to

        Arguments:
            value (rsg_core.shotgun.entities.baseEntity.BaseEntity): the entity to attach this task to
        """
        self._setattr(const.Fields.ENTITY, value)

    @property
    def pipelineStep(self):
        """
        The pipeline step this task is a part of

        Returns:
            rsg_core.shotgun.entities.pipelineStep.PipelineStep
        """
        return self._getattr(const.Fields.PIPELINE_STEP)

    @pipelineStep.setter
    def pipelineStep(self, value):
        """
        The pipeline step to set for this task.

        Args:
            value (rsg_core.shotgun.entities.pipelineStep.PipelineStep): the pipeline step to set for this task

        Returns:

        """
        self._setattr(const.Fields.PIPELINE_STEP, value)

    @property
    def progress(self):
        """
        Gets the progress of the task

        Returns:
            int
        """
        return self._getattr(const.Fields.PROGRESS)

    @progress.setter
    def progress(self, value):
        """
        Sets the progress of the task
        """
        self._setattr(const.Fields.PROGRESS, value)

    @property
    def tags(self):
        """
        The tags associated with this task

        Returns:
            list
        """
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        """
        Sets the tags associated with this task

        Arguments:
            value (list): list of tags to associate with this bug

        Returns:
            list
        """
        self._setattr(const.Fields.TAGS, value, isMutable=True)

    @property
    def tasks(self):
        """
        Gets the tasks under the task

        Returns:
            list
        """
        return self._getmutable(const.Fields.TASKS, mutableType=list)

    @tasks.setter
    def tasks(self, value):
        """
        Sets/adds a task

        Args:
            value (rockstar.shotgun.entities.task.Task): task to add as a child task.
        """
        self._setattr(const.Fields.TASKS, value, isMutable=True)

    @property
    def revision(self):
        """
        The revision number for the bug associated with this task.

        Returns:
            int
        """
        return self._getattr(const.Fields.REVISION)

    @revision.setter
    def revision(self, value):
        """
        Sets the revision number for the bug associated with this task

        Arguments:
            value (int): the revision number for the bug associated with this task
        """
        self._setattr(const.Fields.REVISION, value)

    @property
    def cc(self):
        """
        List of individuals cc'd to the bug associated with this task in bugstar

        Returns:
            list
        """
        return self._getmutable(const.Fields.CC, mutableType=list)

    @cc.setter
    def cc(self, value):
        """
        Sets the list of people cc'd to this task

        Arguments:
            value (list): list of people cc'd to the bug associated with this task in bugstar
        """
        self._setattr(const.Fields.CC, value, isMutable=True)

    # TODO: Remove and replace with a new entity field once we start putting up shoots on shotgun
    @property
    def shootNumber(self):
        return self._getattr(const.Fields.SHOOT_NUMBER)

    @shootNumber.setter
    def shootNumber(self, value):
        self._setattr(const.Fields.SHOOT_NUMBER, value)

    @property
    def mission(self):
        """
        The mission that the bug associated with this task belongs to

        Returns:
            rsg_core.shotgun.entities.mission.Mission
        """
        return self._getattr(const.Fields.MISSION)

    @mission.setter
    def mission(self, value):
        """
        Sets the mission the bug associated with this task belongs to

        Arguments:
            value (rsg_core.shotgun.entities.mission.Mission): mission to set for this task
        """
        self._setattr(const.Fields.MISSION, value)

    @property
    def deadline(self):
        """
        The name of the deadline set for the bug associated with this task

        Returns:
            str
        """
        return self._getattr(const.Fields.BUGSTAR_DEADLINE)

    @deadline.setter
    def deadline(self, value):
        """
        Sets the name of the deadline that is set for the bug this task is associated with.

        Args:
            value (str): name of the deadline
        """
        if value is not None and not self.server.deadline.isInstance(value):
            raise ValueError("Excepted Deadline or None, received {} instead".format(type(value)))
        self._setattr(const.Fields.BUGSTAR_DEADLINE, value)
        self._setattr(const.Fields.DEADLINE, value.name if value else None)  # We are setting this for backwards compatibility purposes

    @property
    def suggestedEstimatedTime(self):
        """
        The suggested estimated time it should take to address the work described in the note based on the frame range
        provided by the note
        """
        return self._getattr(const.Fields.SUGGESTED_ESTIMATED_TIME)

    @property
    def suggestedEstimatedTimeValue(self):
        """
        The human readable version of the suggested estimated time it should take to address the work
        described in the note based on the frame range provided by the note
        """
        return self._getattr(const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE)

    @property
    def goals(self):
        """
        The goals that the bug associated with this task belongs to

        Returns:
            list of rsg_core.shotgun.entities.goal.Goal
        """
        return self._getmutable(const.Fields.GOALS, mutableType=list)

    @goals.setter
    def goals(self, value):
        """
        Sets the goals the bug associated with this task belongs to

        Arguments:
            value (list of ockstar.core.shotgun.entities.goal.Goal): goal to set for this task
        """
        self._setattr(const.Fields.GOALS, value, isMutable=True)

    def _setSuggestedEstimatedTime(self):
        """ Sets the updated suggested estimated time based on all the notes this task has """
        value = 0
        for note in self.notes:
            note._setSuggestedEstimateTime()  # Calculate the latest suggested estimated time
            if note.subtype in (const.SubTypes.FACE, const.SubTypes.BODY) and note.suggestedEstimatedTime is not None:
                value += note.suggestedEstimatedTime
        self._setattr(const.Fields.SUGGESTED_ESTIMATED_TIME, value)
        self._setEstimatedTimeValue(const.Fields.SUGGESTED_ESTIMATED_TIME_VALUE, value)

    def note(self, title, text=None, author=None, subtype=const.SubTypes.NOTE):
        """
        Creates a note for this entity

        Arguments:
            title (str): title of the note
            text (str): text to add to the reply

        Keyword Args:
            author (rsg_core.shotgun.entities.humanUser.HumanUser): user that wrote the note.
                    Defaults to the current user.
            subtype (str): The subtype of the note. Defaults to Note.
        """
        note = self.server.note()
        note.project = self.project
        note.author = author or self.server.currentUser
        note.subtype = subtype
        note.subject = title
        note.body = text or title
        note.task = [self]
        note.link = self.entity
        self.notes.append(note)
        return note


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Task

    @property
    def subtypes(self):
        return SubTypes

    def getByBugSummary(self, bugSummary, filters=None):
        """
        Gets the entity based on the bug summary associated with it

        Arguments:
            bugSummary (str): name of the entity

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.BUG_SUMMARY, const.Operators.IS, bugSummary])
        entity = self.find(filters)
        if entity:
            return entity[0]

    def getByBugNumber(self, bugNumber, filters=None):
        """
        Gets the entity based on the bug number associated with it.

        Notes:
            This method is now deprecated.

        Arguments:
            bugNumber (int): the value of the bug number field on a task

            filters (list, optional): list of addiontal filters to use.

        Returns:
            rsg_clients.shotgun.entities.task.Task
        """
        # TODO: Remove this function
        # Use the new bugstar id field first to make sure we don't break anything.
        entity = self.getByBugstarId(bugNumber, filters)
        if entity:
            return entity

        filters = filters or []
        filters.append([const.Fields.BUG_NUMBER, const.Operators.IS, bugNumber])
        entity = self.find(filters)
        if entity:
            return entity[0]

    def getByBugstarId(self, bugstarId, filters=None):
        """Gets the entity based on the bug number associated with it.

        Arguments:
            bugstarId (int): the value of the bugstar id field on a task

            filters (list, optional): list of addiontal filters to use.

        Returns:
            rsg_clients.shotgun.entities.task.Task
        """
        filters = filters or []
        filters.extend(
            [
                [const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId],
                [const.Fields.ENTITY_SUBTYPE, const.Operators.IS, SubTypes.BUG],
            ]
        )
        entity = self.find(filters)
        if entity:
            return entity[0]
