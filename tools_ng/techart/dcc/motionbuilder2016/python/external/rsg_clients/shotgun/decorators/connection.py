"""
Decorators that deal with the shotgun connection and queries
"""
import httplib
import socket
import functools

from shotgun_api3 import shotgun

from rsg_clients.shotgun import exceptions


def resolve(func):
    """
    Wraps the given CRUD method to resolve some errors into specifc custom errors.

    Args:
        func (function): function being wrapped

    Returns:
        func
    """

    @functools.wraps(func)
    def wrapper(instance, *args, **kwargs):
        """
        Extra logic for the function being wrapped

        Args:
            instance (object): the instance of object that this method is being invoked from
            url (str): string that is being passed into the get or post method

        Keyword Args:
            returnRaw (bool): return the results of this query without any extra processing

        Returns:
            dictionary
        """
        try:
            return func(instance, *args, **kwargs)
        except shotgun.Fault as exception:
            errorString = str(exception)
            if "No space left on device" in errorString:
                raise exceptions.NoDiskSpace(errorString)
            raise exception

        except (socket.error, shotgun.ProtocolError, httplib.IncompleteRead) as exception:
            errorString = str(exception)
            raise exceptions.ServerUnavailable(server=instance.type, message=errorString, connection=instance)

    return wrapper
