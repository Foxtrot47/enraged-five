from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Ticket(baseEntity.BaseEntity):
    """
    Class that represents a Ticket entity
    """

    ENTITY_TYPE = const.Entities.TICKET
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.PROJECT,
        const.Fields.BUG_NUMBER,
        const.Fields.TITLE,
        const.Fields.DESCRIPTION,
        const.Fields.STATUS,
        const.Fields.STATUS_NAME,
        const.Fields.ADDRESSINGS_TO,
        const.Fields.START_DATE,
        const.Fields.ESTIMATED_TIME,
        const.Fields.DUE_DATE,
        const.Fields.ENTITY,
        const.Fields.REPLIES,
    )

    NAME_FIELD = const.Fields.TITLE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        return Builder

    @property
    def project(self):
        return self._getattr(const.Fields.PROJECT)

    @project.setter
    def project(self, value):
        self._setattr(const.Fields.PROJECT, value)

    @property
    def bugNumber(self):
        return self._getattr(const.Fields.BUG_NUMBER)

    @bugNumber.setter
    def bugNumber(self, value):
        self._setattr(const.Fields.BUG_NUMBER, value)

    @property
    def name(self):
        return self._getattr(const.Fields.TITLE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.TITLE, value)

    @property
    def description(self):
        return self._getattr(const.Fields.DESCRIPTION)

    @description.setter
    def description(self, value):
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def status(self):
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        return const.Statuses.getName(self.status)

    @property
    def owners(self):
        return self._getmutable(const.Fields.ADDRESSINGS_TO, mutableType=list)

    @owners.setter
    def owners(self, value):
        if not isinstance(value, (list, tuple)):
            raise ValueError("Excepted a list or tuple, received {} instead".format(value))
        self._setattr(const.Fields.ADDRESSINGS_TO, value, isMutable=True)

    @property
    def qaOwner(self):
        return self._getattr(const.Fields.QA_OWNER)

    @owners.setter
    def qaOwner(self, value):
        self._setattr(const.Fields.QA_OWNER, value)

    @property
    def cc(self):
        return self._getmutable(const.Fields.CC, mutableType=list)

    @cc.setter
    def cc(self, value):
        if not isinstance(value, (list, tuple)):
            raise ValueError("Excepted a list or tuple, received {} instead".format(value))
        self._setattr(const.Fields.CC, value, isMutable=True)

    @property
    def dueDate(self):
        return self._getattr(const.Fields.DUE_DATE)

    @dueDate.setter
    def dueDate(self, value):
        self._setattr(const.Fields.DUE_DATE, value)

    @property
    def dateCreated(self):
        return self._getattr(const.Fields.DATE_CREATED)

    @dateCreated.setter
    def dateCreated(self, value):
        self._setattr(const.Fields.DATE_CREATED, value)

    @property
    def actualTime(self):
        return self._getattr(const.Fields.TICKET_ACTUAL_TIME)

    @actualTime.setter
    def actualTime(self, value):
        self._setattr(const.Fields.TICKET_ACTUAL_TIME, value)

    @property
    def estimatedTime(self):
        return self._getattr(const.Fields.ESTIMATE)

    @estimatedTime.setter
    def estimatedTime(self, value):
        self._setattr(const.Fields.ESTIMATE, value)

    def remainingTime(self):
        return self._getattr(const.Fields.REMAINING_TIME)

    @property
    def priority(self):
        return self._getattr(const.Fields.PRIORITY)

    @priority.setter
    def priority(self, value):
        self._setattr(const.Fields.PRIORITY, value)

    @property
    def category(self):
        return self._getattr(const.Fields.CLASS)

    @category.setter
    def category(self, value):
        self._setattr(const.Fields.CLASS, value)

    @property
    def replies(self):
        return self._getmutable(const.Fields.REPLIES, mutableType=list)

    @replies.setter
    def replies(self, value):
        self._setattr(const.Fields.REPLIES, value, isMutable=True)

    @property
    def tags(self):
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        self._setattr(const.Fields.TAGS, value, mutableType=True)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Ticket

    def getByBugNumber(self, bugNumber, filters=None):
        """`
        Gets the entity based on the bug number associated with it

        Arguments:
            name (str): name of the entity
            shotgun (rsg_core.shotgun.core.Shotgun): shotgun instance to perform operations with

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.BUG_NUMBER, const.Operators.IS, bugNumber])
        entity = self.find(filters)
        if entity:
            return entity[0]
