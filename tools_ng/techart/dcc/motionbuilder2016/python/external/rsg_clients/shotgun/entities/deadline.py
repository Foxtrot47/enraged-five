from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Deadline(baseEntity.BaseEntity):
    """
    Class that represents a Department entity
    """

    ENTITY_TYPE = const.Entities.DEADLINE
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.USERS,
        const.Fields.NOTES,
        const.Fields.PROJECT,
        const.Fields.DESCRIPTION,
        const.Fields.SG_START_DATE,
        const.Fields.SG_DUE_DATE,
        const.Fields.SUBMISSION_DATE,
        const.Fields.PARENT_ENTITY,
        const.Fields.CHILD_ENTITY,
        const.Fields.BUGSTAR_ID
    )
    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def code(self):
        return self._getattr(const.Fields.CODE)

    @code.setter
    def code(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def users(self):
        return self._getmutable(const.Fields.USERS, mutableType=list)

    @users.setter
    def users(self, value):
        self._setattr(const.Fields.USERS, value, isMutable=True)

    @property
    def startDate(self):
        return self._getattr(const.Fields.SG_START_DATE)

    @startDate.setter
    def startDate(self, value):
        self._setattr(const.Fields.SG_START_DATE, value)

    @property
    def dueDate(self):
        return self._getattr(const.Fields.SG_DUE_DATE)

    @dueDate.setter
    def dueDate(self, value):
        self._setattr(const.Fields.SG_DUE_DATE, value)

    @property
    def submissionDate(self):
        return self._getattrtable(const.Fields.SUBMISSION_DATE)

    @submissionDate.setter
    def submissionDate(self, value):
        self._setattr(const.Fields.SUBMISSION_DATE, value)

    @property
    def description(self):
        return self._getattr(const.Fields.DESCRIPTION)

    @submissionDate.setter
    def description(self, value):
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def bugstarId(self):
        """The bugstar Id associated with this task.

        Returns:
            int
        """
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        """Sets the bugstar id associated with this deadline.

        Arguments:
            value (int): the bugstar Id to set for this task
        """
        if not isinstance(value, (int, type(None))):
            typeName = type(value).__name__
            raise ValueError("Expected int or None and got {} instead".format(typeName))
        self._setattr(const.Fields.BUGSTAR_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core.abstracts.AbstractPayload.abstractPayload
        """
        return Deadline

    def getByBugstarId(self, bugstarId, filters=None):
        """
        Gets the version object with the specified Bugstar ID.

        Args:
            bugstarId (int): bugstar Id number of the attachment that the version is presenting
            filters (list, optional): list of [rsg_core.shotgun.const.Fields, rsg_core.shotgun.const.Operations, value]

        Returns:
            rsg_core.shotgun.version.Version
        """
        filters = filters or []
        filters.append([const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None
