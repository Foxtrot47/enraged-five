from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Group(baseEntity.BaseEntity):
    """
    Class that represents a Group entity
    """

    ENTITY_TYPE = const.Entities.GROUP
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.NOTES,
        const.Fields.USERS,
        const.Fields.BUGSTAR_ID,
    )
    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def users(self):
        return self._getmutable(const.Fields.USERS, mutableType=list)

    @users.setter
    def users(self, value):
        self._setattr(const.Fields.USERS, value, isMutable=True)

    @property
    def bugstarId(self):
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        self._setattr(const.Fields.BUGSTAR_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Group

    def getByBugstarId(self, bugstarId):
        """
        Get a group from shotgun based on the team bugstar id

        Arguments:
            bugstarId (int): the bugstar id of the team

        Returns:
            rockstare.shotgun.entities.group.Group
        """
        groups = self.find([[const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId]])
        if groups:
            return groups[0]
