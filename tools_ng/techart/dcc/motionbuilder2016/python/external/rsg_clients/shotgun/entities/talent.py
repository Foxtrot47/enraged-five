from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Talent(baseEntity.BaseEntity):
    """
    Class that represents a talent member entity
    """

    ENTITY_TYPE = const.Entities.TALENT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.STATUS,
        const.Fields.CODE,
        const.Fields.PARENT_ENTITY,
        const.Fields.CHILD_ENTITY,
        const.Fields.WATCHSTAR_ID,
        const.Fields.GENDER,
        const.Fields.SPECIES,
        const.Fields.HEIGHT,
        const.Fields.FIRST_NAME,
        const.Fields.LAST_NAME,
        const.Fields.VIRTUAL_HEIGHT,
    )
    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def status(self):
        """
        The status code that this task is currently set to

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """
        Sets the current status code and updates the status name field to reflect the new status code being set.

        Arguments
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)
        self._setattr(const.Fields.STATUS_NAME, const.Statuses.getName(value))

    @property
    def statusName(self):
        """
        The status name of the current status set for this entity

        Returns:
            str
        """
        return const.Statuses.getName(self.status)

    @property
    def parent(self):
        """
        The parent of this enitity

        Returns:
            Photogrammetry
        """
        return self._getattr(const.Fields.PARENT_ID)

    @parent.setter
    def parent(self, value):
        """
        Sets the parent of this entity

        Arguments
            value (Photogrammetry): the parent for this entity
        """
        self._setattr(const.Fields.PARENT_ID, value)

    @property
    def children(self):
        """
        The children of this entity

        Returns:
            list of Photogrammetry
        """
        return self._getmutable(const.Fields.CHILD_ENTITY, mutableType=list)

    @children.setter
    def children(self, value):
        """
        Sets the current children that are allowed to view this entity

        Arguments
            value (list): children of this entity, they must be of the same type as this entity
        """
        self._setattr(const.Fields.CHILD_ENTITY, value, isMutable=True)

    @property
    def realWorldHeight(self):
        """
        The real world height of the actor in imperial units (feet and inches)

        Returns:
            str
        """
        return self._getattr(const.Fields.HEIGHT)

    @realWorldHeight.setter
    def realWorldHeight(self, value):
        """Sets the real world height of the talent

        Arguments
            value (str): height of the actor in imperial values (feet and inches)
        """
        self._setattr(const.Fields.HEIGHT, value)

    @property
    def virtualHeight(self):
        """
        The ingame height of the actor in imperial units (feet and inches)

        Returns:
            str
        """
        return self._getattr(const.Fields.VIRTUAL_HEIGHT)

    @virtualHeight.setter
    def virtualHeight(self, value):
        """Sets the ingame height of the talent

        Arguments
            value (str): ingame height of the actor in imperial values (feet and inches)
        """
        self._setattr(const.Fields.VIRTUAL_HEIGHT, value)

    @property
    def watchstarId(self):
        """The unique watchstar id for this talent."""
        return self._getattr(const.Fields.WATCHSTAR_ID)

    @watchstarId.setter
    def watchstarId(self, value):
        """Sets the unique watchstar id for this talent.

        Args:
            value (int): the unique id in watchstar for this data
        """
        self._setattr(const.Fields.WATCHSTAR_ID, value)

    @property
    def species(self):
        """The species of the talent

        Returns:
            str
        """
        return self._getattr(const.Fields.SPECIES)

    @species.setter
    def species(self, value):
        """Sets the species of the talent.

        Arguments
            value (str): species of the talent (human, animal, mocap prop)
        """
        self._setattr(const.Fields.SPECIES, value)

    @property
    def gender(self):
        """The gender of the talent

        Returns:
            str
        """
        return self._getattr(const.Fields.GENDER)

    @gender.setter
    def gender(self, value):
        """Sets the gender of the talent.

        Arguments
            value (str): gender of the talent (male, female)
        """
        self._setattr(const.Fields.GENDER, value)


    @property
    def firstName(self):
        """The firstName of the talent

        Returns:
            str
        """
        return self._getattr(const.Fields.FIRST_NAME)

    @firstName.setter
    def firstName(self, value):
        """Sets the firstName of the talent.

        Arguments
            value (str): firstName of the talent 
        """
        self._setattr(const.Fields.FIRST_NAME, value)
        
    @property
    def lastName(self):
        """The lastName of the talent

        Returns:
            str
        """
        return self._getattr(const.Fields.LAST_NAME)

    @lastName.setter
    def lastName(self, value):
        """Sets the lastName of the talent.

        Arguments
            value (str): lastName of the talent (male, female)
        """
        self._setattr(const.Fields.LAST_NAME, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Talent

    def getByWatchstarId(self, watchstarId, filters=None):
        """Gets the entity based on the watchstar id associated with it.

        Args:
            watchstarId (int): the unique watchstar id to look for
            filters (list of list): list of additional filters to include for this search.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseEntity
        """
        filters = filters or []
        filters.append([const.Fields.WATCHSTAR_ID, const.Operators.IS, watchstarId])
        entity = self.find(filters)
        if entity:
            return entity[0]
        return None
