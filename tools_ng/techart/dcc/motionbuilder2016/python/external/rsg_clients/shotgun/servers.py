from rsg_clients.shotgun import core, const

from rsg_core_py.metaclasses import singleton
from rsg_core_py.abstracts import abstractServer


class BaseServer(abstractServer.AbstractServer):
    """
    Base server class for interacting with a shotgun server
    """

    NAME = None
    TYPE = None

    def __init__(self):
        """
        Constructor
        """
        self._shotgun = core.Shotgun(self.NAME)
        self._delegate = None
        self._currentUser = None

    def logout(self):
        """ Logs out the server """
        self._shotgun.logout()

    def builder(self, entityType):
        """
        Gets the builder to create entities of a specific type

        Args:
            entityType (str): name of the shotgun entity type

        Returns:
            rsg_core.shotgun.entities.basePayload.BaseBuilder
        """
        return self.instance._getBuilder(entityType)

    @property
    def instance(self):
        """
        Overrides inherited method.

        The inherited interface for getting shotgun instance to perform all the commands through

        Returns:
            rsg_core.shotgun.core.Shotgun
        """
        return self._shotgun

    @property
    def shotgun(self):
        """
        The shotgun instance to perform all the commands through

        Returns:
            rsg_core.shotgun.core.Shotgun
        """
        return self._shotgun

    @property
    def currentUser(self):
        """
        Gets the shotgun human user instance of the current user accessing shotgun

        Returns:
            rsg_core.shotgun.entities.humanUser.HumanUser
        """
        if not self._currentUser:
            self._currentUser = self.humanUser.getByName(self.account.name)
        return self._currentUser

    @property
    def actionMenuItem(self):
        """
        Action Menu Item Builder
        """
        return self._shotgun.actionMenuItem

    @property
    def asset(self):
        """
        Asset Builder
        """
        return self._shotgun.asset

    @property
    def attachment(self):
        """
        Attachment Builder
        """
        return self._shotgun.attachment

    @property
    def brand(self):
        """
        Brand Builder
        """
        return self._shotgun.brand

    @property
    def casting(self):
        """
        Casting Builder
        """
        return self._shotgun.casting

    @property
    def component(self):
        """Component Builder."""
        return self._shotgun.component

    @property
    def feature(self):
        """
        Feature Builder
        """
        return self._shotgun.feature

    @property
    def department(self):
        """
        Department Builder
        """
        return self._shotgun.department

    @property
    def deadline(self):
        """
        Department Builder
        """
        return self._shotgun.deadline

    @property
    def eventLogEntry(self):
        """
        Event Log Entry Builder
        """
        return self._shotgun.eventLogEntry

    @property
    def faceCaptureVideo(self):
        """
        Face Capture Video Builder
        """
        return self._shotgun.faceCaptureVideo

    @property
    def motionScene(self):
        """
        Motionscene Builder
        """
        return self._shotgun.motionScene

    @property
    def goal(self):
        """
        Goal Builder
        """
        return self._shotgun.goal

    @property
    def group(self):
        """
        Group Builder
        """
        return self._shotgun.group

    @property
    def humanUser(self):
        """
        Human User Builder
        """
        return self._shotgun.humanUser

    @property
    def legalClearance(self):
        """
        Legal Clearance Builder
        """
        return self._shotgun.legalClearance

    @property
    def location(self):
        """Location Builder."""
        return self._shotgun.location

    @property
    def note(self):
        """
        Note Builder
        """
        return self._shotgun.note

    @property
    def packages(self):
        """Packages builder."""
        return self._shotgun.packages

    @property
    def project(self):
        """
        Project Builder
        """
        return self._shotgun.project

    @property
    def photogrammetry(self):
        """
        Photogrammetry Builder
        """
        return self._shotgun.photogrammetry

    @property
    def reply(self):
        """
        Reply Builder
        """
        return self._shotgun.reply

    @property
    def step(self):
        """
        Pipeline Step Builder
        """
        return self._shotgun.step

    @property
    def strand(self):
        """
        Strand Builder
        """
        return self._shotgun.strand

    @property
    def script(self):
        """
        Strand Builder
        """
        return self._shotgun.script

    @property
    def tag(self):
        """
        Tag Builder
        """
        return self._shotgun.tag

    @property
    def talent(self):
        """
        Talent Builder
        """
        return self._shotgun.talent

    @property
    def task(self):
        """
        Task Builder
        """
        return self._shotgun.task

    @property
    def ticket(self):
        """
        Ticket Builder
        """
        return self._shotgun.ticket

    @property
    def unknown(self):
        """
        Unknown Builder
        """
        return self._shotgun.unknown

    @property
    def version(self):
        """
        Version Builder
        """
        return self._shotgun.version

    def batch(self, create=None, update=None, delete=None):
        """
        Batch processes create, update and delete operations in a single call.
        If one fails, then all the commands fail.

        Keyword Args:
            create (list): list of entities to create
            update (list): list of entities to update
            delete (list): list of entities to delete

        Returns:
            list
        """
        return self.instance.batch(create, update, delete)


class Production(BaseServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.PRODUCTION
    TYPE = const.Servers.getType(NAME)


class Development(BaseServer):
    __metaclass__ = singleton.Singleton
    NAME = const.Servers.DEVELOPMENT
    TYPE = const.Servers.getType(NAME)


def getByName(name):
    """
    Gets the server based on the name

    Args:
        name (str): name of the server to get

    Returns:
        rsg_core.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.NAME == name:
            return cls()


def getByType(serverType):
    """
    Gets the server based on the type

    Args:
        serverType (str): name of the type of server to get

    Returns:
        rsg_core.shotgun.servers.BaseServer
    """
    for cls in BaseServer.__subclasses__():
        if cls.TYPE == serverType:
            return cls()


production = Production()
development = Development()
