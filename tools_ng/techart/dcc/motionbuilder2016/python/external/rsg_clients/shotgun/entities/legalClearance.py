"""Entity class to interface with Shotgun Legal Clearance records."""

from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class LegalClearanceTypes(baseEntity.BaseEntitySubtypes):
    """Types of legal clearances. I'm intentionally not calling this "subtypes", because it doesn't use that field."""

    GRAPHICS_2D = "2D Graphics"
    AREA_NAMES = "Area Names"
    BRANDS = "Brands"
    BUILDING_ARCHITECTURE = "Building Architecture"
    BUILDING_SIGNAGE = "Building Signage"
    CHARACTER_NAMES = "Character Names"
    GARMENTS = "Garments"
    INTERIOR_ARCHITECTURE = "Interior Architecture"
    POLICY = "Policy"
    PROPS = "Props"
    RENDERS = "Renders"
    SCULPTURES = "Sculptures"
    TEXT = "Text"
    WEAPONS = "Weapons"


class LegalClearance(baseEntity.BaseEntity):
    """Class that represents a legal clearance entity."""

    ENTITY_TYPE = const.Entities.LEGAL_CLEARANCE

    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.STATUS,
        const.Fields.DESCRIPTION,
        const.Fields.PROJECT,
        const.Fields.LEGAL_CLEARANCE_TYPE,
        const.Fields.ADDITIONAL_INFORMATION,
        const.Fields.CATEGORY,
        const.Fields.SUBCATEGORY,
        const.Fields.THUMBNAIL,
        const.Fields.VERSIONS,
        const.Fields.GRID_LOCATION,
        const.Fields.BUG_NUMBER,
        const.Fields.BUG_URL,
        const.Fields.INSPIRATION_NAMES,
        const.Fields.INSPIRATION_ADDRESS,
        const.Fields.SIMILAR_IRL_SURROUNDINGS,
        const.Fields.IRL_LOCATION_SIGNAGE,
        const.Fields.ARTIST,
    )

    NAME_FIELD = const.Fields.CODE
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def status(self):
        """The status code that this legal clearance is currently set to.

        Returns:
            str
        """
        return self._getattr(const.Fields.STATUS)

    @status.setter
    def status(self, value):
        """Sets the current status code and updates the status name field to reflect the new status code being set.

        Args:
            value (str): status code name to set
        """
        self._setattr(const.Fields.STATUS, value)

    @property
    def project(self):
        """The project that this legal clearance is tied to.

        Returns:
            dict: Project-representing dictionary.
        """
        return self._getattr(const.Fields.PROJECT)

    @project.setter
    def project(self, value):
        """The project to set this legal clearance to.

        Args:
            value (dict): Dictionary representing target project.
        """
        self._setattr(const.Fields.PROJECT, value)

    @property
    def tags(self):
        """The tags that are allowed to view this entity.

        Returns:
            rsg_core.shotgun.entities.tag.Tag
        """
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        """Sets the current tags that are allowed to view this entity.

        Args:
            value (str): tags code name to set
        """
        self._setattr(const.Fields.TAGS, value, isMutable=True)

    @property
    def name(self):
        """Gets the legal clearance name / code of the record.

        Returns:
            str: The legal clearance entity name.
        """
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        """Set the legal clearance name / code of the record.

        Args:
            value (str): The value to set the code to.
        """
        self._setattr(const.Fields.CODE, value)

    @property
    def artist(self):
        """Gets the artist for the record.

        Returns:
            str: The artist entity.
        """
        return self._getattr(const.Fields.ARTIST)

    @artist.setter
    def artist(self, value):
        """Set the artist entity for the record.

        Args:
            value (rsg_clients.shotgun.entities.HumanUser.humanUser): The artist entity.
        """
        self._setattr(const.Fields.ARTIST, value)

    @property
    def additionalInformation(self):
        """Gets the additional information (free-form text field).

        Returns:
            str: Text in the field.
        """
        return self._getattr(const.Fields.ADDITIONAL_INFORMATION)

    @additionalInformation.setter
    def additionalInformation(self, value):
        """Sets the additional information (free-form text field).

        Args:
            value (str): Value to which to set the field.
        """
        self._setattr(const.Fields.ADDITIONAL_INFORMATION, value)

    @property
    def bugNumber(self):
        """Get the bug number associated with this record.

        Returns:
            int: The bug number.
        """
        return self._getattr(const.Fields.BUG_NUMBER)

    @bugNumber.setter
    def bugNumber(self, value):
        """Set the bug number of the record.

        Args:
            value (int): The value to set the bug number to.
        """
        self._setattr(const.Fields.BUG_NUMBER, value)

    @property
    def bugUrl(self):
        """Get the bug URL associated with this record.

        Returns:
            str: The bugstar URL.
        """
        return self._getattr(const.Fields.BUG_URL)

    @bugUrl.setter
    def bugUrl(self, value):
        """set the bug URL associated with this record.

        Args:
            value (str or int): If int, or str of 7-digit str, creates the url; otherwise, just sets the url to value.
        """
        try:
            value = int(value)
            strValue = str(value).zfill(7)
            bugUrl = "bugstar://{}".format(strValue)
            self._setattr(const.Fields.BUG_URL, {"url": bugUrl})

        except ValueError:
            # this means it's not a number - just use the given value as the bug url
            self._setattr(const.Fields.BUG_URL, value)

    @property
    def category(self):
        """Get the category of this record.

        Returns:
            str: The category of this record.
        """
        return self._getattr(const.Fields.CATEGORY)

    @category.setter
    def category(self, value):
        """Set the category of this record.

        Args:
            value (str): The category to set this record to.
        """
        self._setattr(const.Fields.CATEGORY, value)

    @property
    def subcategory(self):
        """Get the subcategory of this record.

        Returns:
            str: The subcategory of this record.
        """
        return self._getattr(const.Fields.SUBCATEGORY)

    @subcategory.setter
    def subcategory(self, value):
        """Set the subcategory of this record.

        Args:
            value (str): The subcategory to set this record to.
        """
        self._setattr(const.Fields.SUBCATEGORY, value)

    @property
    def description(self):
        """Gets the description for this record.

        Returns:
            str: The description.
        """
        return self._getattr(const.Fields.DESCRIPTION)

    @description.setter
    def description(self, value):
        """Sets the description for this record.

        Args:
            Value (str): The new description.
        """
        self._setattr(const.Fields.DESCRIPTION, value)

    @property
    def legalClearanceType(self):
        """Get the legal clearance type of this record.

        Returns:
            str: The legal clearance type.
        """
        return self._getattr(const.Fields.LEGAL_CLEARANCE_TYPE)

    @legalClearanceType.setter
    def legalClearanceType(self, value):
        """Set the legal clearance type of this record.

        Args:
            value (str): Value to set the legal clearance type to.
        """
        self._setattr(const.Fields.LEGAL_CLEARANCE_TYPE, value)

    @property
    def inspirationInformation(self):
        """Get the inspiration information for this record.

        Returns:
              str: inspiration information for this record.
        """
        return self._getattr(const.Fields.INSPIRATION_INFORMATION)

    @inspirationInformation.setter
    def inspirationInformation(self, value):
        """Set the inspiration information for this record.

        Args:
            value (str): New inspiration information.
        """
        self._setattr(const.Fields.INSPIRATION_INFORMATION, value)

    @property
    def inspirationNames(self):
        """Gets the inspiration names for this record.

        Returns:
            str: Inspiration names.
        """
        return self._getattr(const.Fields.INSPIRATION_NAMES)

    @inspirationNames.setter
    def inspirationNames(self, value):
        """Sets the inspiration names.

        Args:
            value (str): New inspiration names.
        """
        self._setattr(const.Fields.INSPIRATION_NAMES, value)

    @property
    def inspirationMapLink(self):
        """Gets the inspiration map link value.

        Returns:
            str: Map link.
        """
        return self._getattr(const.Fields.INSPIRATION_MAP_LINK)

    @inspirationMapLink.setter
    def inspirationMapLink(self, value):
        """Sets the inspiration map link value.

        Args:
            value (str): Map link value.
        """
        self._setattr(const.Fields.INSPIRATION_MAP_LINK, value)

    @property
    def inspirationAddress(self):
        """Gets the inspiration address for this record.

        Returns:
            str: Inspiration address.
        """
        return self._getattr(const.Fields.INSPIRATION_ADDRESS)

    @inspirationAddress.setter
    def inspirationAddress(self, value):
        """Sets the inspiration address.

        Args:
            value (str): New inspiration address.
        """
        self._setattr(const.Fields.INSPIRATION_ADDRESS, value)

    @property
    def similarIrlSurroundings(self):
        """Get the similar irl surroundings value for this record.

        Returns:
            str: Value of the field.
        """
        return self._getattr(const.Fields.SIMILAR_IRL_SURROUNDINGS)

    @similarIrlSurroundings.setter
    def similarIrlSurroundings(self, value):
        """Set the similar irl surroudings value for this record.

        Args:
            value (str): New value for the field.
        """
        self._setattr(const.Fields.SIMILAR_IRL_SURROUNDINGS, value)

    @property
    def irlLocationSignage(self):
        """Get the IRL location signage for this record.

        Returns:
            str: Value of the field.
        """
        return self._getmutable(const.Fields.IRL_LOCATION_SIGNAGE)

    @irlLocationSignage.setter
    def irlLocationSignage(self, value):
        """Set the irl location signage for this record.

        Args:
            value (str): New value for the field.
        """
        self._setattr(const.Fields.IRL_LOCATION_SIGNAGE, value)

    @property
    def location(self):
        """The location entity that this lce corresponds to.

        Returns:
            rsg_clients.shotgun.entities.location.Location: The location entity.
        """
        return self._getattr(const.Fields.GRID_LOCATION)

    @location.setter
    def location(self, value):
        """Sets the location entity for this LCE.

        Args:
            value (rsg_clients.shotgun.entities.location.Location: The location entity to set as this lce's loc.
        """
        self._setattr(const.Fields.GRID_LOCATION, value)


class Builder(baseEntity.BaseBuilder):
    """Photogrammetry builder."""

    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_core.abstracts.AbstractPayload.abstractPayload
        """
        return LegalClearance
