from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Reply(baseEntity.BaseEntity):
    """
    Class that represents a Reply entity
    """

    ENTITY_TYPE = const.Entities.REPLY
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.PROJECT,
        const.Fields.USER,
        const.Fields.CONTENT,
        const.Fields.ENTITY,
        const.Fields.BUGSTAR_CREATED_ON,
        const.Fields.POSITION,
    )
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    NAME_FIELD = const.Fields.CONTENT

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def project(self):
        return self._getattr(const.Fields.PROJECT)

    @project.setter
    def project(self, value):
        self._setattr(const.Fields.PROJECT, value)

    @property
    def author(self):
        return self._getattr(const.Fields.USER)

    @author.setter
    def author(self, value):
        self._setattr(const.Fields.USER, value)

    @property
    def body(self):
        return self._getattr(const.Fields.CONTENT)

    @body.setter
    def body(self, value):
        self._setattr(const.Fields.CONTENT, value)

    @property
    def link(self):
        return self._getattr(const.Fields.ENTITY)

    @link.setter
    def link(self, value):
        return self._setattr(const.Fields.ENTITY, value)

    @property
    def bugstarCreatedOn(self):
        return self._getattr(const.Fields.BUGSTAR_CREATED_ON)

    @bugstarCreatedOn.setter
    def bugstarCreatedOn(self, value):
        self._setattr(const.Fields.BUGSTAR_CREATED_ON, value)

    @property
    def position(self):
        return self._getattr(const.Fields.POSITION)

    @position.setter
    def position(self, value):
        self._setattr(const.Fields.POSITION, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Reply
