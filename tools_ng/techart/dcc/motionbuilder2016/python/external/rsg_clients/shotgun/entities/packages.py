"""Entity class to interact with the Packages entity type in Shotgun."""

from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity

class SubTypes(baseEntity.BaseEntitySubtypes):
    """Packages subtypes and their corresponding strings in Shotgun."""
    PREFAB = "Prefab"
    RECIPE = "Recipe"

class Packages(baseEntity.BaseEntity):
    """Class that represents a Packages entity."""
    ENTITY_TYPE = const.Entities.PACKAGES
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.ENTITY_SUBTYPE,
        const.Fields.GLOBAL_ENTITY_PROJECT,
        const.Fields.STATUS,
        const.Fields.GROUP,
        const.Fields.CODE,
        const.Fields.TAGS
    )
    NAME_FIELD = const.Fields.CODE
    SUBTYPES = SubTypes

    DATETIME_FORMAT = "%Y-%m-%d"

    _INSTANCES = {}

    @classmethod
    def builder(cls):
        """The class for the builder to create instances of Packages.

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def project(self):
        """The project this Packages is currently set to.

        Returns:
            rsg_core.shotgun.entities.project.Project
        """
        return self._getattr(const.Fields.GLOBAL_ENTITY_PROJECT)

    @project.setter
    def project(self, value):
        """Sets the current project.

        Args:
            value (str): Project to set this to point at.
        """
        self._setattr(const.Fields.GLOBAL_ENTITY_PROJECT, value)

    @property
    def subtype(self):
        """The subtype for this packages asset.

        Returns:
            str
        """
        return self._getattr(const.Fields.ENTITY_SUBTYPE)

    @subtype.setter
    def subtype(self, value):
        """Sets this entity's subtype code.

        Args:
            value (str): New subtype value.
        """
        self._setattr(const.Fields.ENTITY_SUBTYPE, value)

    @property
    def tags(self):
        """The tags that are allowed to view this entity.
        Returns:
            rsg_core.shotgun.entities.tag.Tag
        """
        return self._getmutable(const.Fields.TAGS, mutableType=list)

    @tags.setter
    def tags(self, value):
        """Sets the current tags that are allowed to view this ent
        Args:
            value (str): tags code name to set
        """
        self._setattr(const.Fields.TAGS, value, isMutable=True)

class Builder(baseEntity.BaseBuilder):
    """Packages builder."""

    @classmethod
    def cls(cls):
        """Class for the object the builder creates.

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Packages

    @property
    def subtypes(self):
        """Gets subtypes."""
        return SubTypes
