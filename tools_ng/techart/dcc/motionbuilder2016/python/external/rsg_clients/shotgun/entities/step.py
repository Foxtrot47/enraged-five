from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Step(baseEntity.BaseEntity):
    """
    Class that represents a Task entity
    """

    ENTITY_TYPE = const.Entities.STEP
    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []
    DEFAULT_FIELDS = (const.Fields.ID, const.Fields.CODE, const.Fields.ENTITY_TYPE, const.Fields.SHORT_NAME)
    NAME_FIELD = const.Fields.CODE

    @classmethod
    def builder(cls):
        """
        The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def name(self):
        return self._getattr(const.Fields.CODE)

    @name.setter
    def name(self, value):
        self._setattr(const.Fields.CODE, value)

    @property
    def shortName(self):
        return self._getattr(const.Fields.SHORT_NAME)

    @shortName.setter
    def shortName(self, value):
        self._setattr(const.Fields.SHORT_NAME, value)

    @property
    def entityType(self):
        return self._getattr(const.Fields.ENTITY_TYPE)

    @entityType.setter
    def entityType(self, value):
        self._setattr(const.Fields.ENTITY_TYPE, value)

    @property
    def backendStepID(self):
        """Gets the backend (mongo) step ID of this shotgun step (the integer ID, not the internal mongo hash).

        Returns:
            int: The backend step ID.
        """
        return self._getattr(const.Fields.BACKEND_STEP_ID)

    @backendStepID.setter
    def backendStepID(self, value):
        """Sets the backend (mongo) step ID of this shotgun step (the integer ID, not the internal mongo hash).

        Args:
            value (int): The backend step ID.
        """
        return self._setattr(const.Fields.BACKEND_STEP_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Step

    def getByBackendStepID(self, mongoID, filters=None):
        """Gets the step(s) with a give mongo step ID.

        Returns:
            list: A list of shotgun steps that have the mongo ID.
        """
        class_ = self.cls()
        filters = filters or []
        filters.append([const.Fields.BACKEND_STEP_ID, const.Operators.IS, mongoID])
        return self.find(filters)
