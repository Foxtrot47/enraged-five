from rsg_clients.shotgun import const
from rsg_clients.shotgun.entities import baseEntity


class Component(baseEntity.BaseEntity):
    """
    Class that represents a Component entity
    """

    ENTITY_TYPE = const.Entities.COMPONENT
    DEFAULT_FIELDS = (
        const.Fields.ID,
        const.Fields.CODE,
        const.Fields.PROJECT,
        const.Fields.BUGSTAR_ID,
        const.Fields.DATE_CREATED,
        const.Fields.DATE_UPDATED,
        const.Fields.CREATED_BY,
        const.Fields.UPDATED_BY,
    )

    NAME_FIELD = const.Fields.CODE

    _INSTANCES = {}
    _LAST_ACCESSED_INSTANCES = []

    @classmethod
    def builder(cls):
        """The class for the builder that is used to build instances of this class

        Returns:
            rsg_core.shotgun.entities.baseEntity.BaseBuilder
        """
        return Builder

    @property
    def bugstarId(self):
        return self._getattr(const.Fields.BUGSTAR_ID)

    @bugstarId.setter
    def bugstarId(self, value):
        self._setattr(const.Fields.BUGSTAR_ID, value)


class Builder(baseEntity.BaseBuilder):
    @classmethod
    def cls(cls):
        """
        Class for the object the builder creates

        Returns:
            rsg_core_py.abstracts.AbstractPayload.abstractPayload
        """
        return Component

    def getByBugstarId(self, bugstarId):
        """
        Get a project from shotgun based on the bugstar id

        Arguments:
            bugstarId (int): the bugstar id of the project

        Returns:
            rockstare.shotgun.entities.project.Project
        """
        entity = self.find([[const.Fields.BUGSTAR_ID, const.Operators.IS, bugstarId]])
        if entity:
            return entity[0]
