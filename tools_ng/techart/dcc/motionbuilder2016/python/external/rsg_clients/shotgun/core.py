"""Core functionality of the clients library to interface with Shotgun."""
import os
import json
import base64
import collections
from concurrent import futures

import shotgun_api3

from rsg_core import conversions
from rsg_core_py.metaclasses import singleton
from rsg_clients import const as clientsConst
from rsg_clients.decorators import connection as clientsConnection
from rsg_clients.contextManagers import futures as clientFutures
from rsg_clients.internals import baseConnection
from rsg_clients.shotgun.entities import baseEntity
from rsg_clients.shotgun.decorators import connection
from rsg_clients.shotgun import factory, const, exceptions, accounts



class Shotgun(baseConnection.BaseConnection):
    """Shotgun object through which to access and interact with Shotgun."""

    __metaclass__ = singleton.SingletonMemoize

    def __init__(self, server):
        """Connects to the rockstar shotgun server with the given script credentials.

        Args:
            server (str): URL to the shotgun server to query information from.
        """
        # TODO: Switch to the REST Api
        super(Shotgun, self).__init__(server=server)
        self._account = accounts.Core()  # Default Api
        self._shotgun = None
        self._shotgunApi = None
        self._type = const.Servers.getType(server)

        # cache all the builders at initialization
        for subclass in baseEntity.BaseEntity.__subclasses__():
            if subclass.ENTITY_TYPE is not None:
                self._getBuilder(subclass.ENTITY_TYPE)

    @property
    def instance(self):
        """The shotgun_api3 instance."""
        # TODO: Remove login from here
        if self._shotgunApi is None:
            self.login()
        return self._shotgunApi

    @property
    def client(self):
        """The name of this client."""
        return clientsConst.Clients.SHOTGUN

    @connection.resolve
    def authenticate(self, password, username=None):
        self.authenticateApi(password, username=username)
        self.authenticateUrl(password, username=username)

    def authenticateApi(self, password, username=None):
        """Authenticate with the current account.

        Args:
            password (str): Password to use.

            username (str, optional): Alternative username to use other than that of the account.
                            This argument is unsupported but required by the abstract class. Default is None.

        Returns:
            bool: Success of authentication.
        """
        server = const.Servers.getServerUrl(self._server, isAPI=True)
        if self.account.isApiKey:
            self._shotgunApi = shotgun_api3.Shotgun(
                server,
                script_name=self.account.name,
                api_key=base64.b64decode(password).decode(),
                ca_certs=const.Paths.SSL_CERTIFICATE,
                convert_datetimes_to_utc=False,
            )
        else:
            self._shotgunApi = shotgun_api3.Shotgun(
                server,
                login=self.account.name,
                password=base64.b64decode(password),
                ca_certs=const.Paths.SSL_CERTIFICATE,
                convert_datetimes_to_utc=False,
            )
        self._shotgunApi.connect()

    def authenticateUrl(self, password, username=None):
        """Authenticate with the current account.

        Args:
            password (str): Password to use.

            username (str, optional): Alternative username to use other than that of the account.
                            This argument is unsupported but required by the abstract class. Default is None.

        Returns:
            bool: Success of authentication.
        """
        server = const.Servers.getServerUrl(self._server)
        if self.account.isApiKey:
            self._shotgun = shotgun_api3.Shotgun(
                server,
                script_name=self.account.name,
                api_key=base64.b64decode(password),
                ca_certs=const.Paths.SSL_CERTIFICATE,
                convert_datetimes_to_utc=False,
            )
        else:
            self._shotgun = shotgun_api3.Shotgun(
                server,
                login=self.account.name,
                password=base64.b64decode(password),
                ca_certs=const.Paths.SSL_CERTIFICATE,
                convert_datetimes_to_utc=False,
            )
        self._shotgun.connect()

        # TODO: Replace this authentication code with the rest code
        # Rest Authentication
        self._authenticate(username, password)
        return True

    def _authenticate(self, username, password):
        """Authenticates against the rest server."""
        username = username or self.account.name
        if not self._authenticated:
            token = base64.b64encode("{}:{}".format(username, base64.b64decode(password)))
            self.session.headers["authorization"] = "Basic {}".format(token)
            data = {"grant_type": "client_credentials"}

        elif self._token:
            data = {"grant_type": "refresh_token",
                    "refresh_token": self._token}

        url = self.buildUrl([const.Endpoints.AUTH, const.Endpoints.ACCESS_TOKEN])
        headers = {"Content-type": clientsConst.ContentType.URL_ENCODED, "Accept": clientsConst.ContentType.JSON_UTF8}

        result = self.session.post(url, headers=headers, data=data)

        if result.status_code != 200:
            self._authenticated = False
            raise exceptions.NotAuthenticated(
                "Could not authenticate user {} to access the {} server\n"
                "Verify your credentials and try again.\n"
                "If the problem persists, contact techart and the {} team for "
                "further assistance.".format(username, self.server, self.client),
                url=url,
                errorCode=result.status_code,
            )
        accessToken = result.json()["access_token"]
        self._token = result.json()["refresh_token"]
        self.session.headers["authorization"] = "Bearer {}".format(accessToken)
        self._authenticated = True

    def logout(self):
        """Logs out the current Shotgun session."""
        if self.instance is not None:
            try:
                self.instance.close()
            except Exception:
                pass
        super(Shotgun, self).logout()

    def _parseFilters(self, filters, depth=0):
        """Coverts the values of the filter to values that can be sent to Shotgun.

        Args:
            filters (list): List of filters.

        Returns:
            list: Processed Shotgun-compatible filters.
        """
        if isinstance(filters, dict):
            filters = filters.get("filters", {}).get("conditions", [])

        for filter in filters:
            for index, value in enumerate(filter):
                if isinstance(value, dict):
                    self._parseFilters(value, depth + 1)
                elif isinstance(value, baseEntity.BaseEntity):
                    filter[index] = {"type": value.type, "id": value.id}
        return filters

    @connection.resolve
    def find(self, entityType, filters, fields=None, order=None, limit=None, page=None):
        """Finds records from shotgun.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            filters (list): List of lists of filters.

            fields (list, optional): List of fields to return in a query. Default is defined per-entity in that entity
                    class's DEFAULT_FIELDS property.
            order (list, optional): Order in which to return the data.
            limit (int, optional): The amount of results to return. Default is None which return everything.
            page (int, optional): The page of results to return. This flag is only respected if a value to limit is passed.

        Returns:
            list: Records found by the find operation.
        """
        fields = fields or ["id", "project"]
        filters = self._parseFilters(filters)
        return self.instance.find(
            entity_type=entityType, filters=filters, fields=fields, order=order, limit=limit or 0, page=page or 0
        )

    def search(self, endpoint, filters, fields=None, size=None, page=None, iterative=True):
        """Finds records from shotgun entities using rest.

        Args:
            endpoint (str): The endpoint for the type of entity being requested for the shotgun operation.
            filters (list): List of lists of filters.

            fields (list, optional): List of fields to return in a query. Default is defined per-entity in that entity
                    class's DEFAULT_FIELDS property.
            size (int, optional): The amount of results to return. Default is 100 and the max value is 500.
            page (int, optional): The page of results to return. This flag is only respected if a value to limit is passed.
            iterative (bool, optional): iterate through all the pages returned by a search. By default it is True.

        yields:
            list: Records found by the find operation.
        """
        size = size or 100
        if size > 500:
            size = 500

        # Build the payload for the search endpoint
        self._parseFilters(filters)
        payload = {"filters": filters,
                   "fields": fields or [const.Fields.ID, const.Fields.PROJECT],
                   "page": {"size": size, "number": page or 1}}

        contentType = const.ContentType.BASIC_FILTER
        if isinstance(filters, dict):
            contentType = const.ContentType.COMPLEX_FILTER

        # Get the total count of results from this search
        if not iterative:
            url = self.buildUrl([const.Endpoints.ENTITY, endpoint, const.Endpoints.SEARCH])
            results = self.post(url, payload=json.dumps(payload), contentType=contentType)
            yield results.get('data', [])

        else:
            summary = self.summarize(endpoint, filters)
            page = page or 1
            totalCount = summary["data"]["summaries"][const.Fields.ID]
            totalPageCount = totalCount // size + ((totalCount % size) > 0)

            with clientFutures.Futures(connection=self):
                # TODO: Reevaluate this implementation and make a more generic solution for this
                # We are going to leave one free worker/thread so we don't lock up the requests session from being
                # able to process non-concurrent requests
                futuresRequests = []
                maxCount = self.futures.executor._max_workers - 1
                maxRange = totalPageCount if totalPageCount < maxCount else maxCount
                # Bump the totalPageCount by one so the range actually hits that page number since it starts at 0
                for currentPage in range(page, maxRange+1):
                    # This is so the current page starts at 1
                    payload["page"]["number"] = currentPage
                    url = self.buildUrl([const.Endpoints.ENTITY, endpoint, const.Endpoints.SEARCH])
                    future = self.post(url, payload=json.dumps(payload), contentType=contentType, futures=True)
                    future.page = currentPage
                    futuresRequests.append(future)

                currentPage = page or 1
                completedPages = {}
                queues = [futuresRequests]
                for queue in queues:
                    nextQueue = []
                    for future in futures.as_completed(queue):
                        completedPages[future.page] = clientsConnection.resolve.response(future.result(), self, url)
                        while currentPage in completedPages:
                            yield completedPages[currentPage].get("data", [])

                            # Add requests for pages that still need to be processed
                            offsetPage = currentPage + len(futuresRequests)
                            if offsetPage <= totalPageCount:
                                payload["page"]["number"] = offsetPage
                                url = self.buildUrl([const.Endpoints.ENTITY, endpoint, const.Endpoints.SEARCH])
                                future = self.post(url, payload=json.dumps(payload), contentType=contentType, futures=True)
                                future.page = offsetPage
                                nextQueue.append(future)
                            currentPage += 1
                    if nextQueue:
                        queues.append(nextQueue)

    def summarize(self, entityEndpoint, filters, fields=None, size=None, page=None):
        """Summaries the results of a filter.

        Args:
            entityEndpoint (str): The endpoint for the type of entity being requested for the shotgun operation.
            filters (list): List of lists of filters.

            fields (dict, optional): key-value pairs of fields to summarize and how.
                                    Default is to count how many unique results there are (id field: count)
            size (int, optional): The amount of results to return. Default is None which return everything.
            page (int, optional): The page of results to return. This flag is only respected if a value to size is passed.

        Returns:
            list: Records found by the search endpoint.
        """
        self._parseFilters(filters)
        payload = {
            "filters": filters,
            "summary_fields": fields or [{"field": const.Fields.ID, "type": const.SummaryType.COUNT}],
        }

        if size:
            payload["page"] = {"size": size, "number": page or 1}

        contentType = const.ContentType.BASIC_FILTER
        if isinstance(filters, dict):
            contentType = const.ContentType.COMPLEX_FILTER
        url = self.buildUrl([const.Endpoints.ENTITY, entityEndpoint, const.Endpoints.SUMMARIZE])
        return self.post(url, payload=json.dumps(payload), contentType=contentType)

    @connection.resolve
    def create(self, entityType, data):
        """Creates an entity in Shotgun.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            data (dict): Data to add to shotgun.

        Returns:
            Iterator: Objects created as a result of this operation.
        """
        return self._shotgunApi.create(entityType, data)

    @connection.resolve
    def update(self, entityType, index, data):
        """Updates an entity in shotgun.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            index (int): ID of the entity.
            data (dict): Data to update in shotgun entity.

        Returns:
            dict
        """
        return self._shotgunApi.update(entityType, index, data)

    def commit(self, entityType, index, data):
        """Updates an entity in Shotgun.

        Reimplements inherited method.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            index (int): ID of the entity.
            data (dict): Data to update to Shotgun.

        Returns:
            list
        """
        return self.update(entityType, index, data)

    @connection.resolve
    def upload(self, entityType, index, path, field=None, name=None, tags=None):
        """Uploads a file as an attachment to this entity.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            index (int): ID of the entity.
            path (str): Path on disk to the file that should be uploaded.

            field (str, optional): Field at which to add file.
            name (str, optional): Short name for the file, it will default to the file name.
            tags (str, optional): Tags to include on the attachment.

        Returns:
            rsg_core.shotgun.entities.attachment.Attachment: The uploaded attachment, as its Shotgun object.
        """
        if tags:
            tags = ",".join(tags)
        attachmentId = self._shotgun.upload(entityType, index, path, field_name=field, display_name=name, tag_list=tags)
        return self.attachment(attachmentId)

    @connection.resolve
    def batch(self, create=None, update=None, delete=None):
        """Batch processes create, update and delete operations in a single call.

        If one fails, then all the commands fail.

        Args:
            create (list, optional): List of entities to create.
            update (list, optional): List of entities to update.
            delete (list, optional): List of entities to delete.

        Returns:
            list
        """
        batch = []
        entitiesDict = collections.OrderedDict()
        entitiesDict["create"] = {"entities": create or [], "func": lambda entity, data: entity._store(data)}
        entitiesDict["update"] = {"entities": update or [], "func": lambda entity, data: entity._refresh(data)}
        entitiesDict["delete"] = {
            "entities": delete or [],
            "func": lambda entity, data: entity._removeFromCache(force=True),
        }

        # Build the payloads as expected by the batch command
        for crudType, data in entitiesDict.iteritems():
            data["length"] = len(data["entities"])
            for entity in data["entities"]:
                payload = {"request_type": crudType, "entity_type": entity.ENTITY_TYPE}
                if crudType != "create":
                    payload["entity_id"] = entity.id

                if crudType == "create":
                    payload["data"] = entity.toDict()

                elif crudType == "update":
                    data = entity.toDict(onlyDirty=True)
                    data.pop(const.Fields.ID, None)  # The batch command doesn't like that the payload includes the id
                    if not data:
                        continue  # Skip updating payloads with no changes (this just causes errors)
                    payload["data"] = data

                batch.append(payload)

        results = self.instance.batch(batch)

        # Break up the results into their respective sections
        createLen = entitiesDict["create"]["length"]
        updateLen = createLen + entitiesDict["update"]["length"]
        deleteLen = updateLen + entitiesDict["delete"]["length"]

        entitiesDict["create"]["data"] = results[:createLen]
        entitiesDict["update"]["data"] = results[createLen:updateLen]
        entitiesDict["delete"]["data"] = results[updateLen:deleteLen]

        # Update the entities locally
        for content in entitiesDict.itervalues():
            for entity, data in zip(content["entities"], content["data"]):
                content["func"](entity, data)

    @connection.resolve
    def download(self, attachmentEntity, path, fileName):
        """Downloads the attachment to the file system.

        Args:
            attachmentEntity (rsg_clients.shotgun.entities.attachment.Attachment): Attachment entity.
            path (str): Absolute file path to folder where to save the file to.
            fileName (str): Name of the file to be saved on the file system.

        Returns:
            str: Location where the file has been saved.
        """
        downloadLocation = os.path.join(path, fileName)

        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except Exception as error:
                raise exceptions.FolderCreationError(path, message=str(error))

        # To avoid overwriting the already existing files, add the number at the end if the filename already exists
        counter = 1
        name, extension = os.path.splitext(fileName)
        while os.path.exists(downloadLocation):
            downloadLocation = os.path.join(path, "{}({}){}".format(name, counter, extension))
            counter += 1

        return self._shotgunApi.download_attachment(attachmentEntity.toDict(), file_path=downloadLocation)

    @connection.resolve
    def uploadThumbnail(self, entityType, index, path):
        """Uploads an image as a thumbnail to this entity.

        Args:
            entityType (str): the type of entity being requested for the shotgun operation.
            index (int): ID of the entity.
            path (str): Path on disk to the file that should be uploaded.
        """
        return self._shotgun.upload_thumbnail(entityType, index, path)

    @connection.resolve
    def uploadToVersion(self, versionID, filePath, overwrite=True):
        """Uploads a file to the sg_uploaded_movie of the version specified by the versionID.

        Args:
            versionID (int): The ID of the version to upload the movie to.
            filePath (str): The path of the file to upload.
            overwrite (bool): Whether to overwrite an existing file upload on this version.

        Returns:
            int: Result of the upload operation, or None if the upload fails.
        """
        # TODO: investigate a faster way of refreshing when this is called (bugstar://7130309)
        uploadField = const.Fields.UPLOADED_MOVIE

        # if overwrite is false, we should check to make sure we aren't overwriting an existing file
        if not overwrite:
            currentVersion = self._shotgun.find_one(const.Entities.VERSION, [[const.Fields.ID, const.Operators.IS, versionID]]
                                                 [const.Fields.UPLOADED_MOVIE])

            # this will be None if there isn't anything in the sg_uploaded_movie field
            if currentVersion[const.Fields.UPLOADED_MOVIE]:
                return None

        return self._shotgun.upload(const.Entities.VERSION, versionID, filePath, uploadField)

    @connection.resolve
    def delete(self, entityType, index):
        """Deletes an entity in Shotgun.

        Args:
            entityType (str): The type of entity being requested for the shotgun operation.
            index (int): ID of the entity.
        """
        return self._shotgunApi.delete(entityType, index)

    @connection.resolve
    def fields(self, project=None):
        """Gets all the fields for every active entity type and their schemas.

        The data returned is sorted by entity type and then the fields.

        Args:
            project (rsg_clients.shotgun.entities.project.Project, optional): Specifies a project to get fields from.

        Returns:
            dict: The requested fields.
        """
        return self.instance.schema_entity_read(project)

    @connection.resolve
    def createField(self, entityType, fieldType, name, properties=None):
        """Creates a field on shotgun on the given entity type.

        Args:
            entityType (str): The entity type to add the field to.
            fieldType (str): The type for the field.
            name (str): The display name of the field, the generated codename will add an 'sg' prefix and convert
                    any spaces & special characters to underscores.

            properties (dict, optional): Properties to set for the schema of the field.

        Returns:
            str
        """
        return self.instance.schema_field_create(entityType, fieldType, name, properties)

    @connection.resolve
    def readField(self, entityType, field=None, project=None):
        """Gets the schema of all the fields of a given entity.

        If a field name is passed in then only the schema for that field is returned.

        Args:
            entityType (str): The entity type the field field belongs to.

            field (str, optional): The codename of the the field.
            project (rsg_clients.shotgun.entity.project.Project, optional): Project to get the schema for.

        Returns:
            dict
        """
        return self.instance.schema_field_read(entityType, field, project)

    @connection.resolve
    def updateField(self, entityType, field, properties=None, project=None):
        """Updates the schema for the given field.

        Args:
            entityType (str): The entity type that the field belongs to.
            field (str): The codename of the the field.

            properties (dict, optional): Properties to update
            project (rsg_clients.shotgun.entity.project.Project, optional): Project to update schema in.
                    Changes are applied globally otherwise.

        Returns:
            dict
        """
        return self.instance.schema_field_update(entity_type=entityType, field_name=field, properties=properties)

    @connection.resolve
    def deleteField(self, entityType, field):
        """Moves the field to the trash in Shotgun.

        For it to be deleted completely from Shotgun it must be removed manually via the UI Interface.

        Args:
            entityType (str): The entity type that the field belongs to.
            field (str): The codename of the the field.

        Returns:
            bool: Success of the operation.
        """
        return self.instance.schema_field_delete(entityType, field)

    def buildLink(self, entityType, index):
        """Overwrites inherited method.

        Builds a url link to view data for that entity on the web.
        Notes:
            This url is different from the one used for calling rest calls
        Args:
            entityType (str): the type of entity to link to
            index (int): the id of the entity

        Returns:
            str
        """
        return "{}/detail/{}/{}".format(const.Servers.getServerUrl(self._server), entityType, index)

    # TODO: Remove url:bugstar:6079258
    def addNote(self, entity, text, author, title=None, create=True):
        """Attaches a new note to the given entity.

        Notes are how you add comments to most entities on Shotgun.

        Args:
            entity (rsg_core.shotgun.entities.baseEntity.BaseEntity): The entity to attach a note to.
            text (str): The contents of the note.
            author (rsg_core.shotgun.entities.humanUser.HumanUser): The user writing this note.

            title (str, optional): Title for the note, defaults to the text if None is supplied as a title.
            create (bool, optional): Create the note on shotgun. Default is True.

        Returns:
            rsg_core.shotgun.entity.note.Note
        """
        newNote = self.note()
        newNote.project = entity.project
        newNote.author = author
        newNote.subject = title
        newNote.body = text
        newNote.links = [entity]
        entity.notes.append(newNote)
        if create:
            newNote.create()
            entity.updateFields(fields=const.Fields.NOTES)
        return newNote

    def addReply(self, entity, text, author, create=True):
        """Attaches a new reply to the given entity.

        Replies are how you add comments to notes and tickets on Shotgun.

        Args:
            entity (rsg_core.shotgun.entities.baseEntity.BaseEntity): the entity to attach a reply to
            text (str): the contents of the note
            author (rsg_core.shotgun.entities.humanUser.HumanUser): the user writing this note

            create (bool, optional): create this entity. Default is True.

        Returns:
            rsg_core.shotgun.entity.reply.Reply
        """
        newReply = self.reply()
        newReply.author = author
        newReply.body = text
        newReply.link = entity

        entity.replies.append(newReply)

        if create:
            newReply.create()
            entity.updateFields(fields=const.Fields.REPLIES)
        return newReply

    # methods to create instances of specific entities
    def _getBuilder(self, entityType):
        """Gets the builder for the given entity.

        Args:
            entityType (str): the type of the entity

        Returns:
            rsg_core.shotgun.entities.BaseEntity.BaseBuilder
        """
        builder = self._builders.get(entityType, None)
        if not builder:
            cls = factory.resolveEntityClass(entityType)
            builderCls = cls.builder()
            builder = builderCls(self)
            self._builders[entityType] = builder
        return builder

    def clearCache(self):
        """Clears out the local cache of bugstar related objects that were spawned from this connection."""
        for subclass in baseEntity.BaseEntity.__subclasses__():
            subclass._clearCache()

    def cleanLeastAccessedCache(self, isDataStale=False, isCacheStale=True):
        """Cleans out the least accessed cache and the current contents of cached objects.

        Args:
            isDataStale (bool): only clear data that is stale.
            isCacheStale (bool): only removes objects from the cache only if the haven't been recently accessed.
        """
        for subclass in baseEntity.BaseEntity.__subclasses__():
            subclass._cleanDataCache(isDataStale=isDataStale)
            subclass._cleanLeastAccessedCache(isCacheStale=isCacheStale)

    def raiseAuthenticationError(self, url, response):
        """Raises an authentication error if the response returns an html page with said error.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server

        Raises:
            NotAuthenticated
        """
        if "<!doctype html>" in response.text and response.status_code == 401:
            self._authenticated = False
            raise exceptions.NotAuthenticated(
                "Basic Authentication Failure - Reason : AUTHENTICATED_FAILED", url=url, errorCode=response.status_code
            )

    def raiseException(self, url, response, data):
        """Raises an exception if the response returns a non-200 status code.

        Args:
            url (str): url used to invoke the rest query
            response (requests.Response): the response from the server
            data (dict): data extrapolated from the response as a dict

        Raises:
            ShotgunException
        """
        exceptionClass = exceptions.ShotgunException.getByErrorCode(response.status_code)
        if 300 <= response.status_code and not exceptionClass:
            exceptionClass = exceptions.ShotgunException

        if exceptionClass is not None:
            # Get the correct error message, based on the response it may require different keys
            errorDict = data.get("errors", [{}])[0]
            errorMessage = (errorDict.get("detail", None) or errorDict.get("title", None)).replace(". ", ".\n")

            if response.status_code == exceptions.ShotgunServerError.ERROR_CODE:  # This the generic 500 error code
                if exceptions.ServerUnavailable.REGEX.search(errorMessage):
                    raise exceptions.ServerUnavailable(server=self.type, message=errorMessage, url=url, connection=self)

            raise exceptionClass(errorMessage, url=url, errorCode=response.status_code)

    @property
    def restUrl(self):
        """Convenience method for getting the server url with the rest endpoint."""
        return const.Servers.getRestUrl(self._server)

    @property
    def actionMenuItem(self):
        """Action Menu Item Builder."""
        return self._getBuilder(const.Entities.ACTION_MENU_ITEM)

    @property
    def asset(self):
        """Asset Builder."""
        return self._getBuilder(const.Entities.ASSET)

    @property
    def attachment(self):
        """Attachment Builder."""
        return self._getBuilder(const.Entities.ATTACHMENT)

    @property
    def brand(self):
        """Brand Buildre."""
        return self._getBuilder(const.Entities.BRAND)

    @property
    def casting(self):
        """Casting Builder."""
        return self._getBuilder(const.Entities.CASTING)

    @property
    def component(self):
        """Component Builder."""
        return self._getBuilder(const.Entities.COMPONENT)

    @property
    def feature(self):
        """Feature Builder."""
        return self._getBuilder(const.Entities.CONCAT)

    @property
    def department(self):
        """Department Builder."""
        return self._getBuilder(const.Entities.DEPARTMENT)

    @property
    def deadline(self):
        """Department Builder."""
        return self._getBuilder(const.Entities.DEADLINE)

    @property
    def eventLogEntry(self):
        """Event Log Entry Builder."""
        return self._getBuilder(const.Entities.EVENT_LOG_ENTRY)

    @property
    def faceCaptureVideo(self):
        """Face Capture Video Builder."""
        return self._getBuilder(const.Entities.FACE_CAPTURE_VIDEO)

    @property
    def motionScene(self):
        """Motionscene Builder."""
        return self._getBuilder(const.Entities.FBX)

    @property
    def goal(self):
        """Goal Builder."""
        return self._getBuilder(const.Entities.GOAL)

    @property
    def group(self):
        """Group Builder."""
        return self._getBuilder(const.Entities.GROUP)

    @property
    def humanUser(self):
        """Human User Builder."""
        return self._getBuilder(const.Entities.HUMAN_USER)

    @property
    def legalClearance(self):
        """Legal clearance Builder."""
        return self._getBuilder(const.Entities.LEGAL_CLEARANCE)

    @property
    def location(self):
        """Location builder."""
        return self._getBuilder(const.Entities.LOCATION)

    @property
    def note(self):
        """Note Builder."""
        return self._getBuilder(const.Entities.NOTE)

    @property
    def packages(self):
        """Packages builder."""
        return self._getBuilder(const.Entities.PACKAGES)

    @property
    def project(self):
        """Project Builder."""
        return self._getBuilder(const.Entities.PROJECT)

    @property
    def photogrammetry(self):
        """Photogrammetry Builder."""
        return self._getBuilder(const.Entities.PHOTOGRAMMETRY)

    @property
    def reply(self):
        """Reply Builder."""
        return self._getBuilder(const.Entities.REPLY)

    @property
    def step(self):
        """Pipeline Step Builder."""
        return self._getBuilder(const.Entities.STEP)

    @property
    def script(self):
        """Dialogue Script Builder."""
        return self._getBuilder(const.Entities.SCRIPT)

    @property
    def strand(self):
        """Strand Builder."""
        return self._getBuilder(const.Entities.STRAND)

    @property
    def tag(self):
        """Tag Builder."""
        return self._getBuilder(const.Entities.TAG)

    @property
    def talent(self):
        """Talent Builder."""
        return self._getBuilder(const.Entities.TALENT)

    @property
    def task(self):
        """Task Builder."""
        return self._getBuilder(const.Entities.TASK)

    @property
    def ticket(self):
        """Ticket Builder."""
        return self._getBuilder(const.Entities.TICKET)

    @property
    def unknown(self):
        """Unknown Builder."""
        return self._getBuilder("Unknown")

    @property
    def version(self):
        """Version Builder."""
        return self._getBuilder(const.Entities.VERSION)
