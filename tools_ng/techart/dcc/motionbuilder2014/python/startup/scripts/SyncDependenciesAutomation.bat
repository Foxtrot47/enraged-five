@ECHO OFF
::
:: File:: /tools_ng/techart/dcc/motionbuilder[VERSION]/python/startup/scripts/SyncDependenciesAutomation.rb
:: Description:: Perfroce sync file
::
:: Author:: Mark Harrison-Ball <Mark.Harrison-Ball@rockstargames.com>
:: Date:: 20 April 29th 2014
::

CALL setenv.bat > NUL

:: Temp FIx to remove hardcoded python paths if swithcing back to RDR
if %RS_PROJECT% == gta5 (
echo Removing Python RDR Paths
SETX PYTHONPATH ""
SETX PYTHONHOME ""
)

::Used to determine which MB verison we are using
FOR %%V IN ("%~dp0../../../") DO SET filename=%%~dpV
for %%f in (%filename:~0,-1%) do set MB_VERSION=%%~nxf
echo %MB_VERSION%

echo on
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\techart\dcc\%MB_VERSION%\python\startup\scripts\SyncDependenciesAutomation.rb

::pause