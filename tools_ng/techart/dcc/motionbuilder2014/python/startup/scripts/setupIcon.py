import inspect
import sys
import pyfbsdk as mobu
import win32api
import win32con
import os

import RS.Config as Config

from System.Drawing import Icon
from System.Diagnostics import Process

system = mobu.FBSystem()
cycles = 0
runningCustomBuild = 'motionbuilder_custom.exe' in sys.executable
currentFileName = inspect.getfile(inspect.currentframe())
workingInWildwest = currentFileName.lower().startswith('x:/wildwest')
icon = Icon( os.path.join( Config.Script.Path.ToolImages, 'motionbuilder.ico') )
process = Process.GetCurrentProcess()            

def SetIcons(control, event):
    global cycles
    global workingInWildwest
    global runningCustomBuild
    global process
    global icon
    
    if cycles < 50:
        if runningCustomBuild:
            win32api.SendMessage( process.MainWindowHandle.ToInt32(), win32con.WM_SETICON, win32con.ICON_BIG, icon.Handle.ToInt32() );
    
    #Need to wait a while to push through the smaller icon. Not sure why.
    if cycles > 50:
        system.OnUIIdle.Remove(SetIcons) 

        if runningCustomBuild:
            win32api.SendMessage( process.MainWindowHandle.ToInt32(), win32con.WM_SETICON, win32con.ICON_BIG, icon.Handle.ToInt32() );
            win32api.SendMessage( process.MainWindowHandle.ToInt32(), win32con.WM_SETICON, win32con.ICON_SMALL, icon.Handle.ToInt32() );

        #This is temporary code for TA's to be reminded to run the deployed version.
        elif workingInWildwest:
            import pyfbsdk_additions as mobu_additions
            
            tool = mobu_additions.FBCreateUniqueTool("Annoying box")
            x = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachLeft,"")
            y = mobu.FBAddRegionParam(0,mobu.FBAttachType.kFBAttachTop,"")
            w = mobu.FBAddRegionParam(500,mobu.FBAttachType.kFBAttachNone,"")
            h = mobu.FBAddRegionParam(317,mobu.FBAttachType.kFBAttachNone,"")
            tool.AddRegion("main","main", x, y, w, h)
            
            img = mobu.FBImageContainer()
            img.Filename = r'X:\wildwest\dcc\motionbuilder2014\images\deploymentWarning.tif'
            tool.SetControl("main",img )
            
            tool.StartSizeX = 500
            tool.StartSizeY = 317
    
            mobu.ShowTool(tool)            
    
    cycles += 1
        
system.OnUIIdle.Add( SetIcons )

