"""
This module contains logic that should be ran at startup that is dependent on the Motion Builder UI
"""

import os
import sys
import imp
import inspect
import logging
import subprocess
import time
from functools import partial

import pyfbsdk as mobu

from PySide import QtGui, QtCore

LOG = logging.getLogger(__name__)


def Execute():
    """
    Creates a dummy widget that is hidden in Motion Builder,so that once Motion Builder is shown, it triggers the
    show event of this widget to customize the Mobu UI.
    """
    from PySide import QtGui, QtCore
    from RS.Tools.UI import Application

    mainwindow = Application.GetMainWindow()

    class PostStartupWidget(QtGui.QWidget):
        """
        Dummy Widget for triggering call to the _Execute method when Motion Builder is first shown
        """
        def __init__(self, parent=None):
            """ Constructor """
            super(PostStartupWidget, self).__init__(parent=parent)
            self.setMaximumSize(0, 0)
            self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
            self.runPostMotionBuilderSetup = True

        def event(self, event):
            """
            Internal Method.
            It has been overriden so that it triggers the _Execute method the first time MotionBuilder, and by
            extension this widget, is shown.

            Arguments:
                event (QtGui.QEvent): event being invoked by the widget

            Return:
                Boolean
            """
            super(PostStartupWidget, self).event(event)
            if event.type() is QtCore.QEvent.WindowActivate:
                if self.runPostMotionBuilderSetup:
                    self.runPostMotionBuilderSetup = False
                    _Execute(mainwindow)
            return True

        def _hide(self):
            """
            Sends the widget to the back of all the Motion Builder widgets to avoid blocking any other UI
            elements.
            """
            # We are stacking the child beneath All the default widgets to make sure it doesn't block access
            # to the native UI. Before it was blocking the top menu bar.

            for child in mainwindow.children():
                if child is not self:
                    try:
                        self.stackUnder(child)
                    except:
                        pass

    mainwindow.layout().addWidget(PostStartupWidget())


def _Execute(mainwindow):
    """
    Executes all the methods that should be ran at Startup after the UI has been shown
    Arguments:
        mainwindow (QtGui.QWindow): the main window of Motion Builder

    """
    LOG.info("Applying UI Settings.")

    # Set window colors
    from RS.Utils import Scene
    Scene.SetWindowTitle()

    # Register error handling, this is to be moved to the Standalone startup script later
    _registerExceptionHandler()
    # Check that the proper build version is running
    _checkBuildVersion()
    # Setup UI relevant
    _setupQtPlugins()
    # Add custom rockstar menu
    _buildRockstarMenu()
    # Add shelftastic on startup
    _startShelf()
    # Setup the shortcuts
    _startShortcuts()
    # Set the Viewer to be detachable and dockable
    _makeViewerDetachable(mainwindow=mainwindow)
    # Add patch for error with the multiCameraViewer
    _multiCameraViewerPatch(mainwindow=mainwindow)
    # run code that is suppose to run after mobu has completely opened
    _postMotionBuilderOpen()
    _logStartup()
    # Adds Callbacks
    _addCallbacks()


def _addCallbacks():
    """ Adds Callbacks """
    LOG.info("Adding UI Callbacks")
    callbackPath = os.path.join(os.path.dirname(__file__), "Callbacks.py")
    if not os.path.exists(callbackPath):
        from RS import Perforce
        Perforce.Sync(callbackPath, force=True)
    module = imp.load_source("Callbacks", callbackPath)
    module.Add()


def _setupQtPlugins():
    """ Sets up our QT extensions and plugins, and other UI relevant plugins."""
    LOG.info("Setting up UI extensions and plugins")

    # Get the current filename.
    currentFilename = inspect.getfile(inspect.currentframe())
    currentDirectory = os.path.dirname(currentFilename)

    # for the pythoninjectioninteractionmode.pyd - hopefully we can lose this soon.
    sys.path.append(os.path.dirname(currentDirectory))

    QtPluginsPackageDir = os.path.abspath('{0}/../../../python/external/qt/plugins'.format(currentDirectory))

    if not os.path.isdir(QtPluginsPackageDir):
            raise Exception('You are missing the following directoy: {0}.'.format(QtPluginsPackageDir))

    else:
        from PySide import QtGui, QtCore

        if QtPluginsPackageDir not in QtCore.QCoreApplication.libraryPaths():
            QtGui.QApplication.addLibraryPath(QtPluginsPackageDir)

        # Here the LAVSplitter.ax extension for ffdshow is setup
        # This is required so that mpeg videos play in Motion Builder

        # 0 is returned if LAVSplitter is registered
        # 1 is returned if LAVSplitter isn't registered

        if subprocess.call("REG QUERY HKCR\CLSID /f LAVSplitter.ax /s", shell=True):
            batfile = os.path.abspath(
                '{0}/../../python/external/qt/plugins/phonon_backend/dependencies/install_splitter.bat'.format(
                os.path.dirname(currentFilename)))

            subprocess.call(batfile, shell=True)


def _startShelf():
    """ Show our tools shelf while starting up """
    from RS.Utils import UserPreferences
    openShelf = UserPreferences.__Readini__("shelftastic", "OpenShelfOnStartup", notFoundValue=None)

    if openShelf is None:
        openShelf = True
        UserPreferences.__Saveini__("shelftastic", "OpenShelfOnStartup", True)

    if openShelf is True:
        LOG.info("Loading shelf ")
        from RS.Tools.Shelftastic import Shelftastic
        Shelftastic.Run()


def _startShortcuts():
    """
    Start our Shortcut System and load in the previous shortcuts
    """
    from RS.Core.ShortcutKeys import core
    try:
        core.ShortcutKeysManager().loadKeys()

    except:
        import pyfbsdk as mobu
        import mbutils
        appConfig = mobu.FBConfigFile("@Application.txt")
        shortcutPath = os.path.join(mbutils.GetConfigPath(),"Keyboard", "{0}.txt".format(appConfig.Get("Keyboard", "Default")))
        shortcutPath = os.path.abspath(shortcutPath)
        QtGui.QMessageBox.warning(None, "Rockstar Error", "Can't read the following file:\n"
                                                          "{}.\n"
                                                          "Please revert changes to this file and attempt to "
                                                          "open Motion Builder again.".format(shortcutPath))
    from RS.Utils import UserPreferences
    switcherShortcut = UserPreferences.__Readini__("shortcuts", "Enable Switcher Shortcuts", notFoundValue=False)
    if switcherShortcut:
        from RS.Core.Camera import CamUtils
        CamUtils.ToggleSwitcherShortcuts(True)


# TODO: Move to standalone directory after creating an error watcher
def _registerExceptionHandler():
    """
    Register a custom exception handler that will email/log unhandled
    exceptions to the Tech Art team.
    """
    LOG.info("Registering custom exception handler.")

    from RS.Utils import Exception as RSExcep
    from RS.Utils import UserPreferences

    sys.excepthook = RSExcep.UnhandledExceptionHandler
    exceptionState = UserPreferences.__Readini__("Exception", "Send Exceptions", notFoundValue=None)
    if exceptionState is not None:
        exceptionDate = UserPreferences.__Readini__("Exception", "Set Date", notFoundValue=None)
        if exceptionDate is not None:
            currentDate = time.strftime("%d/%m/%Y")
            if currentDate == exceptionDate:
                RSExcep.SEND_EXCEPTION = exceptionState
            else:
                UserPreferences.__RemoveSection__("Exception")


def _checkBuildVersion():
    """Determines if the user is using the correct build of MotionBuilder (including HotFixes)"""
    LOG.info("Checking build version.")

    import RS.Config

    # For custom cuts the name might not parse in this logic so wrapping in a try catch.
    try:

        # Now only check to see if the build version is lower so people testing higher versions don't get spammed
        systemBuildId = str(mobu.FBSystem().BuildId)
        systemBuildIdSplit = systemBuildId.split('#')
        systemBuildIdSplit2 = systemBuildIdSplit[-1].split(')')
        systemChangeList = systemBuildIdSplit2[0]

        targetBuildId = str(RS.Config.Script.TargetBuildVersion)
        targetBuildIdSplit = targetBuildId.split('#')
        targetBuildIdSplit2 = targetBuildIdSplit[-1].split(')')
        targetChangeList = targetBuildIdSplit2[0]

        if int(systemChangeList) < int(targetChangeList):
            mobu.FBMessageBox('Rockstar',
                              ("MotionBuilder version is incorrect."
                               " Please contact your IT department and have the latest Hotfixes/Patches"
                               " installed as soon as possible. Should be running: {0}").format(
                                  RS.Config.Script.TargetBuildTitle), 'OK')

    # Pretty soon we will hopefully have moved to the deployment system so won't need to check build version anymore
    except Exception, e:
        pass


def _buildRockstarMenu():
    """ Adds the Rockstar Menu to the main motion builder menu """
    LOG.info("Adding Rockstar Menu")

    import RS.Utils.MenuBuilder
    RS.Utils.MenuBuilder.Build()
    # Enable for Stager
    import RS.Core.Reference.New.BuildMenu
    RS.Core.Reference.New.BuildMenu.Run()

    # Cutscene Dynamic Menu Builder
    import RS.Core.Menus
    RS.Core.Menus.Cutscene.Run()


def _makeViewerDetachable(mainwindow):
    """
    Makes the viewer detachable

    Arguments:
        mainwindow (QtGui.QWindow): the main motion builder window

    """
    LOG.info("Making viewer detachable")
    def topLevelChanged(mainwindow, viewer, floating):
        """ Set the viewer as the central widget when dropped into the main window """
        if not floating:
            mainwindow.setCentralWidget(viewer)

    viewers = filter(lambda child: "Viewer" in child.windowTitle(),
                     [child for child in mainwindow.children() if hasattr(child, "windowTitle")])

    if viewers:
        viewers[0].setAllowedAreas(QtCore.Qt.AllDockWidgetAreas)
        viewers[0].setFeatures(QtGui.QDockWidget.DockWidgetFloatable | QtGui.QDockWidget.DockWidgetMovable)
        viewers[0].topLevelChanged.connect(partial(topLevelChanged, mainwindow, viewers[0]))

    else:
        LOG.error("!!! Could not find the Viewport to make it dockable")


def _multiCameraViewerPatch(mainwindow):
    """
    Adds a command to the menu option for the multiCameraViewer so that whenever it is pressed it makes
    that window hideable

    Arguments:
        mainwindow (QtGui.QWindow): the main motion builder window

    """
    LOG.info("Adding patch to the MultiCameraViewer")

    from RS.Tools.UI import Application

    # Add an additional call when launching the multi camera viewer so it turns off constraints on cameras
    def MultiCameraViewerPatch():
        viewer = Application.GetChildrenByWindowTitle(Application.GetMainWindow(), "Multi Camera Viewer")
        if viewer:
            viewer[0].setWindowFlags(QtCore.Qt.Window)
            viewer[0].show()

    menuDictionary = Application.GetMainMenuActions(window=mainwindow)
    # Check to make sure that we got the menu dictionary with some values
    if menuDictionary:
        multiCameraViewer = menuDictionary
        # Attempt to get the QAction of the correct menu item if it is available
        for eachMenuName in ["Open Reality", "Tools", "Multi Camera Viewer", "__QAction"]:
            multiCameraViewer = multiCameraViewer.get(eachMenuName, {})
        if multiCameraViewer:
            multiCameraViewer.triggered.connect(MultiCameraViewerPatch)


def _logStartup():
    """ Log to the database that Motion Builder opened successfully """
    LOG.info("Logging Motion Builder Start")
    import RS.Config
    from RS.Utils.Logging import Database

    # We force logging to true because P4 syncing is not working as expected
    # RS.Config.User.IsExternal doesn't seem to return False for Technicolor
    # switching to check if there is an R in the studio name.

    Database.SetLoggingEnabled(RS.Config.User.IsInternal )

    filename = os.path.abspath(inspect.getfile(inspect.currentframe()))

    Database.Log("motionbuilder {0}".format(RS.Config.Script.TargetBuild), Database.Context.Opened,
                 applicationId=Database.Application.MotionBuilder, filename=filename)


def _postMotionBuilderOpen():
    """
    Sends a command to a standalone python interpretter to send back to Mobu once it has finished loading completely
    """
    # Call the script a second into the future to update the File -> Open menu option.
    # The Open menu option isn't available until Motion Builder has fully loaded.
    # and at this point during the execution process the top menu is still missing the Open option

    from RS.Core.Server import Client
    from RS.Core.Camera import CamValidationTools
    import RS.Utils.UserPreferences as userPref

    try:
        mobuCommands = [("import logging;"
                         "LOG = logging.getLogger(\"{0}\");"
                         "LOG.info(\"Running Post-Startup Customization\");"
                         "from RS.Tools.UI.OpenFile import Configure;"
                         "Configure.Setup()").format(__name__)]

        audioSetting = userPref.__Readini__("audioTools", "audioDest", notFoundValue=0)
        if audioSetting:
            mobuCommands.append("from RS.Core.Audio import AudioTools;"
                                "AudioTools.AutoLoadAudio()")

        camUser = CamValidationTools.CheckUserEmail()
        if camUser:
            mobuCommands.append("from RS.Core.Camera import CameraLocker;"
                                "camLock = CameraLocker.LockManager();"
                                "camLock.setAllCamLocks(False)")
        # Add News Feed to post mobu show
        #mobuCommands.append("import RS.Utils.NewsFeed as rsNewsFeed;"
        #                    "rsNewsFeed.Run()")

        mobuCommandString = ";".join(mobuCommands)
        command = ("import time\n"
                   "time.sleep(1)\n"
                   "ExecuteInMotionBuilder('{}')\n".format(mobuCommandString)
                   )
        server = Client.Connection()
        server.Thread(command)
        LOG.info("Adding Post Motion Builder commands")

    except:
        LOG.error("Could not setup the Rockstar Open UI")
