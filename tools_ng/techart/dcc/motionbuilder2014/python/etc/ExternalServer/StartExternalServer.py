"""
Script that is used by the StartExternalServer.bat file to avoid locking files that start up the server.
"""
import re
import os
import sys
import subprocess


def get_environment_variable(environment_var):
    """
    gets the value associated with the environment variable passed

    :param environment_var: string
        the environment variable that you want to query
    :return: string
    """

    # Get the python tools executable
    os_environments = subprocess.check_output('setenv', shell=True)
    regular_expression = r"(?<={environment_var}:)[a-z0-9._:\t\b\\ ]+".format(environment_var=environment_var)
    search_results = re.search(regular_expression, os_environments, re.M|re.I)

    if search_results:
        tools_executable = search_results.group().strip()
    else:
        raise "No system environment matching {environment_var} is setup".format(environment_var=environment_var)

    return tools_executable

#Get the tech art tools root
techart_path = "x:\\wildwest"
if "wildwest" not in __file__:
    techart_path = os.path.join(get_environment_variable("RS_TOOLSROOT"), "techart")

serverScriptPath = os.path.join(techart_path, "\\dcc\\motionbuilder2014\\python\\RS\\Core")
print serverScriptPath
sys.path.append(serverScriptPath)

import Server

server = Server.Connection(10002)
if not server.IsConnected():
    server.Run()
sys.path.remove(serverScriptPath)