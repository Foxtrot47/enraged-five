'''
Usage:
    This module should only contain functionality related to working with color structures (mainly FBColor)
Author:
    Ross George
'''

'''
Returns the hex code for the passed FBColor
'''
def GetHexCode( Color ):
    hexList = list()
    for i in range(3):
        componentValue = round( Color[i] * 255.0, 4 )
        if componentValue % 0.5 != 0:
            componentValue = round( componentValue )

        componentHexValue = hex( int( componentValue ) ).split('x')[1]
        if len( componentHexValue ) == 1: 
            componentHexValue = "0{0}".format( componentHexValue )
        
        hexList.append( componentHexValue )  
    return "#{0}{1}{2}".format( hexList[0], hexList[1], hexList[2] ) 