__author__ = 'mharrison-ball'


'''
Utility to convert an an intermediate in asset

Full Paramater List:

 --input                 Specify input filename.
 --output                Specify output filename.
 --recursive             Recurse directory search (for directories only).
 --wildcard              Recursive directory search file wildcard (required: recursive option).
 --selected              Selected data build; quicker iteration that doesn't build all directory content.
 --rebuild               Rebuild data; ignoring timestamp information.
 --preview               Preview data; output resources to preview folder.
 --proc                  Define processor to use; defaults to RAGE platform conversion.
 --filelist              Filename with a list of files we want to convert.
 --platforms             Override installer set platforms (keys separated by commas or semicolons, e.g. "ps3;ps4"); defaults to installer set platforms.
 --no-sync               Disable engine Perforce dependency sync.
 --no-xge                Disable Xoreax XGE parallel processing engine.
 --no-content            Disable Content-Tree loading (required for most builds).
 --no-content-cache      Disable Content-Tree cache loading.
 --no-default-resolver   Disable Default resource-only resolver (content-tree process required).
 --use-npass-engine      Use new *experimental* N-pass Engine system.
 --debug                 Start debugger session; dialog prompts for debugger to use.
 --nopopups              Hide all popup dialogs; taking default options.
 --help                  Display help information and exit.
 --project               Project to use (e.g. gta5, rdr3, gta5_liberty.
 --branch                Project branch to use (e.g. dev, release).
 --dlc                   Project DLC to use (e.g. dlc_w_ar_heavyrifle).
 --verbose               Use debug logging.

'''

import os, subprocess
import RS

PIPELINE_CONVERT = os.path.join(RS.Config.Tool.Path.Root, "ironlib", "lib", "RSG.Pipeline.Convert.exe")

def convert_asset(asset_path,
                  selected = False,
                  rebuild = False,
                  nopopups = True,
                  preview= False):

    args = PIPELINE_CONVERT
    if selected: args += " --selected"
    if rebuild:  args += " --rebuild"
    if nopopups: args += " --nopopups"
    if nopopups: args += " --preview"
    args += " {0}".format(asset_path)

    return_code = subprocess.call( args, shell=False)
    return return_code