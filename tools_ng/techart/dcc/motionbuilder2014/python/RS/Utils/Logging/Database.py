"""
Description:
    Module for logging tool usage
"""
__DoNotReload__ = True  # Module is NOT safe to be dynamically reloaded


import os
import re
import sys
import inspect
import datetime
import traceback

import xml.etree.cElementTree as xml
from functools import wraps

# Catch statement to make sure modules load on the local server
# RS.Core imports pyfbsdk which can't be imported into external python executables

try:
    from RS.Core import DatabaseConnection
    from RS.Core.Server import Client


except ImportError:
    import Client
    import DatabaseConnection


# # Constants # #

PACKAGE_PATH = os.path.split(__file__)[0]


DB_DRIVER       = '{SQL Server}'
DB_SERVER       = 'RSGSANSQL1\PRODUCTION'
DB_DATABASE     = 'RockstarTools'
DB_TABLENAME    = 'TechArtToolsLogging'

# NY TEST
# DB_SERVER       = 'NYCW-MHB-SQL'
# DB_DATABASE     = 'MotionbuilderToolsStats'

DB_CONN         = None
DB_CURSOR       = None

# Whether or not logging is enabled.l
# Replace with a pointer

# # Enums # #


def Enabled(func):
    """
    Runs the decorated function if logging is enabled
    Argument:
        func = function; function to decorate

    Return:
        function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if IsLoggingEnabled():
            server = Client.Connection("Logging", force=False)
            # Super ugly code as I do not remember the specific error code that happens
            try:
                server.Run()
            except:
                print traceback.format_exc()
                try:
                    server.Run()
                except:
                    pass

            if not server.IsConnected():
                SetLoggingEnabled(False)
                return

            return func(*args, **kwargs)
    return wrapper


class Application(object):
    """
    Application Id's
    """
    Script          = 1
    MotionBuilder   = 2
    Max             = 3
    Pedstar         = 4

    Default         = Script

ApplicationDisplayName = { Application.Script           : 'Script',
                           Application.Max              : '3dsmax',
                           Application.MotionBuilder    : 'MotionBuilder',
                           Application.Pedstar          : 'Pedstar' }


class Type(object):
    """
    The type of record.
    """
    Message         = 1
    Warning         = 2
    Error           = 3
    EventHandler    = 4

    Default         = Message


TypeDisplayName = {Type.Message        : 'Message',
                   Type.Warning        : 'Warning',
                   Type.Error          : 'Error',
                   Type.EventHandler   : 'Event Handler'}


class Context(object):
    """
    Basic context types.  These are not required to use, but are
    useful for common context usage.
    """
    Opened = 'Opened'
    Closed = 'Closed'


class Column(object):
    """
    Constants for the names of the database columns.  The columns are also
    listed in this order within the database table.
    """

    Project             = 'Project'
    Application         = 'Application'
    Name                = 'Name'
    Context             = 'Context'
    Filename            = 'Filename'
    Type                = 'Type'
    Timestamp           = 'Timestamp'
    TotalTime           = 'TotalTime'
    Description         = 'Description'
    Username            = 'Username'
    PerforceRevision    = 'PerforceRevision'
    SceneFilename       = 'SceneFilename'
    Exception           = 'Exception'
    Traceback           = 'Traceback'
    Attachments         = 'Attachments'


# # Classes # #

class Record(object):
    """
    Represents a single record of a logged event.  Attributes are added when the record is built.
    """
    def __init__(self):
        pass


# # Functions # #


def ConnectToDatabase():
    """
    Connect to the TechArt tools logging table in the global tools database.

    Return:
        boolean
    """
    global DB_CONN
    global DB_CURSOR

    DB_CONN = DatabaseConnection.DatabaseConnection(
        driver=DB_DRIVER, server=DB_SERVER, database=DB_DATABASE)

    if DB_CONN.is_connected:
        DB_CURSOR = DB_CONN.cursor()

        return True

    return False


def DeleteAllRecords():
    """ deletes all records on the table on the database"""
    statement = 'DELETE FROM {0}'.format(DB_TABLENAME)
    DB_CONN.sql_query(statement)
    DB_CONN.commit()


def GetColumns():
    """
    Gets columns

    Return:
        List
    """
    # Order matters, don't change it!
    columns = [Column.Project,
               Column.Application,
               Column.Name,
               Column.Context,
               Column.Filename,
               Column.Type,
               Column.Timestamp,
               Column.TotalTime,
               Column.Description,
               Column.Username,
               Column.PerforceRevision,
               Column.SceneFilename,
               Column.Exception,
               Column.Traceback]

    return columns


def GetAllRecords():
    """
    Gets all the records from the database

    Return:
        List
    """
    records = []

    statement = 'SELECT * FROM {0}'.format(DB_TABLENAME)
    rows = DB_CONN.sql_query(statement).fetchall()

    for row in rows:
        record = CreateRecordFromRow(row)

        if record:
            records.append(record)

    return records


def CreateRecordFromRow(row):
    """
    Generates a record with all the information from a row from a table on the database

    Arguments:
        row (database instance): data to store in the record object
    """
    record = Record()
    record.Project = int(row.Project)
    record.Application = int(row.Application)
    record.Name = row.Name
    record.Context = row.Context
    record.Filename = row.Filename
    record.Type = int(row.Type)
    record.Timestamp = row.Timestamp
    record.TotalTime = row.TotalTime
    record.Description = row.Description
    record.Username = row.Username
    record.PerforceRevision = int(row.PerforceRevision)
    record.SceneFilename = row.SceneFilename
    record.Exception = row.Exception
    record.Traceback = row.Traceback

    return record


# ----------- Scene Name Retrieval Methods ----------


def GetSceneNameByApplicationID(applicationID=0):
    """
    Returns the name of the scene based on the application id provided

    Arguments:
        application_id (int): id that corresponds to the application that is running the logger

    Return:
        method
    """
    try:
        # dynamically generate method to get name based on the application id
        getNameMethod = getattr(sys.modules[__name__],
                                "Get{0}SceneName".format(ApplicationDisplayName[applicationID].capitalize()))
        return getNameMethod()

    except ImportError:
        print "Incorrect "
        return ""


def GetStudio():
    """
    Returns Studio User is part of
    Return:
        string
    """
    import RS.Config as config
    return config.User.Studio


def GetMotionbuilderSceneName():
    """
    Returns the name of the current motion builder scene
    Return:
        string
    """
    import pyfbsdk
    return pyfbsdk.FBApplication().FBXFileName


def GetMotionbuilderVersion():
    """
    Returns the version of  motion builder
    Return:
        string
    """
    import pyfbsdk
    return pyfbsdk.FBSystem().BuildId


def GetMotionbuilderBuild():
    """
    Returns the name of the current Motion Builder Build
    Return:
        string
    """
    import pyfbsdk
    return pyfbsdk.FBSystem().Version


def Get3dsmaxSceneName():
    """
    Returns the name of the current 3DS Max scene
    Return:
        string
    """

    import MaxPlus
    return MaxPlus.FileManager().GetFileName()


def GetPedstarSceneName():
    # Do Later
    return ''

def getSystemInfo():
    '''

    :return:
    '''
    import RS.Utils.SystemInfo as sysInfo
    return sysInfo


def IsLoggingEnabled():
    """
    Is logging enabled
    
    Return:
        boolean
    """

    # Using XML as that seems to be what we are all using
    xml_path = os.path.join(PACKAGE_PATH, "Enabled.xml")

    xml_tree = xml.parse(xml_path)
    xml_element = xml_tree.getroot()
    enabled = xml_element.text
    return "True" in enabled


def SetLoggingEnabled(enabled=True):
    """
    Sets if logging is enabled or not

    Arguments:
        enabled (boolean): if logging is enabled or not
    """
    xml_path = os.path.join(PACKAGE_PATH, "Enabled.xml")
    xml_tree = xml.parse(xml_path)
    xml_element = xml_tree.getroot()
    xml_element.text = str(enabled)
    xml_tree.write(xml_path)


@Enabled
def Log(name,
        context,
        recordType=Type.Default,
        applicationId=Application.Default,
        filename="",
        description="",
        timeStamp=None,
        totalTime=None,
        sceneFilename="",
        perforceRevision=-1,
        exception="",
        traceback=""
        ):
    """
    Sends the information on provided to be logged into the Tools database

    Arguments:
        name (string): 
            name of the tool that is being logged
        context (string):
            how the tool is being used, ei. Opened or Closed
        recordType (int):
            type of record
        applicationId: int
            id associated with the application running the tool
        filename (string):
            name of the file
        description: string
            description of something
        timeStamp (string):
            a time stamp generated by the time module
        totalTime (string):
            a time stamp generated by the time module
        sceneFilename (string):
            filename of the scene currently opened where the tool was used
        perforceRevision (int):
            the number of the perforce revision
        exception (string):
            type of exception encountered, if any
        traceback (string):
            error traceback results
    """
    currentProject = 0

    if os.environ['RS_PROJECT'].upper() == 'RDR3':
        currentProject = 2
    elif os.environ['RS_PROJECT'].upper() == 'GTA5':
        currentProject = 1

    # Get file name
    if not filename:
        filename = inspect.getfile(inspect.currentframe().f_back)
    filename = os.path.abspath(filename)
    filename = re.sub('"|\'', '', filename)

    # This fixes escape characters in the string
    filename = re.sub(r"(?<!\\)\\(?=[abfnrtv])", r"\\\\", filename)

    # Bit of a hack to determine if we are running in wildwest. Can't use the development mode unfortunately
    _Branch = filename.split('\\')[1]
    if _Branch.upper() == "WILDWEST":
        currentProject = 3

    # Get scene file name
    if not sceneFilename:
        sceneFilename = get_scene_name(applicationId)

    if sceneFilename:
        sceneFilename = os.path.abspath(sceneFilename)

    # Get time
    if not timeStamp:
        timeStamp = datetime.datetime.now()

    timeStamp = timeStamp.strftime('%Y-%m-%d %H:%M:%S')

    # Get application Version
    mobuVersion = GetMotionbuilderVersion()

    # Get the MB build, i.e 2014or 2015
    mobuBuild = GetMotionbuilderBuild()
    # convert scene name to safePath and lowercase
    sceneFilename = sceneFilename.replace("\\", "/").lower()
    # Get our System Info
    sysInfo = getSystemInfo()
    

    # Call Stored Procedure
    statement = ("exec [ROCKSTAR\mharrison-ball].[TechArtLogging_LogTool]"
                 " {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', {11}, '{12}', '{13}', {14}, '{15}', '{16}', {17}").format(
                                                                        currentProject,
                                                                        name,
                                                                        sceneFilename.replace("\\", "/"),
                                                                        timeStamp,
                                                                        str(os.environ['USERNAME']).lower(),
                                                                        filename,
                                                                        context,
                                                                        exception,
                                                                        str(os.environ['COMPUTERNAME']).lower(),
                                                                        mobuVersion,
                                                                        GetStudio(),
                                                                        mobuBuild,
                                                                        '',
                                                                        sysInfo.get.cpu,
                                                                        sysInfo.get.totalRam,
                                                                        sysInfo.get.gpu,
                                                                        sysInfo.get.os,
                                                                        sysInfo.get.hdTotal

    )


    command = ("import Database\n"
               "Database.ConnectToDatabase()\n"
               "logging.debug('--- Threading ---')\n"
               "Database.ThreadLog(\"{sql_query}\")\n")

    command = command.format(sql_query=statement)
    server = Client.Connection("Logging")
    # Add modules we need to use that aren't on the server
    server.Sync(sys.modules[__name__], DatabaseConnection)
    # thread the command on the server* FR
    result = server.Thread(command, log='Logging {tool} into the database'.format(tool=filename))

    return result


def LogSceneTimeOpen():
    """
    Work out time we have had scene opened for and log results

    Return:
        string
    """
    global gTimeOpened,  gLastSceneOpened

    # incase the file is opened with the refernce system we check it has been declared
    if 'gLastSceneOpened' in globals() and 'gTimeOpened' in globals():
        if gLastSceneOpened != "":
            elapsedTime =  datetime.datetime.now() - gTimeOpened
            d = divmod(elapsedTime.total_seconds(), 86400)
            h = divmod(d[1], 3600)
            m = divmod(h[1], 60)
            s = m[1]

            elapsedTime = '%d:%d:%d' % (h[0],m[0],s)

            # reset time
            gTimeOpened = datetime.datetime.now()

            # Call Stored Procedure
            statement = ("exec [ROCKSTAR\mharrison-ball].[TechArtLogging_LogSceneTime]"
                         " '{0}', '{1}', '{2}'".format(
                                                    str(os.environ['USERNAME']).lower(),
                                                    elapsedTime,
                                                    gLastSceneOpened.replace("\\","/")
                                                    )
                         )

            command = (
                    "import Database\n"
                    "Database.ConnectToDatabase()\n"
                    "logging.debug('--- Threading ---')\n"
                    "Database.ThreadLog(\"{sql_query}\")\n"

                    )

            command = command.format(sql_query=statement)

            server = Client.Connection("Logging")
            # Add modules we need to use that aren't on the server
            server.Sync(sys.modules[__name__], DatabaseConnection)
            # thread the command on the server
            return server.Thread(command, log='Logging {time} into the database'.format(time=elapsedTime))


def LogSceneTimeLoaded(sceneName):
    """
    Log how long it took to load the scene
    
    Arguments: 
        sceneName (string): name of the fbx file
    """
    
    global gTimeLoad

    if 'gTimeLoad' in globals() and sceneName != "":
        elapsedTime = datetime.datetime.now() - gTimeLoad
        d = divmod(elapsedTime.total_seconds(), 86400)
        h = divmod(d[1], 3600)
        m = divmod(h[1], 60)
        s = m[1]

        elapsedTime = '%d:%d:%d' % (h[0],m[0],s)

        # Only log cutscenes for now as they will be the longest files to load
        if "!!scenes" in sceneName.lower():
            # Call Stored Procedure
            statement = ("exec [ROCKSTAR\mharrison-ball].[TechArtLogging_LogLoadTime]"
                         " '{0}', '{1}', '{2}'".format(
                                                    str(os.environ['USERNAME']).lower(),
                                                    elapsedTime,
                                                    sceneName.replace("\\","/")
                                                    )
                         )

            command = (
                    "import Database\n"
                    "Database.ConnectToDatabase()\n"
                    "logging.debug('--- Threading ---')\n"
                    "Database.ThreadLog(\"{sql_query}\")\n"

                    )

            command = command.format(sql_query=statement)

            server = Client.Connection("Logging")
            # Add modules we need to use that aren't on the server
            server.Sync(sys.modules[__name__], DatabaseConnection)
            # thread the command on the server
            return server.Thread(command, log='Logging {time} into the database'.format(time=elapsedTime))

        print "Scene $s load in %s", elapsedTime, sceneName


def initSceneTimeLogging(sceneLastName):
    """
    Init our scene logging and make sure we log on files that have been opened,
    """
    global gTimeOpened,  gLastSceneOpened
    gLastSceneOpened = sceneLastName
    if gLastSceneOpened == "":
        gTimeOpened = datetime.datetime.now()


def initSceneTimeLoad():
    """
    Init our Timer for loading Scenes
    """
    global gTimeLoad
    gTimeLoad = datetime.datetime.now()
    print "Timer Reset"


def ThreadLog(sql_query):
    """
    Sends the data to the server to be logged, this portion of the code is meant to be sent to a separate thread
    to be executed.

    Arguments:
        sql_query (string): sql statement/command to execute

    Return:
        string
    """
    # Debug Return - this code no longer works and was blocking save cameras logic from running on the server BB 4/26/17
    return
    
    DB_CONN.ExecuteQuery(sql_query)
    DB_CONN.Commit()



# ---- Old Method Names for backwards compability ----

def get_scene_name(application_id=0):
    return GetSceneNameByApplicationID(application_id)
