"""
Utilities for working with files.
"""

import os
import fnmatch
import string
from RS import Config 


PATH_MAPPINGS = {
    "$Root": Config.Script.Path.Root,
    "$TechArt": Config.Script.Path.ToolImages
    }


def GetImportPathFromFilename(filename):
    """
    Get the Python import path from the supplied filename.  The supplied filename
    must live under our Rockstar Python tools package directory.  For example, supplying
    the following path:
    
        x:\\wildwest\\dcc\\motionbuilder2014\\python\\RS\\Utils\\Path.py
        
    Would return the following:
    
        RS.Utils.Path
    
    Arguments:
    
        filename (string): The filename to extract import path.
        
    Returns:
    
        The Python import path for the filename.
    """
    if 'motionbuilder{0}\\python\\RS\\'.format(Config.Script.TargetBuild) not in filename:
        assert 0, 'Please supply a path to a Python module under the Rockstar Python package directory!'
        
    rawImport = filename[filename.index('RS\\'):][:-3]
    return rawImport.replace('\\', '.')      


def GetBaseNameNoExtension(filename):
    """
    Get just the name part of the filename, with no file extension. 
    
        x:/wildwest/dcc/motionbuilder2014/python/menu.config
        
    Would be returned as:
        
        menu
        
    Arguments:
    
        filename(string): The filename to extract the base name without the extension
        
    Returns:
    
        Just the name part of the filename.
        
    Example:
    
        basename = GetBaseNameNoExtension(r'x:/wildwest/dcc/motionbuilder2014/python/menu.config')
        # basename returns menu
        print basename 
        
    """
    return os.path.splitext(os.path.basename(filename))[0]


def GetFileExtension(filename, remove_period=False):
    """
    Get the file extension of a file.

    Arguments:

        filename (string): The filename to extract the extension from
        remove_period (boolean): remove the period from the extension
        
    Returns:

        the extension of the filename.
        
    Example:
    
        extension = GetBaseNameNoExtension(r'x:/wildwest/dcc/motionbuilder2014/python/menu.config')
        # extension returns .config
        print basename 
    """
    return os.path.splitext(filename)[-1][remove_period:]


def GetFileFolder(filename):
    """
    Get just the folder where the current fbx filename is located. For Example:
    
    Arguments:
        filename (string): The filename to extract the directory from

    Returns:
        
        just the folder
        
    Example: 
        directory = GetBaseNameNoExtension(r'c:/myfolder/subfolder/myfile.fbx')
        # directory returns  c:/myfolder/subfolder/
        print directory 
        
    """

    return os.path.dirname(filename)


# TODO: Simplify logic
def Walk(pRoot, pRecurse=False, pPattern='*', pReturn_folders=0):
    """
    Walks though the directory
    
    Arguments: 
        pRoot (string): directory to walk through
        pRecurse (boolean): Walk through the child directories of the passed directory
        pPatter (string): pattern to filter file/directories by 
        pReturn_folders (boolean): return folders instead of files
        
    """
    lResult = []
    
    try:
        lNames = os.listdir(pRoot)
        
    except os.error:
        return lResult
    
    lPattern = pPattern or '*'
    lPat_list = string.splitfields(lPattern, ';')
    
    for iName in lNames:
        lFullname = os.path.normpath(os.path.join(pRoot, iName))
        
        for iPat in lPat_list:
            if fnmatch.fnmatch(iName, iPat):
                if os.path.isfile(lFullname) or (pReturn_folders and os.path.isdir(lFullname)):
                    lResult.append(lFullname)
                    
                continue
            
        if pRecurse:
            if os.path.isdir(lFullname) and not os.path.islink(lFullname):
                lResult = lResult + Walk(lFullname, pRecurse, pPattern, pReturn_folders) 
                
    return lResult


def ResolvePath(filePath):
    """
    Method

    Resolves a file path string with a config path in it.

    Example:
        "$Root/derp/derp2/derp.dep" => C:\\FolderRoot\\Folder\\derp\derp2\derp.dep"

    Args:
        filePath (string): The input file path

    Return:
        string: The final output string with config tags resolved
        
    Example:
        ResolvePath("$Root/derp/derp2/derp.dep")
    """
    for key, path in PATH_MAPPINGS.iteritems():
        filePath = filePath.replace(key, path)

    return os.path.abspath(filePath)