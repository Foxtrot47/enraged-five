"""
Usage:
    This module should only contain functionality related to working and preforming operations on FBControlSets (control rigs).    
Author:
    Ross George
"""

import pyfbsdk as mobu
from RS.Utils.Scene import Model

IKEffectorIdList = [mobu.FBEffectorId.kFBHipsEffectorId,
                    mobu.FBEffectorId.kFBLeftAnkleEffectorId,
                    mobu.FBEffectorId.kFBRightAnkleEffectorId,
                    mobu.FBEffectorId.kFBLeftWristEffectorId,
                    mobu.FBEffectorId.kFBRightWristEffectorId,
                    mobu.FBEffectorId.kFBLeftKneeEffectorId,
                    mobu.FBEffectorId.kFBRightKneeEffectorId,
                    mobu.FBEffectorId.kFBLeftElbowEffectorId,
                    mobu.FBEffectorId.kFBRightElbowEffectorId,
                    mobu.FBEffectorId.kFBChestOriginEffectorId,
                    mobu.FBEffectorId.kFBChestEndEffectorId,
                    mobu.FBEffectorId.kFBLeftFootEffectorId,
                    mobu.FBEffectorId.kFBRightFootEffectorId,
                    mobu.FBEffectorId.kFBLeftShoulderEffectorId,
                    mobu.FBEffectorId.kFBRightShoulderEffectorId,
                    mobu.FBEffectorId.kFBHeadEffectorId,
                    mobu.FBEffectorId.kFBLeftHipEffectorId,
                    mobu.FBEffectorId.kFBRightHipEffectorId,
                    mobu.FBEffectorId.kFBLeftHandEffectorId,
                    mobu.FBEffectorId.kFBRightHandEffectorId,
                    mobu.FBEffectorId.kFBLeftHandThumbEffectorId,
                    mobu.FBEffectorId.kFBLeftHandIndexEffectorId,
                    mobu.FBEffectorId.kFBLeftHandMiddleEffectorId,
                    mobu.FBEffectorId.kFBLeftHandRingEffectorId,
                    mobu.FBEffectorId.kFBLeftHandPinkyEffectorId,
                    mobu.FBEffectorId.kFBLeftHandExtraFingerEffectorId,
                    mobu.FBEffectorId.kFBRightHandThumbEffectorId,
                    mobu.FBEffectorId.kFBRightHandIndexEffectorId,
                    mobu.FBEffectorId.kFBRightHandMiddleEffectorId,
                    mobu.FBEffectorId.kFBRightHandRingEffectorId,
                    mobu.FBEffectorId.kFBRightHandPinkyEffectorId,
                    mobu.FBEffectorId.kFBRightHandExtraFingerEffectorId,
                    mobu.FBEffectorId.kFBLeftFootThumbEffectorId,
                    mobu.FBEffectorId.kFBLeftFootIndexEffectorId,
                    mobu.FBEffectorId.kFBLeftFootMiddleEffectorId,
                    mobu.FBEffectorId.kFBLeftFootRingEffectorId,
                    mobu.FBEffectorId.kFBLeftFootPinkyEffectorId,
                    mobu.FBEffectorId.kFBLeftFootExtraFingerEffectorId,
                    mobu.FBEffectorId.kFBRightFootThumbEffectorId,
                    mobu.FBEffectorId.kFBRightFootIndexEffectorId,
                    mobu.FBEffectorId.kFBRightFootMiddleEffectorId,
                    mobu.FBEffectorId.kFBRightFootRingEffectorId,
                    mobu.FBEffectorId.kFBRightFootPinkyEffectorId,
                    mobu.FBEffectorId.kFBRightFootExtraFingerEffectorId,
                    mobu.FBEffectorId.kFBLastEffectorId]
'''
Returns a list of all IKEffectorModels on the control rig
'''
def GetIKEffectorModels(TargetControlSet):
    returnModelList = list()

    for effectorIdIndex in mobu.FBEffectorId.values:
        effectorId = mobu.FBEffectorId.values[effectorIdIndex]
        if effectorId != mobu.FBEffectorId.kFBInvalidEffectorId:
            IKEffectorModel = TargetControlSet.GetIKEffectorModel(effectorId, 0)
            if IKEffectorModel != None:
                returnModelList.append(IKEffectorModel)

    return returnModelList

'''
Returns a list of all fkModels on the control rig
'''
def GetfkModels(TargetControlSet):
    returnModelList = list()

    for bodyNodeIdIndex in mobu.FBBodyNodeId.values:
        bodyNodeId = mobu.FBBodyNodeId.values[bodyNodeIdIndex]
        print bodyNodeId
        if bodyNodeId != mobu.FBBodyNodeId.kFBInvalidNodeId:
            fkModel = TargetControlSet.GetFKModel(bodyNodeId)
            if fkModel != None:
                returnModelList.append(fkModel)

    return returnModelList

def GetAllEffectors(TargetControlSet):
    '''
    Returns a list with all the effectors that belong to a control set.
    The two functions above seem to be crashing mobu, therefore this is a safer(ish) way of getting
    the effector.

    Needs to be improved in the future
    '''
    targetControlSetList = TargetControlSet.GetReferenceModel()
    if targetControlSetList is not None:
        return Model.GetChildren([targetControlSetList], True, True)
