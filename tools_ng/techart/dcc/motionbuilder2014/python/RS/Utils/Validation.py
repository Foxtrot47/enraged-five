"""
Description:
    Series of checks to make sure the file submitted is not broken. For now just checking if the cameras have been nuked
    Probably best to move this into a new py and check against it

Author:
    Mark Harrison-Ball <mark.harrison-ball@rockstargames.com>
"""


import os
import datetime
import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

import pyfbsdk as mobu

from RS import Config
from RS import Globals
import RS.Perforce
import RS.Utils.Exception

SUBJECT = 'MotionBuilder Scene Validation Error'
GSHOWONCE = 0
ENABLE_VALIDATION = True


class SCENETYPE:
    """ Used to determine the type of scene we have open """
    scene_cutscene = "!!scenes"
    scene_ingame = "ingame"
    scene_resources = "resources"
    scene_default = None


def logValidationError():
    """ Sends an email with an error report based the details of the user """
    # Attempt to email the exception.
    if ENABLE_VALIDATION and Config.User.ExchangeServer and Config.User.Email not in RS.Utils.Exception.IgnoreUserList:
        toStr = ''
        
        for emailAddress in RS.Utils.Exception.EmailList:
            toStr = "".join((toStr, emailAddress, ''))
    
        # Format the exception for email.
        message = MIMEMultipart('alternative')
        message['Subject'] = '{0}: {1}'.format(SUBJECT, mobu.FBApplication().FBXFileName)
        message['To'] = toStr
        message['From'] = Config.User.Email   

        # Perforce Revision
        try:
            fileInfo = RS.Perforce.GetFileState(mobu.FBApplication().FBXFileName)
            fileInfoString = 'REVISION: {0}\n\r'.format(fileInfo.HaveRevision, fileInfo.HeadRevision)

        except:
            fileInfoString = 'REVISION: unknown\n'

        body = ''.join((
            '\nUSER: {0}\n\r'.format(Config.User.Name),
            '\nFILE: {0}\n\r'.format(mobu.FBApplication().FBXFileName),
            '\nTIME: {0}\n\r'.format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            fileInfoString))

        htmlBody = MIMEText(body, 'html')
        message.attach(htmlBody)
        
        log_errors = False
        # Email the exception.
        try:
            server = smtplib.SMTP(Config.User.ExchangeServer)
            server.sendmail(Config.User.Email, RS.Utils.Exception.EmailList, message.as_string())
            server.quit()
            
        except:
            log_errors = True            
            
        # Backup email exchange 
        if log_errors:   
            try:                
                server = smtplib.SMTP("rsgldnexg1.rockstar.t2.corp")
                server.sendmail(Config.User.Email, RS.Utils.Exception.EmailList, message.as_string())
                server.quit()              
            except:
                print 'Could not email the exception!'

    return


def rs_resetValidation(control, event):
    """
    resets the Validation

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event; unused
    """
    gShowOnce = 0
 
    
def exceptionChecks():
    """ Checks that the user is not from a Previz machine """
    isValid = True
    # Ignore the Previz machine users
    if Config.User.Name in ['gcpreviz', 'jmaye', 'thoran', 'mags.donaldson', 'kelly.wetzel']: 
        isValid = False     

    return isValid
    

def rs_validate(control, event):
    """
     Run Validation Checks

    Arguments:
        control: Motion Builder object that is calling this method
        event (pyfbsdk.FBEvent)= Motion Builder event; unused
    """
    if ENABLE_VALIDATION and rs_CheckCameras():
        logValidationError()


def rs_returnScenetype():
    """ Return the type of file that we are working on. Ie. Cutscene/ in game / soemthing else """
    tokens = mobu.FBApplication().FBXFileName.split('\\')
    for name in tokens:
        if name.lower() == scenetype.scene_cutscene:  
            return scenetype.scene_cutscene
        elif name.lower() == scenetype.scene_ingame:  
            return scenetype.scene_ingame
        elif name.lower() == scenetype.scene_resources:  
            return scenetype.scene_resources   
    
    return scenetype.scene_default


# For use with Cutscenes Only
def rs_CheckCameras():
    """ Checks that there are cameras attached to the camera switcher and that the camera switcher is keyed """
    global gShowOnce
    status = False    

    # First check the path of the filename and ignore empty scenes
    if mobu.FBApplication().FBXFileName != '' and rs_returnScenetype() == scenetype.scene_cutscene:
        # LOVE IT, if not on base layer if will report cameras are nuked!!! STUPID MOTIONBUILDER
        mobu.FBSystem().CurrentTake.SetCurrentLayer(0) 
        
        # Check we have cameras in the scene
        if len(Globals.gCameras) > 7:  # 7 covers the basic MB cameas (producer + switcher)
            lCameraSwitcher = None
            
            try:
                lCameraSwitcher = mobu.FBCameraSwitcher()
            except:
                status = True
                if gShowOnce == 0:
                    if 'RS_AUTOMATION' not in os.environ: # Check if we are running in Automation Mode
                        result = mobu.FBMessageBoxWithCheck("VALIDATION WARNING!",
                                                            "The camera switcher has been deleted!!\n\n"
                                                            "Please verify before saving the file",
                                                            "Continue", None, None, "Don't Show again", False, 0, 1)
                        gShowOnce = int(result[1])  
                    return status
                
                
                
            lSwitcherKeys = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys  
            
            if len(lSwitcherKeys) == 0:
                status = True
                # Check if we are running in Automation Mode
                if gShowOnce == 0 and 'RS_AUTOMATION' not in os.environ and exceptionChecks():
                    result = mobu.FBMessageBoxWithCheck("VALIDATION WARNING!",
                                                        "Looks like the camera keys in the switcher "
                                                        "might have been deleted!!\n\n"
                                                        "Please verify before saving the file", "Continue",
                                                        None, None, "Don't Show again", False, 0, 1)
                    gShowOnce = int(result[1])

    return status
            
    
gShowOnce = 0
scenetype = SCENETYPE()

gApplication = mobu.FBApplication()
gApplication.OnFileSave.Add(rs_validate)
gApplication.OnFileOpenCompleted.Add(rs_resetValidation)