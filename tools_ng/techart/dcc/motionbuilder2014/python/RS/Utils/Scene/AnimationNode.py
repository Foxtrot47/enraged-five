'''
Usage:
    This module should only contain functionality related to working with FBAnimationNodes.    
Author:
    Ross George
'''

from pyfbsdk import *

import hashlib

import RS.Core
import RS.Utils.Scene.Time

def GetTimeRange( pInputAnimationNode, pInputRecursive = False ):
    '''
    Goes through the passed animation node and optionally any sub animation nodes getting the frame range extents
    Only gets information from the active layer.
    '''    
    #Get our return time range
    lReturnTimeRange = RS.Utils.Scene.Time.Range()
    
    #Check if this animation node has any keys then get their time
    if pInputAnimationNode.FCurve != None:
        if len( pInputAnimationNode.FCurve.Keys ) > 0 :
            lReturnTimeRange.Start = pInputAnimationNode.FCurve.Keys[0].Time
            lReturnTimeRange.End = pInputAnimationNode.FCurve.Keys[-1].Time
    
    #If this operation is recursive we'll check all the sub animation nodes as well
    if pInputRecursive:
        for iSubAnimationNode in pInputAnimationNode.Nodes:
            lSubAnimationNodeTimeRange = GetTimeRange( iSubAnimationNode, True )
            
            #Merge the sub animation time range in
            lReturnTimeRange.Merge( lSubAnimationNodeTimeRange )
    
    #Return our time range
    return lReturnTimeRange

def OffsetKeys( pInputAnimationNode, pInputTimeDelta, pInputRecursive = False ):
    '''
    Goes through the passed animation nodes children recurses if need be offsetting their keys by the passed delta
    Only offsets the keys on the active layer and current take. Can probably use the FBFilters to achieve same result but quicker
    '''    
    
    #Check if this animation node has any keys to offset
    if pInputAnimationNode.FCurve != None:
        if len( pInputAnimationNode.FCurve.Keys ) > 0 :

            #If the delta we want to offset by is positive we'll work in reverse order
            if RS.Utils.Scene.Time.IsPositive ( pInputTimeDelta ):
                for iKey in reversed( pInputAnimationNode.FCurve.Keys ):
                    lTime = FBTime( iKey.Time.Get() + pInputTimeDelta.Get() )
                    iKey.Time = lTime
            
            #If the delta we want to offset by is negative we'll work in normal order
            if RS.Utils.Scene.Time.IsNegative ( pInputTimeDelta ):
                for iKey in pInputAnimationNode.FCurve.Keys:
                    lTime = FBTime( iKey.Time.Get() + pInputTimeDelta.Get() )
                    iKey.Time = lTime 

    #If this operation is recursive we'll check all the sub animation nodes as well
    if pInputRecursive:
        for iSubAnimationNode in pInputAnimationNode.Nodes:
            OffsetKeys( iSubAnimationNode, pInputTimeDelta, True )
                
def DeleteKeys( pInputAnimationNode, pInputRecursive = False ):
    '''
    Deletes all keys on an animation node.
    '''        
    #Check if this animation node has any keys to offset
    if pInputAnimationNode.FCurve != None:
        if len( pInputAnimationNode.FCurve.Keys ) > 0 :
            lKeyStartIndex = 0
            lKeyEndIndex = len( pInputAnimationNode.FCurve.Keys ) - 1
            pInputAnimationNode.FCurve.KeyDeleteByIndexRange( lKeyStartIndex, lKeyEndIndex )
    
    #If this operation is recursive we'll delete the keys on the sub animation nodes as well
    if pInputRecursive:
        for iSubAnimationNode in pInputAnimationNode.Nodes:
            DeleteKeys( iSubAnimationNode, True )

def CopyData( SourceAnimationNode, TargetAnimationNode, Recursive = True):
    '''
    Copies animation data from source animation node to target animation. Will also copy sub animation nodes (X, Y, Z etc...)
    '''    
    if SourceAnimationNode.FCurve != None:
        TargetAnimationNode.FCurve = SourceAnimationNode.FCurve
        #Check for no keys. An FCurve can have no keys but have a value associated with it. We must transfer 
        #this value as well. We have to actually key it though because there doesn't seem to be any other way.
        if len( TargetAnimationNode.FCurve.Keys ) == 0:
            TargetAnimationNode.FCurve.KeyAdd( FBTime(), SourceAnimationNode.FCurve.Evaluate(FBTime()) )
    if Recursive:
        #We could use 'AnimationNode.FindByLabel' here but MoBu is a cunt and that function doesn't work sometimes
        for sourceSubNode in SourceAnimationNode.Nodes:
            for targetSubNode in TargetAnimationNode.Nodes:
                if sourceSubNode.Name.lower() == targetSubNode.Name.lower():
                    CopyData( sourceSubNode, targetSubNode, True )
                    break


def GetSubAnimationNodeList( SourceAnimationNode, Recursive = True):
    '''
    Returns the child nodes of the passed AnimationNode. Works recursively by default
    '''    
    subAnimationNodeList = []
    #The only nodes that have FCurves on the top node, are the ones that don't have the property .Nodes
    if SourceAnimationNode:    
        if SourceAnimationNode.FCurve :
            subAnimationNodeList.append( SourceAnimationNode )
        else:
            for subNodeIndex in range( len ( SourceAnimationNode.Nodes ) ):
                subAnimationNode = SourceAnimationNode.Nodes[subNodeIndex]
                subAnimationNodeList.append( subAnimationNode )
                if Recursive:
                    subAnimationNodeList.extend( GetSubAnimationNodeList( subAnimationNode ) )
    return subAnimationNodeList

def HasKeys( SourceAnimationNode, Recursive = True):
    '''
    Returns True if an animation node has keys present. By default will recurse to sub animation nodes as well
    '''    
    if SourceAnimationNode.KeyCount > 0:
        return True;
    if Recursive:
        for subAnimationNode in GetSubAnimationNodeList(SourceAnimationNode):
            if subAnimationNode.KeyCount > 0:
                return True
    return False

def HaveKeys( animationNode, keyCount ):
    '''
    !!!Obsolete!!! - use HasKeys instead.
    '''    
    if len(animationNode.Nodes) == 0:
        return animationNode.KeyCount > keyCount
    else:
        for node in animationNode.Nodes:
            if HaveKeys(node, keyCount):
                return True
            del(node)


def HashKeys( TargetAnimationNode, UseKeyValue = True, UseKeyTime = True, AnimationHash= hashlib.md5() ):
    '''            
    Updates a hash with the information from the given node.
    Can choose which parts of the key to include in the hash.
    This allows us to quickly compare if keys have changed
    '''   
    if AnimationHash:
        #If the node contains animation
        if TargetAnimationNode.FCurve:
            for key in TargetAnimationNode.FCurve.Keys:
                if UseKeyValue:             
                    AnimationHash.update(str(key.Value))
                if UseKeyTime:
                    AnimationHash.update(str(key.Time))