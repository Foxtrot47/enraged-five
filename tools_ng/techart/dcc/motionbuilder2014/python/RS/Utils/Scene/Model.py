'''
Usage:
    This module should only contain functionality for working with FBModel objects
Author:
    Ross George
'''

import pyfbsdk as mobu

import collections
import json
import math
import os
import posixpath
import tempfile
import time


import RS.Globals
import RS.Core
import RS.Utils.Scene.Time
import RS.Utils.Scene.Take
import RS.Utils.Scene.Layer
import RS.Utils.Scene.AnimationNode


def LockTransform(inputModel):
    """
        Lock an object local Position/Rotation/scale 
        in order to prevent animators to offset rig elements.

        Args:  
            inputModel (FBModel):  working inputModel.
    """
    inputModel.Translation.SetLocked( True )
    inputModel.Rotation.SetLocked( True )
    inputModel.Scaling.SetLocked( True )

    #Lock children as well
    translateHandle = inputModel.PropertyList.Find('Translation (Lcl)') 
    rotateHandle = inputModel.PropertyList.Find('Rotation (Lcl)') 
    scaleHandle = inputModel.PropertyList.Find('Scaling (Lcl)') 

    for channelIndex in xrange(3):
        translateHandle.SetMemberLocked(channelIndex, True)
        rotateHandle.SetMemberLocked(channelIndex, True)
        scaleHandle.SetMemberLocked(channelIndex, True)


def ResetTransform(inputModel,
                   zeroPosition=True,
                   zeroRotation=True,
                   zeroScale=True):
    """
        Reset an object local Position/Rotation/scale values

        Args:  
            inputModel (FBModel):  working inputModel.
            zeroPosition (bool).
            zeroRotation (bool).
            zeroScale (bool).
    """
    if not isinstance(inputModel, mobu.FBModel):
        raise ValueError("Please check if {0} is an FBModel".format(inputModel))

    #check inputModel type
    if zeroPosition == True:
        inputModel.PropertyList.Find('Translation (Lcl)').Data = mobu.FBVector3d()

    if zeroRotation == True:
        inputModel.PropertyList.Find('Rotation (Lcl)').Data = mobu.FBVector3d()

    if zeroScale == True:
        inputModel.PropertyList.Find('Scaling (Lcl)').Data = mobu.FBVector3d(1.0,1.0,1.0)


def BakeWorldGeometrySoup(inputModel,
                          targetSpace=mobu.FBMatrix()):   
    """
        Extract an independant polySoup from inputModel.
        Args:  
            inputModel (FBModel):  working inputModel.
            targetSpace(FBMatrix): matrix used for conversion of space

        returns  (FBModel)
    """
    targetSpaceInverse = mobu.FBMatrix()
    targetSpaceInverse.CopyFrom(targetSpace)    
    targetSpaceInverse.Inverse()

    boundingVolume = None

    sourceModelWorldMatrix = mobu.FBMatrix()
    inputModel.GetMatrix ( sourceModelWorldMatrix ) 

    geometrixOffsetMatrix = mobu.FBMatrix()
    geometrixOffsetPosition = inputModel.GeometricTranslation
    geometrixOffsetScale    = inputModel.GeometricScaling

    geometrixOffsetPosition = mobu.FBVector4d(geometrixOffsetPosition[0],
                                              geometrixOffsetPosition[1],
                                              geometrixOffsetPosition[2],
                                              1.0)

    geometrixOffsetScale  = mobu.FBSVector(geometrixOffsetScale[0],
                                           geometrixOffsetScale[1],
                                           geometrixOffsetScale[2])

    mobu.FBTRSToMatrix(geometrixOffsetMatrix,
                       geometrixOffsetPosition,
                       inputModel.GeometricRotation, 
                       geometrixOffsetScale)

    sourceModelWorldMatrix = targetSpaceInverse*sourceModelWorldMatrix*geometrixOffsetMatrix

    vertexArray = inputModel.Geometry.GetPositionsArray()
    vertexArrayInWS = inputModel.Geometry.GetPositionsArray()

    tmpPosition = mobu.FBVector4d()
    vertexPointInWS = mobu.FBVector4d()

    for j in range(0,len(vertexArray),3):
        tmpPosition[0] = vertexArray[j] 
        tmpPosition[1] = vertexArray[j+1] 
        tmpPosition[2] = vertexArray[j+2] 

        mobu.FBVectorMatrixMult(vertexPointInWS, sourceModelWorldMatrix, tmpPosition) 

        vertexArrayInWS[j]   = vertexPointInWS[0] 
        vertexArrayInWS[j+1] = vertexPointInWS[1] 
        vertexArrayInWS[j+2] = vertexPointInWS[2] 

    #Arbitrary name for the converted geometry mesh
    volumeMesh = mobu.FBMesh("boundindVolumeMesh1")
    triangleIds = range(len(vertexArrayInWS)/3)

    boundingVolume = mobu.FBCreateObject("Browsing/Templates/Elements/Primitives", "Cube", "Cube")

    volumeMesh.GeometryBegin()
    volumeMesh.VertexArrayInit(len(triangleIds), False)
    volumeMesh.SetPositionsArray(vertexArrayInWS)
    volumeMesh.TriangleListAdd(len(triangleIds), triangleIds)    
    volumeMesh.GeometryEnd()

    boundingVolume.Geometry = volumeMesh
    boundingVolume.Show = True
    boundingVolume.Visible = True
    
    #Arbitrary name for the model storing the geometry
    boundingVolume.Name = 'BBox1'

    boundingVolume.SetMatrix(targetSpace)    

    mobu.FBSystem().Scene.Evaluate ()

    return boundingVolume


def ProcessModelForBoundingBox(inputModel,
                               axisAligned=True):
    """
        Returns worldSpace boundingbox of an FBModel
        Args:  
            inputModel (FBModel): object used to extract boundingbox
            axisAligned(bool): align bounding box to object or in worldSpace

        returns  (FBModel)
    """
    sourceModelWorldMatrix = mobu.FBMatrix()
    if axisAligned == True:
        inputModel.GetMatrix(sourceModelWorldMatrix) 
    
    return BakeWorldGeometrySoup(inputModel,
                                 targetSpace=sourceModelWorldMatrix)


def BuildBoundingBoxGeometry(volumeData):
    """
        Build a bounding box mesh from a dictionnary.
        Args:  
            volumeData (dict): produced by computeBoundingBox
    """
    boundingCube = mobu.FBModelCube('{0}_Bounds1'.format(volumeData['sourceName']))
    boundingCube.Show = True
    
    #Shift cube geometry above the pivot
    boundingCube.GeometricTranslation = mobu.FBVector3d(volumeData['length']*0.5,volumeData['width']*0.5,volumeData['height']*0.5)
    boundingCube.GeometricScaling = mobu.FBVector3d(volumeData['length']*0.5,
                                                    volumeData['width']*0.5,
                                                    volumeData['height']*0.5)

    boundingCube.SetMatrix(volumeData['boundSpaceMatrix'])
    boundingCube.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
    
    return boundingCube


def ComputeBoundingBox(inputModel):
    """
        Returns worldSpace boundingbox of an FBModel
        Args:  
            inputModel (FBModel): object used to extract boundingbox
            
        returns dictionnary with keys:  minPoint                (FBVector4d)
                                        maxPoint                (FBVector4d)
                                        length                  (double)
                                        width                   (double)
                                        height                  (double)
                                        boundSpaceMatrix        (FBMatrix)
                                        sourceModelWorldMatrix  (FBMatrix)
                                        sourceName              (str)
    """
    #Extract worldspace matrix 
    sourceModelWorldMatrix = mobu.FBMatrix()
    inputModel.GetMatrix(sourceModelWorldMatrix) 
    
    #Take into account the geometrix offset..
    inputModelIsACamera = False 
    if type(inputModel) == mobu.FBCamera:
        inputModelIsACamera = True 

    geometrixOffsetMatrix = mobu.FBMatrix()
    if inputModel.Geometry != None and inputModelIsACamera == False:
        geometrixOffsetPosition = inputModel.GeometricTranslation
        geometrixOffsetScale    = inputModel.GeometricScaling

        geometrixOffsetPosition = mobu.FBVector4d(geometrixOffsetPosition[0],
                                                  geometrixOffsetPosition[1],
                                                  geometrixOffsetPosition[2],
                                                  1.0)

        geometrixOffsetScale  = mobu.FBSVector(geometrixOffsetScale[0],
                                               geometrixOffsetScale[1],
                                               geometrixOffsetScale[2])

        mobu.FBTRSToMatrix(geometrixOffsetMatrix,
                           geometrixOffsetPosition,
                           inputModel.GeometricRotation, 
                           geometrixOffsetScale)

        sourceModelWorldMatrix = sourceModelWorldMatrix*geometrixOffsetMatrix

    #Get the bounding box in object space
    box_minPoint = mobu.FBVector3d()
    box_maxPoint = mobu.FBVector3d()
    inputModel.GetBoundingBox (box_minPoint, box_maxPoint) 

    #Convert values into modelSpace
    minPointValue = mobu.FBVector4d(box_minPoint[0],box_minPoint[1],box_minPoint[2],1.0)
    maxPointValue = mobu.FBVector4d(box_maxPoint[0],box_maxPoint[1],box_maxPoint[2],1.0)

    worldMinPoint = mobu.FBVector4d()
    worldMaxPoint = mobu.FBVector4d()

    mobu.FBVectorMatrixMult(worldMinPoint, sourceModelWorldMatrix, minPointValue) 
    mobu.FBVectorMatrixMult(worldMaxPoint, sourceModelWorldMatrix, maxPointValue) 

    #We want to Build boundSpaceMatrix with normalize scale values
    scaleValues = mobu.FBSVector()

    boundingVolumeOrientation = mobu.FBVector3d()    
    mobu.FBMatrixToRotation(boundingVolumeOrientation, sourceModelWorldMatrix, mobu.FBRotationOrder.kFBXYZ)

    boundingVolumePosition = mobu.FBVector4d()  
    mobu.FBMatrixToTranslation(boundingVolumePosition, sourceModelWorldMatrix)

    boundSpaceMatrix = mobu.FBMatrix()
    mobu.FBTRSToMatrix(boundSpaceMatrix, boundingVolumePosition, boundingVolumeOrientation, scaleValues)

    #Compute scale in shape space
    boundSpaceMatrixInverse = mobu.FBMatrix()
    boundSpaceMatrixInverse.CopyFrom(boundSpaceMatrix)    
    boundSpaceMatrixInverse.Inverse()

    mobu.FBVectorMatrixMult(minPointValue, boundSpaceMatrixInverse, worldMinPoint) 
    mobu.FBVectorMatrixMult(maxPointValue, boundSpaceMatrixInverse, worldMaxPoint) 

    length = maxPointValue[0]-minPointValue[0]
    width = maxPointValue[1]-minPointValue[1]
    height = maxPointValue[2]-minPointValue[2]

    mobu.FBTRSToMatrix(boundSpaceMatrix, worldMinPoint, boundingVolumeOrientation, scaleValues)

    volumeData = collections.OrderedDict()

    volumeData['minPoint'] = worldMinPoint
    volumeData['maxPoint'] = worldMaxPoint
    volumeData['length'] = length
    volumeData['width'] = width
    volumeData['height'] = height
    volumeData['boundSpaceMatrix'] = boundSpaceMatrix
    volumeData['sourceModelWorldMatrix'] = sourceModelWorldMatrix
    volumeData['sourceName'] = inputModel.Name 

    return volumeData


def UnfreezeAll():
    """
        return (FBModel): object used to restore freeze state
    """
    sceneBaseName = os.path.basename(mobu.FBApplication().FBXFileName)
    sceneBaseName = os.path.splitext(sceneBaseName)[0]

    sceneUniquePath = os.path.dirname(mobu.FBApplication().FBXFileName)
    sceneUniquePath = posixpath.normpath(sceneUniquePath).split(':\\')[-1]

    freezeStateFolder = os.path.join(tempfile.gettempdir(), "mobuFreezeState", sceneUniquePath)

    freezeStateFolder = os.path.join(tempfile.gettempdir(), "mobuFreezeState", sceneUniquePath)
    if not os.path.exists(freezeStateFolder):
        os.makedirs(freezeStateFolder)

    scene = mobu.FBSystem().Scene
    
    unfrozeSets = []
    frozenSets = []

    unfrozeGroups = []
    frozenGroups  = []
    
    unfrozeArrayPaths = []
    frozenArrayPaths = []

    sceneModelArray = mobu.FBComponentList()
    mobu.FBFindObjectsByName('*' ,
                             sceneModelArray,
                             False,
                             True)

    freezeData = mobu.FBModelNull('unfreezeAllData_{0}_1'.format(sceneBaseName))
    freezeDataFile = os.path.join(freezeStateFolder,'{0}.json'.format(freezeData.Name))

    frozenStateProperty = freezeData.PropertyCreate('frozenStateJsonFile',  mobu.FBPropertyType.kFBPT_charptr, 'String', False, True, None ) 
    frozenStateProperty.Data = freezeDataFile

    mobu.FBBeginChangeAllModels () 
    objectToUnfreeze = []

    for group in scene.Groups:
        if group.Pickable:
            unfrozeGroups.append(group.FullName)
        else:
            frozenGroups.append(group.FullName)
        objectToUnfreeze.append(group)
        
    for objectSet in scene.Sets:
        if group.Pickable:
            unfrozeSets.append(objectSet.FullName)
        else:
            frozenSets.append(objectSet.FullName)
        objectToUnfreeze.append(objectSet)

    for modelObject in sceneModelArray:
        if modelObject.Pickable:
            unfrozeArrayPaths.append(modelObject.FullName)
        else:
            frozenArrayPaths.append(modelObject.FullName)
        objectToUnfreeze.append(modelObject)

    for modelObject in objectToUnfreeze:
        modelObject.Pickable = True

    mobu.FBEndChangeAllModels () 

    data = {'unfrozeArrayPaths':unfrozeArrayPaths,
            'frozenArrayPaths':frozenArrayPaths,
            'unfrozeSets':unfrozeSets,
            'frozenSets':frozenSets,
            'unfrozeGroups':unfrozeGroups,
            'frozenGroups':frozenGroups}

    with open(freezeDataFile, 'w') as jsonTargetFile:
         json.dump(data, jsonTargetFile,indent=4, sort_keys=True)


def RestoreFreezeState(freezeStorage,
                       deleteMetaData=True):
    """
        Restore freeze state produced by UnFreeze all function
        Args:  
            freezeStorage (FBModel): object with metadata to restore freeze state
    """
    frozenStateProperty = freezeStorage.PropertyList.Find('frozenStateJsonFile')
    scene = mobu.FBSystem().Scene

    if frozenStateProperty is None :
        return

    freezeDataFile = frozenStateProperty.Data
    data = None

    if not os.path.isfile(freezeDataFile):
        return

    with open(freezeDataFile, 'r') as jsonSourceFile:
        data = json.load(jsonSourceFile)

    mobu.FBBeginChangeAllModels () 

    for objectSet in scene.Sets:
        if unicode(objectSet.FullName) in data['unfrozeSets']:
            objectSet.Pickable = True
        if unicode(objectSet.FullName) in data['frozenSets']:
            objectSet.Pickable = False

    for group in scene.Groups:
        if unicode(group.FullName) in data['unfrozeGroups']:
            group.Pickable = True
        if unicode(group.FullName) in data['frozenGroups']:
            group.Pickable = False

    for modelObjectName in data['frozenArrayPaths']:
        modelObject = mobu.FBFindObjectByFullName(str(modelObjectName))
        if modelObject != None :
            modelObject.Pickable = False

    for modelObjectName in data['unfrozeArrayPaths']:
        modelObject = mobu.FBFindObjectByFullName(str(modelObjectName))
        if modelObject != None :
            modelObject.Pickable = True

    mobu.FBEndChangeAllModels () 

    if deleteMetaData :
        freezeStorage.FBDelete()


'''
Returns the distance between two models
'''
def Distance( InputModelA, InputModelB ):

    ModelATranslation = mobu.FBVector3d();
    ModelBTranslation = mobu.FBVector3d();
    
    InputModelA.GetVector(ModelATranslation)
    InputModelB.GetVector(ModelBTranslation)
    
    return math.sqrt( pow((ModelATranslation[0]-ModelBTranslation[0]),2) + pow((ModelATranslation[1]-ModelBTranslation[1]),2) + pow((ModelATranslation[2]-ModelBTranslation[2]),2))        

def GetTextureList( modelList ):
    '''
    Function that returns a list of textures that belong to a list of models
    '''
    textureList= []
    
    for model in modelList:
        materials= []
        for i in range(model.GetSrcCount()):
            connection = model.GetSrc(i)
            if connection.ClassName() == 'FBMaterial':
                materials.append( connection )

        for material in materials:
            if material.GetTexture():
                textureList.append( material.GetTexture() )

    return textureList
'''
Returns true if the model is visible in the viewport.
Takes into consideration:
    Group.Show property
    Set.Visibility property
    Model.Show property
    Model.Visibility property
Currently doesn't take into consideration:
    Model.VisibilityInheritance
'''    
def IsModelVisible( InputModel ):
    
    if not InputModel.Show: 
        return False
    
    if not InputModel.IsVisible: 
        return False
    
    modelSet = GetSet(InputModel)
    if modelSet != None and not modelSet.Visibility.Data: 
        return False
    
    modelGroups = GetGroups(InputModel)
    for group in modelGroups:
        if not group.Show: 
            return False
    
    return True

'''
Gets the set that the FBModel is a part of. Returns None if it's not part of a set
ToDo: move this to the component module because not just FBModel objects can be contained in a set
'''
def GetSet( InputModel ):
    for index in range(0, InputModel.GetDstCount()):
        plug = InputModel.GetDst( index )
        if type( plug ) is mobu.FBSet:
            return plug
    return None

'''
Gets a list of the FBGroups that the FBModel is a part of    
ToDo: move this to the component module because not just FBModel objects can be contained in a group
'''
def GetGroups( InputModel ):
    groupList = list()
    for index in range(0, InputModel.GetDstCount()):
        plug = InputModel.GetDst( index )
        if type( plug ) is mobu.FBGroup:
            groupList.append( plug )
    return groupList

'''
Gets all the models that act as bones to skeletal deform the target model. 
Will return all and any FBModels not just FBSkeletonModels
'''            
def GetBoneModels( InputSkinModelList ):
    
    #Check if we have been passed an iterable parameter
    if not hasattr(InputSkinModelList,"__iter__"):
        InputSkinModelList = [InputSkinModelList]
    
    returnBoneModelSet = set()
    
    for inputSkinModel in InputSkinModelList:
        if inputSkinModel.SkeletonDeformable:
            for deformer in inputSkinModel.Deformers:
                for component in deformer.Components:
                    for index in range(0, component.GetSrcCount()):
                        returnBoneModelSet.add( component.GetSrc( index ))
                
    return returnBoneModelSet

'''
Gets all the skeletal deformable models that uses the passed model as bone.
'''
def GetSkinModels( InputBoneModelList ):
    
    #Check if we have been passed an iterable parameter
    if not hasattr( InputBoneModelList, "__iter__"):
        InputBoneModelList = [InputBoneModelList]
        
    returnSkinModelSet = set()
    
    for component in RS.Globals.gScene.Components:
        if component != RS.Globals.gScene.RootModel and type( component ) is mobu.FBModel:
            if component.SkeletonDeformable:
                for boneModel in InputBoneModelList:
                    if boneModel in GetBoneModels( [component] ):
                        returnSkinModelSet.add( component )
                    
    return returnSkinModelSet

'''
Gets the the time range of the passed model(s)
'''
def GetTimeRange( pInputModel, pInputTakeList = None, pInputLayerNameList = None, pInputAnimationNodeNameList = None ):
        
    #Get our target take list. If no takes are passed we assume that 
    #the user wants to work with the current take.
    lTargetTakeList = pInputTakeList
    if pInputTakeList == None :
        lTargetTakeList = [ RS.Core.System.CurrentTake ]

    #Setup our return time range
    lReturnTimeRange = RS.Utils.Scene.Time.Range()

    #Loop through each take
    for iTargetTake in lTargetTakeList:

        #Get our target layer list
        lTargetLayerList = []
        if pInputLayerNameList == None : 
            #If no layers names are passed we assume that the user wants to work with all the layers on the take
            lTargetLayerList = RS.Utils.Scene.Layer.GetLayerList( iTargetTake )
        else:
            #Otherwise get the layers from the input list
            lTargetLayerList = RS.Utils.Scene.Layer.GetLayerList( iTargetTake, pInputLayerNameList )
        
        #Go through each of the layers
        for iTargetLayer in lTargetLayerList:

            #Set this layer as the current active layer and select it
            RS.Utils.Scene.Layer.SetLayerActive( iTargetTake, iTargetLayer )
            RS.Utils.Scene.Layer.SetLayerSelected( iTargetTake, iTargetLayer )
            
            #Go through each of the animation nodes on the input model
            for iSubAnimationNode in pInputModel.AnimationNode.Nodes:
                
                #Check the animation node is one that we are interested in
                if pInputAnimationNodeNameList != None :
                    if iSubAnimationNode.UserName not in pInputAnimationNodeNameList:
                        continue
                    
                #Get this sub animation node's frame range
                lSubAnimationNodeTimeRange = RS.Utils.Scene.AnimationNode.GetTimeRange( iSubAnimationNode, True )
                lReturnTimeRange.Merge( lSubAnimationNodeTimeRange )
    
    #Return our time range            
    return lReturnTimeRange

'''
Attempts to get the animation node on the model
DEPRECATED - Not used and not sure what it was used for
'''
'''
def GetAnimationNode( pInputModel, pInputAnimationNodeName ):
    #Go through each of the animation nodes on the input model
    for iSubAnimationNode in pInputModel.AnimationNode.Nodes:
        #Check the animation node is one that we are interested in
        if iSubAnimationNode.UserName == pInputAnimationNodeName :
            return iSubAnimationNode
    #Failed to find the animation node so return None
    return None
'''

'''
Goes through the passed model and offsets all it's keys by the passed delta, on the passed takes, 
on the passed layers and targets specific AnimationNodes
'''
def OffsetKeys( pInputModel, pInputTimeDelta, pInputTakeList = None, pInputLayerNameList = None, pInputAnimationNodeNameList = None ):
        
    #Get our target take list. If no takes are passed we assume that 
    #the user wants to work with the current take.
    lTargetTakeList = pInputTakeList
    if pInputTakeList == None :
        lTargetTakeList = [ RS.Core.System.CurrentTake ]

    #Loop through each take
    for iTargetTake in lTargetTakeList:

        #Get our target layer list
        lTargetLayerList = []
        if pInputLayerNameList == None : 
            #If no layers names are passed we assume that the user wants to work with all the layers on the take
            lTargetLayerList = RS.Utils.Scene.Layer.GetLayerList( iTargetTake )
        else:
            #Otherwise get the layers from the input list
            lTargetLayerList = RS.Utils.Scene.Layer.GetLayerList( iTargetTake, pInputLayerNameList )
        
        #Go through each of the layers
        for iTargetLayer in lTargetLayerList:

            #Set this layer as the current active layer and select it
            RS.Utils.Scene.Layer.SetLayerActive( iTargetTake, iTargetLayer )
            RS.Utils.Scene.Layer.SetLayerSelected( iTargetTake, iTargetLayer )
            
            #Go through each of the animation nodes on the input model
            for iSubAnimationNode in pInputModel.AnimationNode.Nodes:
                
                #Check the animation node is one that we are interested in
                if pInputAnimationNodeNameList != None :
                    if iSubAnimationNode.UserName not in pInputAnimationNodeNameList:
                        continue
                    
                #Offset the keys recursively by the passed Delta
                RS.Utils.Scene.AnimationNode.OffsetKeys( iSubAnimationNode, pInputTimeDelta, True )
                
'''
Copies animation from the source take across to the target take on the passed model
NOTE: Will not work on properties with sub nodes like X, Y, Z. Shoudn't use.
'''
def CopyAnimationBetweenTakes( pInputModel, pInputSourceTake, pInputTargetTakeList, pPropertyNameList ):
    
    #First we set the source take as our current take
    RS.Core.System.CurrentTake = pInputSourceTake
    
    #Store animation nodes from source layer
    lPropertyFCurveList = []
    for iPropertyName in pPropertyNameList:
        lProperty = pInputModel.PropertyList.Find( iPropertyName )
        if ( lProperty != None ):
            if( lProperty.IsAnimated() ):
                lPropertyAnimationNode = lProperty.GetAnimationNode()
                lPropertyFCurve = iPropertyName, lPropertyAnimationNode.FCurve
                lPropertyFCurveList.append( lPropertyFCurve )
            
    #For each take
    for iTargetTake in pInputTargetTakeList:
        #Set it as our current take
        RS.Core.System.CurrentTake = iTargetTake
        
        #For each our our properties
        for iPropertyFCurve in lPropertyFCurveList:
            lProperty = pInputModel.PropertyList.Find( iPropertyFCurve[0] )
            if ( lProperty != None ):
                lProperty.SetAnimated( True )
                lPropertyAnimationNode = lProperty.GetAnimationNode()
                lPropertyAnimationNode.FCurve = iPropertyFCurve[1]
                
'''
Freezes the passed model's transforms so that they can no longer be changed. 
This is now done through DOF
'''
def FreezeTransform( Model ):
    FreezeRotation( Model )
    FreezeTranslation( Model )
    FreezeScaling( Model )

'''
Freezes the passed model's rotation so that they can no longer be changed. 
'''
def FreezeRotation( Model ):   
    Model.Rotation.SetLocked( True )
    Model.PropertyList.Find( 'RotationActive' ).Data = True
    Model.PropertyList.Find( 'RotationMin' ).Data = Model.Rotation.Data
    Model.PropertyList.Find( 'RotationMax' ).Data = Model.Rotation.Data
    Model.PropertyList.Find( 'RotationMinX' ).Data = True
    Model.PropertyList.Find( 'RotationMinY' ).Data = True
    Model.PropertyList.Find( 'RotationMinZ' ).Data = True
    Model.PropertyList.Find( 'RotationMaxX' ).Data = True
    Model.PropertyList.Find( 'RotationMaxY' ).Data = True
    Model.PropertyList.Find( 'RotationMaxZ' ).Data = True
    
'''
Freezes the passed model's translation so that they can no longer be changed. 
'''    
def FreezeTranslation( Model ):    
    Model.Translation.SetLocked( True )
    Model.PropertyList.Find( 'TranslationActive' ).Data = True
    Model.PropertyList.Find( 'TranslationMin' ).Data = Model.Translation.Data
    Model.PropertyList.Find( 'TranslationMax' ).Data = Model.Translation.Data
    Model.PropertyList.Find( 'TranslationMinX' ).Data = True
    Model.PropertyList.Find( 'TranslationMinY' ).Data = True
    Model.PropertyList.Find( 'TranslationMinZ' ).Data = True
    Model.PropertyList.Find( 'TranslationMaxX' ).Data = True
    Model.PropertyList.Find( 'TranslationMaxY' ).Data = True
    Model.PropertyList.Find( 'TranslationMaxZ' ).Data = True

'''
Freezes the passed model's scaling so that they can no longer be changed. 
'''   
def FreezeScaling( Model ):    
    Model.Scaling.SetLocked( True )
    Model.PropertyList.Find( 'ScalingActive' ).Data = True
    Model.PropertyList.Find( 'ScalingMin' ).Data = Model.Scaling.Data
    Model.PropertyList.Find( 'ScalingMax' ).Data = Model.Scaling.Data
    Model.PropertyList.Find( 'ScalingMinX' ).Data = True
    Model.PropertyList.Find( 'ScalingMinY' ).Data = True
    Model.PropertyList.Find( 'ScalingMinZ' ).Data = True
    Model.PropertyList.Find( 'ScalingMaxX' ).Data = True
    Model.PropertyList.Find( 'ScalingMaxY' ).Data = True
    Model.PropertyList.Find( 'ScalingMaxZ' ).Data = True
    
'''
Offsets the models translation in global space by the given amount
'''
def OffsetGlobalTranslation( Model, X = 0, Y = 0, Z = 0 ):
    
    oldModelTranslation = mobu.FBVector3d();
    Model.GetVector( oldModelTranslation, mobu.FBModelTransformationType.kModelTranslation )
    newModelTranslation = mobu.FBVector3d( oldModelTranslation[0] + X,  oldModelTranslation[1] + Y,  oldModelTranslation[2] + Z)  
    Model.SetVector( newModelTranslation, mobu.FBModelTransformationType.kModelTranslation )
    

'''
Sets the models translation in global space by the given amount. None = leave the axis as it is.
'''
def SetGlobalTranslation( Model, X = None, Y = None, Z = None ):
    
    oldModelTranslation = mobu.FBVector3d();
    Model.GetVector( oldModelTranslation, mobu.FBModelTransformationType.kModelTranslation )
    newModelTranslation = mobu.FBVector3d()
    
    if X != None: 
        newModelTranslation[0] = X 
    else:
        newModelTranslation[0] = oldModelTranslation[0]
        
    if Y != None: 
        newModelTranslation[1] = Y 
    else:
        newModelTranslation[1] = oldModelTranslation[1]    

    if Z != None: 
        newModelTranslation[2] = Z 
    else:
        newModelTranslation[2] = oldModelTranslation[2]

    Model.SetVector( newModelTranslation, mobu.FBModelTransformationType.kModelTranslation )

def GetDimensionsOfSelectedModels():
    '''
    Creates a dictionary with the dimentions of the model(s) you have selected
    Dimentions are given in inches and feet
    
    Returns:
        Dict - FBModel : int, int, int, float, float, float
        (FBModel : height in inches,
                   width in inches,
                   length in inches,
                   height in feet,
                   width in feet,
                   length in feet)
    '''
    components = mobu.FBModelList()

    # Get the selected models.
    mobu.FBGetSelectedModels(components)

    dimensionDict = {}

    for component in components:
        if type(component) == mobu.FBModel:
            vectorMin = mobu.FBVector3d(0 ,0 ,0)
            vectorMax = mobu.FBVector3d(0 ,0 ,0)
            component.GetBoundingBox(vectorMin, vectorMax)

            #converting to feet and inches
            heightInches = vectorMax[0] * 39.370
            heightFeet = int(heightInches/12)
            heightInches = round(heightInches - heightFeet*12, 2)
            widthInches = vectorMax[1] * 39.370
            widthFeet = int(widthInches/12)
            widthInches = round(widthInches - widthFeet*12, 2)
            lengthInches = vectorMax[2] * 39.370
            lengthFeet = int(lengthInches/12)
            lengthInches = round(lengthInches - lengthFeet*12, 2)

            dimensionDict[component] = (heightInches,
                                        widthInches,
                                        lengthInches,
                                        heightFeet,
                                        widthFeet,
                                        lengthFeet,
                                        vectorMin,
                                        vectorMax)

    return dimensionDict

def DeleteChildren( model ):  
    '''
    Recursivly goes through all the children of a given model, and deletes them 
    '''
    for child in model.Children:
        if len (child.Children) < 1:
            child.FBDelete()  
        else:
            DeleteChildren(child)
            
def GetChildren( parentModelList, recursive = True, includeParent = True ):
    '''
    Returns the children of all the models in passed list. By default it will also recurse.
    '''
    returnSet = set()
    for parentModel in parentModelList:
        if includeParent:
            returnSet.add( parentModel )
        for childmodel in parentModel.Children:
            returnSet.add( childmodel )
            if recursive:
                returnSet.update( GetChildren([childmodel], True, True ) )
    return returnSet