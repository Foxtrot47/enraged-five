"""
Author: Mark.Harrison-Ball@Rockstargames.com

Clip Model Class
Generic Class to collect Clip Info 

If we want additional info we can then expand this class to include that data

Note we convert to lower case for comparison

"""

########################################################################
class ClipInfo:
    """"""
    Name = None
    Index = None
    Start = None
    End = None
    Duration = None
    CameraName = "No camera"
    Markin = None
    MarkOut = None
    Used = False  # Can be useful for Clip comparrison tests with different tracks
    Error = None  # Can be used to hold error inof about clip


    """
    Methods
    """
def GetClipInfo(pClipNode, index = 0):
    iClipInfo = None
    if pClipNode:
        iClipInfo = ClipInfo()
        iClipInfo.Name = pClipNode.Name
        iClipInfo.Index = index
        iClipInfo.Start = pClipNode.Start.GetFrame()
        iClipInfo.End = pClipNode.Stop.GetFrame()
        iClipInfo.Duration = iClipInfo.End - iClipInfo.Start
        if pClipNode.ShotCamera != None: # Camera Can return None is no camera
            iClipInfo.CameraName = pClipNode.ShotCamera.Name
        iClipInfo.Markin = pClipNode.MarkIn.GetFrame()
        iClipInfo.MarkOut = pClipNode.MarkOut.GetFrame()
    return iClipInfo
        

        
        
        
        
        
        
        
            
        
        
        
        
        
    
    



    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    


