"""
Author: Mark.Harrison-Ball@Rockstargames.com
        RSTrackInfo/TrackInfo.py

TRACK Model Class
Generic Class Wrapper to collect Track Info 

Usages:
We can use this so we can snapshot the track info and do compares

Why:
Because using the FBStory().Track info directly will and asign to a variable will keep the referce
Deepcopy does not work :(

If we want additional info we can then expand this calss to include that data

TODO:
      Add Delete Clip and Rename Fucntions
      
      Need to add unique GUID

"""

from pyfbsdk import *

import RS.Utils.Scene.Story
import RS.Utils.Scene.Story.Clip
import uuid
    
########################################################################
class TrackInfo:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, iTrackNode):
        """Constructor"""
        self.guid = uuid.uuid1() # Create a unique GUID
        self.TrackNode = iTrackNode
        self.Name = iTrackNode.Name
        self.Duration = 0
        self.TotalClips = 0
        self.Trackcount = 0
        self.Clips = []
        self.__GetTrackInfo__() # populate the clip info form our Track
        
        """Methods"""
    def __GetTrackInfo__(self):
        for i in range(len(self.TrackNode.Clips)):
            #print self.TrackNode.Clips[i].Name
            iClipObject = RS.Utils.Scene.Story.Clip.GetClipInfo(self.TrackNode.Clips[i], i)
            self.Duration = self.Duration + iClipObject.Duration
            self.Clips.append(iClipObject)
        
  
    
"""
Common Public Track Methods
"""

# Get Info about the passed in Track
def GetTrackInfoByName(pName):
    pNewTrackInfo = None
    for iTrack in RS.Utils.Scene.Story.Manager.RootEditFolder.Tracks:
        if iTrack.Name.lower() == pName.lower():
            pNewTrackInfo = TrackInfo(iTrack)
            break
    return pNewTrackInfo


# Get all Track Data,  Returns: List of Track Info
# Add parameter to get tracks name containName
def GetTrackInfo(pContainsName = None):
    lTrackArray = []  
    for iTrack in RS.Utils.Scene.Story.Manager.RootEditFolder.Tracks:  
        if pContainsName!= None:
            if pContainsName.lower() in iTrack.Name.lower():
                pNewTrackInfo = TrackInfo(iTrack)
                lTrackArray.append(pNewTrackInfo)
        else:
            pNewTrackInfo = TrackInfo(iTrack)
            lTrackArray.append(pNewTrackInfo)            
            
    return lTrackArray





# Horse!
def ClearCurrentTrack():
    return
    
    
    
    
    
    
        
    
    


