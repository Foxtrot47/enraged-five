"""
Usage:
    This module should only contain functionality related to working with FBTime. 
    Current this module is a little muddle with a custom Range type that should probably use the FBTimeSpan
Author:
    Ross George
"""

import pyfbsdk as mobu

import RS.Core
import RS.Globals


class Range(object):
    """ Represents a range of time. Should favour FBTimeSpan """
    
    def __init__(self, pStart=mobu.FBTime.Infinity, pEnd=mobu.FBTime.MinusInfinity):
        """
        Constructor
        Arguments:
            pStart: FBTime; start time
            pEnd: FBTime: end time
        """
        self.Start = pStart
        self.End = pEnd
    
    def __str__(self):
        """ string representation of the current frame range"""
        return "[{0}][{1}]".format(self.Start, self.End)

    def Merge(self, pInputFrameRange):
        """
        Merges the passed time range with this time range.
        Arguments:
            pInputFrameRange: FBTime; FBTime with data of the new time range
        """
        self.Start = GetLowerTime(self.Start, pInputFrameRange.Start)
        self.End = GetHigherTime(self.End, pInputFrameRange.End)

    def Valid(self):
        """  Checks if the time range contains valid values (i.e. non infinity values where start is less than end) """
        return self.Start.Get() < self.End.Get()

    def GetStartFrame(self):
        """ Returns the start frame based on default FPS """
        return self.Start.GetFrame()

    def GetEndFrame(self):
        """  Return the end frame based on default FPS """
        return self.End.GetFrame()


def GetLowerTime(pInputTimeA, pInputTimeB):
    """
    Returns the lower of the two times
    Arguments:
        pInputTimeA = FBTime; FBTime to compare to pInputTimeB
        pInputTimeB = FBTime; FBTime to compare to pInputTimeA
    Return:
        FBTime
    """
    if pInputTimeA.Get() <= pInputTimeB.Get():
        return pInputTimeA
    else:
        return pInputTimeB


def GetHigherTime(pInputTimeA, pInputTimeB):
    """
    Returns the greater of the two FBTime objects
    Arguments:
        pInputTimeA = FBTime; FBTime to compare to pInputTimeB
        pInputTimeB = FBTime; FBTime to compare to pInputTimeA
    Return:
        FBTime
    """
    if pInputTimeA.Get() >= pInputTimeB.Get():
        return pInputTimeA
    else:
        return pInputTimeB
    
    
def IsNegative(pInputTime):
    """
    Returns true if the FBTime is below zero
    Arguments:
        pInputTimeA = FBTime; FBTime to be checked if it is below zero
    Return:
        boolean
    """
    return pInputTime.Get() < 0 


def IsPositive(pInputTime):
    """
    Returns true if the FBTime is above zero
    Arguments:
        pInputTimeA = FBTime; FBTime to be checked if it is below zero
    Return:
        boolean
    """
    return pInputTime.Get() > 0


class TimelineMonitor(object):
    """ 
    Callback: Select current camera in scene based on current frame
    """    
    def __init__(self):
        """
        Constructor
        """
        self.previousFrame = 0
        self.currentFrame = 0
    
    def TimelineSync(self, control, event):
        """
        Method that monitors changes in the timeline
        Arguments:
            control: object calling this method
            event: FBEvent; the event that caused this method to be called

        """
        self.currentFrame = mobu.FBSystem().LocalTime.GetFrame()
        if self.currentFrame != self.previousFrame:
            self.TimelineMethod(self.currentFrame, self.previousFrame)
            self.previousFrame = self.currentFrame

    def Register(self):
        """ Register TimelineMonitor as a callback in Motion Builder """
        self.Unregister()
        RS.Globals.EvaluateManager.OnSynchronizationEvent.Add(self.TimelineSync)
    
    def Unregister(self):
        """ Unregister TimelineMonitor as a callback in Motion Builder """
        RS.Globals.EvaluateManager.OnSynchronizationEvent.Remove(self.TimelineSync)
    
    def TimelineMethod(self, currentFrame, previousFrame):
        """
        Method that is called when the frame changes. This method is meant to be overridden.
        Arguments:
            currentFrame: int; the current frame
            previousFrame: int; the previous frame
        """
        print currentFrame, previousFrame