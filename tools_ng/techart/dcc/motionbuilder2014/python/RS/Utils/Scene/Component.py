"""
Usage:
    This module should only contain functionality related to working with FBComponents (or their sub types).
Author:
    Ross George
"""

from pyfbsdk import *
import pyfbsdk as mobu

from inspect import ismethod
import re

import unbind
import hashlib

from RS import Globals
from RS.Utils import Class
from RS.Utils.Scene import Plug, Property

DEBUG = False

FBPROPERTY_DICTIONARY =    {str: (FBPropertyType.kFBPT_charptr, ""),
                            FBVector2d: (FBPropertyType.kFBPT_Vector2D, FBVector2d(0, 0)),
                            FBVector3d: (FBPropertyType.kFBPT_Vector3D, FBVector3d(0, 0, 0)),
                            FBVector4d: (FBPropertyType.kFBPT_Vector4D, FBVector4d(0, 0, 0, 0)),
                            int: (FBPropertyType.kFBPT_int, 0),
                            float: (FBPropertyType.kFBPT_double, 0.0),
                            bool: (FBPropertyType.kFBPT_bool, True)}

class SelectionLocker:
    """
    This class allows us to prevent selection of components. WIP - Experimental.
    """

    def __init__(self):
        self._componentMonitorSet = set()

    def _OnConnectionStateNotify(self, source, event):

        if event.Action == FBConnectionAction.kFBSelect:
            if event.Plug in self._componentMonitorSet:
                event.Plug.Selected = False

    def Lock(self, targetComponent):
        self._componentMonitorSet.add(targetComponent)

    def Unlock(self, targetComponent):
        self._componentMonitorSet.remove(targetComponent)

    def Clear(self):
        self._componentMonitorSet.clear()

    def Deactivate(self):
        Globals.System.OnConnectionStateNotify.Remove(self._OnConnectionStateNotify)

    def Activate(self):

        Globals.System.OnConnectionStateNotify.Add(self._OnConnectionStateNotify)


def Delete(targetComponents, ignoreObjectFlags=True, deleteNamespaces=False):
    """
    Deletes a list of components passed in. Does the delete in a certain order
    which avoids certain crashes. For example [2549556]. Can also choose to override the object
    flags on the object if needed. This means that objects that are marked as 'not deletable' will
    be overriden so they are deletable. It will also check for dependencies between certain objects
    passed in. Useful for avoiding the dialog boxes that appear when deleting a constraint being used
    as a macro in something else. Created for [2653407]

    Arguments:
        targetComponents (list of FBComponents):
            Components that we want to delete.
        ignoreObjectFlags (boolean):
            Indicates if the deletion forcably delete objects that are flagged as 'not deletable'
        deleteNamespaces (boolean):
            deletes namespace passed in, by default namespaces are not deleted
    """

    # First we remove any default components - the scene, application, default shader etc...
    # Deleting these can cause problems - crashes or it will fail to delete them and enter a
    # loop repeatably attempting to delete them.
    targetComponents = targetComponents

    remainingComponents = set()

    # All FBStoryClip components
    storyClipComponents = set()

    # All FBCharacter components
    characterComponents = set()

    # All FBCharacterPose components
    characterPoseComponents = set()

    # All FBDevice derived components - in some instances making sure we delete
    # the R* Expression device first prevented crashes [2653361]
    deviceComponents = set()

    # All FBConstraint derived components
    constraintComponents = set()

    # All FBDeformer derived components
    deformerComponents = set()

    # All FBModel derived components that are deformable - fixes [2783660]
    deformableModelComponents = set()

    # Don't delete namespaces - this causes issues [2954140]
    # If a namespace has children, those children need to be deleted first before the parent namespace
    # can be deleted to avoid errors.
    # This makes order important because child namespaces also need to be deleted before the parent namespace.
    namespaceComponents = []

    for component in targetComponents:
        if component.FullName in Globals.DefaultComponentNames:
            continue
        elif isinstance(component, mobu.FBCharacter):
            characterComponents.add(component)
        elif isinstance(component, mobu.FBCharacterPose):
            characterPoseComponents.add(component)
        elif isinstance(component, mobu.FBDevice):
            deviceComponents.add(component)
        elif isinstance(component, mobu.FBConstraint):
            # turn off contraints before deleting. helps with crashes such as
            # url:bugstar:3792887
            component.Active = False
            constraintComponents.add(component)
        elif isinstance(component, mobu.FBDeformer):
            deformerComponents.add(component)
        elif isinstance(component, mobu.FBModel) and component.IsDeformable:
            deformableModelComponents.add(component)
        elif isinstance(component, mobu.FBNamespace):
            namespaceComponents.append(component)
        elif isinstance(component, mobu.FBStoryClip):
            storyClipComponents.add(component)
        else:
            remainingComponents.add(component)

    componentSets =[storyClipComponents, deformerComponents,
                    constraintComponents, characterComponents, characterPoseComponents,
                    deviceComponents, deformableModelComponents, remainingComponents]

    if deleteNamespaces:
        componentSets.append(namespaceComponents)

    # Check that there are components to delete
    hasComponents = 0
    for componentSet in componentSets:
        hasComponents += len(componentSet)
    if not hasComponents:
        return

    # property to check if the merge transaction has started
    # Now delete the components in passes
    for componentSet in componentSets:

        # Only perform the delete if the list contains some content.
        if not componentSet:
            continue

        # Keep spinning until we have deleted everything in the list - checking for dependencies to start with
        # Loop through our components keeping tabs on number deleted
        for component in componentSet:
            # Check the component has not already been deleted in another pass or as a by product
            # of deleting another connected component.

            if IsDestroyed(component):
                continue

            # If we are set to ignore object flags then force through the deletable flag.
            if ignoreObjectFlags and not component.HasObjectFlags(mobu.FBObjectFlag.kFBFlagDeletable):
                component.EnableObjectFlags(mobu.FBObjectFlag.kFBFlagDeletable)

            # Actually delete the component
            component.FBDelete()
            if deleteNamespaces:
                Globals.Scene.NamespaceCleanup()

def GetComponents( Namespace = None,
                   TypeList = [FBPlug],
                   DerivedTypes = True,
                   IncludeRegex = None,
                   ExcludeRegex = None ):
    '''
    Returns a list of components that match the given parameters. Can use type, regex and namespace.
    We are not using NamespaceGetContentList directly because it doesn't work as expected. Need to
    bug this with autodesk. Also this function has more options.

    Keyword Arguments:
        Namespace:
            The namespace to search in. None will search all components.
        TypeList:
            The types to match against.
        DerivedTypes:
            True if we are also interest in the derived types of the types in the TypeList
        IncludeRegex:
            Regular expression to match to for inclusion.
        ExcludeRegex:
            Regular expression to match to for exclusion.

    Returns:
        A list of components that match the passed arguments.
    '''

    #If we are dealing with a single type then turn it into a list
    if type(TypeList) != list:
        TypeList = [ TypeList ]

    #If we are interested in derived types flesh the list out
    if DerivedTypes:
        TypeList = Class.GetDerivedTypeList( TypeList )

    #Compile our regexes first if we have some
    if IncludeRegex != None: IncludeRegex = re.compile( IncludeRegex )
    if ExcludeRegex != None: ExcludeRegex = re.compile( ExcludeRegex )

    #Get all components - note that if we pass None in as the namespace parameter we get back all components
    if Namespace is None or Namespace == "":
        componentList = [Globals.Components]
    else:
        componentList = [component for component in Globals.Components
                         if GetNamespace(component) == Namespace]

    #Go through the list and find the components we are interested in.
    processComponentList = []
    for component in componentList:

        #Check type first
        if not type(component) in TypeList:
            continue #The component is not an instance of the type

        #Process our exclude and include regexes
        if IncludeRegex != None and IncludeRegex.search(component.Name) == None: continue
        if ExcludeRegex != None and ExcludeRegex.search(component.Name) != None: continue

        #If we have got this far then the component is a match!
        processComponentList.append( component )

    return processComponentList


def GetAnimationNodeList(TargetComponent):
    """
    Returns a list of all animation nodes that are used on all the properties of the passed
    component. Including all subanimation nodes.
    """

    animationNodeList = []

    for prop in TargetComponent.PropertyList:
        propAnimationNodeList = Property.GetAnimationNodeList(prop)
        animationNodeList.extend(propAnimationNodeList)

    return animationNodeList

def CopyAnimationByNamespace(componentMatchList):
    '''
    finds a list of componenets with namespaces passed - one for the source asset and one for target.
    using these lists, it will copy animation from the source to the target.

    args:
    componentMatchList: A list of ComponentMatchObjects which pairs matched components together.

    returns:
    a list of ComponentMatchObjects which pairs matched components together.
    '''
    # Copy data and animation data
    initialTake = Globals.System.CurrentTake

    # Then we'll go through each of the takes
    for take in Globals.Scene.Takes:
        Globals.System.CurrentTake = take
        # Get initial layer so we can return state
        initialLayerIndex = take.GetCurrentLayer()
        # Then each layer
        for layerIndex in xrange(take.GetLayerCount()):
            take.SetCurrentLayer(layerIndex)
            # Go through each of our component matches and excute a copy
            for componentMatch in componentMatchList:
                componentMatch.CopyAnimationData()
        # Go back to previous layer
        take.SetCurrentLayer(initialLayerIndex)
    # Go back to initial take
    Globals.System.CurrentTake = initialTake

    # Copy normal (non animated data)
    for componentMatch in componentMatchList:
        componentMatch.CopyData()

class ComponentMatch:
    '''
    Represents a match between two components (for copying/transfering data)
    '''
    def __init__(self, SourceComponent, TargetComponent):
        self.SourceComponent = SourceComponent
        self.TargetComponent = TargetComponent
        self.PropertyMatchList = list()

        # Create property matches (allows us to efficiently look up)
        try:
            sourcePropertyList = list(self.SourceComponent.PropertyList)
            targetPropertyList = list(self.TargetComponent.PropertyList)
            for sourceProp in sourcePropertyList:
                for targetProp in targetPropertyList:
                    if sourceProp.Name == targetProp.Name:
                        if type(sourceProp) == type(targetProp):
                            if Property.IsSupportedCopyType(sourceProp):
                                self.PropertyMatchList.append(Property.PropertyMatch(sourceProp,
                                                                                                    targetProp))
                                targetPropertyList.remove(targetProp)
                                break
        except:
            pass

    def CopyAnimationData(self):
        '''
        Executes a animation copy operation on all property matches on current layer
        on current take
        '''
        for propertyMatch in self.PropertyMatchList:
            propertyMatch.CopyAnimationData()

    def CopyData(self):
        '''
        Executes a animation copy operation on all property matches on current layer
        on current take
        '''
        for propertyMatch in self.PropertyMatchList:
            propertyMatch.CopyData()

def GetNamespace(targetComponent):
    """
    Returns the full namespace of the component. So if the object was called
    'one:two:cube_01' it would return 'one:two'. Returns None if no namespace is present.
    """

    if targetComponent is not None and targetComponent.OwnerNamespace is not None:
        return targetComponent.OwnerNamespace.LongName
    else:
        return None

def SetNamespace( targetComponent, newNamespace ):
    """
    Sets the namespace of the component. Will overwrite any previous namespace.
    If you had an object called 'one:two:cube_01' and set its namespace to 'three'
    you would end up with an object called 'three:cube_01'.
    """

    targetComponent.LongName = newNamespace + ":" + targetComponent.Name

def RemoveNamespace( targetComponent ):
    """
    Completely removes the namespace from a component.
    """

    targetComponent.LongName = targetComponent.Name

def GetComponentMatchList( SourceComponentList,
                           TargetComponentList,
                           TypeMatch = True,
                           Exclude = None):
    '''
    Summary:
        Returns a list of component matches between source and target component lists.
        Matches are based off the name of the component (excluding namespace) and type.
        Exact type matching can be turned off (so just using name to match).

    Keyword Arguments:
        SourceComponentList:
            The list of components to match from.
        TargetComponentList:
            The list of components to match to.
        TypeMatch:
            This indicates if we want the types of the component to be matched to be the same.
            Defaults to true.
        Exclude:
            List of names to exclude

    Returns:
        A list of ComponentMatchObjects which pairs matched components together.
    '''

    # Now go through the components finding matches
    componentMatchList = list()
    Exclude = Exclude or []
    for sourceComponent in SourceComponentList:
        for targetComponent in TargetComponentList:

            # If we want the types of the objects to match exactly check this
            if TypeMatch and type(sourceComponent) != type(targetComponent):
                continue

            # Check that the names match
            sourceName = sourceComponent.Name
            if sourceName == targetComponent.Name and sourceName not in Exclude:

                # If we have got this far then we must have a component match so create one
                componentMatchList.append(ComponentMatch(sourceComponent, targetComponent))

                # Remove this targetComponent to avoid trying to match it with future sourceComponents and break
                TargetComponentList.remove(targetComponent)
                break

    return componentMatchList



class DataStore:
    """
    Can take a snapshot of a set of component's animation data.
    This snapshot can then be pasted back onto the same components on different
    layers and takes via Copy and Paste functions.
    """

    def __init__(self):
        """
        Creates a new DataStore class
        """

        self._componentSet = set()
        self._animationNodeDict = dict()

    def AddComponent(self, TargetComponent):
        """
        Adds a component to the store
        """

        self._componentSet.add(TargetComponent)
        animationNodeList = GetAnimationNodeList(TargetComponent)
        for animationNode in animationNodeList:
            self._animationNodeDict[animationNode] = None

    def RemoveComponent(self, TargetComponent):
        """
        Removes a component from the store
        """

        self._componentSet.add(TargetComponent)
        animationNodeList = GetAnimationNodeList(TargetComponent)
        for animationNode in animationNodeList:
            del self._animationNodeDict[animationNode]

    def Copy(self):
        """
        Copies animation from the components that are registered with the DataStore class
        """

        for animationNode in self._animationNodeDict.keys():
            self._animationNodeDict[animationNode] = animationNode.FCurve

    def Paste(self):
        """
        Pastes animation onto the components that are registered with the DataStore class from
        the previous Copy operation.
        """

        for animationNode in self._animationNodeDict.keys():
            if self._animationNodeDict[animationNode] != None:
                animationNode.FCurve = self._animationNodeDict[animationNode]


def SetProperties(component, force=False, **properties):
    """
    Sets values of properties to the given component. Properties and values are determined by the keyword
    arguments passed into this method. If the property doesn't exist and force is set, a property is created
    to set the data too.

    Arguments:
        component (FBComponent): or any object that inherits FBComponent; component to add properties too
        force (boolean): Create a new property to set data too if the property doesn't exist.
        **properties (dictionary): keyword arguments; the property and values to set on the component
    """

    getattrFind = [component.PropertyList.Find,
                   lambda property_name: getattr(component, property_name, None)]

    for propertyName, value in properties.iteritems():
        try:
            propertyName = propertyName.strip()
            property = None

            # Check that the property is available as a FBProperty or a normal attribute
            # on the component

            counter = 0
            while property is None and counter < 2:
                property = getattrFind[counter](propertyName)
                counter += 1

            # If the property doesn't exist and we don't want to force create, skip it
            if property is None and not force:
                continue

            # Create the missing property
            elif property is None:
                value_class = type(value)
                if isinstance(value, (list, tuple)) and all([isinstance(number, (int, float)) for number in value]):
                    value_class = getattr(mobu, "FBVector{}d".format(len(value)), str)

                property_type, default_value = FBPROPERTY_DICTIONARY.get(value_class, (FBPropertyType.kFBPT_charptr, ""))
                property = component.PropertyCreate(
                    propertyName, property_type, "", False, True, None)

            # Convert lists to the the object that piece of data expects to receive
            if isinstance(value, (list, tuple)) and all((isinstance(_, (int, float)) for _ in value)):

                if hasattr(property, "Data"):
                    value_class = property.Data.__class__
                else:
                    value_class = property.__class__

                try:
                    value = value_class(*value)
                except:
                    value = value_class(value)

            elif isinstance(value, dict):
                for _ in sorted(value.keys()):
                    SetProperties(component, force=force, **{propertyName: value[_]})
                continue

            # Sometimes properties aren't generated on objects, and the property method will return none
            # Also the only way to set data is to use property.Data = value
            # Trying to capture that property in a variable will just give you
            # the value as a completely different object

            if property is not None and getattr(property, "append", None):
                property.append(value)

            elif property is not None and ismethod(property):
                try:
                    property(value)
                except:
                    property(*value)

            elif property is not None and not isinstance(property, FBPropertyString) and hasattr(property, "EnumList") and isinstance(value, basestring):
                counter = 0
                while property.EnumList(counter) is not None:
                    if property.EnumList(counter) == value:
                        property.Data = counter
                        break
                    counter += 1

            elif property is not None and "Data" in dir(property):
                property.Data = value

            else:
                setattr(component, propertyName, value)

        except Exception, e:
            print "Could not set", propertyName, "to",  value, "on", component.Name, "\n", e
            if DEBUG:
                raise Exception, e

    mobu.FBSystem().Scene.Evaluate()


def GetAnimationHashFromList( componentList, animationHash = hashlib.md5() ):
    '''
    Returns a string hex hash from the animation data gathered from the passed component list.

    Arguments:
                animationHash - of type hashlib.md5()
                    example: animationHash = hashlib.md5()

                componentList - list of components you want the animation hash to be created from

    Note: very slow due to the fact it iterates over FCurve data
    '''
    #Iterate over takes
    for take in Globals.System.Scene.Takes:
        #Iterate over layers
        for layerNumber in xrange( Globals.System.CurrentTake.GetLayerCount()):

            #Select consecutive layer
            Globals.System.CurrentTake.GetLayer( layerNumber ).SelectLayer( True, True )

            #Find if any compoents have animation on this layer
            for component in componentList:

                #Check to see if it has animatable properties
                for node in RS.Utils.Scene.Component.GetAnimationNodeList( component ):
                    RS.Utils.Scene.AnimationNode.HashKeys( node, True, False, animationHash )

    return str( animationHash.hexdigest() )

def GetAnimationNodesKeyCount( componentList ):
    keyCount = 0
    #Iterate over takes
    for take in Globals.System.Scene.Takes:
        #Iterate over layers
        for layerNumber in xrange( Globals.System.CurrentTake.GetLayerCount() ):
            #Select consecutively each layer
            Globals.System.CurrentTake.GetLayer( layerNumber).SelectLayer( True, True )
            #Find if any compoents have animation on this layer
            for component in componentList:
                #Check to see if it has animatable properties
                for node in Scene.Component.GetAnimationNodeList( component ):
                    keyCount += node.KeyCount
    return keyCount


def IsDestroyed(component):
    """
    Checks if the object has been destroyed or is in the proccess of being destroyed ei. owned by undo

    Arguments:
        component (mobu.FBComponent): component to check

    Return:
        bool
    """
    return isinstance(component, tuple(unbind.unbound_wrapper.values())) \
           or component.GetObjectStatus(mobu.FBObjectStatus.kFBStatusOwnedByUndo) \
           or 'Unbound' in component.__class__.__name__ #Not really sure why this is needed as it
                                                        #should be caught by the first test but Kat said
                                                        #that it fixed a crash of some sort. Need bug number.