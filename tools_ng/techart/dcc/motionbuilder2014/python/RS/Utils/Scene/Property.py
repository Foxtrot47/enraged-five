'''
Usage:
    This module should only contain functionality related to working with FBProperty (or their sub types - such as FBPropertyAnimatable).    
Author:
    Ross George
'''

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils.Scene.AnimationNode
import RS.Globals

#General properties
Translation = "Lcl Translation"
Rotation = "Lcl Rotation"
Scale = "Lcl Scale"

#Camera properties
FOV = "FieldOfView"
Roll = "Roll"
DOFMultiplier = "DOF Multiplier"
DOFExpand = "DOF Expand"
DOFStrength = "DOF Strength"
DOFStrengthOverride = "DOF Strength Override"
DOFNearPlaneOverride = "DOF NearPlane Override"
DOFFarPlaneOverride = "DOF FarPlane Override"
DOFSimple = "Simple_DOF"
MotionBlur = "Motion_Blur"
Focus = "FOCUS"
SwitcherIndex = "Camera Index"

class PropertyMatch:
    '''
    Represents a match between two properties (for purposes for copying/transfering data)
    '''
    
    def __init__( self, SourceProperty, TargetProperty ):
        '''
        Create a property match between the passed properties
        '''
        
        self.SourceProperty = SourceProperty
        self.TargetProperty = TargetProperty
     
    def CopyAnimationData(self):
        '''
        Executes a animation copy operation if the property is animatable on current layer on current take    
        '''        

        if RS.Utils.Scene.Property.IsAnimatable(self.SourceProperty):
            RS.Utils.Scene.Property.CopyAnimationData( self.SourceProperty, self.TargetProperty )

    def CopyData(self):
        '''
        Executes a animation copy operation if the property is animatable on current layer on current take    
        '''        

        RS.Utils.Scene.Property.CopyData( self.SourceProperty, self.TargetProperty )
            
class _PropertyMotitorInternalData(object):
    def __init__(self, callback, extraArgs=None):
        super(_PropertyMotitorInternalData, self).__init__()
        
        self._callback = callback
        self._extraArgs = extraArgs or {}
        
    def Callback(self, *args):
        self._callback(*args, **self._extraArgs)


class PropertyMonitor:
    '''
    This class allows us to monitor changes on a property and be alerted when a change arrises.
    '''    

    def __init__( self ):
        '''
        Creates a new property monitor. Must be activated before it will start responding to property changes
        '''
        
        self._propertyMonitorDict = dict()
        self._evaluationManager = FBEvaluateManager()
        self._previousTime = RS.Globals.System.LocalTime
        
    def _onSynchronization( self, eventSource, event ):
        '''
        This is where we handle property changes from scrubbing the timeline
        '''
        
        #First we check to see if the time has changed since we last evaluated
        if RS.Globals.System.LocalTime != self._previousTime:
            self._previousTime = RS.Globals.System.LocalTime
            
            #Now go through each of our properties and check if we have had any changes
            for targetProperty in list(self._propertyMonitorDict):
                try:
                    if targetProperty.Data != targetProperty._previousData:
                        self._propertyMonitorDict[ targetProperty ].Callback( targetProperty, targetProperty._previousData )    
                        targetProperty._previousData = targetProperty.Data
                        
                #If something goes wrong we will stop monitoring this property. Ussually this happens when the property becomes Unbound.
                except Exception as e:
                    if( e.message == None or e.message == ""):
                        print "Exception occured in property monitor: {0}".format( str(e) )
                    else:
                        print "Exception occured in property monitor: {0}".format( e.message )
                    
                    del self._propertyMonitorDict[ targetProperty ]

    def _onConnectionDataNotify( self, eventSource, event ):
        '''
        This is where we handle user property changes from user changing controls via the UI
        '''
        
        #First check if the action we are dealing with is in fact a data change - kFBCandidated
        if event.Action == FBConnectionAction.kFBCandidated:
            
            #If the property (plug) is one we monitoring 
            if event.Plug in self._propertyMonitorDict:
                
                #Check for change and forward on
                try:
                    if event.Plug.Data != event.Plug._previousData:
                        self._propertyMonitorDict[ event.Plug ].Callback( event.Plug, event.Plug._previousData )    
                        event.Plug._previousData = event.Plug.Data
                        
                #If something goes wrong we will stop monitoring this property. In this context that is ussually due to the passed callback exceptioning
                except Exception as e:
                    if( e.message == None or e.message == ""):
                        print "Exception occured in property monitor: {0}".format( str(e) )
                    else:
                        print "Exception occured in property monitor: {0}".format( e.message )
                    
                    del self._propertyMonitorDict[ event.Plug ]                                     

    def Add( self, targetProperty, targetCallback , extraArgs=None):
        '''
        Add the target property to the monitor. When the property changes the target callback will be run.
        Extra args can be passed in, as a dict
        '''
        
        if not issubclass( type( targetProperty ), FBProperty ):
            raise Exception("Passed TargetProperty object is not an FBProperty") 
        if not callable( targetCallback ): 
            raise Exception("Passed TargetCallback is not Callable") 

        self._propertyMonitorDict[ targetProperty ] = _PropertyMotitorInternalData(targetCallback, extraArgs=extraArgs)
        targetProperty._previousData = targetProperty.Data
    
    def Remove( self, targetProperty ):
        '''
        Removes the target property.
        '''        

        if targetProperty in self._propertyMonitorDict:
            del self._propertyMonitorDict[targetProperty]
        
    def Clear( self ):
        '''
        Removes all properties from the monitor
        ''' 
        
        self._propertyMonitorDict.clear()
        
    def Deactivate( self ):
        '''
        Stops watching for changes
        ''' 
        
        self._evaluationManager.OnSynchronizationEvent.Remove( self._onSynchronization )
        RS.Globals.System.OnConnectionDataNotify.Remove( self._onConnectionDataNotify )
        
    def Activate( self ):
        '''
        Starts watching for changes
        ''' 
        
        self._evaluationManager.OnSynchronizationEvent.Add( self._onSynchronization )
        RS.Globals.System.OnConnectionDataNotify.Add( self._onConnectionDataNotify )

class PropertyLocker:
    '''
    This class allows us to lock a property to a certian value. WIP - still experimental.
    '''    

    def __init__( self ):
        '''
        Creates a new property locker.
        '''         

        self._componentMonitorDict = dict()
        self._propertyMonitor = PropertyMonitor() 

    def _validateValue( self, targetProperty, previousValue ):
        '''
        Ensures the value of the property is the one we want
        ''' 
        
        try:
            value = self._componentMonitorDict[targetProperty]
            if targetProperty.Data != value:
                targetProperty.Data = value
        except:
            print "Failed locking on", targetProperty.Name, type(targetProperty)
            self._propertyMonitor.Remove( targetProperty )
                  
    def Lock( self, targetProperty, value ):
        '''
        Locks a proprty to the given value
        ''' 
        
        self._componentMonitorDict[targetProperty] = value
        self._propertyMonitor.Add( targetProperty, self._validateValue )
        if targetProperty.AllowsLocking():
            targetProperty.SetLocked(True)

    def Unlock( self, targetProperty ):
        '''
        Unlocks a proprty
        ''' 
        
        if targetProperty in self._componentMonitorDict:
            del self._componentMonitorDict[targetProperty]
        self._propertyMonitor.Remove( targetProperty, self._validateValue )
        if targetProperty.AllowsLocking():
            targetProperty.SetLocked(False)        
        
    def Deactivate( self ):
        '''
        Stop watching for property changes - lifts the property locking.
        '''
        
        self._propertyMonitor.Deactivate()
        
    def Activate( self ):
        '''
        Starts watching for property changes and enforcing the values.
        '''                

        self._propertyMonitor.Activate()


def IsAnimatable( InputProperty ):
    '''
    Returns true if the passed property is animatable (i.e derives from FBPropertyAnimatable)
    '''    

    if issubclass( type(InputProperty), FBPropertyAnimatable ):
        return True
    else:
        return False


def IsSupportedCopyType( InputProperty ):
    '''
    Returns true if the passed property is of a type that can be used in the copy functions
    '''
    
    if type(InputProperty) in [ FBPropertyBool, 
                                FBPropertyDouble, 
                                FBPropertyFloat, 
                                FBPropertyInt, 
                                FBPropertyString, 
                                FBPropertyVector2d, 
                                FBPropertyVector3d , 
                                FBPropertyVector4d, 
                                FBPropertyColor, 
                                FBPropertyColorAndAlpha, 
                                FBPropertyTime,FBPropertyTimeCode ]:
        return True
    if issubclass( type( InputProperty ), FBPropertyAnimatable ):
        return True
    return False

'''
Copies data from one property to another. Will optionally work on multiple takes and animation layers. 
By default it will copy all animation from all takes across all layers.

DEPRECATED
'''
'''
def DeepCopyData( SourceProperty, TargetProperty, TargetTakeList = RS.Globals.Takes, TargetAnimationLayerIndexList = None ):
    formerTake = RS.Globals.System.CurrentTake
    
    for take in TargetTakeList:
        RS.Globals.System.CurrentTake = take
        formerAnimationLayerIndex = take.GetCurrentLayer()
        
        if TargetAnimationLayerIndexList == None:
            TargetAnimationLayerIndexList = range( take.GetLayerCount() )
        for animationLayerIndex in TargetAnimationLayerIndexList:
            take.SetCurrentLayer( animationLayerIndex )
            CopyData(SourceProperty, TargetProperty)
            
        take.SetCurrentLayer( formerAnimationLayerIndex )
            
    RS.Globals.System.CurrentTake = formerTake
'''   

def ClearAnimationData( PropertyList ):
    '''
    Given a propety list, clear all animation on the curves of the properties
    '''
    for propertyItem in PropertyList:
        #Test if the property is animatable
        if IsAnimatable( propertyItem ):
            #Remove all keys
            if propertyItem.GetAnimationNode():
                for node in propertyItem.GetAnimationNode().Nodes:
                    if node.FCurve:
                        node.FCurve.EditClear()
                    for axisNode in node.Nodes:
                        if axisNode.FCurve:
                            axisNode.FCurve.EditClear()                               
            
def CopyData( SourceProperty, TargetProperty):
    '''
    Copies data from one property to another. Only copys on the current take and the current animation layer.
    Note that it doesn't copy animation data (AnimationNode - FCurve data). Just transfers the data at the current 
    time of evaluation.
    '''
    
    if IsSupportedCopyType( SourceProperty ) and type(SourceProperty) == type(TargetProperty):
        
        #Set value
        if not TargetProperty.IsReadOnly():     
            TargetProperty.Data = SourceProperty.Data
    else:
        raise Exception("Property types don't match or is not a supported copy type (only some properties can be copied)")
    

def CopyAnimationData( SourceProperty, TargetProperty):
    '''
    Copies animation data from one property to another. Only copies on the current take on current animation layer
    '''
    
    if type(SourceProperty) == type(TargetProperty):
            
        #Check the property is animatable
        if IsAnimatable(SourceProperty):

            #Copy animation across
            if SourceProperty.IsAnimated():
                TargetProperty.SetAnimated( True )
                #Have to check that we have animation nodes even though we have just called
                #SetAnimated( True ). Might be a bug here where SetAnimated is not working.
                #This stops Mobu crashing in these cases.
                if SourceProperty.GetAnimationNode() and TargetProperty.GetAnimationNode():
                    RS.Utils.Scene.AnimationNode.CopyData( SourceProperty.GetAnimationNode(), 
                                                           TargetProperty.GetAnimationNode(), 
                                                           True)
            else:
                TargetProperty.SetAnimated( False )
        else:
            raise Exception("Property is not animatable so there would be no animation to copy")
    else:
        raise Exception("Property types don't match")
    
    
def GetAnimationNodeList( TargetProperty, Recursive = True):
    '''
    Returns animation node and all subanimation nodes of the passed property
    '''    

    animationNodeList = []
    if IsAnimatable( TargetProperty ):
        if TargetProperty.IsAnimated():
            baseAnimationNode = TargetProperty.GetAnimationNode()
            animationNodeList.append( baseAnimationNode )
            if Recursive:
                if baseAnimationNode:
                    subAnimationNodeList = RS.Utils.Scene.AnimationNode.GetSubAnimationNodeList( baseAnimationNode, Recursive )
                    animationNodeList.extend( subAnimationNodeList )
    return animationNodeList


def HasKeys( TargetProperty, Recursive = True):
    '''
    Returns true if the passed property has some keys
    '''    

    if IsAnimatable( TargetProperty ) and TargetProperty.IsAnimated():
        return RS.Utils.Scene.AnimationNode.HasKeys( TargetProperty.GetAnimationNode() ) 
    else:
        return False