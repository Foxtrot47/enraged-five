# Module for dealing with Relationship Constraints

import ast
import logging
import re
from xml.dom import minidom

from RS import Globals
from RS.Utils import Scene
from RS.Utils.Scene import Constraint

import pyfbsdk as mobu


# Function Box names and what categories they belong to
functionBoxes = {"Boolean": [
                        ("AND", None),
                        ("Flip Flop", None),
                        ("Memory (B1 when REC)", None),
                        ("Memory (last trigger)", None),
                        ("NAND", None),
                        ("NOR", None),
                        ("NOT", None),
                        ("OR", None),
                        ("XNOR", None),
                        ("XOR", None),
                    ],

                 "Converters": [
                        ("Deg To Rad", None),
                        ("HSB To RGB", None),
                        ("Number to Vector", None),
                        ("Number to Vector2", None),
                        ("Rad To Deg", None),
                        ("RGB To HSB", None),
                        ("RGB To RGBA", None),
                        ("RGB To Number", None),
                        ("RGBA To RGB", None),
                        ("Seconds to Time", None),
                        ("Time to seconds", None),
                        ("Time to TimeCode", None),
                        ("TimeCode to Time", None),
                        ("Vector to Number", None),
                        ("Vector2 to Number", None),
                    ],

                 "Macro Tools":[
                        ("Macro Input Bool", None),
                        ("Macro Input ColorAndAlpha", None),
                        ("Macro Input Number", None),
                        ("Macro Input Time", None),
                        ("Macro Input Vector", None),
                        ("Macro Output Bool", None),
                        ("Macro Output ColorAndAlpha", None),
                        ("Macro Output Number", None),
                        ("Macro Output Time", None),
                        ("Macro Output Vector", None),
                    ],

                "Number": [
                        ("Absolute (|a|)", None),
                        ("Add (a + b)", None),
                        ("arccos(a)", None),
                        ("arcsin(a)", None),
                        ("arctan(a)", None),
                        ("arctan2(b/a)", None),
                        ("Cosine cos(a)", None),
                        ("Damp", None),
                        ("Damp (Clock based)", None),
                        ("Distance Numbers", None),
                        ("Divide (a/b)", None),
                        ("exp(a)", None),
                        ("Exponent (a^b)", None),
                        ("IF Cond Then A Else B", None),
                        ("Integer", None),
                        ("Invert (1/a)", None),
                        ("Is Between A and B", (1, 0)),
                        ("Is Different (a != b)", None),
                        ("Is Greater (a > b)", None),
                        ("Is Greater or Equal (a >= b)", None),
                        ("Is Identical (a == b)", None),
                        ("Is Less (a < b)", None),
                        ("Is Less or Equal (a <= b)", None),
                        ("ln(a)", None),
                        ("log(a)", None),
                        ("Max Pass-thru", None),
                        ("Memory (a when REC)", None),
                        ("Min Pass-thru", None),
                        ("Modulo mod(a, b)", None),
                        ("Multiply (a x b)", None),
                        ("Precision Numbers", None),
                        ("Pull Number", None),
                        ("Scale And Offset (Number)", None),
                        ("Sine sin(a)", None),
                        ("sqrt(a)", None),
                        ("Subtract (a - b)", None),
                        ("Sum 10 numbers", None),
                        ("Tangeant tan(a)", None),
                        ("Triggered Delay (Number)", None),
                        ("Triggered Delay with Memory (Number)", None),
                    ],

                 "Other":[
                        ("Bezier Curve", None),
                        ("Damping (3D)", None),
                        ("Damping (3D) (Clock based)", None),
                        ("FCurve Number (%)", None),
                        ("FCurve Number (Time)", None),
                        ("Real Time Filter", None),
                        ("Triggered Plus Minus Counter", None),
                        ("Triggered Random", None),
                    ],

                "Rotation":[
                        ("Add (R1 + R2)", None),
                        ("Angle Difference (Points)", None),
                        ("Angle Difference (Rotations)", None),
                        ("Angular Acceleration", None),
                        ("Angular Speed", None),
                        ("Damp (Rotation)", None),
                        ("Global To Local", None),
                        ("Interpolate", None),
                        ("Local To Global", None),
                        ("Rotation Scaling", None),
                        ("Sensor Rotation Helper", None),
                        ("Subtract (R1 - R2)", None),
                        ("Three-Point Constraint", None),
                    ],

                 "Shapes": [
                        ("Select exclusive", None),
                        ("Select exclusive 24", None),
                        ("Shape calibration", None),
                    ],

                 "Sources": [
                        ("Counter with Play Pause", None),
                        ("Counter with Start Stop", None),
                        ("Half Circle Ramp", None),
                        ("Isoceles Triangle Ramp", None),
                        ("Pulse", None),
                        ("Random", None),
                        ("Right Triangle Ramp", None),
                        ("Sine Ramp", None),
                        ("Square Ramp", None),
                    ],

                 "System": [
                        ("Current Time", None),
                        ("Local Time", None),
                        ("Play Mode", None),
                        ("Reference Time", None),
                        ("System Time", None),
                        ("Transport Control", None),
                    ],

                 "Time": [
                        ("IF Cond Then T1 Else T2", None),
                        ("Is Different (T1 != T2)", None),
                        ("Is Greater (T1 > T2)", None),
                        ("Is Greater or Equal (T1 >= T2)", None),
                        ("Is Identical (T1 == T2)", None),
                        ("Is Less (T1 < T2)", None),
                        ("Is Less or Equal (T1 <= T2)", None),
                    ],

                 "Vector": [
                        ("Acceleration", None),
                        ("Add (V1 + V2)", None),
                        ("Angle", None),
                        ("Center of Mass", None),
                        ("Damp Position", None),
                        ("Derive", None),
                        ("Determinant", None),
                        ("Displacement", None),
                        ("Distance", None),
                        ("Dot Product (V1.V2)", None),
                        ("Gravity", None),
                        ("IF Cond Then A Else B", (1, 3)),
                        ("Is Different (V1 != V2)", None),
                        ("Is Identical (V1 == V2)", None),
                        ("Length", None),
                        ("Memory (V1 when REC)", None),
                        ("Middle Point", None),
                        ("Normalize", None),
                        ("Orbit Attraction", None),
                        ("Precision Vectors", None),
                        ("Pull Vector", None),
                        ("Scale (a x V)", None),
                        ("Scale And Offset (Vector)", None),
                        ("Scale Damping", None),
                        ("Speed", None),
                        ("Subtract (V1 - V2)", None),
                        ("Sum 10 vectors", None),
                        ("Triggered Delay (Vector)", None),
                        ("Triggered Delay with Memory (Vector)", None),
                        ("Vector Product (V1 x V2)", None),
                    ],
                }


def GetCategoryAndMethodForFunctionBox(inputBox):
    """
    Get the category and function of the given input box

    Args:
        inputBox (RelationShipConstraintBox): Input box to get

    Returns:
        string, string: The category and function names. or None, None if not found.
    """
    nodeName = inputBox.Name
    for catagory, data in functionBoxes.iteritems():
        for funcName, nodeData in data:
            regexString = r"\A{0} ?[0-9]*\Z".format(re.escape(funcName))
            if not re.search(regexString, nodeName, 0):
                continue
            if nodeData is None:
                return catagory, funcName
            else:
                reciverIdx, channelNum = nodeData
                propChanNum = len(inputBox.GetReceivers()[reciverIdx].GetProperty().Nodes)
                if channelNum == propChanNum:
                    return catagory, funcName
    return None, None


class _SerializeNode(object):
    """
    Internal Class

    This class is to help the xml writing of a relationship constraint box node
    """
    def __init__(self, node, idNum):
        """
        constructor

        Args:
            node (RelationShipConstraintBox): The box that seaialized uses
            idNum (int): The ID Number of the box. Should be unique to each node
        """
        self._node = node
        self._id = idNum
        self._methodHasOutputs = self._node.IsOnlySending()
        self._methodHasInputs = self._node.IsOnlyReceiving()
        self._local = False
        dynamicInputs = self._methodHasInputs
        self._dynamic = self._node.GetMobuObject() != None

        self._function = None
        self._method = None
        if not self._dynamic:
            self._function, self._method = GetCategoryAndMethodForFunctionBox(self._node)
        else:
            if dynamicInputs:
                self._method = "In"
            else:
                self._method = "Out"

            if self._node.IsModelBox():
                self._local = not self._node.GetBox().UseGlobalTransforms

    def Id(self):
        """
        Node ID Number

        Returns:
            int: The ID number of the node
        """
        return self._id

    def Node(self):
        """
        Return the Box

        Returns:
            RelationShipConstraintBox: The box that seaialized uses
        """
        return self._node

    def WriteXml(self, doc):
        """
        Create xml elements that represents the Node

        Args:
            doc (xml.dom.minidom.Document): The XML Document

        Returns:
            DOM Element: The newly created seaialized Node
        """
        newNode = doc.createElement("node")
        newNode.setAttribute("id", str(self._id))

        if self._dynamic:
            dynamicPart = doc.createElement("dynamic")
            dynamicPart.setAttribute("value", "True")
            dynamicPart.setAttribute("method", self._method)
            dynamicPart.setAttribute("localTransforms", str(self._local))
            newNode.appendChild(dynamicPart)

            namePart = doc.createElement("name")
            namePart.setAttribute("value", self._node.GetMobuObject().Name)
            newNode.appendChild(namePart)

        else:
            dynamicPart = doc.createElement("dynamic")
            dynamicPart.setAttribute("value", "False")
            newNode.appendChild(dynamicPart)

            functionPart = doc.createElement("function")
            functionPart.setAttribute("category", self._function)
            functionPart.setAttribute("method", self._method)
            newNode.appendChild(functionPart)

        xPos, yPos = self._node.GetBoxPosition()
        locationPart = doc.createElement("location")
        locationPart.setAttribute("xPos", str(xPos))
        locationPart.setAttribute("yPos", str(yPos))
        newNode.appendChild(locationPart)

        return newNode


class _SerializeConnection(object):
    """
    Internal Class

    This class is to help the xml writing of a node connection
    """
    def __init__(self, outId, outProperty, inId, inProperty):
        """
        Constructor

        Args:
            outId (int): Out node ID
            outProperty (string): Out property Name
            inId (int): In node ID
            inProperty (string): In property Name
        """
        self._outId = outId
        self._outProperty = outProperty
        self._inId = inId
        self._inProperty = inProperty

    def WriteXml(self, doc):
        """
        Create xml elements that represents the connection

        Args:
            doc (xml.dom.minidom.Document): The XML Document

        Returns:
            DOM Element: The newly created seaialized connection
        """
        newNode = doc.createElement("connection")

        # Out Connection Data
        outPart = doc.createElement("outOf")
        outPart.setAttribute("id", str(self._outId))
        outPart.setAttribute("property", self._outProperty)
        newNode.appendChild(outPart)

        # In Connection Data
        inPart = doc.createElement("inTo")
        inPart.setAttribute("id", str(self._inId))
        inPart.setAttribute("property", self._inProperty)
        newNode.appendChild(inPart)

        return newNode


class _SerializeValues(object):
    """
    Internal Class

    This class is to help the xml writing of a node value
    """
    def __init__(self, boxId, boxProperty, value):
        """
        Constructor

        Args:
            boxId (int): Out node ID
            boxProperty (string): Out property Name
            value (list): Values set to the box property
        """
        self._boxId = boxId
        self._boxProperty = boxProperty
        self._value = value

    def WriteXml(self, doc):
        """
        Create xml elements that represents the value

        Args:
            doc (xml.dom.minidom.Document): The XML Document

        Returns:
            DOM Element: The newly created seaialized value
        """
        newNode = doc.createElement("setValue")

        newNode.setAttribute("nodeId", str(self._boxId))
        newNode.setAttribute("property", self._boxProperty)

        propType = "float"
        if len(self._value) == 3:
            propType = "vector3d"
        elif len(self._value) == 4:
            propType = "color"
        newNode.setAttribute("type", propType)

        for value in self._value:
            valuePart = doc.createElement("valueData")
            valuePart.setAttribute("value", str(value))
            newNode.appendChild(valuePart)
        return newNode


class _SerializeFCurve(object):
    """
    Internal Class

    This class is to help the xml writing of a node's FCurve
    """
    def __init__(self, boxId, curve):
        """
        Constructor

        Args:
            boxId (int): Out node ID
            curve (FBFCurve): The animation Curve to store
        """
        self._boxId = boxId
        self._curve = curve

    def WriteXml(self, doc):
        """
        Create xml elements that represents the connection

        Args:
            doc (xml.dom.minidom.Document): The XML Document

        Returns:
            DOM Element: The newly created seaialized curve data
        """
        newNode = doc.createElement("curveData")

        newNode.setAttribute("nodeId", str(self._boxId))
        for key in self._curve.Keys:
            keyData = doc.createElement("keyData")
            keyData.setAttribute("value", str(key.Value))
            keyData.setAttribute("time", str(key.Time.Get()))
            keyData.setAttribute("bias", str(key.Bias))
            keyData.setAttribute("continuity", str(key.Continuity))
            keyData.setAttribute("interpolation", str(key.Interpolation.real))
            keyData.setAttribute("tension", str(key.Tension))

            keyData.setAttribute("leftBezierTangent", str(key.LeftBezierTangent))
            keyData.setAttribute("leftDerivative", str(key.LeftDerivative))
            keyData.setAttribute("leftTangentWeight", str(key.LeftTangentWeight))

            keyData.setAttribute("rightBezierTangent", str(key.RightBezierTangent))
            keyData.setAttribute("rightDerivative", str(key.RightDerivative))
            keyData.setAttribute("rightTangentWeight", str(key.RightTangentWeight))

            keyData.setAttribute("tangentClampMode", str(key.TangentClampMode.real))
            keyData.setAttribute("tangentBreak", str(key.TangentBreak))
            keyData.setAttribute("tangentConstantMode", str(key.TangentConstantMode.real))
            keyData.setAttribute("tangentMode", str(key.TangentMode.real))
            newNode.appendChild(keyData)
        return newNode


class RelationShipConstraintProperty(object):
    """
    The Relationship Constraint Property

    is created from a sender or reciver on a RelationShipConstraintBox.
    """
    def __init__(self, propertyNode, parent=None):
        """
        Constructor

        Args:
            propertyNode (FBAnimationNode): the property to wrap

        Kwargs:
            parent (RelationShipConstraintBox): the relationship box that contains the property
        """
        super(RelationShipConstraintProperty, self).__init__()

        self._propertyNode = propertyNode
        self._parent = parent

    @property
    def Name(self):
        """
        Property

        Get the name of the property

        Return:
            String: property Name
        """
        name = self._propertyNode.Name
        if name == "":
            return self._propertyNode.UserName
        return self._propertyNode.Name

    def GetName(self):
        """
        Get the name of the property

        Return:
            String: property Name
        """
        return self.Name

    def GetProperty(self):
        """
        Get the Motion Builder object (FBAnimationNode) for the property

        Return:
            FBAnimationNode: Which is the property being wrapped
        """
        return self._propertyNode

    def SetValue(self, value):
        """
        Set the value for the node, will convert from different types to a list internally.

        Args:
            value (object): Object to set as the value. Values will be converted to correct type

        Return:
            bool if the operation was successful
        """
        setValue = []
        if isinstance(value, list) or isinstance(value, tuple):
            setValue = value
        elif isinstance(value, int) or isinstance(value, float):
            setValue = [value]
        elif isinstance(value, mobu.FBVector3d) or isinstance(value, mobu.FBColor):
            setValue = [value[0], value[1], value[2]]
        elif isinstance(value, mobu.FBVector4d):
            setValue = [value[0], value[1], value[2], value[3]]
        else:
            setValue = [0]

        return bool(self._propertyNode.WriteData(setValue))

    def GetValue(self, raw=False):
        """
        Get the value for the node, can convert from raw form to data format, or return as raw

        # Only in Mobu 2016 Custom Cut

        Args:
            raw (bool): If the data returned should be converted or returned as an list

        Return:
            object of data, either float, int, FBVector3d or FBColor
        """
        if not hasattr(self._propertyNode, "ReadData"):
            logging.warning("ReadData is only in motionbuilder 2016 Custom Cut")
            return None
        data = self._propertyNode.ReadData()
        if raw:
            return data
        if len(data) == 1:
            return data[0]
        elif len(data) == 3:
            return mobu.FBVector3d(data[0], data[1], data[2])
        elif len(data) == 4:
            return mobu.FBColor(data[0], data[1], data[2], data[3])
        return data

    def Parent(self):
        """
        Get the Relationship box (RelationShipConstraintBox) that is the parent of the property

        Return:
            RelationShipConstraintBox: The Box which the constraint is part of
        """
        return self._parent

    def ConnectProperty(self, propertyToConnect):
        """
        Args:
            propertyToConnect (RelationShipConstraintProperty): The Receiving connection to connect to

        Connect THIS property to the GIVEN property
        The GIVEN property must be a recivier and THIS must be a sender

        If the connection is made the wrong way around, motion builder will return the connection
        value as True, even when the connection hasnt been made.

        Return:
            bool: if the connection has been made or not
        """
        return mobu.FBConnect(self._propertyNode, propertyToConnect.GetProperty())

    def IsHardDriven(self):
        """
        Gets if the property is being driven by a hard coded value or if it is either not connected
        or conected to another property

        Return:
            bool: if the property is being driven by a hard coded value or not
        """
        if self._propertyNode.GetSrcCount() != 0:
            return False
        if not self._parent.IsOnlyReceiving():
            return True
        val = self.GetValue()
        if val is None:
            return False

        if val == False or val == 0.0 or val == mobu.FBVector3d(0, 0, 0):
            return False
        return True

    def GetConnection(self):
        """
        Get the RelationShipConstraintProperty that is connected to this one as a list, or None
        if nothing is connected to it

        Return:
            list of RelationShipConstraintProperty: All the connections that the property is connected to
        """
        if self._propertyNode.GetSrcCount() > 0:
            prop = self._propertyNode.GetSrc(0)
            parentBox = RelationShipConstraintBox(self._propertyNode.GetSrc(0).GetOwner())
            return [RelationShipConstraintProperty(prop, parentBox)]

        if self._propertyNode.GetDstCount() > 0:
            returnList = []
            for idx in xrange(self._propertyNode.GetDstCount()):
                prop = self._propertyNode.GetDst(idx)
                parentBox = RelationShipConstraintBox(self._propertyNode.GetDst(idx).GetOwner())
                returnList.append(RelationShipConstraintProperty(prop, parentBox))
            return returnList
        return None


class RelationShipConstraintBox(object):
    """
    The Relationship Constraint Box

    This represents a box in the Relationship Constraint Graph Editor, and contains properties that can be connected
    """
    def __init__(self, box, parent=None):
        """
        Constructor

        Args:
            box (FBBox): The Box to wrap the class with

        Kwargs:
            parent (FBConstraintRelation): The part of the box
        """
        super(RelationShipConstraintBox, self).__init__()
        self._box = box
        self._parent = parent or box.GetOwner()

    def Parent(self):
        """
        Get the Relationship Constraint (FBConstraintRelation) that the box is part of.

        Return:
            FBConstraintRelation: The part of the box
        """
        return self._parent

    def GetBox(self):
        """
        Get the FBBox that is wrapped

        Return:
            FBBox: The motion builder component of the box
        """
        return self._box

    @property
    def Name(self):
        """
        Return the Name of the constraint box

        Return:
            String: property Name
        """
        return self._box.Name

    def GetName(self):
        """
        Return the Name of the constraint box

        Get:
            String: property Name
        """
        return self.Name

    def SetName(self, newName):
        """
        Set the Name of the constraint box
        Takes a string

        Args:
            newName (string): The new name of the box
        """
        self._box.Name = newName

    def GetFCurve(self):
        """
        Gets the FCurve of the box, if there is one

        Return:
            the FCurve of the box, if there is one.
        """
        valueBox = [node for node in self._box.AnimationNodeOutGet().Nodes if node.Name == "Value"]
        if len(valueBox) != 1:
            return None
        return valueBox[0].FCurve

    def IsOnlyReceiving(self):
        """
        Is the Box only receiving data (does not send data to any other boxes)

        Return:
            Bool: if the box is only receiving data
        """
        return  (len(self.GetSenders()) == 0 and len(self.GetReceivers ()) > 0) or \
                (len(self.GetSendersBoxes()) == 0 and len(self.GetReceiversBoxes()) > 0)

    def IsOnlySending(self):
        """
        Is the Box only sending data (does not Receiving data to any other boxes)

        Return:
            Bool: if the box is only sending data
        """
        return  (len(self.GetSenders()) > 0 and len(self.GetReceivers ()) == 0) or \
                (len(self.GetSendersBoxes()) > 0 and len(self.GetReceiversBoxes()) == 0)

    def GetSenders(self):
        """
        Get all the sending properties that send data from them

        Return:
            list of RelationShipConstraintProperty: All the properties that send data
        """
        return [RelationShipConstraintProperty(prop, self) for prop in self._GetSendersProperties()]

    def GetReceivers(self):
        """
        Get all the receiving properties that gets data from other properties

        Return:
            list of RelationShipConstraintProperty: All the properties that Receive data
        """
        return [RelationShipConstraintProperty(prop, self) for prop in self._GetReceiversProperties()]

    def _GetReceiversProperties(self):
        """
        Internal Method

        Get all the receiving properties that receive data from them, and filters out any of the
        properties which are driving the actual node properties

        Return:
            list of FBAnimationNodes: The receiving properties
        """
        results = []
        for prop in self._box.AnimationNodeInGet().Nodes:
            if prop.GetSrcCount() == 0:
                results.append(prop)
            elif not isinstance(prop.GetSrc(0).GetOwner(), mobu.FBConstraintRelation):
                results.append(prop)
        return results

    def _GetSendersProperties(self):
        """
        Internal Method

        Get all the sending properties that send data from them, and filters out any of the
        properties which are driving the actual node properties

        Return:
            list of FBAnimationNodes: The sending properties
        """
        results = []
        for prop in self._box.AnimationNodeOutGet().Nodes:
            if prop.GetDstCount() == 0:
                results.append(prop)
            elif not isinstance(prop.GetDst(0).GetOwner(), mobu.FBConstraintRelation):
                results.append(prop)
        return results

    def GetSenderNames(self):
        """
        Get a list of all the names of the sending properties that send data from them

        Return:
            list of Strings: All the names of the sending properties
        """
        return [prop.LongName or prop.Name for prop in self._GetSendersProperties()]

    def GetReceiversNames(self):
        """
        Get a list of all the names of the receiving properties that gets data from other properties

        Return:
            list of Strings: All the names of the receiving properties
        """
        return [prop.LongName or prop.Name for prop in self._GetReceiversProperties()]

    def GetSenderByName(self, propName):
        """
        Get a sending property from a given name

        Args:
            propName (string): The name of the property to find

        Return:
            RelationShipConstraintProperty/None: The property of the correct name if it can be found
        """
        for prop in self._GetSendersProperties():
            if prop.LongName or prop.Name == propName:
                return RelationShipConstraintProperty(prop, self)
        return None

    def GetReceiverByName(self, propName):
        """
        Get a receiving property from a given name

        Args:
            propName (string): The name of the property to find

        Return:
            RelationShipConstraintProperty/None: The property of the correct name if it can be found
        """
        for prop in self._GetReceiversProperties():
            if prop.LongName or prop.Name == propName:
                return RelationShipConstraintProperty(prop, self)
        return None

    def GetSendersByRegex(self, regexString, caseSensitive=False):
        """
        Get any sending properties that meet the regex string given
        Can be case sensitive.

        Args:
            regexString (string): The regex string to use to find sender properties

        Kwargs:
            caseSensitive (bool) : If the regex search is case sensitive

        Return:
            list of RelationShipConstraintProperty: The found property which meet the regex name
        """
        props = []

        flags = 0
        if caseSensitive:
            flags += re.I

        for prop in self._GetSendersProperties():
            if re.search(regexString, prop.LongName or prop.Name, flags) is not None:
                props.append(prop)

        return [RelationShipConstraintProperty(prop, self) for prop in props]

    def GetReceiversByRegex(self, regexString, caseSensitive=False):
        """
        Get any receiving properties that meet the regex string given
        Can be case sensitive.

        Args:
            regexString (string): The regex string to use to find receivers properties

        Kwargs:
            caseSensitive (bool) : If the regex search is case sensitive

        Return:
            list of RelationShipConstraintProperty: The found property which meet the regex name
        """
        props = []

        flags = 0
        if caseSensitive:
            flags += re.I

        for prop in self._GetReceiversProperties():
            if re.search(regexString, prop.LongName or prop.Name, flags) is not None:
                props.append(prop)

        return [RelationShipConstraintProperty(prop, self) for prop in props]

    def GetSendersBoxes(self):
        """
        Gets a dictionary of RelationShipConstraintBox that are sending data from this node,
        and indexed by their property names

        Return:
            Dict: Name and RelationShipConstraintBox of all sending properties
        """
        inPutters = {}
        boxNodeOwner = self.Name
        animNodes = self._GetSendersProperties()
        for animNode in animNodes:
            for i in xrange(0, animNode.GetDstCount()):
                animSource = animNode.GetDst(i)
                if animSource == None:
                    continue
                animSourceObj = animSource.GetOwner()
                if animSourceObj.LongName != boxNodeOwner and animSourceObj != self._parent:
                    inPutters[animNode.LongName] = RelationShipConstraintBox(animNode.GetOwner(), self._parent)
        return inPutters

    def GetInputDrivers(self, carryOverList=None):
        """
        This is a recursive method
        This will get the driving box that drives the box.

        Given an output node, this will return a list of input nodes driving it.

        Kwargs:
            carryOverList (list) : An internal list to carry over to get all the drivers

        Return:
            List of RelationShipConstraintBox: all the sender only boxes which drive this
        """
        if carryOverList is None:
            carryOverList = []
        if self.IsOnlySending():
            if self not in carryOverList:
                carryOverList.append(self)
        for sender in self.GetReceiversBoxes().itervalues():
            sender.GetInputDrivers(carryOverList=carryOverList)
        return carryOverList

    def IsFunctionBox(self):
        """
        Gets if the box is a function box or not

        Return:
            Bool: If the box is a fucntion box
        """

        return self.GetMobuObject() == None

    def IsModelBox(self):
        """
        Gets if the box is an FBModel box or not

        Return:
            Bool: If the box is an FBModel box
        """
        try:
            self._box.Model
            return True
        except:
            pass
        return False

    def IsConstraintBox(self):
        """
        Gets if the box is an FBConstraint box or not

        Return:
            Bool: If the box is an FBConstraint box
        """
        try:
            self._box.Model
        except:
            try:
                self._box.Box
                return True
            except:
                pass

        return False

    def GetMobuObject(self):
        """
        Get the Motion Builder object that is driving, or driven by the FBBox object

        Return:
            FBObject: The motion builder object that is driving the box
        """
        try:
            return self._box.Model
        except:
            try:
                return self._box.Box
            except:
                pass

        return None

    def ReplaceMobuObject(self, newMobuObject):
        """
        Set the Motion Builder object that is driving, or driven by the FBBox object

        Args:
            newMobuObject (FBObect): The new object to set as the box driver
        """
        # Generic stuff that will be helpful later on
        oldBox = self._box
        allReceivers = self.GetReceivers ()
        allSenders = self.GetSenders()
        xPos, yPos = self.GetBoxPosition()

        if self.IsOnlyReceiving():
            self._box = self.Parent().ConstrainObject(newMobuObject)
            self.SetBoxPosition(xPos, yPos)

            for connection in allReceivers :
                connections = connection.GetConnection()

                if connections is None:
                    continue

                # Only deal with the first Item as a Receiver cant accept more than one connection
                newRecivier = self.GetReceiverByName(connection.LongName)
                connections[0].ConnectProperty(newRecivier)
            oldBox.FBDelete()

        elif self.IsOnlySending():
            self._box = self.Parent().SetAsSource(newMobuObject)
            self.SetBoxPosition(xPos, yPos)

            for connection in allSenders:
                connections = connection.GetConnection()

                if connections is None:
                    continue

                for linkage in connections:
                    newSender = self.GetSenderByName(connection.LongName)
                    newSender.ConnectProperty(linkage)
            oldBox.FBDelete()
        else:
            self._box.Model = newMobuObject

    def GetBoxPosition(self):
        """
        Get the position of the box in the relationship constraint settings graph panel

        Return:
            Tuple of Floats: The X Position and Y Position of the box
        """
        _, xPos, yPos = self.Parent().GetBoxPosition(self._box)
        return xPos, yPos

    def SetBoxPosition(self, xPos, yPos):
        """
        Set the position of the box in the relationship constraint settings graph panel in X and Y

        Args:
            xPos (int): The X position of the box
            yPos (int): The Y position of the box
        """
        self.Parent().SetBoxPosition(self._box, xPos, yPos)

    def _ResolveSrc(self, animNode):
        """
        Internal Method

        Will resolve the source of the animNode given, this is a workaround as copying constraints
        leave srcs pointing to their orginal counterparts

        Args:
            animNode (FBAnimationNode): The anim node to resolve

        Return:
            FBAnimationNode: The final resolved node
        """
        if animNode.GetSrcCount() == 0:
            return animNode
        return self._ResolveSrc(animNode.GetSrc(0))

    def GetReceiversBoxes(self):
        """
        Gets a dictionary of RelationShipConstraintBox that are receiving data from other nodes,
        and indexed by their property names

        Return:
            Dict: Name and RelationShipConstraintBox of all receiving properties
        """
        outPutters = {}
        boxNodeOwner = self.Name
        animNodes = self._GetReceiversProperties()
        for animNode in animNodes:
            for idx in xrange(animNode.GetSrcCount()):
                animSource = animNode.GetSrc(idx)
                if animSource == None:
                    continue
                animSourceObj = animSource
                animSourceOwner = animSourceObj.GetOwner()
                if isinstance(animSourceOwner, mobu.FBModelPlaceHolder):
                    animSourceOwner = animSourceOwner.Box
                if animSourceOwner.LongName == boxNodeOwner or isinstance(animSourceOwner, mobu.FBConstraintRelation):
                    continue
                outPutters[animNode.LongName] = RelationShipConstraintBox(animSource.GetOwner(), self._parent)
        return outPutters

    def GetGlobalTransforms(self, value):
        """
        Get if the box sends or receives global transforms

        Return:
            Bool: If the transforms are expressed in Local or Global space
        """
        self._box.UseGlobalTransforms = value

    def SetGlobalTransforms(self, value):
        """
        Sets the box to send or receive global transforms

        Args:
            value (bool): if the transforms are in Global space
        """
        self._box.UseGlobalTransforms = value

    def SetReceiverValueByName(self, name, value):
        """
        Set the value for the named receiver, will convert from different types to a list internally.

        Args:
            name (str): the name of the receiver node to set
            value (object): Object to set as the value. Values will be converted to correct type

        Return:
            bool if the operation was successful
        """
        box = self.GetReceiverByName(name)
        if box is None:
            raise ValueError("Unable to find Receiver named '{0}'".format(name))
        return box.SetValue(value)

    def __eq__(self, other):
        """
        Equality Operator

        Args:
            other (RelationShipConstraintBox): The object to compare to this one

        Return:
            bool: If the two objects are the same, or wrapping the same FBBox item
        """
        if not isinstance(other, RelationShipConstraintBox):
            return False
        return self._box == other._box

    def __ne__(self, other):
        """
        Non-Equality Operator

        Args:
            other (RelationShipConstraintBox): The object to compare to this one

        Return:
            bool: If the two objects are the not same, or wrapping the same FBBox item
        """
        return not self == other


class RelationShipConstraintManager(object):
    """
    The RelationShip Constraint Manager

    This class wraps the FBConstraintRelation class, and provides easy way to introspect the
    constraint and edit it
    If no constraint is passed in, this class will create one and can take a name property
    """
    def __init__(self, constraint=None, name=None):
        """
        Constructor

        Kwargs:
            constraint (FBConstraintRelation): The constraint to wrap
            name (string):  Name of the Constraint if creating new Constraint
        """
        super(RelationShipConstraintManager, self).__init__()
        if constraint is None:
            constraint = mobu.FBConstraintRelation(name)
        self._constraintObj = constraint

    def GetConstraint(self):
        """
        Get the Relationship Constraint that is being used (FBConstraintRelation)

        Return:
            FBConstraintRelation: The underlying motion builder constraint that is being wrapped
        """
        return self._constraintObj

    def GetBoxes(self):
        """
        Get a list of all the boxes in the Relationship Constraint as RelationShipConstraintBox

        Return:
            list of RelationShipConstraintBox: all the boxes in the constraint
        """
        return [self.GetBoxByName(box.Name) for box in self._constraintObj.Boxes]

    def GetBoxByName(self, boxName):
        """
        Get a box from the Relationship Constraint from its name

        Args:
            boxName (string): Name of the box to get from the constraint

        Return:
            RelationShipConstraintBox: The box which matches the given name
        """
        return self._getBox(boxName)

    def GetBoxesByRegex(self, regexString, caseSensitive=False):
        """
        Get any Boxes that meet the regex string given
        Can be case sensitive.

        Args:
            regexString (string): The regex string to use to find boxes

        Kwargs:
            caseSensitive (bool) : If the regex search is case sensitive

        Return:
            list of RelationShipConstraintBox: The found box which meet the regex name
        """
        boxes = []

        flags = 0
        if caseSensitive:
            flags += re.I

        for box in self._constraintObj.Boxes:
            if re.search(regexString, box.Name, flags) is not None:
                boxes.append(box)

        return [self._getBox(None, box=box) for box in boxes]

    def AddSenderBox(self, mobuObject):
        """
        Adds the given Motion Builder object as a sender (only sends data) and returns the resulting
        box as a RelationShipConstraintBox

        Args:
            mobuObject (FBObject): The motion builder object to drive the new sender box

        Return:
            RelationShipConstraintBox: The newly created box
        """
        return self._getBox(None, box=(self._constraintObj.SetAsSource(mobuObject)))

    def AddReceiverBox(self, mobuObject):
        """
        Adds the given Motion Builder object as a receiver (only receivers data) and returns the resulting
        box as a RelationShipConstraintBox

        Args:
            mobuObject (FBObject): The motion builder object to drive the new receiver box

        Return:
            RelationShipConstraintBox: The newly created box
        """
        return self._getBox(None, box=(self._constraintObj.ConstrainObject(mobuObject)))

    def AddFunctionBox(self, boxCategoryName, nameOfBox):
        """
        Add the named function box from the box catagory to the relationship constraint. Returns the newly
        created RelationShipConstraintBox object

        Args:
            boxCategoryName (string): Name of the function box category
            nameOfBox (string): Name of the function to add

        Return:
            RelationShipConstraintBox: The newly created function box
        """
        functionBox = self._constraintObj.CreateFunctionBox(boxCategoryName, nameOfBox)
        if functionBox is None:
            raise ValueError("Unable to find '{0}' under category named '{1}'".format(nameOfBox, boxCategoryName))
        return self._getBox(None, box=functionBox)

    def ConnectByNames(self, nameOfOutputBox, nameOfOutputProperty, nameOfInputBox, nameOfInputProperty):
        """
        Connect two boxes together given the name of the boxes and the name of the properties
        Will Return True if the connection was sucessful, or False if it was not.
        Will raise if any of the boxes or properties do not exist

        Args:
            nameOfOutputBox (string): Name of the output box
            nameOfOutputProperty (string): Name of the output box's property to connect from
            nameOfInputBox (string): Name of the input box
            nameOfInputProperty (string): Name of the input box's property to connect to

        Return:
            Bool: If the two properties were connected
        """
        outBox = self.GetBoxByName(nameOfOutputBox)
        if outBox is None:
            raise ValueError("No Box Called '{0}'".format(nameOfOutputBox))
        inBox = self.GetBoxByName(nameOfInputBox)
        if inBox is None:
            raise ValueError("No Box Called '{0}'".format(nameOfInputBox))

        return self.ConnectBoxes(outBox, nameOfOutputProperty, inBox, nameOfInputProperty)

    def ConnectBoxes(self, outBox, nameOfOutputProperty, inBox, nameOfInputProperty):
        """
        Connect two boxes together given two RelationShipConstraintBox and the name of the properties
        Will Return True if the connection was sucessful, or False if it was not.
        Will raise if any of the properties do not exist

        Args:
            outBox (RelationShipConstraintBox): The output box
            nameOfOutputProperty (string): Name of the output box's property to connect from
            inBox (RelationShipConstraintBox): The input box
            nameOfInputProperty (string): Name of the input box's property to connect to

        Return:
            Bool: If the two properties were connected
        """
        outProp = outBox.GetSenderByName(nameOfOutputProperty)
        if outProp is None:
            raise ValueError("'{0}' has no property Called '{1}'".format(outProp.Name, nameOfOutputProperty))
        inProp = inBox.GetReceiverByName(nameOfInputProperty)
        if inProp is None:
            raise ValueError("'{0}' has no property Called '{1}'".format(inProp.Name, nameOfInputProperty))

        return outProp.ConnectProperty(inProp)

    def GetBoxesByNode(self, mobuObject):
        """
        Gets all the boxes in the relationship constraint driven or driving the given motion builder object

        Args:
            mobuObject (FBObject): The motion builder object to use to find any boxes being driven by it

        Return:
            list of RelationShipConstraintBox: All the boxes which are driven by the given node
        """
        boxes = [box.Model for box in self._constraintObj.Boxes if isinstance(box, mobu.FBModelPlaceHolder) and box.Model is mobuObject]
        return [self._getBox(None, box=box) for box in boxes]

    def _getBox(self, boxName, box=None):
        """
        Internal Method

        Get a box from its name, or wrap a box object as a RelationShipConstraintBox

        Args:
            boxName (string): The name of the box to get, if the box kwarg is not given

        Kwarg:
            box (FBBox): The motion builder box object to wrap

        Return:
            RelationShipConstraintBox: The wrapped box object
        """
        if box is None:
            for bx in self._constraintObj.Boxes:
                if boxName == bx.Name:
                    box = bx
                    break

        if box is None:
            return None

        return RelationShipConstraintBox(box, self._constraintObj)

    @property
    def Name(self):
        """
        Gets the name of the Relationship Constraint

        Return:
            String: property Name
        """
        return self._constraintObj.Name

    def GetName(self):
        """
        Gets the name of the Relationship Constraint

        Return:
            String: property Name
        """
        return self.Name

    def SetName(self, newName):
        """
        Set the Name of the Relationship Constraint

        Args:
            newName (string): New name for the relationship constraint
        """
        self._constraintObj.Name = str(newName)

    def GetActive(self):
        """
        Gets if the constraint is active or not

        Return:
            Bool: If the constraint is active
        """
        return self._constraintObj.Active

    def SetActive(self, value):
        """
        Set if the constraint is active or not

        Args:
            value (bool): If the new constraint is active or not
        """
        self._constraintObj.Active = value

    def GetSendingOnlyBoxes(self):
        """
        Returns a list of (RelationShipConstraintBox) of all boxes which are only sending data from them.
        They have no Receiving properties

        Return:
            list of RelationShipConstraintBox: All the boxes which are only sending data, and have no receiving properties
        """
        inputsOnly = set()
        for box in self.GetBoxes():
            if box.IsOnlySending():
                inputsOnly.add(box)
        return list(inputsOnly)

    def GetRecievingOnlyBoxes(self):
        """
        Returns a list of (RelationShipConstraintBox) of all boxes which are only receiving data from them.
        They have no Sending properties

        Return:
            list of RelationShipConstraintBox: All the boxes which are only receiving data, and have no sending properties
        """
        outputsOnly = set()
        for box in self.GetBoxes():
            if box.IsOnlyReceiving():
                outputsOnly.add(box)
        return list(outputsOnly)

    def _traverseRelationshipGraph(self):
        """
        Internal Method

        Method to get all the nodes and connections in the current relationship graph, and cast
        them as a serializeable class

        Returns:
            list of _SerializeNode, list of _SerializeConnection list of _SerializeValues: the nodes, connections, values and curves
        """
        nodeDict = {}
        connections = []
        values = []
        fcurves = []
        idx = 0
        for box in self.GetBoxes():
            nodeDict[box.GetBox()] = _SerializeNode(box, idx)
            idx += 1

        for box in nodeDict.values():
            inBox = box.Node()
            inId = box.Id()
            curve = inBox.GetFCurve()
            if curve is not None:
                fcurves.append(_SerializeFCurve(inId, curve))
            for prop in inBox.GetReceivers():
                inProperty = prop.Name

                hasDrivenValue = prop.IsHardDriven()
                if hasDrivenValue:
                    val = prop.GetValue(raw=True)
                    values.append(_SerializeValues(inId, inProperty, val))
                    continue

                con = prop.GetConnection()
                if con is None:
                    continue

                outId = nodeDict[con[0].Parent().GetBox()].Id()
                outProperty = con[0].Name

                if inId == outId:
                    continue

                connections.append(_SerializeConnection(outId, outProperty, inId, inProperty))

        return nodeDict.values(), connections, values, fcurves

    def SaveConstaintToFile(self, filePath):
        """
        Save the Relationship Constraint network to an XML file on disk

        Args:
            filePath (string): The file path to save out to
        """
        nodes, connections, values, curves = self._traverseRelationshipGraph()

        fileHandle = open(filePath, "wb")
        implementation = minidom.getDOMImplementation()
        baseDoc = implementation.createDocument(None, "constraint", None)
        baseDocElement = baseDoc.documentElement

        for node in nodes:
            baseDocElement.appendChild(node.WriteXml(baseDoc))

        for connection in connections:
            baseDocElement.appendChild(connection.WriteXml(baseDoc))

        for value in values:
            baseDocElement.appendChild(value.WriteXml(baseDoc))

        for curve in curves:
            baseDocElement.appendChild(curve.WriteXml(baseDoc))

        fileHandle.write(baseDoc.toprettyxml())
        fileHandle.close()

    def _buildBoxFromDict(self, boxDict, namespace=None):
        """
        Internal Method

        This will build a RelationshipConstraintBox from a XML dictionary and return it

        Examples:

        # An input box dict
        boxDict = {
                    "dynamic":
                            {"localTransforms":"False", "method":"In", "value":"True"},
                    "name":
                            {"value":"Cube 1"},
                    "location":
                            {"xPos":"1177", "yPos":"159"}
                    }

        # A function box dict
        boxDict = {
                    "dynamic":
                            {"value":"False"},
                    "function":
                            {"category":"Converters", "method":"Vector to Number"},
                    "location":
                            {"xPos":"340", "yPos":"322"}
                    }

        Args:
            boxDict (Dict): A dict of the data to build a dict

        Kwargs:
            namespace (string): The namespace look for objectes in

        Returns:
            FBBox: The newly created box
        """
        if not boxDict.has_key("dynamic"):
            raise ValueError("Box is not loadable, missing tag: dynamic")

        newBox = None
        if boxDict["dynamic"]["value"] == "True":
            try:
                boxDict["name"]["value"]
            except:
                raise ValueError("Dynamic Box is not loadable, missing tag: name.value")

            if boxDict["dynamic"]["method"] == "Out":
                mobuObject = None
                if namespace is None:
                    mobuObject = Scene.GetComponentByName(str(boxDict["name"]["value"]))
                else:
                    namespaceName = "{0}:{1}".format(namespace, str(boxDict["name"]["value"]))
                    mobuObject = Scene.GetComponentByName(namespaceName)

                if mobuObject is None:
                    return None
                newBox = self.AddSenderBox(mobuObject)
            else:
                mobuObject = None
                if namespace is None:
                    mobuObject = Scene.GetComponentByName(str(boxDict["name"]["value"]))
                else:
                    namespaceName = "{0}:{1}".format(namespace, str(boxDict["name"]["value"]))
                    mobuObject = Scene.GetComponentByName(namespaceName)

                if mobuObject is None:
                    return None
                newBox = self.AddReceiverBox(mobuObject)

            if boxDict["dynamic"].get("localTransforms") == "True":
                newBox.GetBox().UseGlobalTransforms = False
        else:
            # This is a function Box
            try:
                boxCategoryName = str(boxDict["function"]["category"])
                nameOfBox = str(boxDict["function"]["method"])
            except:
                raise ValueError("Dynamic Box is not loadable, missing tag: function.category or function.method")
            newBox = self.AddFunctionBox(boxCategoryName, nameOfBox)

        try:
            xPos = int(boxDict["location"]["xPos"])
            yPos = int(boxDict["location"]["yPos"])
        except:
            raise ValueError("Dynamic Box is not loadable, missing tag: location.xPos or location.yPos")
        newBox.SetBoxPosition(xPos, yPos)

        return newBox

    def LoadConstaintFromFile(self, filePath, namespace=None):
        """
        Load a Relationship Constraint XML file into this relationship constraint

        Args:
            filePath (string): The file path to load in

        Kwargs:
            namespace (string): The namespace look for objectes in
        """

        xmldoc = minidom.parse(filePath)

        constraints = xmldoc.getElementsByTagName("constraint")
        nodesDict = {}
        missingBoxes = []

        for constraint in constraints:
            nodes = constraint.getElementsByTagName("node")
            connections = constraint.getElementsByTagName("connection")
            values = constraint.getElementsByTagName("setValue")
            curves = constraint.getElementsByTagName("curveData")

            # We do the nodes first
            for node in nodes:
                nodeData = dict(node.attributes.items())
                boxDict = {}
                for part in [pt for pt in node.childNodes if not isinstance(pt, minidom.Text)]:
                    boxDict[part.localName] = dict(part.attributes.items())

                newBox = self._buildBoxFromDict(boxDict, namespace=namespace)
                nodesDict[nodeData["id"]] = newBox
                if newBox is None:
                    missingBoxes.append(boxDict["name"]["value"])

            # Then the connections
            for connection in connections:
                conDict = {}
                for part in [pt for pt in connection.childNodes if not isinstance(pt, minidom.Text)]:
                    conDict[part.localName] = dict(part.attributes.items())

                outBox = nodesDict[conDict["outOf"]["id"]]
                outProperty = conDict["outOf"]["property"]
                inBox = nodesDict[conDict["inTo"]["id"]]
                inProperty = conDict["inTo"]["property"]

                if outBox is None or inBox is None:
                    continue

                # Update connection names if either node is a macro node
                if outBox.Name.startswith("Macro Input"):
                    outProperty = outBox.GetSenders()[0].Name
                if inBox.Name.startswith("Macro Output"):
                    inProperty = inBox.GetReceivers()[0].Name

                self.ConnectBoxes(outBox, outProperty, inBox, inProperty)

            # Set the values
            for value in values:
                valueDict = dict(value.attributes.items())

                box = nodesDict[valueDict["nodeId"]]
                property = valueDict["property"]
                valueType = valueDict["type"]
                valuesList = []
                for part in [pt for pt in value.childNodes if not isinstance(pt, minidom.Text)]:
                    valDict = dict(part.attributes.items())
                    valuesList.append(float(valDict["value"]))

                if box is None:
                    continue

                box.SetReceiverValueByName(property, valuesList)

            # Set the FCurve
            for curve in curves:
                curveDict = dict(curve.attributes.items())
                keys = [dict(pt.attributes.items())for pt in curve.childNodes if not isinstance(pt, minidom.Text)]

                box = nodesDict[curveDict["nodeId"]]
                boxCurve = box.GetFCurve()

                boxCurve.EditBegin()
                for keyDict in keys:
                    boxCurve.KeyAdd(mobu.FBTime(int(keyDict["time"])), float(keyDict["value"]))

                for idx, keyDict in enumerate(keys):
                    newKey = boxCurve.Keys[idx]

                    newKey.TangentClampMode = newKey.TangentClampMode.values[int(keyDict["tangentClampMode"])]
                    newKey.TangentBreak = keyDict["tangentBreak"] == "True"
                    newKey.TangentConstantMode = newKey.TangentConstantMode.values[int(keyDict["tangentConstantMode"])]
                    newKey.TangentMode = newKey.TangentMode.values[int(keyDict["tangentMode"])]

                    newKey.Bias = float(keyDict["bias"])
                    newKey.Tension = float(keyDict["tension"])
                    newKey.Continuity = float(keyDict["continuity"])
                    newKey.Interpolation = newKey.Interpolation.values[int(keyDict["interpolation"])]

                    newKey.LeftBezierTangent = float(keyDict["leftBezierTangent"])
                    newKey.LeftDerivative = float(keyDict["leftDerivative"])
                    newKey.LeftTangentWeight = float(keyDict["leftTangentWeight"])

                    newKey.RightBezierTangent = float(keyDict["rightBezierTangent"])
                    newKey.RightDerivative = float(keyDict["rightDerivative"])
                    newKey.RightTangentWeight = float(keyDict["rightTangentWeight"])
                boxCurve.EditEnd()

        self._constraintObj.Active = True

        # Raise if there are missing parts
        if len(missingBoxes) > 0:
            raise ValueError("missing following nodes: {0}".format(",".join(missingBoxes)))


def LoadInConstraint(filePath, newName=None, namespace=None):
    """
    Load a Relationship Constraint XML file into a new relationship constraint

    Args:
        filePath (string): The file path to load in

    Kwargs:
        newName (string): The name of the constraint to find
        namespace (string): The namespace look for objectes in

    Return:
        RelationShipConstraintManager: The newly loaded constraint
    """
    newCon = RelationShipConstraintManager()
    if newName is not None:
        newCon.SetName(newName)

    newCon.LoadConstaintFromFile(filePath, namespace=namespace)
    return newCon


def GetAllRelationshipConstraints():
    """
    Get all the relationship constraint in the scene

    Return:
        list of RelationShipConstraintManager: All the relationship constraints
    """
    return [RelationShipConstraintManager(constraint) for constraint in Globals.Constraints
                                            if isinstance(constraint, mobu.FBConstraintRelation)]


def GetRelationShipConstraintByName(name, namespace=None):
    """
    Get a relationship constraint from its name

    Args:
        name (string): The name of the constraint to find

    Return:
        RelationShipConstraintManager: The named constraint
    """
    longName = None
    if namespace is not None:
        longName = "{0}:{1}".format(namespace, name)
    for constraint in Globals.Constraints:
        if not isinstance(constraint, mobu.FBConstraintRelation):
            continue
        if constraint.Name == name:
            if longName is not None:
                if constraint.LongName == longName:
                    return RelationShipConstraintManager(constraint)
            else:
                return RelationShipConstraintManager(constraint)
    return None
