'''
RS.Utils.Collections

Usage:

    This module should only contain functionality related to working with lists, tuples and dictionaries.
'''

from pyfbsdk import *
import os

def HasItems( targetList ):
    '''Returns true if the list has items.'''
    return len( targetList ) > 0

def IsEmpty( targetList ):
    '''Returns true if the list is empty.'''
    return len( targetList ) == 0

def Peek( targetList ):
    '''Returns the next item on the list but doesn't remove it from the list.'''
    if not IsEmpty( targetList ):
        return targetList[-1]
    else:
        return None

def Pop( targetList ):
    '''
    Returns the next item on the list and removes it from the list.
    There is already a 'pop' on python lists but there is no Peek. 
    So added this function for consistency.
    '''
    if not IsEmpty( targetList ):
        return targetList.pop()
    else:
        return None
    
def RemoveDuplicatesFromList( pArray ):  
    '''
    TODO (Hayes 7/17/2013): This would be faster to use set().  But would need testing.
    (HB): Fixed so it actually returns the array and not None
    '''
    lRemoveDuplicates = []
    [lRemoveDuplicates.append(i) for i in pArray if not lRemoveDuplicates.count( i )]
    
    return lRemoveDuplicates

def ReturnUnmatchedItems( pList, pCompare ):
    '''
    TODO (Hayes 7/17/2013): This would be faster to use set().  But would need testing.
    '''
    for iList in pList:
        for iCompare in pCompare:    
            if iCompare == iList:
                pCompare.remove(iCompare)
                
    return pCompare

def IsIterable( targetObject ):
    '''
    Returns true/false depending if the passed object is iterable
    '''
    return hasattr( targetObject,"__iter__" )

class StringBuilder:
    '''
    Searches through a string and builds a new string, removing any unwanted characters
    '''
    alphaList = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ]
    numberList = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ]
    specialList = [ '_' ]
    stringVariable = ""
    
    def __init__( self, string ):
                    
        for i in range( len( string ) ):
            
            if string[i].lower() in self.alphaList or string[i] in self.numberList or string[i] in self.specialList:
                
                self.stringVariable = self.stringVariable + string[i]