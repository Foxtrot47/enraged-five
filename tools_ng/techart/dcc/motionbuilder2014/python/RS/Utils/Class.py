"""
Usage:
    This module should only contain functionality related to working and preforming 
    operations on classes/types.
Author:
    Ross George
"""

def GetDerivedTypeList( targetTypeList, recursive = True, includeRootType = True ):
    '''
    Returns a list of the types that derive from the target types in the passed list.
    By default it will recursively walk the tree to get a full depth type list.
    '''

    if type( targetTypeList ) != list:
        targetTypeList = [targetTypeList]
    
    returnSet = set()
    for targetType in targetTypeList:
        if includeRootType:
            returnSet.add( targetType )
        for subtype in targetType.__subclasses__():
            returnSet.add( subtype )
            if recursive:
                returnSet.update( GetDerivedTypeList( subtype, True, False ) )
    return list( returnSet )