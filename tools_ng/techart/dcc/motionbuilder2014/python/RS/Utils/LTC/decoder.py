"""
Liner Timecode Reader Decoder file

Example:

sampleFile = r"C:\Users\geoff.samuel\workspace\RS\Utils\LTC\testFiles\000014_09_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav"
chn = 2
timecodeInfo = WavTimecodeExtractor(sampleFile, chn)
tcs = timecodeInfo.timeCodes()
for tc in tcs:
    print tc


References  : http://soundfile.sapp.org/doc/WaveFormat/
            : http://www.topherlee.com/software/pcm-tut-wavformat.html
            : http://www-mmsp.ece.mcgill.ca/documents/audioformats/wave/wave.html
            : http://itk.ilstu.edu/faculty/javila/DataTypes/soundWav.htm

"""
import ctypes
import struct
import pytimecode
import wavReader


class FrameRates(object):
    RATE_30 = 20
    RATE_60 = 30


class WavTimecodeExtractor(object):
    """
    WAV Timecode Extractor

    Assumes all video is at 30 SMPTE
    """
    # All Timecodes end with this sync code
    SYNC_WORD = [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1]

    def __init__(self, filePath, channel=None, frameRate=FrameRates.RATE_30):
        """
        Constructor

        Args:
            filePath (str):    Filepath to the WAV file
            channel (int):     Channel of the WAV to use to extract the timecode
        """
        self._filePath = filePath
        sampleRate, _, data = wavReader.readwav(self._filePath)
        self._tcs = []

        if channel is None:
            for chnIdx in xrange(int(data.shape[1])):
                chdData = self._processChannel(data[:, chnIdx], frameRate)
                if len(data) > 0:
                    self._tcs = data
        else:
            # Check channel is in file
            _, maxChannel = data.shape
            if channel >= int(maxChannel):
                raise ValueError("Channel does not exist in Wav File")
            
            self._tcs = self._processChannel(data[:, channel], frameRate)

    @classmethod
    def _processChannel(cls, wholeCurve, frameRate):
        """
        Class method

        Internal Method

        Process a channel and return all the TC's on that channel
        """
        normData = cls.normalizeCurve(wholeCurve)
        frameStart = 0
        normData = normData[frameStart:]
        waveForm = cls.interprateCurve(normData, frameRate, 5)
        return cls.findAllSyncPoints(waveForm, frameStart / frameRate)

    def timeCodes(self):
        """
        Get all the found timecodes

        return:
            List of Timecode Strings
        """
        return self._tcs

    @classmethod
    def calulateStartAndModulation(cls, normalizeCurve):
        """
        Class Method

        Calculate the start and modulation of the normalised curve

        args:
            normalizedCurve (list):    List of ints that makes up the waveform

        return:
            the start position in the array as an int AND the curve modulation as an int

        """
        # Calculate curve modulation
        lengthData = {}
        lengthCount = 0
        for idx in xrange(len(normalizeCurve) - 1):
            lengthCount += 1
            if normalizeCurve[idx] != normalizeCurve[idx + 1]:
                lengthData[lengthCount] = lengthData.get(lengthCount, 0) + 1
                lengthCount = 0
        try:
            modulation = max(sorted(lengthData, key=lengthData.get, reverse=True)[:4])
        except:
            return 0, 0
        return cls.calulateStartFrame(normalizeCurve, modulation)

    @staticmethod
    def calulateStartFrame(normalizeCurve, modulation, threshold=2):
        """
        
            threshold (int):    Threshold out the sine wave curves

        """
        startPos = 0
        while(True):
            changePos = 0
            for idx in xrange(startPos, len(normalizeCurve) - 1):
                changePos += 1
                if normalizeCurve[idx] != normalizeCurve[idx + 1]:
                    break

            # Deal with wholesome (OFF) signal "bumps"
            if changePos > modulation - threshold and changePos < modulation + threshold:
                break

            startPos += changePos
        return startPos

    @staticmethod
    def interprateCurve(normalizeCurve, bitLenght, threshold):
        """
        Static Method

        Interprate the sine waves in the curve as binary 1's and 0's

        arg:
            normalizedCurve (list):    List of ints that makes up the waveform
            bitLenght (int):    The modulation of the sine wave
            threshold (int):    Threshold out the sine wave curves

        return:
            a list of binary 0's and 1's based off their bit encoding along the length of the modulation
        """
        waveformMod = []

        currentVal = []
        allVals = []
        for idx in xrange(len(normalizeCurve) - 1):
            currentVal.append(normalizeCurve[idx])
            if normalizeCurve[idx] != normalizeCurve[idx + 1]:
                allVals.append(currentVal)
                currentVal = []

        idx = 0
        while(True):
            if idx >= len(allVals):
                break
            val = allVals[idx]

            if len(allVals[idx]) > bitLenght - threshold:
                waveformMod.append(0)
            else:
                waveformMod.append(1)
                idx += 1
            idx += 1

        return waveformMod

    @staticmethod
    def normalizeCurve(curve, tolerance=400):
        """
        Static Method

        Normalised a curve into binary values, where minus values are 0 and positive are 1

        args:
            curve (list):     List of ints that makes up the waveform

        return:
            list of ints of normalised values
        """
        normArray = []
        lastData = 1 if curve[0] > 0 else 0
        for data in curve:
            normVal = 1 if data > 0 else 0
            if normVal != lastData:
                if abs(data) < tolerance:
                    normVal = lastData
            normArray.append(normVal)
        return normArray
            
    @classmethod
    def findAllSyncPoints(cls, waveform, frameStart):
        """
        Class Method

        Extract the Timecodes from the normalised waveform

        args:
            waveform (list):    list of normalised ints from the audio channel
            frameStart (int):    The frames which were lost until a sync was found

        return:
            list of SMPTE timecode strings
        """
        tcs = []
        continutiyLink = []
        syncWordLen = len(cls.SYNC_WORD)

        for idx in xrange(64, len(waveform) - syncWordLen):
            if waveform[idx:idx + syncWordLen] == cls.SYNC_WORD:
                continutiyLink.append(idx)
                tcs.append(cls.decodeBitArray(waveform[idx - 64:idx + syncWordLen]))

        if len(tcs) == 0:
            return []
        
        # Check for continity
        continutiyIdx = 0
        while(True):
            if continutiyIdx > len(tcs) - 4:
                break

            tcIdx = pytimecode.PyTimeCode(30, start_timecode=tcs[continutiyIdx])
            tcIdxPlusOne = pytimecode.PyTimeCode(30, start_timecode=tcs[continutiyIdx + 1])
            tcIdxPlusTwo = pytimecode.PyTimeCode(30, start_timecode=tcs[continutiyIdx + 2])
            tcIdxPlusThree = pytimecode.PyTimeCode(30, start_timecode=tcs[continutiyIdx + 3])
            tcIdx.next()
            if tcIdx.frames != tcIdxPlusOne.frames:
                continutiyIdx += 1
                continue

            tcIdx.next()
            if tcIdx.frames != tcIdxPlusTwo.frames:
                continutiyIdx += 1
                continue

            tcIdx.next()
            if tcIdx.frames != tcIdxPlusThree.frames:
                continutiyIdx += 1
                continue
            break

        timelineIdx = continutiyLink[continutiyIdx] + frameStart - 79

        tcIdx = pytimecode.PyTimeCode(30, start_timecode=tcs[continutiyIdx])
        tcs = tcs[continutiyIdx:]

        for idx in xrange(timelineIdx, 20, -80):
            tcIdx.back()
            tcs.insert(0, tcIdx.make_timecode())

        return tcs

    @classmethod
    def decodeBitArray(cls, inputArray):
        """
        Class Method

        Decode an binary array into a a SMPTE timecode string

        args:
            inputArray (list):    List of 1 and 0 that is extracted from the audio wave
        """
        if inputArray[len(inputArray) - 16:len(inputArray)] != cls.SYNC_WORD:
            raise ValueError("Not a valid LTC bit array")
        frames = 0
        seconds = 0
        minutes = 0
        hours = 0

        dropFrame = ":"

        # Frames
        if inputArray[10] == 1: dropFrame = ";"

        # Frames
        if inputArray[0] == 1: frames += 1
        if inputArray[1] == 1: frames += 2
        if inputArray[2] == 1: frames += 4
        if inputArray[3] == 1: frames += 8

        if inputArray[8] == 1: frames += 10
        if inputArray[9] == 1: frames += 20

        # Seconds
        if inputArray[16] == 1: seconds += 1
        if inputArray[17] == 1: seconds += 2
        if inputArray[18] == 1: seconds += 4
        if inputArray[19] == 1: seconds += 8

        if inputArray[24] == 1: seconds += 10
        if inputArray[25] == 1: seconds += 20
        if inputArray[26] == 1: seconds += 40

        # Minutes
        if inputArray[32] == 1: minutes += 1
        if inputArray[33] == 1: minutes += 2
        if inputArray[34] == 1: minutes += 4
        if inputArray[35] == 1: minutes += 8

        if inputArray[40] == 1: minutes += 10
        if inputArray[41] == 1: minutes += 20
        if inputArray[42] == 1: minutes += 40

        # Hours
        if inputArray[48] == 1: hours += 1
        if inputArray[49] == 1: hours += 2
        if inputArray[50] == 1: hours += 4
        if inputArray[51] == 1: hours += 8

        if inputArray[56] == 1: hours += 10
        if inputArray[57] == 1: hours += 20

        return "{0:0>2}:{2:0>2}:{3:0>2}{1}{4:0>2}".format(hours, dropFrame, minutes, seconds, frames)

