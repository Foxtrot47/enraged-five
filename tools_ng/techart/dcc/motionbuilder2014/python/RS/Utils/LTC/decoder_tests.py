"""
Test the timecode decoder
"""
import os


from RS.Utils.LTC import decoder


testData = [
    ["005600_02_GC_PDJEB_IG2_P1_cam1.wav", 2, "17:41:32:16", decoder.FrameRates.RATE_30],
    ["005600_01_GC_PDJEB_IG2_P1_cam1.wav", 2, "17:38:10:25", decoder.FrameRates.RATE_30],
    ["005603_01_GC_PDJEB_IG5_P1_cam1.wav", 2, "18:14:21:28", decoder.FrameRates.RATE_30],
    ["005605_03_GC_PDDWW_IG3_P1_cam1.wav", 2, "18:48:09:01", decoder.FrameRates.RATE_30],
    ["005606_04_GC_PDDWW_IG4_P1_cam1.wav", 2, "19:12:56:02", decoder.FrameRates.RATE_30],
    ["000014_01_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "15:04:34:08", decoder.FrameRates.RATE_30],
    ["000014_01_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "15:04:34:12", decoder.FrameRates.RATE_30],
    ["000014_02_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "15:13:12:28", decoder.FrameRates.RATE_30],
    ["000014_02_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "15:13:12:28", decoder.FrameRates.RATE_30],
    ["000014_03_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "15:23:33:05", decoder.FrameRates.RATE_30],
    ["000014_03_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "15:23:33:05", decoder.FrameRates.RATE_30],
    ["000014_04_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "15:43:00:18", decoder.FrameRates.RATE_30],
    ["000014_04_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "15:43:02:04", decoder.FrameRates.RATE_30],
    ["000014_05_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "15:56:25:26", decoder.FrameRates.RATE_30],
    ["000014_05_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "15:56:27:02", decoder.FrameRates.RATE_30],
    ["000014_06_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "16:03:21:06", decoder.FrameRates.RATE_30],
    ["000014_06_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "16:03:21:07", decoder.FrameRates.RATE_30],
    ["000014_07_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "16:09:57:08", decoder.FrameRates.RATE_30],
    ["000014_07_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "16:09:57:12", decoder.FrameRates.RATE_30],
    ["000014_08_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "16:15:10:04", decoder.FrameRates.RATE_30],
    ["000014_08_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "16:15:09:29", decoder.FrameRates.RATE_30],
    ["000014_09_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "16:19:41:24", decoder.FrameRates.RATE_30],
    ["000014_09_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "16:19:42:05", decoder.FrameRates.RATE_30],
    ["000014_10_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Female_001.wav", 2, "16:46:17:07", decoder.FrameRates.RATE_30],
    ["000014_10_GC_PROP_HUMAN_SEAT_CHAIR_TABLE_EATING_KNIFE_FORK_AB_Generic_Male_002.wav", 2, "16:46:18:16", decoder.FrameRates.RATE_30],
    ["005707_01_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:06:37:00", decoder.FrameRates.RATE_30],
    ["005707_02_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:08:15:13", decoder.FrameRates.RATE_30],
    ["005707_03_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:10:32:09", decoder.FrameRates.RATE_30],
    ["005707_04_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:16:25:09", decoder.FrameRates.RATE_30],
    ["005707_05_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:18:08:01", decoder.FrameRates.RATE_30],
    ["005707_06_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:22:39:15", decoder.FrameRates.RATE_30],
    ["005707_07_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:27:44:26", decoder.FrameRates.RATE_30],
    ["005707_08_GC_PDMOS_IG6_P1_Arthur_Morgan.wav", 2, "15:33:39:07", decoder.FrameRates.RATE_30],
    ["005709_01_GC_PDMOS_IG7_P1_Arthur_Morgan.wav", 2, "15:43:52:20", decoder.FrameRates.RATE_30],
    ["005709_02_GC_PDMOS_IG7_P1_Arthur_Morgan.wav", 2, "15:46:47:19", decoder.FrameRates.RATE_30],
    ["005714_01_GC_PDMOS_IG8_P1_Arthur_Morgan.wav", 2, "17:57:43:07", decoder.FrameRates.RATE_30],
    ["005714_02_GC_PDMOS_IG8_P1_Arthur_Morgan.wav", 2, "18:00:52:08", decoder.FrameRates.RATE_30],
    ["005715_01_GC_PDMOS_IG9_P1_Arthur_Morgan.wav", 2, "18:06:26:04", decoder.FrameRates.RATE_30],
    ["005715_02_GC_PDMOS_IG9_P1_Arthur_Morgan.wav", 2, "18:10:03:09", decoder.FrameRates.RATE_30],
    ["005742_01_GC_PDBMC_IG8_P1_player.wav", 2, "17:48:48:08", decoder.FrameRates.RATE_30],
    ["005742_02_GC_PDBMC_IG8_P1_player.wav", 2, "17:54:55:15", decoder.FrameRates.RATE_30],
    ["005743_01_GC_PDSFW_IG2_P1_Arthur_Morgan.wav", 2, "18:29:42:15", decoder.FrameRates.RATE_30],
    ["005743_02_GC_PDSFW_IG2_P1_Arthur_Morgan.wav", 2, "18:31:50:16", decoder.FrameRates.RATE_30],
    ["005743_03_GC_PDSFW_IG2_P1_Arthur_Morgan.wav", 2, "18:34:23:01", decoder.FrameRates.RATE_30],
    ["005743_04_GC_PDSFW_IG2_P1_Arthur_Morgan.wav", 2, "18:37:41:02", decoder.FrameRates.RATE_30],
    ["005744_01_GC_PDSFW_IG3_P1_Arthur_Morgan.wav", 2, "18:41:37:23", decoder.FrameRates.RATE_30],
    ["005745_01_GC_PDSFW_IG4_P1_Arthur_Morgan.wav", 2, "18:52:35:29", decoder.FrameRates.RATE_30],
    ["005745_02_GC_PDSFW_IG4_P1_Arthur_Morgan.wav", 2, "18:55:15:15", decoder.FrameRates.RATE_30],
    ["005745_03_GC_PDSFW_IG4_P1_Arthur_Morgan.wav", 2, "19:00:38:08", decoder.FrameRates.RATE_30],
    ["005746_01_GC_PDSFW_IG5_P1_Arthur_Morgan.wav", 2, "19:06:17:22", decoder.FrameRates.RATE_30],
    ["005746_02_GC_PDSFW_IG5_P1_Arthur_Morgan.wav", 2, "19:10:17:08", decoder.FrameRates.RATE_30],
    ["005746_03_GC_PDSFW_IG5_P1_Arthur_Morgan.wav", 2, "19:15:05:10", decoder.FrameRates.RATE_30],
    ["005746_03_GC_PDSFW_IG5_P1_Arthur_Morgan.wav", 2, "19:15:05:10", decoder.FrameRates.RATE_30],
    ["003027_02_BE_CCAM_CONF_CNV1_P1_Arthur_Morgan.wav", 2, "10:30:36:11", decoder.FrameRates.RATE_30],
    ["003035_05_BE_RECP_IG2_LARGE_P1_EXCONFED_YOUNG_3.wav", 2, "13:17:09:25", decoder.FrameRates.RATE_30],
    ["003035_06_BE_RECP_IG2_LARGE_P1_EXCONFED_YOUNG_3.wav", 2, "13:20:49:13", decoder.FrameRates.RATE_30],
    ["003036_02_BE_REIT_IG1_FIREBOMB_ATTACK_P1_EXCONFED_YOUNG_2.wav", 2, "15:06:24:01", decoder.FrameRates.RATE_30],
    ["003036_03_BE_REIT_IG1_FIREBOMB_ATTACK_P1_EXCONFED_YOUNG_3.wav", 2, "15:10:46:12", decoder.FrameRates.RATE_30],
    ["003036_04_BE_REIT_IG1_FIREBOMB_ATTACK_P1_EXCONFED_YOUNG_2.wav", 2, "15:14:15:20", decoder.FrameRates.RATE_30],
    ["003036_05_BE_REIT_IG1_FIREBOMB_ATTACK_P1_EXCONFED_YOUNG_2.wav", 2, "15:18:45:24", decoder.FrameRates.RATE_30],
    ["003036_06_BE_REIT_IG1_FIREBOMB_ATTACK_P1_EXCONFED_YOUNG_3.wav", 2, "15:46:55:04", decoder.FrameRates.RATE_30],
    ["003037_01_BE_REEX_IG1_EXECUTION_P1_EXCONFED_YOUNG_2.wav", 2, "16:12:37:13", decoder.FrameRates.RATE_30],
    ["003037_03_BE_REEX_IG1_EXECUTION_P1_EXCONFED_YOUNG_2.wav", 2, "16:24:03:26", decoder.FrameRates.RATE_30],
    ["003037_02_BE_REEX_IG1_EXECUTION_P1_cam2.wav", 2, "16:18:15:15", decoder.FrameRates.RATE_30],
    ["003037_03_BE_REEX_IG1_EXECUTION_P1_EXCONFED_YOUNG_3.wav", 2, "16:24:04:07", decoder.FrameRates.RATE_30],
    ["V_0770_A_F_M_LagTownfolk_01_BLACK_01_2016-06-21_06-18.wav", 0, "11:31:18:08", decoder.FrameRates.RATE_30],
    ["003359_03_BE_WNT1_INT_WT_P3A1_L1_cam1.wav", 2, "11:11:50:00", decoder.FrameRates.RATE_30],
    ["003360_01_BE_WNT1_INT_WT_P1A1_cam1.wav", 2, "12:05:20:19", decoder.FrameRates.RATE_30],
    ["003522_04_BE_GNG2_IG10_SADIE_CLIMB_BALLOON_ROPE_P1_cam1.wav", 2, "12:18:29:17", decoder.FrameRates.RATE_30],
    ["003523_01_BE_GNG2_IG10_SADIE_CLIMB_BALLOON_ROPE_P2_cam1.wav", 2, "12:48:04:24", decoder.FrameRates.RATE_30],
    ["003524_01_BE_GNG2_IG10_SADIE_CLIMB_BALLOON_ROPE_P3_cam1.wav", 2, "14:25:31:17", decoder.FrameRates.RATE_30],
    ["003524_02_BE_GNG2_IG10_SADIE_CLIMB_BALLOON_ROPE_P3_cam1.wav", 2, "14:41:42:20", decoder.FrameRates.RATE_30],
    ["003527_01_BE_GNG2_MCS3_P3_cam1.wav", 2, "17:37:37:20", decoder.FrameRates.RATE_30],
    ["T_0852_A_F_M_Civ_Poor_Black_AVOID_02_2016-08-08_01-02.wav", 0, "10:11:27:16", decoder.FrameRates.RATE_30],
    ["T_0852_A_F_M_Civ_Poor_Black_AVOID_02_2016-08-08_01-08.wav", 0, "10:12:50:26", decoder.FrameRates.RATE_30],
]

winCount = 0
totalCount = 0
for test in testData:
    totalCount += 1
    sampleFileName, chn, output, rate = test
    sampleFile = os.path.join("X:", "wildwest", "data", "LtcTestData", sampleFileName)
    timecodeInfo = decoder.WavTimecodeExtractor(sampleFile, chn, rate)
    try:
        assert(timecodeInfo.timeCodes()[0] == output)
        winCount += 1
    except:
        print sampleFile
        try:
            print "TCin: {0}".format(timecodeInfo.timeCodes()[0])
            print "RealIn: {0}".format(output)
        except:
            print "No TC Found"
        print "Test Failed"
        print "========"

print "%s failed out of %s tests" % (totalCount - winCount, totalCount)
