#Sandbox
#Author: Mike Wilson (modified by Kristine Middlemiss for MotionBuilder )
#Note: Some modules will not import here

from ctypes import *
from pyfbsdk import *
from pyfbsdk_additions import *
from xml.dom import minidom
from collections import defaultdict
import re
import os
import sys
import time
import subprocess
import xml.etree.cElementTree as xml
import RS

scriptDict = defaultdict(str)

dblclk_timer_start = 0
dblclk_timer_interval = 1
dblclk_timer_nodename = ""


def get_environment_variable(environment_var):
    """
    gets the value associated with the environment variable passed

    :param environment_var: string
        the environment variable that you want to query
    :return: string
    """

    # Get the python tools executable
    try:
        os_environments = subprocess.check_output('setenv', shell=True)
    except subprocess.CalledProcessError:
        setenv = os.path.join(RS.Config.Tool.Path.Bin, "setenv.bat")
        if os.path.exists(setenv):
            os_environments = subprocess.check_output(setenv, shell=True)
        else:
            raise Exception, "{} is missing from this computer. Grab this file from perforce to run this tool."

    regular_expression = r"(?<={environment_var}:)[a-z0-9._:\t\b\\ ]+".format(environment_var=environment_var)
    search_results = re.search(regular_expression, os_environments, re.M|re.I)

    if search_results:
        tools_executable = search_results.group().strip()
    else:
        raise "No system environment matching {environment_var} is setup".format(environment_var=environment_var)

    return tools_executable

def get_sandbox_exception_lists():
    """
    Gets the exception emails from the xml
    """
    tools_root_path = get_environment_variable("RS_TOOLSROOT")
    setup_config_path = os.path.join(tools_root_path, r"techart\dcc\motionbuilder{0}\python\setup.config".format(RS.Config.Script.TargetBuild))

    xml_tree = xml.parse(setup_config_path)
    xml_element = xml_tree.getroot()
    print xml_element.tag
    sandbox_element = xml_element.find("SandboxConfiguration")

    ignore_list = [each.text for each in sandbox_element.find("IgnoreUserList")]
    email_list = [each.text for each in sandbox_element.find("EmailList")]

    return ignore_list, email_list

# Callback
def TreeSelectCallback(control, event):
    global dblclk_timer_start
    global dblclk_timer_interval
    global dblclk_timer_nodename
    
    #print "current time: " + str(time.time())
    #print "against time: " + str(dblclk_timer_start + dblclk_timer_interval)
    # since autodesk suck, had to implement own double click method
    if time.time() <= (dblclk_timer_start + dblclk_timer_interval):
        if (dblclk_timer_start != 0):
            if (dblclk_timer_nodename == str(event.TreeNode.Name)):
                if  str(event.TreeNode.Name) == "Reference Editor":
                    import RS.Tools.UI.ReferenceEditor
                    RS.Tools.UI.ReferenceEditor.Run()
                elif str(event.TreeNode.Name) == "Phase Calculator":
                    import RS.Tools.UI.PhaseCalculator
                    RS.Tools.UI.PhaseCalculator.Run()                
                else:                
                    app = FBApplication()
                    app.ExecuteScript (scriptDict[str(event.TreeNode.Name)])
                dblclk_timer_start = 0
                dblclk_timer_nodename = ""
                return 0

    dblclk_timer_start = time.time()
    dblclk_timer_nodename = event.TreeNode.Name

def PopulateLayout(mainLyt):
    x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
    w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
    h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
    mainLyt.AddRegion("main","main", x, y, w, h)
    
    t = FBTree()
    t.CheckBoxes = False
    t.AutoExpandOnDblClick = True
    t.AutoExpandOnDragOver = True
    t.AutoScroll = True
    t.AutoScrollOnExpand = True
    t.ItemHeight = 20
    t.DeselectOnCollapse = True
    t.EditNodeOn2Select = False
    t.HighlightOnRightClick = True
    t.MultiDrag = True
    t.MultiSelect = False
    t.NoSelectOnDrag = True
    t.NoSelectOnRightClick = False
    t.ShowLines = True
    
    mainLyt.SetControl("main",t)

    scriptDict.clear()

    r = t.GetRoot()
    r1 = t.InsertLast(r, str("Personal Sandbox"))
    tools_root_path = get_environment_variable("RS_TOOLSROOT")
    sandboxmenu_path = os.path.join(tools_root_path, r"techart\sandboxmotionbuilder\SandboxMenu.xml")
    #print "My Menu Path :" ,sandboxmenu_path

    xmldoc = minidom.parse(sandboxmenu_path)
    for node in xmldoc.childNodes:
        for node2 in node.childNodes:
            if node2.nodeType != node.TEXT_NODE: #ignore \n lines and shit
                n = t.InsertLast(r1, str(node2.attributes["name"].value))            
            for node3 in node2.childNodes:
                if node3.nodeType != node.TEXT_NODE:
                    t.InsertLast(n, str(node3.attributes["name"].value))
                    scriptDict[str(node3.attributes["name"].value)] = str(node3.attributes["script"].value)

    t.OnSelect.Add(TreeSelectCallback)


def CreateTool(*args):
    # Tool creation will serve as the hub for all other controls
    t = FBCreateUniqueTool("MotionBuilder {0} Sandbox".format(RS.Config.Script.TargetBuild))
    t.StartSizeX = 200
    t.StartSizeY = 200
    PopulateLayout(t)
    ShowTool(t)

def Run(*args):
    CreateTool()