###############################################################################
# Name: 
#   keyframe_mp_sync.py
#
# Description: 
#   User interface for syncing the Maya timeline with Keyframe MP.
#
# Usage options:
#   1) Run from the script editor by executing entire script.
#
#   2) Create a Python shelf button by copying entire script to shelf.
#
#   3) Save script in the scripts folder (or somewhere on the PYTHONPATH) 
#      and run the following Python commands:
#
#       from keyframe_mp_sync import KeyframeMpSync
#       from keyframe_mp_sync import KeyframeMpHelper
#
#       KeyframeMpSync.display()
#
# Notes:
#   The port set in the Keyframe MP preference must match the 
#   KEYFRAME_MP_PORT constant defined below.
#
# Author: 
#   Chris Zurbrigg (http://zurbrigg.com)
#
###############################################################################

import os
import socket
import subprocess
import sys
import traceback

from pyfbsdk import *
from pyfbsdk_additions import *


class KeyframeMpHelper(object):
    '''
    Helper class for connecting and sending commands to Keyframe MP
    '''
    

    KEYFRAME_MP_PATH = "C:/Program Files/Zurbrigg/Keyframe MP/bin/keyframe_mp.exe"    
    KEYFRAME_MP_PORT = 17171
    kf_socket = None
    
    @classmethod
    def open_player(cls):
        if os.path.exists(cls.KEYFRAME_MP_PATH):
            subprocess.Popen(cls.KEYFRAME_MP_PATH, shell=False, stdin=None, stdout=None, stderr=None)
        else:
            om.MGlobal.displayError("Keyframe MP not found. Please update path.")           
    
    @classmethod
    def connect(cls):
        if cls.is_connected():
            return True
        
        try:
            cls.kf_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            cls.kf_socket.connect(("localhost", cls.KEYFRAME_MP_PORT))
            #om.MGlobal.displayInfo("Connection established");
            print ("Connection established")
            return True
        except:
            cls.kf_socket = None
            traceback.print_exc()
            return False
            
    @classmethod
    def disconnect(cls):
        
        if (cls.kf_socket):
            try:
                cls.kf_socket.close()
                om.MGlobal.displayInfo("Connection disconnected");
            except:
                om.MGlobal.displayError("Failed to disconnect")
        
        cls.kf_socket = None
        
    @classmethod
    def is_connected(cls):
        if not cls.kf_socket:
            return False
        
        try:
            cls.kf_socket.send("ping\n")
            cls.kf_socket.send("ping\n")
            return True
        except:
            return False
        
    @classmethod
    def send(cls, command):
        if not cls.connect():
            return False
        
        try:
            cls.kf_socket.send(command)
            return True
        except:
            traceback.print_exc()
            return False
        
        
    @classmethod
    def load_media(cls, filename, range_start=-1, range_end=-1):
        cls.send("file '{0}' {1} {2}\n".format(filename, range_start, range_end))
        
    @classmethod
    def set_current_frame(cls, frame):
        return cls.send("frame {0}\n".format(frame))
        
    @classmethod
    def set_current_frame_relative(cls, frame):
        return cls.send("frameRelative {0}\n".format(frame))
        
        
class KeyframeMpSync(object):
    '''
    User interface class for syncing Motionbuilder with Keyframe MP
    '''
    
    
    WINDOW_NAME = "KeyframeMpSyncWindow"
    WINDOW_TITLE = "Keyframe MP"
    
    SCRIPT_NODE_NAME = "KeyframeMpSyncScriptNode"
    
    OFFSET_OPTION_VAR = "KeyframeMpOffset"
    
    sync_control = None
    offset_control = None
    
    #sys = FBSystem()
    app = FBApplication()
    lEvaluator = FBEvaluateManager() 
    
    LastFrame = 0
    
    @classmethod
    def display(cls, mainLyt):
        
        mainLyt.AddRegion("main","main",
                      FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
                      FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
                      FBAddRegionParam(140,FBAttachType.kFBAttachRight,""),
                      FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")) 
        
        lyt = FBHBoxLayout(FBAttachType.kFBAttachLeft)      
        mainLyt.SetControl("main",lyt)   
        
        
        cls.sync_control = FBButton()
        cls.sync_control.Style = FBButtonStyle.kFBCheckbox 
        cls.sync_control.Caption = "Sync"
        cls.sync_control.OnClick.Add(KeyframeMpSync.sync_changed)        
        lyt.Add(cls.sync_control ,80)
        
        
        l = FBLabel()
        l.Caption = 'Offset'
        lyt.Add(l,34)
        cls.offset_control = FBEditNumber()
        cls.offset_control.Precision = 1.0
        cls.offset_control.OnChange.Add(KeyframeMpSync.offset_changed)        
        lyt.Add(cls.offset_control ,60)        
        
        
        offset_from_current_btn = FBButton()
        offset_from_current_btn.Caption = "Current"
        offset_from_current_btn.OnClick.Add(KeyframeMpSync.offset_from_current)        
        lyt.Add(offset_from_current_btn ,80)        
        
        
        reset_offset_btn = FBButton()
        reset_offset_btn.Caption = "Reset"
        reset_offset_btn.OnClick.Add(KeyframeMpSync.reset)        
        lyt.Add(reset_offset_btn ,50)           

     
    @classmethod    
    def CreateTool(cls):
        t = FBCreateUniqueTool("Keyframe MP")
        t.StartSizeX = 344
        t.StartSizeY = 44        
        cls.display(t)
        ShowTool(t)
        
        #print cls.lEvaluator.OnSynchronizationEvent.callbacks
        
        #if len(cls.lEvaluator.OnSynchronizationEvent.callbacks) > 0:
        #    cls.sync_control.State = True
            
        
        
        
        
        
        
        
        
        
    '''
    #@classmethod REMOVE!!!!
    def _display(cls):
        if cmds.window(cls.WINDOW_NAME, exists=True):
            cmds.deleteUI(cls.WINDOW_NAME, window=True)
            
        main_window = cmds.window(cls.WINDOW_NAME, title=cls.WINDOW_TITLE, tlb=False, rtf=True, mnb=False, mxb=False)
        main_layout = cmds.formLayout(parent=main_window);
        
        cls.sync_control = cmds.checkBox(label="Sync", 
                                         value=KeyframeMpSync.script_node_exists(), 
                                         cc="KeyframeMpSync.sync_changed()",
                                         parent=main_layout)
        
        cls.offset_control = cmds.intFieldGrp(label="Offset ", 
                                              value1=KeyframeMpSync.offset(),
                                              columnWidth2=(60, 60),
                                              cc="KeyframeMpSync.offset_changed()",
                                              parent=main_layout)
        
        offset_from_current_btn = cmds.button(label="Current",
                                              c="KeyframeMpSync.offset_from_current()",
                                              parent=main_layout)
        
        reset_offset_btn = cmds.button(label="Reset",
                                       c="KeyframeMpSync.set_offset(0)",
                                       parent=main_layout)
        
        cmds.formLayout(main_layout, e=True, af=(cls.sync_control, "top", 6))
        cmds.formLayout(main_layout, e=True, af=(cls.sync_control, "left", 5))
        
        cmds.formLayout(main_layout, e=True, af=(cls.offset_control, "top", 4))
        cmds.formLayout(main_layout, e=True, ac=(cls.offset_control, "left", 0, cls.sync_control))
        
        cmds.formLayout(main_layout, e=True, af=(offset_from_current_btn, "top", 4))
        cmds.formLayout(main_layout, e=True, ac=(offset_from_current_btn, "left", 0, cls.offset_control))
        
        
        cmds.formLayout(main_layout, e=True, af=(reset_offset_btn, "top", 4))
        cmds.formLayout(main_layout, e=True, ac=(reset_offset_btn, "left", 0, offset_from_current_btn))
        cmds.formLayout(main_layout, e=True, af=(reset_offset_btn, "right", 5))
        
        cmds.showWindow(main_window)
    '''
    
    @classmethod
    def reset(cls, control, event):
        cls.offset_control.Value = 0
        cls.set_offset(0)
        
    @classmethod
    def set_offset(cls, value):
        currentFrame = FBSystem().LocalTime.GetFrame()
        KeyframeMpHelper.set_current_frame_relative(currentFrame + cls.offset_control.Value)

        
    @classmethod
    def offset_changed(cls, control, event):
        new_offset = control.Value
        cls.set_offset(new_offset)
        
    @classmethod
    def offset_from_current(cls):
        new_offset = -cmds.currentTime(q=True) + 1
        cls.set_offset(new_offset)
        
    @classmethod
    def sync_changed(cls, control, event):
        enabled = control.State      
        if (enabled):
            cls.registerCallback()
        else:
            cls.unregisterCallback()


        
    
    '''
    @classmethod
    def script_node_exists(cls):
        return cmds.objExists(cls.SCRIPT_NODE_NAME)
    
    @classmethod
    def create_script_node(cls):
        if not KeyframeMpSync.script_node_exists():
            
            cmds.scriptNode(scriptType=7,
                            beforeScript="try: KeyframeMpSync.on_maya_time_change()\nexcept: pass",
                            name=cls.SCRIPT_NODE_NAME,
                            sourceType="python")
            
    @classmethod
    def delete_script_node(cls):
        if KeyframeMpSync.script_node_exists():
            cmds.delete(cls.SCRIPT_NODE_NAME)
    
          
    @classmethod
    def on_maya_time_change(cls):
        frame = cmds.currentTime(q=True)
        
        if not KeyframeMpHelper.set_current_frame_relative(frame + cls.offset()):
            try:
                cmds.checkBox(cls.sync_control, e=True, value=False)
                cls.sync_changed()
                print("Sync with Keyframe MP disabled. (see Script Editor for details)")
            except:
                cls.delete_script_node();
                print("Disconnected from Keyframe MP (Connection no longer available)")
    '''
                
    
    @classmethod
    def registerCallback(cls, control=None, event=None):  
        cls.lEvaluator.OnSynchronizationEvent.Add(cls.OnSyncUpdate)
        cls.app.OnFileExit.Add(cls.unregisterCallback)        
      
    @classmethod
    def unregisterCallback(cls, control=None, event=None):
        cls.lEvaluator.OnSynchronizationEvent.Remove(cls.OnSyncUpdate)
     
    @classmethod    
    def OnSyncUpdate(cls, control, event):
        currentFrame = FBSystem().LocalTime.GetFrame()
        if cls.LastFrame != currentFrame:
            cls.LastFrame = currentFrame
            try:
                KeyframeMpHelper.set_current_frame_relative(currentFrame + cls.offset_control.Value)
            except:
                ''''''
                
            
      
        
        
    
    
                
def Run():
    KeyframeMpHelper.open_player()
    KeyframeMpSync.CreateTool()

    
Run()
    

    