"""
Remote Console
"""
import clr
import sys
import os
from subprocess import Popen, PIPE, STDOUT




clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory
_config = ConfigFactory.CreateConfig()


"""
Public
"""

"""Write only."""   
def AddCmd(data, value=None):
    _writeQuery_(data, value, 'a')
    return

"""Write and execute."""    
def RunCmd(cmd=None, value=None ):
    return _runRagCmd_(cmd,value, 'w')

"""Get Rag's IP connection"""    
def GetIP():
    return _getRagIP_()
    
    

"""
Private
"""

"""Write out our query for Rag to execute."""

_ragCmdPath = _config.ToolsTemp+"/cmd.txt"


""" GEt IP, revist this with a more robust porcess"""
def _getRagIP_():
    _resetCmd_()
    cmd = "debug"
    _writeQuery_(cmd,None, 'w')
    stdOut =  _runRagProcess_() 
    for line in iter(stdOut.readline,''):
        if "gameip" in line:
            sline =line.split(',')
            ip = sline[2].split(":")[1]
            _resetCmd_()
            return sline[2].split('"')[3]
    
    
    
    
    

def _runRagCmd_(cmd, value, mode):
    result = False
    if cmd:
        _writeQuery_(cmd, value, mode)
        print cmd
     
    data =  _runRagProcess_() 
    if data:
        result = _readQuery_(data)
    
    # Only reset the command after to have finished the read
    _resetCmd_()
    return result   
    

def _writeQuery_(cmd, value, mode):
    file = open(_ragCmdPath, mode)    
    file.write('widget "'+cmd+'"')
    if value:
        file.write(' '+value+'\n')
    else:
        file.write('\n')
        
    file.close() 
    
def _readQuery_(stdOut):
    result = []
    read = False
    for line in iter(stdOut.readline,''):
        if "closing" in line:
            read = False
            return result
        if read == True:
            #print line.strip('\n\r')
            result.append(line.strip('\n\r'))
        elif "cmd.txt" in line:
            read = True           
      
    return result

"""Remove last commands from our Rag query""" 
def _resetCmd_():
    f = open(_ragCmdPath, 'w')
    f.close()
   

"""Run our Rag Command."""
def _runRagProcess_():
    if os.path.exists(_ragCmdPath):    
        cmd = "{path}/rag/ragCmd.exe {path2}"
        cmd = cmd.format(path=_config.ToolsBin, path2=_ragCmdPath)        
        return _processCmd_(cmd)
    return False


def _processCmd_(cmd):
    p = Popen(cmd,shell=True, stdout=PIPE, stderr=STDOUT)   
    return p.stdout
    
 
"""  EXAMPLE USAGE """
    


#AddCmd("Camera/Create camera widgets")
#AddCmd("Camera/Free camera/Override streaming focus", "true" )
#AddCmd("Camera/Frame propagator/Override FOV", "true")
#AddCmd("Camera/Free camera/Position Y")
#AddCmd("Camera/Free camera/Position Z")
#x =RunCmd()
#print x








