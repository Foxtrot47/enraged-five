import sys
import os
import glob
import xml.etree.cElementTree

from pyfbsdk import *
from PySide import QtCore, QtGui

import RS.Utils.Scene
import RS.Config
import RS.Perforce


## Defines ##

OUTFITS_DIR = "{0}\\techart\\etc\\config\\characters\\outfits\\".format( RS.Config.Tool.Path.Root )


## Functions ##

def LoadOutfits():
    '''
    Loads all outfits from the XML files.  Returns each collection as a dictionary.
    '''
    RS.Perforce.Sync( "{0}....xml".format( OUTFITS_DIR ) )
    
    collections = {}
    outfitFiles = glob.glob("{0}*.xml".format( OUTFITS_DIR ) )
    
    for outfitFile in outfitFiles:
        try:
            doc = xml.etree.cElementTree.parse( outfitFile )
            
        except:
            QtGui.QMessageBox.warning( None, "Rockstar", "An unknown error occurred while trying to read the outfit definition file:\n\n   {0}\n\nPlease check that the XML syntax is correct.  A missing or malformed XML tag is the likely cause.".format( os.path.basename( outfitFile ) ) )
            continue
        
        root = doc.getroot()
        
        if root:
            namespace = root.get( "namespace" )
            friendlyName = root.get( "friendlyName" )
            
            collection = OutfitCollection( namespace, friendlyName )
            
            outfitNodes = root.findall( "Outfit" )
            
            for outfitNode in outfitNodes:
                outfitName = outfitNode.get( "name" )
                
                modelNodes = outfitNode.find("Models").findall( "Model" )
                modelNames = []
                
                for modelNode in modelNodes:
                    modelName = modelNode.get( "name" )
                    modelNames.append( modelName )
                    
                outfit = collection.CreateOutfit( outfitName, modelNames )
                    
                accessoryNodes = outfitNode.find("Accessories").findall( "Accessory" )
                
                for accessoryNode in accessoryNodes:
                    modelName = accessoryNode.get( "name" )
                    friendlyName = accessoryNode.get( "friendlyName" )
                    outfit.AddAccessory( modelName, friendlyName )
                           
            collections[ collection.Namespace ] = collection
            
    return collections
    

## Classes ##

class OutfitCollection:
    '''
    A collection of outfits for a character.
    '''
    def __init__( self, namespace, friendlyName ):
        self.__namespace = namespace
        self.__friendlyName = friendlyName
        self.__outfits = {}
       
    
    ## Properties ##
     
    @property
    def Namespace( self ):
        return self.__namespace
        
    @property
    def FriendlyName( self ):
        return self.__friendlyName
        
    @property
    def Outfits( self ):
        return self.__outfits
    
    
    ## Methods ##
    
    def CreateOutfit( self, friendlyName, modelNameList ):
        '''
        Creates a new outfit for this collection.
        '''
        if friendlyName not in self.__outfits:
            outfit = Outfit( friendlyName, self.__namespace )
            
            for modelName in modelNameList:
                outfit.AddModelName( modelName )
                
            self.__outfits[ friendlyName ] = outfit
            
            return outfit
        
    def AddOutfit( self, outfit ):
        '''
        Adds an existing outfit to this colleciton.
        '''
        if outfit.Name not in self.__outfits:
            self.__outfits[ outfit.Name ] = outfit
            
        else:
            print "Outfit using the name {0} already exists in this collection!".format( outfit.Name )
          
    def Visible( self, outfitName, state ):
        '''
        Set the visibility of the supplied outfit name.
        '''
        if outfitName in self.__outfits:
            for key, val in self.__outfits.iteritems():
                if key != outfitName:
                    val.Visible( False )
                    
            self.__outfits[ outfitName ].Visible( state )
            
        else:
            print "Outfit using the name {0} does not exist in this collection!".format( outfit.Name )

class OutfitAccessory:
    def __init__( self, parentOutfit, modelName, friendlyName ):
        self.__modelName = modelName
        self.__friendlyName = friendlyName
        self.__parentOutfit = parentOutfit
        self.__model = None
        
        if parentOutfit.RootModel != None:
            self.__model = parentOutfit.FindModel( modelName, parentOutfit.RootModel )
            

    ## Properties ##
        
    @property
    def Name( self ):
        return self.__modelName
        
    @property
    def FriendlyName( self ):
        return self.__friendlyName
        
    @property
    def Model( self ):
        return self.__model
    
    @property
    def ParentOutfit( self ):
        return self.__parentOutfit
      
      
    ## Methods ##
      
    def IsVisible( self ):
        if self.__model != None:
            return self.__model.IsVisible
            
        return False
        
    def Visible( self, state ):
        if self.__model != None:
            self.__model.Show = state
            self.__model.Visibility = state

class Outfit:
    '''
    Represents an outfit for a character.
    '''
    def __init__( self, outfitName, namespace ):
        self.__namespace = namespace
        self.__name = outfitName
        self.__models = []
        
        # IG character component geometry is organized under a 'Geometry' node.
        if namespace.lower().startswith( "ig_" ):
            self.__rootModel = RS.Utils.Scene.FindModelByName( "{0}:Geometry".format( self.__namespace ), True )
            
        else:
            self.__rootModel = None
            
            dummy = RS.Utils.Scene.FindModelByName( "{0}:Dummy01".format( self.__namespace ), True )
            
            if dummy:
                for child in dummy.Children:
                    if child.Name.startswith( self.__namespace ) or child.Name.lower() == "geometry":
                        self.__rootModel = child
            
        self.__accessories = {}
        
    
    ## Properties ##
    
    @property
    def Name( self ):
        return self.__name
        
    @property
    def Namespace( self ):
        return self.__namespace
        
    @property
    def Models( self ):
        return self.__models
        
    @property
    def Accessories( self ):
        return self.__accessories
    
    @property
    def RootModel( self ):
        return self.__rootModel
        
    
    ## Methods ##
    
    def FindModel( self, modelName, rootModel ):
        if rootModel != None:
            for child in rootModel.Children:
                if child.Name.lower().startswith( modelName.lower() ):
                    return child
                
    def AddAccessory( self, modelName, friendlyName ):
        if modelName not in self.__accessories:
            accessory = OutfitAccessory( self, modelName, friendlyName )
            self.__accessories[ modelName ] = accessory
            
        else:
            print "{0} already exists as an accessory for this outfit!".format( modelName )
        
    
    def AddModelName( self, modelName ):
        '''
        Add the name of a FBModel to this outfit.
        '''
        if modelName not in self.__models:
            model = self.FindModel( modelName, self.__rootModel )

            if model:
                self.__models.append( model )
            
    def IsVisible( self ):
        '''
        Returns whether all of the models for this outfit are visible.
        '''
        for model in self.__models:
            if not model.IsVisible:
                return False
                        
        return True
        
    def Visible( self, state ):
        '''
        Sets the visiblity of the outfit.
        '''
        for model in self.__rootModel.Children:
            if model in self.__models:
                model.Show = state
                model.Visibility = state
                model.VisibilityInheritance = not state
                
            else:
                model.Show = False
                model.Visibility = False
                model.VisibilityInheritance = False
           