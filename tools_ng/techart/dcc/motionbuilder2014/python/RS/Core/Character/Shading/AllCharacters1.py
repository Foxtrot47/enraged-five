###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AmbientShading_1_AllCharacters.py
## Written And Maintained By: Kathryn Bodey
## Contributors: -
## Description: Sets Ambient Shading nodes to 0 for all characters in the scene.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo
import RS.Utils.Scene

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_AmbientShading_AllCharacters
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_AmbientShading_1_AllCharacters():

    for lCharacter in glo.gCharacters:
    
        lCharHips = lCharacter.GetModel(FBBodyNodeId.kFBHipsNodeId)
        
        lRootRefNull = RS.Utils.Scene.GetParent(lCharHips)
        
        lCharacterModelList = []
        RS.Utils.Scene.GetChildren(lRootRefNull, lCharacterModelList)
        #lCharacterModelList.extend(eva.gChildList)
        #del eva.gChildList[:]
        
        for iModel in lCharacterModelList:
            for iComp in iModel.Components:
                if iComp.ClassName() == "FBMaterial":
                    if "_Contact" in iComp.Name:
                        None
                    else:
                        iComp.Ambient = FBColor(1,1,1)
