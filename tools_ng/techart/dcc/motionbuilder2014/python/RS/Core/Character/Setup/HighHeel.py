###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_HighHeel_Setup
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Function to set up characters - ingame and cutscene
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

import os

from pyfbsdk import *

import RS.Globals as glo
import RS.Utils.Scene
import RS.Config

global heelsCheckList

heelsCheckList = False

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

gHighHeels = [

             [ 'EO_L_Foot', 'SKEL_L_Foot', FBVector3d(1.49012e-008, -1.49012e-008, -1.49012e-008), FBVector3d(0.76604, -9.30131e-007, -35.0008),  FBVector3d(1, 1, 1)],
             [ 'EO_L_Toe',  'EO_L_Foot',   FBVector3d(0.139711, 1.49012e-008, 2.98023e-008),       FBVector3d(179.633, -0.265396, 54.1545),       FBVector3d(1, 1, 1)],
             [ 'EO_R_Foot', 'SKEL_R_Foot', FBVector3d(-1.49012e-008, 1.49012e-008, 0),             FBVector3d(-0.766098, 4.54281e-006, -35.0007), FBVector3d(1, 1, 1)],
             [ 'EO_R_Toe',  'EO_R_Foot',   FBVector3d(0.139712, -1.49012e-008, 0),                 FBVector3d(-179.633, 0.26535, 54.1543),        FBVector3d(1, 1, 1)],
            
             ]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_HeelsSetup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_HeelsSetup():
    
    global heelsCheckList
    
    #First find if the character already has the heel slider nulls present in the file
    lRectHeelHeight = None
    lHeelHeight = None
    lHeelHeightText = None
    lRectHeelHeight = RS.Utils.Scene.FindModelByName("RECT_HeelHeight") 
    lHeelHeight = RS.Utils.Scene.FindModelByName("HeelHeight")
    lHeelHeightText = RS.Utils.Scene.FindModelByName("TEXT_HeelHeight")
    
    if lRectHeelHeight != None:
        if lHeelHeight != None and lHeelHeightText != None:
        
            #Heel Slider null exist! We need to set DOFs on each null, so aniamtors can only use them in one way.
            lRectHeelHeight.PropertyList.Find("Enable Transformation").Data = False  
            lRectHeelHeight.PropertyList.Find("Enable Selection").Data = False  
                
            lHeelHeight.PropertyList.Find("Enable Translation DOF").Data = True
            lHeelHeight.PropertyList.Find("TranslationMinX").Data = True
            lHeelHeight.PropertyList.Find("TranslationMinY").Data = True
            lHeelHeight.PropertyList.Find("TranslationMinZ").Data = True
            lHeelHeight.PropertyList.Find("TranslationMin").Data = FBVector3d(0,0,0)
            lHeelHeight.PropertyList.Find("TranslationMaxX").Data = True
            lHeelHeight.PropertyList.Find("TranslationMaxY").Data = True
            lHeelHeight.PropertyList.Find("TranslationMaxZ").Data = True
            lHeelHeight.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0)  
        
            lHeelHeightText.PropertyList.Find("Enable Transformation").Data = False  
            lHeelHeightText.PropertyList.Find("Enable Selection").Data = False 
        
            #Create a custom property for the slider, Min 0 Max 1
            lSlider = lHeelHeight.PropertyCreate('HeelHeight', FBPropertyType.kFBPT_double, 'Number',True, True, None)
            lSlider.SetMin(0)
            lSlider.SetMax(1)
            lSlider.SetAnimated(True)
            
            #Create a relation constraint to feed the trns info from HeelHeight to the custom property
            lRelationCon = FBConstraintRelation('HeelHeight')
            
            lSender = lRelationCon.SetAsSource(lHeelHeight)
            lReceiver = lRelationCon.ConstrainObject(lHeelHeight)
            lVectorNumber = lRelationCon.CreateFunctionBox('Converters', 'Vector to Number')
            
            lSender.UseGlobalTransforms = False  
            lReceiver.UseGlobalTransforms = False 
            
            lRelationCon.SetBoxPosition(lSender, 100, 0)
            lRelationCon.SetBoxPosition(lVectorNumber, 500, 0)
            lRelationCon.SetBoxPosition(lReceiver, 700, 0)
            
            RS.Utils.Scene.ConnectBox(lSender, 'Lcl Translation', lVectorNumber, 'V')   
            RS.Utils.Scene.ConnectBox(lVectorNumber, 'Y', lReceiver, 'HeelHeight')              
            
            lRelationCon.Active = True
            
            #Parent the RECT HeelHeight to the mover
            lMover = None
            lMover = RS.Utils.Scene.FindModelByName("mover")
            
            if lMover != None:
                lRectHeelHeight.Parent = lMover
            
            #Adjust the Slider UI scale, position and the control's start translation value
            lRectHeelHeight.Translation.Data = FBVector3d(-0.5, 0, -0.8)
            lRectHeelHeight.Scaling.Data = FBVector3d(0.2, 0.2, 0.2)
            lHeelHeight.Translation.Data = FBVector3d(0, 1, 0)
            
            lRectHeelHeight.PropertyList.Find("TranslationActive").Data = True

            lRectHeelHeight.PropertyList.Find("TranslationMinX").Data = True
            lRectHeelHeight.PropertyList.Find("TranslationMinY").Data = True
            lRectHeelHeight.PropertyList.Find("TranslationMinZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("TranslationMaxX").Data = True
            lRectHeelHeight.PropertyList.Find("TranslationMaxY").Data = True
            lRectHeelHeight.PropertyList.Find("TranslationMaxZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("TranslationMin").Data = FBVector3d(-0.5,0,-0.8)
            lRectHeelHeight.PropertyList.Find("TranslationMax").Data = FBVector3d(-0.5,0,-0.8)
            
            lRectHeelHeight.PropertyList.Find("RotationActive").Data = True
            
            lRectHeelHeight.PropertyList.Find("RotationMinX").Data = True
            lRectHeelHeight.PropertyList.Find("RotationMinY").Data = True
            lRectHeelHeight.PropertyList.Find("RotationMinZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("RotationMaxX").Data = True
            lRectHeelHeight.PropertyList.Find("RotationMaxY").Data = True
            lRectHeelHeight.PropertyList.Find("RotationMaxZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,180)
            lRectHeelHeight.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,180)
            
            lRectHeelHeight.PropertyList.Find("ScalingActive").Data = True
            
            lRectHeelHeight.PropertyList.Find("ScalingMinX").Data = True
            lRectHeelHeight.PropertyList.Find("ScalingMinY").Data = True
            lRectHeelHeight.PropertyList.Find("ScalingMinZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("ScalingMaxX").Data = True
            lRectHeelHeight.PropertyList.Find("ScalingMaxY").Data = True
            lRectHeelHeight.PropertyList.Find("ScalingMaxZ").Data = True
            
            lRectHeelHeight.PropertyList.Find("ScalingMin").Data = FBVector3d(0.2,0.2,0.2)
            lRectHeelHeight.PropertyList.Find("ScalingMax").Data = FBVector3d(0.2,0.2,0.2)
                        
            heelsCheckList = True
            
        #Launch a warning for the user if any of the nulls are missing
        else:
            heelsCheckList = "False    [Error: RECT_HeelHeight was found, but 'HeelHeight' and/or 'TEXT_HeelHeight' are missing]"
            
          
    #Heel Slider nulls do not exist in MB file
    else:
        if "ingame" in glo.gApp.FBXFileName:
                           
            for i in range(len(gHighHeels)):
                            
                lModel = RS.Utils.Scene.FindModelByName(gHighHeels[i][0])
                
                print lModel
                
                if lModel:
                    
                    print 'Found: ' + lModel.Name + '. Dont not need to clone the feet.'
                    
                else:
                    
                    lParent = RS.Utils.Scene.FindModelByName(gHighHeels[i][1])
                    lParentClone = lParent.Clone()
                    lParentClone.Name = gHighHeels[i][0]
                    lParentClone.Parent = lParent
                    glo.gScene.Evaluate()
                    lParentClone.Translation.Data = gHighHeels[i][2]
                    glo.gScene.Evaluate()
                    lParentClone.Rotation.Data = gHighHeels[i][3]
                    glo.gScene.Evaluate()
                    lParentClone.Scaling.Data = gHighHeels[i][4]
                    glo.gScene.Evaluate()
    
            lCube_L = FBCreateObject( "Browsing/Templates/Elements/Primitives", "Cube", "Cube" )
            lCube_L.Parent = RS.Utils.Scene.FindModelByName("EO_L_Foot")
            lCube_L.Name = "EO_L_Proxy"
            glo.gScene.Evaluate()
            lCube_L.Translation.Data = FBVector3d(0.15, -0.01, 0)
            glo.gScene.Evaluate()
            lCube_L.Rotation.Data = gHighHeels[1][3]
            glo.gScene.Evaluate()
            lCube_L.Scaling.Data = FBVector3d(0.01, 0.006, 0.005)
            glo.gScene.Evaluate()
            
            lCube_R = FBCreateObject( "Browsing/Templates/Elements/Primitives", "Cube", "Cube" )
            lCube_R.Parent = RS.Utils.Scene.FindModelByName("EO_R_Foot")
            lCube_R.Name = "EO_R_Proxy"
            glo.gScene.Evaluate()
            lCube_R.Translation.Data = FBVector3d(0.15, -0.01, 0)
            glo.gScene.Evaluate()
            lCube_R.Rotation.Data = gHighHeels[3][3]
            glo.gScene.Evaluate()
            lCube_R.Scaling.Data = FBVector3d(0.01, 0.006, 0.005)
            glo.gScene.Evaluate()

            heelFBX = '{0}\\wildwest\\etc\\config\\characters\\HighHeels.FBX'.format (RS.Config.Tool.Path.Root)
            
            options = FBFbxOptions( True, heelFBX )
            options.SetAll( FBElementAction.kFBElementActionMerge, False )
            options.Models = FBElementAction.kFBElementActionMerge
            options.Constraints = FBElementAction.kFBElementActionMerge
            options.TakeSpan = FBTakeSpanOnLoad.kFBLeaveAsIs
            
            isFile = os.path.isfile(heelFBX)
            if isFile == True:
                                
                glo.gApp.FileMerge(heelFBX, False, options )

                lRectHeelHeight = RS.Utils.Scene.FindModelByName("RECT_HeelHeight")
                lHeelHeight = RS.Utils.Scene.FindModelByName("HeelHeight")
                
                lMainGroup = None
                
                for iGroup in glo.gGroups:
                    
                    if iGroup.Name == os.path.basename(glo.gApp.FBXFileName).partition(".")[0]:
                        
                        lMainGroup = iGroup
                
                if lHeelHeight:
                                       
                    lRectHeelHeight.Parent = RS.Utils.Scene.FindModelByName("mover")
                    lRectHeelHeight.Translation.Data = FBVector3d(-0.5, 0, -0.8)
                    glo.gScene.Evaluate()
                    lRectHeelHeight.Rotation.Data = FBVector3d(0, 0, 180)
                    glo.gScene.Evaluate()
                    lRectHeelHeight.Scaling.Data = FBVector3d(0.2, 0.2, 0.2)
                    glo.gScene.Evaluate()
                    
                    lRectHeelHeight.PropertyList.Find("RotationActive").Data = True
                    glo.gScene.Evaluate()
                    
                    lHeelHeight.Translation.Data = FBVector3d(0, 1, 0)
                    glo.gScene.Evaluate()
                    
                    #set lock options url:bugstar:1363950
                    lRectHeelHeight.PropertyList.Find("Enable Transformation").Data = False  
                    lRectHeelHeight.PropertyList.Find("Enable Selection").Data = False  
                
                    lHeelHeight.PropertyList.Find("ScalingActive").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMinX").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMinY").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMinZ").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMaxX").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMaxY").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMaxZ").Data = True
                    lHeelHeight.PropertyList.Find("ScalingMin").Data = FBVector3d( 1, 1, 1 )
                    lHeelHeight.PropertyList.Find("ScalingMax").Data = FBVector3d( 1, 1, 1 )
                
                #bool to return to checkList in rs_AssetSetupCharacters.py
                heelsCheckList = True
                
            else:
                heelsCheckList = "False    [Error: {0} cannot be found]".format( heelFBX )
                
            
            
        else:
            heelsCheckList = "False    [This character is not being setup in the /characters/models/ingame folder,\n                                                      therefore it is assumed proxy heels setup is not required.]"
