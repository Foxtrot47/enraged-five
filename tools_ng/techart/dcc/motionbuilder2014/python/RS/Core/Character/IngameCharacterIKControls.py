from pyfbsdk import *

from RS.Utils import Path, Scene
from RS import Config, Globals
from RS import ProjectData

global faceDirectionCheckList
global lookAtCheckList

faceDirectionCheckList = False
lookAtCheckList = False

# get control prefix, which is project-specifc
controlPrefix, controlLookAtPrefix = ProjectData.data.GetHelperPrefix()

def rs_ConstraintsFolder():
    lFolder = None
    for iFolder in Globals.gFolders:
        if iFolder.Name == 'Constraints 1':
            lFolder = iFolder
    if lFolder == None:
        lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder("Constraints 1", lPlaceholder)
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        FBSystem().Scene.Evaluate()
        lPlaceholder.FBDelete()
    return lFolder

def rs_IK_ConstraintsFolder():
    lFolder = None
    for iFolder in Globals.gFolders:
        if iFolder.Name == '{0}_Constraints'.format(controlPrefix):
            lFolder = iFolder
    if lFolder == None:
        lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder("{0}_Constraints".format(controlPrefix), lPlaceholder)
        FBSystem().Scene.Evaluate()
        lPlaceholder.FBDelete()
    if lFolder != None:
        lConFolder = rs_ConstraintsFolder()
    return lFolder

def rs_MaterialsFolder():
    lMatFolder = None
    for iFolder in Globals.gFolders:
        if "Material" in iFolder.Name:
            lMatFolder = iFolder
    if lMatFolder == None:
        lPlaceholder = FBMaterial('Remove_Me')
        lMatFolder = FBFolder("Materials", lPlaceholder)
        lTag = lMatFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        FBSystem().Scene.Evaluate()
        lPlaceholder.FBDelete()
    return lMatFolder

def rs_FacingDirectionArrow():
    global faceDirectionCheckList
    # set up project's dict
    coneDict = ProjectData.data.GetFacingDirectionArrowDict()
    # check that at least one null exists int he file, from the dict
    for key in coneDict.iterkeys():
        helperNull = Scene.FindModelByName(key)
        if not helperNull:
            # null doesnt exist, get out of script
            faceDirectionCheckList = "False    [Error: 'IK_Root' Null is missing"
            return

    lFileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lMover = Scene.FindModelByName("mover")
    # create a zeroing null, so the facing direction can start at zero.
    zeroingNull = FBModelNull( 'ZeroingNull' )
    zeroingNull.Parent = lMover
    zeroingNull.SetVector(FBVector3d(0,0,0), FBModelTransformationType.kModelRotation, False)
    zeroingNull.SetVector(FBVector3d(-90,0,0), FBModelTransformationType.kModelRotation, True)
    zeroingNull.SetVector(FBVector3d(0,0,0), FBModelTransformationType.kModelTranslation, True)
    FBSystem().Scene.Evaluate()
    # get main group, to use with cones created below
    lMainGroup = None
    for iGroup in Globals.gGroups:
        if iGroup.Name == lFileName:
            lMainGroup = iGroup
    # create relation constraint to be used for cones created below
    lRelationCon = FBConstraintRelation('FacingArrow_{0}Bones'.format(controlPrefix))
    # Create OH_FacingDirection Arrow
    for key, value in coneDict.iteritems():
        helperNull = Scene.FindModelByName(key)
        if helperNull:
            cone = FBCreateObject("Browsing/Templates/Elements/Primitives", "Cone", "Cone")
            cone.Name = value[0]
            cone.Parent = lMover
            cone.Show = True
            cone.PropertyList.Find("GeometricTranslation").Data = FBVector3d(0,10,0)
            cone.Selected = True
            cone.ShadingMode = FBModelShadingMode.kFBModelShadingWire
            cone.Selected = False
            tag = cone.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = "rs_Contacts"
            # cone properties
            cone.PropertyList.Find("GeometricTranslation").Data = FBVector3d(0,10,0)
            cone.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = FBVector3d(0, 0, 0)
            cone.PropertyList.Find("Rotation Pivot (Auto Offset)").Data = FBVector3d(0, 0, 0)
            cone.SetVector(FBVector3d(-90,0,0), FBModelTransformationType.kModelRotation, True)
            cone.SetVector(FBVector3d(0,value[1],0), FBModelTransformationType.kModelTranslation, True)
            FBSystem().Scene.Evaluate()
            # parent cone to zeroing null
            cone.Parent = zeroingNull
            FBSystem().Scene.Evaluate()
            if lMainGroup:
                lFacingDirecGroup = FBGroup(value[0])
                lFacingDirecGroup.ConnectSrc(cone)
                lMainGroup.ConnectSrc(lFacingDirecGroup)
                lFacingDirecGroup.Show = False
            # Create Relation Constraint
            lSender = lRelationCon.SetAsSource(cone)
            lRelationCon.SetBoxPosition(lSender, 0, 0)
            lSender.UseGlobalTransforms = False
            lReceiver = lRelationCon.ConstrainObject(helperNull)
            lRelationCon.SetBoxPosition(lReceiver, 350, 0)
            lReceiver.UseGlobalTransforms = False
            Scene.ConnectBox(lSender, 'Lcl Rotation', lReceiver, 'Lcl Rotation')
            FBSystem().Scene.Evaluate()
            cone.Scaling = FBVector3d(5.3,3.5,0.04)

            cone.PropertyList.Find("Enable Translation DOF").Data = True
            cone.PropertyList.Find("TranslationMinX").Data = True
            cone.PropertyList.Find("TranslationMinY").Data = True
            cone.PropertyList.Find("TranslationMinZ").Data = True
            cone.PropertyList.Find("TranslationMin" ).Data = FBVector3d(0,0,0)
            cone.PropertyList.Find("TranslationMaxX").Data = True
            cone.PropertyList.Find("TranslationMaxY").Data = True
            cone.PropertyList.Find("TranslationMaxZ").Data = True
            cone.PropertyList.Find("TranslationMax").Data = FBVector3d(0,0,value[1])
    # snap constraint and add to a folder
    lRelationCon.Snap()
    lConFolder = rs_IK_ConstraintsFolder()
    lConFolder.Items.append(lRelationCon)
    #bool to return to checkList in rs_AssetSetupCharacters.py
    faceDirectionCheckList = True

    # OH_Mover setup url:bugstar:2364332
    # create offset cone and set properties
    mover = Scene.FindModelByName('mover')
    ohMover = Scene.FindModelByName('OH_Mover')
    if ohMover:
        cone = FBCreateObject("Browsing/Templates/Elements/Primitives", "Cone", "Cone")
        cone.Name = 'OH_MoverOffset'
        cone.Parent = mover
        cone.Show = False
        cone.PropertyList.Find("GeometricTranslation").Data = FBVector3d(0,10,0)
        Scene.Align(cone, mover, True, True, True, True, True, True)
        scaleVector = FBVector3d(5.3, 3.4, 0.04)
        cone.SetVector(scaleVector, FBModelTransformationType.kModelScaling)
        rotationVector = FBVector3d(90, 0, 180)
        cone.SetVector(rotationVector, FBModelTransformationType.kModelRotation)
        cone.Selected = True
        cone.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        cone.Selected = False
        tag = cone.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = "rs_Contacts"

        FBSystem().Scene.Evaluate()

        # Create Relation Constraint
        sender = lRelationCon.SetAsSource(cone)
        lRelationCon.SetBoxPosition(sender, 0, 150)
        sender.UseGlobalTransforms = False
        receiver = lRelationCon.ConstrainObject(ohMover)
        lRelationCon.SetBoxPosition(receiver, 350, 150)
        receiver.UseGlobalTransforms = False
        Scene.ConnectBox(sender, 'Lcl Rotation', receiver, 'Lcl Rotation')

        # OH Mover Group setup
        lMainGroup = None
        for iGroup in Globals.gGroups:
            if iGroup.Name == lFileName:
                lMainGroup = iGroup
        if lMainGroup != None:
            ohMoverGroup = FBGroup("OH_MoverOffset")
            lMainGroup.ConnectSrc(ohMoverGroup)
            ohMoverGroup.ConnectSrc(cone)
            ohMoverGroup.Pickable = True
            ohMoverGroup.Transformable = True
            ohMoverGroup.Show = False

    # OH_Torso setup url:bugstar:2413523
    # create offset cone and set properties
    ohTorsoDir = Scene.FindModelByName('OH_TorsoDir')
    ohUpperFixupDirection = Scene.FindModelByName('OH_UpperFixupDirection')
    
    if ohTorsoDir and ohUpperFixupDirection and mover:
        cone = FBCreateObject("Browsing/Templates/Elements/Primitives", "Cone", "Cone")
        cone.Name = 'OH_TorsoDirection'
        cone.Parent = ohUpperFixupDirection
        cone.Show = False
        cone.PropertyList.Find("GeometricTranslation").Data = FBVector3d(0,10,0)
        Scene.Align(cone, mover, True, True, True, True, True, True)
        scaleVector = FBVector3d(5.3, 3.4, 0.04)
        cone.SetVector(scaleVector, FBModelTransformationType.kModelScaling)
        cone.PropertyList.Find("Enable Translation DOF").Data = True
        cone.PropertyList.Find("TranslationMinX").Data = True
        cone.PropertyList.Find("TranslationMinY").Data = True
        cone.PropertyList.Find("TranslationMinZ").Data = True
        cone.PropertyList.Find("TranslationMin" ).Data = FBVector3d(0,0,0)
        cone.PropertyList.Find("TranslationMaxX").Data = True
        cone.PropertyList.Find("TranslationMaxY").Data = True
        cone.PropertyList.Find("TranslationMaxZ").Data = True
        cone.PropertyList.Find("TranslationMax").Data = FBVector3d(0,0,0)
        cone.Selected = True
        cone.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        cone.Selected = False
        tag = cone.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = "rs_Contacts"
        FBSystem().Scene.Evaluate()

        # Create Relation Constraint
        sender = lRelationCon.SetAsSource(cone)
        lRelationCon.SetBoxPosition(sender, 0, 300)
        sender.UseGlobalTransforms = False
        receiver = lRelationCon.ConstrainObject(ohTorsoDir)
        lRelationCon.SetBoxPosition(receiver, 350, 300)
        receiver.UseGlobalTransforms = False
        Scene.ConnectBox(sender, 'Lcl Rotation', receiver, 'Lcl Rotation')
        # OH_TorsoDirection Group setup
        lMainGroup = None
        for iGroup in Globals.gGroups:
            if iGroup.Name == lFileName:
                lMainGroup = iGroup
        if lMainGroup != None:
            ohTorsoGroup = FBGroup("OH_TorsoDirection")
            lMainGroup.ConnectSrc(ohTorsoGroup)
            ohTorsoGroup.ConnectSrc(cone)
            ohTorsoGroup.Pickable = True
            ohTorsoGroup.Transformable = True
            ohTorsoGroup.Show = False


def rs_HeadLookAt():
    global lookAtCheckList
    lEyeLControl = None
    lEyeRControl = None 
    for iComponent in Globals.gComponents:
        if iComponent.Name.lower() == 'ctrl_l_eye':
            lEyeLControl = iComponent
        elif iComponent.Name.lower() == 'eye_l_ctrl':
            lEyeLControl = iComponent
        elif iComponent.Name.lower() == 'ctrl_r_eye':
            lEyeRControl = iComponent
        elif iComponent.Name.lower() == 'eye_r_ctrl':
            lEyeRControl = iComponent

    lIKHead = None
    lIKHead = Scene.FindModelByName('{0}'.format(controlLookAtPrefix))
    if lIKHead:
        lFileName = Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        lMover = Scene.FindModelByName("mover")
        lHead = Scene.FindModelByName("SKEL_Head")
        # IK_HEAD_LOOKAT & TARGETPARENT - Create Nulls
        lLookAtCube = FBModelCube('{0}_Lookat'.format(controlLookAtPrefix))
        lLookAtCube.Show = True
        lLookAtCube.Look = FBMarkerLook.kFBMarkerLookHardCross
        lLookAtCube.Scaling = FBVector3d(4, 4, 4)
        lLookAtCube.Selected = True
        lLookAtCube.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        lLookAtCube.Selected = False 
        lTag = lLookAtCube.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts" 
        lLookAtCubeMat = FBMaterial("{0}_Lookat".format(controlLookAtPrefix))
        lLookAtCubeMat.Diffuse = FBColor(1,0,0)
        lTag = lLookAtCubeMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Materials" 
        lLookAtCube.Materials.append(lLookAtCubeMat)
        lMatFolder = rs_MaterialsFolder()
        lMatFolder.Items.append(lLookAtCubeMat)
        lTargetParentNull = FBModelNull('TargetParent')
        lTag = lTargetParentNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts" 
        lVector = FBVector3d()
        lHead.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)   
        lX = lVector[0]
        lY = lVector[1] + 6
        lZ = lVector[2]
        lTargetParentNull.SetVector(FBVector3d(lX, lY, lZ), FBModelTransformationType.kModelTranslation, True)
        lLookAtCube.Parent = lTargetParentNull
        lLookAtCube.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,80)
        lLookAtCube.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
        FBSystem().Scene.Evaluate()
        # MOVERSPACETARGET - Create Null and Constraint
        lMoverSpaceNull = FBModelNull('MoverSpaceTarget')
        lTag = lMoverSpaceNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        #Parent Con 
        lMoverParentConst = FBConstraintManager().TypeCreateConstraint(3)
        lMoverParentConst.Name = ("MoverTracer")
        lMoverParentConst.ReferenceAdd (0,lMoverSpaceNull)
        lMoverParentConst.ReferenceAdd (1,lMover)
        lMoverParentConst.Active = True
        lMoverParentConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lMoverParentConst)
        FBSystem().Scene.Evaluate()
        # HEADSPACETARGET - Create Null and Constraint
        lHeadSpaceNull = FBModelNull('HeadSpaceTarget')
        lTag = lHeadSpaceNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        #Parent Con
        lHeadParentConst = FBConstraintManager().TypeCreateConstraint(3)
        lHeadParentConst.Name = ("HeadTracer")
        lHeadParentConst.ReferenceAdd (0,lHeadSpaceNull)
        lHeadParentConst.ReferenceAdd (1,lHead)
        lHeadParentConst.Active = True  
        lHeadParentConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lHeadParentConst) 
        FBSystem().Scene.Evaluate()
        # EYEORIGIN - Create Null and Constraint   
        lEyeOriginNull = FBModelNull('EyeOrigin')
        lTag = lEyeOriginNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        lEyeOriginNull.Parent = lHeadSpaceNull
        lEyeOriginNull.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
        lVector = FBVector3d()
        lHeadSpaceNull.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        lX = lVector[0]
        lY = lVector[1] + 6
        lZ = lVector[2] + 9
        lEyeOriginNull.SetVector(FBVector3d(lX, lY, lZ), FBModelTransformationType.kModelTranslation, True)
        FBSystem().Scene.Evaluate()
        #Rotation Con
        lEyeRotationConst = FBConstraintManager().TypeCreateConstraint(10)
        lEyeRotationConst.Name = ("EyeOriginRotation")
        lEyeRotationConst.ReferenceAdd (0,lEyeOriginNull)
        lEyeRotationConst.ReferenceAdd (1,lHeadSpaceNull)
        lEyeRotationConst.Snap()  
        lEyeRotationConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lEyeRotationConst)
        FBSystem().Scene.Evaluate()
        # LOOK_AT_DIRECTION - Create Null and Constraint 
        lLookAtDirectionNull = FBModelNull('Look_At_Direction')
        lLookAtDirectionNull.Show = True
        lLookAtDirectionNull.PropertyList.Find("Enable Selection").Data = False
        lLookAtDirectionNull.Size = 500
        lTag = lLookAtDirectionNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        lLookAtDirectionNull.Parent = lEyeOriginNull
        lLookAtDirectionNull.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
        lLookAtDirectionNull.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
        FBSystem().Scene.Evaluate()
        #ik_head limits
        if lIKHead != None:
            lLimitMax = lLookAtCube.PropertyCreate('Limit_Max', FBPropertyType.kFBPT_Vector3D, 'Vector', True, True, None)
            lLimitMax.Data = FBVector3d(20,0,17)
            lLimitMax.SetAnimated(True)
            lLimitMin = lLookAtCube.PropertyCreate('Limit_Min', FBPropertyType.kFBPT_Vector3D, 'Vector', True, True, None)
            lLimitMin.Data = FBVector3d(-20,0,-20)
            lLimitMin.SetAnimated(True)
        #Relation Con
        lRelationCon = FBConstraintRelation('Head_Delta')
        lLookAtCubeSender = lRelationCon.SetAsSource(lLookAtCube)
        lRelationCon.SetBoxPosition(lLookAtCubeSender, 0, 0)
        lLookAtCubeSender.UseGlobalTransforms = False
        lSender = lRelationCon.SetAsSource(lLookAtDirectionNull)
        lRelationCon.SetBoxPosition(lSender, 0, 300)
        lSender.UseGlobalTransforms = False
        lClamp = lRelationCon.CreateFunctionBox( 'Vector', 'Scale And Offset (Vector)' )
        lRelationCon.SetBoxPosition(lClamp, 600, 0)
        lReceiver = lRelationCon.ConstrainObject(lIKHead)
        lRelationCon.SetBoxPosition(lReceiver, 1000, 0)
        lReceiver.UseGlobalTransforms = False
        Scene.ConnectBox(lSender, 'Lcl Rotation', lClamp, 'X')
        Scene.ConnectBox(lLookAtCubeSender, 'Limit_Max', lClamp, 'Clamp Max')
        Scene.ConnectBox(lLookAtCubeSender, 'Limit_Min', lClamp, 'Clamp Min')
        Scene.ConnectBox(lClamp, 'Result', lReceiver, 'Lcl Rotation')
        lRelationCon.Snap()
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lRelationCon)
        FBSystem().Scene.Evaluate()
        #Aim Con
        lLookAtAimConst = FBConstraintManager().TypeCreateConstraint(0)
        lLookAtAimConst.Name = ("Lookat_Aim")
        lLookAtAimConst.ReferenceAdd (0,lLookAtDirectionNull)
        lLookAtAimConst.ReferenceAdd (1,lLookAtCube)
        lLookAtAimConst.ReferenceAdd (2,lHead)
        lLookAtAimConst.PropertyList.Find("Rotation Offset").Data = FBVector3d(0,0,0)
        lLookAtAimConst.PropertyList.Find("Aim Vector").Data = FBVector3d(0,1,0)
        lLookAtAimConst.PropertyList.Find("Up Vector").Data = FBVector3d(1,0,0)
        lLookAtAimConst.PropertyList.Find("World Up Type").Data = 2
        lLookAtAimConst.PropertyList.Find("World Up Vector").Data = FBVector3d(1,0,0)
        lLookAtAimConst.Active = True
        lLookAtAimConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lLookAtAimConst)
        FBSystem().Scene.Evaluate()
        #Constraining the Lookat Control
        lMoverSpaceParentConst = FBConstraintManager().TypeCreateConstraint(3)
        lMoverSpaceParentConst.Name = ("MoverSpaceConstraint")
        lMoverSpaceParentConst.ReferenceAdd (0,lTargetParentNull)
        lMoverSpaceParentConst.ReferenceAdd (1,lMoverSpaceNull)
        lMoverSpaceParentConst.Weight = 0
        lMoverSpaceParentConst.Weight.SetAnimated(True)
        lMoverSpaceParentConst.Active = True
        lMoverSpaceParentConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lMoverSpaceParentConst) 
        FBSystem().Scene.Evaluate()
        lHeadSpaceParentConst = FBConstraintManager().TypeCreateConstraint(3)
        lHeadSpaceParentConst.Name = ("HeadSpaceConstraint")
        lHeadSpaceParentConst.ReferenceAdd (0,lTargetParentNull)
        lHeadSpaceParentConst.ReferenceAdd (1,lHeadSpaceNull)
        lHeadSpaceParentConst.Weight.SetAnimated(True)
        lHeadSpaceParentConst.Snap()
        lHeadSpaceParentConst.Lock = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lHeadSpaceParentConst) 
        FBSystem().Scene.Evaluate()
        # LOOKAT SWITCH - Create Custom Property and Constraint 
        lLookAtTag = lLookAtCube.PropertyCreate('Follow_Head', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
        lLookAtTag.SetAnimated(True)
        lLookAtCube.PropertyList.Find("Follow_Head").Data = True
        lSwitchRelationCon = FBConstraintRelation('FollowHead_Bool')
        lSender = lSwitchRelationCon.SetAsSource(lLookAtCube)
        lSwitchRelationCon.SetBoxPosition(lSender, 0, 0)
        lCondBox = lSwitchRelationCon.CreateFunctionBox( 'Number', 'IF Cond Then A Else B' )
        lSwitchRelationCon.SetBoxPosition(lCondBox, 400, 0)
        lCondBoxa = Scene.FindAnimationNode(lCondBox.AnimationNodeInGet(),'a')
        lCondBoxa.WriteData([0])
        lCondBoxb = Scene.FindAnimationNode(lCondBox.AnimationNodeInGet(),'b')
        lCondBoxb.WriteData([100])
        lCondBox2 = lSwitchRelationCon.CreateFunctionBox( 'Number', 'IF Cond Then A Else B' )
        lSwitchRelationCon.SetBoxPosition(lCondBox2, 400, 100)
        lCondBox2a = Scene.FindAnimationNode(lCondBox2.AnimationNodeInGet(),'a')
        lCondBox2a.WriteData([100])
        lCondBox2b = Scene.FindAnimationNode(lCondBox2.AnimationNodeInGet(),'b')
        lCondBox2b.WriteData([0])
        lMoverReceiver = lSwitchRelationCon.ConstrainObject(lMoverSpaceParentConst)
        lSwitchRelationCon.SetBoxPosition(lMoverReceiver, 700, 0)
        lHeadReceiver = lSwitchRelationCon.ConstrainObject(lHeadSpaceParentConst)
        lSwitchRelationCon.SetBoxPosition(lHeadReceiver, 700, 100)
        Scene.ConnectBox(lSender, 'Follow_Head', lCondBox, 'Cond')
        Scene.ConnectBox(lCondBox, 'Result', lMoverReceiver, 'Weight')
        Scene.ConnectBox(lSender, 'Follow_Head', lCondBox2, 'Cond')
        Scene.ConnectBox(lCondBox2, 'Result', lHeadReceiver, 'Weight')
        lSwitchRelationCon.Snap()
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lSwitchRelationCon)
        FBSystem().Scene.Evaluate()
        # FACIAL RIG EYE CONTROL - Create Relation Constraint   
        if lEyeLControl != None and lEyeRControl != None: 
            lEyeLookAtRelationCon = FBConstraintRelation('IK_Head_Eye_Control')
            lIKHeadSender = lEyeLookAtRelationCon.SetAsSource(lIKHead)
            lEyeLookAtRelationCon.SetBoxPosition(lIKHeadSender, 0, 0)
            lIKHeadSender.UseGlobalTransforms = False
            lVec2NumBox = lEyeLookAtRelationCon.CreateFunctionBox( 'Converters', 'Vector to Number' )
            lEyeLookAtRelationCon.SetBoxPosition(lVec2NumBox ,350 ,0)
            lMultiplyBox = lEyeLookAtRelationCon.CreateFunctionBox( 'Number', 'Multiply (a x b)' )
            lMultiplyBoxb = Scene.FindAnimationNode(lMultiplyBox.AnimationNodeInGet(),'b')
            lMultiplyBoxb.WriteData([0.06])
            lEyeLookAtRelationCon.SetBoxPosition(lMultiplyBox, 600, -100) 
            lMultiplyBox2 = lEyeLookAtRelationCon.CreateFunctionBox( 'Number', 'Multiply (a x b)' )
            lMultiplyBox2b = Scene.FindAnimationNode(lMultiplyBox2.AnimationNodeInGet(),'b')
            lMultiplyBox2b.WriteData([-0.06])
            lEyeLookAtRelationCon.SetBoxPosition(lMultiplyBox2, 600, 100)
            lNum2VecBox = lEyeLookAtRelationCon.CreateFunctionBox( 'Converters', 'Number to Vector' )
            lEyeLookAtRelationCon.SetBoxPosition(lNum2VecBox , 900 , 0)
            lEyeLControlReceiver = lEyeLookAtRelationCon.ConstrainObject(lEyeLControl)
            lEyeLookAtRelationCon.SetBoxPosition(lEyeLControlReceiver, 1200, -100)
            lEyeLControlReceiver.UseGlobalTransforms = False
            lEyeRControlReceiver = lEyeLookAtRelationCon.ConstrainObject(lEyeRControl)
            lEyeLookAtRelationCon.SetBoxPosition(lEyeRControlReceiver, 1200, 100)
            lEyeRControlReceiver.UseGlobalTransforms = False
            Scene.ConnectBox(lIKHeadSender, 'Lcl Rotation', lVec2NumBox, 'V')
            Scene.ConnectBox(lVec2NumBox, 'X', lMultiplyBox, 'a')
            Scene.ConnectBox(lVec2NumBox, 'Z', lMultiplyBox2, 'a')
            Scene.ConnectBox(lMultiplyBox, 'Result', lNum2VecBox, 'X')
            Scene.ConnectBox(lMultiplyBox2, 'Result', lNum2VecBox, 'Y')
            Scene.ConnectBox(lNum2VecBox, 'Result', lEyeLControlReceiver, 'Lcl Translation')
            Scene.ConnectBox(lNum2VecBox, 'Result', lEyeRControlReceiver, 'Lcl Translation')
            lEyeLookAtRelationCon.Snap()
            lEyeLookAtRelationCon.Weight = 0
            lEyeLookAtRelationCon.Weight.SetAnimated(True)
            lConFolder = rs_IK_ConstraintsFolder()
            lConFolder.Items.append(lEyeLookAtRelationCon)
            FBSystem().Scene.Evaluate()
            # EYE C CONTROL HOOKUP - Create Custom Property and Constraint 
            lFacialRigTag = lLookAtCube.PropertyCreate('Eye_Track', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
            lFacialRigTag.SetAnimated(True)
            lEyeSwitchRelationCon = FBConstraintRelation('Eye_Track_Bool')
            lLookAtCubeSender = lEyeSwitchRelationCon.SetAsSource(lLookAtCube)
            lEyeSwitchRelationCon.SetBoxPosition(lLookAtCubeSender, 0, 0)
            lCondBox = lEyeSwitchRelationCon.CreateFunctionBox( 'Number', 'IF Cond Then A Else B' ) 
            lEyeSwitchRelationCon.SetBoxPosition(lCondBox, 400, 0)      
            lCondBoxa = Scene.FindAnimationNode(lCondBox.AnimationNodeInGet(),'a')
            lCondBoxa.WriteData([100])
            lCondBoxb = Scene.FindAnimationNode(lCondBox.AnimationNodeInGet(),'b')
            lCondBoxb.WriteData([0])
            lEyeRigReceiver = lEyeSwitchRelationCon.ConstrainObject(lEyeLookAtRelationCon)
            lEyeSwitchRelationCon.SetBoxPosition(lEyeRigReceiver, 700, 0)   
            Scene.ConnectBox(lLookAtCubeSender, 'Eye_Track', lCondBox, 'Cond')
            Scene.ConnectBox(lCondBox, 'Result', lEyeRigReceiver, 'Weight')
            lEyeSwitchRelationCon.Snap()
            lConFolder = rs_IK_ConstraintsFolder()
            lConFolder.Items.append(lEyeSwitchRelationCon)
            FBSystem().Scene.Evaluate()
        else:
            print
            print 'Did not find eye controls, script will not set up the constraints\n"IK_Head_Eye_Control" and "Eye_Track_Bool".\nScript can still proceed with the rest of the IK LookAt Setup.'
            print
        #Cleanup Nulls
        lIKControlNull = FBModelNull('IK_Control_Rig')
        lTargetParentNull.Parent = lIKControlNull
        FBSystem().Scene.Evaluate()
        lMoverSpaceNull.Parent = lIKControlNull
        FBSystem().Scene.Evaluate()
        lHeadSpaceNull.Parent = lIKControlNull
        FBSystem().Scene.Evaluate()
        #Create Groups
        lMainGroup = None
        for iGroup in Globals.gGroups:
            if iGroup.Name == lFileName:
                lMainGroup = iGroup
        if lMainGroup != None:
            lHeadLookAtGroup = FBGroup('{0}_Lookat'.format(controlLookAtPrefix))
            lHeadLookAtGroup.ConnectSrc(lLookAtCube)
            lMainGroup.ConnectSrc(lHeadLookAtGroup)
            lHeadLookAtGroup.Show = False
            lLookAtDirectionGroup = FBGroup('{0}_LookAtNull'.format(controlLookAtPrefix))
            lLookAtDirectionGroup.ConnectSrc(lLookAtDirectionNull)
            lMainGroup.ConnectSrc(lLookAtDirectionGroup)
            lLookAtDirectionGroup.Show = False
            lLookAtDirectionGroup.Pickable = False
            lLookAtDirectionGroup.Transformable = False
        else:
            print "Main Group Missing. Unable to create one for 'IK_Head_Lookat'"
        #bool to return to checkList in rs_AssetSetupCharacters.py
        lookAtCheckList = True
    else:
        lookAtCheckList = "False    [Error: 'IK_Head' Null is missing]"