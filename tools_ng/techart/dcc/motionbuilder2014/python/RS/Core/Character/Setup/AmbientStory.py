###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Script Name: rs_CharactersGeoGroups
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Basic Setup of Generic Character Geo Groups
##
## Rules: Definitions: Prefixed with "rs_"
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

from pyfbsdk import *

import RS.Utils.Path
import RS.Utils.Scene
import RS.Globals as glo

def AmbientStory_GeoSetup():

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: rs_ConstraintsFolder
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    def rs_ConstraintsFolder():

        lFolder = None

        for iFolder in glo.gFolders:
            if iFolder.Name == 'Constraints 1':
                lFolder = iFolder

        if lFolder == None:
            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Constraints 1", lPlaceholder)
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"

            FBSystem().Scene.Evaluate()

            lPlaceholder.FBDelete()

        return lFolder

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Get a list of Geo nulls in scene
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lGeoList = []
    lAccList = []

    lMasterArray = []
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lHead = None
    lHead = FBFindModelByLabelName("SKEL_Head")

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lGeoDummy = None
    for iScene in lSceneList:
        if iScene.Name.lower() == lFileName.lower():
            lGeoDummy = iScene
        elif iScene.Name == "Geometry":
            lGeoDummy = iScene

    if lGeoDummy:

        RS.Utils.Scene.GetChildren(lGeoDummy, lGeoList, "", False)
        #lGeoList.extend(eva.gChildList)
        #del eva.gChildList[:]

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Create a multi-dimensional array with geo arrays inside a main array
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    if len(lGeoList) > 1:

        for i in range(26):

            lGeoArray = []

            for iGeo in lGeoList:

                if i < 10:
                    if "00" + str(i) in iGeo.Name:
                        lGeoArray.append(iGeo)

                else:

                    if "0" + str(i) in iGeo.Name:
                        lGeoArray.append(iGeo)

            if len(lGeoArray) >= 1:
                lMasterArray.append(lGeoArray)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Loop through multi-dimensional array to create and append groups
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lGroupList = []

    lMasterGroup = FBGroup("GeoVariations")
    lAccGroup = FBGroup("accessories")
    lPropsGroup = FBGroup("props")
    lDefaultHeadGroup  = FBGroup("default_head")
    lDefaultUpprGroup  = FBGroup("default_uppr")
    lDefaultHandGroup  = FBGroup("default_hand")
    lDefaultLowrGroup  = FBGroup("default_lowr")

    if lFileName != 'A_M_Y_Skater_01':

        lDefaultHead = None
        lDefaultTeef = None
        lDefaultEyes = None
        lDefaultUppr = None
        lDefaultHand = None
        lDefaultLowr = None

        for iGeo in lGeoList:
            if "head_000" in iGeo.Name.lower():
                if "p_" in iGeo.Name.lower():
                    None
                else:
                    lDefaultHead = iGeo
            if 'teef_000' in iGeo.Name.lower():
                lDefaultTeef = iGeo
            if 'eyes' in iGeo.Name.lower():
                if ( 'decl_000' in iGeo.Name.lower() ) or ( 'berd_000' in iGeo.Name.lower() ):
                    lDefaultEyes = iGeo
            elif "hand_000" in iGeo.Name.lower():
                if "p_" in iGeo.Name.lower():
                    None
                else:
                    lDefaultHand = iGeo
            elif "uppr_000" in iGeo.Name.lower():
                lDefaultUppr = iGeo
            elif "lowr_000" in iGeo.Name.lower():
                lDefaultLowr = iGeo

        if lDefaultHead != None:
            lDefaultHeadGroup.ConnectSrc(lDefaultHead)
        if lDefaultTeef != None:
            lDefaultHeadGroup.ConnectSrc(lDefaultTeef)
        if lDefaultEyes != None:
            lDefaultHeadGroup.ConnectSrc(lDefaultEyes)
        if lDefaultUppr != None:
            lDefaultUpprGroup.ConnectSrc(lDefaultUppr)
        if lDefaultHand != None:
            lDefaultHandGroup.ConnectSrc(lDefaultHand)
        if lDefaultLowr != None:
            lDefaultLowrGroup.ConnectSrc(lDefaultLowr)

    else:
        lDefaultHead = None
        lDefaultUppr = None
        lDefaultHand = None
        lDefaultLowr = None

        for iGeo in lGeoList:
            if 'head_000' in iGeo.Name.lower():
                lDefaultHead = iGeo
            elif 'hand_000' in iGeo.Name.lower():
                lDefaultHand = iGeo
            elif 'uppr_000' in iGeo.Name.lower():
                lDefaultUppr = iGeo
            elif 'lowr_000' in iGeo.Name.lower():
                lDefaultLowr = iGeo

        if lDefaultHead != None:
            lDefaultHeadGroup.ConnectSrc(lDefaultHead)
        if lDefaultUppr != None:
            lDefaultUpprGroup.ConnectSrc(lDefaultUppr)
        if lDefaultHand != None:
            lDefaultHandGroup.ConnectSrc(lDefaultHand)
        if lDefaultLowr != None:
            lDefaultLowrGroup.ConnectSrc(lDefaultLowr)

    lMasterGroup.ConnectSrc(lAccGroup)
    lMasterGroup.ConnectSrc(lPropsGroup)
    lMasterGroup.ConnectSrc(lDefaultHeadGroup)
    lMasterGroup.ConnectSrc(lDefaultUpprGroup)
    lMasterGroup.ConnectSrc(lDefaultHandGroup)
    lMasterGroup.ConnectSrc(lDefaultLowrGroup)

    for iArray in lMasterArray:

        if len(iArray) >= 1:

            lGroup = FBGroup("var_0")
            lMasterGroup.ConnectSrc(lGroup)
            lGroupList.append(lGroup)

            for iGeo in iArray:

                if "acc" in iGeo.Name.lower() or "p_" in iGeo.Name.lower() or "berd" in iGeo.Name.lower():

                    if "acc" in iGeo.Name.lower():
                        lAccGroup.ConnectSrc(iGeo)
                    elif "berd" in iGeo.Name.lower():
                        lAccGroup.ConnectSrc(iGeo)
                    elif "p_" in iGeo.Name.lower():
                        lPropsGroup.ConnectSrc(iGeo)
                        lAccList.append(iGeo)

                else:
                    lGroup.ConnectSrc(iGeo)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Add 'Acc' and 'P_Head' Objects to the SKEL_Head via constraint
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lGeoPropDict = {
                    'p_he':'SKEL_Head',
                    'p_ey':'SKEL_Head',
                    'p_ea':'SKEL_Head',
                    'p_lw':'SKEL_L_Forearm',
                    'p_rw':'SKEL_R_Forearm',
                    'p_lh':'SKEL_Pelvis',
                    'p_rh':'SKEL_Pelvis',
                    'p_lf':'SKEL_L_Foot',
                    'p_rf':'SKEL_R_Foot'
                    }

    if len(lAccList) >= 1:
        if lHead != None:

            lPlaceholder = FBConstraintRelation('Remove_Me')
            lFolder = FBFolder("Geo_Prop_Constraints", lPlaceholder)
            lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"
            lPlaceholder.FBDelete()

            for i in range(len(lAccList)):
                for lProp, lJoint in lGeoPropDict.iteritems():
                    if lProp in lAccList[i].Name.lower():
                        lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
                        lParentChildConstraint.Name = lAccList[i].Name + "_Attach_PC"
                        #FBSystem().Scene.Constraints.append(lParentChildConstraint)
                        lParentChildConstraint.ReferenceAdd (0,lAccList[i])
                        lParentChildConstraint.ReferenceAdd (1,FBFindModelByLabelName(lJoint))
                        lParentChildConstraint.Active = True

                        FBSystem().Scene.Evaluate()

                        lFolder.Items.append(lParentChildConstraint)

                        lTag = lParentChildConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        lTag.Data = "rs_Constraints"

            lConFolder = rs_ConstraintsFolder()
            lConFolder.Items.append(lFolder)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ## Description: Add to Character Group if it exists
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

    lCharacterGroup = None
    lVar0Group = None
    for iGroup in glo.gGroups:
        if iGroup.Name == lFileName:
            lCharacterGroup = iGroup
        if iGroup.Name == 'var_0':
            lVar0Group = iGroup

    if lCharacterGroup != None:
        lCharacterGroup.ConnectSrc(lMasterGroup)
    if lVar0Group != None:
        lMasterGroup.Show = False
        lDefaultHeadGroup.Show = True
        lDefaultUpprGroup.Show = True
        lDefaultHandGroup.Show = True
        lDefaultLowrGroup.Show = True
        lVar0Group.Show = False
        lVar0Group.Show = True

    glo.gScene.Evaluate()

    lMasterGroup.Pickable = False
    lMasterGroup.Transformable = False
