###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_GeoSetup_ButtonLaunch
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Basic Setup of Generic Character Geo Groups
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Path
import RS.Core.Character.Setup.PlayerOne as one
import RS.Core.Character.Setup.PlayerTwo as two
import RS.Core.Character.Setup.PlayerZero as zero
import RS.Core.Character.Setup.AmbientStory as amb

reload(one)
reload(two)
reload(zero)
reload(amb)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: geo setup launch
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 


def GeoSetup(pControl, pEvent):
    
    PROJECT_IDS = ["rdr3"]
    
    projName = str( RS.Config.Project.Name ).lower()
    if projName in PROJECT_IDS :
        
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        
        if lFileName.lower() == 'player_zero':
            print 'Player_Zero Geo Setup'
            zero.PlayerZero_GeoSetup( projName )
        elif lFileName.lower() == 'player_one':
            print 'Player_One Geo Setup'
            one.PlayerOne_GeoSetup( projName )
        elif lFileName.lower() == 'player_two':
            print 'Player_Two Geo Setup'
            two.PlayerTwo_GeoSetup( projName )
        else :
            print 'Story/Ambient Geo Setup'
            amb.AmbientStory_GeoSetup()
            
    else :
        print 'Project not included in ID List'
        
        print 'Story/Ambient Geo Setup'
        amb.AmbientStory_GeoSetup()
            