import os
import tempfile
import subprocess
import re
import uuid
import platform
from xml.dom import minidom

from RS.Core.IncrediBuild import Errors


class _incrediBuildBase(object):
    """
    Base class for all incredibuild sub typed classes
    """
    def __init__(self):
        super(_incrediBuildBase, self).__init__()

    def _writeXml(self, parent, rootXml):
        """
        Virtual Method, This must be reimplemented
        """
        raise NotImplementedError("Re-Implement this!")

    def _getXmlValue(self, val):
        """
        Internal Method

        Resolve the value if the value is a variable, otherwise cast to as string and return it
        """
        if isinstance(val, Variable):
            return "$({0})".format(val.name)
        return str(val)


class Enviroment(_incrediBuildBase):
    def __init__(self, name=None):
        super(Enviroment, self).__init__()
        self._name = name or "Default"
        self._tools = []
        self._vars = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = str(newName)

    def getTools(self):
        return [tool for tool in self._tools]

    def getToolByName(self, name):
        for tool in self._tools:
            if name == tool.name:
                return tool
        return None

    def getVars(self):
        return [var for var in self._vars]

    def getVarByName(self, name):
        for var in self._var:
            if name == var.name:
                return var
        return None

    def addTool(self, tool):
        self._tools.append(tool)

    def addVariable(self, var):
        self._vars.append(var)

    def addNewTool(self, toolName, toolPath, allowRemoting=True, outputPrefix=None,
                   outputFileMasks=None, params=None, timeLimit=None, allowRestartOnLocal=False,
                   singleInstancePerAgent=False, groupPrefix=None):
        newTool = Tool(toolName, toolPath, allowRemoting=allowRemoting,
                             outputPrefix=outputPrefix, outputFileMasks=outputFileMasks,
                             params=params, timeLimit=timeLimit, allowRestartOnLocal=allowRestartOnLocal,
                             singleInstancePerAgent=singleInstancePerAgent, groupPrefix=groupPrefix)
        self._tools.append(newTool)
        return newTool

    def addNewVariable(self, varName, varValue):
        newVar = Variable(varName, varValue)
        self._vars.append(newVar)
        return newVar

    def _writeXml(self, parent, rootXml):
        enviromentBase = rootXml.createElement("Environment")
        enviromentBase.setAttribute("Name", self._getXmlValue(self._name))

        if len(self._tools) > 0:
            toolBase = rootXml.createElement("Tools")
            for tool in self._tools:
                tool._writeXml(toolBase, rootXml)
            enviromentBase.appendChild(toolBase)

        if len(self._vars) > 0:
            varBase = rootXml.createElement("Variables")
            for var in self._vars:
                var._writeXml(varBase, rootXml)
            enviromentBase.appendChild(varBase)

        parent.appendChild(enviromentBase)


class Variable(_incrediBuildBase):
    def __init__(self, name, value):
        super(Variable, self).__init__()
        self._name = name
        self._value = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = str(newName)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, newVal):
        self._value = newVal

    def _writeXml(self, parent, rootXml):
        varEntry = rootXml.createElement("Variable")

        varEntry.setAttribute("Name", self._name)
        varEntry.setAttribute("Value", str(self._value))

        parent.appendChild(varEntry)


class Tool(_incrediBuildBase):
    def __init__(self, name, toolPath, allowRemoting=True, outputPrefix=None, outputFileMasks=None,
                params=None, timeLimit=None, allowRestartOnLocal=False, singleInstancePerAgent=False,
                groupPrefix=None):
        super(Tool, self).__init__()
        self._name = name
        self._path = toolPath
        self._allowRemote = allowRemoting
        self._outputPrefix = outputPrefix
        self._outputFileMasks = outputFileMasks
        self._params = params
        self._timeLimit = timeLimit
        self._allowRestartOnLocal = allowRestartOnLocal
        self._singleInstancePerAgent = singleInstancePerAgent
        self._groupPrefix = groupPrefix

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = str(newName)

    @property
    def toolPath(self):
        return self._path

    @toolPath.setter
    def toolPath(self, newVal):
        self._path = newVal

    @property
    def allowRemoting(self):
        return self._allowRemote

    @allowRemoting.setter
    def allowRemoting(self, newVal):
        self._allowRemote = newVal

    @property
    def outputPrefix(self):
        return self._outputPrefix

    @outputPrefix.setter
    def outputPrefix(self, newVal):
        self._outputPrefix = newVal

    @property
    def outputFileMasks(self):
        return self._outputFileMasks

    @outputFileMasks.setter
    def outputFileMasks(self, newVal):
        self._outputFileMasks = newVal

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, newVal):
        self._params = newVal

    @property
    def timeLimit(self):
        return self._timeLimit

    @timeLimit.setter
    def timeLimit(self, newVal):
        self._timeLimit = newVal

    @property
    def allowRestartOnLocal(self):
        return self._allowRestartOnLocal

    @allowRestartOnLocal.setter
    def allowRestartOnLocal(self, newVal):
        self._allowRestartOnLocal = newVal

    @property
    def singleInstancePerAgent(self):
        return self._singleInstancePerAgent

    @singleInstancePerAgent.setter
    def singleInstancePerAgent(self, newVal):
        self._singleInstancePerAgent = newVal

    @property
    def groupPrefix(self):
        return self._groupPrefix

    @groupPrefix.setter
    def groupPrefix(self, newVal):
        self._groupPrefix = newVal

    def _writeXml(self, parent, rootXml):
        toolEntry = rootXml.createElement("Tool")

        toolEntry.setAttribute("Name", self._getXmlValue(self._name))
        toolEntry.setAttribute("Path", self._getXmlValue(self._path))
        toolEntry.setAttribute("AllowRemote", str(self._allowRemote).lower())

        if self._outputPrefix is not None:
            toolEntry.setAttribute("OutputPrefix", self._getXmlValue(self._outputPrefix))

        if self._outputFileMasks is not None:
            toolEntry.setAttribute("OutputFileMasks", self._getXmlValue(self._outputFileMasks))

        if self._params is not None:
            toolEntry.setAttribute("Params", self._getXmlValue(self._params))

        if self._timeLimit is not None:
            toolEntry.setAttribute("TimeLimit", self._getXmlValue(self._timeLimit))

        toolEntry.setAttribute("AllowRestartOnLocal", str(self._allowRestartOnLocal).lower())
        toolEntry.setAttribute("SingleInstancePerAgent", str(self._singleInstancePerAgent).lower())

        if self._groupPrefix is not None:
            toolEntry.setAttribute("GroupPrefix", self._getXmlValue(self._groupPrefix))

        parent.appendChild(toolEntry)


class TaskGroup(_incrediBuildBase):
    def __init__(self, name, tool, project, dependsOn=None, workingDir=None):
        super(TaskGroup, self).__init__()
        self._name = name
        self._tool = tool
        self._dependsOn = dependsOn or ()
        self._workingDir = workingDir

        self._tasks = []
        self._project = project

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = str(newName)

    @property
    def tool(self):
        return self._tool

    @tool.setter
    def tool(self, newVal):
        self._tool = newVal

    @property
    def dependsOn(self):
        return self._dependsOn

    @dependsOn.setter
    def dependsOn(self, newVal):
        newVal = newVal or ()
        self._dependsOn = newVal

    @property
    def project(self):
        return self._project

    @project.setter
    def project(self, newVal):
        self._project = newVal

    @property
    def workingDir(self):
        return self._workingDir

    @workingDir.setter
    def workingDir(self, newVal):
        self._workingDir = newVal

    def addDependsOn(self, task):
        self._dependsOn += (task,)

    def addTask(self, task):
        task.parentTask = self
        self._tasks.append(task)

    def addNewTask(self, params, caption, name=None, tool=None, skipIfProjectFailed=False,
                   sourceFile=None, dependsOn=None):
        newTask = Task(name or self._name, tool or self._tool, params, caption, parentTask=self,
                       skipIfProjectFailed=skipIfProjectFailed, sourceFile=sourceFile,
                       dependsOn=dependsOn)
        self._tasks.append(newTask)
        return newTask

    def addNewChildTaskGroup(self, name, tool):
        return self._project.addNewTaskGroup(name, tool, dependsOn=(self,))

    def _writeXml(self, parent, rootXml):
        taskGroupEntry = rootXml.createElement("TaskGroup")

        taskGroupEntry.setAttribute("Name", self._getXmlValue(self._name))

        if self._tool is not None:
            taskGroupEntry.setAttribute("Tool", self._tool.name)

        if len(self._dependsOn) > 0:
            taskGroupEntry.setAttribute("DependsOn", ";".join([item.name for item in self._dependsOn]))

        if self._workingDir is not None:
            taskGroupEntry.setAttribute("WorkingDir", self._getXmlValue(self._workingDir))

        for task in self._tasks:
            task._writeXml(taskGroupEntry, rootXml)

        parent.appendChild(taskGroupEntry)


class Task(_incrediBuildBase):
    def __init__(self, name, tool, params, caption, parentTask=None, dependsOn=None,
                 skipIfProjectFailed=False, sourceFile=None):
        super(Task, self).__init__()
        self._params = params
        self._caption = caption
        self._tool = tool
        self._name = name
        self._skipIfProjectFailed = skipIfProjectFailed
        self._sourceFile = sourceFile

        self._dependsOn = dependsOn or ()
        self._parentTask = parentTask

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newVal):
        self._name = newVal

    @property
    def tool(self):
        return self._tool

    @tool.setter
    def tool(self, newVal):
        self._tool = newVal

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, newVal):
        self._params = newVal

    @property
    def caption(self):
        return self._caption

    @caption.setter
    def caption(self, newVal):
        self._caption = newVal

    @property
    def parentTask(self):
        return self._parentTask

    @parentTask.setter
    def parentTask(self, newVal):
        self._parentTask = newVal

    @property
    def dependsOn(self):
        return self._dependsOn

    @dependsOn.setter
    def dependsOn(self, newVal):
        newVal = newVal or ()
        self._dependsOn = newVal

    @property
    def skipIfProjectFailed(self):
        return self._skipIfProjectFailed

    @skipIfProjectFailed.setter
    def skipIfProjectFailed(self, newVal):
        self._skipIfProjectFailed = newVal

    @property
    def sourceFile(self):
        return self._sourceFile

    @sourceFile.setter
    def sourceFile(self, newVal):
        self._sourceFile = newVal

    def addDependsOn(self, task):
        self._dependsOn += (task,)

    def _writeXml(self, parent, rootXml):
        taskEntry = rootXml.createElement("Task")

#         if self._parentTask is None or self._parentTask.tool is None:
        taskEntry.setAttribute("Name", self._getXmlValue(self._name))
        taskEntry.setAttribute("Tool", self._tool.name)

        if len(self._dependsOn) > 0:
            taskEntry.setAttribute("DependsOn", ";".join([item.name for item in self._dependsOn]))

        taskEntry.setAttribute("Params", self._getXmlValue(self._params))
        taskEntry.setAttribute("Caption", self._getXmlValue(self._caption))

        taskEntry.setAttribute("SkipIfProjectFailed", str(self._skipIfProjectFailed).lower())

        if self._sourceFile is not None:
            taskEntry.setAttribute("SourceFile", self._getXmlValue(self._sourceFile))

        parent.appendChild(taskEntry)


class Project(_incrediBuildBase):
    def __init__(self, name, env=None, workingDir=None):
        super(Project, self).__init__()
        self._name = name
        self._env = env or Enviroment()
        self._workingDir = workingDir

        self._taskGroups = []
        self._tasks = []
        self._endTasks = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = str(newName)

    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, newVal):
        self._env = newVal

    @property
    def workingDir(self):
        return self._workingDir

    def getToolByName(self, name):
        return self._env.getToolByName(name)

    def getVarByName(self, name):
        return self._env.getVarByName(name)

    def addNewTool(self, toolName, toolPath, allowRemoting=True, outputPrefix=None,
                   outputFileMasks=None, params=None, timeLimit=None, allowRestartOnLocal=False,
                   singleInstancePerAgent=False, groupPrefix=None):
        return self._env.addNewTool(toolName, toolPath, allowRemoting=allowRemoting,
                             outputPrefix=outputPrefix, outputFileMasks=outputFileMasks,
                             params=params, timeLimit=timeLimit, allowRestartOnLocal=allowRestartOnLocal,
                             singleInstancePerAgent=singleInstancePerAgent, groupPrefix=groupPrefix)

    def addNewVariable(self, varName, varValue):
        return self._env.addNewVariable(varName, varValue)

    @workingDir.setter
    def workingDir(self, newVal):
        self._workingDir = newVal

    def addTaskGroup(self, taskGroup):
        taskGroup.project = self
        self._taskGroups.append(taskGroup)

    def addNewTaskGroup(self, name, tool, dependsOn=None, workingDir=None):
        newTaskGroup = TaskGroup(name, tool, self, dependsOn=dependsOn, workingDir=workingDir)
        self._taskGroups.append(newTaskGroup)
        return newTaskGroup

    def addTask(self, task):
        task.parentTask = None
        self._tasks.append(task)

    def addNewTask(self, name, tool, params, caption, dependsOn=None, skipIfProjectFailed=False):
        newTask = Task(name, tool, params, caption, dependsOn=dependsOn, skipIfProjectFailed=skipIfProjectFailed)
        self._tasks.append(newTask)
        return newTask

    def addEndTask(self, task):
        task.parentTask = None
        self._endTasks.append(task)

    def addNewEndTask(self, name, tool, params, caption, skipIfProjectFailed=False):
        newTask = Task(name, tool, params, caption, skipIfProjectFailed=skipIfProjectFailed)
        self._endTasks.append(newTask)
        return newTask

    def _writeXml(self, parent, rootXml):
        projectEntry = rootXml.createElement("Project")

        projectEntry.setAttribute("Name", self._getXmlValue(self._name))
        projectEntry.setAttribute("Env", self._env.name)

        if self._workingDir is not None:
            projectEntry.setAttribute("WorkingDir", self._getXmlValue(self._workingDir))

        allTaskNames = []
        for taskGroup in self._taskGroups:
            taskGroup._writeXml(projectEntry, rootXml)
            allTaskNames.append(taskGroup)

        for task in self._tasks:
            task._writeXml(projectEntry, rootXml)
            allTaskNames.append(task)

        for task in self._endTasks:
            task.dependsOn = allTaskNames
            task._writeXml(projectEntry, rootXml)
            task.dependsOn = None

        parent.appendChild(projectEntry)


class IncrediBuildGraph(_incrediBuildBase):
    def __init__(self, prjName, graphFolder=None):
        super(IncrediBuildGraph, self).__init__()
        self._graphID = str(uuid.uuid1())

        self._graphFolder = graphFolder or os.path.join(tempfile.gettempdir(), "incredibuldGraph", self._graphID)
        self._project = Project(prjName)
        self._enviroments = [self._project.env]

    def addNewEnviroment(self, name):
        newEnv = Enviroment(name)
        self._enviroments.append(newEnv)
        return newEnv

    def addEnviroment(self, env):
        if env not in self._enviroments:
            self._enviroments.append(env)

    @property
    def project(self):
        return self._project

    @project.setter
    def project(self, newVal):
        self._project = newVal

    @property
    def graphFolder(self):
        return self._graphFolder

    @property
    def outputFilePath(self):
        return os.path.join(self._graphFolder, "output.txt")

    @property
    def logFilePath(self):
        return os.path.join(self._graphFolder, "log.txt")

    @property
    def graphFilePath(self):
        return os.path.join(self._graphFolder, "graph.xml")

    @graphFolder.setter
    def graphFolder(self, newVal):
        self._graphFolder = newVal

    def isAgentServiceRunning(self):
        """
        Check if the agent service is running

        return:
            bool if the service is running or not
        """
        agentService = "Incredibuild_Agent"
        return self._isServiceRunning(agentService)

    def _isServiceRunning(serviceName):
        """
        Internal Method

        Check if a service is running or not

        args:
            serviceName (str): Name of the service to find

        return:
            bool if the service is running or not
        """
        args = ["tasklist", "/FI", "SERVICES eq {0}".format(serviceName)]
        proc = subprocess.Popen(args, shell=True, stdout=subprocess.PIPE)
        output = proc.communicate()[0]
        return "INFO: No tasks are running which match the specified criteria" not in output

    def runJob(self, showOutput=True, waitForFinish=True, raiseIfFailed=True):
        farmProc = "xgconsole"
        motionerCall = "/openmonitor"
        noLogoCall = "/nologo"
        showCmdCall = "/showcmd"
        titleCall = "/title='{0}'".format(self._project.name)
        logCall = "/log={0}".format(self.logFilePath)
        outCall = "/out={0}".format(self.outputFilePath)
        jobTaskPath = self.graphFilePath

        if not os.path.exists(self._graphFolder):
            os.makedirs(self._graphFolder)

        self.writeJob(jobTaskPath)
        argsToRun = [farmProc, jobTaskPath, titleCall, logCall, outCall, noLogoCall, showCmdCall]

        for derp in [self.logFilePath, self.outputFilePath]:
            handler = open(derp, "w")
            handler.close()

        if not self.isAgentServiceRunning():
            raise ValueError("Agent Service is not running, unable to run. Graph at '{0}'".format(jobTaskPath))

        if showOutput:
            argsToRun.append(motionerCall)

        if waitForFinish:
            proc = subprocess.Popen(argsToRun, stdout=subprocess.PIPE)
            proc.wait()
            output = proc.communicate()

            if not raiseIfFailed:
                return

            # Check for build errors
            resultStringRegex = re.compile("build system error\(s\)\r\n \r\n\Z")
            if len(resultStringRegex.findall(output)) > 0:
                raise Errors.IncrediBuildBuildError("build system error, see monitor for errors")

            resultStringRegex = re.compile("\r\n    Build: \d succeeded, \d failed, \d skipped\r\n\Z")
            results = resultStringRegex.findall(output)
            if len(results) != 1:
                raise Errors.IncrediBuildUnknownError("Unable to find results, assuming somthing went wrong")

            _, failed, _ = re.compile("\d").findall(results[0])
            if failed != "0":
                raise Errors.IncrediBuildTaskError("{0} tasks failed".format(failed))
        else:
            subprocess.Popen(argsToRun)

    def writeJob(self, filePath):
        fileHandle = open(filePath, "w+")
        implementation = minidom.getDOMImplementation()
        buildSet = implementation.createDocument(None, "BuildSet", None)
        buildSetElement = buildSet.documentElement
        buildSetElement.setAttribute("FormatVersion", "1")

        self._writeXml(buildSetElement, buildSet)
        fileHandle.write(buildSet.toprettyxml())
        fileHandle.close()

    def _writeXml(self, parent, rootXml):
        envBase = rootXml.createElement("Environments")
        for env in self._enviroments:
            env._writeXml(envBase, rootXml)
        parent.appendChild(envBase)

        self._project._writeXml(parent, rootXml)
