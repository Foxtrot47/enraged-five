import os
import time

from RS.Core.IncrediBuild import IncrediBuild
from RS.Core.IncrediBuild.Modules import VideoProcessing, ExternalApps


###################################################################################################
#                                         SAMPLES                                                 #
###################################################################################################

# Sample 1: Create the same XML as the example

derp = IncrediBuild.IncrediBuildGraph("Xml Demo Project")

prj = derp.project
# Vars
delayVar = prj.addNewVariable("Delay", 2000)
longDelayVar = prj.addNewVariable("LongDelay", 9000)
dirVar = prj.addNewVariable("CurrentDirectory", r"C:\Program Files (x86)\Xoreax\IncrediBuild\Samples\Dev Tools Acceleration\DevTools Interfaces Usage Samples\XML Interface2")
# Tools
procToolLocal = prj.addNewTool("DummyProcessLocal", "$(CurrentDirectory)\DummyProcess.exe", True, "DummyProcess", "*.", "&quot;$(TimeToRun)&quot; ")
fToolLocal = prj.addNewTool("FakeDummyProcess", "$(CurrentDirectory)\DummyProcesss.exe", False, "DummyProcess", "*.", "&quot;$(TimeToRun)&quot; ")
procToolRemote = prj.addNewTool("DummyProcessRemote", "$(CurrentDirectory)\DummyProcess.exe", True, "DummyProcess", "*.", "&quot;$(TimeToRun)&quot; ")
prj.workingDir = dirVar


startingTaskGroup = prj.addNewTaskGroup("Starting Proceses", procToolRemote)
sideTaskGroup = prj.addNewTaskGroup("Side Proceses", procToolLocal)
dependantTaskGroup = startingTaskGroup .addNewChildTaskGroup("Dependant Processes", procToolRemote)

for idx in xrange(1, 2):
    startingTaskGroup.addNewTask(delayVar, "Starting Process {0}".format(idx))
    dependantTaskGroup.addNewTask(delayVar, "Dependant Process {0}".format(idx))

for idx in xrange(1, 2):
    sideTaskGroup.addNewTask(longDelayVar, "Side Process {0}".format(idx))

for idx in xrange(0, 2):
    prj.addNewEndTask("End Procs", procToolLocal, delayVar, "Single Task")

prj.addNewTask("ShortProcesses", procToolLocal, "asdasdasd", "Single Task", dependsOn=(dependantTaskGroup,))

tool = ExternalApps.addPython(prj, False)

argDerp = ["C:\\Users\\geoff.samuel\\workspace\\RS\\Core\\Video\\sendReport.py",
       "-l {0}".format(derp.logFilePath),
       "-o {0}".format(derp.outputFilePath),
       "-g {0}".format(derp.graphFilePath),
       "-s {0}".format(time.time()),
       "-u {0}".format(os.getenv("USERNAME")),
       "-w \"['Geoff.Samuel@rockstarnorth.com', 'Mark.Harrison-Ball@rockstargames.com']\"",
       "-f \"['Geoff.Samuel@rockstarnorth.com']\"",
        ]
# prj.addNewTask("Fail Procs", tool, "C:\\Users\\geoff.samuel\\workspace\\RS\\Core\\Video\\test.py", "Single Task")

# prj.addNewEndTask("End Procs", tool, " ".join(argDerp), "Single Task")


# Uncomment this to run it
derp.runJob(showOutput=True, waitForFinish=False)

print os.path.dirname(derp.logFilePath)
# Sample 2: Transcode task, with fake parts for file paths

# Fake data stubbs
mediaJobs = [
             {
              "outCompressPath": "/nope/",
              "CompressArgs": "/nope/",
              "ConcatArgs": "/nope/",
              "CleanUpArgs": "/nope/",
              "Values": "/notSureWhatThisIs/"
              },

             {
              "outCompressPath": "/nope/",
              "CompressArgs": "/nope/",
              "ConcatArgs": "/nope/",
              "CleanUpArgs": "/nope/",
              "Values": "/notSureWhatThisIs/"
              }]

toolsBinDir = "/nope/"
p4Path = "/nope/"
ffmpegPathname = "/nope/"

videoProc = IncrediBuild.IncrediBuildGraph("Video Transcoding Process")
prj = videoProc.project
# Vars
binVar = prj.addNewVariable("BinVar", os.path.join(toolsBinDir, "video", "ffmpeg.exe"))
compressPathDir = prj.addNewVariable("CompressVar", mediaJobs[0]["outCompressPath"])
# Tools
p4Job = prj.addNewTool("p4job", p4Path, True, timeLimit=3600, groupPrefix="Serialising remotely...")
cmdJob = prj.addNewTool("CMD", "cmd", True, timeLimit=3600, groupPrefix="Serialising remotely...")
ffmpgLocalJob = prj.addNewTool("ffmpeg Local", ffmpegPathname, False, groupPrefix="Serialising remotely...")
ffmpgInstanceJob = prj.addNewTool("ffmpeg Instnace", ffmpegPathname, True, groupPrefix="Serialising remotely...")

# task groups
ExtractCompressTaskGroup = prj.addNewTaskGroup("ExtractCompress", ffmpgLocalJob, workingDir=binVar)
MergeTaskGroup = prj.addNewTaskGroup("Merge", ffmpgLocalJob, workingDir=compressPathDir, dependsOn=ExtractCompressTaskGroup)

# tasks
for job in mediaJobs:
    extractTask = ExtractCompressTaskGroup.addNewTask(
                                        job["Values"],
                                        "{0} Extract".format(job["CompressArgs"]),
                                        name="{0} Extract".format(job["CompressArgs"]),
                                        tool=ffmpgLocalJob,
                                        skipIfProjectFailed=False,
                                        )

    compressTask = ExtractCompressTaskGroup.addNewTask(
                                        job["Values"],
                                        "{0} Compress".format(job["CompressArgs"]),
                                        name="{0} Compress".format(job["CompressArgs"]),
                                        tool=ffmpgLocalJob,
                                        skipIfProjectFailed=False,
                                        dependsOn=extractTask
                                        )

    deleteTask = ExtractCompressTaskGroup.addNewTask(
                                        job["Values"],
                                        "{0} Delete Temp".format(job["CompressArgs"]),
                                        name="{0} Delete Temp".format(job["CompressArgs"]),
                                        tool=cmdJob,
                                        skipIfProjectFailed=False,
                                        dependsOn=compressTask
                                        )

    mergeTask = MergeTaskGroup.addNewTask(
                                        job["Values"],
                                        "mergeing"
                                        )

# svideoProc.runJob(showOutput=True, waitForFinish=False)
# videoProc.writeJob(r"X:\pythonIncredibuildTest.xml")


videoProc = IncrediBuild.IncrediBuildGraph("Video Transcoding Process")
prj = videoProc.project

VideoProcessing.mosaicTask(r"X:\Work\1.mov", r"X:\Work\2.mov", r"X:\Work\d3.mov", None, r"X:\Work\output1.mov", prj, runRemote=False)
VideoProcessing.mosaicTask(r"X:\Work\1.mov", r"X:\Work\2.mov", r"X:\Work\3.mov", None, r"X:\Work\output2.mov", prj, runRemote=False)
VideoProcessing.mosaicTask(r"X:\Work\4.mov", r"X:\Work\1.mov", r"X:\Work\2.mov", r"X:\Work\3.mov", r"X:\Work\output3.mov", prj, runRemote=False)
VideoProcessing.mosaicTask(r"X:\Work\1d.mov", None, r"X:\Work\d3.mov", None, r"X:\Work\output4.mov", prj, runRemote=False)
# videoProc.runJob(showOutput=True, waitForFinish=False)
print "job done"
videoProc.writeJob(r"X:\pythonIncredibuildTest.xml")
