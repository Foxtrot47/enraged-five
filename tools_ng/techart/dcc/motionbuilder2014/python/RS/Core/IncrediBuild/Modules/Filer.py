import uuid
import os

from RS.Core.IncrediBuild.Modules import ExternalApps


def createDirTask(dirToCreate, incredibuildProject, taskGroup=None, dependsOn=None, runRemote=True):
    taskName = "Create Directory ({1})".format(str(uuid.uuid1()))

    commandLine = "mkdir {0}".format(dirToCreate)
    tool = ExternalApps._addCmd(incredibuildProject, runRemote)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            commandLine,
                            taskName,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )
    else:
        return taskGroup.addNewTask(
                            commandLine,
                            taskName,
                            name=taskName,
                            tool=tool,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )


def removeFileTask(fileToRemove, incredibuildProject, taskGroup=None, dependsOn=None, runRemote=True):
    taskName = "Remove Files({0})".format(str(uuid.uuid1()))

    commandLine = ["import os", "os.remove(r'{0}')".format(fileToRemove)]
    return ExternalApps.pythonCommandLineTask(
                                              commandLine,
                                              incredibuildProject,
                                              taskGroup=taskGroup,
                                              dependsOn=dependsOn,
                                              runRemote=runRemote
                                              )
