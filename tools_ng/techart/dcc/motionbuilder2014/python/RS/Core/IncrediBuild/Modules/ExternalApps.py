import uuid
import os


def addPython(prject, remote=True):
    pythonDict = {
                  True: "python Remote",
                  False: "python Local"
    }

    pythonFilePath = r"X:\rdr3\tools\bin\python\Python27\x64\bin\python.exe"

    nameOfTool = pythonDict[remote]
    tool = prject.getToolByName(nameOfTool)

    if tool is None:
        tool = prject.addNewTool(nameOfTool, pythonFilePath, remote)
    return tool


def addCmd(prject, remote=True):
    pythonDict = {
                  True: "CMD Remote",
                  False: "CMD Local"
    }

    pythonFilePath = r"cmd"

    nameOfTool = pythonDict[remote]
    tool = prject.getToolByName(nameOfTool)

    if tool is None:
        tool = prject.addNewTool(nameOfTool, pythonFilePath, remote)
    return tool


def pythonScriptTask(pythonScriptLocation, incredibuildProject, taskGroup=None, dependsOn=None, runRemote=True):
    taskName = "python Script {0} ({1})".format(os.path.basename(pythonScriptLocation), str(uuid.uuid1()))

    tool = addPython(incredibuildProject, runRemote)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            pythonScriptLocation,
                            taskName,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )
    else:
        return taskGroup.addNewTask(
                            pythonScriptLocation,
                            taskName,
                            name=taskName,
                            tool=tool,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )


def pythonCommandLineTask(pythonLines, incredibuildProject, taskGroup=None, dependsOn=None, runRemote=True):
    taskName = "python Command Line ({0})".format(str(uuid.uuid1()))

    tool = addPython(incredibuildProject, runRemote)
    commandLine = "-c \"{0}\"".format(";".join(pythonLines))

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            commandLine,
                            taskName,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )
    else:
        return taskGroup.addNewTask(
                            commandLine,
                            taskName,
                            name=taskName,
                            tool=tool,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )

def commandlineTask(commandLine, incredibuildProject, taskGroup=None, dependsOn=None, runRemote=True):
    taskName = "commandline ({1})".format(str(uuid.uuid1()))

    tool = _addPython(incredibuildProject, runRemote)

    if taskGroup is None:
        return incredibuildProject.addNewTask(
                            taskName,
                            tool,
                            commandLine,
                            taskName,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )
    else:
        return taskGroup.addNewTask(
                            commandLine,
                            taskName,
                            name=taskName,
                            tool=tool,
                            dependsOn=dependsOn,
                            skipIfProjectFailed=False,
        )
