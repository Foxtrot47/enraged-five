import codecs
import csv
import os

import RS.Config
import RS.Perforce
import xml.dom.minidom as minidom



#
# Class for file object
#
class meta_file( object ):
    def __init__( self, lPath):
        
        self.LocalPath = lPath
        self.Name = os.path.basename( lPath ).split(".")[0]
        self.FileName = os.path.basename( lPath )
        self.Search = ""
        self.Replace = ""
        

#
# Class that reads and stores entire csv data
#
class csv_meta( object ):
    def __init__( self, csvFilePath ):
        
        # Init variables
        self.SyncPaths = []
        self.LocalPaths = []
        self.CSVFilePath = csvFilePath
        self.CSVDB = []
        self.CSVData = {}
        self.CSVObjects = {}
        self.ScannedFiles = {}
        self.SearchTag = ""
        self.Comment = ""
        self.NewMetas = {}
        self.OtherMetas = {}
        self.P4CL = None
        
        # Read csv file into array using readCSV
        self.CSVDB = self.readCSV( csvFilePath )
        
        # Populate variables
        #
        self.SyncPaths = [ s for s in self.CSVDB[0][1:] if s ]
        
        for lPath in self.SyncPaths:
            if not os.path.splitdrive( lPath )[0]:
                self.LocalPaths.append( RS.Perforce.SwitchPathStyle( lPath ) )
            else:
                self.LocalPaths.append( lPath )
        
        for group in self.CSVDB[7:]:
            key = group[0]
            if key not in self.CSVData:
                self.CSVData[ key ] = []
            for value in group[1:]:
                if value:
                    self.CSVData[ key ].append( value )
        
        self.SearchTag = self.CSVDB[1][1]
        self.Comment = self.CSVDB[1][3]
        
        '''
        #Don't really need these.  Can populate this after scanning meta files.
        i = 1
        trimList = filter(lambda a: a != "", self.CSVDB[2])
        while i < len( trimList ):
            self.NewMetas[ trimList[i] ] = self.CSVDB[3][i]
            i+=1
            '''
        
        i = 1
        trimList = filter(lambda a: a != "", self.CSVDB[4])
        while i < len( trimList ):
            #self.OtherMetas[ trimList[i].lower() ] = self.CSVDB[5][i]
            self.OtherMetas[ trimList[i].lower() ] = ""
            i+=1
        #
        ###
    
    # Swap axes of the array for ease of data manip. later
    def swapAxes( self, theArray ):
        lDB = []
        i = 0
        longestRow = 0
        for row in theArray:
            # Setup Empty Columns in new array
            while len( lDB ) < len( row ):
                lDB.append( [] )
            
            # Update row length
            if longestRow < len( row ):
                longestRow = len( row )
                
            j = 0
            for col in row:
                # pre-fill with null up to current row
                while len( lDB[j] ) < i:
                    lDB[j].append( "" )
                # Grab values
                lDB[j].append( col )
                j+=1
                
            while j < longestRow:
                # append with null until matches length of db
                lDB[j].append( "" )
                j+=1
                
            i+= 1
        return lDB
    
    # Reads csv file into an array with cells: [ [a1, a2, ...], [b1, b2, ...], ... ]
    # Then swaps axes to: [ [a1, b1, c1, ...], [a2, b2, c2, ...], ... ]
    # This makes data easier to update on the back-end.
    def readCSV( self, lFilePath ):
        lDB = []
        with open( lFilePath, 'rb' ) as csvfile:
            spamreader = csv.reader( csvfile, dialect='excel', delimiter=',', quotechar='|' )
            
            # Transfer data to temp Array
            for row in spamreader:
                lDB.append( row )
            
            lDB = self.swapAxes( lDB )
                
        return lDB
        
        
    def writeCSV( self, lFilePath, lDB ):
        lDB = self.swapAxes( lDB )
        with open( lFilePath, 'wb' ) as csvfile:
            spamwriter = csv.writer( csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL )
            for row in lDB:
                spamwriter.writerow( row )
    
    def syncPaths( self ):
        for lPath in self.SyncPaths:
            # Prepare path and sync entire folder
            lFolderSync = os.path.join( lPath, "..." ).replace( "\\", "/" )
            RS.Perforce.Sync( lFolderSync, force=False )
            
    # Looks at all files in folders and creates an object for each one
    def createFileObjects( self ):
        for lPath in self.LocalPaths:
            for root, dirs, files in os.walk( lPath, topdown=False ):
                for lFile in files:
                    lFilePath = os.path.join( root, lFile )
                    
                    lMetaFileObj = meta_file( lFilePath )
                    if lMetaFileObj.Name not in self.ScannedFiles:
                        self.ScannedFiles[ lMetaFileObj.Name.lower() ] = lMetaFileObj
                    
    # Looks at csv content for files to modify.  Copies object to new smaller dict, and marks for edit.
    def checkOutFiles( self ):
        
        lSyncList = []
            
        for key in self.CSVData:
            for char in self.CSVData[key]:
                
                # Dict keys are lowercase to make spreadsheet data case insensitive.
                char = char.lower()
                if char in self.ScannedFiles:
                    lPath = self.ScannedFiles[char].LocalPath
                    if lPath not in lSyncList:
                        lSyncList.append( lPath )
                        
                    # Additional info stored in object.
                    self.CSVObjects[char] = self.ScannedFiles[char]
                    self.CSVObjects[char].Search = self.SearchTag
                    self.CSVObjects[char].Replace = key
                            
        RS.Perforce.Revert( lSyncList )
        RS.Perforce.Edit( lSyncList, self.P4CL.Number )
        
        isWritable = True
        for lFile in lSyncList:
            if not os.access( lFile, os.W_OK ):
                checkWritable = False
        
        return isWritable
    
    # This will be used to update info in meta files.
    def editFiles( self ):
        
        lSyncList = []
        
        for char in self.CSVObjects:
            lChar = self.CSVObjects[char]
            filePath = lChar.LocalPath
            lSyncList.append( filePath )
            
            # Parse file as minidom object
            dom = minidom.parse( filePath )
            
            # Look for "Search" tag
            searchTag = dom.getElementsByTagName( lChar.Search )
            replaceData = lChar.Replace
            print "Setting {0} for {1} to {2}".format( lChar.Search, lChar.Name, replaceData )
            
            # If one tag found:
            if len( searchTag ) == 1:
                
                # If tag looks like <tag/>, replace with empty textnode instead <tag></tag>
                if len( searchTag[0].childNodes ) == 0:
                    searchTag[0].appendChild( dom.createTextNode( "" ) )
                
                # Pick the first child to test for text_node
                lChild = searchTag[0].childNodes[0]
                if lChild.nodeType == lChild.TEXT_NODE:
                    
                    # Set Data
                    lChild.data = replaceData
                    
                    # Format XML - fix first line, needs newline.
                    splitXML = dom.toxml( encoding="utf-8" ).split( "\n", 1 )
                    headerLines = splitXML[0].replace( "><", ">\n<" )
                    bodyLines = splitXML[1]
                    domXML = u"{0}\n{1}".format( headerLines, bodyLines )
                    
                    # Format XML - fix spaces, because minidom changes formatting.
                    tempXML = domXML.split( "\n" )
                    domXML = u""
                    for line in tempXML:
                        if line.endswith("/>"):
                            line = line.replace( "/>", " />" )
                        if domXML != "":
                            domXML = u"{0}\n{1}".format( domXML, line )
                        else:
                            domXML = line
                    
                    # Save file.
                    f = open( filePath, "w" )
                    f.write( domXML )
                    f.close()
                    
            else:
                print "Found too many tags matching the search"
                
    def updateValues( self, charDict ):
        for char in charDict:
            if char in self.ScannedFiles:
                #print self.SearchTag
                lFilePath = self.ScannedFiles[char].LocalPath
                
                # Parse file as minidom object
                dom = minidom.parse( lFilePath )
                
                # Look for "Search" tag
                searchTag = dom.getElementsByTagName( self.SearchTag )
                
                # If one tag found:
                if len( searchTag ) == 1:
                    
                    # Pick the first child to test for text_node
                    if len( searchTag[0].childNodes ) == 1:
                        lChild = searchTag[0].childNodes[0]
                        if lChild.nodeType == lChild.TEXT_NODE:
                            charDict[ char ] = lChild.data
                    else:
                        charDict[ char ] = ""
                else:
                    charDict[ char ] = ""
                
        return charDict
    
    # This will write to csv file.  Need an "ERROR" Column for files that weren't found.
    def updateDB( self ):
        # will need to update value entries for "new" and "other", 4 columns
        del self.CSVDB[2][1:]
        del self.CSVDB[3][1:]
        del self.CSVDB[4][1:]
        del self.CSVDB[5][1:]
        for key in sorted( self.NewMetas ):
            if key in self.ScannedFiles:
                self.CSVDB[2].append( self.ScannedFiles[key].Name )
                self.CSVDB[3].append( self.NewMetas[key] )
        for key in sorted( self.OtherMetas ):
            if key in self.ScannedFiles:
                self.CSVDB[4].append( self.ScannedFiles[key].Name )
                self.CSVDB[5].append( self.OtherMetas[key] )
        
    # This will pull out files that aren't already in "other" or "data" columns for "new"
    def gatherNewFiles( self ):
        for lFile in self.ScannedFiles:
            if lFile not in self.OtherMetas:
                self.NewMetas[lFile] = self.ScannedFiles[lFile]
    
    def loadFile( self ):
        self.P4CL = RS.Perforce.CreateChangelist( self.Comment )
        
        self.syncPaths()
        self.createFileObjects()
        
    def updateCSVFile( self ):
        
        #RS.Perforce.Revert( self.CSVFilePath )
        RS.Perforce.Sync( self.CSVFilePath )
        RS.Perforce.Edit( self.CSVFilePath, self.P4CL.Number )
        
        self.updateValues( self.OtherMetas )
        self.gatherNewFiles()
        self.updateValues( self.NewMetas )
        self.updateDB()
        if os.access( self.CSVFilePath, os.W_OK ):
            self.writeCSV( self.CSVFilePath, self.CSVDB )
            print "Updated CSV File"
            return True
        else:
            print "Failed to update CSV File"
            return False
        
        
    def processFiles( self ):
        # Run the functions:
        if self.checkOutFiles():
            self.editFiles()
            return True
        return False
