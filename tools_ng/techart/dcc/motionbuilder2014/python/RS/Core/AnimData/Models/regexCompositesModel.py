"""
Models for dealing with contexts
"""
from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem


class CompositiesModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single select
    """

    def __init__(self, composite, parent=None):
        """
        Constructor
        """
        super(CompositiesModelItem, self).__init__(parent=parent)
        self._composite = composite

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._composite.name

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class RegexTakesDataModel(baseModel.BaseModel):
    """
    Base Model Type
    """

    def __init__(self, project=None, regex=None, parent=None):
        self._project = project
        self._regex = regex
        super(RegexTakesDataModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def setProject(self, newProject):
        self._project = newProject
        self.reset()

    def project(self):
        return self._project

    def setRegex(self, newRegex):
        self._regex = newRegex
        self.reset()

    def regex(self):
        return self._regex

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._project is None or self._regex is None:
            return

        for trial in self._project.getCompositesByRegex(self._regex):
            print trial
            parent.appendChild(CompositiesModelItem(trial, parent=parent))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        return QtCore.QModelIndex()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    from RS.Core.AnimData import Context

    prj = Context.animData.getProjectByName("Redemption2")
    reg = '^ROBT_S1_MCS1_P1A1_T01$'
    mainModel = RegexTakesDataModel(prj)
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    mainModel.setRegex(reg)
    win.show()
    sys.exit(app.exec_())
