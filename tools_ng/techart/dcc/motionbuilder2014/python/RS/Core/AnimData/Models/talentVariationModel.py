"""
Models for dealing with Talents
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts as animContexts
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, lazyLoadModelItem


class ContextTalentItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextProjectItem(prj, self._context, self, rootItem=self._rootItem) for prj in self._context.getVariationProjects()]


class ContextProjectItem(baseContext.ContextBaseItem):
    def __init__(self, context, talent, parent=None, rootItem=None):
        super(ContextProjectItem, self).__init__(context, parent=parent, rootItem=rootItem)
        self._talent = talent
        
    def _populateChildren(self):
        return [ContextTalentVariation(var, self, rootItem=self._rootItem) for var in self._context.getTalentVariations(self._talent)]


class ContextTalentVariation(baseContext.ContextBaseItem):
    def __init__(self, context, parent=None, rootItem=None):
        super(ContextTalentVariation, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)

    def _populateChildren(self):
        return []

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False

    def canFetchMore(self, parent):
        return False


class TalentVairationModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, parent=None):
        super(TalentVairationModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        for talent in Context.animData.getAllTalent():
            parent.appendChild(ContextTalentItem(talent, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.Context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()
        return QtCore.QModelIndex()



if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    mainModel = TalentVairationModel()
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
