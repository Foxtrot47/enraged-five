import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import captureDayModel
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView


class AbstractDayContextWidgets(QtCore.QObject):
    """
    Abstract Context Widget mixin

    has two signals:
        contextSelected (Context): The currently selected context
        daySelected (CaptureDay): The currently selected Capture Day context
    """
    contextSelected = QtCore.Signal(object)
    daySelected = QtCore.Signal(object)

    def __init__(self):
        super(AbstractDayContextWidgets, self).__init__()

    def setContext(self, context):
        """
        VIRTUAL
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot or Day
        """
        raise NotImplementedError("This method needs to be implemented in subclasses")

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        return [index.data(QtCore.Qt.UserRole) for index in self.selectedIndexes()]

    def getSelectedTrials(self):
        """
        get all the currently selected trial contexts

        return:
            a list of all the selected trial contexts
        """
        returnVals = []
        for index in self.selectedIndexes():
            val = index.data(QtCore.Qt.UserRole)
            if isinstance(val, contexts._trialBase):
                returnVals.append(val)
        return returnVals

    def _handleContextClick(self, index):
        """
        Internal Method

        Handle the selection change, to re-emit when a context object or trial is selected

        args:
            index (QModelIndex): The model index which is selected
        """
        val = index.data(QtCore.Qt.UserRole)
        if val is not None:
            self.contextSelected.emit(val)

        if isinstance(val, contexts._trialBase):
            self.daySelected.emit(val)


class ContextDayTreeView(QtGui.QTreeView, AbstractDayContextWidgets):
    """
    Context Tree View for the AnimData
    """
    def __init__(self, parent=None):
        super(ContextDayTreeView, self).__init__(parent=parent)

        # Model stuff
        mainModel = captureDayModel.CaptureDayModel()
        self.setModel(mainModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


class ContextDayColumnView(columnFilterView.ColumModelView, AbstractDayContextWidgets):
    """
    Context Column View for the AnimData
    """

    def __init__(self, parent=None):
        super(ContextDayColumnView, self).__init__(parent=parent)

        # Model stuff
        mainModel = captureDayModel.CaptureDayModel()
        self.setModel(mainModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)


class ContextDayComboxBoxView(abstractComboView.ContextComboxBoxView, AbstractDayContextWidgets):
    """
    Context Combo Box for the AnimData

    has two signals:
        contextSelected (Context): The currently selected context
        daySelected (CaptureDay): The currently selected Day context
    """
    def __init__(self, parent=None):
        super(ContextDayComboxBoxView, self).__init__(parent=parent)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def model(self):
        """
        Virtual Method
        """
        return captureDayModel.CaptureDayModel()

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ("Shoot:", 1),
               ("Capture Day:", 2),
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        self.contextSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        self.daySelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getCurrentDay(self):
        trials = self.getSelectedTrials()
        if len(trials) == 1:
            return trials[0]
        return None

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedDays(self):
        """
        get all the currently selected days contexts

        return:
            a list of all the selected days contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts._trialBase):
            return [val]
        return []

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    win = ContextDayTreeView()
    # win = ContextDayColumnView()
    # win = ContextDayComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.daySelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
