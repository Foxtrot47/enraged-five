import os

from PySide import QtCore, QtGui

from RS import Config
from RS.Core.AnimData import Context
from RS.Core.AnimData.Widgets import stageContextWidget


class _UpdateRunner(QtCore.QThread):

    updateTimerDing = QtCore.Signal()

    def __init__(self, parent=None):
        super(_UpdateRunner, self).__init__(parent=parent)
        self._running = False

    def run(self):
        self._running = True
        while self._running is True:
            self.updateTimerDing.emit()
            self.msleep(2000)

    def stop(self, wait=False):
        self._running = False
        if wait is True:
            self.wait()


class ContextFollowWidget(QtGui.QWidget):
    
    _UNLOCKED_ICON = None
    _LOCKED_ICON = None
    
    def __init__(self, context=None, autoUpdate=True, parent=None):
        if self._UNLOCKED_ICON is None:
            self._setupIcons()
        
        self._autoUpdate = autoUpdate
        super(ContextFollowWidget, self).__init__(parent=parent)
        self.setupUi()
        self.setContext(context)

        self._runner = _UpdateRunner(self)
        self._runner.updateTimerDing.connect(self._checkForUpdates)

    @classmethod
    def _setupIcons(cls):
        basePath = os.path.abspath(os.path.join(Config.Script.Path.ToolImages, "animData", "icons"))
        cls._UNLOCKED_ICON = QtGui.QIcon(os.path.join(basePath, "unlocked.png"))
        cls._LOCKED_ICON = QtGui.QIcon(os.path.join(basePath, "locked.png"))

    def setupUi(self):
        """
        Setup the Note
        """
        # Create Layouts
        mainLayout = QtGui.QVBoxLayout()
        locationLayout = QtGui.QHBoxLayout()
        
        # Create Widgets
        self._stageWidget = stageContextWidget.StageContextWidget()
        self._lockStageButton = QtGui.QPushButton("")
        self._takeName = QtGui.QLineEdit()
        
        # Configure Widgets
        self._lockStageButton.setIcon(self._LOCKED_ICON)
        lockSize = QtCore.QSize(30,30)
        lockIconSize = QtCore.QSize(lockSize.height() - 5, lockSize.width() - 5)
        self._lockStageButton.setMaximumSize(lockSize)
        self._lockStageButton.setMinimumSize(lockSize)
        self._lockStageButton.setIconSize(lockIconSize)
        self._lockStageButton.setCheckable(True)
        locationLayout.setContentsMargins(0,0,0,0)
        self._takeName.setReadOnly(True)

        # Assign Layouts to Widgets
        locationLayout.addWidget(self._stageWidget)
        locationLayout.addWidget(self._lockStageButton)
        mainLayout.addLayout(locationLayout)
        mainLayout.addWidget(QtGui.QLabel("Take:"))
        mainLayout.addWidget(self._takeName )
        
        self._stageWidget.locationSelected.connect(self._checkForUpdates)
        self._lockStageButton.pressed.connect(self._handleLockStateChange)
        
        self.setLayout(mainLayout)

    def _handleLockStateChange(self):
        if self._stageWidget.isEnabled():
            self._lockStage()
        else:
            self._unlockStage()

    def _lockStage(self):
        self._lockStageButton.setIcon(self._UNLOCKED_ICON)
        self._stageWidget.setEnabled(False)

    def _unlockStage(self):
        self._lockStageButton.setIcon(self._LOCKED_ICON)
        self._stageWidget.setEnabled(True)

    def setContext(self, newContext):
        self._stageWidget.setContext(newContext)
        self._checkForUpdates()

    def showEvent(self, event):
        super(ContextFollowWidget, self).showEvent(event)
        if self._autoUpdate is True:
            self._runner.start()

    def __del__(self):
        self.__killThreading()

    def closeEvent(self, event):
        self.__killThreading()
        super(ContextFollowWidget, self).closeEvent(event)

    def __killThreading(self):
        if self._runner is not None:
            self._runner.stop()
            self._runner.terminate()
            self._runner.wait()
            self._runner = None

    def _updateTrialName(self, newName):
        if self._takeName.text() == newName:
            return
        self._takeName.setText(newName)

    def _checkForUpdates(self):
        stage = self._stageWidget.getSelectedStage()
        if stage is None:
            self._updateTrialName("<No Stage Selected>")
            return
        trial = Context.animData.getCurrentTrialByStage(stage)
        if trial is None:
            self._updateTrialName("<No Active Take>")
        else:
            self._updateTrialName(str(trial.name))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = ContextFollowWidget()
    wid.show()
    sys.exit(app.exec_())
