from PySide import QtGui


class ModelColours(object):
    SELECT = QtGui.QColor("#8CD18C")
    NONSELECT = QtGui.QColor("#F5BCBC")


class PipelineStepStatus(object):
    COMPLETE = "Completed"
    REWORK = "Rework"
    NOT_COMPLETE = "Not Completed"


class PipelineStages(object):
    CAPTURED = "Captured"
    PROCESSED = "Processed"
    TRACKED = "Tracked"
    REFINED = "Refined"
    CHAR_MAPPED = "Char-Mapped"
    IMPORTED_INTO_MOBU = "Imported Into MoBu"
    TRIAL_FINISHED = "Trial Finished"
    ADJUSTED = "Adjusted"
    ROM_VERIFIED = "ROM Verified"
    CHECKED_INTO_PERFORCE = "Checked Into P4v"


class AssignedTasks(object):
    TRACK_AND_REFINE = "Track & Refine"
    REVIST_REFINE = "Revist Refine"
    CHARMAP = "Charmap"
    REVIST_CHARMAP = "Revist Charmap"
    THROUGH_CHARMAP = "Through Charmap"
    RELATTICE = "Re-Lattice"
    CURVE_AND_LATTICE = "Curve & Lattice"
    COMPLETE_PROCESS = "Complete Process"


class FilePathTables(object):
    CAPTURE = "Capture"
    FEATURE = "Feature"
    HELMET_CAM_CALIBRATION = "HelmetCamCalibration"
    MOTION_SCENE = "MotionScene"
    MOTION_SCENE_ASSET = "MotionSceneAsset"
    PHYSICAL_PROP_VERSION = "PhysicalPropVersion"
    PROBSPEC = "Probspec"
    PROJECT_3D_ASSET = "Project3DAsset"
    MOCAP_ASSET = "MocapAsset"
    SHOT = "Shot"
    STAGE_CALIBRATION = "StageCalibration"
    STAGE_PROP_PROJECT = "StagePropProject"
    TALENT_PROJECT = "TalentProject"
    TALENT_PROJECT_VARIATION = "TalentProjectVariation"
    TRIAL_PRODUCTION_ASSET = "TrialProductionAsset"


class AdditionalData(object):
    """
    Key names for contexts._trialBase's additional data dict.
    """
    ORIGINAL_TRIAL_NAME = "originalTrialName"
    ORIGINAL_TRIAL_ID = "originalTrialId"


# Primarily used in context object names when one of the sub-contexts is not yet populated.
EMPTY_SUBCONTEXT_NAME = "<NONE>"
