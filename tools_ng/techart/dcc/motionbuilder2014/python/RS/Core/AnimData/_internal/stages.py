"""
Module for all the different context types, how they all fit together
"""
from PySide import QtCore

from RS import Config
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize
from RS.Core.AnimData._internal import contexts
from RS.Tools.CameraToolBox.PyCore.Metaclasses import singleton


class _locationBase(contexts._contextBase):
    """
    Internal Class

    Base Class for all Location Context
    """
    def __init__(self, services):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
        """
        super(_locationBase, self).__init__(services)

    def _handleStageCreation(self, stagesFromWatchstar, location):
        """
        Internal Method

        Handles creating the Stages from the returned watchstar data

        args:
            stagesFromWatchstar (list of dict): The json dict from the watchstar database
            location (Location): The parent location which the stage lives under

        return:
            list of stages (Stages) objects
        """
        return [Stage(location, self.getServices(), data) for data in stagesFromWatchstar]


class Location(_locationBase):
    """
    Context object for a Location
    """

    def __init__(self, services, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Location, self).__init__(services)
        self._locationName = dataDict.get("displayName")
        self._dataDict = dataDict

    @property
    def locationID(self):
        """
        get the code ID of the location

        return:
            location code ID as a string
        """
        return self._dataDict.get("id")

    @property
    def name(self):
        """
        get the name of the location

        return:
            location name as a string
        """
        return self._locationName

    def city(self):
        """
        Get the name of the city that the location is in

        return:
            String City Name
        """
        return self._dataDict.get("city")

    def locationAbbreviation(self):
        """
        Get the location abbreviation

        return:
            location abbreviation
        """
        return self._dataDict.get("abbrev")

    def hostName(self):
        """
        Get the hostname of the location

        return:
            string value for the hostname
        """
        return self._dataDict.get("hostName", "")

    def locationActive(self):
        """
        Get if the location is active or not

        return:
            bool if the location is still active
        """
        return self._dataDict.get("isActive", False)

    def previsFolderPath(self):
        """
        Get the perforce folder path for the locations previz folder location

        return:
            string p4 path name
        """
        return self._dataDict.get("previzFolderPath", "")

    @memorize.memoized
    def getStages(self):
        """
        get all the stages at this location

        return:
            list of stages (Stage) under the location
        """
        data = self.getServices().watchstar.executeCommand(
                                                       "tools.LocationStageSearchByLocation",
                                                       [
                                                            self.getServices().getUserName(),
                                                            self.locationID,
                                                        ]
                                                       )
        if data is None:
            return []
        return self._handleStageCreation(data.get("stages"), self)

    def __repr__(self):
        """
        Internal Method

        Pretty Print object
        """
        return "Location object for '{0}'".format(self.name)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.locationID == other.locationID

    def __hash__(self):
        """
        Hash the object
        """
        return self.locationID.__hash__()


class Stage(_locationBase):
    """
    Context object for a stage
    """

    def __init__(self, location, services, dataDict):
        """
        Constructor

        args:
            location (AnimData.contexts.Location): The location this stage is in
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Stage, self).__init__(services)

        self._stageName = dataDict.get("name")
        self._location = location
        self._dataDict = dataDict

    @property
    def stageId(self):
        """
        get the code ID of the location

        return:
            location code ID as a string
        """
        return self._dataDict.get("id")

    @property
    def name(self):
        """
        get the name of the stage

        return:
            stage name as a string
        """
        return self._stageName

    def location(self):
        """
        Get the location for this stage

        return:
            the location (AnimData.contexts.Location) for the stage
        """
        return self._location

    def previsFolderPath(self):
        """
        Get the perforce folder path for the locations previz folder location

        return:
            string p4 path name
        """
        return self._location.previsFolderPath()

    def stageActive(self):
        """
        Get if the stage is active or not

        return:
            bool if the stage is still active
        """
        return self._dataDict.get("isActive", False)

    def captureWorkstationIp(self):
        """
        Get the Capture Machine IP for the current stage

        return:
            string of the IP of the capture machine
        """
        return self._dataDict.get("captureWorkstationIP", "000.000.000.000")

    def __repr__(self):
        """
        Internal Method

        Pretty Print object
        """
        return "Stage object for '{0}' at '{1}'".format(self.name, self.location().name)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.stageId == other.stageId

    def __hash__(self):
        """
        Hash the object
        """
        return self.stageId.__hash__()


class LocalLocation(Location):
    _LOCAL_STAGE_ID = -91

    __metaclass__ = singleton.Singleton

    def __init__(self, services):
        """
        Context object for a local Location
        """
        self._settings = QtCore.QSettings("RockstarSettings", "AnimData")
        dataDict = {
            "id": -90,
            "abbrev": "Lcl",
            "city": "everywhere",
            "displayName": "Local Location",
            "hostName": "",
            "isActive": False,
            "isOffsite": False,
            "previzFolderPath": "",
        }
        super(LocalLocation, self).__init__(services, dataDict)

        self._localStage = self._getLocalStage()
        self._storedStages = self._getLocalStoredStages()

    def _getLocalStage(self):
        """
        Get the local hostname as a stage

        return:
            (Stage) with the local IP of the host
        """
        localIp = "192.168.0.0"#Config.User.system.ipaddress
        dataDict = {
            "id": self._LOCAL_STAGE_ID,
            "captureWorkstationIP": localIp,
            "name": "Local Stage ({})".format(localIp),
        }
        return Stage(self, self.getServices(), dataDict)

    def _readStoredStages(self):
        data = self._settings.value("CustomLocationStages", []) or []
        if not isinstance(data, list):
            data = [data]
        return data

    def _saveStoredStages(self):
        ips = [stg.captureWorkstationIp() for stg in self._storedStages]
        self._settings.setValue("CustomLocationStages", ips)

    def _clearStoredStages(self):
        self._settings.setValue("CustomLocationStages", [])

    def _getLocalStoredStages(self):
        """
        get all the custom stages at this location

        return:
            list of the custom stages (Stage) under the location
        """
        allStoredStages = []
        for idx, ip in enumerate(self._readStoredStages()):
            dataDict = {
                "id": self._LOCAL_STAGE_ID - idx,
                "captureWorkstationIP": ip,
                "name": "Local Stage ({})".format(ip),
            }
            allStoredStages.append(Stage(self, self.getServices(), dataDict))
        return allStoredStages

    def addCustomStage(self, ipAddress):
        """
        Add a new stage to the local location
        """
        currentIdx = min([stg.stageId for stg in self._storedStages] or [self._LOCAL_STAGE_ID])
        dataDict = {
            "id": currentIdx - 1,
            "captureWorkstationIP": ipAddress,
            "name": "Local Stage ({})".format(ipAddress),
        }
        self._storedStages.append(Stage(self, self.getServices(), dataDict))
        self._saveStoredStages()

    def clearCustomStages(self):
        """
        Clear all the custom stages
        """
        self._clearStoredStages()

    def getStages(self):
        """
        get all the stages at this location

        return:
            list of stages (Stage) under the location
        """
        return [self._localStage] + self._storedStages
