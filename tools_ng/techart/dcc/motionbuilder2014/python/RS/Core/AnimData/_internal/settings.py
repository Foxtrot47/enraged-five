"""
Module for all the different context types, how they all fit together
"""


class _settingsBase(object):
    """
    Internal Class

    Base Class for all settings Context
    """
    def __init__(self, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database

        """
        super(_settingsBase, self).__init__()
        self._dataDict = dataDict

    def getDataDict(self):
        """
        Get the data dict for the context

        return:
             A nested dict of values from the database
        """
        return self._dataDict

    @property
    def name(self):
        """
        Get the name of the Watchstar object.

        warning:
            This key is NOT present in all object json.

        return:
            str
        """
        return str(self._dataDict.get("name", ""))

    @property
    def id(self):
        """
        Get the ID of the Watchstar object.

        return:
            int: The object's ID
        """
        return int(self._dataDict["id"])

    @property
    def settingsID(self):
        """
        get the code ID of the settings

        return:
            str: settings ID as a string
        """
        return self.id

    def isActive(self):
        """
        Get if the setting is active or not.

        return:
            bool: if the setting is still active
        """
        return self._dataDict.get("isActive", False)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Settings {0} object for '{1}'".format(self.__class__.__name__, self.name)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} (id: {}) at {}>".format(self.__str__(), self.id, hex(id(self)))

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.settingsID == other.settingsID

    def __hash__(self):
        """
        Hash the object
        """
        return self.settingsID.__hash__()


class VideoSetting(_settingsBase):
    """
    object for the VideoSettings
    """


class VideoCodec(_settingsBase):
    """
    object for the VideoCodec
    """


class CaptureTypes(_settingsBase):
    """
    object for the Capture Types
    """
    @property
    def name(self):
        """
        get the name of the location

        return:
            location name as a string
        """
        return "{0} - {1}".format(self._dataDict.get("captureCategory"), self._dataDict.get("captureType"))

    def isActive(self):
        """
        NOT USED
        """
        return False


class LatticeStatus(_settingsBase):
    """
    object for the Lattice Status
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class FilePathTableNames(_settingsBase):
    """
    object for the File Path Table Names
    """

    @property
    def id(self):
        """
        Reimplemented from _settingsBase

        The data dict for this settings object only has a name field.

        return:
            int: The object's ID
        """
        return -1


class FilePathTypes(_settingsBase):
    """
    object for the File Types
    """


class Variations(_settingsBase):
    """
    object for the Variations
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class User(_settingsBase):
    """
    object for user
    """
    @property
    def name(self):
        """
        get the name of the location

        return:
            location name as a string
        """

        return self._dataDict.get("username")

    def isActive(self):
        """
        NOT USED
        """
        return False


class UserAssignmentTasks(_settingsBase):
    """
    object for user assignment tasks.
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class FaceAudioPack(_settingsBase):
    """
    object for Face Audio Pack.
    """
    def deviceType(self):
        """
        Get the device type back as a string

        return:
            str of the device type
        """
        return self._dataDict["deviceType"]


class FaceAudioType(_settingsBase):
    """
    object for Face Audio Type.
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class Priority(_settingsBase):
    """
    object for Priority
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class PipelineStep(_settingsBase):
    """
    object for PipelineStep
    """
    def isActive(self):
        """
        NOT USED
        """
        return False


class FeatureType(_settingsBase):
    """
    object for a Feature Type
    """
    @property
    def name(self):
        """
        get the name of the feature

        return:
            feature name as a string
        """
        return "{0} - {1}".format(self._dataDict.get("featureCategory"), self._dataDict.get("name"))
    
    def featureCategory(self):
        return self._dataDict["featureCategory"]


class FeatureGroup(_settingsBase):
    """
    object for a Feature Group
    """


class FeatureCategory(_settingsBase):
    """
    object for a Feature Category
    """


class AssetClass(_settingsBase):
    """
    object for an Asset Class
    """


class AssetType(_settingsBase):
    """
    object for Asset3DType
    """
    def assetClassId(self):
        return self._dataDict["assetClassID"]


class MetaPack(_settingsBase):
    """
    object for an Meta pack
    """


class ProductionRole(_settingsBase):
    """
    object for a Production Role
    """


class MarkerSet(_settingsBase):
    """
    object for a Markerset
    """


class CameraType(_settingsBase):
    """
    object for a Camera Type
    """
