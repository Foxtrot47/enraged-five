class _baseSettingsConst(object):
    def __init__(self, settingsManager):
        self._settingsManager = settingsManager


class _assetClasses(_baseSettingsConst):
    @property
    def ANIMAL(self):
        return self._settingsManager.getAssetClassByName("Animal")

    @property
    def CHARACTER(self):
        return self._settingsManager.getAssetClassByName("Character")

    @property
    def ENVIRONMENT(self):
        return self._settingsManager.getAssetClassByName("Environment")

    @property
    def PROP(self):
        return self._settingsManager.getAssetClassByName("Prop")

    @property
    def VEHICLE(self):
        return self._settingsManager.getAssetClassByName("Vehicle")


class _featureCategories(_baseSettingsConst):
    @property
    def CUTSCENE(self):
        return self._settingsManager.getFeatureCategoryByName("Cutscene")

    @property
    def INGAME(self):
        return self._settingsManager.getFeatureCategoryByName("In-Game")

    @property
    def ASSET(self):
        return self._settingsManager.getFeatureCategoryByName("3D Asset")
    
    @property
    def GENERIC(self):
        return self._settingsManager.getFeatureCategoryByName("Generic")


class _captureTypes(_baseSettingsConst):
    @property
    def TRIAL(self):
        return self._settingsManager.getCaptureTypesByName("Trial - Trial")

    @property
    def BODY_ROM(self):
        return self._settingsManager.getCaptureTypesByName("Init - Body ROM")

    @property
    def FACE_ROM(self):
        return self._settingsManager.getCaptureTypesByName("Init - Face ROM")

    @property
    def BODY_SCALE(self):
        return self._settingsManager.getCaptureTypesByName("Init - Body Scale")

    @property
    def HEAD_SCAN(self):
        return self._settingsManager.getCaptureTypesByName("Init - Head Scan")


class _priorities(_baseSettingsConst):
    @property
    def PRIORITY_1(self):
        return self._settingsManager.getPriorityByName("1 (Highest)")

    @property
    def PRIORITY_2(self):
        return self._settingsManager.getPriorityByName("2")

    @property
    def PRIORITY_3(self):
        return self._settingsManager.getPriorityByName("3")

    @property
    def PRIORITY_4(self):
        return self._settingsManager.getPriorityByName("4")

    @property
    def PRIORITY_5(self):
        return self._settingsManager.getPriorityByName("5 (Lowest)")

    @property
    def NOT_PRIORITIZED(self):
        return self._settingsManager.getPriorityByName("Not Prioritized")


class _filePathTypes(_baseSettingsConst):
    @property
    def REF_VIDEO(self):
        return self._settingsManager.getFilePathTypeByName("Stage Video")

    @property
    def FACE_VIDEO(self):
        return self._settingsManager.getFilePathTypeByName("Face Video")

    @property
    def TALENT_VARIATION_IMAGE(self):
        return self._settingsManager.getFilePathTypeByName("Talent Variation Image")

    @property
    def FBX_SCENE(self):
        return self._settingsManager.getFilePathTypeByName("FBX Scene")

    @property
    def ANM_MOCAP_ASSET(self):
        return self._settingsManager.getFilePathTypeByName("ANM Mocap Asset")

    @property
    def FBX_MOCAP_ASSET(self):
        return self._settingsManager.getFilePathTypeByName("FBX Mocap Asset")

    @property
    def GIANT_PRJ(self):
        return self._settingsManager.getFilePathTypeByName("Giant PRJ")

    @property
    def GIANT_SCL(self):
        return self._settingsManager.getFilePathTypeByName("Giant SCL")

    @property
    def GIANT_CHARACTERMAP(self):
        return self._settingsManager.getFilePathTypeByName("Giant CM")

    @property
    def GIANT_CPJ(self):
        return self._settingsManager.getFilePathTypeByName("Giant CPJ")

    @property
    def GIANT_CAL(self):
        return self._settingsManager.getFilePathTypeByName("Giant CAL")

    @property
    def GIANT_ANM(self):
        return self._settingsManager.getFilePathTypeByName("Giant ANM")

    @property
    def GIANT_BMO(self):
        return self._settingsManager.getFilePathTypeByName("Giant BMO")

    @property
    def GIANT_GID(self):
        return self._settingsManager.getFilePathTypeByName("Giant GID")

    @property
    def GIANT_PSF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PSF")

    @property
    def GIANT_RAW(self):
        return self._settingsManager.getFilePathTypeByName("Giant RAW")

    @property
    def GIANT_RTPARAMS(self):
        return self._settingsManager.getFilePathTypeByName("Giant RTPARAMS")

    @property
    def GIANT_MDL(self):
        return self._settingsManager.getFilePathTypeByName("Giant MDL")

    @property
    def GIANT_SCH(self):
        return self._settingsManager.getFilePathTypeByName("Giant SCH")

    @property
    def GIANT_SDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant SDF")

    @property
    def GIANT_LMT(self):
        return self._settingsManager.getFilePathTypeByName("Giant LMT")

    @property
    def GIANT_PDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PDF")

    @property
    def GIANT_BDF(self):
        return self._settingsManager.getFilePathTypeByName("Giant BDF")

    @property
    def GIANT_PPF(self):
        return self._settingsManager.getFilePathTypeByName("Giant PPF")

    @property
    def GIANT_PSP(self):
        return self._settingsManager.getFilePathTypeByName("Giant PSP")

    @property
    def FBX_AUDIO(self):
        return self._settingsManager.getFilePathTypeByName("FBX Audio")

    @property
    def GIANT_REFINEDATA(self):
        return self._settingsManager.getFilePathTypeByName("Giant Refine ANM")

    @property
    def GIANT_DEFORMER(self):
        return self._settingsManager.getFilePathTypeByName("Giant Lattice FFD")

    @property
    def EST_AUDIO(self):
        return self._settingsManager.getFilePathTypeByName("EST FBX Audio")

    def getAllFilePathTypes(self):
        return [
                self.REF_VIDEO,
                self.FACE_VIDEO,
                self.TALENT_VARIATION_IMAGE,
                self.FBX_SCENE,
                self.ANM_MOCAP_ASSET,
                self.FBX_MOCAP_ASSET,
                self.GIANT_PRJ,
                self.GIANT_SCL,
                self.GIANT_CHARACTERMAP,
                self.GIANT_CPJ,
                self.GIANT_CAL,
                self.GIANT_ANM,
                self.GIANT_BMO,
                self.GIANT_GID,
                self.GIANT_PSF,
                self.GIANT_RAW,
                self.GIANT_RTPARAMS,
                self.GIANT_MDL,
                self.GIANT_SCH,
                self.GIANT_SDF,
                self.GIANT_LMT,
                self.GIANT_PDF,
                self.GIANT_BDF,
                self.GIANT_PPF,
                self.GIANT_PSP,
                self.FBX_AUDIO,
                self.GIANT_REFINEDATA,
                self.GIANT_DEFORMER,
                self.EST_AUDIO,
                ]


class SettingsConst(_baseSettingsConst):
    def __init__(self, settingsManager):
        super(SettingsConst, self).__init__(settingsManager)
        self._assetClasses = _assetClasses(settingsManager)
        self._featureCategories = _featureCategories(settingsManager)
        self._captureTypes = _captureTypes(settingsManager)
        self._priorities = _priorities(settingsManager)
        self._filePathTypes = _filePathTypes(settingsManager)

    @property
    def AssetClasses(self):
        return self._assetClasses

    @property
    def FeatureCategories(self):
        return self._featureCategories

    @property
    def CaptureTypes(self):
        return self._captureTypes

    @property
    def Priorities(self):
        return self._priorities

    @property
    def FilePathTypes(self):
        return self._filePathTypes
