"""
Models for dealing with Talents
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import contexts as animContexts
from RS.Core.AnimData.Models import baseContext
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class ContextProjectItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextTalentItem(var, self, rootItem=self._rootItem) for var in self._context.getAllTalent()]


class ContextTalentItem(baseContext.ContextBaseItem):
    def _populateChildren(self):
        return [ContextTalentVariation(talVar, self, rootItem=self._rootItem) for talVar in self._context.getVariations()]


class ContextTalentVariation(baseContext.ContextBaseItem):
    def __init__(self, context, parent=None, rootItem=None):
        super(ContextTalentVariation, self).__init__(context, parent=parent, rootItem=rootItem, addLazyLoader=False)

    def _populateChildren(self):
        return []

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False

    def canFetchMore(self, parent):
        return False


class ProjectTalentVairationModel(baseModel.BaseModel):
    """
    Base Model Type
    """

    def __init__(self, initalProject=None, parent=None):
        self._initalProject = initalProject
        super(ProjectTalentVairationModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Context"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        for prj in Context.animData.getAllProjects():
            parent.appendChild(ContextProjectItem(prj, parent=parent, rootItem=self))

    def findContextIndex(self, context):
        """
        Get the index of a given context, if found

        args:
            context (AnimData.context): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()

        sameType = False
        # Project
        if isinstance(context, animContexts.Project):
            projName = context.name
            sameType = True
        else:
            projName = context.project().name

        projIdx = self.match(self.createIndex(0, 0), 0, projName, 1, QtCore.Qt.MatchExactly)
        if len(projIdx) == 0:
            return QtCore.QModelIndex()
        projIdx = projIdx[0]
        if sameType:
            return projIdx
        proj = self.data(projIdx, QtCore.Qt.UserRole + 1)

        # Talent
        if isinstance(context, animContexts.Talent):
            con = context
            sameType = True
        else:
            con = context.talent()
        talIdx = proj.findContextIndex(con, projIdx)
        if talIdx is None:
            return QtCore.QModelIndex()

        if sameType:
            return talIdx
        talent = self.data(talIdx, QtCore.Qt.UserRole + 1)

        # Variation
        if isinstance(context, animContexts.TalentVariation):
            con = context
            sameType = True
        else:
            con = context.talentVariation()
        varIdx = talent.findContextIndex(con, talIdx)
        if varIdx is None:
            return QtCore.QModelIndex()
        return varIdx


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)

    mainModel = ProjectTalentVairationModel()
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
