"""
Motion Trials Files Model
"""
from PySide import QtGui, QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem


class FilesModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single File
    """
    def __init__(self, fileObj, parent=None):
        """
        Constructor
        """
        super(FilesModelItem, self).__init__(parent=parent)
        self._fileObj = fileObj

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._fileObj.name
            elif column == 1:
                return self._fileObj.fileType().name
            elif column == 2:
                return self._fileObj.filePath()
            elif column == 3:
                return self._fileObj.thumbnailPath()
            elif column == 4:
                return self._fileObj.fileOnMocapServer()
        elif role == QtCore.Qt.UserRole:
            return self._fileObj

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class AssociatedFilesModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, trial=None, parent=None):
        self._trial = trial
        self._filters = []
        super(AssociatedFilesModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Name", "Type", "Path", "Thumbnail", "MocapP4"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setTrialContext(self, trial):
        self._trial = trial
        self.reset()

    def trialContext(self):
        return self._trial

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._trial is None:
            return

        for fileObj in [obj for obj in self._trial.getAllFiles()]:
            parent.appendChild(FilesModelItem(fileObj, parent=parent))


if __name__ == "__main__":
    import sys

    from rockstar.animData import context, const

    app = QtGui.QApplication(sys.argv)
    trial = context.animData.getProjectByName("Redemption2").getTrialsByRegex('008024_02_BE_ODR4_EXT_P1')[0]
    mainModel = AssociatedFilesModel(trial)

    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()

    sys.exit(app.exec_())
