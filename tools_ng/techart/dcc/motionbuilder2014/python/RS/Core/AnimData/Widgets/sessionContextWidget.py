import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import sessionModel, proxyFilterModels
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView


class AbstractSessionContextWidgets(QtCore.QObject):
    """
    Abstract Context Widget mixin

    has two signals:
        contextSelected (Context): The currently selected context
        sessionSelected (Session): The currently selected Session context
    """
    contextSelected = QtCore.Signal(object)
    sessionSelected = QtCore.Signal(object)

    def __init__(self):
        super(AbstractSessionContextWidgets, self).__init__()

    def setContext(self, context):
        """
        VIRTUAL
        Set the selected context of the widget

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
            or Session
        """
        raise NotImplementedError("This method needs to be implemented in subclasses")

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        Return:
            a list of context objects which are selected
        """
        return [index.data(QtCore.Qt.UserRole) for index in self.selectedIndexes()]

    def getSelectedTrials(self):
        """
        get all the currently selected trial contexts

        Return:
            a list of all the selected trial contexts
        """
        returnVals = []
        for index in self.selectedIndexes():
            val = index.data(QtCore.Qt.UserRole)
            if isinstance(val, contexts._trialBase):
                returnVals.append(val)
        return returnVals

    def _handleContextClick(self, index):
        """
        Internal Method

        Handle the selection change, to re-emit when a context object or trial 
        is selected

        Args:
            index (QModelIndex): The model index which is selected.
        """
        val = index.data(QtCore.Qt.UserRole)
        if val is not None:
            self.contextSelected.emit(val)

        if isinstance(val, contexts._trialBase):
            self.sessionSelected.emit(val)


class ContextSessionTreeView(QtGui.QTreeView, AbstractSessionContextWidgets):
    """
    Context Tree View for the AnimData
    """
    def __init__(self, parent=None):
        super(ContextSessionTreeView, self).__init__(parent=parent)

        # Model stuff
        mainModel = sessionModel.ShootSessionsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
                Day or Trial.
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        Args:
            index (QModelIndex): Model index to expand to.
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


class ContextSessionColumnView(columnFilterView.ColumModelView, AbstractSessionContextWidgets):
    """
    Context Column View for the AnimData
    """

    def __init__(self, parent=None):
        super(ContextSessionColumnView, self).__init__(parent=parent)

        # Model stuff
        mainModel = sessionModel.ShootSessionsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context, setFitlerText=True):
        """
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
                Day or Trial.
        Kwargs:
            setFitlerText (bool): If the filter text should be set to the given 
                context names.
        """
        if context is None:
                return
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx, setFitlerText=setFitlerText)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def projectColumnVisibility(self, state):
        self.setColumnVisibility(0, state)


class ContextSessionComboxBoxView(abstractComboView.ContextComboxBoxView, AbstractSessionContextWidgets):
    """
    Context Combo Box for the AnimData

    has two signals:
        contextSelected (Context): The currently selected context
        daySelected (Session): The currently selected Day context
    """
    def __init__(self, parent=None):
        mainModel = sessionModel.ShootSessionsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)

        super(ContextSessionComboxBoxView, self).__init__(parent=parent)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
            Session or Trial.
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def model(self):
        """
        Virtual Method
        """
        return sessionModel.ShootSessionsModel()

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ("Shoot:", 1),
               ("Session:", 2),
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        self.contextSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        self.sessionSelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getCurrentDay(self):
        trials = self.getSelectedTrials()
        if len(trials) == 1:
            return trials[0]
        return None

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        Return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedDays(self):
        """
        Get all the currently selected days contexts

        Return:
            a list of all the selected days contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts._trialBase):
            return [val]
        return []

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Session or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    win = ContextSessionTreeView()
    # win = ContextSessionColumnView()
    # win = ContextSessionComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.sessionSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
