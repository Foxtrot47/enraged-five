import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import projectsModel, proxyFilterModels
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView


class AbstractProjectWidgets(QtCore.QObject):
    """
    Abstract Context Widget mixin

    has two signals:
        contextSelected (Context): The currently selected context
    """
    contextSelected = QtCore.Signal(object)

    def __init__(self):
        super(AbstractProjectWidgets, self).__init__()

    def setContext(self, context):
        """
        VIRTUAL
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select Project
        """
        raise NotImplementedError("This method needs to be implemented in subclasses")

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        return [index.data(QtCore.Qt.UserRole) for index in self.selectedIndexes()]

    def getSelectedProjects(self):
        """
        get all the currently selected project contexts

        return:
            a list of all the selected project contexts
        """
        returnVals = []
        for index in self.selectedIndexes():
            val = index.data(QtCore.Qt.UserRole)
            if isinstance(val, contexts.Project):
                returnVals.append(val)
        return returnVals

    def _handleContextClick(self, index):
        """
        Internal Method

        Handle the selection change, to re-emit when a context object or trial is selected

        args:
            index (QModelIndex): The model index which is selected
        """
        val = index.data(QtCore.Qt.UserRole)
        if val is not None:
            self.contextSelected.emit(val)


class ProjectTreeView(QtGui.QTreeView, AbstractProjectWidgets):
    """
    Context Tree View for the AnimData
    """
    def __init__(self, parent=None):
        super(ProjectTreeView, self).__init__(parent=parent)

        # Model stuff
        self._mainModel = projectsModel.ProjectsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index

        args:
            index (QModelIndex): Model index to expand to
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions)


class ProjectColumnView(columnFilterView.ColumModelView, AbstractProjectWidgets):
    """
    Context Column View for the AnimData
    """

    def __init__(self, parent=None):
        super(ProjectColumnView, self).__init__(parent=parent)

        # Model stuff
        self._mainModel = projectsModel.ProjectsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        if context is None:
                return
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions)


class ProjectsComboxBoxView(abstractComboView.ContextComboxBoxView, AbstractProjectWidgets):
    """
    Context Combo Box for the AnimData

    has two signals:
        contextSelected (Context): The currently selected context
    """
    def __init__(self, parent=None):
        self._mainModel = projectsModel.ProjectsModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        super(ProjectsComboxBoxView, self).__init__(parent=parent)
        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def model(self):
        """
        Virtual Method
        """
        return self._proxyModel

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        self.contextSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedProjects(self):
        """
        get all the currently selected project contexts

        return:
            a list of all the selected project contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts.Project):
            return [val]
        return []

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select Project
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    # win = ProjectTreeView()
    win = ProjectsComboxBoxView()
    # win = ProjectColumnView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.contextSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
