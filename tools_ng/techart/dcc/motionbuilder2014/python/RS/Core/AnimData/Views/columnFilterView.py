import functools

from PySide import QtGui, QtCore


class ColumnSortFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    Custom Column Sort filter model to allow for multiple columns to have different filters
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._index = None
        super(ColumnSortFilterProxyModel, self).__init__(parent)

    def setSourceIndex(self, index):
        """
        Set the Root Souce index to fiter for

        args:
            index (QModelIndex): the root model index to filter children only
        """
        self._index = index

    def filterAcceptsRow(self, row, parent):
        """
        Returns if the row should be filtered out or not, only affects the row of the current set
        index
        """
        if self._index is None:
            return super(ColumnSortFilterProxyModel, self).filterAcceptsRow(row, parent)

        if parent != self._index:
            return True
        index = self.sourceModel().index(row, 0, parent)

        if self._index is not None:
            return self.filterRegExp().indexIn(index.data(0)) != -1
        return True


class ColumnModelViewColumnWidget(QtGui.QWidget):
    """
    Single Listbox and Edit text box widget
    """
    itemSelected = QtCore.Signal(object)
    customContextMenuRequested = QtCore.Signal(object, object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ColumnModelViewColumnWidget, self).__init__(parent=parent)
        self._idx = None
        self.setupUi()

    def setupUi(self):
        """
        Setup the UI
        """
        # Widgets and Layotus
        columnLayout = QtGui.QVBoxLayout()
        self._filterBox = QtGui.QLineEdit()
        self._modelBox = QtGui.QTableView()  # QtGui.QListView()
        self._proxyFilter = ColumnSortFilterProxyModel()

        # Assign widgets to Layouts
        columnLayout.addWidget(self._filterBox)
        columnLayout.addWidget(self._modelBox)

        # Configure Widgets and Layouts
        self._filterBox.setPlaceholderText("<Filter Text>")
        self._modelBox.setModel(self._proxyFilter)
        self._modelBox.horizontalHeader().setStretchLastSection(True)
        self._modelBox.verticalHeader().hide()
        self._modelBox.verticalHeader().setDefaultSectionSize(22)
        self.setHeaderVisability(False)
        self._modelBox.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        columnLayout.setContentsMargins(0, 0, 0, 0)
        self._modelBox.setSelectionMode(QtGui.QListView.ExtendedSelection)
        self._proxyFilter.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        palette = self._modelBox.palette()
        highlighedColour = palette.color(QtGui.QPalette.Normal, QtGui.QPalette.Highlight)
        colourText = "rgb({}, {}, {})".format(highlighedColour.red(), highlighedColour.green(), highlighedColour.blue())
        self._modelBox.setStyleSheet('QTableView::item::selected{background-color: %s;}' % (colourText))

        self.setLayout(columnLayout)

        # Setup Connections
        self._filterBox.textChanged.connect(self._handleFilterTextChaning)
        self._modelBox.clicked.connect(self._handleItemSelect)
        self._modelBox.customContextMenuRequested.connect(self._handleCustomContextMenuRequested)

    def setHeaderVisability(self, value):
        if value is True:
            self._modelBox.horizontalHeader().show()
        else:
            self._modelBox.horizontalHeader().hide()

    def resizeColumnsToFitContents(self):
        self._modelBox.resizeColumnsToContents()

    def setContextMenuPolicy(self, newPolicy):
        self._modelBox.setContextMenuPolicy(newPolicy)

    def setSelectionBehavior(self, value):
        self._modelBox.setSelectionBehavior(value)

    def setSelectionMode(self, value):
        self._modelBox.setSelectionMode(value)

    def setCurrentIndex(self, index):
        """
        Set the column filter text

        args:
                index (QModelIndex): The Model Index to set as the current index
        """
        self._modelBox.setCurrentIndex(self._proxyFilter.mapFromSource(index))
        self._modelBox.selectionModel().select(self._proxyFilter.mapFromSource(index), QtGui.QItemSelectionModel.Select)
        self.itemSelected.emit(index)

    def setFilterText(self, text):
        """
        Set the column filter text

        args:
                text (str): The string text to set as the filter string
        """
        self._filterBox.setText(text)

    def selectedIndexes(self):
        """
        Get the Currently selected indexs

        return:
                list of QModelIndexs of the selected items
        """
        return {sel.row():sel for sel in self._modelBox.selectedIndexes()}.values()

    def setAlternatingRowColors(self, val):
        """
        Set the Alternating Row Colours

        args:
            val (bool): If the alternative rows should be coloured
        """
        self._modelBox.setAlternatingRowColors(val)

    def _handleFilterTextChaning(self, newText):
        """
        Internal Method

        Update the filter to the newly entered text in the filter box
        """
        for escapedChar in ["[", "]"]:
            newText = newText.replace(escapedChar, "\{0}".format(escapedChar))
        self._proxyFilter.setFilterRegExp(newText)

    def _handleCustomContextMenuRequested(self, position):
        self.customContextMenuRequested.emit(self._modelBox, position)

    def _handleItemSelect(self, item):
        """
        Internal Method

        Handle the last item being selected, pass it back to the parent widget
        """
        self.itemSelected.emit(self._proxyFilter.mapToSource(item))

    def setModel(self, newModel, index=None):
        """
        Set the model to be used by the widget

        args:
            newModel (QAbstractItemModel): The Model to use

        kwargs:
            index (QModelIndex): The Root Index of the model
        """
        self._proxyFilter.setSourceModel(newModel)
        self._idx = index
        if index is not None:
            if newModel.canFetchMore(index) is True:
                newModel.fetchMore(index)
            self._proxyFilter.setSourceIndex(index)
            self._modelBox.setRootIndex(self._proxyFilter.mapFromSource(index))

    def rootIndex(self):
        return self._modelBox.rootIndex()

    def rootModelIndex(self):
        return self._modelBox.model().mapToSource(self._modelBox.rootIndex())


class ColumModelView(QtGui.QWidget):
    """
    ColumnModelView is similar to the QColumnView without all the extra baggage and with filter
    text inputs at the top of each column
    """
    clicked = QtCore.Signal(object)
    customContextMenuRequested = QtCore.Signal(object, object, object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ColumModelView, self).__init__(parent=parent)
        self._columns = []
        self._contextMenuPolicy = None
        self._selectionBehavior = None
        self._selectionMode = None
        self._columnVisibity = {}
        self._fixedColumnCount = -1
        self._blankColumns = []
        self._columnSplitterValues = []
        self._model = None
        self._altColours = False
        self.setupUi()

        self._tempColumnModelIndexes = []

    def model(self):
        """
        Get the current model for the view

        returns:
            The Qt Model driving the view
        """
        return self._model

    def setModel(self, model):
        """
        Set the model for the view

        args:
            model (Qt Model): The Model to drive the view
        """
        self._model = model
        self._clearModel()
        self._addColumn()
        self._columns[0].setModel(self._model)
        self._model.layoutChanged.connect(self._handleModelLayoutChange)
        self._model.layoutAboutToBeChanged.connect(self._handleModelBeginLayoutChange)

    def _handleModelLayoutChange(self):
        if len(self._tempColumnModelIndexes) == 0:
            return

        if not isinstance(self._model, QtGui.QAbstractProxyModel):
            return

        if len(self._columns) < 2:
            return

        for idx in xrange(1, len(self._columns)):
            modelIdx = self._tempColumnModelIndexes[idx]
            proxyFilterModelIdx = self._model.mapFromSource(modelIdx)

            if not proxyFilterModelIdx.isValid():
                self._clearModel(idx)
                break

        self._tempColumnModelIndexes = []

    def _handleModelBeginLayoutChange(self):
        if not isinstance(self._model, QtGui.QAbstractProxyModel):
            return

        self._tempColumnModelIndexes = [self._model.mapToSource(self._columns[idx].rootModelIndex()) for idx in xrange(len(self._columns))]

    def _clearModel(self, colIdx=1):
        """
        Internal Model

        Clear the model to a given column index, removing the column widgets as well

        kargs:
            coldIdx (int): The column to remove from
        """
        for _ in xrange(colIdx, len(self._columns)):
            self._columns[colIdx].setParent(None)
            self._columns[colIdx].deleteLater()
            self._columns.pop(colIdx)
        self._buildBlankColumns()

    def setAlternatingRowColors(self, val):
        """
        Set the Alternating Row Colours

        args:
            val (bool): If the alternative rows should be coloured
        """
        self._altColours = True
        for col in self._columns:
            col.setAlternatingRowColors(val)

    def setSelectionBehavior(self, value):
        self._selectionBehavior = value
        for col in self._columns:
            col.setSelectionBehavior(value)

    def setSelectionMode(self, value):
        self._selectionMode = value
        for col in self._columns:
            col.setSelectionMode(value)

    def getSelectedColumnsIndexes(self):
        """
        Get the selected indexes for each of the columns, each columns selected indexs as nested list

        return:
            list of list of QModelIndexs
        """
        return  [col.selectedIndexes() for col in self._columns]

    def getRootColumnIndexs(self):
        """
        Get the selected indexes for each of the columns, each columns selected indexs as nested list

        return:
            list of list of QModelIndexs
        """
        indexs = []
        for col in self._columns:
            rootIdx = col.rootIndex()
            if rootIdx.model() is not None:
                indexs.append(rootIdx.model().mapToSource(rootIdx))
            else:
                indexs.append(rootIdx)
        return indexs

    def setColumnsIndexs(self, indexList):
        """
        PROTOTYPE METHOD - DO NOT USE

        set the selected indexes for each of the columns

        args:
            indexList (QModelIndexs): list of list of QModelIndexs
        """
        for col, index in zip(self._columns[1:], indexList[1:]):
            col.setModel(index.model(), index)

    def setColumnVisibility(self, column, state):
        """
        Set the visibility on a given column to either show or hide

        args:
            column (int): Column to hide or show
            state (bool): if the column should be hidden or shown
        """
        if column < len(self._columns):
            self._columns[column].setVisible(state)
        self._columnVisibity[column] = state

    def storeColumnState(self):
        return {
                "columnSplitterValues": self._columnSplitterValues,
                "columnSplitterVisability": self._columnVisibity
                }

    def restoreColumnState(self, state):
        sizeValues = state.get("columnSplitterValues")
        if sizeValues is not None:
            self._columnSplitterValues = sizeValues
            self._splitter.setSizes(self._columnSplitterValues)

        splitVisability = state.get("columnSplitterVisability")
        if splitVisability is not None:
            self._columnVisibity = splitVisability

    def setupUi(self):
        """
        Setup the UI
        """
        # Layouts and Widgets
        layout = QtGui.QHBoxLayout()
        self._splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)

        layout.addWidget(self._splitter)
        self._addColumn()

        self._splitter.splitterMoved.connect(self._handleSplitterMoved)
        self.setLayout(layout)

    def _addColumn(self):
        """
        Internal Method

        Add a column to the view

        returns:
            A ColumnModelViewColumnWidget
        """
        newWid = ColumnModelViewColumnWidget()
        newWid.customContextMenuRequested.connect(functools.partial(self._handleCustomContextMenuRequested, len(self._columns)))
        if self._contextMenuPolicy is not None:
            newWid.setContextMenuPolicy(self._contextMenuPolicy)
        if self._selectionBehavior is not None:
            newWid.setSelectionBehavior(self._selectionBehavior)
        if self._selectionMode is not None:
            newWid.setSelectionMode(self._selectionMode)

        if self._fixedColumnCount != -1:
            for _ in xrange(0, len(self._blankColumns)):
                self._blankColumns[0].setParent(None)
                self._blankColumns[0].deleteLater()
                self._blankColumns.pop(0)

            if len(self._columns) >= self._fixedColumnCount:
                return newWid

        self._columns.append(newWid)
        self._splitter.addWidget(newWid)

        if self._fixedColumnCount != -1:
            for _ in xrange(self._fixedColumnCount - len(self._columns)):
                blankWid = ColumnModelViewColumnWidget()
                self._blankColumns.append(blankWid)
                self._splitter.addWidget(blankWid)

        currentSizes = self._splitter.sizes()
        if len(currentSizes) <= len(self._columnSplitterValues):
            self._splitter.setSizes(self._columnSplitterValues)

        newWid.setVisible(self._columnVisibity.get(len(self._columns) - 1, True))

        newWid.setAlternatingRowColors(self._altColours)
        newWid.itemSelected.connect(functools.partial(self._handleSingleSelection, len(self._columns)))
        return newWid

    def setFixedCoulmnCount(self, number):
        self._fixedColumnCount = number
        self._buildBlankColumns()
        colSize = float((self.width() or 1)) / (number or 1)
        colSizes = [colSize] * number

        self._splitter.setSizes(colSizes)
        self._columnSplitterValues = colSizes

    def _buildBlankColumns(self):
        for _ in xrange(0, len(self._blankColumns)):
            self._blankColumns[0].setParent(None)
            self._blankColumns[0].deleteLater()
            self._blankColumns.pop(0)

        if len(self._columns) >= self._fixedColumnCount:
            return

        for _ in xrange(self._fixedColumnCount - len(self._columns)):
            blankWid = ColumnModelViewColumnWidget()
            self._blankColumns.append(blankWid)
            self._splitter.addWidget(blankWid)
        self._splitter.setSizes(self._columnSplitterValues)

    def resizeInternalColumnsToFitContents(self, col):
        self._columns[col].resizeColumnsToFitContents()

    def setColumnHeaderVisability(self, col, value):
        self._columns[col].setHeaderVisability(value)

    def selectedIndexes(self):
        """
        Get a list of the currently selected indexes
        """
        return self._columns[-1].selectedIndexes()

    def setContextMenuPolicy(self, newPolicy):
        self._contextMenuPolicy = newPolicy
        for col in self._columns:
            col.setContextMenuPolicy(newPolicy)

    def _handleSplitterMoved(self, pos, item):
        sizes = self._splitter.sizes()
        if len(sizes) > 1:
            self._columnSplitterValues = sizes + self._columnSplitterValues[len(sizes):]

    def _handleSingleSelection(self, idx, item):
        """
        Internal Method

        Handle the selection chaning on a column view, to add more children bassed off its selection

        args:
            idx (QModelIndex): The Model index of what was selected
            item (QAbstractItemModel): The model item which was selected
        """
        self.clicked.emit(item)
        self._clearModel(idx)

        if item.model().hasChildren(item):
            newWid = self._addColumn()
            newWid.setModel(self._model, item)

    def _handleCustomContextMenuRequested(self, index, parent, position):
        self.customContextMenuRequested.emit(index, parent, position)

    def setParentContext(self, index, setFitlerText=True):
        """
        Set parent context,helpful to set the parents context allowing for a bottom up setting approach

        args:
            index (QModelIndex): Index to set

        kwargs:
            setFitlerText (Bool): If the filter text should be set to the given data text
        """
        self._setParentContext(index, setFilterText=setFitlerText)

    def _setParentContext(self, index, setFitlerText=True):
        """
        Internal Method

        Set parent context,helpful to set the parents context allowing for a bottom up setting approach

        args:
            index (QModelIndex): Index to set

        kwargs:
            setFitlerText (Bool): If the filter text should be set to the given data text
        """
        if not index.isValid():
            self._clearModel(0)
            return 0

        col = self._setParentContext(index.parent(), setFitlerText=setFitlerText)
        newWid = self._addColumn()
        newWid.setModel(self._model, index.parent())
        if setFitlerText is True:
            self._columns[col].setFilterText(index.data(QtCore.Qt.DisplayRole))
        self._columns[col].setCurrentIndex(index)

        return col + 1
