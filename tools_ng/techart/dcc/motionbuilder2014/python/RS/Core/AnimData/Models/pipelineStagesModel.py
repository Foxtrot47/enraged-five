"""
Motion Trials Selects Model
 - Seemed easier than trying to hack it with QTableWidgetItem's
"""
from PySide import QtGui, QtCore

from RS.Core.AnimData import Const
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem, baseModel


class PipelineStagesModelItem(baseModelItem.BaseModelItem):
    """
    This represents a single select
    """
    def __init__(self, pipelineStage, parent=None):
        """
        Constructor
        """
        super(PipelineStagesModelItem, self).__init__(parent=parent)
        self._pipelineStage = pipelineStage

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return str(self._pipelineStage.stepNumber())
            elif column == 1:
                return self._pipelineStage.name
            elif column == 2:
                baseText = "{0} {1}".format(self._pipelineStage.logStatus(), self._pipelineStage.logInfo() or "")
                if self._pipelineStage.isRework() is True:
                    
                    baseText = "{0}\n{1}".format(baseText, self._pipelineStage.reworkNotes())
                return baseText
            
            elif column == 3:
                return self._pipelineStage.reworkNotes()
                
        elif role == QtCore.Qt.BackgroundColorRole:
            if column == 2:
                if self._pipelineStage.isRework() is True:
                    return QtGui.QColor("#d20007")
                elif self._pipelineStage.logStatus() == "Completed":
                    return QtGui.QColor("#8CD18C")
                else:
                    return QtGui.QColor("#ce5b00")

        elif role == QtCore.Qt.UserRole:
            return self._pipelineStage

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class PipelineStagesModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, trial=None, parent=None):
        self._trial = trial
        super(PipelineStagesModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["#", "Step", "Log"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def getTrialContext(self):
        return self._trial

    def setTrialContext(self, trial):
        self._trial = trial
        self.reset()

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        if self._trial is None:
            return

        for select in self._trial.getPipelineStages():
            parent.appendChild(PipelineStagesModelItem(select, parent=parent))


if __name__ == "__main__":
    import sys
    from RS.Core.AnimData import Context
    app = QtGui.QApplication(sys.argv)

    trial = Context.animData.getTrialByID(101567)  # Redemption2/009532_04_BE_RHMR0_IG2_HOSEA_DISTRACTS_FAMILY_P1

    mainModel = PipelineStagesModel(trial)
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
