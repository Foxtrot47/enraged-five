import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import stages
from RS.Core.AnimData.Models import stageModel
from RS.Core.AnimData.Views import abstractComboView


class StageContextWidget(abstractComboView.ContextComboxBoxView):
    """
    Stage Context Widget for the AnimData
    """
    locationSelected = QtCore.Signal(object)
    stageSelected = QtCore.Signal(object)

    def __init__(self, parent=None, includeLocalLocation=False):
        """
        Constructor

        kargs:
            includeLocalLocation (bool): if the local location/stage should be added
        """
        self._includeLocalLocation = includeLocalLocation
        super(StageContextWidget, self).__init__(parent=parent)

    def model(self):
        """
        Virtual Method
        """
        return stageModel.StageModel(includeLocalLocation=self._includeLocalLocation)

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
                ("Location:", 0),
                ("Stage:", 1),
                ]

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected
        """
        self.locationSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected
        """
        self.stageSelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getSelectedLocation(self):
        """
        Get the currently selected contexts

        return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return contextBox.itemData(self.boxes[0].currentIndex(), QtCore.Qt.UserRole)

    def getSelectedStage(self):
        """
        get all the currently selected trial contexts

        return:
            a list of all the selected trial contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, stages.Stage):
            return val
        return None

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findStageContextIndex(context)
        self._setParentContext(contextIdx)


if __name__ == "__main__":
    from RS.Core.AnimData import Context
    from RS.Core.AnimData._internal import services

    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedStage()
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    win = StageContextWidget()
    # win = ContextColumnView()
    # win = ContextComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.locationSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
