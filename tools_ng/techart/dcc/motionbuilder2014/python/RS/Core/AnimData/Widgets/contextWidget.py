import functools

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData.Models import contextModel, proxyFilterModels
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData.Views import abstractComboView, columnFilterView


class AbstractContextWidgets(QtCore.QObject):
    """
    Abstract Context Widget mixin

    has two signals:
        contextSelected (Context): The currently selected context
        trialSelected (_trialBase): The currently selected Trial context
    """
    contextSelected = QtCore.Signal(object)
    trialSelected = QtCore.Signal(object)

    def __init__(self):
        super(AbstractContextWidgets, self).__init__()

    def setContext(self, context):
        """
        VIRTUAL
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
            Day or Trial.
        """
        raise NotImplementedError("This method needs to be implemented in subclasses")

    def getSelectedContexts(self):
        """
        Get the currently selected contexts

        Return:
            a list of context objects which are selected
        """
        return [index.data(QtCore.Qt.UserRole) for index in self.selectedIndexes()]

    def getSelectedTrials(self):
        """
        Get all the currently selected trial contexts

        Return:
            a list of all the selected trial contexts
        """
        returnVals = []
        for index in self.selectedIndexes():
            val = index.data(QtCore.Qt.UserRole)
            if isinstance(val, contexts._trialBase):
                returnVals.append(val)
        return returnVals

    def _handleContextClick(self, index):
        """
        Internal Method

        Handle the selection change, to re-emit when a context object or trial 
        is selected

        Args:
            index (QModelIndex): The model index which is selected.
        """
        val = index.data(QtCore.Qt.UserRole)
        if val is not None:
            self.contextSelected.emit(val)

        if isinstance(val, contexts._trialBase):
            self.trialSelected.emit(val)


class ContextTreeView(QtGui.QTreeView, AbstractContextWidgets):
    """
    Context Tree View for the AnimData
    """
    def __init__(self, parent=None):
        super(ContextTreeView, self).__init__(parent=parent)

        # Model stuff
        mainModel = contextModel.AnimDataModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Re-implemented AbstractContextWidgets mixin method.
        
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
                Day or Trial.
        """
        contextIdx = self.model().findContextIndex(context)

        # Set Selection
        selection = self.selectionModel()
        selection.select(contextIdx, QtGui.QItemSelectionModel.Select)
        self.setSelectionModel(selection)

        # Expand to selection
        self.expandChildren(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def expandChildren(self, index):
        """
        Method to expand the view to the given index.

        Args:
            index (QModelIndex): Model index to expand to.
        """
        if not index.isValid():
            return
        self.expandChildren(index.parent())

        self.setExpanded(index, True)


class ContextColumnView(columnFilterView.ColumModelView, AbstractContextWidgets):
    """
    Context Column View for the AnimData.
    """

    def __init__(self, parent=None):
        super(ContextColumnView, self).__init__(parent=parent)

        # Model stuff
        mainModel = contextModel.AnimDataModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)
        self.setModel(self._proxyModel)

        # Customise
        self.setAlternatingRowColors(True)
        self.clicked.connect(self._handleContextClick)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context, setFitlerText=True):
        """
        Re-implemented AbstractContextWidgets mixin method.
        
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
                Day or Trial.
        Kwargs:
            setFitlerText (bool): If the filter text should be set to the given 
                context names.
        """
        if context is None:
                return
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx, setFitlerText=setFitlerText)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def getSelectedProject(self):
        projs = self.getSelectedColumnsIndexes()[0]
        if len(projs) > 0:
            proj = projs[0].data(QtCore.Qt.UserRole)
            if isinstance(proj, contexts.Project):
                return proj
        return None

    def projectColumnVisibility(self, state):
        self.setColumnVisibility(0, state)


class ContextComboxBoxView(abstractComboView.ContextComboxBoxView, AbstractContextWidgets):
    """
    Context Combo Box for the AnimData.

    has two signals:
        contextSelected (Context): The currently selected context
        trialSelected (_trialBase): The currently selected Trial context
    """
    def __init__(self, parent=None):
        mainModel = contextModel.AnimDataModel()
        self._proxyModel = proxyFilterModels.AnimDataTrialsModelFilterModel()
        self._proxyModel.setSourceModel(mainModel)

        super(ContextComboxBoxView, self).__init__(parent=parent)

        # Set To Project By Default
        self.setContext(Context.animData.getCurrentProject())

    def setContext(self, context):
        """
        Re-implemented AbstractContextWidgets mixin method.
        
        Set the selected context of the widget.

        Args:
            context (AnimData Context): The context to select, Project, Shoot, 
            Day or Trial.
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None,
                         completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes,
                                          selectesOnly=selectesOnly,
                                          sortAssending=sortAssending,
                                          completeSessions=completeSessions,
                                          filterPipeline=filterPipeline)

    def model(self):
        """
        Virtual Method
        """
        return self._proxyModel

    def _boxDef(self):
        """
        Internal Method

        Virtual Method
        """
        return [
               ("Project:", 0),
               ("Shoot:", 1),
               ("Capture Day:", 2),
               ("Trial:", 3),
               ]

    def _handleContextClick(self, index):
        """
        Re-implemented
        """
        pass

    def itemSelected(self, boxIndex):
        """
        Method for when any box is selected.
        """
        self.contextSelected.emit(self.boxes[boxIndex].itemData(self.boxes[boxIndex].currentIndex(), QtCore.Qt.UserRole))

    def childItemSelected(self):
        """
        Method for when the bottom most box is selected.
        """
        self.trialSelected.emit(self.boxes[-1].itemData(self.boxes[-1].currentIndex(), QtCore.Qt.UserRole))

    def getCurrentContext(self):
        return self.getSelectedContexts()[0]

    def getCurrentTrial(self):
        trials = self.getSelectedTrials()
        if len(trials) == 1:
            return trials[0]
        return None

    def getSelectedContexts(self):
        """
        Get the currently selected contexts.

        Return:
            a list of context objects which are selected
        """
        contextBox = self._getDeepestEditedBox()
        return [contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)]

    def getSelectedTrials(self):
        """
        Get all the currently selected trial contexts.

        Return:
            a list of all the selected trial contexts
        """
        contextBox = self._getDeepestEditedBox()
        val = contextBox.itemData(contextBox.currentIndex(), QtCore.Qt.UserRole)
        if isinstance(val, contexts._trialBase):
            return [val]
        return []

    def setContext(self, context):
        """
        Set the selected context of the widget

        args:
            context (AnimData Context): The context to select, Project, Shoot, Day or Trial
        """
        contextIdx = self.model().findContextIndex(context)
        self._setParentContext(contextIdx)

    def setFilterOptions(self, takeTypes=None, selectesOnly=None, sortAssending=None, completeSessions=None, filterPipeline=None):
        self._proxyModel.setFilterOptions(takeTypes=takeTypes, selectesOnly=selectesOnly, sortAssending=sortAssending, completeSessions=completeSessions, filterPipeline=filterPipeline)


if __name__ == "__main__":
    def _handleSelect(item):
        print item

    def _handleButton(*args, **kwargs):
        print win.getSelectedContexts()

    import sys
    app = QtGui.QApplication(sys.argv)
    wid = QtGui.QWidget()
    lay = QtGui.QVBoxLayout()
    # win = QtGui.QColumnView()
    # win = ContextTreeView()
    win = ContextColumnView()
    # win = ContextComboxBoxView()
    but = QtGui.QPushButton("Test")
    lay.addWidget(win)
    lay.addWidget(but)
    wid.setLayout(lay)
    but.pressed.connect(_handleButton)
    win.trialSelected.connect(_handleSelect)

    wid.show()
    sys.exit(app.exec_())
