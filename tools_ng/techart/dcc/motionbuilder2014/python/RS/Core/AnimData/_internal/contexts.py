"""
Module for all the different context types, how they all fit together.

More info: https://hub.rockstargames.com/display/RSGTECHART/AnimData
"""
import os
import json
from datetime import datetime
from dateutil import parser
import time
import collections

from RS.Tools.CameraToolBox.PyCore.Decorators import memorize
from RS.Core.AnimData import Const


class _contextBase(object):
    """
    Internal Base Class

    Base Class for all contexts

    Notes:
        In order to prevent a cyclic import dependency, this module must access type data via:
            self._services.animDataSettingsManager.getterMethod()
    """
    DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

    def __init__(self, services):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
        """
        super(_contextBase, self).__init__()
        self._services = services

    def getServices(self):
        """
        Get the services with the current context.

        return:
            (AnimData.services.Services): The services for communicating with databases
        """
        return self._services

    @staticmethod
    def _formatText(value):
        """
        Formats the text input so that it can be passed into a Watchstar proc.

        Primarily used for the notes and comments fields.

        Args:
            value (str): The text destined for a field in a Watchstar object.

        Returns:
            str
        """
        return value.replace("'", "''")

    @staticmethod
    def _textBoolToBool(value):
        """
        Internal Method

        Convert a text bool "true" "TRUE" to a python bool value.

        args:
            value (string): The input string value
        return:
            bool value from the input string
        """
        return value.lower() == "true" or value.lower() == "1"

    def _handleProjectCreation(self, data):
        """
        Internal Method

        Handles creating the projects from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database

        return:
            list of Projects (Projects) objects
        """
        dataDicts = data.get("anyProjects", [])
        return [Project(self.getServices(), dataDict) for dataDict in dataDicts]

    def _handleShootsCreation(self, data, project):
        """
        Internal Method

        Handles creating the shoots from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database
            project (Project): The current project context

        return:
            list of Shoot (Shoots) objects
        """
        dataDicts = data.get("shoots", [])
        return [Shoots(project, self._services, dataDict) for dataDict in dataDicts]

    def _handleShotCreation(self, data, shoot, project):
        """
        Internal Method

        Handles creating the shoots from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            shoot (Shoots): The parent shoot which the shots lives under
            project (Project): The current project context

        return:
            list of shots (Shots) objects
        """
        dataDicts = data.get("shots", [])
        return [Shot(shoot, project, self._services, dataDict) for dataDict in dataDicts]

    def _handleFeatureCreation(self, data, project):
        """
        Internal Method

        Handles creating the Features from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of Feature objects
        """
        featureTypeDict = {
            self._services.animDataSettingsManager.consts.FeatureCategories.CUTSCENE: Cutscene,
            self._services.animDataSettingsManager.consts.FeatureCategories.INGAME: InGame,
            self._services.animDataSettingsManager.consts.FeatureCategories.ASSET: AssetFeature,
            self._services.animDataSettingsManager.consts.FeatureCategories.GENERIC: GenericFeature,
        }

        returnFeatures = []

        dataDicts = data.get("feature", [])
        if len(dataDicts) == 0:
            return returnFeatures

        for dataDict in dataDicts:
            featureTypeSetting = self.getServices().animDataSettingsManager.getFeatureTypes(dataDict.get("featureTypeID", 0))
            featureCat = self.getServices().animDataSettingsManager.getFeatureCategoryByName(featureTypeSetting.featureCategory())
            featureClass = featureTypeDict.get(featureCat)
            if featureClass is None:
                raise ValueError("Unknown Feature type of '{0}'".format(dataDict.get("featureTypeID", "-1")))
            returnFeatures.append(featureClass(project, self._services, dataDict))
        return returnFeatures

    def _handleCutscenesCreation(self, data, project):
        """
        Internal Method

        Handles creating the CutScenes from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of cut scene (Cutscene) objects
        """
        dataDicts = data.get("feature", [])
        return [Cutscene(project, self._services, dataDict) for dataDict in dataDicts]

    def _handleSessionCreation(self, data, shoot, project):
        """
        Internal Method

        Handles creating the sessions from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            shoot (Shoots): The parent shoot which the session lives under
            project (Project): The current project context

        return:
            list of session (Session) objects
        """
        dataDicts = data.get("shootSessions")
        return [Session(shoot, project, self._services, dataDict) for dataDict in dataDicts]

    def _handleTrialCreation(self, data, session, shoot, project):
        """
        Internal Method

        Handles creating the motion trials from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            session (Session): The parent session which the trial lives under
            shoot (Shoots): The parent shoot which the session lives under
            project (Project): The current project context

        return:
            list of motion trials (MotionTrial) objects
        """
        captureTypeDict = {
            "Trial - Trial": MotionTrialBase,
            "Init - Body ROM": BodyRomTrial,
            "Init - Body Scale": BodyScaleTrial,
            "Init - Face ROM": FaceRomTrial,
            "Init - Head Scan": FaceScanTrial,
        }
        featureTypeDict = {
            self._services.animDataSettingsManager.consts.FeatureCategories.CUTSCENE: CutSceneTrial,
            self._services.animDataSettingsManager.consts.FeatureCategories.INGAME: InGameTrial,
            self._services.animDataSettingsManager.consts.FeatureCategories.GENERIC: GenericTrial,
            self._services.animDataSettingsManager.consts.FeatureCategories.ASSET: AssetTrial,
        }
        returnTrials = []

        dataDicts = data.get("captures", [])
        if len(dataDicts) == 0:
            return returnTrials

        for dataDict in dataDicts:
            capType = self.getServices().animDataSettingsManager.getCaptureTypes(dataDict.get("captureTypeID", "0"))
            trialClass = captureTypeDict.get(capType.name)
            if trialClass is None:
                raise ValueError("Unknown Trial type of '{0}'".format(dataDict.get("captureTypeID", "-1")))
            if trialClass is MotionTrialBase:
                featureTypeId = dataDict.get("featureTypeID", "-1")
                featureTypeSetting = self.getServices().animDataSettingsManager.getFeatureTypes(featureTypeId)
                featureCat = self.getServices().animDataSettingsManager.getFeatureCategoryByName(featureTypeSetting.featureCategory())
                trialClass = featureTypeDict.get(featureCat)
                if trialClass is None:
                    print "[WARNING] Unsupported feature type '{}'!".format(featureTypeSetting.name)
                    continue
                returnTrials.append(trialClass(session, shoot, project, self._services, dataDict))
            else:
                returnTrials.append(trialClass(project, self._services, dataDict))
        return returnTrials

    def _handleSelectCreation(self, data, parentContext):
        """
        Internal Method

        Handles creating the selects from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            parentContext (contexts.Trial | contexts.MotionTrialSelect): The context to which the select is linked to.

        return:
            list of MotionSelect objects
        """
        dataDicts = data.get("productionActions", [])
        return [MotionSelect(parentContext, self._services, dataDict) for dataDict in dataDicts]

    def _handleTrialSelectCreation(self, data, trial, project):
        """
        Internal Method

        Handles creating the trial selects from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            trial (Trial): The trial context to which the trial select is linked to.
            project (Project): The current project context.

        return:
            list of MotionTrialSelect objects
        """
        dataDicts = data.get("trialSelects", [])
        return [MotionTrialSelect(trial, project, self._services, dataDict) for dataDict in dataDicts]

    def _handleCompositeCreation(self, data, project):
        """
        Internal Method

        Handles creating the motion Composite from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of motion Composite (MotionComposite) objects
        """
        dataDicts = data.get("motionScenes", [])
        return [MotionComposite(project, self._services, dataDict) for dataDict in dataDicts]

    def _handleNoteCreation(self, data, trial, project):
        """
        Internal Method

        Handles creating the motion trial notes from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            trial (MotionTrial): The motion trial where the notes are for.
            project (Project): The current project context

        return:
            list of motion trials notes (MotionTrialNote) objects
        """
        # Filter out 'notes' with no actual notes data on them
        dataDicts = [dataDict for dataDict in data.get("productionCrew", []) if dataDict["notes"] != ""]
        return [MotionTrialNote(trial, project, self._services, dataDict) for dataDict in dataDicts]

    def _handlePipelineStagesCreation(self, data, trial):
        """
        Internal Method

        Handles creating the pipeline stages from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            trial (MotionTrial): The motion trial where the pipeline stages are for.

        return:
            list of pipeline stages (PipelineStage) objects
        """
        dataDicts = data.get("capturePipeline", [])
        return [PipelineStage(trial, self._services, dataDict) for dataDict in dataDicts]

    def _handleGenericFileCreation(self, dataDicts, castingClass, parent, project, fileTable):
        """
        Internal Method

        Handles creating the Generic Files from the returned Watchstar data.

        args:
            dataDicts (list): The file dicts pre-extracted from the Watchstar database's JSON dict.
            trial (MotionTrial): The motion trial where the videos are for.
            project (Project): The current project context
            fileTable (Const.FilePathTables): The file path table to use for operations

        return:
            list of files (AssociatedFile) objects
        """
        return [castingClass(fileTable, parent, project, self._services, dataDict) for dataDict in dataDicts]

    @memorize.memoized
    def _handleProductionAssetCreation(self, data, project, assetClassFilter=None):
        """
        Internal Method

        Handles creating the ProductionAsset classes from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        kwargs:
            assetClassFilter (settings.AssetClass): If specified, only assets matching this asset class are returned.
                default: None

        return:
            list of _productionAssetBase derived objects
        """
        dataDicts = data.get("productionAssets", [])

        assetClassDict = {
            "Animal": Animal,
            "Character": Character,
            "Environment": Environment,
            "Prop": Prop,
            "Vehicle": Vehicle,
        }
        contexts = []
        for dataDict in dataDicts:
            assetClassID = dataDict.get("assetClassID", -1)
            if assetClassID > 0:
                assetClassSetting = self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassID)
            elif assetClassID == -1 and "physicalPropVersionID" in set(dataDict.keys()):
                # This is a known use case and we must treat this asset as a prop. It should only happen for something
                # like a prop clone, but the operator might not have known which script/game asset to use at the time.
                assetClassSetting = self._services.animDataSettingsManager.consts.AssetClasses.PROP
            else:
                # Ignore anything else that doesn't have an asset class ID defined.
                continue
            if assetClassFilter is not None and assetClassSetting.settingsID != assetClassFilter.settingsID:
                continue
            assetClass = assetClassDict[assetClassSetting.name]
            contexts.append(assetClass(self._services, project, dataDict))
        return contexts

    @memorize.memoized
    def _handleAsset3dCreation(self, data, project):
        """
        Internal Method

        Handles creating the 3d Assets from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of _asset3DBase derived objects
        """
        dataDicts = data.get("3DAssets", [])

        assetClassDict = {
            "Animal": GameAnimal,
            "Character": GameCharacter,
            "Environment": GameEnvironment,
            "Prop": GameProp,
            "Vehicle": GameVehicle,
        }
        contexts = []
        for dataDict in dataDicts:
            assetClassID = dataDict["assetClassID"]
            assetClassSetting = self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassID)
            assetClass = assetClassDict[assetClassSetting.name]
            contexts.append(assetClass(self._services, project, dataDict))
        return contexts

    def _handleMocapAssetCreation(self, data, project, parentAsset):
        """
        Internal Method

        Handles creating the Mocap Assets from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context.
            parentAsset (AssetGameBase): The parent 3d asset for the mocap asset.

        return:
            list of MocapAsset objects
        """
        dataDicts = data.get("mocapAssets", [])
        # Filter out any assets that are not for the current project.
        dataDicts = [dataDict for dataDict in dataDicts if dataDict.get("anyProjectID") == project.id]
        return [MocapAsset(self._services, project, parentAsset, dataDict) for dataDict in dataDicts]

    @memorize.memoized
    def _handleAssetScriptCreation(self, data, project):
        """
        Internal Method

        Handles creating the Script Characters from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of AssetScriptBase derived objects
        """
        dataDicts = data.get("scriptAssets", [])

        assetClassDict = {
            "Animal": ScriptAnimal,
            "Character": ScriptCharacter,
            "Environment": ScriptEnvironment,
            "Prop": ScriptProp,
            "Vehicle": ScriptVehicle,
        }
        contexts = []
        for dataDict in dataDicts:
            assetClassID = dataDict["assetClassID"]
            assetClassSetting = self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassID)
            assetClass = assetClassDict[assetClassSetting.name]
            contexts.append(assetClass(self._services, project, dataDict))
        return contexts

    @memorize.memoized
    def _handleTalentCreation(self, data, project=None):
        """
        Internal Method

        Handles creating the talent from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of talent (Talent) objects
        """
        dataDicts = data.get("talent", [])
        talent = [Talent(self._services, dataDict, project) for dataDict in dataDicts]
        talent.sort()
        return talent

    @memorize.memoized
    def _handleTalentVariationCreation(self, data, project):
        """
        Internal Method

        Handles creating the talent variation from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of talent variation (TalentVariation) objects
        """
        dataDicts = data.get("talentVariation", [])
        return [TalentVariation(self._services, project, dataDict) for dataDict in dataDicts]

    def _handleProbspecCreation(self, data, project):
        """
        Internal Method

        Handles creating the probspecs from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): The current project context

        return:
            list of probspecs (Probspec) objects
        """
        dataDicts = data.get("probspecs", [])
        return [Probspec(self._services, project, dataDict) for dataDict in dataDicts]

    def _handleMocapPropCreation(self, data, project):
        """
        Internal Method

        Handles creating the mocap props from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): Project that the prop version is under

        return:
            list of mocap prop (MocapProp) objects
        """
        dataDicts = data.get("stageProps", [])
        return [MocapProp(project, self._services, dataDict) for dataDict in dataDicts]

    def _handleMocapPropVariationCreation(self, data, mocapProp, project):
        """
        Internal Method

        Handles creating the mocap prop variation from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            mocapProp (MocapProp): The prop the variation is for
            project (Project): Project that the prop version is under

        return:
            list of mocap prop variation (MocapPropVariation) objects
        """
        return [MocapPropVariation(mocapProp, project, self._services, dataDict) for dataDict in data]

    def _handleMocapPropVersionCreation(self, data, mocapPropVariation, project):
        """
        Internal Method

        Handles creating the mocap prop versions from the returned Watchstar data.

        args:
            data (list): List of the physicalPropVersion JSON dict from the Watchstar database.
            mocapPropVariation (MocapPropVariation): The variation the version is for
            project (Project): Project that the prop version is under

        return:
            list of mocap prop versions (MocapPropVersion) objects
        """
        return [MocapPropVersion(mocapPropVariation, project, self._services, dataDict) for dataDict in data]

    def _handleStageCalibrationCreation(self, data):
        """
        Internal Method

        Handles creating the Stage Calibrations from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.

        return:
            list of stage calibration (StageCalibration) objects
        """
        dataDicts = data.get("stageCalibrations", [])
        return [StageCalibration(self._services, dataDict) for dataDict in dataDicts]

    def _handleCustomMocapTrialListCreation(self, data, project):
        """
        Internal Method

        Handles creating the Custom Mocap Trial Lists from the returned Watchstar data.

        args:
            data (dict): The JSON dict from the Watchstar database.
            project (Project): Project that the shoot is under

        return:
            list of files (AssociatedCompositeFile) objects
        """
        dataDicts = data.get("mocapLists", [])
        return [CustomMocapTrialList(project, self._services, dataDict) for dataDict in dataDicts]

    def _handleHelmetCamCreation(self, data):
        """
        Internal Method

        Handles creating the HelmetCam from the returned Watchstar data.

        Args:
            data (dict): The JSON dict from the Watchstar database.

        Returns:
            list of HelmetCam objects
        """
        dataDicts = data.get("helmetCams", [])
        return [HelmetCam(self._services, dataDict) for dataDict in dataDicts]

    def _handleHelmetCamCalibrationCreation(self, data):
        """
        Internal Method

        Handles creating the HelmetCamCalibrations from the returned Watchstar data.

        Args:
            data (dict): The JSON dict from the Watchstar database.

        Returns:
            list of HelmetCamCalibration objects
        """
        dataDicts = data.get("helmetCamCalibrations", [])
        return [HelmetCamCalibration(self._services, dataDict) for dataDict in dataDicts]

    def _handleHelmetCamRevisionCreation(self, data):
        """
        Internal Method

        Handles creating the HelmetCamRevisions from the returned Watchstar data.

        Args:
            data (dict): The JSON dict from the Watchstar database.

        Returns:
            list of HelmetCamRevision objects
        """
        dataDicts = data.get("helmetCamRevisions", [])
        return [HelmetCamRevision(self._services, dataDict) for dataDict in dataDicts]

    def _convertUtcDbStringToLocalTime(self, dbUtcTimeString):
        """
        Internal Method

        Convert the UTC database string into a local time string based on the current timezone
        """
        utcDt = parser.parse(dbUtcTimeString)
        epoch = time.mktime(utcDt.timetuple())
        offsetDt = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
        localDt = utcDt + offsetDt
        return localDt.strftime(self.DATE_FORMAT)

    @property
    def name(self):
        """
        Name of the context, reimplement in subclass if needed
        """
        return ""

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.name == other.name

    def __ne__(self, other):
        """
        Not equals method, checks for types and names to check that two contexts are the not same
        """
        return not self == other

    def __hash__(self):
        return self.name.__hash__()


class _dataDictContextBase(_contextBase):
    """
    Internal Base Class

    Base class for contexts with a data dictionary.
    """
    def __init__(self, services, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(_dataDictContextBase, self).__init__(services)
        self._dataDict = dataDict

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}'".format(self.__class__.__name__, self.name)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} (id: {}) at {}>".format(self.__str__(), self.id, hex(id(self)))

    def getDataDict(self):
        """
        Get the data dict for the context

        return:
             A nested dict of values from the database
        """
        return self._dataDict

    @property
    def id(self):
        """
        Get the ID of the Watchstar object.

        return:
            int: The object's ID
        """
        return int(self._dataDict["id"])

    @property
    def name(self):
        """
        Get the name of the Watchstar object.

        warning:
            This key is NOT present in all object json.

        return:
            str
        """
        return str(self._dataDict.get("name", ""))

    def __lt__(self, other):
        """
        Helps with the sorting of objects by their name
        """
        if not isinstance(other, _dataDictContextBase):
            return False
        return self.name < other.name

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.id == other.id


class _fileContextBase(_dataDictContextBase):
    """
    Internal Base Class

    Base class for all contexts which store files.

    File getter methods:
        * Methods must expect that the specified file type doesn't exist.
        * If specified file type does not exist, return None.
    """
    def __init__(self, fileTable, fileCasting, services, dataDict):
        """
        Constructor

        args:
            fileTable (Const.FilePathTables): The file path table to use for operations
            fileCasting (_baseFilePath): Class to cast the file objects as
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        self._fileTable = fileTable
        self._fileCasting = fileCasting
        super(_fileContextBase, self).__init__(services, dataDict)

    def _refresh(self):
        """
        Virtual method

        Requery the object and set all internal data as needed.
        """
        raise NotImplementedError("Needs reimplemented in base class!")

    def _getFilePaths(self, pathType=None, refresh=False):
        """
        Internal method that returns the raw "filePaths" data.

        Args:
            pathType (settings.FilePathTypes): The path type to filter the returned list of files with.
            refresh (bool): If True, will run the context's refresh method before getting files.

        Returns:
            list
        """
        if refresh is True:
            self._refresh()

        filePaths = self._dataDict.get("filePaths", [])
        if pathType is not None:
            filePaths = [path for path in filePaths if path.get("pathTypeID") == pathType.settingsID]
        return filePaths

    def getAllFiles(self, pathType=None, refresh=False):
        """
        Get all the files associated with the trial as a list

        return:
            List of AssociatedFile objects
        """
        dataDicts = self._getFilePaths(pathType=pathType, refresh=refresh)
        return self._handleGenericFileCreation(dataDicts, self._fileCasting, self, self.project(), self._fileTable)

    def addNewFile(self, fileType, filePath, thumbnailPath=None):
        """
        Add a new file to the object, given its type, path and thumbnail path

        args:
            fileType (settings.FilePathType): The type of the file
            filePath (str): The string P4 Path to the file
            thumbnailPath (str): The string path to the thumbnail
        """
        if filePath == "" or filePath is None:
            raise ValueError("Unable to set a file path to an empty string")

        # Without this check, the db query may raise a "THIS PATH ALREADY EXISTS FOR THE SPECIFIED CAPTURE" error.
        filePaths = [fileObj.rawFilePath() for fileObj in self.getAllFiles()]
        if filePath in filePaths:
            return

        val = []
        # TODO: Readd this back in
        # val.append("@ThumbnailPath='{0}'".format(newThumbnailPath))

        self._services.watchstar.executeCommand("toolswritepub.AddFilePathAssociation",
                                                [
                                                    self._services.getUserName(),
                                                    self._fileTable,
                                                    self.id,
                                                    fileType.settingsID,
                                                    filePath,
                                                ],
                                                kwargList=val
                                                )


class ProjectConfigSetting(_dataDictContextBase):
    """
    Context object for a project config setting
    """
    def __init__(self, services, projectConfig, dataDict):
        super(ProjectConfigSetting, self).__init__(services, dataDict)
        self._projectConfig = projectConfig

    def projectConfig(self):
        """
        The project that the config belongs to.

        return:
            str: contexts.Projects
        """
        return self._projectConfig

    @property
    def id(self):
        """
        Reimplemented from _dataDictContextBase.

        Get the ID of the Watchstar object.

        return:
            int: The object's ID
        """
        return int(self._dataDict["projectConfigSettingsID"])

    @property
    def name(self):
        """
        Reimplemented from _dataDictContextBase.

        Get the name of the Watchstar object.

        return:
            str
        """
        return "{} - {}".format(self.settingType(), self.rootPathType())

    def csig(self):
        """
        Get the setting's csig value.

        This

        return:
            str: E.g. "CS", "IG", "Both"
        """
        return str(self._dataDict.get("csig"))

    def rootPath(self):
        """
        Get the setting's root path.

        return:
            str:
        """
        return str(self._dataDict.get("rootPath"))

    def rootPathType(self):
        """
        Get the setting's root path type.

        return:
            str:
        """
        return str(self._dataDict.get("rootPathType"))

    def settingType(self):
        """
        Get the setting's type.

        return:
            str:
        """
        return str(self._dataDict.get("settingType"))

    def __str__(self):
        return "Project config setting: {0} - {1} - {2}".format(self._projectConfig.project().name, self._projectConfig.name, self.name)


class ProjectConfig(_dataDictContextBase):
    """
    Context object for a project config
    """
    def __init__(self, services, project, dataDict):
        super(ProjectConfig, self).__init__(services, dataDict)
        self._project = project

    def project(self):
        """
        The project that the config belongs to.

        return:
            str: contexts.Projects
        """
        return self._project

    @property
    def id(self):
        """
        Reimplemented from _dataDictContextBase.

        Get the ID of the Watchstar object.

        return:
            int: The object's ID
        """
        return int(self._dataDict["projectConfigID"])

    @property
    def name(self):
        """
        Reimplemented from _dataDictContextBase.

        Get the name of the Watchstar object.

        return:
            str
        """
        return str(self._dataDict.get("projectConfigName", ""))

    def isDefault(self):
        """
        Test if this project config is the current default for the project.

        return:
            bool: True if default, else False.
        """
        return self._dataDict.get("isDefault")

    def settings(self, rootPathType=None, settingType=None, csig=None):
        """
        Get all project config settings objects

        args:
            rootPathType (str): Filter returned list based on rootPathType.
            settingType (str): Filter returned list based on settingType.
            csig (str): Filter returned list based on csig.

        return:
            list of contexts.ProjectConfigSetting
        """
        settings = [ProjectConfigSetting(self._services, self, data) for data in self._dataDict.get("projectConfigSettings", ())]
        if rootPathType is not None and settingType is not None:
            settings = [cs for cs in settings if cs.rootPathType() == rootPathType and cs.settingType() == settingType]
        elif rootPathType is not None:
            settings = [cs for cs in settings if cs.rootPathType() == rootPathType]
        elif settingType is not None:
            settings = [cs for cs in settings if cs.settingType() == settingType]

        if csig is not None:
            settings = [cs for cs in settings if cs.csig() == csig]

        return settings

    def getGameDepotRootPath(self):
        """
        Get the Game Depo Root Path as a string

        return:
            string of the game depo root
        """
        configSettings = self.settings(rootPathType="Game Depot")
        if configSettings:
            return (configSettings[0].rootPath() or "").rstrip("/")
        return ""

    def getCSFbxSceneRootPath(self):
        """
        Get the cutscene FBX scene root path.

        return:
            str
        """
        configSettings = self.settings(rootPathType="FBX Scene", settingType="File Path Type Root", csig="CS")
        if configSettings:
            return (configSettings[0].rootPath() or "").rstrip("/")
        return ""

    def getIGFbxSceneRootPath(self):
        """
        Get the ingame FBX scene root path.

        return:
            str
        """
        configSettings = self.settings(rootPathType="FBX Scene", settingType="File Path Type Root", csig="IG")
        if configSettings:
            return (configSettings[0].rootPath() or "").rstrip("/")
        return ""

    def getCSFbxAudioRootPath(self):
        """
        Get the cutscene FBX audio root path.

        return:
            str
        """
        configSettings = self.settings(rootPathType="FBX Audio", settingType="File Path Type Root", csig="CS")
        if configSettings:
            return (configSettings[0].rootPath() or "").rstrip("/")
        return ""

    def getIGFbxAudioRootPath(self):
        """
        Get the ingame FBX audio root path.

        return:
            str
        """
        configSettings = self.settings(rootPathType="FBX Audio", settingType="File Path Type Root", csig="IG")
        if configSettings:
            return (configSettings[0].rootPath() or "").rstrip("/")
        return ""

    def __str__(self):
        return "Project config: {0} - {1}".format(self._project.name, self.name)


class Project(_dataDictContextBase):
    """
    Context object for a project
    """
    def __init__(self, services, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Project, self).__init__(services, dataDict)
        self._projectName = str(dataDict.get("name"))

        self._additionalData = {}

        self._getAdditionalData()

    def _getAdditionalData(self):
        addData = self._dataDict.get("additionalData")
        if addData not in ["", None]:
            self._additionalData = json.loads(addData)

    def _updateAdditionalData(self, data):
        jsonStr = json.dumps(data)
        self._services.watchstar.executeCommand("toolswritepub.UpdateProjectAdditionalData",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                    jsonStr,
                                                ])
        self._dataDict = self._services.animDataContext.getProjectByID(self.id).getDataDict()

    def additionalData(self):
        return self._additionalData

    def setAdditionalData(self, key, value):
        """
        Set an additional data key and its value.

        Notes:
            The value must be pickleable.

        args:
            key (string): the name of the value to set
            value (object): The object to set
        """
        self._additionalData[key] = value
        self._updateAdditionalData(self._additionalData)

    def removeAdditionalData(self, key):
        """
        Remove an additional Data key and its value.

        args:
            key (string): the name of the value to remove
        """
        self._additionalData.pop(key)
        self._updateAdditionalData(self._additionalData)

    @property
    def name(self):
        """
        get the name of the project

        return:
            project name as a string
        """
        if os.getenv("RS_USEPROJCODENAMES", None) is not None:
            return self.codeName()
        return self._projectName

    def getAllConfigs(self):
        """
        Get the project configs for the this project.

        return:
            list of contexts.ProjectConfig
        """
        return [ProjectConfig(self._services, self, data) for data in self._dataDict.get("projectConfigs", ())]

    def getDefaultConfig(self):
        """
        Get the default project config for the this project.

        notes:
            There will always be only one config set as default.

        return:
            contexts.ProjectConfig
        """
        return [cf for cf in self.getAllConfigs() if cf.isDefault() is True][0]

    def getConfigByID(self, configId):
        """
        Get the project config for the this project that matches the specified ID.

        args:
            configId (int): Filter the list based on a project config ID.

        return:
            contexts.ProjectConfig: if config with ID is found | None: No config with matching ID
        """
        configs = [cf for cf in self.getAllConfigs() if cf.id == configId]
        if configs:
            return configs[0]
        return None

    def projectName(self):
        """
        get the name of the project

        return:
            project name as a string
        """
        return self._projectName

    @property
    def projectID(self):
        """
        get the ID of the Project

        return:
            Project ID as an int
        """
        return self.id

    def codeName(self):
        """
        get the codename of the project

        return:
            project code name as a string
        """
        return str(self._dataDict.get("codeName"))

    def isDLC(self):
        """
        Get if the project is DLC or not

        return:
            bool if the project is DLC or not
        """
        return self._dataDict.get("isDLC", False)

    def getAllShoots(self):
        """
        Get all the shoots in the project

        return:
            list of (Shoots) shoots that are under the project
        """
        data = self._services.watchstar.executeCommand("tools.ShootSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID
                                                       ])
        if data is None:
            return []
        return self._handleShootsCreation(data, self)

    @memorize.memoized
    def getShootByID(self, shootID):
        """
        Get a shoot in the project by its ID

        return:
            None or a single shoot (Shoots) shoots that has the same ID
        """
        data = self._services.watchstar.executeCommand("tools.ShootSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           shootID
                                                       ])
        if data is None:
            return None
        returnedShoots = self._handleShootsCreation(data, self)
        if len(returnedShoots) != 1:
            return None
        return returnedShoots[0]

    def getAllCutscenes(self):
        """
        Get all the cutscenes in the project

        return:
            list of (Cutscene) cutscenes that are under the project
        """
        kwargs = [
            "@FeatureCategoryID={0}".format(self._services.animDataSettingsManager.consts.FeatureCategories.CUTSCENE.id),
        ]
        data = self._services.watchstar.executeCommand("tools.FeatureSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ],
                                                       kwargList=kwargs
                                                       )
        if data is None:
            return []
        return self._handleCutscenesCreation(data, self)

    def getCutsceneByID(self, featureId):
        data = self.getServices().watchstar.executeCommand("tools.FeatureSearchByID",
                                                           [
                                                               self._services.getUserName(),
                                                               featureId
                                                           ])
        if data is None:
            return None
        returnedFeatures = self._handleCutscenesCreation(data, self)
        if len(returnedFeatures) != 1:
            return None
        return returnedFeatures[0]

    def getCutsceneByName(self, name):
        """
        The cutscene by its name

        args:
            name (str): The cutscene name to get

        return:
            a single cutscene (Cutscene) of that name, if it exists
        """
        cutscenes = self.getAllCutscenes()
        for cutscene in cutscenes:
            if cutscene.name == name:
                return cutscene
        return None

    @memorize.memoized
    def getShotByID(self, shotID):
        """
        Get a shot in the project by its ID

        return:
            None or a single shot (Shot) shoots that has the same ID
        """
        data = self._services.watchstar.executeCommand("tools.ShotSearchByID",
                                                       [
                                                           self.projectID,
                                                           self._services.getUserName(),
                                                           shotID
                                                       ])
        if data is None:
            return None
        returnedShots = self._handleShotCreation(data, None, self)
        if len(returnedShots) != 1:
            return None
        return returnedShots[0]

    def getShotByName(self, name):
        """
        Get a shot from its name

        args:
            name (str): The shot name to get

        return:
            a single shot (Shot) of that name, if it exists
        """
        data = self._services.watchstar.executeCommand("tools.ShotSearchByShootName",
                                                        [
                                                            self.projectID,
                                                            self._services.getUserName(),
                                                            name
                                                        ])
        if data is None:
            return []
        returnedShots = self._handleShotCreation(data, None, self)
        if len(returnedShots) != 1:
            return None
        return returnedShots[0]

    def getShotByShootName(self, shootName):
        """
        Get a shot from its shoot name

        args:
            name (str): The shoot name to get

        return:
            a list of Shots (shots) which are children of the given shoot
        """
        data = self._services.watchstar.executeCommand("tools.ShotSearchByShootName ",
                                                       [
                                                           self.projectID,
                                                           self._services.getUserName(),
                                                           shootName
                                                       ])
        if data is None:
            return []
        return self._handleShotCreation(data, None, self)

    def getTrialsByRegex(self, regex):
        """
        Get a list of trials by trial name regex

        args:
            regex (str): The regex string to use

        return:
            A list of Trials which match the given regex command
        """
        data = self._services.watchstar.executeCommand("tools.CaptureSearchByRegex",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           regex
                                                       ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self)

    def getCompositesByRegex(self, regex):
        """
        Get a list of composities by trial name regex

        args:
            regex (str): The regex string to use

        return:
            A list of Composites which match the given regex command
        """
        data = self._services.watchstar.executeCommand("tools.MotionSceneSearchByRegex",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           regex
                                                       ])
        if data is None:
            return []
        return self._handleCompositeCreation(data, self)

    @memorize.memoized
    def getCompositeByID(self, compositeID):
        """
        Get a motion composite by ID.

        args:
            compositeID (int): The motion scene's ID.

        return:
            A MotionComposite which matches the given ID.
        """
        data = self._services.watchstar.executeCommand("tools.MotionSceneSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           compositeID
                                                       ])
        if data is None:
            return None
        return self._handleCompositeCreation(data, self)[0]

    def getTrialByBugstarID(self, bugstarID):
        """
        Get a trials based off the bugstar ID

        kargs:
            bugstarID (int): The ID of the bug to search for

        return:
            A Trial
        """
        data = self._services.watchstar.executeCommand("tools.CaptureSearchByBugNumber",
                                                       [
                                                           self._services.getUserName(),
                                                           bugstarID,
                                                       ],
                                                       )
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self)[0]

    def getFeatureByBugstarID(self, bugstarID):
        """
        Get a Feature based off the bugstar ID

        kargs:
            bugstarID (int): The ID of the bug to search for

        return:
            A Feature
        """
        data = self._services.watchstar.executeCommand("tools.FeatureSearchByBugNumber",
                                                       [
                                                           self._services.getUserName(),
                                                           bugstarID,
                                                       ],
                                                       )
        if data is None:
            return []
        return self._handleFeatureCreation(data, self)[0]

    def getMotionCompositeByBugstarID(self, bugstarID):
        """
        Get a Motion Composite based off the bugstar ID

        kargs:
            bugstarID (int): The ID of the bug to search for

        return:
            A Motion Composite object
        """
        data = self._services.watchstar.executeCommand("tools.MotionSceneSearchByBugNumber",
                                                       [
                                                           self._services.getUserName(),
                                                           bugstarID,
                                                       ],
                                                       )
        if data is None:
            return []
        return self._handleCompositeCreation(data, self)[0]

    def getTrialsByUserAssignment(self, user=None, task=None):
        """
        Get a list of trials based off either/or/and the UserID or the Task

        kargs:
            user (settings.User): The User to find tasks for
            task (settings.UserAssignmentTasks): The task type to get

        return:
            A list of Trials
        """
        if user is None and task is None:
            return []
        val = []

        if task is not None:
            val.append("@UserAssignmentTaskID={0}".format(task.settingsID))

        if user is not None:
            val.append("@AssignedToUserID={0}".format(user.settingsID))

        data = self._services.watchstar.executeCommand("tools.CaptureSearchByUserAssignment",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ],
                                                       kwargList=val)
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self)

    def getTrialsByUserCheckout(self, user):
        """
        Get a list of trials based off what user has them checked out

        kargs:
            user (settings.User): The User to find tasks for

        return:
            A list of Trials
        """
        data = self._services.watchstar.executeCommand("tools.CaptureSearchByUserCheckout",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           user.settingsID
                                                       ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self)

    @memorize.memoized
    def getTrialByID(self, trialId):
        """
        Get a trial by its ID

        return:
            None or Trial object that is associated with the trial ID
        """
        trial = self._services.animDataContext.getTrialByID(trialId)
        if trial is not None:
            trial._project = self
        return trial

    def getTrialSelectByID(self, trialSelectId):
        """
        Get a trial select by its ID

        return:
            TrialSelect object that is associated with the ID
        """
        data = self.getServices().watchstar.executeCommand("tools.TrialSelectSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               trialSelectId
                                                           ])
        if data is None:
            return None
        return self._handleTrialSelectCreation(data, None, self)[0]

    @memorize.memoized
    def getSessionByID(self, sessionId):
        """
        Get a session by its ID

        return:
            Session object that is associated with the session ID
        """
        data = self.getServices().watchstar.executeCommand("tools.ShootSessionSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               sessionId
                                                           ])
        if data is None:
            return None
        return self._handleSessionCreation(data, None, self)[0]

    def getMocapDepotRootPath(self):
        """
        Get the Mocap Depot Root Path as a string

        return:
            str: the mocap depot root
        """
        depotRoot = str(self._dataDict.get("mocapDepotRoot", ""))
        if depotRoot[-1] == "/":
            return depotRoot[:-1]
        return depotRoot

    def getShotFromDateRange(self, startDay, startMonth, startYear, endDay, endMonth, endYear):
        """
        Get a list Shots from a given date range

        args:
            startDay (int): The start date range day
            startMonth (int): The start date range month
            startYear (int): The start date range year
            endDay (int): The end date range day
            endMonth (int): The end date range month
            endYear (int): The end date range year

        return:
            list of Shots within the date range
        """
        startDate = "{0}-{1}-{2}".format(startDay, startMonth, startYear)
        endDate = "{0}-{1}-{2}".format(endDay, endMonth, endYear)

        data = self._services.watchstar.executeCommand("tools.ShotSearchByShootDateRange",
                                                       [
                                                           self.projectID,
                                                           self._services.getUserName(),
                                                           startDate,
                                                           endDate
                                                       ])
        if data is None:
            return []
        return self._handleShotCreation(data, None, self)

    def getAllGameAssets(self, assetClass=None):
        """
        Get all the game assets in the project

        return:
            list of AssetGameBase derived contexts.
        """
        kwargs = []
        if assetClass is not None:
            kwargs.append("@AssetClassID={}".format(assetClass.settingsID))
        data = self._services.watchstar.executeCommand("tools.Asset3DSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ],
                                                       kwargList=kwargs)
        if data is None:
            return []
        return self._handleAsset3dCreation(data, self)

    def getAllGameCharacters(self):
        """
        Get all the game characters in the project

        return:
            list of GameCharacter
        """
        return self.getAllGameAssets(assetClass=self._services.animDataSettingsManager.consts.AssetClasses.CHARACTER)

    @memorize.memoized
    def getAllGameAnimals(self):
        """
        Get all the game animals in the project

        return:
            list of GameAnimal
        """
        return self.getAllGameAssets(assetClass=self._services.animDataSettingsManager.consts.AssetClasses.ANIMAL)

    @memorize.memoizeWithExpiry(30)
    def getAsset3dByID(self, assetID):
        """
        Get the asset in the project with the given ID (cached)

        args:
            assetID (int): The ID of the asset to get

        return:
            list of GameCharacter|
        """
        return self.getRawAsset3dByID(assetID)

    def getRawAsset3dByID(self, assetID):
        """
        Get the 3d asset in the project with the given ID

        Notes:
            * Not cached - use this method to refresh/get latest data for a 3d Asset child class.

        args:
            assetID (int): The ID of the asset to get

        return:
            list of GameCharacter|
        """
        data = self._services.watchstar.executeCommand("tools.Asset3DSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           assetID
                                                       ])
        if data is None:
            return None
        assets = self._handleAsset3dCreation(data, self)
        if len(assets) == 1:
            return assets[0]
        return None

    def getRawMocapAssetByID(self, assetID):
        """
        Get the mocap asset in the project with the given ID

        Notes:
            * Not cached - use this method to refresh/get latest data for a MocapAsset.

        args:
            assetID (int): The ID of the mocap asset to get

        return:
            list of GameCharacter|
        """
        data = self._services.watchstar.executeCommand("tools.AssetMocapSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           assetID
                                                       ])
        if data is None:
            return None
        assets = self._handleMocapAssetCreation(data, self, None)
        if len(assets) == 1:
            return assets[0]
        return None

    def addGameCharacter(self, characterSubType, trialType, isActive, fbxPath, notes=None, gsSubstitute=None):
        """
        Add a game character to the project

        args:
            characterSubType (settings.AssetType): the SubCharacter to use
            trialType (str): name of the trial type
            isActive (bool): if the character is active or not
            fbxPath (str): string fbx path

        kwargs:
            notes (str): The notes for the new game character
            gsSubstitute (GameCharacter): A game character to use the GS data for
        """
        kwargs = [
            "@UserName='{0}'".format(self._services.getUserName()),
            "@AnyProjectID={0}".format(self.projectID),
            "@FBXGameAssetPath='{0}'".format(fbxPath),
            "@IsActive={0}".format(int(isActive)),
            "@Asset3DTypeID='{0}'".format(characterSubType.settingsID),
            "@CSIG='{0}'".format(trialType),
        ]

        if gsSubstitute is not None:
            kwargs.append("@MocapSubstituteID={0}".format(gsSubstitute.charId))

        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))

        self._services.watchstar.executeCommand("toolswritepub.AddAsset3D",
                                                kwargList=kwargs
                                                )

    def getAllScriptAssets(self, assetClass=None):
        """
        Get all the script assets in the project

        return:
            list of AssetScriptBase derived contexts.
        """
        kwargs = []
        if assetClass is not None:
            kwargs.append("@AssetClassID={}".format(assetClass.settingsID))
        data = self._services.watchstar.executeCommand("tools.AssetScriptSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ],
                                                       kwargList=kwargs)
        if data is None:
            return []
        return self._handleAssetScriptCreation(data, self)

    def getAllScriptCharacters(self):
        """
        Get all the script characters in the project

        return:
            list of ScriptCharacters
        """
        return self.getAllScriptAssets(assetClass=self._services.animDataSettingsManager.consts.AssetClasses.CHARACTER)

    @memorize.memoized
    def getMocapProps(self):
        """
        Get all the Mocap Props

        return:
            list of contexts.MocapProp
        """
        data = self.getServices().watchstar.executeCommand("tools.StagePropSearchByProject",
                                                           [
                                                               self._services.getUserName(),
                                                               self.projectID
                                                           ])
        if data is None:
            return []
        return self._handleMocapPropCreation(data, self)

    @memorize.memoized
    def getAssetScriptByID(self, assetScriptID):
        """
        Get the script asset in the project with the given ID

        args:
            charID (int): The ID of the char to get

        return:
            list of ScripCharacters
        """
        data = self._services.watchstar.executeCommand("tools.AssetScriptSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           assetScriptID
                                                       ])
        if data is None:
            return None
        assets = self._handleAssetScriptCreation(data, self)
        if len(assets) == 1:
            return assets[0]
        return None

    @memorize.memoizeWithExpiry(60)
    def getAllTalent(self):
        """
        Get all the talent in the project

        return:
            list of all the Talent in the project
        """
        data = self._services.watchstar.executeCommand("tools.TalentSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ])

        if data is None:
            return []
        return self._handleTalentCreation(data, project=self)

    @memorize.memoizeWithExpiry(60)
    def getTalentByID(self, talentProjectID):
        """
        Get the talent with the given ID

        args:
            talentProjectID(int): ID number of the talent to get

        return:
            A single Talent items
        """
        data = self.getServices().watchstar.executeCommand("tools.TalentSearchByTalentProject",
                                                           [
                                                               self.getServices().getUserName(),
                                                               talentProjectID
                                                           ])
        if data is None:
            return None
        return self._handleTalentCreation(data, project=self)[0]

    @memorize.memoized
    def getAllStaff(self):
        """
        Get all the talent in the project

        return:
            list of all the Talent in the project
        """
        return [tal for tal in self.getAllTalent() if tal.isStaff() is True]

    def getTalentByName(self, name):
        """
        Get all the talent in the project

        return:
            list of all the Talent in the project
        """
        data = self._services.watchstar.executeCommand("tools.TalentSearchByName",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           name
                                                       ])
        if data is None:
            return None
        return self._handleTalentCreation(data, project=self)

    @memorize.memoized
    def getTalentVariationById(self, varID):
        """
        Get the Talent variations in the project by its ID

        args:
            varID (int): The Talent variation ID to get

        return:
           single Talent Variations
        """
        data = self._services.watchstar.executeCommand("tools.TalentVariationSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           varID
                                                       ])
        if data is None:
            return None
        return self._handleTalentVariationCreation(data, self)[0]

    def addTalentVariation(self, talent, variationSetting):
        """
        Add a new talent variation to the project.

        Args:
            talent (contexts.Talent): The talent for which to create a new variation.
            variationSetting (settings.Variation): The type of variation to create for the talent.

        Returns:
            contexts.TalentVariation
        """
        data = self._services.watchstar.executeCommand("toolswritepub.AddTalentVariation",
                                                       [
                                                           self._services.getUserName(),
                                                           talent.id,
                                                           variationSetting.settingsID,
                                                       ])
        if data is None:
            raise ValueError("Received no data back from Watchstar - check web ui to confirm talent variation was added.")
        return self._handleTalentVariationCreation(data, self)[0]

    @memorize.memoized
    def getProbspecs(self):
        """
        Get the probspecs in the project

        return:
            list of Probspecs
        """
        data = self._services.watchstar.executeCommand("tools.ProbspecSearchByProject", [
                                                            self._services.getUserName(),
                                                            self.projectID,
                                                       ])
        if data is None:
            return []
        return self._handleProbspecCreation(data, self)

    def getProbspecByName(self, name):
        """
        Search for probspecs matching specified name.

        Args:
            name (str): The name of the probspec.

        Returns:
            str: if match is found | None: if no match can be found
        """
        for probSpec in self.getProbspecs():
            if probSpec.name == name:
                return probSpec
        return None

    @memorize.memoized
    def getProbspecById(self, probID):
        """
        Get the probspec in the project by its ID

        args:
            probID (int): The probspec ID to get

        return:
           None or single Probspec
        """
        data = self._services.watchstar.executeCommand("tools.ProbspecSearchByID", [
                                                            self._services.getUserName(),
                                                            probID,
                                                       ])
        if data is None:
            return None
        return self._handleProbspecCreation(data, self)[0]

    @memorize.memoized
    def getGameProps(self):
        """
        Get all the game props for the project

        return:
           list of GameProp objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.PROP
        data = self._services.watchstar.executeCommand("tools.Asset3DSearchByProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           assetClass.settingsID
                                                       ])
        if data is None:
            return []
        return self._handleAsset3dCreation(data, self)

    def getMocapPropByID(self, mocapPropID):
        """
        Get a Mocap Prop by its ID

        args:
            mocapPropID(int): ID for the MocapProp to find

        return:
            contexts.MocapProp | None
        """
        data = self.getServices().watchstar.executeCommand("tools.StagePropSearchByStagePropProject",
                                                           [
                                                               self.getServices().getUserName(),
                                                               mocapPropID
                                                           ])
        if data is None:
            return None
        props = self._handleMocapPropCreation(data, self)
        if len(props) != 1:
            return None
        return props[0]

    def getMocapPropVersionByID(self, mocapPropVersionID):
        """
        Get a MocapPropVersion by its ID

        args:
            mocapPropID(int): ID for the MocapProp to find

        return:
            contexts.MocapProp | None
        """
        data = self.getServices().watchstar.executeCommand("tools.PhysicalPropVersionSearchByID",
                                                           [
                                                               self.getServices().getUserName(),
                                                               mocapPropVersionID
                                                           ])
        if data is None:
            return None
        dataDicts = data.get("physicalPropVersions")

        mocapPropId = dataDicts[0].get("stagePropProjectID")
        mocapProp = self.getMocapPropByID(mocapPropId)

        # TODO update once we can get this proc: tools.PhysicalPropSearchByID
        mocapPropVariationId = dataDicts[0].get("physicalPropID")
        mocapPropVariations = [mpv for mpv in mocapProp.propVariations() if mpv.id == mocapPropVariationId]
        if len(mocapPropVariations) != 1:
            return None
        return self._handleMocapPropVersionCreation(dataDicts, mocapPropVariations[0], self)[0]

    def getMocapTrialLists(self):
        """
        Get the mocap trial lists in the project

        return:
            list of Mocap Trial Lists
        """
        data = self._services.watchstar.executeCommand("tools.CustomMocapListSearch",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                       ])
        if data is None:
            return []
        return self._handleCustomMocapTrialListCreation(data, self)

    def getOrCreateMocapTrialListByName(self, listName):
        trialList = {item.name: item for item in self.getMocapTrialLists()}.get(listName)
        if trialList is None:
            return self.addMocapTrialLists(listName)
        return trialList

    def addMocapTrialLists(self, listName):
        """
        Add a mocap trial lists to the project

        args:
            listName (str): The name of the list

        return:
            a CustomMocapTrial object of the new list
        """
        data = self._services.watchstar.executeCommand("toolswritepub.AddCustomMocapList",
                                                       [
                                                           self._services.getUserName(),
                                                           self.projectID,
                                                           listName,
                                                       ])
        if data is None:
            raise ValueError("Received no data back from Watchstar - check web ui to confirm mocap trial list was added.")
        return self._handleCustomMocapTrialListCreation(data, self)[0]


class Shoots(_dataDictContextBase):
    """
    Shoots Context object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        if project is None:
            raise ValueError("Shoot initialized without a project!")

        super(Shoots, self).__init__(services, dataDict)
        self._project = project

    @property
    def shootID(self):
        """
        Get the ID of the shoot as a string

        return:
            shoot ID as a string
        """
        return self._dataDict.get("id", "0")

    def isDelivered(self):
        """
        Get the delivered state of the shoot

        return:
            bool of the delivered state of the shoot
        """
        return self._dataDict.get("isDelivered", False)

    def project(self):
        """
        Get the project for the shoot

        return:
            the project (Project) for the shoot
        """
        return self._project

    def getAllSessions(self, onlyNonDeliveredSessions=False):
        """
        Get all the Sessions which are under the Shoot

        return:
            list of (Sessions) which are under the Shoot
        """
        data = self._services.watchstar.executeCommand("tools.ShootSessionSearchByShootID",
                                                       [
                                                            self._services.getUserName(),
                                                            self.shootID,
                                                            int(onlyNonDeliveredSessions)
                                                        ])
        if data is None:
            return []
        return self._handleSessionCreation(data, self, self._project)

    def getAllShots(self):
        """
        Get all the Shot which are under the Shoot

        return:
            list of (Shots) which are under the Shoot
        """
        data = self._services.watchstar.executeCommand("tools.ShotSearchByShootID",
                                                       [
                                                           self._services.getUserName(),
                                                           self.shootID,
                                                       ])
        if data is None:
            return []
        return self._handleShotCreation(data, self, self._project)

    def projectConfig(self):
        """
        Get the project config for the project.

        return:
            contexts.ProjectConfig
        """
        # There should only be one config in the list with the id.
        return self.project().getConfigByID(self._dataDict.get("projectConfigID"))


class Shot(_fileContextBase):
    """
    Shot Context Object
    """
    def __init__(self, shoot, project, services, dataDict):
        """
        Constructor

        args:
            shoot (Shoot): The Shoot the session is under
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        if project is None:
            raise ValueError("Shot initialized without a project!")

        super(Shot, self).__init__(Const.FilePathTables.SHOT, _baseFilePath, services, dataDict)
        self._shoot = shoot
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getShotByID(self.shotID)
        self._validateSearchByIdResult(context, self.name, "shot", self.captureId)
        self._dataDict = context.getDataDict()

    @property
    def shotID(self):
        """
        get the ID of the Shot

        return:
            Shot ID as a string
        """
        return self.id

    @property
    def captureType(self):
        """
        get the shot capture type

        return:
            shot capture type
        """
        featureType = self._services.animDataSettingsManager.getFeatureTypes(self._dataDict.get("featureTypeID"))
        return self._services.animDataSettingsManager.getFeatureCategoryByName(featureType.featureCategory)

    def shotDate(self):
        """
        Get the shot Date as a string

        return:
            string of the Date for the shot
        """
        return self._dataDict.get("shootDate", "0000-00-00")

    def shoot(self):
        """
        Get the Shoot for the Shot

        return:
            the shoot (Shoots) for the Shot
        """
        if self._shoot is None:
            self._shoot = self._getShoot()
        return self._shoot

    def _getShoot(self):
        """
        Get all the shoots in the project

        return:
            list of (Shoots) shoots that are under the project
        """
        data = self._services.watchstar.executeCommand("tools.ShootSearchByShotID",
                                                       [
                                                           self._services.getUserName(),
                                                           self.shotID,
                                                       ])
        if data is None:
            return None
        shoots = self._handleShootsCreation(data, self._project)
        if len(shoots) == 1:
            return shoots[0]
        return None

    def project(self):
        """
        Get the project for the Shot

        return:
            the project (Project) for the shot
        """
        return self._project

    def mission(self):
        """
        Get the mission of the Shot

        return:
            string
        """
        # The key will only be in the json if the feature was associated to a Mission.
        return self._dataDict.get("mission", None)

    def featureName(self):
        """
        Get the name of the associated feature.

        return:
            str: The feature name.
        """
        return str(self._dataDict.get("featureName", ""))

    def featureType(self):
        """
        Get the feature type, e.g. 'In-Game - In-Game', 'Cutscene - Cutscene'

        Returns:
            settings.FeatureType: The settings feature type object of the trial's feature
        """
        featureId = self._dataDict.get("featureTypeID", "-1")
        return self._services.animDataSettingsManager.getFeatureTypes(featureId)

    def cutscene(self):
        """
        Get the cutscene name for the Shot

        warning:
            This doesn't check to confirm that the attached feature is of type Cutscene.

        return:
            str
        """
        return self.featureName()

    def latticeStatus(self):
        """
        Get the Lattice Status for the Shot

        return:
            string
        """
        return self._services.animDataSettingsManager.getLatticeStatus(self._dataDict.get("latticeStatusID", "0"))

    def latticeComments(self):
        """
        The get comments for the lattice on the Shot

        return:
            string
        """
        return self._dataDict["latticeComments"]

    def getAssets(self, assetClass=None):
        return self._handleProductionAssetCreation(self._dataDict, self.project(), assetClassFilter=assetClass)

    def getCharacters(self):
        """
        Get the characters from the shot

        return:
            List of Character objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.CHARACTER
        return self.getAssets(assetClass=assetClass)

    def getProps(self):
        """
        Get the props from the shot

        return:
            List of Prop objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.PROP
        return self.getAssets(assetClass=assetClass)

    def getAnimals(self):
        """
        Get the animals from the shot

        return:
            List of Animal objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ANIMAL
        return self.getAssets(assetClass=assetClass)

    def getEnvironments(self):
        """
        Get the environments from the shot

        return:
            List of Environment objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ENVIRONMENT
        return self.getAssets(assetClass=assetClass)

    def getVehicles(self):
        """
        Get the vehicles from the shot

        return:
            List of Vehicle objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.VEHICLE
        return self.getAssets(assetClass=assetClass)

    def getAllTrials(self, selectsOnly=False):
        """
        Get all the trials for the shot.

        return:
            list of trials for the shot.
        """
        data = self._services.watchstar.executeCommand("tools.TrialSearchByShot",
                                                       [
                                                            self._services.getUserName(),
                                                            self.shotID,
                                                            selectsOnly
                                                        ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self.project())


class FeatureBase(_fileContextBase):
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the Feature is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(FeatureBase, self).__init__(Const.FilePathTables.FEATURE, _baseFilePath, services, dataDict)
        self._project = project

    @property
    def featureTypeId(self):
        return self._dataDict["featureTypeID"]

    @property
    def name(self):
        """
        Reimplemented from _contextBase.
        """
        return str(self._dataDict["featureName"])

    def project(self):
        """
        Get the project for the Capture cutscene

        return:
            the project (Project) for the cutscene
        """
        return self._project


class GenericFeature(FeatureBase):
    """
    Generic Context Object
    """


class AssetFeature(FeatureBase):
    """
    3D Asset Context Object
    """


class InGame(FeatureBase):
    """
    InGame Context Object
    """


class Cutscene(FeatureBase):
    """
    Cutscene Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            cutesceneName (str): The name of the cutscene
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Cutscene, self).__init__(project, services, dataDict)

    @property
    def cutsceneId(self):
        """
        get the ID of the cutscene

        return:
            cutscene ID as a string
        """
        return self.id

    @memorize.memoizeWithExpiry(30)
    def getAllTrials(self, selectsOnly=False):
        """
        Get all the trials for the cutscene

        return:
            list of trials (MotionTrialBase & NonMotionTrialBase) under the cutscene
        """
        data = self._services.watchstar.executeCommand("tools.CaptureSearchByFeature", [
                                                             self._services.getUserName(),
                                                             self.cutsceneId,
                                                             selectsOnly
                                                         ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self.project())

    def getAllComposites(self):
        """
        Get all the composites for the cutscene

        return:
            list of composites (MotionComposite) under the cutscene
        """
        data = self._services.watchstar.executeCommand("tools.MotionSceneSearchByFeature ",
                                                       [
                                                           self._services.getUserName(),
                                                           self.cutsceneId
                                                       ])
        if data is None:
            return []
        return self._handleCompositeCreation(data, self.project())


class Session(_dataDictContextBase):
    """
    Shoot Session Context Object
    """
    def __init__(self, shoot, project, services, dataDict):
        """
        Constructor

        args:
            sessionName (str): The name of the session
            shoot (Shoot): The Shoot the Session is under
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        if project is None:
            raise ValueError("Session initialized without a project!")

        super(Session, self).__init__(services, dataDict)
        self._sessionName = dataDict.get("name")
        self._shoot = shoot
        self._project = project
        self._stage = None

    @property
    def name(self):
        """
        get the name of the Session

        return:
            Session name as a string
        """
        return "{0} [{1} - {2}]".format(self._sessionName, self.location().locationAbbreviation(), self.stage.name)

    def rawName(self):
        """
        get the raw name of the Session

        return:
            Raw Session name as a string
        """
        return self._sessionName

    @property
    def sessionID(self):
        """
        get the ID of the Session

        return:
            int: Session ID
        """
        return self.id

    @property
    def stage(self):
        """
        Get the capture Stage for the session

        return:
            stage object where the session is shot
        """
        if self._stage is None:
            self._stage = self._services.animDataContext.getStageFromID(self._dataDict.get("locationStageID", "-1"))
        return self._stage

    def sessionName(self):
        """
        get the name of the Session without the location

        return:
            Session name as a string
        """
        return self._sessionName

    def getTalentForSession(self):
        data = self._services.watchstar.executeCommand("tools.TalentSearchByShootSession",
                                                       [
                                                           self._services.getUserName(),
                                                           self.sessionID
                                                       ])

        if data is None:
            return None
        return self._handleTalentCreation(data, project=self._project)

    def _getShoot(self):
        """
        Internal Method

        get the shoot for the session

        return:
            a single shoot (Shoot) object
        """
        data = self._services.watchstar.executeCommand("tools.ShootSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           self._dataDict.get("shootID", "-1")
                                                       ])

        if data is None:
            return None
        shoots = self._handleShootsCreation(data, self._project)
        if len(shoots) == 0:
            return None
        return shoots[0]

    def getAllShots(self):
        """
        Get all the shots for the current Session

        return:
            list of shot (Shots) for the session
        """
        data = self._services.watchstar.executeCommand("tools.ShotSearchByShootSessionID",
                                                       [
                                                           self._services.getUserName(),
                                                           self.sessionID
                                                       ])
        if data is None:
            return []
        return self._handleShotCreation(data, self, self._project)

    @memorize.memoized
    def shoot(self):
        """
        Get the Shoot for the Session

        return:
            the shoot (Shoots) for the Session
        """
        if self._shoot is None:
            shootId = self._dataDict.get("shootID", "-1")
            self._shoot = self._project.getShootByID(shootId)
        return self._shoot

    @memorize.memoized
    def project(self):
        """
        Get the project for the Session

        return:
            the project (Project) for the Session
        """
        return self._project

    def isDelivered(self):
        """
        Get the delivered state of the Session

        return:
            bool of the delivered state of the Session
        """
        return self._dataDict.get("isDelivered", False)

    def notes(self):
        """
        Get the notes for the Session

        return:
            string of the notes for the Session
        """
        return self._dataDict.get("sessionNotes", "")

    def productionTrackingNotes(self):
        """
        Get the production tracking notes for the Session

        return:
            string of the production tracking notes for the Session
        """
        return self._dataDict.get("productionTrackingNotes", "")

    def sessionDate(self):
        """
        Get the session Date as a string

        return:
            string of the Date for the Session
        """
        return self._dataDict.get("sessionDate", "0000-00-00")

    def getSessionName(self):
        """
        Get the folder name for the Session

        return:
            string of the folder name
        """
        year, month, day = self._dataDict.get("sessionDate", "0000-00-00").split("-", 3)
        _, sessionNum = self._dataDict.get("name", "Session -1").split(" ", 2)
        return "{0}_{1}{2}_Session{3}".format(year, month, day, sessionNum)

    def getSessionFolderName(self):
        legName = self._dataDict.get("legacyName") or None
        if legName is None:
            return self.getSessionName()
        year, month, day = self.sessionDate().split("-", 3)
        _, _, dayNum = legName.split(" ", 3)
        return "{0}_{1}{2}_Day{3}".format(year, month, day, dayNum)

    def getAllTrials(self, selectsOnly=False, captureTypeId=None):
        """
        Get all the trials under the Session

        return:
            list of trials (MotionTrialBase & NonMotionTrialBase) under the Session
        """
        if captureTypeId is None:
            data = self._services.watchstar.executeCommand("tools.CaptureSearchByShootSession",
                                                           [
                                                                self._services.getUserName(),
                                                                self.sessionID,
                                                                selectsOnly
                                                           ])
        else:
            data = self._services.watchstar.executeCommand("tools.CaptureSearchByShootSession",
                                                           [
                                                                self._services.getUserName(),
                                                                self.sessionID,
                                                                selectsOnly,
                                                                captureTypeId
                                                           ])
        if data is None:
            return []
        return self._handleTrialCreation(data, self, self._shoot, self._project)

    def location(self):
        """
        Get the location the session was shot at

        returns:
            a single Location object
        """
        return self.stage.location()

    def updateSession(self, productionTrackingNotes=None):
        """
        Method to update the trial

        kwargs:
            productionTrackingNotes (str): the notes to set to the trial
        """
        updateDict = {}
        kwargs = []

        if productionTrackingNotes is not None:
            cleanTxt = self._formatText(productionTrackingNotes)
            kwargs.append("@ProductionTrackingNotes='{0}'".format(cleanTxt))
            updateDict["productionTrackingNotes"] = productionTrackingNotes

        if len(kwargs) == 0:
            return

        self._services.watchstar.executeCommand("toolswritepub.UpdateShootSessionProductionTrackingNotes",
                                                [
                                                    self._services.getUserName(),
                                                    self.sessionID,
                                                ],
                                                kwargList=kwargs)
        self._dataDict.update(updateDict)

    def addBodyScaleTrial(self, probspec, talent, variation=None, markerSet=None, isStageGrab=False):
        """
        Adds a new body scale and returns the new trial. The trial will automatically be checked-out for the user.

        A body scale can only be created if there exists a shoot session for the current day and the body scale's talent
        has been added to the shoot session (not as adr).

        Args:
            probspec (contexts.Probspec):
            talent (contexts.Talent): Should be a Talent that has been added to this session as not ADR.

        Keyword Args:
            variation (settings.Variation): The variation setting to use to get or create the talent variation.
                Default: None
            markerSet (settings.MarkerSet): The marker set to use for the new scale. If not specified, the MarkersetID
                will become whichever Markerset is tagged as the global default in Watchstar.
                Default: None
            isStageGrab (bool): Set as stage grab.
                Default: False (opposite of the Watchstar proc's default value if not specified; better for use case).

        Returns:
            contexts.BodyScaleTrial: The newly created trial.
        """
        kwargs = [
            "@ShootSessionID={}".format(self.id),
            "@ProbspecID={}".format(probspec.id),
            "@TalentProjectID={}".format(talent.id),
        ]
        if variation is not None:
            kwargs.append("@VariationID={}".format(variation.id))
        if markerSet:
            kwargs.append("@MarkersetID={}".format(markerSet.id))
        if isStageGrab:
            kwargs.append("@IsStageGrab={}".format(int(isStageGrab)))
        data = self._services.watchstar.executeCommand("toolswritepub.AddBodyScale",
                                                       [
                                                           self._services.getUserName(),
                                                       ],
                                                       kwargList=kwargs)
        if data is None:
            raise ValueError("Received no trial data back from Watchstar - check web ui to confirm scale was added.")
        return self._handleTrialCreation(data, self, self.shoot(), self.project())[0]

    def addBodyRomTrial(self, bodyScaleTrial, isStageGrab=False):
        """
        Adds a new body rom and returns the new trial. The trial will automatically be checked-out for the user.

        Args:
            bodyScaleTrial (contexts.BodyScaleTrial): The scale to use for the new ROM.

        Keyword Args:
            isStageGrab (bool): Set as stage grab.
                Default: False (opposite of the Watchstar proc's default value if not specified; better for use case).

        Returns:
            contexts.BodyRomTrial: The newly added trial.
        """
        kwargs = [
            "@ShootSessionID={}".format(self.id),
            "@BodyScaleID={}".format(bodyScaleTrial.id),
        ]
        if isStageGrab:
            kwargs.append("@IsStageGrab={}".format(int(isStageGrab)))
        data = self._services.watchstar.executeCommand("toolswritepub.AddBodyRom",
                                                       [
                                                           self._services.getUserName(),
                                                       ],
                                                       kwargList=kwargs)
        if data is None:
            raise ValueError("Received no trial data back from Watchstar - check web ui to confirm ROM was added.")
        return self._handleTrialCreation(data, self, self.shoot(), self.project())[0]


class PipelineStage(_dataDictContextBase):
    """
    Pipeline Stage Context Object
    """
    def __init__(self, trial, services, dataDict):
        """
        Constructor

        args:
            trial (_trialBase): Trial the pipeline stage belongs to
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(PipelineStage, self).__init__(services, dataDict)
        self._trial = trial

    @property
    def pipelineStageId(self):
        """
        get the ID of the Pipeline Stage

        return:
            Pipeline Stage id as an int
        """
        return self.id

    @property
    def name(self):
        """
        Get the step name

        return:
            string for the step name
        """
        return self.pipelineStep().name

    def pipelineStep(self):
        """
        return the Pipeline step settings object

        Return:
            A single PipelineStep settings object
        """
        return self._services.animDataSettingsManager.getPipelineSteps(self._dataDict.get("pipelineStepID", "0"))

    def trial(self):
        """
        get the trial the pipeline stage is for

        return:
            _trialBase object for the trial
        """
        return self._trial

    def stepNumber(self):
        """
        Get the step number as an int

        return:
            int for the step number
        """
        return int(self._dataDict["stepNum"])

    def logInfo(self):
        """
        Get the log info for the pipeline stage

        return:
            string log info
        """
        return self._dataDict["logInfo"]

    def logStatus(self):
        """
        Get the log status for the pipeline stage

        return:
            string log status
        """
        return self._dataDict["logStatus"]

    def isRework(self):
        """
        Get the if the pipeline stage is a reword

        return:
            bool value if the stage is a rework
        """
        return self._dataDict["isRework"]

    def reworkNotes(self):
        """
        Get the pipeline stage reword notes

        return:
            String value for the rework notes
        """
        return self._dataDict["reworkNotes"]

    def setCaptureStageState(self, newState, reworkNotes=None):
        """
        Set the capture stage's pipeline status state and optionally the rework notes.

        Args:
            newState (Const.PipelineStepStatus option):
            reworkNotes (str): The content for the rework notes.
        """
        kwargs = []

        if newState == Const.PipelineStepStatus.NOT_COMPLETE:
            raise ValueError("Unable to set trial to Not Complete")

        # if its complete, it can only be set to re-work, so check for that
        if self.logStatus == Const.PipelineStepStatus.COMPLETE:
            if newState != Const.PipelineStepStatus.REWORK:
                raise ValueError("Can only set a Complete to a ReWork status")

        # if its a rework, it can only be set to complete
        if self.logStatus == Const.PipelineStepStatus.REWORK:
            if newState not in [Const.PipelineStepStatus.COMPLETE, Const.PipelineStepStatus.REWORK]:
                raise ValueError("Can only a Rework to Complete status")

        if reworkNotes is not None and newState == Const.PipelineStepStatus.REWORK:
            cleanTxt = self._formatText(reworkNotes)
            kwargs.append("@ReworkNotes='{0}'".format(cleanTxt))

        self._services.watchstar.executeCommand("toolswritepub.UpdateCapturePipelineStep",
                                                [
                                                    self._services.getUserName(),
                                                    self.pipelineStageId,
                                                    newState
                                                ],
                                                kwargList=kwargs or None)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Pipeline Step ({0}) object for '{1}'".format(self.name, self.trial().name)


class _trialBase(_fileContextBase):
    """
    Internal Base Class

    Trial Base Context Object

    Trials and Types
        * A trial will always be linked to a single feature - the feature type is used to decide what
        trial class to instantiate.
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(_trialBase, self).__init__(Const.FilePathTables.CAPTURE, AssociatedFile, services, dataDict)
        self._project = project
        self._shoot = None
        self._session = None
        self._currentPipelineState = None
        self._additionalData = {}

        self._getAdditionalData()

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object with id of '{}' for {}".format(self.__class__.__name__, self.fileId, self.trial())

    def _getAdditionalData(self):
        addData = self._dataDict.get("additionalData")
        if addData not in ["", None]:
            self._additionalData = json.loads(addData)

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._services.animDataContext.getRawTrialByID(self.captureId)
        self._dataDict = context.getDataDict()
        self._getAdditionalData()

    @property
    def captureId(self):
        """
        get the ID of the Trial

        return:
            Trial id as an int
        """
        return self.id

    def getFileName(self):
        """
        Get the filename on disk of the trial (accounts for the legacy name if
        exists)

        return:
            the filename of the trial as a string
        """
        return self._dataDict.get("legacyName") or self.name

    def legacyName(self):
        """
        Get the legacy Name of the trial

        return:
            str: the legacy name of the of trial
        """
        return self._dataDict.get("legacyName") or ""

    @property
    def captureType(self):
        """
        get the trial capture type

        return:
            trial capture type
        """
        captureTypeId = self._dataDict.get("captureTypeID", "0")
        return self._services.animDataSettingsManager.getCaptureTypes(captureTypeId)

    def isCheckedOut(self):
        """
        Get if the take is currently checked out

        return:
            bool if the take is checked out
        """
        return bool(self._dataDict.get("isCaptureCheckedOut", 0))

    def pipelineStage(self):
        """
        get the trial pipline stage name

        return:
            name of the pipeline stage the trial is at
        """
        return self._dataDict.get("currentPipelineStep", "")

    def pipelineStageRework(self):
        """
        Get if the current pipeline step is a rework or not

        return:
            Bool if the current pipleine step is a rework
        """
        return self._dataDict.get("currentPipelineStepIsRework", False)

    def getPipelineStages(self):
        """
        Get a list of pipeline stages for the trial

        return:
            list of PipelineStage objects that make up the history of the trial
        """
        data = self._services.watchstar.executeCommand("tools.CapturePipelineSearchByCapture",
                                                       [
                                                           self._services.getUserName(),
                                                           self.captureId
                                                       ])
        if data is None:
            return []

        return self._handlePipelineStagesCreation(data, self)

    def _getEstimatedPipelineStages(self):
        """
        Virtual method
        """
        return []

    def estimatePipelineStages(self):
        """
        Get a mapping of pipeline stage: pipeline step status pairs for the trial.
        """
        pipelineStages = self._getEstimatedPipelineStages()
        pipelineDict = collections.OrderedDict()
        for stage in pipelineStages:
            pipelineDict[stage] = Const.PipelineStepStatus.NOT_COMPLETE

        currentStage = self.pipelineStage()
        matchedPart = 0

        for idx, stage in enumerate(pipelineStages):
            if currentStage == stage:
                matchedPart = idx + 1
                break

        for idx in xrange(matchedPart):
            pipelineDict[pipelineStages[idx]] = Const.PipelineStepStatus.COMPLETE

        if self.pipelineStageRework() is True:
            pipelineDict[pipelineStages[idx]] = Const.PipelineStepStatus.REWORK

        return pipelineDict

    def _calculateCurrentPipelineState(self):
        """
        Internal Method

        Calculate the current pipeline step based off whats complete and if its
        a re-work or not
        """
        for stage in self.getPipelineStages():
            if stage.logStatus() == 'Completed' and stage.isRework() is True:
                return stage.name
        return self._dataDict.get("currentPipelineStep", "")

    def setCaptureStageState(self, stageName, newState, reworkNotes=None):
        for stage in self.getPipelineStages():
            # we only care about the stage we're looking for
            if stage.name == stageName:
                return stage.setCaptureStageState(newState, reworkNotes=reworkNotes)

    def _getSession(self):
        """
        Get the Session from the Motion Trials bstarProject attribute

        return:
            the session (Session) for the trial
        """
        return self.project().getSessionByID(self._dataDict["shootSessionID"])

    def session(self):
        """
        Get the session for the Motion Trial

        return:
            the session (Session) for the trial
        """
        if self._session is None:
            self._session = self._getSession()
        return self._session

    def shoot(self):
        """
        Get the shoot for the Motion Trial

        return:
            the shoot (Shoot) for the trial
        """
        if self._shoot is None:
            self._shoot = self.session().shoot()
        return self._shoot

    def isSelected(self):
        """
        Get the select status of the Trial

        return:
            bool
        """
        return self._dataDict["isSelect"]

    def _getProject(self):
        """
        Get the project from the Motion Trials attribute

        return:
            the project (Project) for the trial
        """
        return self._services.animDataContext.getProjectByID(self._dataDict["anyProjectID"])

    @memorize.memoized
    def project(self):
        """
        Get the project for the Motion Trial

        return:
            the project (Project) for the trial
        """
        if self._project is None:
            self._project = self._getProject()
        return self._project

    def addNewTrialNote(self, comment, role, talentProject=None):
        """
        Add a new trial note

        args:
            comment (str): The comment to add
            role (settings.ProductionRole): Role of the user

        kwargs:
            talentProject (settings.ProductionRole): Talent to use for the note
        """
        talentProject = talentProject or self._project.getTalentByName(self._services.getUserName())
        if talentProject is None:
            raise ValueError("No Valid Talent to add comment for")
        result = self._services.watchstar.executeCommand("toolswritepub.AddCaptureStaff",
                                                         [
                                                             self._services.getUserName(),
                                                             self.captureId,
                                                             role.settingsID,
                                                             talentProject.id,
                                                             comment,
                                                         ])
        if result is not None:
            dataDicts = result.get("captures", [])
            if dataDicts:  # There should only be one in this case.
                self._dataDict = dataDicts[0]
                self._getAdditionalData()

    def userCheckoutInfo(self):
        """
        Get the user checkout information for the trial.

        return:
            (settings.User, str): The user settings object and the checkout time as listed in database.
        """
        data = self._services.watchstar.executeCommand("tools.UserCheckoutSearchByCapture",
                                                       [
                                                           self._services.getUserName(),
                                                           self.captureId,
                                                       ])
        checkoutUsers = data.get("userCheckouts", None)
        if checkoutUsers in [None, []]:
            return None, None
        userCheckout = checkoutUsers[0]
        user = self._services.animDataSettingsManager.getUsers(userCheckout.get("userID", "0"))
        return (user, str(userCheckout.get("checkoutTime")))

    def getTrialNotes(self, refresh=False):
        """
        Get all trial notes for this trial

        return:
            list of trial notes (MotionTrialNotes) that are for the current Motion Trial
        """
        if refresh:
            self._refresh()
        return self._handleNoteCreation(self._dataDict, self, self.project())

    def onHold(self):
        """
        Get if the trial is on hold or not

        return:
            bool if the trial is on hold
        """
        return self._dataDict.get("isOnHold", False)

    def setOnHold(self, onHold):
        """
        Set if the trial is on hold or not

        args:
            onHold (bool): If the trial is on hold or not
        """
        self.updateTrial(isOnHold=onHold)

    def createdAt(self):
        """
        Get who the Date and time was created at

        return:
            the string of date and time it was created
        """
        return self._dataDict.get("creationTime", "")

    def createdBy(self):
        """
        Get who the take was created by

        return:
            the string of capturing user
        """
        return self._dataDict.get("createdBy", "")

    def additionalData(self):
        return self._additionalData

    def setAdditionalData(self, key, value):
        """
        Set an additional Data key and its value

        args:
            key (string): the name of the value to set
            value (object): The object to set

        Note: The value must be pickleable
        """
        self._additionalData[key] = value
        self.updateTrial()

    def removeAdditionalData(self, key):
        """
        Remove an additional Data key and its value

        args:
            key (string): the name of the value to remove

        """
        self._additionalData.pop(key)
        self.updateTrial()

    @memorize.memoizeWithExpiry(30)
    def getAllFiles(self, pathType=None):
        """
        Get all the files associated with the trial as a list

        return:
            List of AssociatedFile objects
        """
        return super(_trialBase, self).getAllFiles(pathType=pathType)

    def updateTrial(self):
        """
        Virtual method.
        """
        raise NotImplementedError()

    def projectConfig(self):
        """
        Get the project config for the project.

        return:
            contexts.ProjectConfig
        """
        # There should only be one config in the list with the id.
        return self.project().getConfigByID(self._dataDict.get("projectConfigID"))

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Trial ({0}) object for '{1}'".format(self.captureType.name, self.name)


class MotionTrialBase(_trialBase):
    """
    Base Motion Trial Context Object
    """
    def __init__(self, session, shoot, project, services, dataDict):
        """
        Constructor

        args:
            session (Session): The capture session the trial is under
            shoot (Shoot): The Shoot the session is under
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MotionTrialBase, self).__init__(project, services, dataDict)
        self._shoot = shoot
        self._session = session
        self._shot = None

    def _getShot(self):
        data = self._services.watchstar.executeCommand("tools.ShotSearchByID",
                                                       [
                                                           self._services.getUserName(),
                                                           self._dataDict["shotID"]
                                                       ])
        if data is None:
            return None
        shots = self._handleShotCreation(data, None, self.project())
        if len(shots) > 0:
            return shots[0]
        return None

    def shot(self):
        """
        Get the shot for the Motion Trial

        return:
            the shot (Shot) for the trial
        """
        if self._shot is None:
            self._shot = self._getShot()
        return self._shot

    @property
    def sessionID(self):
        """
        get the ID of the Motion Trial

        return:
            Motion Trial ID as a string
        """
        return self._dataDict["shootSessionID"]

    def mission(self):
        """
        Get the mission of the Trial

        return:
            string
        """
        return self.shot().mission()

    def featureType(self):
        """
        Get the feature type, e.g. 'In-Game - In-Game', 'Cutscene - Cutscene'

        Returns:
            settings.FeatureType: The settings feature type object of the trial's feature
        """
        featureId = self._dataDict.get("featureTypeID", "-1")
        return self._services.animDataSettingsManager.getFeatureTypes(featureId)

    def featureName(self):
        """
        Get the name of the associated feature.

        return:
            str: The feature name.
        """
        return str(self._dataDict.get("featureName", ""))

    def cutscene(self):
        """
        Get the cutscene of the Trial

        warning:
            This doesn't check to confirm that the attached feature is of type Cutscene.

        return:
            str
        """
        return self.featureName()

    def inGameType(self):
        """
        Get the type of ingame motion of the Trial

        return:
            string
        """
        return self.shot().captureType

    def latticeStatus(self):
        """
        Get the Lattice Status for the trial

        return:
            string
        """
        return self._services.animDataSettingsManager.getLatticeStatus(self._dataDict.get("latticeStatusID", "0"))

    def isLayer(self):
        """
        Is this a layered trial?

        Returns:
            bool: True if layered, False if not.
        """
        return self._dataDict.get("isLayer", False)

    def layerComment(self):
        """
        Get the layer comment.

        Returns:
            str
        """
        return str(self._dataDict.get("layerComment", ""))

    def layerComposite(self):
        """
        Get the layer's "parent" MotionComposite for the trial.

        Returns:
            MotionComposite if isLayer is True | None: if isLayer if False
        """
        motionSceneId = self._dataDict.get("layerMotionSceneID", None)
        if motionSceneId is None:
            return None
        return self.project().getCompositeByID(motionSceneId)

    def assignmentTask(self):
        """
        Get the assigned Task for the trial

        return:
            A UserAssignmentTasks settings object, or None if not task has been assigned to it
        """
        users = self._dataDict.get("userAssignment", [])
        taskID = None
        if users:
            taskID = users[0].get("taskID")
        if taskID is None:
            return None
        return self._services.animDataSettingsManager.getUserAssignmentTasks(taskID)

    def assignmentUser(self):
        """
        Get the assigned User for the trial

        return:
            A User settings object, or None if no user has been assigned to it
        """
        users = self._dataDict.get("userAssignment", [])
        userID = None
        if users:
            userID = users[0].get("userID")
        if userID is None:
            return None
        return self._services.animDataSettingsManager.getUsers(userID)

    def latticeComments(self):
        """
        The get comments for the lattice on the trial

        return:
            string
        """
        return self._dataDict["latticeComment"]

    def _addProductionAsset(self,
                            performerRom=None,
                            mocapPropVersion=None,
                            gameAsset=None,
                            scriptAsset=None,
                            faceAudioPack=None,
                            namespace=None,
                            hasAudio=None,
                            hasMocap=None,
                            hasVideo=None,
                            comment=None,
                            ):
        """
        Internal method.

        Add a production asset.

        Notes:
            * The Watchstar API call/proc has some mutually exclusive args. Some validation is provided here.

        Keyword Args:
            performerRom (contexts.BodyRomTrial): The rom for the asset.
            mocapPropVersion (contexts.MocapPropVersion): The mocap prop version (PhysicalPropVersion) for the asset.
            gameAsset (contexts.GameCharacter): Optional game asset (3d asset) for the asset.
            scriptAsset (contexts.ScriptCharacter): Optional script asset (script asset) for the asset.
            faceAudioPack (settings.FaceAudioPack): Optional facepack for the asset.
            namespace (str): Optional game asset namespace, i.e. motionbuilder namespace.
            hasAudio (bool): Set to True if the asset has audio.
            hasMocap (bool): Set to True if the asset has mocap.
            hasVideo (bool): Set to True if the asset has video.
            comment (str): Optional comment about the asset.
        """
        # Check for mutually exclusive args.
        if performerRom and mocapPropVersion:
            raise ValueError("Both a performer rom and a mocap prop version have been provided - set only one of these args.")

        # Run the query.
        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@TrialID={}".format(self.id),
        ]
        kwargs = []
        if performerRom is not None:
            kwargs.append("@BodyRomID={}".format(performerRom.id))
        if mocapPropVersion is not None:
            kwargs.append("@PhysicalPropVersionID={}".format(mocapPropVersion.id))
        if gameAsset is not None:
            kwargs.append("@Project3DAssetID={}".format(gameAsset.id))
        if scriptAsset is not None:
            kwargs.append("@ProjectScriptAssetID={}".format(scriptAsset.id))
        if faceAudioPack is not None:
            kwargs.append("@AudioFaceDeviceID={}".format(faceAudioPack.settingsID))
        if namespace is not None:
            kwargs.append("@GameAssetNamespace='{}'".format(namespace))
        if hasAudio is not None:
            kwargs.append("@HasAudio={}".format(int(bool(hasAudio))))
        if hasMocap is not None:
            kwargs.append("@HasMocap={}".format(int(bool(hasMocap))))
        if hasVideo is not None:
            kwargs.append("@HasVideo={}".format(int(bool(hasVideo))))
        if comment is not None:
            kwargs.append("@Comment='{}'".format(comment))

        result = self._services.watchstar.executeCommand("toolswritepub.AddTrialProductionAsset",
                                                         kwargList=defaultArgs + kwargs)
        if result is not None:
            dataDicts = result.get("captures", [])
            if dataDicts:  # There should only be one in this case.
                self._dataDict = dataDicts[0]
                self._getAdditionalData()

    def addCharacter(self,
                     performerRom,
                     gameChar=None,
                     scriptChar=None,
                     faceAudioPack=None,
                     namespace=None,
                     hasAudio=None,
                     hasMocap=None,
                     hasVideo=None,
                     comment=None,
                     ):
        """
        Add a Character (Production Asset)

        Does not include the mutually exclusive ADR related params.

        Args:
            performerRom (contexts.BodyRomTrial): The rom for the asset.

        Kwargs:
            gameChar (contexts.GameCharacter): Optional game character (3d asset) for the asset.
            scriptChar (contexts.ScriptCharacter): Optional script character (script asset) for the asset.
            faceAudioPack (settings.FaceAudioPack): Optional facepack for the asset.
            namespace (str): Optional game asset namespace, i.e. motionbuilder namespace.
            hasAudio (bool): Set to True if the asset has audio.
            hasMocap (bool): Set to True if the asset has mocap.
            hasVideo (bool): Set to True if the asset has video.
            comment (str): Optional comment about the asset
        """
        self._addProductionAsset(
            performerRom=performerRom,
            gameAsset=gameChar,
            scriptAsset=scriptChar,
            faceAudioPack=faceAudioPack,
            namespace=namespace,
            hasAudio=hasAudio,
            hasMocap=hasMocap,
            hasVideo=hasVideo,
            comment=comment,
        )

    def addProp(self,
                mocapPropVersion,
                gameAsset=None,
                scriptAsset=None,
                faceAudioPack=None,
                namespace=None,
                hasAudio=None,
                hasMocap=None,
                hasVideo=None,
                comment=None,
                ):
        """
        Add a Prop (Production Asset)

        Args:
            mocapPropVersion (contexts.MocapPropVersion): The mocap prop version (PhysicalPropVersion) for the asset.

        Kwargs:
            gameAsset (contexts.GameCharacter): Optional game asset (3d asset) for the asset.
            scriptAsset (contexts.ScriptCharacter): Optional script asset (script asset) for the asset.
            faceAudioPack (settings.FaceAudioPack): Optional facepack for the asset.
            namespace (str): Optional game asset namespace, i.e. motionbuilder namespace.
            hasAudio (bool): Set to True if the asset has audio.
            hasMocap (bool): Set to True if the asset has mocap.
            hasVideo (bool): Set to True if the asset has video.
            comment (str): Optional comment about the asset
        """
        self._addProductionAsset(
            mocapPropVersion=mocapPropVersion,
            gameAsset=gameAsset,
            scriptAsset=scriptAsset,
            faceAudioPack=faceAudioPack,
            namespace=namespace,
            hasAudio=hasAudio,
            hasMocap=hasMocap,
            hasVideo=hasVideo,
            comment=comment,
        )

    def getAssets(self, assetClass=None):
        return self._handleProductionAssetCreation(self._dataDict, self.project(), assetClassFilter=assetClass)

    def getCharacters(self, refresh=False):
        """
        Get the characters from the trial

        return:
            List of Character objects
        """
        if refresh is True:
            self._refresh()
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.CHARACTER
        return self.getAssets(assetClass=assetClass)

    def getCharacterByID(self, charID, refresh=False):
        """
        Get a characters from the trial based off its ID

        args:
            charID (int): The character ID

        return:
            a Character objects
        """
        charDict = {char.id: char for char in self.getCharacters(refresh=refresh)}
        return charDict.get(charID)

    def getProps(self):
        """
        Get the props from the trial

        return:
            List of prop objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.PROP
        return self.getAssets(assetClass=assetClass)

    def getAnimals(self):
        """
        Get the animals from the trial

        return:
            List of Animal objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ANIMAL
        return self.getAssets(assetClass=assetClass)

    def getEnvironments(self):
        """
        Get the environments from the trial

        return:
            List of Environment objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ENVIRONMENT
        return self.getAssets(assetClass=assetClass)

    def getVehicles(self):
        """
        Get the vehicles from the trial

        return:
            List of Vehicle objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.VEHICLE
        return self.getAssets(assetClass=assetClass)

    @memorize.memoized
    def getSelects(self):
        """
        Get the actions for the trial (in V4 of Watchstar, these objects were called "selects").

        return:
            List of MotionSelect objects
        """
        return self._handleSelectCreation(self._dataDict, self)

    def getTrialSelects(self):
        """
        Get the selects for the trial

        return:
            List of MotionTrialSelect objects
        """
        # "trialSelects" is the key name in toolswritepub.AddTrialSelect and tools.TrialSelectSearchByID procs' JSON.
        # "selects" is the key name in the trial capture's JSON
        data = {"trialSelects": [selectData for selectData in self._dataDict.get("selects", [])]}
        return self._handleTrialSelectCreation(data, self, self.project())

    def addTrialSelect(self, fbxDepotPath, notes=None):
        """
        Adds a new trial select to the trial, creates a MotionScene in Watchstar and adds the FBX depot path to it.

        Notes:
            * "FBX Audio" and "EST FBX Audio" files can only be saved on a MotionComposite (W* MotionScene).
                * The only way to get to these audio files is by a TrialSelect which links the trail to the MotionScene.

        Args:
            fbxDepotPath (str): Depot path to the FBX scene file to be added to a new MotionComposite(W* MotionScene).

        Keyword Args:
            notes (str): Any extra notes to be added to the trial select.
                Default: None

        Returns:
            contexts.MotionTrialSelect: The newly created trial select.
        """
        kwargs = [
            "@TrialID={}".format(self.id),
            "@FeatureID={}".format(self.getDataDict().get("featureID")),
            "@FBXScenePath='{}'".format(fbxDepotPath),
        ]
        if notes is not None:
            cleanText = self._formatText(notes)
            kwargs.append("@SelectNotes='{}'".format(cleanText))

        data = self._services.watchstar.executeCommand("toolswritepub.AddTrialSelect",
                                                       [
                                                           self._services.getUserName(),
                                                       ],
                                                       kwargList=kwargs)
        if data is None:
            raise ValueError("No TrialSelect data received from Watchstar - check web ui to confirm it was added.")
        return self._handleTrialSelectCreation(data, self, self.project())[0]

    def descriptionOfAction(self):
        """
        Get the description of action

        return:
            the string of description of action
        """
        return self._dataDict.get("actionDescription", "")

    def setDescriptionOfAction(self, description):
        """
        Set the description of action

        args:
            description (str): The description of the action
        """
        self.updateTrial(actionDescription=description)

    def postProductionNote(self):
        """
        Get the post production note

        return:
            the string of post production note
        """
        return self._dataDict.get("postNotes", "")

    def setPostProductionNote(self, note):
        """
        Set the post Production Notes of the trial

        args:
            note (str): The Post Production Notes for the trial
        """
        self.updateTrial(postNotes=note)

    def priority(self):
        """
        Get the priority of the capture

        return:
            A priority settings object
        """
        settingsID = self._dataDict.get("priorityID")
        if settingsID is None:
            return None
        return self._services.animDataSettingsManager.getPriority(settingsID)

    def setPriority(self, priority):
        """
        Set the priority of the trial

        args:
            priority (Priority Settings Object): the priorty to set to the trial
        """
        self.updateTrial(priority=priority)

    def _getEstimatedPipelineStages(self):
        """
        Reimplemented from _trialBase
        """
        if self.isSelected() is False:
            return []
        return [
            Const.PipelineStages.CAPTURED,
            Const.PipelineStages.PROCESSED,
            Const.PipelineStages.TRACKED,
            Const.PipelineStages.REFINED,
            Const.PipelineStages.CHAR_MAPPED,
            Const.PipelineStages.IMPORTED_INTO_MOBU,
            Const.PipelineStages.TRIAL_FINISHED,
        ]

    def updateTrial(self,
                    acqFrameRangeStart=None,
                    acqFrameRangeEnd=None,
                    actionDescription=None,
                    complexity=None,
                    isOnHold=None,
                    latticeComments=None,
                    latticeStatus=None,
                    markerset=None,
                    postNotes=None,
                    priority=None,
                    productionNotes=None,
                    timecodeInTime=None,
                    timecodeInFrame=None,
                    timecodeOutTime=None,
                    timecodeOutFrame=None,
                    usedInEdit=None,
                    ztcFrameRangeStart=None,
                    ztcFrameRangeEnd=None,
                    ):
        """
        Method to update the trial

        kwargs:
            acqFrameRangeStart (int): Acquired frame in
            acqFrameRangeEnd (int): Acquired frame out
            actionDescription  (str): the description of the action
            complexity (ComplexitySettings Object): The complexity to set it out
            isOnHold (bool): if the trial is on hold
            latticeComments (str): the lattice comment to set
            latticeStatus (LatticeSettings object): The lattice setting to set
            markerset (MarkerSetSettings Object): The markerset to set to the trial
            postNotes (str): The notes to set to the trial
            priority (PrioritySettings Object): the priority to set to the trial
            productionNotes (str): the notes to set to the trial
            timecodeInTime (str): HH:MM:SS for the time code in
            timecodeInFrame (str): two-digit characters between '00' and '29'
            timecodeOutTime (str): HH:MM:SS for the time code out
            timecodeOutFrame (str): two-digit characters between '00' and '29'
            usedInEdit (bool): if the trial is used in an edit
            ztcFrameRangeStart (int): Zero Cut Frame Range Start
            ztcFrameRangeEnd (int): Zero Cut Frame Range End
        """
        kwargs = []

        if acqFrameRangeStart is not None:
            kwargs.append("@AcqFrameRangeStart={0}".format(acqFrameRangeStart))

        if acqFrameRangeEnd is not None:
            kwargs.append("@AcqFrameRangeEnd={0}".format(acqFrameRangeEnd))

        if actionDescription is not None:
            cleanTxt = self._formatText(actionDescription)
            kwargs.append("@ActionDescription='{0}'".format(cleanTxt))

        if complexity is not None:
            kwargs.append("@ComplexityID={0}".format(complexity.settingsID))

        if isOnHold is not None:
            kwargs.append("@IsOnHold='{0}'".format(str(isOnHold).lower()))

        if latticeComments is not None:
            cleanTxt = self._formatText(latticeComments)
            kwargs.append("@LatticeComments='{0}'".format(cleanTxt))

        if latticeStatus is not None:
            kwargs.append("@LatticeStatusID={0}".format(latticeStatus.settingsID))

        if markerset is not None:
            kwargs.append("@MarkersetID={0}".format(markerset.settingsID))

        if postNotes is not None:
            cleanTxt = self._formatText(postNotes)
            kwargs.append("@PostNotes='{0}'".format(cleanTxt))

        if priority is not None:
            kwargs.append("@PriorityID={0}".format(priority.settingsID))

        if productionNotes is not None:
            cleanTxt = self._formatText(productionNotes)
            kwargs.append("@ProductionNotes='{0}'".format(cleanTxt))

        if timecodeInTime is not None:
            kwargs.append("@TimecodeInTime='{0}'".format(timecodeInTime))

        if timecodeInFrame is not None:
            kwargs.append("@TimecodeInFrame={0}".format(timecodeInFrame))

        if timecodeOutTime is not None:
            kwargs.append("@TimecodeOutTime='{0}'".format(timecodeOutTime))

        if timecodeOutFrame is not None:
            kwargs.append("@TimecodeOutFrame={0}".format(timecodeOutFrame))

        if usedInEdit is not None:
            kwargs.append("@UsedInEdit='{0}'".format(str(usedInEdit).lower()))

        if ztcFrameRangeStart is not None:
            kwargs.append("@ZtcFrameRangeStart={0}".format(ztcFrameRangeStart))

        if ztcFrameRangeEnd is not None:
            kwargs.append("@ZtcFrameRangeEnd={0}".format(ztcFrameRangeEnd))

        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        self._services.watchstar.executeCommand("toolswritepub.UpdateTrial",
                                                [
                                                    self._services.getUserName(),
                                                    self.captureId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict = self._services.animDataContext.getTrialByID(self.captureId)._dataDict


class AssetTrial(MotionTrialBase):
    """
    3D Asset Trial Context Object
    """
    @property
    def captureType(self):
        """
        get the trial capture type

        return:
            trial capture type as a settings.FeatureCategory
        """
        return self._services.animDataSettingsManager.consts.FeatureCategories.ASSET
    
    
class GenericTrial(MotionTrialBase):
    """
    Generic Trial Context Object
    """
    @property
    def captureType(self):
        """
        get the trial capture type

        return:
            trial capture type as a settings.FeatureCategory
        """
        return self._services.animDataSettingsManager.consts.FeatureCategories.GENERIC


class InGameTrial(MotionTrialBase):
    """
    InGame Trial Context Object
    """
    @property
    def captureType(self):
        """
        get the trial capture type

        return:
            trial capture type as a settings.FeatureCategory
        """
        return self._services.animDataSettingsManager.consts.FeatureCategories.INGAME


class CutSceneTrial(MotionTrialBase):
    """
    CutScene Trial Context Object
    """
    @property
    def captureType(self):
        """
        get the trial capture type

        return:
            trial capture type as a settings.FeatureCategory
        """
        return self._services.animDataSettingsManager.consts.FeatureCategories.CUTSCENE


class NonMotionTrialBase(_trialBase):
    """
    Non Motion Trial Context Object
    """


class BodyRomTrial(NonMotionTrialBase):
    """
    Body Rom Trial Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(BodyRomTrial, self).__init__(project, services, dataDict or {})

    def bodyScale(self):
        """
        Get the bodyScale Trial of the Trial

        return:
            BodyScaleTrial
        """
        return self.project().getTrialByID(int(self._dataDict["bodyScaleID"]))

    def defaultBodyScale(self):
        """
        Get the default body scale of the same markerset as the ROM

        return:
            BodyScaleTrial
        """
        bodyScale = self.bodyScale()
        talentVariation = bodyScale.talentVariation()
        return talentVariation.defaultScales().get(bodyScale.markerSet())

    def primaryPatchColor(self):
        """
        get the primary path color of the actor

        return:
            The colour as a hex colour value
        """
        return self._dataDict.get("primaryPatchColor")

    def secondaryPatchColor(self):
        """
        get the secondary path color of the actor

        return:
            The colour as a hex colour value
        """
        return self._dataDict.get("secondaryPatchColor")

    def softcapColor(self):
        """
        gets the colour of the softcap, if the actor is in a softcap

        Returns:
            string hex colour
        """
        return self._dataDict.get("softcapColor", "")

    def helmetCamRevision(self):
        """
        Get the helmetCamRevision.

        Get the helmet camera revision if the actor is in a helmet

        return:
            HelmetCamRevision or None
        """
        helmetCamRevisionId = self._dataDict.get("helmetCamRevisionID", None)
        if helmetCamRevisionId is None:
            return None
        for helmetCam in self._services.animDataContext.getHelmetCams():
            for helmetCamRevision in helmetCam.revisions():
                if helmetCamRevision.id == helmetCamRevisionId:
                    return helmetCamRevision
        return None

    def helmetCamCalibration(self):
        """
        Get the helmet cam's calibration, if the actor is in a helmet

        return:
            HelmetCamCalibration or None
        """
        helmetCamCalibrationId = self._dataDict.get("helmetCamCalibrationID", None)
        if helmetCamCalibrationId is None:
            return None
        return self._services.animDataContext.getHelmetCamCalibrationByID(helmetCamCalibrationId)

    def _getEstimatedPipelineStages(self):
        """
        Reimplemented from _trialBase
        """
        return [
            Const.PipelineStages.CAPTURED,
            Const.PipelineStages.ROM_VERIFIED,
        ]

    def updateTrial(self,
                    acqFrameRangeStart=None,
                    acqFrameRangeEnd=None,
                    isOnHold=None,
                    bodyScale=None,
                    isSelect=None,
                    ):
        """
        Method to update the trial

        kwargs:
            acqFrameRangeStart (int): Acquired frame in
            acqFrameRangeEnd (int): Acquired frame out
            isOnHold (bool): if the trial is on hold
            bodyScale (BodyScaleTrial): Body Scale trial to set as the scale
            isSelect (bool): if the trial is a select
        """
        kwargs = []

        if acqFrameRangeStart is not None:
            kwargs.append("@AcqFrameRangeStart={0}".format(acqFrameRangeStart))

        if acqFrameRangeEnd is not None:
            kwargs.append("@AcqFrameRangeEnd={0}".format(acqFrameRangeEnd))

        if isOnHold is not None:
            kwargs.append("@IsOnHold='{0}'".format(str(isOnHold).lower()))

        if bodyScale is not None:
            kwargs.append("@BodyScaleID='{0}'".format(bodyScale.captureId))

        if isSelect is not None:
            kwargs.append("@IsSelect='{0}'".format(str(isSelect).lower()))

        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        self._services.watchstar.executeCommand("toolswritepub.UpdateBodyRom",
                                                [
                                                    self._services.getUserName(),
                                                    self.captureId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict = self._services.animDataContext.getTrialByID(self.captureId)._dataDict


class BodyScaleTrial(NonMotionTrialBase):
    """
    Body Rom Scale Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(BodyScaleTrial, self).__init__(project, services, dataDict or {})

    def talentVariation(self):
        """
        Get the talent Variation of the Trial

        return:
            Talent Variation (TalentVariation)
        """
        return self.project().getTalentVariationById(int(self._dataDict["talentProjectVariationID"]))

    def probspec(self):
        """
        Get the Probspec of the Trial

        return:
            problem setup (Probspec)
        """
        propSpecID = int(self._dataDict.get("probspecID", "-1"))
        if propSpecID == -1:
            return None
        return self.project().getProbspecById(propSpecID)

    def scaleStatus(self):
        """
        Get the scale Status

        return:
            the string of scale status
        """
        return self._dataDict.get("scaleStatus", "")

    def markerSet(self):
        """
        Get the marker set of the body scale trial.

        return:
            settings.MarkerSet: if markersetID is in trial json | None: if markersetID is not in trial json
        """
        markerSetId = int(self._dataDict.get("markersetID", "-1"))
        if markerSetId == -1:
            return None
        return self.getServices().animDataSettingsManager.getMarkerSets(settingID=markerSetId)

    def _getEstimatedPipelineStages(self):
        """
        Reimplemented from _trialBase
        """
        return [
            Const.PipelineStages.CAPTURED,
            Const.PipelineStages.ADJUSTED,
        ]

    def updateTrial(self,
                    isOnHold=None,
                    productionNotes=None,
                    propspec=None,
                    scaleStatus=None,
                    isSelect=None,
                    ):
        """
        Method to update the trial

        kwargs:
            isOnHold (bool): if the trial is on hold
            productionNotes (str): the notes to set to the trial
            propspec (Propspec): Propspec to set as the propspec for the trial
            scaleStatus (str): The scale status
            isSelect (bool): if the trial is a select
        """
        kwargs = []

        if isOnHold is not None:
            kwargs.append("@IsOnHold='{0}'".format(str(isOnHold).lower()))

        if productionNotes is not None:
            cleanTxt = self._formatText(productionNotes)
            kwargs.append("@ProductionNotes='{0}'".format(cleanTxt))

        if propspec is not None:
            kwargs.append("@ProbspecID={0}".format(propspec.id))

        if scaleStatus is not None:
            cleanTxt = self._formatText(scaleStatus)
            kwargs.append("@ScaleStatus='{0}'".format(cleanTxt))

        if isSelect is not None:
            kwargs.append("@IsSelect='{0}'".format(str(isSelect).lower()))

        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        self._services.watchstar.executeCommand("toolswritepub.UpdateBodyScale",
                                                [
                                                    self._services.getUserName(),
                                                    self.captureId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict = self._services.animDataContext.getTrialByID(self.captureId)._dataDict


class FaceRomTrial(NonMotionTrialBase):
    """
    Face rom Trial Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(FaceRomTrial, self).__init__(project, services, dataDict or {})

    def scriptChar(self):
        """
        Get the script asset of the Trial

        return:
            ScriptCharacter
        """
        projectScriptAssetID = int(self._dataDict["projectScriptAssetID"])
        return self.project().getAssetScriptByID(projectScriptAssetID)

    def talent(self):
        """
        Get the talent of the Trial

        return:
            Talent
        """
        projectTalentId = int(self._dataDict.get("talentProjectID", "0"))
        return self.project().getTalentByID(projectTalentId)

    def helmetCamRevision(self):
        """
        Get the helmetCamRevision.

        return:
            HelmetCamRevision
        """
        helmetCamRevisionId = self._dataDict.get("helmetCamRevisionID", None)
        if helmetCamRevisionId is None:
            return None
        for helmetCam in self._services.animDataContext.getHelmetCams():
            for helmetCamRevision in helmetCam.revisions():
                if helmetCamRevision.id == helmetCamRevisionId:
                    return helmetCamRevision
        return None

    def helmetCamCalibration(self):
        """
        Get the helmet cam's calibration.

        This attr is only in the json if the helmet cam revision is dual cam.

        Returns:
            HelmetCamCalibration or None
        """
        helmetCamCalibrationId = self._dataDict.get("helmetCamCalibrationID", None)
        if helmetCamCalibrationId is None:
            return None
        return self._services.animDataContext.getHelmetCamCalibrationByID(helmetCamCalibrationId)

    def updateTrial(self,
                    acqFrameRangeStart=None,
                    acqFrameRangeEnd=None,
                    isOnHold=None,
                    productionNotes=None,
                    ):
        """
        Method to update the trial

        kwargs:
            acqFrameRangeStart (int): Aquired frame in
            acqFrameRangeEnd (int): Aquired frame out
            isOnHold (bool): if the trial is on hold
            productionNotes (str): the notes to set to the trial
        """
        kwargs = []

        if acqFrameRangeStart is not None:
            kwargs.append("@AcqFrameRangeStart={0}".format(acqFrameRangeStart))

        if acqFrameRangeEnd is not None:
            kwargs.append("@AcqFrameRangeEnd={0}".format(acqFrameRangeEnd))

        if isOnHold is not None:
            kwargs.append("@IsOnHold='{0}'".format(str(isOnHold).lower()))

        if productionNotes is not None:
            cleanTxt = self._formatText(productionNotes)
            kwargs.append("@ProductionNotes='{0}'".format(cleanTxt))

        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        self._services.watchstar.executeCommand("toolswritepub.UpdateFaceRom",
                                                [
                                                    self._services.getUserName(),
                                                    self.captureId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict = self._services.animDataContext.getTrialByID(self.captureId)._dataDict


class FaceScanTrial(NonMotionTrialBase):
    """
    Face scan Trial Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(FaceScanTrial, self).__init__(project, services, dataDict or {})

    def scriptChar(self):
        """
        Get the scriptChar of the Trial

        return:
            ScriptCharacter
        """
        projectScriptAssetID = int(self._dataDict["projectScriptAssetID"])
        return self.project().getAssetScriptByID(projectScriptAssetID)

    def talent(self):
        """
        Get the talent of the Trial

        return:
            Talent
        """
        projectTalentId = int(self._dataDict.get("talentProjectID", "0"))
        return self.project().getTalentByID(projectTalentId)

    def updateTrial(self,
                    acqFrameRangeStart=None,
                    acqFrameRangeEnd=None,
                    isOnHold=None,
                    productionNotes=None,
                    ):
        """
        Method to update the trial

        kwargs:
            acqFrameRangeStart (int): Aquired frame in
            acqFrameRangeEnd (int): Aquired frame out
            isOnHold (bool): if the trial is on hold
            productionNotes (str): the notes to set to the trial
        """
        kwargs = []

        if acqFrameRangeStart is not None:
            kwargs.append("@AcqFrameRangeStart={0}".format(acqFrameRangeStart))

        if acqFrameRangeEnd is not None:
            kwargs.append("@AcqFrameRangeEnd={0}".format(acqFrameRangeEnd))

        if isOnHold is not None:
            kwargs.append("@IsOnHold='{0}'".format(str(isOnHold).lower()))

        if productionNotes is not None:
            cleanTxt = self._formatText(productionNotes)
            kwargs.append("@ProductionNotes='{0}'".format(cleanTxt))

        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        self._services.watchstar.executeCommand("toolswritepub.UpdateHeadScan",
                                                [
                                                    self._services.getUserName(),
                                                    self.captureId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict = self._services.animDataContext.getTrialByID(self.captureId)._dataDict


class MotionComposite(_fileContextBase):
    """
    Motion Composite Context Object
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            compositeName (str): The name of the motion composite
            project (Project): Project that the motion is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        if project is None:
            raise ValueError("MotionComposite initialized without a project!")

        super(MotionComposite, self).__init__(Const.FilePathTables.MOTION_SCENE, AssociatedCompositeFile, services, dataDict)
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getCompositeByID(self.compositeID)
        self._dataDict = context.getDataDict()

    def fbxScenePath(self):
        """
        Get the FBX scene depot path that was used to create the motion scene.

        Returns:
            str
        """
        return str(self._dataDict["fbxScenePath"])

    def isSelected(self):
        return self._dataDict["isSelect"]

    @property
    def compositeID(self):
        """
        get the ID of the Motion Composite

        return:
            Motion Composite ID as a string
        """
        return self.id

    def _getProject(self):
        """
        Get the project from the Motion Composite bstarProjectID attribute

        return:
            the project (Project) for the Composite
        """
        return self._services.animDataContext.getProjectByID(self._dataDict["anyProjectID"])

    def project(self):
        """
        Get the project for the Motion Composite

        return:
            the project (Project) for the Composite
        """
        if self._project is None:
            self._project = self._getProject()
        return self._project

    def mission(self):
        """
        Get the mission of the Trial

        return:
            string
        """
        return self._dataDict["mission"]

    def cutscene(self):
        """
        Get the name of the cutscene of the Trial

        return:
            string
        """
        featureId = self._dataDict["featureID"]
        return self._project.getCutsceneByID(featureId).name

    def getAssets(self, assetClass=None):
        # Replace the key name to be consistent for the handler.
        data = {"productionAssets": self._dataDict.get("motionSceneAssets", [])}
        return self._handleProductionAssetCreation(data, self.project(), assetClassFilter=assetClass)

    def getCharacters(self):
        """
        Get the characters from the motion composite

        return:
            List of Character objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.CHARACTER
        return self.getAssets(assetClass=assetClass)

    def getProps(self):
        """
        Get the props from the motion composite

        return:
            List of Prop objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.PROP
        return self.getAssets(assetClass=assetClass)

    def getAnimals(self):
        """
        Get the animals from the motion composite

        return:
            List of Animal objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ANIMAL
        return self.getAssets(assetClass=assetClass)

    def getEnvironments(self):
        """
        Get the environments from the motion composite

        return:
            List of Environment objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.ENVIRONMENT
        return self.getAssets(assetClass=assetClass)

    def getVehicles(self):
        """
        Get the vehicles from the motion composite

        return:
            List of Vehicle objects
        """
        assetClass = self._services.animDataSettingsManager.consts.AssetClasses.VEHICLE
        return self.getAssets(assetClass=assetClass)

    def getSelects(self):
        """
        Get the selects for the Composite

        return:
            List of MotionCompositeSelect objects
        """
        baseData = self._dataDict.get("selectedActions", {}).get("selectedAction", [])
        if isinstance(baseData, dict):
            baseData = [baseData]
        return [MotionCompositeSelect(self, self._services, data) for data in baseData]

    @memorize.memoizeWithExpiry(30)
    def getAllFiles(self, pathType=None):
        """
        Get all the files associated with the trial as a list

        return:
            List of AssociatedCompositeFile objects
        """
        dataDicts = self._getFilePaths(pathType=pathType, refresh=False)
        return self._handleGenericFileCreation(dataDicts, AssociatedCompositeFile, self, self.project(), self._fileTable)


class MotionTrialNote(_dataDictContextBase):
    """
    Motion Trial Note Context Object
    """
    def __init__(self, trial, project, services, dataDict):
        """
        Constructor

        args:
            trial (_trialBase): The Trial the note belongs to
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MotionTrialNote, self).__init__(services, dataDict)
        self._trial = trial
        self._project = project

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}'".format(self.__class__.__name__, self.name)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} (id: {}) at {}>".format(self.__str__(), self.id, hex(id(self)))

    @property
    def name(self):
        """
         Get the name of the file, without the file extention

         return:
             string name of the file
        """
        return "{} - {}".format(self.role().name, self.creator().name)

    @property
    def noteId(self):
        """
        get the ID of the note

        return:
            Note ID as a string
        """
        return self._dataDict["id"]

    def comment(self):
        """
        Get the comment on the note

        return:
            Note comment as a string
        """
        return self._dataDict["notes"]

    def role(self):
        """
        Get the user role on the note

        return:
            Note role of the user as a string
        """
        return self._services.animDataSettingsManager.getProductionRoles(self._dataDict["productionRoleID"])

    def creator(self):
        """
        Get the name of the staff of the note leaver

        return:
            Talent object for the note creator
        """
        return self.project().getTalentByID(self._dataDict.get("staffProjectID"))

    def creationTime(self):
        """
        Get the note created time

        return:
            Note created time as a string in local time
        """
        return self._convertUtcDbStringToLocalTime(self._dataDict["utcCreationTime"])

    def lastModifiedBy(self):
        return str(self._dataDict.get("lastModifiedBy", self.creator()))

    def lastModifiedTime(self):
        """
        Get the note last modified time

        return:
            str: last modified time in local time | None: if the note has not been modified
        """
        if self._dataDict["utcLastModifiedTime"] == self._dataDict["utcCreationTime"]:
            return None
        return self._convertUtcDbStringToLocalTime(self._dataDict["utcLastModifiedTime"])

    def creationTimeUtc(self):
        """
        Get the note created time

        return:
            str: Note created time (in UTC)
        """
        utcDt = parser.parse(self._dataDict["utcCreationTime"])
        return utcDt.strftime(self.DATE_FORMAT)

    def lastModifiedTimeUtc(self):
        """
        Get the note last modified time

        return:
            str: last modified time in UTC time | None: if the note has not been modified
        """
        if self._dataDict["utcLastModifiedTime"] == self._dataDict["utcCreationTime"]:
            return None
        utcDt = parser.parse(self._dataDict["utcLastModifiedTime"])
        return utcDt.strftime(self.DATE_FORMAT)

    def trial(self):
        """
        Get the Trial from the Note

        return:
            the Trial the note if for
        """
        return self._trial

    def project(self):
        """
        Get the project for the note

        return:
            the project (Project) for the note
        """
        if self._project is None:
            self._project = self.trial().project()
        return self._project

    def deleteNote(self):
        """
        Delete the current note.

        Returns:
            dict: The captures dict as returned from the tool proc's JSON.
        """
        result = self._services.watchstar.executeCommand("toolswritepub.DeleteCaptureStaff",
                                                         [
                                                             self._services.getUserName(),
                                                             self.noteId,
                                                         ])
        if result is not None:
            self._dataDict["comment"] = "<DELETED>"
            return result

    def editComment(self, newComment):
        """
        Edit the current comment on this note.

        args:
            newComment (str): The new comment to leave
        """
        self._services.watchstar.executeCommand("toolswritepub.UpdateCaptureStaff",
                                                [
                                                    self._services.getUserName(),
                                                    self.noteId,
                                                    self._dataDict["productionRoleID"],
                                                    self._dataDict["staffProjectID"],
                                                    newComment
                                                ])

        self._dataDict["notes"] = newComment

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, MotionTrialNote) and self.noteId == other.noteId

    def __hash__(self):
        return self.noteId.__hash__()


class _baseFilePath(_dataDictContextBase):
    """
    Internal Base Class

    File Path Context Object
    """
    def __init__(self, table, parent, project, services, dataDict):
        """
        Constructor

        args:
            table (Const.FilePathTables): The file path table to use for operations
            parent (AnimData._contextBase): the Parent of the file
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(_baseFilePath, self).__init__(services, dataDict)
        # Required for add/update/delete operations.
        self._fileTable = table
        self._parent = parent

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{} ({})'".format(self.__class__.__name__, self.name, self.fileType().name)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} (id: {}) at {}>".format(self.__str__(), self.id, hex(id(self)))

    @property
    def fileId(self):
        """
        get the ID of the file

        return:
            file ID as a string
        """
        return self.id

    @property
    def name(self):
        """
         Get the name of the file, without the file extention

         return:
             string name of the file
        """
        return os.path.splitext(os.path.basename(self.filePath()))[0]

    def parent(self):
        """
         Get the parent of the file

         return:
             AnimData._contextBase of the parent
        """
        return self._parent

    def fileType(self):
        """
        Get the Type of file as a settings.FilePathType object

        return:
            settings.FilePathType object
        """
        return self._services.animDataSettingsManager.getFilePathTypes(self._dataDict.get("pathTypeID", "0"))

    def filePath(self):
        """
        get the file path for the file, i.e. the "display name".

        return:
            String path to the file
        """
        return str(self._dataDict.get("path", "")) or "<NO {0} FILE SET>".format(self.fileType().name.upper())

    def rawFilePath(self):
        """
        get the raw file path for the file, i.e. "the p4 depot path".

        return:
            String path to the file
        """
        return str(self._dataDict.get("path", ""))

    def fileOnMocapServer(self):
        """
        get if the file is located on the mocap p4 server or not

        return:
            bool if the file is on the mocap p4 server
        """
        return self.filePath().startswith("//depot/projects/")

    def thumbnailPath(self):
        """
        get the file path for the thumbnail

        return:
            String path to the thumbnail
        """
        return self._dataDict.get("thumbnailPath", "")

    def updatePath(self, newFilePath=None, newThumbnailPath=None):
        """
        Edit the file object properties

        kargs:
            newFilePath (str): The new path to the file
            newThumbnailPath (str): The new path to the thumbnail
        """
        updateDict = {}
        val = []
        if newFilePath is not None:
            if newFilePath == "":
                raise ValueError("Unable to set a file path to an empty string")
            updateDict["filePath"] = newFilePath
            val.append("@FilePath='{0}'".format(newFilePath))

        if newThumbnailPath is not None:
            # TODO: Readd this back in
            pass
            # val.append("@ThumbnailPath='{0}'".format(newThumbnailPath))
            # updateDict["thumbnailPath"] = newThumbnailPath

        if len(val) == 0:
            return

        self._services.watchstar.executeCommand("toolswritepub.UpdateFilePathAssociation",
                                                [
                                                    self._services.getUserName(),
                                                    self._fileTable,
                                                    self.parent().id,
                                                    self.fileId,
                                                ],
                                                kwargList=val
                                                )

        self._dataDict.update(updateDict)

    def deletePath(self):
        """
        This will remove the file from the parent.

        THIS IS UNDOABLE
        """
        self._services.watchstar.executeCommand("toolswritepub.DeleteFilePathAssociation",
                                                [
                                                    self._services.getUserName(),
                                                    self._fileTable,
                                                    self.parent().id,
                                                    self.fileId,
                                                ])

    def __hash__(self):
        return self.fileId.__hash__()


class AssociatedFile(_baseFilePath):
    """
    Associated File Context Object
    """
    def __init__(self, table, trial, project, services, dataDict):
        """
        Constructor

        args:
            table (Const.FilePathTables): The file path table to use for operations
            trial (_trialBase): The Trial the file belongs to
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(AssociatedFile, self).__init__(file, trial, project, services, dataDict)
        self._trial = trial
        self._project = project

    def project(self):
        """
        get the project of the Associated file

        return:
            project (Project) of the Associated file
        """
        return self._project

    def trial(self):
        """
        get the trial the file is from

        return:
            MotionTrialBase & NonMotionTrialBase which the file is for
        """
        return self._trial

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, AssociatedFile) and self.fileId == other.fileId


class AssociatedCompositeFile(_baseFilePath):
    """
    Associated Composite File Context Object
    """
    def __init__(self, table, composite, project, services, dataDict):
        """
        Constructor

        args:
            table (Const.FilePathTables): The file path table to use for operations
            composite (Composite): The composite the file belongs to
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(AssociatedCompositeFile, self).__init__(Const.FilePathTables.MOTION_SCENE, composite, project, services, dataDict)
        self._composite = composite
        self._project = project

    def project(self):
        """
        get the project of the Associated file

        return:
            project (Project) of the Associated file
        """
        return self._project

    def composite(self):
        """
        get the composite the file is from

        return:
            Mcomposite which the file is for
        """
        return self._composite

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, AssociatedFile) and self.fileId == other.fileId


class Asset3dFile(_baseFilePath):
    """
    Associated asset File Context Object
    """
    def __init__(self, table, asset, project, services, dataDict):
        """
        Constructor

        args:
            table (Const.FilePathTables): The file path table to use for operations
            asset (AssetGameBase): The asset the file belongs to
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Asset3dFile, self).__init__(Const.FilePathTables.PROJECT_3D_ASSET, asset, project, services, dataDict)
        self._asset = asset
        self._project = project

    def project(self):
        """
        get the project of the Associated file

        return:
            project (Project) of the Associated file
        """
        return self._project

    def asset(self):
        """
        get the asset the file is from

        return:
            AssetGameBase which the file is for
        """
        return self._asset

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for {}".format(self.__class__.__name__, self.asset())

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, Asset3dFile) and self.fileId == other.fileId


class MocapAssetFile(_baseFilePath):
    """
    Associated asset File Context Object
    """
    def __init__(self, table, asset, project, services, dataDict):
        """
        Constructor

        args:
            table (Const.FilePathTables): The file path table to use for operations
            asset (AssetGameBase): The asset the file belongs to
            project (Project): Project that the shoot is under
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MocapAssetFile, self).__init__(Const.FilePathTables.MOCAP_ASSET, asset, project, services, dataDict)
        self._asset = asset
        self._project = project

    def project(self):
        """
        get the project of the Associated file

        return:
            project (Project) of the Associated file
        """
        return self._project

    def asset(self):
        """
        get the asset the file is from

        return:
            AssetGameBase which the file is for
        """
        return self._asset

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for {}".format(self.__class__.__name__, self.asset())

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, MocapAssetFile) and self.fileId == other.fileId


class AssociatedHelmetCamCalibrationFile(_baseFilePath):
    """
    Associated Helmet Cam Calibration File Context Object
    """
    def __init__(self, table, helmetCamCalibration, services, dataDict):
        super(AssociatedHelmetCamCalibrationFile, self).__init__(Const.FilePathTables.HELMET_CAM_CALIBRATION,
                                                                 helmetCamCalibration,
                                                                 None,
                                                                 services,
                                                                 dataDict)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, AssociatedHelmetCamCalibrationFile) and self.fileId == other.fileId


class _performanceAssetBase(_fileContextBase):
    """
    Internal Base Class

    Base class for all performance assets.
    """
    def __init__(self, services, project, dataDict):
        super(_performanceAssetBase, self).__init__(Const.FilePathTables.TRIAL_PRODUCTION_ASSET, _baseFilePath, services, dataDict)
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getAsset3dByID(self.id)
        self._dataDict = context.getDataDict()

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScript = assetScript.name
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3d = os.path.basename(asset3d.fbxGameAssetPath())
        return "{} - {}".format(assetScript or Const.EMPTY_SUBCONTEXT_NAME, asset3d or Const.EMPTY_SUBCONTEXT_NAME)

    def project(self):
        return self._project

    def hasAudio(self):
        return self._dataDict.get("hasAudio", False)

    def hasMocap(self):
        return self._dataDict.get("hasMocap", False)

    def hasVideo(self):
        return self._dataDict.get("hasVideo", False)

    def isPickupRequired(self):
        return self._dataDict.get("isPickupRequired", False)

    def isAdr(self):
        """
        Get if the character is only for ADR

        return:
            bool if the character is only ADR
        """
        return self._dataDict.get("isADR", False)

    def comment(self):
        """
        Get the comment for the character

        returns:
            string comment for the character
        """
        return self._dataDict.get("comment", "")

    def gameAssetNamespace(self):
        """
        MoBu namespace.

        varchar field with a max of 100 characters

        Returns:
            str
        """
        return self._dataDict.get("gameAssetNamespace", "")

    def getAssetClass(self):
        assetClassId = self._dataDict.get("assetClassID", -1)
        if assetClassId == -1 and "physicalPropVersionID" in set(self._dataDict.keys()):
            # This is a known use case and we must treat this asset as a prop.
            assetClassSetting = self._services.animDataSettingsManager.consts.AssetClasses.PROP
        else:
            assetClassSetting = self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassId)
        return assetClassSetting

    def getAsset3d(self):
        asset3dID = self._dataDict.get("asset3DID", None)
        if asset3dID is None:
            return None
        return self._project.getAsset3dByID(asset3dID)

    def getAssetScript(self):
        assetScriptID = self._dataDict.get("assetScriptID", None)
        if assetScriptID is None:
            return None
        return self._project.getAssetScriptByID(assetScriptID)

    def update(self):
        """
        Needs to call a context specific Watchstar proc.
        """
        raise NotImplementedError("Implement in child class!")

    def delete(self):
        """
        Needs to call a context specific Watchstar proc.
        """
        raise NotImplementedError("Implement in child class!")


class MotionSceneAsset(_performanceAssetBase):
    """
    Base class for motion scene's production assets.
    """


class _productionAssetBase(_performanceAssetBase):
    """
    Internal Base Class

    Base class for classes populated by JSON from trial production assets.

    Notes:
        * Production Assets in Watchstar can have a PhysicalPropVersion regardless of their asset type.
        * A Production Asset will either have a BodyRomID or a PhysicalPropVersionID - not both.
    """
    def __init__(self, services, project, dataDict):
        super(_productionAssetBase, self).__init__(services, project, dataDict)

    def update(self):
        self._services.watchstar.executeCommand("toolswritepub.UpdateTrialSelectAsset",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                ])

    def delete(self):
        """
        Deletes the production asset from the trial.
        """
        self._services.watchstar.executeCommand("toolswritepub.DeleteTrialProductionAsset",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                ])

    def hasMocapProp(self):
        return bool(self._dataDict.get("physicalPropVersionID", False))

    def mocapProp(self):
        """
        Get the mocap prop version.

        return:
            the mocap prop (MocapProp) object
        """
        physicalPropVersionID = self._dataDict.get("physicalPropVersionID", None)
        if physicalPropVersionID is None:
            return None
        return self.project().getMocapPropVersionByID(physicalPropVersionID)

    def hasPerformerROM(self):
        return bool(self._dataDict.get("bodyRomID", False))


class Animal(_productionAssetBase):
    """
    An animal object.

    Notes:
        * Populated by JSON data from a Watchstar Capture's ProductionAsset with an asset class of "animal".
    """
    def __init__(self, services, project, dataDict):
        super(Animal, self).__init__(services, project, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        propVerName = Const.EMPTY_SUBCONTEXT_NAME
        mocapPropVersion = self.mocapPropVersion()
        if mocapPropVersion is not None:
            propVerName = mocapPropVersion.name

        asset3dName = Const.EMPTY_SUBCONTEXT_NAME
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3dName = asset3d.name

        assetScriptName = Const.EMPTY_SUBCONTEXT_NAME
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScriptName = assetScript.name
        return "{} - {} - {}".format(propVerName, asset3dName, assetScriptName)

    def mocapPropVersion(self):
        """
        Get the mocap prop version.

        Notes:
            * This is listed as the physical asset in the Watchstar production assets view.

        return:
            the mocap prop version (MocapPropVersion) object
        """
        physicalPropVersionID = self._dataDict.get("physicalPropVersionID", None)
        if physicalPropVersionID is None:
            return None
        return self.project().getMocapPropVersionByID(physicalPropVersionID)


class Vehicle(_productionAssetBase):
    """
    A vehicle object.

    Notes:
        * Populated by JSON data from a Watchstar Capture's ProductionAsset with an asset class of "vehicle".
    """
    def __init__(self, services, project, dataDict):
        super(Vehicle, self).__init__(services, project, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        propVerName = Const.EMPTY_SUBCONTEXT_NAME
        mocapPropVersion = self.mocapPropVersion()
        if mocapPropVersion is not None:
            propVerName = mocapPropVersion.name

        asset3dName = Const.EMPTY_SUBCONTEXT_NAME
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3dName = asset3d.name

        assetScriptName = Const.EMPTY_SUBCONTEXT_NAME
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScriptName = assetScript.name
        return "{} - {} - {}".format(propVerName, asset3dName, assetScriptName)

    def mocapPropVersion(self):
        """
        Get the mocap prop version.

        Notes:
            * This is listed as the physical asset in the Watchstar production assets view.

        return:
            the mocap prop version (MocapPropVersion) object
        """
        physicalPropVersionID = self._dataDict.get("physicalPropVersionID", None)
        if physicalPropVersionID is None:
            return None
        return self.project().getMocapPropVersionByID(physicalPropVersionID)


class Environment(_productionAssetBase):
    """
    An environment object.

    Notes:
        * Populated by JSON data from a Watchstar Capture's ProductionAsset with an asset class of "environment".
    """
    def __init__(self, services, project, dataDict):
        super(Environment, self).__init__(services, project, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        propVerName = Const.EMPTY_SUBCONTEXT_NAME
        mocapPropVersion = self.mocapPropVersion()
        if mocapPropVersion is not None:
            propVerName = mocapPropVersion.name

        asset3dName = Const.EMPTY_SUBCONTEXT_NAME
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3dName = asset3d.name

        assetScriptName = Const.EMPTY_SUBCONTEXT_NAME
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScriptName = assetScript.name
        return "{} - {} - {}".format(propVerName, asset3dName, assetScriptName)

    def mocapPropVersion(self):
        """
        Get the mocap prop version.

        Notes:
            * This is listed as the physical asset in the Watchstar production assets view.

        return:
            the mocap prop version (MocapPropVersion) object
        """
        physicalPropVersionID = self._dataDict.get("physicalPropVersionID", None)
        if physicalPropVersionID is None:
            return None
        return self.project().getMocapPropVersionByID(physicalPropVersionID)


class Prop(_productionAssetBase):
    """
    A prop object.

    Notes:
        * Populated by JSON data from a Watchstar Capture's ProductionAsset with an asset class of "prop".
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            project (Project): Project that the prop is under
            dataDict (dict): A nested dict of values from the database
        """
        super(Prop, self).__init__(services, project, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        propVerName = Const.EMPTY_SUBCONTEXT_NAME
        mocapPropVersion = self.mocapPropVersion()
        if mocapPropVersion is not None:
            propVerName = mocapPropVersion.name

        asset3dName = Const.EMPTY_SUBCONTEXT_NAME
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3dName = asset3d.name

        assetScriptName = Const.EMPTY_SUBCONTEXT_NAME
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScriptName = assetScript.name
        return "{} - {} - {}".format(propVerName, asset3dName, assetScriptName)

    def gameProp(self):
        """
        Get the game prop object

        notes:
            * Only here to satisfy old api. Use self.getAsset3d() instead.

        return:
            the game prop (GameProp) object
        """
        return self.getAsset3d()

    def mocapPropVersion(self):
        """
        Get the mocap prop version.

        Notes:
            * This is listed as the physical asset in the Watchstar production assets view.

        return:
            the mocap prop version (MocapPropVersion) object
        """
        physicalPropVersionID = self._dataDict.get("physicalPropVersionID", None)
        if physicalPropVersionID is None:
            return None
        return self.project().getMocapPropVersionByID(physicalPropVersionID)

    def updateProp(self, mocapPropVersion=None, scriptAsset=None, gameAsset=None):
        """
        Update the Prop
        """
        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@TrialProductionAssetID={}".format(self.id),
        ]
        kwargs = []

        if mocapPropVersion is not None:
            kwargs.append("@PhysicalPropVersionID={}".format(mocapPropVersion.id))
        if scriptAsset is not None:
            kwargs.append("@ProjectScriptAssetID={}".format(scriptAsset.id))
        if gameAsset is not None:
            kwargs.append("@Project3DAssetID={}".format(gameAsset.id))

        returnData = self._services.watchstar.executeCommand("toolswritepub.UpdateTrialProductionAsset",
                                                kwargList=defaultArgs + kwargs)

        #TODO: Bug for watchstar to improve this return value
        for assetDict in returnData['captures'][0]['productionAssets']:
            if assetDict["asset3DID"] == self.id:
                self._dataDict = assetDict
                break


class Character(_productionAssetBase):
    """
    A character object.

    Notes:
        * Populated by JSON data from a Watchstar Capture's ProductionAsset with an asset class of "character".
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            project (Project): Project that the character is under
            dataDict (dict): A nested dict of values from the database
        """
        super(Character, self).__init__(services, project, dataDict)
        self._project = project

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        performerROM = self.performerROM()
        if performerROM is not None:
            performerROM = performerROM.name
        assetScript = self.getAssetScript()
        if assetScript is not None:
            assetScript = assetScript.name
        asset3d = self.getAsset3d()
        if asset3d is not None and asset3d.fbxGameAssetPath() is not None:
            asset3d = os.path.basename(asset3d.fbxGameAssetPath())
        return "{} ({}) - {}".format(performerROM or Const.EMPTY_SUBCONTEXT_NAME,
                                     assetScript or Const.EMPTY_SUBCONTEXT_NAME,
                                     asset3d or Const.EMPTY_SUBCONTEXT_NAME)

    def performerROM(self):
        """
        Get the performer ROM.

        Notes:
            * This is listed as the physical asset in the Watchstar production assets view.

        return:
            None: There is no rom. | BodyRomTrial: The current performer rom.
        """
        dataID = self._dataDict.get("bodyRomID")
        if dataID is None:
            return None
        return self.project().getTrialByID(dataID)

    def color(self):
        """
        get the color of the character

        return:
            The colour as a hex colour value
        """
        if self.performerROM():
            return self.performerROM().primaryPatchColor()
        return None

    def helmetCamRevision(self):
        """
        Get the helmetCamRevision.

        This attr is only in the json if the talent was wearing a helmet.

        return:
            HelmetCamRevision or None
        """
        helmetCamRevisionId = self._dataDict.get("helmetCamRevisionID", None)
        if helmetCamRevisionId is None:
            return None
        for helmetCam in self._services.animDataContext.getHelmetCams():
            for helmetCamRevision in helmetCam.revisions():
                if helmetCamRevision.id == helmetCamRevisionId:
                    return helmetCamRevision
        return None

    def helmetCamCalibration(self):
        """
        Get the helmet cam's calibration.

        This attr is only in the json if the talent was wearing a helmet and the helmet cam revision
        is dual cam.

        return:
            HelmetCamCalibration or None
        """
        helmetCamCalibrationId = self._dataDict.get("helmetCamCalibrationID", None)
        if helmetCamCalibrationId is None:
            return None
        return self._services.animDataContext.getHelmetCamCalibrationByID(helmetCamCalibrationId)

    def gameChar(self):
        """
        get the character object of the game character

        return:
            Game Character (GameCharacter) object for the character
        """
        return self.getAsset3d()

    def scriptChar(self):
        """
        Get the script character object

        return:
            Script Character (ScriptCharacter) object for the character
        """
        return self.getAssetScript()

    @memorize.memoized
    def talent(self):
        if self.isAdr():
            return self.project().getTalentByID(self._dataDict["adrTalentProjectID"])
        else:
            try:
                return self.performerROM().bodyScale().talentVariation().talent()
            except Exception as error:
                # TODO: add logging support here.
                return None

    @memorize.memoized
    def talentVariation(self):
        if self.isAdr():
            # ADR doesnt have Variation
            return None

        dataID = self._dataDict.get("talentVariationID")
        if dataID is None:
            rom = self.performerROM()
            if rom is None:
                return None
            bodyScaleID = rom.getDataDict().get("bodyScaleID")
            bodyScale = self.project().getTrialByID(bodyScaleID)
            if not isinstance(bodyScale, BodyScaleTrial):
                return None
            return bodyScale.talentVariation()
        return self.project().getTalentVariationById(dataID)

    def scriptCharClass(self):
        """
        Get the script character class

        return:
            Script character class as a string
        """
        return self.getAssetScript().getAssetClass().name

    def faceAudioPack(self):
        """
        Get the face Audio Pack

        return:
            face Audio Pack as a string
        """
        audioFaceDeviceId = self._dataDict.get("audioFaceDeviceID", None)
        if audioFaceDeviceId is None:
            return None
        return self._services.animDataSettingsManager.getFaceAudioPack(audioFaceDeviceId)

    def updateCharacter(self, performerRom=None, scriptChar=None, gameChar=None, faceAudioPack=None):
        """
        Update the Character

        Does not include the mutually exclusive ADR related params.
        """
        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@TrialProductionAssetID={}".format(self.id),
        ]
        kwargs = []

        if performerRom is not None:
            kwargs.append("@RomBodyID={}".format(performerRom.captureId))

        if scriptChar is not None:
            kwargs.append("@ProjectScriptAssetID={}".format(scriptChar.charId))

        if gameChar is not None:
            kwargs.append("@Project3DAssetID={}".format(gameChar.charId))

        if faceAudioPack is not None:
            kwargs.append("@AudioFaceDeviceID={}".format(faceAudioPack.settingsID))

        returnData = self._services.watchstar.executeCommand("toolswritepub.UpdateTrialProductionAsset",
                                                kwargList=defaultArgs + kwargs)

        #TODO: Bug for watchstar to improve this return value
        for assetDict in returnData['captures'][0]['productionAssets']:
            if assetDict["asset3DID"] == self.id:
                self._dataDict = assetDict
                break

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, Character) and self._dataDict == other._dataDict

    def __hash__(self):
        return self._dataDict.get("scriptCharID", "-1").__hash__()


class AssetGameBase(_fileContextBase):
    """
    Base class for all classes populated by JSON data from Watchstar Asset3d objects.

    Notes:
        * This base class is not an internal class, as it is needed for type checking in models.
    """
    def __init__(self, services, project, dataDict):
        super(AssetGameBase, self).__init__(Const.FilePathTables.PROJECT_3D_ASSET, Asset3dFile, services, dataDict)
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getRawAsset3dByID(self.id)
        self._dataDict = context.getDataDict()

    @property
    def name(self):
        """
        Get the name of the game character (driven from its FBX file path name)

        return:
            str
        """
        return str(os.path.basename(self.fbxGameAssetPath()))

    def fbxGameAssetPath(self):
        """
        Get the file path for the fbx game asset

        return:
            str: p4 depot path
        """
        return str(self._dataDict["fbxGameAssetPath"])

    def filePath(self):
        """
        Get the file path for the fbx game asset

        notes:
            * Only here to satisfy old api. Use self.fbxGameAssetPath().name instead.

        return:
            str: p4 depot path
        """
        return self.fbxGameAssetPath()

    def assetId(self):
        """
        Get the Asset ID of the asset

        return:
            asset ID as an int
        """
        return self.id

    def project(self):
        """
        Get the project of the character

        return:
            project (Project) of the character
        """
        return self._project

    def notes(self):
        """
        Get the notes for the asset

        return:
            str
        """
        return str(self._dataDict["notes"])

    def isActive(self):
        """
        Get if the asset is still active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    @property
    def mocapSubstituteID(self):
        return self._dataDict.get("mocapSubstituteID", None)

    def trialType(self):
        """
        Get the trial type of the asset (IG or CS)

        return:
            str: type of the character
        """
        return str(self._dataDict["csig"])

    def getAssetClass(self):
        assetClassID = self._dataDict.get("assetClassID", None)
        if assetClassID is None:
            return None
        return self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassID)

    def getAssetType(self):
        assetTypeID = self._dataDict["assetTypeID"]
        return self._services.animDataSettingsManager.getAssetType(assetTypeID)

    @property
    def charId(self):
        """
        Get the ID of the character

        return:
            int: character ID
        """
        return self.id

    def characterClass(self):
        """
        Get the class of the character

        return:
            str: character class
        """
        return self.getAssetClass()

    def characterSubClass(self):
        """
        Get the sub class of the character

        return:
            CharacterSubType settings object
        """
        return self.getAssetType()

    def gsSubstitute(self):
        """
        Get the gs Substitute Character for this character

        Notes:
            * This method shouldn't be memoized.

        return:
            GameCharacter object which is used to substitute out the GS Values
        """
        subID = self.mocapSubstituteID or None
        if subID is None:
            return None
        return self.project().getAsset3dByID(subID)

    def mocapAsset(self):
        """
        Get the mocap asset if one exists for this 3d asset.

        Notes:
            * In Watchstar, an Asset3d object can have only one MocapAsset object parented to it.

        return:
            MocapAsset: If a mocap asset exists for the 3d asset. | None: If no MocapAsset exists.
        """
        mocapAssets = self._handleMocapAssetCreation(self._dataDict, self.project(), self)
        if mocapAssets:
            return mocapAssets[0]
        return None

    def isGsReady(self):
        """
        Get if the asset is ready for use on stage or not.

        return:
            bool
        """
        return self._dataDict["isMocapReady"]

    def updateCharacter(self, characterSubType=None, fbxPath=None, isActive=None, isGsReady=None, notes=None,
                        trialType=None, gsSubstitute=None, clearGsSubstitute=False):
        """
        Edit the 3d asset object properties.

        kwargs:
            characterSubType (): The character sub-type.
            fbxPath (str): Depot path for the FBX.
            isActive (bool): If the asset is still active or not
            isGsReady (bool): If the asset is 'mocap ready', this was called 'GS Ready'
            notes (str): New text for the notes field.
            trialType (str): CS or IG
            gsSubstitute (int): Id of the gs substitute to add.
            clearGsSubstitute (bool): If True, will remove the gs substitute.
        """
        kwargs = []
        if characterSubType is not None:
            kwargs.append("@Asset3DTypeID={0}".format(characterSubType.settingsID))

        if fbxPath is not None:
            kwargs.append("@FBXGameAssetPath='{0}'".format(fbxPath))

        if isActive is not None:
            kwargs.append("@IsActive={0}".format(str(isActive).lower()))

        if isGsReady is not None:
            kwargs.append("@IsMocapReady={0}".format(str(isGsReady).lower()))

        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))

        if trialType is not None:
            kwargs.append("@CSIG='{0}'".format(trialType))

        if gsSubstitute is not None:
            kwargs.append("@MocapSubstituteID={0}".format(gsSubstitute.charId))

        if clearGsSubstitute is True:
            kwargs.append("@MocapSubstituteID={0}".format(-1))

        if len(kwargs) == 0:
            return

        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@Project3DAssetID={}".format(self.charId)
        ]

        self._services.watchstar.executeCommand("toolswritepub.UpdateAsset3D",
                                                kwargList=defaultArgs + kwargs
                                                )
        self._refresh()

    def addMocapAsset(self, anmDepotPath, isMocapReady=None, notes=None, footwear=None):
        """
        Adds a MocapAsset for this Asset3d.

        Only one MocapAsset can be added to an Asset3d.

        args:
            anmDepotPath (str): The depot path for the .anm file.

        kwargs:
            isMocapReady (bool): If the asset is 'mocap ready', this was called 'GS Ready'
                Default: True
            notes (str): The notes for the asset.
                Default: ""
            footwear (str): The footwear used.
                Default: ""

        return:
            MocapAsset
        """
        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@AnyProjectID={}".format(self.project().id),
            "@ANMMocapAssetPath='{}'".format(anmDepotPath),
            "@Parent3DAssetID={}".format(self.id),
        ]

        kwargs = []
        if isMocapReady is not None:
            kwargs.append("@IsMocapReady={0}".format(str(isMocapReady).lower()))
        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))
        if footwear is not None:
            kwargs.append("@Footwear='{0}'".format(footwear))

        self._services.watchstar.executeCommand("toolswritepub.AddAssetMocap",
                                                kwargList=defaultArgs + kwargs
                                                )

        self._refresh()


class GameAnimal(AssetGameBase):
    """
    A game animal object.

    Notes:
        * Populated by JSON data from a Watchstar Asset3d object with an asset class of "animal".
    """


class GameEnvironment(AssetGameBase):
    """
    An environment animal object.

    Notes:
        * Populated by JSON data from a Watchstar Asset3d object with an asset class of "environment".
    """


class GameVehicle(AssetGameBase):
    """
    A game vehicle object.

    Notes:
        * Populated by JSON data from a Watchstar Asset3d object with an asset class of "vehicle".
    """


class GameCharacter(AssetGameBase):
    """
    A game character object.

    Notes:
        * Populated by JSON data from a Watchstar Asset3d object with an asset class of "character".
    """

    @property
    def charId(self):
        """
        Get the ID of the character

        return:
            character ID as an int
        """
        return self.id

    def characterClass(self):
        """
        Get the class of the character

        return:
            character class as a string
        """
        return self.getAssetClass()

    def characterSubClass(self):
        """
        Get the sub class of the character

        return:
            CharacterSubType settings object
        """
        return self.getAssetType()


class MocapAsset(_fileContextBase):
    """
    A mocap asset object.

    Notes:
        * Populated by JSON data from a Watchstar MocapAsset.
        * In Watchstar, an Asset3d object can have only one MocapAsset object parented to it.
    """
    def __init__(self, services, project, parentAsset, dataDict):
        super(MocapAsset, self).__init__(Const.FilePathTables.MOCAP_ASSET, MocapAssetFile, services, dataDict)
        self._project = project
        self._parentAsset = parentAsset
        self._anmFilePath = self._services.animDataSettingsManager.consts.FilePathTypes.ANM_MOCAP_ASSET
        self._fbxFilePath = self._services.animDataSettingsManager.consts.FilePathTypes.FBX_MOCAP_ASSET

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getRawMocapAssetByID(self.id)
        self._dataDict = context.getDataDict()

    def getAllFiles(self, pathType=None, refresh=False, validate=True):
        """
        Reimplemented from _fileContextBase

        Get all the files associated with the trial as a list

        return:
            List of AssociatedFile objects
        """
        assocFiles = super(MocapAsset, self).getAllFiles(pathType=pathType, refresh=refresh)

        # validation
        groupedByType = {}
        for assocFile in assocFiles:
            assocFileType = assocFile.fileType()
            group = groupedByType.get(assocFileType, [])
            group.append(assocFile)
            groupedByType[assocFileType] = group

        gsfbxFiles = groupedByType.get(self._services.animDataSettingsManager.consts.FilePathTypes.FBX_MOCAP_ASSET, [])
        if validate is True and len(gsfbxFiles) > 1:
            # This specific error message text is checked elsewhere. If you change it, grep the codebase for
            # "More than one FBX_MOCAP_ASSET" and update those lines as well.
            raise ValueError("More than one FBX_MOCAP_ASSET file found on '{}'. "
                             "Please pass this info onto the Techart/Watchstar teams to resolve.".format(repr(self)))

        return assocFiles

    @property
    def name(self):
        """
        Get the name of the game character (driven from its ANM file path name)

        return:
            str
        """
        return str(self._dataDict["name"])

    def project(self):
        """
        Get the project of the character

        return:
            project (Project) of the character
        """
        return self._project

    @memorize.memoized
    def parent3dAsset(self):
        """
        Get the 3d Asset for which the mocap asset was created.

        return:
            child class of AssetGameBase
        """
        if self._parentAsset is not None:
            return self._parentAsset
        parentAssetId = str(self._dataDict["parent3DAssetID"])
        return self.project().getRawAsset3dByID(parentAssetId)

    def notes(self):
        """
        Get the notes for the asset.

        return:
            str: notes
        """
        return str(self._dataDict["notes"])

    def isActive(self):
        """
        Get if the asset is still active or not.

        return:
            bool
        """
        return self._dataDict["isActive"]

    def footwear(self):
        """
        Get the footwear of the asset

        return:
            str: the footwear
        """
        return str(self._dataDict.get("footwear", ""))

    def gsAnm(self):
        """
        Get the gs ANM of the character.

        return:
            str: gs ANM depot path
        """
        return str(self._dataDict["anmMocapAssetPath"])

    def setGsAnm(self, filePath):
        """
        Set the gs .anm file.

        The .anm is stored on the object, not as a separate file object.

        Args:
            filePath (str): Depot path to the .anm file.
        """
        self.update(anmDepotPath=filePath)

    def gsFBX(self):
        """
        Get the gs FBX of the character.

        return:
            str: gs FBX depot path
        """
        files = self.getAllFiles(self._fbxFilePath)
        if len(files) != 1:
            return None
        return files[0].filePath()

    def setGsFBX(self, filePath):
        """
        Set the gs .fbx file.

        The .fbx is stored as a separate file object.

        Args:
            filePath (str): Depot path to the .anm file.
        """
        files = self.getAllFiles(self._fbxFilePath)
        if filePath is None or filePath == "":
            if len(files) == 1:
                # Delete it!
                files[0].deletePath()
        else:
            if len(files) == 1:
                # Update it!
                files[0].updatePath(newFilePath=filePath)
            elif len(files) < 1:
                # Add a new file type!
                self.addNewFile(self._fbxFilePath, filePath)
            else:
                # This specific error message text is checked elsewhere. If you change it, grep the codebase for
                # "More than one FBX_MOCAP_ASSET" and update those lines as well.
                raise ValueError("More than one FBX_MOCAP_ASSET file found on '{}'. "
                                 "Please pass this info onto the Techart/Watchstar teams to resolve.".format(repr(self)))

        self._refresh()

    def update(self, anmDepotPath=None, isActive=None, notes=None, footwear=None):
        """
        Edit the mocap asset object properties in Watchstar.

        Refreshes the data dict after committing the updates to Watchstar so all methods are accessing the latest data.

        kwargs:
            anmDepotPath (str): The .anm p4 depot path. This sets the object name.
            isActive (bool): If the asset is still active or not
            notes (str): The notes for the asset.
            footwear (str): The footwear used.
        """
        defaultArgs = [
            "@UserName='{}'".format(self._services.getUserName()),
            "@MocapAssetID={}".format(self.id)
        ]

        kwargs = []
        if anmDepotPath is not None:
            kwargs.append("@ANMMocapAssetPath='{0}'".format(anmDepotPath))
        if isActive is not None:
            kwargs.append("@IsActive={0}".format(str(isActive).lower()))
        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))
        if footwear is not None:
            kwargs.append("@Footwear='{0}'".format(footwear))
        if len(kwargs) == 0:
            return

        self._services.watchstar.executeCommand("toolswritepub.UpdateAssetMocap",
                                                kwargList=defaultArgs + kwargs
                                                )
        self._refresh()


class AssetScriptBase(_dataDictContextBase):
    """
    Base class for all classes populated by JSON data from Watchstar AssetScript objects.

    Notes:
        * This base class is not an internal class, as it is needed for type checking in models.
    """
    def __init__(self, services, project, dataDict):
        super(AssetScriptBase, self).__init__(services, dataDict)
        self._project = project

    def project(self):
        """
        Get the project of the character

        return:
            project (Project) of the character
        """
        return self._project

    def notes(self):
        """
        Get the notes for the asset

        return:
            string notes
        """
        return self._dataDict.get("notes", "")

    def codeName(self):
        """
        Get the code name for the asset

        return:
            string notes
        """
        return self._dataDict["codeName"]

    def getAssetClass(self):
        assetClassId = self._dataDict.get("assetClassID", None)
        return self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassId)

    def characterClass(self):
        """
        Get the class of the character

        return:
            character class as a string
        """
        return self.getAssetClass()


class ScriptAnimal(AssetScriptBase):
    """
    Script animal

    Notes:
        * Populated by JSON data from a Watchstar AssetScript object with an asset class of "animal".
    """


class ScriptProp(AssetScriptBase):
    """
    A script prop object.

    Notes:
        * Populated by JSON data from a Watchstar AssetScript object with an asset class of "prop".
    """


class ScriptEnvironment(AssetScriptBase):
    """
    A script environment object.

    Notes:
        * Populated by JSON data from a Watchstar AssetScript object with an asset class of "environment".
    """


class ScriptVehicle(AssetScriptBase):
    """
    A script vehicle object.

    Notes:
        * Populated by JSON data from a Watchstar AssetScript object with an asset class of "vehicle".
    """


class ScriptCharacter(AssetScriptBase):
    """
    A script character object.

    Notes:
        * Populated by JSON data from a Watchstar AssetScript object with an asset class of "character".
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            project (Project): Project that the character is under
            dataDict (dict): A nested dict of values from the database
        """
        super(ScriptCharacter, self).__init__(services, project, dataDict)

    @property
    def charId(self):
        """
        Get the ID of the character

        return:
            character ID as an int
        """
        return self.id

    def characterClass(self):
        """
        Get the class of the character

        return:
            character class as a string
        """
        assetClassId = self._dataDict["assetClassID"]
        return self._services.animDataSettingsManager.getAssetClasses(settingID=assetClassId)

    def characterName(self):
        """
        Get the name of the character

        return:
            character name as a string
        """
        return self.name

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, ScriptCharacter) and self.charId == other.charId


class MotionSelect(_dataDictContextBase):
    """
    A motion select object.

    Notes:
        * Populated by JSON data from a Watchstar ProductionAction object.
    """
    def __init__(self, parentContext, services, dataDict):
        """
        Constructor

        args:
            parentContext (contexts._contextBase): A MotionTrialBase derived or MotionTrialSelect object.
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MotionSelect, self).__init__(services, dataDict)
        self._parentContext = parentContext

    def actionNumber(self):
        """
        Get the action Number

        return:
            int for the action Number
        """
        return int(self._dataDict.get("actionNum", "0"))

    def hasMocap(self):
        """
        Get if the mocap is selected

        return:
            bool: if the mocap is selected
        """
        return self._dataDict.get("hasMocap", False)

    def hasAudio(self):
        """
        Get if the audio is selected

        return:
            bool if the audio is selected
        """
        return self._dataDict.get("hasAudio", False)

    def hasVideo(self):
        """
        Get if the video is selected

        return:
            bool if the video is selected
        """
        return self._dataDict.get("hasVideo", False)

    def timecode(self):
        """
        Get the start timecode for the select

        return:
            str
        """
        return self._dataDict.get("timeCode", None)

    def notes(self):
        """
        Get the notes for the select

        return:
            A string of the notes for the select
        """
        return self._dataDict.get("notes", "")

    @property
    def name(self):
        """
        Get the name of the object from the parent context; Actions do not have a "name" key.

        return:
            str
        """
        return "action #{} on '{}'".format(self.actionNumber(), self._parentContext.name)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for {}".format(self.__class__.__name__, self.name)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, MotionSelect) and self._dataDict == other._dataDict

    def __hash__(self):
        return self.actionNumber().__hash__()


class MotionTrialSelect(_dataDictContextBase):
    """
    Trial Select Context Object
    """
    def __init__(self, motionTrial, project, services, dataDict):
        """
        Constructor

        args:
            motionTrial (MotionTrialBase): The trial this trial select is on.
            project (Project): The project that the trial select is on.
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MotionTrialSelect, self).__init__(services, dataDict)
        self._trial = motionTrial
        self._project = project

    def trial(self):
        """
        Get the trial that the trial select is on.

        return:
            MotionTrialBase
        """
        if self._trial is None:
            trialId = self._dataDict["trialID"]  # Should always have the trial ID.
            self._trial = self._services.animDataContext.getTrialByID(trialId)
        return self._trial

    def project(self):
        """
        The project that the trial select is on.

        Returns:
            Project
        """
        return self._project

    def composite(self):
        motionCompositeId = self._dataDict.get("motionSceneID", None)
        if motionCompositeId:
            return self._project.getCompositeByID(motionCompositeId)
        return None

    def selectNumber(self):
        """
        Get the select number.

        return:
            int: The select number.
        """
        return int(self._dataDict.get("selectNumber", "0"))

    def getSelects(self):
        """
        Get the actions for the trial (in V4 of Watchstar, these objects were called "selects").

        return:
            List of MotionSelect objects
        """
        return self._handleSelectCreation(self._dataDict, self)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}' on '{}'".format(self.__class__.__name__, self.name, self.trial().name)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, MotionTrialSelect) and self._dataDict == other._dataDict


class MotionCompositeSelect(MotionSelect):
    """
    Composite Select Context Object
    """
    def __init__(self, motionComposite, services, dataDict):
        """
        Constructor

        args:
            motionComposite (MotionComposite): The motionComposite this select is from
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MotionCompositeSelect, self).__init__(services, dataDict)
        self._composite = motionComposite

    def composite(self):
        """
        Get the composite the select is from

        return:
            MotionTrialBase which the select is for
        """
        return self._composite

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Select action ({0}) object for of '{1}'".format(self.actionNumber(), self._composite.name)

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, MotionCompositeSelect) and self._dataDict == other._dataDict


class Talent(_fileContextBase):
    """
    Talent Context Object
    """
    def __init__(self, services, dataDict, project):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Talent, self).__init__(Const.FilePathTables.TALENT_PROJECT, _baseFilePath, services, dataDict)
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getTalentByID(self.id)
        self._dataDict = context.getDataDict()

    def project(self):
        """
        Get the project for the Talent

        return:
            the project (Project) for the talent
        """
        return self._project

    @property
    def name(self):
        """
         Get the name of the talent

         return:
             string name of the talent
        """
        return "{0} {1} ({2})".format(self.firstName(), self.lastName(), self.scaleAbbreviation())

    @property
    def id(self):
        """
        Reimplemented from _dataDictBase

        Get the talentProjectID of the Talent

        return:
            int: TalentProjecID
        """
        return int(self._dataDict["talentProjectID"])

    @property
    def talentId(self):
        """
        Get the ID of the Talent

        return:
            Talent ID as an int
        """
        return self.id

    def firstName(self):
        """
        Get the first name of the Talent

        return:
            first name as an string
        """
        return self._dataDict.get("firstName")

    def lastName(self):
        """
        Get the last name of the Talent

        return:
            last name as an string
        """
        return self._dataDict.get("lastName", "")

    def scaleAbbreviation(self):
        """
        Get the scale Abbreviation of the Talent

        return:
            the scale abbreviation as a string
        """
        return self._dataDict.get("scaleAbbrev")

    def gender(self):
        """
        Get the gender of the Talent

        return:
            the gender as a string
        """
        return self._dataDict.get("gender")

    def height(self):
        """
        Get the height of the Talent

        return:
            the height as a string
        """
        return self._dataDict.get("height")

    def age(self):
        """
        Get the age of the Talent

        return:
            the age as a string
        """
        return self._dataDict.get("birthDate")

    @memorize.memoizeWithExpiry(60)
    def getVariations(self):
        """
        Get the Talent variations

        return:
            list of Talent Variations
        """
        data = self._services.watchstar.executeCommand("tools.TalentVariationSearchByTalentProject",
                                                       [
                                                           self._services.getUserName(),
                                                           self.talentId
                                                       ])
        if data is None:
            return []
        return self._handleTalentVariationCreation(data, self.project())

    def notes(self):
        """
        Get the notes for the Talent

        return:
            the notes as a string
        """
        return self._dataDict.get("notes", "")

    def species(self):
        """
        Get the species for the Talent

        return:
            the species as a string
        """
        return self._dataDict.get("species")

    def isActive(self):
        """
        Get the isActive for the Talent

        return:
            the isActive as a bool
        """
        return self._dataDict.get("isActive")

    def getVariationProjects(self):
        """
        Get a list of projects where the is a varaition for the talent

        return:
            list of projects (Projects)
        """
        data = self._services.watchstar.executeCommand("tools.AnyProjectSearchByTalent",
                                                       [
                                                           self._services.getUserName(),
                                                           self.talentId
                                                       ])
        if data is None:
            return []
        return self._handleProjectCreation(data)

    def isStaff(self):
        """
        Get if the talent is also staff

        return:
            True of False if the Talent is also staff
        """
        return self._dataDict.get("talentStaff") == "Staff"

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "{} object for '{}' on '{}'".format(self.__class__.__name__, self.name, self.project().name)

    def __repr__(self):
        """
        Printable representation of object
        """
        return "<{} (id: {}) on '{}'at {}>".format(self.__str__(), self.id, self.project().name, hex(id(self)))


class TalentVariation(_fileContextBase):
    """
    Talent Variation Context Object
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            project (Project): Project that the Talent Variation is under
            dataDict (dict): A nested dict of values from the database
        """
        if project is None:
            raise ValueError("TalentVariation initialized without a project!")

        super(TalentVariation, self).__init__(Const.FilePathTables.TALENT_PROJECT_VARIATION, _baseFilePath, services, dataDict)
        self._project = project

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._project.getTalentByID(self.id)
        self._dataDict = context.getDataDict()

    def project(self):
        """
        Get the project of the Talent Variation

        return:
            project (Project) of the Talent Variation
        """
        return self._project

    def talentVariation(self):
        """
        Get the variation type the Talent

        return:
            Setting variation object
        """
        return self._services.animDataSettingsManager.getVariations(self._dataDict.get("variationID", "0"))

    def getLatestBodyRomsForTalentVariation(self):
        """
        Get the last 10 body ROMs for the talent variation

        return:
            list of upto 10 BodyRomTrials
        """
        data = self._services.watchstar.executeCommand("tools.BodyRomSearchByTalentVariation",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id
                                                       ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self.project())

    def talentId(self):
        """
        Get the ID of the Talent

        return:
            Talent ID as an int
        """
        return int(self._dataDict["talentProjectID"])

    def talent(self):
        """
        Get the Talent that the variation is under

        return:
            Talent as a Talent object
        """
        return self._project.getTalentByID(self.talentId())

    @memorize.memoized
    def defaultScales(self):
        """
        Get a dict of the default scaless of the talent variation and their markersets

        return:
            dict of MarkerSet settings to MotionScaleTrial of the default scales
        """
        returnScales = {}
        scales = self._dataDict.get("defaultScales", [])
        for scale in scales:
            scaleID = scale.get("id")
            if scaleID is not None:
                scale = self._project.getTrialByID(scaleID)
                returnScales[scale.markerSet()] = scale
        return returnScales

    @memorize.memoizeWithExpiry(30)
    def _getBodyScaleTrials(self, username, talentVariationId):
        """
        Query Watchstar for all the body scale trial data from the talent variation.

        Cached for limited time to help performance when user is browsing back and forth between ROMs.

        return:
            list of BodyScaleTrials
        """
        data = self._services.watchstar.executeCommand("tools.BodyScaleSearchByTalentVariation",
                                                       [
                                                           username,
                                                           talentVariationId
                                                       ])
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self.project())

    def getBodyScaleTrials(self):
        """
        Get all the body scale trials from the talent variation.

        return:
            list of BodyScaleTrials
        """
        return self._getBodyScaleTrials(self._services.getUserName(), self.id)

    def getBodyRomTrials(self, selectsOnly=False):
        """
        Get the 10 most recent body rom trials from the talent variation.

        return:
            list of BodyScaleTrials
        """
        val = []
        val.append("@SelectsOnly={0}".format(str(selectsOnly).lower()))
        data = self._services.watchstar.executeCommand("tools.BodyRomSearchByTalentVariation",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id
                                                       ],
                                                       kwargList=val
                                                       )
        if data is None:
            return []
        return self._handleTrialCreation(data, None, None, self.project())

    def imagePath(self):
        """
        Get the p4 depot image Path of the Talent variation

        return:
            the imagePath as a string
        """
        pathType = self._services.animDataSettingsManager.consts.FilePathTypes.TALENT_VARIATION_IMAGE
        data = self._getFilePaths(pathType=pathType, refresh=False)
        if not data:
            return None
        return data[0].get("path")

    @property
    def name(self):
        return self.talentVariation().name

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "TalentVariation object of '{0}' for '{1}'".format(self.name, self.talent().name)


class Probspec(_dataDictContextBase):
    """
    object for problem setups
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(Probspec, self).__init__(services, dataDict)
        self._project = project
        self._name = os.path.basename(dataDict.get("giantPSPPath"))

    def project(self):
        """
        Get the project of the probspec

        return:
            project (Project) of the probspec
        """
        return self._project

    @property
    def name(self):
        """
        Get the name of the probspec

        return:
            probspec name as a string
        """
        return self._name

    def path(self):
        """
        Get the probspec path as a string

        return:
            probspec path as a string
        """
        return self._dataDict.get("giantPSPPath")

    def isUsableInNewScales(self):
        """
        Is the propspec usable in new scales

        return:
            bool
        """
        return self._dataDict.get("isUsableInNewScales", False)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Probspec object for '{0}' in '{1}'".format(self.name, self.project().name)


class MocapProp(_fileContextBase):
    """
    A mocap prop object.

    Notes:
        * Populated by JSON data from a Watchstar StageProp object.
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (Project): The project
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MocapProp, self).__init__(Const.FilePathTables.STAGE_PROP_PROJECT, _baseFilePath, services, dataDict)
        self._project = project

    @property
    def id(self):
        """
        Get the ID of the Watchstar object.

        return:
            int: The object's ID
        """
        return int(self._dataDict["stagePropProjectID"])

    def isActive(self):
        """
        If the prop is active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    def isPerformanceProp(self):
        """
        If the prop is a performance prop or not

        return:
            bool
        """
        return self._dataDict["isPerformanceProp"]

    def isGlobal(self):
        """
        If the prop is 'global' or not

        return:
            bool
        """
        return self._dataDict["isGlobal"]

    def notes(self):
        """
        Get the prop's notes

        return:
            str of the notes
        """
        return self._dataDict["notes"]

    def project(self):
        """
        Get the project

        return:
            Project
        """
        return self._project

    def propVariations(self):
        """
        Get the prop's variations

        return:
            list of MocapPropVariations
        """
        physicalProps = self._dataDict.get("physicalProps")
        if physicalProps is None:
            return []
        return self._handleMocapPropVariationCreation(physicalProps, self, self.project())

    def category(self):
        """
        Get the prop's category.

        return:
            settings.MocapPropCategory
        """
        settingsId = self._dataDict["stagePropCategoryID"]
        return self._services.animDataSettingsManager.getMocapPropCategories(settingID=settingsId)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Mocap Prop object for '{}' in '{}'".format(self.name, self.project().name)


class MocapPropVariation(_dataDictContextBase):
    """
    A mocap prop variation object.

    Notes:
        * Populated by JSON data from a Watchstar PhysicalProp object (aka: a physical prop at a specific location).
    """
    def __init__(self, mocapProp, project, services, dataDict):
        """
        Constructor

        args:
            mocapProp (MocapProp): The mocap prop the variation is for
            project (Project): The project
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        self._mocapProp = mocapProp
        self._project = project
        super(MocapPropVariation, self).__init__(services, dataDict)

    def isActive(self):
        """
        If the prop is active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    def prop(self):
        """
        The prop the variation is for

        return:
            MocapProp
        """
        return self._mocapProp

    def location(self):
        """
        The location the prop variation is for (aka: a specific stage)

        return:
            Location
        """
        return self._services.animDataContext.getLocationFromID(self._dataDict['currLocationID'])

    def comment(self):
        """
        The comment for the prop variation

        return:
            str
        """
        return self._dataDict['comment']

    def barcode(self):
        """
        The barcode for the prop variation

        return:
            str
        """
        return self._dataDict['barcode']

    def isMarkered(self):
        """
        If the prop variation is markered or not

        return:
            str
        """
        return self._dataDict['currIsMarkered']

    def project(self):
        """
        Get the project

        return:
            Project
        """
        return self._project

    def propVersions(self):
        """
        Get the prop versions for the prop variation

        return:
            list of MocapPropVersion
        """
        data = self._services.watchstar.executeCommand("tools.PhysicalPropVersionSearchByPhysicalPropID",
                                                       [
                                                           self._services.getUserName(),
                                                           self.project().id,
                                                           self.id
                                                       ])
        return self._handleMocapPropVersionCreation(data["physicalPropVersions"], self, self.project())

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Mocap Prop Variation object '{}' for '{}' in '{}'".format(self.name, self.prop().name, self.project().name)


class MocapPropVersion(_fileContextBase):
    """
    A mocap prop version object.

    Notes:
        * Populated by JSON data from a Watchstar PhysicalPropVersion object.
    """
    def __init__(self, mocapPropVariation, project, services, dataDict):
        """
        Constructor

        args:
            mocapPropVariation (MocapPropVariation): The prop variation the version is for
            project (Project): The project
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(MocapPropVersion, self).__init__(Const.FilePathTables.PHYSICAL_PROP_VERSION, _baseFilePath, services, dataDict)
        self._project = project
        self._mocapPropVariation = mocapPropVariation

        self._additionalData = {}

        self._getAdditionalData()

    def _getAdditionalData(self):
        addData = self._dataDict.get("additionalData")
        if addData not in ["", None]:
            self._additionalData = json.loads(addData)

    def additionalData(self):
        return self._additionalData

    def setAdditionalData(self, key, value):
        """
        Set an additional data key and its value.

        Notes:
            The value must be pickleable.

        args:
            key (string): the name of the value to set
            value (object): The object to set
        """
        self._additionalData[key] = value
        self.updatePropVersion()

    def removeAdditionalData(self, key):
        """
        Remove an additional Data key and its value.

        args:
            key (string): the name of the value to remove
        """
        self._additionalData.pop(key)
        self.updatePropVersion()

    @property
    def name(self):
        """
        Get the name of the calibration

        return:
            calibration name as a string
        """
        return str(self._dataDict.get("name"))

    def project(self):
        """
        Get the project

        return:
            Project
        """
        return self._project

    def isActive(self):
        """
        If the prop is active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    def propVariation(self):
        """
        Get the prop variation the version is for

        return:
            MocapPropVariation
        """
        return self._mocapPropVariation

    def prop(self):
        """
        Get the mocap prop the version is for

        return:
            MocapProp
        """
        return self._mocapPropVariation.prop()

    def updatePropVersion(self, comment=None, isActive=None):
        """
        Updates the prop version's data in the database.

        Notes:
            * The AdditionalData field is always updated with the self._additionalData value when this method is called.
                * This is consistent with how the other AdditionalData fields are handled.

        args:
            comment (str): The comment associated with the prop version. Default: None - no comment is added.
            isActive (bool): If True, sets the prop version to be active, False sets it to non active. Default: True
        """
        kwargs = []
        if comment is not None:
            kwargs.append("@Comment='{0}'".format(comment))
        if isActive is not None:
            kwargs.append("@IsActive={0}".format(int(bool(isActive))))
        kwargs.append("@AdditionalData='{0}'".format(json.dumps(self._additionalData)))

        data = self._services.watchstar.executeCommand("toolswritepub.UpdatePhysicalPropVersion",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id,
                                                       ],
                                                       kwargList=kwargs,
                                                       )

        propVersion = self._handleMocapPropVersionCreation(data["physicalPropVersions"], self, self.project())[0]
        self._dataDict = propVersion._dataDict

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Mocap Prop Version for '{}' in '{}'".format(self.name, self.project().name)


class GameProp(AssetGameBase):
    """
    A game prop object.

    Notes:
        * Populated by JSON data from a Watchstar Asset3d object with an asset class of "prop".
    """
    def __init__(self, services, project, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(GameProp, self).__init__(services, project, dataDict)

    def path(self):
        """
        Get the game prop path as a string

        notes:
            * Only here to satisfy old api. Use self.fbxGameAssetPath() instead.

        return:
            game prop path as a string
        """
        return self.fbxGameAssetPath()

    def category(self):
        """
        Get the name of the category for the game prop

        notes:
            * Only here to satisfy old api. Use self.getAssetType().name instead.

        return:
            game prop category as a string
        """
        return self.getAssetType().name


class AssociatedCalibrationFile(_baseFilePath):
    """
    Associated File Context Object
    """
    def __init__(self, stageCalibration, services, dataDict):
        """
        Constructor

        args:
            stageCalibration (StageCalibration): The Stage Calibration the file belongs to
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(AssociatedCalibrationFile, self).__init__(Const.FilePathTables.STAGE_CALIBRATION, stageCalibration, None, services, dataDict)
        self._stageCalibration = stageCalibration

    def stageCalibration(self):
        """
        Get the stage calibration the file is from

        return:
            StageCalibration
        """
        return self._stageCalibration

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, AssociatedCalibrationFile) and self.fileId == other.fileId


class StageCalibration(_fileContextBase):
    """
    A Stage Calibration object

    Notes:
        * Populated by JSON data from a Watchstar StageCalibration object.
    """
    def __init__(self, services, dataDict):
        """
        Constructor

        args:
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(StageCalibration, self).__init__(Const.FilePathTables.STAGE_CALIBRATION, AssociatedCalibrationFile, services, dataDict)

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._services.animDataContext.getS(self.id)
        self._dataDict = context.getDataDict()

    @property
    def calibrationID(self):
        """
        Get the ID of the Calibration

        return:
            Calibration ID as a string
        """
        return self._dataDict.get("id")

    @property
    def name(self):
        """
        Get the name of the calibration

        return:
            calibration name as a string
        """
        return str(self._dataDict.get("captureName"))

    def stage(self):
        """
        Get stage the calibration is for

        return:
            A Stage object
        """
        return self._services.animDataContext.getStageFromID(self._dataDict["stageID"])

    def captureDateTime(self):
        """
        Get the date and time the calibration when the calibration was done

        return:
            calibration created time as a string in local time
        """
        return self._convertUtcDbStringToLocalTime(self._dataDict["utcCaptureDateTime"])

    def notes(self):
        """
        Get the notes for the stage calibration

        return:
            string of the notes for the stage calibration
        """
        return self._dataDict.get("notes", "")

    def getAllFiles(self, pathType=None):
        """
        Get all the files associated with the trial as a list

        return:
            List of AssociatedFile objects
        """
        data = self._getFilePaths(pathType=pathType, refresh=False)
        return [AssociatedCalibrationFile(self, self._services, fData) for fData in data]

    def addNewFile(self, fileType, filePath):
        """
        Add a new file to the calibration, given its type and path

        args:
            fileType (settings.FilePathType): The type of the file
            filePath (str): The string P4 Path to the file
        """
        if filePath == "" or filePath is None:
            raise ValueError("Unable to set a file path to an empty string")

        # Without this check, the db query may raise a "THIS PATH ALREADY EXISTS FOR THE SPECIFIED CAPTURE" error.
        filePaths = [associatedFile.rawFilePath() for associatedFile in self.getAllFiles()]
        if filePath in filePaths:
            return

        self._services.watchstar.executeCommand("toolswritepub.AddStageCalibrationFilePath",
                                                [
                                                    self._services.getUserName(),
                                                    self.calibrationID,
                                                    fileType.settingsID,
                                                    filePath,
                                                ])

    def updateCalibration(self, stage=None, stageCalibrationName=None, stageCalibrationDate=None, notes=None):
        """
        Edit the calibration object properties

        kargs:
            stage (Stage): the stage the calibration is used for
            stageCalibrationName (str): the name for the calibration
            stageCalibrationDate (??): The date time object that the calibration was recorded for
            notes (str): any notes for the calibration
        """
        updateDict = {}
        kwargs = []
        if stage is not None:
            kwargs.append("@StageCalibrationID='{0}'".format(stage.stageId))
            updateDict["stageID"] = stage.stageId

        if stageCalibrationName is not None:
            kwargs.append("@StageCalibrationName='{0}'".format(stageCalibrationName))
            updateDict["captureName"] = stageCalibrationName

        if stageCalibrationDate is not None:
            kwargs.append("@StageCalibrationDate='{0}'".format(stageCalibrationDate))
            updateDict["utcCaptureDateTime"] = stageCalibrationDate

        if notes is not None:
            cleanTxt = self._formatText(notes)
            kwargs.append("@Notes='{0}'".format(cleanTxt))
            updateDict["notes"] = stage.stageId

        if len(kwargs) == 0:
            return

        self._services.watchstar.executeCommand("toolswritepub.UpdateStageCalibrationFilePath",
                                                [
                                                    self._services.getUserName(),
                                                    self.fileId,
                                                ],
                                                kwargList=kwargs
                                                )
        self._dataDict.update(updateDict)

    def __str__(self):
        """
        Nicely printable representation of object
        """
        return "Stage Calibration object for '{0}' at '{1}'".format(self.stage().name, self.captureDateTime())

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.calibartionID == other.calibartionID

    def __hash__(self):
        """
        Hash the object
        """
        return self.calibartionID.__hash__()


class CustomMocapTrialList(_dataDictContextBase):
    """
    A Custom Mocap Trial List Object.

    Notes:
        * Populated by JSON data from a Watchstar CustomMocapList object.
    """
    def __init__(self, project, services, dataDict):
        """
        Constructor

        args:
            project (AnimData.Project): The project the list belongs with
            services (AnimData.services.Services): The services for communicating with databases
            dataDict (dict): A nested dict of values from the database
        """
        super(CustomMocapTrialList, self).__init__(services, dataDict)
        self._project = project
        self._commentDict = {}

    def project(self):
        """
        Get the project for the shoot

        return:
            the project (Project) for the shoot
        """
        return self._project

    def getTrials(self):
        """
        Get the project for the shoot

        return:
            the project (Project) for the shoot
        """
        data = self._services.watchstar.executeCommand("tools.CustomMocapListTrialSearch",
                                                       [
                                                           self._services.getUserName(),
                                                           self.id
                                                       ])
        if data is None:
            return []
        trials = self._handleTrialCreation(data, None, None, self.project())
        for trial in trials:
            self._commentDict[trial.captureId] = trial.getDataDict()["customMocapListTrialComment"]
        return trials

    def getCommentForTrial(self, trial):
        return self._commentDict.get(trial.captureId, "")

    def setCommentForTrial(self, trial, comment):
        self._services.watchstar.executeCommand("toolswritepub.UpdateCustomMocapListTrialComment",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                    trial.captureId,
                                                    comment
                                                ])

        self._commentDict[trial.captureId] = comment

    def addTrialToList(self, trial):
        self._services.watchstar.executeCommand("toolswritepub.AddCustomMocapListTrial",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                    trial.captureId
                                                ])

    def removeTrialFromList(self, trial):
        self._services.watchstar.executeCommand("toolswritepub.DeleteCustomMocapListTrial",
                                                [
                                                    self._services.getUserName(),
                                                    self.id,
                                                    trial.captureId
                                                ])

    def clearAllTrialsFromList(self):
        self._services.watchstar.executeCommand("toolswritepub.ClearCustomMocapList",
                                                [
                                                    self._services.getUserName(),
                                                    self.id
                                                ])

    def deleteTrialList(self):
        self._services.watchstar.executeCommand("toolswritepub.DeleteCustomMocapList",
                                                [
                                                    self._services.getUserName(),
                                                    self.id
                                                ])

    def __eq__(self, other):
        """
        Equals method, checks for types and names to check that two contexts are the same
        """
        return isinstance(other, type(self)) and self.id == other.id

    def __hash__(self):
        """
        Hash the object
        """
        return self.id.__hash__()


class HelmetCamRevision(_dataDictContextBase):
    """
    Represents a revision for a physical helmet camera.

    Notes:
        * Populated by JSON data from a Watchstar HelmetCamRevision object.
    """
    def __init__(self, services, dataDict):
        super(HelmetCamRevision, self).__init__(services, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        return str(self._dataDict["helmetCamRevisionName"])

    def isActive(self):
        """
        If the revision is active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    def notes(self):
        """
        Get the notes for the revision

        return:
            str: contents of the notes field
        """
        return self._dataDict.get("notes", "")

    def cameraType(self):
        """
        Get the camera type.

        Returns:
            None
        """
        cameraTypeId = self._dataDict.get("cameraTypeID", -1)
        return self._services.animDataSettingsManager.getCameraTypes(cameraTypeId)


class HelmetCam(_dataDictContextBase):
    """
    Represents a physical helment camera.

    Notes:
        * Populated by JSON data from a Watchstar HelmetCam object.
    """
    SINGLE_CAM = "Single"
    DUAL_CAM = "Dual"

    def __init__(self, services, dataDict):
        super(HelmetCam, self).__init__(services, dataDict)

    @property
    def name(self):
        """
        Reimplemented from _contextBase

        Returns:
            str
        """
        activeRevisions = [rev for rev in self.revisions() if rev.isActive() is True]
        if activeRevisions:
            return activeRevisions[0].name
        return ""

    def isActive(self):
        """
        If the prop is active or not

        return:
            bool
        """
        return self._dataDict["isActive"]

    def numCams(self):
        """
        Get the number of cameras mounted on the helmet.

        Returns:
            str: "Single"|"Dual"
        """
        return self._dataDict["numCams"]

    def color(self):
        return self._dataDict["helmetCamColor"]

    def hexColor(self):
        return self._dataDict["helmetCamHexColor"]

    def location(self):
        """
        Get the location of the helmet cam.

        Returns:
            stage.Location: The location matching the "facilityLocationID" of the helmet camera.
        """
        facilityLocationId = self._dataDict["facilityLocationID"]
        return self._services.animDataContext.getLocationFromID(facilityLocationId)

    def revisions(self):
        return self._handleHelmetCamRevisionCreation(self._dataDict)


class HelmetCamCalibration(_fileContextBase):
    """
    Represents a calibration for a physical helmet camera.

    Notes:
        * Populated by JSON data from a Watchstar HelmetCamCalibration object.
    """
    def __init__(self, services, dataDict):
        super(HelmetCamCalibration, self).__init__(Const.FilePathTables.HELMET_CAM_CALIBRATION, AssociatedHelmetCamCalibrationFile, services, dataDict)

    def _refresh(self):
        """
        Reimplemented from _contextBase
        """
        context = self._services.animDataContext.getHelmetCamCalibrationByID(self.id)
        self._dataDict = context.getDataDict()

    def notes(self):
        """
        Get the notes for the calibration

        return:
            str: contents of the notes field
        """
        return self._dataDict.get("notes", "")

    def calibrationDate(self):
        """
        Get the date for the calibration

        return:
            str: contents of the notes field
        """
        return self._convertUtcDbStringToLocalTime(self._dataDict.get("calibrationDate", ""))

    def getAllFiles(self, pathType=None):
        """
        Get all the files associated with the trial as a list

        return:
            List of AssociatedFile objects
        """
        data = self._getFilePaths(pathType=pathType, refresh=False)
        return [AssociatedHelmetCamCalibrationFile(self, self._services, fData) for fData in data]
