"""
Base Context Model for the lazy loaded contexts
"""
import os

from PySide import QtGui, QtCore

from RS.Core.AnimData import Context
from RS.Core.AnimData._internal import stages as animStages
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem


class LocationModelItem(baseModelItem.BaseModelItem):
    """
    This represents a location
    """

    def __init__(self, location, parent=None):
        """
        Constructor
        """
        super(LocationModelItem, self).__init__(parent=parent)
        self._location = location
        self._populateChildren()

    def name(self):
        return self._location.name

    def findContextIndex(self, context, index):
        """
        Get the current Context of the item

        args:
                context (AnimData Context): The AnimData context item to find
                index (QModelIndex): The parent model index to base the child index off

        return:
                The index of the found context
        """
        modelIdx = QtCore.QModelIndex()
        if self.canFetchMore(modelIdx):
            self.fetchMore(modelIdx)

        for idx, child in enumerate(self.childItems):
            if child.data(modelIdx, QtCore.Qt.UserRole) == context:
                return index.child(idx, 0)
        return modelIdx

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.name()

        elif role == QtCore.Qt.UserRole:
            return self._location

        elif role == QtCore.Qt.UserRole + 1:
            return self

    def _populateChildren(self):
        for stage in self._location.getStages():
            self.appendChild(StageModelItem(stage, parent=self))

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class StageModelItem(baseModelItem.BaseModelItem):
    """
    Depot File Item Model Type

    This represents an actual P4 File
    """

    def __init__(self, stage, parent=None):
        """
        Constructor
        """
        super(StageModelItem, self).__init__(parent=parent)
        self._stage = stage

    def name(self):
        return self._stage.name

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.name()

        elif role == QtCore.Qt.UserRole:
            return self._stage

        elif role == QtCore.Qt.UserRole + 1:
            return self

    def findContextIndex(self, context, index):
        """
        Get the current Context of the item

        args:
                context (AnimData Context): The AnimData context item to find
                index (QModelIndex): The parent model index to base the child index off

        return:
                The index of the found context
        """
        modelIdx = QtCore.QModelIndex()
        if self.canFetchMore(modelIdx):
            self.fetchMore(modelIdx)

        for idx, child in enumerate(self.childItems):
            if child.data(modelIdx, QtCore.Qt.UserRole) == context:
                return index.child(idx, 0)
        return modelIdx

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False


class StageModel(baseModel.BaseModel):
    """
    Base Model Type
    """

    def __init__(self, parent=None, includeLocalLocation=False):
        """
        Constructor

        Args:
            includeLocalLocation (bool): if the local location/stage should be added
        """
        self._includeLocalLocation = includeLocalLocation
        super(StageModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Mocap Stage"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Get the amount of columns in the model

        args:
            parent (QModelIndex): The parent index

        returns:
            Int number of columns in the model
        """
        return len(self.getHeadings())

    def includeLocalLocation(self, value):
        """
        sets if the Local location is included or not

        args:
            value (bool): if the local location/stage should be added
        """
        self._includeLocalLocation = value
        self.reset()

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        for location in Context.animData.getAllLocations(includeLocal=self._includeLocalLocation):
            parent.appendChild(LocationModelItem(location, parent=parent))

    def findStageContextIndex(self, context):
        """
        Get the index of a given location or stage context, if found

        args:
            context (AnimData.Stage/AnimData.Location): The context to find

        returns:
            QModelIndex of the location
        """
        if context is None:
            return QtCore.QModelIndex()
        sameType = False
        # Project
        if isinstance(context, animStages.Location):
            locName = context.name
            sameType = True
        else:
            locName = context.location().name

        locIdx = self.match(self.createIndex(0, 0), 0, locName, 1, QtCore.Qt.MatchExactly)
        if len(locIdx) == 0:
            return QtCore.QModelIndex()
        locIdx = locIdx[0]
        if sameType:
            return locIdx
        loc = self.data(locIdx, QtCore.Qt.UserRole + 1)

        # Shoot
        stageIdx = loc.findContextIndex(context, locIdx)
        if stageIdx is None:
            return QtCore.QModelIndex()

        return stageIdx


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    mainModel = StageModel()
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
