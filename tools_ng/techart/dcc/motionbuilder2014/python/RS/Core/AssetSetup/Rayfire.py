###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AssetSetupRayfire
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Setup for Rayfire
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Core.AssetSetup.Lib as ass
import RS.Utils.Path
import RS.Utils.Scene
import RS.Globals as glo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Clean up Materials & Save
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CleanMaterialsRayfireSetup():

    lDetachList = []
    lRemoveList = []
    lRayFireList = []
    lRayFireMeshList = []

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True) 

    for iSceneItem in lSceneList:
        if 'dummy' in iSceneItem.Name.lower():
            lDummy = iSceneItem
            lDummy.Name = "Dummy01"
            break 

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lFilePath = FBApplication().FBXFileName
    lPointNull = FBFindModelByLabelName(lDummy.Children[0].Name)
    
    RS.Utils.Scene.GetChildren(lPointNull, lRayFireList)
    #lRayFireList.extend(eva.gChildList)
    #del eva.gChildList[:] 

    for iMaterial in glo.gMaterials:
        lDetachList.append(iMaterial)
        lRemoveList.append(iMaterial)

    for iComponent in lDetachList:
        for iNull in lRayFireList:
             iNull.DisconnectSrc(iNull)
    
    for iComponent in lRemoveList:
        if iComponent.Name != "DefaultMaterial":
            iComponent.FBDelete()
            
    lRootNode = lDummy.Children[0]

    lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "Rayfire" 
            
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Setup Rayfire
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupRayfire(pControl, pEvent):

    lRayFireList = []

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lFilePath = FBApplication().FBXFileName

    rs_CleanMaterialsRayfireSetup()

    lDummy = FBFindModelByLabelName("Dummy01")
    lPointNull = FBFindModelByLabelName(lDummy.Children[0].Name)

    RS.Utils.Scene.GetChildren(lDummy, lRayFireList)
    #lRayFireList.extend(eva.gChildList)
    #del eva.gChildList[:]

    for iCamera in glo.gCameras:
        if iCamera.Name == "Producer Perspective":
            iCamera.PropertyList.Find("Far Plane").Data = 400000
    
    lRayfireGroup = FBGroup("Rayfire")
    lRayfireGroup.ConnectSrc(lDummy)
    
    lRedMaterial = FBMaterial("Rayfire_Material")
    lRedMaterial.Diffuse = FBColor(1,0,0.1)

    for iNull in lRayFireList:
        iNull.Materials.append(lRedMaterial)
        iNull.ShadingMode = FBModelShadingMode.kFBModelShadingAll
        lRayfireGroup.ConnectSrc(iNull)

    for iTake in glo.gTakes:
        if iTake.Name == "Take 001":
            iTake.Name = lFileName

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)
    
    ass.rs_AssetSetupCustomProperties()

    lSplitPathName = lFilePath.split(".")
    lNewFilePath = lSplitPathName[0] + "_animation.FBX"
    FBApplication().FileSave(lFilePath)
    FBApplication().FileSave(lNewFilePath)
    FBApplication().FileOpen(lFilePath)

    FBMessageBox("Rayfire Setup", lFileName + " has now been setup", "Ok")
    
#rs_AssetSetupRayfire(None, None)
