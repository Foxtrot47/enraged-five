###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Script Name: rs_AssetSetupCoreFunctions
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Core Functions for the Setup of Assets
##
## Rules: Definitions: Prefixed with "rs_"
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

from pyfbsdk import *

import re
import os

import RS.Config
import RS.Utils.Path
import RS.Utils.Scene

import RS.Globals as glo
import RS.Utils.Collections
import RS.Core.Animation.Lib
import RS.Utils.Creation as cre
import RS.Utils.Scene.Component

MODEL_DICTIONARY = {}


"""
Methods that support our current AssetSetup process.
"""

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## get max properties
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################
def GetGeoDummy():
    '''
    Gather property data from the max custom properties

    Returns:
        String, String : GEODUMMYNODE (bool if the FBModel is the geodummy)
                         REVISION (revision of the max file used in the fbx)
    '''
    # go through hierarchy and find the max property 'GEODUMMYNODE'
    maxProperty = None
    geoDummyNode = None
    for component in RS.Globals.Components:
        maxProperty = component.PropertyList.Find('UDP3DSMAX')
        if maxProperty and 'GEODUMMYNODE' in maxProperty.Data:
            geoDummyNode = component
            break

    if maxProperty == None:
        return None, None

    geoDummy = None
    dataList = maxProperty.Data.split('\n')
    for data in dataList:
        if 'GEODUMMYNODE' in data:
            geoDummy = data.replace('\r', '',)

    geoDummyString = geoDummy.split(' ')[-1]

    return geoDummyString, geoDummyNode

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Create Variables
##                         - Get Important Variables And Return List
##
## HISTORY:
##      kyle.hansen@rockstarsandiego.com  - Changed the searching of mover and dummy to only pickup the first item found.
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupVariables():
    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lAssetVariableList = []

    project = str( RS.Config.Project.Name ).lower()


    if project == 'gta5':
        lDummy = None
        dummyList = []
        for i in RS.Globals.Components:
            if 'dummy' in i.Name.lower():
                dummyList.append( i )

        if dummyList:
            for i in dummyList:
                try:
                    if 'mover' in i.Children[0].Name.lower():
                        lDummy = i
                        lDummy.Name = 'Dummy01'
                except:
                    pass

        if lDummy == None:
            FBMessageBox( 'R* Error', 'Cannot Find Dummy Node.', 'Ok' )


    # this method was crashing setup on GTA5, so making it project specific.
    else:
        #== Changed this to pickup the first item it finds rather than the last...  khansen
        lDummy = None
        for iRoot in lSceneList:
            if 'dummy' in iRoot.Name.lower():

                # Hayes: Ensure that the dummy we are searching for has a mover node underneath.  There
                # can be multiple top level nodes with the name Dummy in it.
                if len( iRoot.Children ) > 0:
                    for child in iRoot.Children:
                        if 'mover' in child.Name.lower():
                            lDummy = iRoot
                            lDummy.Name = "Dummy01"
                            break

    #== Changed this to pickup the first item it finds rather than the last...   khansen
    lMover = None
    for iScene in lSceneList:
        if 'mover' in iScene.Name.lower():
            if iScene.Parent == lDummy and lMover == None:
                lMover = iScene
                lMover.Name = "mover"
                break

    lGeo = None
    for iScene in lSceneList:
        # check if geo dummy is named the same as the filename
        if lFileName.lower() == iScene.Name.lower():
            lGeo = iScene
            break
        # check if geo dummy is named the same as the filename, plus '_skin' (vehicles)
        elif iScene.Name.lower() == lFileName.lower() + '_skin':
            lGeo = iScene
            break
    # if we still cannot find the geo, try searching via the max properties
    # (characters only have this property)
    geoDummyString, geoDummyNode = GetGeoDummy()
    if lGeo == None and geoDummyString == 'TRUE' and geoDummyNode:
        lGeo = geoDummyNode
    elif RS.Utils.Scene.FindModelByName('Geometry') and isinstance(RS.Utils.Scene.FindModelByName('Geometry'), FBModel):
        lGeo = RS.Utils.Scene.FindModelByName('Geometry')

    if lDummy:
        if lMover:
            if lGeo:

                lAssetVariableList.append(lDummy)
                lAssetVariableList.append(lMover)
                lAssetVariableList.append(lGeo)

            else:
                FBMessageBox('Error', 'Scene is missing a Geometry Node, or it is spelled incorrectly. Script aborted.', 'Ok')
        else:
            FBMessageBox('Error', 'Scene is missing a mover Node, or it is spelled incorrectly. Script aborted.', 'Ok')
    else:
        FBMessageBox('Error', 'Scene is missing a Dummy Node, or it is spelled incorrectly. Script aborted.', 'Ok')

    return lAssetVariableList

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Delete Unwanted Items
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupDeleteItems():

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lDeleteList = []

    lUnwantedList = ['collision', 'colbox', 'particle', 'col_box', 'colmesh', 'box001', 'col_mesh',
                     'constraint', 'skindata_', 'lod_camera', 'target001']

    for iSceneItem in lSceneList:
        for iUnwanted in lUnwantedList:
            name = iSceneItem.Name.lower()
            # check if the
            if iUnwanted in name:
                if iUnwanted != 'constraint':
                    lDeleteList.append(iSceneItem)
                    continue
                # if the null has 'constraint' int he name and has no children, add to delete
                # list. If constraint item has children it is probably a legit vehicle bone.
                elif len(iSceneItem.Children) == 0:
                    lDeleteList.append(iSceneItem)

    # This is to get rid of the HIK Solvers
    for iSceneSolver in glo.gConstraintSlovers:
        lDeleteList.append(iSceneSolver)


    lDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lDeleteList)

    for iDelete in lDeleteList:
        if iDelete:
            try:
                iDelete.FBDelete()
            except:
                None




###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - ToyBox Marker
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_CreateToyBoxMarker():

    assetSetupVars = rs_AssetSetupVariables()
    lDummy = assetSetupVars[0]
    lMover = assetSetupVars[1]


    lToyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
    lToyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    lToyBoxMarker.Size = 500
    lToyBoxMarker.Color = FBColor(1, 0, 1)
    lToyBoxMarker.Parent = lDummy
    lToyBoxMarker.Translation.Data = lMover.Translation.Data
    lToyBoxMarker.Rotation.Data = lMover.Rotation.Data

    lTag = lToyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Contacts"

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Mover Control
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupMoverControl():

    lDummy = rs_AssetSetupVariables()[0]
    lMover = rs_AssetSetupVariables()[1]
    lGeo = rs_AssetSetupVariables()[2]
    lSkelRoot = lMover.Children[0]

    lMover.Selected = True
    lMover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lMover.Selected = False
    lMover.PropertyList.Find("Visibility").Data = True

    lMoverMarker = FBModelMarker("mover_Control")
    lMoverMarker.Show = True
    lMoverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    lMoverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)

    lVector = FBVector3d()
    lMover.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
    lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
    lMover.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    FBSystem().Scene.Evaluate()

    lMoverMarker.Parent = lSkelRoot

    lRelationCon = FBConstraintRelation('mover_Control')
    lMoverMarkerSender = lRelationCon.SetAsSource(lMoverMarker)
    lRelationCon.SetBoxPosition(lMoverMarkerSender, 0, 0)
    lMoverReceive = lRelationCon.ConstrainObject(lMover)
    lRelationCon.SetBoxPosition(lMoverReceive, 300, 0)

    RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Translation', lMoverReceive, 'Translation')
    RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Rotation', lMoverReceive, 'Rotation')

    lRelationCon.Active = False

    return lRelationCon

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Folder Create
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupFolderTidy():

    lMaterials = FBSystem().Scene.Materials
    lMaterialFolderStr = "Materials"
    lTextures = FBSystem().Scene.Textures
    lTexturesFolderStr = "Textures"
    lConstraints = FBSystem().Scene.Constraints
    lConstraintFolderStr = "Constraints"
    lShaders = FBSystem().Scene.Shaders
    lShaderFolderStr = "Shaders"
    lVideoClips = FBSystem().Scene.VideoClips

    if len(lVideoClips) > 0:
        lVideoFolder = FBFolder ("Videos", lVideoClips[0])
        for lVideo in lVideoClips:
            lVideoFolder.ConnectSrc(lVideo)

    cre.rs_CreateFolderAddComponents(lMaterials, lMaterialFolderStr)
    cre.rs_CreateFolderAddComponents(lTextures, lTexturesFolderStr)
    cre.rs_CreateFolderAddComponents(lConstraints, lConstraintFolderStr)
    cre.rs_CreateFolderAddComponents(lShaders, lShaderFolderStr)

    for lMat in lMaterials:
        if lMat:
            if "_Contact" in lMat.Name:
                None
            else:
                lMat.Ambient = FBColor(1,1,1)
                lMat.Diffuse = FBColor(1,1,1)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Custom Properties
##                         - Properties for use with the Referencing System
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupCustomProperties():

    lGeoList = []
    lBoneList = []
    lContactsList = []
    lRayfireList = []
    lSetGeoList = []

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lFilePath = os.path.dirname(FBApplication().FBXFileName) + '\\' + lFileName + '.fbx'

    lScene = FBSystem().Scene

    lDummy = None
    for iRoot in glo.gRoot.Children:
        if 'dummy' in iRoot.Name.lower():
            lDummy = iRoot

    for iRoot in glo.gRoot.Children:
        if 'setroot' in iRoot.Name.lower():
            lDummy = iRoot
            RS.Utils.Scene.GetChildren(lDummy, lSetGeoList, "", False)
        #lSetGeoList.extend(eva.gChildList)
        #del eva.gChildList[:]

    lMover = FBFindModelByLabelName('mover')
    if lMover:
        None
    else:
        lMover = None

    lGeo = None

    for iSceneItem in lSceneList:
        if iSceneItem.Name.lower() == lFileName.lower():
            lGeo = iSceneItem

    if FBFindModelByLabelName("Geometry"):
        lGeo = FBFindModelByLabelName("Geometry")

    if FBFindModelByLabelName(lFileName.lower() + '_skin'):
        lGeo = FBFindModelByLabelName(lFileName.lower() + '_skin')

    lRayfire = None
    lRayfire = FBFindModelByLabelName(lFileName + '_rayfire')

    if lRayfire != None:
        RS.Utils.Scene.GetChildren(lRayfire, lRayfireList)
    #lRayfireList.extend(eva.gChildList)
    #del eva.gChildList[:]

    lGeoContact = FBFindModelByLabelName(lFileName + "_Control")

    if lMover:
        lExportable = lMover.Children[0]
    else:
        lExportable = lDummy.Children[0]

    if lGeoContact == None:
        RS.Utils.Scene.GetChildren(lExportable, lBoneList)
    #lBoneList.extend(eva.gChildList)
    #del eva.gChildList[:]

    if lGeo != None:
        RS.Utils.Scene.GetChildren(lGeo, lGeoList)
    #lGeoList.extend(eva.gChildList)
    #del eva.gChildList[:]

    for iSceneItem in lSceneList:
        if "_Control" in iSceneItem.Name:
            lContactsList.append(iSceneItem)
        if "_Contact" in iSceneItem.Name:
            lContactsList.append(iSceneItem)

    if lDummy != None:
        lTag = lDummy.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Dummy"

    if lMover != None:
        lTag = lMover.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Mover"

    if len(lSetGeoList) >= 1:
        for iSetGeo in lSetGeoList:
            lTag = iSetGeo.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Mesh"

    if len(lGeoList) >= 1:
        for iGeo in lGeoList:
            lTag = iGeo.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Mesh"

    if len(lRayfireList) >= 1:
        for iBone in lBoneList:
            if lGeo.Name == iBone.Name:
                lBoneList.remove(iBone)
            if iBone.Name == 'mover_Control':
                lBoneList.remove(iBone)

    if len(lRayfireList) >= 1:
        for iRayfire in lRayfireList:
            lTag = iRayfire.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Rayfire"

    if len(lBoneList) >= 1:
        for iBone in lBoneList:
            lTag = iBone.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Bone"

    if len(lContactsList) >= 1:
        for iContacts in lContactsList:
            lTag = iContacts.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"

    lPathProp = lDummy.PropertyCreate('Asset_Path', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lPathProp.Data = "%s"%lFilePath

    lDos = os.popen('p4 fstat %s'%lFilePath)
    for x in lDos:
        if "haveRev" in x:
            lSplit = x.split(" ")
            lVersion = lSplit[-1]
            lVersionNum = int(lVersion) + 1
            lVersionProp = lDummy.PropertyCreate('P4_Version', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lVersionProp.Data = "%s" %lVersionNum

    lVersionProp = lDummy.PropertyList.Find("P4_Version")
    if lVersionProp:
        None
    else:
        lVersionProp = lDummy.PropertyCreate('P4_Version', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lVersionProp.Data = "1"

    def rs_MetaTaggingNavigator(pSceneItem, prs_Name):
        if len(pSceneItem) != 0:
            for iItem in pSceneItem:
                lTag = iItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                if lTag:
                    lTag.Data = prs_Name

    lActorFaces = "rs_ActorFaces"
    rs_MetaTaggingNavigator(lScene.ActorFaces, lActorFaces)

    lActors = "rs_Actors"
    rs_MetaTaggingNavigator(lScene.Actors, lActors)

    lAudioClips = "rs_AudioClips"
    rs_MetaTaggingNavigator(lScene.AudioClips, lAudioClips)

    lCameras = "rs_Cameras"
    rs_MetaTaggingNavigator(lScene.Cameras, lCameras)

    lCharacterExt = "rs_CharacterExtensions"
    rs_MetaTaggingNavigator(lScene.CharacterExtensions, lCharacterExt)

    lCharacterFaces = "rs_CharacterFaces"
    rs_MetaTaggingNavigator(lScene.CharacterFaces, lCharacterFaces)

    lCharacterPoses = "rs_CharacterPoses"
    rs_MetaTaggingNavigator(lScene.CharacterPoses, lCharacterPoses)

    lConstraintSolvers = "rs_ConstraintSolvers"
    rs_MetaTaggingNavigator(lScene.ConstraintSolvers, lConstraintSolvers)

    lControlSets = "rs_ControlSets"
    rs_MetaTaggingNavigator(lScene.ControlSets, lControlSets)

    lDevices = "rs_Devices"
    rs_MetaTaggingNavigator(lScene.Devices, lDevices)

    lFolders = "rs_Folders"
    rs_MetaTaggingNavigator(lScene.Folders, lFolders)

    lGroups = "rs_Groups"
    rs_MetaTaggingNavigator(lScene.Groups, lGroups)

    lHandle = "rs_Handles"
    rs_MetaTaggingNavigator(lScene.Handles, lHandle)

    lLights = "rs_Lights"
    rs_MetaTaggingNavigator(lScene.Lights, lLights)

    lMarkerSets = "rs_MarkerSets"
    rs_MetaTaggingNavigator(lScene.MarkerSets, lMarkerSets)

    lMaterials = "rs_Materials"
    rs_MetaTaggingNavigator(lScene.Materials, lMaterials)

    lNotes = "rs_Notes"
    rs_MetaTaggingNavigator(lScene.Notes, lNotes)

    lObjectPoses = "rs_ObjectPoses"
    rs_MetaTaggingNavigator(lScene.ObjectPoses, lObjectPoses)

    lPhysicalProperties = "rs_PhysicalProperties"
    rs_MetaTaggingNavigator(lScene.PhysicalProperties, lPhysicalProperties)

    lPoses = "rs_Poses"
    rs_MetaTaggingNavigator(lScene.Poses, lPoses)

    lShaders = "rs_Shaders"
    rs_MetaTaggingNavigator(lScene.Shaders, lShaders)

    lTakes = "rs_Takes"
    rs_MetaTaggingNavigator(lScene.Takes, lTakes)

    lTextures = "rs_Textures"
    rs_MetaTaggingNavigator(lScene.Textures, lTextures)

    lUserObjects = "rs_UserObjects"
    rs_MetaTaggingNavigator(lScene.UserObjects, lUserObjects)

    lVideoClips = "rs_VideoClips"
    rs_MetaTaggingNavigator(lScene.VideoClips, lVideoClips)

    if len(lScene.Constraints) != 0:
        for iConstraint in lScene.Constraints:
            if iConstraint != None:
                if iConstraint.ClassName() != 'FBCharacter':
                    lTag = iConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                    if lTag:
                        lTag.Data = "rs_Constraints"

"""
Classes and methods that support the Parser package
"""

class MissingAssetsError(Exception):
    """ Custom exception handle for missing assets in the scene."""


def GetDummyMoverGeometryAssets(raiseException=False):
    """
    Retrieves dummy , mover and the null that holds the character's geometry in the scene if available.

    Arguments:
        raiseException: boolean; should an exception be raised if not all

    Return: list[FBModelNull(), FBModelNull(), FBModel()]
    """
    lSceneList = Models()
    lFileName = RS.Utils.Scene.GetSceneName()

    project = str( RS.Config.Project.Name ).lower()
    componentList = [lSceneList, RS.Globals.Components][project in 'gta5']

    #Get the Dummy
    lDummy = None
    lMover = None

    #We are using a while loop to avoid using an if statement in a for loop
    counter = 0
    while not lDummy and counter < len(componentList):

        # Hayes: Ensure that the dummy we are searching for has a mover node underneath.  There
        # can be multiple top level nodes with the name Dummy in it.
        eachComponent = componentList[counter]

        #We use getattr to make sure we always get an empty list incase we get an object without the Children property
        for child in getattr(eachComponent, "Children", []):
            if 'mover' in child.Name.lower():
                lDummy = eachComponent
                lDummy.Name = "Dummy01"
                lMover = child

        counter += 1

    #Find the geo model
    lGeo = GetGeometryAsset(lFileName, lDummy)

    #Build warning message
    warningFormat = 'Scene is missing a {} node, or it is spelled incorrectly.\n'
    warning = [ warningFormat.format(["Dummy", "mover", "Geometry"][eachNum])
                for eachNum, each in enumerate((lDummy, lMover, lGeo)) if not each]
    if warning:
        FBMessageBox('Error', "".join(warning), 'Ok')

    if warning and raiseException:
        raise MissingAssetsError(warning)

    return lDummy, lMover, lGeo


def GetGeometryAsset(filename=None, dummy=None):
    """
    Returns the Geometry ModelNull in the scene.

    Arguments:
        filename : name of the scene
        dummy: FBModelNull(); the dummy node in the scene that holds the mover and the character rig
    """
    geometry = None

    if not filename:
        filename = RS.Utils.Scene.GetSceneName()

    if not dummy:
        dummy = FBFindModelByLabelName("Dummy01")

    for model in Models():

        if filename.lower() == model.Name.lower() \
        or model.Name == '{}_skin'.format(filename) \
        or model.Name == "Geometry" and dummy and model.Parent == dummy:
            geometry = model
            break

    return geometry


def Plot(component, properties=["Translation","Rotation"], frame=-100, children=True):
    """
    Sets a key on the properties provided to the function. If recursive is set true,
    keys are set on all the children components as well.

    Arguments:
        component: Any object that is considered a model by MB (FBModel(), FBModelNull, FBLight, etc.)
        properties: list[string, etc.]; any property that you want to have keyed.
        frame: int; frame number to create keys on
        children: boolen; key properties of all children components as well
    """
    RS.Core.Animation.Lib.PlotOnFrame(component, properties, frame, children)

def PlotAnimation():
    """
    Plots animation from all FBCharacters in the scene to the skeletons they are associated with
    """
    plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton

    plotOptions = FBPlotOptions()
    plotOptions.PlotAllTakes = False
    plotOptions.PlotOnFrame = True
    #plotOptions.PlotPeriod = FBTime(0, 0, 0, 1)
    plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
    plotOptions.UseConstantKeyReducer = False
    plotOptions.ConstantKeyReducerKeepOneKey = False
    plotOptions.PlotTranslationOnRootOnly = True

    for char in FBSystem().Scene.Characters:
        char.PlotAnimation(plotWhere, plotOptions)


def ClearAnimation(model, properties=["Translation"]):
    """
    Clears the animation on the properties passed to this function

    Arguments:
        model : FBComponent() or object that inherits FBComponent; object to clear keys on
        properties: list[string, etc.]; name of the properties with AnimationNodes to clear
    """
    if isinstance(model, basestring):
        model = RS.Utils.Scene.FindModelByName(model)

    for property in properties:
        animationNode = getattr(model, property).GetAnimationNode()

        if not animationNode: continue

        for node in animationNode.Nodes:
            node.FCurve.EditClear()


def PerforceRevisionNumber():
    """
    Gets the perforce revision number of the current file. If the file isn't on perforce, 1 is returne

    return int
    """
    lDos = os.popen('p4 fstat %s'% FBApplication().FBXFileName)
    lVersionNum = 1
    for x in lDos:
        if "haveRev" in x:
            lSplit = x.split(" ")
            lVersion = lSplit[-1]
            lVersionNum = int(lVersion) + 1
            break
    return lVersionNum


def CreateReferenceEditorProperties():
    """
    Creates properties that the reference editor uses for importing characters
    """
    filename = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    filepath, extension = os.path.splitext(FBApplication().FBXFileName)
    filepath = "{}{}".format(filepath, extension.lower())

    #Move to XML ? We can keep this here for now until the Reference Editor is reworked
    propertyTypesDictionary = {
        "ActorFaces":   {},
        "Actors":       {},
        "AudioClips":   {},
        "Cameras":      {},
        "CharacterExtensions": {},
        "CharacterFaces": {},
        "CharacterPoses": {},
        "Contacts"      : {
            "model" : {"names" : [FBSystem().Scene.RootModel.Name],
                       "filterByName" : ["_Control", "_Contact"], "includeChildren":True}
        },
        "Constraints":   {
            "filterOutByType":["FBCharacter", "FBCharacterSolver"]
        },
        "ConstraintSolvers": {},
        "ControlSets":  {},
        "Devices":      {},
        "Dummy"  :      {
            "model": {"names": ["setroot", "dummy[0-9]+"], "ignoreCase": True},
            "Asset_Path": filepath, "P4_Version": str(PerforceRevisionNumber()),
            },
        "Folders":      {},
        "Groups":       {},
        "Handles":      {},
        "Lights":       {},
        "MarkerSets":   {},
        "Materials":    {},
        "Mesh" :        {
            "model": {
                "names": ["{}_skin".format(filename), "Geometry", filename],
                "includeChildren" : True, "ignoreCase": True}
        },
        "ModelSkeletons": { "rs_Type": "rs_Bones", "filterOutByName": ["mover_Control"]},
        "Mover":        {
            "model": {"names": "mover"}
        },
        "Notes":        {},
        "ObjectPoses":  {},
        "PhysicalProperties": {},
        "Poses":        {},
        "Rayfire":      {
            "model": {"names": "{}_rayfire".format(filename),
                      "filterOutByName" : ["mover_Control"], "ignoreCase": True}
        },
        "Set":          {
            "rs_Type": "rs_Mesh",
            "model": {"names": "setroot", "includeChildren": True}
        },
        "Shaders":      {},
        "Takes":        {},
        "Textures":     {},
        "UserObjects":  {},
        "VideoClips":   {}
    }


    for eachType, keywords in propertyTypesDictionary.iteritems():

        modelKeywords = keywords.pop("model", {})

        keywords.setdefault("rs_Type", "rs_{}".format(eachType))
        SetMetaTags(getattr(FBSystem().Scene, eachType, ModelComponents(**modelKeywords)), **keywords)


def SetProperties(component, force=False, **properties):
    """
    Sets values of properties to the given component. Properties and values are determined by the keyword
    arguments passed into this method. If the property doesn't exist and force is set, a property is created
    to set the data too.

    Arguments:
        component : FBComponent() or any object that inherits FBComponent; component to add properties too
        force : boolean; Create a new property to set data too if the property doesn't exist.
        **properties : keyword arguments; the property and values to set on the component
    """
    return RS.Utils.Scene.Component.SetProperties(component, force, **properties)


def SetMetaTags(componentList, filterOutByType=[],  filterOutByName=[], filterByType=[],filterByName=[],**properties):
    """
    Creates/sets properties for all the components in the passed list.

    Arguments:
        componentList: list[FBComponents, etc.]; list of components to add/set properties.
        filterByType: list[string, etc.]; filters the children returned to those that do match the class type in
                this list.
        filterByName: list[string, etc.]; filters the children returned to those that do match the strings in
                this list. Strings in here can have regular expression syntax.
        filterOutByType: list[string, etc.]; filters the children returned to those that don't match the class type in
                this list.
        filterOutByName: list[string, etc.]; filters the children returned to those that don't match the strings in
                this list. Strings in here can have regular expression syntax.
        **properties : keyword arguments; the property and values to set on the component
    """

    for eachComponent in componentList:
        skip = False

        for counter, eachFilter in enumerate([filterByType, filterOutByType, filterByName, filterOutByName]):
            isTypeFilter = counter / 2
            isOutFilter = counter % 2

            if isTypeFilter:
                filterString = "".join(["{}|".format(each) for each in eachFilter])[:-1]
                skip = re.search(filterString, eachComponent.Name)
            else:
                skip = eachComponent.__class__.__name__ in eachFilter

            skip = (skip == bool(isOutFilter)) and eachFilter
            if skip: break

        if skip: continue

        SetProperties(eachComponent, True, **properties)


def ModelComponents(names=[], includeChildren=False, filterByName=[], filterOutByName=[], ignoreCase=False):
    """
    Gets a list of all the components that are model types (FBModel, FBModelNull, FBModelSkeleton, etc.)
    in the scene that match the given parameters.

    Arguments:
        names : string or list[string, ect.]; name or names of the component you are trying to find.
                accepts regular expression syntax
        includeChildren: boolean; returns all the children of the component
        filterByName: list[string, etc.]; filters the children returned to those that match the strings in
                this list. Strings in here can have regular expression syntax.
        filterOutByName: list[string, etc.]; filters the children returned to those that don't match the strings in
                this list. Strings in here can have regular expression syntax.
        ignoreCase: boolean; if case should be ignored when matching names

    returns list[FBComponents(), etc.]
    """

    component = None

    if not isinstance(names, list):
        names = [names]

    #componentName accepts multiple values, once a component is matched to that name it stops

    for eachName in names:
        component = GetModelByExpression(eachName, ignoreCase=ignoreCase)
        if component: break

    if not component: return []

    componentList = [component]
    ignoreCase = [re.M, re.I][ignoreCase]

    #Get Children
    if includeChildren:
        filterStringList = ["{}|".format(each) for each in filterByName]
        filterString = "".join(filterStringList)[:-1]
        componentList += RS.Utils.Scene.GetChildrenRegex(component, filterString, ignoreCase)

    if filterOutByName:
        filterStringList = ["{}|".format(each) for each in filterOutByName]
        filterString = "".join(filterStringList)[:-1]
        filterMethod = re.compile(filterString,[0, re.I][ignoreCase]).search
        componentList = filter(lambda eachModel: not filterMethod(eachModel.Name), ignoreCase)

    return componentList


def Models():
    """
    Returns a list of all the model components in the motion builder scene.
    Basically the property that FBScene should have but lacks.

    return FBModelList()
    """

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)
    return lSceneList


def ModelsDictionary(modelList=None):
    """
    Converts a list of models into a dictionary and adds 'regularExpression' with all
    the names concatenated into a single string; each name is separated by a semicolon (;)

    Arguments:
        modelList = list; list of FBModel objects

    return dictionary
    """
    if not modelList:
        modelList = Models()

    global MODEL_DICTIONARY
    MODEL_DICTIONARY = {each.Name : each for each in modelList}

    stringList = ["{};".format(each) for each in MODEL_DICTIONARY
                  if each is not "__lowercaseKeys"][:-1]

    MODEL_DICTIONARY["regularExpression"] = "".join(stringList)
    return MODEL_DICTIONARY


def GetModelByExpression(name, ignoreCase=True):
    """
    Gets a model based on its name.
    The name supports regular expression syntax and if multiple matches are found, then only the first object is
    returned.

    Arguments:
        name: string; name of the model that you want to retrieve. Supports regular expression syntax.
        ignoreCase: boolean; if case should be ignored when matching names

    return FBModel()
    """

    ModelsDictionary()

    ignoreCase = [re.I] * int(ignoreCase)

    result = re.findall(name, MODEL_DICTIONARY["regularExpression"], *ignoreCase)

    if result: return MODEL_DICTIONARY[result[0]]


def GetConfigFilePath(*paths):
    """
    Builds the path to config files in the techart\etc\config\characters\skeleton directory

    Arguments:
        *paths = list[string, etc.]; paths that you want to add to the end of techart\etc\config\characters\skeleton

    return string
    """
    return os.path.join(RS.Config.Tool.Path.TechArt, r"etc\config\characters\skeleton", *paths)


def GetConfigSkelFilePaths(directoryPath):
    """
    Gets the skel.fatProperty files that live in the characters/skeleton directory of
    the setup xml passed to this file. These skel.fatProperty files do not live in the
    metadata folder because they only contain motion builder relevant information.

    Arguments:
        directoryPath = string; path to the directory you want to check.
                        File search is from the bottom up, so you want to pass in the bottom most directory
                        you want to start the search from.

    Return:
        list[strings, etc.]; paths to skel.fatProperty files
    """
    propertyFiles = []

    currentPath = directoryPath
    currentFile = "current"

    while not currentPath.endswith(r"characters\skeleton") and currentFile:
        for filename in os.listdir(currentPath):
            if filename.endswith("skel.fatProperty"):
                propertyFiles.append(os.path.join(currentPath, filename))
        currentPath, currentFile = os.path.split(currentPath)

    #We are getting the files from the bottom up, so naturally we want to reverse the list
    #so we read the properties in the right order, as the properties of the last xml will always
    #overwrite the properties of the previous xmls for the properties they share.

    propertyFiles.reverse()
    return propertyFiles


def CreateMaxFigPoseTake():
    """ CREATES THE MAXFIGPOSE (POSE THAT COMES FROM MAX) AND STANCEPOSE TAKES (T-POSE FOR MOCAP) """
    #Own Function
    #FBSystem().CurrentTake.ClearAllProperties(False)
    FBSystem().Scene.Takes[0].Name = 'MaxFigPose'
    FBSystem().Scene.Takes[0].CopyTake ('MBStancePose')
    FBSystem().CurrentTake = FBSystem().Scene.Takes[1]


def DeleteLODMeshes():
    """ FIND AND DELETE LOD MESHES. """
    lLODList = ([], [])

    for eachModel in Models():
        if not eachModel.Name: continue
        itemName = eachModel.Name.lower()
        nameCheck = ('-l' in itemName, '-l2' in itemName)
        trueCount = nameCheck.count(True)

        if True in nameCheck and not lLODList[trueCount-1]:
            RS.Utils.Scene.GetChildren(eachModel, lLODList[trueCount-1])

    for lLOD in lLODList[0] + lLODList[1]:
        lLOD.FBDelete()

def AssetSetupDeleteItems():
    lSceneList = Models()

    lUnwantedList = ['collision', 'colbox', 'particle', 'col_box', 'colmesh', 'box001', 'col_mesh',
                     'constraint', 'skindata_', 'lod_camera', 'target001']

    unwantedExpression = "".join(["{}|".format(each) for each in lUnwantedList])[:-1]
    unwantedExpression = re.compile(unwantedExpression, re.I).search
    for iSceneItem in lSceneList:
        itemName = iSceneItem.Name
        if not itemName: itemName = ""

        if unwantedExpression(itemName)\
        or iSceneItem in glo.gConstraintSlovers\
        or (iSceneItem.ClassName() == "FBGroup" and not iSceneItem.GetSrcCount()):
            iSceneItem.FBDelete()

def ScaleDownNulls():
    for component in glo.Components:
        if isinstance(component, FBModelNull):
            component.Size = 0