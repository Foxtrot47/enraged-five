from pyfbsdk import *

import RS.Globals



def RemovePyScripts():
    scriptDeleteList = []
    # find py script in scene
    for i in RS.Globals.gComponents:
        if '.py' in i.LongName.lower() and i.ClassName() == 'FBComponent':
            scriptDeleteList.append( i )
    # delete found scripts    
    for i in scriptDeleteList:
        try:
            i.FBDelete()
        except:
            pass
    
    # delete empty folders from scene
    for folder in FBSystem().Scene.Folders:
        for item in folder.Items:
            if isinstance(item,FBCharacter):
                folder.Items.remove(item)
    
        lFolderList = []
        for lFolder in RS.Globals.gFolders:    
            if len(lFolder.Items) == 0:
                lFolderList.append(lFolder)
                
        map( FBComponent.FBDelete, lFolderList )
        

RemovePyScripts()