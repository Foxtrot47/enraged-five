from pyfbsdk import *
import RS.Utils.Scene.AnimationNode
import RS.Globals as glo
import RS.Perforce
import RS.Utils.Scene.Take
from ctypes import *
from pyfbsdk_additions import *
import os


SCENE = FBSystem().Scene


def rs_CharacterExtension(lCharacter):

    lExtensionCreated = False
    lIKRHand = RS.Utils.Scene.FindModelByName('IK_R_Hand')
    lCHLHand = RS.Utils.Scene.FindModelByName('CH_L_Hand')         
    
    lExtList = [lIKRHand, lCHLHand]
    
    lCharExtensions = lCharacter.CharacterExtensions
    for lExtension in lCharExtensions:
        if(lExtension.Name=='Char_Extention'):
            FBConnect(lIKRHand,lExtension)
            FBConnect(lCHLHand,lExtension)
            lExtensionCreated = True
    if lExtensionCreated == False:
        FBConnect(lIKRHand,lCharExtensions[0])
        FBConnect(lCHLHand,lCharExtensions[0])        
       
    FBSystem().Scene.Evaluate()
    return lCharExtensions

def getNodeByName(useNameSpace, name, nameSpace, modelsOnly):
    returnNode = None

    cl = FBComponentList()
    searchString = name
    if nameSpace != None:
        searchString = nameSpace+":"+name
        
    FBFindObjectsByName(searchString, cl, useNameSpace, modelsOnly)     
    
    ##print "Number of matches: "+str(len(cl))
    if len(cl) == 1:
        returnNode = cl[0]
        ##print "Found "+cl[0].LongName
    else:
        if len(cl) == 0:
            print "Couldn't find "+str(nameSpace)+":"+name
        else:
            print "Found multiple matches for "+name
        
    return returnNode

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def DisconnectBox( nodeOut, nodeOutPos, nodeIn, nodeInPos ):
    myNodeOut = FindAnimationNode( nodeOut.AnimationNodeOutGet(), nodeOutPos )
    myNodeIn = FindAnimationNode( nodeIn.AnimationNodeInGet(), nodeInPos )
    if myNodeOut and myNodeIn:
        FBDisconnect( myNodeOut, myNodeIn )


# This patch fixes the following problems:
#   -Ensure CH_L/R_Hand helpers are created in the scene.
#   -Recreate relation constraint for the Preview Camera to make sure X is pitch and Y is yaw rotation

def initialiseFPS_Cam_Patch():
    lApp = FBApplication()
    lCurrentCharacter = lApp.CurrentCharacter
    lNameSpace = lCurrentCharacter.LongName.split(":")
    lNameSpace = lNameSpace[0]
 
    CHRHand = getNodeByName(True, "CH_L_Hand", lNameSpace, True)
 
    if CHRHand == None:
        CHRHand = getNodeByName(True, "IK_R_Hand", lNameSpace, True)
        CHRHand.Name = "CH_R_Hand"
     
        IKRHand = FBModelNull(lNameSpace+":"+"IK_R_Hand")
        RHand =  getNodeByName(True, "SKEL_R_Hand", lNameSpace, True)
        IKRHand.Parent = RHand
        rVec = FBVector3d(0,0,0)
        IKRHand.Rotation.Data = rVec
        tVec = FBVector3d(0.054,0,0)
        IKRHand.Translation.Data = tVec 
           
        CHLHand = FBModelNull(lNameSpace+":"+"CH_L_Hand")
        LHand =  getNodeByName(True, "SKEL_L_Hand", lNameSpace, True)
        CHLHand.Parent = LHand
        rVec = FBVector3d(0,0,0)
        CHLHand.Rotation.Data = rVec
        tVec = FBVector3d(0.054,0,0)
        CHLHand.Translation.Data = tVec
    
        chLHandCon = FBConstraintManager().TypeCreateConstraint(3)
        chLHandCon.ReferenceAdd (0,CHLHand)
        chLHandCon.ReferenceAdd (1,CHRHand)  
    
        chLHandCon.Active = True
        chLHandCon.Lock = True
        
        ## Adds new bones, CH_L and IK_R, to the Character Extension
        rs_CharacterExtension(lCurrentCharacter)
        
        FBSystem().Scene.Evaluate()
    else:
        print 'CL_R/L_Hand is already setup'

    ## This part fixes the relation constraints of the Preview Cam    
    lConstraints = SCENE.Constraints
    
    for each in lConstraints:
        if each.Name == 'DOFCone_Relation':
            DOF_Cone = each

   
    DisconnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MaxX',DOF_Cone.Boxes[10],'a')
    RS.Utils.Scene.ConnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MaxY', DOF_Cone.Boxes[10],'a')
    lMultiplyB = RS.Utils.Scene.FindAnimationNode(DOF_Cone.Boxes[10].AnimationNodeInGet(),'b')
    lMultiplyB.WriteData([1.0])   
    
    DisconnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MaxY', DOF_Cone.Boxes[12],'a')
    RS.Utils.Scene.ConnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MaxX', DOF_Cone.Boxes[12],'a')

    DisconnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MinX', DOF_Cone.Boxes[6],'a')
    RS.Utils.Scene.ConnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MinY', DOF_Cone.Boxes[6],'a')
    lMultiplyB = RS.Utils.Scene.FindAnimationNode(DOF_Cone.Boxes[6].AnimationNodeInGet(),'b')
    lMultiplyB.WriteData([1.0])  
    
    DisconnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MinY', DOF_Cone.Boxes[14],'a')
    RS.Utils.Scene.ConnectBox(DOF_Cone.Boxes[1], 'FirstPersonCamPivotAttrs_DOF_MinX', DOF_Cone.Boxes[14],'a')
  
initialiseFPS_Cam_Patch()

























