from pyfbsdk import *
import RS.Utils
from pyfbsdk_additions import *
import os
import RS.Perforce
import RS.Config

#Align Function Declaration

def Align( pModel, pAlignTo, pAlignTransX, pAlignTransY, pAlignTransZ, pAlignRotX, pAlignRotY, pAlignRotZ):

   lAlignTransPos = FBVector3d();
   lModelTransPos = FBVector3d();
   lAlignRotPos = FBVector3d();
   lModelRotPos = FBVector3d();   
   
   pAlignTo.GetVector(lAlignTransPos)
   pModel.GetVector(lModelTransPos)

   pAlignTo.GetVector(lAlignRotPos, FBModelTransformationType.kModelRotation )
   pModel.GetVector(lModelRotPos, FBModelTransformationType.kModelRotation)
   
   if pAlignTransX:
     lModelTransPos[0] = lAlignTransPos[0]
   if pAlignTransY:
     lModelTransPos[1] = lAlignTransPos[1]
   if pAlignTransZ:
     lModelTransPos[2] = lAlignTransPos[2]

   if pAlignRotX:
     lModelRotPos[0] = lAlignRotPos[0]
   if pAlignRotY:
     lModelRotPos[1] = lAlignRotPos[1]
   if pAlignRotZ:
     lModelRotPos[2] = lAlignRotPos[2]        
     
   pModel.SetVector(lModelTransPos)
   pModel.SetVector(lModelRotPos, FBModelTransformationType.kModelRotation)


def MergeControlRig( characterName ):
    
    print characterName, "is my character"
    
    #Merging the Manipulators
    app = FBApplication()
    #filename = "D:\Rigging Tasks\Quadrupeds Shoulder Issue\Source.FBX" "X:\rdr3\art\animation\resources\characters\Models\Animals\Rigging_Plugins\Source.FBX"
    filename = "{0}/art/animation/resources/characters/Models\Animals/Rigging_Plugins/Shoulder_Fix/Source.FBX".format( RS.Config.Project.Path.Root )
    
    if os.path.exists( filename ):
        
        RS.Perforce.Sync( filename )
        
        app.FileMerge(filename, False)
        
        
        # find character node via name
        character = None
        for i in RS.Globals.Characters:
            if i.LongName == characterName:
                character = i
        
        if character:
            Spine3Controller = character.GetCtrlRigModel( FBBodyNodeId.kFBSpine3NodeId )
            LeftShoulderController = character.GetCtrlRigModel( FBBodyNodeId.kFBLeftCollarNodeId )
            RightShoulderController = character.GetCtrlRigModel( FBBodyNodeId.kFBRightCollarNodeId )
            
            
        #Storing the namespace into a variable
        split = characterName.split( ':' )
        mynamespace = split[0]
        
        #Declaring Variables    
        BlankLeftManipulator = RS.Utils.Scene.FindModelByName( 'Left_Clavicle_Manipulator', False )
        BlankRightManipulator = RS.Utils.Scene.FindModelByName( 'Right_Clavicle_Manipulator', False )
        
        #Adding namespace to the Manipulators
        BlankRightManipulator.LongName = mynamespace + ':' + 'P_Right_Clavicle_Manipulator'
        BlankLeftManipulator.LongName = mynamespace + ':' + 'P_Left_Clavicle_Manipulator'
        
        #Putting the manipulators with new names into variables    
        LeftManipulator = RS.Utils.Scene.FindModelByName( mynamespace + ':' + 'P_Left_Clavicle_Manipulator', True )
        RightManipulator = RS.Utils.Scene.FindModelByName( mynamespace + ':' + 'P_Right_Clavicle_Manipulator', True )
        
        
        
        LeftClavicleExtension = None
        RightClavicleExtension = None
        for charext1 in FBSystem().Scene.CharacterExtensions:
            if charext1.Name == 'Left Clavicle Character Extension':               
                charext1.LongName =  mynamespace + ':' + charext1.LongName
                LeftClavicleExtension = charext1
        for charext2 in FBSystem().Scene.CharacterExtensions:
            if charext2.Name == 'Right Clavicle Character Extension':
                charext2.LongName =  mynamespace + ':' + charext2.LongName
                RightClavicleExtension = charext2
                    
        
        #Parenting the Manipulators to the spine3 controller
        RightManipulator.Parent = Spine3Controller
        LeftManipulator.Parent = Spine3Controller
        FBSystem().Scene.Evaluate()
        
        #Aligning the Manipulators to the shoulders controllers
        Align (LeftManipulator,LeftShoulderController,LeftShoulderController,LeftShoulderController,LeftShoulderController,LeftShoulderController,LeftShoulderController,LeftShoulderController)
        Align (RightManipulator,RightShoulderController,RightShoulderController,RightShoulderController,RightShoulderController,RightShoulderController,RightShoulderController,RightShoulderController)
        FBSystem().Scene.Evaluate() 
        
        #Ataching the character to our character extension
    
        for char in FBSystem().Scene.Characters:
            if char.LongName == characterName:
                for charext in FBSystem().Scene.CharacterExtensions:
                    if charext.LongName == LeftClavicleExtension.LongName:              
                        char.AddCharacterExtension( charext )
                    if charext.LongName == RightClavicleExtension.LongName:              
                        char.AddCharacterExtension( charext )
        
            
        ######################################
        #Creating Parent/Child Constraints   #
        ######################################
    
        #Creating Left Clavicle
        Parentchild_LeftClavicle = FBConstraintManager().TypeCreateConstraint ('Parent/Child')
        Parentchild_LeftClavicle.Name = "Left Clavicle Constraint"
        ChildProperty = Parentchild_LeftClavicle.PropertyList.Find( "Constrained object (Child)" )
        LeftShoulderController.ConnectSrc( ChildProperty )
        
        SourceProperty = Parentchild_LeftClavicle.PropertyList.Find( "Source (Parent)" )
        LeftManipulator.ConnectDst( SourceProperty )
        
        Parentchild_LeftClavicle.Active = True
        
        #Disabling Rotation in the Constraint
        DisaRotX = Parentchild_LeftClavicle.PropertyList.Find( 'AffectRotationX' )
        DisaRotY = Parentchild_LeftClavicle.PropertyList.Find( 'AffectRotationY' )
        DisaRotZ = Parentchild_LeftClavicle.PropertyList.Find( 'AffectRotationZ' )
        DisaRotX.Data = 0
        DisaRotY.Data = 0
        DisaRotZ.Data = 0
        
        #Creating Right Clavicle
        Parentchild_RightClavicle = FBConstraintManager().TypeCreateConstraint ('Parent/Child')
        Parentchild_RightClavicle.Name = "Right Clavicle Constraint"
        ChildProperty = Parentchild_RightClavicle.PropertyList.Find( "Constrained object (Child)" )
        RightShoulderController.ConnectSrc( ChildProperty )
        
        SourceProperty = Parentchild_RightClavicle.PropertyList.Find( "Source (Parent)" )
        RightManipulator.ConnectDst( SourceProperty )
        
        Parentchild_RightClavicle.Active = True
        
        #Disabling Rotation in the Constraint
        DisaRotX = Parentchild_RightClavicle.PropertyList.Find( 'AffectRotationX' )
        DisaRotY = Parentchild_RightClavicle.PropertyList.Find( 'AffectRotationY' )
        DisaRotZ = Parentchild_RightClavicle.PropertyList.Find( 'AffectRotationZ' )
        DisaRotX.Data = 0
        DisaRotY.Data = 0
        DisaRotZ.Data = 0
        FBSystem().Scene.Evaluate()
    
    else:
        if RS.Config.Project.Name == 'GTA5':
            FBMessageBox( 'R* Error', 'This tool is not available for the current project!', 'Ok' )
        else:
            FBMessageBox( 'R* Error', 'Unable to find custom rig file:\n\n{0}'.format( filename ), 'Ok' )

