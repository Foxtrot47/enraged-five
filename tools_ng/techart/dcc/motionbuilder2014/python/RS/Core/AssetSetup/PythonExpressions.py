import RS.Core.Metadata as meta
import RS.Core.AssetSetup.ExprToAst as work
from pyfbsdk import *

metaFile = None
newBoneLst = None

def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName or lNode.Label == pName:
            lResult = lNode
            break
    return lResult

def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
    if mynodeOut and mynodeIn:
        FBConnect(mynodeOut, mynodeIn)

def HasDestinationConnection(nodeOut,nodeOutPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
    if mynodeOut.GetDstCount() > 0:
        return True
    return False

def recursivelyParseExpression( expression, relConstraint ):
    
    global depth
    global boxCount
    
    returnBox = None
    
    if str(expression.__class__) == "<type 'float'>":
        return float(expression)
    
    if "#" in expression:
        
        depth += 1
        
        controlAttribute = expression.split("#")
        
        model = FBFindModelByLabelName(str(controlAttribute[0]))

        if model:

            for iProp in model.PropertyList:  
                if iProp.IsUserProperty():
                    if str(type(iProp)) == "<class 'pyfbsdk.FBPropertyAnimatableDouble'>":
                        iProp.SetAnimated(True)
            
            modelSender = relConstraint.SetAsSource(model)
            modelSender.UseGlobalTransforms = False
            
            if "translate" in controlAttribute[1]:
                
                relConstraint.SetBoxPosition(modelSender, ((depth * -400) - 1000), boxCount * 200)
    
                zeroController = relConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')
                ConnectBox(modelSender, 'Lcl Translation', zeroController, 'V1')
                zeroVector = model.Translation.Data
                zeroControllerV2 = FindAnimationNode(zeroController.AnimationNodeInGet(),'V2')
                zeroControllerV2.WriteData([zeroVector[0], zeroVector[1], zeroVector[2]])
                relConstraint.SetBoxPosition(zeroController, ((depth * -400) - 400), boxCount * 200)
                                
                converterBox = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')
                ConnectBox(zeroController, 'Result', converterBox, 'V')
                relConstraint.SetBoxPosition(converterBox, depth * -400, boxCount * 200)
    
                if "X" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "X"]
                    
                if "Y" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "Y"]
                    
                if "Z" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "Z"]
                
            elif "rotate" in controlAttribute[1]:
                
                relConstraint.SetBoxPosition(modelSender, ((depth * -400) - 1400), boxCount * 200)
                
                zeroController = relConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')
                ConnectBox(modelSender, 'Lcl Rotation', zeroController, 'V1')
                zeroVector = model.Rotation.Data
                zeroControllerV2 = FindAnimationNode(zeroController.AnimationNodeInGet(),'V2')
                zeroControllerV2.WriteData([zeroVector[0], zeroVector[1], zeroVector[2]])
                relConstraint.SetBoxPosition(zeroController, ((depth * -400) - 800), boxCount * 200)
                converterBox = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')
                ConnectBox(zeroController, 'Result', converterBox, 'V')
                relConstraint.SetBoxPosition(converterBox, ((depth * -400) - 400) , boxCount * 200)
                degToRadBox = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')
                
                if "X" in controlAttribute[1]:
                    
                    ConnectBox(converterBox, 'X', degToRadBox, 'a')
                    relConstraint.SetBoxPosition(degToRadBox, depth * -400, boxCount * 200)
                    depth -= 1
                    return [degToRadBox, "Result"]

                    
                if "Y" in controlAttribute[1]:
                    
                    ConnectBox(converterBox, 'Y', degToRadBox, 'a')
                    relConstraint.SetBoxPosition(degToRadBox, depth * -400, boxCount * 200)
                    depth -= 1
                    return [degToRadBox, "Result"]

                    
                if "Z" in controlAttribute[1]:
                    
                    ConnectBox(converterBox, 'Z', degToRadBox, 'a')
                    relConstraint.SetBoxPosition(degToRadBox, depth * -400, boxCount * 200)
                    depth -= 1
                    return [degToRadBox, "Result"]

            
            elif "scale" in controlAttribute[1]:
                
                relConstraint.SetBoxPosition(modelSender, ((depth * -400) - 400), boxCount * 200)
                converterBox = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')
                ConnectBox(modelSender, 'Lcl Scaling', converterBox, 'V')
                relConstraint.SetBoxPosition(converterBox, depth * -400, boxCount * 200)
                
                if "X" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "X"]
                    
                if "Y" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "Y"]
                    
                if "Z" in controlAttribute[1]:
                    
                    depth -= 1
                    return [converterBox, "Z"]
                    

    
    try:
        if str(expression[0].__class__) == "<type 'list'>":
            
            depth += 1
            returnBox = recursivelyParseExpression( expression[0], relConstraint )
            return returnBox

        else:
                
            """ kEqualTo """
            if expression[0] == "kEqualTo":
                
                depth += 1
                
                equalToBox = relConstraint.CreateFunctionBox('Number', 'Is Identical (a == b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    equalToBoxA = FindAnimationNode(equalToBox.AnimationNodeInGet(),'a')
                    equalToBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', equalToBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], equalToBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    equalToBoxB = FindAnimationNode(equalToBox.AnimationNodeInGet(),'b')
                    equalToBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', equalToBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], equalToBox, 'b')
                    
                relConstraint.SetBoxPosition(equalToBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return equalToBox
            
            """ kNotEqualTo """
            if expression[0] == "kNotEqualTo":
                
                depth += 1
                
                notEqualToBox = relConstraint.CreateFunctionBox('Number', 'Is Different (a != b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    notEqualToBoxA = FindAnimationNode(notEqualToBox.AnimationNodeInGet(),'a')
                    notEqualToBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', notEqualToBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], notEqualToBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    notEqualToBoxB = FindAnimationNode(notEqualToBox.AnimationNodeInGet(),'b')
                    notEqualToBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', notEqualToBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], notEqualToBox, 'b')
                    
                relConstraint.SetBoxPosition(notEqualToBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return notEqualToBox
            
            """ kLessThan """
            if expression[0] == "kLessThan":
                
                depth += 1
                
                lessThanBox = relConstraint.CreateFunctionBox('Number', 'Is Less (a < b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    lessThanBoxA = FindAnimationNode(lessThanBox.AnimationNodeInGet(),'a')
                    lessThanBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', lessThanBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], lessThanBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    lessThanBoxB = FindAnimationNode(lessThanBox.AnimationNodeInGet(),'b')
                    lessThanBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', lessThanBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], lessThanBox, 'b')
                    
                relConstraint.SetBoxPosition(lessThanBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return lessThanBox
            
            """ kLessThanEqualTo """
            if expression[0] == "kLessThanEqualTo":
                
                depth += 1
                
                lessThanEqualToBox = relConstraint.CreateFunctionBox('Number', 'Is Less or Equal (a <= b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    lessThanEqualToBoxA = FindAnimationNode(lessThanEqualToBox.AnimationNodeInGet(),'a')
                    lessThanEqualToBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', lessThanEqualToBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], lessThanEqualToBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    lessThanEqualToBoxB = FindAnimationNode(lessThanEqualToBox.AnimationNodeInGet(),'b')
                    lessThanEqualToBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', lessThanEqualToBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], lessThanEqualToBox, 'b')
                    
                relConstraint.SetBoxPosition(lessThanEqualToBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return lessThanEqualToBox
            
            """ kGreaterThan """
            if expression[0] == "kGreaterThan":
                
                depth += 1
                
                greaterThanBox = relConstraint.CreateFunctionBox('Number', 'Is Greater (a > b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    greaterThanBoxA = FindAnimationNode(greaterThanBox.AnimationNodeInGet(),'a')
                    greaterThanBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', greaterThanBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], greaterThanBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    greaterThanBoxB = FindAnimationNode(greaterThanBox.AnimationNodeInGet(),'b')
                    greaterThanBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', greaterThanBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], greaterThanBox, 'b')
                    
                relConstraint.SetBoxPosition(greaterThanBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return greaterThanBox
            
            """ kGreaterThanEqualTo """
            if expression[0] == "kGreaterThanEqualTo":
                
                depth += 1
                
                greaterThanEqualToBox = relConstraint.CreateFunctionBox('Number', 'Is Greater or Equal (a >= b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    greaterThanEqualToBoxA = FindAnimationNode(greaterThanEqualToBox.AnimationNodeInGet(),'a')
                    greaterThanEqualToBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', greaterThanEqualToBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], greaterThanEqualToBox, 'a')
                    
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    greaterThanEqualToBoxB = FindAnimationNode(greaterThanEqualToBox.AnimationNodeInGet(),'b')
                    greaterThanEqualToBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', greaterThanEqualToBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], greaterThanEqualToBox, 'b')
                    
                relConstraint.SetBoxPosition(greaterThanEqualToBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return greaterThanEqualToBox
            
            """ kMultiply """    
            if expression[0] == "kMultiply":
                
                depth += 1
                
                multBox = relConstraint.CreateFunctionBox('Number', 'Multiply (a x b)') 
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    multBoxA = FindAnimationNode(multBox.AnimationNodeInGet(),'a')
                    multBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', multBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], multBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    multBoxB = FindAnimationNode(multBox.AnimationNodeInGet(),'b')
                    multBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', multBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], multBox, 'b')
                
                relConstraint.SetBoxPosition(multBox, depth * -400, boxCount * 200) 
    
                depth -= 1
                return multBox
            
            """ kDivide """    
            if expression[0] == "kDivide":
                
                depth += 1
                
                divBox = relConstraint.CreateFunctionBox( 'Number', 'Divide (a/b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    divBoxA = FindAnimationNode(divBox.AnimationNodeInGet(),'a')
                    divBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', divBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], divBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    divBoxB = FindAnimationNode(divBox.AnimationNodeInGet(),'b')
                    divBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', divBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], divBox, 'b')
                
                relConstraint.SetBoxPosition(divBox, depth * -400, boxCount * 200) 
    
                depth -= 1
                
                return divBox
            
            """ kAdd """  
            if expression[0] == "kAdd":
                
                depth += 1 
                
                addBox = relConstraint.CreateFunctionBox('Number', 'Add (a + b)') 
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    addBoxA = FindAnimationNode(addBox.AnimationNodeInGet(),'a')
                    addBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', addBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], addBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    addBoxB = FindAnimationNode(addBox.AnimationNodeInGet(),'b')
                    addBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', addBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], addBox, 'b')
                
                relConstraint.SetBoxPosition(addBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return addBox
            
            """ kSubtract """  
            if expression[0] == "kSubtract":
                
                depth += 1 
                
                subBox = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)') 
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    subBoxA = FindAnimationNode(subBox.AnimationNodeInGet(),'a')
                    subBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', subBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], subBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    subBoxB = FindAnimationNode(subBox.AnimationNodeInGet(),'b')
                    subBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', subBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], subBox, 'b')
                
                relConstraint.SetBoxPosition(subBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return subBox
            
            """ kAbs """  
            if expression[0] == "kAbs":
                
                depth += 1 
                
                absBox = relConstraint.CreateFunctionBox('Number', 'Absolute(|a|)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    absBoxA = FindAnimationNode(absBox.AnimationNodeInGet(),'a')
                    absBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', absBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], absBox, 'a')
                
                relConstraint.SetBoxPosition(absBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return absBox
            
            """ kAcos """  
            if expression[0] == "kAcos":
                
                depth += 1 
                
                acosBox = relConstraint.CreateFunctionBox('Number', 'arccos(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    acosBoxA = FindAnimationNode(acosBox.AnimationNodeInGet(),'a')
                    acosBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', acosBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], acosBox, 'a')
                
                relConstraint.SetBoxPosition(acosBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return acosBox
            
            """ kAsin """  
            if expression[0] == "kAsin":
                
                depth += 1 
                
                asinBox = relConstraint.CreateFunctionBox('Number', 'arcsin(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    asinBoxA = FindAnimationNode(asinBox.AnimationNodeInGet(),'a')
                    asinBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', asinBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], asinBox, 'a')
                
                relConstraint.SetBoxPosition(asinBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return asinBox
            
            """ kATan """  
            if expression[0] == "kATan":
                
                depth += 1 
                
                atanBox = relConstraint.CreateFunctionBox('Number', 'arctan(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    atanBoxA = FindAnimationNode(atanBox.AnimationNodeInGet(),'a')
                    atanBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', atanBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], atanBox, 'a')
                
                relConstraint.SetBoxPosition(atanBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return atanBox
            
            """ kCeil """  
            if expression[0] == "kCeil":
                
                depth += 1 
                
                ceilBox = relConstraint.CreateFunctionBox('My Macros', 'ceil(x)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    ceilBoxA = FindAnimationNode(ceilBox.AnimationNodeInGet(),'a')
                    ceilBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', ceilBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], ceilBox, 'a')
                
                relConstraint.SetBoxPosition(ceilBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return ceilBox
            
            """ kCos """  
            if expression[0] == "kCos":
                
                depth += 1 
                
                cosBox = relConstraint.CreateFunctionBox('Number', 'Cosine cos(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    cosBoxA = FindAnimationNode(cosBox.AnimationNodeInGet(),'a')
                    cosBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', cosBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], cosBox, 'a')
                
                relConstraint.SetBoxPosition(cosBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return cosBox
            
            """ kDToR """  
            if expression[0] == "kDToR":
                
                depth += 1 
                
                dToRBox = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    dToRBoxA = FindAnimationNode(dToRBox.AnimationNodeInGet(),'a')
                    dToRBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', dToRBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], dToRBox, 'a')
                
                relConstraint.SetBoxPosition(dToRBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return dToRBox
            
            """ kExp """  
            if expression[0] == "kExp":
                
                depth += 1 
                
                expBox = relConstraint.CreateFunctionBox('Number', 'exp(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    expBoxA = FindAnimationNode(expBox.AnimationNodeInGet(),'a')
                    expBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', expBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], expBox, 'a')
                
                relConstraint.SetBoxPosition(expBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return expBox
            
            """ kFloor """  
            if expression[0] == "kFloor":
                
                depth += 1 
                
                floorBox = relConstraint.CreateFunctionBox('My Macros', 'floor(x)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    floorBoxA = FindAnimationNode(floorBox.AnimationNodeInGet(),'a')
                    floorBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', floorBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], floorBox, 'a')
                
                relConstraint.SetBoxPosition(floorBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return floorBox
            
            """ kLn """  
            if expression[0] == "kLn":
                
                depth += 1 
                
                lnBox = relConstraint.CreateFunctionBox('Number', 'ln(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    lnBoxA = FindAnimationNode(lnBox.AnimationNodeInGet(),'a')
                    lnBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', lnBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], lnBox, 'a')
                
                relConstraint.SetBoxPosition(lnBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return lnBox
            
            """ kLog """  
            if expression[0] == "kLog":
                
                depth += 1 
                
                logBox = relConstraint.CreateFunctionBox('Number', 'log(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    logBoxA = FindAnimationNode(logBox.AnimationNodeInGet(),'a')
                    logBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', logBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], logBox, 'a')
                
                relConstraint.SetBoxPosition(logBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return logBox
            
            """ kRToD """  
            if expression[0] == "kRToD":
                
                depth += 1 
                
                rToDBox = relConstraint.CreateFunctionBox('Converters', 'Rad To Deg')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    rToDBoxA = FindAnimationNode(rToDBox.AnimationNodeInGet(),'a')
                    rToDBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', rToDBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], rToDBox, 'a')
                
                relConstraint.SetBoxPosition(rToDBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return rToDBox
            
            """ kSin """  
            if expression[0] == "kSin":
                
                depth += 1 
                
                sinBox = relConstraint.CreateFunctionBox('Number', 'Sine sin(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    sinBoxA = FindAnimationNode(sinBox.AnimationNodeInGet(),'a')
                    sinBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', sinBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], sinBox, 'a')
                
                relConstraint.SetBoxPosition(sinBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return sinBox
            
            """ kSqrt """  
            if expression[0] == "kSqrt":
                
                depth += 1 
                
                sqrtBox = relConstraint.CreateFunctionBox('Number', 'sqrt(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    sqrtBoxA = FindAnimationNode(sqrtBox.AnimationNodeInGet(),'a')
                    sqrtBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', sqrtBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], sqrtBox, 'a')
                
                relConstraint.SetBoxPosition(sqrtBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return sqrtBox
            
            """ kTan """  
            if expression[0] == "kTan":
                
                depth += 1 
                
                tanBox = relConstraint.CreateFunctionBox('Number', 'Tangeant tan(a)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    tanBoxA = FindAnimationNode(tanBox.AnimationNodeInGet(),'a')
                    tanBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', tanBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], tanBox, 'a')
                
                relConstraint.SetBoxPosition(tanBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return tanBox
            
            """ kMax """  
            if expression[0] == "kMax":
                
                depth += 1 
                
                maxBox = relConstraint.CreateFunctionBox('My Macros', 'max(x,y)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    maxBoxA = FindAnimationNode(maxBox.AnimationNodeInGet(),'a')
                    maxBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', maxBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], maxBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    maxBoxB = FindAnimationNode(maxBox.AnimationNodeInGet(),'b')
                    maxBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', maxBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], maxBox, 'b')
                
                relConstraint.SetBoxPosition(maxBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return maxBox
            
            """ kMin """  
            if expression[0] == "kMin":
                
                depth += 1 
                
                minBox = relConstraint.CreateFunctionBox('My Macros', 'min(x,y)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )

                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    minBoxA = FindAnimationNode(minBox.AnimationNodeInGet(),'a')
                    minBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', minBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], minBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    minBoxB = FindAnimationNode(minBox.AnimationNodeInGet(),'b')
                    minBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', minBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], minBox, 'b')
                
                relConstraint.SetBoxPosition(minBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return minBox
        
            """ kMod """  
            if expression[0] == "kMod":
                
                depth += 1 
                
                modBox = relConstraint.CreateFunctionBox('Number', 'Modulo mod(a, b)')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    modBoxA = FindAnimationNode(modBox.AnimationNodeInGet(),'a')
                    modBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', modBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], modBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    modBoxB = FindAnimationNode(modBox.AnimationNodeInGet(),'b')
                    modBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', modBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], modBox, 'b')
                
                relConstraint.SetBoxPosition(modBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return modBox
            
            """ kIfThenElse """  
            if expression[0] == "kIfThenElse":
                
                depth += 1 
                
                ifThenElseBox = relConstraint.CreateFunctionBox('Number', 'IF Cond Then A Else B')
                
                inputA = recursivelyParseExpression( expression[1], relConstraint )
                
                boxCount += 1
                
                if str(inputA.__class__) == "<type 'float'>":
                    ifThenElseBoxA = FindAnimationNode(ifThenElseBox.AnimationNodeInGet(),'a')
                    ifThenElseBoxA.WriteData([inputA])
                elif str(inputA.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputA, 'Result', ifThenElseBox, 'a')
                elif str(inputA.__class__) == "<type 'list'>":
                    ConnectBox(inputA[0], inputA[1], ifThenElseBox, 'a')
                
                inputB = recursivelyParseExpression( expression[2], relConstraint )
                
                if str(inputB.__class__) == "<type 'float'>":
                    ifThenElseBoxB = FindAnimationNode(ifThenElseBox.AnimationNodeInGet(),'b')
                    ifThenElseBoxB.WriteData([inputB])
                elif str(inputB.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputB, 'Result', ifThenElseBox, 'b')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], ifThenElseBox, 'b')
                    
                inputC = recursivelyParseExpression( expression[3], relConstraint )
                
                if str(inputC.__class__) == "<type 'float'>":
                    ifThenElseBoxC = FindAnimationNode(ifThenElseBox.AnimationNodeInGet(),'Cond')
                    ifThenElseBoxC.WriteData([inputC])
                elif str(inputC.__class__) == "<class 'pyfbsdk.FBBox'>":
                    ConnectBox(inputC, 'Result', ifThenElseBox, 'Cond')
                elif str(inputB.__class__) == "<type 'list'>":
                    ConnectBox(inputB[0], inputB[1], ifThenElseBox, 'Cond')
                
                relConstraint.SetBoxPosition(ifThenElseBox, depth * -400, boxCount * 200) 
                
                depth -= 1
                
                return ifThenElseBox
    except:
        try:
            print "Except: " +  str(expression[0])
            
        except:
            print "Except: " +  str(expression)
        
    return returnBox

def buildExpressions(newBoneLst):
    
    global depth
    global boxCount
    
    lMgr = FBConstraintManager()
    lScene = FBSystem().Scene
    
    constraintFolder = None
    
    for folder in lScene.Folders:
        
        if folder.Name == "Constraints 1":
            
            constraintFolder = folder
    
    for bone in newBoneLst:
        
        depth = 0    
        boxCount = 0
        
        exprBone = FBFindModelByLabelName(str(bone.name))
        
        if not exprBone:
            continue
           
        relConstraint   = FBConstraintRelation( str(bone.name) )        
        relConstraint.Active = True
        
        constrainedModel = relConstraint.ConstrainObject(exprBone)
        constrainedModel.UseGlobalTransforms = False
        
        transNumToVec   = relConstraint.CreateFunctionBox('Converters', 'Number To Vector') 
        rotNumToVec     = relConstraint.CreateFunctionBox('Converters', 'Number To Vector')
        scaleNumToVec   = relConstraint.CreateFunctionBox('Converters', 'Number To Vector') 
        
        xTransNode      = None
        yTransNode      = None
        zTransNode      = None
        
        xRotNode        = None
        yRotNode        = None
        zRotNode        = None
        
        xScaleNode      = None
        yScaleNode      = None
        zScaleNode      = None
        
        print "bone.name: " + str(bone.name)
        print "bone.xTransLst: " + str(len(bone.xTransLst))
        print bone.xTransLst
        if len(bone.xTransLst) > 0: 
            xTransNode  = recursivelyParseExpression( bone.xTransLst, relConstraint ) 
            print xTransNode
            depth       = 0
        print "bone.yTransLst: " + str(len(newBoneLst[0].yTransLst))
        if len(newBoneLst[0].yTransLst) > 0: 
            yTransNode  = recursivelyParseExpression( bone.yTransLst, relConstraint )
            depth       = 0
        print "bone.zTransLst: " + str(len(bone.zTransLst))
        if len(bone.zTransLst) > 0: 
            zTransNode  = recursivelyParseExpression( bone.zTransLst, relConstraint ) 

        print "bone.xRotLst: " + str(len(bone.xRotLst))
        if len(bone.xRotLst) > 0:  
            xRotNode = recursivelyParseExpression( bone.xRotLst, relConstraint ) 
            depth = 0
        print "bone.yRotLst: " + str(len(bone.yRotLst))
        if len(bone.yRotLst) > 0:  
            yRotNode = recursivelyParseExpression( bone.yRotLst, relConstraint )
            depth = 0
        print "bone.zRotLst: " + str(len(bone.zRotLst))
        if len(bone.zRotLst) > 0:  
            zRotNode = recursivelyParseExpression( bone.zRotLst, relConstraint ) 
                
        print "bone.xScaleLst: " + str(len(bone.xScaleLst))
        if len(bone.xScaleLst) > 0: 
            xScaleNode = recursivelyParseExpression( bone.xScaleLst, relConstraint )
            depth = 0
        print "bone.yScaleLst: " + str(len(bone.yScaleLst))
        if len(bone.yScaleLst) > 0: 
            yScaleNode = recursivelyParseExpression( bone.yScaleLst, relConstraint )
            depth = 0
        print "bone.zScaleLst: " + str(len(bone.zScaleLst))
        if len(bone.zScaleLst) > 0: 
            zScaleNode = recursivelyParseExpression( bone.zScaleLst, relConstraint )  
        
        if xTransNode != None:
            ConnectBox(xTransNode, 'Result', transNumToVec, 'X')
        else:
            transNumToVecX = FindAnimationNode(transNumToVec.AnimationNodeInGet(),'X')
            transNumToVecX.WriteData([0])  
        if yTransNode != None:
            ConnectBox(yTransNode, 'Result', transNumToVec, 'Y')
        else:
            transNumToVecY = FindAnimationNode(transNumToVec.AnimationNodeInGet(),'Y')
            transNumToVecY.WriteData([0]) 
        if zTransNode != None:
            ConnectBox(zTransNode, 'Result', transNumToVec, 'Z') 
        else:
            transNumToVecZ = FindAnimationNode(transNumToVec.AnimationNodeInGet(),'Z')
            transNumToVecZ.WriteData([0])
            
        if xRotNode:
            ConnectBox(xRotNode, 'Result', rotNumToVec, 'X')  
        else:
            rotNumToVecX = FindAnimationNode(rotNumToVec.AnimationNodeInGet(),'X')
            rotNumToVecX.WriteData([0]) 
        if yRotNode:
            ConnectBox(yRotNode, 'Result', rotNumToVec, 'Y')
        else:
            rotNumToVecY = FindAnimationNode(rotNumToVec.AnimationNodeInGet(),'Y')
            rotNumToVecY.WriteData([0]) 
        if zRotNode:
            ConnectBox(zRotNode, 'Result', rotNumToVec, 'Z') 
        else:
            rotNumToVecZ = FindAnimationNode(rotNumToVec.AnimationNodeInGet(),'Z')
            rotNumToVecZ.WriteData([0]) 
    
        if xScaleNode:
            ConnectBox(xScaleNode, 'Result', scaleNumToVec, 'X')   
        else:
            scaleNumToVecX = FindAnimationNode(scaleNumToVec.AnimationNodeInGet(),'X')
            scaleNumToVecX.WriteData([0]) 
        if yScaleNode:
            ConnectBox(yScaleNode, 'Result', scaleNumToVec, 'Y')
        else:
            scaleNumToVecY = FindAnimationNode(scaleNumToVec.AnimationNodeInGet(),'Y')
            scaleNumToVecY.WriteData([0]) 
        if zScaleNode:
            ConnectBox(zScaleNode, 'Result', scaleNumToVec, 'Z')
        else:
            scaleNumToVecZ = FindAnimationNode(scaleNumToVec.AnimationNodeInGet(),'Z')
            scaleNumToVecZ.WriteData([0]) 
            
        
        offsetTransController = relConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
        offsetTransVector = exprBone.Translation.Data
        offsetTransControllerV2 = FindAnimationNode(offsetTransController.AnimationNodeInGet(),'V2')
        offsetTransControllerV2.WriteData([offsetTransVector[0], offsetTransVector[1], offsetTransVector[2]])
        
        offsetRotController = relConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
        offsetRotVector = exprBone.Rotation.Data
        offsetRotControllerV2 = FindAnimationNode(offsetRotController.AnimationNodeInGet(),'V2')
        offsetRotControllerV2.WriteData([offsetRotVector[0], offsetRotVector[1], offsetRotVector[2]])
        
        offsetScaleController = relConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
        offsetScaleVector = exprBone.Scaling.Data
        offsetScaleControllerV2 = FindAnimationNode(offsetScaleController.AnimationNodeInGet(),'V2')
        offsetScaleControllerV2.WriteData([offsetScaleVector[0], offsetScaleVector[1], offsetScaleVector[2]])
            
        relConstraint.SetBoxPosition(transNumToVec, depth * -400, boxCount * 200) 
        relConstraint.SetBoxPosition(offsetTransController, (depth - 1) * -400, boxCount * 200) 
        ConnectBox(transNumToVec , 'Result', offsetTransController, 'V1')
        
        relConstraint.SetBoxPosition(rotNumToVec, depth * -400, (boxCount + 1) * 200)  
        relConstraint.SetBoxPosition(offsetRotController, (depth - 1) * -400, (boxCount + 1) * 200) 
        ConnectBox(rotNumToVec, 'Result', offsetRotController, 'V1')
        
        relConstraint.SetBoxPosition(scaleNumToVec, depth * -400, (boxCount + 2) * 200) 
        relConstraint.SetBoxPosition(offsetScaleController, (depth - 1) * -400, (boxCount + 2) * 200) 
        ConnectBox(scaleNumToVec, 'Result', offsetScaleController, 'V1')
         
        relConstraint.SetBoxPosition(constrainedModel, (depth - 2) * -400, (boxCount + 2) * 200) 
    
        ConnectBox(offsetTransController , 'Result', constrainedModel, 'Lcl Translation')
        ConnectBox(offsetRotController, 'Result', constrainedModel, 'Lcl Rotation')
        ConnectBox(offsetScaleController, 'Result', constrainedModel, 'Lcl Scaling')
        
        constraintFolder.ConnectSrc(relConstraint)
        
        print "bone.aimLst: " + str(bone.aimLst)
        print
        if len(bone.aimLst) > 0: 
            
            aimConstraint = lMgr.TypeCreateConstraint( 0 )
            aimConstraint.Name = "{0}_Aim".format( bone.aimLst[1] )
            aimConstraint.ReferenceAdd(0,exprBone)
            
            aimAtNode = FBFindModelByLabelName(str(bone.aimLst[0]))
            aimConstraint.ReferenceAdd(1,aimAtNode)
            
            worldUpNode = FBFindModelByLabelName(str(bone.aimLst[2]))
            aimConstraint.ReferenceAdd(2,worldUpNode)
            
            aimConstraint.PropertyList.Find( "WorldUpType" ).Data = 1
            
            aimConstraint.Snap()
            
            constraintFolder.ConnectSrc(aimConstraint)


def Run():
    fileBrowser = FBFilePopup()
    fileBrowser.Path = r"X:\gta5\assets\anim\expressions"
    fileBrowser.Filter = '*.xml'
    fileBrowser.Caption = 'Open Expression XML File'
    if fileBrowser.Execute():
        metaFile = meta.ParseMetaFile(fileBrowser.Path + "\\" + fileBrowser.FileName)
        newBoneLst = work.populateBoneLst(metaFile, True)
        buildExpressions(newBoneLst)
    else:
        FBMessageBox("No Expression Selected", "Script requires an XML file to be selected", "OK")

    