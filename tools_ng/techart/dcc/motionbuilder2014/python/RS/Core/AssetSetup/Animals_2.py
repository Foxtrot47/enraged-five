
from PySide import QtCore, QtGui
import pyfbsdk as mobu

import os

from RS import Config, Globals, Perforce
from RS.Core.Character import IngameCharacterIKControls
from RS.Utils import Path, Scene, Creation
from RS.Utils.Scene import Constraint, Model, RelationshipConstraintManager
from RS.Core import Metadata
from RS.Core.AssetSetup import Lib
from RS.Tools import UI


class DialogWithCustomButton(QtGui.QMessageBox):
                def __init__(self, parent= None):
                                super(DialogWithCustomButton, self).__init__()
                                self._modelSetup = ModelSetup()

                                self.dialog()

                def dialog(self):
                                msgBox = QtGui.QMessageBox()
                                msgBox.setWindowTitle("Missing XML for Model")
                                msgBox.setText("A Skeleton Setup file does not exist for this model - this is required for the characterisation.\n\nThis file has now been prepped for characterization for you.  Please do the following:\n\n1a. manually slot, from a blank template, all of the relevant bones into the Mapping List sections\nor\n1b. select the 'use existing xml' button, which loads an existing xml from another character to start as your characterisation template.\n\n2. click the button 'Create Skeleton XML'\n\nFuture setups of this model will then be automated for setup.")
                                msgBox.addButton('Continue from Scratch', QtGui.QMessageBox.YesRole)
                                msgBox.addButton('Use Existing XML as Template', QtGui.QMessageBox.AcceptRole)
                                msgBox.addButton('Cancel', QtGui.QMessageBox.RejectRole)
                                iconPath = defaultDirPath = os.path.join(
                                                Config.Script.Path.ToolImages,
                                                "rsLogo.png"
                                )
                                msgBox.setIconPixmap(QtGui.QPixmap(iconPath))
                                msgBox.exec_()
                                result = msgBox.buttonRole(msgBox.clickedButton())
                                if result == QtGui.QMessageBox.ButtonRole.AcceptRole:
                                                self._handleAddTemplateXml()
                                elif result == QtGui.QMessageBox.ButtonRole.YesRole:
                                                self._modelSetup.PrepCharacterForManualCharacterization()
                                else:
                                                return

                def _handleAddTemplateXml(self):
                                # popup for user to select new reference
                                popup = QtGui.QFileDialog()
                                popup.setWindowTitle('Select an xml as a template for the characterisation:')
                                popup.setViewMode(QtGui.QFileDialog.List)
                                popup.setNameFilters(["*.xml"])
                                defaultDirPath = os.path.join(
                                                Config.Project.Path.Root,
                                                "art",
                                                "animation",
                                                "resources",
                                                "characters",
                                                "skeletonSetupXML"
                                )
                                popup.setDirectory(defaultDirPath)
                                if popup.exec_() :
                                                if len(popup.selectedFiles()) > 0:
                                                                filePath = popup.selectedFiles()[0]
                                                                fileInfo = QtCore.QFileInfo(filePath)
                                                                fileName = fileInfo.baseName()
                                                                # prep character for characterization
                                                                self._modelSetup.PrepCharacterForManualCharacterization()
                                                                # add template from selected xm;
                                                                self._modelSetup.SetupCharacterization(fileName, False)


class ModelSetup(object):
                '''
                generic initial setup of animals
                '''
                def __init__(self):
                                self.MuscleBoneList = []
                                self.fileName = self.GetFileName()
                                self.skelRootModelNull = self.GetSkelRootModelNull()
                                self.parentModelNull = self.GetDummyParentModelNull()
                                self.geoParentModelNull = self.GetGeoParentModelNull()
                                self.moverModelNull = self.GetMoverModelNull()

                def GetFileName(self):
                                '''
                                gets the name of the file

                                returns: String - the name of the file in lower case
                                '''
                                return Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName).lower()

                def GetSkelRootModelNull(self):
                                '''
                                finds 'SKEL_Root' scene object

                                returns: FBModel - 'SKEL_Root' scene object
                                '''
                                return Scene.FindModelByName('SKEL_ROOT')

                def GetDummyParentModelNull(self):
                                '''
                                use 'SKEL_Root' to find the top null of the main heirarchy.
                                this scene object should be present in all animals.

                                returns: FBModel - 'Dummy01' scene object
                                '''
                                if self.skelRootModelNull is None:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "Cannot find the scene object: SKEL_Root.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # get the parent node of the hierarchy - which should be the dummy
                                parentModelNull = Scene.GetParent(self.skelRootModelNull)
                                if parentModelNull is None:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "Unable to find Dummy node via 'SKEL_Root'.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return None
                                return parentModelNull

                def GetMoverModelNull(self):
                                '''
                                finds 'mover' scene object

                                returns: FBModel - 'mover' scene object
                                '''
                                if self.parentModelNull is None:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "Cannot find the scene object: Dummy01.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                if len(self.parentModelNull.Children) > 0:
                                                for child in self.parentModelNull.Children:
                                                                if child.Name == 'mover':
                                                                                self.moverModelNull = child
                                                                                return self.moverModelNull

                def GetGeoDummyMaxProperty(self):
                                '''
                                Gather property data from the max custom properties

                                Returns:
                                    String, String : GEODUMMYNODE (bool if the FBModel is the geodummy)
                                '''
                                # go through hierarchy and find the max property 'GEODUMMYNODE'
                                maxProperty = None
                                geoDummyNode = None
                                for component in Globals.Components:
                                                maxProperty = component.PropertyList.Find('UDP3DSMAX')
                                                if maxProperty and 'GEODUMMYNODE' in maxProperty.Data:
                                                                geoDummyNode = component
                                                                break

                                if maxProperty == None and geoDummyNode == None:
                                                return None, None

                                geoDummy = None
                                dataList = maxProperty.Data.split('\n')
                                for data in dataList:
                                                if 'GEODUMMYNODE' in data:
                                                                geoDummy = data.replace('\r', '',)

                                geoDummyString = geoDummy.split(' ')[-1]

                                return geoDummyString, geoDummyNode

                def GetGeoParentModelNull(self):
                                '''
                                find the geo parent
                                geo is usually named the same as the file name. if the file name does nto match, it will
                                try find a 'head_000' object, which it will then attempt to trace back to the parent geo
                                object from this.

                                returns: FBModel - top level geometry scene object
                                '''
                                geoParentModelNull = None
                                for component in Globals.Components:
                                                # try to find the geo parent by checking for the filename as an FBModel
                                                if component.Name.lower() == self.fileName and isinstance(component, mobu.FBModel):
                                                                geoParentModelNull = component
                                if geoParentModelNull is None:
                                                # try find the geo parent by checking for the max property 'GEODUMMYNODE'
                                                geoDummyString, geoDummyNode = self.GetGeoDummyMaxProperty()
                                                if geoDummyString == 'TRUE' and geoDummyNode:
                                                                geoParentModelNull = geoDummyNode
                                if geoParentModelNull is None:
                                                # try find the geo parent by a piece of common geo
                                                headList = []
                                                headGeoModel ='head_000_'
                                                for component in Globals.Components:
                                                                if headGeoModel in component.Name.lower() and isinstance(component, mobu.FBModel):
                                                                                headList.append(component)
                                                if len(headList) == 1:
                                                                geoParentModelNull = headList[0].Parent
                                                elif len(headList) > 1:
                                                                QtGui.QMessageBox.warning(
                                                                                None,
                                                                                "Warning",
                                                                                "Found multiple occurances of the geo 'Head_000_'.  There should only be one.\nPlease ensure you remove any unused 'head' geo nulls.",
                                                                                QtGui.QMessageBox.Ok
                                                                )
                                                                return

                                                else:
                                                                QtGui.QMessageBox.warning(
                                                                                None,
                                                                                "Warning",
                                                                                "Unable to find geo parent.  Looking for a null named after the file name,\nor via a 'head_000_' null.",
                                                                                QtGui.QMessageBox.Ok
                                                                )
                                                                return
                                return geoParentModelNull

                def GetUnusedModelList(self):
                                '''
                                generates a list of unused models and lods

                                returns: List() - a list of scene objects
                                '''
                                lodParentList = []
                                deleteList = []
                                unusedModelList = ['collision', 'colbox', 'particle', 'col_box', 'colmesh', 'box001',
                                                   'col_mesh', 'constraint', 'skindata_', 'lod_camera', 'target001',
                                                   'marker_']

                                for component in Globals.Components:
                                                # add lods to parent list
                                                if '-l2' in component.Name.lower():
                                                                lodParentList.append(component)
                                                                continue
                                                elif '-l' in component.Name.lower():
                                                                lodParentList.append(component)
                                                                continue

                                                for unusedModel in unusedModelList:
                                                                if unusedModel in component.Name.lower():
                                                                                deleteList.append(component)

                                for lodParent in lodParentList:
                                                Scene.GetChildren(lodParent, deleteList)
                                return deleteList

                def LockAllConstraints(self):
                                exceptionList = ['CameraMover_Constraint',
                                                'CameraMoverInterest_Constraint',]
                                for constraint in Globals.Constraints:
                                                lockProperty = constraint.PropertyList.Find('Lock')
                                                if constraint.Name not in exceptionList and lockProperty:
                                                                lockProperty.Data = True

                def SetupCustomPropertiesForMainHierarchy(self):
                                '''
                                add rs and asset type custom properties to the main hierarchy of the asset
                                '''
                                Lib.rs_AssetSetupCustomProperties()

                                assetTypeProperty = self.parentModelNull.PropertyCreate('Asset_Type',
                                                                                        mobu.FBPropertyType.kFBPT_charptr,
                                                                                        "",
                                                                                        False,
                                                                                        True,
                                                                                        None)
                                assetTypeProperty.Data = "Animal"

                def CreateMasterNull(self, characterName):
                                sceneRootList = []
                                # get all root nulls in scene
                                for root in Globals.Scene.RootModel.Children:
                                                sceneRootList.append(root)
                                # create master scene null
                                null = mobu.FBModelNull(characterName)
                                # parent root nulls to master null
                                for root in sceneRootList:
                                                root.Parent = null
                                return

                def SetupZeroPose(self):
                                '''
                                add all rotations, for animals, to the prerotations. Was agreed we will not be changing the
                                values to zero them, but keeping the original max bind pose.
                                TODO: We will need to have a file to verify the bind pose hasnt changed between iterations.
                                '''
                                mainHierarchyList = []
                                Scene.GetChildren(self.skelRootModelNull, mainHierarchyList, "", True)

                                for item in mainHierarchyList:
                                                currentLclRotation = item.Rotation
                                                enableRotationDOFProperty = item.PropertyList.Find('Enable Rotation DOF')
                                                preRotationProperty = item.PropertyList.Find('Pre Rotation')
                                                if enableRotationDOFProperty and preRotationProperty:
                                                                enableRotationDOFProperty.Data = True
                                                                preRotationProperty.Data = mobu.FBVector3d(currentLclRotation)
                                                item.Rotation = mobu.FBVector3d(0,0,0)

                def SetupCharacterization(self, fileName, characterzationBool):
                                '''
                                read in xml file
                                create a dictionary of bones and link slots
                                characterize

                                returns: FBCharacter
                                '''
                                # get character
                                character = self.GetCharacter()
                                if not character:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "There is no character node present in the file./n/nEnsure you have hit the 'Prep Skeleton for XML' Button\nbefore doing this step.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # get skeletonDict, created from xml
                                skeletonDict, characterizeType = self.ReadSkeletonFBXSetupMetaFile(fileName)
                                if not skeletonDict:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "XML hasn't returned any data. See a TA for help.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # set up mapping
                                for key, value in skeletonDict.iteritems():
                                                bone = Scene.FindModelByName(key)
                                                if bone:
                                                                boneLinkProperty = character.PropertyList.Find(value[0])
                                                                if boneLinkProperty != None:
                                                                                boneLinkProperty.append(bone)

                                # characterize
                                if characterzationBool:
                                                
                                                characterize = character.SetCharacterizeOn(characterizeType)
                                                if not characterize:
                                                                QtGui.QMessageBox.warning(
                                                                                None,
                                                                                "Warning",
                                                                                "Unable to characterize - there must be a bone missing from the mapping.",
                                                                                QtGui.QMessageBox.Ok
                                                                )
                                                                return None

                                return character

                def SetupSkeletonColors(self):
                                '''
                                Edit the skeleton colors in the model properties
                                - different colors to represent the right, left and centre bones.
                                '''
                                # get skeletonDict, created from xml
                                skeletonDict, characterizeType = self.ReadSkeletonFBXSetupMetaFile(self.fileName)
                                if skeletonDict == None:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "XML hasn't returned any data. See a TA for help.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                for key, value in skeletonDict.iteritems():
                                                bone = Scene.FindModelByName(key)
                                                if bone and bone.PropertyList.Find('Color RGB'):
                                                                bone.PropertyList.Find('Color RGB').Data = mobu.FBColor(value[1])

                def SetupMoverControl(self):
                                '''
                                sets up the mover control - an FBModelMarker object

                                returns: FBModelMarker - 'mover_Control' scene object
                                '''
                                moverMarker = mobu.FBModelMarker("mover_Control")
                                moverMarker.Show = True
                                moverMarker.Look = mobu.FBMarkerLook.kFBMarkerLookHardCross
                                moverMarker.PropertyList.Find("Color RGB").Data = mobu.FBColor(0,1,0)

                                vector = mobu.FBVector3d()
                                self.moverModelNull.GetVector(vector, mobu.FBModelTransformationType.kModelRotation, True)
                                moverMarker.SetVector(vector, mobu.FBModelTransformationType.kModelRotation, True)
                                self.moverModelNull.GetVector(vector, mobu.FBModelTransformationType.kModelTranslation, True)
                                moverMarker.SetVector(vector, mobu.FBModelTransformationType.kModelTranslation, True)
                                mobu.FBSystem().Scene.Evaluate()

                                moverMarker.Parent = self.skelRootModelNull

                                relationConstraint = mobu.FBConstraintRelation('mover_Control')
                                moverMarkerSender = relationConstraint.SetAsSource(moverMarker)
                                relationConstraint.SetBoxPosition(moverMarkerSender, 0, 0)
                                moverReceiver = relationConstraint.ConstrainObject(self.moverModelNull)
                                relationConstraint.SetBoxPosition(moverReceiver, 300, 0)

                                Scene.ConnectBox(moverMarkerSender, 'Translation', moverReceiver, 'Translation')
                                Scene.ConnectBox(moverMarkerSender, 'Rotation', moverReceiver, 'Rotation')

                                relationConstraint.Active = False
                                return moverMarker

                def SetupToybox(self):
                                '''
                                sets up the toybox - an FBModelMarker object.

                                returns: FBModelMarker - 'ToyBox Marker' scene object
                                '''
                                toyBoxMarker = mobu.FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
                                toyBoxMarker.Look = mobu.FBMarkerLook.kFBMarkerLookHardCross
                                toyBoxMarker.Size = 500
                                toyBoxMarker.Color = mobu.FBColor(1, 0, 1)
                                toyBoxMarker.Parent = self.parentModelNull
                                toyBoxMarker.Translation.Data = self.moverModelNull.Translation.Data
                                toyBoxMarker.Rotation.Data = self.moverModelNull.Rotation.Data
                                rsCustomProperty = toyBoxMarker.PropertyCreate('rs_Type',
                                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                                               "",
                                                                               False,
                                                                               True,
                                                                               None)
                                rsCustomProperty.Data = "rs_Contacts"
                                return toyBoxMarker

                def SetupMoverCamera(self):
                                '''
                                sets up the mover camera - an FBCamera object

                                returns: FBCamera - 'MoverCamera' scene object
                                '''
                                moverCamera = mobu.FBCamera("MoverCamera")
                                moverCameraInterest = mobu.FBModelNull("MoverCamera_Interest")
                                moverCamera.Interest = moverCameraInterest
                                moverCamera.Show = True
                                moverCameraInterest.Show = True
                                moverCamConstraint = mobu.FBConstraintManager().TypeCreateConstraint(3)
                                moverCamConstraint.Name = "CameraMover_Constraint"
                                moverCamConstraint.ReferenceAdd (0,moverCamera)
                                moverCamConstraint.ReferenceAdd (1,self.moverModelNull)
                                moverCamConstraint.Snap()
                                moverCamInterestConstraint = mobu.FBConstraintManager().TypeCreateConstraint(3)
                                moverCamInterestConstraint.Name = "CameraMoverInterest_Constraint"
                                moverCamInterestConstraint.ReferenceAdd (0,moverCameraInterest)
                                moverCamInterestConstraint.ReferenceAdd (1,self.moverModelNull)
                                moverCamInterestConstraint.Snap()
                                moverCamera.Translation = mobu.FBVector3d(-319.68,100,0)
                                Scene.Align(moverCameraInterest, self.moverModelNull, True, True, True, False, False, False)
                                return moverCamera

                def ScaleDownNulls(self):
                                '''
                                nulls are set to a zero scale
                                '''
                                for component in Globals.Components:
                                                if isinstance(component, mobu.FBModelNull):
                                                                component.Size = 0

                def SetSliderControls(self):
                                '''
                                set dofs on slider controls
                                '''
                                # get slider parent controls
                                # (assuming they all have a parent with a name that startswith 'RECT_')
                                sliderParentList = []
                                for component in Globals.Components:
                                                if component.Name.startswith('RECT_') and isinstance(component, mobu.FBModel):
                                                                sliderParentList.append(component)

                                # select the sliders to check the dimensions
                                Scene.DeSelectAll()
                                for sliderParent in sliderParentList:
                                                sliderParent.Selected = True

                                # find what type of sliders are in the scene by checking the RECT_ object's dimensions
                                dimensionDict = Model.GetDimensionsOfSelectedModels()
                                for key, value in dimensionDict.iteritems():
                                                # get control child
                                                control = None
                                                for child in key.Children:
                                                                if 'text' not in child.Name.lower():
                                                                                control = child
                                                                                if control:
                                                                                                # set dofs
                                                                                                height = value[0]
                                                                                                width = value[1]
                                                                                                defaultValueAmount = 2
                                                                                                # get rect and control translations
                                                                                                keyTrns = mobu.FBVector3d()
                                                                                                controlTrns = mobu.FBVector3d()
                                                                                                key.GetVector(keyTrns)
                                                                                                control.GetVector(controlTrns)
                                                                                                if [round(keyTrns[0], 4), round(keyTrns[1], 4), round(keyTrns[2], 4)] == [round(controlTrns[0], 4), round(controlTrns[1], 4), round(controlTrns[2], 4)]:
                                                                                                                # if the control is at the pivot of the slider, 
                                                                                                                # it is likely this means there are 3 values: -1, 0 1
                                                                                                                # until we have better code support, we will go by this assumption.
                                                                                                                defaultValueAmount = 3
                                                                                                if height > width:
                                                                                                                # if height > width it is Y for control
                                                                                                                control.PropertyList.Find("Enable Translation DOF").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinZ").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxZ").Data = True
                                                                                                                if defaultValueAmount == 2:
                                                                                                                                control.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(0,0,0)
                                                                                                                                control.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,1,0)
                                                                                                                else:
                                                                                                                                control.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(0,-1,0)
                                                                                                                                control.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,1,0)
                                                                                                elif width > height:
                                                                                                                # if width > height it is X for control
                                                                                                                control.PropertyList.Find("Enable Translation DOF").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinZ").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxZ").Data = True
                                                                                                                if defaultValueAmount == 2:
                                                                                                                                control.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(0,0,0)
                                                                                                                                control.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(1,0,0)
                                                                                                                else:
                                                                                                                                control.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(-1,0,0)
                                                                                                                                control.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(1,0,0)
                                                                                                elif height == width:
                                                                                                                # if height == width it is Y & X for control
                                                                                                                control.PropertyList.Find("Enable Translation DOF").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMinZ").Data = True
                                                                                                                control.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(-1,-1,0)
                                                                                                                control.PropertyList.Find("TranslationMaxX").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxY").Data = True
                                                                                                                control.PropertyList.Find("TranslationMaxZ").Data = True
                                                                                                                control.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(1,1,0)
                                                                                else:
                                                                                                QtGui.QMessageBox.warning(
                                                                                                                None,
                                                                                                                "Warning",
                                                                                                                "Cannot find the control for {0}.\n\nUnable to set up the dofs for this slider.".format(key.Name),
                                                                                                                QtGui.QMessageBox.Ok
                                                                                                )
                                Scene.DeSelectAll()

                def GenerateAimConstraint(self, constraintName, constrainedObject, aimAtObject1, worldUpObject):
                                '''
                                creates an aim constraint and sets the properties
                                '''
                                aimConstraint = mobu.FBConstraintManager().TypeCreateConstraint(0)
                                aimConstraint.Name = constraintName
                                aimConstraint.ReferenceAdd(0, constrainedObject)
                                aimConstraint.ReferenceAdd(1, aimAtObject1)
                                aimConstraint.ReferenceAdd(2, worldUpObject)
                                aimConstraint.PropertyList.Find('AimVector').Data = mobu.FBVector3d(1,0,0)
                                aimConstraint.PropertyList.Find('UpVector').Data = mobu.FBVector3d(0,0,1)
                                aimConstraint.PropertyList.Find('WorldUpType').Data = 2
                                aimConstraint.PropertyList.Find('WorldUpVector').Data = mobu.FBVector3d(0,0,1)
                                aimConstraint.Snap()
                                return aimConstraint

                def SetupLookAtConstraints(self):
                                '''
                                sets up Lookat aim constraints for animals with MH Support nodes
                                '''
                                aimDict = {Scene.FindModelByName('MH_L_HandSupport'):(Scene.FindModelByName('MH_L_Hand'), Scene.FindModelByName('SKEL_L_Hand')),
                                           Scene.FindModelByName('MH_R_HandSupport'):(Scene.FindModelByName('MH_R_Hand'), Scene.FindModelByName('SKEL_L_Hand')),
                                           Scene.FindModelByName('MH_L_Finger00Support'):(Scene.FindModelByName('MH_L_Finger00'), Scene.FindModelByName('SKEL_L_Finger00')),
                                           Scene.FindModelByName('MH_R_Finger00Support'):(Scene.FindModelByName('MH_R_Finger00'), Scene.FindModelByName('SKEL_R_Finger00')),
                                           Scene.FindModelByName('MH_L_Toe0Support'):(Scene.FindModelByName('MH_L_Toe0'), Scene.FindModelByName('SKEL_L_Toe0')),
                                           Scene.FindModelByName('MH_R_Toe0Support'):(Scene.FindModelByName('MH_R_Toe0'), Scene.FindModelByName('SKEL_R_Toe0')),
                                           Scene.FindModelByName('MH_L_Toe1Support'):(Scene.FindModelByName('MH_L_Toe2'), Scene.FindModelByName('SKEL_L_Toe1')),
                                           Scene.FindModelByName('MH_R_Toe1Support'):(Scene.FindModelByName('MH_R_Toe2'), Scene.FindModelByName('SKEL_R_Toe1')),}

                                for key, value in aimDict.iteritems():
                                                if key and value[0] and value[1]:
                                                                self.GenerateAimConstraint('{0}_LOOKAT'.format(value[0].Name), value[0], key, value[1])

                def SetupShoulderConstraints(self):
                                '''
                                sets up shoulder aim constraints for animals with RBShoulderHelpers
                                '''
                                # check if the should helper rb exist, to see if we need these constraints
                                rShoulderHelper = Scene.FindModelByName('RB_R_ShoulderHelper')
                                lShoulderHelper = Scene.FindModelByName('RB_L_ShoulderHelper')
                                if rShoulderHelper and lShoulderHelper:
                                                # get scene bones needed
                                                rbRUpper = Scene.FindModelByName('RB_R_UpperArm')
                                                rForearm = Scene.FindModelByName('SKEL_R_Forearm')
                                                rUpper = Scene.FindModelByName('SKEL_R_UpperArm')
                                                rbLUpper = Scene.FindModelByName('RB_L_UpperArm')
                                                lForearm = Scene.FindModelByName('SKEL_L_Forearm')
                                                lUpper = Scene.FindModelByName('SKEL_L_UpperArm')
                                                if rbRUpper and rForearm and rUpper and rbLUpper and lForearm and lUpper:
                                                                # create aim constraints
                                                                self.GenerateAimConstraint('RB_R_ShoulderHelper_Aim', rbRUpper, rForearm, rUpper)
                                                                self.GenerateAimConstraint('RB_L_ShoulderHelper_Aim', rbLUpper, lForearm, lUpper)

                def SetupMuscleConstraints(self):
                                '''
                                sets up muscles -a joint squashing and stretching based on the distance between 2 points
                                '''
                                muscleBoneList = []
                                for component in Globals.Components:
                                                if 'MB_Muscle' in component.Name and isinstance(component, mobu.FBModel):
                                                                muscleBoneList.append(component)
                                                                continue

                                if not len(muscleBoneList) > 0:
                                                return

                                for muscleBone in muscleBoneList:
                                                muscleBoneSplitName = muscleBone.Name.split('Muscle_')
                                                muscleNumber = muscleBoneSplitName[-1]

                                                muscleStart = Scene.FindModelByName('MS_Muscle_{0}'.format(muscleNumber))
                                                muscleEnd = Scene.FindModelByName('ME_Muscle_{0}'.format(muscleNumber))
                                                skelBone = muscleEnd.Parent
                                                if muscleStart and muscleEnd and skelBone:
                                                                # apply wireframe to muscle meshes
                                                                muscleBone.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire

                                                                # create aim constraint
                                                                self.GenerateAimConstraint('{0}_Aim'.format(muscleBone.Name), muscleStart, muscleEnd, skelBone)

                                                                # get MS/ME length
                                                                msVector = mobu.FBVector3d()
                                                                muscleStart.GetVector(msVector, mobu.FBModelTransformationType.kModelTranslation, True)
                                                                meVector = mobu.FBVector3d()
                                                                muscleEnd.GetVector(meVector, mobu.FBModelTransformationType.kModelTranslation, True)
                                                                subVector = msVector.__sub__(meVector)
                                                                length = subVector.Length()
                                                                lengthDivided = float(length / 100)

                                                                # create relation constraint
                                                                relationConstraint = mobu.FBConstraintManager().TypeCreateConstraint(7)
                                                                relationConstraint.Name = '{0}_Scale_Relation'.format(muscleBone.Name)
                                                                relationConstraint.Active = True

                                                                msSender = relationConstraint.SetAsSource(muscleStart)
                                                                relationConstraint.SetBoxPosition(msSender, 0, 0)

                                                                meSender = relationConstraint.SetAsSource(muscleEnd)
                                                                relationConstraint.SetBoxPosition(meSender, 0, 200)

                                                                subtractVector = relationConstraint.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')
                                                                relationConstraint.SetBoxPosition(subtractVector, 400, 100)

                                                                lengthVector = relationConstraint.CreateFunctionBox("Vector", "Length")
                                                                relationConstraint.SetBoxPosition(lengthVector, 800, 100)

                                                                divideNumber = relationConstraint.CreateFunctionBox("Number", "Divide (a/b)")
                                                                relationConstraint.SetBoxPosition(divideNumber, 1600, 100)
                                                                divideNumberb = Scene.FindAnimationNode(divideNumber.AnimationNodeInGet(),'b')
                                                                divideNumberb.WriteData([lengthDivided])

                                                                divideNumber2 = relationConstraint.CreateFunctionBox("Number", "Divide (a/b)")
                                                                relationConstraint.SetBoxPosition(divideNumber2, 2100, 0)
                                                                divideNumber2b = Scene.FindAnimationNode(divideNumber2.AnimationNodeInGet(),'b')
                                                                divideNumber2b.WriteData([100])

                                                                divideNumber3 = relationConstraint.CreateFunctionBox("Number", "Divide (a/b)")
                                                                relationConstraint.SetBoxPosition(divideNumber3, 2100, 200)
                                                                divideNumber3a = Scene.FindAnimationNode(divideNumber3.AnimationNodeInGet(),'a')
                                                                divideNumber3a.WriteData([1])

                                                                multiplyNumber = relationConstraint.CreateFunctionBox("Number", "Multiply (a x b)")
                                                                relationConstraint.SetBoxPosition(multiplyNumber, 2500, 200)
                                                                multiplyNumberb = Scene.FindAnimationNode(multiplyNumber.AnimationNodeInGet(),'b')
                                                                multiplyNumberb.WriteData([100])

                                                                numberToVector = relationConstraint.CreateFunctionBox('Converters', 'Number to Vector')
                                                                relationConstraint.SetBoxPosition(numberToVector, 2900, 100)

                                                                mbReceiver = relationConstraint.ConstrainObject(muscleBone)
                                                                relationConstraint.SetBoxPosition(mbReceiver, 3300, 100)
                                                                mbReceiver.UseGlobalTransforms = False

                                                                Scene.ConnectBox(msSender, 'Translation', subtractVector, 'V1')
                                                                Scene.ConnectBox(meSender, 'Translation', subtractVector, 'V2')
                                                                Scene.ConnectBox(subtractVector, 'Result', lengthVector, 'Vector')
                                                                Scene.ConnectBox(lengthVector, 'Result', divideNumber, 'a')
                                                                Scene.ConnectBox(divideNumber, 'Result', divideNumber2, 'a')
                                                                Scene.ConnectBox(divideNumber, 'Result', divideNumber3, 'b')
                                                                Scene.ConnectBox(divideNumber3, 'Result', multiplyNumber, 'a')
                                                                Scene.ConnectBox(divideNumber2, 'Result', numberToVector, 'X')
                                                                Scene.ConnectBox(multiplyNumber, 'Result', numberToVector, 'Y')
                                                                Scene.ConnectBox(multiplyNumber, 'Result', numberToVector, 'Z')
                                                                Scene.ConnectBox(numberToVector, 'Result', mbReceiver, 'Lcl Scaling')

                def SetupInvertedJoints(self, character):
                                invertModelList = ['a_c_turtlesea_01',
                                                   'a_c_turtlesea_01a']

                                for model in invertModelList:
                                                if character.Name.lower() == model:
                                                                leftElbowInvert = character.PropertyList.Find('Inverted Left Elbow')
                                                                rightElbowInvert = character.PropertyList.Find('Inverted Right Elbow')
                                                                leftKneeInvert = character.PropertyList.Find('Inverted Left Knee')
                                                                rightKneeInvert = character.PropertyList.Find('Inverted Right Knee')

                                                                if leftElbowInvert and rightElbowInvert and leftKneeInvert and rightKneeInvert:
                                                                                leftElbowInvert.Data = True
                                                                                rightElbowInvert.Data = True
                                                                                leftKneeInvert.Data = False
                                                                                rightKneeInvert.Data = True

                def SetupFacingDir(self):
                                # Create OH_FacingDirection Arrow
                                ohFacingDir = Scene.FindModelByName('OH_FacingDir')
                                fileName = Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName)
                                mover = Scene.FindModelByName("mover")
                                skelRoot = Scene.FindModelByName("SKEL_ROOT")
                                if ohFacingDir:
                                                # create a zeroing null, so the facing direction can start at zero.
                                                zeroingNull = mobu.FBModelNull('ZeroingNull')
                                                zeroingNull.Parent = mover
                                                zeroingNull.SetVector(mobu.FBVector3d(0,0,0), mobu.FBModelTransformationType.kModelRotation, False)
                                                zeroingNull.SetVector(mobu.FBVector3d(-90,0,0), mobu.FBModelTransformationType.kModelRotation, True)
                                                zeroingNull.SetVector(mobu.FBVector3d(0,0,0), mobu.FBModelTransformationType.kModelTranslation, True)
                                                mobu.FBSystem().Scene.Evaluate()
                                                cone = mobu.FBCreateObject("Browsing/Templates/Elements/Primitives", "Cone", "Cone")
                                                cone.Name = 'OH_FacingDirection'
                                                cone.Parent = mover
                                                cone.Show = True
                                                cone.PropertyList.Find("GeometricTranslation").Data = mobu.FBVector3d(0,10,0)
                                                cone.Selected = True
                                                cone.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
                                                cone.Selected = False
                                                tag = cone.PropertyCreate('rs_Type', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
                                                tag.Data = "rs_Contacts"
                                                # cone properties
                                                cone.PropertyList.Find("GeometricTranslation").Data = mobu.FBVector3d(0,10,0)
                                                cone.PropertyList.Find("Scaling Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)
                                                cone.PropertyList.Find("Rotation Pivot (Auto Offset)").Data = mobu.FBVector3d(0, 0, 0)
                                                cone.SetVector(mobu.FBVector3d(-90,0,0), mobu.FBModelTransformationType.kModelRotation, True)
                                                Scene.Align(cone, skelRoot, True, True, True, False, False, False)
                                                mobu.FBSystem().Scene.Evaluate()
                                                # parent cone to zeroing null
                                                cone.Parent = zeroingNull
                                                mobu.FBSystem().Scene.Evaluate()
                                                # add to group
                                                mainGroup = None
                                                for iGroup in Globals.gGroups:
                                                                if iGroup.Name == fileName:
                                                                                mainGroup = iGroup
                                                if mainGroup:
                                                                lFacingDirecGroup = mobu.FBGroup('OH_FacingDirection')
                                                                lFacingDirecGroup.ConnectSrc(cone)
                                                                mainGroup.ConnectSrc(lFacingDirecGroup)
                                                                lFacingDirecGroup.Show = False
                                                # Create Relation Constraint
                                                lRelationCon = mobu.FBConstraintRelation('FacingArrow_OHBones')
                                                lSender = lRelationCon.SetAsSource(cone)
                                                lRelationCon.SetBoxPosition(lSender, 0, 0)
                                                lSender.UseGlobalTransforms = False
                                                lReceiver = lRelationCon.ConstrainObject(ohFacingDir)
                                                lRelationCon.SetBoxPosition(lReceiver, 350, 0)
                                                lReceiver.UseGlobalTransforms = False
                                                Scene.ConnectBox(lSender, 'Lcl Rotation', lReceiver, 'Lcl Rotation')
                                                mobu.FBSystem().Scene.Evaluate()
                                                cone.Scaling = mobu.FBVector3d(5.3,3.5,0.04)
                                                # set dof
                                                skelRootVector = mobu.FBVector3d()
                                                skelRoot.GetVector(skelRootVector, mobu.FBModelTransformationType.kModelTranslation, True)
                                                print skelRootVector[0], skelRootVector[1], skelRootVector[2]
                                                cone.PropertyList.Find("Enable Translation DOF").Data = True
                                                cone.PropertyList.Find("TranslationMinX").Data = True
                                                cone.PropertyList.Find("TranslationMinY").Data = True
                                                cone.PropertyList.Find("TranslationMinZ").Data = True
                                                cone.PropertyList.Find("TranslationMin" ).Data = mobu.FBVector3d(0,0,0)
                                                cone.PropertyList.Find("TranslationMaxX").Data = True
                                                cone.PropertyList.Find("TranslationMaxY").Data = True
                                                cone.PropertyList.Find("TranslationMaxZ").Data = True
                                                cone.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,0,skelRootVector[1])
                                                lRelationCon.Snap()
                                                # add constraint to folder
                                                for folder in Globals.Folders:
                                                                if folder.Name == 'OH_Constraints':
                                                                                lConFolder.Items.append(lRelationCon)
                                                                                break

                def SetupSafeBoneGroup(self):
                                '''
                                setup a group for the 'safe' bones
                                '''
                                safeBoneList = ['mover',
                                                 'SKEL_L_Clavicle',
                                                 'SKEL_L_UpperArm',
                                                 'SKEL_L_Forearm',
                                                 'SKEL_L_Finger00',
                                                 'SKEL_L_Finger01',
                                                 'SKEL_L_Finger02',
                                                 'SKEL_L_Finger10',
                                                 'SKEL_L_Finger11',
                                                 'SKEL_L_Finger12',
                                                 'SKEL_L_Finger20',
                                                 'SKEL_L_Finger21',
                                                 'SKEL_L_Finger22',
                                                 'SKEL_L_Finger30',
                                                 'SKEL_L_Finger31',
                                                 'SKEL_L_Finger32',
                                                 'SKEL_L_Finger40',
                                                 'SKEL_L_Finger40',
                                                 'SKEL_L_Finger41',
                                                 'SKEL_L_Finger42',
                                                 'SKEL_R_Clavicle',
                                                 'SKEL_R_UpperArm',
                                                 'SKEL_R_Forearm',
                                                 'SKEL_R_Finger00',
                                                 'SKEL_R_Finger01',
                                                 'SKEL_R_Finger02',
                                                 'SKEL_R_Finger10',
                                                 'SKEL_R_Finger11',
                                                 'SKEL_R_Finger12',
                                                 'SKEL_R_Finger20',
                                                 'SKEL_R_Finger21',
                                                 'SKEL_R_Finger22',
                                                 'SKEL_R_Finger30',
                                                 'SKEL_R_Finger31',
                                                 'SKEL_R_Finger32',
                                                 'SKEL_R_Finger40',
                                                 'SKEL_R_Finger41',
                                                 'SKEL_R_Finger42',
                                                 'SKEL_L_Thigh',
                                                 'SKEL_R_Thigh',
                                                 'SKEL_ROOT',
                                                 'SKEL_L_Calf',
                                                 'SKEL_L_Foot',
                                                 'SKEL_L_Toe0',
                                                 'SKEL_R_Calf',
                                                 'SKEL_R_Foot',
                                                 'SKEL_R_Toe0',
                                                 'SKEL_R_Hand',
                                                 'SKEL_L_Hand',
                                                 'SKEL_Spine0',
                                                 'SKEL_Spine1',
                                                 'SKEL_Spine2',
                                                 'SKEL_Spine3',
                                                 'SKEL_Spine4',
                                                 'SKEL_Spine5',
                                                 'SKEL_Spine6',
                                                 'SKEL_Neck0',
                                                 'SKEL_Neck1',
                                                 'SKEL_Neck2',
                                                 'SKEL_Head',
                                                 'SKEL_Pelvis',
                                                 'SKEL_Spine_Root',
                                                 'PH_L_Hand',
                                                 'PH_R_Hand',
                                                 'IK_L_Hand',
                                                 'IK_R_Hand',
                                                 'PH_L_Foot',
                                                 'PH_R_Foot']

                                if Scene.FindModelByName(safeBoneList[0]):
                                                safeBoneGroup = mobu.FBGroup("SafeBones")
                                                for bone in safeBoneList:
                                                                if Scene.FindModelByName(bone):
                                                                                safeBoneGroup.ConnectSrc(Scene.FindModelByName(bone))
                                                                else:
                                                                                None
                                return safeBoneGroup

                def SetupGeoVariationGroup(self, masterGroup):
                                '''
                                separates the character's geo into different groups, based on their numbers (000,001,002 etc.)
                                '''
                                # create default groups
                                accGroup = mobu.FBGroup("accessories")
                                propsGroup = mobu.FBGroup("props")
                                masterGroup.ConnectSrc(accGroup)
                                masterGroup.ConnectSrc(propsGroup)
                                # get list of model's geo
                                if self.geoParentModelNull == None:
                                                return
                                geoNullList = []
                                Scene.GetChildren(self.geoParentModelNull, geoNullList, "", False)
                                # organise geo into lists of their number types (000, 001, 002 etc.)
                                masterGeoNumberArray = []
                                for idx in range(26):
                                                geoNumberArray = []
                                                for geoNull in geoNullList:
                                                                if idx < 10:
                                                                                if "00{0}_".format(str(idx)) in geoNull.Name:
                                                                                                geoNumberArray.append(geoNull)
                                                                else:
                                                                                if "0{0}_".format(str(idx)) in geoNull.Name:
                                                                                                geoNumberArray.append(geoNull)
                                                if len(geoNumberArray) > 0:
                                                                masterGeoNumberArray.append(geoNumberArray)
                                # add geo to groups
                                groupList = []
                                accList = []
                                
                                for geoNumberArray in masterGeoNumberArray:
                                                if len(geoNumberArray) > 0:
                                                                numberGroup = mobu.FBGroup("var_0")
                                                                masterGroup.ConnectSrc(numberGroup)
                                                                groupList.append(numberGroup)
                                                                for geoNumber in geoNumberArray:
                                                                                if "acc" in geoNumber.Name.lower():
                                                                                                accGroup.ConnectSrc(geoNumber)
                                                                                elif "berd" in geoNumber.Name.lower():
                                                                                                accGroup.ConnectSrc(iGeo)
                                                                                elif "p_" in geoNumber.Name.lower():
                                                                                                propsGroup.ConnectSrc(geoNumber)
                                                                                                accList.append(geoNumber)
                                                                                else:
                                                                                                numberGroup.ConnectSrc(geoNumber)

                def SetupMHGroups(self):
                                '''
                                setup muscle groups
                                '''
                                mhLegList =['ME_Muscle_010',
                                                'MS_Muscle_008',
                                                'ET_Muscle_032',
                                                'MB_Muscle_008',
                                                'MS_Muscle_009',
                                                'ET_Muscle_031',
                                                'MB_Muscle_009',
                                                'MH_L_thigh',
                                                'MS_Muscle_010',
                                                'ET_Muscle_033',
                                                'MB_Muscle_010',
                                                'MH_L_Ankle',
                                                'MH_L_Foot',
                                                'MH_L_Toe0',
                                                'MH_L_BackHoof',
                                                'MH_L_Toe0Support',
                                                'MH_L_Toe1',
                                                'MH_L_Toe2',
                                                'MH_L_Toe1Support',
                                                'ME_Muscle_004',
                                                'MS_Muscle_005',
                                                'ET_Muscle_015',
                                                'MB_Muscle_005',
                                                'MS_Muscle_003',
                                                'ET_Muscle_014',
                                                'MB_Muscle_003',
                                                'MH_R_thigh',
                                                'MH_R_Ankle',
                                                'MS_Muscle_004',
                                                'ET_Muscle_016',
                                                'MB_Muscle_004',
                                                'MH_R_Foot',
                                                'MH_R_Toe0',
                                                'MH_R_BackHoof',
                                                'MH_R_Toe0Support',
                                                'MH_R_Toe1',
                                                'MH_R_Toe2',
                                                'MH_R_Toe1Support',
                                                'MS_Muscle_006',
                                                'ET_Muscle_034',
                                                'MB_Muscle_006',
                                                'ME_Muscle_011',
                                                'ME_Muscle_007',
                                                'ME_Muscle_006',
                                                'MH_L_Elbow',
                                                'MH_L_Forearm',
                                                'MS_Muscle_007',
                                                'ET_Muscle_035',
                                                'MB_Muscle_007',
                                                'MH_L_Knee',
                                                'MH_L_Hand',
                                                'MH_L_Finger00',
                                                'MH_L_FrontHoof',
                                                'MH_L_HandSupport',
                                                'MH_L_Finger00Support',
                                                'MH_L_Finger01',
                                                'MH_L_Finger02',
                                                'MH_L_Finger01Support',
                                                'MS_Muscle_001',
                                                'ET_Muscle_036',
                                                'MB_Muscle_001',
                                                'ME_Muscle_012',
                                                'ME_Muscle_001',
                                                'MH_R_Elbow',
                                                'ME_Muscle_002',
                                                'MH_R_Forearm',
                                                'MS_Muscle_002',
                                                'ET_Muscle_020',
                                                'MB_Muscle_002',
                                                'MH_R_Knee',
                                                'MH_R_Hand',
                                                'MH_R_Finger00',
                                                'MH_R_FrontHoof',
                                                'MH_R_HandSupport',
                                                'MH_R_Finger00Support',
                                                'MH_R_Finger01',
                                                'MH_R_Finger02',
                                                'MH_R_Finger01Support']

                                mhRibList = ['MH_RIB'
                                                'MH_RIB 7'
                                                'MH_RIB 6'
                                                'MH_RIB 5'
                                                'MH_RIB 4'
                                                'MH_RIB 3'
                                                'MH_RIB 2'
                                                'MH_RIB 1']

                                if Scene.FindModelByName(mhLegList[0]):
                                                mhGroup = mobu.FBGroup("MH")
                                                mhLegsGroup = mobu.FBGroup("MH_Legs")
                                                mhGroup.ConnectSrc(mhLegsGroup)
                                                for leg in mhLegList:
                                                                if Scene.FindModelByName(leg):
                                                                                mhLegsGroup.ConnectSrc(Scene.FindModelByName(leg))
                                                if Scene.FindModelByName(mhRibList[0]):
                                                                mhRibGroup = mobu.FBGroup("MH_Ribs")
                                                                mhGroup.ConnectSrc(mhRibGroup)
                                                                for rib in mhRibList:
                                                                                mhRibGroup.ConnectSrc(rib)
                                                return mhGroup

                def SetupSliderExpressionGroups(self):
                                # get slider controls
                                # (assuming they all have a parent with a name that startswith 'RECT_')
                                sliderParentList = []
                                for component in Globals.Components:
                                                if component.Name.startswith('RECT_') and isinstance(component, mobu.FBModel):
                                                                for child in component.Children:
                                                                                sliderParentList.append(child)
                                                                sliderParentList.append(component)
                                if len(sliderParentList) > 0:
                                                sliderGroup = mobu.FBGroup('ExpressionSliders')
                                                for sliderNull in sliderParentList:
                                                                sliderGroup.ConnectSrc(sliderNull)
                                                return sliderGroup
                                else:
                                                return

                def SetupIKGroup(self):
                                '''
                                setup a group for the IK lookat
                                '''
                                ikGroups = []
                                ikList =['IK_FacingDirection',
                                         'IK_Head_Lookat',
                                         'Look_At_Direction']
                                for ik in ikList:
                                                if Scene.FindModelByName(ik):
                                                                group = mobu.FBGroup(ik)
                                                                group.ConnectSrc(Scene.FindModelByName(ik))
                                                                ikGroups.append(group)
                                if len(ikGroups) > 0:
                                                ikGroup = mobu.FBGroup('IK')
                                                for group in ikGroups:
                                                                ikGroup.ConnectSrc(group)
                                                return ikGroup
                                else:
                                                return

                def SetupStirrupGroup(self):
                                '''
                                Horse Specific: setup stirrup groups
                                '''
                                stirrupRightObjectList = ['SH_R_Stirrup0',
                                                          'SH_R_Stirrup1',
                                                          'SH_R_Stirrup2',
                                                          'SH_R_Stirrup3']
                                stirrupLeftObjectList = ['SH_L_Stirrup0',
                                                         'SH_L_Stirrup1',
                                                         'SH_L_Stirrup2',
                                                         'SH_L_Stirrup3']

                                # add sirrups to a group, if they exist
                                if Scene.FindModelByName(stirrupRightObjectList[0]):
                                                mainStirrupGroup = mobu.FBGroup("SH_Stirrup")
                                                stirrupRGroup = mobu.FBGroup("SH_R_Stirrup")
                                                mainStirrupGroup.ConnectSrc(stirrupRGroup)
                                                for stirrup in stirrupRightObjectList:
                                                                if Scene.FindModelByName(stirrup):
                                                                                stirrupRGroup.ConnectSrc(Scene.FindModelByName(stirrup))
                                                stirrupLGroup = mobu.FBGroup("SH_L_Stirrup")
                                                mainStirrupGroup.ConnectSrc(stirrupLGroup)
                                                for stirrup in stirrupLeftObjectList:
                                                                if Scene.FindModelByName(stirrup):
                                                                                stirrupLGroup.ConnectSrc(Scene.FindModelByName(stirrup))
                                                return mainStirrupGroup

                def SetupFacialGroup(self):
                                '''
                                set groups for face controls and gui elements
                                '''
                                # ambient_ui
                                # create face group and sub-groups
                                faceList = []
                                ctrlList = []
                                frameList = []
                                for component in Globals.Components:
                                                if 'ambient_ui' in component.Name.lower():
                                                                Scene.GetChildren(component, faceList)

                                for face in faceList:
                                                if "CTRL" in face.Name:
                                                                ctrlList.append(face)
                                                elif "TEXT" in face.Name:
                                                                frameList.append(face)
                                                elif "RECT" in face.Name:
                                                                frameList.append(face)
                                                elif "Ambient_UI" in face.Name:
                                                                frameList.append(face)
                                                elif "FaceFX" in face.Name:
                                                                frameList.append(face)
                                                else :
                                                                ctrlList.append(face)

                                if len(ctrlList) > 0 or len(frameList) > 0:
                                                facialGroup = mobu.FBGroup("Ambient_Facial")
                                                faceCTRLGroup = mobu.FBGroup("Amb_Controls")
                                                facialGroup.ConnectSrc(faceCTRLGroup)
                                                faceCTRLGroup.Show = False
                                                faceCTRLGroup.Pickable = True
                                                faceCTRLGroup.Transformable = True
                                if len(frameList) > 0:
                                                faceFrameGroup = mobu.FBGroup("Amb_Frames")
                                                facialGroup.ConnectSrc(faceFrameGroup)
                                                faceFrameGroup.Show = False
                                                faceFrameGroup.Pickable = False
                                                faceFrameGroup.Transformable = True

                                # main face ui (for horse)
                                # face lists
                                facialCFacialRoot = Scene.FindModelByName('FACIAL_C_FacialRoot')
                                ctrlFaceGUI = Scene.FindModelByName('CTRL_faceGUI')
                                faceist = []
                                if ctrlFaceGUI == None and facialCFacialRoot == None:
                                                return

                                Scene.GetChildren(facialCFacialRoot, faceist)
                                Scene.GetChildren(ctrlFaceGUI, faceist)

                                eyeCtrlList = []
                                eyeGuiList = []
                                mainCtrlList = []
                                mainGuiList = []
                                facialBoneList = []
                                otherList = []
                                for face in faceist:
                                                # eye facial controls
                                                if 'ctrl_convergence' in face.Name.lower():
                                                                eyeCtrlList.append(face)
                                                elif '_lookat' in face.Name.lower():
                                                                eyeCtrlList.append(face)
                                                elif 'convergence' in face.Name.lower():
                                                                eyeGuiList.append(face)
                                                # main facial controls
                                                elif 'ctrl_' in face.Name.lower():
                                                                mainCtrlList.append(face)
                                                elif 'frm_' in face.Name.lower():
                                                                mainGuiList.append(face)
                                                                mainGuiList.append(ctrlFaceGUI)
                                                # facial bones
                                                elif 'facial_' in face.Name.lower():
                                                                facialBoneList.append(face)
                                                # other
                                                else:
                                                                otherList.append(face)

                                # group create
                                facialGroup = mobu.FBGroup("Facial")
                                if mainCtrlList > 0:
                                                mainFacialGroup = mobu.FBGroup("Main")
                                                ctrlMainFacialGroup = mobu.FBGroup("mainControls")
                                                guiMainFacialGroup = mobu.FBGroup("mainGUI")
                                                facialGroup.ConnectSrc(mainFacialGroup)
                                                mainFacialGroup.ConnectSrc(ctrlMainFacialGroup)
                                                mainFacialGroup.ConnectSrc(guiMainFacialGroup)
                                                for ctrl in mainCtrlList:
                                                                ctrlMainFacialGroup.ConnectSrc(ctrl)
                                                for gui in mainGuiList:
                                                                guiMainFacialGroup.ConnectSrc(gui)
                                if eyeCtrlList > 0:
                                                eyeFacialGroup = mobu.FBGroup("Eyes")
                                                ctrlEyeFacialGroup = mobu.FBGroup("eyeControls")
                                                guiEyeFacialGroup = mobu.FBGroup("eyeGUI")
                                                facialGroup.ConnectSrc(eyeFacialGroup)
                                                eyeFacialGroup.ConnectSrc(ctrlEyeFacialGroup)
                                                eyeFacialGroup.ConnectSrc(guiEyeFacialGroup)
                                                for ctrl in eyeCtrlList:
                                                                ctrlEyeFacialGroup.ConnectSrc(ctrl)
                                                for gui in eyeGuiList:
                                                                guiEyeFacialGroup.ConnectSrc(gui)
                                return facialGroup

                def SetupGroups(self):
                                '''
                                sets up groups
                                '''
                                jointList = []
                                geoList = []

                                # create main group
                                mainGroup = mobu.FBGroup(self.geoParentModelNull.Name)

                                # create the rest of the groups
                                Scene.GetChildren(self.skelRootModelNull, jointList)
                                Scene.GetChildren(self.geoParentModelNull, geoList)

                                dummyGroup = mobu.FBGroup("Dummy01")
                                dummyGroup.ConnectSrc(self.parentModelNull)
                                mainGroup.ConnectSrc(dummyGroup)
                                dummyGroup.Show = False
                                dummyGroup.Pickable = False
                                dummyGroup.Transformable = False

                                moverGroup = mobu.FBGroup("mover")
                                moverGroup.ConnectSrc(self.moverModelNull)
                                mainGroup.ConnectSrc(moverGroup)
                                moverGroup.Show = False

                                if jointList >= 1:
                                                jointGroup = mobu.FBGroup("Joints")
                                                mainGroup.ConnectSrc(jointGroup)
                                                for joint in jointList:
                                                                jointGroup.ConnectSrc(joint)

                                safeBoneGroup = self.SetupSafeBoneGroup()
                                if safeBoneGroup:
                                                mainGroup.ConnectSrc(safeBoneGroup)

                                sliderGroup = self.SetupSliderExpressionGroups()
                                if sliderGroup:
                                                mainGroup.ConnectSrc(sliderGroup)

                                if geoList >= 1:
                                                geoGroup = mobu.FBGroup("GeoVariations")
                                                mainGroup.ConnectSrc(geoGroup)
                                                self.SetupGeoVariationGroup(geoGroup)
                                                geoGroup.Pickable = False
                                                geoGroup.Transformable = False

                                faceGroup = self.SetupFacialGroup()
                                if faceGroup:
                                                mainGroup.ConnectSrc(faceGroup)

                                mhGroup = self.SetupMHGroups()
                                if mhGroup:
                                                mainGroup.ConnectSrc(mhGroup)

                                ikGroup = self.SetupIKGroup()
                                if ikGroup:
                                                mainGroup.ConnectSrc(ikGroup)

                                stirrupGroup = self.SetupStirrupGroup()
                                if stirrupGroup:
                                                mainGroup.ConnectSrc(stirrupGroup)
                                return

                def SetupSkelConstraints(self):
                                '''
                                sets up additional constraints for animals

                                SkelRoot_Pelvis: Parent Child Constraint
                                SKEL_Forearm_Relation: Relation Constraint
                                '''
                                # parent child constraint for SKEL_Root to SKEL_Pelvis
                                pelvisModelNull = Scene.FindModelByName('SKEL_Pelvis')
                                if pelvisModelNull != None:
                                                parentChildConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
                                                parentChildConstraint.Name = ("SkelRoot_Pelvis")
                                                parentChildConstraint.ReferenceAdd (0, self.skelRootModelNull)
                                                parentChildConstraint.ReferenceAdd (1, pelvisModelNull)
                                                parentChildConstraint.Snap()

                                # rotation constraint for SKEL_SADDLE (horse)
                                skelSaddle = Scene.FindModelByName('SKEL_SADDLE')
                                if skelSaddle and self.moverModelNull:
                                                rotationConstraint = Constraint.CreateConstraint(Constraint.ROTATION)
                                                rotationConstraint.Name = ("SKEL_SADDLE_mover_Rotation")
                                                rotationConstraint.ReferenceAdd (0, skelSaddle)
                                                rotationConstraint.ReferenceAdd (1, self.moverModelNull)
                                                rotationConstraint.Snap()

                                # relation constraint for SKEL_Forearm
                                lForearmModelNull = Scene.FindModelByName('SKEL_L_Forearm')
                                rForearmModelNull = Scene.FindModelByName('SKEL_R_Forearm')
                                lUpperArmExtModelNull = Scene.FindModelByName('SKEL_L_UpperArm_ext')
                                rUpperArmExtModelNull = Scene.FindModelByName('SKEL_R_UpperArm_ext')

                                skelArmList = [lForearmModelNull,
                                               rForearmModelNull,
                                               lUpperArmExtModelNull,
                                               rUpperArmExtModelNull]

                                for skelArm in skelArmList:
                                                if skelArm is None:
                                                                return

                                relationConstraint = mobu.FBConstraintRelation('SKEL_Forearm_Relation')

                                lForearmSender = relationConstraint.SetAsSource(lForearmModelNull)
                                lForearmSender.UseGlobalTransforms = False
                                relationConstraint.SetBoxPosition(lForearmSender, 0, 0)
                                vectorToNumberBox = relationConstraint.CreateFunctionBox('Converters', 'Vector to Number')
                                relationConstraint.SetBoxPosition(vectorToNumberBox, 300, 0)
                                multiplyBox = relationConstraint.CreateFunctionBox('Number', 'Multiply (a x b)')
                                relationConstraint.SetBoxPosition(multiplyBox, 600, 0)
                                multiplyBox_a = Scene.FindAnimationNode( multiplyBox.AnimationNodeInGet(),'b' )
                                multiplyBox_a.WriteData([-0.001])
                                numberToVectorBox = relationConstraint.CreateFunctionBox('Converters', 'Number to Vector')
                                relationConstraint.SetBoxPosition(numberToVectorBox, 900, 0)
                                lUpperArmExtReceiver = relationConstraint.ConstrainObject(lUpperArmExtModelNull)
                                relationConstraint.SetBoxPosition(lUpperArmExtReceiver, 1200, 0)
                                lUpperArmExtReceiver.UseGlobalTransforms = False

                                rForearmSender = relationConstraint.SetAsSource(rForearmModelNull)
                                rForearmSender.UseGlobalTransforms = False
                                relationConstraint.SetBoxPosition(rForearmSender, 0, 200)
                                vectorToNumberBox2 = relationConstraint.CreateFunctionBox('Converters', 'Vector to Number')
                                relationConstraint.SetBoxPosition(vectorToNumberBox2, 300, 200)
                                multiplyBox2 = relationConstraint.CreateFunctionBox('Number', 'Multiply (a x b)')
                                relationConstraint.SetBoxPosition(multiplyBox2, 600, 200)
                                multiplyBox2_a = Scene.FindAnimationNode( multiplyBox2.AnimationNodeInGet(),'b' )
                                multiplyBox2_a.WriteData([-0.001])
                                numberToVectorBox2 = relationConstraint.CreateFunctionBox('Converters', 'Number to Vector')
                                relationConstraint.SetBoxPosition(numberToVectorBox2, 900, 200)
                                rUpperArmExtReceiver = relationConstraint.ConstrainObject(rUpperArmExtModelNull)
                                relationConstraint.SetBoxPosition(rUpperArmExtReceiver, 1200, 200)
                                rUpperArmExtReceiver.UseGlobalTransforms = False

                                Scene.ConnectBox(lForearmSender, 'Lcl Rotation', vectorToNumberBox, 'V')
                                Scene.ConnectBox(vectorToNumberBox, 'Y', multiplyBox, 'a')
                                Scene.ConnectBox(multiplyBox, 'Result', numberToVectorBox, 'Y')
                                Scene.ConnectBox(numberToVectorBox, 'Result', lUpperArmExtReceiver, 'Lcl Translation')

                                Scene.ConnectBox(rForearmSender, 'Lcl Rotation', vectorToNumberBox2, 'V')
                                Scene.ConnectBox(vectorToNumberBox2, 'Y', multiplyBox2, 'a')
                                Scene.ConnectBox(multiplyBox2, 'Result', numberToVectorBox2, 'Y')
                                Scene.ConnectBox(numberToVectorBox2, 'Result', rUpperArmExtReceiver, 'Lcl Translation')

                                relationConstraint.Active = True
                                return

                def SetupCharacterExtensions(self, character):
                                '''
                                sets up character extensions for animals
                                SKEL_Spine_Root, mover, SKEL_Root are all added
                                '''
                                spineRootModelNull = Scene.FindModelByName("SKEL_Spine_Root")

                                extensionObjectList = ['mover',
                                                       'mover_Control',
                                                       'SKEL_ROOT',
                                                       'SKEL_Spine_Root',
                                                       'IK_R_Hand',
                                                       'IK_L_Hand',
                                                       'IK_R_Foot',
                                                       'IK_L_Foot',
                                                       'PH_R_Hand',
                                                       'PH_L_Hand',
                                                       'PH_R_Foot',
                                                       'PH_L_Foot',
                                                       'PH_R_Heel',
                                                       'PH_L_Heel',
                                                       'PH_R_FrontHeel',
                                                       'PH_L_FrontHeel',
                                                       'PH_bridle_attach_R',
                                                       'PH_bridle_attach_L',
                                                       'PH_bridle_wagon_R',
                                                       'PH_bridle_wagon_L',
                                                       'PH_saddle_attach_R',
                                                       'PH_saddle_attach_L']
                                tailObjectList = ['SKEL_Tail0',
                                                  'SKEL_Tail1',
                                                  'SKEL_Tail2',
                                                  'SKEL_Tail3',
                                                  'SKEL_Tail4',
                                                  'SKEL_Tail5',
                                                  'SKEL_Tail_05_NUB',
                                                  'SKEL_Tail_05_A',
                                                  'SKEL_Tail_05_B',
                                                  'SKEL_Tail_05_C',
                                                  'SKEL_Tail_05_D',
                                                  'SKEL_Tail_05_E',
                                                  'SKEL_Tail_05_F',]
                                stirrupRightObjectList = ['SH_R_Stirrup0',
                                                          'SH_R_Stirrup1',
                                                          'SH_R_Stirrup2',
                                                          'SH_R_Stirrup3']
                                stirrupLeftObjectList = ['SH_L_Stirrup0',
                                                         'SH_L_Stirrup1',
                                                         'SH_L_Stirrup2',
                                                         'SH_L_Stirrup3']
                                shoulderControlList = ['R_ClavicleExpression',
                                                       'L_ClavicleExpression',
                                                       'R_Clavicle',
                                                       'L_Clavicle']
                                shoulderControlList = ['R_ClavicleExpression',
                                                       'L_ClavicleExpression',
                                                       'R_Clavicle',
                                                       'L_Clavicle']

                                # main extensions creation
                                characterExtension = mobu.FBCharacterExtension('Char_Extension')
                                characterExtension.Label = 'Char_Extension'
                                character.AddCharacterExtension(characterExtension)
                                customProperty = characterExtension.PropertyCreate('rs_Type',
                                                                                   mobu.FBPropertyType.kFBPT_charptr,
                                                                                   "",
                                                                                   False, True, None)
                                customProperty.Data = "rs_Extensions"
                                for extensionobject in extensionObjectList:
                                                if Scene.FindModelByName(extensionobject):
                                                                mobu.FBConnect(Scene.FindModelByName(extensionobject), characterExtension)

                                # stirrup extensions
                                if Scene.FindModelByName(stirrupRightObjectList[0]):

                                                stirrupLExtension = mobu.FBCharacterExtension('SH_L_Stirrup')
                                                stirrupLExtension.Label = 'SH_L_Stirrup'
                                                character.AddCharacterExtension(stirrupLExtension)
                                                customProperty = stirrupLExtension.PropertyCreate('rs_Type',
                                                                                                  mobu.FBPropertyType.kFBPT_charptr,
                                                                                                  "",
                                                                                                  False, True, None)
                                                customProperty.Data = "rs_Extensions"
                                                for extensionObject in stirrupLeftObjectList:
                                                                if Scene.FindModelByName(extensionObject):
                                                                                mobu.FBConnect(Scene.FindModelByName(extensionObject), stirrupLExtension)

                                                stirrupRExtension = mobu.FBCharacterExtension('SH_R_Stirrup')
                                                stirrupRExtension.Label = 'SH_R_Stirrup'
                                                character.AddCharacterExtension(stirrupRExtension)
                                                customProperty = stirrupRExtension.PropertyCreate('rs_Type',
                                                                                                  mobu.FBPropertyType.kFBPT_charptr,
                                                                                                  "",
                                                                                                  False, True, None)
                                                customProperty.Data = "rs_Extensions"
                                                for extensionObject in stirrupRightObjectList:
                                                                if Scene.FindModelByName(extensionObject):
                                                                                mobu.FBConnect(Scene.FindModelByName(extensionObject), stirrupRExtension)
                                # shoulder control extension
                                if Scene.FindModelByName(shoulderControlList[0]):
                                                shoulderControlExtension = mobu.FBCharacterExtension('Shoulder_Controls')
                                                shoulderControlExtension.Label = 'Shoulder_Controls'
                                                character.AddCharacterExtension(shoulderControlExtension)
                                                customProperty = shoulderControlExtension.PropertyCreate('rs_Type',
                                                                                                         mobu.FBPropertyType.kFBPT_charptr,
                                                                                                         "",
                                                                                                         False, True, None)
                                                customProperty.Data = "rs_Extensions"
                                                for extensionObject in shoulderControlList:
                                                                if Scene.FindModelByName(extensionObject):
                                                                                mobu.FBConnect(Scene.FindModelByName(extensionObject), shoulderControlExtension)
                                # tail extension
                                if Scene.FindModelByName(tailObjectList[0]):
                                                tailExtension = mobu.FBCharacterExtension('SKEL_Tail')
                                                tailExtension.Label = 'SKEL_Tail'
                                                character.AddCharacterExtension(tailExtension)
                                                customProperty = tailExtension.PropertyCreate('rs_Type',
                                                                                                         mobu.FBPropertyType.kFBPT_charptr,
                                                                                                         "",
                                                                                                         False, True, None)
                                                customProperty.Data = "rs_Extensions"
                                                for extensionObject in tailObjectList:
                                                                if Scene.FindModelByName(extensionObject):
                                                                                mobu.FBConnect(Scene.FindModelByName(extensionObject), tailExtension)

                                # remove extra character constraint
                                for folder in Globals.Folders:
                                                if folder.Name == 'Constraints 1':
                                                                for item in folder.Items:
                                                                                if isinstance(item, mobu.FBCharacter):
                                                                                                folder.Items.remove(item)
                                return

                def SetupFolders(self):
                                '''
                                sets up folders.
                                adds items in each component to a folder.
                                '''
                                Creation.rs_CreateFolderAddComponents(mobu.FBSystem().Scene.Materials, "Materials")
                                Creation.rs_CreateFolderAddComponents(mobu.FBSystem().Scene.Textures, "Textures")
                                Creation.rs_CreateFolderAddComponents(mobu.FBSystem().Scene.Shaders, "Shaders")
                                Creation.rs_CreateFolderAddComponents(mobu.FBSystem().Scene.VideoClips, "Videos")
                                # remove floating folder generated by IK_Lookat Setup (but needed for character setup)
                                for folder in Globals.Folders:
                                                if folder.Name == 'Constraints 1':
                                                                folder.FBDelete()
                                                                break
                                Creation.rs_CreateFolderAddComponents(mobu.FBSystem().Scene.Constraints, "Constraints")
                                return


                def CheckModelXMLExists(self, fileName):
                                '''
                                Check if an xml file exists for model

                                returns: File Path - path to the xml file
                                '''
                                # path to xml
                                metaPath = os.path.join(
                                                Config.Project.Path.Root,
                                                'art',
                                                'animation',
                                                'resources',
                                                'characters',
                                                'skeletonSetupXML',
                                                fileName + '.xml'
                                )
                                # perforce sync file
                                Perforce.Sync(metaPath)

                                # check if file exists
                                if not os.path.exists(metaPath):
                                                return

                                return metaPath

                def PrepModelForCharacterization(self):
                                '''
                                Prep file and model for characterization process
                                prep, characterisation and final setup stages are separated in case the model does not have an existing xml.

                                returns: FBCharacter
                                '''
                                # check if a character node already exists
                                character = self.GetCharacter()
                                if character:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "This file has a character node present, please make sure the file is a fresh\nexport from max, unedited, before running the prep process.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # start player control at frame 0
                                mobu.FBPlayerControl().LoopStart = mobu.FBTime(0,0,0,0)

                                # remove animation
                                mobu.FBSystem().CurrentTake.ClearAllProperties(False)

                                # parent geo parent to dummy
                                self.geoParentModelNull.Parent = self.parentModelNull

                                # remove unused models
                                deleteList = self.GetUnusedModelList()
                                if len(deleteList) > 1:
                                                for delete in deleteList:
                                                                try:
                                                                                delete.FBDelete()
                                                                except:
                                                                                print "Cannot delete unused item. Likely it doesnt exist (already deleted)."

                                # add wire shader to mover
                                self.moverModelNull.Selected = True
                                self.moverModelNull.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
                                self.moverModelNull.Selected = False
                                self.moverModelNull.PropertyList.Find("Visibility").Data = True

                                # zero dummy pre-rotations, add dummy's rotation back in 
                                # -it is zero'd when removing prerotation
                                self.parentModelNull.PropertyList.Find("Pre Rotation").Data = mobu.FBVector3d(0,0,0)
                                mobu.FBDisconnect(self.moverModelNull, self.parentModelNull)
                                self.parentModelNull.Rotation = mobu.FBVector3d(-90,0,0)
                                mobu.FBConnect(self.moverModelNull, self.parentModelNull)
                                mobu.FBSystem().Scene.Evaluate()

                                # move current lcl rotations to pre rotation property, so we can zero out lcl rotation
                                self.SetupZeroPose()

                                # create character
                                character = mobu.FBCharacter(self.geoParentModelNull.Name)
                                return character

                def GetCharacter(self):
                                characterList = Globals.Characters
                                if len (characterList) > 0:
                                                character = characterList[0]
                                                return character
                                return None

                def FinalSetupAndCleanups(self):
                                '''
                                Final stages of setup after characterisation
                                prep, characterisation and final setup stages are separated in case the model does not
                                have an existing xml.
                                '''
                                # get character
                                character = self.GetCharacter()
                                if character == None:
                                                return

                                # set skeleton colors
                                self.SetupSkeletonColors()

                                # set up mover control
                                moverControlModelNull = self.SetupMoverControl()

                                # set up toybox
                                self.SetupToybox()

                                # setup mover camera
                                self.SetupMoverCamera()

                                # setup some SKEL contraints
                                self.SetupSkelConstraints()

                                # setup character extensions
                                self.SetupCharacterExtensions(character)

                                # setup inverted joints (if applies)
                                self.SetupInvertedJoints(character)

                                # setup muscle constraints
                                self.SetupMuscleConstraints()

                                # setup shoulder helper constraints
                                self.SetupShoulderConstraints()

                                # setup look at foot and hand constraints
                                self.SetupLookAtConstraints()

                                # setup groups
                                self.SetupGroups()

                                # scale down the nulls
                                self.ScaleDownNulls()

                                # lock constraints
                                self.LockAllConstraints()

                                # set slider controls
                                self.SetSliderControls()

                                # setup custom properties
                                self.SetupCustomPropertiesForMainHierarchy()

                                # set up facing direction
                                self.SetupFacingDir()

                                # setup folders
                                self.SetupFolders()

                                # set up lookat rigs
                                IngameCharacterIKControls.rs_HeadLookAt()

                                # setup master null
                                self.CreateMasterNull(character.Name)

                def AnimalSetup(self):
                                '''
                                set up the character
                                '''
                                # check if character already exists - if it does we know the file has been edited 
                                # user will need to export a fresh file from max.
                                character = self.GetCharacter()
                                if character:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "This file has a character node present, please make sure the file is a fresh\nexport from max, unedited, before running the prep process.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return False

                                # check if model has an available .xml for characterisation
                                if not self.CheckModelXMLExists(self.fileName):
                                                DialogWithCustomButton()

                                                # this return is to tell the ui to enable/disable relevant buttons for manual
                                                # characterizing
                                                return True

                                # prep the model for characterization
                                character = self.PrepModelForCharacterization()

                                # set up characterization
                                self.SetupCharacterization(self.fileName, True)

                                # final setup and cleanups
                                self.FinalSetupAndCleanups()

                                # user feedback
                                QtGui.QMessageBox.information(
                                                None,
                                                "Setup Complete",
                                                "Model has now completed setup.",
                                                QtGui.QMessageBox.Ok
                                )
                                return False

                def PrepCharacterForManualCharacterization(self):
                                '''
                                initial prep for the character, before characterization is carried out
                                '''
                                filePrepped = self.PrepModelForCharacterization()
                                if filePrepped:
                                                QtGui.QMessageBox.information(
                                                                None,
                                                                "Prep Complete",
                                                                "Characterization prep complete.\n\nPlease add the relevant bones to the characterization slots and hit 'Create Skeleton XML'\nbutton once the character has been characterized.",
                                                                QtGui.QMessageBox.Ok
                                                )

                def CreateSkeletonFBXSetupMetaFile(self):
                                '''
                                create the xml file for the character

                                xml contains:
                                bone scene object
                                characterization slot
                                bone's Rotation
                                bone's Translation
                                bone's Scaling
                                color - based of whether it a a left, right or center bone

                                returns: FBCharacter
                                '''
                                # make sure the user has the latest .psc definitions file
                                definitionsFilePath = os.path.join(Config.Project.Path.Root,
                                                                   'assets',
                                                                   'metadata',
                                                                   'definitions',
                                                                   'tools',
                                                                   'motionbuilder',
                                                                   'skeletonFBXSetup.psc')
                                Perforce.Sync(definitionsFilePath)

                                # get character and error check
                                character = self.GetCharacter()
                                if character == None:
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "There is no character node present in the file./n/nEnsure you have hit the 'Prep Skeleton for XML' Button\nbefore doing this step.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # get characterize type property and data
                                characterTypeProperty = character.PropertyList.Find("Character Type")
                                characterTypeData = 0
                                if characterTypeProperty:
                                                characterTypeData = characterTypeProperty.Data

                                # get psc file and error check
                                if not os.path.exists(definitionsFilePath):
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "Unable to find .psc definitions file.\n\nCheck you have the following file:\n\n{0}".format(definitionsFilePath),
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # check characterization
                                if not character.GetCharacterize():
                                                QtGui.QMessageBox.warning(
                                                                None,
                                                                "Warning",
                                                                "Character is not characterized - check all of the Base Slots have been\nfilled and that you have checked the characterize button On.",
                                                                QtGui.QMessageBox.Ok
                                                )
                                                return

                                # get slot and bone linked
                                characterizeDict = {}
                                for prop in character.PropertyList:
                                                if prop.Name.endswith('Link'):
                                                                for idx in range(prop.GetSrcCount()):
                                                                                slotBone = prop.GetSrc(idx)
                                                                                characterizeDict[slotBone] = prop.Name
                                                                                continue

                                # create a dict of all of the SKEL nulls
                                skelList = []
                                skeletonDict = {}
                                Scene.GetChildren(self.skelRootModelNull, skelList, "", True)

                                for item in skelList:
                                                if not item.Name.startswith('SKEL_'):
                                                                skelList.remove(item)

                                for item in skelList:
                                                slotBone = ''
                                                for key, value in characterizeDict.iteritems():
                                                                if item == key:
                                                                                slotBone = value
                                                                                break
                                                # get color
                                                if '_L_' in item.Name:
                                                                # green
                                                                color = mobu.FBVector3d(0.4, 0.7, 0)
                                                elif '_R_' in item.Name:
                                                                # orange
                                                                color = mobu.FBVector3d(0.9, 0.4, 0)
                                                else:
                                                                # blue
                                                                color = mobu.FBVector3d(0, 0.2, 0.65)

                                                skeletonDict[item] = slotBone, item.Rotation, item.Translation, item.Scaling, color

                                # create meta file
                                metaFile = Metadata.CreateMetaFile("SkeletonSetupFile");
                                metaFile.ModelName = self.fileName
                                metaFile.CharacterizeType = str(characterTypeData)

                                for key, value in skeletonDict.iteritems():
                                                metaItem = metaFile.SkeletonProperties.Create()
                                                metaItem.SKELName = key.Name
                                                metaItem.CharacterizeSlot = value[0]
                                                metaItem.SKELLclRot.X = value[1][0]
                                                metaItem.SKELLclRot.Y = value[1][1]
                                                metaItem.SKELLclRot.Z = value[1][2]
                                                metaItem.SKELLclTrns.X = value[2][0]
                                                metaItem.SKELLclTrns.Y = value[2][1]
                                                metaItem.SKELLclTrns.Z = value[2][2]
                                                metaItem.SKELLclScale.X = value[3][0]
                                                metaItem.SKELLclScale.Y = value[3][1]
                                                metaItem.SKELLclScale.Z = value[3][2]
                                                metaItem.SKELColor.X = value[4][0]
                                                metaItem.SKELColor.Y = value[4][1]
                                                metaItem.SKELColor.Z = value[4][2]

                                metaPath = os.path.join(Config.Project.Path.Root,
                                                        'art',
                                                        'animation',
                                                        'resources',
                                                        'characters',
                                                        'skeletonSetupXML',
                                                        self.fileName + '.xml')

                                metaFile.SetFilePath(metaPath)
                                metaFile.Save()

                                # mark xml for add in p4
                                Perforce.Add(metaPath)

                                # finish setup
                                self.FinalSetupAndCleanups()

                                QtGui.QMessageBox.information(
                                                None,
                                                "Setup Complete",
                                                "Skeleton XML Saved and Marked for Add in Perforce:\n{0}\n\nModel has completed setup and is now ready to be checked over and\nsubmitted to Perforce.".format(metaPath),
                                                QtGui.QMessageBox.Ok
                                )
                                return character

                def ReadSkeletonFBXSetupMetaFile(self, fileName):
                                '''
                                read back xml

                                returns: Dictionary -
                                Key:
                                bone scene object
                                Values:
                                characterization slot
                                bone's Rotation
                                bone's Translation
                                bone's Scaling
                                color
                                '''
                                skeletonDict = {}

                                # read xml
                                metaPath = self.CheckModelXMLExists(fileName)
                                if metaPath == None:
                                                return

                                # parse xml
                                metaFile = Metadata.ParseMetaFile(metaPath)

                                # create dict from metadata
                                for skelProperty in metaFile.SkeletonProperties:
                                                skelName = str(skelProperty.SKELName)
                                                characterizeSlot = str(skelProperty.CharacterizeSlot)
                                                skelColorX = float(skelProperty.SKELColor.X)
                                                skelColorY = float(skelProperty.SKELColor.Y)
                                                skelColorZ = float(skelProperty.SKELColor.Z)
                                                skelColor = (skelColorX, skelColorY, skelColorZ)

                                                skeletonDict[skelName] = characterizeSlot, skelColor


                                # set characterize type property data
                                characterizeType = metaFile.CharacterizeType
                                if characterizeType == 0:
                                                characterizeType = True
                                else:
                                                characterizeType = False

                                return skeletonDict, characterizeType