from pyfbsdk import *
import re
import os

import RS.Globals
import RS.Perforce

wrinkleMaps =  [ "HEAD_Wrinkle_A", "HEAD_Wrinkle_B" ]

size = 512, 512

mapDict =  {"Head_WA-000":"HEAD_Wrinkle_AMask_0__A.tga",
            "Head_WA-001":"HEAD_Wrinkle_AMask_0__B.tga",
            "Head_WA-002":"HEAD_Wrinkle_AMask_0__C.tga",
            "Head_WA-003":"HEAD_Wrinkle_AMask_0__D.tga",
            
            "Head_WA-004":"HEAD_Wrinkle_AMask_1__A.tga",
            "Head_WA-005":"HEAD_Wrinkle_AMask_1__B.tga",
            "Head_WA-006":"HEAD_Wrinkle_AMask_1__C.tga",
            "Head_WA-007":"HEAD_Wrinkle_AMask_1__D.tga",
            
            "Head_WA-008":"HEAD_Wrinkle_AMask_2__A.tga",
            "Head_WA-009":"HEAD_Wrinkle_AMask_2__B.tga",
            "Head_WA-010":"HEAD_Wrinkle_AMask_2__C.tga",
            "Head_WA-011":"HEAD_Wrinkle_AMask_2__D.tga",
            
            "Head_WB-000":"HEAD_Wrinkle_BMask_0__A.tga",
            "Head_WB-001":"HEAD_Wrinkle_BMask_0__B.tga",
            "Head_WB-002":"HEAD_Wrinkle_BMask_3__C.tga",
            "Head_WB-003":"HEAD_Wrinkle_BMask_3__D.tga",
            
            "Head_WB-004":"HEAD_Wrinkle_BMask_4__A.tga",
            "Head_WB-005":"HEAD_Wrinkle_BMask_4__B.tga",
            "Head_WB-006":"HEAD_Wrinkle_BMask_4__C.tga",
            "Head_WB-007":"HEAD_Wrinkle_BMask_4__D.tga",
            
            "Head_WB-008":"HEAD_Wrinkle_BMask_5__A.tga",
            "Head_WB-009":"HEAD_Wrinkle_BMask_5__B.tga",
            "Head_WB-010":"HEAD_Wrinkle_BMask_5__C.tga",
            "Head_WB-011":"HEAD_Wrinkle_BMask_5__D.tga"}

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

def CreateLayeredRelation():

    lConstraintRelation = FBConstraintRelation( 'LayeredTextureRelation' )
    lConstraintRelation.Active = True
    
    for i, key  in enumerate(mapDict):
        
        model = FBFindModelByName(key.replace("-", "_"))
        texture = None
        for tex in FBSystem().Scene.Textures:
            if tex.Name == mapDict[key]:
                texture = tex
        
        texture.PropertyList.Find("Transparency").SetAnimated(True)
        
        modelBox = lConstraintRelation.SetAsSource( model )
        modelBox.UseGlobalTransforms = False
        lConstraintRelation.SetBoxPosition(modelBox, 0, (200 * i))
        
        lNumberToVectorBox = lConstraintRelation.CreateFunctionBox( 'Converters', 'Vector to Number' )
        lConstraintRelation.SetBoxPosition( lNumberToVectorBox, 350, (200 * i) )
        
        textureBox = lConstraintRelation.ConstrainObject( texture )
        lConstraintRelation.SetBoxPosition(textureBox, 700, (200 * i))
        
        modelBoxOut       = FindAnimationNode( modelBox.AnimationNodeOutGet(), 'Lcl Translation' )
        lNumberToVectorBoxIn  = FindAnimationNode( lNumberToVectorBox.AnimationNodeInGet(), 'V' )
        if modelBoxOut and lNumberToVectorBoxIn:
            FBConnect( modelBoxOut, lNumberToVectorBoxIn )
        
        lNumberToVectorBoxOut  = FindAnimationNode( lNumberToVectorBox.AnimationNodeOutGet(), 'Y' )    
        textureBoxOut       = FindAnimationNode( textureBox.AnimationNodeInGet(), 'Texture alpha' )    
        if lNumberToVectorBoxOut and textureBoxOut:
            FBConnect( lNumberToVectorBoxOut, textureBoxOut )

def createWrinkleMapMasks( path, wrinklemap, destination):

    infile = path + "\\" + wrinklemap + ".tga"
    outfile = destination + "\\" + wrinklemap + "_512.tif"
    if infile != outfile:
        try:
            image = FBImage(infile) 
            image.ConvertSize(512,512)
            image.WriteToTif(outfile, "", True)
            
            #fs = FileStream(infile, FileMode.Open, FileAccess.Read)
            #inputImage = Image.FromStream( fs )
            #outputBitmap = Bitmap( inputImage, Size( 512,512 ) )
            #outputBitmap.Save( outfile, ImageFormat.Bmp )
            
        except IOError:
            print "cannot create thumbnail for '%s'" % infile

    for i in range(6):
        
        mask = path + "\\HEAD_Wrinkle_Mask_{0}.tga".format(i)

        if infile != outfile:

            maskIm = Image.open(mask)
            maskArray = [None, None, None, None]
            maskArray[0], maskArray[1], maskArray[2], maskArray[3] = maskIm.split()

            alphaArray = ["_A", "_B", "_C", "_D"]
            
            for j in range(len(maskIm.split())):
            
                try:

                    maskA = destination + "\\" + wrinklemap + "Mask_{0}_{1}.tga".format(i, alphaArray[j])
                    im = Image.open(outfile)
                    im.putalpha(maskArray[j])
                    im.save(maskA, "TGA")

                except IOError:
                    print "cannot create thumbnail for '%s'" % infile


def SetUpWrinkleMaps():

    lFp = FBFolderPopup()
    lFp.Caption = "Select Character Texture Folder"
    lFp.Path = r"X:\gta5\art\peds"
    lRes = lFp.Execute()
    
    texturePath = None
    mypath = None
    highResPath = None
    
    if lRes:
        texturePath = lFp.Path
    else:
        FBMessageBox( "FBFolderPopup example", "Selection canceled", "OK" )
    
    if os.path.isdir(os.path.join(texturePath + "\wrinkles")):
        
        mypath = os.path.join(texturePath + "\wrinkles")
        highResPath = os.path.join(texturePath + "\HighRes")
        
    else:
        
        os.mkdir(os.path.join(texturePath + "\wrinkles"))
        mypath = os.path.join(texturePath + "\wrinkles")
        highResPath = os.path.join(texturePath + "\HighRes")
    
    createWrinkleMapMasks(highResPath, wrinkleMaps[0], mypath)
    createWrinkleMapMasks(highResPath, wrinkleMaps[1], mypath)
    
    head = FBFindModelByName("head_000_r")
    headTexture = None
    headMaterial = None
    
    for i in range(head.GetSrcCount()):
        
        if str(head.GetSrc(i).ClassName()) ==  "FBMaterial":
            
            headMaterial = head.GetSrc(i)
            headTexture = headMaterial.GetTexture()
            
    print headTexture.Name
    headMaterial.FBDelete()
    
    diffusedLayered = FBLayeredTexture("Layered Diffuse")
    diffusedLayered.Layers.append(headTexture)
    
    onlyfiles = [ f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath,f)) ]
    normalLayered = FBLayeredTexture("Layered Normal")
    
    texture = FBTexture(r"X:\gta5\art\peds\Player\MICHAEL\Textures\HighRes\head_normal_000.tga")
    normalLayered.Layers.append(texture)
    texture.BlendMode = FBTextureBlendMode.kFBTextureBlendTranslucent
            
    for i in range(len(onlyfiles)):
    
        if "Mask" in onlyfiles[i]:
            
            texture = FBTexture(os.path.join(mypath,onlyfiles[i]))
            normalLayered.Layers.append(texture)
            texture.BlendMode = FBTextureBlendMode.kFBTextureBlendTranslucent
            texture.PropertyList.Find("Transparency").Data = 0
            
    material = FBMaterial('myMaterial')
    material.SetTexture(diffusedLayered, FBMaterialTextureType.kFBMaterialTextureDiffuse)
    material.SetTexture(normalLayered, FBMaterialTextureType.kFBMaterialTextureNormalMap)
        
    head.Materials.append(material) 
    
    lShaderManager = FBShaderManager()
    
    shader = lShaderManager.CreateShader( "Dynamic Lighting" )
    
    head.Shaders.append(shader) 
    
    CreateLayeredRelation()
    
#SetUpWrinkleMaps()


#New Layered texture logic

class NoHeadModelsFoundError (Exception): 
    def __init__( self, message ):
        self.message = message
        
class MissingBlushTextureError (Exception): 
    def __init__( self, message ):
        self.message = message

class MissingBlushControlerError (Exception): 
    def __init__( self, message ):
        self.message = message

def rsta_ConfigureCustsceneBlushSlider():

    csBlushPath = "X:\\gta5\\art\\peds\\3LateralSetup\\BlushSlider.FBX"
    RS.Perforce.Sync(csBlushPath)
    if os.path.isfile( csBlushPath ): 

        lControlNodeParent = RS.Utils.Scene.FindModelByName("faceControls_OFF")
        
        if lControlNodeParent:
            print "Found faceControls_OFF"
            lChildNode = RS.Utils.Scene.FindModelByName("RECT_cutsceneBlush_OFF")
            if lChildNode:
                print ("Existing cutsceneBlush_OFF found. Skipping import")                
            else:
                FBApplication().FileMerge( csBlushPath, False)    
                
            lChildNode = RS.Utils.Scene.FindModelByName("RECT_cutsceneBlush_OFF")            
            if lChildNode:
                lChildNode.Parent = lControlNodeParent
                print "Parented RECT_cutsceneBlush_OFF to faceControls_OFF"
            else:
                 print "cannot find RECT_cutsceneBlush_OFF"
        else:
            print "cannot find faceControls_OFF"
    else:
        raise MissingBlushControlerError("Missing blush controller file: {0}".format(csBlushPath)) 

BLUSH_DIFFUSE_IMAGE_FILE_PATH = "{0}\\animation\\resources\\textures\\HEAD_Blush.tga".format(RS.Config.Project.Path.Art)

def SetupBlush():
    
    #First ensure the cutscene blush slider is present
    rsta_ConfigureCustsceneBlushSlider()   
    
    #Get all the head models (will likely just be one)
    headModelList = []
    for component in RS.Globals.Components:
        if type( component ) == FBModel and re.search( ':HEAD_0', component.FullName, re.IGNORECASE ) and not re.search( 'LOD', component.FullName, re.IGNORECASE ):
            headModelList.append( component )
    
    #Check we have some head models
    if len(headModelList) == 0:
        raise NoHeadModelsFoundError("Not head models found in scene.")

    #Check we have our blush texture
    RS.Perforce.Sync(BLUSH_DIFFUSE_IMAGE_FILE_PATH)
    if not os.path.exists( BLUSH_DIFFUSE_IMAGE_FILE_PATH ): 
        raise MissingBlushTextureError("Missing diffuse texture {0}".format(BLUSH_DIFFUSE_IMAGE_FILE_PATH))

    #Go through each model and material 
    for headModel in headModelList:
        print 'Processing head model [{0}]'.format(headModel.Name)
        
        #Store off old materials because we don't want to mess with them directly - they might be used on other models
        oldMaterials = list( headModel.Materials )
        headModel.Materials.removeAll()
        for material in oldMaterials:
            #Process the diffuse texture
            baseDiffuseTexture = material.GetTexture( FBMaterialTextureType.kFBMaterialTextureDiffuse )
            
            #Check that we aren't already dealing with a FBLayeredTexture
            if type( baseDiffuseTexture ) == FBLayeredTexture :
                print "Texture already layered on material [{0}] making sure the diffuse is setup correctly".format( material.Name )
                
                #Check we have got some layers
                if len(baseDiffuseTexture.Layers) < 2:
                    print "Not enough layers in layered texture so cannot be a blush layered texture on [{0}]".format( material.Name )
                    headModel.Materials.append( material )
                    continue                    
                
                #Should be top texture
                blushDiffuseTexture = baseDiffuseTexture.Layers[0]
                
                #Check to see if it is named as we would expect
                if not re.search( 'blush_diffuse_texture', blushDiffuseTexture.Name, re.IGNORECASE ):
                    print "Top texture on layered texture doesn't seem to be a blush texture on [{0}]".format( material.Name )
                    headModel.Materials.append( material )
                    continue
                
                blushDiffuseTexture.Video.Filename = BLUSH_DIFFUSE_IMAGE_FILE_PATH
                print "Repointed texture successfully"
                headModel.Materials.append( material )
                continue                
            
            #Check if we are dealing with a standard FBTexture    
            elif type( baseDiffuseTexture ) == FBTexture :
                if baseDiffuseTexture == None or baseDiffuseTexture.Video == None or baseDiffuseTexture.Video.Filename == None:
                    print "Missing texture so skipping material [{0}]".format( material.Name )
                    headModel.Materials.append( material )
                    continue
                
                #Check that texture that we are working with has 'Head' in the name
                if not re.search( 'HEAD', baseDiffuseTexture.Video.Filename, re.IGNORECASE ):
                    print "Not a head texture so skipping [{0}]".format( baseDiffuseTexture.Video.Filename )
                    headModel.Materials.append( material )
                    continue
                
                blushDiffuseTexture = FBTexture( r'{0}_blush_diffuse_texture'.format( headModel.Name ) )
                blushDiffuseTexture.Video = FBVideoClipImage( r'{0}_blush_diffuse_image'.format( headModel.Name ) )
                blushDiffuseTexture.Video.Filename = BLUSH_DIFFUSE_IMAGE_FILE_PATH                

                #Create our new layer texture
                diffuseLayeredTexture = FBLayeredTexture( "{0}_layered_diffuse_texture".format( headModel.Name ) )
                diffuseLayeredTexture.Layers.append( blushDiffuseTexture )
                diffuseLayeredTexture.Layers.append( baseDiffuseTexture )

                #Setup a new material so we don't mess with the old one
                newMaterial = FBMaterial( material.Name + "_layered" )
                newMaterial.SetTexture( diffuseLayeredTexture, FBMaterialTextureType.kFBMaterialTextureDiffuse )
                headModel.Materials.append( newMaterial )

            else:
                print "Texture type not supported on material [{0}] skipping".format( material.Name )
                headModel.Materials.append( material )
                continue
            
            #Now connect the blush slider to the blush texture via a relation constraint
            
##=======================================================================================            
##            RelationCon = FBConstraintRelation('BlushConstraint')
##            BlushJoystick = RS.Utils.Scene.FindModelByName("cutsceneBlush_OFF")
##            
##            RelationSender = RelationCon.SetAsSource(BlushJoystick)
##            RelationCon.SetBoxPosition(RelationSender, 0, 0)
##            
##            vectToNumBox = RelationCon.CreateFunctionBox( 'Converters', "Vector to Number" )            
##            RelationCon.SetBoxPosition(vectToNumBox, 300, 100)
##            
##            maxMacro = RelationCon.CreateFunctionBox( 'My Macros', "max(x,y)" )
##            RelationCon.SetBoxPosition(maxMacro, 600,200)
##            
##            minMacro = RelationCon.CreateFunctionBox( 'My Macros', "min(x,y)" )
##            RelationCon.SetBoxPosition(minMacro, 900,300)
##                        
##            RelationSender.UseGlobalTransforms = False
##            
##            print "Relation sender: "+RelationSender.Name
##            RS.Utils.Scene.ConnectBox( RelationSender, 'Lcl Translation', vectToNumBox, 'V' )
##                
##            RS.Utils.Scene.ConnectBox( vectToNumBox, 'Y', maxMacro, 'MacroInput0' )
##            maxUppVal = RS.Utils.Scene.FindAnimationNode( maxMacro.AnimationNodeInGet(),'MacroInput1' )
##            maxUppVal.WriteData([0.0])
##            
##            RS.Utils.Scene.ConnectBox( maxMacro, 'MacroOutput0', minMacro, "MacroInput0" )
##            minLowVal = RS.Utils.Scene.FindAnimationNode( minMacro.AnimationNodeInGet(),'MacroInput1' )
##            minLowVal.WriteData([1.0])
##=======================================================================================            

            RelationCon = FBConstraintRelation('BlushConstraint')
            BlushJoystick = RS.Utils.Scene.FindModelByName("cutsceneBlush_OFF")
            
            RelationSender = RelationCon.SetAsSource(BlushJoystick)
            RelationCon.SetBoxPosition(RelationSender, 0, 0) 
            RelationSender.UseGlobalTransforms = False            
                
            vectToNumBox = RelationCon.CreateFunctionBox( 'Converters', "Vector to Number" )            
            RelationCon.SetBoxPosition(vectToNumBox, 300, 100)
            
            RS.Utils.Scene.ConnectBox( RelationSender, 'Lcl Translation', vectToNumBox, 'V' )            
            
            maxIf = RelationCon.CreateFunctionBox( 'Number', 'IF Cond Then A Else B')
            RelationCon.SetBoxPosition(maxIf, 900,200)
            maxIsGreater = RelationCon.CreateFunctionBox( 'Number', "Is Greater (a > b)")
            RelationCon.SetBoxPosition(maxIsGreater, 600,200)
            
            maxIfBConstantVal = RS.Utils.Scene.FindAnimationNode( maxIf.AnimationNodeInGet(),'b' )
            maxIfBConstantVal.WriteData([0.0])
            maxIsGreatConstantVal = RS.Utils.Scene.FindAnimationNode( maxIsGreater.AnimationNodeInGet(),'b' )
            maxIsGreatConstantVal.WriteData([0.0])
            
            RS.Utils.Scene.ConnectBox( vectToNumBox, 'Y', maxIsGreater, 'a' )
            RS.Utils.Scene.ConnectBox( maxIsGreater, 'Result', maxIf, 'Cond' )
            RS.Utils.Scene.ConnectBox( vectToNumBox, 'Y', maxIf, 'a' )
            
            minIf = RelationCon.CreateFunctionBox( 'Number', 'IF Cond Then A Else B')
            RelationCon.SetBoxPosition(minIf, 900,400)
            
            minIsLess = RelationCon.CreateFunctionBox( 'Number', "Is Less (a < b)")
            RelationCon.SetBoxPosition(minIsLess, 600,400)
            
            minIfBConstantVal = RS.Utils.Scene.FindAnimationNode( minIf.AnimationNodeInGet(),'b' )
            minIfBConstantVal.WriteData([1.0])
            minIsLessConstantVal = RS.Utils.Scene.FindAnimationNode( minIsLess.AnimationNodeInGet(),'b' )
            minIsLessConstantVal.WriteData([1.0])
            

            RS.Utils.Scene.ConnectBox( maxIf, 'Result', minIsLess, 'a' )
            RS.Utils.Scene.ConnectBox( minIsLess, 'Result', minIf, 'Cond' )
            RS.Utils.Scene.ConnectBox( maxIf, 'Result', minIf, 'a' )    
            
            transVal = blushDiffuseTexture.Alpha
            transVal.SetAnimated(True)

##            RelationReceiver = RelationCon.SetAsReceiver
            RelationReceiver = RelationCon.ConstrainObject( blushDiffuseTexture )
            RelationCon.SetBoxPosition( RelationReceiver, 1200, 400 )            
            
##            RS.Utils.Scene.ConnectBox( minMacro, 'MacroOutput0', RelationReceiver, 'Texture alpha' )
            RS.Utils.Scene.ConnectBox( minIf, 'Result', RelationReceiver, 'Texture alpha' )
            
            RelationCon.Active = True
            
            for i in RS.Globals.gGroups:
##                if i.Name == '3Lateral':
                if i.Name == '3L_Controls':    
                    i.ConnectSrc(BlushJoystick) ##do for other piueces of joystick too
                    
                    BlushJoystick.PropertyList.Find("Enable Translation DOF").Data = True
                    BlushJoystick.PropertyList.Find("TranslationMinX").Data = True
                    BlushJoystick.PropertyList.Find("TranslationMinY").Data = True
                    BlushJoystick.PropertyList.Find("TranslationMinZ").Data = True                    
                    BlushJoystick.PropertyList.Find("TranslationMin").Data = FBVector3d(0,0,0)
                    BlushJoystick.PropertyList.Find("TranslationMaxX").Data = True
                    BlushJoystick.PropertyList.Find("TranslationMaxY").Data = True 
                    BlushJoystick.PropertyList.Find("TranslationMaxZ").Data = True                     
                    BlushJoystick.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0)        
                
                if i.Name == '3L_Frames':
                    blushFrame = RS.Utils.Scene.FindModelByName("RECT_cutsceneBlush_OFF")
                    blushFrame.Translation = FBVector3d(-14.0,0.00,0.00)
                    FBSystem().Scene.Evaluate()
                    
                    blushText = RS.Utils.Scene.FindModelByName("TEXT_cutsceneBlush_OFF")
                    i.ConnectSrc(blushFrame)
                    i.ConnectSrc(blushText)
                    
            print "Cutscene Blush setup complete."