###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AssetSetupProps
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Function to set up props - ingame, cutscene, skinned, weapons
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import re
import os
import RS.Utils.Scene
import RS.Utils.Path
import RS.Core.AssetSetup.Lib as ass

#####################################################################################
# Reset Pivot Offset - MB14 bug workaround (logged with autodesk) url:bugstar:1713249
#####################################################################################

def ResetPivotOffset( parent ):

	markerList = []

	RS.Utils.Scene.GetChildren( parent, markerList, "", True )

	for i in markerList:

		pivotRotProp = i.PropertyList.Find( 'RotationPivot' )
		pivotScaleProp = i.PropertyList.Find( 'ScalingPivot' )
		offsetRotProp = i.PropertyList.Find( 'RotationOffset' )
		offsetScaleProp = i.PropertyList.Find( 'ScalingOffset' )

		if pivotRotProp:
			pivotRotProp.Data = FBVector3d( 0,0,0 )

		if pivotScaleProp:
			pivotScaleProp.Data = FBVector3d( 0,0,0 )        

		if offsetRotProp:
			offsetRotProp.Data = FBVector3d( 0,0,0 )

		if offsetScaleProp:
			offsetScaleProp.Data = FBVector3d( 0,0,0 )

		FBSystem().Scene.Evaluate()    


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Setup mover and toybox relation - for use in rs_CSCharacterSetup & rs_IGCharacterSetup
##              This was only added in Jan/13 so needs to be adjusted if it was used at the beginning of the production 
##              Currently only BoxStar Tool calls this function, not anywhere else in this module - KM
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupPropToyboxMoverControl(lNameSpace, lDummy, lMover, lMoverControl):

	# Check for ToyBox Marker, however most files should have this at this point.        
	lFound = False
	for lToyBoxMarker in lDummy.Children:
		if lToyBoxMarker.Name == "ToyBox Marker":
			lFound = True

	if lFound == False:

		lToyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
		lToyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross  
		lToyBoxMarker.Size = 500
		lToyBoxMarker.Color = FBColor(1, 0, 1)
		lToyBoxMarker.Parent = lDummy
		lToyBoxMarker.Translation.Data = lMover.Translation.Data
		lToyBoxMarker.Rotation.Data = lMover.Rotation.Data
		lTag = lToyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
		lTag.Data = "rs_Contacts"       

	# Creating the mover_toybox_Control Relation Constraint
	lRelationCon = FBConstraintRelation(lNameSpace+':mover_toybox_Control') # Why is this call slow..
	lRelationCon.Active = False 

	# Add senders/receivers to the Relation Constraint
	lMoverMarkerSender = lRelationCon.SetAsSource(lMoverControl)
	lToyboxMarkerReceive = lRelationCon.ConstrainObject(lToyBoxMarker)
	lToyBoxMarkerSender = lRelationCon.SetAsSource(lToyBoxMarker)
	lMoverReceive = lRelationCon.ConstrainObject(lMover)

	# Position the boxes and set the properties    
	lRelationCon.SetBoxPosition(lMoverMarkerSender, 0, 0)
	lRelationCon.SetBoxPosition(lToyboxMarkerReceive, 300, 0)    
	lRelationCon.SetBoxPosition(lToyBoxMarkerSender, 0, 150)    
	lRelationCon.SetBoxPosition(lMoverReceive, 350, 150)
	lToyBoxMarkerSender.UseGlobalTransforms = False
	lMoverReceive.UseGlobalTransforms = False

	# Make the property connections
	RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Translation', lToyboxMarkerReceive, 'Translation')
	RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Rotation', lToyboxMarkerReceive, 'Rotation')   
	RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Translation', lMoverReceive, 'Lcl Translation')
	RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Rotation', lMoverReceive, 'Lcl Rotation')

	return lRelationCon

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Marker Setup 
##                         - Setup markers for each null (Not including Dummy)
##                         - Create Marker, align to null, create constraint
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupCreateMarkers():      

	lSceneList = FBModelList()
	FBGetSelectedModels (lSceneList, None, False)
	FBGetSelectedModels (lSceneList, None, True)

	lNullList = []
	lMarkerList = []
	lSpawnPointList = []

	lDummy = ass.rs_AssetSetupVariables()[0]
	lMover = ass.rs_AssetSetupVariables()[1]
	lGeo = ass.rs_AssetSetupVariables()[2]
	lExportable = lMover.Children[0]

	lNullRelConst = FBConstraintRelation("Bone_Relation_Constraint")
	lNullRelConst.Active = True

	RS.Utils.Scene.GetChildren(lMover, lNullList, "", False)

	x = 0
	y = 0
	for iNull in lNullList:

		lMarker = FBModelMarker(iNull.Name + "_Control")
		lMarkerList.append(lMarker)

		lMarker.Show = True
		lMarker.Look = FBMarkerLook.kFBMarkerLookSphere
		lMarker.PropertyList.Find("Size").Data = 100
		lMarker.PropertyList.Find("Color RGB").Data = FBColor(1,0,0)

		lVector = FBVector3d()
		iNull.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
		lMarker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
		iNull.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
		lMarker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
		FBSystem().Scene.Evaluate()

		lSender = lNullRelConst.SetAsSource(lMarker)
		lReceiver = lNullRelConst.ConstrainObject(iNull)

		if lNullRelConst.SetBoxPosition(lSender, 0, x):
			x = x + 150
		else:
			lNullRelConst.SetBoxPosition(lSender, 0, x)

		if lNullRelConst.SetBoxPosition(lReceiver, 400, y):
			y = y + 150
		else:
			propConst.SetBoxPosition(lReceiver, 400, y)                    

		RS.Utils.Scene.ConnectBox(lSender, 'Rotation', lReceiver, 'Rotation')
		RS.Utils.Scene.ConnectBox(lSender, 'Translation', lReceiver, 'Translation')        

		FBSystem().Scene.Evaluate()

	lMoverRelConst = FBConstraintRelation("mover_Relation_Constraint")
	#FBSystem().Scene.Constraints.append(lMoverRelConst)
	lMoverRelConst.Active = True

	lMoverMarker = FBModelMarker(lMover.Name + "_Control")

	lMoverMarker.Show = True
	lMoverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
	lMoverMarker.PropertyList.Find("Size").Data = 120
	lMoverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)

	lMoverVector = FBVector3d()
	lMover.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
	lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
	lMover.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
	lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
	FBSystem().Scene.Evaluate()

	lMoverSender = lMoverRelConst.SetAsSource(lMoverMarker)
	lMoverReceiver = lMoverRelConst.ConstrainObject(lMover)

	lMoverRelConst.SetBoxPosition(lMoverSender, 0, 0)
	lMoverRelConst.SetBoxPosition(lMoverReceiver, 400, 0)                    

	RS.Utils.Scene.ConnectBox(lMoverSender, 'Rotation', lMoverReceiver, 'Rotation')
	RS.Utils.Scene.ConnectBox(lMoverSender, 'Translation', lMoverReceiver, 'Translation')            

	lMultiDimensionalArray = []

	for iNull in lNullList:
		ChildrenArray = []
		ChildrenArray.append(FBFindModelByLabelName(iNull.Name + "_Control"))
		for iChild in iNull.Children:
			ChildrenArray.append(FBFindModelByLabelName(iChild.Name + "_Control"))
		lMultiDimensionalArray.append(ChildrenArray)

	for i in range(len(lMultiDimensionalArray)):
		if len(lMultiDimensionalArray[i]) > 0:
			for j in range(len(lMultiDimensionalArray[i]) - 1):
				lMultiDimensionalArray[i][j + 1].Parent = lMultiDimensionalArray[i][0]

	lExportableMarker = FBFindModelByLabelName(lExportable.Name + "_Control")

	lMoverMarker.Parent = lExportableMarker
	lExportableMarker.Parent = lDummy

	lMoverMarker.PropertyList.Find("Enable Transformation").Data = False
	lMoverMarker.PropertyList.Find("Enable Selection").Data = False

	for iSceneItem in lSceneList:
		if "spawnpoint" in iSceneItem.Name.lower():
			lSpawnPointList.append(iSceneItem)

	for iSpawnPoint in lSpawnPointList:

		lSpawnControl = FBModelMarker(iSpawnPoint.Name + "_visual")
		lSpawnControl.Show = True
		lSpawnControl.Look = FBMarkerLook.kFBMarkerLookSphere
		lSpawnControl.PropertyList.Find("Size").Data = 150
		lSpawnControl.PropertyList.Find("Color RGB").Data = FBColor(1,1,0)
		lSpawnControl.PropertyList.Find("Enable Transformation").Data = False            

		lVector = FBVector3d() 
		iSpawnPoint.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
		lSpawnControl.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
		iSpawnPoint.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
		lSpawnControl.SetVector(lVector, FBModelTransformationType.kModelRotation, True)

		FBSystem().Scene.Evaluate()

		lSpawnControl.Parent = lGeo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Tidy Scene
##                         - Remove Unwanted Nulls, Create Folders, Create groups
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupTidyScene():

	lControlNullList = []
	lExportNullList = []
	lDeleteList = []

	lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
	lMover = ass.rs_AssetSetupVariables()[1]
	lGeo = ass.rs_AssetSetupVariables()[2]
	lExportable = lMover.Children[0]

	ass.rs_AssetSetupDeleteItems()

	lControlGroup = FBGroup("Controls")
	lPlotGroup = FBGroup("Plot Group")
	lGeoGroup = FBGroup("Geo")

	lMainGroup = FBGroup(lFileName)
	lMainGroup.ConnectSrc(lControlGroup)
	lMainGroup.ConnectSrc(lPlotGroup)
	lMainGroup.ConnectSrc(lGeoGroup)

	lControlRoot = FBFindModelByLabelName(lExportable.Name + "_Control")

	RS.Utils.Scene.GetChildren(lControlRoot, lControlNullList)
	#lControlNullList.extend(eva.gChildList)
	#del eva.gChildList[:]

	RS.Utils.Scene.GetChildren(lMover, lExportNullList)   
	#lExportNullList.extend(eva.gChildList)
	#del eva.gChildList[:]

	for iChild in lControlNullList:
		lControlGroup.ConnectSrc(iChild)

	for iChild in lExportNullList:
		lPlotGroup.ConnectSrc(iChild)

	lGeoGroup.ConnectSrc(lGeo)
	

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: Render Target Check - adds a custom property onto the mesh if a render target exists on it
##				url:bugstar:1299744
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def RenderTargetCheck():
	mesh = None
	for i in RS.Globals.Components:
		rsType = i.PropertyList.Find( "rs_Type" )
		if rsType:
			if rsType.Data == 'rs_Mesh':
				mesh = i

	materialList = []		
	if mesh:
		if mesh.GetSrcCount() != 0:
			for i in range( mesh.GetSrcCount() ):
				sourceObject = mesh.GetSrc( i )
				if sourceObject.ClassName() == 'FBMaterial':
					materialList.append( sourceObject )

		renderTargetList = []	
		if materialList:
			for i in materialList:
				texture = i.GetTexture()
				if texture:
					video = texture.Video
					if video:
						if 'script_rt' in video.Filename:

							split = video.Filename.split( '\\' )						
							rtNameExtensionSplit = split[-1].split( '.' )
							rtName = rtNameExtensionSplit[0]

							rtPathProperty = mesh.PropertyCreate( 'RenderTargetPath', FBPropertyType.kFBPT_charptr, "", False, True, None )
							rtPathProperty.Data = video.Filename
							rtPathProperty.SetLocked( True )							

							rtNameProperty = mesh.PropertyCreate( 'RenderTargetName', FBPropertyType.kFBPT_charptr, "", False, True, None )
							rtNameProperty.Data = rtName
							rtNameProperty.SetLocked( True )


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Prop Setup 
##                         - Prop Setup Functions:
##                           Delete animation, Delete unwanted nulls, Get Exportable Null Variable, 
##                           Hide mover, Parent geo to dummy, Brighten Materials
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AssetSetupProps(pControl, pEvent):

	lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)        

	FBSystem().CurrentTake.ClearAllProperties(False)

	if len(ass.rs_AssetSetupVariables()) == 3:

		ass.rs_AssetSetupDeleteItems()

		FBSystem().Scene.Evaluate()

		lDummy = ass.rs_AssetSetupVariables()[0]
		lMover = ass.rs_AssetSetupVariables()[1]
		lGeo = ass.rs_AssetSetupVariables()[2]
		lGripR = FBFindModelByLabelName("Gun_GripR")
		lGripL = FBFindModelByLabelName("Gun_GripL")
		lExportable = lMover.Children[0]

		lDummy.Translation = FBVector3d(0,0,0)

		FBSystem().Scene.Evaluate()

		lMover.Selected = True
		lMover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
		lMover.Selected = False
		lMover.PropertyList.Find("Visibility").Data = True

		FBSystem().Scene.Evaluate()

		if lGeo.Parent == None:
			lGeo.Parent = lDummy

		FBSystem().Scene.Evaluate()

		lDummy.PropertyList.Find("Enable Transformation").Data = False
		lDummy.PropertyList.Find("Enable Selection").Data = False
		lMover.PropertyList.Find("Enable Transformation").Data = False
		lMover.PropertyList.Find("Enable Selection").Data = False

		FBSystem().Scene.Evaluate()

		rs_AssetSetupCreateMarkers()

		FBSystem().Scene.Evaluate()

		rs_AssetSetupTidyScene()

		FBSystem().Scene.Evaluate()

		if lGripR:          

			lRootControl = FBFindModelByLabelName(lExportable.Name + "_Control")
			lGripRControl = FBFindModelByLabelName(lGripR.Name + "_Control")           

			lGripRControl.Parent = None
			lRootControl.Parent = None 

			FBSystem().Scene.Evaluate()

			lGripRControl.Parent = lDummy                        
			lRootControl.Parent = lGripRControl

			FBSystem().Scene.Evaluate()

			if lGripL:

				lGripLControl = FBFindModelByLabelName(lGripL.Name + "_Control")

				lGripLControl.Parent = None

				FBSystem().Scene.Evaluate()

				if lFileName == "W_SG_BullpupShotgun" or lFileName == "W_SG_PumpShotgun" or lFileName == "W_SG_SawnOff"  :
					lGripLControl.Parent = FBFindModelByLabelName("Gun_Cock1_Control")
				else:
					lGripLControl.Parent = lGripRControl

				FBSystem().Scene.Evaluate()

			lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
			lTypeTag.Data = "Props Weapon"

			FBSystem().Scene.Evaluate()

			FBPlayerControl().LoopStart = FBTime(0,0,0,0)

		else:

			if 'root' in lExportable.Name.lower():
				lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
				lTypeTag.Data = "Props Skinned"
			else:
				lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
				lTypeTag.Data = "Props"

			FBPlayerControl().LoopStart = FBTime(0,0,0,0)

		FBSystem().Scene.Evaluate()

		ass.rs_CreateToyBoxMarker()

		ass.rs_AssetSetupFolderTidy()

		ass.rs_AssetSetupCustomProperties()  

		# checks for render targets in the mesh materials, adds as a custom property url:bugstar:1299744
		RenderTargetCheck()

		# scale down nulls
		ass.ScaleDownNulls()

		FBMessageBox("Complete", lGeo.Name + " has now been set up.", "Ok")


	else:
		print "An Error occured in the 'rs_AssetSetupVariables' step."