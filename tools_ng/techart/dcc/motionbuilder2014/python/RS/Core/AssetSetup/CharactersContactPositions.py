###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CharactersContactPositions
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Function to save Chaarcter's Hand and Foot Contact position for quick setup
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
from os import *
import RS.Perforce
import RS.Config
#import os
lLogRootPath = ( RS.Config.Tool.Path.Root + '\\techart\\dcc\\motionbuilder2014\\python\\RS\\Core\\AssetSetup\\outputs\\character_contact_settings\\')

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition to gather property data. Used in rs_SaveContactPostion
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

lPropList = []
lPropDataList = []

def rs_PropertyInformationtoLists(pProp):

    lPropData = pProp.Data
    lPropList.append(pProp.Name)
    lPropDataList.append(lPropData)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition to load property data. Used in rs_LoadContactPostion
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_PropertyLoad(pDict, pCharacter):
    for iPropName, iPropData in pDict.iteritems():
        iPropNameProperty = pCharacter.PropertyList.Find( iPropName )
        if iPropNameProperty:
            iPropNameProperty.Data = float(iPropData)
        
        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition to SAVE contacts into a text doc
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SaveContactPostion(pCharacter, pSaveName):

    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand Height"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand Back"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand Middle"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand Front"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand In Side"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Hand Out Side"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot Height"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot Back"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot Middle"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot Front"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot In Side"))
    rs_PropertyInformationtoLists(pCharacter.PropertyList.Find("Foot Out Side"))
    
    lPropertyDict = dict(zip(lPropList, lPropDataList))

    lLogList = [] 
    
    for iPropName, iPropData in lPropertyDict.iteritems():
        lDictString = ("'" + iPropName + "'" + ":" + "'%s" %iPropData + "'" +'\n')
        lLogList.append(lDictString) 
          
    lSep = ''
    lRosetta = lSep.join(lLogList)
    lLogFilePath =  (lLogRootPath + pSaveName +'.txt')
    lMyFile1 = open(lLogFilePath, O_CREAT)
    lMyFile = open(lLogFilePath, O_WRONLY)
    write (lMyFile, lRosetta)
    close (lMyFile) 
    close (lMyFile1)

    FBMessageBox("Success!", "\nContact positions have saved.\n" + lLogFilePath + '\n\nRemember:  Check the text doc into P4.', "Ok") 
    print "File Saved: " + lLogFilePath    

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition to LOAD contacts into a text doc
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_LoadContactPostion(pCharacter, pSaveName):

    lFilename = (lLogRootPath + pSaveName + '.txt')
    
    if not path.isfile( lFilename ):
        RS.Perforce.Sync( lFilename )
    
    if path.isfile( lFilename ):
        lFile = file(lFilename ,"r")
        lTextLines = lFile.readlines()
        lPropNameList = []
        lPropDataList = []
        for iLine in lTextLines:
            
            lLineSplit = iLine.split(":")
        
            lPropNameSplit1 = lLineSplit[0]
            lPropNameSplit2 = lPropNameSplit1.split("'")
            if len(lPropNameSplit2) >1:
                lPropName = lPropNameSplit2[1]
                lPropNameList.append(lPropName)
            if len(lLineSplit) >1:            
                lPropDataSplit1 = lLineSplit[1]
                lPropDataSplit2 = lPropDataSplit1.split("'")
                lPropData = lPropDataSplit2[1]
                lPropDataList.append(lPropData)
                
        lLoadPropDict = dict(zip(lPropNameList, lPropDataList))
        
        rs_PropertyLoad(lLoadPropDict, pCharacter)
                   
        FBMessageBox("Success!", "Properties for Foot and Hand Contact Positions\nhave loaded on to " + pCharacter.Name + ".", "Ok")
        print "File Loaded: " + lFilename
        
    else:
        FBMessageBox( 'Rockstar', 'The file does not exist! Cannot load contact positions.\n\n{0}'.format( lFilename ), 'OK' )

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Find what type of character is open, and run script to SAVE
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SaveContactPostionLaunch(pControl, pEvent):

    lCharacter = FBSystem().Scene.Characters[0]
    FBPlayerControl().Goto(FBTime(0,0,0, 0))
    
    if "CS_" in lCharacter.Name or "IG_" in lCharacter.Name:
        lSaveName = lCharacter.Name
        rs_SaveContactPostion(lCharacter, lSaveName)
        
    else:
        if "_F_" in lCharacter.Name:
            lSaveName = "CClass_Female"
            rs_SaveContactPostion(lCharacter, lSaveName)
        else:
            lSaveName = "CClass_Male"
            rs_SaveContactPostion(lCharacter, lSaveName)  

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Find what type of character is open, and run script to LOAD
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################      

def rs_LoadContactPostionLaunch(pControl, pEvent):

    lCharacter = FBSystem().Scene.Characters[0]
    FBPlayerControl().Goto(FBTime(0,0,0, 0))
    
    if "CS_" in lCharacter.Name or "IG_" in lCharacter.Name:
        lSaveName = lCharacter.Name
        rs_LoadContactPostion(lCharacter, lSaveName)
        
    else:
        if "_F_" in lCharacter.Name:
            lSaveName = "CClass_Female"
            rs_LoadContactPostion(lCharacter, lSaveName)
        else:
            lSaveName = "CClass_Male"
            rs_LoadContactPostion(lCharacter, lSaveName)  

