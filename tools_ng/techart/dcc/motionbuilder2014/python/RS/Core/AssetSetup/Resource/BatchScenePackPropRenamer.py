"""
Renamer for props that need the pack prefix  - For Batching
"""

import pyfbsdk as mobu

import RS.Globals
import RS.Utils.Scene


def ValidComponentToRename(component):
    """
    Returns if a component is valid

    Arguments:
        component (FBComponent): component to check if it should be renamed
    """
    refProperty = component.PropertyList.Find('rs_Type')
    if refProperty:
        if refProperty.Data in ['rs_Bone', 'rs_Contacts'] or 'REFERENCED' in refProperty.Data:
            if 'mover_Control' in component.Name or 'ToyBox Marker' in component.Name:
                return False
    return True


def Run():
    """ Renames props with the pack prefix """

    # define names and paths
    packList = ['agt_']
    
    agtPropList = ['Prop_Agent_magnet', 'Prop_CS_SIM', 'Prop_CS_UAV_Remote', 'Prop_DLC_Headphone_01',
                   'Prop_DLC_USB_Drive', 'Prop_Trev_Milk_Pot_01', 'Prop_Trev_Saucer_01', 'Prop_Trev_Sugar_Pot_01',
                   'Prop_Trev_Tea_Cup_01', 'Prop_Trev_Tea_Pot_01', 'Prop_Agent_office_Chair', 'Prop_Cliff_Coffin',
                   'Prop_Plane_int_1st_AG', 'Prop_Plane_int_2nda_AG', 'Prop_Plane_int_2ndb_AG', 'Prop_Plane_int_fus_AG',
                   'Prop_Plane_int_fus2_AG', 'Prop_Plane_Int_rear_door', 'P_tigershark_S', 'Prop_Agent_Plane_Trolly',
                   'Prop_Cliff_Beaker', 'Prop_Cliff_StraightRazor', 'Prop_CS_Mace', 'Prop_DLC_Boarding_Pass',
                   'Prop_Marshal_Badge_01', 'Prop_SPA_Jetpack', 'Prop_CS_Coffin_Tray', 'Prop_Agent_din_chair_01']
    
    foundNullsList = [component for nullName in agtPropList for component in RS.Globals.gComponents
                      if "RS_Null:{}{}".format(packList[0], nullName) in component.LongName]
        
    for RSNull in foundNullsList:
        if not RSNull:
            continue

        dummy = RS.Utils.Scene.FindModelByName("{}:Dummy01".format(RSNull.Name), True)

        if dummy:
            # rename appropriate parts of the prop's hierarchy
            assetHierarchyList = []
            RS.Utils.Scene.GetChildren(dummy, assetHierarchyList, "", False)

            # rename nulls
            for component in assetHierarchyList:
                if ValidComponentToRename(component):
                    print component.LongName
                    component.Name = component.Name.split(packList[0])[-1]
                    print component.Name
                    print

    mobu.FBApplication().FileSave(mobu.FBApplication().FBXFileName)