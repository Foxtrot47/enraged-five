###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AssetSetupEnvironments
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: 2 functions to set up environments - 1 for 3ds setup & one for obj setup
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Path
import RS.Utils.Scene
import RS.Config
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Core.AssetSetup.Lib as ass
from RS import ProjectData

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Setup floor group structure
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

#def rs_SetupIPLGroupStructure(parentGroupNode, IPLGroupNode):



###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Setup floor group structure
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SetupFloorGroupStructure(floorGroupNode, floorSceneNode):
    for child in floorSceneNode.Children:
        objectMat = None
        newGroupNode = FBGroup(child.Name)
        floorGroupNode.ConnectSrc(newGroupNode)


        """
        lMatWall = FBMaterial('Wall')
        lMatGround = FBMaterial('Ground')
        lMatDressing = FBMaterial('Dressing')
        lMatProps = FBMaterial('Props')
        lMatDes = FBMaterial('DES')
        lMatPed = FBMaterial('Peds')
        """

        # Find the correct material for this group...
        for mat in FBSystem().Scene.Materials:
            if "wall" in child.Name.lower():
                if mat.Name == "Wall":
                    objectMat = mat
            if "ground" in child.Name.lower():
                if mat.Name == "Ground":
                    objectMat = mat
            if "dressing" in child.Name.lower():
                if mat.Name == "Dressing":
                    objectMat = mat
            if "props" in child.Name.lower():
                if mat.Name == "Props":
                    objectMat = mat
            if "des" in child.Name.lower():
                if mat.Name == "DES":
                    objectMat = mat 
            if "peds" in child.Name.lower():
                if mat.Name == "Peds":
                    objectMat = mat

        for subChild in child.Children:
            newGroupNode.ConnectSrc(subChild)
            if objectMat != None:
                subChild.Materials.append(objectMat) 



###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Strip Existing Materials
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_StripExistingMaterials():

    lDetachList = []
    lRemoveList = []
    lSetList = []

    lSetRoot = RS.Utils.Scene.FindModelByName("SetRoot")
    if lSetRoot:
        None
    else:
        lSceneList = FBModelList()
        FBGetSelectedModels (lSceneList, None, False)
        FBGetSelectedModels (lSceneList, None, True)
        for iScene in lSceneList:
            if "_root" in iScene.Name.lower():
                lSetRoot = iScene
                lSetRoot.Name = "SetRoot"

    RS.Utils.Scene.GetChildren(lSetRoot, lSetList, "", False)
    #lSetList.extend(eva.gChildList)
    #del eva.gChildList[:] 

    for lMaterial in glo.gMaterials:
        lDetachList.append(lMaterial)
        lRemoveList.append(lMaterial)

    for lTexture in glo.gTextures:
        lDetachList.append(lTexture)
        lRemoveList.append(lTexture)

    for iComponent in lDetachList:
        for iSetItem in lSetList:
            iSetItem.DisconnectSrc(iComponent)

    for iComponent in lRemoveList:
        if iComponent.Name != "DefaultMaterial":
            iComponent.FBDelete()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Environment Setup from 3dsMax
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CreateMaterialsEnvironmentSetup():

    lSetList = []

    lPropList = []
    lWallList = []
    lDressingList = []
    lGroundList = []
    lDESList = []
    lCeilingList = []
    lDressingWallList = []
    lPedList = []
    lActionList = []
    lEnvironmentList = []
    lFloorsList = {}
    lDoorsList = []

    FBSystem().CurrentTake.ClearAllProperties(False)

    lSetRoot = RS.Utils.Scene.FindModelByName("SetRoot")
    if lSetRoot:
        None
    else:
        lSceneList = FBModelList()
        FBGetSelectedModels (lSceneList, None, False)
        FBGetSelectedModels (lSceneList, None, True)
        for iScene in lSceneList:
            if "_root" in iScene.Name.lower():
                lSetRoot = iScene
                lSetRoot.Name = "SetRoot"

    RS.Utils.Scene.GetChildren(lSetRoot, lSetList, "", False)
    #print lSetList
    #lSetList.extend(eva.gChildList)
    #del eva.gChildList[:]     

    lTypeTag = lSetRoot.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "Environment"

    lSetRoot.Show = False

    for iCamera in glo.gCameras:
        if iCamera.Name == "Producer Perspective":
            iCamera.PropertyList.Find("Far Plane").Data = 400000

    lMatWall = FBMaterial('Wall')
    lMatGround = FBMaterial('Ground')
    lMatDressing = FBMaterial('Dressing')
    lMatProps = FBMaterial('Props')
    lMatDes = FBMaterial('DES')
    lMatPed = FBMaterial('Peds')

    lMatWall.Diffuse = FBColor(0.33, 0.8, 0.67)
    lMatGround.Diffuse = FBColor(0.02, 0.26, 0.44)
    lMatDressing.Ambient = FBColor(0.86, 0.65, 0.2)
    lMatDressing.Diffuse = FBColor(0.93, 0.8, 0.23)
    lMatProps.Ambient = FBColor(0.71, 0.37, 0.2)
    lMatProps.Diffuse = FBColor(0.97, 0.37, 0)
    lMatDes.Diffuse = FBColor(1, 0, 1)
    lMatPed.Diffuse = FBColor(1, 0.67, 1)

    lTrans = FBShaderLighted('Transparency')
    FBConnect(lMatWall, lTrans)
    lMatWall.DisplayMode = FBModelShadingMode.kFBModelShadingAll
    lTrans.Transparency = (FBAlphaSource.kFBAlphaSourceAccurateAlpha)
    lTrans.Alpha = 0.462
    lCartoonShad = FBShaderManager().CreateShader("FlatCartoonShader")
    lCartoonShad.PropertyList.Find("All Edges").Data = True

    lMaterialStr = "Materials"
    lShaderStr = "Shaders"

    lMatAction = lMatWall

    cre.rs_CreateFolderAddComponents(glo.gMaterials, lMaterialStr)
    cre.rs_CreateFolderAddComponents(glo.gShaders, lShaderStr)

    lMainGrp = FBGroup(lSetRoot.Name)
    lWallGrp = FBGroup("Wall")
    lGroundGrp = FBGroup("Ground")
    lPropsGrp = FBGroup("Props")
    lDressingGrp = FBGroup("Dressing")
    lDESGrp = FBGroup("DES")
    lCeilingGrp = FBGroup("Ceiling")
    lPedGrp =  FBGroup("Peds")
    lActionGrp =  FBGroup("Action")
    lEnvironmentGrp =  FBGroup("Environment")
    lFloorsGrp = FBGroup("Floors")
    lIPLSGrp = FBGroup("IPLS")
    lDoorsGrp = FBGroup("Doors")

    lMainGrp.ConnectSrc(lCeilingGrp)
    lMainGrp.ConnectSrc(lGroundGrp)
    lMainGrp.ConnectSrc(lWallGrp)
    lMainGrp.ConnectSrc(lPropsGrp)
    lMainGrp.ConnectSrc(lDressingGrp)
    lMainGrp.ConnectSrc(lDESGrp)
    lMainGrp.ConnectSrc(lPedGrp) 
    lMainGrp.ConnectSrc(lActionGrp) 
    lMainGrp.ConnectSrc(lEnvironmentGrp)
    lMainGrp.ConnectSrc(lFloorsGrp)
    lMainGrp.ConnectSrc(lIPLSGrp)
    lMainGrp.ConnectSrc(lDoorsGrp)

    for iNull in lSetList:
        iNullName = ProjectData.data.GetNullName(iNull)
        if "prop" in iNullName:
            lPropList.append(iNull)
        elif "vehicle" in iNullName:
            lDressingList.append(iNull)
        elif "dressing" in iNullName:
            lDressingList.append(iNull)
        elif "ground" in iNullName:
            lGroundList.append(iNull)
        elif "DES" in iNull.Name:
            lDESList.append(iNull) 
        elif "wall" in iNullName:
            lWallList.append(iNull) 
        elif "ceiling" in iNullName:
            lCeilingList.append(iNull)  
        elif "_ped" in iNullName:
            lPedList.append(iNull)
        elif "action" in iNullName:
            lActionList.append(iNull)
        elif "environment" in iNullName:
            lEnvironmentList.append(iNull)
        elif "door" in iNullName:
            lDoorsList.append(iNull)
        # we are making sub groups now for floors
        elif "floor" in iNullName:
            newFloorGroup = FBGroup(iNullName)
            lFloorsGrp.ConnectSrc(newFloorGroup)
            rs_SetupFloorGroupStructure(newFloorGroup, iNull)
        # we are making sub groups now for IPLs
        elif "ipls" in iNullName:
            newIplGroup = FBGroup(iNullName)
            lIPLSGrp.ConnectSrc(newIplGroup)
            for child in iNull.Children:
                newIplGroup.ConnectSrc(child) 


    for iProp in lPropList:
        lPropsGrp.ConnectSrc(iProp)
    for iVeh in lDressingList:
        lDressingGrp.ConnectSrc(iVeh)
    for iDressing in lDressingList:
        lDressingGrp.ConnectSrc(iDressing)
    for iGround in lGroundList:
        lGroundGrp.ConnectSrc(iGround)
    for iWall in lWallList:
        lWallGrp.ConnectSrc(iWall)
    for iDES in lDESList:
        lDESGrp.ConnectSrc(iDES)
    for iCeiling in lCeilingList:
        lCeilingGrp.ConnectSrc(iCeiling)
    for iPed in lPedList:
        lPedGrp.ConnectSrc(iPed)
    for iAction in lActionList:
        lActionGrp.ConnectSrc(iAction)
    for iEnvironment in lEnvironmentList:
        lEnvironmentGrp.ConnectSrc(iEnvironment)
    for iDoor in lDoorsList:
        lDoorsGrp.ConnectSrc(iDoor)

    lCeilingGrp.Show = False

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Materials Apply
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ApplyMaterialsEnvironmentSetup():

    for iGroup in glo.gGroups:
        if iGroup.Name == "Ground":
            lGroundGrp = iGroup
        if iGroup.Name == "Wall":
            lWallGrp = iGroup
        if iGroup.Name == "Dressing":
            lDressingGrp = iGroup
        if iGroup.Name == "Props":
            lPropsGrp = iGroup
        if iGroup.Name == "DES":
            lDESGrp = iGroup
        if iGroup.Name == "Peds":
            lPedGrp = iGroup 
        if iGroup.Name == "Ceiling":
            lCeilingGrp = iGroup
        if iGroup.Name == "Action":
            lActionGrp = iGroup

    for iMaterial in glo.gMaterials:
        if iMaterial.Name == "Ground":
            lMatGround = iMaterial
        if iMaterial.Name == "Wall":
            lMatWall = iMaterial
        if iMaterial.Name == "Dressing":
            lMatDressing = iMaterial
        if iMaterial.Name == "Props":
            lMatProps = iMaterial
        if iMaterial.Name == "DES":
            lMatDes = iMaterial
        if iMaterial.Name == "Peds":
            lMatPed = iMaterial
        if iMaterial.Name == "Action":
            lMatAction = iMaterial

    for iShader in FBSystem().Scene.Shaders:
        if iShader.Name == "Transparency":
            lTrans = iShader
        if iShader.Name == "FlatCartoonShader":
            lCartoonShad = iShader

    FBSystem().Scene.Evaluate()

    for iItem in lPedGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatPed) 

    FBSystem().Scene.Evaluate()

    for iItem in lGroundGrp.Items:
        if iItem.Name != "BaseAnimation":
            iItem.Materials.append(lMatGround)
            FBSystem().Scene.Evaluate()
            iItem.Shaders.append(lCartoonShad)

    for iItem in lDressingGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatDressing)
            if "wall" in iItem.Name.lower():
                iItem.Shaders.append(lTrans)
                FBSystem().Scene.Evaluate()
                iItem.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    for iItem in lPropsGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatProps)

    for iItem in lDESGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatDes)

    for iItem in lWallGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatWall)
            iItem.Shaders.append(lTrans)
            FBSystem().Scene.Evaluate()
            iItem.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    for iItem in lActionGrp.Items:
        if iItem.Name == "BaseAnimation":
            None
        else:
            iItem.Materials.append(lMatWall)
            iItem.Shaders.append(lTrans)
            FBSystem().Scene.Evaluate()  
            iItem.ShadingMode = FBModelShadingMode.kFBModelShadingAll

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - 3dsMax Environment Setup
##                         - Setus up materials, groups, custom properties & applies materials.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_3dsMaxEnvironmentSetup(pControl, pEvent):

    rs_StripExistingMaterials()

    rs_CreateMaterialsEnvironmentSetup()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lFilePath = FBApplication().FBXFileName

    rs_ApplyMaterialsEnvironmentSetup()

    ass.rs_AssetSetupCustomProperties()

    lSetRoot = RS.Utils.Scene.FindModelByName('SetRoot')

    if lSetRoot != None:
        lTypeTag = lSetRoot.PropertyCreate('Asset_Setup', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTypeTag.Data = "3dsMax"

        # get parent and children ina  list and disable transforms
        envNullList = []
        RS.Utils.Scene.GetChildren(lSetRoot, envNullList, '', True)
        for null in envNullList:
            enableTrnsProp = null.PropertyList.Find('Enable Transformation')
            if enableTrnsProp:
                enableTrnsProp.Data = False

    # scale down nulls
    ass.ScaleDownNulls()

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    FBMessageBox("3ds Setup", lFileName + " has now been setup.", "Ok")

#rs_3dsMaxEnvironmentSetup(None, None)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - obj Environment Setup PART 1
##                         - Sets up the groups, materials, custom properties
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_objEnvironmentSetup(pControl, pEvent):

    rs_StripExistingMaterials()

    rs_CreateMaterialsEnvironmentSetup()

    lSetRoot = None
    lSetRoot = RS.Utils.Scene.FindModelByName("SetRoot")

    if lSetRoot != None:
        lSetRoot.PropertyList.Find("Enable Rotation DOF").Data = False

        # get parent and children ina  list and disable transforms
        envNullList = []
        RS.Utils.Scene.GetChildren(lSetRoot, envNullList, '', True)
        for null in envNullList:
            enableTrnsProp = null.PropertyList.Find('Enable Transformation')
            if enableTrnsProp:
                enableTrnsProp.Data = False

    # scale down nulls
    ass.ScaleDownNulls()

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    FBMessageBox("obj Setup", "Please assign each mesh null to the appropriate groups\nbefore pressing the 'Assign Materials' button.", "Ok")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - obj Environment Setup PART 2
##                         - Applies Materials to groups
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_objApplyMaterialEnvironmentSetup(pControl, pEvent):

    rs_ApplyMaterialsEnvironmentSetup()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName) 

    ass.rs_AssetSetupCustomProperties()

    lSetRoot = RS.Utils.Scene.FindModelByName('SetRoot')

    lTypeTag = lSetRoot.PropertyCreate('Asset_Setup', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "RAG obj"

    lSetRoot = None
    lSetRoot = RS.Utils.Scene.FindModelByName("SetRoot")

    if lSetRoot != None:
        lSetRoot.PropertyList.Find("Enable Rotation DOF").Data = False

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    FBMessageBox("obj Setup", lFileName + " has now been setup.", "Ok")
