from pyfbsdk import *

from PySide import QtCore, QtGui

import os

import xml.dom.minidom as minidom

import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Core.Reference.SceneConfigXML as xml
import RS.Core.Face.RigUISetup as lat
import RS.Core.Face.ConnectRigInABox as concs
import RS.Core.Face.ConnectRigInABoxAmbient as conig
import RS.Core.Face.UFC_Setup as ufcsetup
import RS.Core.Character.IngameCharacterIKControls as ikcon
import RS.Core.Character.Setup.HighHeel as heel
import RS.Perforce as p4
import RS.Utils.Path
import RS.Utils.Scene
from RS import Perforce
from RS.Utils.Scene import Character
from RS import ProjectData


class CharacterSolver():
    def __init__( self ):
        # mb character solver
        self.CharacterSolver = 2


def rs_IK_ConstraintsFolder():
    # creates the constraints folder
    lFolder = None
    for iFolder in glo.gFolders:
        if iFolder.Name == 'Constraints 1':
            lFolder = iFolder
    if lFolder == None:
        lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder("Constraints 1", lPlaceholder)
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        FBSystem().Scene.Evaluate()
        lPlaceholder.FBDelete()
    return lFolder


def rs_AssetSetupCharToyboxMoverControl():
    # setup mover and toybox relation - for use in rs_CSCharacterSetup & rs_IGCharacterSetup
    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]
    lSkelRoot = lMover.Children[0]

    lToyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
    lToyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    lToyBoxMarker.Size = 500
    lToyBoxMarker.Color = FBColor(1, 0, 1)
    lToyBoxMarker.Parent = lDummy
    lToyBoxMarker.Translation.Data = lMover.Translation.Data
    lToyBoxMarker.Rotation.Data = lMover.Rotation.Data
    lTag = lToyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Contacts"

    lMover.Selected = True
    lMover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lMover.Selected = False
    lMover.PropertyList.Find("Visibility").Data = True
    lMoverMarker = FBModelMarker("mover_Control")
    lMoverMarker.Show = True
    lMoverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    lMoverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)
    lVector = FBVector3d()
    lMover.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
    lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
    lMover.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    lMoverMarker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    FBSystem().Scene.Evaluate()
    lMoverMarker.Parent = lSkelRoot

    lRelationCon = FBConstraintRelation('mover_toybox_Control')
    lMoverMarkerSender = lRelationCon.SetAsSource(lMoverMarker)
    lRelationCon.SetBoxPosition(lMoverMarkerSender, 0, 0)
    lToyboxMarkerReceive = lRelationCon.ConstrainObject(lToyBoxMarker)
    lRelationCon.SetBoxPosition(lToyboxMarkerReceive, 300, 0)
    lToyBoxMarkerSender = lRelationCon.SetAsSource(lToyBoxMarker)
    lRelationCon.SetBoxPosition(lToyBoxMarkerSender, 0, 150)
    lToyBoxMarkerSender.UseGlobalTransforms = False
    lMoverReceive = lRelationCon.ConstrainObject(lMover)
    lRelationCon.SetBoxPosition(lMoverReceive, 350, 150)
    lMoverReceive.UseGlobalTransforms = False
    RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Translation', lToyboxMarkerReceive, 'Translation')
    RS.Utils.Scene.ConnectBox(lMoverMarkerSender, 'Rotation', lToyboxMarkerReceive, 'Rotation')
    RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Translation', lMoverReceive, 'Lcl Translation')
    RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Rotation', lMoverReceive, 'Lcl Rotation')
    lRelationCon.Active = False
    return lRelationCon


def rs_AssetSetupFacingDirConstraint():
    if not ProjectData.data.GetConfig("CharSetupFacingDirection"):
        # project does not need this setup
        return

    # constraint OH_UpperFixup and OH_IndependentMover to OH_FacingDir
    lIKFacingDirection = RS.Utils.Scene.FindModelByName('OH_FacingDirection')
    lOHUpperFixupDirection = RS.Utils.Scene.FindModelByName('OH_UpperFixupDirection')
    lOHIndependentMoverDirection = RS.Utils.Scene.FindModelByName('OH_IndependentMoverDirection')

    if lIKFacingDirection and lOHUpperFixupDirection and lOHIndependentMoverDirection:

        lUpperFixupConstraint = FBConstraintManager().TypeCreateConstraint(10)
        lUpperFixupConstraint.Name = "OH_UpperFixup_Constraint"
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lUpperFixupConstraint)
        lUpperFixupConstraint.ReferenceAdd (0,lOHUpperFixupDirection)
        lUpperFixupConstraint.ReferenceAdd (1,lIKFacingDirection)
        lUpperFixupConstraint.Snap()
        lIndependentMoverConstraint = FBConstraintManager().TypeCreateConstraint(10)
        lIndependentMoverConstraint.Name = "OH_IndependentMover_Constraint"
        lConFolder.Items.append(lIndependentMoverConstraint)
        lIndependentMoverConstraint.ReferenceAdd (0,lOHIndependentMoverDirection)
        lIndependentMoverConstraint.ReferenceAdd (1,lIKFacingDirection)
        lIndependentMoverConstraint.Snap()


def rs_PrepJointLooperCharacterSetup(pModel, pX, pY, pZ,pColor, pBool):
    # prep joints loop - for use in rs_PrepJointsCharacterSetup
    lVector = FBVector3d()
    lColor = pModel.PropertyList.Find('Color RGB')
    pModel.SetVector(FBVector3d(pX, pY, pZ), FBModelTransformationType.kModelRotation, pBool)
    FBSystem().Scene.Evaluate()
    lColor.Data = pColor


def rs_SpinePrepJointLooperCharacterSetup(pModel, pColor, pBool):
    if not ProjectData.data.GetConfig("CharSetupPrepSpine"):
        # project does not need this setup
        return
    # prep spine joints loop
    lVector = FBVector3d()
    lColor = pModel.PropertyList.Find('Color RGB')
    FBSystem().Scene.Evaluate()
    lColor.Data = pColor


def rs_PrepFeetJointsCharacterSetup(pModel, pCSSetup=True):
    # prep joints - for use in rs_AssetSetupCSCharacters
    lLThighRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[46]).PropertyList.Find("Lcl Rotation").Data
    lRThighRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[47]).PropertyList.Find("Lcl Rotation").Data
    lLCalfRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[49]).PropertyList.Find("Lcl Rotation").Data
    lRCalfRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[53]).PropertyList.Find("Lcl Rotation").Data
    lLThighPlusCalf = lLThighRot[2] + lLCalfRot[2]
    lRThighPlusCalf = lRThighRot[2] + lRCalfRot[2]

    lLFootRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[50]).PropertyList.Find("Lcl Rotation").Data
    lRFootRot = RS.Utils.Scene.FindModelByName(glo.gSkelArray[54]).PropertyList.Find("Lcl Rotation").Data
    if pModel.Name == glo.gSkelArray[50]:
        if lLThighPlusCalf > 0:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lLFootRot[2] - lLThighPlusCalf), glo.gGreen, False)
            FBSystem().Scene.Evaluate()
        else:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lLFootRot[2] + lLThighPlusCalf), glo.gGreen, False)
            FBSystem().Scene.Evaluate()
    if pModel.Name == glo.gSkelArray[54]:
        if lRThighPlusCalf > 0:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lRFootRot[2] - lRThighPlusCalf), glo.gOrange, False)
            FBSystem().Scene.Evaluate()
        else:
            rs_PrepJointLooperCharacterSetup(pModel, 0,0,(lRFootRot[2] + lRThighPlusCalf), glo.gOrange, False)
            FBSystem().Scene.Evaluate()
    if pModel.Name == glo.gSkelArray[52]:
        lColor = pModel.PropertyList.Find('Color RGB').Data = glo.gGreen
    if pModel.Name == glo.gSkelArray[56]:
        lColor = pModel.PropertyList.Find('Color RGB').Data = glo.gOrange

def rs_PrepJointsCharacterSetup(pModel, pFemaleSetup, pCSSetup=True):
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lSize = pModel.PropertyList.Find('Size')
    if lSize:
        lSize.Data = 0.85

    if ProjectData.data.GetConfig("CharSetupNeckZeroTrns"):
        # this is to make sure the Neck0 has no translation at all which causes problems to the control rig
        lNeck0 = RS.Utils.Scene.FindModelByName(glo.gSkelArray[66])
        if lNeck0:
            lNeck0.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)

    # run the projdata scripts so it picks up the correct project for the setup
    xRot, yRot, zRot, color, useGlobal, size, prepDef = ProjectData.data.PrepJointsCharacterSetup(pModel, pFemaleSetup, lFileName, pCSSetup)
    if size is not None:
        lSize.Data = size

    if prepDef is 0:
        # joint prep
        rs_PrepJointLooperCharacterSetup(pModel, xRot, yRot, zRot, FBColor(*color), useGlobal)
    elif prepDef is 1:
        # spine prep
        rs_SpinePrepJointLooperCharacterSetup(pModel, FBColor(*color), useGlobal)
    elif prepDef is 2:
        # color prep
        pModel.PropertyList.Find('Color RGB').Data = FBColor(*color)

    FBSystem().Scene.Evaluate()


def rs_FootHandContactCharacterSetup(pCharacter):
    #Foot/hand Contact Setup - for use in rs_AssetSetupCSCharacters
    lCalf = RS.Utils.Scene.FindModelByName(glo.gSkelArray[49])
    rCalf = RS.Utils.Scene.FindModelByName(glo.gSkelArray[53])

    lGlobePlane = FBModelPlane("Global_Contact")
    lGlobePlane.Show = True
    lGlobePlane.Translation = FBVector3d(-0.2,0,6)
    lGlobePlane.Rotation = FBVector3d(0,-0,0)
    lGlobePlane.Scaling = FBVector3d(0.56,0.39,0.20)

    lRHandPlane = FBModelPlane("R_Hand_Contact")
    lRHandPlane.Show = True
    lRHandPlane.Translation = FBVector3d(-38.99,0,5)
    lRHandPlane.Rotation = FBVector3d(0,-0,0)
    lRHandPlane.Scaling = FBVector3d(0.15,0.16,0.08)
    lLHandPlane = FBModelPlane("L_Hand_Contact")
    lLHandPlane.Show = True
    lLHandPlane.Translation = FBVector3d(39.18,0,5)
    lLHandPlane.Rotation = FBVector3d(0,-0,0)
    lLHandPlane.Scaling = FBVector3d(0.15,0.16,0.08)

    lRFootPlane = FBModelPlane("R_Foot_Contact")
    lRFootPlane.Show = True
    lRFootPlane.Translation = FBVector3d(0,0,6.32)
    RS.Utils.Scene.AlignTranslation(lRFootPlane, RS.Utils.Scene.FindModelByName(glo.gSkelArray[53]), True, False, False)
    lRFootPlane.Rotation = FBVector3d(0,-0,0)
    lRFootPlane.Scaling = FBVector3d(0.08,0.15,0.18)
    lLFootPlane = FBModelPlane("L_Foot_Contact")
    lLFootPlane.Show = True
    lLFootPlane.Translation = FBVector3d(0,0,6.32)
    RS.Utils.Scene.AlignTranslation(lLFootPlane, RS.Utils.Scene.FindModelByName(glo.gSkelArray[49]), True, False, False)
    lLFootPlane.Rotation = FBVector3d(0,-0,0)
    lLFootPlane.Scaling = FBVector3d(0.08,0.15,0.18)

    lGlobalPlaneMat = FBMaterial('Global_Contact_Mat')
    lGlobalPlaneMat.Emissive = FBColor(0, 0, 0)
    lGlobalPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lGlobalPlaneMat.Diffuse = FBColor(0.15, 0.15, 0.15)
    lGlobalPlaneMat.ConnectDst(lGlobePlane)
    lLHandPlaneMat = FBMaterial('L_Hand_Contact_Mat')
    lLHandPlaneMat.Emissive = FBColor(0, 0, 0)
    lLHandPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lLHandPlaneMat.Diffuse = FBColor(0.4,0.7,0)
    lLHandPlaneMat.ConnectDst(lLHandPlane)
    lRHandPlaneMat = FBMaterial('R_Hand_Contact_Mat')
    lRHandPlaneMat.Emissive = FBColor(0, 0, 0)
    lRHandPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lRHandPlaneMat.Diffuse = FBColor(0.9,0.4,0)
    lRHandPlaneMat.ConnectDst(lRHandPlane)
    lLFootPlaneMat = FBMaterial('L_Foot_Contact_Mat')
    lLFootPlaneMat.Emissive = FBColor(0, 0, 0)
    lLFootPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lLFootPlaneMat.Diffuse = FBColor(0.4,0.7,0)
    lLFootPlaneMat.ConnectDst(lLFootPlane)
    lRFootPlaneMat = FBMaterial('R_Foot_Contact_Mat')
    lRFootPlaneMat.Emissive = FBColor(0, 0, 0)
    lRFootPlaneMat.Ambient = FBColor(0.2, 0.2, 0.2)
    lRFootPlaneMat.Diffuse = FBColor(0.9,0.4,0)
    lRFootPlaneMat.ConnectDst(lRFootPlane)

    lWireShader = FBShaderManager().CreateShader("WireShader")
    lWireShader.PropertyList.Find("Line Width").Data = 3.23
    lWireShader.Name = ("Contact_WireShader")

    lGlobePlane.Shaders.append(lWireShader)
    lRHandPlane.Shaders.append(lWireShader)
    lLHandPlane.Shaders.append(lWireShader)
    lRFootPlane.Shaders.append(lWireShader)
    lLFootPlane.Shaders.append(lWireShader)

    lGlobePlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lGlobePlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lRHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lRHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lLHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lLHandPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lRFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lRFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lLFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lLFootPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    lRHandPlane.Parent = lGlobePlane
    lLHandPlane.Parent = lGlobePlane
    lRFootPlane.Parent = lGlobePlane
    lLFootPlane.Parent = lGlobePlane
    lGlobePlane.Parent = RS.Utils.Scene.FindModelByName("Dummy01")

    #re-set rotations for foot planes. Seems to need this in MB14!
    lRFootPlane.Rotation = FBVector3d(0,0,0)
    lLFootPlane.Rotation = FBVector3d(0,0,0)

def rs_SetHingeDOF():
    # setting Degrees of Freedom for Hinges
    hingeList =[RS.Utils.Scene.FindModelByName(glo.gSkelArray[14]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[15]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[17]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[18]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[20]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[21]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[23]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[24]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[26]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[27]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[32]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[33]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[35]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[36]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[38]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[39]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[41]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[42]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[44]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[45]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[12]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[30]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[49]),
                RS.Utils.Scene.FindModelByName(glo.gSkelArray[53])]
    for joint in hingeList:
        if joint:
            joint.PropertyList.Find("Enable Rotation DOF").Data = True
            joint.PropertyList.Find("RotationMinX").Data = True
            joint.PropertyList.Find("RotationMinY").Data = True
            joint.PropertyList.Find("RotationMin").Data = FBVector3d(-2,-2,0)
            joint.PropertyList.Find("RotationMaxX").Data = True
            joint.PropertyList.Find("RotationMaxY").Data = True
            joint.PropertyList.Find("RotationMax").Data = FBVector3d(2,2,0)


def rs_CharacterSetupTidy():
    lJointList = []
    lContactList = []

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lDummy = RS.Utils.Scene.FindModelByName("Dummy01")
    lMover = RS.Utils.Scene.FindModelByName("mover")
    lExportable = RS.Utils.Scene.FindModelByName("SKEL_ROOT")
    lContactMain = RS.Utils.Scene.FindModelByName("Global_Contact")
    lGeo = RS.Utils.Scene.FindModelByName(lFileName)
    RS.Utils.Scene.GetChildren(lContactMain, lContactList)
    RS.Utils.Scene.GetChildren(lExportable, lJointList)

    # eyebrow setup url:bugstar:2198726
    facialHairGeoList = []
    eyeBrowString = 'mis2_'
    beardString = 'berd_'
    hairString = 'hair_'
    for component in RS.Globals.Components:
        if eyeBrowString in component.Name.lower():
            facialHairGeoList.append(component)
        if beardString in component.Name.lower():
            facialHairGeoList.append(component)
        if hairString in component.Name.lower():
            facialHairGeoList.append(component)
    if facialHairGeoList:
        lightedShader = FBShaderLighted('facialHair_Lighted')
        lightedShader.Transparency = FBAlphaSource.kFBAlphaSourceMatteAlpha
        for geo in facialHairGeoList:
            if geo.ClassName() == "FBModel":
                lightedShader.Append(geo)
                geo.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    # folders
    lMaterials = FBSystem().Scene.Materials
    lMaterialFolderStr = "Materials"
    lTextures = FBSystem().Scene.Textures
    lTexturesFolderStr = "Textures"
    lConstraints = FBSystem().Scene.Constraints
    lConstraintFolderStr = "Constraints"
    lShaders = FBSystem().Scene.Shaders
    lShaderFolderStr = "Shaders"
    lVideoClips = FBSystem().Scene.VideoClips
    if len(lVideoClips) > 0:
        lVideoFolder = FBFolder ("Videos", lVideoClips[0])
        for lVideo in lVideoClips:
            lVideoFolder.ConnectSrc(lVideo)
    cre.rs_CreateFolderAddComponents(lMaterials, lMaterialFolderStr)
    cre.rs_CreateFolderAddComponents(lTextures, lTexturesFolderStr)
    cre.rs_CreateConstraintFolderAddComponents(lConstraints, lConstraintFolderStr)
    cre.rs_CreateFolderAddComponents(lShaders, lShaderFolderStr)

    # default materials
    for lMat in lMaterials:
        if lMat:
            if "_Contact" in lMat.Name:
                None
            else:
                lMat.Ambient = FBColor(0,0,0)
                lMat.Diffuse = FBColor(1,1,1)

    # parent facial
    lFaceControls = RS.Utils.Scene.FindModelByName("faceControls_OFF")
    lFaceScaleOffset = RS.Utils.Scene.FindModelByName("RECT_ScaleOffset")
    lFaceScaleOffsetCtrl = RS.Utils.Scene.FindModelByName("ScaleOffset")
    lFaceAttrGUI = RS.Utils.Scene.FindModelByName("FacialAttrGUI")

    if lFaceScaleOffset:
        lFaceScaleOffset.Parent = None
    if lFaceScaleOffsetCtrl:
        if lFaceScaleOffsetCtrl.Translation.GetAnimationNode():
            animNodes = lFaceScaleOffsetCtrl.Translation.GetAnimationNode().Nodes
            for node in animNodes:
                node.FCurve.EditClear()
        lFaceScaleOffsetCtrl.Translation.Data = FBVector3d(0,0,0)
    if lFaceAttrGUI and lFaceControls:
        if lFaceAttrGUI.Translation.GetAnimationNode():
            if not ProjectData.data.GetConfig("CharSetupFaceGUI"):
                animNodes = lFaceScaleOffsetCtrl.Translation.GetAnimationNode().Nodes
            else:
                animNodes = lFaceAttrGUI.Translation.GetAnimationNode().Nodes
            for node in animNodes:
                node.FCurve.EditClear()
        if ProjectData.data.GetConfig("CharSetupFaceTempParent"):
            tempParent = lFaceControls.Parent
            lFaceControls.Parent = None
            newPos = lFaceControls.Translation.Data + FBVector3d(21,6,0)
            lFaceControls.Parent = tempParent
            lFaceAttrGUI.Translation.Data = newPos
        else:
            newPos = lFaceControls.Translation.Data + FBVector3d(21,6,0)
            lFaceAttrGUI.Translation.Data = newPos

    #facial lists
    lFaceText = RS.Utils.Scene.FindModelByName("TEXT_Normals")
    lFaceText2 = RS.Utils.Scene.FindModelByName("Text001")
    lFacialTextList = []
    if lFaceText:
        RS.Utils.Scene.GetChildren(lFaceText, lFacialTextList)
    if lFaceText2:
        RS.Utils.Scene.GetChildren(lFaceText2, lFacialTextList)

    lFaceFacial = RS.Utils.Scene.FindModelByName("Facial_UI")
    lFaceAttr = RS.Utils.Scene.FindModelByName("FacialAttrGUI")
    lFacial3LateralList = []
    if lFaceFacial:
        RS.Utils.Scene.GetChildren(lFaceFacial, lFacial3LateralList)
    if lFaceControls:
        RS.Utils.Scene.GetChildren(lFaceControls, lFacial3LateralList)
    if lFaceAttr:
        RS.Utils.Scene.GetChildren(lFaceAttr, lFacial3LateralList)

    lAmbient = RS.Utils.Scene.FindModelByName("Ambient_UI")
    lFaceFX = RS.Utils.Scene.FindModelByName("FaceFX")
    lFacialAmbientList = []
    if lAmbient:
        RS.Utils.Scene.GetChildren(lAmbient, lFacialAmbientList)
    if lFaceFX:
        RS.Utils.Scene.GetChildren(lFaceFX, lFacialAmbientList)

    # groups
    lMainGroup = FBGroup(lFileName)
    if len(lFacial3LateralList) > 0:
        lThreeLateralGroup = FBGroup("3Lateral")
    else:
        lThreeLateralGroup = None
    if len(lFacialAmbientList) > 0:
        lAmbientFacialGroup = FBGroup("Ambient_Facial")
    else:
        lAmbientFacialGroup = None
    lDummyGroup = FBGroup("Dummy01")
    lMoverGroup = FBGroup("mover")
    lJointGroup = FBGroup("Joints")
    lContactsGroup = FBGroup("Contacts")
    lPHGroup = FBGroup("PH_Nulls")
    lHeelsGroup = None

    if len(lFacialTextList) > 0:
        for iTextItem in lFacialTextList:
            if iTextItem:
                lTag = iTextItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_3Lateral"
                iTextItem.Show = False
                iTextItem.Pickable = False
    if len(lFacial3LateralList) > 0:
        for i3LateralItem in lFacial3LateralList:
            if i3LateralItem:
                lTag = i3LateralItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_3Lateral"
                lThreeLateralGroup.ConnectSrc(i3LateralItem)
    if len(lFacialAmbientList) > 0:
        for iAmbientItem in lFacialAmbientList:
            if iAmbientItem:
                lTag = iAmbientItem.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = "rs_3Lateral"
                lAmbientFacialGroup.ConnectSrc(iAmbientItem)

    lDummyGroup.ConnectSrc(lDummy)
    lMoverGroup.ConnectSrc(lMover)
    for iJoint in lJointList:
        lJointGroup.ConnectSrc(iJoint)
    for iContact in lContactList:
        lContactsGroup.ConnectSrc(iContact)

    PH_List = FBComponentList()
    FBFindObjectsByName( "PH_*", PH_List, False, True )
    for each in PH_List:
        each.PropertyList.Find( 'Look' ).Data = 1
        each.PropertyList.Find( 'Size' ).Data = 2
        lPHGroup.ConnectSrc( each )

    lHeelsList = []
    lHeels = None
    lHeelProxyGroup = None
    lHeels = RS.Utils.Scene.FindModelByName("RECT_HeelHeight")
    if lHeels != None:
        lHeelsGroup = FBGroup("HighHeel_Slider")
        lHeelsGroup.Show = False
        lHeelsGroup.Pickable = True
        lHeelsGroup.Transformable = True
        lHeelProxyGroup = FBGroup("HighHeel_Proxies")
        lHeelProxyGroup.Show = False
        lHeelProxyGroup.Pickable = False
        lHeelProxyGroup.Transformable = False
        RS.Utils.Scene.GetChildren(lHeels, lHeelsList)
    if len( lHeelsList ) > 0:
        for iHeel in lHeelsList:
            lTag = iHeel.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"
            lHeelsGroup.ConnectSrc(iHeel)

    lEORProxy = RS.Utils.Scene.FindModelByName("EO_R_Proxy")
    if lEORProxy:
        lTag = lEORProxy.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        if lHeelProxyGroup != None:
            lHeelProxyGroup.ConnectSrc(lEORProxy)
    lEOLProxy = RS.Utils.Scene.FindModelByName("EO_L_Proxy")
    if lEOLProxy:
        lTag = lEOLProxy.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"
        if lHeelProxyGroup != None:
            lHeelProxyGroup.ConnectSrc(lEOLProxy)

    if lThreeLateralGroup != None:
        lMainGroup.ConnectSrc(lThreeLateralGroup)
    if lAmbientFacialGroup != None:
        lMainGroup.ConnectSrc(lAmbientFacialGroup)
    lMainGroup.ConnectSrc(lDummyGroup)
    lMainGroup.ConnectSrc(lMoverGroup)
    lMainGroup.ConnectSrc(lJointGroup)
    lMainGroup.ConnectSrc(lContactsGroup)
    lJointGroup.ConnectSrc(lPHGroup)
    if lHeelsGroup:
        lMainGroup.ConnectSrc(lHeelsGroup)
        lMainGroup.ConnectSrc(lHeelProxyGroup)
    for iGroup in glo.gGroups:
        if iGroup.Name == 'GeoVariations':
            lMainGroup.ConnectSrc(iGroup)
            iGroup.Transformable = False
            iGroup.Pickable = False
            iGroup.Show = True

    if lThreeLateralGroup != None:
        lThreeLateralGroup.Transformable = True
        lThreeLateralGroup.Pickable = True
        lThreeLateralGroup.Show = False
    if lAmbientFacialGroup != None:
        lAmbientFacialGroup.Transformable = True
        lAmbientFacialGroup.Pickable = True
        lAmbientFacialGroup.Show = False
    lDummyGroup.Show = False
    lDummyGroup.Pickable = False
    lDummyGroup.Transformable = False
    lJointGroup.Pickable = False
    lJointGroup.Transformable = False
    lContactsGroup.Pickable = False
    lContactsGroup.Transformable = False
    lMoverGroup.Show = False
    lMoverGroup.Pickable = False
    lMoverGroup.Transformable = False

    #safe bone group
    lSafeBoneGroup = FBGroup("SafeBones")
    lTag = lSafeBoneGroup .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Groups"

    boneStringList = ProjectData.data.SafeBoneList()
    for boneString in boneStringList:
        boneModel = RS.Utils.Scene.FindModelByName(boneString)
        if boneModel:
            lSafeBoneGroup.ConnectSrc(boneModel)

    # expressions group
    lExpressionsList = []
    for iComponent in glo.gComponents:
        if iComponent.Name.startswith('MH_'):
            lExpressionsList.append(iComponent)
        if iComponent.Name.startswith('RB_'):
            lExpressionsList.append(iComponent)
        if iComponent.Name.startswith('SM_'):
            lExpressionsList.append(iComponent)
        if iComponent.Name.startswith('SPR_'):
            lExpressionsList.append(iComponent)
    lExpressionsGroup = FBGroup("Expressions")
    lTag = lExpressionsGroup .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Groups"
    for iExpr in lExpressionsList:
        if iExpr:
            lExpressionsGroup.ConnectSrc(iExpr)
        else:
            None

    # delete setup items
    ass.rs_AssetSetupDeleteItems()


def rs_CharacterExtension(character):
    # character extensions
    boneList = ProjectData.data.CharacterExtensionBoneStringList()

    placeholder = FBCharacterExtension('Remove_Me')
    folder = FBFolder("character_extensions", placeholder)
    rsProperty = folder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    rsProperty.Data = "rs_Folders"

    characterExtensionList = []
    for bone in boneList:
        model = RS.Utils.Scene.FindModelByName(bone)
        if model:
            characterExt = FBCharacterExtension(model.Name)
            if model.Name == 'mover':
                characterExt.Label = 'mover_prop_extension'
            else:
                characterExt.Label = model.Name
            character.AddCharacterExtension(characterExt)
            FBConnect(model, characterExt)
            characterExtensionList.append(characterExt)
            folder.Items.append(characterExt)
            rsProperty = characterExt.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            rsProperty.Data = "rs_Extensions"

    for folder in glo.gFolders:
        if folder.Name == 'Constraints 1':
            for item in folder.Items:
                if isinstance(item, FBCharacter):
                    folder.Items.remove(item)
    placeholder.FBDelete()
    FBSystem().Scene.Evaluate()
    return characterExtensionList


def rs_FixShoulders():
    # Dave Bailey's Traps Shoulder Fix Def url:bugstar:754532
    lTrapRSupport = RS.Utils.Scene.FindModelByName("Trap_R_Support")
    lUpperArmR    = RS.Utils.Scene.FindModelByName("SKEL_R_UpperArm")
    lTrapLSupport = RS.Utils.Scene.FindModelByName("Trap_L_Support")
    lUpperArmL    = RS.Utils.Scene.FindModelByName("SKEL_L_UpperArm")

    if lTrapRSupport and lUpperArmR:
        lConstraintManager = FBConstraintManager()
        lAimConstraint = lConstraintManager.TypeCreateConstraint(0)
        lAimConstraint.Name = "Trap_R_Support_Aim"
        lAimConstraint.ReferenceAdd(0,lTrapRSupport)
        lAimConstraint.ReferenceAdd(1,lUpperArmR)
        lAimConstraint.PropertyList.Find("AimVector").Data = FBVector3d(1,0,0)
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("UpVector").Data  = FBVector3d(0,0,1)
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("WorldUpType").Data = 0
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("WorldUpVector").Data = FBVector3d(0,1,0)
        glo.gScene.Evaluate()
        lAimConstraint.Active = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lAimConstraint)

    if lTrapLSupport and lUpperArmL:
        lConstraintManager = FBConstraintManager()
        lAimConstraint = lConstraintManager.TypeCreateConstraint(0)
        lAimConstraint.Name = "Trap_L_Support_Aim"
        lAimConstraint.ReferenceAdd(0,lTrapLSupport)
        lAimConstraint.ReferenceAdd(1,lUpperArmL)
        lAimConstraint.PropertyList.Find("AimVector").Data = FBVector3d(1,0,0)
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("UpVector").Data  = FBVector3d(0,0,-1)
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("WorldUpType").Data = 0
        glo.gScene.Evaluate()
        lAimConstraint.PropertyList.Find("WorldUpVector").Data = FBVector3d(0,1,0)
        glo.gScene.Evaluate()
        lAimConstraint.Active = True
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lAimConstraint)


class BoneRotation:
    # Save rotation and name of the bones - This function is used to store the Max pose
    name = ''
    rotation = []

def rs_SaveBoneRotation(pBone,pBoneRotationList):
    lTempBone = BoneRotation()
    lTempBone.name=''
    lTempBone.rotation=[]
    lTempBone.name = pBone.Name
    lTempBone.rotation.append (pBone.Rotation[0])
    lTempBone.rotation.append (pBone.Rotation[1])
    lTempBone.rotation.append (pBone.Rotation[2])
    pBoneRotationList.append (lTempBone)


def rs_LoadBoneRotation(pBoneList):
    # Load rotation of bones - This function is used to Load the stored max rotation of each bone
    # back into the skeleton on MaxFigPose take
    for bone in pBoneList:
        ModelFound = FBFindModelByLabelName(bone.name)
        ModelFound.Rotation.SetAnimated(True)
        CreateAnimation(ModelFound.Rotation.GetAnimationNode(),bone.rotation)


def CreateAnimation(pNode, pRotation):
    if pNode.Nodes:
        lXFCurve = pNode.Nodes[0].FCurve
        lXFCurve.KeyAdd(FBTime.Zero, pRotation[0])
        lYFCurve = pNode.Nodes[1].FCurve
        lYFCurve.KeyAdd(FBTime.Zero, pRotation[1])
        lZFCurve = pNode.Nodes[2].FCurve
        lZFCurve.KeyAdd(FBTime.Zero, pRotation[2])


def rs_PrepSetupCharacters(pCSSetup=True):
    # Prepping Character Setup
    lSkelList = []
    lLODList = []
    lLODParent = None
    lLODList2 = []
    lLODParent2 = None
    lBoneRotationList = []
    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]
    lSkelRoot = lMover.Children[0]

    # dummy property
    lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "Character"

    # delete all animation
    FBSystem().CurrentTake.ClearAllProperties(False)

    # delete lods
    for iSceneItem in lSceneList:
        if '-l2' in iSceneItem.Name.lower():
            lLODParent2 = iSceneItem
            break
    for iSceneItem in lSceneList:
        if '-l' in iSceneItem.Name.lower() and '-l2' not in iSceneItem.Name.lower():
            lLODParent = iSceneItem
            break
    for iSceneItem in lSceneList:
        if 'misc_' in iSceneItem.Name.lower():
            iSceneItem.Parent = lGeo
    if lLODParent != None:
        RS.Utils.Scene.GetChildren(lLODParent, lLODList)
    if lLODParent2 != None:
        RS.Utils.Scene.GetChildren(lLODParent2, lLODList2)
        for lLOD in lLODList:
            lLOD.FBDelete()
        for lLOD2 in lLODList2:
            lLOD2.FBDelete()

    # set pre-rotation on dummy
    lDummy.PropertyList.Find("Pre Rotation").Data = FBVector3d(0,0,0)

    # adjust rotation of dummy and mover
    FBDisconnect(lMover, lDummy)
    lDummy.Rotation = FBVector3d(-90,0,0)
    FBConnect(lMover, lDummy)
    lMover.Rotation = FBVector3d(0,0,0)
    lGeo.Parent = lDummy
    FBSystem().Scene.Evaluate()

    # create takes for max and t-poses
    FBSystem().Scene.Takes[0].Name = 'MaxFigPose'
    FBSystem().Scene.Takes[0].CopyTake ('MBStancePose')
    FBSystem().CurrentTake = FBSystem().Scene.Takes[1]

    RS.Utils.Scene.GetChildren(lMover, lSkelList, "", False)
    pFemaleSetup = None
    pFemaleSetup = RS.Utils.Scene.FindModelByName("SPR_L_Breast")

    # setup feet
    for iSkel in lSkelList:
        rs_PrepFeetJointsCharacterSetup(iSkel, pCSSetup)
        FBSystem().Scene.Evaluate()

    # t-pose character
    for iSkel in lSkelList:
        # save max pose, before t-posing. this will be applied to the maxfig pose take
        rs_SaveBoneRotation(iSkel, lBoneRotationList)
        # apply rotation to the bones for T-Pose
        rs_PrepJointsCharacterSetup(iSkel, pFemaleSetup, pCSSetup)
        FBSystem().Scene.Evaluate()

    # create character
    lCharacter = FBCharacter(lFileName)

    # New for MB14 - it defaults to 'none', so you have to force it to select the char to be active
    lCharacter.Selected = True

    # foot and hand contacts setup
    rs_FootHandContactCharacterSetup(lCharacter)
    FBSystem().Scene.Evaluate()

    # assign joints
    RS.Utils.Scene.AssignJoints(lCharacter)

    # set solver type
    SolverClass = CharacterSolver()
    lCharacter.PropertyList.Find("CharacterSolverSelector").Data = SolverClass.CharacterSolver

    # set characterize On
    lCharacter.SetCharacterizeOn(True)

    # load the pre-saved max pose onto the maxfigpose take
    FBSystem().CurrentTake = FBSystem().Scene.Takes[0]
    lCharacter.ActiveInput = False
    rs_LoadBoneRotation(lBoneRotationList)

    # default the current take to the t-pose
    FBSystem().CurrentTake = FBSystem().Scene.Takes[1]
    lCharacter.ActiveInput = True
    FBSystem().Scene.Evaluate()

def ReturnMaxProperties():
    '''
    Gather property data from the max custom properties

    Returns:
        String, String : MAXFILE (path tot he max file)
                         REVISION (revision of the max file used in the fbx)
    '''
    # go through hierarchy and find the max properties
    maxProperty = None
    for component in RS.Globals.Components:
        maxProperty = component.PropertyList.Find('UDP3DSMAX')
        if maxProperty:
            break
    if maxProperty == None:
        return None, None

    maxRevisionResult = None
    maxPathResult = None
    dataList = maxProperty.Data.split('\n')
    for data in dataList:
        if 'REVISION' in data:
            maxRevision = data.replace('\r', '')
            maxRevisionResult = maxRevision.split(' ')[-1]
        elif 'MAXFILE' in data:
            maxPath = data.replace('\r', '')
            maxPathResult = maxPath.split('=')[-1]

    if maxRevisionResult and maxPathResult:
        return  maxRevisionResult, maxPathResult
    else:
        return None, None


def CheckMaxRevision():
    '''
    Compares the current p4 max file revision with the one used by the resourcer.
    Will notify resourcer if their max asset is out of date.

    Returns:
        String : 'Cancel' if the user has selected the Cancel button.
    '''
    currentMaxRevision, maxPath = ReturnMaxProperties()

    if currentMaxRevision == None and maxPath == None:
        return

    # get latest max p4 revision
    pathFileState = Perforce.GetFileState(maxPath)
    if isinstance(pathFileState, list):
        return

    headRevision = pathFileState.HeadRevision

    if int(currentMaxRevision) == int(headRevision):
        return
    else:
        result = QtGui.QMessageBox.warning(
                        None,
                        "Warning",
                        "You are using revision #{0} of this asset's max file.\n\nThere is a newer version in perforce #{1}.\n\nHit Cancel to stop the setup process of this asset,\nor OK to continue.".format(int(currentMaxRevision), int(headRevision)),
                        QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel
                                )
        if result != QtGui.QMessageBox.Cancel:
            return
        return 'Cancel'


def OHPelvisSetup():
    if not ProjectData.data.GetConfig("CharSetupPelvisSetup"):
        # project does not need this setup
        return
    # oh_pelvis setup
    ohPelvis = RS.Utils.Scene.FindModelByName('OH_Pelvis')
    pelvis = RS.Utils.Scene.FindModelByName('SKEL_Pelvis')
    mover = RS.Utils.Scene.FindModelByName('mover')
    dummy = RS.Utils.Scene.FindModelByName('Dummy01')
    skelRoot = RS.Utils.Scene.FindModelByName('SKEL_ROOT')
    if not ohPelvis:
        ohPelvis = FBModelNull('OH_Pelvis')
        skelRootTrns = FBVector3d()
        skelRootRot = FBVector3d()
        skelRoot.GetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
        skelRoot.GetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
        ohPelvis.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
        ohPelvis.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
        ohPelvis.Parent = skelRoot
        ohPelvis.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling)

    if not ohPelvis and pelvis and mover and dummy and skelRoot:
        return

    # create nulls for hierarchy
    ohPelvisNull = FBModelNull("OH_Pelvis_Control")
    moverTracerNull = FBModelNull("Mover_Tracer")
    offsetParentNull =FBModelNull("Offset_Parent_Node")
    offsetNull = FBModelNull("Offset_Node")
    rootTracerNull = FBModelNull("Root_Tracer")
    zeroNull = FBModelNull("Zero_Node")

    # parent and position new nulls
    pelvisTrns = FBVector3d()
    pelvisRot = FBVector3d()
    pelvis.GetVector(pelvisTrns, FBModelTransformationType.kModelTranslation, True)
    pelvis.GetVector(pelvisRot, FBModelTransformationType.kModelRotation, True)
    skelRootTrns = FBVector3d()
    skelRootRot = FBVector3d()
    skelRoot.GetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    skelRoot.GetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    moverTrns = FBVector3d()
    moverRot = FBVector3d()
    mover.GetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    mover.GetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    ohPelvisNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    ohPelvisNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    ohPelvisNull.Parent = dummy
    moverTracerNull.SetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    moverTracerNull.SetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    moverTracerNull.Parent = ohPelvisNull
    offsetParentNull.SetVector(moverTrns, FBModelTransformationType.kModelTranslation, True)
    offsetParentNull.SetVector(moverRot, FBModelTransformationType.kModelRotation, True)
    offsetParentNull.Parent = moverTracerNull
    offsetNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    offsetNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    offsetNull.Parent = offsetParentNull
    rootTracerNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    rootTracerNull.SetVector(skelRootRot, FBModelTransformationType.kModelRotation, True)
    rootTracerNull.Parent = ohPelvisNull
    zeroNull.SetVector(skelRootTrns, FBModelTransformationType.kModelTranslation, True)
    zeroNull.SetVector(FBVector3d(-90,0,0), FBModelTransformationType.kModelRotation, True)
    zeroNull.Parent = rootTracerNull

    # create constraints
    moverTracerConstraint = FBConstraintManager().TypeCreateConstraint(3)
    moverTracerConstraint.Name = 'moverTracer_parentChild'
    moverTracerConstraint.ReferenceAdd (0, moverTracerNull)
    moverTracerConstraint.ReferenceAdd (1, mover)
    moverTracerConstraint.Snap()

    pelvisOffsetConstraint = FBConstraintManager().TypeCreateConstraint(3)
    pelvisOffsetConstraint.Name = 'pelvisOffset_parentChild'
    pelvisOffsetConstraint.ReferenceAdd (0, offsetNull)
    pelvisOffsetConstraint.ReferenceAdd (1, zeroNull)
    trnsOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset T')
    rotOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset R')
    scaleOffsetProperty = pelvisOffsetConstraint.PropertyList.Find('Zero_Node.Offset S')
    if trnsOffsetProperty and rotOffsetProperty and scaleOffsetProperty:
        trnsOffsetProperty.Data = FBVector3d(0,0,0)
        rotOffsetProperty.Data = FBVector3d(0,0,0)
        scaleOffsetProperty.Data = FBVector3d(1,1,1)
    pelvisOffsetConstraint.Snap()
    pelvisOffsetConstraint.Lock = True

    rootTracerConstraint = FBConstraintManager().TypeCreateConstraint(3)
    rootTracerConstraint.Name = 'rootTracer_parentChild'
    rootTracerConstraint.ReferenceAdd (0, rootTracerNull)
    rootTracerConstraint.ReferenceAdd (1, skelRoot)
    rootTracerConstraint.Snap()

    pelvisExportConstraint = FBConstraintRelation('OH_Pelvis_Export')
    pelvisExportSender = pelvisExportConstraint.SetAsSource(offsetNull)
    pelvisExportConstraint.SetBoxPosition(pelvisExportSender, 0, 0)
    pelvisExportSender.UseGlobalTransforms = False
    pelvisExportReceiver = pelvisExportConstraint.ConstrainObject(ohPelvis)
    pelvisExportConstraint.SetBoxPosition(pelvisExportReceiver, 300, 0)
    pelvisExportReceiver.UseGlobalTransforms = False
    RS.Utils.Scene.ConnectBox(pelvisExportSender, 'Lcl Translation', pelvisExportReceiver, 'Lcl Translation')
    RS.Utils.Scene.ConnectBox(pelvisExportSender, 'Lcl Rotation', pelvisExportReceiver, 'Lcl Rotation')
    pelvisExportConstraint.Active = False

    pelvisLockConstraint = FBConstraintRelation('OH_Pelvis_Lock')
    pelvisLockSender = pelvisLockConstraint.SetAsSource(skelRoot)
    pelvisLockConstraint. SetBoxPosition(pelvisLockSender, 0, 0)
    pelvisLockReceiver = pelvisLockConstraint.ConstrainObject(ohPelvis)
    pelvisLockConstraint.SetBoxPosition(pelvisLockReceiver, 300, 0)
    RS.Utils.Scene.ConnectBox(pelvisLockSender, 'Translation', pelvisLockReceiver, 'Translation')
    RS.Utils.Scene.ConnectBox(pelvisLockSender, 'Rotation', pelvisLockReceiver, 'Rotation')
    pelvisLockConstraint.Active = True

    ohPelvis.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    ohPelvisNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    moverTracerNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    offsetParentNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    offsetNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    rootTracerNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)
    zeroNull.SetVector(FBVector3d(1, 1, 1), FBModelTransformationType.kModelScaling, False)

    ohPelvis.Translation.GetAnimationNode().Nodes[0].FCurve.EditClear()
    ohPelvis.Translation.GetAnimationNode().Nodes[1].FCurve.EditClear()
    ohPelvis.Translation.GetAnimationNode().Nodes[2].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[0].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[1].FCurve.EditClear()
    ohPelvis.Rotation.GetAnimationNode().Nodes[2].FCurve.EditClear()

    # add OH Constraints into a folder
    ohConstraintFolder = None
    constraintFolder = None
    for folder in RS.Globals.Folders:
        if folder.Name == 'OH_Constraints':
            ohConstraintFolder = folder
        if folder.Name == 'Constraints 1':
            constraintFolder = folder
    if ohConstraintFolder:
        folder.Items.append(moverTracerConstraint)
        folder.Items.append(pelvisOffsetConstraint)
        folder.Items.append(rootTracerConstraint)
        folder.Items.append(pelvisExportConstraint)
        folder.Items.append(pelvisLockConstraint)
        if constraintFolder:
            try:
                constraintFolder.Items.append(ohConstraintFolder)
            except:
                pass
    else:
        placeholder = FBCharacterExtension('Remove_Me')
        ohConstraintFolder = FBFolder("OH_Constraints", placeholder)
        ohConstraintFolder.Items.append(moverTracerConstraint)
        ohConstraintFolder.Items.append(pelvisOffsetConstraint)
        ohConstraintFolder.Items.append(rootTracerConstraint)
        ohConstraintFolder.Items.append(pelvisExportConstraint)
        ohConstraintFolder.Items.append(pelvisLockConstraint)
        placeholder.FBDelete()
        if constraintFolder:
            try:
                constraintFolder.Items.append(ohConstraintFolder)
            except:
                pass


def rs_AssetSetupFacialGroups():
    # Accessing the scene components to view all the groups
    lScene = FBSystem().Scene
    lAllGroups = lScene.Groups
    for iGroup in lAllGroups:

        # Do the 3Lateral
        if iGroup.Name == "3Lateral":
            lateralGUI = RS.Utils.Scene.FindModelByName("faceControls_OFF")
            if lateralGUI:

                # Gather Objects
                lCtrls_Brows = FBModelList()
                lCtrls_Eyes = FBModelList()
                lCtrls_Mouth = FBModelList()
                lFrames = FBModelList()
                for item in glo.gBrowCtrls :
                    lName = RS.Utils.Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Brows.append( lName )
                for item in glo.gEyeCtrls :
                    lName = RS.Utils.Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Eyes.append( lName )
                for item in glo.gMouthCtrls :
                    lName = RS.Utils.Scene.FindModelByName( item )
                    if lName :
                        lCtrls_Mouth.append( lName )
                for item in glo.gCAFaceAttrGui :
                    lName = RS.Utils.Scene.FindModelByName( item )
                    if lName :
                        lFrames.append( lName )

                # Gather Names
                name3L_ctrls = "3L_Controls"
                name3L_frames = "3L_Frames"
                name3L_brows = "Brows"
                name3L_eyes = "Eyes"
                name3L_mouth = "Mouth"

                # Remove Groups and their members
                for group in lAllGroups:
                    if group.LongName == name3L_ctrls :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in lAllGroups:
                    if group.LongName == name3L_frames :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in lAllGroups:
                    if group.LongName == name3L_brows :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in lAllGroups:
                    if group.LongName == name3L_eyes :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in lAllGroups:
                    if group.LongName == name3L_mouth :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                while len(iGroup.Items) != 0 :
                    iGroup.Items.pop()

                # Create Groups
                lGrp_ctrls = FBGroup (name3L_ctrls)
                lGrp_frames = FBGroup (name3L_frames)
                lGrp_brows = FBGroup (name3L_brows)
                lGrp_eyes = FBGroup (name3L_eyes)
                lGrp_mouth = FBGroup (name3L_mouth)

                # Parent Groups
                lGrp_ctrls.Items.append(lGrp_brows)
                lGrp_ctrls.Items.append(lGrp_eyes)
                lGrp_ctrls.Items.append(lGrp_mouth)
                iGroup.Items.append(lGrp_ctrls)
                iGroup.Items.append(lGrp_frames)
                for each in lCtrls_Brows :
                    lGrp_brows.ConnectSrc(each)
                for each in lCtrls_Eyes :
                    lGrp_eyes.ConnectSrc(each)
                for each in lCtrls_Mouth :
                    lGrp_mouth.ConnectSrc(each)
                for each in lFrames :
                    lGrp_frames.ConnectSrc(each)
                lGrp_ctrls.Show = False
                lGrp_ctrls.Pickable = True
                lGrp_ctrls.Transformable = True
                lGrp_frames.Show = False
                lGrp_frames.Pickable = False
                lGrp_frames.Transformable = True
                glo.gScene.Evaluate()

        # Do the Ambient
        if iGroup.Name == "Ambient_Facial":

            ambientGUI = RS.Utils.Scene.FindModelByName("Ambient_UI")
            faceFXGUI = RS.Utils.Scene.FindModelByName("FaceFX")
            if ambientGUI:

                # Object Containers
                amb_Ctrls = []
                amb_Frames = []

                # Recursively gather Ambient Information
                def getAmbientInfo(currentObj) :
                    if "CTRL" in currentObj.Name :
                        amb_Ctrls.append(currentObj)
                    elif "TEXT" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "RECT" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "Ambient_UI" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    elif "FaceFX" in currentObj.Name :
                        amb_Frames.append(currentObj)
                    else :
                        amb_Ctrls.append(currentObj)

                    # Recurse
                    for child in currentObj.Children:
                        getAmbientInfo(child)
                getAmbientInfo(ambientGUI)
                if faceFXGUI :
                    getAmbientInfo(faceFXGUI)

                # Gather Names
                nameAmb_ctrls = "Amb_Controls"
                nameAmb_frames = "Amb_Frames"

                # Remove Groups
                for group in lAllGroups:
                    if group.LongName == nameAmb_ctrls :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                for group in lAllGroups:
                    if group.LongName == nameAmb_frames :
                        while len(group.Items) != 0 :
                            group.Items.pop()
                        group.FBDelete()
                while len(iGroup.Items) != 0 :
                    iGroup.Items.pop()

                # Create Groups
                lGrp_ctrls = FBGroup (nameAmb_ctrls)
                lGrp_frames = FBGroup (nameAmb_frames)

                # Parent Groups
                iGroup.Items.append(lGrp_ctrls)
                iGroup.Items.append(lGrp_frames)
                for each in amb_Ctrls :
                    lGrp_ctrls.ConnectSrc(each)
                for each in amb_Frames :
                    lGrp_frames.ConnectSrc(each)
                lGrp_ctrls.Show = False
                lGrp_ctrls.Pickable = True
                lGrp_ctrls.Transformable = True
                lGrp_frames.Show = False
                lGrp_frames.Pickable = False
                lGrp_frames.Transformable = True
                glo.gScene.Evaluate()

    # Reset group selection and transformable settings, seems buggy.
    for etc in glo.Groups:
        if etc.Name == "Brows":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "Eyes":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "Mouth":
            etc.Pickable = False
            etc.Transformable = False
            etc.Pickable = True
            etc.Transformable = True
        if etc.Name == "3L_Frames":
            etc.Pickable = False
            etc.Transformable = True


def MoverCameraCreate(lMover):
    # onyl for ingame setup
    lMoverCam = FBCamera("MoverCamera")
    lMoverCamInterest = FBModelNull("MoverCamera_Interest")
    lMoverCam.Interest = lMoverCamInterest
    lMoverCam.Show = True
    lMoverCamInterest.Show = True
    lMoverCamConstraint = FBConstraintManager().TypeCreateConstraint(3)
    lMoverCamConstraint.Name = "CameraMover_Constraint"
    lMoverCamConstraint.ReferenceAdd (0,lMoverCam)
    lMoverCamConstraint.ReferenceAdd (1,lMover)
    lMoverCamConstraint.Snap()
    lMoverCamInterestConstraint = FBConstraintManager().TypeCreateConstraint(3)
    lMoverCamInterestConstraint.Name = "CameraMoverInterest_Constraint"
    lMoverCamInterestConstraint.ReferenceAdd (0,lMoverCamInterest)
    lMoverCamInterestConstraint.ReferenceAdd (1,lMover)
    lMoverCamInterestConstraint.Snap()
    lMoverCam.Translation = FBVector3d(-319.68,100,0)
    lVector = FBVector3d()
    lMover.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    lMoverCamInterest.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
    lTag = lMoverCamInterest.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Cameras"
    lCameraGroup = FBGroup("Camera")
    lCameraGroup.ConnectSrc(lMoverCam)
    lCameraGroup.Show = False
    lCameraGroup.Pickable = False
    lCameraGroup.Transformable = False
    FBSystem().Scene.Evaluate()
    return lCameraGroup


def ShadowPlaneCreate(lGeo):
    # only for ingame setup
    lShadowList = []
    lShadowLight = FBLight("ShadowLight")
    lShadowLight.Show = True
    lShadowLight.LightType = FBLightType.kFBLightTypeInfinite
    lShadowLight.Intensity = 15.38
    lShadowLight.Translation = FBVector3d(46.04,268.12,19.48)
    lShadowLight.Rotation = FBVector3d(29.32, 47.13, 2.46)
    lShadowLight.Scaling = FBVector3d(0,0,0)
    lAdditionalLight = FBLight("AdditionalLight1")
    lAdditionalLight.Show = True
    lAdditionalLight.LightType = FBLightType.kFBLightTypeInfinite
    lAdditionalLight.Intensity = 116.92
    lAdditionalLight.Translation = FBVector3d(-3.02, 205.57,-131.06)
    lAdditionalLight.Rotation = FBVector3d(-115.49, 64.44, 105.69)
    lAdditionalLight.Scaling = FBVector3d(0,0,0)
    lAdditionalLight2 = FBLight("AdditionalLight2")
    lAdditionalLight2.Show = True
    lAdditionalLight2.LightType = FBLightType.kFBLightTypeInfinite
    lAdditionalLight2.Intensity = 52.31
    lAdditionalLight2.Translation = FBVector3d(70.11, 218.59, -99.49)
    lAdditionalLight2.Rotation = FBVector3d(-23.38, 38, 112.86)
    lAdditionalLight2.Scaling = FBVector3d(0,0,0)
    lAdditionalLight3 = FBLight("AdditionalLight3")
    lAdditionalLight3.Show = True
    lAdditionalLight3.LightType = FBLightType.kFBLightTypeInfinite
    lAdditionalLight3.Intensity = 89.23
    lAdditionalLight3.Translation = FBVector3d(-7.42, 242.38, -89.97)
    lAdditionalLight3.Rotation = FBVector3d(44.88, 40.25, -92.65)
    lAdditionalLight3.Scaling = FBVector3d(0,0,0)

    lShadowShader = FBShaderManager().CreateShader("ShadowLiveShader")
    lShadowShader.Name = "Live Shadow"
    lShadowShader.ShadowType = FBShadowType.kFBShadowTypeShadowPlanar
    lShadowShader.PropertyList.Find("Shadow Intensity").Data = 80
    lShadowShader.Lights.append(lShadowLight)

    RS.Utils.Scene.GetChildren(lGeo, lShadowList)

    for iShadowItem in lShadowList:
        lShadowShader.Models.append(iShadowItem)

    lShadowPlane = FBModelPlane("ShadowPlane")
    lShadowPlane.Show = True
    lShadowPlane.Scaling = FBVector3d(2.85,2.85,2.85)
    lShadowPlane.Translation = FBVector3d(0,0,0)
    lPlaneTexture = FBTexture("ShadowPlaneTexture")
    lPlaneTexture.ConnectDst(lShadowPlane)
    lShadowPlane.Shaders.append(lShadowShader)
    lShadowPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    # custom properties
    lTag = lShadowPlane .PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Contacts"

    # groups
    lMainShadowGroup = FBGroup("Shadow")
    lLightGroup = FBGroup ("Lights")
    lPlaneGroup = FBGroup ("Plane")
    lLightGroup.ConnectSrc(lShadowLight)
    lLightGroup.ConnectSrc(lAdditionalLight)
    lLightGroup.ConnectSrc(lAdditionalLight2)
    lLightGroup.ConnectSrc(lAdditionalLight3)
    lPlaneGroup.ConnectSrc(lShadowPlane)
    lMainShadowGroup.ConnectSrc(lLightGroup)
    lMainShadowGroup.ConnectSrc(lPlaneGroup)
    lMainShadowGroup.Pickable = False
    lMainShadowGroup.Transformable = False
    return lMainShadowGroup


def CharacterPropertySetup(lCharacter):
    lCharacter.PropertyList.Find('LeftLegRollMode').Data = True
    lCharacter.PropertyList.Find('LeftLegRoll').Data = 100
    lCharacter.PropertyList.Find('RightLegRollMode').Data = True
    lCharacter.PropertyList.Find('RightLegRoll').Data = 100
    lCharacter.PropertyList.Find('LeftForeArmRollMode').Data = True
    lCharacter.PropertyList.Find('LeftForeArmRoll').Data = 100
    lCharacter.PropertyList.Find('RightForeArmRollMode').Data = True
    lCharacter.PropertyList.Find('RightForeArmRoll').Data = 100
    lCharacter.PropertyList.Find("Feet Floor Contact").Data = False
    lCharacter.PropertyList.Find("Toes Floor Contact").Data = False
    lCharacter.PropertyList.Find("Hands Floor Contact").Data = False
    lCharacter.PropertyList.Find("Fingers Floor Contact").Data = False
    lCharacter.PropertyList.Find("Automatic Finger Base").Data = True
    lCharacter.PropertyList.Find("Hands Contact Type").Data = 2
    lCharacter.PropertyList.Find("Hips Level Mode").Data = False
    lCharacter.PropertyList.Find("Feet Spacing Mode").Data = False
    lCharacter.PropertyList.Find("Ankle Height Compensation Mode").Data = False
    lCharacter.PropertyList.Find("Match Source").Data = True
    lCharacter.PropertyList.Find("Left Hand Thumb Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Index Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Middle Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Ring Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Pinky Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Extra Finger Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Thumb Tip").Data =  0.10
    lCharacter.PropertyList.Find("Right Hand Index Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Middle Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Ring Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Pinky Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Extra Finger Tip").Data = 0.10
    lCharacter.PropertyList.Find("Automatic Finger Base").Data = True
    lCharacter.PropertyList.Find("Hands Contact Type").Data = 2
    FBSystem().Scene.Evaluate()


def IGAndCSFinalSetups(lCharacter):
    '''
    Start Timeline at Zero
    Setup Facial Groups
    Rotation Order for Upper Arms
    Setup Finger Limits
    Plot Animation to Skeleton
    Master Null Creation
    Snap and Play on Frames
    Scale Down Nulls
    Set Hair Scale to 1
    Unparent Face Controls
    Set Default Material Properties
    Character Property Setup
    Setup Facial Groups
    '''
    # start timeline at 0
    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    # setup facial groups
    rs_AssetSetupFacialGroups()

    # setup facial groups
    if ProjectData.data.GetConfig("CharSetupUFCSetup"):
        ufcsetup.setupUFCFacialGroups()

    # Change the rotation order of the Upper Arms
    if ProjectData.data.GetConfig("CharSetupArmRotation"):
        lBoneNames = ('SKEL_R_UpperArm', 'SKEL_L_UpperArm')
        lBoneObjects = []
        for lBoneName in lBoneNames:
            lObj = FBFindModelByLabelName(lBoneName)
            if lObj: lBoneObjects.append( lObj )
        for lBone in lBoneObjects:
            lBone.PropertyList.Find( 'RotationActive' ).Data = True
            lBone.PropertyList.Find( 'RotationOrder' ).Data = 3
        FBSystem().Scene.Evaluate()

    # setup finger limits
    rs_SetHingeDOF()

    # plot animation to the skeleton
    if ProjectData.data.GetConfig("CharSetupPlot"):
        plotWhere = 'skeleton'
        if plotWhere == 'skeleton':
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
        elif plotWhere == 'rig':
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnControlRig
        else:
            plotWhere = FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
        plotOptions = FBPlotOptions()
        plotOptions.PlotAllTakes = False
        plotOptions.PlotOnFrame = True
        plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
        plotOptions.UseConstantKeyReducer = False
        plotOptions.ConstantKeyReducerKeepOneKey = False
        plotOptions.PlotTranslationOnRootOnly = True
        for char in FBSystem().Scene.Characters:
            char.PlotAnimation(plotWhere, plotOptions)

    # create a master null and parent everything under it
    if ProjectData.data.GetConfig("CharSetupMasterDummy"):
        lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        lSceneParentNull = cre.rs_CreateNullElement(lFileName,0,0,0)
        lSceneRootNodes = FBComponentList()
        for model in FBSystem().Scene.RootModel.Children:
            lSceneRootNodes.append (model)
        for model in lSceneRootNodes:
            if model.Name != lSceneParentNull.Name:
                model.Parent = lSceneParentNull
        FBSystem().Scene.Evaluate()

    # snap and play on frames
    FBPlayerControl().SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames

    # scale down nulls
    ass.ScaleDownNulls()

    # set hair scale to 1
    lHairScale = RS.Utils.Scene.FindModelByName("HairScale")
    if lHairScale:
        lHairScale.Translation.Data = FBVector3d(0, -1, 0)

    # adjust default materials
    for iMat in glo.gMaterials:
        if iMat:
            if "_Contact" in iMat.Name:
                None
            else:
                iMat.Ambient = FBColor(1,1,1)

    # sets up character properties
    CharacterPropertySetup(lCharacter)

    # create gesture safe area
    if ProjectData.data.GetConfig("CharSetupGestureSafeArea"):
        gestureSafe = FBCreateObject( "Browsing/Templates/Elements/Primitives", "Cylinder", "Cylinder" )
        gestureSafe.Name =("GestureSafeArea")
        gestureSafe.Show = True
        gestureSafe.Translation = FBVector3d(1.87,100,91.19)
        gestureSafe.Rotation = FBVector3d(0,0,180)
        gestureSafe.Scaling = FBVector3d(16,10,16)
        gestureSafe.Selected = True
        gestureSafe.ShadingMode = FBModelShadingMode.kFBModelShadingWire
        moverModel = RS.Utils.Scene.FindModelByName( "mover" )
        if moverModel:
            gestureSafe.Parent = moverModel

    # refresh
    FBSystem().Scene.Evaluate()


def rs_AssetSetupCSCharacters(pControl, pEvent):
    # check max revision
    result = CheckMaxRevision()
    if result != None:
        return

    # tidy face nodes
    if ProjectData.data.GetConfig("CharSetupGTAFaceFix"):
        faceControls = RS.Utils.Scene.FindModelByName("faceControls_OFF")
        faceRoot = RS.Utils.Scene.FindModelByName("facialRoot_C_OFF")
        if faceControls and faceRoot:
            faceRoot.Parent = faceControls
            FBSystem().Scene.Evaluate()

    # strip layers
    while FBSystem().CurrentTake.GetLayerCount() > 1:
        FBSystem().CurrentTake.GetLayer(1).FBDelete()

    # prep character into t-pose and characterise
    rs_PrepSetupCharacters(True)

    # get variables
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]
    lSkelRoot = lMover.Children[0]

    # character
    lCharacter = glo.gCharacters[0]

    # toybox setup
    lMoverConstraint = rs_AssetSetupCharToyboxMoverControl()
    lMoverConstraint.Active = True

    # set mover properties - DOFs
    lMover.PropertyList.Find("Enable Rotation DOF").Data = True
    lMover.PropertyList.Find("RotationMinX").Data = True
    lMover.PropertyList.Find("RotationMinY").Data = True
    lMover.PropertyList.Find("RotationMaxX").Data = True
    lMover.PropertyList.Find("RotationMaxY").Data = True

    # set pelvis properties
    lPelvisSkel = RS.Utils.Scene.FindModelByName('SKEL_Pelvis')
    lPelvisSkel.PropertyList.Find("Enable Selection").Data = False
    lPelvisSkel.PropertyList.Find("Enable Transformation").Data = False

    # rotate mover marker
    lCharacter.InputType.kFBCharacterInputStance
    lCharacter.ActiveInput = True
    lMoverMarker = RS.Utils.Scene.FindModelByName("mover_Control")
    lMoverMarker.PropertyList.Find("Lcl Rotation").Data = FBVector3d(90, 0, 0)
    FBSystem().Scene.Evaluate()
    lCharacter.ActiveInput = False
    lCharacter.ActiveInput = True

    # set up custom proeprties for ref edtior - not sure if needed for new ref editor?
    ass.rs_AssetSetupCustomProperties()

    # fix shoulders
    rs_FixShoulders()
    FBSystem().Scene.Evaluate()

    # parent the high heels to the hierarchy and set it to 0
    lHeelRoot = None
    lHeelRoot = RS.Utils.Scene.FindModelByName("RECT_HeelHeight")
    if lHeelRoot != None:
        lHeelRoot.Parent = lMover
        lHeelHeight = None
        lHeelHeight = RS.Utils.Scene.FindModelByName("HeelHeight")
        if lHeelHeight != None:
            lHeelHeight.PropertyList.Find("Lcl Translation").SetAnimated(True)
            lHeelHeight.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
    FBSystem().Scene.Evaluate()

    # set up character extensions
    rs_CharacterExtension(lCharacter)
    FBSystem().Scene.Evaluate()

    # final setups used in both cs and ig
    IGAndCSFinalSetups(lCharacter)

    # tidy scene - groups, folders etc
    rs_CharacterSetupTidy()


def rs_AssetSetupIGCharacters(pControl, pEvent):
    # get max revision info
    result = CheckMaxRevision()
    if result != None:
        return

    # tidy face nodes
    if ProjectData.data.GetConfig("CharSetupGTAFaceFix"):
        faceControls = RS.Utils.Scene.FindModelByName("faceControls_OFF")
        faceRoot = RS.Utils.Scene.FindModelByName("facialRoot_C_OFF")
        head = RS.Utils.Scene.FindModelByName("SKEL_Head")
        if faceControls and head and faceRoot:
            faceControls.Parent = head
            faceRoot.Parent = faceControls
            FBSystem().Scene.Evaluate()

    # prep character into t-pose and characterise
    rs_PrepSetupCharacters(False)

    # heels setup
    heel.rs_HeelsSetup()

    # get variables
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]
    lSkelRoot = lMover.Children[0]

    # character
    lCharacter = glo.gCharacters[0]

    # setup toybox
    lMoverConstraint = rs_AssetSetupCharToyboxMoverControl()
    lMoverConstraint.Active = False
    lMover.Show = True

    # character set to stance pose
    lCharacter.InputType.kFBCharacterInputStance
    lCharacter.ActiveInput = True

    # some head selection/transform properties
    head = RS.Utils.Scene.FindModelByName('SKEL_Head')
    if head:
        head.PropertyList.Find("Enable Selection").Data = False
        head.PropertyList.Find("Enable Transformation").Data = False

    # create movercam
    lCameraGroup = MoverCameraCreate(lMover)

    # create lighting rig and shadow setup - return the group
    lMainShadowGroup = ShadowPlaneCreate(lGeo)

    # tidy scene - groups, folders etc
    rs_CharacterSetupTidy()

    # add safebone and expressions to main groups
    lCharacterGroup = None
    lSafeBoneGroup = None
    lExpressionsGroup = None
    for iGroup in glo.gGroups:
        if iGroup.Name.lower() == lFileName.lower():
            lCharacterGroup = iGroup
        if iGroup.Name.lower() == 'safebones':
            lSafeBoneGroup = iGroup
        if iGroup.Name.lower() == 'expressions':
            lExpressionsGroup = iGroup
    if lCharacterGroup:
        if lSafeBoneGroup:
            lCharacterGroup.ConnectSrc(lSafeBoneGroup)
        if lExpressionsGroup:
            lCharacterGroup.ConnectSrc(lExpressionsGroup)

    # additional group tidy
    for iGroup in glo.gGroups:
        if iGroup.Name == lFileName:
            lMainGroup = iGroup
        if iGroup.Name == 'Contacts':
            lContactGroup = iGroup
        if iGroup.Name == 'mover':
            lMoverGroup = iGroup
        if iGroup.Name == 'Joints':
            lJointsGroup = iGroup
    if lMainShadowGroup:
        lMainGroup.ConnectSrc(lMainShadowGroup)
    if lCameraGroup:
        lMainGroup.ConnectSrc(lCameraGroup)
    lMoverGroup.Show = False
    lMoverGroup.Pickable = True
    lMoverGroup.Transformable = True
    lContactGroup.Show = False
    lJointsGroup.Show = True

    # sets up IK bones
    boneStringList = ProjectData.data.CharacterIKPHBoneList()

    for boneString in boneStringList:
        iPHIK = RS.Utils.Scene.FindModelByName(boneString)
        if iPHIK:
            lookProperty = iPHIK.PropertyList.Find('Look')
            if lookProperty:
                lookProperty.Data = 1
            sizeProperty = iPHIK.PropertyList.Find('Size')
            if sizeProperty:
                sizeProperty.Data = 3.0
            if boneString in ['IK_L_Hand', 'IK_R_Hand', 'PH_L_Hand', 'PH_R_Hand', 'mover']:
                iPHIK.PropertyList.Find("Enable Transformation").Data = True

    # rotate mover marker on 'ig_' chars - not sure why
    lMoverMarker = RS.Utils.Scene.FindModelByName("mover_Control")
    if lFileName.startswith("IG_"):
        lMoverMarker.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0, 0, 0)
        FBSystem().Scene.Evaluate()
    lMoverMarker.PropertyList.Find("Enable Transformation").Data = True

    # rename geo parent
    lGeo.Name = "Geometry"

    # fix shoulders
    rs_FixShoulders()
    FBSystem().Scene.Evaluate()

    # facing direction and lookat cones setup (upperfixup, independant mover & facing direction)
    ikcon.rs_FacingDirectionArrow()
    ikcon.rs_HeadLookAt()
    rs_AssetSetupFacingDirConstraint()
    FBSystem().Scene.Evaluate()

    # generate character extensions
    characterExtensionList = rs_CharacterExtension(lCharacter)
    for characterExtension in characterExtensionList:
        characterExtension.PlotAllowed = FBPlotAllowed.kFBPlotAllowed_Skeleton

    # get status of heel & IK setups - reassures the resource team these setups are complete
    # - they were essential to be set up correctly for gtaV
    heelBool =  heel.heelsCheckList
    faceDirectionBool = ikcon.faceDirectionCheckList
    lookAtBool = ikcon.lookAtCheckList

    # OH Pelvis setup
    OHPelvisSetup()

    # setup expression solver - implement when approved (see Kat). In for debug only.
    #Character.SetupExpressionSolver(lCharacter)

    # final setups used in both cs and ig
    IGAndCSFinalSetups(lCharacter)


'''
SHOULDNT NEED THE BELOW EXPRESSION SETUPS AFTER NEW EXPRESSION SYSTEM IS IN PLACE url:bugstar:2449964
'''
def onFacePositionFix():
    # facial hookups
    themBones = [
        "FACIAL_L_lipCornerSDK",
        "FACIAL_L_lipUpperSDK",
        "FACIAL_lipUpperSDK",
        "FACIAL_R_lipUpperSDK",
        "FACIAL_R_lipCornerSDK",
        "FACIAL_L_lipLowerSDK",
        "FACIAL_lipLowerSDK",
        "FACIAL_R_lipLowerSDK",
        "FACIAL_L_eyelidUpperOuterSDK",
        "FACIAL_L_eyelidUpperInnerSDK",
        "FACIAL_L_eyelidLowerOuterSDK",
        "FACIAL_L_eyelidLowerInnerSDK",
        "FACIAL_R_eyelidUpperOuterSDK",
        "FACIAL_R_eyelidUpperInnerSDK",
        "FACIAL_R_eyelidLowerOuterSDK",
        "FACIAL_R_eyelidLowerInnerSDK",
        "FACIAL_jaw" ]

    themControllers = [
        "lipCorner_L_OFF",
        "upperLip_L_OFF",
        "upperLip_C_OFF",
        "upperLip_R_OFF",
        "lipCorner_R_OFF",
        "lowerLip_L_OFF",
        "lowerLip_C_OFF",
        "lowerLip_R_OFF",
        "eyelidUpperOuter_L_OFF",
        "eyelidUpperInner_L_OFF",
        "eyelidLowerOuter_L_OFF",
        "eyelidLowerInner_L_OFF",
        "eyelidUpperOuter_R_OFF",
        "eyelidUpperInner_R_OFF",
        "eyelidLowerOuter_R_OFF",
        "eyelidLowerInner_R_OFF",
        "jaw_C_OFF" ]

    i = 0
    while i != len( themBones ):
        theBone = FBFindModelByLabelName( themBones[i] )
        theController = FBFindModelByLabelName( themControllers[i] )

        if ( theBone != None ) and ( theController != None ):
            glo.gScene.Evaluate()
            translationValue = theBone.Translation.Data
            theController.Translation.Data = translationValue
            glo.gScene.Evaluate()
        i+=1


def rs_CSFacialSetup( pControl, pEvent ):
    # Gather & Delete Facial Constraints and Camera Objects
    deleteMe = []
    searchItems = [ "3Lateral", "Viseme Relation" ]

    for constraint in glo.gScene.Constraints:
        for each in searchItems:
            if each in constraint.Name:
                deleteMe.append( constraint )

    deleteMe = list( set( deleteMe ) )
    for each in deleteMe:
        each.FBDelete()

    deleteObjects = FBComponentList()
    FBFindObjectsByName("3Lateral*Camera*", deleteObjects, True, False)
    for each in deleteObjects:
        each.FBDelete()

    # Odds and ends
    # onFacePositionFix()

    lRECT = None
    lRECT = RS.Utils.Scene.FindModelByName( "RECT_ScaleOffset" )
    if lRECT != None:
        lRECT.Parent = None

    glo.gScene.Evaluate()
    # Odds and ends - Done

    check = lat.rs_3LateralUISetUp()
    if check:
        onFacePositionFix()
        concs.rs_ConnectRigInABox()
        concs.rs_ConnectRigInABox1()

        conig.rs_SetUpAmbientUI()

        glo.gScene.Evaluate()
        lFileName = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName )


def rs_IGFacialSetup( pControl, pEvent ):
    conig.rs_FacialPreRotation()
    conig.rs_ConnectAmbientFaceRig()
    conig.rs_SetUpAmbientUI()

    glo.gScene.Evaluate()

    lRECT = None
    lRECT = RS.Utils.Scene.FindModelByName("RECT_ScaleOffset")
    if lRECT != None:
        lRECT.Parent = None

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(glo.App.FBXFileName)


def rs_getScaleFromPedsMeta( charName ):
    scaleValue = 1.0
    gProjRoot = RS.Config.Project.Path.Root
    pXmlFileName = gProjRoot + "\\assets\\export\\data\\peds.pso.meta"
    if(os.path.exists(pXmlFileName)):
        p4.Sync( pXmlFileName )
        charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
        lXmlFile = minidom.parse( pXmlFileName )
        rootData = lXmlFile.getElementsByTagName("InitDatas")
        charData =  rootData[0].getElementsByTagName("Name")
        scaleData = rootData[0].getElementsByTagName("CutsceneScale")
        for char in charData:
            if char.childNodes[0].data == charName:
                for scale in scaleData:
                    if char.parentNode == scale.parentNode:
                        scaleValue = float( scale.getAttribute("value") )
    return float( scaleValue )


def thighRollFix ( relConstrName, oldThighName, newThighName ):
# Custom Thigh Roll Fix after applying standard expressions.
    for relation in FBSystem().Scene.Constraints:
        if relation.Name == relConstrName:

            # Gather boxes
            oldThighBoxes = []
            inputPlug = None
            multBox = None
            for box in relation.Boxes:
                if "Add (V1 + V2)" in box.Name:
                    for plug in box.AnimationNodeInGet().Nodes:
                        if plug.Name == "V2":
                            inputPlug = plug
                if oldThighName in box.Name:
                    oldThighBoxes.append( box )
                if "Multiply (a x b)" in box.Name:
                    multBox = box

            # Create new connection box
            newThighBox_obj = FBFindModelByLabelName( newThighName )
            newThighBox = relation.SetAsSource( newThighBox_obj )
            newThighBox.UseGlobalTransforms = False
            relation.SetBoxPosition( newThighBox, 0, 500 )

            # Disconnect invalid connection
            for box in oldThighBoxes:
                for outputPlug in box.AnimationNodeOutGet().Nodes:
                    if outputPlug.Name == "Lcl Rotation":
                        if inputPlug != None:
                            FBDisconnect( outputPlug, inputPlug )

            # Connect new connection
            for outputPlug in newThighBox.AnimationNodeOutGet().Nodes:
                if outputPlug.Name == "Lcl Rotation":
                    if inputPlug != None:
                        FBConnect( outputPlug, inputPlug )

            # Update multiply box Value
            for plug in multBox.AnimationNodeInGet().Nodes:
                if plug.Name == "b":
                    plug.WriteData( [-.5], None )


def fixupExpressions( pControl, pEvent ):
    deactivateConstraints = ["MH_Hair_Scale-X_scale",
                            "MH_Hair_Scale-Y_scale",
                            "MH_Hair_Scale-Z_scale",
                            "MH_Hair_Crown-X_Position",
                            "MH_Hair_Crown-X_scale",
                            "MH_Hair_Crown-Y_scale",
                            "MH_Hair_Crown-Z_scale",]

    removeConstraints = ["RB_L_ThighRoll-Y_rotation",
                        "RB_L_ThighRoll-Z_rotation",
                        "RB_R_ThighRoll-Y_rotation",
                        "RB_R_ThighRoll-Z_rotation",
                        "RB_L_ForeArmRoll-Y_rotation",
                        "RB_L_ForeArmRoll-Z_rotation",
                        "RB_L_ArmRoll-Y_rotation",
                        "RB_L_ArmRoll-Z_rotation",
                        "RB_R_ForeArmRoll-Y_rotation",
                        "RB_R_ForeArmRoll-Z_rotation",
                        "RB_R_ArmRoll-Y_rotation",
                        "RB_R_ArmRoll-Z_rotation",
                        "SKEL_ROOT-X_Position",
                        "SKEL_ROOT-Y_Position",
                        "SKEL_ROOT-Z_Position"]

    deleteMe = FBComponentList()
    for constr in FBSystem().Scene.Constraints:
        if constr.Name in deactivateConstraints:
            constr.Active = False
        if constr.Name in removeConstraints:
            deleteMe.append(constr)
    for item in deleteMe:
        item.FBDelete()

    thighRollFix( "RB_L_ThighRoll-X_rotation", "RB_L_ThighRoll", "SKEL_L_Thigh" )
    thighRollFix( "RB_R_ThighRoll-X_rotation", "RB_R_ThighRoll", "SKEL_R_Thigh" )
