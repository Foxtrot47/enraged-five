###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_DeleteAsset
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Removes and Asset's hierarchy and all attached components
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Scene
import RS.Globals as glo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Clean up Materials & Save
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run():
    lAssetList = []
    lDeleteList = []
    
    lSelectedAsset = FBFindModelByLabelName("CS_Male_Proxy^5:Dummy01")
    lSelectedAssetSplit = lSelectedAsset.LongName.split(":")
    lSelectedAssetName = lSelectedAssetSplit[0]
    lNewAssetName = "DELETE_" + lSelectedAssetName
    
    RS.Utils.Scene.GetChildren(lSelectedAsset, lAssetList)
    #lAssetList.extend(eva.gChildList)
    #del eva.gChildList[:]
    
    for iComp in glo.gComponents:
        lCompSplit = iComp.LongName.split(":")
        lCompName = lCompSplit[0]
        if lCompName == lSelectedAssetName:
            if not iComp in lDeleteList:
                if iComp.ClassName() == "FBGroup" or iComp.ClassName() == "FBShader" or iComp.ClassName() == "FBVideoClip":
                    None
                else:
                    lDeleteList.append(iComp)
                
    for iDelete in lDeleteList:
        print iDelete.ClassName()
        if iDelete.ClassName() == "FBModel":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBActor":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBActorFace":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBAudioClip":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBCamera":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBCharacter":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBCharacterPose":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBCharacterFace":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBCharacterExtension":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBControlSet":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBHandle":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBFolder":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBConstraintRelation":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBConstraint":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBSet":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBLight":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBGroup":
            #iDelete.FBDelete()    
            None
        elif iDelete.ClassName() == "FBMaterial":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBNote":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBPose":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBShader":
            #iDelete.FBDelete()
            None
        elif iDelete.ClassName() == "FBShaderLighted":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBTake":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBTexture":
            iDelete.FBDelete()
        elif iDelete.ClassName() == "FBVideoClip":
            #iDelete.FBDelete()   
            None


