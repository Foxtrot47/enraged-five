###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Script Name: rs_AssetSetupVehicles
## Written And Maintained By: Kathryn Bodey
## Contributors: Kristine Middlemiss
## Description: function to set up vehicles
##
## Rules: Definitions: Prefixed with "rs_"
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

from pyfbsdk import *

import re
import os

import RS.Utils.Path
import RS.Utils.Scene
import RS.Utils.Collections
import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo
import RS.Config
import RS.Core.Metadata
import RS.Core.AssetSetup.Vehicle_Rowboat_CustomRig
import RS.Core.AssetSetup.VehicleWheelSetup

reload( RS.Core.AssetSetup.VehicleWheelSetup )
reload( RS.Core.AssetSetup.Vehicle_Rowboat_CustomRig )

import os

global lGeo
lGeo = None

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Scene Main Variables
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_VehicleAssetSetupVariables():

    global lGeo

    lAssetVariableList = []

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lDummy = None
    lExportable = None
    lMover = None

    for iScene in lSceneList:

        if 'dummy' in iScene.Name.lower():
            if 'chassis' in iScene.Name.lower():
                None
            else:
                lDummy = iScene
                lDummy.Name = 'Dummy01'

        elif '_skel' in iScene.Name.lower():
            lMover = iScene
            lMover.Name = 'mover'

        elif iScene.Name.lower() == 'mover':
            lMover = iScene

        elif iScene.Name.lower() == 'chassis':
            lExportable = iScene

        else:
            if lFileName.lower() + '_skin' in iScene.Name.lower():
                lGeo = iScene

    if lDummy:
        if lMover:
            if lExportable:
                if lGeo:

                    lAssetVariableList.append(lDummy)
                    lAssetVariableList.append(lMover)
                    lAssetVariableList.append(lExportable)
                    lAssetVariableList.append(lGeo)

                else:
                    FBMessageBox('Error', 'Scene is missing a Geometry Node, or it is spelled incorrectly. Script aborted.', 'Ok')
            else:
                FBMessageBox('Error', 'Scene is missing a mover Node, or it is spelled incorrectly. Script aborted.', 'Ok')
        else:
            FBMessageBox('Error', 'Scene is missing a chassis Node, or it is spelled incorrectly. Script aborted.', 'Ok')
    else:
        FBMessageBox('Error', 'Scene is missing a Dummy01 Node, or it is spelled incorrectly. Script aborted.', 'Ok')

    return lAssetVariableList

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Extra Nulls To Delete For Vehicles Only
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_DeleteItemsVehicleSetup():

    global lGeo

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lDeleteList = []

    lUnwantedList = ['_l1', '_l2', 'object', 'bound', 'rotationconstraint', 'lowlod', '_col', '_ng']

    for iSceneItem in lSceneList:
        for iUnwanted in lUnwantedList:
            if iUnwanted in iSceneItem.Name.lower() and iSceneItem != lGeo:
                lDeleteList.append(iSceneItem)

    lDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lDeleteList)

    for iDelete in lDeleteList:
        if iDelete:
            iDelete.FBDelete()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Group Setup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_CreateGroupsVehicleSetup():

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDummy = rs_VehicleAssetSetupVariables()[0]
    lMover = rs_VehicleAssetSetupVariables()[1]
    lExportable = rs_VehicleAssetSetupVariables()[2]
    lGeo = rs_VehicleAssetSetupVariables()[3]

    lControlGroup = FBGroup("Controls")
    lPlotGroup = FBGroup("Plot Group")
    lGeoGroup = FBGroup("Geo")

    lMainGroup = FBGroup(lFileName)
    lMainGroup.ConnectSrc(lControlGroup)
    lMainGroup.ConnectSrc(lPlotGroup)
    lMainGroup.ConnectSrc(lGeoGroup)

    lControlRoot = FBFindModelByLabelName(lExportable.Name + "_Control")
    lControlRoot.PropertyList.Find("Size").Data = 400

    lControlNullList = []
    lExportNullList = []

    RS.Utils.Scene.GetChildren(lControlRoot, lControlNullList)
    #lControlNullList.extend(eva.gChildList)
    #del eva.gChildList[:]

    RS.Utils.Scene.GetChildren(lMover, lExportNullList)
    #lExportNullList.extend(eva.gChildList)
    #del eva.gChildList[:]

    for iChild in lControlNullList:
        lControlGroup.ConnectSrc(iChild)

    for iChild in lExportNullList:
        lPlotGroup.ConnectSrc(iChild)

    lGeoGroup.ConnectSrc(lGeo)

    # scale down nulls
    ass.ScaleDownNulls()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Setup mover and toybox relation - for use in rs_CSCharacterSetup & rs_IGCharacterSetup
##              This was only added in Dec/12 so needs to be adjusted if it was used at the beginning of the production
##              Currently only BoxStar Tool calls this function, not anywhere else in this module - KM
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupVehicleToyboxMoverControl(lNameSpace, lDummy, lMover, lChassis):
    # Check for ToyBox Marker, however most files should have this at this point.
    lFound = False
    for lToyBoxMarker in lDummy.Children:
        if lToyBoxMarker.Name == "ToyBox Marker":
            lFound = True

    if lFound == False:

        lToyBoxMarker = FBCreateObject("Browsing/Templates/Elements", "Marker", "ToyBox Marker")
        lToyBoxMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
        lToyBoxMarker.Size = 500
        lToyBoxMarker.Color = FBColor(1, 0, 1)
        lToyBoxMarker.Parent = lDummy
        lToyBoxMarker.Translation.Data = lMover.Translation.Data
        lToyBoxMarker.Rotation.Data = lMover.Rotation.Data
        lTag = lToyBoxMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"

    # Creating the mover_toybox_Control Relation Constraint
    lRelationCon = FBConstraintRelation(lNameSpace+':mover_toybox_Control') # Why is this call slow..
    lRelationCon.Active = False

    # Add senders/receivers to the Relation Constraint
    lChassisMarkerSender = lRelationCon.SetAsSource(lChassis)
    lToyboxMarkerReceive = lRelationCon.ConstrainObject(lToyBoxMarker)
    lToyBoxMarkerSender = lRelationCon.SetAsSource(lToyBoxMarker)
    lMoverReceive = lRelationCon.ConstrainObject(lMover)

    # Position the boxes and set the properties
    lRelationCon.SetBoxPosition(lChassisMarkerSender, 0, 0)
    lRelationCon.SetBoxPosition(lToyboxMarkerReceive, 300, 0)
    lRelationCon.SetBoxPosition(lToyBoxMarkerSender, 0, 150)
    lRelationCon.SetBoxPosition(lMoverReceive, 350, 150)
    lToyBoxMarkerSender.UseGlobalTransforms = False
    lMoverReceive.UseGlobalTransforms = False

    # Make the property connections
    RS.Utils.Scene.ConnectBox(lChassisMarkerSender, 'Translation', lToyboxMarkerReceive, 'Translation')
    RS.Utils.Scene.ConnectBox(lChassisMarkerSender, 'Rotation', lToyboxMarkerReceive, 'Rotation')
    RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Translation', lMoverReceive, 'Lcl Translation')
    RS.Utils.Scene.ConnectBox(lToyBoxMarkerSender, 'Lcl Rotation', lMoverReceive, 'Lcl Rotation')

    return lRelationCon

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Clone Wheels Definition
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def VehicleSetupDuplicateWheel():

    lWheelBoneList = []
    lWheelMeshList = []

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lTargetWheelMesh = None
    for iScene in lSceneList:
        if 'wheelmesh_' in iScene.Name.lower():
            lTargetWheelMesh = iScene
            break

    if lTargetWheelMesh != None:

        lTargetWheelBone = lTargetWheelMesh.Parent

        for iScene in lSceneList:
            if "wheel_" in iScene.Name:
                if iScene.Name == lTargetWheelBone.Name:
                    None
                else:
                    lWheelBoneList.append(iScene)

        lWheelBoneList = RS.Utils.Collections.RemoveDuplicatesFromList(lWheelBoneList)

        for iWheelBone in lWheelBoneList:
            lSplitName = iWheelBone.Name.split("_")
            lCloneWheelMesh = lTargetWheelMesh.Clone()
            lCloneWheelMesh.Name = "wheelmesh_" + lSplitName[-1]

            lVector = FBVector3d()
            iWheelBone.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
            lCloneWheelMesh.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
            iWheelBone.GetVector(lVector, FBModelTransformationType.kModelScaling, True)
            lCloneWheelMesh.SetVector(lVector, FBModelTransformationType.kModelScaling, True)

            if "_rf" in lCloneWheelMesh.Name:
                lCloneWheelMesh.Rotation = FBVector3d(-90, 180, 0)
            else:
                if "_rr" in lCloneWheelMesh.Name:
                    lCloneWheelMesh.Rotation = FBVector3d(-90, 180, 0)
                else:
                    iWheelBone.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
                    lCloneWheelMesh.SetVector(lVector, FBModelTransformationType.kModelRotation, True)

            iWheelBone.ConnectSrc(lCloneWheelMesh)
    else:
        FBMessageBox("Error", "WheelMesh does not exist.  Possibly a boat or chopper...", "Ok")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Set Up Vehicle Markers
## NOTE: I have commented out the marker control rig as we added it too late in the project for it to be of use.
##       Keeping it in the script so we can use on the next project.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_VehicleSetupCreateMarkers():

##    lSceneList = FBModelList()
##    FBGetSelectedModels (lSceneList, None, False)
##    FBGetSelectedModels (lSceneList, None, True)
##
##    lBoneToMarkerList = []
##    lMarkerList = []
##
##    lBoneNameList = ['seat', 'window', 'extra', 'door', 'boot', 'bonnet', 'chassis']
##
##    for iScene in lSceneList:
##        for iBoneName in lBoneNameList:
##            if iBoneName in iScene.Name.lower():
##                lBoneToMarkerList.append(iScene)
##
##    lBoneToMarkerList = RS.Utils.Collections.RemoveDuplicatesFromList(lBoneToMarkerList)

    lDummy = rs_VehicleAssetSetupVariables()[0]
    lMover = rs_VehicleAssetSetupVariables()[1]
    lChassis = rs_VehicleAssetSetupVariables()[2]

    lNullRelConst = FBConstraintRelation("Marker_Suspension_Relation")
    #FBSystem().Scene.Constraints.append(lNullRelConst)
    lNullRelConst.Active = True

##    x = 0
##    y = 0
##    for iNull in lBoneToMarkerList:
##
##        lMarker = FBModelMarker(iNull.Name + "_Control")
##        lMarkerList.append(lMarker)
##
##        lMarker.Show = True
##        lMarker.Look = FBMarkerLook.kFBMarkerLookSphere
##        lMarker.PropertyList.Find("Size").Data = 100
##        lMarker.PropertyList.Find("Color RGB").Data = FBColor(1,0,0)
##
##        lVector = FBVector3d()
##        iNull.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
##        lMarker.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
##        iNull.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
##        lMarker.SetVector(lVector, FBModelTransformationType.kModelRotation, True)
##        FBSystem().Scene.Evaluate()
##
##        lSender = lNullRelConst.SetAsSource(lMarker)
##        lReceiver = lNullRelConst.ConstrainObject(iNull)
##
##        if lNullRelConst.SetBoxPosition(lSender, 0, x):
##            x = x + 150
##        else:
##            lNullRelConst.SetBoxPosition(lSender, 0, x)
##
##        if lNullRelConst.SetBoxPosition(lReceiver, 400, y):
##            y = y + 150
##        else:
##            propConst.SetBoxPosition(lReceiver, 400, y)
##
##        RS.Utils.Scene.ConnectBox(lSender, 'Rotation', lReceiver, 'Rotation')
##        RS.Utils.Scene.ConnectBox(lSender, 'Translation', lReceiver, 'Translation')
##
##        FBSystem().Scene.Evaluate()

    lMoverMarker = FBModelMarker(lMover.Name + "_Control")

    lMoverMarker.Show = True
    lMoverMarker.Look = FBMarkerLook.kFBMarkerLookHardCross
    lMoverMarker.PropertyList.Find("Size").Data = 500
    lMoverMarker.PropertyList.Find("Color RGB").Data = FBColor(0,1,0)

    lMoverVector = FBVector3d()
    lMover.GetVector(lMoverVector, FBModelTransformationType.kModelTranslation, True)
    lMoverMarker.SetVector(lMoverVector, FBModelTransformationType.kModelTranslation, True)
    lMover.GetVector(lMoverVector, FBModelTransformationType.kModelRotation, True)
    lMoverMarker.SetVector(lMoverVector, FBModelTransformationType.kModelRotation, True)
    FBSystem().Scene.Evaluate()

    lMoverSender = lNullRelConst.SetAsSource(lMoverMarker)
    lMoverReceiver = lNullRelConst.ConstrainObject(lMover)

    lNullRelConst.SetBoxPosition(lMoverSender, 0, 0)
    lNullRelConst.SetBoxPosition(lMoverReceiver, 400, 0)

    RS.Utils.Scene.ConnectBox(lMoverSender, 'Rotation', lMoverReceiver, 'Rotation')
    RS.Utils.Scene.ConnectBox(lMoverSender, 'Translation', lMoverReceiver, 'Translation')

    FBSystem().Scene.Evaluate()

    lChassisMarker = FBModelMarker(lChassis.Name + "_Control")

    lChassisMarker.Show = True
    lChassisMarker.Look = FBMarkerLook.kFBMarkerLookSphere
    lChassisMarker.PropertyList.Find("Size").Data = 100
    lChassisMarker.PropertyList.Find("Color RGB").Data = FBColor(1,0,0)

    lChassisVector = FBVector3d()
    lChassis.GetVector(lChassisVector, FBModelTransformationType.kModelTranslation, True)
    lChassisMarker.SetVector(lChassisVector, FBModelTransformationType.kModelTranslation, True)
    lChassis.GetVector(lChassisVector, FBModelTransformationType.kModelRotation, True)
    lChassisMarker.SetVector(lChassisVector, FBModelTransformationType.kModelRotation, True)

    FBSystem().Scene.Evaluate()

    lChassisSender = lNullRelConst.SetAsSource(lChassisMarker)
    lChassisReceiver = lNullRelConst.ConstrainObject(lChassis)

    lNullRelConst.SetBoxPosition(lChassisSender, 0, 200)
    lNullRelConst.SetBoxPosition(lChassisReceiver, 400, 200)

    RS.Utils.Scene.ConnectBox(lChassisSender, 'Rotation', lChassisReceiver, 'Rotation')
    RS.Utils.Scene.ConnectBox(lChassisSender, 'Translation', lChassisReceiver, 'Translation')

    FBSystem().Scene.Evaluate()

##    lMultiDimensionalArray = []
##
##    for iNull in lBoneToMarkerList:
##        ChildrenArray = []
##        ChildrenArray.append(FBFindModelByLabelName(iNull.Name + "_Control"))
##        for iChild in iNull.Children:
##            if FBFindModelByLabelName(iChild.Name + "_Control"):
##                ChildrenArray.append(FBFindModelByLabelName(iChild.Name + "_Control"))
##        lMultiDimensionalArray.append(ChildrenArray)
##
##    for i in range(len(lMultiDimensionalArray)):
##        if len(lMultiDimensionalArray[i]) > 0:
##            for j in range(len(lMultiDimensionalArray[i]) - 1):
##                lMultiDimensionalArray[i][j + 1].Parent = lMultiDimensionalArray[i][0]
##
##    lExportableMarker = FBFindModelByLabelName(lChassis.Name + "_Control")

    lMoverMarker.Parent = lChassisMarker
    lChassisMarker.Parent = lDummy

    lMoverMarker.PropertyList.Find("Enable Transformation").Data = False
    lMoverMarker.PropertyList.Find("Enable Selection").Data = False

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Specific Align for Wheel Planes (Used in Suspension Rig)
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AlignWheelPlaneOnX(pModel, pAlignTo, pAlignX):

    lAlignPos = FBVector3d();
    lModelPos = FBVector3d();
    pAlignTo.GetVector(lAlignPos)
    pModel.GetVector(lModelPos)
    if pAlignX:
        if lModelPos[0] > 0:
            lModelPos[0] = lAlignPos[0] - 25.4
        else:
            if lModelPos[0] < 0:
                lModelPos[0] = lAlignPos[0] + 25.4
    pModel.SetVector(lModelPos)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Specific Align for Wheel Spinners (Used in Suspension Rig)
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AlignWheelSpinner( pModel, pAlignTo, pAlignX, pAlignY, pAlignZ):
    lAlignPos = FBVector3d();
    lModelPos = FBVector3d();
    pAlignTo.GetVector(lAlignPos)
    pModel.GetVector(lModelPos)
    if pAlignX:
        lModelPos[0] = lAlignPos[0]
    if pAlignZ:
        lModelPos[2] = lAlignPos[2]

    if pAlignY:
        if lModelPos[2] > 0:
            lModelPos[1] = lAlignPos[1] + 140
        else:
            if lModelPos[2] < 0:
                lModelPos[1] = lAlignPos[1] + 100
    pModel.SetVector(lModelPos)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Set Up Vehicle Suspension Rig
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def VehicleSetupSuspensionRig():

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    lDummy = rs_VehicleAssetSetupVariables()[0]
    lMover = rs_VehicleAssetSetupVariables()[1]
    lExportable = rs_VehicleAssetSetupVariables()[2]
    lGeo = rs_VehicleAssetSetupVariables()[3]
    lExportableMarker = FBFindModelByLabelName(lExportable.Name + "_Control")

    for iFolder in glo.gFolders:
        if 'constraint' in iFolder.Name.lower():
            lConstraintFolder = iFolder
        elif 'material' in iFolder.Name.lower():
            lMaterialFolder = iFolder
        elif 'shaders' in iFolder.Name.lower():
            lShaderFolder = iFolder
        elif 'textures' in iFolder.Name.lower():
            lTextureFolder = iFolder

    lWheelBoneList = []

    for iScene in lSceneList:
        if 'wheel_' in iScene.Name.lower():
            lWheelBoneList.append(iScene)

    lRedShader = FBShaderManager().CreateShader("FlatShader")
    lRedShader.Name = "RedShader"
    lRedShader.PropertyList.Find("Custom Color").Data = FBColor(0.64,0,0)
    lRedShader.PropertyList.Find("Color Source").Data = 1
    lShaderFolder.ConnectSrc(lRedShader)

    lWheelTexture = FBTexture('WheelImage')
    lWheelTexture.Video = FBVideoClip("{0}\\VehicleSetup_wheel.tga".format( RS.Config.Script.Path.ToolImages ))
    lTextureFolder.ConnectSrc(lWheelTexture)

    lBasePlane = FBModelPlane("FloorContact")
    lBasePlane.Show = True
    lBasePlane.ConnectSrc(lRedShader)
    lBasePlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    RS.Utils.Scene.AlignTranslation(lBasePlane, lMover, True, False, True)

    FBSystem().Scene.Evaluate()

    lPlaneMarker = FBModelMarker("FloorContact_Control")
    lPlaneMarker.Show = True
    lPlaneMarker.Look = FBMarkerLook.kFBMarkerLookSphere
    lPlaneMarker.PropertyList.Find("Color RGB").Data = FBColor(1,0,1)
    lPlaneMarker.PropertyList.Find("Size").Data = 500
    lVector = FBVector3d()
    RS.Utils.Scene.AlignTranslation(lExportableMarker, lMover, True, False, True)
    #lExportableMarker.Rotation = FBVector3d(0,0,0)

    FBSystem().Scene.Evaluate()

    lPlaneMarker.Parent = lDummy
    lBasePlane.Parent = lPlaneMarker

    lParentConst = FBConstraintManager().TypeCreateConstraint(3)
    #FBSystem().Scene.Constraints.append(lParentConst)
    lParentConst.Name = ("FloorContact_Control_Parent")
    lParentConst.ReferenceAdd (0,lPlaneMarker)
    lParentConst.ReferenceAdd (1,lExportableMarker)
    lParentConst.Snap()
    lConstraintFolder.ConnectSrc(lParentConst)

    for lWheelBone in lWheelBoneList:

        lSplitName = lWheelBone.Name.split("_")
        lWheelPlane = FBModelPlane(lSplitName[-1] + "_ContactPlane")
        lWheelPlane.Show = True
        lWheelPlane.Scaling = FBVector3d(0.12,1,0.36)
        lWheelPlane.ConnectSrc(lRedShader)
        lWheelPlane.ShadingMode = FBModelShadingMode.kFBModelShadingAll
        RS.Utils.Scene.AlignTranslation(lWheelPlane, lWheelBone, True, False, True)

        FBSystem().Scene.Evaluate()

        lWheelPlane.Parent = lBasePlane

        FBSystem().Scene.Evaluate()

        lHelperNull = FBModelNull (lSplitName[-1] + "_Null")
        lHelperNull.Show = True
        lHelperNullCenter = FBModelNull (lSplitName[-1] + "_CentreNull")
        lHelperNullCenter.Show = True

        RS.Utils.Scene.AlignTranslation(lHelperNull, lWheelBone, True, True, True)
        RS.Utils.Scene.AlignTranslation(lHelperNullCenter, lWheelBone, True, True, True)
        lHelperNull.Rotation = FBVector3d(-90,0,0)
        lHelperNullCenter.Rotation = FBVector3d(-90,0,0)

        FBSystem().Scene.Evaluate()

        rs_AlignWheelPlaneOnX(lHelperNull, lHelperNull, True)

        FBSystem().Scene.Evaluate()

        lHelperNullCenter.Parent = lWheelPlane
        lHelperNull.Parent = lExportableMarker

        lWishBone = FBModelRoot(lSplitName[-1] + "_WishBone")
        lWishBone.Show = True

        lWheelAttach = FBModelSkeleton(lSplitName[-1] + "_WheelAttach")
        lWheelAttach.Show = True

        RS.Utils.Scene.AlignTranslation(lWishBone, lHelperNull, True, True, True)
        lWishBone.Rotation = FBVector3d(-90,0,0)
        RS.Utils.Scene.AlignTranslation(lWheelAttach, lHelperNullCenter, True, True, True)

        FBSystem().Scene.Evaluate()

        lWheelAttach.Parent = lWishBone
        lWishBone.Parent = lExportableMarker

        lWheelAttach.PropertyList.Find("Enable Rotation DOF").Data = True
        lWheelAttach.PropertyList.Find("RotationMinY").Data = True
        lWheelAttach.PropertyList.Find("RotationMinZ").Data = True
        lWheelAttach.PropertyList.Find("RotationMaxY").Data = True
        lWheelAttach.PropertyList.Find("RotationMaxZ").Data = True
        lWheelAttach.PropertyList.Find("RotationMin").Data = FBVector3d(0,-10,-45)
        lWheelAttach.PropertyList.Find("RotationMax").Data = FBVector3d(0,10,45)

        lWishBone.PropertyList.Find("Enable Rotation DOF").Data = True
        lWishBone.PropertyList.Find("RotationMinX").Data = True
        lWishBone.PropertyList.Find("RotationMinY").Data = True
        lWishBone.PropertyList.Find("RotationMinZ").Data = True
        lWishBone.PropertyList.Find("RotationMaxX").Data = True
        lWishBone.PropertyList.Find("RotationMaxY").Data = True
        lWishBone.PropertyList.Find("RotationMaxZ").Data = True
        lWishBone.PropertyList.Find("RotationMin").Data = FBVector3d(0,-10,0)
        lWishBone.PropertyList.Find("RotationMax").Data = FBVector3d(0,10,0)

        lWheelSpin = FBModelMarker(lSplitName[-1] + "_WheelSpinner")
        lWheelSpin.Show = True
        lWheelSpin.Look = FBMarkerLook.kFBMarkerLookLightCross
        lWheelSpin.PropertyList.Find("Color RGB").Data = FBColor(0,0,0)
        lWheelSpin.PropertyList.Find("Size").Data = 250

        rs_AlignWheelSpinner(lWheelSpin, lWheelBone, True, True, True)

        FBSystem().Scene.Evaluate()

        lWheelSpin.Parent = lExportableMarker

        lWheelSpinHandle = FBHandle(lSplitName[-1] + "_Handle")
        lWheelSpinHandle.Show = True
        lWheelSpinHandle.Follow.append(lWheelSpin)
        lWheelSpinHandle.Image.append(lWheelTexture.Video)
        lWheelSpinHandle.Manipulate.append(lWheelSpin)
        lWheelSpinHandle.PropertyList.Find("Default Manipulation Mode").Data = 6
        lWheelSpinHandle.PropertyList.Find("Size").Data = 100
        lWheelSpinHandle.PropertyList.Find("Translation Offset").Data = FBVector3d(0,0,0)

        lPCConst = FBConstraintManager().TypeCreateConstraint(3)
        #FBSystem().Scene.Constraints.append(lPCConst)
        lPCConst.Name = (lSplitName[-1] + "_Wheel_Attach_PC")
        lPCConst.ReferenceAdd (0,lWheelBone)
        lPCConst.ReferenceAdd (1,lWheelAttach)
        FBSystem().Scene.Evaluate()
        lPCConst.Snap()
        FBSystem().Scene.Evaluate()
        lConstraintFolder.ConnectSrc(lPCConst)

        lAimConst = FBConstraintManager().TypeCreateConstraint(0)
        #FBSystem().Scene.Constraints.append(lAimConst)
        lAimConst.Name = (lSplitName[-1] + "_Wishbone_Aim")
        lAimConst.ReferenceAdd (0,lWishBone)
        lAimConst.ReferenceAdd (1,lHelperNullCenter)
        lAimConst.Snap()
        lConstraintFolder.ConnectSrc(lAimConst)

    lOptions = FBFbxOptions(True)
    lOptions.SetAll(FBElementAction.kFBElementActionMerge, True)
    lOptions.CameraSwitcherSettings = FBElementAction.kFBElementActionDiscard
    lOptions.Cameras = FBElementAction.kFBElementActionDiscard

    FBApplication().FileMerge("{0}\\tools\wildwest\script\MotionBuilderPython\ToolImages\VehicleSetup_SteeringWheel.FBX".format( RS.Config.Project.Path.Root ))
    lSteering = RS.Utils.Scene.FindModelByName("SteeringWheel")
    if lSteering is not None:
        lSteeringMaterial = FBMaterial("SteeringWheel_Mat")
        lSteeringMaterial.PropertyList.Find("Diffuse").Data = FBColor(0.33, 0.11, 0.55)
        lSteering.Materials.append(lSteeringMaterial)
        lSteering.Translation = FBVector3d(0, 230, 0)
        FBSystem().Scene.Evaluate()
        lMaterialFolder.ConnectSrc(lSteeringMaterial)

        lVideoClips = FBSystem().Scene.VideoClips
        if len(lVideoClips) > 0:
            lVideoFolder = FBFolder ("Videos", lVideoClips[0])
            for lVideo in lVideoClips:
                lVideoFolder.ConnectSrc(lVideo)

        lSteering.PropertyList.Find("Enable Rotation DOF").Data = True
        lSteering.PropertyList.Find("RotationMinX").Data = True
        lSteering.PropertyList.Find("RotationMinY").Data = True
        lSteering.PropertyList.Find("RotationMinZ").Data = True
        lSteering.PropertyList.Find("RotationMaxX").Data = True
        lSteering.PropertyList.Find("RotationMaxY").Data = True
        lSteering.PropertyList.Find("RotationMaxZ").Data = True
        lSteering.PropertyList.Find("RotationMin").Data = FBVector3d(-90,-44,-180)
        lSteering.PropertyList.Find("RotationMax").Data = FBVector3d(-90,44,-180)
        lSteering.PropertyList.Find("Pre Rotation").Data = FBVector3d(0,0,0)

        lSteering.Parent = lExportableMarker
        FBSystem().Scene.Evaluate()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Set Up Vehicle Suspension Rig Relation Constraint
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def VehicleSetupSuspensionRigConstraint():

    lWheelSpinList = []
    lWheelAttachList = []

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)

    for iScene in lSceneList:
        if '_WheelSpinner' in iScene.Name:
            lWheelSpinList.append(iScene)
        elif '_WheelAttach' in iScene.Name:
            lWheelAttachList.append(iScene)

    for iCon in glo.gConstraints:
        if iCon.Name == "Marker_Suspension_Relation":
            lRelConst = iCon

    lSteeringWheel = FBFindModelByLabelName("SteeringWheel")
    lMover = rs_VehicleAssetSetupVariables()[1]
    lExportable = rs_VehicleAssetSetupVariables()[2]
    lExportableControl = FBFindModelByLabelName(lExportable.Name + "_Control")
    lPlaneControl = FBFindModelByLabelName("FloorContact_Control")

    lSteeringSender = lRelConst.SetAsSource(lSteeringWheel)
    lSteeringSender.UseGlobalTransforms = False
    lRelConst.SetBoxPosition(lSteeringSender, 800, -50)

    lVecNumConvSteering = lRelConst.CreateFunctionBox('Converters', 'Vector to Number')
    lRelConst.SetBoxPosition(lVecNumConvSteering, 1200, -50)

    RS.Utils.Scene.ConnectBox(lSteeringSender, 'Lcl Rotation', lVecNumConvSteering, 'V')

    x = 100
    for i in range(len(lWheelSpinList)):

        lWheelSpinSender = lRelConst.SetAsSource(lWheelSpinList[i])
        lWheelSpinSender.UseGlobalTransforms = False
        if lRelConst.SetBoxPosition(lWheelSpinSender, 800, x):
            x = x + 150
        else:
            lRelConst.SetBoxPosition(lWheelSpinSender, 800, x)

        if "f_" in lWheelSpinList[i].Name:

            lVecNumConv = lRelConst.CreateFunctionBox('Converters', 'Vector to Number')
            lRelConst.SetBoxPosition(lVecNumConv, 1200, (x - 150))

            lNumVecConv = lRelConst.CreateFunctionBox('Converters', 'Number to Vector')
            lRelConst.SetBoxPosition(lNumVecConv, 1600, (x - 150))

            lWheelAttachReceiver = lRelConst.ConstrainObject(lWheelAttachList[i])
            lWheelAttachReceiver.UseGlobalTransforms = False
            lRelConst.SetBoxPosition(lWheelAttachReceiver, 1900, (x - 150))

            RS.Utils.Scene.ConnectBox(lWheelSpinSender, 'Lcl Rotation', lVecNumConv, 'V')
            RS.Utils.Scene.ConnectBox(lVecNumConv, 'X', lNumVecConv, 'X')
            RS.Utils.Scene.ConnectBox(lNumVecConv, 'Result', lWheelAttachReceiver, 'Lcl Rotation')
            RS.Utils.Scene.ConnectBox(lVecNumConvSteering, 'Y', lNumVecConv, 'Z')

        else:
            lWheelAttachReceiver = lRelConst.ConstrainObject(lWheelAttachList[i])
            lWheelAttachReceiver.UseGlobalTransforms = False
            lRelConst.SetBoxPosition(lWheelAttachReceiver, 1900, (x - 150))

            RS.Utils.Scene.ConnectBox(lWheelSpinSender, 'Lcl Rotation', lWheelAttachReceiver, 'Lcl Rotation')

    FBSystem().Scene.Evaluate()


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: GetLayoutType - grabbing layout data from the vehicle.meta
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

# add layout type data as a custom property on chassis
def GetLayoutType( vehicleName ):

    layout = None
    if RS.Config.Project.Name.lower() == 'gta5':
        devPath = 'dev_ng'
    else:
        devPath = 'dev'

    project = RS.Config.Project.Name.lower()

    metafile = os.path.join(RS.Config.Project.Path.Root,
                            'build',
                            devPath,
                            'common',
                            'data',
                            'levels',
                            project,
                            'vehicles.meta')
    if os.path.exists(metafile):

        RS.Perforce.Sync( metafile, force=True )

        metafileRead = RS.Core.Metadata.ParseMetaFile( metafile )
        for i in metafileRead.Root.InitDatas:
            if i.modelName.lower() == vehicleName.lower():
                layout = i.layout
                break

    return layout


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: GetSteeringWheelOffset - grabbing steering wheel offset data from the vehiclelayouts.meta
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def GetSteeringWheelOffset( layout ):

    steeringOffset = None
    if RS.Config.Project.Name.lower() == 'gta5':
        devPath = 'dev_ng'
    else:
        devPath = 'dev'

    project = RS.Config.Project.Name.lower()

    metafile = r'{0}\\build\\{1}\\common\\data\\ai\\vehiclelayouts.meta'.format( RS.Config.Project.Path.Root, devPath, project )

    if os.path.exists( metafile ):

        RS.Perforce.Sync( metafile, force=True )

        metafileRead = RS.Core.Metadata.ParseMetaFile( metafile )
        for i in metafileRead.Root.VehicleLayoutInfos:
            if i.Name.lower() == layout.lower():
                steeringOffset = i.SteeringWheelOffset.X, i.SteeringWheelOffset.Y, i.SteeringWheelOffset.Z
                break

    return steeringOffset

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Vehicle Setup
##                         - Vehicle Setup Functions:
##                           Delete animation, Delete unwanted nulls, Get Exportable Null Variable,
##                           Hide mover, Parent geo to dummy, Brighten Materials
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupVehicles(pControl, pEvent):

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDummy = rs_VehicleAssetSetupVariables()[0]
    lMover = rs_VehicleAssetSetupVariables()[1]
    lExportable = rs_VehicleAssetSetupVariables()[2]
    lGeo = rs_VehicleAssetSetupVariables()[3]

    lDummy.Scaling = FBVector3d(100,100,100)
    FBSystem().Scene.Evaluate()

    lRemoveMePropList = []

    FBSystem().CurrentTake.ClearAllProperties(False)

    rs_DeleteItemsVehicleSetup()
    ass.rs_AssetSetupDeleteItems()

    lExportableControl = FBFindModelByLabelName(lExportable.Name + "_Control")
    lPlaneControl = FBFindModelByLabelName("FloorContact_Control")

    if lGeo.Parent == None:
        lGeo.Parent = lDummy

    lMover.Name = "mover"

    RS.Core.AssetSetup.VehicleWheelSetup.SetupWheels()

    rs_VehicleSetupCreateMarkers()

    RS.Utils.Scene.GetChildren(lExportable, lRemoveMePropList, "", False)

    for iNull in lRemoveMePropList:
        iNull.PropertyCreate("RemoveMe", FBPropertyType.kFBPT_bool, "Bool", True, True, None)
    
    
    lTrnsShader = FBShaderLighted('Window_Lighted')
    lTrnsShader.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha
    lGeo.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    lGeo.Shaders.append(lTrnsShader)

    ass.rs_AssetSetupFolderTidy()

    ass.rs_AssetSetupCustomProperties()

    lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "Vehicles"

    rs_CreateGroupsVehicleSetup()

    for iMat in FBSystem().Scene.Materials:
        if iMat:
            iMat.Ambient = FBColor(0,0,0)
    
    for iCamera in glo.gCameras:
        if iCamera.Name == "Producer Perspective":
            iCamera.PropertyList.Find("Far Plane").Data = 400000

    # add layout type data as a custom property on chassis url:bugstar:2029360
    layoutData = GetLayoutType( lFileName )

    if layoutData:
        tag = lExportable.PropertyCreate('layouttype', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = str( layoutData )
    else:
        print 'unable to find layout type data in meta file, vehicle name may be incorrect.'

    # add new null to represent the steering wheel offset url:bugstar:2029360
    if layoutData:
        steeringOffset = GetSteeringWheelOffset( str( layoutData ) )
        if steeringOffset:

            print 'Vehicle: ', lFileName
            print 'Layout Type: ', layoutData
            print 'Steering Offset: ', steeringOffset
            print

            steeringOffsetNull = FBModelNull( 'SteeringOffset_' + str( layoutData ) )

            frontDoorSideSeat = None
            for i in RS.Globals.Components:
                if i.Name == 'seat_dside_f':
                    frontDoorSideSeat = i

            if frontDoorSideSeat:
                steeringOffsetNull.Parent = frontDoorSideSeat
                steeringOffsetNull.SetVector( FBVector3d( steeringOffset ), FBModelTransformationType.kModelTranslation, False )
                tag = steeringOffsetNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = "rs_Mesh"

    ass.rs_CreateToyBoxMarker()

    # custom rig setup for rdr3 rowboat url:bugstar:2111684
    additional_feedback_string = ""
    if RS.Config.Project.Name.lower() == 'rdr3':
        if lFileName.lower() == 'rowboat':
            mover = FBFindModelByLabelName( "mover" )
            mover_control = FBFindModelByLabelName( "mover_Control" )
            chassis_control = FBFindModelByLabelName( "chassis_Control" )
            if mover_control and mover and chassis_control:
                mover_control.Show = False
                mover.Show = False
                chassis_control.Show = False

            RS.Core.AssetSetup.Vehicle_Rowboat_CustomRig.Rowboat_Oar_Custom_Rig_Setup()
            additional_feedback_string = additional_feedback_string + "\n\nNote: rowboat has received an additional custom rig for the oars.\nPlease see Tech Art if you're unsure how this works."
    
    FBMessageBox( "Vehicle Setup", lFileName + " has now been prepped.{0}".format( additional_feedback_string ), "Ok" )

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Suspension Rig Setup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_HubConstraints():

    WheelArray = ["wheel_rf",
                  "wheel_rr",
                  "wheel_lf",
                  "wheel_lr"]

    HubArray =   ["hub_rf",
                  "hub_rr",
                  "hub_lf",
                  "hub_lr"]

    lConstraintFolder = None

    for iFolder in glo.gFolders:
        if 'constraint' in iFolder.Name.lower():
            lConstraintFolder = iFolder

    for i in range(len(WheelArray)):

        Hub   = FBFindModelByLabelName(HubArray[i])
        Wheel = FBFindModelByLabelName(WheelArray[i])

        if Hub and Wheel:

            lParentConst = FBConstraintManager().TypeCreateConstraint(3)
            #FBSystem().Scene.Constraints.append(lParentConst)
            lParentConst.Name = (HubArray[i] + "_Parent")
            lParentConst.ReferenceAdd (0,Hub)
            lParentConst.ReferenceAdd (1,Wheel)
            FBSystem().Scene.Evaluate()
            lParentConst.Snap()

            FBSystem().Scene.Evaluate()

            if lConstraintFolder:
                lConstraintFolder.ConnectSrc(lParentConst)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Description: Definition - Suspension Rig Setup
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

def rs_AssetSetupRigVehicles(pControl, pEvent):

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    VehicleSetupSuspensionRig()

    VehicleSetupSuspensionRigConstraint()

    rs_HubConstraints()

    FBMessageBox("Vehicle Setup", lFileName + " has now been prepped with a Suspension Rig. Please double check it is correct!", "Ok")