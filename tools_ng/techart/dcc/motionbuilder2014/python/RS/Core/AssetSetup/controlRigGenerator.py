from PySide import QtGui

import pyfbsdk as mobu

from RS import Globals
from RS.Utils import Path
from RS.Core.AssetSetup.Serializer import Generic
from RS.Core.Animation import Lib

# get latest on folder:
# //rdr3/assets/metadata/characters/skeleton/


class ControlRigGenerator(Generic.Serializer):
    """
    Creates an xml file for the model's control rig and all properties, components that relate to it
    """
    def __init__(self):
        """
        Constructor
        """
        self.rigRoot = None
        character = self._getCharacter()
        if character and Lib.HasControlRig(character):
            rigRoot = Lib.GetControlRigReferenceNull(character)

            super(ControlRigGenerator, self).__init__(characterNamespaceDictionary={
                rigRoot.LongName.split(":")[0]: character.Name})

            self._rootElement.append(self.SerializeComponent(character, maxDepth=1, excludeByType=[
                mobu.FBConstraintSolver, mobu.FBControlSet, mobu.FBMesh]))
            self._rootElement.append(self.SerializeComponent(rigRoot, excludeByType=[mobu.FBMesh]))
            self._rootElement[0].attrib = {"Name": character.Name, "ActiveInput": "True"}

    def _getFileName(self):
        """
        Gets the name of the file

        Returns:
            String - the name of the file in lower case
        """
        return Path.GetBaseNameNoExtension(mobu.FBApplication().FBXFileName).lower()

    def _getCharacter(self):
        """
        Gets the first character node in the scene.

        Returns:
            FBCharacter
        """
        characterList = Globals.Characters
        if not characterList:
            QtGui.QMessageBox.warning(
                None,
                "Character Error",
                "Character does not exist in the scene.",
                QtGui.QMessageBox.Ok
            )
            return None
        return characterList[0]

    def _isMobuRig(self):
        """
        Checks if the control rig in the scene is a fully custom rig or a mobu rig (which could
        have custom elements).
        
        Returns:
            FBControlSetType or None if no mobu rig is found.
        """
        if not self.rigRoot:
            return

        # get character's hips in control rig
        character = self._getCharacter()
        controlRig = character.GetCurrentControlSet()
        if not controlRig:
            return None
        return str(controlRig.ControlSetType)