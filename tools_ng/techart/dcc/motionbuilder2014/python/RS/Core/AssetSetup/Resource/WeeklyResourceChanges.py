"""
Generates a report about the changes in the <project>\\art\\animation\\resources\\ directory
"""
import datetime
import RS.Config
import RS.Perforce
import RS.Utils.Html

from PySide import QtGui


def WeeklyResourceChanges():
    """
    Creates an html report about the changes made during the last week in the
    <project>\\art\\animation\\resources\\ directory
    """
    # # Constants # #
    
    NUM_PREV_DAYS_TO_REPORT = 7
    
    weeklyChanges = {}
    
    RS.Perforce.p4.Connect()
    
    # Collect the past week's worth of changes.
    lastWeekDate = datetime.datetime.now() - datetime.timedelta(days=NUM_PREV_DAYS_TO_REPORT)
    recordSet = RS.Perforce.p4.Run('changes',
                                   ['-l', '{rootPath}\\art\\animation\\resources\\...@{year}/{month}/{day},@now'.format(
                                       rootPath=RS.Config.Project.Path.Root, year=lastWeekDate.year,
                                       month=lastWeekDate.month, day=lastWeekDate.day)])
    
    for record in recordSet:
        description = record['desc'].rstrip()
        changelist = record['change']
        
        changeRecordSet = RS.Perforce.p4.Run('files', ['@={0}'.format(changelist)])
        
        for changeRecord in changeRecordSet:
            depotFile = changeRecord['depotFile']
            
            if changelist not in weeklyChanges:
                weeklyChanges[changelist] = [description, [depotFile]]
                
            else:
                data = weeklyChanges[changelist]
                data[-1].append(depotFile)
                
                weeklyChanges[changelist] = data
                
    changelists = weeklyChanges.keys()
    changelists.sort()
    
    # Generate the HTML report.
    
    todayMonth = datetime.datetime.now().month
    todayYear = datetime.datetime.now().year
    todayDay = datetime.datetime.now().day
    prevMonth = lastWeekDate.month
    prevYear = lastWeekDate.year
    prevDay = lastWeekDate.day
    
    splitProjectName = RS.Config.Project.Path.Root.split('\\')
    docPath = ("{rootPath}\\techart\\dcc\\motionbuilder{0}\\python\\RS\\Core\\"
               "AssetSetup\\outputs\\ResourceWeeklyUpdate\\katResourceChanges_{1}_{2}.html").format(
        RS.Config.Script.TargetBuild, splitProjectName[-1],
        "{}_{}_{}".format(todayDay, todayMonth, todayYear), rootPath=RS.Config.Tool.Path.Root)

    doc = RS.Utils.Html.Document(docPath, 'TechArt - Resource Changes')
    fontStyle = RS.Utils.Html.FontStyle()

    doc.AddHeader('TechArt - Resource Changes - {0}\{1}\{2} - {3}\{4}\{5}'.format(
        prevMonth, prevDay, prevYear, todayMonth, todayDay, todayYear),
        RS.Utils.Html.FontStyle(fontSize=5, textStyleFlags=RS.Utils.Html.TextStyle.Bold))
    
    tableHeaderFontStyle = RS.Utils.Html.FontStyle(foregroundColor='white', textStyleFlags=RS.Utils.Html.TextStyle.Bold)
    tableHeaderCellStyle = RS.Utils.Html.TableCellStyle(tableHeaderFontStyle, backgroundColor='gray')
    
    tableCellStyle = RS.Utils.Html.TableCellStyle(fontStyle, widthStyle=RS.Utils.Html.WidthStyle.Fixed,
                                                  align=RS.Utils.Html.AlignStyle.Left)
    tableCellStyleCenter = RS.Utils.Html.TableCellStyle(fontStyle, align=RS.Utils.Html.AlignStyle.Center)
    
    reportTable = RS.Utils.Html.Table(numColumns=3, cellSpacing=0, widthStyle=RS.Utils.Html.WidthStyle.Percentage)
    
    reportTable.AddItem('Changelist', tableHeaderCellStyle)
    reportTable.AddItem('Description', tableHeaderCellStyle)
    reportTable.AddItem('Files', tableHeaderCellStyle)
    
    for changelist in changelists:
        desc, depotFiles = weeklyChanges[changelist]
        desc = desc.replace('\n', '<br>')
        reportTable.AddItem(changelist, tableCellStyleCenter)
        reportTable.AddItem(desc, tableCellStyle)
        
        files = ''.join("{}<br>".format(depotFile) for depotFile in depotFiles)
            
        reportTable.AddItem(files, tableCellStyle)
        
    doc.AddTable(reportTable)
    doc.Write()
    
    QtGui.QMessageBox.information(None, 'R*', 'Weekly Update output to:\n{0}'.format(docPath))