import RS.Core.Metadata as meta
import RS.Core.AssetSetup.ExprToAst as work
from pyfbsdk import *

unaryOpDict     = {"kNegate"                :"-1 *", 
                   "kAbs"                   :"abs", 
                   "kAcos"                  :"acos", 
                   "kAsin"                  :"asin", 
                   "kATan"                  :"atan", 
                   "kCeil"                  :"ceil", 
                   "kCos"                   :"cos", 
                   "kCosH"                  :"cosh", 
                   "kDToR"                  :"0.0174532925 *", 
                   "kExp"                   :"exp", 
                   "kLn"                    :"ln",
                   "kFloor"                 :"floor", 
                   "kLog"                   :"log", 
                   "kRToD"                  :"57.2957795 *", 
                   "kSin"                   :"sin", 
                   "kSinH"                  :"sinH", 
                   "kSqrt"                  :"sqrt", 
                   "kTan"                   :"tan", 
                   "kTanH"                  :"tanh", 
                   "kCosD"                  :"cosd", 
                   "kSinD"                  :"sind", 
                   "kTanD"                  :"tand",
                   "kAcosD"                 :"acosd", 
                   "kAsinD"                 :"asind", 
                   "kAtanD"                 :"atand"}

binaryOpDict    = {"kAssign"                :"=", 
                   "kAdd"                   :"+", 
                   "kSubtract"              :"-", 
                   "kMultiply"              :"*",
                   "kDivide"                :"/",
                   "kGreaterThan"           :">", 
                   "kGreaterThanEqualTo"    :">",
                   "kLessThan"              :"<",             
                   "kLessThanEqualTo"       :"<=", 
                   "kEqualTo"               :"==", 
                   "kNotEqualTo"            :"!="} 
                   
binaryFuncDict  = {"kMax"                   :"max", 
                   "kMin"                   :"min", 
                   "kMod"                   :"mod", 
                   "kPow"                   :"pow"}

metaFile = meta.ParseMetaFile( r"X:\gta5\assets\anim\expressions\P_M_One\P_M_One.xml" )

newBoneLst = work.populateBoneLst(metaFile)

ifCount = 0

positionInputArray  = []
rotationInputArray  = []
scaleInputArray     = []
customInputArray    = []

positionOutputArray  = []
rotationOutputArray  = []
scaleOutputArray     = []
customOutputArray    = []

def recursivelyParseExpression( expression ):
    
    global ifCount
    
    exprString = ""
    
    if str(expression.__class__) == "<type 'float'>":
        return str(expression)
        
    if "#" in expression:
        partitionEpression = expression.partition("#")
        if partitionEpression[2].startswith("translate"):
            if not ( partitionEpression[0] + "_XYZ_Pos") in positionInputArray:
                positionInputArray.append(( partitionEpression[0] + "_XYZ_Pos"))
            if "X" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Pos[0]")
            if "Y" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Pos[1]")
            if "Z" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Pos[2]")
        elif partitionEpression[2].startswith("rotate"):
            if not ( partitionEpression[0] + "_XYZ_Rot") in rotationInputArray:
                rotationInputArray.append(( partitionEpression[0] + "_XYZ_Rot"))
            if "X" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Rot[0]")
            if "Y" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Rot[1]")
            if "Z" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Rot[2]")
        elif partitionEpression[2].startswith("scale"):
            if not ( partitionEpression[0] + "_XYZ_Scale") in scaleInputArray:
                scaleInputArray.append(( partitionEpression[0] + "_XYZ_Scale"))
            if "X" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Scale[0]")
            if "Y" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Scale[1]")
            if "Z" in partitionEpression[2]:
                return str(partitionEpression[0] + "_XYZ_Scale[2]")
        else:
            if not "ifVar" in partitionEpression[0]:
                if not ( partitionEpression[0] + "_" + partitionEpression[2]) in customInputArray:
                    customInputArray.append( partitionEpression[0] + "_" + partitionEpression[2])
                if partitionEpression[2] != "":
                    return str(partitionEpression[0] + "_" + partitionEpression[2])
                else:
                    return str(partitionEpression[0])

    try:
        if str(expression[0].__class__) == "<type 'list'>":

            exprString = recursivelyParseExpression( expression[0] )
    except:
        None
        
    else:
          
        if expression[0] in unaryOpDict:
              
            unaryString = " {0}( ".format(unaryOpDict[expression[0]])
            unaryString += recursivelyParseExpression( expression[1] )
            unaryString += " ) "
            return unaryString
            
        elif expression[0] in binaryOpDict:

            binaryString = " ( "
            binaryString += recursivelyParseExpression( expression[1] )
            binaryString += " {0} ".format(binaryOpDict[expression[0]])
            binaryString += recursivelyParseExpression( expression[2] )
            binaryString += " ) "
            return binaryString
            
        elif expression[0] in binaryFuncDict:

            binaryString = " {0}( ".format(binaryFuncDict[expression[0]])
            binaryString += recursivelyParseExpression( expression[1] )
            binaryString += " , "
            binaryString += recursivelyParseExpression( expression[2] )
            binaryString += " ) "
            return binaryString

        if expression[0] == "kIfThenElse":
            
            ifVar = "ifVar{0}".format(ifCount)
            ifString = " if( "
            ifString += recursivelyParseExpression( expression[1] )
            ifString += " )\n {\n\t"
            trueVar = recursivelyParseExpression( expression[2] )
            
            if trueVar.startswith(" if( "):   
                ifString += trueVar      
            else:
                ifString += "{0} = {1};".format(ifVar, trueVar)
            
            ifString += "\n }else{\n\t" 
            falseVar = recursivelyParseExpression( expression[3] )
            
            if falseVar.startswith(" if( "):    
                ifString += falseVar     
            else: 
                ifString += "{0} = {1};".format(ifVar, falseVar) 
            ifString += "\n }\n"
            return ifString

    return exprString


def recurseIfStatements( array ):
    
    for node in array:
        if node == "kIfThenElse":
            return True   
        elif node == "<type 'list'>":
            recurseIfStatements( node )          
    return False

global ifNodes
ifNodes = []

def splitOutIfStatements( array, returnArray ):
    
    global ifCount
    global ifNodes
    
    ifVars = []
    exprNodes = [] 
        
    for node in array:
        
        if node == "kIfThenElse":
            return array
            
        elif str(node.__class__) == "<type 'list'>":
            returnedArray = splitOutIfStatements( node, False )
            if str(returnedArray.__class__) == "<type 'list'>":
                ifNodes.append(returnedArray)
    
    for i in range(len(ifNodes)):
    
        if ifNodes[i] in array:
                
            ifVars.append("ifVar{0}".format(ifCount + i))
            index = array.index(ifNodes[i])
            if index > 0:
                array[index] = "ifVar{0}#".format(ifCount + (i + 1))
                 
    if returnArray:
        
        exprNodes.append(array)
        returnArray = ["Split", ifVars, ifNodes, exprNodes]
        return returnArray
        

def buildExpression( name, array, axis, transform, index):
    
    global ifCount
    global ifNodes
    
    if len(array) > 0: 
        
        print "//###############################################################################################################"
        print "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
        print "//##"
        print "//## Description: {0} {1}".format(axis, transform)
        print "//##"
        print "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
        print "//###############################################################################################################"
        print
        
        ifArray = []
        exprArray = []
        ifVarArray = []
        transIfExprArray = []
        transExprArray = []
        inLine = False
        
        if transform != "Scaling":
        
            for expr in array:
                
                findIf = recurseIfStatements(expr)
                
                if findIf:
                    ifArray.append(expr)
                else:
                    exprArray.append(expr)  

        else:
            
            for expr in array:
                del ifNodes[:]
                findIf = None
                findIf = splitOutIfStatements( expr, True )
                #print "FINDIF: " + str(findIf)
                
                if str(findIf.__class__) == "<type 'list'>":
                    if findIf[0] != "Split":
                        ifArray.extend( findIf )
                        inLine = False     
                    else:
                        del ifVarArray[:]
                        del ifArray[:]
                        del exprArray[:]
                        
                        inLine = True
                        #ifVarArray = findIf[1]
                        ifArray = findIf[2]
                        exprArray = findIf[3] 
                        del findIf

        
        for i in range(len(ifArray)):
            
            #print "IFARRAY[i]: " + str(ifArray[i])
            #if inLine == False:
            ifCount += 1
            ifVarArray.append("ifVar{0}".format(ifCount))
            transIfExprArray.append(recursivelyParseExpression( ifArray[i] ))
        
        for i in range(len(ifVarArray)):
            print "double {0} = 0.0;".format(ifVarArray[i]) 
        print
        for i in range(len(transIfExprArray)):
            print transIfExprArray[i]
        
        print name + "_XYZ_{0}[{1}] = \n".format(transform, index)
        
        for i in range(len(ifVarArray)):
            if inLine == False:
                if i == 0:

                    if transform != "Rotation": 
                        if len(ifVarArray) == 1 and len(exprArray) == 0:
                            print " {0};".format(ifVarArray[i])
                        else:
                            print " {0} + \n".format(ifVarArray[i])
                    else:
                        if len(ifVarArray) == 1 and len(exprArray) == 0:
                            print " ( {0} ) *  57.2957795;".format(ifVarArray[i])
                        else:
                            print " ( {0} + \n".format(ifVarArray[i])

                elif i == len(ifVarArray) - 1:
                    if len(exprArray) == 0:
                        if transform != "Rotation": 
                            print " {0};".format(ifVarArray[i])
                        else:
                            print " {0} ) *  57.2957795;".format(ifVarArray[i])
                    else:
                        print " {0} + \n".format(ifVarArray[i])
                else:
                    print " {0} + \n".format(ifVarArray[i])
                
        for i in range(len(exprArray)):        
            transExprArray.append(recursivelyParseExpression( exprArray[i] ))
                
        for i in range(len(transExprArray)):
            if transform != "Rotation":
                if i == len(transExprArray) - 1:
                    print " {0};".format(transExprArray[i])
                else:
                    print " {0} + \n".format(transExprArray[i])
            else:
                if i == 0:
                    if len(ifVarArray) == 0:
                        if i == len(transExprArray) - 1:
                            print " ( {0} ) *  57.2957795;".format(transExprArray[i])
                        else:
                            print " ( {0} + \n".format(transExprArray[i])
                    else:
                        if i == len(transExprArray) - 1:
                            print " {0} ) *  57.2957795;".format(transExprArray[i])
                        else:
                            print " {0} + \n".format(transExprArray[i])
                elif i == len(transExprArray) - 1:
                    print " {0} ) *  57.2957795;".format(transExprArray[i])
                else:
                    print " {0} + \n".format(transExprArray[i])
                    
                    
                
        print

    else:
        
        print "// {0} {1}".format(axis, transform)
        print
        if transform == "Scaling":
            print name + "_XYZ_{0}[{1}] = 1;\n".format(transform, index)
        else:
            print name + "_XYZ_{0}[{1}] = 0;\n".format(transform, index)
        print

for bone in newBoneLst:

    if "WB" not in bone.name and "WA" not in bone.name and not "WM" in bone.name:
        
        positionOutputArray.append( bone.name + "_XYZ_Position")
        rotationOutputArray.append( bone.name + "_XYZ_Rotation")
        scaleOutputArray.append( bone.name + "_XYZ_Scaling")
        
        print "//###############################################################################################################"
        print "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
        print "//##"
        print "//## Description: " + bone.name
        print "//##"
        print "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
        print "//###############################################################################################################"
        
        boneExprLst = ( (bone.name, bone.xTransLst, "X", "Position",    0),
                        (bone.name, bone.yTransLst, "Y", "Position",    1),
                        (bone.name, bone.zTransLst, "Z", "Position",    2),
                        (bone.name, bone.xRotLst,   "X", "Rotation",    0),
                        (bone.name, bone.yRotLst,   "Y", "Rotation",    1),
                        (bone.name, bone.zRotLst,   "Z", "Rotation",    2),
                        (bone.name, bone.xScaleLst, "X", "Scaling",     0),
                        (bone.name, bone.yScaleLst, "Y", "Scaling",     1),
                        (bone.name, bone.zScaleLst, "Z", "Scaling",     2) )
    
        for k in range(len(boneExprLst)):
            
            buildExpression( boneExprLst[k][0], boneExprLst[k][1], boneExprLst[k][2], boneExprLst[k][3], boneExprLst[k][4])

def inputOutputs( array, transfrom, inOut ):
    
    print "\t//###############################################################################################################"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//##"
    print "\t//## Description:{0} {1}".format(transfrom, inOut)
    print "\t//##"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//###############################################################################################################"
    print
    
    for item in array:
    
        print "\tHFBAnimationNode\tm{0};".format(item)   
        
    print

"""

THIS PRINT OUT THE PRIVATE VARIABLES FOR THE HEADER FILE

"""
inputOutputs( positionInputArray,   "POSITION", "INPUT")
inputOutputs( rotationInputArray,   "ROTATION", "INPUT")
inputOutputs( scaleInputArray,      "SCALE",    "INPUT")
inputOutputs( customInputArray,     "CUSTOM",   "INPUT")

inputOutputs( positionOutputArray,  "POSITION", "OUTPUT")
inputOutputs( rotationOutputArray,  "ROTATION", "OUTPUT")
inputOutputs( scaleOutputArray,     "SCALE",    "OUTPUT")

global inputCount
inputCount = 0

def createInput( array, transfrom, inOut ):
    
    global inputCount
    print "\t//###############################################################################################################"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//##"
    print "\t//## Description:{0} {1}".format(transfrom, inOut)
    print "\t//##"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//###############################################################################################################"
    
    for i, item in enumerate(array):
        
        inputCount = inputCount + 1
        
        if transfrom == "CUSTOM":
            print '\tm{0} = AnimationNodeInCreate ( {1}, "{0}", ANIMATIONNODE_TYPE_NUMBER );'.format(item, inputCount)  
        else:
            print '\tm{0} = AnimationNodeInCreate ( {1}, "{0}", ANIMATIONNODE_TYPE_VECTOR );'.format(item, inputCount)   
        
    print 
    
def createOutput( array, transfrom, inOut ):
    
    global inputCount
    print "\t//###############################################################################################################"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//##"
    print "\t//## Description:{0} {1}".format(transfrom, inOut)
    print "\t//##"
    print "\t//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
    print "\t//###############################################################################################################"
    
    for i, item in enumerate(array):
        
        inputCount = inputCount + 1
        
        if transfrom == "CUSTOM":
            print '\tm{0} = AnimationNodeOutCreate ( {1}, "{0}", ANIMATIONNODE_TYPE_NUMBER );'.format(item, inputCount)   
        else:
            print '\tm{0} = AnimationNodeOutCreate ( {1}, "{0}", ANIMATIONNODE_TYPE_VECTOR );'.format(item, inputCount)    

    print 

"""

THIS PRINT OUT THE PRIVATE VARIABLES FOR FBCREATE FOR THE CPP FILE

"""  
createInput( positionInputArray,   "POSITION", "INPUT")
createInput( rotationInputArray,   "ROTATION", "INPUT")
createInput( scaleInputArray,      "SCALE",    "INPUT")
createInput( customInputArray,     "CUSTOM",   "INPUT")

createOutput( positionOutputArray,  "POSITION", "OUTPUT")
createOutput( rotationOutputArray,  "ROTATION", "OUTPUT")
createOutput( scaleOutputArray,     "SCALE",    "OUTPUT")


statusCount = len(positionInputArray) + len(rotationInputArray) + len(scaleInputArray) + len(customInputArray)

print "\tint lStatus[{0}];".format(statusCount)

allVecInputs = []
allVecInputs.extend(positionInputArray)
allVecInputs.extend(rotationInputArray)
allVecInputs.extend(scaleInputArray)

allVecOutputs = []
allVecOutputs.extend(positionOutputArray)
allVecOutputs.extend(rotationOutputArray)
allVecOutputs.extend(scaleOutputArray)

allVecs = []
allVecs.extend(allVecInputs)
allVecs.extend(allVecOutputs)

"""

THIS PRINT OUT THE LSTATUS INPUTS AND OUTS FOR THE ANIMATION NOTIFY IN CPP FILE

"""
print

for i, item in enumerate(allVecs):
    
    if i == 0:
        print "\tFBVector3d {0},".format(item)
    elif i == len(allVecs) - 1:
        print "\t           {0};".format(item)
    else:
        print "\t           {0},".format(item)

print

for i, item in enumerate(customInputArray):
    
    if i == 0:
        print "\tdouble {0},".format(item)
    elif i == len(customInputArray) - 1:
        print "\t           {0};".format(item)
    else:
        print "\t           {0},".format(item)

print

statusCount = 0

for i, item in enumerate(allVecInputs):
    
    print "\tlStatus[{0}] = m{1} -> ReadData( {1}, pEvaluateInfo );".format(i, item)
    statusCount = i
    
for i, item in enumerate(customInputArray):
    
    print "\tlStatus[{0}] = m{1} -> ReadData( &{1}, pEvaluateInfo );".format((statusCount + (i + 1)), item)
    
print

for i, item in enumerate(allVecOutputs):
    
    print "\tm{0} -> WriteData( &{0}[0], pEvaluateInfo );".format(item)
    
print
