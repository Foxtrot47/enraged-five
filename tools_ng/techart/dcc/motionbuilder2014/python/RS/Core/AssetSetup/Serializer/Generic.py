"""
Module for generating an xml based on a hierarchy of a component in a motion builder scene
"""
import re
from xml.dom import minidom
from xml.etree import cElementTree as xml

import pyfbsdk as mobu

from RS.Utils import Scene

# //rdr3/assets/metadata/characters/skeleton/
# X:\wildwest\etc\config\characters\skeleton\bird\vulture\controlrig.xml


class Serializer(object):
    """ Creates an xml file for the model's control rig and all properties, components that relate to it """
    def __init__(self, component=None, characterNamespaceDictionary=None, rootElementName="Setup"):
        """
        Constructor

        Arguments:
            component (mobu.FBComponent): component whose hiearchy should be saved
            characterNamespaceDictionary (dictionary): map of the control rig namespaces to the FBCharacters they belong
                to. Each key is the namespace and the value is the FBCharacter Name
        """
        self._rootComponent = component
        self._rootElement = xml.Element(rootElementName)
        self._characterDictionary = characterNamespaceDictionary or {}

    def Serialize(self):
        """ Store the FBComponent hierarchy and the property of each of its components into an xml file """
        self._rootElement.append(self.SerializeComponent(self._rootComponent))

    @staticmethod
    def isDataAccessible(fbproperty):
        """
        Is the data for the FBProperty accessible is accessible by the Data variable. If it isn't accessible it is
        because the FBProperty is a method, list or a property of the object it belongs to and must be accessed by
        those means.

        Arguments:
            fbproperty (pyfbsdk.FBProperty): Property to check if the data is accessible by the Data variable.

        Return:
            boolean
        """
        try:
            fbproperty.Data

        except NotImplementedError:
            return False
        return True

    @staticmethod
    def ConvertDataToString(fbproperty):
        """
        Checks if the property's data exists and returns as a string if it does.
        Also filters out any non-printable characters from the string, so it can be read in an xml.

        Arguments:
            fbproperty (mobu.FBProperty): property to get data from

        Returns:
            String: Property Data as a string
        """

        if hasattr(fbproperty, "__call__"):
            propDataString = str(fbproperty())

        else:
            propDataString = str(fbproperty.Data)

        if "(" in propDataString:
            propDataString = propDataString[propDataString.find("("):]

        # remove any non printable characters
        # (some properties added an unusual character which was crashing the script)

        propDataString = re.sub(r'[^a-zA-Z0-9_()/\\\-\[\]:\^.,]', '', propDataString)
        return propDataString

    def BuildElement(self, component):
        """
        Creates an xml element out of the given component
        Arguments:
            component (pyfbsdk.FBComponent): component to store in an xml format

        Return:
            xml.etree.cElementTree.Element
        """
        element = xml.Element(component.__class__.__name__[2:])
        element.attrib = {fbproperty.Name.replace(" ", "_"): self.ConvertDataToString(fbproperty)
                          for fbproperty in component.PropertyList if self.isDataAccessible(fbproperty)}

        element.attrib["Name"] = self.BuildNameString(component.LongName)

        if component.__class__ in [mobu.FBConstraint, mobu.FBConstraintRelation]:
            element.text = component.Description

            referenceAddString = ""
            counter = 0
            while component.ReferenceGetCount(counter):
                _component = component.ReferenceGet(counter)
                referenceAddString = "{}({}, {})||".format(referenceAddString, counter,
                                                           self.BuildNameString(_component.LongName,
                                                                                includeNamespaceCharacter=True))
                counter += 1
            if referenceAddString:
                element.attrib["ReferenceAdd"] = referenceAddString[:-2]

        return element

    def BuildNameString(self, name, includeNamespaceCharacter=False):
        """
        Converts the string into a format that the RS.Core.AssetSetup.Parser recognizes that takes into account
        namespaces

        Arguments:
            name (string): name to edit
            includeNamespaceCharacter (boolean): include $ prefix

        Return:
            string
        """
        if ":" in name and self._characterDictionary.get(name.split(":")[0], None):
            namespace, name = name.split(":", 1)
            name = "$[{}]{}".format(self._characterDictionary.get(namespace), name)

        if includeNamespaceCharacter and not name.startswith("$"):
            name = "${}".format(name)
        return name

    def SerializeComponent(self, component, excludeByType=[], maxDepth=None, depth=0):
        """
        Create elements, from the 'component' argument, for an xml:
        character, components, materials, shader, constraints

        Assign the item's property data as attributes

        Arguments:
            component (FBComponent): the FBModel null to recurse over
            parentElement (xml.etree.cElementTree.Element): the child of the previous component

        Returns:
            xml.etree.cElementTree.Element
        """
        # check a valid component has been passed
        if not component:
            return

        element = self.BuildElement(component)
        # create root element, which will be the character model
        constraints = [component.GetDst(index)
                       for index in xrange(component.GetDstCount())
                       if component.GetDst(index).__class__ == mobu.FBConstraint]

        for child in Scene.GetAllChildren(component, onlyTopChildren=True) + constraints:
            if maxDepth is not None and depth + 1 > maxDepth:
                continue

            if child.__class__ not in excludeByType:
                element.append(self.SerializeComponent(component=child, excludeByType=excludeByType, maxDepth=maxDepth,
                                                       depth=depth + 1))

        return element

    def Write(self, path):
        """
        Writes out an XML using the dict generated from '_recursiveBuildOfRig'
        """
        if not self._rootElement:
            return

        # create xml
        tree = xml.ElementTree(self._rootElement)
        tree.write(path)

        # make purdy
        xmlDoc = minidom.parse(path)
        with open(path, "wb") as fileHandle:
            fileHandle.write(xmlDoc.toprettyxml())