###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: Toybox.py
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Creates a Toybox for use with BoxStar
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Create Toybox
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run(showMessage=True):
    lToybox = FBModelCube("ToyBox")
    lToybox.Show = True
    lToybox.Scaling = FBVector3d(100,100,100)
    
    #Cube Material
    lToyboxMat = FBMaterial("ToyBox_Material")
    lToyboxMat.Diffuse = FBColor(1,0,1)
    lToyboxMat.Opacity = 0.50
    lToybox.Materials.append(lToyboxMat)
    
    #Cube Shader
    lToyboxShader = FBShaderManager().CreateShader("FlatShader")
    lToyboxShader.Name = "ToyBox_Shader"
    lToyboxShader.PropertyList.Find("Transparency Type").Data = 1
    lToybox.Shaders.append(lToyboxShader)
    lToybox.ShadingMode = FBModelShadingMode.kFBModelShadingAll
    
    #Handle
    lToyboxHandle = FBHandle("ToyBox_Handle")
    lToyboxHandle.Follow.append(lToybox)
    lToyboxHandle.Manipulate.append(lToybox)
    lToyboxHandle.PropertyList.Find("2D Display Color").Data = FBColor(1,0,1)
    lToyboxHandle.PropertyList.Find("Translation Offset").Data = FBVector3d(0,150,0)
    lToyboxHandle.PropertyList.Find("Size").Data = 40
    lToyboxHandle.PropertyList.Find("Label").Data = "ToyBox"
    
    #Group
    lToyboxGroup = FBGroup("ToyBox")
    lToyboxGroup.ConnectSrc(lToybox)
    lToyboxGroup.Show = True
    lToyboxGroup.Pickable = True
    lToyboxGroup.Transformable = True

    if showMessage:
        FBMessageBox("Toybox","Toybox Created.", "Ok")
