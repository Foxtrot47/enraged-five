###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_AssetSetupAnimals
## Written And Maintained By: Kathryn Bodey
## Contributors: Kristine Middlemiss
## Description: Function to set up animals - avian, aquatic & quadruped
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Core.AssetSetup.Lib as ass
import RS.Globals as glo
import RS.Utils.Creation as cre
import RS.Utils.Path
import RS.Utils.Scene
import RS.Core.AssetSetup.Characters as cha
import RS.Core.Character.IngameCharacterIKControls as ikcon
import os

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Align pModel node to pAlignTo node. pass in true or false for which trans/rot axis required.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################


def AlignBones( pModel, pAlignTo, pAlignTransX, pAlignTransY, pAlignTransZ, pAlignRotX, pAlignRotY, pAlignRotZ):

    lAlignTransPos = FBVector3d();
    lModelTransPos = FBVector3d();
    lAlignRotPos = FBVector3d();
    lModelRotPos = FBVector3d();   

    pAlignTo.GetVector(lAlignTransPos)
    pModel.GetVector(lModelTransPos)

    pAlignTo.GetVector(lAlignRotPos, FBModelTransformationType.kModelRotation )
    pModel.GetVector(lModelRotPos, FBModelTransformationType.kModelRotation)

    if pAlignTransX:
        lModelTransPos[0] = lAlignTransPos[0]
    if pAlignTransY:
        lModelTransPos[1] = lAlignTransPos[1]
    if pAlignTransZ:
        lModelTransPos[2] = lAlignTransPos[2]

    if pAlignRotX:
        lModelRotPos[0] = lAlignRotPos[0]
    if pAlignRotY:
        lModelRotPos[1] = lAlignRotPos[1]
    if pAlignRotZ:
        lModelRotPos[2] = lAlignRotPos[2]        

    pModel.SetVector(lModelTransPos)
    pModel.SetVector(lModelRotPos, FBModelTransformationType.kModelRotation)    

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Animal Prep Script - to be use in each animal category Setup Script
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimalSetupPrep():

    lDeleteArray = [] 

    lLODParent = None
    lLODParent2 = None

    for iComponent in glo.gComponents:
        if iComponent.LongName.startswith("Marker"):
            lDeleteArray.append(iComponent)                 

    for iComponent in glo.gComponents:
        if '-l2' in iComponent.Name.lower():  
            lLODParent2 = iComponent
            break

        if '-l' in iComponent.Name.lower() and '-l2' not in iComponent.Name.lower():  
            lLODParent = iComponent
            break

    if lLODParent != None:         
        RS.Utils.Scene.GetChildren(lLODParent, lDeleteArray)
        #lDeleteArray.extend(eva.gChildList)
        #del eva.gChildList[:]

    if lLODParent2 != None:         
        RS.Utils.Scene.GetChildren(lLODParent2, lDeleteArray)
        #lDeleteArray.extend(eva.gChildList)
        #del eva.gChildList[:]

    for iDelete in lDeleteArray:
        iDelete.FBDelete()
        FBSystem().Scene.Evaluate()

    FBSystem().CurrentTake.ClearAllProperties(False)

    FBPlayerControl().LoopStart = FBTime(0,0,0,0)

    ass.rs_AssetSetupDeleteItems()

    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]

    lMover.Selected = True
    lMover.ShadingMode = FBModelShadingMode.kFBModelShadingWire
    lMover.Selected = False
    lMover.PropertyList.Find("Visibility").Data = True

    lDummy.PropertyList.Find("Pre Rotation").Data = FBVector3d(0,0,0)
    FBDisconnect(lMover, lDummy)
    lDummy.Rotation = FBVector3d(-90,0,0)
    FBConnect(lMover, lDummy)
    FBSystem().Scene.Evaluate()

    ass.rs_AssetSetupMoverControl()

    FBSystem().Scene.Evaluate()

    lGeo.Parent = lDummy    

    FBSystem().Scene.Evaluate()

    ass.rs_CreateToyBoxMarker()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Create Folders & Groups
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimalGroupSetup():

    lJointList = []
    lControlList = []
    lRigList = []
    lGeoList = []
    lAmbientFaceList = []

    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]
    lSkelRoot = FBFindModelByLabelName('SKEL_ROOT')

    for iComponent in glo.gComponents:

        if 'ctrlrig' in iComponent.Name.lower():
            lRigList.append(iComponent)  

        if 'ambient_ui' in iComponent.Name.lower(): 
            RS.Utils.Scene.GetChildren(iComponent, lAmbientFaceList)
            #lAmbientFaceList.extend(eva.gChildList)        

    lCtrlList = []
    lFrameList = [] 

    if len(lAmbientFaceList) > 0:

        for iFace in lAmbientFaceList:           

            if "CTRL" in iFace.Name :
                lCtrlList.append(iFace)
            elif "TEXT" in iFace.Name :
                lFrameList.append(iFace)
            elif "RECT" in iFace.Name :
                lFrameList.append(iFace)
            elif "Ambient_UI" in iFace.Name :
                lFrameList.append(iFace)
            elif "FaceFX" in iFace.Name :
                lFrameList.append(iFace)
            else :
                lCtrlList.append(iFace)  

    lFaceCTRLGroup = FBGroup("Amb_Controls")
    lFaceFrameGroup = FBGroup("Amb_Frames") 

    if len(lCtrlList) > 0:
        for iCTRL in lCtrlList:
            lFaceCTRLGroup.ConnectSrc(iCTRL)

    if len(lFrameList) > 0:       
        for iFrame in lFrameList:
            lFaceFrameGroup.ConnectSrc(iFrame)

    RS.Utils.Scene.GetChildren(lSkelRoot, lJointList)
    #lJointList.extend(eva.gChildList)
    #del eva.gChildList[:] 

    RS.Utils.Scene.GetChildren(lGeo, lGeoList)
    #lGeoList.extend(eva.gChildList)
    #del eva.gChildList[:]

    lMainGroup = FBGroup(lGeo.Name)
    lFaceGroup = FBGroup("Ambient_Facial")
    lDummyGroup = FBGroup("Dummy01")
    lMoverGroup = FBGroup("mover")
    lJointGroup = FBGroup("Joints")
    lGeoGroup = FBGroup("Geometry")
    lRigGroup = FBGroup("CTRLRIG")

    lDummyGroup.ConnectSrc(lDummy)
    lMoverGroup.ConnectSrc(lMover)
    if lGeoList >= 1:
        for iGeo in lGeoList:
            lGeoGroup.ConnectSrc(iGeo)
    if lJointList >= 1:
        for iJoint in lJointList:
            lJointGroup.ConnectSrc(iJoint)
    if lControlList >= 1:
        for iControl in lControlList:
            lControlGroup.ConnectSrc(iControl)
    if lRigList >= 1:
        for iRig in lRigList:
            lRigGroup.ConnectSrc(iRig)

    lFaceGroup.ConnectSrc(lFaceCTRLGroup)
    lFaceGroup.ConnectSrc(lFaceFrameGroup)    

    lMainGroup.ConnectSrc(lDummyGroup)
    lMainGroup.ConnectSrc(lMoverGroup)
    lMainGroup.ConnectSrc(lJointGroup)
    lMainGroup.ConnectSrc(lRigGroup)
    lMainGroup.ConnectSrc(lGeoGroup)
    lMainGroup.ConnectSrc(lFaceGroup)

    lFaceCTRLGroup.Show = False
    lFaceCTRLGroup.Pickable = True
    lFaceCTRLGroup.Transformable = True

    lFaceFrameGroup.Show = False
    lFaceFrameGroup.Pickable = False
    lFaceFrameGroup.Transformable = True

    lDummyGroup.Show = False
    lDummyGroup.Pickable = False
    lDummyGroup.Transformable = False
    lGeoGroup.Pickable = False
    lGeoGroup.Transformable = False 
    lMoverGroup.Show = False

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Constrain Pelvis to SkelRoot
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SkelPelvisConstraintAnimalSetup():

    lPelvisSkel = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[18])
    lSkelRootSkel = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[10])

    lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)

    #glo.gConstraints.append(lParentChildConstraint)
    lParentChildConstraint.Name = ("SkelRoot_Pelvis")
    lParentChildConstraint.ReferenceAdd (0,lSkelRootSkel)
    lParentChildConstraint.ReferenceAdd (1,lPelvisSkel)
    lParentChildConstraint.Snap()  

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Aquatic:  shark, fish
##                                     - both of these models share identical skeletons
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AnimalJointRigConstraints():

    lSkelRoot = glo.gAnimalSkelArray[10]
    lPelvisRoot = glo.gAnimalSkelArray[17]
    lPelvis = glo.gAnimalSkelArray[18]
    lPelvis1 = glo.gAnimalSkelArray[19]
    lSpine_Root = glo.gAnimalSkelArray[11]
    lSpine0 = glo.gAnimalSkelArray[12]
    lSpine1 = glo.gAnimalSkelArray[13]
    lSpine2 = glo.gAnimalSkelArray[14]
    lSpine3 = glo.gAnimalSkelArray[16]
    lNeck1 = glo.gAnimalSkelArray[0]
    lNeck2 = glo.gAnimalSkelArray[1]   
    lHead = glo.gAnimalSkelArray[2]
    lHeadNUB = glo.gAnimalSkelArray[3]
    lAntlerRoot = glo.gAnimalSkelArray[8]
    lAntlerRootNUB = glo.gAnimalSkelArray[9] 
    lLThigh = glo.gAnimalSkelArray[42]
    lLCalf = glo.gAnimalSkelArray[44]
    lLFoot = glo.gAnimalSkelArray[45]
    lLToe0 = glo.gAnimalSkelArray[46]
    lLToe0NUB = (glo.gAnimalSkelArray[47])
    lLToe1 = glo.gAnimalSkelArray[48]  
    lLToe1NUB = (glo.gAnimalSkelArray[49])  
    lRThigh = glo.gAnimalSkelArray[50]
    lRCalf = glo.gAnimalSkelArray[52]
    lRFoot = glo.gAnimalSkelArray[53]
    lRToe0 = glo.gAnimalSkelArray[54]
    lRToe0NUB = glo.gAnimalSkelArray[55]
    lRToe1 = glo.gAnimalSkelArray[56]
    lRToe1NUB = (glo.gAnimalSkelArray[57])  
    lLClavicle = glo.gAnimalSkelArray[20]
    lLUpperArm = glo.gAnimalSkelArray[22]
    lLForeArm = glo.gAnimalSkelArray[23]
    lLHand = glo.gAnimalSkelArray[24]
    lLFinger00 = glo.gAnimalSkelArray[25]
    lLFinger00NUB = glo.gAnimalSkelArray[26]
    lLFinger01 = glo.gAnimalSkelArray[27]
    lLFinger01NUB = glo.gAnimalSkelArray[28] 
    lRClavicle = glo.gAnimalSkelArray[31]
    lRUpperArm = glo.gAnimalSkelArray[33]
    lRForeArm = glo.gAnimalSkelArray[34]
    lRHand = glo.gAnimalSkelArray[35]
    lRFinger00 = glo.gAnimalSkelArray[36]
    lRFinger00NUB = glo.gAnimalSkelArray[37]   
    lRFinger01 = glo.gAnimalSkelArray[38]
    lRFinger01NUB = glo.gAnimalSkelArray[39] 
    lTail01 = glo.gAnimalSkelArray[58]
    lTail02 = glo.gAnimalSkelArray[59]
    lTail02NUB = glo.gAnimalSkelArray[60]
    lTailM01 = glo.gAnimalSkelArray[61]
    lTailM02 = glo.gAnimalSkelArray[62]
    lTailM03 = glo.gAnimalSkelArray[63]
    lTailM03NUB = glo.gAnimalSkelArray[64]    


    lJointList = [lSkelRoot,lPelvis,lPelvis1,lPelvisRoot,lLThigh,lLCalf,lLFoot,lLToe0,lLToe0NUB,lLToe1,lRThigh,lRCalf,lRFoot,lRToe0,lRToe0NUB,
                  lRToe1,lTail01,lTail02,lTail02NUB,lTailM01,lTailM02,lTailM03,lTailM03NUB,lSpine_Root,lSpine0,lSpine1,lSpine2,lSpine3,lNeck1,
                  lNeck2,lHead,lHeadNUB,lLClavicle,lLUpperArm,lLForeArm,lLHand,lLFinger00,lLFinger01,lLFinger00NUB,lRClavicle,lRUpperArm,
                  lRForeArm,lRHand,lRFinger00,lRFinger01,lRFinger00NUB,lAntlerRoot,lAntlerRootNUB,lLToe1NUB,lRToe1NUB,lLFinger01NUB]

    for iJoint in lJointList:
        lSrc = RS.Utils.Scene.FindModelByName(glo.gAnimalPrefixCtrlRig + iJoint)
        lDst = RS.Utils.Scene.FindModelByName(glo.gAnimalPrefixSkel + iJoint)
        if lSrc and lDst:                    
            RS.Utils.Scene.CreateParentConstraint(lSrc, lDst, False, True, True) 
        else :
            None


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Animal Custom Tag
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CustomTagsAnimalSetup():

    lDummy = ass.rs_AssetSetupVariables()[0]

    lTypeTag = lDummy.PropertyCreate('Asset_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTypeTag.Data = "Animal"   

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Create tail Character Extensions
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CreateTailCharacterExtension(lCharacter):
    #--- Create a Character Extension
    lTailExtension = FBCharacterExtension("Char_Extension_Tail")
    lCharacter.AddCharacterExtension(lTailExtension)
    #now add the joints in
    lTailJointArray = []
    for obj in glo.gComponents:
        thisName = obj.Name
        lThisNameLower = thisName.lower()
        lSubstringName = lThisNameLower[0:9]
        thisNameLength = len(thisName)
        if lSubstringName == "skel_tail":
##            #print "Found "+obj.Name
            if lThisNameLower[(thisNameLength - 3):thisNameLength] != "nub":
                #print "Appending "+obj.Name+" cos its NOT a nub!"
                lTailJointArray.append(obj)       
            else:
                print "Skipping adding "+obj.Name+" to "+lTailExtension.Name+" cos its a nub!"

    for tailJoint in lTailJointArray:
        FBConnect(tailJoint,lTailExtension)
        ##by default, translation properties get added so we need to remove these
        tProp = lTailExtension.PropertyList.Find(tailJoint.Name+".Lcl Translation")
        if tProp:
            lTailExtension.PropertyRemove(tProp)
        lTailExtension.PropertyList.Find("MirrorLabel").Data = 1 # this sets the Mirror Partner property to 'Self'

    lTailExtension.UpdateStancePose()  

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Character Extensions
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CharacterExtension(pCharacter, pIncludeTail):

    lMover = FBFindModelByLabelName('mover')
    lMoverControl = FBFindModelByLabelName('mover_Control')

    lCharacterExt = FBCharacterExtension("Char_Extention")
    lCharacterExt.Label = "mover_extension"
    pCharacter.AddCharacterExtension(lCharacterExt)
    FBConnect(lMover,lCharacterExt)
    FBConnect(lMoverControl,lCharacterExt)

    ## this is a hack for the new setups
    ##gonna not just do tail gonna put the skel root in here too
    # dont wanna do it globally cvos it'll fuck the old ones
    if pIncludeTail == True:
        for iTail in glo.gAnimalSkelTailArray:
            iTailNull = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + iTail)
            if iTailNull:
                FBConnect(iTailNull,lCharacterExt)
    else:
        SkelRootJoint = FBFindModelByLabelName("SKEL_ROOT")
        FBConnect(SkelRootJoint,lCharacterExt)
        rs_CreateTailCharacterExtension(pCharacter)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Aquatic:  shark, fish
##                                     - both of these models share identical skeletons
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AquaticAnimalSetup(pControl, pEvent):

    rs_AnimalSetupPrep()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDeleteArray = []

    for iComponent in glo.gComponents:
        if iComponent.LongName.startswith("CTRLRIG"):
            lDeleteArray.append(iComponent)

    for iDelete in lDeleteArray:
        iDelete.FBDelete()
        FBSystem().Scene.Evaluate()

    lSkelRoot = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[10])
    lSpineRoot = RS.Utils.Scene.FindModelByName('SKEL_Spine_Root')
    lSpine1 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[12])
    lSpine2 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[13])
    lTail1 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[58])
    lTail2 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[59])
    lTail3 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[60])

    # Yellow CTRL RIG 
    lRootRig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[10])
    lSpineRootRig = None
    if lSpineRoot:
        lSpineRootRig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + lSpineRoot.Name)
    lSkelSpine1Rig = None
    if lSpine1:
        lSkelSpine1Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12])
    lSkelSpine2Rig = None
    if lSpine2:
        lSkelSpine2Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
    lSkelTail1Rig = None
    if lTail1:
        lSkelTail1Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[58])
    lSkelTail2Rig = None
    if lTail2:
        lSkelTail2Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[59])
    lSkelTail3Rig = None
    if lTail3:
        lSkelTail3Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[60])

    # Red Helper Markers
    lRootControl = FBModelMarker(lSkelRoot.Name + '_Control')
    lSpineRootControl = None
    if lSpineRoot:
        lSpineRootControl = FBModelMarker(lSpineRoot.Name + '_Control')
    lSkelSpine1Control = None
    if lSpine1:
        lSkelSpine1Control = FBModelMarker(lSpine1.Name + '_Control')
    lSkelSpine2Control = None
    if lSpine2:
        lSkelSpine2Control = FBModelMarker(lSpine2.Name + '_Control')
    lSkelTail1Control = None
    if lTail1:
        lSkelTail1Control = FBModelMarker(lTail1.Name + '_Control')
    lSkelTail2Control = None
    if lTail1:
        lSkelTail2Control = FBModelMarker(lTail2.Name + '_Control')
    lSkelTail3Control = None
    if lTail1:
        lSkelTail3Control = FBModelMarker(lTail3.Name + '_Control')

    lSkelList = [lSkelRoot, lSpineRoot, lSpine2, lSpine1, lTail1, lTail2, lTail3]
    lRigList = [lRootRig, lSpineRootRig, lSkelSpine1Rig, lSkelSpine2Rig, lSkelTail1Rig, lSkelTail2Rig, lSkelTail3Rig]
    lControllerList = [lRootControl, lSpineRootControl, lSkelSpine1Control, lSkelSpine2Control, lSkelTail1Control, lSkelTail2Control, lSkelTail3Control]

    for i in range(len(lSkelList)):
        if lSkelList[i]:

            lControllerList[i].Show = True
            lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere
            lRigList[i].Show = True
            lControllerList[i].Show = True
            lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere

            RS.Utils.Scene.GetSetVector(lSkelList[i], lRigList[i], True)
            FBSystem().Scene.Evaluate()
            RS.Utils.Scene.GetSetVector(lRigList[i], lControllerList[i], True)
            FBSystem().Scene.Evaluate()

            lControlTrns = lControllerList[i].Translation
            lControllerList[i].Translation = FBVector3d(lControlTrns[0],lControlTrns[1] + 37.23, lControlTrns[2])
            FBSystem().Scene.Evaluate()

            lTag = lRigList[i].PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"

            FBSystem().Scene.Evaluate()

    if lSkelTail1Rig and lSkelTail2Rig and lSkelTail3Rig:
        lSkelTail3Rig.Parent = lSkelTail2Rig
        lSkelTail2Rig.Parent = lSkelTail1Rig
        lSkelTail1Rig.Parent = lSkelSpine2Rig
    if lSkelSpine2Rig:
        lSkelSpine2Rig.Parent = lSkelSpine1Rig
    if lSkelSpine1Rig:
        lSkelSpine1Rig.Parent = lRootRig

    if lSkelTail1Control and lSkelTail2Control and lSkelTail3Control:
        lSkelTail3Control.Parent = lSkelTail2Control
        lSkelTail2Control.Parent = lSkelTail1Control
        lSkelTail1Control.Parent = lSkelSpine2Control
    if lSkelSpine2Control:
        lSkelSpine2Control.Parent = lSkelSpine1Control
    if lSkelSpine1Control:
        lSkelSpine1Control.Parent = lRootControl

    for i in range(len(lControllerList)):
        if lControllerList[i]:
            lVector = FBVector3d()
            lVector2 = FBVector3d()
            lSkelTrns = lSkelList[i].GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
            lConTrns = lControllerList[i].GetVector(lVector2, FBModelTransformationType.kModelTranslation, True)
            x = float(lVector[0] - lVector2[0])
            y = float(lVector[1] - lVector2[1])
            z = float(lVector[2] - lVector2[2])
            lControllerList[i].PropertyList.Find("Scaling Pivot (Auto Offset)").Data = FBVector3d(x, y, z)
            lControllerList[i].PropertyList.Find("Rotation Pivot (Auto Offset)").Data = FBVector3d(x, y, z)
            FBSystem().Scene.Evaluate()

    for i in range(len(lRigList)):
        if lRigList[i]:
            lParentChildConstraint2 = FBConstraintManager().TypeCreateConstraint(3)
            lParentChildConstraint2.Name = lSkelList[i].Name + "_" + lRigList[i].Name
            lParentChildConstraint2.ReferenceAdd (0,lSkelList[i])
            lParentChildConstraint2.ReferenceAdd (1,lRigList[i])
            FBSystem().Scene.Evaluate()
            lParentChildConstraint2.Snap()

            lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
            lParentChildConstraint.Name = lRigList[i].Name + "_" + lControllerList[i].Name
            lParentChildConstraint.ReferenceAdd (0,lRigList[i])
            lParentChildConstraint.ReferenceAdd (1,lControllerList[i])
            lParentChildConstraint.Snap()

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    ass.rs_AssetSetupCustomProperties()

    rs_CustomTagsAnimalSetup()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 

#rs_AquaticAnimalSetup(None, None)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Aquatic:  Dolphin
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_DolphinAnimalSetup(pControl, pEvent):
##def rs_AquaticAnimalSetup(pControl, pEvent):    

    rs_AnimalSetupPrep()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDeleteArray = []

    for iComponent in glo.gComponents:
        if iComponent.LongName.startswith("CTRLRIG"):
            lDeleteArray.append(iComponent)

    for iDelete in lDeleteArray:
        iDelete.FBDelete()
        FBSystem().Scene.Evaluate()

    lSkelRoot = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[10])
    lSpine1 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[12]) #tail0
    if lSpine1 != None:
        print "lSpine1 set to "+lSpine1.Name
    if lSpine1 == None: #this is for the dolphin
        lSpine1 = RS.Utils.Scene.FindModelByName(glo.gAnimalPrefixSkel +"_Tail0") #tail0
        print "lSpine1 set to "+lSpine1.Name

    lSpine2 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[13]) #tail1
    if lSpine2 == None: #this is for the dolphin
        lSpine2 = RS.Utils.Scene.FindModelByName(glo.gAnimalPrefixSkel +"_Tail1") #tail1
        print "lSpine2 set to "+lSpine2.Name


    lTail1 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[58])
    print (glo.gAnimalPrefixSkel +"_Tail0")
    if lTail1 == None: #this is for the dolphin
        lTail1 = RS.Utils.Scene.FindModelByName(glo.gAnimalPrefixSkel +"_Tail2") #tail2
        print "Tail1 set to "+lTail1.Name

    lTail2 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[59])
    if lTail2 == None: #this is for the dolphin
        lTail2 = FBFindModelByLabelName(glo.gAnimalPrefixSkel +"_Tail3")    #tail3
        print "Tail2 set to "+lTail2.Name        

    lTail3 = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[60])
    if lTail3 == None: #this is for the dolphin
        lTail3 = FBFindModelByLabelName(glo.gAnimalPrefixSkel +"_Tail4" )   #tail4
        print "Tail3 set to "+lTail3.Name

    # Yellow CTRL RIG 
##    lRootRig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[10])
##    lSkelSpine1Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12])
##    lSkelSpine2Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
##    lSkelTail1Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[58])
##    lSkelTail2Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[59])
##    lSkelTail3Rig = FBModelSkeleton(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[60])
    lRootRig = lSkelRoot
    lSkelSpine1Rig = lSpine1
    lSkelSpine2Rig = lSpine2
    lSkelTail1Rig = lTail1
    lSkelTail2Rig = lTail2
    lSkelTail3Rig = lTail3

    # Red Helper Markers
    lRootControl = FBModelMarker(lSkelRoot.Name + '_Control')
    lSkelSpine1Control = FBModelMarker(lSpine1.Name + '_Control')
    lSkelSpine2Control = FBModelMarker(lSpine2.Name + '_Control')
    lSkelTail1Control = FBModelMarker(lTail1.Name + '_Control')
    lSkelTail2Control = FBModelMarker(lTail2.Name + '_Control')
    lSkelTail3Control = FBModelMarker(lTail3.Name + '_Control')

    lSkelList = [lSkelRoot, lSpine2, lSpine1, lTail1, lTail2, lTail3]
    lRigList = [lRootRig, lSkelSpine1Rig, lSkelSpine2Rig, lSkelTail1Rig, lSkelTail2Rig, lSkelTail3Rig]
    lControllerList = [lRootControl, lSkelSpine1Control, lSkelSpine2Control, lSkelTail1Control, lSkelTail2Control, lSkelTail3Control]

    for i in range(len(lSkelList)):

        lControllerList[i].Show = True
        lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere
        lRigList[i].Show = True
        lControllerList[i].Show = True
        lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere

        RS.Utils.Scene.GetSetVector(lSkelList[i], lRigList[i], True)
        FBSystem().Scene.Evaluate()
        RS.Utils.Scene.GetSetVector(lRigList[i], lControllerList[i], True)   
        FBSystem().Scene.Evaluate()     

        lControlTrns = lControllerList[i].Translation
        lControllerList[i].Translation = FBVector3d(lControlTrns[0],lControlTrns[1] + 37.23, lControlTrns[2])
        FBSystem().Scene.Evaluate()

        lTag = lRigList[i].PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Contacts"  

        FBSystem().Scene.Evaluate()

    lSkelTail3Rig.Parent = lSkelTail2Rig
    lSkelTail2Rig.Parent = lSkelTail1Rig   
    lSkelTail1Rig.Parent = lSkelSpine2Rig 
    lSkelSpine2Rig.Parent = lSkelSpine1Rig
    lSkelSpine1Rig.Parent = lRootRig

    lSkelTail3Control.Parent = lSkelTail2Control
    lSkelTail2Control.Parent = lSkelTail1Control
    lSkelTail1Control.Parent = lSkelSpine2Control     
    lSkelSpine2Control.Parent = lSkelSpine1Control
    lSkelSpine1Control.Parent = lRootControl

    for i in range(len(lControllerList)):
        lVector = FBVector3d()
        lVector2 = FBVector3d()
        lSkelTrns = lSkelList[i].GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        lConTrns = lControllerList[i].GetVector(lVector2, FBModelTransformationType.kModelTranslation, True)
        x = float(lVector[0] - lVector2[0])
        y = float(lVector[1] - lVector2[1])
        z = float(lVector[2] - lVector2[2])
        lControllerList[i].PropertyList.Find("Scaling Pivot (Auto Offset)").Data = FBVector3d(x, y, z)
        lControllerList[i].PropertyList.Find("Rotation Pivot (Auto Offset)").Data = FBVector3d(x, y, z)
        FBSystem().Scene.Evaluate()

    for i in range(len(lRigList)):

##        if lSkelList[i].Name != lRigList[i].Name:
##            lParentChildConstraint2 = FBConstraintManager().TypeCreateConstraint(3)
##            lParentChildConstraint2.Name = lSkelList[i].Name + "_" + lRigList[i].Name
##            #FBSystem().Scene.Constraints.append(lParentChildConstraint2)
##            lParentChildConstraint2.ReferenceAdd (0,lSkelList[i])
##            lParentChildConstraint2.ReferenceAdd (1,lRigList[i])
##            FBSystem().Scene.Evaluate()
##            lParentChildConstraint2.Snap()

        if lRigList[i].Name != lControllerList[i].Name:
            lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
            lParentChildConstraint.Name = lRigList[i].Name + "_" + lControllerList[i].Name
            #FBSystem().Scene.Constraints.append(lParentChildConstraint)
            lParentChildConstraint.ReferenceAdd (0,lRigList[i])
            lParentChildConstraint.ReferenceAdd (1,lControllerList[i])
            lParentChildConstraint.Snap()        

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    ass.rs_AssetSetupCustomProperties()

    rs_CustomTagsAnimalSetup()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Avian:  pigeon, seagull, hen
##                                  - pigeon and seagull share identical skeletons. hen is slightly different.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AvianAnimalSetup(pControl, pEvent):

    rs_AnimalSetupPrep()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    FBSystem().Scene.Evaluate()

    lScene = FBSystem().Scene

    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]

    lPelvisRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18])
    lPelvisAUXRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18] + "_AUX")
    lLThighRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[42])
    lLCalfRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[44])
    lLFootRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[45])
    lRThighRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[50])
    lRCalfRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[52])
    lRFootRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[53])
    lSpineRootRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[11])
    lSpine0Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12]) 
    lLHandRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[24])
    lRHandRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[35])
    lLClavicleRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[20])
    lRClavicleRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[31])
    lLUpperarmRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[22])
    lRUpperarmRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[33])
    lLForearmRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[23])
    lRForearmRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[34])
    lHeadRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[2])
    lLToe0Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[46])
    lLToe0NUB = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[47])
    lRToe0Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[54])
    lRToe0NUB = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[45])
    lLFinger00Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[25]) 
    lLFinger00NUBRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[26]) 
    lRFinger00Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[36])
    lRFinger00NUBRig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[37])
    lNeck1Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[0])
    lSpine1Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
    lSpine2Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[14])
    lSpine3Rig = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])


    if lGeo.Name != 'A_C_Hen': #A_C_Pigeon' or lGeo.Name == 'A_C_Seagull':

        def rs_AssignAvianJoints():
            lBoneAssignment =   {'Hips':lPelvisAUXRig,
                                 'LeftUpLeg':lLThighRig,
                                 'LeftLeg':lLCalfRig,
                                 'LeftFoot':lLFootRig,
                                 'RightUpLeg':lRThighRig,
                                 'RightLeg':lRCalfRig,
                                 'RightFoot':lRFootRig,
                                 'Spine':lSpineRootRig,
                                 'LeftHand':lLHandRig,
                                 'RightHand':lRHandRig,
                                 'LeftShoulder':lLClavicleRig,
                                 'RightShoulder':lRClavicleRig,
                                 'LeftArm':lLUpperarmRig,
                                 'RightArm':lRUpperarmRig,
                                 'LeftForeArm':lLForearmRig,
                                 'RightForeArm':lRForearmRig,
                                 'Head':lHeadRig,
                                 'LeftToeBase':lLToe0Rig,
                                 'RightToeBase':lRToe0Rig,
                                 'LeftFingerBase':lLFinger00Rig,
                                 'RightFingerBase':lRFinger00Rig,
                                 'Neck':lNeck1Rig,
                                 'Spine1':lSpine0Rig,
                                 'Spine2':lSpine1Rig,
                                 'Spine3':lSpine2Rig,
                                 'Spine4':lSpine3Rig,}


            for iSlot, iBone in lBoneAssignment.iteritems():
                lBoneLink = (iSlot + "Link")
                lProperty = lCharacter.PropertyList.Find(lBoneLink)
                lProperty.append(iBone)

        lCharacter = FBCharacter(lGeo.Name)

        rs_AssignAvianJoints()

        lCharacter.SetCharacterizeOn(True)

        lCharacter.PropertyList.Find("CharacterSolverSelector").Data = 2

        rs_CharacterExtension(lCharacter, True)

        lCharacter.PropertyList.Find("Hands Contact Type").Data = 3
        lCharacter.PropertyList.Find("Feet Contact Type").Data = 3

        rs_AnimalJointRigConstraints()


    if lGeo.Name == 'A_C_Hen':    

        lLHandSkel = FBModelNull(glo.gAnimalPrefixCtrlRig + "_LeftHand_DONOTUSE")
        lRHandSkel = FBModelNull(glo.gAnimalPrefixCtrlRig + "_RightHand_DONOTUSE")
        lLForearmSkel = FBModelNull(glo.gAnimalPrefixCtrlRig + "_LeftForeArm_DONOTUSE")
        lRForearmSkel = FBModelNull(glo.gAnimalPrefixCtrlRig + "_RightForeArm_DONOTUSE")
        lPelvisSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18])
        lSkelRootSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[10])
        lLThighSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[42])
        lLCalfSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[44])
        lLFootSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[45])
        lRThighSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[50])
        lRCalfSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[52])
        lRFootSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[53])
        lSpine0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12]) 
        lLClavicleSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[20])
        lRClavicleSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[31])
        lHeadSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[2])
        lLToe0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[46])
        lRToe0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[54])
        lLToe0NUBSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[47])
        lRToe0NUBSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[55])
        lNeck1Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[0])
        lSpine1Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
        lSpine2Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[14])
        lSpine3Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])  

        RS.Utils.Scene.GetSetVector(lLClavicleSkel, lLHandSkel, True)
        RS.Utils.Scene.GetSetVector(lRClavicleSkel, lRHandSkel, True)
        lLHandSkel.Parent = lLClavicleSkel
        lRHandSkel.Parent = lRClavicleSkel
        lLHandSkel.PropertyList.Find('Lcl Translation').Data = FBVector3d(0.1,0,0)
        lRHandSkel.PropertyList.Find('Lcl Translation').Data = FBVector3d(0.1,0,0)

        RS.Utils.Scene.GetSetVector(lLHandSkel, lLForearmSkel, True)
        RS.Utils.Scene.GetSetVector(lRHandSkel, lRForearmSkel, True)
        lLForearmSkel.Parent = lLHandSkel
        lRForearmSkel.Parent = lRHandSkel
        lLForearmSkel.PropertyList.Find('Lcl Translation').Data = FBVector3d(0.125,0,0)
        lRForearmSkel.PropertyList.Find('Lcl Translation').Data = FBVector3d(0.125,0,0)

        def rs_AssignAvianJoints2():
            lBoneAssignment =   {'Hips':lPelvisSkel,
                                 'HipsTranslation':lSkelRootSkel,
                                 'LeftUpLeg':lLThighSkel,
                                 'LeftLeg':lLCalfSkel,
                                 'LeftFoot':lLFootSkel,
                                 'RightUpLeg':lRThighSkel,
                                 'RightLeg':lRCalfSkel,
                                 'RightFoot':lRFootSkel,
                                 'Spine':lSpine0Skel,
                                 'LeftHand':lLHandSkel,
                                 'RightHand':lRHandSkel,
                                 'LeftForeArm':lLForearmSkel,
                                 'RightForeArm':lRForearmSkel,
                                 'Head':lHeadSkel,
                                 'LeftToeBase':lLToe0Skel,
                                 'RightToeBase':lRToe0Skel,
                                 'LeftArm':lLClavicleSkel,
                                 'RightArm':lRClavicleSkel,                                
                                 'Neck':lNeck1Skel,
                                 'Spine1':lSpine1Skel,
                                 'Spine2':lSpine2Skel,
                                 'Spine3':lSpine3Skel,                                
                                 'LeftFootMiddle1':lLToe0NUBSkel,
                                 'RightFootMiddle1':lRToe0NUBSkel}                              

            for iSlot, iBone in lBoneAssignment.iteritems():
                lBoneLink = (iSlot + "Link")
                lProperty = lCharacter.PropertyList.Find(lBoneLink)
                lProperty.append(iBone)
                FBSystem().Scene.Evaluate()

        lCharacter = FBCharacter(lGeo.Name)

        rs_AssignAvianJoints2()

        lCharacter.SetCharacterizeOn(True)

        lCharacter.PropertyList.Find("CharacterSolverSelector").Data = 2

        rs_CharacterExtension(lCharacter, True)

        lCharacter.PropertyList.Find("Hands Contact Type").Data = 3
        lCharacter.PropertyList.Find("Feet Contact Type").Data = 3

        rs_AnimalJointRigConstraints()

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)  

    for iScene in lSceneList:
        if 'ctrlrig' in iScene.Name.lower():
            lTag = iScene.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"   

    FBSystem().Scene.Evaluate()  

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    ass.rs_AssetSetupCustomProperties()

    rs_CustomTagsAnimalSetup()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Quadrupeds: rottweiler, retriever, horse, boar, cow, coyote, lion, pig
##                         - boar & cow have Antler joints
##                         - rottweiler & retriever both use a gs CTRL RIG for characterisation (as they are mocapped)
##                           (the rest use SKEL)
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_QuadrupedAnimalSetup(pControl, pEvent):

    rs_AnimalSetupPrep()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName )

    lGeo = ass.rs_AssetSetupVariables()[2]

    if lGeo.Name not in glo.gHoovedQuadrupeds: 

        lCharacter = FBCharacter(lGeo.Name)

        lDeleteArray = []

        lgsCTRL = False 

        for iComponent in glo.gComponents:
            if iComponent.LongName.startswith("gs:CTRLRIG"):
                lgsCTRL = True
                break

        # bone assignments if gs:CTRLRIG is found (created in original max file 
        if lgsCTRL == True:

            for iComponent in glo.gComponents:
                if iComponent.LongName.startswith("CTRLRIG"):
                    lDeleteArray.append(iComponent)
                    FBSystem().Scene.Evaluate()

            for iDelete in lDeleteArray:
                iDelete.FBDelete()
                FBSystem().Scene.Evaluate()

            lPelvisAUXRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18] + '_AUX')
            lLThighRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[42])
            lLCalfRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[44])
            lLFootRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[45])
            lRThighRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[50])
            lRCalfRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[52])
            lRFootRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[53])
            lPelvisRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18])
            lPelvis1Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[19])
            lLUpperArmRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[22])
            lLForeArmRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[23])
            lLHandRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[24])
            lRUpperArmRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[33])
            lRForeArmRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[34])
            lRHandRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[35])
            lHeadRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[2])
            lLToe0Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[46])
            lRToe0Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[54])
            lLToe1Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[48])
            lRToe1Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[56])        
            lLClavicleRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[20])
            lRClavicleRig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[31])
            lNeck1Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[0])
            lLFinger00Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[25])
            lRFinger00Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[36])
            lLFinger01Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[27])
            lRFinger01Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[38])  
            lSpine0Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12])
            lSpine1Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
            lSpine2Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[14])
            lSpine3Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])
            lNeck2Rig = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[1])      


            lBoneAssignment = {'Hips':lPelvisAUXRig,
                               'LeftUpLeg':lLThighRig,
                               'LeftLeg':lLCalfRig,
                               'LeftFoot':lLFootRig,
                               'RightUpLeg':lRThighRig,
                               'RightLeg':lRCalfRig,
                               'RightFoot':lRFootRig,
                               'Spine':lPelvisRig,
                               'LeftArm':lLUpperArmRig,
                               'LeftForeArm':lLForeArmRig,
                               'LeftHand':lLHandRig,
                               'RightArm':lRUpperArmRig,
                               'RightForeArm':lRForeArmRig,
                               'RightHand':lRHandRig,
                               'Head':lHeadRig,
                               'LeftToeBase':lLToe0Rig,
                               'RightToeBase':lRToe0Rig,
                               'LeftShoulder':lLClavicleRig,
                               'RightShoulder':lRClavicleRig,
                               'Neck':lSpine2Rig,
                               'LeftFingerBase':lLFinger00Rig,
                               'RightFingerBase':lRFinger00Rig,
                               'Spine1':lPelvis1Rig,
                               'Spine2':lSpine0Rig,    
                               'Spine3':lSpine1Rig,
                               'Neck1':lSpine3Rig,
                               'Neck2':lNeck1Rig,
                               'Neck3':lNeck2Rig,
                               'RightFootIndex1':lRToe1Rig,
                               'LeftFootIndex1':lLToe1Rig,
                               'RightHandIndex1':lRFinger01Rig,
                               'LeftHandIndex1':lLFinger01Rig}

            for iSlot, iBone in lBoneAssignment.iteritems():
                if iBone:
                    print 'found!'
                    lBoneLink = (iSlot + "Link")
                    lProperty = lCharacter.PropertyList.Find(lBoneLink)
                    lProperty.append(iBone)
                    FBSystem().Scene.Evaluate()
                else:
                    print 'not found'

        # bone assignments if the gs:CTRLRIG doesnt exist	
        if lgsCTRL == False:
            lLThigh = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[42])
            lLCalf = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[44])
            lLFoot = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[45])
            lRThigh = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[50])
            lRCalf = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[52])
            lRFoot = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[53])
            lPelvis = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[18])
            lPelvis1 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[19])
            lLUpperArm = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[22])
            lLForeArm = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[23])
            lLHand = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[24])
            lRUpperArm = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[33])
            lRForeArm = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[34])
            lRHand = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[35])
            lHead = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[2])
            lLToe0 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[46])
            lRToe0 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[54])
            lLToe1 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[48])
            lRToe1 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[56])        
            lLClavicle = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[20])
            lRClavicle = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[31])
            lLFinger00 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[25])
            lRFinger00 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[36])
            lLFinger01 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[27])
            lRFinger01 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[38])  
            lSpine0 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[12])
            lSpine1 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[13])
            lSpine2 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[14])
            lSpine3 = FBFindModelByLabelName( glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[16])
            lNeck0 = RS.Utils.Scene.FindModelByName( "SKEL_Neck0" )
            lNeck1 = RS.Utils.Scene.FindModelByName( "SKEL_Neck1" )           
            lNeck2 = RS.Utils.Scene.FindModelByName( "SKEL_Neck2" )

            lBoneAssignment = {'Hips':lPelvis,
                               'LeftUpLeg':lLThigh,
                               'LeftLeg':lLCalf,
                               'LeftFoot':lLFoot,
                               'RightUpLeg':lRThigh,
                               'RightLeg':lRCalf,
                               'RightFoot':lRFoot,
                               'Spine':lSpine0,
                               'LeftArm':lLUpperArm,
                               'LeftForeArm':lLForeArm,
                               'LeftHand':lLHand,
                               'RightArm':lRUpperArm,
                               'RightForeArm':lRForeArm,
                               'RightHand':lRHand,
                               'Head':lHead,
                               'LeftToeBase':lLToe0,
                               'RightToeBase':lRToe0,
                               'LeftShoulder':lLClavicle,
                               'RightShoulder':lRClavicle,
                               'Neck':lNeck0,
                               'LeftFingerBase':lLFinger00,
                               'RightFingerBase':lRFinger00,
                               'Spine1':lSpine1,
                               'Spine2':lSpine2,    
                               'Neck1':lNeck1,
                               'Neck2':lNeck2,}

            for iSlot, iBone in lBoneAssignment.iteritems():
                if iBone:
                    print 'found!'
                    lBoneLink = (iSlot + "Link")
                    lProperty = lCharacter.PropertyList.Find(lBoneLink)
                    lProperty.append(iBone)
                    FBSystem().Scene.Evaluate()
                else:
                    print 'not found'

        lCharacter.SetCharacterizeOn(False)  

        lCharacter.PropertyList.Find("CharacterSolverSelector").Data = 2

        rs_CharacterExtension(lCharacter, False)

        lCharacter.PropertyList.Find("Hands Contact Type").Data = 3
        lCharacter.PropertyList.Find("Feet Contact Type").Data = 3

        ## now setup a tail extension

##        def rs_CreateTailCharacterExtension():
##            #--- Create a Character Extension
##            lTailExtension = FBCharacterExtension("Char_Extension_Tail")
##            lCharacter.AddCharacterExtension(lTailExtension)
##            #now add the joints in
##            lTailJointArray = []
##    
##            for obj in glo.gComponents:
##                thisName = obj.Name
##                lThisNameLower = thisName.lower()
##                lSubstringName = lThisNameLower[0:9]
##                thisNameLength = len(thisName)
##                if lSubstringName == "skel_tail":
##                    #print "Found "+obj.Name
##                    
##                    if lThisNameLower[(thisNameLength - 3):thisNameLength] != "nub":
##                        #print "Appending "+obj.Name+" cos its NOT a nub!"
##                        lTailJointArray.append(obj)       
##                    else:
##                        print "Skipping adding "+obj.Name+" to "+lTailExtension.Name+" cos its a nub!"
##                        
##            for tailJoint in lTailJointArray:
##                FBConnect(tailJoint,lTailExtension)
##                ##by default, translation properties get added so we need to remove these
##                tProp = lTailExtension.PropertyList.Find(tailJoint.Name+".Lcl Translation")
##                if tProp:
##                    lTailExtension.PropertyRemove(tProp)
##       
##            lTailExtension.PropertyList.Find("MirrorLabel").Data = 1 # this sets the Mirror Partner property to 'Self'
##            
##            lTailExtension.UpdateStancePose    ()    

##        print "testAttr == "+str(testAttr)

        rs_SkelPelvisConstraintAnimalSetup()  

        rs_AnimalJointRigConstraints()

        lCharacter.SetCharacterizeOn(True) 

        # removing this for latest setups
        #lQuadRotationOrderGroup = [glo.gAnimalSkelLeftArmArray, 
                                            #glo.gAnimalSkelRightArmArray, 
                                            #glo.gAnimalSkelLeftLegArray,
                                            #glo.gAnimalSkelRightLegArray,
                                            #glo.gAnimalSkelHeadNeckArray]

        #for iGroup in lQuadRotationOrderGroup:
                #for iBone in iGroup:
                    #lCTRLBone = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + iBone)
                    #lSKELBone = FBFindModelByLabelName(glo.gAnimalPrefixSkel + iBone)

                    #if lCTRLBone:
                        #lCTRLBone.PropertyList.Find("RotationActive").Data = True
                        #lCTRLBone.PropertyList.Find("RotationOrder").Data = 1

                    #if lSKELBone:
                        #lSKELBone.PropertyList.Find("RotationActive").Data = True
                        #lSKELBone.PropertyList.Find("RotationOrder").Data = 1

        lCTRLBone = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])
        lSKELBone = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[16])

        if lCTRLBone:
            lCTRLBone.PropertyList.Find("RotationActive").Data = True
            lCTRLBone.PropertyList.Find("RotationOrder").Data = 1

        if lSKELBone:
            lSKELBone.PropertyList.Find("RotationActive").Data = True
            lSKELBone.PropertyList.Find("RotationOrder").Data = 1  


        lCharacter.InputType.kFBCharacterInputStance
        lCharacter.ActiveInput = True
        FBSystem().Scene.Evaluate()
        lCharacter.ActiveInput = False
        FBSystem().Scene.Evaluate()

    else:

        lCTRLDeleteArray = []

        lgsCTRL = False 

        for iComponent in glo.gComponents:
            if iComponent.LongName.startswith("gs:CTRLRIG"):
                lgsCTRL = True

        if lgsCTRL == True:
            for iComponent in glo.gComponents:
                if iComponent.LongName.startswith("CTRLRIG"):
                    lCTRLDeleteArray.append(iComponent)
                    FBSystem().Scene.Evaluate()

        if len(lCTRLDeleteArray) > 0:  
            for iDelete in lCTRLDeleteArray:
                iDelete.FBDelete()
                FBSystem().Scene.Evaluate()        

        lPelvisRig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[0])
        lLThighRig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[1])
        lLCalfRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[2])
        lLFootRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[3])
        lRThighRig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[4])
        lRCalfRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[5])
        lRFootRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[6])
        lSpine0Rig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[7])

        lLUpperArmRig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[8])
        lLForeArmRig    = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[9])
        lLHandRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[10])
        lRUpperArmRig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[11])
        lRForeArmRig    = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[12])
        lRHandRig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[13])
        lHeadRig        = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[14])

        lLToe0Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[15])
        lRToe0Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[16])

        lLClavicleRig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[17])
        lRClavicleRig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[18])

        lNeck0Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[19])

        lLFinger00Rig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[20])
        lRFinger00Rig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[21])


        lSpine1Rig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[22])
        lSpine2Rig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[23])
        lSpine3Rig      = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[24])

        lNeck1Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[25])
        lNeck2Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[26])
        lNeck3Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[27])
        lNeck4Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[28])
        lNeck5Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[29])

        lRToe1Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[30]) 
        lLToe1Rig       = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[31])

        lRFinger01Rig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[32]) 
        lLFinger01Rig   = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gNewAnimalSkelArray[33])



        def rs_AssignQuadrupedJoints2():
            lBoneAssignment = {'Hips':lPelvisRig,               #SKEL_Pelvis
                               'LeftUpLeg':lLThighRig,         #SKEL_L_Femur
                               'LeftLeg':lLCalfRig,            #SKEL_L_Tibia
                               'LeftFoot':lLFootRig,           #SKEL_L_Metatarsus
                               'RightUpLeg':lRThighRig,        #SKEL_R_Femur
                               'RightLeg':lRCalfRig,           #SKEL_R_Tibia
                               'RightFoot':lRFootRig,          #SKEL_R_Metatarsus
                               'Spine':lSpine0Rig,             #SKEL_Spine0
                               'LeftArm':lLUpperArmRig,        #SKEL_L_Humerus
                               'LeftForeArm':lLForeArmRig,     #SKEL_L_Radius
                               'LeftHand':lLHandRig,           #SKEL_L_Canon
                               'RightArm':lRUpperArmRig,       #SKEL_R_Humerus
                               'RightForeArm':lRForeArmRig,    #SKEL_R_Radius
                               'RightHand':lRHandRig,          #SKEL_R_Canon
                               'Head':lHeadRig,                #SKEL_Head
                               'LeftToeBase':lLToe0Rig,        #SKEL_L_RearUpperPhalange
                               'RightToeBase':lRToe0Rig,       #SKEL_R_RearUpperPhalange
                               'LeftShoulder':lLClavicleRig,   #SKEL_L_Scapula
                               'RightShoulder':lRClavicleRig,  #SKEL_R_Scapula
                               'Neck':lNeck0Rig,               #SKEL_Neck0
                               'LeftFingerBase':lLFinger00Rig, #SKEL_L_FrontUpperPhalange
                               'RightFingerBase':lRFinger00Rig,#SKEL_R_FrontUpperPhalange
                               'Spine1':lSpine1Rig,            #SKEL_Spine1
                               'Spine2':lSpine2Rig,            #SKEL_Spine2
                               'Spine3':lSpine3Rig,            #SKEL_Spine3
                               'Neck1':lNeck1Rig,              #SKEL_Neck1           
                               'Neck2':lNeck2Rig,              #SKEL_Neck2
                               'Neck3':lNeck3Rig,              #SKEL_Neck3
                               'Neck4':lNeck4Rig,              #SKEL_Neck4
                               'Neck5':lNeck5Rig,              #SKEL_Neck5
                               'RightFootIndex1':lRToe1Rig,    #SKEL_R_RearLowerPhalange
                               'LeftFootIndex1':lLToe1Rig,     #SKEL_L_RearLowerPhalange
                               'RightHandIndex1':lRFinger01Rig,#SKEL_R_FrontLowerPhalange
                               'LeftHandIndex1':lLFinger01Rig} #SKEL_L_FrontLowerPhalange

            for iSlot, iBone in lBoneAssignment.iteritems():
                lBoneLink = (iSlot + "Link")
                lProperty = lCharacter.PropertyList.Find(lBoneLink)
                lProperty.append(iBone)
                FBSystem().Scene.Evaluate()

        lCharacter = FBCharacter(lGeo.Name)

        rs_AssignQuadrupedJoints2()

        lCharacter.SetCharacterizeOn(False) 
        lCharacter.PropertyList.Find("CharacterSolverSelector").Data = 2

        rs_CharacterExtension(lCharacter, True)

        lCharacter.PropertyList.Find("Hands Contact Type").Data = 3
        lCharacter.PropertyList.Find("Feet Contact Type").Data = 3

        rs_SkelPelvisConstraintAnimalSetup()  

        #rs_AnimalJointRigConstraints()

        lCharacter.SetCharacterizeOn(True) 
##        
##        lQuadRotationOrderGroup = [glo.gAnimalSkelLeftArmArray, 
##                                   glo.gAnimalSkelRightArmArray, 
##                                   glo.gAnimalSkelLeftLegArray,
##                                   glo.gAnimalSkelRightLegArray,
##                                   glo.gAnimalSkelHeadNeckArray]
##        
##        for iGroup in lQuadRotationOrderGroup:
##            for iBone in iGroup:
##                lCTRLBone = FBFindModelByName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + iBone)
##                lSKELBone = FBFindModelByName(glo.gAnimalPrefixSkel + iBone)
##                
##                if lCTRLBone:
##                    lCTRLBone.PropertyList.Find("RotationActive").Data = True
##                    lCTRLBone.PropertyList.Find("RotationOrder").Data = 1
##                    
##                if lSKELBone:
##                    lSKELBone.PropertyList.Find("RotationActive").Data = True
##                    lSKELBone.PropertyList.Find("RotationOrder").Data = 1
##                    
        lCTRLBone = FBFindModelByLabelName(glo.gAnimalPrefixGS + glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])
        lSKELBone = FBFindModelByLabelName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[16])

        if lCTRLBone:
            lCTRLBone.PropertyList.Find("RotationActive").Data = True
            lCTRLBone.PropertyList.Find("RotationOrder").Data = 1

        if lSKELBone:
            lSKELBone.PropertyList.Find("RotationActive").Data = True
            lSKELBone.PropertyList.Find("RotationOrder").Data = 1  

        lSkelSaddle = FBFindModelByLabelName("SKEL_SADDLE")

        if lSkelSaddle:

            lConstraintManager = FBConstraintManager()    
##            lPositionConstraint = lConstraintManager.TypeCreateConstraint( 5 )
##            
##            if ( lPositionConstraint ):
##                
##                lSpine0 = FBFindModelByName(glo.gAnimalPrefixSkel + glo.gAnimalSkelArray[12])
##                
##                lPositionConstraint.Name = "%s->%s_Position" %( lSkelSaddle.Name.replace(':','_'), lSpine0.Name.replace(':','_') )
##                lPositionConstraint.ReferenceAdd(0,lSkelSaddle)
##                lPositionConstraint.ReferenceAdd(1,lSpine0)
##                
##                lPositionConstraint.Active = True

            lRotationConstraint = lConstraintManager.TypeCreateConstraint( 10 )

            if ( lRotationConstraint ):

                # set Saddle rotation
                # X -91.85
                # Y 0
                # Z -90
                # And Snap

                lMover = FBFindModelByLabelName("mover")

                lRotationConstraint.Name = "%s->%s_Rotation" %( lSkelSaddle.Name.replace(':','_'), lMover.Name.replace(':','_') )
                lRotationConstraint.ReferenceAdd(0,lSkelSaddle)
                lRotationConstraint.ReferenceAdd(1,lMover)

                lRotationConstraint.Active = True

        lCharacter.InputType.kFBCharacterInputStance
        lCharacter.ActiveInput = True
        FBSystem().Scene.Evaluate()
        lCharacter.ActiveInput = False
        FBSystem().Scene.Evaluate()

        lMover = FBFindModelByLabelName('mover')
        lMoverCam = FBCamera("MoverCamera")
        lMoverCamInterest = FBModelNull("MoverCamera_Interest")
        lMoverCam.Interest = lMoverCamInterest
        lMoverCam.Show = True
        lMoverCamInterest.Show = True

        lMoverCamConstraint = FBConstraintManager().TypeCreateConstraint(3)
        lMoverCamConstraint.Name = "CameraMover_Constraint"
        #FBSystem().Scene.Constraints.append(lMoverCamConstraint)
        lMoverCamConstraint.ReferenceAdd (0,lMoverCam)
        lMoverCamConstraint.ReferenceAdd (1,lMover)
        lMoverCamConstraint.Snap()
        lMoverCamInterestConstraint = FBConstraintManager().TypeCreateConstraint(3)
        lMoverCamInterestConstraint.Name = "CameraMoverInterest_Constraint"
        #FBSystem().Scene.Constraints.append(lMoverCamInterestConstraint)
        lMoverCamInterestConstraint.ReferenceAdd (0,lMoverCamInterest)
        lMoverCamInterestConstraint.ReferenceAdd (1,lMover)
        lMoverCamInterestConstraint.Snap()

        lMoverCam.Translation = FBVector3d(-319.68,100,0)
        lVector = FBVector3d()
        lMoverCamInterest.Translation.Data = FBVector3d(0,105,0)

        FBSystem().Scene.Evaluate() 

    #else:#RAT!

        #lLThighSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[42])
        #lLCalfSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[44])
        #lLFootSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[45])
        #lRThighSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[50])
        #lRCalfSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[52])
        #lRFootSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[53])
        #lPelvisSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[18])
        #lLUpperArmSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[22])
        #lLForeArmSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[23])
        #lLHandSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + "_L_Forearm_NUB")
        #lRUpperArmSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[33])
        #lRForeArmSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[34])
        #lRHandSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + "_R_Forearm_NUB")
        #lHeadSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[2])
        #lLToe0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[46])
        #lRToe0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[54])
        #lLClavicleSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[20])
        #lRClavicleSkel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[31])
        #lNeck1Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[0])
        #lLFinger00Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[25])
        #lRFinger00Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[36])
        #lSpine0Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[12])
        #lSpine1Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[13])
        #lSpine2Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[14])
        #lSpine3Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[16])
        #lNeck2Skel = FBFindModelByLabelName(glo.gAnimalPrefixCtrlRig + glo.gAnimalSkelArray[1])

        #def rs_AssignQuadrupedJoints2():
                #lBoneAssignment = {'Hips':lPelvisSkel,
                                        #'LeftUpLeg':lLThighSkel,
                                        #'LeftLeg':lLCalfSkel,
                                        #'LeftFoot':lLFootSkel,
                                        #'RightUpLeg':lRThighSkel,
                                        #'RightLeg':lRCalfSkel,
                                        #'RightFoot':lRFootSkel,
                                        #'Spine':lSpine0Skel,
                                        #'LeftArm':lLUpperArmSkel,
                                        #'LeftForeArm':lLForeArmSkel,
                                        #'LeftHand':lLHandSkel,
                                        #'RightArm':lRUpperArmSkel,
                                        #'RightForeArm':lRForeArmSkel,
                                        #'RightHand':lRHandSkel,
                                        #'Head':lHeadSkel,
                                        #'LeftToeBase':lLToe0Skel,
                                        #'RightToeBase':lRToe0Skel,
                                        #'LeftShoulder':lLClavicleSkel,
                                        #'RightShoulder':lRClavicleSkel,
                                        #'Neck':lNeck1Skel,
                                        #'LeftFingerBase':lLFinger00Skel,
                                        #'RightFingerBase':lRFinger00Skel,
                                        #'Spine1':lSpine1Skel,
                                        #'Spine2':lSpine2Skel,  
                                        #'Spine3':lSpine3Skel,                                    
                                        #'Neck1':lNeck2Skel}

                #for iSlot, iBone in lBoneAssignment.iteritems():
                    #lBoneLink = (iSlot + "Link")
                    #lProperty = lCharacter.PropertyList.Find(lBoneLink)
                    #lProperty.append(iBone)

        #lCharacter = FBCharacter(lGeo.Name)        

        #rs_AssignQuadrupedJoints2()

        #lCharacter.SetCharacterizeOn(False)  

        #lCharacter.PropertyList.Find("CharacterSolverSelector").Data = 2

        #rs_CharacterExtension(lCharacter)

        #lCharacter.PropertyList.Find("Hands Contact Type").Data = 3
        #lCharacter.PropertyList.Find("Feet Contact Type").Data = 3

        ##rs_SkelPelvisConstraintAnimalSetup()  

        #rs_AnimalJointRigConstraints()

        #lCharacter.SetCharacterizeOn(True) 


    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)  

    for iScene in lSceneList:
        if 'ctrlrig' in iScene.Name.lower():
            lTag = iScene.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"    

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    ass.rs_AssetSetupCustomProperties()        

    rs_CustomTagsAnimalSetup()

    lCharacter.SetCharacterizeOn(True) 
    lCharacter.SetCharacterizeOn(False) 

    # add lookat url:bugstar:2023868
    ikcon.rs_FacingDirectionArrow()
    ikcon.rs_HeadLookAt()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 

#rs_QuadrupedAnimalSetup(None,None)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Aquatic:  Dolphin
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_DolphinAnimalSetup(pControl, pEvent):

    rs_AnimalSetupPrep()

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDeleteArray = []
    lTailArray = []
    lSpineArray = []
    lFinArray = []
    lHeadArray = []
    lTailFinArray = []
    lDorsalArray = []
    lPelvisArray = []
    lControllerList = []

    def rs_populateAnimalArray(componentObj, strFrom, strTo, nameStr, arrayToAppendTo):
        if componentObj.Name[strFrom:strTo] == nameStr: #new handling for tails
            iName = componentObj.Name
            lNameLength = len(iName)
            lThisNameLower = iName.lower()
            ##print "Regular name:"+iName+" Lowercase name:"+lThisNameLower
            if lThisNameLower[(lNameLength - 3):lNameLength] != "nub":
                if lThisNameLower[0:4] == "skel":    
                    arrayToAppendTo.append(componentObj)        
                    ##print "Appending "+componentObj.Name+" to "+nameStr+" array."
                ##  else:
                    ##print componentObj.Name+" is not prefixed with SKEL. It was:"+lThisNameLower[0:4]
            ##else:
                ##print iName+" appears to be a nub."

    #populate the data arrays

##    for iComponent in glo.gComponents:
    for iComponent in RS.Globals.Components:    
        ##print iComponent.Name
        if iComponent.LongName.startswith("CTRLRIG"):
            lDeleteArray.append(iComponent)

        rs_populateAnimalArray(iComponent, 5, 9, 'Tail', lTailArray)
        rs_populateAnimalArray(iComponent, 5, 10, 'Spine', lSpineArray)
        rs_populateAnimalArray(iComponent, 7, 10, 'Fin', lFinArray)
        rs_populateAnimalArray(iComponent, 5, 9, 'Head', lHeadArray)
        rs_populateAnimalArray(iComponent, 5, 8, 'Jaw', lHeadArray)
        rs_populateAnimalArray(iComponent, 7, 14, 'TailFin', lTailFinArray)
        rs_populateAnimalArray(iComponent, 5, 11, 'Dorsal', lDorsalArray)
        rs_populateAnimalArray(iComponent, 5, 11, 'Pelvis', lPelvisArray)                                   

    for iDelete in lDeleteArray:
        iDelete.FBDelete()
        FBSystem().Scene.Evaluate()

    lSkelRoot = FBFindModelByLabelName("SKEL_ROOT")

    # Create Controller Markers
    ##LOOK INTO COLOURING THESE MARKERS VIA STORED COLOUR FROM MR SKELETON JOINTS XML

    lRootControl = FBModelMarker(lSkelRoot.Name + '_Control')
    lRootControl.Size = 500
    lRootControl.Color = FBColor( 1,0.8,0.4)
    lControllerList.append(lRootControl)

    lSkelList = [lSkelRoot]

    lRootRig = lSkelRoot

    def rs_setupMarkerControls(arrayOfNodes, nodeSize, nodeColour):
        for itemNode in arrayOfNodes:
            lSkelList.append(itemNode)
            controlNode = FBModelMarker(itemNode.Name + '_Control')   
            print "Generated "+controlNode.Name     
            controlNode.Size = nodeSize
            if itemNode.Name[5:8] == "Jaw":   
                controlNode.Size = 750
            if itemNode.Name[5:15] == "Spine_Root":       
                controlNode.Size = 400
            controlNode.Color = FBColor(nodeColour)

            print "Attempting to align "+controlNode.Name+" to "+itemNode.Name

            AlignBones(controlNode, itemNode, False, False, False, True, True, True) ##align the orientation of marker to joint
            lControllerList.append(controlNode)

    rs_setupMarkerControls(lSpineArray, 800, (0,0,1))
    rs_setupMarkerControls(lTailArray, 900, ( 0,0,1))
    rs_setupMarkerControls(lHeadArray, 950, ( 0,0,1))
    rs_setupMarkerControls(lFinArray, 800, (0,0,1))        
    rs_setupMarkerControls(lTailFinArray, 800, (0,0,1))
    rs_setupMarkerControls(lDorsalArray, 750, ( 0,0.6,0.8))        
    rs_setupMarkerControls(lPelvisArray, 1500, ( 1,0,1))        

    #sort out the visibility etc
    lControlOffset = 60

    for i in range(len(lSkelList)):

        lControllerList[i].Show = True
        lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere
        lControllerList[i].Show = True
        lControllerList[i].Look = FBMarkerLook.kFBMarkerLookSphere

        RS.Utils.Scene.GetSetVector(lSkelList[i], lControllerList[i], True)
        lVector = FBVector3d()
        lVector2 = FBVector3d()

        lSkelTrns = lSkelList[i].GetVector(lVector, FBModelTransformationType.kModelTranslation, True)        

        x = float(lVector[0] )
        y = float(lVector[1] )
        z = float(lVector[2] )
        lControllerList[i].Translation = FBVector3d(x, y, z)

        #print "Controller shit fiddled with for "+lControllerList[i].Name+" to "+str(lControllerList[i].Translation)
        FBSystem().Scene.Evaluate()


    #parent up the controller nodes
    #NEED TO FIND THE CORRSEPONDING JOINT TO THIS MARKER THEN FIND THE MARKER FOR THAT TO USE AS THE PARENT
    for controlNodeIndex in range(len(lControllerList)):
        thisCont = lControllerList[controlNodeIndex]
        thisContName = lControllerList[controlNodeIndex].Name
        if thisCont.Name != "SKEL_ROOT_Control":
            thisContNameLength = len(thisContName)
            skelJointName = thisContName[0:(thisContNameLength - 8)]
            skelJoint = FBFindModelByLabelName(skelJointName)
            if skelJoint != None:
                skelJointParent = skelJoint.Parent
                ##print "Parent of "+skelJoint.Name+" is "+skelJointParent.Name
                thisContParent = FBFindModelByLabelName(skelJointParent.Name+"_Control")
                if thisContParent != None:
                    if thisContParent.Name != "SKEL_ROOT_Control":
                        if thisContParent.Name != "SKEL_Spine_Root_Control":
                            thisCont.Parent = thisContParent
                            print "Parented " + thisCont.Name + " to " +thisContParent.Name
                else:
                    print "b) Couldn't find "+skelJoint.Name+"_Control"
            else:
                print "a) Couldn't find "+skelJointName

    #now offset the control geo so its outside of the object geo to make selection easy.
    ## THESE ARE A LITTLE HARD CODED FOR MY LIKING NEED TO FIND A WAY TO DO A GLOBAL OFFSET
    ## OR ALTERNATIVELY HAVE SOME KIND OF DATA FILE MAYBE?
    for i in range(len(lControllerList)):
        x = float(0)         
        y = float(0)
        z = float(70)

        if lControllerList[i].Name == "SKEL_Spine_Root_Control":
            lControllerList[i].Color = FBColor( 0,1,0)
            lControllerList[i].Size = 0            
            x = float(50)
            y = float(50)
            z = float(0)
        if lControllerList[i].Name == "SKEL_ROOT_Control":
            lControllerList[i].Color = FBColor( 0,1,0)
            lControllerList[i].Size = 0
            x = float(0)
            y = float(-50)
            z = float(50)

        if lControllerList[i].Name[4:7] == "_L_":
            lControllerList[i].Color = FBColor( 0,1,0)
            x = float(50)
            z = z * -1
        if lControllerList[i].Name[4:7] == "_R_":        
            lControllerList[i].Color = FBColor( 1,0,0)              
            x = float(50)              
            z = z * -1            
        if lControllerList[i].Name[5:8] == "Jaw":        
            y = float(-20) 
            z = z * -1
            lControllerList[i].Color = FBColor( 0,0.6,0.8)                        
        if lControllerList[i].Name[5:11] == "Dorsal": 
            x = float(25)
            z = float(-30)
        if lControllerList[i].Name[5:9] == "Head":        
            z = z * -1
        if lControllerList[i].Name[5:11] == "Pelvis": 
            x = float(30)       
            z = z * -1            
        lControllerList[i].PropertyList.Find("GeometricTranslation").Data = FBVector3d(x, y, z)
        FBSystem().Scene.Evaluate()

    # SET THE PRE ROTATIONS AND ZERO OUT THE NORMAL ROTATIONS
    for controlNode in lControllerList:
        lclRot = controlNode.PropertyList.Find("Lcl Rotation").Data
        controlNode.PropertyList.Find("PreRotation").Data = lclRot

        controlNode.PropertyList.Find("RotationActive").Data = True
        controlNode.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)

    def rs_constrainRootJoints(parentNode, childNode):
##        if parentNode:
##            if childNode:
        lParentChildConstraint = FBConstraintManager().TypeCreateConstraint(3)
        lParentChildConstraint.Name = childNode.Name+"_Constraint"
        print "Parent node: "+parentNode.Name
        print "Child node: "+childNode.Name

        lParentChildConstraint.ReferenceAdd (0,childNode)
        lParentChildConstraint.ReferenceAdd (1,parentNode)
        lParentChildConstraint.Snap()     

    #now we need to constrain the skel to the control nodes.

    for i in range(len(lSkelList)):
        parentNode = lControllerList[i]
        childNode = lSkelList[i]

        if parentNode.Name != "SKEL_ROOT_Control":
            if parentNode.Name != "SKEL_Spine_Root_Control":
                print "Constraining "+childNode.Name+" to "+parentNode.Name
                rs_constrainRootJoints(parentNode, childNode)  

    #Now we've got all the controllers constraining their joints, we're gonna constrain the root controller to the pelvis

    toConstNodes = [
        #"SKEL_Spine_Root_Control",
        "SKEL_ROOT"]

    for childName in toConstNodes:
        parentNode = FBFindModelByLabelName("SKEL_Pelvis_Control")
        childNode = FBFindModelByLabelName(childName)
        rs_constrainRootJoints(parentNode, childNode)                

    lControlsToRemove = [
        "SKEL_Spine_Root_Control",
        "SKEL_ROOT_Control"]

    for thisCont in lControlsToRemove:             

        thisNode = FBFindModelByLabelName(thisCont)
        if thisNode:
            print "We NEED TO DELETE "+thisNode.Name
            thisNode.FBDelete()          

    #now we need to constrain the ROOT nodes to the pelvis

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    ass.rs_AssetSetupCustomProperties()

    rs_CustomTagsAnimalSetup()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Biped:  monkey
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_BipedAnimalSetup(pControl, pEvent):

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    lDummy = ass.rs_AssetSetupVariables()[0]
    lMover = ass.rs_AssetSetupVariables()[1]
    lGeo = ass.rs_AssetSetupVariables()[2]

    rs_AnimalSetupPrep()

    lSkelList = []

    RS.Utils.Scene.GetChildren(lMover, lSkelList, "", False)
    #lSkelList.extend(eva.gChildList)
    #del eva.gChildList[:]

    for iSkel in lSkelList:
        cha.rs_PrepFeetJointsCharacterSetup(iSkel, True)
        FBSystem().Scene.Evaluate()

    for iSkel in lSkelList:
        cha.rs_PrepJointsCharacterSetup(iSkel, True)
        FBSystem().Scene.Evaluate()

    FBSystem().Scene.Evaluate()

    lCharacter = FBCharacter(lGeo.Name)

    RS.Utils.Scene.AssignJoints(lCharacter)

    lCharacter.SetCharacterizeOn(True) 

    lCharacter.PropertyList.Find('LeftLegRollMode').Data = True
    lCharacter.PropertyList.Find('LeftLegRoll').Data = 100
    lCharacter.PropertyList.Find('RightLegRollMode').Data = True
    lCharacter.PropertyList.Find('RightLegRoll').Data = 100
    lCharacter.PropertyList.Find('LeftForeArmRollMode').Data = True
    lCharacter.PropertyList.Find('LeftForeArmRoll').Data = 100
    lCharacter.PropertyList.Find('RightForeArmRollMode').Data = True
    lCharacter.PropertyList.Find('RightForeArmRoll').Data = 100    
    lCharacter.PropertyList.Find("Feet Floor Contact").Data = False
    lCharacter.PropertyList.Find("Toes Floor Contact").Data = False
    lCharacter.PropertyList.Find("Hands Floor Contact").Data = False
    lCharacter.PropertyList.Find("Fingers Floor Contact").Data = False
    lCharacter.PropertyList.Find("Automatic Finger Base").Data = True
    lCharacter.PropertyList.Find("Hands Contact Type").Data = 2
    lCharacter.PropertyList.Find("Hips Level Mode").Data = False
    lCharacter.PropertyList.Find("Feet Spacing Mode").Data = False
    lCharacter.PropertyList.Find("Ankle Height Compensation Mode").Data = False
    lCharacter.PropertyList.Find("Match Source").Data = True
    lCharacter.PropertyList.Find("Left Hand Thumb Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Index Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Middle Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Ring Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Pinky Tip").Data = 0.10
    lCharacter.PropertyList.Find("Left Hand Extra Finger Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Thumb Tip").Data =  0.10
    lCharacter.PropertyList.Find("Right Hand Index Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Middle Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Ring Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Pinky Tip").Data = 0.10
    lCharacter.PropertyList.Find("Right Hand Extra Finger Tip").Data = 0.10
    lCharacter.PropertyList.Find("Automatic Finger Base").Data = True
    lCharacter.PropertyList.Find("Hands Contact Type").Data = 2

    cha.rs_RelationConstraintCharacterSetup()

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True)  

    for iScene in lSceneList:
        if 'ctrlrig' in iScene.Name.lower():
            lTag = iScene.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Contacts"    

    rs_AnimalGroupSetup()

    ass.rs_AssetSetupFolderTidy()

    ass.ScaleDownNulls()

    rs_CharacterExtension(lCharacter, True)

    ass.rs_AssetSetupCustomProperties()        

    rs_CustomTagsAnimalSetup()

    FBMessageBox("Animal Setup", lFileName + " has now been setup for animation", "Ok") 