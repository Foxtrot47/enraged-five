from pyfbsdk import *

import os

import RS.Core.Metadata
import RS.Globals


class VehicleWheelSetup():

    def __init__( self ):

        # class variables
        self.WheelDataDict = {}
        self.RadiusTypeList = []
        self.RadiusScaleDict = {}
        self.ExistingMeshDict = {}
        self.VehicleName = None

        # get vehicle name
        self.VehicleName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

        # a few exceptions with the file name url:bugstar:2119971
        print self.VehicleName
        if self.VehicleName == 'daemon':
            self.VehicleName = 'daemon_'
        if self.VehicleName.lower() == 'lazer':
            self.VehicleName = 'laser'

        # remove unwanted nodes
        self.RemoveUnwantedWheelNodes()

        # build dict for wheel data
        self.VehicleWheelDataXML()


    def RemoveUnwantedWheelNodes( self ):

        unwantedList = [ '_col', '_l1', '_l2', '_ng' ]
        deleteList = []

        for i in RS.Globals.Components:
            for j in unwantedList:
                if j in i.Name:
                    deleteList.append( i )

        for i in deleteList:
            try:
                i.FBDelete()
            except:
                pass


    def VehicleWheelDataXML( self ):

        metaPath = r'X:\VehicleWheelData_{0}.xml'.format( self.VehicleName )

        if os.path.exists( metaPath ):

            try:
                metaFile = RS.Core.Metadata.ParseMetaFile( metaPath )

                for i in metaFile.WheelDataList:

                    boneName = str( i.BoneName )
                    meshName = str( i.MeshName )
                    collisionName = str( i.CollisionName )
                    collisionRadius = float( i.CollisionRadius )

                    # create wheel data dictionary
                    self.WheelDataDict[ boneName ] = meshName, collisionName, collisionRadius

                    # create radius list
                    if collisionRadius in self.RadiusTypeList:
                        None
                    else:
                        self.RadiusTypeList.append( collisionRadius )

                    # create a dict for existing mesh and the radius of its collision
                    if meshName != "None":
                        self.ExistingMeshDict[ meshName ] = collisionRadius

            except:
                pass
        else:
            FBMessageBox( 'R* Error', "Unable to find vehicle's xml:\n\n{0}".format( metaPath ), 'Ok' )


    def RadiusScaleValue( self, diffBone, newBone ):
        radiusScaleDict = {}
        newWheelRadius = None
        diffWheelRadius = None
        scalingPercentage = None

        for key, value in self.WheelDataDict.iteritems():
            if newBone.Name == key:
                newWheelRadius = value[2]

            elif diffBone.Name == key:
                diffWheelRadius = value[2]

        if newWheelRadius and diffWheelRadius:

            difference = newWheelRadius/diffWheelRadius

        return difference


def CloneWheelMesh( mesh, wheelName, wheelBone, rotation ):

    clonedWheelMesh = mesh.Clone()
    clonedWheelMesh.Name = 'wheelmesh_' + wheelName

    vector = FBVector3d()
    wheelBone.GetVector( vector, FBModelTransformationType.kModelTranslation, True )
    clonedWheelMesh.SetVector( vector, FBModelTransformationType.kModelTranslation, True )    

    clonedWheelMesh.Rotation = FBVector3d( rotation )
    clonedWheelMesh.Scaling = FBVector3d( mesh.Scaling )
    wheelBone.ConnectSrc( clonedWheelMesh )

    return clonedWheelMesh


def SetupWheels():

    VehicleWheelSetupClass = VehicleWheelSetup()

    for key, value in VehicleWheelSetupClass.WheelDataDict.iteritems():

        boneName = key
        meshName = value[0]
        collName = value[1]
        collRadius = value[2]

        wheelBone = RS.Utils.Scene.FindModelByName( boneName )
        wheelMesh = RS.Utils.Scene.FindModelByName( meshName )
        wheelCollision = RS.Utils.Scene.FindModelByName( collName )

        # check if mesh exists on the left side
        if wheelBone and wheelMesh:
            # mesh exists so duplicate this to the right side
            split = wheelBone.Name.split( '_' )
            suffixName = ''
            if len(split) >2:
                suffixList = split[2:]
                for name in suffixList:
                    suffixName = '_{0}'.format(name)
            rightWheelName = split[1].replace( 'l', 'r' )
            rightWheel = RS.Utils.Scene.FindModelByName( 'wheel_' + rightWheelName + suffixName )
            if rightWheel:
                # wagons have an existing rotation on the Z-axis, for a more realistic wagon look. need to take this into account now
                rotVector = FBVector3d()
                rightWheel.GetVector( rotVector, FBModelTransformationType.kModelRotation, True )
                CloneWheelMesh( wheelMesh, rightWheelName + suffixName, rightWheel, ( 90, 0 , rotVector[2] + 180 ) )
        else:
            # mesh does not exist, get left bone
            # 'damaged' string find is a hack :/
            if '_l' in boneName and 'damaged' not in boneName:

                if len( VehicleWheelSetupClass.RadiusTypeList ) > 1:
                    # if there are multiple radius sizes, match the mesh based on existing mesh with the same radius
                    meshMatchSetup = False
                    for k, v in VehicleWheelSetupClass.ExistingMeshDict.iteritems():
                        if collRadius == v:
                            meshNameMatch = k
                            wheelMeshMatch = RS.Utils.Scene.FindModelByName( meshNameMatch ) 
                            if wheelMeshMatch:
                                # mesh exists so duplicate this to the missing left and right sides
                                split = wheelBone.Name.split( '_' )
                                suffixName = ''
                                if len(split) >2:
                                    suffixList = split[2:]
                                    for name in suffixList:
                                        suffixName = '_{0}'.format(name)
                                leftWheelName = split[1]
                                CloneWheelMesh( wheelMeshMatch, leftWheelName + suffixName, wheelBone, ( 0, 0 , 0 ) )
                                rightWheelName = split[1].replace( 'l', 'r' )
                                rightWheel = RS.Utils.Scene.FindModelByName( 'wheel_' + rightWheelName + suffixName)
                                # wagons have an existing rotation on the Z-axis, for a more realistic wagon look. need to take this into account now
                                rotVector = FBVector3d()
                                rightWheel.GetVector( rotVector, FBModelTransformationType.kModelRotation, True )
                                CloneWheelMesh( wheelMeshMatch, rightWheelName + suffixName, rightWheel, ( 90, 0 , rotVector[2] + 180 ) )
                                meshMatchSetup = True

                    if meshMatchSetup == False:
                        # mesh doesnt exist so duplicate the front wheel mesh and scale based on radius difference
                        for k, v in VehicleWheelSetupClass.ExistingMeshDict.iteritems():
                            leftFrontWheelBone = RS.Utils.Scene.FindModelByName( 'wheel_lf' )
                            leftFrontWheelMesh = RS.Utils.Scene.FindModelByName( 'wheelmesh_lf' )
                            if leftFrontWheelMesh:
                                split = wheelBone.Name.split( '_' )
                                suffixName = ''
                                if len(split) >2:
                                    suffixList = split[2:]
                                    for name in suffixList:
                                        suffixName = '_{0}'.format(name)
                                leftWheelName = split[1]
                                newLeftMesh = CloneWheelMesh( leftFrontWheelMesh, leftWheelName + suffixName, wheelBone, ( 0, 0 , 0 ) )

                                rightWheelName = leftWheelName.replace( 'l', 'r' )
                                rightWheel = RS.Utils.Scene.FindModelByName( 'wheel_' + rightWheelName + suffixName )
                                if rightWheel is not None:
                                    # wagons have an existing rotation on the Z-axis, for a more realistic wagon look. need to take this into account now
                                    rotVector = FBVector3d()
                                    rightWheel.GetVector( rotVector, FBModelTransformationType.kModelRotation, True )
                                    newRightMesh = CloneWheelMesh( leftFrontWheelMesh, rightWheelName + suffixName, rightWheel, ( 90, 0 , rotVector[2] + 180 ) )

                                radiusDifference = VehicleWheelSetupClass.RadiusScaleValue( leftFrontWheelBone, wheelBone )
                                if radiusDifference:
                                    # set scale
                                    vector = FBVector3d()
                                    leftFrontWheelMesh.GetVector( vector, FBModelTransformationType.kModelScaling, True )
                                    scaleX = vector[0] * radiusDifference
                                    scaleY = vector[1] * radiusDifference
                                    scaleZ = vector[2] * radiusDifference
                                    vector = FBVector3d( scaleX, scaleY, scaleZ )

                                    newLeftMesh.SetVector( vector, FBModelTransformationType.kModelScaling, True )
                                    if rightWheel is not None:
                                        newRightMesh.SetVector( vector, FBModelTransformationType.kModelScaling, True )

                elif len( VehicleWheelSetupClass.RadiusTypeList ) == 1:
                    # there is only 1 radius size for all bone collisions
                    # we want to match any middle missing meshes with the rear left mesh
                    if 'm' in boneName:
                        leftRearWheelMesh = RS.Utils.Scene.FindModelByName( 'wheelmesh_lr' )
                        if leftRearWheelMesh:
                            split = wheelBone.Name.split( '_' )
                            suffixName = ''
                            if len(split) >2:
                                suffixList = split[2:]
                                for name in suffixList:
                                    suffixName = '_{0}'.format(name)
                            leftWheelName = split[1]
                            CloneWheelMesh( leftRearWheelMesh, leftWheelName + suffixName, wheelBone, ( 0, 0 , 0 ) )

                            rightWheelName = leftWheelName.replace( 'l', 'r' )
                            rightWheel = RS.Utils.Scene.FindModelByName( 'wheel_' + rightWheelName + suffixName )
                            if rightWheel is not None:
                                # wagons have an existing rotation on the Z-axis, for a more realistic wagon look. need to take this into account now
                                rotVector = FBVector3d()
                                rightWheel.GetVector( rotVector, FBModelTransformationType.kModelRotation, True )
                                CloneWheelMesh( leftRearWheelMesh, rightWheelName + suffixName, rightWheel, ( 90, 0 , rotVector[2] + 180 ) )

                    else:
                        # if the bone isn't a middle bone, then we create missing meshes with the front left mesh
                        leftFrontWheelMesh = RS.Utils.Scene.FindModelByName( 'wheelmesh_lf' )
                        if leftFrontWheelMesh:
                            split = wheelBone.Name.split( '_' )
                            suffixName = ''
                            if len(split) >2:
                                suffixList = split[2:]
                                for name in suffixList:
                                    suffixName = '_{0}'.format(name)
                            leftWheelName = split[1]
                            CloneWheelMesh( leftFrontWheelMesh, leftWheelName + suffixName, wheelBone, ( 0, 0 , 0 ) )

                            rightWheelName = leftWheelName.replace( 'l', 'r' )
                            rightWheel = RS.Utils.Scene.FindModelByName( 'wheel_' + rightWheelName + suffixName)
                            if rightWheel is not None:
                                # wagons have an existing rotation on the Z-axis, for a more realistic wagon look. need to take this into account now
                                rotVector = FBVector3d()
                                rightWheel.GetVector( rotVector, FBModelTransformationType.kModelRotation, True )
                                CloneWheelMesh( leftFrontWheelMesh, rightWheelName + suffixName, rightWheel, ( 90, 0 , rotVector[2] + 180 ) )

                else:
                    print 'No radius exists, apparently!  Something must be wrong with the xml output.'