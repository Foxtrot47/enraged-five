import os
import RS
import RS.Perforce as p4
import RS.Utils.UserPreferences as userPref
import RS.Tools.UI.Application as uiAppTool
import RS.Core.Camera.SaveCameras as SaveCameras

from pyfbsdk import *


LOAD_CUTSCENE_MENU_NAME   = "Open Cutscene (P4 sync & edit)"
CUTSCENE_PATH             = "animation\\cutscene\\!!scenes"

SAVE_CAMS_HOTKEY          = "Ctrl+F12"
SAVE_CAMS_MENU_NAME       = "&Save Cameras...        {0}".format(SAVE_CAMS_HOTKEY)

app = FBApplication()


"""
Loading cutscene Logic with P4
Needs to be solid
"""
def cutsceneMenuHandler( source, event ):
    '''

    :param source:
    :param event:
    :return:
    '''
    filename = getattr(source, "path")

    #filename = "{0}\\{1}".format(CUTSCENE_PATH,val)
    # seem to get a wierd bug with the wrong filename attr asigned so will use the event name instead
    pathName = os.path.dirname(filename)    
    filename = os.path.join(pathName, event.Name)
    openFile(filename)

def camSaveMenuHandler(source, event):
    '''

    :param source:
    :param event:
    :return:
    '''
    if event.Name == SAVE_CAMS_MENU_NAME:
        SaveCameras.Run()
       
def openFile(filename):
    '''
    Open up file but check if is locked by user, also sync up file before loading into p4 and checkout
    :param filename:
    :return:
    '''

    print 'Syncing file {0}'.format(filename)

    bFileisLocked = False
    
    fileState = p4.GetFileState(filename)
    
    if fileState == []:
        FBMessageBox("Error", "Unable to determine File state in perforce please load manually\n\n{0}".format(filename), "Abort", None, None) 
        return
    
    if fileState.OtherLocked:
        msgStr = ''
        for user in fileState.OtherOpenUsers:
            msgStr +=(user) 
        msgResult = FBMessageBox("Error", "File is currently locked by\n\n{0}\n\nContinue to load in?".format(msgStr), "Yes", "No", None)
        if msgResult == 1:
            bFileisLocked = True
        if msgResult == 2:
            return


    if not p4.IsLatest(fileState):
        FBMessageBox("Message", "File is not up to date, need to sync first\n\n{0}".format(filename), "Sync", None, None)
        p4.Sync(filename)

    fileState = p4.GetFileState(filename)
    if not p4.IsLatest(fileState):
        FBMessageBox("Error", "Unable to Sync file, might be writeable\n\n{0}".format(filename), "Abort", None, None)
    else:
        if bFileisLocked:
            app.FileOpen(filename, False)
        else:
            # using custom Edit process
            msgResult = FBMessageBox("Message", "File is synced to latest! Open File for Edit \n\n{0}".format(filename), "Yes", "No" , None)
            if msgResult == 1:
                cmd = "edit"
                args = ["{0}".format(filename)]
                p4Files = p4.Run(cmd, args)
                # Load in file
                app.FileOpen(filename, False)
            else:
                msgResult = FBMessageBox("Message", "Load File into Motionbuilder Still?\n\n{0}".format(filename), "Yes", "No" , None)
                if msgResult == 1:
                    app.FileOpen(filename, False)

"""
    Get valid P4 files from depot 
    Valid p4 Fields
        time
        depotFile
        action
        type
        change
        rev 
"""
def getDepotFiles(project ,ext = 'fbx' ):
    DEPOT_PATH   = "{0}/{1}".format(project.Path.Art, CUTSCENE_PATH)

    fileList = []
    cmd = "files"
    args = ["-e","{0}\...{1}".format(DEPOT_PATH,ext)]
    p4Files = p4.Run(cmd, args)

    for record in  p4Files.Records:

        sPath = record['depotFile'].lower().split('!!scenes/')[1]
        fileList.append(str(sPath))
    return fileList

"""
    Get valid files from local drive
"""     
def returnFBXNames(rootdir, ext = 'fbx'):
    fileList = []
    for root, subFolders, files in os.walk(rootdir):
        for filename in files:
            if filename.split('.')[1] == ext:                
                relDir = os.path.relpath(root, rootdir)
                
                lPath = os.path.abspath(os.path.join(root, filename))
                sPath = lPath.split('!!scenes\\')[1]
                fileList.append(sPath)
        

    return fileList
        
def _getFBXfiles(project):
    return getDepotFiles(project)
    
              
            
"""
    Build our menus
    This could be done better by not needing to check for exisitng menus but it seems pretty fast
"""


def buildProjectFBXmenu(gMenuMgr, project):
    # Switched to use Depot Path
    fbxNames = _getFBXfiles(project)

    # Only add projects that have fbx files
    if len(fbxNames) > 0:
        gMenuMgr.InsertLast("File/{0}".format(LOAD_CUTSCENE_MENU_NAME), str(project.Name))

        for filename in fbxNames:

            paths = filename.split('/')
            lmenu = "File/{0}/{1}".format(LOAD_CUTSCENE_MENU_NAME, str(project.Name))

            for n in range(len(paths)):
                rsMenu = gMenuMgr.GetMenu("{0}/{1}".format(lmenu, paths[n]))

                # Inset our Cutscene path part into the menu
                if rsMenu == None:
                    gMenuMgr.InsertLast(lmenu, paths[n])

                # Add a callback to trigger opening the cutscene
                if n == (len(paths) - 1):
                    rsMenu = gMenuMgr.GetMenu(lmenu)
                    # remove any existing callbacks
                    rsMenu.OnMenuActivate.RemoveAll()
                    rsMenu.OnMenuActivate.Add(cutsceneMenuHandler)
                    setattr(rsMenu, "path", "{0}/{1}/{2}".format(project.Path.Art,CUTSCENE_PATH,filename))

                lmenu += "/{0}".format(paths[n])





def ToggleLoadCutsceneMenu():
    gMenuMgr = FBMenuManager()

    if userPref.__Readini__("menu","Enable Cutscene Menu", True):            

        rsMenu = gMenuMgr.GetMenu("File/{0}".format(LOAD_CUTSCENE_MENU_NAME))
        if rsMenu == None:

            gMenuMgr.InsertLast("File", LOAD_CUTSCENE_MENU_NAME)


            project = RS.Config.Project
            buildProjectFBXmenu(gMenuMgr, project)

            # Add our DLC Projects
            for dlcname in RS.Config.DLCProjects:
                project  = RS.Config.DLCProjects[dlcname]
                buildProjectFBXmenu(gMenuMgr, project)
    else:
      ""
      rsMenu = gMenuMgr.GetMenu("File/{0}".format(LOAD_CUTSCENE_MENU_NAME))
      if rsMenu:
          rsMenu.FBDelete()


def ToggleSaveCamerasMenu():
    # Create Basic Menu Objects
    gMenuMgr = FBMenuManager()
    fileMenu = gMenuMgr.GetMenu("File")
    saveCamMenu = fileMenu.GetItem(69)

    # Detect SaveCams Enabled in User Preferences
    if userPref.__Readini__("menu","Enable Save Cameras Menu", True):

        # Detect SaveCams Present in MB File Menu
        if saveCamMenu == None:

            # Create Save Cameras Menu
            fileMenu.InsertLast(SAVE_CAMS_MENU_NAME, 69)
            #fileMenu.InsertBefore("File", "/Send To Maya", SAVE_CAMS_MENU_NAME)
            fileMenu.OnMenuActivate.RemoveAll()
            fileMenu.OnMenuActivate.Add(camSaveMenuHandler)

            # Enable Save Cams Hotkey
            uiAppTool.AddShortCut(SAVE_CAMS_HOTKEY, "import RS.Core.Camera.SaveCameras as SaveCameras\n"
                                                    "SaveCameras.Run()")

    else:
        # Detect and Delete SaveCams from MB File Menu
        if saveCamMenu:
            fileMenu.DeleteItem(saveCamMenu)

            # Disable Save Cams Hotkey
            uiAppTool.RemoveShortCut(SAVE_CAMS_HOTKEY)


def Run():
    ToggleLoadCutsceneMenu()
    ToggleSaveCamerasMenu()