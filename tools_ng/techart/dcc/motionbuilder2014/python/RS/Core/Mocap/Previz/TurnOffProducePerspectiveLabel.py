'''

 Script Path: RS/Core/Mocap/Previz/TurnOffProducePerspectiveLabel.py
 
 Written And Maintained By: Kristine Middlemiss	
 
 Created: 22 May 2014
 
 Description: Turns off the label name in the Viewer for the Producer Perspective, it overlaps the new HUD

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the Play module from MocapCommands.py
mocapCommand.TurnOffProducePerspectiveLabel()