'''

 Script Path: RS/Core/Mocap/Toolbox/Decks.py

 Written And Maintained By: Kathryn Bodey

 Created: 01 November 2013

 Description: Functions for Creating, Duplicating and printingout Decks. 
              Used with the Mocap Tools UI.

 Functions: MeasureConstraint, CreateMocapDeck, DuplicateMocapDecks, PrintMocapDecks, 
            LoadMocapDecks, DisplayDeckMeasurement, HideDeckMeasurement

'''


from pyfbsdk import *

import os
import csv

import RS.Config
import RS.Globals
import RS.Utils.Scene
import RS.Core.Mocap.Toolbox.Stages


#################################################################################################
## MeasureConstraint: Function for creating the constraints in 'Create' & 'Duplicate' scripts
#################################################################################################
def MeasureConstraint( decksObject, stagesObject, userName ):

    markerList = []
    pointDict = {}

    for child in decksObject.Children:
        splitName = userName.split( ':' )
        child.LongName = splitName[0] + ':' + child.Name
        markerList.append( child )

    # constraint folder
    constraintPlaceholder = FBConstraintRelation( "ToBeDeleted" )
    constraintFolder = FBFolder( userName + '_Relations', constraintPlaceholder )
    constraintPlaceholder.FBDelete()

    # get upMarker variable
    upMarker = stagesObject.Children[0]

    # create dictionary with information on the deck children
    for marker in markerList:

        splitName = marker.Name.split( '_' )
        if splitName[1]  == 'Quarter':
            pointColor = splitName[3]
        else:
            pointColor = splitName[1]


        if 'Bottom' in marker.Name: 
            bottomPoint = marker

            for marker2 in markerList:
                if 'Bottom' not in marker2.Name and pointColor in marker2.Name:
                    topPoint = marker2 

                    pointDict[bottomPoint] = ( topPoint, pointColor )

    # create deck properties, result markers and constraints for measurement calculations
    for key, value in pointDict.iteritems():

        bottompoint = key
        topPoint = value[0]
        pointColor = value[1]

        # deck properties
        crossVectorProperty = decksObject.PropertyCreate( pointColor + '_CrossVectorMeasure', FBPropertyType.kFBPT_double, 'Number',True, True, None )
        crossVectorProperty.SetAnimated(True)
        pointProperty = decksObject.PropertyCreate( pointColor + '_PointMeasure', FBPropertyType.kFBPT_double, 'Number',True, True, None )
        pointProperty.SetAnimated(True)        

        # create end marker for cross vector length
        resultMarker = FBModelMarker( userName.split( ':' )[0] + ':' + pointColor + '_result'   ) 
        resultMarker.Size = 200
        resultMarker.Color = topPoint.Color
        resultMarker.Look = FBMarkerLook.kFBMarkerLookLightCross

        # constraint to measure the cross vector,to get the perpindicular distance from the point to the stage
        crossVectorConstraint = FBConstraintRelation( pointColor + '_' + userName + '_CrossVectorMeasure_Relation' )

        markerSender = crossVectorConstraint.SetAsSource( upMarker )
        stageSender = crossVectorConstraint.SetAsSource( stagesObject )
        bottomPointSender = crossVectorConstraint.SetAsSource( bottompoint )
        topPointSender = crossVectorConstraint.SetAsSource( topPoint )          
        subtract = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Subtract (V1 - V2)' )  
        subtract2 = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Subtract (V1 - V2)' )  
        subtract3 = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Subtract (V1 - V2)' )          
        dot = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Dot Product (V1.V2)' )  
        dot2 = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Dot Product (V1.V2)' )  
        multiply = crossVectorConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )  
        multiplyAnim = RS.Utils.Scene.FindAnimationNode(multiply.AnimationNodeInGet(),'b')
        multiplyAnim.WriteData([-1])        
        absolute = crossVectorConstraint.CreateFunctionBox( 'Number', 'Absolute (|a|)' )  
        divide = crossVectorConstraint.CreateFunctionBox( 'Number', 'Divide (a/b)' ) 
        greater = crossVectorConstraint.CreateFunctionBox( 'Number', 'Is Greater (a > b)' ) 
        scale = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Scale (a x V)' ) 
        add = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Add (V1 + V2)' ) 
        cond = crossVectorConstraint.CreateFunctionBox( 'Vector', 'IF Cond Then A Else B' ) 
        condAnim = RS.Utils.Scene.FindAnimationNode(cond.AnimationNodeInGet(),'b')
        condAnim.WriteData([0,0,0])
        subtract4 = crossVectorConstraint.CreateFunctionBox( 'Vector', 'Subtract (V1 - V2)' )  
        vecToNum = crossVectorConstraint.CreateFunctionBox( 'Converters', 'Vector to Number' )          
        vecToNum2 = crossVectorConstraint.CreateFunctionBox( 'Converters', 'Vector to Number' )          
        vecToNum3 = crossVectorConstraint.CreateFunctionBox( 'Converters', 'Vector to Number' )          
        multiply2 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )  
        multiply3 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )  
        multiply4 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Multiply (a x b)' )  
        add2 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Add (a + b)' ) 
        add3 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Add (a + b)' ) 
        sqrt = crossVectorConstraint.CreateFunctionBox( 'Number', 'sqrt(a)' ) 
        divide2 = crossVectorConstraint.CreateFunctionBox( 'Number', 'Divide (a/b)' ) 
        divide2Anim = RS.Utils.Scene.FindAnimationNode(divide2.AnimationNodeInGet(),'b')
        divide2Anim.WriteData([2.54])  
        deckReceiver = crossVectorConstraint.ConstrainObject( decksObject )
        resultReceiver = crossVectorConstraint.ConstrainObject( resultMarker )

        crossVectorConstraint.SetBoxPosition( markerSender, 0, 100 )  
        crossVectorConstraint.SetBoxPosition( stageSender, 0, 250 ) 
        crossVectorConstraint.SetBoxPosition( bottomPointSender, 0, 400 )             
        crossVectorConstraint.SetBoxPosition( topPointSender, 0, 550 )             
        crossVectorConstraint.SetBoxPosition( subtract, 410, 150 ) 
        crossVectorConstraint.SetBoxPosition( subtract2, 400, 300 ) 
        crossVectorConstraint.SetBoxPosition( subtract3, 400, 450 ) 
        crossVectorConstraint.SetBoxPosition( dot, 700, 200 ) 
        crossVectorConstraint.SetBoxPosition( dot2, 700, 350 ) 
        crossVectorConstraint.SetBoxPosition( multiply, 1100, 200 ) 
        crossVectorConstraint.SetBoxPosition( absolute, 1100, 350 ) 
        crossVectorConstraint.SetBoxPosition( divide, 1400, 200 ) 
        crossVectorConstraint.SetBoxPosition( greater, 1400, 350 ) 
        crossVectorConstraint.SetBoxPosition( scale, 1700, 200 ) 
        crossVectorConstraint.SetBoxPosition( add, 2100, 200 ) 
        crossVectorConstraint.SetBoxPosition( cond, 2400, 350 ) 
        crossVectorConstraint.SetBoxPosition( subtract4, 2700, 350 ) 
        crossVectorConstraint.SetBoxPosition( resultReceiver, 2700, 500 ) 
        crossVectorConstraint.SetBoxPosition( vecToNum, 3000, 200 ) 
        crossVectorConstraint.SetBoxPosition( vecToNum2, 3000, 350 ) 
        crossVectorConstraint.SetBoxPosition( vecToNum3, 3000, 500 ) 
        crossVectorConstraint.SetBoxPosition( multiply2, 3300, 200 ) 
        crossVectorConstraint.SetBoxPosition( multiply3, 3300, 350 ) 
        crossVectorConstraint.SetBoxPosition( multiply4, 3300, 500 ) 
        crossVectorConstraint.SetBoxPosition( add2, 3600, 350 ) 
        crossVectorConstraint.SetBoxPosition( add3, 3900, 500 ) 
        crossVectorConstraint.SetBoxPosition( sqrt, 4200, 500 )        
        crossVectorConstraint.SetBoxPosition( divide2, 4550, 500 ) 
        crossVectorConstraint.SetBoxPosition( deckReceiver, 4850, 500 ) 

        RS.Utils.Scene.ConnectBox( markerSender, 'Translation', subtract, 'V1' )
        RS.Utils.Scene.ConnectBox( stageSender, 'Translation', subtract, 'V2' )
        RS.Utils.Scene.ConnectBox( stageSender, 'Translation', subtract2, 'V2' )
        RS.Utils.Scene.ConnectBox( bottomPointSender, 'Translation', subtract2, 'V1' )
        RS.Utils.Scene.ConnectBox( bottomPointSender, 'Translation', subtract3, 'V2' )
        RS.Utils.Scene.ConnectBox( bottomPointSender, 'Translation', add, 'V1' )
        RS.Utils.Scene.ConnectBox( topPointSender, 'Translation', subtract3, 'V1' )
        RS.Utils.Scene.ConnectBox( topPointSender, 'Translation', subtract4, 'V1' )
        RS.Utils.Scene.ConnectBox( subtract, 'Result', dot, 'V1' )
        RS.Utils.Scene.ConnectBox( subtract, 'Result', dot2, 'V1' )
        RS.Utils.Scene.ConnectBox( subtract2, 'Result', dot, 'V2' )
        RS.Utils.Scene.ConnectBox( subtract3, 'Result', dot2, 'V2' )
        RS.Utils.Scene.ConnectBox( subtract3, 'Result', scale, 'Vector' )
        RS.Utils.Scene.ConnectBox( dot, 'Result', multiply, 'a' )
        RS.Utils.Scene.ConnectBox( dot2, 'Result', absolute, 'a' )
        RS.Utils.Scene.ConnectBox( dot2, 'Result', divide, 'b' )
        RS.Utils.Scene.ConnectBox( multiply, 'Result', divide, 'a' )
        RS.Utils.Scene.ConnectBox( absolute, 'Result', greater, 'a' )
        RS.Utils.Scene.ConnectBox( divide, 'Result', scale, 'Number' )
        RS.Utils.Scene.ConnectBox( greater, 'Result', cond, 'Cond' )
        RS.Utils.Scene.ConnectBox( scale, 'Result', add, 'V2' )
        RS.Utils.Scene.ConnectBox( add, 'Result', cond, 'a' )
        RS.Utils.Scene.ConnectBox( cond, 'Result', resultReceiver, 'Translation' )
        RS.Utils.Scene.ConnectBox( cond, 'Result', subtract4, 'V2' )
        RS.Utils.Scene.ConnectBox( subtract4, 'Result', vecToNum, 'V' )
        RS.Utils.Scene.ConnectBox( subtract4, 'Result', vecToNum2, 'V' )
        RS.Utils.Scene.ConnectBox( subtract4, 'Result', vecToNum3, 'V' )
        RS.Utils.Scene.ConnectBox( vecToNum, 'X', multiply2, 'a' )
        RS.Utils.Scene.ConnectBox( vecToNum, 'X', multiply2, 'b' )        
        RS.Utils.Scene.ConnectBox( vecToNum2, 'Y', multiply3, 'a' )
        RS.Utils.Scene.ConnectBox( vecToNum2, 'Y', multiply3, 'b' )
        RS.Utils.Scene.ConnectBox( vecToNum3, 'Z', multiply4, 'a' )        
        RS.Utils.Scene.ConnectBox( vecToNum3, 'Z', multiply4, 'b' )
        RS.Utils.Scene.ConnectBox( multiply2, 'Result', add2, 'a' )
        RS.Utils.Scene.ConnectBox( multiply3, 'Result', add2, 'b' ) 
        RS.Utils.Scene.ConnectBox( multiply4, 'Result', add3, 'b' ) 
        RS.Utils.Scene.ConnectBox( add2, 'Result', add3, 'a' )        
        RS.Utils.Scene.ConnectBox( add3, 'Result', sqrt, 'a' )        
        RS.Utils.Scene.ConnectBox( sqrt, 'Result', divide2, 'a' )        
        RS.Utils.Scene.ConnectBox( divide2, 'Result', deckReceiver, crossVectorProperty.Name )  

        constraintFolder.ConnectSrc( crossVectorConstraint )
        crossVectorConstraint.Active = True 

        #add basepreviz cleaner property
        basePrevizProperty = crossVectorConstraint.PropertyCreate('BasePreviz', FBPropertyType.kFBPT_charptr, "", False, True, None)
        basePrevizProperty.Data = "removeMe"

        # constraint to measure the point to the stage. just a standard point-to-point measurement.
        # does not account for rotation on the stage.
        measureConstraint = FBConstraintRelation( pointColor + '_' + userName + '_PointMeasure_Relation' )

        markerSender = measureConstraint.SetAsSource( topPoint )
        stageSender = measureConstraint.SetAsSource( stagesObject )
        subtract = measureConstraint.CreateFunctionBox( 'Vector', 'Subtract (V1 - V2)' )  
        vectorToNumber = measureConstraint.CreateFunctionBox( 'Converters', 'Vector to Number' ) 
        divide = measureConstraint.CreateFunctionBox( 'Number', 'Divide (a/b)' ) 
        divideB = RS.Utils.Scene.FindAnimationNode(divide.AnimationNodeInGet(),'b')
        divideB.WriteData([2.54])
        deckReceiver = measureConstraint.ConstrainObject( decksObject )

        measureConstraint.SetBoxPosition( markerSender, 0, 100 )
        measureConstraint.SetBoxPosition( stageSender, 0, 300 )
        measureConstraint.SetBoxPosition( subtract, 700, 200 )
        measureConstraint.SetBoxPosition( vectorToNumber, 1050, 200 )
        measureConstraint.SetBoxPosition( divide, 1350, 200 )
        measureConstraint.SetBoxPosition( deckReceiver, 1650, 200 )

        RS.Utils.Scene.ConnectBox( markerSender, 'Translation', subtract, 'V1' )
        RS.Utils.Scene.ConnectBox( stageSender, 'Translation', subtract, 'V2' )
        RS.Utils.Scene.ConnectBox( subtract, 'Result', vectorToNumber, 'V' )
        RS.Utils.Scene.ConnectBox( vectorToNumber, 'Y', divide, 'a' )
        RS.Utils.Scene.ConnectBox( divide, 'Result', deckReceiver, pointProperty.Name )     

        constraintFolder.ConnectSrc( measureConstraint )
        measureConstraint.Active = True      

        #add basepreviz cleaner property
        basePrevizProperty = measureConstraint.PropertyCreate('BasePreviz', FBPropertyType.kFBPT_charptr, "", False, True, None)
        basePrevizProperty.Data = "removeMe"


#################################################################################################
## CreateMocapDeck: Merges in Decks based on what the User has selected
#################################################################################################
def CreateMocapDeck( userInputName, userInputColor, userInputDeckType, stageSelect ):

    #get stage object
    stageObject = None
    stageObject = RS.Utils.Scene.FindModelByName( stageSelect )

    #get userinputdecktype name to match to a deck file name
    inputType = userInputDeckType.split( ' ' )[-1]

    mergePath = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Decks\\{1}.fbx'.format( RS.Config.Project.Path.Root, inputType )

    #merge options
    options = FBFbxOptions( True )
    options.NamespaceList = "PlaceholderNamespace"
    options.CamerasAnimation = False
    options.BaseCameras = False
    options.CurrentCameraSettings = False
    options.CameraSwitcherSettings = False

    perspectiveCamera = FBSystem().Scene.Cameras[0]
    cameraFarPlane = perspectiveCamera.FarPlaneDistance

    #Merge file in
    if os.path.isfile( mergePath ):

        FBApplication().FileMerge( mergePath, False, options )

        deck = None
        userColourMaterial = None

        for iComponent in RS.Globals.gComponents:

            #Find Deck Mesh & assign a variable
            if iComponent.LongName.startswith( "PlaceholderNamespace" ):
                if iComponent.ClassName() == 'FBModel':
                    deck = iComponent

                #Remove placeholder namespace
                iComponent.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

        if deck != None:

            #Rename Deck using user defined name        
            deck.LongName = userInputName + ':' + deck.Name

            #Update Deck 'userColor' Material using user defined color 
            for iMaterial in deck.Materials:
                if iMaterial.Name == 'userColor':
                    userColourMaterial = iMaterial

            if userColourMaterial != None:
                userColourMaterial.Diffuse = userInputColor
                #Renaming material so multiple versions of this material in the scene do not clash
                userColourMaterial.Name = userInputName + '_userColor'

            else:
                FBMessageBox( 'Error','Cannot find Deck Material. Material color change aborted.','Ok' )

            if stageObject != None:

                #add a custom property to tag the stage it is connected with
                stageProperty = deck.PropertyCreate('Stage Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                stageProperty.Data = stageObject.LongName

                #Align deck to selected stage           
                RS.Utils.Scene.Align( deck, stageObject )
                FBSystem().Scene.Evaluate()

                #create measure constraint
                MeasureConstraint( deck, stageObject, userInputName )   

                # reset far plane for perspective camera
                perspectiveCamera = FBSystem().Scene.Cameras[0]
                perspectiveCamera.FarPlaneDistance = cameraFarPlane

    else:
        FBMessageBox( 'Error','Cannot find Path to Deck:\n\{0}. \n\nScript aborted.'.format( mergePath ),'Ok' )


#################################################################################################
## DuplicateMocapDeck: Duplicates User Selected Decks
#################################################################################################
def DuplicateMocapDeck( control, event ):

    modelsList = FBModelList()
    FBGetSelectedModels( modelsList )    

    for item in modelsList:

        dupeObject = item
        deckProperty = dupeObject.PropertyList.Find( "Decks" )
        deckStageProperty = dupeObject.PropertyList.Find( "Stage Relation" )

        if deckProperty != None:

            mergePath = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Decks\\{1}.fbx'.format( RS.Config.Project.Path.Root, deckProperty.Data )

            # merge options
            options = FBFbxOptions( True )
            options.NamespaceList = "PlaceholderNamespace"
            options.CamerasAnimation = False
            options.BaseCameras = False
            options.CurrentCameraSettings = False
            options.CameraSwitcherSettings = False
            options.ViewingMode = FBVideoRenderViewingMode().FBViewingModeXRay                

            perspectiveCamera = FBSystem().Scene.Cameras[0]
            cameraFarPlane = perspectiveCamera.FarPlaneDistance    

            #Merge file in
            if os.path.isfile( mergePath ):

                FBApplication().FileMerge( mergePath, False, options )

                deck = None
                userColourMaterial = None
                originalDiffuse = None
                stageObject = None

                for iComponent in RS.Globals.gComponents:

                    #Find Deck Mesh & assign a variable
                    if iComponent.LongName.startswith( "PlaceholderNamespace" ):
                        if iComponent.ClassName() == 'FBModel':
                            deck = iComponent

                        #Remove placeholder namespace
                        iComponent.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

                #update deck properties
                if deck != None:

                    deck.LongName = item.Name + '_clone_1'

                    for child in deck.Children:
                        child.Name = child.Name + '_clone_1'

                    #get material diffuse proeprties for original deck
                    for iMaterial in dupeObject.Materials:
                        if '_userColor' in iMaterial.Name:
                            originalDiffuse = iMaterial

                    for iMaterial in deck.Materials:
                        if iMaterial.Name == 'userColor':
                            userColourMaterial = iMaterial

                    if userColourMaterial != None:
                        #Renaming material so multiple versions of this material in the scene do not clash
                        userColourMaterial.Name = deck.LongName + '_userColor'

                        #copy diffuse
                        if originalDiffuse != None:
                            userColourMaterial.Diffuse = FBColor( originalDiffuse.Diffuse )

                        #align new deck to selected decks
                        RS.Utils.Scene.Align( deck, dupeObject )
                        FBSystem().Scene.Evaluate()

                    #find stage relation on original deck, and copy it to cloned deck
                    if deckStageProperty != None:

                        stageObject = RS.Utils.Scene.FindModelByName( deckStageProperty.Data )

                        stageProperty = deck.PropertyCreate('Stage Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        stageProperty.Data = stageObject.LongName

                    #create measure constraint
                    if stageObject != None:
                        MeasureConstraint( deck, stageObject, deck.LongName ) 
                    else:
                        FBMessageBox( 'R* Error',"Unable to find Original Deck's Stage.\nMeasuring constraints have not been set up for this clone",'Ok' )

                # reset far plane for perspective camera
                perspectiveCamera = FBSystem().Scene.Cameras[0]
                perspectiveCamera.FarPlaneDistance = cameraFarPlane  

            else:
                FBMessageBox( 'R* Error','Cannot find Path to Deck:\n\{0}. \n\nScript aborted.'.format( mergePath ),'Ok' )                   


#################################################################################################
## PrintMocapDecks: Outputs deck info from scene
#################################################################################################
def PrintMocapDecks( control, event ):

    deckList = []
    deckDict = {}

    for iComponent in RS.Globals.gComponents:  

        if iComponent.PropertyList.Find( 'Decks' ):
            deckList.append( iComponent )

    #create dictionary
    for item in deckList:

        for material in item.Materials:
            if 'userColor' in material.Name:
                itemMat = material

        name = item.LongName
        propertyType = item.PropertyList.Find( 'Decks' ).Data
        propertySize = item.PropertyList.Find( 'Deck Size' ).Data
        trns = item.Translation
        rot = item.Rotation
        diff = itemMat.Diffuse
        propertyStage = item.PropertyList.Find( 'Stage Relation' ).Data

        stageObject = RS.Utils.Scene.FindModelByName( propertyStage )
        stageTrns = stageObject.Translation
        stageRot = stageObject.Rotation
        stageDiff = stageObject.Materials[0].Diffuse

        propertyYellow = None
        propertyYellow = item.PropertyList.Find( 'Yellow_PointMeasure' )
        if propertyYellow != None:        
            propertyYellow = round( item.PropertyList.Find( 'Yellow_PointMeasure' ).Data, 2 )
        else:
            propertyYellow = 'None'

        propertyGreen = round( item.PropertyList.Find( 'Green_PointMeasure' ).Data, 2 )
        propertyRed = round( item.PropertyList.Find( 'Red_PointMeasure' ).Data, 2 )
        propertyBlue = round( item.PropertyList.Find( 'Blue_PointMeasure' ).Data, 2 )

        deckDict[ name ] = propertyType, propertySize, propertyYellow, propertyGreen, propertyRed, propertyBlue, '', trns, rot, diff, name, '', propertyStage, stageTrns, stageRot, stageDiff 

    #output dictionary into a csv file
    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    csvPath = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Decks\\deck_prints\\{1}_deckprint.csv'.format( RS.Config.Project.Path.Root, fileName )

    headerString = [ 'DECK_TYPE', 'DECK_SIZE', 'YELLOW_POINT_INCHES', 'GREEN_POINT_INCHES', 'RED_POINT_INCHES', 'BLUE_POINT_INCHES', '', 'DECK_TRANSLATION', 'DECK_ROTATION', 'DECK_COLOR', 'DECKS_ORIGINAL_NAME', '', 'STAGE_RELATION', 'STAGE_TRANSLATION', 'STAGE_ROTATION', 'STAGE_COLOR' ]

    try:       
        csvFile = open( csvPath, 'wb' )
        dictWriter = csv.writer(csvFile)
        dictWriter.writerow(headerString)
        dictWriter.writerow(' ')       
        for i in range( len( deckDict.values() ) ) :
            dictWriter.writerow(deckDict.values()[i])  

    except IOError, err:
        print err

    finally:
        csvFile.close() 
        FBMessageBox( 'R* Print Decks', 'Print saved here:\n{0}'.format( csvPath ), 'Ok' )


#################################################################################################
## LoadMocapDeck: Loads Decks selected from a dropdown
#################################################################################################
def LoadMocapDecks( control, event ):

    #user selectes path
    filePath = FBFilePopup()
    filePath.Caption = "Select a file to load"
    filePath.Style = FBFilePopupStyle.kFBFilePopupOpen

    filePath.Filter = "*"
    filePath.Path = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Decks\\deck_prints\\'.format( RS.Config.Project.Path.Root )

    filePath.Execute()

    selectedPath = filePath.FullFilename

    #open csv
    if '.csv' in selectedPath:

        csvDict = {}
        csvInfoList = []

        try:
            openFile = open( selectedPath, 'rb' )
            readFile = csv.reader(openFile)
            readFile.next()
            readFile.next()

            for row in readFile:
                csvInfoList.append( row )

        except IOError, err:
            print err

        #put each item into a list
        if len( csvInfoList ) >= 1:

            for info in csvInfoList:
                csvDict[ info[10] ] = info[0], info[1], info[7], info[8], info[9], info[12], info[13], info[14], info[15]

        for key, value in csvDict.iteritems():

            deckName = key
            deckType = value[0]
            deckSize = value[1]
            deckTrns = value[2]
            deckRot = value[3] 
            deckColor = value[4] 
            stageName = value[5] 
            stageTrns = value[6] 
            stageRot = value[7] 
            stageColor = value[8]
            print key

            #get trns as individual floats
            trnsSplit = deckTrns.split(')')
            trnsSplit2 = trnsSplit[0].split('(')
            trnsSplit3 = trnsSplit2[-1].split(',')

            trnsStageSplit = stageTrns.split(')')
            trnsStageSplit2 = trnsStageSplit[0].split('(')
            trnsStageSplit3 = trnsStageSplit2[-1].split(',')            

            #get rot as individual floats
            rotSplit = deckRot.split(')')
            rotSplit2 = rotSplit[0].split('(')
            rotSplit3 = rotSplit2[-1].split(',')

            rotStageSplit = stageRot.split(')')
            rotStageSplit2 = rotStageSplit[0].split('(')
            rotStageSplit3 = rotStageSplit2[-1].split(',')

            #get color as individual floats
            colorSplit = deckColor.split(')')
            colorSplit2 = colorSplit[0].split('(')
            colorSplit3 = colorSplit2[-1].split(',')            

            colorStageSplit = stageColor.split(')')
            colorStageSplit2 = colorStageSplit[0].split('(')
            colorStageSplit3 = colorStageSplit2[-1].split(',')   

            #build decks
            mergePath = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Decks\\{1}.fbx'.format( RS.Config.Project.Path.Root, deckType )

            # merge options
            options = FBFbxOptions( True )
            options.NamespaceList = "PlaceholderNamespace"
            options.CamerasAnimation = False
            options.BaseCameras = False
            options.CurrentCameraSettings = False
            options.CameraSwitcherSettings = False
            options.ViewingMode = FBVideoRenderViewingMode().FBViewingModeXRay                

            perspectiveCamera = FBSystem().Scene.Cameras[0]
            cameraFarPlane = perspectiveCamera.FarPlaneDistance            

            #merge file in
            if os.path.isfile( mergePath ):

                FBApplication().FileMerge( mergePath, False, options )

                deck = None
                userColourMaterial = None

                for iComponent in RS.Globals.gComponents:

                    #find deck mesh & assign a variable
                    if iComponent.LongName.startswith( "PlaceholderNamespace" ):
                        if iComponent.ClassName() == 'FBModel':
                            deck = iComponent

                        #remove placeholder namespace
                        iComponent.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

                #add deck properties
                if deck != None:

                    deck.LongName = key

                    deck.Translation = FBVector3d( float( trnsSplit3[0] ), float( trnsSplit3[1] ), float( trnsSplit3[2] ) )
                    deck.Rotation = FBVector3d( float( rotSplit3[0] ), float( rotSplit3[1] ), float( rotSplit3[2] ) )

                    stageProperty = deck.PropertyCreate('Stage Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                    stageProperty.Data = stageName

                    for iMaterial in deck.Materials:
                        if iMaterial.Name == 'userColor':
                            userColourMaterial = iMaterial

                    if userColourMaterial != None:
                        userColourMaterial.Diffuse = FBColor( float( colorSplit3[0] ), float( colorSplit3[1] ), float( colorSplit3[2] ) )
                        #Renaming material so multiple versions of this material in the scene do not clash
                        userColourMaterial.Name = deckType + '_userColor'

                    else:
                        FBMessageBox( 'Error','Cannot find Deck Material. Material color change aborted.','Ok' )

                    #create stage for measurement constraints to work - first checking if stage exists
                    stageExists = False
                    for iComponent in RS.Globals.gComponents:

                        if iComponent.LongName == stageName:
                            stageExists = True
                            break

                    if stageExists: 
                        stage = iComponent
                        MeasureConstraint( deck, stage, deckName )

                    else:
                        stage = RS.Core.Mocap.Toolbox.Stages.rs_MocapStages( stageName, FBColor( float( colorStageSplit3[0] ), float( colorStageSplit3[1] ), float( colorStageSplit3[2] ) ) )
                        stage.Translation = FBVector3d( float( trnsStageSplit3[0] ), float( trnsStageSplit3[1] ), float( trnsStageSplit3[2] ) )
                        stage.Rotation = FBVector3d( float( rotStageSplit3[0] ), float( rotStageSplit3[1] ), float( rotStageSplit3[2] ) )

                        MeasureConstraint( deck, stage, deckName )

                # reset far plane for perspective camera
                perspectiveCamera = FBSystem().Scene.Cameras[0]
                perspectiveCamera.FarPlaneDistance = cameraFarPlane           

    else:
        FBMessageBox('R* Error', 'You did not select a .csv file, please try again.', 'Ok')


######################################################################################################
## DisplayDeckMeasurement/HideDeckMeasurement: Gives user a visual on/off for measurements in viewport
######################################################################################################
def DeckMeasurement( control, event ):

    # generate a dictionary to store information for decks, markers and properties
    deckList = []
    deckDictionary = {}        

    for iComponent in RS.Globals.gComponents:  
        if iComponent.PropertyList.Find( 'Decks' ):
            deckList.append( iComponent )

    for item in deckList:
        childList = []
        colorList = []
        for child in item.Children:
            if 'bottom' not in child.Name:
                childList.append(child)

                splitName = child.Name.split( '_' )
                if splitName[1]  == 'Quarter':
                    pointColor = splitName[3]
                else:
                    pointColor = splitName[1]

                colorList.append(pointColor)

        deckDictionary[ item ] = childList, colorList

    #show labels
    if control.State == 1:

        for key, value in deckDictionary.iteritems():

            for i in range( len( value[1] ) ):

                #find if nulls exist already
                if len( value[0][i].Children ) >= 1:
                    for propertyFind in key.PropertyList:
                        if 'Point' in control.Caption:
                            # show point-to-point 
                            if value[1][i] + '_PointMeasure' in propertyFind.Name:
                                for child in value[0][i].Children:
                                    splitMarkerName = value[0][i].LongName.split( ':' )
                                    child.LongName = splitMarkerName[0] + '_{0}:{1}_inches'.format(splitMarkerName[-1], round( propertyFind.Data, 2) )
                                    child.Show = True
                                    child.PropertyList.Find('ShowLabel').Data = True
                        else:
                            # show cross-vector 
                            if value[1][i] + '_CrossVectorMeasure' in propertyFind.Name:
                                for child in value[0][i].Children:
                                    splitMarkerName = value[0][i].LongName.split( ':' )
                                    child.LongName = splitMarkerName[0] + '_{0}:{1}_inches'.format(splitMarkerName[-1], round( propertyFind.Data, 2) )
                                    child.Show = True
                                    child.PropertyList.Find('ShowLabel').Data = True                            



                #if visual null does not exist 
                else:
                    if 'Bottom' not in value[0][i].Name:
                        measureMarker = FBModelMarker( 'marker' )
                        RS.Utils.Scene.Align( measureMarker, value[0][i] )
                        value[0][i].ConnectSrc( measureMarker )

                    for propertyFind in key.PropertyList:
                        if 'Point' in control.Caption:
                            if value[1][i] + '_PointMeasure' in propertyFind.Name:
                                for child in value[0][i].Children:
                                    splitMarkerName = value[0][i].LongName.split( ':' )
                                    child.LongName = splitMarkerName[0] + '_{0}:{1}_inches'.format(splitMarkerName[-1], round( propertyFind.Data, 2) )
                                    child.Show = True
                                    child.PropertyList.Find('ShowLabel').Data = True
                        else:
                            if value[1][i] + '_CrossVectorMeasure' in propertyFind.Name:
                                for child in value[0][i].Children:
                                    splitMarkerName = value[0][i].LongName.split( ':' )
                                    child.LongName = splitMarkerName[0] + '_{0}:{1}_inches'.format(splitMarkerName[-1], round( propertyFind.Data, 2) )
                                    child.Show = True
                                    child.PropertyList.Find('ShowLabel').Data = True  

    # hide labels
    if control.State == 0:

        for key, value in deckDictionary.iteritems():
            for i in range( len( value[1] ) ): 

                if len( value[0][i].Children ) >= 1:
                    for child in value[0][i].Children:
                        child.Show = False


######################################################################################################
## MergeStepMeasure: Merges in the Step Measure object
## textures: //rdr3/art/animation/resources/mocap/previz/BASE_PREVIZ.fbm/GizmoVertRuler_007_F.jpg
######################################################################################################
def MergeStepMeasure( stageSelect ):

    # get step object
    stageObject = None
    stageObject = RS.Utils.Scene.FindModelByName( stageSelect )

    # merge path
    mergePath = '{0}\\art\\animation\\resources\\mocap\\pre build models\\Step_Measure.fbx'.format( RS.Config.Project.Path.Root )

    # merge options
    options = FBFbxOptions( True )
    options.NamespaceList = "PlaceholderNamespace"
    options.CamerasAnimation = False
    options.BaseCameras = False
    options.CurrentCameraSettings = False
    options.CameraSwitcherSettings = False
    options.ViewingMode = FBVideoRenderViewingMode().FBViewingModeXRay                


    perspectiveCamera = FBSystem().Scene.Cameras[0]
    cameraFarPlane = perspectiveCamera.FarPlaneDistance

    # merge file in
    if os.path.isfile( mergePath ):

        FBApplication().FileMerge( mergePath, False, options )

        step = None

        for iComponent in RS.Globals.gComponents:

            # find step mesh & assign a variable
            if iComponent.LongName.startswith( "PlaceholderNamespace" ):
                if iComponent.ClassName() == 'FBModel':
                    step = iComponent

                # remove placeholder namespace
                iComponent.ProcessObjectNamespace( FBNamespaceAction.kFBRemoveAllNamespace, '', '' )

        step.Name = step.Name + '_1'

        # align to stage
        if stageObject != None:

            #add a custom property to tag the stage it is connected with
            stageProperty = step.PropertyCreate('Stage Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
            stageProperty.Data = stageObject.LongName

            # align step to selected stage
            RS.Utils.Scene.Align( step, stageObject, pAlignRotX = False, pAlignRotY = False, pAlignRotZ = False )
            FBSystem().Scene.Evaluate()
            
            step.Translation = FBVector3d( step.Translation[0], step.Translation[1] + 214.396, step.Translation[2] )
            FBSystem().Scene.Evaluate()
            
        # reset far plane for perspective camera
        perspectiveCamera = FBSystem().Scene.Cameras[0]
        perspectiveCamera.FarPlaneDistance = cameraFarPlane    

    else:
        FBMessageBox('R* Error', 'Unable to find file {0}, merge aborted.'.format( mergePath ), 'Ok' )
