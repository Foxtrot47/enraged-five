'''
RS.Core.Mocap.Audio

Author:

    Jason Hayes <jason.hayes@rockstarsandiego.com>
    
Description:

    Library module providing audio functionality for mocap tools.
'''

import os
import math

import pydub.audio_segment
import pytimecode


## Functions ##

def FindActionSlateBeeps( audioFilename, upperDecibelLevel = 70, lowerDecibelLevel = 10, maxBeepDuration = 0.3 ):
    '''
    Attempts to find the action slate beeps.
    
    Arguments:
    
        audioFilename: The .wav file to analyze.
        
    Keyword Arguments:
    
        upperDecibelLevel: If the sampled level of a frame in the audio hit this number, then we
                           we might have found a beep.
        lowerDecibelLevel: The decibel level where we check to see if a deep has ended.
        maxBeepDuration: Maximum duration of a beep in seconds.  If we found that the upper decibel level of the
                         audio segment hit our intended target, but the duration was longer than this number,
                         then it might have not been a beep.
        
    Returns:
    
        A list of SMPTE time codes of where the beeps were found.  Example:
        
            [ '00:00:37:921', '00:00:38:515', '00:00:39:120' ]
    '''
    audioFilename = str( audioFilename ).lower()
    
    if os.path.isfile( audioFilename ):
        if audioFilename.endswith( '.wav' ):
            beeps = []
            
            wav = pydub.audio_segment.AudioSegment.from_wav( audioFilename )
            
            frameNum = 0
            foundBeep = False
            beepDuration = 0
            beepStart = 0
            
            for frame in wav:
                if frame.rms != 0:
                    level = math.log10( frame.rms )
                    
                else:
                    level  = 0
                    
                db = 20 * level    
                
                if db >= upperDecibelLevel and not foundBeep:
                    beepStart = frameNum
                    foundBeep = True
                    
                elif db < lowerDecibelLevel:
                    if foundBeep:
                        if ( float( beepDuration ) / 1000.0 ) <= maxBeepDuration:
                            tc = pytimecode.PyTimeCode( 'ms', frames = beepStart )
                            beeps.append( tc.make_timecode() )
                    
                    foundBeep = False
                    beepStart = 0
                    beepDuration = 0
                    
                if foundBeep:
                    beepDuration += 1
                    
                frameNum += 1
                
            return beeps
        
        else:
            assert 0, 'The provided audio file must be a .wav file!'
        
    else:
        print 'The provided audio filename does not exist! ', audioFilename
    
    return None