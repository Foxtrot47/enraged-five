'''

 Script Path: RS/Core/Mocap/Previz/ActionSlateBeeps.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 08 October 2013
 
 Description: Launcher for ActionSlateBeeps UI

'''

from pyfbsdk import *

import RS.Tools.UI.ActionSlateBeeps


#calling the ActionSlateBeeps module from MocapCommands.py
RS.Tools.UI.ActionSlateBeeps.Run()