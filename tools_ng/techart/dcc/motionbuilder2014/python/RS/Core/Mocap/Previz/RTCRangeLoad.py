'''

 Script Path: RS/Core/Mocap/Previz/RTCRangeLoad.py
 
 Maintained By: Kathryn Bodey
 
 Created: 28 November 2013
 
 Description: Launcher for RTCRangeLoad module from MocapCommands.py
              uploads existing .txt output (fromRTCRangeSave) and sets 
              the hard and soft ranges based on the .txt info 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the CamRockBake module from MocapCommands.py
mocapCommand.RTCRangeLoad()