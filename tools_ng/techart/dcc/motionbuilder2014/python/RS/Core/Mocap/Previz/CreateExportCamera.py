'''

 Script Path: RS/Core/Mocap/Previz/CreateExportCamera.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 24 October 2013
 
 Description: Launcher for creating an export camera, using HB's CameraCore functions

'''

from pyfbsdk import *

import RS.Core.Camera.CameraCore

#calling the CreateExportCamera module from CameraCore.py
RS.Core.Camera.CameraCore.camUpdate.CreateExportCamera()