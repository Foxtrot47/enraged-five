'''

 Script Path: RS/Core/Mocap/Previz/Keybaord/Maya.py
 
 Written And Maintained By: Kristine Middlemiss
 
 Created: 31 December 2013
 
 Description: Launcher for ChangeKeyboardConfiguration function from MocapCommands.py
              Switched the Keyboard preferences to for Maya

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand

mocapCommand.ChangeKeyboardConfiguration("Maya")