'''

 Script Path: RS/Core/Mocap/Previz/CamRockBake.py
 
 Maintained By: Kathryn Bodey
 
 Created: 28 November 2013
 
 Description: Launcher for CamRockBake module from MocapCommands.py
              Plotting CamRocks to a new camera 

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the CamRockBake module from MocapCommands.py
mocapCommand.CamRockBake()