'''

 Script Path: RS/Core/Mocap/Previz/GiantDeviceOff.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 07 October 2013
 
 Description: Launcher for GiantDeviceOff module from MocapCommands.py
              Sets 'Online', 'Recording' and 'Live' options to OFF

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the GiantDeviceOff module from MocapCommands.py
mocapCommand.GiantDeviceOff()