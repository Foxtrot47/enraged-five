###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: MeasuringTool.py
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: 
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
from pyfbsdk_additions import *
import RS.Utils.Scene

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Create Markers, Null with Custom Property and Relation Constraint
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_MeasuringTool(pControl, pEvent):
    
    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True) 
    
    lExistingMeasuringToolList = []

    for iScene in lSceneList:
        lProp = iScene.PropertyList.Find("rs_Type")
        if lProp:
            if lProp.Data == "rs_MeasuringTool":
                lExistingMeasuringToolList.append(iScene)
        
    if len(lExistingMeasuringToolList) >= 1:
        FBMessageBox("Error: Measuring Tool", "Measuring Tool already exists in scene", "Ok")
    else:  
        lMeasureAMarker = FBModelMarker("Measure Tape A")
        lMeasureBMarker = FBModelMarker("Measure Tape B")
        lDistanceNull = FBModelNull("Distance Readout")
        
        lMeasureAMarker.Show = True
        lMeasureAMarker.Size = 250
        lMeasureAMarker.Color = FBColor(1,0,0)
        lMeasureAMarker.Look = FBMarkerLook.kFBMarkerLookLightCross
        lMeasureAMarker.PropertyList.Find("ShowLabel").Data = True
        lATagProperty = lMeasureAMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lATagProperty.Data = "rs_MeasuringTool"
       
        lMeasureBMarker.Show = True
        lMeasureBMarker.Size = 250
        lMeasureBMarker.Color = FBColor(1,0,0)
        lMeasureBMarker.Look = FBMarkerLook.kFBMarkerLookLightCross
        lMeasureBMarker.PropertyList.Find("ShowLabel").Data = True
        lBTagProperty = lMeasureBMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lBTagProperty.Data = "rs_MeasuringTool"
        
        lDistanceNull.Show = False
        lDistanceNull.PropertyList.Find("Enable Transformation").Data = False
        lDistanceNull.PropertyList.Find("Enable Selection").Data = False
        lDistanceTagProperty = lDistanceNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lDistanceTagProperty.Data = "rs_MeasuringTool"
        lRealProperty = lDistanceNull.PropertyCreate('Distance Readout', FBPropertyType.kFBPT_float, 'Number', True, True, None)
        lRealProperty.SetMax(5000)
        lRealProperty.SetAnimated(True)
        
        lRelationCon = FBConstraintRelation('MeasuringTool')
        lConTagProperty = lRelationCon.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lConTagProperty.Data = "rs_MeasuringTool"
        lMeasureAMarkerSender = lRelationCon.SetAsSource(lMeasureAMarker)
        lMeasureBMarkerSender = lRelationCon.SetAsSource(lMeasureBMarker)
        lDistanceVec = lRelationCon.CreateFunctionBox('Vector', 'Distance')
        lDistanceReceive = lRelationCon.ConstrainObject(lDistanceNull)
        lRelationCon.SetBoxPosition(lMeasureAMarkerSender, 0, 100)
        lRelationCon.SetBoxPosition(lMeasureBMarkerSender, 0, 250)
        lRelationCon.SetBoxPosition(lDistanceVec, 300, 170)
        lRelationCon.SetBoxPosition(lDistanceReceive, 700, 150)
        RS.Utils.Scene.ConnectBox(lMeasureAMarkerSender, 'Translation', lDistanceVec, 'v1 (Position)')
        RS.Utils.Scene.ConnectBox(lMeasureBMarkerSender, 'Translation', lDistanceVec, 'v2 (Position)')    
        RS.Utils.Scene.ConnectBox(lDistanceVec, 'Result', lDistanceReceive, 'Distance Readout')         
        lRelationCon.Active = True

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Delete Existing Measuring tool
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_DeleteMeasuringTool():
        
    lDeleteMeasuringToolList = []

    for iComp in FBSystem().Scene.Components:
        lProp = iComp.PropertyList.Find("rs_Type")
        if lProp:
            if lProp.Data == "rs_MeasuringTool":
                lDeleteMeasuringToolList.append(iComp)
    
    if len(lDeleteMeasuringToolList) >= 1:
        for iDelete in lDeleteMeasuringToolList:
            iDelete.FBDelete()
    else:
        FBMessageBox("Error: Measuring Tool", "Cannot delete Measuring Tool as it cannot be found in Scene", "Ok")     
