''' 
 Written And Maintained By: Geoff Samuel
 
 Created: 15 January 2015
 
 Description: Will Show the selected items, or warn the user that nothing is selected, if nothing is selected

'''
import pyfbsdk as mobu

def ShowAllUnlessNothingIsSelectedThenJustError():
    lModels = mobu.FBModelList()
    mobu.FBGetSelectedModels(lModels)
    
    if len(lModels) == 0:
        mobu.FBMessageBox("ShowOnlySelected", "Nothing Selected to Show", "Ok")
        return
    
    for model in lModels:
        model.Show = True


ShowAllUnlessNothingIsSelectedThenJustError()