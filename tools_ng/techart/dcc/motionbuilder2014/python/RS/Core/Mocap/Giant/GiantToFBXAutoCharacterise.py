###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: GiantToFBXAutoCharacterise
## Written And Maintained By: Kathryn Bodey
## Contributors:
## Description: Auto Characterises one or multiple characters in a giant-exported scene
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Path
import RS.Utils.Scene
import RS.Globals as glo

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Finding how many characters there are and adding their names to a list
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_GetCharacterNames():

    lRootNullList = []
    lSkelRootList = []
    lCharacterNameList = []

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, False)
    FBGetSelectedModels (lSceneList, None, True) 

    for iSceneItem in lSceneList:
        if iSceneItem.Parent:
            None
        else:
            lRootNullList.append(iSceneItem)
            
    for iRootNull in lRootNullList:
        if len(iRootNull.Children) >= 1:
            if 'waist' in iRootNull.Children[0].Name.lower():
                lSkelRootList.append(iRootNull)
    
    for iSkelRoot in lSkelRootList:
        lSpitName = iSkelRoot.Name.rpartition("_")
        lCharacterName = lSpitName[-1]
        lCharacterNameList.append(lCharacterName)
        
    return lCharacterNameList

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: T-Pose
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_TPose():

    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    for iCharacterName in rs_GetCharacterNames():
        
        lSkelList = []
        
        lRoot = FBFindModelByLabelName("root_" + iCharacterName)
        if lRoot:
            None
        else:
            print lFileName + ": Cannot find " + glo.gGiantSkelArray[0] + iCharacterName + ".Is the skel following usual naming covnentions?"
                    
        RS.Utils.Scene.GetChildren(lRoot, lSkelList, "", False)
        #lSkelList.extend(eva.gChildList)
        #del eva.gChildList[:]
        
        for iSkel in lSkelList:
            iSkel.Selected = True
            if 'mesh' in iSkel.Name:
                iSkel.Selected = False
  
    lTPoseList = FBModelList()
    FBGetSelectedModels (lTPoseList, None, True)    

    for iJoint in lTPoseList:
        iJoint.SetVector(FBVector3d(0,0,0), FBModelTransformationType.kModelRotation, True)         
    
    FBSystem().Scene.Evaluate()
    
    lArmArray =['l_up_arm_', 'r_up_arm_']
    
    for iCharacterName in rs_GetCharacterNames():
        for iArm in lArmArray:
            if 'l_' in iArm: 
                lUpArm = FBFindModelByLabelName(iArm + iCharacterName)
                lUpArm.SetVector(FBVector3d(0,0,90), FBModelTransformationType.kModelRotation, True)
            else:
                rUpArm = FBFindModelByLabelName(iArm + iCharacterName)
                rUpArm.SetVector(FBVector3d(0,0,-90), FBModelTransformationType.kModelRotation, True)
        
        FBSystem().Scene.Evaluate()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Characterise
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_Characterise(pCharacter, pCharacterName):
      
    lJointAssignment = {'LeftShoulder':FBFindModelByLabelName(glo.gGiantSkelArray[14] + "_" + pCharacterName),
                        'LeftArm':FBFindModelByLabelName(glo.gGiantSkelArray[15] + "_" + pCharacterName),
                        'LeftForeArm':FBFindModelByLabelName(glo.gGiantSkelArray[17] + "_" + pCharacterName),
                        'LeftHandThumb1':FBFindModelByLabelName(glo.gGiantSkelArray[22] + "_" + pCharacterName),
                        'LeftHandThumb2':FBFindModelByLabelName(glo.gGiantSkelArray[23] + "_" + pCharacterName),
                        'LeftHandThumb3':FBFindModelByLabelName(glo.gGiantSkelArray[24] + "_" + pCharacterName),
                        'LeftHandIndex1':FBFindModelByLabelName(glo.gGiantSkelArray[27] + "_" + pCharacterName),
                        'LeftHandIndex2':FBFindModelByLabelName(glo.gGiantSkelArray[28] + "_" + pCharacterName),
                        'LeftHandIndex3':FBFindModelByLabelName(glo.gGiantSkelArray[29] + "_" + pCharacterName),
                        'LeftHandMiddle1':FBFindModelByLabelName(glo.gGiantSkelArray[32] + "_" + pCharacterName),
                        'LeftHandMiddle2':FBFindModelByLabelName(glo.gGiantSkelArray[33] + "_" + pCharacterName),
                        'LeftHandMiddle3':FBFindModelByLabelName(glo.gGiantSkelArray[34] + "_" + pCharacterName),
                        'LeftHandRing1':FBFindModelByLabelName(glo.gGiantSkelArray[37] + "_" + pCharacterName),
                        'LeftHandRing2':FBFindModelByLabelName(glo.gGiantSkelArray[38] + "_" + pCharacterName),
                        'LeftHandRing3':FBFindModelByLabelName(glo.gGiantSkelArray[39] + "_" + pCharacterName),
                        'LeftHandPinky1':FBFindModelByLabelName(glo.gGiantSkelArray[42] + "_" + pCharacterName),
                        'LeftHandPinky2':FBFindModelByLabelName(glo.gGiantSkelArray[43] + "_" + pCharacterName),
                        'LeftHandPinky3':FBFindModelByLabelName(glo.gGiantSkelArray[44] + "_" + pCharacterName),
                        'RightShoulder':FBFindModelByLabelName(glo.gGiantSkelArray[46] + "_" + pCharacterName),
                        'RightArm':FBFindModelByLabelName(glo.gGiantSkelArray[47] + "_" + pCharacterName),
                        'RightForeArm':FBFindModelByLabelName(glo.gGiantSkelArray[49] + "_" + pCharacterName),
                        'RightHandThumb1':FBFindModelByLabelName(glo.gGiantSkelArray[54] + "_" + pCharacterName),
                        'RightHandThumb2':FBFindModelByLabelName(glo.gGiantSkelArray[55] + "_" + pCharacterName),
                        'RightHandThumb3':FBFindModelByLabelName(glo.gGiantSkelArray[56] + "_" + pCharacterName),
                        'RightHandIndex1':FBFindModelByLabelName(glo.gGiantSkelArray[59] + "_" + pCharacterName),
                        'RightHandIndex2':FBFindModelByLabelName(glo.gGiantSkelArray[60] + "_" + pCharacterName),
                        'RightHandIndex3':FBFindModelByLabelName(glo.gGiantSkelArray[61] + "_" + pCharacterName),
                        'RightHandMiddle1':FBFindModelByLabelName(glo.gGiantSkelArray[64] + "_" + pCharacterName),
                        'RightHandMiddle2':FBFindModelByLabelName(glo.gGiantSkelArray[65] + "_" + pCharacterName),
                        'RightHandMiddle3':FBFindModelByLabelName(glo.gGiantSkelArray[66] + "_" + pCharacterName),
                        'RightHandRing1':FBFindModelByLabelName(glo.gGiantSkelArray[69] + "_" + pCharacterName),
                        'RightHandRing2':FBFindModelByLabelName(glo.gGiantSkelArray[70] + "_" + pCharacterName),
                        'RightHandRing3':FBFindModelByLabelName(glo.gGiantSkelArray[71] + "_" + pCharacterName),
                        'RightHandPinky1':FBFindModelByLabelName(glo.gGiantSkelArray[74] + "_" + pCharacterName),
                        'RightHandPinky2':FBFindModelByLabelName(glo.gGiantSkelArray[75] + "_" + pCharacterName),
                        'RightHandPinky3':FBFindModelByLabelName(glo.gGiantSkelArray[76] + "_" + pCharacterName),
                        'LeftUpLeg':FBFindModelByLabelName(glo.gGiantSkelArray[78] + "_" + pCharacterName),
                        'RightUpLeg':FBFindModelByLabelName(glo.gGiantSkelArray[83] + "_" + pCharacterName),
                        'Hips':FBFindModelByLabelName(glo.gGiantSkelArray[0] + "_" + pCharacterName),
                        'LeftLeg':FBFindModelByLabelName(glo.gGiantSkelArray[79] + "_" + pCharacterName),
                        'LeftFoot':FBFindModelByLabelName(glo.gGiantSkelArray[80] + "_" + pCharacterName),
                        'LeftToeBase':FBFindModelByLabelName(glo.gGiantSkelArray[81] + "_" + pCharacterName),
                        'RightLeg':FBFindModelByLabelName(glo.gGiantSkelArray[84] + "_" + pCharacterName),
                        'RightFoot':FBFindModelByLabelName(glo.gGiantSkelArray[85] + "_" + pCharacterName),
                        'RightToeBase':FBFindModelByLabelName(glo.gGiantSkelArray[86] + "_" + pCharacterName),
                        'RightHand':FBFindModelByLabelName(glo.gGiantSkelArray[51] + "_" + pCharacterName),
                        'LeftHand':FBFindModelByLabelName(glo.gGiantSkelArray[19] + "_" + pCharacterName),
                        'Spine':FBFindModelByLabelName(glo.gGiantSkelArray[2] + "_" + pCharacterName),
                        'Spine1':FBFindModelByLabelName(glo.gGiantSkelArray[3] + "_" + pCharacterName),
                        'Spine2':FBFindModelByLabelName(glo.gGiantSkelArray[4] + "_" + pCharacterName),
                        'Spine3':FBFindModelByLabelName(glo.gGiantSkelArray[5] + "_" + pCharacterName),
                        'Spine4':FBFindModelByLabelName(glo.gGiantSkelArray[6] + "_" + pCharacterName),
                        'Spine5':FBFindModelByLabelName(glo.gGiantSkelArray[7] + "_" + pCharacterName),
                        'Spine6':FBFindModelByLabelName(glo.gGiantSkelArray[8] + "_" + pCharacterName),
                        'Neck':FBFindModelByLabelName(glo.gGiantSkelArray[9] + "_" + pCharacterName),
                        'Neck1':FBFindModelByLabelName(glo.gGiantSkelArray[10] + "_" + pCharacterName),                        
                        'Neck2':FBFindModelByLabelName(glo.gGiantSkelArray[11] + "_" + pCharacterName),                        
                        'Head':FBFindModelByLabelName(glo.gGiantSkelArray[12] + "_" + pCharacterName)}

    for iSlot, iJoint in lJointAssignment.iteritems():
        lJointLink = (iSlot + "Link")
        lProp = pCharacter.PropertyList.Find(lJointLink)
        lProp.append (iJoint)    

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_GiantToFBXAutoCharacterise
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_GiantToFBXAutoCharacterise():
       
    lFileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)
    
    FBPlayerControl().GotoStart() 
    
    rs_GetCharacterNames()
    
    if len(rs_GetCharacterNames()) >= 1:
            
        lNewTake = FBSystem().Scene.Takes[0].CopyTake('CharacteriseTake')
        FBSystem().CurrentTake = lNewTake 
    
        FBSystem().CurrentTake.ClearAllProperties(False)
        
        rs_TPose()
        
        for iCharacter in rs_GetCharacterNames():
            
            lRoot = FBFindModelByLabelName("root_" + iCharacter)
            
            lYTrans = lRoot.Translation.Data[1]
            lRoot.SetVector(FBVector3d(0,lYTrans,0), FBModelTransformationType.kModelTranslation, True)
        
        FBSystem().Scene.Evaluate()
    
    
        for iCharacter in rs_GetCharacterNames():
            lCharacter = FBCharacter(iCharacter)
            rs_Characterise(lCharacter, iCharacter)
            lCharacter.SetCharacterizeOn(True)
        
        print "Finished Setup of Characters in file: " + lFileName
    
    else:
        print lFileName +  ": No characters found in the scene, or the skeleton heirarchy does not follow the standard naming convention (should also not have the 'Offset' null at this point)"
