'''

 Script Path: RS/Core/Mocap/Previz/CreateToybox.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 08 October 2013
 
 Description: Launcher for CreateToybox module from MocapCommands.py
              Creates Toybox object. Sets materials, shaders and group.

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the CreateToybox module from MocapCommands.py
mocapCommand.CreateToybox()