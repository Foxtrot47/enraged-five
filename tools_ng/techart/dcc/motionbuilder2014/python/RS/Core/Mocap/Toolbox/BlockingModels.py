###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: BlockingModels.py
## Written And Maintained By: Kathryn Bodey
## Contributors: Kristine Middlemiss
## Description: 
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Utils.Scene
import RS.Globals as glo
import RS.Config
from RS.Tools import UI

# rs_BlockingModelCreate
def rs_BlockingModelCreate(pModelType, pModelPath, pUserInputName, pUserInputColor):
    lOptions = FBFbxOptions(True)
    lOptions.NamespaceList = pUserInputName
    lOptions.SetAll(FBElementAction.kFBElementActionMerge, True)
    lOptions.CameraSwitcherSettings = FBElementAction.kFBElementActionDiscard
    lOptions.Cameras = FBElementAction.kFBElementActionDiscard

    # Discard camera from incoming scene
    lOptions.BaseCameras = False

    glo.gApp.FileMerge( pModelPath, False, lOptions)

    lMaterial = FBMaterial(pUserInputName + "_BlockingModel")
    lMaterial.Diffuse = pUserInputColor
    lMaterialFolder = FBFolder(pUserInputName + "_BlockingModel", lMaterial)

    lGroup = FBGroup(pUserInputName + "_BlockingModel")

    lModelNullList = []
    RS.Utils.Scene.GetChildren(FBFindModelByLabelName(pUserInputName + ":" + pModelType), lModelNullList)

    for iNull in lModelNullList:
        iNull.Materials.append(lMaterial)
        lGroup.ConnectSrc(iNull)

    lHandle = FBHandle(pUserInputName + "_BlockingModel")
    if not lHandle:
        return

    lHandle.Follow.append(lModelNullList[0])
    lHandle.Manipulate.append(lModelNullList[0])
    lHandle.PropertyList.Find("Label").Data = pUserInputName
    lHandle.PropertyList.Find("2D Display Color").Data = pUserInputColor
    lHandle.PropertyList.Find("Auto-Size to Viewer").Data = True 
    if pModelType == "Cone":
        lHandle.PropertyList.Find("Translation Offset").Data = FBVector3d(0,200,0)
    else:
        lHandle.PropertyList.Find("Translation Offset").Data = FBVector3d(0,0,200)

    lModelNullList.append(lMaterial)
    lModelNullList.append(lGroup)
    lModelNullList.append(lHandle)

    for iNull in lModelNullList:
        if iNull:
            iNull.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None).Data = "rs_Contact"    
            iNull.PropertyCreate('Previz_Cleaner', FBPropertyType.kFBPT_charptr, "", False, True, None).Data = "RemovePostMocap"

# rs_FemaleBlockingModel
def rs_FemaleBlockingModel(pUserInputName, pUserInputColor):
    if pUserInputName == "":
        pUserInputName = "Female_A"
    lFemalePath = "{0}\\Female_A.fbx".format( RS.Config.Script.Path.ToolImages )
    lFemaleType = "Female_A"
    rs_BlockingModelCreate(lFemaleType,lFemalePath, pUserInputName, pUserInputColor)

#  rs_MaleBlockingModel
def rs_MaleBlockingModel(pUserInputName, pUserInputColor):
    if pUserInputName == "":
        pUserInputName = "Male_Tall_A"
    lMalePath = "{0}\\Male_Tall_A.fbx".format( RS.Config.Script.Path.ToolImages )
    lMaleType = "Male_Tall_A"
    rs_BlockingModelCreate(lMaleType, lMalePath, pUserInputName, pUserInputColor)

# rs_ConeBlockingModel
def rs_ConeBlockingModel(pUserInputName, pUserInputColor):
    if pUserInputName == "":
        pUserInputName = "Cone"
    lConePath = "{0}\\Cone.fbx".format( RS.Config.Script.Path.ToolImages )
    lConeType = "Cone"
    rs_BlockingModelCreate(lConeType, lConePath, pUserInputName, pUserInputColor)

# CubeWithCenterPivot
def CubeWithCenterPivot(pUserInputName, pUserInputColor):
    # check userInputName
    if pUserInputName == "":
        pUserInputName = "Cube"
    # create cube
    cube = FBModelCube(pUserInputName)
    # set properties
    cube.SetVector(FBVector3d(0, 25, 0))
    cube.SetVector(FBVector3d(25, 25, 25), FBModelTransformationType.kModelScaling)
    prop = cube.PropertyList.Find('Scaling Pivot')
    prop.Data = FBVector3d(0,-1,0)
    # add material
    material = FBMaterial(pUserInputName + "_BlockingModel")
    material.Diffuse = pUserInputColor
    cube.Materials.append(material)
    # The object must be set visible to be present in the system
    cube.Visible = True
    cube.Show = True