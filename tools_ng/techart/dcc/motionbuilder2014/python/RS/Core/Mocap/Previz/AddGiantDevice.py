'''

 Script Path: RS/Core/Mocap/Previz/AddGiantDevice.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 08 October 2013
 
 Description: Launcher for AddGiantDevice module from MocapCommands.py
              Adds the giant device to the scene

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the AddGiantDevice module from MocapCommands.py
mocapCommand.AddGiantDevice()