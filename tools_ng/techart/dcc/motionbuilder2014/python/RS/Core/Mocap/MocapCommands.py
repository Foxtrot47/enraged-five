'''

 Script Path: RS/Core/Mocap/MocapCommands.py

 Written And Maintained By: Kathryn Bodey and Kristine Middlemiss

 Created: 04 October 2013

 Description: Various small commands for Mocap pre-shoot, on-shoot & post-shoot.
              Designed to be assigned to hotkeys, and added to a Previz Toolbox UI.

 Functions: RecordOverwrite(), Play(), RecordOverwritePlay(), GiantDeviceOn()
            GiantDeviceOff(), NonGiantDevicesOn(), NonGiantDevicesOff(), AddGiantDevice(),
            DeleteDevices(), CreateToybox(), DeleteBasePreviz(), DeleteNonRefNonAnimObjects(),
            AddBasePrevizProperty(), PrepLattice(), PlotMocapAssets(), SaveSelectedCameras(),
            gsSkelInput(), RTCOffset(), RTCRangeSave(), RTCRangeLoad(), ZeroOutScene(control, event),
            ChangeKeyboardConfiguration(appChoose), SetActiveStage(pTargetStage), FindActiveStage(),
            GetStages(), PreVizSetup(), rs_Folders(pFolderName, folderType), RTEBaseSetup(CameraCreateNum),
            TurnOffProducePerspectiveLabel (), TurnRedToggle(), OpenMultiCameraViewer(), GetPropToys(),
            GetPropToysinScene(), addProp(itemName), and CreateConstraint(pChild, pChild, pType), MergeRefPose,
            PlotRefPose, SwapCamRocksInSwitcher, PropSetMatch

'''


from pyfbsdk import *
import ctypes
import os
import re
import time
import xml.etree.cElementTree
import glob

from PySide import QtGui, QtCore

from RS.Tools import UI

import RS.Config
import RS.Perforce
import RS.Utils.Scene
import RS.Core.Reference
import RS.Utils.Scene.Plot
import RS.Utils.Collections
import RS.Core.Mocap.Previz
import RS.Core.Animation.Lib
import RS.Core.Reference.HealthCheck
import RS.Core.Reference.NonRefItemViewerDict
import RS.Core.Camera.Lib as camCore
from RS.Tools.VirtualProduction import const
import RS.Core.Camera.PlotExportCamera as plo
from RS.Core.AnimData import Context
from RS import Globals
from RS.Utils.Scene import Component, Constraint
from RS.Tools.CameraToolBox.PyCore import singleton
from pythoninjectioninteractionmode import * # Needed for Keyboard Shortcuts

from RS import Perforce, ProjectData

reload(RS.Utils.Scene)
reload(RS.Utils.Scene.Plot)
reload(RS.Perforce)
reload (camCore)


class InteractionModes(object):
    """
    enum for the interaction modes to switch to
    """
    MotionBuilder = "MotionBuilder"
    MotionBuilderClassic = "MotionBuilder Classic"
    Maya = "Maya"
    Max = "3ds Max"
    LightWave = "Lightwave"
    Softimage = "Softimage"



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# RecordOverwrite(): record & overwrite player controls
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def RecordOverwrite():

    playerControl = FBPlayerControl()

    # if timeline is already recording, make it stop instead url:bugstar:1685279
    if playerControl.IsRecording:
        playerControl.Record(False, False)

    else:
        playerControl.Record(True, False)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Play(): play player controls
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def Play():

    playerControl = FBPlayerControl()

    # if timeline is already playing, make it stop instead url:bugstar:1685279
    if playerControl.IsPlaying:
        playerControl.Stop()
        playerControl.GotoStart()

    else:
        playerControl.Play()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# RecordOverwritePlay(): record, overwrite & play player controls
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def RecordOverwritePlay():

    # Set the correct Framerate and playback rate url:bugstar:2171334
    Globals.Player.SetTransportFps(FBTimeMode.kFBTimeMode30Frames, 0.0)

    # Autodesk seems to have changed the playback speed values between 2014 & 2016
    if Globals.System.Version < 16000:
        # 2015 & Earlier
        Globals.Player.SetPlaySpeed(FBTransportPlaySpeed.kFBSpeed_1x)
    else:
        # 2016 and up
        Globals.Player.SetPlaySpeed(FBTransportPlaySpeed.kFBSpeed_1_5x)


    # if timeline is already playing and recording, make it stop instead url:bugstar:1685279
    if not Globals.Player.IsPlaying and not Globals.Player.IsRecording:
        RecordOverwrite()
        Play()

    else:
        if Globals.Player.IsRecording:
            Globals.Player.Record(False, False)
        if Globals.Player.IsPlaying:
            Globals.Player.Stop()
            Globals.Player.GotoStart()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# GiantDeviceOn(): turn giant device on
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def GiantDeviceOn():

    giantDevice = None

    for device in Globals.Scene.Devices:
        if device.Name.lower() == "giant studios device":
            giantDevice = device

    if giantDevice:

        # if giant device is already on, toggle off and on again to wake it up url:bugstar:1685276
        if giantDevice.Online:
            giantDevice.Online = False
            time.sleep(1)
            giantDevice.Live = False
            giantDevice.RecordMode = False

            giantDevice.Online = True
            time.sleep(1)
            giantDevice.Live = True
            giantDevice.RecordMode = True

        else:
            giantDevice.Online = True
            time.sleep(1)
            giantDevice.Live = True
            giantDevice.RecordMode = True

    else:
        FBMessageBox('R* Error', 'Cannot find Giant Device. Script finished.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# GiantDeviceOff(): turn giant device off
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def GiantDeviceOff():

    giantDevice = None

    for device in Globals.Scene.Devices:
        if device.Name.lower() == "giant studios device":
            giantDevice = device

    if giantDevice:
        giantDevice.Online = False
        time.sleep(1)
        giantDevice.Live = False
        giantDevice.RecordMode = False

    else:
        FBMessageBox('R* Error', 'Cannot find Giant Device. Script finished.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# NonGiantDevicesOn(): turn non-giant devices on
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def NonGiantDevicesOn():

    deviceList = []

    for device in Globals.Scene.Devices:
        if device.Name.lower() != "giant studios device":
            deviceList.append(device)

    if len(deviceList) >= 1:
        for item in deviceList:
            item.Online = True
            time.sleep(1)
            item.Live = True
            item.RecordMode = True

    else:
        FBMessageBox('R* Error', 'Cannot find any Devices (ignoring giant device). Script finished.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# NonGiantDevicesOff(): turn non-giant devices on
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def NonGiantDevicesOff():

    deviceList = []

    for device in Globals.Scene.Devices:
        if device.Name.lower() != "giant studios device":
            deviceList.append(device)

    if len(deviceList) >= 1:
        for item in deviceList:
            item.Online = False
            time.sleep(1)
            item.Live = False
            item.RecordMode = False

    else:
        FBMessageBox('R* Error', 'Cannot find any Devices (ignoring giant device). Script finished.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# AddGiantDevice(): adds the giant device to the scene
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def AddGiantDevice():

    giantDevice = FBCreateObject("Browsing/Templates/Devices", "giant studios device", "giant studios device")
    Globals.Scene.Devices.append(giantDevice)

    # merging in an fbx with the giant device instead of added it directly from devices templates
    # in a bid to prevent crash url:bugstar:1855512
    # options = FBFbxOptions(True)
    # options.UseASCIIFormat = True
    # options.BaseCameras = False
    # options.CameraSwitcherSettings = False
    # options.CurrentCameraSettings = False
    # options.GlobalLightingSettings = False
    # options.TransportSettings = False

    # mergePath =  r'{0}\art\animation\resources\mocap\previz\giant studios device.fbx'.format(RS.Config.Project.Path.Root)

    # if os.path.exists(mergePath):
        # FBApplication().FileMerge(mergePath, False, options)

    # else:
        # FBMessageBox('R* Error', 'Unable to find Giant Device file to merge in.  It should be here:\n\n{0}'.format(
            # mergePath), 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# AddFacewareLiveDevice(): adds the SpaceBall device to the scene
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def AddFacewareLiveDevice():

    lFLDevice = FBCreateObject("Browsing/Templates/Devices", "Faceware Live Client Device",
                               "Faceware Live Client Device")
    Globals.Scene.Devices.append(lFLDevice)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# DeleteDevices(): turn off Giant Device and delete it
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #a
def DeleteGiantDevice():

    giantDevice = None

    for device in Globals.Scene.Devices:
        if device.Name.lower() == "giant studios device":
            giantDevice = device

    if giantDevice:
        giantDevice.Online = False
        time.sleep(1)
        giantDevice.Live = False
        giantDevice.RecordMode = False
        giantDevice.FBDelete()

    else:
        FBMessageBox('R* Error', 'Cannot find Giant Device. Script finished.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# DeleteDevices(): turn off all Devices and delete them
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def DeleteDevices():

    deviceList = []

    for device in Globals.Scene.Devices:
        deviceList.append(device)

    if len(deviceList) >= 1:
        for item in deviceList:
            if item.Name != 'giant studios device':
                item.Online = False
                time.sleep(1)
                item.Live = False
                item.RecordMode = False
                item.FBDelete()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# DeleteDevices(): creates toybox in scene
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def CreateToybox():

    toybox = FBModelCube("ToyBox")
    toybox.Show = True
    toybox.Scaling = FBVector3d(100, 100, 100)
    toybox.Translation = FBVector3d(0, -1200, 0)

    toyboxMaterial = FBMaterial("ToyBox_Material")
    toyboxMaterial.Diffuse = FBColor(1, 0, 1)
    toyboxMaterial.Opacity = 0.50
    toybox.Materials.append(toyboxMaterial)

    toyboxShader = FBShaderManager().CreateShader("FlatShader")
    toyboxShader.Name = "ToyBox_Shader"
    toyboxShader.PropertyList.Find("Transparency Type").Data = 1
    toybox.Shaders.append(toyboxShader)
    toybox.ShadingMode = FBModelShadingMode.kFBModelShadingAll

    toyboxGroup = FBGroup("ToyBox")
    toyboxGroup.ConnectSrc(toybox)
    toyboxGroup.Show = True
    toyboxGroup.Pickable = True
    toyboxGroup.Transformable = True


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# AddBasePrevizProperty(): adds 'basepreviz' custom property to all objects in file
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def AddBasePrevizProperty():

    propertyAddList = []
    basePrevizProperty = None

    # iterate through all scene components
    for component in Globals.gComponents:
        if component.PropertyList.Find('BasePreviz'):
            # if property already exists on component, ignore it
            None
        else:
            # if the property doesnt exist add it to a list
            propertyAddList.append(component)

    # iterate through list adding the property to each item
    if len(propertyAddList) >= 1:

        for item in propertyAddList:
            # fbmeshes crash when trying to delete, so skip these
            if item.ClassName() == 'FBMesh' or item.ClassName() == 'FBDeformer' \
                                    or item.ClassName() == 'FBModelPlaceHolder' \
                                    or item.ClassName() == 'FBBox' or item.Name.startswith('Cluster'):
                None
            else:
                basePrevizProperty = item.PropertyCreate('BasePreviz', FBPropertyType.kFBPT_charptr, "", False, True,
                                                         None)

            # ignore none-types
            if basePrevizProperty:
                basePrevizProperty.Data = "removeMe"

    # if old remove property exists, delete it
    for component in Globals.gComponents:
        if component.PropertyList.Find('Previz_Cleaner'):
            component.PropertyRemove(component.PropertyList.Find('Previz_Cleaner'))
        else:
            None


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# DeleteBasePreviz(): deletes base previz objects
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def DeleteBasePreviz():

    basePrevizList = []

    # search through all scene components and find custom property
    previzRemoveProperty = None

    for component in Globals.gComponents:
        if component.PropertyList.Find('BasePreviz'):
            basePrevizList.append(component)

    # remove camera interest from list, otherwise script fails
    # interest will be auto-deleted when the camera has been deleted
    for item in basePrevizList:
        if item.Name == ('SlateCam Interest'):
            basePrevizList.remove(item)
        if item.Name == ('Take 001'):
            basePrevizList.remove(item)

    # delete base previz
    if len(basePrevizList) >= 1:
        try:
            for item in basePrevizList:
                item.FBDelete()
                Globals.Scene.Evaluate()
        except:
            FBMessageBox('R* Error', 'Error when deleting mocap objects.', 'Ok')

    # clean out floating textures, videos and folders
    RS.Core.Reference.HealthCheck.rs_DeleteTextureVideoClip()
    RS.Core.Reference.HealthCheck.rs_DeleteFolders_Check()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# PrepLattice(): helps prep a file for lattice
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def PrepLattice(control, event):

    app = FBApplication()

    filename = RS.Utils.Path.GetBaseNameNoExtension(app.FBXFileName)

    # check user has added items to each field
    if len(control.StageControl.Items) == 0:
        FBMessageBox("R* Error", "Your stage dragdrop field has been left empty.", "OK")
        return

    elif len(control.GroundGeoControl.Items) == 0:
        FBMessageBox("R* Error", "Your ground dragdrop field has been left empty.", "OK")
        return

    else:
        # check the ground objects are part of 'set root' hierarchy
        continueScript = True

        for item in control.GroundGeoControl.Items:
            groundObject = FBFindModelByLabelName(item)
            parent = RS.Utils.Scene.GetParent(groundObject.Name)
            if parent.Name != 'SetRoot':
                continueScript = False

        # all checks complete, continue with script
        if continueScript:

            # get a handle on stage scene object
            stageObject = FBFindModelByLabelName(control.StageControl.Items[0])

            # add ground objects to a list
            groundObjectList = []
            parentList = []
            parentSceneObjectList = []
            cloneList = []
            groundOriginalsDict = {}

            for geo in control.GroundGeoControl.Items:
                groundObjectList.append(str(geo))

            # add all parent nodes to the same list
            for item in groundObjectList:
                groundObject = FBFindModelByLabelName(item)
                RS.Utils.Scene.GetParentList(groundObject.Name, parentSceneObjectList)

            for parent in parentSceneObjectList:
                if not str(parent) in parentList:
                    parentList.append(str(parent.LongName))

            # unparent ground objects
            for item in groundObjectList:
                sceneItem = FBFindModelByLabelName(item)
                if sceneItem:
                    sceneItemParent = sceneItem.Parent
                    sceneItem.Parent = None

                    # dict for scene item and parent, so we can reparent them at the end
                    groundOriginalsDict[sceneItem] = sceneItemParent

            # clone ground and stage
            RS.Utils.Scene.DeSelectAll()

            for item in groundObjectList:
                sceneItem = FBFindModelByLabelName(item)
                if sceneItem:
                    cloneGround = sceneItem.Clone()
                    cloneGround.Name = item + '_lattice'
                    cloneList.append(cloneGround)

            cloneStage = stageObject.Clone()
            cloneStage.Name = '{0}_lattice'.format(Component.GetNamespace(stageObject))

            # re-parent original ground objects to their original parents
            for key, value in groundOriginalsDict.iteritems():
                key.Parent = value

            # create null, align to stage
            null = FBModelNull('temp_null')

            RS.Utils.Scene.Align(null, cloneStage)
            Globals.Scene.Evaluate()

            # parent dupes to null & move to origin
            for i in cloneList:
                i.Parent = null
            cloneStage.Parent = null

            null.Translation = FBVector3d(0, 0, 0)
            null.Rotation = FBVector3d(0, 0, 0)
            Globals.Scene.Evaluate()

            # unparent objects & delete null
            for i in cloneList:
                i.Parent = None
            cloneStage.Parent = None

            Globals.Scene.Evaluate()
            null.FBDelete()
            cloneStage.FBDelete()

            RS.Utils.Scene.DeSelectAll()

            for i in cloneList:
                i.Selected = True

            # save options
            saveOptions = FBFbxOptions(False)
            saveOptions.FileFormatAndVersion = FBFileFormatAndVersion.kFBFBX2014_2015 # Fix for LEI not support FBX 2016
            saveOptions.SaveSelectedModelsOnly = True
            saveOptions.UseASCIIFormat = True
            saveOptions.BaseCameras = False
            saveOptions.CameraSwitcherSettings = False
            saveOptions.CurrentCameraSettings = False
            saveOptions.GlobalLightingSettings = False
            saveOptions.TransportSettings = False

            # get stage name
            stageName = Component.GetNamespace(stageObject)

            # save directory
            if stageName != None:
                application = FBApplication()
                filePath = os.path.join(RS.Config.Project.Path.MocapRoot,
                                            "assetPrep",
                                            "latticePrep",
                                            "latticeFBX",
                                            "{0}_latticePrep.fbx".format(stageName))

                # save selected
                if application.FileSave(filePath, saveOptions):
                    control.StageControl.Items.removeAll()
                    control.GroundGeoControl.Items.removeAll()
                    cloneGround.FBDelete()
                    # script complete. give the user feedback
                    FBMessageBox("R* Prepare Lattice",
                                 'Lattice is ready for giant stage.\n\nSaved As:\n{0}'.format(str(filePath)), "OK")
                else:
                    # save failed. give the user feedback
                    FBMessageBox("R* Error", "Failed to save file: {0}".format(filePath), "OK")

        else:
            # script errored. give the user feedback & reload UI
            FBMessageBox("R* Error", "One or more of your ground objects are not part of the SetRoot. Exiting script.", "OK")
            reload(RS.Tools.UI.Mocap.Giant)
            RS.Tools.UI.Mocap.Giant.Run()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# PlotMocapAssets(): plots assets after mocap
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def PlotMocapAssets():

    plottedString = ""

    # Fix any cases of mulitple references
    RS.Core.Reference.Lib.rs_CheckForMultipleSceneReferences(True)

    # look for reference hierarchy
    referenceScene = None

    for component in Globals.gComponents:
        if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':
            referenceScene = component

    if referenceScene != None:
        # create asset lists and populate them
        refFullList = []

        refPropList = []
        refVehicleList = []
        refAnimalList = []

        RS.Utils.Scene.GetChildren(referenceScene, refFullList, "", False)

        if len(refFullList) > 0:
            for asset in refFullList:
                if asset.PropertyList.Find('rs_Asset_Type'):
                    typeProperty = asset.PropertyList.Find('rs_Asset_Type')
                    if typeProperty.Data == 'Props':
                        refPropList.append(asset)
                    if typeProperty.Data == 'Vehicles':
                        refVehicleList.append(asset)
                    if typeProperty.Data == 'Animal':
                        refAnimalList.append(asset)

        # plot characters (only if they are active with a character input)
        notPlottedList = []

        for character in Globals.gCharacters:
            if character.ActiveInput and character.InputType == FBCharacterInputType.kFBCharacterInputCharacter:

                # transfer over rtsource property before plotting
                inputCharacter = character.InputCharacter
                inputCharSkelRoot = inputCharacter.GetModel(FBBodyNodeId.kFBHipsNodeId)
                sourceCharSkelRoot = character.GetModel(FBBodyNodeId.kFBHipsNodeId)

                if inputCharSkelRoot:
                    originalRTSourceProperty = inputCharSkelRoot.PropertyList.Find('RTSource')

                    if originalRTSourceProperty:
                        newRTSourceProperty = sourceCharSkelRoot.PropertyCreate('RTSource', FBPropertyType.kFBPT_charptr,
                                                                                "", False, True, None)
                        newRTSourceProperty.Data = originalRTSourceProperty.Data

                # plot storytrack active chars
                RS.Utils.Scene.Plot.PlotStoryTrackCharacter(character)
                # now plot all other chars
                RS.Utils.Scene.Plot.PlotOnCharacterSkeleton(character)

                Globals.Scene.Evaluate()

                plottedString = plottedString + character.LongName + '\n'

        # plot prop, animal & vehicle list
        selectList = []

        RS.Utils.Scene.DeSelectAll()

        # props
        for component in Globals.gComponents:

            for prop in refPropList:
                if component.LongName == prop.Name + ':Dummy01' and component.ClassName() == "FBModelNull":
                    plottedString = plottedString + prop.Name + '\n'
                    RS.Utils.Scene.GetChildren(component, selectList, "", False)

            for vehicle in refVehicleList:
                if component.LongName == vehicle.Name + ':Dummy01' and component.ClassName() == "FBModelNull":
                    plottedString = plottedString + vehicle.Name + '\n'
                    RS.Utils.Scene.GetChildren(component, selectList, "", False)

            for animal in refAnimalList:
                if component.LongName == animal.Name + ':Dummy01' and component.ClassName() == "FBModelNull":
                    plottedString = plottedString + animal.Name + '\n'
                    RS.Utils.Scene.GetChildren(component, selectList, "", False)

        for item in selectList:
            item.Selected = True

        # plot selected
        RS.Utils.Scene.Plot.PlotCurrentTakeonSelected()

        # user feedback
        if plottedString == "":
            FBMessageBox('R*', 'Nothing has been plotted.', 'Ok')
        else:
            FBMessageBox('R*', 'Plotted the following assets:\n\n{0}'.format(plottedString), 'Ok')

    else:
        FBMessageBox("R* Error", "Unable to find Reference Hierarchy. Exiting script.", "OK")


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# SaveSelectedCameras(): cameras saved as selected
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def SaveSelectedCameras():

    cameraParentList = []
    cameraHierarchyList = []
    selectedList = []

    # deselect all items
    RS.Utils.Scene.DeSelectAll()

    # select cameras
    for camera in Globals.gCameras:
        if camera.SystemCamera:
            None
        elif camera.Name.lower() == 'movercamera' or '3lateral' in camera.Name.lower() or 'headcam' in camera.Name.lower():
            None
        else:
            parentNull = RS.Utils.Scene.GetParent(camera)
            cameraParentList.append(parentNull)

    FBCameraSwitcher().Selected = True

    for null in cameraHierarchyList:
        null.Selected = True

    # select camera hierarchies
    for parent in cameraParentList:
        RS.Utils.Scene.GetChildren(parent, cameraHierarchyList, "", True)

    # select components linked to cameras
    for null in cameraHierarchyList:
        for i in range(null.GetSrcCount()):
            null.GetSrc(i).Selected = True

        for i in range(null.GetDstCount()):
            null.GetDst(i).Selected = True

    # select folders for components
    for component in Globals.gComponents:
        if component.Selected:
            selectedList.append(component)

    for item in selectedList:
        if item.ClassName() == 'FBMaterial' or item.ClassName() == 'FBShader' or item.ClassName() == 'FBConstraint':
            for i in range(item.GetDstCount()):
                if item.GetDst(i).ClassName() == 'FBFolder':
                    item.GetDst(i).Selected = True

    # save selected
    application = FBApplication()

    currentFilePath = application.FBXFileName
    fileName = RS.Utils.Path.GetBaseNameNoExtension(FBApplication().FBXFileName)

    fileBuildString = ''
    for each in currentFilePath.split('\\')[0:-1]:
        fileBuildString = fileBuildString + str(each) + '\\'

    saveSelectedPath = fileBuildString + fileName + '_CameraBackup.fbx'

    saveOptions = FBFbxOptions(False)
    saveOptions.SaveSelectedModelsOnly = True
    saveOptions.UseASCIIFormat = True
    saveOptions.BaseCameras = True
    saveOptions.CameraSwitcherSettings = True

    application.FileSave(saveSelectedPath, saveOptions)

    # deselect all items
    RS.Utils.Scene.DeSelectAll()

    # user feedback
    FBMessageBox('R* MocapTools: Saved Cams', 'Selected Cameras, along with their components, saved as:\n\n{0}'.format(
        saveSelectedPath), 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# gsSkelInput(): Merges in gs Skels and Matches Character-to-Character Inputs
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def gsSkelInput(mergePath, origCharacterNamespace, origCharacterNode):

    # request user to enter actor's name - this will be added to the front of the namespace
    actorName = FBMessageBoxGetUserValue("Enter Actor Name", "Actor Name: ", "", FBPopupInputType.kFBPopupString, "Ok")

    # merge file
    options = FBFbxOptions(True)
    if actorName[-1] != '':
        options.NamespaceList = actorName[-1] + ":" + origCharacterNamespace
    else:
        options.NamespaceList = origCharacterNamespace
    FBApplication().FileMerge(mergePath, False, options)

    # activate character input
    gsCharacter = Globals.Scene.Characters[-1]
    origCharacterNode.InputCharacter = gsCharacter
    origCharacterNode.InputType = FBCharacterInputType.kFBCharacterInputCharacter
    origCharacterNode.ActiveInput = True

    Globals.Scene.Evaluate()

def gsSkelMatchMerge():

    # create a dictionary with each character and names
    characterDict = {}

    for character in Globals.gCharacters:

        # ignore any preexisting '_gs' chars in the scene
        if '_Skel_gs' in character.LongName or '_gs' in character.LongName:
            None

        # ignore any character with an existing active character input
        elif character.ActiveInput  and character.InputType == FBCharacterInputType.kFBCharacterInputCharacter:
            None

        else:
            splitName = character.LongName.split(':')
            characterDict[character.LongName] = (splitName[0], splitName[-1], character)

    for key, value in characterDict.iteritems():
        # dict variables
        dupeSuffix = ''
        longName = key
        if '^' in value[0]:
            split = value[0].split('^')
            namespace = split[0]
            dupeSuffix = "^" + split[-1]
        else:
            namespace = value[0]
        name = value[1]
        characterNode = value[2]

        # search for a matching gs skeleton
        basePath = '{0}\\'.format(ProjectData.data.GetGSSkelFolder())
        suffixName = '_Skel_gs.fbx'
        mergePath = basePath + namespace + suffixName

        filePath = FBFilePopup()
        filePath.Caption = "Select a gs Skeleton to match with " + longName
        filePath.Style = FBFilePopupStyle.kFBFilePopupOpen

        filePath.Filter = "*"
        filePath.Path = basePath

        filePath.Execute()

        selectedPath = filePath.FullFilename

        if '.fbx' in selectedPath.lower():

            gsNamespace = namespace + dupeSuffix + '_Skel_gs'
            gsSkelInput(selectedPath, gsNamespace, characterNode)

            # SNAP EVERYTHING TO THE ACTIVE STAGE
            # find active stage
            lActiveStage = FindActiveStage()

            # find offset null of the gs skel
            gsCharacter = Globals.Scene.Characters[-1]
            hips = gsCharacter.GetModel(FBBodyNodeId.kFBHipsNodeId)
            parent = RS.Utils.Scene.GetParent(hips)

            # Active Stage Found
            if lActiveStage:
                RS.Utils.Scene.AlignTranslation(parent, lActiveStage, True, True, True)

            # Stages found, but none marked as active.
            elif not lActiveStage:
                lStages = GetStages()
                lStages.sort()
                result = RS.Utils.Widgets.RsChoiceDialog(
                    "Choose which stage you would like to make the Active Stage?", lStages)

                if result.value != None:
                    lTargetStage = FBFindModelByLabelName(result.value)
                    if lTargetStage:
                        # Set the Active Stage so you don't have to do it again
                        SetActiveStage(lTargetStage)
                        RS.Utils.Scene.AlignTranslation(parent, lTargetStage, True, True, True)

        else:
            FBMessageBox('R* Error',
                         'You did not select an .fbx file.\n\nNo gs Character will be brought in for {0}'.format(
                             namespace), 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# RTCOffset(): CamRock Offset & scene camera snap and constrain based on user input from RTCCamRockSnapTool
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def RTCOffset(offsetObject, camObject, noneConstraintBool=True, pcConstraintBool=False, positionConstraintBool=False):

    # align cam
    offsetObjectNull = FBFindModelByLabelName(offsetObject)
    camObjectNull = FBFindModelByLabelName(camObject)

    RS.Utils.Scene.Align(offsetObjectNull, camObjectNull)
    Globals.Scene.Evaluate()

    # constraint creation
    if noneConstraintBool:
        None

    elif pcConstraintBool:
        pcConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        pcConstraint.Name = ("RTC_" + offsetObject + "_pcConstraint")
        pcConstraint.ReferenceAdd (0, offsetObjectNull)
        pcConstraint.ReferenceAdd (1, camObjectNull)
        pcConstraint.Snap()

    elif positionConstraintBool:
        positionConstraint = Constraint.CreateConstraint(Constraint.POSITION)
        positionConstraint.Name = ("RTC_" + offsetObject + "_positionConstraint")
        positionConstraint.ReferenceAdd (0, offsetObjectNull)
        positionConstraint.ReferenceAdd (1, camObjectNull)
        positionConstraint.Snap()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# RTCRangeSave(): Outputs current hard and soft ranges to a .txt file
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def RTCRangeSave():

    hardRangeList = []
    softRangeList = []

    # hard ranges
    startTime = FBPlayerControl().LoopStart
    startHardFrame = startTime.GetTimeString()
    hardRangeList.append(startHardFrame)

    stopTime = FBPlayerControl().LoopStop
    stopHardFrame = stopTime.GetTimeString()
    hardRangeList.append(stopHardFrame)

    # soft ranges
    startZoomTime = FBPlayerControl().ZoomWindowStart
    startSoftFrame = startZoomTime.GetFrame()
    softRangeList.append(startSoftFrame)

    stopZoomTime = FBPlayerControl().ZoomWindowStop
    stopSoftFrame = stopZoomTime.GetFrame()
    softRangeList.append(stopSoftFrame)

    # temporary section until David Bailey outputs the original hard and soft ranges to the cutscene database
    # currentFilePath = FBApplication().FBXFileName
    # split = currentFilePath.lower().split(".fbx")
    # txtPath = split[0] + '.txt'
    txtPath = 'C:\\rtc_ranges.txt'
    try:
        with open(txtPath, 'w') as txtFile:
            txtFile.write(str(hardRangeList))
            txtFile.write(str(softRangeList))
    except IOError, err:
        print err


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# RTCRangeLoad(): uploads existing .txt output (fromRTCRangeSave) and sets the hard and soft ranges based on
# the .txt info
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def RTCRangeLoad():

    # currentFilePath = FBApplication().FBXFileName
    # split = currentFilePath.lower().split(".fbx")
    # txtPath = split[0] + '.txt'
    txtPath = 'C:\\rtc_ranges.txt'

    try:
        # open txt
        txtFile = open(txtPath, 'r')
        readTxt = txtFile.read()

        finished = False
        result = []

        while not finished:
            idx = readTxt.find(']')

            try:
                listPart = eval(readTxt[:idx + 1])
                result.append(listPart)

            except:
                pass

            readTxt = readTxt[idx + 1:]

            if idx == -1:
                finished = True

        # populate lists
        hardRangeList, softRangeList = result

        txtFile.close()

        # set hard and soft ranges
        FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan(FBTime(0, 0, 0, int(hardRangeList[0])),
                                                          FBTime(0, 0, 0, int(hardRangeList[-1])))

        startTime = FBTime(0, 0, 0, int(softRangeList[0]))
        startTime = startTime.Get()
        FBPlayerControl().ZoomWindowStart = FBTime(startTime)

        stopTime = FBTime(0, 0, 0, int(softRangeList[-1]))
        stopTime = stopTime.Get()
        FBPlayerControl().ZoomWindowStop = FBTime(stopTime)

        # no longer removing txt file
        # os.remove(txtPath)

    except IOError, err:
        FBMessageBox('R* Error', 'Unable to find file: {0}'.format(txtPath), 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# CamRockBake(): Plotting CamRocks to a new camera
#                updated from Mondo's original bake scripts
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def CamRockBake(AllKeys=True, Plot=True, Frame=0):

    RS.Utils.Scene.DeSelectAll()

    # Field of View index
    LensIndex = {69.98: 0,
                 47.85: 1,
                 36.99: 2,
                 25.24: 3,
                 17.10: 4,
                 10.00: 5}

    validCameraNames = ['camrock1_cam', 'camrock2_cam', 'camrock3_cam', 'camrock4_cam', 'camrocktab_cam']

    # *NOTE* will need to change prop list soon due to new custom properties added url:bugstar:1733892

    # Properties whose values we can just copy to the duplicate camera
    settableProperties = ['FrameSizeMode', 'ResolutionMode', 'ResolutionHeight',
                         'PixelAspectRatio', 'FarPlaneDistance', 'FilmBackType']

    # Properties that should be connected using a relation constraint
    constraintProperties = ['Translation', 'Rotation', 'Lens', 'FieldOfView', 'FOCUS (cm)', 'CoC', 'CoC Override',
                            'CoC Night', 'CoC Night Override', 'Near Outer Override', 'Near Inner Override',
                            'Far Outer Override', 'Far Inner Override', 'Motion_Blur', 'Shallow_DOF', 'Simple_DOF']

    for camera in Globals.gCameras:

        if not camera.Name.lower() in validCameraNames: continue

        currentCamera = camera

        # add new cam
        RS.Core.Camera.Lib.camUpdate.CreateCustomCamera()
        newCamera = Globals.gCameras[-1]

        # add custom property for where cam originally came from
        camRockProp = newCamera.PropertyCreate('Original CamRock', FBPropertyType.kFBPT_charptr, "",
                                                False, True, None)
        camRockProp.Data = camera.Name

        # assign currentCam's properties to newCamera
        for property in settableProperties + constraintProperties * AllKeys:
            currentCamProperty = currentCamera.PropertyList.Find(property)
            newCamProperty = newCamera.PropertyList.Find(property)
            if all([currentCamProperty, newCamProperty]):
                if type(newCamProperty) not in [FBPropertyEnum, FBPropertyDouble, FBPropertyVector3d]:
                    newCamera.PropertyList.Find(property).SetAnimated(True)
                newCamProperty.Data = currentCamProperty.Data

        newCamera.FrameColor = currentCamera.FrameColor / 2
        newCamera.UseFrameColor = True

        if not AllKeys:
            # This is to make sure the camera moves the correct spot
            mtrx = FBMatrix()
            currentCamera.GetMatrix(mtrx)
            newCamera.SetMatrix(mtrx)

            # Set the FieldOfView by setting up the proper Lens value
            FOV = round(float(currentCamera.FieldOfView), 2)
            newCamera.PropertyList.Find("Lens").Data = LensIndex.get(FOV, 6)
            newCamera.PropertyList.Find("FieldOfView").Data = FOV
            if Plot: RS.Core.Animation.Lib.PlotOnFrame(newCamera,
                                                       constraintProperties + ["Scaling"], Frame, False)

        if AllKeys and Plot:

            # relation constraint to pass property data through from currentCam to newCam
            tempConstraint = FBConstraintRelation('TempRelation')

            currentCameraBox = tempConstraint.SetAsSource(currentCamera)
            currentCameraBox.UseGlobalTransforms = True
            tempConstraint.SetBoxPosition(currentCameraBox, 0, 0)

            newCameraBox = tempConstraint.ConstrainObject(newCamera)
            newCameraBox.UseGlobalTransforms = True
            tempConstraint.SetBoxPosition(newCameraBox, 500, 0)

            for property in constraintProperties:
                newCamProperty = newCamera.PropertyList.Find(property)
                if newCamProperty:
                    if type(newCamProperty) not in [FBPropertyEnum, FBPropertyDouble, FBPropertyVector3d]:
                        newCamProperty.SetAnimated(True)
                    currentCameraBoxOUT = RS.Utils.Scene.FindAnimationNode(currentCameraBox.AnimationNodeOutGet(),
                                                                           property)
                    newCameraBoxIN = RS.Utils.Scene.FindAnimationNode(newCameraBox.AnimationNodeInGet(), property)

                    if currentCameraBoxOUT and newCameraBoxIN:
                        FBConnect(currentCameraBoxOUT, newCameraBoxIN)

            tempConstraint.Active = True
            newCamera.Selected = True

            # plot

            plotOptions = FBPlotOptions ()
            plotOptions.UseConstantKeyReducer = False
            plotOptions.ConstantKeyReducerKeepOneKey = False
            plotOptions.PlotOnFrame = True
            plotOptions.PlotPeriod = FBTime (0, 0, 0, 1)

            currentTake = FBSystem().CurrentTake
            currentTake.PlotTakeOnSelected(plotOptions)

            # delete temporary constraint
            tempConstraint.Active = False
            tempConstraint.FBDelete()

            # rename the new cam to match the camrock name
            newCamera.Name = currentCamera.Name + '_RSCamera'

        newCamera.Selected = False
        return newCamera

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# ZeroOutScene(): Zero's Out the Desired Action
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

'''
auto bleep find not working, so only working off user input frame value for now
'''


def ZeroOutScene(frame, showMessage=True):

    if frame != 0:
        # get filter
        filterManager = FBFilterManager()
        timeShiftScaleFilter = filterManager.CreateFilter('Time Shift And Scale')
        # set shift property
        propShift = timeShiftScaleFilter.PropertyList.Find('Shift')
        # needs to be negative
        propShift.Data = FBTime(0, 0, 0, -(int(frame)))
        for i in Globals.Scene.Components:
            if i.ClassName() != 'FBMidiInstrument':
                for prop in i.PropertyList:
                    if prop.IsAnimatable() and prop.IsAnimated() and prop.GetAnimationNode():
                        timeShiftScaleFilter.Apply(prop.GetAnimationNode(), True)

        if showMessage:
            FBMessageBox('R*', 'Scene Time Shifted:  -{0}'.format(frame), 'Ok')

        timeShiftScaleFilter.FBDelete()
        Globals.Scene.Evaluate()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# ChangeKeyboardConfiguration(): Changes the product you want use for the keyboard shortcuts
# By The One and Only Amazing Geoff 'The Boss' Samuel *Crowd cheers*!!!!!
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def ChangeKeyboardConfiguration(appChoose=InteractionModes.MotionBuilder):
    """
    Change the Keyboard Configuration

    kwargs:
        appChoose(MocapCommands.InteractionModes/str): The app to use
    """
    _getActionManager = "?Object_GetActionManager@@YAPEAVKActionManager@@XZ"
    _loadConfiguration = "?LoadConfiguration@KActionManager@@QEAA_NPEBD@Z"

    handle = ctypes.windll.kernel32.GetModuleHandleA("object.dll")
    if handle is False:
        error = ctypes.GetLastError()
        logging.error("{0} - {1}".format(str(error), ctypes.FormatError(error)))

    actionManager = getattr(ctypes.cdll.object, _getActionManager)()
    loadConfigurationPtr = getattr(ctypes.cdll.object, _loadConfiguration)
    loadConfigurationPtr(actionManager, appChoose)

    config = mobu.FBConfigFile("@Application.txt")
    if config:
        config.Set("Keyboard", "Default", appChoose)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# FindActiveStage():
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def FindActiveStage():
    lStages = True
    # If there are multiple stege in the scene
    for lModel in Globals.Scene.Components:
        if lModel:
            lProp = lModel.PropertyList.Find("Stages")
            if lProp and lProp.Data in ["R*GC_Stage", "R*North_Stage"]:
                lStages = False
                lActiveStage = lModel.PropertyList.Find("Active Stage")
                if lActiveStage and lActiveStage.Data:
                    return lModel
    if not lStages:
        return False
    return None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GetStages():
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def GetStages():
    lSceneStage = []
    # Get all the stages in the scene.
    for lModel in Globals.Scene.Components:
        if lModel:
            lProp = lModel.PropertyList.Find("Stages")
            if lProp and lProp.Data in ["R*GC_Stage", "R*North_Stage"]:
                lSceneStage.append(lModel.Name)
    return lSceneStage

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# SetActiveStage():
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def SetActiveStage(pTargetStage):
    # Set the Active Stage so you don't have to do it again
    if pTargetStage:
        targetStageProperty = pTargetStage.PropertyList.Find("Active Stage")
        if targetStageProperty:
            targetStageProperty.Data = True

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# PreVizSetup():
# By Kristine Middlemiss
#
# 1. Button for Add Base_PreViz (x:\gta5\art\animation\resources\mocap\previz\_BASE_PREVIZ.fbx)
# 2. Default Merge Properties need the merge all camera settings for the HUD to come through
# 3. Snapped to Active Stage (use Align code from RTE Base Setup)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def PreVizSetup(currentStudio, stage):
    options = FBFbxOptions(True) # True for Load, not Save
    options.BaseCameras = True # Need Producer Perspective
    options.CameraSwitcherSettings = False
    options.CurrentCameraSettings = False
    options.GlobalLightingSettings = False
    options.TransportSettings = False
    lApp = FBApplication()

    # glen cove base previz
    if currentStudio == const.Studio.NewYork:
        # get latest on base file and merge
        lBasePreVizPath = 'x:\\virtualproduction\\previz\\nyc\\basePreviz\\_BASE_PREVIZ.fbx'
        RS.Perforce.Sync(lBasePreVizPath)
        lApp.FileMerge(lBasePreVizPath, False, options)

        # Gather up all the BasePreviz Stuff
        lBasePrevizList = []
        for lModel in Globals.Scene.Components:
            if lModel and lModel.PropertyList.Find("BasePreviz"):
                if lModel.Name.endswith("_null") or lModel.Name.endswith("_camera"):
                    lBasePrevizList.append(lModel)

        # snap to active stage
        activeStage = stage
        if activeStage:
            for item in lBasePrevizList:
                RS.Utils.Scene.AlignTranslation(item, activeStage, True, True, True)

    elif currentStudio == const.Studio.North:
        lBasePreVizPath = 'x:\\virtualproduction\\previz\\north\\basePreviz\\_BASE_PREVIZ_NORTH.fbx'
        RS.Perforce.Sync(lBasePreVizPath)
        lApp.FileMerge(lBasePreVizPath, False, options)

        # Gather up all the BasePreviz Stuff
        lBasePrevizList = []
        for lModel in Globals.Scene.Components:
            if lModel and lModel.PropertyList.Find("BasePreviz"):
                if lModel.Name.endswith("_null") or lModel.Name.endswith("_camera"):
                    lBasePrevizList.append(lModel)

        # snap to active stage
        activeStage = stage
        if activeStage:
            for item in lBasePrevizList:
                RS.Utils.Scene.AlignTranslation(item, activeStage, True, True, True)

    else:
        FBMessageBox('R* Error', 'Cannot find selected Base Previz.', 'Ok')

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# RTEBaseSetup():
# By Kristine Middlemiss
#
# 1. Create from scratch (using the Camera ToolBox tool) the number of camera's you want based on your UI input
#    (the cameras will no longer reside in the file "_RTE_BASE.fbx", I will remove them).
# 2. Put the create cameras and their constraint in folders.
# 3. Select all of the RTE base cameras and align -> translation to the stage, so that the cameras are easier for
#    the director/camera person to frame within your scene.
#      a. I am thinking of adding a checkbox (default True) so you can choose.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def rs_Folders(pFolderName, folderType):
    lFolder = None

    for iFolder in Globals.gFolders:
        if iFolder.Name == pFolderName:
            lFolder = iFolder

    if lFolder == None:
        if folderType == "FBCamera":
            lPlaceholder = FBCamera('Remove_Me')
        elif folderType == "FBConstraint":
            lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder(pFolderName, lPlaceholder)
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"

        Globals.Scene.Evaluate()
        lPlaceholder.FBDelete()

    return lFolder


def RTEBaseSetup(CameraCreateNum):

    FBMergeTransactionBegin()

    # Right now if no cameras are created none of this script is completed.
    if CameraCreateNum != None or CameraCreateNum > 0:

        lCameraObjList = []
        lPlanesInList = []

        # CAMERA CREATION
        for lCam in range(CameraCreateNum):

            # create a RTE_Cams folders if there is not one, or just get the handle to the current one if there
            # is already one.
            lCameraFolder = rs_Folders('RTE_Cams', "FBCamera")
            lConstraintFolder = rs_Folders('RTE_Constraints', "FBConstraint")

            # Create Custom Camera
            lNewCamera = camCore.camUpdate.CreateCustomCamera()
            lCameraObjList.append(lNewCamera)

            # Add the Camera to the camera Folder
            lCameraFolder.Items.append(lNewCamera)
            for i in range(lNewCamera.GetSrcCount()):
                if "Relation" in lNewCamera.GetSrc(i).Name:
                    # Add the Camera Constraint to the Constraint Folder
                    lConstraintFolder.Items.append(lNewCamera.GetSrc(i))

        # align to active stage, if there is no active stage, add to origin as normal
            stageList = [component for component in Globals.Components
                         if isinstance(component, FBModel)
                         and component.PropertyList.Find('Active Stage') is not None]

            activeStage = None
            for stage in stageList:
                if stage.PropertyList.Find('Active Stage').Data == 1:
                    activeStage = stage
                    break

            if activeStage:
                # Align each camera in the list
                for lCam in lCameraObjList:
                    RS.Utils.Scene.AlignTranslation(lCam, activeStage, True, True, True)

        # Add the ActionSlateHUD to the RTE Cameras
        for lHud in Globals.Scene.HUDs:
            if lHud.Name == 'ActionSlateHUD':
                for lCam in lCameraObjList:
                    lCam.ConnectSrc(lHud) # Connect to Custom camera

    FBMergeTransactionEnd()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# PlotPreviz():
# By Kristine Middlemiss
#
# After we have a select(the animation you like), the steps are as follows:
# 1. go offline with all faceware devices (there will be one per character)
#
# 2. select the faceware heads (faceware heads for each character are in a hierarchy that starts with Player_Name_Scale
# and are named Player_Name_Head)
# They are named according to the namespace on the character (ie if there are multiple Male_Skeleton characters in would
#  be Male_Skeleton_Head, Male_Skeleton^1_Head, etc)
#
# 3. Plot Selected with settings: plot on frame checked, plot all takes checked, constant key reducer NOT checked
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


def PlotFacewareLiveHeads():

    # TURN OFF ALL DEVICES
    GiantDeviceOff()
    NonGiantDevicesOff()

    # MAKE SURE NOTHING IS SELECTED
    for lConst in Globals.gComponents:
        if lConst.Selected:
            lConst.Selected = False

    # SELECT CAMERA SWITCHER
    lModelList = FBComponentList()
    FBFindObjectsByName("Camera Switcher", lModelList, False, False)
    if len(lModelList) == 1:
        lModelList[0].Selected = True

    # PLOT SELECTED (CAMERA SWITCHER (ALL PROPERTIES)...
    lOptions = FBPlotOptions()
    lOptions.mPlotOnFrame = True
    lOptions.mPlotAllTakes = True
    lOptions.mUseConstantKeyReducer = False
    lSystem = FBSystem()
    lSystem.CurrentTake.PlotTakeOnSelected(lOptions)


class WatchstarTrial(object):
    """
    Helper class to deal with watchstar trials
    """
    __metaclass__ = singleton.Singleton

    def __init__(self, stage=None):
        """
        Constructor
        """
        super(WatchstarTrial, self).__init__()
        self._settings = QtCore.QSettings("RockstarSettings", "WatchstarTrial")
        lastStageId = self._settings.value("stageId")
        lastStage = None
        if lastStageId is not None:
            stageDict = {}
            for loc in Context.animData.getAllLocations():
                stageDict.update({int(stg.stageId):stg for stg in loc.getStages()})
            lastStage = stageDict.get(int(lastStageId))

        self._stage = stage or lastStage or self._getLocalStage()
        self._currentTrial = None

    def _getLocalStage(self):
        currentLoc = Context.animData.getCurrentLocation()
        if currentLoc is None:
            return None
        stages = currentLoc.getStages()
        if len(stages) == 0:
            return None
        return stages[0]

    @property
    def stage(self):
        return self._stage

    @stage.setter
    def stage(self, stage):
        self._stage = stage
        stageId = None
        if stage is not None:
            stageId = stage.stageId
        self._settings.setValue("stageId", stageId)

    def _generateUniqueTakeName(self, intputName):
        takeName = str(intputName)
        for take in Globals.System.Scene.Takes:
            if take.Name == takeName:
                takeName = takeName.split("_")
                ver = int(takeName[1])
                if ver + 1 < 10:
                    takeName[1] = "0" + str(ver + 1)
                else:
                    takeName[1] = str(ver + 1)
                takeName = "_".join(takeName)
                takeName = self._generateUniqueTakeName(takeName)
                break

        return takeName

    def _addToClipBoard(self, text):
        clipboard = QtGui.QApplication.clipboard()
        clipboard.setText(text)

    def grabTake(self):
        """
        Grab the current Take from the set stage
        """
        if self._stage is None:
            raise ValueError("No Stage Set")

        return Context.animData.getCurrentTrialByStage(self._stage)
        
    def grabTakeName(self, layer=False, rawName=False):
        """
        Grab the current Take name from the set stage
        """
        if self._stage is None:
            raise ValueError("No Stage Set")

        trial = Context.animData.getCurrentTrialByStage(self._stage)
        if trial is None:
            return None

        trialName = trial.name
        if rawName:
            self._addToClipBoard(trialName)
            return trialName

        # Need to Take Number Functionality.
        newTakeName = self._generateUniqueTakeName(trialName)

        if layer:
            Globals.System.CurrentTake.CopyTake(newTakeName)
        else:
            newTake = FBTake(newTakeName)
            Globals.System.Scene.Takes.append(newTake)
            Globals.System.CurrentTake = newTake

        # Copy into the Windows Clip Board so people can copy to a bug and Rag
        self._addToClipBoard(newTakeName)

        return newTakeName


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# TurnOffProducePerspectiveLabel(): Turns off the label name in the Viewer for the Producer Perspective,
#  it overlaps the new HUD
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def TurnOffProducePerspectiveLabel ():
    for lCam in Globals.Scene.Cameras:
        if lCam.Name == "Producer Perspective":
            lProp = lCam.PropertyList.Find("ShowName")
            if lProp:
                if lProp.Data:
                    lProp.Data = False
                else:
                    lProp.Data = True


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Turn the selected object(s) red by changing the diffuse property
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def TurnRedToggle():

    lSceneList = FBModelList()
    FBGetSelectedModels (lSceneList, None, True)

    # find selected
    if len(lSceneList) == 0:
        FBMessageBox("Toggle Red", "Nothing was selected", "OK")
        return

    else:
        # find/create red material
        lFound = False
        redMaterial = None

        for material in Globals.Scene.Materials:
            if material.Name == "Red Material":
                lFound = True
                redMaterial = material
                redMaterial.Diffuse = FBColor(1, 0, 0)
                break

        if not lFound:
            redMaterial = FBMaterial("Red Material")
            redMaterial.Diffuse = FBColor(1, 0, 0)

        # get selected
        for selected in lSceneList:
            # find selected's parent
            selectedParent = RS.Utils.Scene.GetParent(selected)
            # unparent for cloning
            selected.Parent = None
            # clone object
            clonedSelected = selected.Clone()
            clonedSelected.ProcessObjectNamespace(FBNamespaceAction.kFBRemoveAllNamespace, '', '')
            # reparent original object
            selected.Parent = selectedParent
            # add property to retain original object's name
            clonedProperty = clonedSelected.PropertyCreate(
                                            'ClonedSetObject',
                                            FBPropertyType.kFBPT_charptr, "",
                                            False,
                                            True,
                                            None
                                        )
            clonedProperty.Data = selected.LongName
            # detatch existing material
            if int(clonedSelected.GetSrcCount()) > 0:
                for idx in reversed(xrange(clonedSelected.GetSrcCount())):
                    if clonedSelected.GetSrc(idx).ClassName() == 'FBMaterial':
                        clonedSelected.DisconnectSrcAt(idx)
                # apply new red material
                clonedSelected.ConnectSrc(redMaterial)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Open the C++ Multi Camera Viewer.
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def OpenMultiCameraViewer():

    # Open Multi Camera Viewer
    FBPopNormalTool ("Multi Camera Viewer")


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Get Prop Toy Files
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def GetPropToys(currentStudio):

    PathDict = {}
    lRootPathList = []

    if currentStudio == const.Studio.NewYork:
        lRootPathList = ['x:\\virtualproduction\\previz\\nyc\\propToys\\',
                         'x:\\virtualproduction\\previz\\globalAssets\\propClones\\']
    if currentStudio == const.Studio.North:
        lRootPathList = ['x:\\virtualproduction\\previz\\north\\propToys\\',
                         'x:\\virtualproduction\\previz\\globalAssets\\propClones\\']

    for rootPath in lRootPathList:
        lFilePaths = glob.glob(rootPath + "*.fbx")
        for i in lFilePaths:
            Perforce.Sync(i)
            FileName = i.split(rootPath)[1]
            PropName = FileName.split(".")[0]
            if PropName.startswith("CAM_ROCK") or PropName.startswith('CamRock') or PropName.startswith('Deck') or \
                            PropName == '_Slate' or PropName == 'ROCK_CAMS_ALL_NEW' or PropName == 'gs_template' or \
                            PropName == '_BASE_PREVIZ' or PropName == 'ActionSlate_02':
                pass
            else:
                PathDict[PropName] = i

    return PathDict


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Get Scene Prop Toy
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def GetPropToysinScene():
    scenePropToys = []
    modelList = []
    # model list is only up to 2 children - mocap properties are on the child of the root model
    for model in Globals.Scene.RootModel.Children:
        modelList.append(model)
        for subChild in [child for child in model.Children]:
            modelList.append(subChild)
            modelList.extend([child for child in subChild.Children])
    # iterate through model list
    for model in modelList:
        propToyProperty = model.PropertyList.Find('Mocap PropToy')
        if propToyProperty:
            scenePropToys.append(model.Name)
    return scenePropToys


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Get Scene Prop Toy
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def GetNonPropToyAssets():

    nonPropToyAssetDict = {}
    refNullList = []
    sceneAssetList = []
    referenceScene = None

    for component in Globals.gComponents:
        if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':
            referenceScene = component

    if referenceScene:
        RS.Utils.Scene.GetChildren(referenceScene, refNullList, "", False)

    if refNullList:
        for i in refNullList:
            sceneRef = RS.Utils.Scene.FindModelByName(i.Name + ':Dummy01', True)
            if sceneRef:
                sceneAssetList.append(sceneRef)
            elif RS.Utils.Scene.FindModelByName(i.Name + ':SetRoot', True):
                sceneRef = RS.Utils.Scene.FindModelByName(i.Name + ':SetRoot', True)
                sceneAssetList.append(sceneRef)

    for i in sceneAssetList:
        childList = []
        RS.Utils.Scene.GetChildren(i, childList, "", False)
        nonPropToyAssetDict[i] = childList

    return nonPropToyAssetDict


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Add Prop to the scene
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def addProp(propPath, stage):

    # GET LATEST AND MERGE IN THE PROPS

    if os.path.exists(propPath):
        RS.Perforce.Sync(propPath)

        options = FBFbxOptions(True) # True for Load, not Save
        options.BaseCameras = False # Need Producer Perspective
        options.NamespaceList = "PlaceholderNamespace" # add namespace so we can find it after merge
        options.CameraSwitcherSettings = False
        options.CurrentCameraSettings = False
        options.GlobalLightingSettings = False
        options.TransportSettings = False

        lApp = FBApplication()
        lApp.FileMerge(propPath, False, options)

        # find prop via temporary namespace, get parent(s), set property, remove temp namespace
        propToyComponentList = []
        propParentList = []
        for i in Globals.gComponents:
            if i.LongName.startswith("PlaceholderNamespace"):
                if i.ClassName() in ['FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMaterial', 'FBTexture',
                                      'FBVideoClipImage', 'FBCharacter', 'FBGroup', 'FBMesh', 'FBModelRoot', 'FBDevice',
                                      'FBNote', 'FBStoryTrack', 'FBConstraintRelation', 'FBConstraint', 'FBFolder', 'FBSet',
                                      'FBShader', 'FBHandle', 'FBCamera']:
                    propToyComponentList.append(i)

        for i in propToyComponentList:
            if i.ClassName() in ['FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMesh', 'FBModelRoot']:
                propParentNull = RS.Utils.Scene.GetParent(i)
                if propParentNull:
                    propParentList.append(propParentNull)

            if '_bone' in i.Name.lower():
                tag = i.PropertyCreate('Mocap PropToy', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Constraint Node'
            else:
                if i.ClassName() in ['FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMesh', 'FBModelRoot']:
                    propParentNull = RS.Utils.Scene.GetParent(i)
                    if propParentNull:
                        tag = propParentNull.PropertyCreate('Mocap PropToy', FBPropertyType.kFBPT_charptr, "", False, True,
                                                            None)
                        tag.Data = 'Constraint Node'

            i.ProcessObjectNamespace(FBNamespaceAction.kFBRemoveAllNamespace, '', '')

        if propParentList:
            # snap to active stage
            activeStage = stage
            # Active Stage Found
            if activeStage:
                for prop in propParentList:
                    RS.Utils.Scene.AlignTranslation(prop, activeStage, True, True, True)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Create Prop Constraint
# By Kristine Middlemiss
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def CreateConstraint(pChild, pParent, pType):
    lConstraint = Constraint.CreateConstraint(pType)
    lConstraint.Name = pChild.encode('ascii')
    lParentObj = FBFindModelByLabelName(pParent.encode('ascii'))
    lChildObj = FBFindModelByLabelName(pChild.encode('ascii'))
    if lParentObj and lChildObj:
        lConstraint.ReferenceAdd (0, lChildObj)
        lConstraint.ReferenceAdd (1, lParentObj)
        # match object translation to proptoy
        RS.Utils.Scene.Align(lChildObj, lParentObj, True, True, True, False, False, False)
    return lConstraint

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Ref Pose Defs for MocapToolbox
# By Kat url:bugstar:1783551
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

'''
merge ref pose callback
'''
def MergeRefPose(CharacterCheckboxList):

    '''
    CharacterCheckboxList -
    CharacterCheckBoxObject: reference to the checkbox object. Used in Toolbox script, not this one.
    GenderComboBoxObject: reference to the combobox object. Used in Toolbox script, not this one.
    CheckboxBool: Check or Unchecked
    CharacterString: Checkbox text (character name)
    GenderString: Female or Male
    CharacterNode: reference to the character node related to the checkbox
    CharacterRGBColor: int r, g, b
    '''
    FBMergeTransactionBegin()
    for item in CharacterCheckboxList:
        if item.CheckboxBool:
            for idx in range(item.RefCountInt):
                refGroupList = []
                refPosePath = None

                if item.GenderString == "Male":
                    refPosePath = '{0}/art/animation/resources/mocap/REF_POSES/MALE_REF_POSE_01.fbx'.format(RS.Config.Project.Path.Root)
                else:
                    refPosePath = '{0}/art/animation/resources/mocap/REF_POSES/FEM_REF_POSE_01.fbx'.format(RS.Config.Project.Path.Root)

                if os.path.exists(refPosePath):
                    RS.Perforce.Sync(refPosePath)

                    options = FBFbxOptions(True, refPosePath) # True for Load, not Save
                    options.BaseCameras = False
                    options.CameraSwitcherSettings = False
                    options.CurrentCameraSettings = False
                    options.GlobalLightingSettings = False
                    options.TransportSettings = False
                    options.SetTakeSelect(0, False)
                    options.NamespaceList = "PlaceholderNamespace" # placeholder namespace so we can find merged-in items, this will be removed once we find them again

                    app = FBApplication()

                    app.FileMerge(refPosePath, False, options)

                    # ref pose gather ref pose info
                    refPoseComponentList = []

                    for sceneComp in Globals.gComponents:
                        if sceneComp.LongName.startswith("PlaceholderNamespace"):
                            if sceneComp.ClassName() in ['FBModelNull', 'FBModel', 'FBModelSkeleton', 'FBMaterial', 'FBTexture', 'FBVideoClipImage', 'FBCharacter', 'FBGroup', 'FBShader', 'FBShaderLighted', 'FBFolder']:
                                refPoseComponentList.append(sceneComp)

                    for refComp in refPoseComponentList:

                        refShader = None
                        refMaterial = None
                        characterTarget = None

                        if item.CharacterRGBColor:
                            split = item.CharacterRGBColor.split(', ')
                            r = round(int(split[0]) / 256.0, 3)
                            g = round(int(split[1]) / 256.0, 3)
                            b = round(int(split[2]) / 256.0, 3)
                            if refComp.ClassName() == 'FBMaterial':
                                refMaterial = refComp
                                refComp.Diffuse = FBColor(r, g, b)
                                refComp.Ambient = FBColor(r, g, b)
                                refComp.Specular = FBColor(0, 0, 0)
                            if refComp.ClassName() == 'FBShaderLighted':
                                refShader = refComp

                        if refComp.ClassName() == 'FBGroup':
                            refGroupList.append(refComp)

                        if refComp.ClassName() == 'FBCharacter':
                            characterTarget = refComp

                        # if there are multiples of a ref pose colour, we want to adjust the lightness of the colour
                        # (limit of 6)
                        if refShader and refMaterial:
                            materialCounter = 0
                            for material in Globals.Materials:
                                if material == refMaterial:
                                    None
                                else:
                                    if round(material.Diffuse[0], 3) == r and round(material.Diffuse[1], 3) == g and \
                                                    round(material.Diffuse[2], 3) == b:
                                        materialCounter = materialCounter + 1

                            luminosityProperty = refShader.PropertyList.Find('Luminosity')
                            if luminosityProperty and materialCounter < 201:
                                luminosityProperty.Data = luminosityProperty.Data + (materialCounter * 15)

                        # remove placeholder namespace
                        refComp.ProcessObjectNamespace(FBNamespaceAction.kFBRemoveAllNamespace, '', '')

                        # add new name
                        refComp.LongName = 'REF_POSE_' + item.CharacterString + ':' + refComp.Name

                    for refComp in refPoseComponentList:
                        if characterTarget:
                            refPoseProperty = refComp.PropertyCreate('Ref Pose Component', FBPropertyType.kFBPT_charptr, "", False, True, None)
                            refPoseProperty.Data = characterTarget.LongName

                    # add property to main group (to use in plot script)
                    if refGroupList:
                        refPoseMainGroup = refGroupList[-1]
                        mainGroupProperty = refPoseMainGroup.PropertyCreate('Ref Pose Group Plot', FBPropertyType.kFBPT_charptr, "", False, True, None)
                        mainGroupProperty.Data = None

                    # drive  ref pose off character
                    characterSource = None
                    namespace = None

                    if characterTarget:
                        characterTarget.InputCharacter = item.CharacterNode
                        characterTarget.InputType = (FBCharacterInputType.kFBCharacterInputCharacter)
                        characterTarget.ActiveInput = True
                        characterTarget.PropertyList.Find('Match Source').Data = True

                    else:
                        FBMessageBox('R* Error', 'Unable to find character nodes for one or both of:\n- {0}\n- {1}\n\nPlease check the namespaces are correct'.format(item.Character.LongName, 'REF_POSE_' + str(item.CharacterCheckbox.text())) , 'Ok')

                else:
                    FBMessageBox('R* Error', 'Unable to find path:\n\n[{0}]'.format(refPosePath), 'Ok')
    FBMergeTransactionEnd()


def RefPoseGroupPopulate(frame, refGroup):

    groupFound = False
    for group in Globals.Groups:
        if group.Name == 'REF_POSE_[{0}]'.format(frame):
            groupFound = True
            for i in range(refGroup.GetDstCount()):
                if refGroup.GetDst(i).ClassName() == 'FBGroup':
                    try:
                        refGroup.DisconnectDst(refGroup.GetDst(i))
                    except:
                        pass
            group.ConnectSrc(refGroup)
            break

    if not groupFound:
        group = FBGroup('REF_POSE_[{0}]'.format(frame))
        for i in range(refGroup.GetDstCount()):
            if refGroup.GetDst(i).ClassName() == 'FBGroup':
                try:
                    refGroup.DisconnectDst(refGroup.GetDst(i))
                except:
                    pass
        group.ConnectSrc(refGroup)

    return group


'''
plot ref pose callback
'''
def PlotRefPose(refPoseCharacterList, checkBoxPlotWholeTake, spinBoxFrame):
    characterDeleteList = []
    if refPoseCharacterList:
        for refPoseCharacter in refPoseCharacterList:
            refPoseCharacterName = refPoseCharacter.LongName
            # plot whole take
            if checkBoxPlotWholeTake.isChecked():
                RS.Utils.Scene.Plot.PlotOnCharacterSkeleton(refPoseCharacter, plotPeriod=FBTime(0, 0, 0, 1))
                refPoseCharacter.ActiveInput = False

                # update groups to reflect plot type
                for group in Globals.Groups:
                    groupProperty = group.PropertyList.Find('Ref Pose Group Plot')
                    refPoseNameProperty = group.PropertyList.Find('Ref Pose Component')
                    if refPoseNameProperty and groupProperty:
                        if refPoseNameProperty.Data == refPoseCharacterName:
                            refPoseNameProperty.Data = refPoseCharacterName + '[plotted]'
                            groupProperty.Data = 'ANIM'
                            RefPoseGroupPopulate('ANIM', group)
                            break

                refPoseCharacter.ActiveInput = False
                characterDeleteList.append(refPoseCharacter)

            # plot frame
            else:
                frame = spinBoxFrame.value()

                FBPlayerControl().Goto(FBTime(0, 0, 0, frame))

                refPoseList = []

                hips = refPoseCharacter.GetModel(FBBodyNodeId.kFBHipsNodeId)
                parent = RS.Utils.Scene.GetParent(hips)
                RS.Utils.Scene.GetChildren(parent, refPoseList, "", False)

                for i in refPoseList:
                    if i.Translation.GetAnimationNode():
                        for node in range(len(i.Translation.GetAnimationNode().Nodes)):
                            i.Translation.GetAnimationNode().Nodes[node].KeyAdd(FBTime(0, 0, 0, frame), i.Translation[node])
                    if i.Rotation.GetAnimationNode():
                        for node in range(len(i.Rotation.GetAnimationNode().Nodes)):
                            i.Rotation.GetAnimationNode().Nodes[node].KeyAdd(FBTime(0, 0, 0, frame), i.Rotation[node])

                refPoseCharacter.ActiveInput = False
                characterDeleteList.append(refPoseCharacter)

            # update groups to reflect plot type
            for group in Globals.Groups:
                groupProperty = group.PropertyList.Find('Ref Pose Group Plot')
                refPoseNameProperty = group.PropertyList.Find('Ref Pose Component')
                if refPoseNameProperty and groupProperty:
                    if refPoseNameProperty.Data == refPoseCharacterName:
                        refPoseNameProperty.Data = refPoseCharacterName + '[plotted]'
                        groupProperty.Data = str(frame)
                        RefPoseGroupPopulate(str(frame), group)
                        break

    for character in characterDeleteList:
        character.FBDelete()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Swap camrocks for baked cams in switcher for MocapToolbox
# By Kat url:bugstar:1910710
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def SwapCamRocksInSwitcher():

    cameraList = []
    bakedCamDict = {}
    cameraDict = {}

    for i in Globals.Cameras:
        if not i.SystemCamera:
            cameraList.append(i)

    # create a list of switcher keys
    cameraSwitcher = FBCameraSwitcher()
    switcherKeys = cameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys

    # create a list of baked cameras
    for i in range(len(cameraList)):
        camRockProperty = cameraList[i].PropertyList.Find('Original CamRock')
        if camRockProperty:
            bakedCamDict[cameraList[i]] = (i + 1)

    if bakedCamDict:
        # create a dictionary with the baked and rock cameras
        for baked, index in bakedCamDict.iteritems():
            originalCamRock = baked.PropertyList.Find('Original CamRock').Data
            for cam in cameraList:
                if originalCamRock == cam.Name:
                    cameraDict[baked] = (cam, None, index)

        # add switcher key value to dictionary
        for i, key in enumerate(switcherKeys):
            camera = cameraList[int(key.Value) - 1]
            for baked, rock in cameraDict.iteritems():
                if rock[1]:
                    None
                else:
                    if rock[0].Name == camera.Name:
                        cameraDict[baked] = (rock[0], key.Time.GetFrame(), rock[2])
                        break

        # replace rocks with baked cams in the switcher
        for i in switcherKeys:
            for key, value in cameraDict.iteritems():
                bakedCam = key
                camRock = value[0]
                frame = value[1]
                bakedCamIndex = value[2]

                if frame == i.Time.GetFrame():
                    # remove cam rock
                    switcherFCurve = switcherKeys = cameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve
                    switcherFCurve.KeyDelete(FBTime(0, 0, 0, i.Time.GetFrame()), FBTime(0, 0, 0, i.Time.GetFrame()))

                    # add baked cam
                    switcherFCurve.KeyAdd(FBTime(0, 0, 0, frame), bakedCamIndex)

    else:
        print FBMessageBox('R* Error', 'No baked Cams are present in the scene.', 'Ok')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Prop & Set Snap
# By Kat url:bugstar:1802602
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def PropSetDictionarySnap():

    referenceScene = None
    refNullList = []
    refPropList = []
    refSetList = []
    propDict = {}
    setDummyList = []
    snapDict = {}
    dlcPrefix = 'hei_'
    mainControl = None

    for component in Globals.gComponents:
        if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':
            referenceScene = component

    if referenceScene:
        RS.Utils.Scene.GetChildren(referenceScene, refNullList, "", False)

        if len(refNullList) > 0:
            for asset in refNullList:
                if asset.PropertyList.Find('rs_Asset_Type'):
                    typeProperty = asset.PropertyList.Find('rs_Asset_Type')
                    if typeProperty.Data == 'Props':
                        splitName = asset.LongName.split(':')
                        refPropList.append(splitName[-1])
                    if typeProperty.Data == 'Sets':
                        splitName = asset.LongName.split(':')
                        refSetList.append(splitName[-1])

    dlcPrefix = 'hei_'
    for namespace in refPropList:

        if dlcPrefix in namespace:
            splitName = namespace.split(dlcPrefix)
            propName = splitName[-1]
        else:
            propName = namespace

        if '^' in propName:
            splitName = namespace.split('^')
            propName = splitName[0]

        if RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_Control', True):
            mainControl = RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_Control', True)
        elif RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_frag__Control', True):
            mainControl = RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_frag__Control', True)
        elif RS.Utils.Scene.FindModelByName(namespace + ':' + 'Gun_GripR_Control', True):
            mainControl = RS.Utils.Scene.FindModelByName(namespace + ':' + 'Gun_GripR_Control', True)
        elif RS.Utils.Scene.FindModelByName(namespace + ':' 'Root_' + propName + '_Control', True):
            mainControl = RS.Utils.Scene.FindModelByName(namespace + ':' 'Root_' + propName + '_Control', True)
        elif RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_Root_Control', True):
            mainControl = RS.Utils.Scene.FindModelByName(namespace + ':' + propName + '_Root_Control', True)

        if mainControl:
            propDict[namespace] = propName, mainControl

    for i in refSetList:
        setChildrenList = []
        setMatchList = []
        setRoot = RS.Utils.Scene.FindModelByName(i + ':SetRoot', True)
        if setRoot:
            RS.Utils.Scene.GetChildren(setRoot, setChildrenList, "", False)

            for key, value in propDict.iteritems():
                setPropList = []

                for setProp in setChildrenList:
                    if value[0].lower() in setProp.Name.lower():
                        setPropList.append(setProp)

                snapDict[key] = value[0], value[1], setPropList

    return snapDict


def PropSetSnap():

    snapDict = PropSetDictionarySnap()
    dictLength = len(list(snapDict.keys()))

    popup = False
    for key, value in snapDict.iteritems():
        namespace = key
        propName = value[0]
        mainControl = value[1]
        setList = value[2]

        if len(setList) > 1:
            popup = True
        elif len(setList) == 1:
            RS.Utils.Scene.Align(mainControl, setList[0])
            Globals.Scene.Evaluate()
        else:
            None

    return popup



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Audio Clip & Track Delete for Playback
# By Kat url:bugstar:1911798
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def AudioClipAndTrackDelete():

    deleteList = []

    for track in FBStory().RootFolder.Tracks:
        if track.Type == FBStoryTrackType.kFBStoryTrackAudio:
            deleteList.append(track)
        for clip in track.Clips:
            for audio in Globals.Scene.AudioClips:
                if clip.Name == audio.Name:
                    deleteList.append(audio)


    for track in FBStory().RootEditFolder.Tracks:
        if track.Type == FBStoryTrackType.kFBStoryTrackAudio:
            deleteList.append(track)
        for clip in track.Clips:
            for audio in Globals.Scene.AudioClips:
                if clip.Name == audio.Name:
                    deleteList.append(audio)


    for i in deleteList:
        try:
            i.FBDelete()
        except:
            pass

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Get R* North Prop Toy Files
# By Kat url:bugstar:1676327
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def GetRSNorthPropToys():

    PathDict = {}

    rootPath = 'x:\\virtualproduction\\previz\\north\\prototypeSetup\\stagineModels\\'

    filePaths = glob.glob(rootPath + "*.fbx")

    for i in filePaths:
        if os.path.exists(i):
            RS.Perforce.Sync(i)
            fileName = i.split(rootPath)[1]
            extension = RS.Utils.Path.GetFileExtension(i)
            propName = fileName.split(extension)[0]
            PathDict[propName] = i

    return PathDict

class RockCamTabCheck(object):

    def __init__(self):
        """
        Checks if the components that make up the CamRockTab setup exists. If not all of the components exist, then
        delete all the components that exists and import a fresh RockCamTab setup into the scene.
        """

        # sync required files
        pathStringList = []

        for pathString in ["Camera.xml", "CamRockTab.fbx"]:
            path = os.path.join(RS.Config.Tool.Path.TechArt, "etc", "Mocap", "Camera", pathString)
            RS.Perforce.Sync(path)
            if not os.path.exists(path):
                FBMessageBox("R* Error)File doesn't exist:\n\n{0}\n\nUnable to setup Tablet Camera.".format(path), 'Ok')
                return
            pathStringList.append(path)

        # files are synced and exist
        xmlPath, fbxPath = pathStringList

        tree = xml.etree.cElementTree.parse(xmlPath)

        # We do not want the root but the child of the root
        root = tree.getroot()[0]

        self._import = False
        self.checkComponents(root, root)
        if self._import:
            FBApplication().FileMerge(fbxPath)

    def checkComponents(self, element, topelement, delete=False, level=0):
            """
            Recursive method for checking that all the components are in the scene based on the contents of the
            techart/etc/Mocap/Camera.xml file.

            Arguments:
                element : elementTree.Element(); element that represents a component that makes up the CamRockTab setup
                topelement: elementTree.Element(); the root element from the xml that represents the CamRockSetup
                delete: boolean; whether a component should be deleted or not.

            Return:
                Boolean; returns True if components where deleted
            """

            # Get Children Components
            if re.search("Model|Camera", element.tag):
                component = filter(None, [FBFindModelByLabelName(element.text.strip())])

            else:
                component = filter(lambda each: each.Name == element.text.strip(),
                                   getattr(Globals, "g{}s".format(element.tag), Globals.gComponents))


            if not component and not delete:
                self._import = True
                self.checkComponents(topelement, topelement, delete=True, level=level + 1)

            else:
                for subelement in element:
                    self.checkComponents(subelement, topelement, delete, level=level + 1)

            if component and delete:
                component[0].FBDelete()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Create Lead-in/Lead-Out Cameras
# By Glenn url:bugstar:2262826
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def CreateLeadinLeadOutConstraints(Name, Selection, Camera):
    conManager = FBConstraintManager()
    # creates constraint type
    camCons = Constraint.CreateConstraint(Constraint.POSITION)
    # assigns name to the constraint
    camCons.Name = 'Lead{0}'.format(Name)
    # adds the selected object to parent and cameras to child. Snaps constraint
    camCons.ReferenceAdd(1, Selection)
    camCons.ReferenceAdd(0, Camera)
    camCons.Snap()
    conProperty = camCons.PropertyCreate('LeadCamera',
                                        FBPropertyType.kFBPT_charptr,
                                        "",
                                        False,
                                        True,
                                        None)
    conProperty.Data = Name

def SetupLeadinLeadOutConstraints():
    # get the selected models in the scene
    modelList = FBModelList()
    FBGetSelectedModels(modelList)
    selectedModel = None
    if len(modelList) == 1:
        selectedModel = modelList[0]
    else:
        QtGui.QMessageBox.warning(None,
                                  "Warning",
                                  "Please select 1 item/model and try again.",
                                  QtGui.QMessageBox.Ok)
        return

    if selectedModel is None:
        return

    # defines the cameras by finding them in the scene
    leadIn = FBFindModelByLabelName("R*Camera 1")
    leadOut = FBFindModelByLabelName("R*Camera 9")
    # checks to see if the cameras have been created in the scene
    if leadIn is not None and leadOut is not None:
        # create constraint
        CreateLeadinLeadOutConstraints("In", selectedModel, leadIn)
        CreateLeadinLeadOutConstraints("Out", selectedModel, leadOut)
    else:
        QtGui.QMessageBox.warning(None,
                                  "Warning",
                                  "Please ensure you have R*Cameras 1 and 9, created via the Multiviewer.",
                                  QtGui.QMessageBox.Ok)
        return
