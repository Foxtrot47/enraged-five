import os

import pyfbsdk as mobu

from RS import Config
from RS.Core.Mocap import MocapCommands


def SetAudioRecordingFilePath(stage=None):
    """
    Set the audio recording file path based off what is in watchstar
    """
    watchstarTrial = MocapCommands.WatchstarTrial()
    if stage is not None:
        watchstarTrial.stage = stage

    audioInputs = mobu.FBSystem().AudioInputs
    # Check we have a single audio input device to use
    if len(audioInputs) == 0:
        raise ValueError("Unable to detect any audio input devices")

    audioInput = audioInputs[0]

    # Get the take
    take = watchstarTrial.grabTake()
    if take is None:
        raise ValueError("Unable to find source current Take from Stage '{0}'".format(str(watchstarTrial.stage)))

    # see if its a CS or IG
    outputPath = None
    takeData = take.getDataDict()
    if takeData.get("trialType") == "Cutscene":
        cutscene = take.getDataDict().get("cutscene")
        cutsceneParts = cutscene.split("_", 2)
        nameParts = take.name.split("_", 3)

        outputPath = os.path.join(Config.Project.Path.Art,
                     "animation", "cutscene", "!!scenes",
                     cutsceneParts[0],
                     cutscene,
                     nameParts[-1],
                     "{0}_T{1}".format(nameParts[-1], nameParts[1]),
                     "{0}.wav".format(take.name)
                     )

    else:
        outputPath = os.path.join(Config.Project.Path.Art,
                     "animation", "ingame", "previz",
                     take.shoot().name.replace(" ", ""),
                     "{0}.wav".format(take.name)
                     )

    print "Setting path to '{0}' for '{1}'".format(outputPath, take)

    dirName = os.path.dirname(outputPath)
    if not os.path.exists(dirName):
        os.makedirs(os.path.dirname(outputPath))

    # Set the path to record to
    audioInput.SetOnline(True)
    audioInput.PrepareToRecord(outputPath)
