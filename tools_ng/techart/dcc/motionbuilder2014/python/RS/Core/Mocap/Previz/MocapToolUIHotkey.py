'''

 Script Path: RS/Core/Mocap/Previz/MocapToolUIHotkey.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 29 October 2013
 
 Description: Launcher for Mocap Toolbox for Kelly to assign to a hotkey

'''

from pyfbsdk import *

import RS.Tools.UI.Mocap.Toolbox as mocap


#calling the DeleteGiantDevice module from MocapCommands.py
mocap.Run()

