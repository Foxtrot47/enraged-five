import wave
import numpy as np
import sys
import os

class BeepToFrame:

    @staticmethod
    def freqToN(freq, fftData, sampleRate, bufferSize):
    
        fftFreq = np.fft.fftfreq(fftData.size, d=float(1) / float(sampleRate))
        currentFreq = 0
        currentFreq = 0
        x = 0
        while currentFreq < freq:
            currentFreq = fftFreq[x]
            x = x + 1
    
        thefreq = 0
    
        while (x > 0) and thefreq <= fftFreq[x]:
        
            if (x != len(fftData) - 1):
                y0,y1,y2 = np.log(fftData[x - 1:x + 2:])
                x1 = (y2 - y0) * .5 / (2 * y1 - y2 - y0)
                
            thefreq = (x + x1) * sampleRate / bufferSize
            x = x - 1
        return x

    def __init__(self, filename, threshold=5.5):
           self.filename = filename
           self.threshold = threshold

    def getFileName(self):
        return (self.filename)
        
    def getStartFrames(self, framerate=30):
        threshold = self.threshold
        bufferSize = 512    
        beepFrequency = 442
        wf = wave.open(os.path.abspath(self.filename), 'rb')
        swidth = wf.getsampwidth()
        samplerate = wf.getframerate()
        channels = wf.getnchannels()
        window = np.blackman(bufferSize * channels)

        
        

        totalFrames = ((wf.getnframes() / samplerate) * framerate)
        results = [False] * (totalFrames)
        resultsInFrames = []
        actualStartFrames = []
        
        
        maxSize = 0                      
        data = wf.readframes(bufferSize)
        fulldata = []
        while len(data) == bufferSize * swidth * channels:
            currentBuffer = (np.array(wave.struct.unpack("%dh" % (len(data) / (swidth)),\
                data)) * window)
            if np.max(currentBuffer) > maxSize:
                maxSize = np.max(currentBuffer)
            fulldata.append(currentBuffer)
            data = wf.readframes(bufferSize)
        
        normalizer = (16383*swidth) / maxSize
        
            
        for currentSection in range(0, len(fulldata)):
   
            indata = fulldata[currentSection]
           
            indata = np.multiply(indata, normalizer)
            fftData = abs(np.fft.rfft(indata))
    
            freq = np.fft.rfftfreq(fftData.size, d=(float(1) / float(samplerate)))
            
            n = BeepToFrame.freqToN(beepFrequency,fftData, samplerate, bufferSize)
            n3 = BeepToFrame.freqToN(beepFrequency * 3,fftData,samplerate, bufferSize)
            n5 = BeepToFrame.freqToN(beepFrequency * 5,fftData,samplerate, bufferSize)
            if np.log10(fftData[n]) > threshold:
                if np.log10(fftData[n3]) > threshold - 0.3:
                    if np.log10(fftData[n5]) > threshold - 0.5:      
                       frameNo = int((currentSection * bufferSize / float(samplerate)) * framerate)
                       if(frameNo < len(results) - 1):
                            results[frameNo] = True
       
    
        y = 0
    
        while y < totalFrames:
            z = 1
            if results[y] == True:
                resultsInFrames.append(y)       
            
                while results[y + z] == True:
                    z = 1 + z    
               
            y = y + z

        for x in range(2, len(resultsInFrames)):
            if (resultsInFrames[x - 1] - resultsInFrames[x - 2]) > (0.5 * framerate):
                if  (resultsInFrames[x - 1] - resultsInFrames[x - 2]) < (0.8 * framerate):
                    if (resultsInFrames[x] - resultsInFrames[x - 1]) > (0.5 * framerate):
                        if  (resultsInFrames[x] - resultsInFrames[x - 1]) < (0.8 * framerate):
                            actualStartFrames.append(resultsInFrames[x])
        return(actualStartFrames)
