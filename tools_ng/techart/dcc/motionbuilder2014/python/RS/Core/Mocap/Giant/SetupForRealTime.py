"""
Library for setting up a character for mobu Realtime after the fbx2anim conversion.
Has all the steps needed as well as a method called SetupForRealTime which will process the
current scene in the correct order.

This is a linux friendly script.
"""

import pyfbsdk as mobu

from PySide import QtGui

from RS import ProjectData


def FindModelByName(name, includeNamespace=False):
    """
    Mimics the deprecated behavior of mobu.FBFindModelByName.

    args:
        name: The model name to search.

    returns:
        The found model, None otherwise.
    """
    componentList = mobu.FBComponentList()
    mobu.FBFindObjectsByName(name, componentList, includeNamespace, True)

    if len(componentList) > 0:
        return componentList[0]

    else:
        return None

def RemoveAllGroups():
    """
    Remove all groups from the scene
    """
    groups = [grp for grp in mobu.FBSystem().Scene.Groups]
    for group in groups:
        group.FBDelete()


def SetupGroups():
    """
    Method to setup the groups around the skeleton and geo groups, as well as the main group for
    the whole character
    """
    skels = _createGroupFromList("skeleton", GetAllSkeletons())
    geos = _createGroupFromList("geo", GetAllGeometry())
    mats = _createGroupFromList("material", GetAllMaterials())

    _createGroupFromList(_getCharactersName(), [skels, geos, mats])


def _createGroupFromList(nameOfGroup, items):
    """
    Helper method to create a new group with the given name and add the given items into that group.

    args:
        nameOfGroup (string): The name of the group
        items (list of FBModels): The items to add into the group

    returns:
        FBGroup: The newly created group
    """
    group = mobu.FBGroup(nameOfGroup)
    for item in items:
        group.ConnectSrc(item)
    return group


def GetAllSkeletons():
    """
    Get all skeletons in the scene.

    returns:
        list of FBModelSkeletons
    """
    root = _getSkelRoot()
    # we have to use be strict about the type to only get the skeletons and not the geo as well
    return [item for item in GetAllChildren(root) if type(item) == mobu.FBModelSkeleton]


def GetAllGeometry():
    """
    Get all geometry in the scene.

    returns:
        list of FBModel
    """
    root = _getSkelRoot()
    # we have to use be strict about the type to only get the geo
    return [item for item in GetAllChildren(root) if type(item) == mobu.FBModel]


def GetAllMaterials():
    """
    Get All the materials in the scene.

    returns:
        list of FBMaterials
    """
    return [mat for mat in mobu.FBSystem().Scene.Materials]


def CreateAndParentOffsetNull():
    """
    Create an offsetNull with the correct name and parent the skeletons root under it
    """
    null = mobu.FBModelNull("{0}_MocapOffset".format(_getCharactersName()))
    root = _getSkelRoot()
    root.Parent = null


def RenameMaterials():
    """
    Rename all the materials applied to a given character
    """
    charName = _getCharactersName()
    for mat in GetAllMaterials():
        mat.Name = "{0}_{1}".format(charName, mat.Name)


def CreateAndConfigureCharacter():
    """
    Create a new character with the correct name and add the correct bones into the correct
    character slots and characterize the whole thing.

    Uses the skelDict to resolve slots to bone names
    """
    char = mobu.FBCharacter(_getCharactersName())
    skelDict = ProjectData.data.GetGiantCharacterizationDict()
    for charName, skelNames in skelDict.iteritems():
        for skelName in skelNames:
            skel = FindModelByName(skelName)
            if skel is None:
                continue
            slot = char.PropertyList.Find("{0}Link".format(charName))
            slot.append(skel)
            break
    char.SetCharacterizeOn(True)


def _getCharactersName():
    """
    Helper method to the the character name from the namepsace in the scene
    """
    scene = mobu.FBSystem().Scene
    skels = [comp for comp in scene.Components if isinstance(comp, mobu.FBModelSkeleton)]
    try:
        return skels[0].LongName.split(":")[0]
    except:
        return "Standard_Male01_gs"


def _getSkelRoot():
    """
    Internal Method

    Get the Skeleton Root object

    returns:
        FBModel of the SKEL_ROOT
    """
    return FindModelByName("SKEL_ROOT")


def GetAllChildren(element, currentList=None):
    """
    Recursive method to get all the child items from an object

    args:
        element (FBModel): Object to get the children from

    kwargs:
        currentList (list of FBModels): The internal list of all found models.

    returns:
        list of FBModels: All the models found as children of the top element
    """
    if currentList is None:
        currentList = []
    currentList.append(element)

    for comp in element.Components:
        if isinstance(comp, mobu.FBMaterial):
            currentList.append(comp)

    for child in element.Children:
        currentList = GetAllChildren(child, currentList)
    return currentList


def SetupForRealTime():
    """
    Main method to setup a giant converted character for realtime in motion builder
    """
    CreateAndConfigureCharacter()
    CreateAndParentOffsetNull()
    RenameMaterials()
    RemoveAllGroups()
    SetupGroups()


def Run(show=True):
    SetupForRealTime()

    if show:
        QtGui.QMessageBox.information(
                                      None,
                                      "Prep Character", "Character has been prepped for Realtime"
                                      )