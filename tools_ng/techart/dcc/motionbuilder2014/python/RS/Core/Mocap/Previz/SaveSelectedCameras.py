'''

 Script Path: RS/Core/Mocap/Previz/SaveSelectedCameras.py
 
 Written And Maintained By: Kathryn Bodey

 Created: 31 October 2013 Halloweeeeeeen {^o^}
                                        
 Description: Launcher for SaveSelectedCameras module from MocapCommands.py
              Saves out cameras, along with their linked components, as a 
              separate file

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommands


#calling the SaveSelectedCameras module from MocapCommands.py
mocapCommands.SaveSelectedCameras()