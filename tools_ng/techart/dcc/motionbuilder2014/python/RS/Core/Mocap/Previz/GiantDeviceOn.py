'''

 Script Path: RS/Core/Mocap/Previz/GiantDeviceOn.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 07 October 2013
 
 Description: Launcher for GiantDeviceOn module from MocapCommands.py
              Sets 'Online', 'Recording' and 'Live' options to ON

'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the GiantDeviceOn module from MocapCommands.py
mocapCommand.GiantDeviceOn()