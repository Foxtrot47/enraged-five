"""
Libaray for layering motion capture Animations for use on set 
"""
import copy

import pyfbsdk as mobu
 
from RS import Globals
from RS.Utils.Scene import AnimationNode, Character, Groups
from RS.Core.Mocap import Clapper
from RS.Utils import Vector


class AnimationLayering(object):
    """
    Logic and methods to layer animation in Motion Builder, as well as class methods to get the 
    clap frames and duplicate characters
    """
    def __init__(self, charactersToLayer, offsetFrame=None, layerName=None, props=None):
        """
        Constructor
        
        Args:
            charactersToLayer (list of FBCharacters): The characters to layer
            
        Kwargs:
            offsetFrame (int): The amount of frames to zero out the animation
            layerName (string): The layer name to use to namespace the characters layer
            props (list of FBGroups): Any groups that make up props to be duplicated
        """
        super(AnimationLayering, self).__init__()
        
        self._layerAnimation(charactersToLayer, offsetFrame=offsetFrame, layerName=layerName, props=props)
        
    @classmethod
    def _layerAnimation(cls, charactersToLayer, offsetFrame=None, layerName=None, props=None):
        """
        Internal Method
        
        Class Method
        
        Main logic for creating the animation layering given the specific characters, offset frame
        and layername
        
        Args:
            charactersToLayer (list of FBCharacters): The characters to layer
            
        Kwargs:
            offsetFrame (int): The amount of frames to zero out the animation
            layerName (string): The layer name to use to namespace the characters layer
            props (list of FBGroups): Any groups that make up props to be duplicated
        """
        # Put them in a group
        topGroup = mobu.FBGroup("Group")
        topGroup.LongName = "{0}:{1}".format(layerName, topGroup.LongName)
        
        # Duplicate the characters
        for char in charactersToLayer:
            dupChar = Character.DuplicateCharacter(char, newNamespace=layerName, createGroup=True)
            for group in Groups.GetGroupsFromObject(dupChar):
                topGroup.Items.append(group)
        
        if props is not None:
            for prop in props:
                topGroup.Items.append(cls._duplicateGroupAndAnimation(prop, newNamespace=layerName))
        
        # Duplicate the slate
        slateParts = Clapper.DuplicateClapper(newNamespace=layerName, createGroup=True)
        for group in Groups.GetGroupsFromObject(slateParts[0]):
            topGroup.Items.append(group)
            
        # Zero out the scene to the offset frame
        if offsetFrame is not None or offsetFrame != 0:
            cls.ZeroOutScene(offsetFrame)

    @classmethod
    def _duplicateGroupAndAnimation(cls, group, newNamespace=None):
        """
        Class Method
        
        Duplicate the given group and its children, giving the option to set a new name and or namespace

        Kwargs:
            group (FBGroup): The group to duplicate
            newNamespace (str): The new namespace to give each part of the group
            createGroup (bool): If the group objects should be added to a new group
        
        Return:
            FBGroup: New duplicated Group
        """
        groupItems = [item for item in group.Items if isinstance(item, mobu.FBModel)]
    
        newGroup = mobu.FBGroup("{0}:{1}".format(newNamespace or "CLONE", group.LongName))
    
        dupDict = {}
        for item in groupItems:
            itemName = str(item.LongName)
            if newNamespace is None:
                newLongName = itemName
            else:
                newLongName = "{0}:{1}".format(newNamespace, itemName)
                
            newItem = copy.copy(item)
            newItem.LongName = newLongName
            dupDict[item] = newItem
            
            newGroup.ConnectSrc(newItem)
    
        # Re-parent newBones to newBone equivalent of orgBones
        for item in groupItems:
            parItem = item.Parent
            if parItem is None:
                continue
            
            newItem = dupDict[item]
            newItemBone = dupDict.get(parItem, parItem)
            newItem.Parent = newItemBone
    
        for item in groupItems:
            newItem = dupDict[item]
            
            AnimationNode.CopyData(item.AnimationNode, newItem.AnimationNode)
            
            newItem.Translation = Vector.CloneVector(item.Translation)
            newItem.Rotation = Vector.CloneVector(item.Rotation)
        
        return newGroup

    @classmethod
    def ZeroOutScene(cls, frame):
        """
        Class Method
        
        Method to zero out all animation in a scene bringing the given frame number to be at frame
        zero.
        
        Args:
            frame (int): The frame number to bring to zero
        """
        if frame == 0 or frame == None:
            return False
        # get filter
        filterManager = mobu.FBFilterManager()
        timeShiftScaleFilter = filterManager.CreateFilter('Time Shift And Scale')
        # set shift property
        propShift = timeShiftScaleFilter.PropertyList.Find('Shift')
        # needs to be negative
        propShift.Data = mobu.FBTime(0, 0, 0, -(int(frame)))
        midiInstrument = [item for item in mobu.FBSystem().Scene.Components 
                                                        if item.ClassName() != 'FBMidiInstrument']
        for item in midiInstrument:
            for prop in item.PropertyList:
                if prop.IsAnimatable() and prop.IsAnimated() and prop.GetAnimationNode():
                    timeShiftScaleFilter.Apply(prop.GetAnimationNode(), True)

        timeShiftScaleFilter.FBDelete()
        mobu.FBSystem().Scene.Evaluate()
        return True


