"""
Create Take with no Layering
"""
from PySide import QtGui

from RS.Core.Camera import Lib
from RS.Core.Mocap import MocapCommands


def GrabTakeWithNoLayeringHotKey():
    """
    calling the CamRockBake module from MocapCommands.py
    """
    try:
        newTakeName = MocapCommands.WatchstarTrial().grabTakeName(False)
        if newTakeName is None:
            QtGui.QMessageBox.information(
                                          None,
                                          "Virtual Production Toolbox",
                                          "No Active take for set {0}".format(MocapCommands.WatchstarTrial().stage)
                                          )
            return
        Lib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    except ValueError:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      "No Valid set, Please set the current Mocap Shoothing stage in the Virtual Production Tool"
                                      )
        return


GrabTakeWithNoLayeringHotKey()
