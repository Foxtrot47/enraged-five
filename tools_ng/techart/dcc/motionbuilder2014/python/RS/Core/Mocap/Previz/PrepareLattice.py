'''

 Script Path: RS/Core/Mocap/Previz/PrepLattice.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 11 October 2013
 
 Description: Launcher for PrepareLattice UI
              Prepares the ground and stage, which the user selects, 
              for the lattice setup in giant

'''

from pyfbsdk import *

import RS.Tools.UI.Mocap.Giant as lattice


#calling the PrepLatticeToolBox UI
lattice.Run()