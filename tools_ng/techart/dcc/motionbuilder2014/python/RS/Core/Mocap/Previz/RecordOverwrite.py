'''

 Script Path: RS/Core/Mocap/Previz/RecordOverwrite.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 07 October 2013
 
 Description: Launcher for RecordOverwrite module from MocapCommands.py
              Records and overwrites take via player controls
              
'''

from pyfbsdk import *

import RS.Core.Mocap.MocapCommands as mocapCommand


#calling the RecordOverwrite module from MocapCommands.py
mocapCommand.RecordOverwrite()