'''

 Script Path: RS/Core/Mocap/Previz/CleanMocapPreviz.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 11 October 2013
 
 Description: Launcher for DeleteBasePreviz & rs_NonReferencedItems()
              Deletes items with a particular custom property and launches a 
              UI displaying what objects are not part of ref system and if 
              they have animation or not

'''

from pyfbsdk import *

import time

import RS.Core.Mocap.MocapCommands as mocapCommand
import RS.Tools.UI.NonReferenceItemView as nonRefUi

reload( nonRefUi )

#calling the DeleteBasePreviz module from MocapCommands.py
mocapCommand.DeleteBasePreviz()

#sleep to allow the script to catchup and display whats left in the scene
time.sleep(1)


#calling the rs_NonReferencedItems UI from rs_NonReferencedItems.py
nonRefUi.Run()