"""
Core Library of methods around the clapper to help duplicate, get and work with it
"""
import math
import copy
import operator

import pyfbsdk as mobu
from RS import Globals
from RS.Utils.Scene import AnimationNode, Groups
from RS.Utils import Scene, Vector


def _AnalyaizeClapperData(clapperData, valueThreshold=7):
    """
    Internal Method

    Function to create a highpass filter to run over the clapper data and to retrive the peaks
    of the filter data, and follow that to where the clapper is closed again

    Args:
        clapperData (dict): The clapper data, where the key is the frame number and the value
                            is the Z axis rotation of the top the clapper

    Kwarg:
        valueThreshold (int): The high pass filter threshold

    Return:
        list of int: All the frame numbers where the clapper hits down after raising up
    """
    clapFrames = set()

    # Filter out all the values higher than the threshold
    valFil = dict([(key, value) for key, value in clapperData.iteritems() if value > valueThreshold])
    frames = valFil.keys()
    frames.sort()

    # There's no data, it was all filtered
    if len(valFil) == 0:
        return []

    clapGroups = {0:{}}

    # break up the high pass filter results into groups/clumps
    index = 0
    lastFrame = frames[0] - 1
    for frame in frames:
        value = valFil[frame]
        if frame != lastFrame + 1:
            index += 1
            clapGroups[index] = {}
        clapGroups[index][frame] = value
        lastFrame = frame

    # Get the highest point from each group/clump
    highPoints = []
    for data in clapGroups.itervalues():
        key, value = max(data.iteritems(), key=operator.itemgetter(1))
        highPoints.append(key)

    # Follow the high point down to where it hits, and store the lowest rotation value frame
    maxClapperDownFrameAllowance = 50
    maxClapperDownThreshold = 1.0
    for highPoint in highPoints:
        lastFrameValue = 1000000
        for frameIdx in xrange(1, maxClapperDownFrameAllowance):
            inspectFrame = highPoint + frameIdx
            value = clapperData.get(inspectFrame)
            if value is None:
                break
            # Check the value is under the threshold before we 'consider' it
            if value > valueThreshold:
                continue
            delta = lastFrameValue - value
            if delta < maxClapperDownThreshold:
                clapFrames.add(int(math.ceil(inspectFrame)))
                break
            lastFrameValue = value
    sortedClapFrames = list(clapFrames)
    sortedClapFrames.sort()
    return sortedClapFrames


def GetClaps(maxClumpSeperation=120):
    """
    Get the last frame in a clump of claps where the clapper board hits down, if a clapper board exists in the scene

    kwargs:
        maxClumpSeperation (int): The number of frames to clump groups of clumps together

    Return:
        dict of Key:object value:list of int - object is the clapper board bone and list is each
                                               3rd clap frame found
    """
    clapsDict = {}
    for component in Globals.Components:
        # Check Type
        if not isinstance(component, mobu.FBModel):
            continue

        # Check name
        if component.Name not in ['SlateTop_bone', 'SlateClapperTop_bone']:
            continue

        rot = component.PropertyList.Find("Lcl Rotation")
        rotAnim = rot.GetAnimationNode()

        # Check it has an animation node (not 100% sure why it wouldn't)
        if rotAnim is None:
            continue

        for channel in rotAnim.Nodes:
            if str(channel.Name).lower() != "z":
                continue
            # Check to make sure it has at least 1 key
            if len(channel.FCurve.Keys) == 0:
                continue
            clapperDict = {}
            for idx in xrange(channel.FCurve.Keys[0].Time.GetFrame(), channel.FCurve.Keys[-1].Time.GetFrame()):
                clapperDict[idx] = channel.FCurve.Evaluate(mobu.FBTime(0, 0, 0, idx))
            sortedClapperList = _AnalyaizeClapperData(clapperDict)
            claps = []

            # Dynamically sort the claps into "clumps"
            clapParts = []
            currentClapPart = []
            for part in sortedClapperList:
                if len(currentClapPart) == 0:
                    currentClapPart.append(part)
                    continue
                if (part - currentClapPart[0]) <= maxClumpSeperation:
                    currentClapPart.append(part)
                else:
                    clapParts.append(currentClapPart)
                    currentClapPart = [part]
            clapParts.append(currentClapPart)

            # Now get the last one from each "clump"
            claps = [clapPart[-1] for clapPart in clapParts if len(clapPart) > 0]

            # Check we have claps to add
            if len(claps) > 0:
                # final dict
                clapsDict[component] = claps

    return clapsDict


def GetTakeSlateGroup():
    """
    Get the Take Slate Group

    Return:
        FBGroup: The take slate motion builder group
    """
    for group in Globals.gGroups:
        if group.Name == "PREVIZ_SLATE":
            return group


def DuplicateClapper(newName=None, newNamespace=None, createGroup=False):
    """
    Duplicate the scenes clapper board, giving the option to set a new name and or namespace
    as well as creating a new group for the new clapper. All animation is copied.

    NOTE:
        The clapper board constraint is not copied at this time

    Kwargs:
        newName (str): The new name for the clapper board
        newNamespace (str): The new namespace to give each part of the clapper board
        createGroup (bool): If the new clapper board should be added to a new group

    Return:
        list of FBModels: All the parts which make up the duplicated clapper board
    """
    slateGroup = GetTakeSlateGroup()
    if slateGroup is None:
        raise ValueError("No Slate in scene")
    slateItems = [item for item in slateGroup.Items if isinstance(item, mobu.FBModel)]

    newGroup = None
    if createGroup:
        newGroup = mobu.FBGroup("{0}:PREVIZ_SLATE".format(newNamespace or "CLONE"))

    dupDict = {}
    for item in slateItems:
        itemName = newName or str(item.LongName)
        if newNamespace is None:
            newLongName = itemName
        else:
            newLongName = "{0}:{1}".format(newNamespace, itemName)

        newItem = copy.copy(item)
        newItem.LongName = newLongName
        dupDict[item] = newItem

        if newGroup is not None:
            newGroup.ConnectSrc(newItem)

    # Re-parent newBones to newBone equivalent of orgBones
    for item in slateItems:
        parItem = item.Parent
        if parItem is None:
            continue

        newItem = dupDict[item]
        newItemBone = dupDict.get(parItem, parItem)
        newItem.Parent = newItemBone

    for item in slateItems:
        newItem = dupDict[item]

        AnimationNode.CopyData(item.AnimationNode, newItem.AnimationNode)

        newItem.Translation = Vector.CloneVector(item.Translation)
        newItem.Rotation = Vector.CloneVector(item.Rotation)

    return dupDict.values()
