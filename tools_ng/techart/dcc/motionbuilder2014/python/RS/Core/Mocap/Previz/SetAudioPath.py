"""
Set the audio path based off the current watchstar take name
"""
from PySide import QtGui

from RS.Core.Camera import Lib
from RS.Core.Mocap import MocapCommands, AudioRecording


def SetAudioPath():
    watchstarTrial = MocapCommands.WatchstarTrial()
    if watchstarTrial.stage is None:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      "No Valid set, Please set the current Mocap Shoothing stage in the Virtual Production Tool"
                                      )
        return
    try:
        AudioRecording.SetAudioRecordingFilePath()
    except ValueError, expMsg:
        QtGui.QMessageBox.information(
                                      None,
                                      "Virtual Production Toolbox",
                                      str(expMsg)
                                      )
        return



SetAudioPath()
