#David Bailey
#14/09/2010

from ctypes import *
from pyfbsdk import *
from pyfbsdk_additions import *
from xml.dom import minidom
from collections import defaultdict
import sys
import os

import RS.Config

def PopulateRunScriptsLayout(mainLyt, label):
    Label = FBLabel()
    Label.Caption = label
    
    x = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
    w = FBAddRegionParam(300,FBAttachType.kFBAttachNone,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    
    mainLyt.AddRegion("Label","Label", x, y, w, h)
    mainLyt.SetControl("Label",Label)
    
def CreateRunScriptsTool(label):    
    global RunScripts
    RunScripts = FBCreateUniqueTool("Executing Script: " + label)
    RunScripts.StartSizeX = 300
    RunScripts.StartSizeY = 20
    PopulateRunScriptsLayout(RunScripts, label)
    ShowTool(RunScripts)


def getFileName(f):
    
    d, filename = os.path.split(f)
    if filename:
        return os.path.splitext(filename)[0]
    else:
        return "unknown"
#############################################################################################################
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#############################################################################################################    
lScene = FBSystem().Scene
lRoot = lScene.RootModel
lApp = FBApplication()
lFileFullName = lApp.FBXFileName
SceneName = getFileName(lFileFullName)
lFilePath = os.path.dirname(lFileFullName)
newPath1 = lFilePath + "\\" + SceneName + "_RelationCode"
childArray = []
#############################################################################################################
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#############################################################################################################    
def findChildren(Model):
    global childArray
    childArray.append(Model)
    for child in Model.Children:
        childArray.append(child)
        if child.Children == None:
            return childArray
        else:
            findChildren(child)

def DisplayAllNulls(control, event):     
    global lRoot     
    global b
    
    b.Enabled = False
    
    for child in lRoot.Children:
        findChildren(child)
        
    for child in childArray:
        if child.Name.startswith("MH"):
            child.PropertyList.Find("Look").Data = 1
            child.Size = 1
            child.Selected = True
        elif child.Name.startswith("SM"):
            if child.PropertyList.Find("Look"):
                child.PropertyList.Find("Look").Data = 1
            child.Size = 1
            child.Selected = True
#############################################################################################################
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#############################################################################################################          
def Walk( root, recurse=0, pattern='*', return_folders=0 ):
    import fnmatch, os, string
    result = []
    try:
        names = os.listdir(root)
    except os.error:
        return result
    pattern = pattern or '*'
    pat_list = string.splitfields( pattern , ';' )
    for name in names:
        fullname = os.path.normpath(os.path.join(root, name))
        for pat in pat_list:
            if fnmatch.fnmatch(name, pat):
                if os.path.isfile(fullname) or (return_folders and os.path.isdir(fullname)):
                    result.append(fullname)
                continue
        if recurse:
            if os.path.isdir(fullname) and not os.path.islink(fullname):
                result = result + Walk( fullname, recurse, pattern, return_folders )        
    return result
    
def RunAllScripts(control, event):
    global newPath1
    global RunScripts
    global SceneName
    
    lScene = FBSystem().Scene
    lConstraints = lScene.Constraints
    lFolders = lScene.Folders
    
    lReferencedConstraintSolver = FBConstraintSolver("ToBeDeleted")
    lReferencedConstraintSolverFolder = FBFolder ((SceneName + ": Expressions"), lReferencedConstraintSolver)
    lReferencedConstraintSolver.FBDelete()##CREATES FOLDER IN CONSTRAINTS
    
    files = Walk(newPath1, 0, "*.py", 0)
    
    fileLength = len(files)
    
    lConstraintArray = []
         
    for constraint in lConstraints:
        lConstraintArray.append(constraint)
    
    lApp.FileAppend("{0}/tools/wildwest/script/motionbuilder/DaveBailey/MaxToMotionbuilder/MaxExpressionFunctionList.fbx".format( RS.Config.Project.Path.Root ))
    
    lReferencedConstraintArray = []
    
         
    for constraint in lConstraints:
        lReferencedConstraintArray.append(constraint)
    
    for Orig in lConstraintArray:
        for Ref in lReferencedConstraintArray:    
            if Ref == Orig:
                lReferencedConstraintArray.remove(Ref)
                

    
    FunctionFolder = None
    ExpressionFolder = None
    CharacterFolder = None
    
    for folder in lFolders:
        if folder.Name == "FunctionList":
            folder.LongName = (SceneName + ":FunctionList") 
            FunctionFolder = folder
        if folder.LongName == (SceneName + ": Expressions"):
            ExpressionFolder = folder
        if folder.LongName == (SceneName + ":Character_Constraints"):
            CharacterFolder = folder
    
    FunctionFolder.ConnectDst(ExpressionFolder)
        
    for i in range(len(files)):
##        FBMessageBox( "Run Script", fil, "Ok" )
        CreateRunScriptsTool(files[i])
        lApp.ExecuteScript(files[i])
        CloseTool(RunScripts)
        FBSleep(50)
        
    for constraint in lConstraints:
        if constraint.LongName.startswith(SceneName):
            constraint.ConnectDst(ExpressionFolder)
            
    for Ref in lReferencedConstraintArray:
        Ref.LongName = SceneName + ":" + Ref.LongName
        
    del lConstraintArray
    del lReferencedConstraintArray
    
    if CharacterFolder != None:
        ExpressionFolder.ConnectDst(CharacterFolder)
            
    CreateTool()
#############################################################################################################
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#############################################################################################################    

##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                                 Globals And Classes
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------ 

lstMods = []
lstBones = []
lstMods3 = []

class Mod:
    name = None
    converter = None
    sender = None
    
class Mod2:
    name = None
    translation = None
    rotation = None
    model = None
    
class TransformEntry:
    name = None
    controllerName = None
    
class Bone:
    name = None
    transforms = []
    
class RecieverEntry:
    recieverName = None
    reciever = None
    convertType = None
    
class SourceEntry:
    sourceName = None
    source = None
    convertType = None
    
class RecieverBox:
    name = None
    reciever = None
    
lstRecievers = []
lstPipes = []
lstSources = []
lstRecieverBoxes = []
relConstraint = None
d = {}

relConstraint = None
previousControlName = None

GreaterThanEqualToCounter = 0
GreaterThanCounter = 0
LessThanCounter = 0
LessThanEqualToCounter = 0
MultiplyCounter = 0
DivideCounter = 0
EqualToCounter = 0
NotEqualToCounter = 0
AddCounter = 0
SubtractCounter = 0
SqrtCounter = 0
AbsCounter = 0
CosCounter = 0
SinCounter = 0
DToRCounter = 0
RToDCounter = 0
IfThenElseCounter = 0
MinCounter = 0
MaxCounter = 0
converterDegToRadCounter = 0

loopCounter = 0
globalArray = []
HasDestinationArray = []
variableArray = []
modelCounter = 0
Trans = 0
controlNameArray = []

Y = 1
LayoutArray = []
  
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                       Definition To Find Animation Node
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------ 
def FindAnimationNode(pParent, pName):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                            Definition to connect nodes together in relation constraint
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------    
def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):
        mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
        mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
        if mynodeOut and mynodeIn:
            FBConnect(mynodeOut, mynodeIn)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                        Definition to disconnect nodes from one another in relation constraint
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------             
def DisconnectBox(nodeOut,nodeOutPos):
        mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
        
        if mynodeOut.GetDstCount() > 0:
            plug = mynodeOut.GetDst(0)
            FBDisconnect(mynodeOut, plug)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                           Definition to determine whether desination of FBBox is connected
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------             
def HasDestinationConnection(nodeOut,nodeOutPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
        
    if mynodeOut.GetDstCount() > 0:
        return True
            
    return False
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                             Definition to determine whether source of FBBox is connected
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------ 
def HasSourceConnection(nodeOut,nodeOutPos):
    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
        
    if mynodeOut.GetSrcCount() > 0:
        return True
            
    return False
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                      Definition to load and parse and XML file
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------      
def LoadXML(filename):
    xmldoc = minidom.parse(filename)
    for node in xmldoc.childNodes:
        for node2 in node.childNodes:
            if node2.nodeName == "boneLibrary":
                ProcessBoneLibrary(node2)
        for node2 in node.childNodes:
            if node2.nodeName == "controllerLibrary":
                odd = 1
                ProcessControllerLibrary(node2)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                    Definition to populate lstBones with XML bone names
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------             
def ProcessBoneLibrary(node):
    for node2 in node.childNodes:
        if node2.nodeType != node.TEXT_NODE:
            bone = Bone()
            for node3 in node2.childNodes:
                if node3.nodeName == "id":
                    bone.name = node3.firstChild.nodeValue
                for node4 in node3.childNodes:
                    if node4.nodeType != node.TEXT_NODE:
                        transform = TransformEntry()
                        for node5 in node4.childNodes:
                            if node5.nodeName == "controllerId":
                                transform.controllerName = node5.firstChild.nodeValue
                            if node5.nodeName == "id":
                                transform.name = node5.firstChild.nodeValue
                        bone.transforms.append(transform)
            lstBones.append(bone)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              Definition to get controller name
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------            
def GetControllerName(node):
    if node.attributes["type"].value == "rage__AeXYZController":
            for node2 in node.childNodes:
                if node2.nodeName == "id":
                    targetControlName = str(node2.firstChild.nodeValue)
                    index = targetControlName.find("xyz")
                    return targetControlName[:index-1]
                    
    elif node.attributes["type"].value == "rage__AeBlendController": 
        for node2 in node.childNodes:
            if node2.nodeName == "id":
                targetControlName = str(node2.firstChild.nodeValue)
                controllerName = targetControlName
                index = targetControlName.find("blend")
                return targetControlName[:index-1]
    
    return "Error"
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                           Definition to process controller Library, 
##                     This Starts Appending The Script to globalArray and on second pass writes it to disk
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------              
def ProcessControllerLibrary(node):
    global relConstraint
    global previousControlName
    global loopCounter
    global globalArray
    global variableArray
    global GreaterThanEqualToCounter
    global GreaterThanCounter
    global LessThanCounter
    global LessThanEqualToCounter
    global MultiplyCounter
    global DivideCounter
    global EqualToCounter
    global NotEqualToCounter
    global AddCounter
    global SubtractCounter
    global SqrtCounter
    global AbsCounter
    global CosCounter
    global SinCounter
    global DToRCounter
    global RToDCounter
    global IfThenElseCounter
    global controlNameArray
    global Trans
    global Y
    
    lApp = FBApplication()
    lFileFullName = lApp.FBXFileName
    lFilePath = os.path.dirname(lFileFullName)
    SceneName = getFileName(lFileFullName)
    newPath1 = lFilePath + "\\" + SceneName + "_RelationCode"

    for node2 in node.childNodes:
        
        GreaterThanEqualToCounter = 0
        GreaterThanCounter = 0
        LessThanCounter = 0
        LessThanEqualToCounter = 0
        MultiplyCounter = 0
        DivideCounter = 0
        EqualToCounter = 0
        NotEqualToCounter = 0
        AddCounter = 0
        SubtractCounter = 0
        SqrtCounter = 0
        AbsCounter = 0
        CosCounter = 0
        SinCounter = 0
        DToRCounter = 0
        RToDCounter = 0
        IfThenElseCounter = 0
        modelCounter = 0
        MinCounter = 0
        MaxCounter = 0
        Y = 1
        
        Title = ""
        Relation = ""

        if loopCounter == 0:
            if not os.path.isdir(lFilePath + "\\" + SceneName + "_RelationCode"): 
                newPath = os.mkdir(lFilePath + "\\" + SceneName + "_RelationCode")
                FinalPath = os.mkdir(lFilePath + "\\" + SceneName + "_RelationCode\\Final")
        else:
            if Trans == 1:
                pythonFilePath = (lFilePath + "\\" + SceneName + "_RelationCode\\" + controlName + "_Relation_Trans.py")
                Title = "# " + controlName + "_Relation_Trans.py\n"
                Relation = "relConstraint = FBConstraintRelation('" + SceneName + ":" + controlName + "_Trans')\n"
##                HasDestinationArray.append("LayoutConstraint('" + controlName + "_Trans')\n")
                HasDestinationArray.append("\n")
            else:
                pythonFilePath = (lFilePath + "\\" + SceneName + "_RelationCode\\" + controlName + "_Relation_Rot.py")
                Title = "# " + controlName + "_Relation_Rot.py\n"
                Relation = "relConstraint = FBConstraintRelation('" + SceneName + ":" + controlName + "_Rot')\n"
##                HasDestinationArray.append("LayoutConstraint('" + controlName + "_Rot')\n")
                HasDestinationArray.append("\n")
                
            pythonFile = open(pythonFilePath, 'w')
            joinedvariableArray = ""
            joinedvariableArray = joinedvariableArray.join(variableArray)
            del variableArray
            variableArray = []
            for i in range(len(globalArray)):
                if i == 2:
                    if globalArray[i] == Title:
                        globalArray.remove(Title)
                    globalArray.insert( i, Title)
                elif i == 5:
                    if globalArray[i] == Relation:
                        globalArray.remove(Relation)
                    globalArray.insert( i, Relation)
                elif i == 6:
                    globalArray.insert( i, joinedvariableArray)

            for destination in HasDestinationArray:
                globalArray.append(destination)
            del HasDestinationArray[:]
            for code in globalArray:
            
                pythonFile.writelines(code)
                
            pythonFile.close()
        if node2.nodeType != node.TEXT_NODE:
            controlName = GetControllerName(node2)
            controlNameArray.append(controlName + "\n")
            loopCounter = loopCounter + 1
            del globalArray
            globalArray = []
            globalArray.append("from pyfbsdk import *\n")
            globalArray.append("#----------------------------------------------------------------------\n")
            globalArray.append("# This Python Script Has Been Generated By XML2Python.py\n")
            globalArray.append("#----------------------------------------------------------------------\n")
            globalArray.append("\n")
            globalArray.append("def FindAnimationNode(pParent, pName):\n")
            globalArray.append("    lResult = None\n")
            globalArray.append("    for lNode in pParent.Nodes:\n")
            globalArray.append("        if lNode.Name == pName:\n")
            globalArray.append("            lResult = lNode\n")
            globalArray.append("            break\n")
            globalArray.append("    return lResult\n")
            globalArray.append("\n")
            globalArray.append("def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):\n")
            globalArray.append("    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )\n")
            globalArray.append("    mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )\n")
            globalArray.append("    if mynodeOut and mynodeIn:\n")
            globalArray.append("        FBConnect(mynodeOut, mynodeIn)\n")
            globalArray.append("\n")
            globalArray.append("def HasDestinationConnection(nodeOut,nodeOutPos):\n")
            globalArray.append("    mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )\n")
            globalArray.append("    if mynodeOut.GetDstCount() > 0:\n")
            globalArray.append("        return True\n")
            globalArray.append("    return False\n")
            globalArray.append("\n")
##            globalArray.append("spacerY = 0\n")
##            globalArray.append("\n")
##            globalArray.append("def RecurseBoxes(lBox):\n")
##            globalArray.append("    global spacerY\n")
##            globalArray.append("    global lCon\n")
##            globalArray.append("    lAnimationNodesOUT = lBox.AnimationNodeOutGet().Nodes\n")
##            globalArray.append("    for lAnimationNode in lAnimationNodesOUT:\n")
##            globalArray.append("        for i in range(lAnimationNode.GetDstCount()):\n")
##            globalArray.append("            if lAnimationNode.GetDst(0) != None:\n")
##            globalArray.append("                if lAnimationNode.GetDstCount() > 1:\n") 
##            globalArray.append("                    SenderPosition = lCon.GetBoxPosition(lBox)\n")
##            globalArray.append("                    lCon.SetBoxPosition(lAnimationNode.GetDst(i).GetOwner(), SenderPosition[1] + 550, (200 * i) + SenderPosition[2])\n")
##            globalArray.append("                    spacerY = spacerY + 1\n")
##            globalArray.append("                    RecurseBoxes(lAnimationNode.GetDst(i).GetOwner())\n")
##            globalArray.append("                else:\n")
##            globalArray.append("                    SenderPosition = lCon.GetBoxPosition(lBox)\n")
##            globalArray.append("                    lCon.SetBoxPosition(lAnimationNode.GetDst(i).GetOwner(), SenderPosition[1] + 550, SenderPosition[2])\n")
##            globalArray.append("                    RecurseBoxes(lAnimationNode.GetDst(i).GetOwner())\n")
##            globalArray.append("\n")
##            globalArray.append("def LayoutConstraint(conName):\n")
##            globalArray.append("    spacerY = 0\n")
##            globalArray.append("    Bone = conName.partition('_')\n")
##            globalArray.append("    global lCon\n")
##            globalArray.append("    lCons = FBSystem().Scene.Constraints\n")
##            globalArray.append("    for lCon in lCons:\n")
##            globalArray.append("        if lCon.Name == conName:\n")
##            globalArray.append("            receiver = []\n")
##            globalArray.append("            for lBox in lCon.Boxes:\n")
##            globalArray.append("                if not lCon.Is( FBConstraintRelation_TypeInfo() ):\n")
##            globalArray.append("                    continue\n") 
##            globalArray.append("                lOut = 0\n")
##            globalArray.append("                lIn = 0\n")
##            globalArray.append("                lAnimationNodesOUT = lBox.AnimationNodeOutGet().Nodes\n")
##            globalArray.append("                for lAnimationNode in lAnimationNodesOUT:\n")
##            globalArray.append("                    for i in range(lAnimationNode.GetDstCount()):\n")
##            globalArray.append("                        if lAnimationNode.GetDst(0) != None: \n")
##            globalArray.append("                            if not lAnimationNode.GetDst(i).GetOwner().Name.startswith(Bone[0]):\n")
##            globalArray.append("                                lOut = 1\n")
##            globalArray.append("                lAnimationNodesIN = lBox.AnimationNodeInGet().Nodes\n")
##            globalArray.append("                for lAnimationNode in lAnimationNodesIN:\n")
##            globalArray.append("                    for i in range(lAnimationNode.GetSrcCount()):\n")
##            globalArray.append("                        if lAnimationNode.GetSrc(0) != None:\n")
##            globalArray.append("                            lIn = 1\n")
##            globalArray.append("                if lOut == 0 and lIn == 1:\n")
##            globalArray.append("                    receiver.append(lBox)\n")
##            globalArray.append("                if lOut == 1 and lIn == 0 or lBox.Name.startswith('con'):\n")
##            globalArray.append("                    lCon.SetBoxPosition(lBox, 0, 200 * spacerY)\n")
##            globalArray.append("                    spacerY = spacerY + 1\n")
##            globalArray.append("                    for lAnimationNode in lAnimationNodesOUT:\n")
##            globalArray.append("                        for i in range(lAnimationNode.GetDstCount()):\n")
##            globalArray.append("                            if lAnimationNode.GetDst(0) != None: \n")
##            globalArray.append("                                if lAnimationNode.GetDstCount() > 1:\n")
##            globalArray.append("                                    SenderPosition = lCon.GetBoxPosition(lBox)\n")
##            globalArray.append("                                    lCon.SetBoxPosition(lAnimationNode.GetDst(i).GetOwner(), SenderPosition[1] + 550, (200 * i) + SenderPosition[2])\n")
##            globalArray.append("                                    spacerY = spacerY + 1\n")
##            globalArray.append("                                    RecurseBoxes(lAnimationNode.GetDst(i).GetOwner())\n")
##            globalArray.append("                                else:\n")
##            globalArray.append("                                    SenderPosition = lCon.GetBoxPosition(lBox)\n")
##            globalArray.append("                                    lCon.SetBoxPosition(lAnimationNode.GetDst(i).GetOwner(), SenderPosition[1] + 550, SenderPosition[2])\n")
##            globalArray.append("                                    RecurseBoxes(lAnimationNode.GetDst(i).GetOwner())\n")
##            globalArray.append("                if lOut == 0 and lIn == 0 and lBox.Name.startswith('Add'):\n")
##            globalArray.append("                    lBox.FBDelete() \n")
##            globalArray.append("                if lOut == 0 and lIn == 0 and lBox.Name.startswith('Vector to Number'):\n")
##            globalArray.append("                    lBox.FBDelete() \n")
##            globalArray.append("\n") 
            globalArray.append("relConstraint.Active = True\n")
            globalArray.append("\n") 
            relConstraint = controlName
            del lstRecievers[:]
            del lstSources[:]
            del lstPipes[:]
            del lstMods[:]
            ProcessController(relConstraint, node2)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------               
def ProcessController(relConstraint, node):
    targetControlName = ""
    controllerName = ""
    if node.attributes["type"].value == "rage__AeXYZController":
        for node2 in node.childNodes:
            if node2.nodeName == "id":
                targetControlName = str(node2.firstChild.nodeValue)
                controllerName = targetControlName
                index = targetControlName.find("xyz")
                targetControlName = targetControlName[:index-1]
            if node2.nodeName == "pXController" or node2.nodeName == "pYController" or node2.nodeName == "pZController":
                if node2.attributes["type"].value == "rage__AeExpressionController":
                    for node3 in node2.childNodes:
                        if node3.nodeName == "pExprTree":
                            for node4 in node3.childNodes:
                                if node4.nodeType != node.TEXT_NODE:
                                    if node2.nodeName == "pXController":
                                        ProcessExpression(relConstraint, node4, targetControlName, "X", controllerName)
                                    elif node2.nodeName == "pYController":
                                        ProcessExpression(relConstraint, node4, targetControlName, "Y", controllerName)
                                    elif node2.nodeName == "pZController":
                                        ProcessExpression(relConstraint, node4, targetControlName, "Z", controllerName)
                elif node2.attributes["type"].value == "rage__AeBlendController":
                    for node3 in node2.childNodes:
                                for node4 in node3.childNodes:
                                    for node5 in node4.childNodes:
                                        if node5.nodeName == "pController":
                                            for node6 in node5.childNodes:
                                                if node6.nodeName == "id":
                                                    targetControlName = str(node6.firstChild.nodeValue)
                                                    index = targetControlName.find("expr")
                                                    targetControlName = targetControlName[:index-1]
                                                if node5.attributes["type"].value == "rage__AeExpressionController":
                                                        if node6.nodeName == "pExprTree":
                                                            for node7 in node6.childNodes:
                                                                if node7.nodeType != node.TEXT_NODE:
                                                                    if node2.nodeName == "pXController":
                                                                        ProcessExpression(relConstraint, node7, targetControlName, "X", controllerName)
                                                                    elif node2.nodeName == "pYController": 
                                                                        ProcessExpression(relConstraint, node7, targetControlName, "Y", controllerName)
                                                                    elif node2.nodeName == "pZController": 
                                                                        ProcessExpression(relConstraint, node7, targetControlName, "Z", controllerName)
                            
    elif node.attributes["type"].value == "rage__AeBlendController": 
        for node2 in node.childNodes:
            if node2.nodeName == "id":
                targetControlName = str(node2.firstChild.nodeValue)
                controllerName = targetControlName
                index = targetControlName.find("blend")
                targetControlName = targetControlName[:index-1]   
            for node3 in node2.childNodes:
                for node4 in node3.childNodes:
                    if node4.nodeName == "pController":
                        for node5 in node4.childNodes:
                            if node5.nodeName == "pXController" or node5.nodeName == "pYController" or node5.nodeName == "pZController":
                                if node5.attributes["type"].value == "rage__AeExpressionController":
                                    for node6 in node5.childNodes:
                                        if node6.nodeName == "pExprTree":
                                            for node7 in node6.childNodes:
                                                if node7.nodeType != node.TEXT_NODE:
                                                    if node5.nodeName == "pXController":
                                                        ProcessExpression(relConstraint, node7, targetControlName, "X", controllerName)
                                                    elif node5.nodeName == "pYController":
                                                        ProcessExpression(relConstraint, node7, targetControlName, "Y", controllerName)
                                                    elif node5.nodeName == "pZController":
                                                        ProcessExpression(relConstraint, node7, targetControlName, "Z", controllerName)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------                                  
def ProcessExpression(relConstraint, node, targetControlName, axis, controllerName):
    
    global MultiplyCounter
    global AddCounter
    global RToDCounter
    global Trans
    global Y
    global LayoutArray
    
    control = ProcessControlBox(relConstraint, node, None, axis)
    if control != None:
    
        ###############################################################################
        globalArray.append("## Put placement code here \n")
        globalArray.append("\n")
        globalArray.append("" + control + "Pos = relConstraint.GetBoxPosition(" + control + ")\n")
        globalArray.append("relConstraint.SetBoxPosition(" + control + ", " + control + "Pos[1], (" + control + "Pos[2] * " + str(Y) + "))\n")
        for i in range(len(LayoutArray)):
            globalArray.append(LayoutArray[(len(LayoutArray) - 1) - i])
        del LayoutArray[:]
        globalArray.append("\n")
        Y = Y + 2
        ###############################################################################
        globalArray.append("model = FBFindModelByLabelName('" + targetControlName + "')\n")
        globalArray.append("\n")
        model = FBFindModelByLabelName(targetControlName)
         
        if model == None:
            print "Error: Model does not exist in scene: " + targetControlName
            return
         
        pipConverterBox = None
        converterBox = None
        axisConverterBox = None
        modelReciever = None
        convertType = None
        go = False
         
        for pipe in lstPipes:
            if pipe.sourceName == model.Name:
                pipConverterBox = pipe.sourceName
        
        if pipConverterBox == None:
            globalArray.append("pipConverterBox = relConstraint.CreateFunctionBox('Converters', 'Vector To Number')\n")
            globalArray.append("pipSource = relConstraint.SetAsSource(model)\n")
            globalArray.append("pipSource.UseGlobalTransforms = False\n")
            globalArray.append("\n")   
                
            for bone in lstBones:
                if bone.name == targetControlName:
                    for transform in bone.transforms:
                        if transform.name == "translate" and transform.controllerName == controllerName:
                            convertType = "translate"
                            globalArray.append("ConnectBox(pipSource, 'Lcl Translation', pipConverterBox, 'V')\n")
                            globalArray.append("\n")
                        elif transform.name == "rotate" and transform.controllerName == controllerName:
                            convertType = "rotate"
                            globalArray.append("ConnectBox(pipSource, 'Lcl Rotation', pipConverterBox, 'V')\n")
                            globalArray.append("\n")
                
            newSource = SourceEntry()
            newSource.sourceName = model.Name            
            lstPipes.append(newSource)
        else:
            for bone in lstBones:
                if bone.name == targetControlName:
                    for transform in bone.transforms:
                        if transform.name == "translate" and transform.controllerName == controllerName:
                            convertType = "translate"
                        elif transform.name == "rotate" and transform.controllerName == controllerName:
                            convertType = "rotate"
                            
        for reciever in lstRecievers:
            if reciever.name == model.Name:
                go = True
                converterBox = model.Name
                
        if converterBox == None:
            globalArray.append("modelReciever = relConstraint.ConstrainObject(model)\n") 
            globalArray.append("\n") 
            globalArray.append("converterBox = relConstraint.CreateFunctionBox('Converters', 'Number To Vector')\n")
            globalArray.append("\n")
            modelReciever = model
            if modelReciever == None:
                for recieverBox in lstRecieverBoxes:
                    if recieverBox.name == model.Name:
                        modelReciever = recieverBox.reciever
            else:
                globalArray.append("modelReciever.UseGlobalTransforms = False\n")
                globalArray.append("\n")


        for bone in lstBones:
            if bone.name == targetControlName:
                for transform in bone.transforms:
                    if transform.name == "translate" and transform.controllerName == controllerName:
                        Trans = 1
                    elif transform.name == "rotate" and transform.controllerName == controllerName:
                        Trans = 0

        if axis == "X":
            globalArray.append("addBox" + str(AddCounter) + " = relConstraint.CreateFunctionBox('Number', 'Add (a + b)')\n")
            globalArray.append("\n")
            
            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
            globalArray.append("Result = None\n")
            
            globalArray.append("for outNode in lAnimationNodesOUT:\n")
            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
            globalArray.append("        Result = outNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("ConnectBox(" + control + ", Result, addBox" + str(AddCounter) + ", 'a')\n")
            ##########################################################################################
            globalArray.append("" + control + "Pos = relConstraint.GetBoxPosition(" + control + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(addBox" + str(AddCounter) + ", (" + control + "Pos[1] + 400), " + control + "Pos[2])\n")
            ##########################################################################################
            if Trans == 1:
                globalArray.append("ConnectBox(pipConverterBox, 'X', addBox" + str(AddCounter) + ", 'b')\n")
                #####################################################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                #####################################################################################################################
            else:
                globalArray.append("axisConverterDegToRadX = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                globalArray.append("ConnectBox(pipConverterBox, 'X', axisConverterDegToRadX, 'a')\n")
                globalArray.append("ConnectBox(axisConverterDegToRadX, 'Result', addBox" + str(AddCounter) + ", 'b')\n")
                ##########################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(axisConverterDegToRadX, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("axisConverterDegToRadXPos = relConstraint.GetBoxPosition(axisConverterDegToRadX)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (axisConverterDegToRadXPos[1] - 400), axisConverterDegToRadXPos[2])\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                ##########################################################################################

            globalArray.append("ConnectBox(addBox" + str(AddCounter) + ", 'Result', converterBox, 'X')\n")
            ##########################################################################################
            globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(converterBox, (addBox" + str(AddCounter) + "Pos[1] + 400), addBox" + str(AddCounter) + "Pos[2])\n")
            ##########################################################################################
            globalArray.append("\n") 

        elif axis == "Y":
            globalArray.append("addBox" + str(AddCounter) + " = relConstraint.CreateFunctionBox('Number', 'Add (a + b)')\n")
            globalArray.append("\n")
            
            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
            globalArray.append("Result = None\n")
            
            globalArray.append("for outNode in lAnimationNodesOUT:\n")
            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
            globalArray.append("        Result = outNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("ConnectBox(" + control + ", Result, addBox" + str(AddCounter) + ", 'a')\n")
            ##########################################################################################
            globalArray.append("" + control + "Pos = relConstraint.GetBoxPosition(" + control + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(addBox" + str(AddCounter) + ", (" + control + "Pos[1] + 400), " + control + "Pos[2])\n")
            ##########################################################################################\n")
            if Trans == 1:
                globalArray.append("ConnectBox(pipConverterBox, 'Y', addBox" + str(AddCounter) + ", 'b')\n")
                #####################################################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                #####################################################################################################################
            else:
                globalArray.append("axisConverterDegToRadY = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                globalArray.append("ConnectBox(pipConverterBox, 'Y', axisConverterDegToRadY, 'a')\n")
                globalArray.append("ConnectBox(axisConverterDegToRadY, 'Result', addBox" + str(AddCounter) + ", 'b')\n")
                ##########################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(axisConverterDegToRadY, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("axisConverterDegToRadYPos = relConstraint.GetBoxPosition(axisConverterDegToRadY)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (axisConverterDegToRadYPos[1] - 400), axisConverterDegToRadYPos[2])\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                ##########################################################################################
            
            globalArray.append("ConnectBox(addBox" + str(AddCounter) + ", 'Result', converterBox, 'Y')\n")
            ##########################################################################################
            globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(converterBox, (addBox" + str(AddCounter) + "Pos[1] + 400), addBox" + str(AddCounter) + "Pos[2])\n")
            ##########################################################################################
            globalArray.append("\n") 

        elif axis == "Z":
            globalArray.append("addBox" + str(AddCounter) + " = relConstraint.CreateFunctionBox('Number', 'Add (a + b)')\n")
            globalArray.append("\n")
            
            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
            globalArray.append("Result = None\n")
            
            globalArray.append("for outNode in lAnimationNodesOUT:\n")
            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
            globalArray.append("        Result = outNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("ConnectBox(" + control + ", Result, addBox" + str(AddCounter) + ", 'a')\n")
            ##########################################################################################
            globalArray.append("" + control + "Pos = relConstraint.GetBoxPosition(" + control + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(addBox" + str(AddCounter) + ", (" + control + "Pos[1] + 400), " + control + "Pos[2])\n")
            ##########################################################################################
            if Trans == 1:
                globalArray.append("ConnectBox(pipConverterBox, 'Z', addBox" + str(AddCounter) + ", 'b')\n")
                #####################################################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                #####################################################################################################################
            else:
                globalArray.append("axisConverterDegToRadZ = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                globalArray.append("ConnectBox(pipConverterBox, 'Z', axisConverterDegToRadZ, 'a')\n")
                globalArray.append("ConnectBox(axisConverterDegToRadZ, 'Result', addBox" + str(AddCounter) + ", 'b')\n")
                ##########################################################################################
                globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
                globalArray.append("relConstraint.SetBoxPosition(axisConverterDegToRadZ, (addBox" + str(AddCounter) + "Pos[1] - 400), (addBox" + str(AddCounter) + "Pos[2] + 200))\n")
                globalArray.append("axisConverterDegToRadZPos = relConstraint.GetBoxPosition(axisConverterDegToRadZ)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipConverterBox, (axisConverterDegToRadZPos[1] - 400), axisConverterDegToRadZPos[2])\n")
                globalArray.append("pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                globalArray.append("relConstraint.SetBoxPosition(pipSource, (pipConverterBoxPos[1] - 400), pipConverterBoxPos[2])\n")
                ##########################################################################################
                
            globalArray.append("ConnectBox(addBox" + str(AddCounter) + ", 'Result', converterBox, 'Z')\n")
            ##########################################################################################
            globalArray.append("addBox" + str(AddCounter) + "Pos = relConstraint.GetBoxPosition(addBox" + str(AddCounter) + ")\n")
            globalArray.append("relConstraint.SetBoxPosition(converterBox, (addBox" + str(AddCounter) + "Pos[1] + 400), addBox" + str(AddCounter) + "Pos[2])\n")
            ##########################################################################################
            globalArray.append("\n") 

            AddCounter = AddCounter + 1
    
        if go == True:
            return
            
        if axis == "X":
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'Y') == False:\n")

            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Y', converterBox, 'Y')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadY = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Y', axisConverterDegToRadY, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadY, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] - 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadY, 'Result', converterBox, 'Y')\n")
                HasDestinationArray.append("\n")
   
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'Z') == False:\n")
            
            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Z', converterBox, 'Z')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadZ = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Z', axisConverterDegToRadZ, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadZ, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] + 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadZ, 'Result', converterBox, 'Z')\n")
                HasDestinationArray.append("\n")
            
        elif axis == "Y":
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'X') == False:\n")
            
            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'X', converterBox, 'X')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadX = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'X', axisConverterDegToRadX, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadX, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] - 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadX, 'Result', converterBox, 'X')\n")
                HasDestinationArray.append("\n")  
    
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'Z') == False:\n")
            
            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Z', converterBox, 'Z')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadZ = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Z', axisConverterDegToRadZ, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadZ, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] - 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadZ, 'Result', converterBox, 'Z')\n")
                HasDestinationArray.append("\n")
            
        elif axis == "Z":
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'X') == False:\n")
            
            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'X', converterBox, 'X')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadX = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'X', axisConverterDegToRadX, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadX, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] - 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadX, 'Result', converterBox, 'X')\n")
                HasDestinationArray.append("\n")  
    
            HasDestinationArray.append("if HasDestinationConnection(pipConverterBox, 'Y') == False:\n")
            
            if Trans == 1:
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Y', converterBox, 'Y')\n")
                HasDestinationArray.append("\n")
            else:
                HasDestinationArray.append("    axisConverterDegToRadY = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                HasDestinationArray.append("    ConnectBox(pipConverterBox, 'Y', axisConverterDegToRadY, 'a')\n")
                ##########################################################################################
                HasDestinationArray.append("    pipConverterBoxPos = relConstraint.GetBoxPosition(pipConverterBox)\n")
                HasDestinationArray.append("    relConstraint.SetBoxPosition(axisConverterDegToRadX, (pipConverterBoxPos[1] + 400), (pipConverterBoxPos[2] - 100))\n")
                ##########################################################################################
                HasDestinationArray.append("    ConnectBox(axisConverterDegToRadY, 'Result', converterBox, 'Y')\n")
                HasDestinationArray.append("\n")
        
        globalArray.append("axisConverterBox = relConstraint.CreateFunctionBox('Converters', 'Vector To Number')\n")
        globalArray.append("axisConverterBox2 = relConstraint.CreateFunctionBox('Converters', 'Number To Vector')\n")
        
##        if Trans == 1:
##            globalArray.append("ConnectBox(axisConverterBox2, 'Result', modelReciever, 'Lcl Translation')\n")
##            globalArray.append("\n")
##        else:
##            globalArray.append("ConnectBox(axisConverterBox2, 'Result', modelReciever, 'Lcl Rotation')\n")
##            globalArray.append("\n")
                        
        if Trans == 1:
            
            globalArray.append("ConnectBox(converterBox, 'Result', axisConverterBox, 'V')\n")
            
            ################################################################################################
            globalArray.append("converterBoxPos = relConstraint.GetBoxPosition(converterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterBox, (converterBoxPos[1] + 400), converterBoxPos[2])\n")
            ################################################################################################
            
            globalArray.append("multiplyBoxA = relConstraint.CreateFunctionBox('Number', 'Multiply (a x b)')\n")
            globalArray.append("ConnectBox(axisConverterBox, 'X', axisConverterBox2, 'X')\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterBox2, (axisConverterBoxPos[1] + 800), axisConverterBoxPos[2])\n")
            ################################################################################################
            
            globalArray.append("ConnectBox(axisConverterBox, 'Y', multiplyBoxA, 'a')\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(multiplyBoxA, (axisConverterBoxPos[1] + 400), (axisConverterBoxPos[2] + 200))\n")
            ################################################################################################
            
            globalArray.append("ConnectBox(multiplyBoxA, 'Result', axisConverterBox2, 'Y')\n")
            globalArray.append("ConnectBox(axisConverterBox, 'Z', axisConverterBox2, 'Z')\n")
            globalArray.append("value = FindAnimationNode(multiplyBoxA.AnimationNodeInGet(),'b')\n")
            globalArray.append("value.WriteData([float(1)])\n")
            
            #################################################################################################
            globalArray.append("ConnectBox(axisConverterBox2, 'Result', modelReciever, 'Lcl Translation')\n")
            globalArray.append("axisConverterBox2Pos = relConstraint.GetBoxPosition(axisConverterBox2)\n")
            globalArray.append("relConstraint.SetBoxPosition(modelReciever, (axisConverterBox2Pos[1] + 400), axisConverterBox2Pos[2])\n")
            #################################################################################################
            
            globalArray.append("\n")
            
        else:
            
            globalArray.append("axisConverterRadToDegX = relConstraint.CreateFunctionBox('Converters', 'Rad To Deg')\n")
            globalArray.append("axisConverterRadToDegY = relConstraint.CreateFunctionBox('Converters', 'Rad To Deg')\n")
            globalArray.append("axisConverterRadToDegZ = relConstraint.CreateFunctionBox('Converters', 'Rad To Deg')\n")
            
            globalArray.append("ConnectBox(converterBox, 'Result', axisConverterBox, 'V')\n")
            
            ################################################################################################
            globalArray.append("converterBoxPos = relConstraint.GetBoxPosition(converterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterBox, (converterBoxPos[1] + 400), converterBoxPos[2])\n")
            ################################################################################################
            
            globalArray.append("multiplyBoxA = relConstraint.CreateFunctionBox('Number', 'Multiply (a x b)')\n")
            globalArray.append("ConnectBox(axisConverterBox, 'X', axisConverterRadToDegX, 'a')\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterBox2, (axisConverterBoxPos[1] + 900), axisConverterBoxPos[2])\n")
            ################################################################################################
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterRadToDegX, (axisConverterBoxPos[1] + 500), (axisConverterBoxPos[2] - 200))\n")
            ################################################################################################
            
            globalArray.append("ConnectBox(axisConverterRadToDegX, 'Result', axisConverterBox2, 'X')\n")
            globalArray.append("ConnectBox(axisConverterBox, 'Y', multiplyBoxA, 'a')\n")
            globalArray.append("ConnectBox(multiplyBoxA, 'Result', axisConverterRadToDegY, 'a')\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(multiplyBoxA, (axisConverterBoxPos[1] + 300), (axisConverterBoxPos[2]))\n")
            ################################################################################################
            
            globalArray.append("ConnectBox(axisConverterRadToDegY, 'Result', axisConverterBox2, 'Y')\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterRadToDegY, (axisConverterBoxPos[1] + 600), axisConverterBoxPos[2])\n")
            ################################################################################################
            
            globalArray.append("ConnectBox(axisConverterBox, 'Z', axisConverterRadToDegZ, 'a')\n")
            globalArray.append("ConnectBox(axisConverterRadToDegZ, 'Result', axisConverterBox2, 'Z')\n")
            globalArray.append("value = FindAnimationNode(multiplyBoxA.AnimationNodeInGet(),'b')\n")
            globalArray.append("value.WriteData([float(1)])\n")
            
            ################################################################################################
            globalArray.append("axisConverterBoxPos = relConstraint.GetBoxPosition(axisConverterBox)\n")
            globalArray.append("relConstraint.SetBoxPosition(axisConverterRadToDegZ, (axisConverterBoxPos[1] + 500), (axisConverterBoxPos[2] + 200))\n")
            ################################################################################################
            
            #################################################################################################
            globalArray.append("ConnectBox(axisConverterBox2, 'Result', modelReciever, 'Lcl Rotation')\n")
            globalArray.append("axisConverterBox2Pos = relConstraint.GetBoxPosition(axisConverterBox2)\n")
            globalArray.append("relConstraint.SetBoxPosition(modelReciever, (axisConverterBox2Pos[1] + 400), axisConverterBox2Pos[2])\n")
            globalArray.append("\n")
            #################################################################################################
            globalArray.append("\n")

            
        newReciever = RecieverEntry()
        newReciever.name = model.Name 
                                        
        lstRecievers.append(newReciever)
        
        for recieverBox in lstRecieverBoxes:
            if recieverBox.name == model.Name:
                return
                
        box = RecieverBox()
        box.name = model.Name
        box.reciever = modelReciever
        lstRecieverBoxes.append(box)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------                     
def AddModelProperties(modelName, modelSender, modelConverter):
    mod = Mod()
    mod.name = modelName
    mod.sender = modelSender
    mod.converter = modelConverter
    lstMods.append(mod)
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------          
def ModelHasProperties(modelName):
    for model in lstMods:
        if model.name == modelName:
            return 1
    return 0
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------          
def GetMod(modelName):
    for model in lstMods:
        if model.name == modelName:
            return model
    return None
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------         
def ProcessEntry(relConstraint, parentControl, node, position, axis):
    
    global modelCounter
    global converterDegToRadCounter
    global LayoutArray

    if node.attributes["type"].value == "rage__ExprNodeBinaryOperator":
        if position == "pLeft" or position == "pChild":    
            control = ProcessControlBox(relConstraint, node, parentControl, axis)

            globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
            globalArray.append("a = None\n")
            
            globalArray.append("for inNode in lAnimationNodesIN:\n")
            globalArray.append("    if inNode.Label == 'a' or inNode.Name == 'a':\n")
            globalArray.append("        a = inNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
            globalArray.append("Result = None\n")
            
            globalArray.append("for outNode in lAnimationNodesOUT:\n")
            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
            globalArray.append("        Result = outNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("ConnectBox(" + control + ", Result, " +  parentControl + ", a)\n")
            ######################################################################################
            LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + control + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
            #######################################################################################
            globalArray.append("\n")
        elif position == "pRight":    
            control = ProcessControlBox( relConstraint, node, parentControl, axis)

            globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
            globalArray.append("b = None\n")
            
            globalArray.append("for inNode in lAnimationNodesIN:\n")
            globalArray.append("    if inNode.Label == 'b' or inNode.Name == 'b':\n")
            globalArray.append("        b = inNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
            globalArray.append("Result = None\n")
            
            globalArray.append("for outNode in lAnimationNodesOUT:\n")
            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
            globalArray.append("        Result = outNode.Name\n")
            globalArray.append("\n")
            
            globalArray.append("ConnectBox(" + control + ", Result, " +  parentControl + ", b)\n")
            ######################################################################################
            LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + control + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
            #######################################################################################
            globalArray.append("\n")
            
    if node.attributes["type"].value == "rage__ExprNodeTernaryOperator":
          
            control = ProcessControlBox( relConstraint, node, parentControl, axis)

            globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
            globalArray.append("a = None\n")
            
            globalArray.append("for inNode in lAnimationNodesIN:\n")
            globalArray.append("    if inNode.Label == 'a' or inNode.Name == 'a':\n")
            globalArray.append("        a = inNode.Name\n")
            
            globalArray.append("ConnectBox(" + control + ", 'Result', " +  parentControl + ", a)\n")
            ######################################################################################
            LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + control + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
            #######################################################################################
            globalArray.append("\n")
    
    if node.attributes["type"].value == "rage__ExprNodeConstant":
        for node2 in node.childNodes:
            if node2.nodeName == "value":
                if position == "pLeft" or position == "pMiddle" or position == "pChild":

                        globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
                        globalArray.append("a = None\n")
                        
                        globalArray.append("for inNode in lAnimationNodesIN:\n")
                        globalArray.append("    if inNode.Label == 'a' or inNode.Name == 'a':\n")
                        globalArray.append("        a = inNode.Name\n")
                        
                        variableArray.append("" + parentControl + "FloatA = " + str("[float(" + node2.attributes["value"].value + ")]") + "\n")
                        globalArray.append("value = FindAnimationNode(" + parentControl + ".AnimationNodeInGet(), a)\n")
                        globalArray.append("value.WriteData( " + parentControl + "FloatA  )\n")
                        globalArray.append("\n")    
                elif position == "pRight":

                        globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
                        globalArray.append("b = None\n")
                        
                        globalArray.append("for inNode in lAnimationNodesIN:\n")
                        globalArray.append("    if inNode.Label == 'b' or inNode.Name == 'b':\n")
                        globalArray.append("        b = inNode.Name\n")
                        
                        variableArray.append("" + parentControl + "FloatB = " + str("[float(" + node2.attributes["value"].value + ")]") + "\n")
                        globalArray.append("value = FindAnimationNode(" + parentControl + ".AnimationNodeInGet(), b)\n")
                        globalArray.append("value.WriteData( " + parentControl + "FloatB )\n")
                        globalArray.append("\n")                   
    
    if node.attributes["type"].value == "rage__ExprNodeUnaryOperator":
        
        control = ProcessControlBox( relConstraint, node, parentControl, axis)
        
        if position == "pLeft" or position == "pMiddle":
            globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
            globalArray.append("a = None\n")
            
            globalArray.append("for inNode in lAnimationNodesIN:\n")
            globalArray.append("    if inNode.Label == 'a' or inNode.Name == 'a':\n")
            globalArray.append("        a = inNode.Name\n")
            globalArray.append("\n")
        
        elif position == "pRight":
            globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
            globalArray.append("b = None\n")
            
            globalArray.append("for inNode in lAnimationNodesIN:\n")
            globalArray.append("    if inNode.Label == 'b' or inNode.Name == 'b':\n")
            globalArray.append("        b = inNode.Name\n")
            globalArray.append("\n")
        
        globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
        globalArray.append("Result = None\n")
        
        globalArray.append("for outNode in lAnimationNodesOUT:\n")
        globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
        globalArray.append("        Result = outNode.Name\n")
        globalArray.append("\n")
        
        if position == "pLeft" or position == "pMiddle":
            globalArray.append("ConnectBox(" + control + ", Result, " +  parentControl + ", a)\n")
            ######################################################################################
            LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + control + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
            #######################################################################################
            globalArray.append("\n") 
        
        elif position == "pRight":
            globalArray.append("ConnectBox(" + control + ", Result, " +  parentControl + ", b)\n")
            ######################################################################################
            LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + control + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
            #######################################################################################
            globalArray.append("\n")  
        
                        
    if node.attributes["type"].value == "rage__ExprNodeNameAttrPair":
        attrName = ""
        nodeName = ""
        for node2 in node.childNodes:
                if node2.nodeName == "name":
                    nodeName = str(node2.firstChild.nodeValue)
                elif node2.nodeName == "attr":
                    attrName = str(node2.firstChild.nodeValue)
                    
                    if attrName == "translateX":
                        model = FBFindModelByLabelName(nodeName)
                        print "ParentControl: " + parentControl
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName 
                            return
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            if position == "pLeft" or position == "pChild":
                                
                                globalArray.append("zeroControllerTransX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                                
##                                globalArray.append("ConnectBox(converterBox1, 'X', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerTransX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransX, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                                
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")    
                            elif position == "pRight":

                                globalArray.append("zeroControllerTransX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox(converterBox1, 'X', " + parentControl + ", 'b')\n")                                
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerTransX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransX, 'Result', " + parentControl + ", 'b')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n") 
                        else:
                            modelCounter = modelCounter + 1 
                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n") 

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n")

                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Translation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":

                                globalArray.append("zeroControllerTransX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'X', " + parentControl + ", 'a')\n")                               
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerTransX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransX, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":

                                globalArray.append("zeroControllerTransX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox(converterBox1, 'X', " + parentControl + ", 'b')\n")                                
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerTransX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransX, 'Result', " + parentControl + ", 'b')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                                
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")                
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")
                    
                    elif attrName == "translateY":
                        model = FBFindModelByLabelName(nodeName)
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName 
                            return
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            if position == "pLeft" or position == "pChild":
                                
                                globalArray.append("zeroControllerTransY = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Y', " + parentControl + ", 'a')\n")                              
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerTransY, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransY.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransY, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
                                
                                globalArray.append("zeroControllerTransY = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Y', " + parentControl + ", 'b')\n")                             
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerTransY, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransY.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransY, 'Result', " + parentControl + ", 'b')\n") 
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n") 
                        else:
                            modelCounter = modelCounter + 1 
                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n") 

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n")

                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Translation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":
                                globalArray.append("zeroControllerTransY = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Y', " + parentControl + ", 'a')\n")                              
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerTransY, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransY.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransY, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
                                
                                globalArray.append("zeroControllerTransY = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Y', " + parentControl + ", 'b')\n")                             
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerTransY, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransY.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransY, 'Result', " + parentControl + ", 'b')\n") 
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")           
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")
                            
                    elif attrName == "translateZ":
                        model = FBFindModelByLabelName(nodeName)
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName 
                            return
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            if position == "pLeft" or position == "pChild":
                                
                                globalArray.append("zeroControllerTransZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox(converterBox1, 'Z', " + parentControl + ", 'a')\n")                             
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerTransZ, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransZ.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransZ, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")       
                            elif position == "pRight":

                                globalArray.append("zeroControllerTransZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox(converterBox1, 'Z', " + parentControl + ", 'b')\n")                          
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerTransZ, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransZ.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransZ, 'Result', " + parentControl + ", 'b')\n") 
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")   
                        else:
                            modelCounter = modelCounter + 1 
                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n")

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n") 

                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Translation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":
                                globalArray.append("zeroControllerTransZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox(converterBox1, 'Z', " + parentControl + ", 'a')\n")                             
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerTransZ, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransZ.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransZ, 'Result', " + parentControl + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")    
                            elif position == "pRight":
                                
                                
                                globalArray.append("zeroControllerTransZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Z', " + parentControl + ", 'b')\n")                         
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerTransZ, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerTransZ.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Translation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerTransZ, 'Result', " + parentControl + ", 'b')\n") 
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterBox1, (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")               
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")
                            
                    elif attrName == "rotateX":
                        model = FBFindModelByLabelName(nodeName)
                        CurrentConverterDegToRadCounter = converterDegToRadCounter + 1
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName 
                            return
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            
                            ExistsInCode = 0
                            
                            for line in globalArray:
                                if line.startswith("converterDegToRadX" + model.Name):
                                    ExistsInCode = 1
                            if ExistsInCode == 0:
                                globalArray.append("converterDegToRadX" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                                
                                globalArray.append("zeroControllerX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                                
##                                globalArray.append("ConnectBox( converterBox1, 'X', converterDegToRadX" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                               
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerX, 'Result', converterDegToRadX" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                                                                
                                ######################################################################################
                                LayoutArray.append("converterDegToRadX" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadX" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadX" + model.Name + "Pos[1] - 400), converterDegToRadX" + model.Name + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n") 
                            
                            if position == "pLeft" or position == "pChild": 
                                globalArray.append("ConnectBox( converterDegToRadX" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadX" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")    
                            elif position == "pRight":
                                globalArray.append("ConnectBox( converterDegToRadX" + model.Name + ", 'Result', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadX" + model.Name + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")  
                        else:
                            modelCounter = modelCounter + 1 
                            converterDegToRadCounter = converterDegToRadCounter + 1

                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSenderRotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n") 

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n") 
                            
                            globalArray.append("zeroControllerX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                            
                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Rotation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 

                            globalArray.append("converterDegToRadX" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
##                            globalArray.append("ConnectBox( converterBox1, 'X', converterDegToRadX" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                            globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerX, 'a')\n")
                            globalArray.append("value = FindAnimationNode(zeroControllerX.AnimationNodeInGet(), 'b')\n")
                            globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[0]) + "] )\n")
                            globalArray.append("ConnectBox( zeroControllerX, 'Result', converterDegToRadX" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                            
                            
                            ######################################################################################
                            LayoutArray.append("converterDegToRadX" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadX" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadX" + model.Name + "Pos[1] - 400), converterDegToRadX" + model.Name + "Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 
                            
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox( converterDegToRadX" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadX" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
##                                globalArray.append("ConnectBox( converterBox1, 'X', " + parentControl + ", 'b')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                                globalArray.append("ConnectBox( converterBox1, 'X', zeroControllerX, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerX.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[0]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerX, 'Result', " + parentControl + ", 'b')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################                         
                                ######################################################################################
                                LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), (converterBox1Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")             
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")

                    elif attrName == "rotateY":
                        model = FBFindModelByLabelName(nodeName)
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName 
                            return
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            
                            ExistsInCode = 0
                            
                            for line in globalArray:
                                if line.startswith("converterDegToRadY" + model.Name):
                                    ExistsInCode = 1
                            if ExistsInCode == 0:
                                globalArray.append("converterDegToRadY" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                                globalArray.append("zeroControllerY = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                                globalArray.append("ConnectBox( converterBox1, 'Y', converterDegToRadY" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                                globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerY, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerY.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[1]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerY, 'Result', converterDegToRadY" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                                ######################################################################################
                                LayoutArray.append("converterDegToRadY" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadY" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadY" + model.Name + "Pos[1] - 400), converterDegToRadY" + model.Name + "Pos[2])\n")
                                #######################################################################################
                                
                                globalArray.append("\n") 
                            
                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox( converterDegToRadY" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadY" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                
                                globalArray.append("\n")      
                            elif position == "pRight":
                                globalArray.append("ConnectBox( converterDegToRadY" + model.Name + ", 'Result', " + parentControl + ", 'b')\n")
                                
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadY" + model.Name + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                
                                globalArray.append("\n") 
                        else:
                            modelCounter = modelCounter + 1 
                            converterDegToRadCounter = converterDegToRadCounter + 1

                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n")

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n")

                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Rotation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n")  
                            
                            globalArray.append("converterDegToRadY" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
##                            globalArray.append("ConnectBox( converterBox1, 'Y', converterDegToRadY" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                            globalArray.append("ConnectBox( converterBox1, 'Y', zeroControllerY, 'a')\n")
                            globalArray.append("value = FindAnimationNode(zeroControllerY.AnimationNodeInGet(), 'b')\n")
                            globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[1]) + "] )\n")
                            globalArray.append("ConnectBox( zeroControllerY, 'Result', converterDegToRadY" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                            ######################################################################################
                            LayoutArray.append("converterDegToRadY" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadY" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadY" + model.Name + "Pos[1] - 400), converterDegToRadY" + model.Name + "Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 
                            
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox( converterDegToRadY" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadY" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
                                globalArray.append("ConnectBox( converterDegToRadY" + model.Name + ", 'Result', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadY" + model.Name + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")           
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")
                            
                    elif attrName == "rotateZ":
                        model = FBFindModelByLabelName(nodeName)
                        
                        if model == None:
                            print "Error: Model does not exist in scene: " + nodeName
                            return 
                                        
                        if ModelHasProperties(str(nodeName)) == 1:
                            mod = GetMod(str(nodeName))
                            
                            ExistsInCode = 0
                            
                            for line in globalArray:
                                if line.startswith("converterDegToRadZ" + model.Name):
                                    ExistsInCode = 1
                            if ExistsInCode == 0:
                                globalArray.append("converterDegToRadZ" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                                
                                globalArray.append("zeroControllerZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                                
##                                globalArray.append("ConnectBox( converterBox1, 'Z', converterDegToRadZ" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                                globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerZ, 'a')\n")
                                globalArray.append("value = FindAnimationNode(zeroControllerZ.AnimationNodeInGet(), 'b')\n")
                                globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[2]) + "] )\n")
                                globalArray.append("ConnectBox( zeroControllerZ, 'Result', converterDegToRadZ" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                                ######################################################################################
                                LayoutArray.append("converterDegToRadZ" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadZ" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadZ" + model.Name + "Pos[1] - 400), converterDegToRadZ" + model.Name + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n") 
                                
                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox( converterDegToRadZ" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadZ" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
                                globalArray.append("ConnectBox( converterDegToRadZ" + model.Name + ", 'Result', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadZ" + model.Name + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n") 
                        else:
                            modelCounter = modelCounter + 1 
                            converterDegToRadCounter = converterDegToRadCounter + 1

                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n")

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n") 

                            globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", 'Lcl Rotation', converterBox1, 'V')\n")
                            ######################################################################################
                            LayoutArray.append("converterBox1Pos = relConstraint.GetBoxPosition(converterBox1)\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (converterBox1Pos[1] - 400), converterBox1Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 
                            
                            globalArray.append("converterDegToRadZ" + model.Name + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                            
                            globalArray.append("zeroControllerZ = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
##                            globalArray.append("ConnectBox( converterBox1, 'Z', converterDegToRadZ" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
                            globalArray.append("ConnectBox( converterBox1, 'Z', zeroControllerZ, 'a')\n")
                            globalArray.append("value = FindAnimationNode(zeroControllerZ.AnimationNodeInGet(), 'b')\n")
                            globalArray.append("value.WriteData( [" + str(model.PropertyList.Find("Lcl Rotation").Data[2]) + "] )\n")
                            globalArray.append("ConnectBox( zeroControllerZ, 'Result', converterDegToRadZ" + model.Name + ", 'a')\n")
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  
                            ######################################################################################
                            LayoutArray.append("converterDegToRadZ" + model.Name + "Pos = relConstraint.GetBoxPosition(converterDegToRadZ" + model.Name + ")\nrelConstraint.SetBoxPosition(converterBox1, (converterDegToRadZ" + model.Name + "Pos[1] - 400), converterDegToRadZ" + model.Name + "Pos[2])\n")
                            #######################################################################################
                            globalArray.append("\n") 
                            
                            globalArray.append("\n") 

                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox( converterDegToRadZ" + model.Name + ", 'Result', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadZ" + model.Name + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")     
                            elif position == "pRight":
                                globalArray.append("ConnectBox( converterDegToRadZ" + model.Name + ", 'Result', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(converterDegToRadZ" + model.Name + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")           
                            AddModelProperties(str(nodeName), ("modelSender" + str(modelCounter)), "converterBox1")
                            
                    else:
                        index = nodeName.find(attrName)
                        model = FBFindModelByLabelName(nodeName[:(index-1)])
                        
                        if model == None:
                            print "Error: Model does not exist in scene_: " + nodeName[:(index-1)]
                            return
                        
                        if ModelHasProperties(nodeName[:(index-1)]) == 1:
                            mod = GetMod(nodeName[:(index-1)])
                            
                            for key in d.iterkeys():
                                if cmp(str(d[key].strip()),str(attrName).strip()) == 0:
                                    attrName = key
                                    break
                            
                            foundProperty = 0
                            for prop in model.PropertyList:
                                index = prop.Name.find(attrName)
                                if index != -1:
                                    foundProperty = 1
                                    attrName = prop.Name
                                    
                            if foundProperty == 0:
##                                print "Error: Unable to find property: " + attrName
                                myString = attrName
                                rPartitionMyString = myString.rpartition("_")
                                myNewString = str(rPartitionMyString[0] + ".IM_ATTRS_A." + rPartitionMyString[2])
##                                print "Changing AttrName: " + attrName + " To " + myNewString
                                attrName = myNewString
                                #return
                                    
                            if position == "pLeft" or position == "pChild":
                                globalArray.append("ConnectBox(" + mod.sender + ", '" + attrName + "', " + parentControl + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + mod.sender + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                #######################################################################################
                                globalArray.append("\n")    
                            if position == "pRight":
                                globalArray.append("ConnectBox(" + mod.sender + ", '" + attrName + "', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(" + mod.sender + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")  
                        else:
                            modelCounter = modelCounter + 1
                            globalArray.append("model" + str(modelCounter) + " = FBFindModelByLabelName('" + model.Name + "')\n")
                            globalArray.append("modelSender" + str(modelCounter) + " = relConstraint.SetAsSource(model" + str(modelCounter) + ")\n")
                            globalArray.append("modelSender" + str(modelCounter) + ".UseGlobalTransforms = False\n")
                            globalArray.append("## modelSender Translation = " + str(model.PropertyList.Find("Lcl Translation").Data)+ ")\n")
                            globalArray.append("## modelSender Translation X = " + str(model.PropertyList.Find("Lcl Translation").Data[0])+ ")\n")
                            globalArray.append("## modelSender Rotation = " + str(model.PropertyList.Find("Lcl Rotation").Data)+ ")\n")
                            globalArray.append("## modelSender Scaling = " + str(model.PropertyList.Find("Lcl Scaling").Data)+ ")\n")
                            globalArray.append("\n")

                            globalArray.append("converterBox1 = relConstraint.CreateFunctionBox('Converters', 'Vector to Number')\n")
                            globalArray.append("\n")
                            
                            
                            
                            
                            
                            globalArray.append("zeroInputX = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                            globalArray.append("\n")
                            
                            
                            
                            
                            
                            for key in d.iterkeys():
                                if cmp(str(d[key].strip()),str(attrName).strip()) == 0:
                                    attrName = key
                                    break

                            foundProperty = 0
                            for prop in model.PropertyList:
                                index = prop.Name.find(attrName)
                                if index != -1:
                                    foundProperty = 1
                                    attrName = prop.Name
                                    
                            if foundProperty == 0:
##                                print "Error: Unable to find property: " + attrName
                                myString = attrName
                                rPartitionMyString = myString.rpartition("_")
                                myNewString = str(rPartitionMyString[0] + ".IM_ATTRS_A." + rPartitionMyString[2])
##                                print "Changing AttrName: " + attrName + " To " + myNewString
                                attrName = myNewString
                                #return
                                    
                            if position == "pLeft" or position == "pChild":

                                    globalArray.append("lAnimationNodesIN = " + parentControl + ".AnimationNodeInGet().Nodes\n")
                                    globalArray.append("a = None\n")
                                    
                                    globalArray.append("for inNode in lAnimationNodesIN:\n")
                                    globalArray.append("    if inNode.Label == 'a' or inNode.Name == 'a':\n")
                                    globalArray.append("        a = inNode.Name\n")
                                    
                                    globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", '" + attrName + "', " + parentControl + ", 'a')\n") 
                                    ######################################################################################
                                    LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (" + parentControl + "Pos[1] - 400), " +  parentControl + "Pos[2])\n")
                                    #######################################################################################
                                    globalArray.append("\n")   
                            if position == "pRight":
                                globalArray.append("ConnectBox( modelSender" + str(modelCounter) + ", '" + attrName + "', " + parentControl + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("" + parentControl + "Pos = relConstraint.GetBoxPosition(" + parentControl + ")\nrelConstraint.SetBoxPosition(modelSender" + str(modelCounter) + ", (" + parentControl + "Pos[1] - 400), (" +  parentControl + "Pos[2] + 100))\n")
                                #######################################################################################
                                globalArray.append("\n")
                            AddModelProperties(str(nodeName[:(index-1)]), ("modelSender" + str(modelCounter)), "converterBox1")
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------      
def ProcessControlBox(relConstraint, node, parentBox, axis):
    
    global GreaterThanEqualToCounter
    global GreaterThanCounter
    global LessThanCounter
    global LessThanEqualToCounter
    global MultiplyCounter
    global DivideCounter
    global EqualToCounter
    global NotEqualToCounter
    global AddCounter
    global SubtractCounter
    global SqrtCounter
    global AbsCounter
    global CosCounter
    global SinCounter
    global DToRCounter
    global RToDCounter
    global IfThenElseCounter
    global MinCounter
    global MaxCounter
    
    global LayoutArray
    
    foundOpType = 0
    for node2 in node.childNodes:
        if node2.nodeName == "opType":
            foundOpType = 1
            if node2.firstChild.nodeValue == "kGreaterThanEqualTo":
                GreaterThanEqualToCounter = GreaterThanEqualToCounter + 1
                CurrentGreaterThanEqualToCounter = GreaterThanEqualToCounter
                globalArray.append("ifGreaterEqualBox" + str(GreaterThanEqualToCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Greater or Equal (a >= b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("ifGreaterEqualBox" + str(CurrentGreaterThanEqualToCounter)), node3, node3.nodeName, axis)
                returnVal = ("ifGreaterEqualBox" + str(CurrentGreaterThanEqualToCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kGreaterThan":
                GreaterThanCounter = GreaterThanCounter + 1
                CurrentGreaterThanCounter = GreaterThanCounter
                globalArray.append("ifGreaterBox" + str(GreaterThanCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Greater (a > b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("ifGreaterBox" + str(CurrentGreaterThanCounter)), node3, node3.nodeName, axis)   
                returnVal = ("ifGreaterBox" + str(CurrentGreaterThanCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kLessThan":
                LessThanCounter = LessThanCounter + 1
                CurrentLessThanCounter = LessThanCounter
                globalArray.append("isLessBox" + str(LessThanCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Less (a < b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("isLessBox" + str(CurrentLessThanCounter)), node3, node3.nodeName, axis)    
                returnVal = ("isLessBox" + str(CurrentLessThanCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kLessThanEqualTo":
                LessThanEqualToCounter = LessThanEqualToCounter + 1
                CurrentLessThanEqualToCounter = LessThanEqualToCounter
                globalArray.append("isLessEqualBox" + str(LessThanEqualToCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Less or Equal (a <= b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("isLessEqualBox" + str(CurrentLessThanEqualToCounter)), node3, node3.nodeName, axis)   
                returnVal = ("isLessEqualBox" + str(CurrentLessThanEqualToCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kMultiply":
                MultiplyCounter = MultiplyCounter + 1
                CurrentMultCounter = MultiplyCounter
                globalArray.append("multiplyBox" + str(MultiplyCounter) + " = relConstraint.CreateFunctionBox('Number', 'Multiply (a x b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("multiplyBox" + str(CurrentMultCounter)), node3, node3.nodeName, axis)
                returnVal = ("multiplyBox" + str(CurrentMultCounter))
                return str(returnVal)
                                
            elif node2.firstChild.nodeValue == "kDivide":
                DivideCounter = DivideCounter + 1
                CurrentDivideCounter = DivideCounter
                globalArray.append("divideBox" + str(DivideCounter) + " = relConstraint.CreateFunctionBox('Number', 'Divide (a/b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("divideBox" + str(CurrentDivideCounter)), node3, node3.nodeName, axis)     
                returnVal = ("divideBox" + str(CurrentDivideCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kEqualTo":
                EqualToCounter = EqualToCounter + 1
                CurrentEqualToCounter = EqualToCounter
                globalArray.append("equalTo" + str(EqualToCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Identical (a == b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("equalTo" + str(CurrentEqualToCounter)), node3, node3.nodeName, axis)     
                returnVal = ("equalTo" + str(CurrentEqualToCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kNotEqualTo":
                NotEqualToCounter = NotEqualToCounter + 1
                CurrentNotEqualToCounter = NotEqualToCounter
                globalArray.append("notEqualTo" + str(NotEqualToCounter) + " = relConstraint.CreateFunctionBox('Number', 'Is Different (a != b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("notEqualTo" + str(CurrentNotEqualToCounter)), node3, node3.nodeName, axis)     
                returnVal = ("notEqualTo" + str(CurrentNotEqualToCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kAdd":
                AddCounter = AddCounter + 1
                CurrentAddCounter = AddCounter
                globalArray.append("add" + str(AddCounter) + " = relConstraint.CreateFunctionBox('Number', 'Add (a + b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("add" + str(CurrentAddCounter)), node3, node3.nodeName, axis)    
                returnVal = ("add" + str(CurrentAddCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kSubtract":
                SubtractCounter = SubtractCounter + 1
                CurrentSubtractCounter = SubtractCounter
                globalArray.append("subtract" + str(SubtractCounter) + " = relConstraint.CreateFunctionBox('Number', 'Subtract (a - b)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("subtract" + str(CurrentSubtractCounter)), node3, node3.nodeName, axis)    
                returnVal = ("subtract" + str(CurrentSubtractCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kSqrt":
                SqrtCounter = SqrtCounter + 1
                CurrentSqrtCounter = SqrtCounter
                globalArray.append("squareRoot" + str(SqrtCounter) + " = relConstraint.CreateFunctionBox('Number', 'sqrt (a)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("squareRoot" + str(CurrentSqrtCounter)), node3, node3.nodeName, axis)   
                returnVal = ("squareRoot" + str(CurrentSqrtCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kAbs":
                AbsCounter = AbsCounter + 1
                CurrentAbsCounter = AbsCounter
                globalArray.append("absolute" + str(AbsCounter) + " = relConstraint.CreateFunctionBox('Number', 'Absolute (|a|)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("absolute" + str(CurrentAbsCounter)), node3, node3.nodeName, axis)  
                returnVal = ("absolute" + str(CurrentAbsCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kCos":
                CosCounter = CosCounter + 1
                CurrentCosCounter = CosCounter
                globalArray.append("cosine" + str(CosCounter) + " = relConstraint.CreateFunctionBox('Number', 'Cosine cos(a)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("cosine" + str(CurrentCosCounter)), node3, node3.nodeName, axis)  
                returnVal = ("cosine" + str(CurrentCosCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kSin":
                SinCounter = SinCounter + 1
                CurrentSinCounter = SinCounter
                globalArray.append("sine" + str(SinCounter) + " = relConstraint.CreateFunctionBox('Number', 'Sine sin(a)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("sine" + str(CurrentSinCounter)), node3, node3.nodeName, axis)  
                returnVal = ("sine" + str(CurrentSinCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kDToR":
                DToRCounter = DToRCounter + 1
                CurrentDToRCounter = DToRCounter
                globalArray.append("dtoRBox" + str(DToRCounter) + " = relConstraint.CreateFunctionBox('Converters', 'Deg To Rad')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("dtoRBox" + str(CurrentDToRCounter)), node3, node3.nodeName, axis)  
                returnVal = ("dtoRBox" + str(CurrentDToRCounter))
                return str(returnVal)
                
            elif node2.firstChild.nodeValue == "kRToD":
                RToDCounter = RToDCounter + 1
                CurrentRToDCounter = RToDCounter
                globalArray.append("rtoDBox" + str(RToDCounter) + " = relConstraint.CreateFunctionBox('Converters', 'Rad To Deg')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("rtoDBox" + str(CurrentRToDCounter)), node3, node3.nodeName, axis)  
                returnVal = ("rtoDBox" + str(CurrentRToDCounter))
                return str(returnVal)
                    
            ##---------------------------------------------------------------------------------------------------------------------------------
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ##                                                  MACROS IMPLEMENTED HERE
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ##---------------------------------------------------------------------------------------------------------------------------------
            elif node2.firstChild.nodeValue == "kMin":
                MinCounter = MinCounter + 1
                CurrentMinCounter = MinCounter
                globalArray.append("minBox" + str(MinCounter) + " = relConstraint.CreateFunctionBox('My Macros', 'min(x,y)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("minBox" + str(CurrentMinCounter)), node3, node3.nodeName, axis)  
                returnVal = ("minBox" + str(CurrentMinCounter))
                return str(returnVal)
                    
            elif node2.firstChild.nodeValue == "kMax":
                MaxCounter = MaxCounter + 1
                CurrentMaxCounter = MaxCounter
                globalArray.append("maxBox" + str(MaxCounter) + " = relConstraint.CreateFunctionBox('My Macros', 'max(x,y)')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        ProcessEntry(relConstraint, ("maxBox" + str(CurrentMaxCounter)), node3, node3.nodeName, axis)  
                returnVal = ("maxBox" + str(CurrentMaxCounter))
                return str(returnVal)
            ##---------------------------------------------------------------------------------------------------------------------------------
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ##                                                  MACROS IMPLEMENTED HERE
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ##---------------------------------------------------------------------------------------------------------------------------------
                        
            elif node2.firstChild.nodeValue == "kIfThenElse":
                IfThenElseCounter = IfThenElseCounter + 1
                CurrentIfThenElseCounter = IfThenElseCounter
                globalArray.append("ifThenElseBox" + str(IfThenElseCounter) + " = relConstraint.CreateFunctionBox('Number', 'IF Cond Then A Else B')\n")
                globalArray.append("\n")
                for node3 in node.childNodes:
                    if node3.nodeType != node.TEXT_NODE and node3.nodeName != "opType":
                        control = ProcessControlBox(relConstraint, node3, ("ifThenElseBox" + str(IfThenElseCounter)), axis)
                        if control != None:
                            globalArray.append("lAnimationNodesOUT = " + control + ".AnimationNodeOutGet().Nodes\n")
                            globalArray.append("Result = None\n")
                            
                            globalArray.append("for outNode in lAnimationNodesOUT:\n")
                            globalArray.append("    if outNode.Label == 'Result' or outNode.Name == 'Result':\n")
                            globalArray.append("        Result = outNode.Name\n")
                            globalArray.append("\n")
                            if node3.nodeName == "pLeft":
                                globalArray.append("ConnectBox(" + control + ", Result, ifThenElseBox" + str(CurrentIfThenElseCounter) + ", 'Cond')\n")
                                ######################################################################################
                                LayoutArray.append("ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos = relConstraint.GetBoxPosition(ifThenElseBox" + str(CurrentIfThenElseCounter) + ")\nrelConstraint.SetBoxPosition(" + control + ", (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[1] - 400), (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[2]))\n")
                                #######################################################################################
                                globalArray.append("\n")
                            elif node3.nodeName == "pMiddle":
                                globalArray.append("ConnectBox(" + control + ", Result, ifThenElseBox" + str(CurrentIfThenElseCounter) + ", 'a')\n")
                                ######################################################################################
                                LayoutArray.append("ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos = relConstraint.GetBoxPosition(ifThenElseBox" + str(CurrentIfThenElseCounter) + ")\nrelConstraint.SetBoxPosition(" + control + ", (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[1] - 400), (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[2] - 200))\n")
                                #######################################################################################
                                globalArray.append("\n")
                            elif node3.nodeName == "pRight":
                                globalArray.append("ConnectBox(" + control + ", Result, ifThenElseBox" + str(CurrentIfThenElseCounter) + ", 'b')\n")
                                ######################################################################################
                                LayoutArray.append("ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos = relConstraint.GetBoxPosition(ifThenElseBox" + str(CurrentIfThenElseCounter) + ")\nrelConstraint.SetBoxPosition(" + control + ", (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[1] - 400), (ifThenElseBox" + str(CurrentIfThenElseCounter) + "Pos[2] - 400))\n")
                                #######################################################################################
                                globalArray.append("\n")
                returnVal = ("ifThenElseBox" + str(CurrentIfThenElseCounter))
                return str(returnVal)
                
            else:
                print "Error: Operation unsupported: " + node2.firstChild.nodeValue
                
    if foundOpType == 0:         
        ProcessEntry(relConstraint, parentBox, node, node.nodeName, axis)
                                    
    return None
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------  
def sortAndUniq(input):
    output = []
    for x in input:
        if x not in output:
            output.append(x)
    output.sort()
    return output
##------------------------------------------------------------------------------------------------------------------
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *     
##
##                                              TODO: DEF DESCRIPTION
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *   
##------------------------------------------------------------------------------------------------------------------          
def Run(control, event):
    global controlNameArray
    global b1
    
    b1.Enabled = False
    
    lApp = FBApplication()
    lFileFullName = lApp.FBXFileName
    lFilePath = os.path.dirname(lFileFullName)
    
    fileBrowser = FBFilePopup()
    fileBrowser.Path = r"{0}\assets\anim\expressions".format( RS.Config.Project.Path.Root )
    fileBrowser.Filter = '*.xml'
    fileBrowser.Caption = 'Open Expression XML File'

    if fileBrowser.Execute():
        LoadXML(fileBrowser.Path + "\\" + fileBrowser.FileName)
    
    controlNameArray = sortAndUniq(controlNameArray)
    
##    ControlName = (lFilePath + "\\" + SceneName + "_RelationCode\\JointList.txt")
##    ControlFile = open(ControlName, 'w')    
##    for control in controlNameArray:
##            
##                ControlFile.writelines(control)
                
##    ControlFile.close()
#############################################################################################################
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  
##
## * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
#############################################################################################################    
def PopulateLayout(mainLyt):
    x = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
    y = FBAddRegionParam(5,FBAttachType.kFBAttachTop,"")
    w = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
    h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
    mainLyt.AddRegion("main","main", x, y, w, h)
    lyt = FBHBoxLayout()
    mainLyt.SetControl("main",lyt)
    
    global b
    
    b = FBButton()
    b.Caption = "Toggle Nulls"
    b.Style = FBButtonStyle.kFB2States
    b.Look = FBButtonLook.kFBLookColorChange
    b.Justify = FBTextJustify.kFBTextJustifyCenter
    b.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.8, 0.8, 0.8))
    b.SetStateColor(FBButtonState.kFBButtonState1,FBColor(0.0, 1.0, 0.5))
    mainLyt.AddRegion("But1","But1", x, y, w, h)
    mainLyt.SetControl("But1",b)
    b.OnClick.Add(DisplayAllNulls)
    
    global b1
    
    b1 = FBButton()
    b1.Caption = "Parse XML"
    b1.Style = FBButtonStyle.kFB2States
    b1.Look = FBButtonLook.kFBLookColorChange
    b1.Justify = FBTextJustify.kFBTextJustifyCenter
    b1.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.8, 0.8, 0.8))
    b1.SetStateColor(FBButtonState.kFBButtonState1,FBColor(0.0, 1.0, 0.5))
    y2 = FBAddRegionParam(40,FBAttachType.kFBAttachTop,"")
    mainLyt.AddRegion("But2","But2", x, y2, w, h)
    mainLyt.SetControl("But2",b1)
    b1.OnClick.Add(Run)
    
    global b2
    
    b2 = FBButton()
    b2.Caption = "Execute Scripts"
    b2.Style = FBButtonStyle.kFB2States
    b2.Look = FBButtonLook.kFBLookColorChange
    b2.Justify = FBTextJustify.kFBTextJustifyCenter
    b2.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.8, 0.8, 0.8))
    b2.SetStateColor(FBButtonState.kFBButtonState1,FBColor(0.0, 1.0, 0.5))
    y3 = FBAddRegionParam(75,FBAttachType.kFBAttachTop,"")
    mainLyt.AddRegion("But3","But3", x, y3, w, h)
    mainLyt.SetControl("But3",b2)
    b2.OnClick.Add(RunAllScripts)

    

def CreateTool():
    # Tool creation will serve as the hub for all other controls
    t = FBCreateUniqueTool("Parse Expression XML")
    t.StartSizeX = 300
    t.StartSizeY = 140
    PopulateLayout(t)
    ShowTool(t)
    
def Run():
    CreateTool()