from pyfbsdk import *

import RS.Config
import RS.Utils.Scene

import xml.etree.ElementTree as ET


def rs_ParseExpressionXMLForCustomAttributeValues(fatXmlFile):
    
##    ##NEED TO DO A FILE PICKER FOR THE XML FILE HERE
##    lFp = FBFilePopup()
##    lFp.Caption = "Pick your expression xml file"
##    lFp.Style = FBFilePopupStyle.kFBFilePopupOpen
##
##    lFp.Filter = "*" # BUG: If we do not set the filter, we will have an exception.
##    
##    # Set the default path.
##    lFp.Path = r"C:\\"
##    
##    # Get the GUI to show.
##    lRes = lFp.Execute()
##    
##    exprXmlFile = None
##    
##    # If we select files, show them, otherwise indicate that the selection was canceled.
##    if lRes:
    if fatXmlFile != None:
      #  exprXmlFile = lFp.FileName
            
        tree = ET.parse(fatXmlFile)
        root = tree.getroot()
        
        exprAttrDataArray = []
        
        for child in root:
    
            
            if child.tag == "customAttributeData":
                for childNode in child:
                    
                    objName = str(childNode.tag)
                    
                    for rolloutChildNode in childNode:
                        rolloutName = rolloutChildNode.tag
                        
                        for attrElem in rolloutChildNode:
                            if attrElem.tag != "attributeDefinition":
                                attrName = attrElem.tag
                                
                                for pBlock in attrElem:
                                    if pBlock.tag == "paramBlockDefinition":
                                        #print str(pBlock[5])
                                        for pBlockAttr in pBlock:
                                            if pBlockAttr.tag == "default":
                                                attr = pBlockAttr.attrib
                                                
                                                attrValue = attr['value']
                                                print attrValue
                                                
                                                print "objName: "+objName+" attrName:"+attrName+" attrValue:"+attrValue
                                                
                                                ##ok now we have the dat we need so lets make a dictionary from it
                                                ## then put it in exprAttrDataArray
                                                
                                                attrData = {'objName':objName, 'attrName':attrName, 'attrValue':attrValue}
                                                
                                                exprAttrDataArray.append(attrData)
    
        #now we've got all our data in the exprAttrDataArray (in dictionary form)
        #we can then find each object in the dict and see if we can find that object in the scene.
        #if we can then we can set its property.     
        
        for item in exprAttrDataArray:
            objName = item['objName']
            attrName = item['attrName']
            attrValue = item['attrValue']
            
            print "objName "+objName
            
           # obj = FBFindObjectsByName(objName)
            
            obj = RS.Utils.Scene.FindModelByName( objName)
            
            if obj != None:
                attrProperty = obj.PropertyList.Find(attrName)
                print "Property: "+str(attrProperty)
                if attrProperty != None:
                    print "Setting to "+str(attrValue)
                    obj.PropertyList.Find(attrName).Data = float(attrValue)
      
    else:
      #FBMessageBox( "FBFilePopup example", "Selection canceled", "OK", None, None )    
      print "No fatXmlFile found so skipping attribute parameter setting."

#rs_ParseExpressionXMLForCustomAttributeValues()