import os
import time

from xml.dom import minidom
from pyfbsdk import *
from subprocess import Popen, PIPE

import RS.Globals as glo
import RS.Config
import RS.Utils.Collections
import RS.Core.AssetSetup.rs_TrapsShoulderScaleFixup as shoulders

gFileArray = []
gDrivenArray = []
gDriverArray = []
gExpressionArray = []
gInputVector3d = []
gOutputVector3d = []
gOutputDoubleArray = []
gInputDoubleArray = []
gZeroValueArray = []
gIter = 0

def rs_ParseExpressionXML(pXmlFileName):
    
    global gFileArray
    gFileArray = []
    
    global gDrivenArray
    gDrivenArray = []
    
    global gDriverArray
    gDriverArray = []
    
    global gExpressionArray
    gExpressionArray = []
    

    glo.gApp.FileAppend( RS.Config.Tool.Path.TechArt + "\\script\\standalone\\Antlr4.0\\MaxFloatExpression_2014\\MaxExpressionFunctionList.fbx" )
    
    lXmlFile = minidom.parse(pXmlFileName)
    
    #Look for a FFX .xml alongside the one passed in
    lFFXXmlFilePath = pXmlFileName.split('RSNXML')[0] + "FFX_RSNXML.xml"
    
    #If we find one we will in and merge it in so that we get body and FFX in one pass
    if(os.path.exists(lFFXXmlFilePath)):
        lFFXXmlFile = minidom.parse(lFFXXmlFilePath)
        while( len(lFFXXmlFile.firstChild.childNodes) > 0 ):
            lXmlFile.firstChild.appendChild(lFFXXmlFile.firstChild.firstChild)

    for iNode in lXmlFile.childNodes:
        gExpression = ""
        for iNode1 in iNode.childNodes:
            if iNode1.nodeType != iNode1.TEXT_NODE: 
                if iNode1.nodeName == "driven":
                    
                    #probably dealing with a spring controller so skip over
                    if not 'expObj' in iNode1.attributes.keys():
                        continue
                    
                    lModel = iNode1.attributes["expObj"].value
                    
                    lAttribute = iNode1.attributes["expCont"].value
                    
                    lDriven = (lModel + "-" + lAttribute)
                    
                    gDrivenArray.append(lDriven)
                    
                    lExpression = iNode1.attributes["expString"].value
                    
                    for iNode2 in iNode1.childNodes:
                        if iNode2.nodeType != iNode2.TEXT_NODE: 
                            if iNode2.nodeName == "driver":

                                    lDriver = iNode2.attributes["scalarObjects"].value + "#" + iNode2.attributes["scalarConts"].value
                                    
                                    lExpression = lExpression.replace(iNode2.attributes["scalarNames"].value, lDriver, len(lExpression))
                                    
                                    gDriverArray.append(lDriver)
                    
                    gExpressionArray.append(lExpression)
                    
                    gDriverArray = RS.Utils.Collections.RemoveDuplicatesFromList(gDriverArray)   
                    gDriverArray = sorted(gDriverArray) 
                                        
    lTempExpressionArray = []
    lAllConditionals = []
    
    ###########################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Concatinate All Expression For Each Driven Joint
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###########################################################################################################
    
    lTempDrivenArray = RS.Utils.Collections.RemoveDuplicatesFromList(gDrivenArray) 
    
    for i in range(len(lTempDrivenArray)):
        lExpression =  ""
        for j in range(len(gDrivenArray)):
            if lTempDrivenArray[i] == gDrivenArray[j]:
                if lExpression == "":
                    lExpression = gExpressionArray[j]
                else:
                    lExpression = lExpression + " + " + gExpressionArray[j]
        
        lTempExpressionArray.append(lExpression)
        
                    
    for i in range(len(lTempDrivenArray)):
                                    
                    stringArray = ""
                    
                    stringArray = stringArray + "from pyfbsdk import *\n"
                    stringArray = stringArray + "import RS.Globals as glo\n"
                    stringArray = stringArray + "\n"
                    stringArray = stringArray + "def rs_FindAnimationNode(pParent, pName):\n"
                    stringArray = stringArray + "    lResult = None\n"
                    stringArray = stringArray + "    for iNode in pParent.Nodes:\n"
                    stringArray = stringArray + "        if iNode.Name == pName or iNode.Label == pName:\n"
                    stringArray = stringArray + "            lResult = iNode\n"
                    stringArray = stringArray + "            break\n"
                    stringArray = stringArray + "    return lResult   \n"
                    stringArray = stringArray + "\n"
                    stringArray = stringArray + "def rs_ConnectBox(pNodeOut,pNodeOutPos, pNodeIn, pNodeInPos):\n"
                    stringArray = stringArray + "    lNodeOut = rs_FindAnimationNode(pNodeOut.AnimationNodeOutGet(), pNodeOutPos )\n"
                    stringArray = stringArray + "    lNodeIn = rs_FindAnimationNode(pNodeIn.AnimationNodeInGet(), pNodeInPos )\n"
                    stringArray = stringArray + "    if lNodeOut and lNodeIn:\n"
                    stringArray = stringArray + "        FBConnect(lNodeOut, lNodeIn)\n"
                    stringArray = stringArray + "\n"
                    stringArray = stringArray + "lRelConstraint = FBConstraintRelation('" + lTempDrivenArray[i] + "')\n"
                    stringArray = stringArray + "lRelConstraint.Active = True\n"
                    stringArray = stringArray + "\n"
                    
                    stringArray = stringArray + "lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, '', False, True, None)\n"
                    stringArray = stringArray + "lTag.Data = 'rs_Constraints'\n"
                    stringArray = stringArray + "\n"
                    
                    stringArray = stringArray + "lFolder = None\n"
                    stringArray = stringArray + "lConstraints1Folder = None\n"                    
                    stringArray = stringArray + "lFunctionListFolder = None\n"   
                    stringArray = stringArray + "\n"
                    
                    stringArray = stringArray + "for iFolder in glo.gFolders:\n"
                    stringArray = stringArray + "   if iFolder.Name == 'Expressions':\n"
                    stringArray = stringArray + "       lFolder = iFolder\n"
                    stringArray = stringArray + "   if iFolder.Name == 'Constraints 1':\n"
                    stringArray = stringArray + "       lConstraints1Folder = iFolder\n"
                    stringArray = stringArray + "   if iFolder.Name == 'FunctionList':\n"
                    stringArray = stringArray + "       lFunctionListFolder = iFolder\n"                    
                    stringArray = stringArray + "\n"
                    
                    stringArray = stringArray + "if lFolder == None:\n"
                    stringArray = stringArray + "   lPlaceholder = FBConstraintRelation('Remove_Me')\n"
                    stringArray = stringArray + "   lFolder = FBFolder('Expressions', lPlaceholder)\n"   
                    stringArray = stringArray + "   FBSystem().Scene.Evaluate()\n"
                    stringArray = stringArray + "   lPlaceholder.FBDelete()\n"
                    stringArray = stringArray + "   FBSystem().Scene.Evaluate()\n"
                    stringArray = stringArray + "   lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, '', False, True, None)\n"
                    stringArray = stringArray + "   lTag.Data = 'rs_Folders'\n"
                    
                    stringArray = stringArray + "   if lConstraints1Folder != None:\n"
                    stringArray = stringArray + "       lConstraints1Folder.Items.append(lFolder)\n"                                        
                    stringArray = stringArray + "\n"
                    
                    #stringArray = stringArray + "if lFunctionListFolder != None:\n"
                    #stringArray = stringArray + "   lConstraints1Folder.Items.append(lFunctionListFolder)\n"                    
                    #stringArray = stringArray + "\n"
                                        
                    stringArray = stringArray + "lFolder.Items.append(lRelConstraint)\n"
                    stringArray = stringArray + "\n"
                    
                    lWorkingDir = os.getcwd()
                    Expression = 'java Parse "%s"' %lTempExpressionArray[i]
                    os.chdir( RS.Config.Tool.Path.TechArt + "\\script\\standalone\\Antlr4.0\\MaxFloatExpression_2014" )
                    
                    variable = Popen(Expression, bufsize=0, executable=None, stdin=None, stdout=PIPE, stderr=None, preexec_fn=None, close_fds=False, shell=True, cwd=None, env=None, universal_newlines=False, startupinfo=None, creationflags=0)
                    os.chdir(lWorkingDir)
                    array = variable.stdout.read() 
                    array = array.replace("MODEL_NAME", lTempDrivenArray[i].split("-")[0])
                    lAttribute = lTempDrivenArray[i].split("-")[1]
                    
                    if "position" in lAttribute.lower():
                        
                        array = array.replace("TRANSFORM_NAME", "Lcl Translation")
                        
                        if "X" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "X")
                            
                        elif "Y" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "Y")
                            
                        else:
                            
                            array = array.replace("AXIS_NAME", "Z")
                            
                      
                    elif "rotation" in lAttribute.lower():
                        
                        array = array.replace("TRANSFORM_NAME", "Lcl Rotation")
                        
                        if "X" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "X")
                            
                        elif "Y" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "Y")
                            
                        else:
                            
                            array = array.replace("AXIS_NAME", "Z")
                        
                    elif "scale" in lAttribute.lower():
                        
                        array = array.replace("TRANSFORM_NAME", "Lcl Scaling")
                        
                        if "X" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "X")
                            
                        elif "Y" in lAttribute:
                            
                            array = array.replace("AXIS_NAME", "Y")
                            
                        else:
                            
                            array = array.replace("AXIS_NAME", "Z")
                    
                    else:
                    
                        array = array.replace("ATTRIBUTE_NAME", lAttribute)



                    # Since exec will not work, have to write to file
                    f = open('X:\Expression.py', 'w')
                    f.write(stringArray)
                    f.write(array)                    
                    f.close()
                    lApp = FBApplication()
                    lApp.ExecuteScript('X:\Expression.py')
                    
                    del lApp
                    del f
                    
                    #os.remove('X:\Expression.py')
                    

def rs_XMLPopup(pControl, pEvent):
    lWorkingDir = os.getcwd()
    os.chdir( RS.Config.Tool.Path.TechArt + "\\script\\standalone\\Antlr4.0\\MaxFloatExpression_2014" )
    Popen("MaxFloatExpression.bat", bufsize=0, executable=None, stdin=None, stdout=PIPE, stderr=None, preexec_fn=None, close_fds=False, shell=True, cwd=None, env=None, universal_newlines=False, startupinfo=None, creationflags=0)
    time.sleep(10)
    os.chdir(lWorkingDir)
    
    gProjRoot = RS.Config.Project.Path.Root
    
    lFilePopup = FBFilePopup();
    lFilePopup.Filter = '*.xml'
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFilePopup.Path = gProjRoot + "\\art"
    
    gFileName = ""
    
    if lFilePopup.Execute():
        
        # Remove existing expressions
        deleteMe = []
        for folder in glo.gFolders:
            if ( "Expressions" in folder.Name ) or ( "FunctionList" in folder.Name ):
                deleteMe.append( folder )
                for item in folder.Components:
                    deleteMe.append( item )
                
        for each in deleteMe:
            each.FBDelete()
            
        gFileName = lFilePopup.FullFilename
        
        rs_ParseExpressionXML(gFileName)
        
        lConstraintsFolder = None
        lFunctionListFolder = None
        for folder in glo.gFolders:
            if ( "Constraints 1" in folder.Name ):
                lConstraintsFolder = folder
            if ( "FunctionList" in folder.Name ):
                lFunctionListFolder = folder
                
        if lFunctionListFolder == None:
            tempConstraint = FBConstraintRelation("Temp")
            lFunctionListFolder = FBFolder("FunctionList",tempConstraint)
            tempConstraint.FBDelete()

        if lConstraintsFolder == None:
            tempConstraint = FBConstraintRelation("Temp")
            lConstraintsFolder = FBFolder("Constraints 1",tempConstraint)
            tempConstraint.FBDelete()

        if ( lConstraintsFolder != None ) and ( lFunctionListFolder != None ):
            lConstraintsFolder.Items.append( lFunctionListFolder )

    shoulders.rs_ShoulderScaleFixup("_R_")
    shoulders.rs_ShoulderScaleFixup("_L_")
##    print "Shoulder fix completed."    
            
#rs_XMLPopup(None, None)