import ctypes

class ExpressionSolver(object):
    '''
    This module allows us to Get and Set properties with the RS Expression Solver Device.
    '''
    def __init__(self):
        '''
        Constructor
        '''
        super(ExpressionSolver, self).__init__()
        self._currentDeviceLongName = None

    def GetScale(self, deviceLongName):
        '''
        Return:
            Double: the Scale set for the device
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the return type
        ctypes.cdll.MBExprSolver.GetScale_Py.restype = ctypes.c_double
        return ctypes.cdll.MBExprSolver.GetScale_Py()

    def GetNamespace(self, deviceLongName):
        '''
        Return:
            String: Namespace
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the return type
        ctypes.cdll.MBExprSolver.GetNamespace_Py.restype = ctypes.c_char_p
        return ctypes.cdll.MBExprSolver.GetNamespace_Py()

    def SetBoneListPath(self, string, deviceLongName):
        '''
        Return:
            String: the File Path set for the skel
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the argument type
        ctypes.cdll.MBExprSolver.SetBonelist_Py.argtypes = [ctypes.c_char_p]
        # carry out the method
        ctypes.cdll.MBExprSolver.SetBonelist_Py(string)

    def GetSkel(self, deviceLongName):
        '''
        Return:
            String: the File Path set for the skel
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the return type
        ctypes.cdll.MBExprSolver.GetSkel_Py.restype = ctypes.c_char_p
        return ctypes.cdll.MBExprSolver.GetSkel_Py()

    def GetExprByIndex(self, index, deviceLongName):
        '''
        Args:
            Int: index of the file position in the list of expression files
        Return:
            String: the File Path for the expression file for the specified index
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the return type
        ctypes.cdll.MBExprSolver.GetExpr_Py.restype = ctypes.c_char_p
        return ctypes.cdll.MBExprSolver.GetExpr_Py(index)

    def SetScale(self, double, deviceLongName):
        '''
        Sets the scale for the Device
        Args:
            Double: value to set the scale to
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the argument type
        ctypes.cdll.MBExprSolver.SetScale_Py.argtypes = [ctypes.c_double]
        # carry out the method
        ctypes.cdll.MBExprSolver.SetScale_Py(double)

    def SetNamespace(self, string, deviceLongName):
        '''
        Adds the Namespace for the Device
        Args:
            String: Namespace (character's name)
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the argument type
        ctypes.cdll.MBExprSolver.SetNamespace_Py.argtypes = [ctypes.c_char_p]
        # carry out the method
        ctypes.cdll.MBExprSolver.SetNamespace_Py(string)

    def SetSkelPath(self, string, deviceLongName):
        '''
        Adds the skeleton file path for the Device
        Args:
            String: File Path to the skel file
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the argument type
        ctypes.cdll.MBExprSolver.SetSkel_Py.argtypes = [ctypes.c_char_p]
        # carry out the method
        ctypes.cdll.MBExprSolver.SetSkel_Py(string)

    def AddExprPath(self, string, deviceLongName):
        '''
        Adds the expression file path for the Device (you can add multiple expression files to the
        device, but only one at a time through this method).
        Args:
            String: File Path to the skel file
        '''
        # set active device
        self.SetActiveDevice(deviceLongName)
        # set the argument type
        ctypes.cdll.MBExprSolver.AddExpr_Py.argtypes = [ctypes.c_char_p]
        # carry out the method
        ctypes.cdll.MBExprSolver.AddExpr_Py(string)

    def SetActiveDevice(self, deviceLongName):
        '''
        Sets the current active device
        Args:
            String: Device LongName
        '''
        # check if cached device name is the same, if so no need to reset it as active
        if self._currentDeviceLongName == deviceLongName:
            return
        # device name is different, add new name to cached variable
        self._currentDeviceLongName = deviceLongName
        # set the argument type
        ctypes.cdll.MBExprSolver.SetActiveDevice_Py.argtypes = [ctypes.c_char_p]
        # carry out the method
        ctypes.cdll.MBExprSolver.SetActiveDevice_Py(deviceLongName)
