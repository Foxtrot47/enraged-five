try:
    """
    This is a split to allow the Core module to be loaded in outside of mobu.

    We should look at refactoring code away from using this Core.Player and Core.System
    """
    import pyfbsdk as mobu

    System = mobu.FBSystem()
    Player = mobu.FBPlayerControl()
except ImportError:
    pass
