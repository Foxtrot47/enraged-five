"""
Shortcut keys manager class
"""
import os
import copy
import traceback
from xml.dom import minidom

from PySide import QtGui, QtCore
import mbutils
import pyfbsdk as mobu

from RS.Tools.UI import Application
from RS.Tools.CameraToolBox.PyCore import singleton

import RS.Core.Mocap.MocapCommands as mocapCommand


class ThisIsNotAShortCutKeyError(Exception):
    """
    Exception for if executed or files errored
    """
    def __init__(self, value, code=None, filePath=None):
        self.value = value
        self.code = code
        self.filePath = filePath

    def __str__(self):
        moreInfo = ""
        if self.code is not None:
            moreInfo = "Code Run: {0}".format(self.code)
        if self.filePath is not None:
            moreInfo = "File Run: {0}".format(self.filePath)
        return "\nThis is not a Shortcut Key Error:\n\n{0}\n\nOriginal Error:\n{1}".format(moreInfo, self.value)


class ShortcutHotkey(object):
    """
    This represents a single shortcut hotkey, such as what to run, and the keys used to run it
    """
    def __init__(self, name, keys, category, description, code=None, filePath=None, runCode=True):
        """
        args:
            name (str): The name of the shortcut
            keys (str): The key combo to use to execute the shortcut
            category (str): The category the shortcut is part of
            description (str): The shortcut description

        kwargs:
            code (str): The code to run when the shortcut is executed
            filePath (str): The file path of a python file to run when the shortcut is executed
            runCode (bool): If the shortcut should run code or file path
        """
        self._name = name
        self._code = code
        self._category = category
        self._description = description
        self._codeFilePath = filePath
        self._runCode = runCode
        self._keys = keys
        self._editable = True

    def editable(self):
        """
        Get if the shortcut is editable

        returns:
            bool
        """
        return self._editable

    def name(self):
        """
        Gets the name of the shortcut

        return:
            string name for the shortcut
        """
        return self._name

    def setName(self, newName):
        """
        Sets the name of the shortcut

        args:
            newName (str): The string name of the shortcut
        """
        self._name = newName

    def execute(self):
        """
        Logic to trigger the shortcut
        """
        if self._runCode is True:
            self._executeCode()
        else:
            self._executeFile()

    def keys(self):
        """
        get the keys used to trigger the hotkey

        returns:
            string keys combo
        """
        return self._keys

    def setKeys(self, newKeys):
        """
        set the keys used to trigger the hotkey

        Note:
            This does not update the Qt events, this is for storage

        args:
            newKeys (str): The key combo to use to trigger the hotkey
        """
        self._keys = newKeys

    def category(self):
        """
        Get the category for the shortcut

        return:
            string category name
        """
        return self._category

    def setCategory(self, newCat):
        """
        Set the category for the shortcut

        args:
            newCat (str): The new category Name
        """
        self._category = newCat

    def description(self):
        """
        Get the description for the shortcut

        return:
            string description
        """
        return self._description

    def setDescription(self, newDescription):
        """
        Set the description for the shortcut

        args:
            newDescription (str): The new description
        """
        self._description = newDescription

    def runCodeWhenExecuted(self):
        """
        Returns if the code is executed or if a file is run when the shortcut is run

        return:
            bool
        """
        return self._runCode

    def setRunCodeWhenExecuted(self, newValue):
        """
        Sets if the code is executed or if a file is run when the shortcut is run

        args:
            newValue (bool): If the code is run, or if a filepath is run
        """
        self._runCode = newValue

    def codeToRun(self):
        """
        Gets the code to run

        returns:
            string code to run
        """
        return self._code

    def setCodeToRun(self, newCode):
        """
        Gets the code to run

        args:
            newCode (str): The code to run
        """
        self._code = newCode

    def fileToRun(self):
        """
        Gets the file to run

        returns:
            string path to the file to run
        """
        return self._codeFilePath

    def setFileToRun(self, newPath):
        """
        Gets the file to run

        args:
            newPath (str): path to the file to run
        """
        self._codeFilePath = newPath

    def _executeCode(self):
        """
        Internal Method

        Run and execute the given code text.

        ANY ERRORS FROM THIS FUNCTION ARE LIKELY DUE TO ERRORS OR PROBLEMS IN THE EXCUTED CODE FILE
        AND NOT FROM THIS FUNCTION.
        """
        try:
            exec(str(self._code))
        except Exception, ex:
            raise ThisIsNotAShortCutKeyError(traceback.format_exc(), code=str(self._code))

    def _executeFile(self):
        """
        Internal Method

        Run and execute the given code file.

        ANY ERRORS FROM THIS FUNCTION ARE LIKELY DUE TO ERRORS OR PROBLEMS IN THE EXCUTED CODE FILE
        AND NOT FROM THIS FUNCTION.
        """
        try:
            # Python "Querk" exec doesnt work with __buildtin__ so globals and import * have issues. workaround
            glob = copy.copy(globals())
            execfile(self._codeFilePath, glob, glob)
        except Exception, ex:
            raise ThisIsNotAShortCutKeyError(traceback.format_exc(), filePath=self._codeFilePath)

    def __getstate__(self):
        """
        Pickle the class
        """
        return self._name, self._code, self._codeFilePath, self._runCode, self._keys, self._category, self._description

    def __setstate__(self, state):
        """
        unPickle the class
        """
        self._name, self._code, self._codeFilePath, self._runCode, self._keys, self._category, self._description = state
        self._editable = True


class MotionBuilderBuiltInShortcutHotkey(ShortcutHotkey):
    """
    This represents a single motion builder build in shortcut hotkey, such as what to run, and the keys used to run it
    """
    def __init__(self, name, keys):
        """
        args:
            name (str): The name of the shortcut
            keys (str): The key combo to use to execute the shortcut
        """
        self._wholeName = name
        nameSplits = name.split(".")

        category = nameSplits[1]
        name =  " ".join(nameSplits[2:])

        description = "Build in Motion Builder Shortcut"

        super(MotionBuilderBuiltInShortcutHotkey, self).__init__(name, keys, category, description)
        self._editable = False

    def __getstate__(self):
        """
        Pickle the class
        """
        return ""

    def __setstate__(self, state):
        """
        unPickle the class
        """
        return


class ShortcutKeysManager(object):
    """
    Singleton class to handle and manage all the custom shortcut keys
    """
    __metaclass__ = singleton.Singleton

    def __init__(self):
        """
        Constructor
        """
        super(ShortcutKeysManager, self).__init__()
        self._shortcuts = {}
        self._buildInShortcuts = {}
        self._settings = QtCore.QSettings("RockstarSettings", "CustomShortcutKeys")
        self._mainApp = Application.GetMainWindow()
        self._loadBuiltInMobuKeys()

    def loadKeys(self):
        """
        Load and activate all the hotkeys
        """
        self.removeAllShortcuts()

        for item in self._settings.value("shortcuts", []):
            shortcut = QtGui.QShortcut(QtGui.QKeySequence(item.keys()), self._mainApp)
            shortcut.activated.connect(item.execute)
            self._shortcuts[item] = shortcut

    def _loadBuiltInMobuKeys(self):
        """
        Loads all the mobu hotkeys
        """
        self._buildInShortcuts = {}
        # Load in Mobu defaults
        appConfig = mobu.FBConfigFile("@Application.txt")
        keyboard = appConfig.Get("Keyboard", "Default")
        if keyboard in (None, "None"):
            appConfig.Set("Keyboard", "Default", "MotionBuilder")
            keyboard = "MotionBuilder"
        shortcutPath = os.path.join(mbutils.GetConfigPath(), "Keyboard", "{0}.txt".format(keyboard))
        config = mbutils.OpenConfigFile(shortcutPath)

        items = config.items("Actions")
        items.sort(key=lambda pair : pair[0])

        for action, shortcutKeys in items:
            if action == "":
                continue
            shortcut = MotionBuilderBuiltInShortcutHotkey(action, self._paseMobuShortcut(shortcutKeys))
            self._buildInShortcuts[shortcut] = None

    def _paseMobuShortcut(self, inputKeys):
        if inputKeys == "":
            return ""
        inputKeys = inputKeys.split("|")[0]
        strippedKeys = inputKeys[1:-1]
        mod, key = strippedKeys.split(":")
        key, _ = key.split("*")
        mod = mod.lower()
        if mod == "shft":
            return "Shift+{0}".format(key)
        elif mod == "alsh":
            return "Alt+{0}".format(key)
        elif mod == "alt":
            return "Alt+{0}".format(key)
        elif mod == "ctsh":
            return "Command+{0}".format(key)
        elif mod == "ctrl":
            return "Ctrl+{0}".format(key)
        return key

    def checkForKeyConflicts(self, shortcut, newKeySequence):
        """
        Check if there are any key conflicts between the new key sequence and any other key, excluding
        the given shortcut

        args:
            shortcut (ShortcutHotkey): The key where the new shortcut is coming from
            newKeySequence (str): The new key sequence to check for conflicts

        return:
            ShortcutHotkey item if a conflict if found, or None, if no conflict
        """
        if newKeySequence == "":
            return None

        newKey = QtGui.QKeySequence(newKeySequence)
        for item in self._shortcuts.keys() + self._buildInShortcuts.keys():
            if item == shortcut:
                continue

            if QtGui.QKeySequence(item.keys()) == newKey:
                return item
        return None

    def saveKeys(self):
        """
        Save all the hotkeys
        """
        self._settings.setValue("shortcuts", self._shortcuts.keys())

    def removeAllShortcuts(self):
        """
        Remove all the custom shortcuts added
        """
        for item, shortcut in self._shortcuts.iteritems():
            shortcut.setKey(QtGui.QKeySequence(""))
            try:
                shortcut.activated.disconnect(item.execute)
            except RuntimeError:
                pass
            shortcut.deleteLater()
            del shortcut
        self._shortcuts = {}

    def removeShortcut(self, item):
        """
        Remove a given shortcut

        args:
            item (ShortcutHotkey): The item to delete
        """
        shortcut = self._shortcuts.get(item)
        if shortcut is None:
            return

        shortcut.setKey(QtGui.QKeySequence(""))
        try:
            shortcut.activated.disconnect(item.execute)
        except RuntimeError:
                pass
        shortcut.deleteLater()
        del shortcut

        self._shortcuts.pop(item)
        self.saveKeys()

    def getAllShortcuts(self):
        """
        Get a list of all the shortcut objects that are in use

        return:
            list of <ShortcutHotkey> objects
        """
        return self._shortcuts.keys() + self._buildInShortcuts.keys()

    def updateShortcut(self, item):
        """
        Update the key combo used to trigger a shortcut

        args:
            shortcut (ShortcutHotkey): The shortcut hotkey to update the key combo
        """
        shortcut = self._shortcuts.get(item)
        if shortcut is not None:
            shortcut.setKey(QtGui.QKeySequence(""))
            try:
                shortcut.activated.disconnect(item.execute)
            except RuntimeError:
                pass
            shortcut.deleteLater()
            del shortcut

        shortcut = QtGui.QShortcut(QtGui.QKeySequence(item.keys()), self._mainApp)
        shortcut.activated.connect(item.execute)
        self._shortcuts[item] = shortcut
        self.saveKeys()

    def getAllCategories(self):
        """
        Get a list of all the shortcut categories

        return:
            list of stings
        """
        return sorted(list(set([item.category() for item in self._shortcuts.keys() + self._buildInShortcuts.keys()])))

    def getShortcutsInCategory(self, category):
        """
        Get a list of all the shortcuts in the given category
        
        return:
            list of ShortcutHotkey objects in the category
        """
        return sorted([item for item in self._shortcuts.keys() + self._buildInShortcuts.keys() if item.category() == category])

    def getShortcutByCommandAndCaterogyName(self, category, command):
        """
        Get a shortcut by its caterogy and command name
        
        args:
            category (str): The category of the shortcut
            command (str): The name of the shortcut command
        
        return:
            single ShortcutHotkey objects which fits the command and category, if one is found
        """
        shortcuts = self.getShortcutsInCategory(category)
        for shortcut in shortcuts:
            if shortcut.name() == command:
                return shortcut
        return None

    def addShortcut(self, item):
        """
        Add a new shortcut given the ShortcutHotkey item

        args:
            item (ShortcutHotkey): The shortcut to add
        """
        newShortcut = QtGui.QShortcut(QtGui.QKeySequence(item.keys()), self._mainApp)
        newShortcut.activated.connect(item.execute)
        self._shortcuts[item] = newShortcut
        self.saveKeys()

    def addNewShortcut(self, name, keys, code=None, filePath=None, runCode=True, category='', description=''):
        """
        Add a new shotcut to the system

        args:
            name (str): The name of the shortcut
            keys (str): The key combo to use to execute the shortcut

        kwargs:
            code (str): The code to run when the shortcut is executed
            filePath (str): The file path of a python file to run when the shortcut is executed
            runCode (bool): If the shortcut should run code or file path
        """
        item = ShortcutHotkey(name, keys, category, description, code, filePath, runCode)
        newShortcut = QtGui.QShortcut(QtGui.QKeySequence(item.keys()), self._mainApp)
        newShortcut.activated.connect(item.execute)
        self._shortcuts[item] = newShortcut
        self.saveKeys()

    def exportShortcutKeys(self, filePath, keysToExport=None):
        """
        Export Shortcut keys to a file, either all the keys or just the ones specified
        
        args:
            filePath (str): The file path to export the keys to
            
        kwargs:
            keysToExport (list of ShortcutHotkey): List of ShortcutHotkeys to export
        """
        keysToExport = keysToExport or self._shortcuts.values()

        fileHandle = open(filePath, "wb")
        implementation = minidom.getDOMImplementation()
        baseShortcuts = implementation.createDocument(None, "shortcuts", None)
        baseShortcutsElement = baseShortcuts.documentElement

        for key in keysToExport:
            shortcutNode = baseShortcuts.createElement("shortcut")
            
            tabsToAdd = []
            for propName, propValue in [
                                ("name", key.name()),
                                ("category", key.category()),
                                ("codeToRun", key.codeToRun()),
                                ("description", key.description()),
                                ("fileToRun", key.fileToRun()),
                                ("keys", key.keys()),
                                ("runCodeWhenExecuted", key.runCodeWhenExecuted()),
                                ]: 
                property = baseShortcuts.createElement(propName)
                property.appendChild(baseShortcuts.createTextNode(str(propValue)))
                shortcutNode.appendChild(property)
            baseShortcutsElement.appendChild(shortcutNode)
        
        fileHandle.writelines(baseShortcuts.toprettyxml())
        fileHandle.close()

    def importShortcutKeys(self, filePath):
        """
        Import Shortcut keys from a file
        
        args:
            filePath (str): The file path to import the xml from
        """
        xmldoc = minidom.parse(filePath)

        shortcuts = xmldoc.getElementsByTagName("shortcut")
        newKeys = []
        # Read the XML
        for key in shortcuts:
            keyData = {}
            for node in key.childNodes:
                if  isinstance(node, minidom.Text):
                    continue
                if len(node.childNodes) == 0:
                    continue
                keyData[str(node.nodeName)] = node.childNodes[0].nodeValue
            newKeys.append(keyData)
        
        # Add the keys
        for key in newKeys:
            name = key.get("name")
            catagory = key.get("category")
            keys = key.get("keys")
            description = key.get("description")
            codeToRun = key.get("codeToRun")
            fileToRun = key.get("fileToRun")
            runCodeWhenExecuted = key.get("runCodeWhenExecuted")
            
            cmd = self.getShortcutByCommandAndCaterogyName(catagory, name)
            if cmd is None:
                # New Key, add it
                self.addNewShortcut(
                                    name, 
                                    keys, 
                                    codeToRun, 
                                    fileToRun, 
                                    runCodeWhenExecuted, 
                                    catagory, 
                                    description,
                                )
            else:
                # Key exist, update it
                cmd.setKeys(keys)
                cmd.setDescription(description)
                cmd.setRunCodeWhenExecuted(runCodeWhenExecuted)
                cmd.setCodeToRun(codeToRun)
                cmd.setFileToRun(fileToRun)
                self.updateShortcut(cmd)
            
        
