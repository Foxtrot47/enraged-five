"""
Description:
    Object class definitions for storing camera data in a object tree hierarchy.
    Also a module for comparing two instances of the same object together.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""


def CreateCompareDict(compareObj):
    """
    Function for comparing object attributes.
    Also has a hard coded ignoreAttributes list for values we want to ignore when comparing.
    Arguments:
        compareObj: A camKey object instance to create a dictionary of it's attributes.
    Returns:
        A dictionary of the object's attribute names and values.
    """
    
    compareDict = {}
    ignoredAttributes = ['FbxPath', 'UserEmail', 'CreationTime']  # attributes we ignore when comparing data
    for attributeName in compareObj.Order:
        if attributeName in ignoredAttributes:
            continue
        attributeValue = getattr(compareObj, attributeName)
        if type(attributeValue) == float:
            compareDict[attributeName] = repr(attributeValue)
        elif type(attributeValue) != list:
            compareDict[attributeName] = str(attributeValue)
        else:
            compareDict[attributeName] = [CreateCompareDict(childObj) for childObj in attributeValue]
    return compareDict


class CamInfo(object):
    """
    Master object that stores basic info, SwitcherKeyList, and CamList.
    """

    __order = ['FbxPath', 'UserEmail', 'CreationTime', 'ShakeWeight', 'SwitcherKeyList', 'CamList']

    def __init__(self):
        self.FbxPath = None
        self.UserEmail = None
        self.CreationTime = None
        self.ShakeWeight = None

        self.SwitcherKeyList = []
        self.CamList = []

    def __eq__(self, other):
        return CreateCompareDict(self) == CreateCompareDict(other)

    @property
    def Order(self):
        return self.__order


class ParentItem(object):
    """
    Object for storing a key's parent object name, type, and children info.
    """

    __order = ['Type', 'Name', 'ChildList']

    def __init__(self):
        self.Type = None
        self.Name = None
        self.ChildList = []

    def __eq__(self, other):
        return CreateCompareDict(self) == CreateCompareDict(other)

    @property
    def Order(self):
        return self.__order


class KeyItem(object):
    """
    Object for storing all MB key data.
    """

    __order = ['Time', 'Value', 'TangentFlag',
                'LeftDerivative', 'LeftTangentWeight', 'RightDerivative', 'RightTangentWeight']

    def __init__(self):
        self.Time = None
        self.Value = None
        self.TangentFlag = None
        self.LeftDerivative = None
        self.LeftTangentWeight = None
        self.RightDerivative = None
        self.RightTangentWeight = None

    def __eq__(self, other):
        return CreateCompareDict(self) == CreateCompareDict(other)

    @property
    def Order(self):
        return self.__order