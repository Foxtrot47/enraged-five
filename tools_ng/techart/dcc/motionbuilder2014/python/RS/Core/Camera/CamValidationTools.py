"""
Description:
    Simple functions for validating user and cutscene Fbx for camera tools.
    We use these on a few cam tools now, so I've modularized them into this script.

    This was written for our current (Feb 2015) naming conventions and folder
    structure, so if those change drastically down the road this will need to be updated.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import subprocess
import RS.Config
from xml.dom import minidom


# Global Variables for Config Path, Fbx Types, and Art Roots - current as of 2/25/2015
camTeamConfigPath = RS.Config.Tool.Path.TechArt + "\\etc\\config\\camera\\camTeam_config.xml"
validArtRoots = ['x:/rdr3/art/',
                 'x:/gta5_dlc/mppacks/mpheist/art/',
                 'x:/gta5_dlc/mppacks/mplowrider/art/',
                 'x:/gta5_dlc/sppacks/dlc_agenttrevor/art/',
                 'x:/gta5_dlc/mppacks/mpapartment/art/',
                 'x:/gta5_dlc/mppacks/mpgunrunning/art/']


def CheckUserEmail():
    """
    Checks if the user is a member of the camera team based on config files.
    Returns:
        A bool - true if they are a cam team member, false if not.
    """
    #Check user has the .xml file - if not return 'False'
    if not os.path.isfile( camTeamConfigPath ):
        return False
    
    # Create List of Valid CamTeam Emails
    camTeamEmails = []
    xmlFile = minidom.parse(camTeamConfigPath)
    for node in xmlFile.getElementsByTagName("camTeamEmail"):
        tagValue = (str(node.firstChild.nodeValue)).lower()
        camTeamEmails.append(tagValue)

    # Get User's Email from RS.Config
    userEmail = RS.Config.User.Email.lower()
    # Check for userEmail in camTeamEmails List
    if userEmail in camTeamEmails:
        return True
    else:
        return False


def CheckFbxPath(fbxPath):
    """
    Checks if the fbxPath provided is a valid camera FBX.
    Arguments:
        fbxPath: A string potentially containing an FBX path.
    Returns:
        A string representing our results:
        'VALID' if the FBX is valid, or a string with an error message.
    """

    # Force Forward Slashes and Lowercase
    fbxPath = fbxPath.replace("\\", "/")
    fbxPath = fbxPath.lower()
    
    # Force Local Paths Instead of Perforce
    if fbxPath.startswith("//"):
        fbxPath = "X:/"
    if "/depot/" in fbxPath:
        fbxPath = fbxPath.replace("/depot/", "/")

    # Detect Empty FBXs
    if len(fbxPath) < 10:
        return "The FBX is empty or improperly named."

    # Detect Art Root
    if '/art/' not in fbxPath:
        return "The FBX is not saved within the project's art folder."

    # Detect Valid Project Art Root - not necessary BB 4/26/17
    # fbxArtRoot = fbxPath.split("/art/")[0] + "/art/"
    # if fbxArtRoot not in validArtRoots:
    #     return "The FBX is not saved within a valid project's art folder."

    # Detect FBX Isn't Face FBX
    if "/face/" in fbxPath:
        return "The FBX is saved within a face folder, and shouldn't be used for cutscenes."

    # Detect FBX Isn't in Backup Folder
    if "/backup/" in fbxPath:
        return "The FBX is saved within a backup folder, and shouldn't be used for cutscenes."

    # FBX is Valid
    return "VALID"


def CheckCutFiles(fbxPath):
    """
    Checks an FBX's related cutfiles exist in perforce using at terminal command.
    Arguments:
        fbxPath: A string of the FBX path to check.
    Returns:
        A bool - True if the cutfile exists, False otherwise.
    """
    # Get Paths from fbxPath
    fbxPath = os.path.normpath(fbxPath).lower()
    projectName = fbxPath.split(os.path.sep)[1].upper()
    fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0]
    p4FbxPath = ("/" + fbxPath.replace("\\", "/")[2:]).replace("//gta5/", "//depot/gta5/")
    
    # Get Asset Folder - GTA dlc uses assets_ng
    assetsFolder = "assets"
    if projectName == "GTA5_DLC":
        assetsFolder = "assets_ng"
    
    # Get p4CutxmlPath
    p4CutFolder = "/".join([p4FbxPath.split("/art/")[0], assetsFolder, "cuts", fbxName])
    p4CutxmlPath = p4CutFolder + "/data.cutxml"

    # Change Current Directory to Project Root
    projectRoot = fbxPath.split("{0}{1}{0}".format(os.path.sep, "art"))[0]
    os.chdir(projectRoot)

    # Check for the data.cutxml file on P4 via a Terminal Command
    commandText = 'p4 files -e ' + p4CutFolder + "/..."
    termProcess = subprocess.Popen(commandText, shell=True, stdout=subprocess.PIPE)
    output = termProcess.communicate()[0]

    # Return False - if the data.cutxml isn't found in the output
    if p4CutxmlPath not in str(output).lower():
        return False

    # Or Return True
    else:
        return True