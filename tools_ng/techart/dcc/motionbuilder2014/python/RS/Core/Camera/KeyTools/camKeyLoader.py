"""
Description:
    Lower level functions for loading data from a camInfoObj instance into the currently opened FBX.
    Assumes higher level setup steps have already happened, via camKeyTasks or otherwise.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""


from pyfbsdk import *
import RS.Core.Camera.Lib as CameraLib
import camKeyDef

RDRLENSLIST = [73.7397952917, 51.2820116486, 37.8492888321, 26.9914665616, 18.1805538416, 11.421186275]

def DeleteOldCamsFromFbx():
    """
    Deletes all switcher keys and cameras from the FBX using the CameraLib's RemoveNonSwitcherCameras function.
    """
    # Delete Old Switcher Keys
    FBSystem().CurrentTake.SetCurrentLayer(0)
    mbSwitcherNode = FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    if len(mbSwitcherNode.FCurve.Keys) > 0:
        maxKey = len(mbSwitcherNode.FCurve.Keys) - 1
        mbSwitcherNode.FCurve.KeyDeleteByIndexRange(0, maxKey)
    
    # Delete Old Cams
    _camUpdateObj_ = CameraLib._camUpdate()
    _camUpdateObj_.bSilent = True
    _camUpdateObj_.rs_RemoveNonSwitcherCameras()


def LoadCamsIntoFbx(camInfoObj):
    """
    Creates new cameras in the current FBX from the camList attribute of a camInfoObj instance.
    Arguments:
        camInfoObj: An instance of the camInfo object containing camera data.
    """
    
    # Create CamLib Object - needed for creating R* cameras
    _camFunObj_ = CameraLib._camFunctions()
    
    # Create Layer List
    mbLayerList = []
    mbLayerCount = FBSystem().CurrentTake.GetLayerCount()
    for i in range(mbLayerCount):
        mbLayerName = FBSystem().CurrentTake.GetLayer(i).Name
        mbLayerList.append(mbLayerName)

    # CAMERA LOOP
    for eachCam in camInfoObj.CamList:
        if eachCam.Name == "ExportCamera":
            _camFunObj_._CreateBaseCamera(ExportCamera=True)
        else:
            _camFunObj_._CreateBaseCamera(specificName=eachCam.Name)
        mbCamObj = FBFindModelByLabelName(eachCam.Name)

        # PROPERTY LOOP
        for eachProp in eachCam.ChildList:
            if ("Translation (Lcl)" in eachProp.Name) or ("Rotation (Lcl)" in eachProp.Name):
                parentProp = mbCamObj.PropertyList.Find(eachProp.Name[:-1]).GetAnimationNode()
                axisNumber = 'XYZ'.index(eachProp.Name[-1:])
                mbCamPropNode = parentProp.Nodes[axisNumber]
            else:
                mbCamProp = mbCamObj.PropertyList.Find(eachProp.Name)
                mbCamProp.SetAnimated(True)
                mbCamPropNode = mbCamProp.GetAnimationNode()
            
            # RDR Lens Hack - workaround to force RDR lens values when reloading cams
            if eachProp.Name == "Lens":
                lensIndex = int(float(eachProp.ChildList[0].ChildList[0].Value))
                if lensIndex < 6:
                    fovValue = RDRLENSLIST[lensIndex]
                    mbCamObj.FieldOfView = fovValue
                    mbCamObj.FieldOfView.KeyAt(FBTime(0,0,0,0))
                continue
            
            # LAYER LOOP
            for eachLayer in eachProp.ChildList:
                if eachLayer.Name not in mbLayerList:
                    FBSystem().CurrentTake.CreateNewLayer()
                    newLayer = FBSystem().CurrentTake.GetLayer(len(mbLayerList))
                    newLayer.Name = eachLayer.Name
                    mbLayerList.append(newLayer.Name)

                layerIndex = FBSystem().CurrentTake.GetLayerByName(eachLayer.Name).GetLayerIndex()
                FBSystem().CurrentTake.SetCurrentLayer(layerIndex)
                currentKey = camKeyDef.KeyItem()

                # MAIN KEY LOOP - Sets time, value, and tangent modes
                for eachKey in eachLayer.ChildList:
                    # Update CurrentKey Object
                    for currentPropName in eachKey.Order:
                        currentPropValue = getattr(eachKey, currentPropName)
                        if currentPropValue is not None:
                            setattr(currentKey, currentPropName, currentPropValue)

                    # Create New Key
                    keyTime = FBTime()
                    keyTime.SetSecondDouble(float(currentKey.Time))
                    keyValue = float(currentKey.Value)
                    mbCamPropNode.KeyAdd(keyTime, keyValue)
                    mbKey = mbCamPropNode.FCurve.Keys[-1]

                    # Set Tangent Modes
                    tangentFlag = currentKey.TangentFlag
                    mbKey.Interpolation = mbKey.Interpolation.values[int(tangentFlag[:1])]
                    mbKey.TangentBreak = bool(int(tangentFlag[1:2]))
                    mbKey.TangentMode = mbKey.TangentMode.values[int(tangentFlag[-1:])]
                    mbKey.TangentConstantMode = mbKey.TangentConstantMode.values[int(tangentFlag[3:4])]
                    mbKey.TangentClampMode = mbKey.TangentClampMode.values[int(tangentFlag[2:3])]

                # Reset Current Key
                currentKey = camKeyDef.KeyItem()

                # SECOND KEY LOOP - Sets LeftRight tangent values
                for keyIndex, eachKey in enumerate(eachLayer.ChildList):
                    mbKey = mbCamPropNode.FCurve.Keys[keyIndex]
                    for propName in eachKey.Order[3:]:
                        propValue = getattr(eachKey, propName)
                        if propValue is None:
                            propValue = getattr(currentKey, propName)
                        else:
                            setattr(currentKey, propName, float(propValue))
                        if propValue is not None:
                            # Use Right Props for First Key - as they don't have left tangents
                            if keyIndex == 0 and "Left" in propName:
                                propName = propName.replace("Left", "Right")
                            setattr(mbKey, propName, float(propValue))

    # Set Shake Weight - if one exists
    if camInfoObj.ShakeWeight:
        shakeLayer = FBSystem().CurrentTake.GetLayerByName("ShakeCam")
        if shakeLayer is None:
            FBSystem().CurrentTake.CreateNewLayer()
            shakeLayer = FBSystem().CurrentTake.GetLayer(mbLayerCount)
            shakeLayer.Name = "ShakeCam"
            mbLayerCount += 1
        FBSystem().CurrentTake.SetCurrentLayer(shakeLayer.GetLayerIndex())
        shakeLayer.Weight = float(camInfoObj.ShakeWeight)

    # Select the Base Layer
    FBSystem().CurrentTake.SetCurrentLayer(0)


def LoadSwitcherIntoFbx(camInfoObj):
    """
    Restores the switcher key data from the SwitcherKeyList attribute of a camInfoObj instance.
    Arguments:
        camInfoObj: An instance of the camInfo object containing switcher data.
    """
    
    # Select the Base Layer
    FBSystem().CurrentTake.SetCurrentLayer(0)
    
    # Create mbSwitcherNode and cameraNameList
    mbSwitcherNode = FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    cameraNameList = []
    for camera in FBSystem().Scene.Cameras:
        cameraNameList.append(camera.Name)
    cameraNameList = cameraNameList[7:]
    
    # Switcher Loop
    for eachKey in camInfoObj.SwitcherKeyList:
        keyTime = int(eachKey.Time)
        keyValue = float(cameraNameList.index(eachKey.Value)) + 1.0
        
        # Create Key
        mbSwitcherNode.KeyAdd(FBTime(0, 0, 0, keyTime), keyValue)
        currentKey = mbSwitcherNode.FCurve.Keys[-1]
        
        # Set TangentOptions
        currentKey.Interpolation = currentKey.Interpolation.values[0]
        currentKey.TangentMode = currentKey.TangentMode.values[0]