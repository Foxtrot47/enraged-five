"""
Description:
    Generic Mobu functions used frequently with camera and automation scripts.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu
import unbind

import RS.Config
import RS.Perforce as P4
import RS.Core.Camera.Lib as CameraLib
import RS.Core.Camera.Plot as rsPlot
from RS.Core.Camera import CameraLocker

from RS import Globals
from RS.Tools.UI import Application

camUpdate = CameraLib._camUpdate()
camFunctions = CameraLib._camFunctions()

CAMERA_SELECTION = {"previousCamera": None}

def DeleteSwitcherKeys():
    """
    Deletes all keys from the camera switcher.
    """
    # Unlock Switcher - not using CameraLocker here, it's just 2 lines
    switcherIndexProp = mobu.FBCameraSwitcher().PropertyList.Find("Camera Index")
    switcherIndexProp.SetLocked(False)

    # Remove All Switcher Keys
    mobu.FBSystem().CurrentTake.SetCurrentLayer(0)
    mbSwitcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    if len(mbSwitcherNode.FCurve.Keys) > 0:
        maxKey = len(mbSwitcherNode.FCurve.Keys) - 1
        mbSwitcherNode.FCurve.KeyDeleteByIndexRange(0, maxKey)


def DeleteAllCams():
    """
    Deletes all cameras from an FBX.
    """
    DeleteSwitcherKeys()
    camUpdate.bSilent = True
    camUpdate.rs_RemoveNonSwitcherCameras()


def DeleteInvalidCams():
    """
    Deletes any invalid cams and cam folders from the FBX (like 3Lateral face cams).
    """
    invalidCamNames = ["3lateral", "gizmolaser", "buildcamera"]
    invalidFolderNames = ["buildcameras"]
    cameraList = list(mobu.FBSystem().Scene.Cameras[7:])
    for camera in cameraList:
        for invalidName in invalidCamNames:
            if invalidName in camera.Name.lower():
                camUpdate.DeleteCamera(camera, RemoveCam=True)
                break
    camFolders = [folder for folder in mobu.FBSystem().Scene.Folders if folder.Name.lower() in invalidFolderNames]
    for folder in camFolders:
        folder.FBDelete()


def DeleteCamByName(camName):
    """
    Deletes a camera with the camName provided if it exists.
    Arguments:
        camName: A string of the name of the camera to delete.
    """
    cameraObjs = mobu.FBSystem().Scene.Cameras[7:]
    cameraNames = [str(camera.Name) for camera in cameraObjs]
    if camName in cameraNames:
        camToDelete = cameraObjs[cameraNames.index(camName)]
        camUpdate.DeleteCamera(camToDelete, RemoveCam=True)


def CreateCamByName(camName):
    """
    Creates a new camera with the camName provided. Also detects an
    export camera automatically (which uses a slightly different call).
    Arguments:
        camName: A string of the name for the new camera.
    """
    if camName == "ExportCamera":
        newCam = camFunctions._CreateBaseCamera(ExportCamera=True)
    else:
        newCam = camFunctions._CreateBaseCamera(specificName=camName)

    return newCam


def RemoveStarsFromSwitcherKeys():
    """
    Looks for any switcher keys with stars (that are between frames
    and marked with an asterick) and moves them to exact frames instead.
    """
    switcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    switcherKeys = switcherNode.FCurve.Keys
    for eachKey in switcherKeys:
        if eachKey.Time.GetTimeString().endswith("*"):
            fixedTime = eachKey.Time.GetFrame()
            eachKey.Time = mobu.FBTime(0, 0, 0, fixedTime)


def OverrideSwitcherKeys(camName):
    # Get Cam Index
    camNameList = [cam.Name for cam in mobu.FBSystem().Scene.Cameras[7:]]
    camIndex = float(camNameList.index(camName)) + 1.0

    # Select Base Layer
    mobu.FBSystem().CurrentTake.SetCurrentLayer(0)

    # Replace Existing Keys
    mbSwitcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    for eachKey in mbSwitcherNode.FCurve.Keys:
        eachKey.Value = camIndex


def SetupExportCam(removeFades=True):
    """
    Cleans the switcher and sets up the export camera based on various conditions.

    We create an export camera if one doesn't exist, plot if cams and keys are found,
    key/plot the first cam if some cams are found (but no keys), or just move the
    export cam to the producer cam position if there's no keys or cams at all.
    """
    # Create Switcher Node, Keys, and Camera List
    switcherNode = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index').GetAnimationNode()
    switcherKeys = switcherNode.FCurve.Keys
    cameraList = mobu.FBSystem().Scene.Cameras

    # Create Export Cam - if one doesn't exist already
    expCamFound = [cam for cam in cameraList if cam.Name == "ExportCamera"]
    if len(expCamFound) == 0:
        expCam = CreateCamByName("ExportCamera")
    else:
        expCam = expCamFound[0]

        # Validate Existing Exp Cam - if broken we trash and recreate
        camInvalid = camFunctions.GetRequiresUpdate(expCam)
        if camInvalid:
            DeleteCamByName("ExportCamera")
            expCam = CreateCamByName("ExportCamera")

    # Unlock All Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.setAllCamLocks(False)

    # Remove Screen Fades - needed for captures with mid cutscene fades
    if removeFades:
        DeleteScreenFades()

    # SWITCHER KEYS FOUND - Clean switcher and plot
    if switcherKeys:
        RemoveStarsFromSwitcherKeys()
        GoToFrameNumber(-1)
        rsPlot.PlotSwitcherToExportCamera(mobu.FBSystem().CurrentTake, Silent=True)

    # NO KEYS - wipe expCam keys and move it to the producer perspective
    else:
        # Get Producer and KeyTime
        prodCam = mobu.FBSystem().Scene.Cameras[0]
        keyTime = mobu.FBTime()
        keyTime.SetFrame(0)

        # Set Export Cam to Animated
        expCam.Translation.SetAnimated(True)
        expCam.Rotation.SetAnimated(True)
        
        # Erase Any Old Keys
        for propNode in expCam.AnimationNode.Nodes:
            if propNode.FCurve:
                propNode.FCurve.EditClear()
            for axisNode in propNode.Nodes:
                axisNode.FCurve.EditClear()

        # Key Export Cam At Producer Cam's Position
        expCam.Translation.GetAnimationNode().KeyAdd(keyTime, list(prodCam.Translation))
        expCam.Rotation.GetAnimationNode().KeyAdd(keyTime, list(prodCam.Rotation))

        # Key FOV and Lens - if lens prop found
        expCam.PropertyList.Find("Field of View").Data = 37.0
        lensProp = expCam.PropertyList.Find("Lens")
        if lensProp:
            lensProp.Data = 2

    return expCam


def SetHardRanges(endFrame):
    """
    Sets the FBX's hardranges based on a given endFrame.
    Arguments:
        endFrame: An int of the desired endFrame.
    """
    # Force 30 Fps - Needed during fastCaps if Mobu's default FPS isn't 30.
    mobu.FBPlayerControl().SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)

    # Set Ranges in Empty FBX
    fixedTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, int(endFrame)))
    mobu.FBSystem().CurrentTake.LocalTimeSpan = fixedTimeSpan


def GetThumbnailFrame():
    """
    Uses the switcher keys to determine an ideal thumbnail frame.
    Returns:
        An int of an ideal thumbnail frame.
    """
    # Remove Any Switcher Stars
    RemoveStarsFromSwitcherKeys()

    # Get Switcher Frames
    switcherIndexProp = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index')
    switcherKeys = switcherIndexProp.GetAnimationNode().FCurve.Keys
    switcherFrames = [eachKey.Time.GetFrame() for eachKey in switcherKeys]

    # Empty Switcher - Add a frame at 0 if there's no keys in the switcher
    if len(switcherFrames) == 0:
        switcherFrames.append(0)

    # Create Basic Cut List - from keys within our hard ranges
    endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    cutList = [frame for frame in switcherFrames if 0 <= frame < endFrame]

    # Adjust First Cut - Negative keys need an extra cut at 0.
    if switcherFrames[0] < 0:
        cutList = [0] + cutList
    # Otherwise we just force first key to 0.
    else:
        cutList[0] = 0

    # Pick Frame Based on Number Cuts
    if len(cutList) >= 3:
        thumbnailFrame = cutList[2]
    elif len(cutList) == 2:
        thumbnailFrame = cutList[1]
    elif int(endFrame) > 600:
        thumbnailFrame = 600
    else:
        thumbnailFrame = 0

    return thumbnailFrame


def GoToFrameNumber(frameNumber):
    """
    Moves Mobu's player to the given frameNumber.
    Arguments:
        frameNumber: An int of the frame number to move to.
    """
    newPosition = mobu.FBTime()
    newPosition.SetFrame(int(frameNumber))
    mobu.FBPlayerControl().Goto(newPosition)


def DeleteAllCameraPlanes():
    """
    Deletes all planes from all cameras in an FBX.
    Useful for comparison renders, where broken planes can obscure shots.
    """
    planeNameList = ["ScreenFade", "FarPlane", "NearPlane",
                     "DOF_Strength_FarPlane", "DOF_Strength_NearPlane"]

    camList = mobu.FBSystem().Scene.Cameras[7:]
    for eachCam in camList:
        camPlanes = [eachChild.FBDelete() for eachChild in list(eachCam.Children)
                     if eachChild.Name.split(eachCam.Name)[-1].strip() in planeNameList]


def DeleteScreenFades():
    """
    Deletes all screen fade keys from the export camera in an FBX.  Assumes base layer and unlocked cams.
    Useful for captures after first pass, where mid cutscene fades can obscure shots.
    """
    # Check For Export Camera
    cameraList = mobu.FBSystem().Scene.Cameras
    expCamFound = [cam for cam in cameraList if cam.Name == "ExportCamera"]
    if expCamFound:
        expCam = expCamFound[0]

        # Check For Screen Fade
        screenFadeModels = [eachChild for eachChild in expCam.Children if eachChild.Name == "ExportCamera ScreenFade"]
        if screenFadeModels:
            screenFadeMats = [eachMat for eachMat in screenFadeModels[0].Materials if eachMat.Name == "ExportCamera ScreenFadeMat"]
            if screenFadeMats:
                screenFadeNodes = [eachNode for eachNode in screenFadeMats[0].AnimationNodeInGet().Nodes if eachNode.Name == "TransparentColor"]
                if screenFadeNodes and screenFadeNodes[0].KeyCount > 0:

                    # Screen Fade Found - remove keys and reset value
                    keyTimes = [eachKey.Time for eachKey in screenFadeNodes[0].Nodes[0].FCurve.Keys]
                    for eachTime in keyTimes:
                        screenFadeNodes[0].KeyRemoveAt(eachTime)
                    screenFadeMats[0].TransparentColor = mobu.FBColor(1,1,1)


def GrabCutsceneZip():
    """
    Detects the current FBX's zip path and syncs via perforce. Useful during exports.
    """
    # Verify Cutscene FBX
    fbxPath = mobu.FBApplication().FBXFileName
    if fbxPath and "!!scenes" in fbxPath.lower():

        # Get Default Zip Folder
        fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0]
        zipName = "{}.icd.zip".format(fbxName.lower())
        zipRoot = os.path.join(RS.Config.Project.Path.Export, "anim", "cutscene")

        # Detect Specific Zip Folder
        perforceZips = [record["depotFile"] for record in P4.Run("files", ["-e", "{}....icd.zip".format(zipRoot)])]
        matchedZips = [zipPath for zipPath in perforceZips if zipPath.lower().endswith(zipName)]

        # Sync Zip Folder - force sync matched zips or normal sync all zips
        if matchedZips:
            zipFolder = os.path.split(matchedZips[0])[0]
            P4.Sync("{}/...zip".format(zipFolder), force=True)
        else:
            P4.Sync("{}/...zip".format(zipRoot))


def SelectCurrentSwitcherCamera(source=None, event=None):
    """
    Selects the current switcher camera and sets the focus to the properties of the previously selected camera.
    When the last previously selected camera can't be found, it just assumses the last camera selected was the
    current camera.
    """
    if not Globals.CameraSwitcher.CurrentCamera.Selected:
        currentCamera = Globals.CameraSwitcher.CurrentCamera
        # Python cheat, since FBCameraSwitcher is a python object, it is possible to add custom properties/variables
        # to it. In this code the previous camera property is added to store the previous selected camera
        previousCamera = CAMERA_SELECTION.get("PreviousCamera", currentCamera)
        
        if isinstance(previousCamera, tuple(unbind.unbound_wrapper.values())):
            CAMERA_SELECTION["previousCamera"] = Globals.CameraSwitcher.CurrentCamera
            return

        for fbproperty in ("Translation (Lcl)", "Rotation (Lcl)", "Field Of View", "Roll", "Lens", "FOCUS (cm)"):
            try:
                currentCamera.PropertyList.Find(fbproperty).SetFocus(previousCamera.PropertyList.Find(fbproperty).IsFocused())
            except:
                pass
        [setattr(camera, "Selected", False) for camera in Globals.Cameras if camera.Selected]
        currentCamera.Selected = True
        CAMERA_SELECTION["previousCamera"] = Globals.CameraSwitcher.CurrentCamera


def StepSwitcherKeys(forward=True):
    """
    Moves the player to the next switcher key or previous switcher key.
    Keyword Arguments:
        forward (bool): True for next key, false for previous
    """
    switcher = Globals.CameraSwitcher
    switcherKeys = switcher.PropertyList.Find('Camera Index').GetAnimationNode().FCurve.Keys
    currentFrame = Globals.System.LocalTime.GetFrame()
    for index, key in enumerate(switcherKeys):
        keyFrame = key.Time.GetFrame()
        if forward:
            if keyFrame > currentFrame:
                Globals.Player.Goto(mobu.FBTime(0, 0, 0, keyFrame))
                break
        elif keyFrame < currentFrame:
            nextFrame = switcherKeys[index+1].Time.GetFrame() if index + 1 != len(switcherKeys) else None
            if nextFrame is None or nextFrame >= currentFrame:
                Globals.Player.Goto(mobu.FBTime(0, 0, 0, keyFrame))
                break


def ToggleSwitcherShortcuts(enabled=True):
    """
    Toggles the keyboard shortcuts for stepping through switcher keys.
    Keyword Arguments:
        enabled (bool): True to add shortcut keys, False to remove them
    """
    stepForwardCombo = "alt+Right"
    stepBackCombo = "alt+Left"
    if enabled:
        Application.AddShortCut(stepForwardCombo, lambda *args: StepSwitcherKeys(True))
        Application.AddShortCut(stepBackCombo, lambda *args: StepSwitcherKeys(False))
    else:
        Application.RemoveShortCut(stepForwardCombo)
        Application.RemoveShortCut(stepBackCombo)
