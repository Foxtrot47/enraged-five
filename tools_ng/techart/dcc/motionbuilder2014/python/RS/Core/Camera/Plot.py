"""
Camera Plotter module
"""
import time
import logging
import pyfbsdk as mobu
from PySide import QtGui, QtCore

import RS.Config
import RS.Globals
import RS.Utils.Scene
import RS.Core.Camera.Lib
from RS.Tools.UI import Application
from RS.Core.Animation import Lib

CameraSwitcherKeysInterpolationError = ("Take [{}] keys on camera switcher are broken.\n"
                                        "Likely has smooth interpolation when should be set to stepped interpolation.\n"
                                        "Aborting Plot.")

CameraSwitcherMissingDataError = "Skipping Take [{}] due to missing keys on camera switcher.\n"

MissingExportCamera = "Export Camera missing from Scene\nAborting Plot"

logging.basicConfig(filename=r"X:\render.log", level=logging.DEBUG, format='%(levelname)s %(asctime)s : %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

oldGtaCamProps = {values[0]: values[-2] for values in RS.Core.Camera.Lib.gCamCustomProperties[2:-1]}


def GetAutoClipEndFrame():
    """
    Gets the final endFrame of all autoClips detected in an FBX - for the final clip switcher fix.
    Returns:
        endFrame (Int or None): the last autoClip's endFrame, or None if not found.
    """
    clipFrames = []
    for track in RS.Globals.Story.RootEditFolder.Tracks:
        if not track.Name.endswith("_EST"):
            continue
        estClips = [clip.MarkOut.GetFrame() for clip in track.Clips if clip.Name.startswith("AutoClip")]
        clipFrames.extend(estClips)
    endFrame = max(clipFrames) if clipFrames else None
    return endFrame


def ShowResults(plotCount, takeCount, successMessages, errorMessages, show=False):
    """
    Shows the results of the plot

    Arguments:
        plotCount (int): number of succesfully plotted takes
        takeCount (int): number of takes in the scene
        errorMessages (list[string, etc.]): error messages
        show (boolean): show results

    """

    if not show:
        return

    errorString = "".join(successMessages + ["\n"] + errorMessages)
    plotMessage = "Plotted {} out of {} takes\n\n{}".format(plotCount, takeCount, errorString)

    mainWindow = Application.GetMainWindow()
    position = mainWindow.pos()
    size = mainWindow.size()
    messagePos = QtCore.QPoint(position.x() + (size.width()/2), position.y() + (size.height()/2))

    messageBox = QtGui.QMessageBox()
    messageBox.setWindowTitle("Plot Results")
    messageBox.setText(plotMessage)
    messageBox.setIcon((QtGui.QMessageBox.Information, QtGui.QMessageBox.Critical)[bool(errorMessages)])
    messageBox.move(messagePos)
    messageBox.exec_()


def PlotSwitcherToExportCamera(TakeList, Silent=False):
    """
    Plots cameras in the switcher to the export camera
    Arguments:
        TakeList (list): list of mobu take objects to plot
        Silent (boolean): suppress results dialog if true
    """
    start = time.time()
    if not isinstance(TakeList, (list, tuple, mobu.FBPropertyListTake)):
        TakeList = [TakeList]
    plotCount = 0
    successMessages = []
    errorMessages = []

    # Detect Project Name - via config or fbx path
    projectName = RS.Config.Project.Name
    if "gta5" in RS.Globals.App.FBXFileName.lower().split("art")[0]:
        projectName = "GTA5"

    # Set Extra Properties - only using the old properties for GTA
    extraProps = {"Field Of View": 37.0, "Roll": 0.0, "Lens": 6, "FOCUS (cm)": 150.0}
    if projectName == "GTA5":
        extraProps.update(oldGtaCamProps)

    # Create FrameTime and SourceMatrix
    frameTime = mobu.FBTime()
    sourceMatrix = mobu.FBMatrix()

    # Get Former Mute Sate and Frame Number
    formerStoryMuteState = RS.Globals.Story.Mute
    formerFrameNumber = RS.Globals.System.LocalTime.GetFrame()

    # Force Selection, Story Mute, and Base Layer
    RS.Utils.Scene.DeSelectAll()
    RS.Globals.Story.Mute = True
    RS.Globals.System.CurrentTake.SetCurrentLayer(0)

    # Get CamList, SwitcherProp, and ExportCam
    camList = [cam for cam in RS.Globals.Cameras if not cam.SystemCamera]
    switcherProp = mobu.FBCameraSwitcher().PropertyList.Find('Camera Index')
    exportCamFound = [cam for cam in camList if cam.Name == "ExportCamera"]
    if not exportCamFound:
        return ShowResults(plotCount, len(TakeList), successMessages, [MissingExportCamera], show=not Silent)
    exportCam = exportCamFound[0]

    # Force All Cam Scaling to 1.0
    defaultCamScale = mobu.FBVector3d(1, 1, 1)
    for cam in camList:
        sourceScale = cam.PropertyList.Find("Lcl Scaling")
        sourceScale.Data = defaultCamScale

    # Set Animated Export Cam Props
    for propName in extraProps:
        prop = exportCam.PropertyList.Find(propName)
        if prop:
            prop.SetAnimated(True)

    # Detect AutoClip EndFrame
    clipEndFrame = GetAutoClipEndFrame()

    # MAIN TAKE LOOP
    for take in TakeList:
        RS.Globals.System.CurrentTake = take
        frameRange = (take.LocalTimeSpan.GetStart().GetFrame(), take.LocalTimeSpan.GetStop().GetFrame() + 1)
        switcherKeys = switcherProp.GetAnimationNode().FCurve.Keys

        # Skip Takes with Empty Switchers
        if not switcherKeys:
            errorMessages.append(CameraSwitcherMissingDataError.format(take.Name))
            continue

        # Force Switcher Tangent Options, Remove Key Stars, Clip EndFrame Fix
        for key in switcherKeys:
            key.TangentMode = mobu.FBTangentMode.kFBTangentModeAuto
            key.Interpolation = mobu.FBInterpolation.kFBInterpolationConstant
            key.TangentConstantMode = mobu.FBTangentConstantMode.kFBTangentConstantModeNormal
            if key.Time.GetTimeString().endswith("*"):
                keyFrame = key.Time.GetFrame()
                key.Time = mobu.FBTime(0, 0, 0, keyFrame)
            if key.Time.GetFrame() == clipEndFrame:
                key.Time = mobu.FBTime(0, 0, 0, clipEndFrame + 1)

        # Clear Export Cam's Old Keys
        for prop in exportCam.PropertyList:
            if prop.IsAnimatable():
                propNode = prop.GetAnimationNode()
                if propNode and propNode.KeyCount:
                    keyedNodes = propNode.Nodes if propNode.Nodes else [propNode]
                    [node.FCurve.EditClear() for node in keyedNodes]

        # FRAME LOOP
        for frame in xrange(*frameRange):
            frameTime.SetFrame(frame)
            RS.Globals.Player.Goto(frameTime)
            RS.Globals.Scene.Evaluate()

            # Get Current Camera Matrix
            sourceCam = camList[switcherProp.Data - 1]
            sourceCam.GetMatrix(sourceMatrix)

            # Convert Matrix to Translation and Key
            sourceVector = mobu.FBVector3d((sourceMatrix[12], sourceMatrix[13], sourceMatrix[14]))
            exportCam.Translation.Data = sourceVector
            exportCam.Translation.Key()

            # Convert Matrix to Rotation and Key
            mobu.FBMatrixToRotation(sourceVector, sourceMatrix, mobu.FBRotationOrder.kFBXYZ)
            exportCam.Rotation.Data = sourceVector
            exportCam.Rotation.Key()

            # EXTRA PROP LOOP
            for propName, defaultValue in extraProps.iteritems():
                exportCamProp = exportCam.PropertyList.Find(propName)
                sourceCamProp = sourceCam.PropertyList.Find(propName)

                # Get Source Cam Data - via a default value or prop data
                sourceCamData = sourceCamProp.Data if sourceCamProp else defaultValue

                # TODO: Investigate custom cam props for GTA5 that plot incorrectly -bbuck 1/28/16
                # Calling .Data on some custom cam props (CoCs) gives a different value
                # than when using .GetAnimationNode().FCurve.Evaluate(frameTime). Not sure
                # if it's a bug necessarily, but I did find mismatched values old GTA scenes.

                # Key Extra Prop - if prop and data found
                if exportCamProp is not None and sourceCamData is not None:
                    exportCamProp.GetAnimationNode().KeyAdd(frameTime, sourceCamData)

        # Take Plotted
        plotCount += 1
        successMessages.append("Take [{}] successfully plotted".format(take.Name))

        # Plot DOF Planes - GTA5 Only
        if projectName == "GTA5":
            planeModels = [child for child in exportCam.Children if "Plane" in child.Name]
            frameRange = (take.LocalTimeSpan.GetStart().GetFrame(), take.LocalTimeSpan.GetStop().GetFrame() + 1)
            Lib.PlotTimeRange(planeModels, frameRange=frameRange)

    # Reset Story Mute and Frame Number
    RS.Globals.Story.Mute = formerStoryMuteState
    RS.Globals.Player.Goto(mobu.FBTime(0, 0, 0, formerFrameNumber))
    RS.Globals.Scene.Evaluate()

    # Show Results Output
    totalTime = time.time() - start
    successMessages[0:0] = ["Plot Time: {0:02}:{1:02}\n\n".format(int(totalTime/60), int(totalTime % 60))]
    ShowResults(plotCount, len(TakeList), successMessages, errorMessages, show=not Silent)
