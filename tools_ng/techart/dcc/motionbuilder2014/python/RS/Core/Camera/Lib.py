###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CameraCoreFunctions.py
## Written And Maintained By: Kathryn Bodey, Mark Harrison-Ball
## Contributors: - David Bailey
## Refactored: Mark Harrison-Ball
## Description: Camera create, Export Camera create, Updated Cameras, Removed Non-Switcher Cameras, 
##              Hide/Show DOF Planes
##    
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *

import RS.Globals as glo
import RS.Utils.Collections
import RS.Core.Reference.Manager
import RS.Core.Camera.CameraRelationConstraint
import RS.Utils.Scene
import RS.Utils.UserPreferences as userPref
from RS.Utils.Scene import Constraint

import os
import time

reload( RS.Core.Camera.CameraRelationConstraint )

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

'''
    if Script version changed then there has been a change to the camera values
        If camera has less than 4 planes then full Update
            else
        if camera has a differnt list to our current list:
            Rebuild Properties and Constraints
        else camera has new ranges in teh proepties
            Update Rnages, do not delete the camera proepties
'''
gScriptVersionNumber = "19" # 16 :*major change* new constraints and custom properties added
                            # 17: focus marker lcl rotation reset
                            # 18: fixed plane name issue breaking constraints url:bugstar:1800125
                            # 19: fixed issue in camera relation constrain script url:bugstar:1821585

'''
        History
    44 - Added Simple DOF
    34 - Pushed out focus/ far plane (version update)
    33 - Strip out old invalid custom properties. Hardcoded!
    13 - Fix for DOF near/Far controls
        12 - Added wider expand vlaues and a furthur FOcus distance
        11 - adding in near and far plane overrides
        10 - Added new constraint system
'''

enumList = [ "16mm", "25mm", "35mm", "50mm", "75mm", "120mm", "16-120mm zoom" ]
enumListInitial = len( enumList ) -1

#Made all the kFBPT_floats to kFBPT_doubles because it seems to revert to kFBPT_double even if you specify kFBPT_float. May as well embrace this.
gCamCustomProperties = [['Lens', FBPropertyType.kFBPT_enum, 'Enum', True, True, None, None, None, enumListInitial, True, enumList],
                        ['FOCUS (cm)', FBPropertyType.kFBPT_double, 'Number', True, True, None, 0, 5000, 150, True],                        
                        ['CoC', FBPropertyType.kFBPT_int, 'Integer', True, True, None, 0, 15, 8, True],
                        ['CoC Override', FBPropertyType.kFBPT_int, 'Integer', True, True, None, 0, 15, 0, True],                        
                        ['CoC Night', FBPropertyType.kFBPT_int, 'Integer', True, True, None, 0, 15, 11, True],                        
                        ['CoC Night Override', FBPropertyType.kFBPT_int, 'Integer', True, True, None, 0, 15, 0, True],                        
                        ['Near Outer Override', FBPropertyType.kFBPT_double, 'Number', True, True, None, -1, 1, 0, True],
                        ['Near Inner Override', FBPropertyType.kFBPT_double, 'Number', True, True, None, -1, 1, 0, True],                        
                        ['Far Inner Override', FBPropertyType.kFBPT_double, 'Number', True, True, None, -1, 1, 0, True],
                        ['Far Outer Override', FBPropertyType.kFBPT_double, 'Number', True, True, None, -1, 1, 0, True],
                        ['Motion_Blur', FBPropertyType.kFBPT_double, 'Number', True, True, None, 0, 1, 0, True],                 
                        ['Shallow_DOF', FBPropertyType.kFBPT_bool, 'Bool', True, True, None, 0, 0, False, True],
                        ['Simple_DOF', FBPropertyType.kFBPT_bool, 'Bool', True, True, None, 0, 0, False, True],
                        ['Script_Version', FBPropertyType.kFBPT_charptr, "", False, True, None, 0, 1, gScriptVersionNumber, True]]

# Some scenes have old custom properties, so we should remove thse
gCamInvalidCustomProperties = ( "Near Blend",
                                "Near Blend Multi",
                                "DOF Multiplier",
                                "DOF Expand",
                                "DOF Strength",
                                "DOF Strength Override",
                                "DOF NearPlane Override",
                                "DOF FarPlane Override" )


gCamNewProperties = []


# Default vals for CamProp Info
class CamPropInfo:
    Name = None,
    Type = FBPropertyType.kFBPT_float,
    TypeName = 'Number',
    Min = 0,
    Max = 8000,
    inital = 0,
    Animated = True

# Better way to store camera properties    
def UpdateCamProperties():
    #for propName in gCamPropNames:
    for propName in gCamCustomProperties:
        NewCamPropInfo = CamPropInfo()
        NewCamPropInfo.Name = propName[0]
        NewCamPropInfo.Type = propName[1]
        NewCamPropInfo.TypeName = propName[2]
        NewCamPropInfo.Min = propName[6]
        NewCamPropInfo.Max = propName[7]
        NewCamPropInfo.inital = propName[8]
        NewCamPropInfo.Animated = propName[9]
        gCamNewProperties.append(NewCamPropInfo)
UpdateCamProperties()    

debug = False;

###############################################################################################################
## Simple Debug rs_print. Note you need to run MB with -Console to get the live update Console Commands
###############################################################################################################
def rs_print(strName):    
    if debug:
        FBTrace(strName+"\n")
    else:
        print strName

# Slighty better porcess fo rusing a enum list    
########################################################################
class CamUpdateType:
    none    = None
    shaders = 'Shaders'
    version = 'Version'
    planes  = 'Planes'
    lateral = None







# Python's verison of a enum list
class UpdateCamType:
    Ignore,Interest,All,Constraints,Properties,shaders = range(6)

"""
#*******************************************************************************
     Simple Camera Class for creating cameras
#*******************************************************************************
"""
class _camFunctions:
    def __init__(self):
        self.upDateCam = UpdateCamType() # We use this to define the type of update the camera needs
        self.SwitcherCameras = [] # This will hold the current list of Cameras in the swticher (Animated ones)
        self.lCameraList = [] # This holds teh current camera list, ALL cameras
        #self.lcamUpdateType = CamUpdateType()
        
        self.FocusMarkerScale = 500        
        
        self.FocusMarkerScale = userPref.__Readini__( "CameraToolbox","FocusMarkerScale", self.FocusMarkerScale )
        if self.FocusMarkerScale == None:
            self.FocusMarkerScale = 500

    """
       Class Properties
    """

    # Returns Type of Camera. Will extend this to retrun a camera type
    def isCameraLateral(self, iCam):
        camName = iCam.Name
        if camName.startswith('3Lateral'):
            return True
        return False 

    # Does the camera's plane have a valid material on it.
    """
        Screenfade = FBMOdelPlane
    Planes     = FBMOdel
    Each Plane has one shader and one materail
    """
    def HasInvalidValidMaterial(self, iCam):
        for child in iCam.Children:
            if child.ClassName() == "FBModelPlane" or "FBModel":
                if child.Name.find("_Marker") == -1: # ignore marker Models
                    if len(child.Materials) != 1 or len(child.Shaders) != 1:
                        return True

        return False      

    # Does the Camera have valid Properties
    # We shoudl count the number of peropeties incase there is addiaonl ones added
    def HasInvalidProperties(self, iCam):

        for iProp in gCamCustomProperties:
            bHasProperty= iCam.PropertyList.Find(iProp[0])

            if bHasProperty == None:
                return True

        if self.GetCameraVersion(iCam) < int(gScriptVersionNumber):
            return True

        return False 

    #def HasInvalidConstraints(self, iCam):

    # Is the camera a RTC camera, if so we can't update it until its been baked(print'ed out)
    # FOr now we check if the camera has a parent _bone
    # Also check if cam is a 'valueholder_cam', we dont want to update these either
    # FOr now we check if the camera has a parent _null
    def isRTC_Camera(self, iCam):    
        if iCam.Parent:
            if iCam.Parent.Name.lower().endswith("_bone"):
                return True
            if iCam.Parent.Name.lower().startswith("cameraleadin"):
                return True
        return False


    # New Update Proce CHeck
    def CamUpdateType(self, iCam): 

        if self.isRTC_Camera(iCam):
            return self.upDateCam.Ignore 

        #if iCam.Name == "ExportCamera":
        #    return self.upDateCam.Ignore

        if self.isCameraLateral(iCam):
            return self.upDateCam.Ignore # Requires no Update

        if iCam.Interest:
            return self.upDateCam.Interest # Rrquires a new camera and full update   

        if len(iCam.Children) != 6: # Requires Full update
            return self.upDateCam.All

        if int(self.GetCameraVersion(iCam)) < 18: # Forced Update
            return self.upDateCam.Constraints

        if self.HasInvalidProperties(iCam):  # Requires Rebuild of Properteis, Keeps DOF Keys
            #print "Updating invlaid camera proeprties"
            return self.upDateCam.Properties

        if self.HasInvalidValidMaterial(iCam):
            return self.upDateCam.shaders    

        # Needs to be last Argument
        if int(self.GetCameraVersion(iCam)) == int(gScriptVersionNumber):
            return self.upDateCam.Ignore # Requires no Update

        ' Camera only needs property changes'
        return self.upDateCam.Properties  # Requires just new Proeprty Value CHanges, Keeps Animation


    # Does Camera need to be updated
    # Used by the UI
    # Returns enum
    def GetRequiresUpdate(self, iCam):        
        icamUpdate = CamUpdateType()


        if self.isCameraLateral(iCam):
            return icamUpdate.lateral

        result = icamUpdate.none    
        if int(self.GetCameraVersion(iCam)) < int(gScriptVersionNumber):        
            result = icamUpdate.version
        if len(iCam.Children) != 6:
            result =  icamUpdate.planes
        if self.HasInvalidValidMaterial(iCam):             
            result = icamUpdate.shaders
            
        return result


    # Return our Camera Script Version
    def GetCameraVersion(self, iCam):

        iCurrentProperty = iCam.PropertyList.Find('Script_Version')


        if iCurrentProperty == None:
            return 0
        if iCurrentProperty.Data == "":
            return 0
        return iCurrentProperty.Data

    # Check if we have the five required Planes and they are anmed correctly
    def GetPlanesCount(self,iCam):    
        return (len(iCam.Children)) 

    # Clean up camera Properteis
    def RemoveInvalidProperties(self, iCam):
        for iProp in gCamInvalidCustomProperties:
            bHasProperty = iCam.PropertyList.Find(iProp)
            if (bHasProperty):
                iCam.PropertyRemove(bHasProperty)
                #print ("Found Invalid Proeprty:"+iProp)
            # If we find the invalid property, will rmeove it






    """
    Private Methods
    """ 
    def UpdateActiveSwitcherCameras(self):

        self.__GetCameraList__() # Get current list of cameras
        self.SwitcherCameras = [] # Clear current list of cameras


        lCameraSwitcher = FBCameraSwitcher()
        lSwitcherKeys = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys    

        for i,key in enumerate(lSwitcherKeys):

            lCamera = self.lCameraList[int(key.Value)-1] 

            if lCamera not in self.SwitcherCameras:
                self.SwitcherCameras.append(lCamera)  
        # Add ExportCamera
        lExportCamera = FBFindModelByLabelName("ExportCamera")  
        if (lExportCamera):

            self.SwitcherCameras.append(lExportCamera)




    def __GetCameraList__(self):
        self.lCameraList = []
        for iCamera in glo.gCameras:
            if not "Producer" in iCamera.Name:
                self.lCameraList.append(iCamera) 

    ###############################################################################################################
    ## Definition: PRIVATE: Basic Camera Setup
    ###############################################################################################################    
    def _CreateBaseCamera(self, ExportCamera=False, specificName=None):
        lCamera = self.rs_CameraCreateNoInterest(specificName)
        if ExportCamera:
            lCamera.Name = "ExportCamera"
        lCamera.PropertyList.Find("FieldOfView").SetAnimated(True)        
        self.rs_Planes(lCamera) 

        self.updateCameraProperties(lCamera)

        #for i in range(len(gCamCustomProperties)):
        #  self.rs_CustomProperties(lCamera, gCamCustomProperties[i]) 

        lFarPlane = None
        lNearPlane = None    
        for iChild in lCamera.Children:

            if "FarPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFFarPlane = iChild
                else:
                    lFarPlane = iChild

            elif "NearPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFNearPlane = iChild
                else:
                    lNearPlane = iChild

            elif "Focus_Marker" in iChild.Name:
                lFocusMarker = iChild    
        # Set Camera Materials
        if ExportCamera:
            for iComp in lFarPlane.Components:  
                if iComp.ClassName() == "FBMaterial":
                    iComp.Emissive = FBColor(0,1,0)
                    iComp.Diffuse = FBColor(0,1,0)

            for iComp in lNearPlane.Components:  
                if iComp.ClassName() == "FBMaterial":
                    iComp.Emissive = FBColor(0.75,0.02,1)
                    iComp.Diffuse = FBColor(0.75,0.02,1)

            for iComp in lDOFFarPlane.Components:  
                if iComp.ClassName() == "FBMaterial":
                    iComp.Emissive = FBColor(0,0,1)
                    iComp.Diffuse = FBColor(0,0,1)

            for iComp in lDOFNearPlane.Components:  
                if iComp.ClassName() == "FBMaterial":
                    iComp.Emissive = FBColor(1,0,0)
                    iComp.Diffuse = FBColor(1,0,0)

            # Create custom background colors            
            lCamera.FrameColor = FBColor(1,0,0)    

        self.rs_CameraRelationConstraint(lCamera, lFarPlane, lNearPlane, lDOFFarPlane, lDOFNearPlane, lFocusMarker)
        return lCamera    

    """
       public Methods
    """
    ###############################################################################################################
    ## Description: Definition - Relation Constraint
    ###############################################################################################################    
    def rs_CameraRelationConstraint(self, pCamera, pFarPlane, pNearPlane, pDOFFarPlane, pDOFNearPlane, pFocusMarker):
        #rs_print("Creating Constraints for Camera:"+pCamera.Name)

        RS.Core.Camera.CameraRelationConstraint.CameraConstraintSetup( pCamera )

        # Set plane Data
        ######################################################################################
        self.__updatePlaneInfo__( pFarPlane, pNearPlane, pDOFNearPlane)

    def __updatePlaneInfo__(self, pFarPlane, pNearPlane, pDOFNearPlane):
        
        if pFarPlane == None:
            None
        else:
            pFarPlane.PropertyList.Find("Enable Translation DOF").Data = True
            pFarPlane.PropertyList.Find("TranslationMinX").Data = True
            pFarPlane.PropertyList.Find("TranslationMinY").Data = True
            pFarPlane.PropertyList.Find("TranslationMinZ").Data = True
            pFarPlane.PropertyList.Find("TranslationMaxX").Data = True
            pFarPlane.PropertyList.Find("TranslationMaxY").Data = True
            pFarPlane.PropertyList.Find("TranslationMaxZ").Data = True
            pFarPlane.PropertyList.Find("TranslationMin").Data = FBVector3d(6,0,0)
            pFarPlane.PropertyList.Find("TranslationMax").Data = FBVector3d(99999999,0,0)
    
            pNearPlane.PropertyList.Find("Enable Translation DOF").Data = True
            pNearPlane.PropertyList.Find("TranslationMinX").Data = True
            pNearPlane.PropertyList.Find("TranslationMinY").Data = True
            pNearPlane.PropertyList.Find("TranslationMinZ").Data = True
            pNearPlane.PropertyList.Find("TranslationMaxY").Data = True
            pNearPlane.PropertyList.Find("TranslationMaxZ").Data = True
            pNearPlane.PropertyList.Find("TranslationMin").Data = FBVector3d(6,0,0)
    
            pDOFNearPlane.PropertyList.Find("Enable Translation DOF").Data = True
            pDOFNearPlane.PropertyList.Find("TranslationMinX").Data = True
            pDOFNearPlane.PropertyList.Find("TranslationMin").Data = FBVector3d(4.86,0,0)    

    ###############################################################################################################
    ## Description: Definition - Camera Custom Property Setup
    ###############################################################################################################    
    def rs_CustomProperties(self, pCamera, pCreateCustomProperty):
        #rs_print("Updating Camera Properties for:"+pCamera.Name)

        iCurrentProperty = pCamera.PropertyList.Find(pCreateCustomProperty[0])

        if not iCurrentProperty:    

            lCamProperty = pCamera.PropertyCreate(pCreateCustomProperty[0], pCreateCustomProperty[1], pCreateCustomProperty[2], pCreateCustomProperty[3], pCreateCustomProperty[4], pCreateCustomProperty[5])

            if pCreateCustomProperty[6] != None:
                lCamProperty.SetMin(pCreateCustomProperty[6], True)
            if pCreateCustomProperty[7] != None:
                lCamProperty.SetMax(pCreateCustomProperty[7], True)

            # create the enum list for lens property
            if len ( pCreateCustomProperty ) > 10:        
                lEnumList = lCamProperty.GetEnumStringList(True)
                for lensFocus in pCreateCustomProperty[10]:
                    lEnumList.Add( lensFocus )
                lCamProperty.NotifyEnumStringListChanged()

            if pCreateCustomProperty[8] != None:    
                lCamProperty.Data = pCreateCustomProperty[8]

            if  pCreateCustomProperty[1] != FBPropertyType.kFBPT_charptr:
                lCamProperty.SetAnimated(pCreateCustomProperty[9])   

        else: # Lets check in the min/max values of your property has changed

            iCurrentVal = pCamera.PropertyList.Find(pCreateCustomProperty[0]).Data
            iMaxVal = pCamera.PropertyList.Find(pCreateCustomProperty[0]).GetMax() # Our new Max Value        
            # If you current value is less than the Max then leave that value else we set it to the Max Value            

            if pCreateCustomProperty[6] != None:
                iCurrentProperty.SetMin(pCreateCustomProperty[6], True)
            if pCreateCustomProperty[7] != None:
                iCurrentProperty.SetMax(pCreateCustomProperty[7], True)

            if iCurrentVal > iMaxVal:
                iCurrentProperty.data =pCreateCustomProperty[8]

            # make sure we update the script version 
            if pCreateCustomProperty[0] == 'Script_Version':
                iCurrentProperty.Data = pCreateCustomProperty[8]

        return

    ###############################################################################################################
    ## Description: Definition - Camera Plane Setup - Create, Offset, Materials & Shaders, - Focus Marker Setup
    ###############################################################################################################    
    def rs_Planes(self,pCamera):

        lFadePlane = FBModelPlane(pCamera.Name + " ScreenFade")
        lFadePlane.Show = True
        lFadePlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,90)    
        lTag = lFadePlane.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts'

        lFarPlane = FBModelPlane(pCamera.Name + " FarPlane")
        lFarPlane.Show = False    
        lTag = lFarPlane.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts' 

        lDOFStrengthFarPlane = FBModelPlane(pCamera.Name + " DOF_Strength_FarPlane")
        lDOFStrengthFarPlane.Show = False
        lDOFStrengthFarPlane.Scaling = FBVector3d(0.5, 0.5, 0.5)
        lDOFStrengthFarPlane.Rotation = FBVector3d(0,0,90)            
        lTag = lDOFStrengthFarPlane.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts' 

        lNearPlane = FBModelPlane(pCamera.Name + " NearPlane")
        lNearPlane.Show = False
        lTag = lNearPlane.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts' 

        lDOFStrengthNearPlane = FBModelPlane(pCamera.Name + " DOF_Strength_NearPlane")
        lDOFStrengthNearPlane.Show = False    
        lDOFStrengthNearPlane.Scaling = FBVector3d(0.5, 0.5, 0.5)
        lDOFStrengthNearPlane.Rotation = FBVector3d(0,0,90)     
        lTag = lDOFStrengthNearPlane.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts' 

        lFadePlane.Parent = pCamera
        lFadePlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(50.76,0,0)
        lFadePlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,90)
        lFarPlane.Parent = pCamera
        lNearPlane.Parent = pCamera
        lDOFStrengthFarPlane.Parent = pCamera
        lDOFStrengthNearPlane.Parent = pCamera

        lFadePlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
        lFarPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
        lNearPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
        lDOFStrengthFarPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
        lDOFStrengthNearPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)

        lFadePlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(50.76,0,0)
        lFarPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(183.08,0,0)    
        lNearPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(116.92,0,0)
        lDOFStrengthFarPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(227.19,0,0)
        lDOFStrengthNearPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(94.86,0,0)

        lFadePlaneMat = FBMaterial(pCamera.Name + " ScreenFadeMat")
        lFadePlaneMat.Diffuse = FBColor(0.0,0.0,0.0)
        lFadePlaneMat.PropertyList.Find("Diffuse").SetAnimated(True)
        lFadePlaneMat.TransparencyFactor = 1
        lFadePlaneMat.PropertyList.Find("TransparencyFactor").SetAnimated(True)    
        lTag = lFadePlaneMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Materials'

        lFarPlaneMat = FBMaterial(pCamera.Name + " FarPlane")    
        lFarPlaneMat.Emissive = FBColor(0,0,1)
        lFarPlaneMat.Diffuse = FBColor(0.02,0.26,0.10)      
        lTag = lFarPlaneMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Materials'

        lDOFStrengthFarPlaneMat = FBMaterial(pCamera.Name + " DOF_Strength_FarPlane")    
        lDOFStrengthFarPlaneMat.Emissive = FBColor(0,1,1)
        lDOFStrengthFarPlaneMat.Diffuse = FBColor(0.8,0.8,1)      
        lTag = lDOFStrengthFarPlaneMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Materials'

        lNearPlaneMat = FBMaterial(pCamera.Name + " NearPlane")
        lNearPlaneMat.Emissive = FBColor(0.8,0.28,0.01)
        lNearPlaneMat.Diffuse = FBColor(0.8,0.28,0.01) 
        lTag = lNearPlaneMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Materials'

        lDOFStrengthNearPlaneMat = FBMaterial(pCamera.Name + " DOF_Strength_NearPlane")    
        lDOFStrengthNearPlaneMat.Emissive = FBColor(1,0,1)
        lDOFStrengthNearPlaneMat.Diffuse = FBColor(1,0.8,0.8)      
        lTag = lDOFStrengthNearPlaneMat.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Materials'

        lMaterialArray = [lFadePlaneMat, lFarPlaneMat, lNearPlaneMat, lDOFStrengthFarPlaneMat, lDOFStrengthNearPlaneMat]

        lPlanesTrans = FBShaderLighted(pCamera.Name + " Transparency_Planes")
        lPlanesTrans.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha
        lPlanesTrans.Alpha  = 0.46
        lTag = lPlanesTrans.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Shaders'

        lFadePlaneTrans = FBShaderManager().CreateShader("FlatShader")
        lFadePlaneTrans.Name = pCamera.Name + " ScreenFadeShader"
        lFadePlaneTrans.PropertyList.Find("Transparency Type").Data = 1
        lTag = lFadePlaneTrans.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Shaders'

        lShaderArray = [lPlanesTrans, lFadePlaneTrans]

        lFadePlane.Materials.append(lFadePlaneMat) 
        lFarPlane.Materials.append(lFarPlaneMat)       
        lNearPlane.Materials.append(lNearPlaneMat)
        lDOFStrengthFarPlane.Materials.append(lDOFStrengthFarPlaneMat)
        lDOFStrengthNearPlane.Materials.append(lDOFStrengthNearPlaneMat)    

        lFadePlane.Shaders.append(lFadePlaneTrans)
        lFadePlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll    
        lFarPlane.Shaders.append(lPlanesTrans)
        lFarPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
        lNearPlane.Shaders.append(lPlanesTrans)
        lNearPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
        lDOFStrengthNearPlane.Shaders.append(lPlanesTrans)
        lDOFStrengthNearPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
        lDOFStrengthFarPlane.Shaders.append(lPlanesTrans)
        lDOFStrengthFarPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll

        lMaterialFolder = FBFolder(pCamera.Name + ":Materials", lFadePlaneMat)
        lTag = lMaterialFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Folders' 
        for iMat in lMaterialArray:
            lMaterialFolder.ConnectSrc(iMat)

        lShaderFolder = FBFolder(pCamera.Name + ":Shaders", lPlanesTrans)
        lTag = lShaderFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Folders'
        lShaderFolder.ConnectSrc(lFadePlaneTrans)

        lFocusMarker = FBModelMarker(pCamera.Name + "Focus_Marker")
        lFocusMarker.Show = True
        lFocusMarker.Look = FBMarkerLook.kFBMarkerLookHardCross 
        lFocusMarker.Size = float( self.FocusMarkerScale )
        lFocusMarker.Color = FBColor(0, 1, 0)
        lFocusMarker.PropertyList.Find("Enable Selection").Data = False
        lTag = lFocusMarker.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Contacts'
        lFocusMarker.Parent = pCamera

        pCamera.PropertyCreate(pCamera.Name + ' ScreenFadeMat.Diffuse', FBPropertyType.kFBPT_Reference, "", True, True, lFadePlaneMat.PropertyList.Find('Diffuse'))
        pCamera.PropertyCreate(pCamera.Name + ' ScreenFadeMat.Transparency', FBPropertyType.kFBPT_Reference, "", True, True, lFadePlaneMat.PropertyList.Find('Transparency'))  

    ###############################################################################################################
    ## Description: Definition - Camera Create - without interest 
    ###############################################################################################################    
    def rs_CameraCreateNoInterest(self, specificName=None):
        lCam = FBCamera("R*Camera 1")
        if specificName:
            lCam.Name = specificName
        lCam.Show = True
        lCam.ResolutionMode = FBCameraResolutionMode.kFBResolutionCustom
        lCam.ResolutionWidth = 1920
        lCam.ResolutionHeight = 1080
        lCam.FrameColor = FBColor(0,0,0)
        lCam.UseFrameColor = True
        lCam.SafeAreaMode = FBCameraSafeAreaMode.kFBSafeAreaSquare 
        lCam.ViewDisplaySafeArea = True
        lCam.BackGroundImageFit = False
        lCam.BackGroundPlaneDistance = 100
        lCam.ForeGroundMaterialThreshold = 50
        lCam.ForeGroundPlaneDistanceMode = FBCameraDistanceMode.kFBDistModeAbsoluteFromCamera
        lCam.FilmBackType = FBCameraFilmBackType.kFBFilmBackCustom
        lCam.FarPlaneDistance = 400000000   
        lCam.ViewShowGrid = False
        # Change the default value of FieldOfView
        lProp = lCam.PropertyList.Find("FieldOfView")
        if lProp and lProp.IsAnimatable():
            lProp.SetAnimated(True)
            lProp.Data = 37.0
        
        lTag = lCam.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = 'rs_Cameras' 
        currentCamera = FBSystem().Renderer.CurrentCamera
        if currentCamera:
            lCam.Translation = FBVector3d(currentCamera.Translation)
            lCam.Rotation = FBVector3d(currentCamera.Rotation)
        return lCam 

    ###############################################################################################################
    ## Definition: PUBLIC: Add Camera Properties 
    ###############################################################################################################    
    def updateCameraProperties(self, iCam, HasPropertyValues=[]):    
        for i in range(len(gCamCustomProperties)):
            self.rs_CustomProperties(iCam, gCamCustomProperties[i])     

    ###############################################################################################################
    ## Definition: PUBLIC: Remove Camera Properties 
    ###############################################################################################################     
    def removeCameraProperties(self, iCam):
        propertyValues =[]
        for i in range(len(gCamCustomProperties)):
            if iCam.PropertyList.Find(gCamCustomProperties[i][0]):
                propertyValues.append(iCam.PropertyList.Find(gCamCustomProperties[i][0]).Data)
                iCam.PropertyRemove(iCam.PropertyList.Find(gCamCustomProperties[i][0])) 
            else:
                propertyValues.append(gCamCustomProperties[i][7]) # Append default value
        return propertyValues

    ###############################################################################################################
    ## Definition: Create a Custom Camera
    ###############################################################################################################    
    def CreateExportCustomCamera(self, silent=False):
        lExportCamera = FBFindModelByLabelName("ExportCamera")
        if lExportCamera:
            if not silent:
                FBMessageBox("Create Export Cam", "Export Custom Camera already exists. Aborting Script.", "OK")
                return None
        else:  
            lExportCamera = self._CreateBaseCamera(True)
            return lExportCamera



    ###############################################################################################################
    ## Definition: Create a New Export Camera
    ###############################################################################################################    
    def CreateCustomCamera(self):
        lCamera = self._CreateBaseCamera()
        return lCamera
    
    
    '''
    BUG: 1869497
    
    This 'should' remove all rouge planes and anything parented to them because of the way using lists to delete objects can
    cause SDK bind errors (which does make sense) we need to put trys/catches and delete the full range of items
    NOTE: WOuld be good to put togeather a proper delete class.
    '''
    def CleanUpParentLessPlanes(self, pCamera = "ExportCamera"):
                _planesList=["ScreenFade",
               "FarPlane",
               "NearPlane",
               "DOF_Strength_FarPlane",
               "DOF_Strength_NearPlane",
               "Focus_Marker"]
    
                lDeleteList = []
                 
                # Only check the scene root for rouge camera planes/focus markers 
                for obj in glo.gScene.RootModel.Children:
                        for p in _planesList:
                                if obj.Name.endswith(p):
                                        # Get any children parented (shaders, constraints etc)
                                        for i in range(obj.GetSrcCount()):
                                                try:
                                                        # Becuase we are delting from an active list we need to try deleting the rnage of items again as they will beomce obsolete. SDK Error
                                                        for i in range(obj.GetSrcCount()):  
                                                                obj.GetSrc(i).FBDelete()
                                                except:
                                                        ''''''
                                            
                                        lDeleteList.append(obj.Name)
                                                
        
                # We can delete the models safely use teh name look up.
                for i in lDeleteList:
                        obj = FBFindModelByLabelName(i)
                        if obj:
                                print 'Deelting Rouge Planes:', obj.Name
                                obj.FBDelete()
    
    
    '''
    Definition: Delete Floating Planes where people did not remove the camera's children planes, etc. - 1170315
    HB: I've disabled this code for now as it does not always work due to the way nothing seeminly works in MB as expected.
    '''
    def _CleanUpParentLessPlanes(self, pCamera = "ExportCamera"):
        Planes = [["ScreenFade", "ScreenFadeMat", "ScreenFadeShader"], ["FarPlane", "FarPlane", "Transparency_Planes"], 
                  ["NearPlane", "NearPlane", "Transparency_Planes"], ["DOF_Strength_FarPlane", "DOF_Strength_FarPlane", "Transparency_Planes"], 
                  ["DOF_Strength_NearPlane", "DOF_Strength_NearPlane", "Transparency_Planes"]]
        Marker = "Focus_Marker"
        Relation = "Relation"

        lDeleteList = []          

        lCamera = FBFindModelByLabelName(pCamera)
        if not lCamera:

            # Delete Planes, Material and Shaders
            for i in range(len(Planes)):    
                lConnectedExportCamera = False                   
                lPlane = FBFindModelByLabelName(pCamera + " " + Planes[i][0])
                if lPlane:
                    lConnectedExportCamera = False
                    for i in range(lPlane.GetDstCount()):
                        if lPlane.GetDst(i).Name == pCamera:
                            lConnectedExportCamera = True
                    if lConnectedExportCamera == False: # Not Connected to the Camera      

                        # Delete Relation Constraint, it's safe to do it here because there is no camera we are looking for, so the relation won't work
                        for lConst in FBSystem().Scene.Constraints:
                            if lConst.Name == pCamera + " " + Relation:
                                lConst.FBDelete()

                        lShaderMatDelete = []
                        for i in range(lPlane.GetSrcCount()):                 
                            if lPlane.GetSrc(i).Name == pCamera + " " + Planes[i][1] or lPlane.GetSrc(i).Name == pCamera + " " + Planes[i][2]:
                                lShaderMatDelete.append(lPlane.GetSrc(i))

                        map(FBComponent.FBDelete, lShaderMatDelete)

                    lDeleteList.append(lPlane)

            map(FBComponent.FBDelete, lDeleteList)

            # Delete Focus Marker
            lConnectedExportCamera = False
            lFocusMarker = FBFindModelByLabelName(pCamera + Marker)
            if lFocusMarker:
                for i in range(lFocusMarker.GetDstCount()):
                    if lFocusMarker.GetDst(i).Name == pCamera:
                        lConnectedExportCamera = True                
                if lConnectedExportCamera == False:
                    lFocusMarker.FBDelete()    

"""
#*******************************************************************************
     Simple Camera Class for updating cameras
#*******************************************************************************
"""  
class _camUpdate:
    def __init__(self):    
        self.camFunctions = _camFunctions() 
        self.bSilent = False
        #self.lFbp = FBProgress() # Generic Progress Bar
        
        self.FocusMarkerScale = 500        
        
        self.FocusMarkerScale = userPref.__Readini__( "CameraToolbox","FocusMarkerScale", self.FocusMarkerScale )
        if self.FocusMarkerScale == None:
            self.FocusMarkerScale = 500

        # Testing Colour Button Workaround
        self.GreyButton = FBColor(0.36, 0.36, 0.36)
        self.RedButton = FBColor(1.0, 0.0, 0.0)    


    ###############################################################################################################
    ## Description: Update Cams if they had less than 5 planes and add in Export Cam
    ###############################################################################################################    
    """
    Private Methods
    """  

    # Fade Key Events
    def __AddFadeInKey__(self):
        return

    def __AddFadeOutKey__(self):
        return   

    def __SetFadeLength__(self):
        return   

    def __ShowFadeKeyed__(self):
        return     


    def _rebuildCamShaders(self, iCam):
        
        # Instanced Transparency Shader
        lPlanesTrans = FBShaderLighted(iCam.Name + " Transparency_Planes")
        lPlanesTrans.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha
        lPlanesTrans.Alpha  = 0.46        


        for iChild in iCam.Children:
            #print iChild.ClassName()
            if iChild.ClassName() == "FBModelPlane" or "FBModel":        
                if iChild.Name.find("Marker") < 0 and iChild.Name.find("ScreenFade") < 0:
                    deleteList = []
                    for mat in iChild.Materials:
                        deleteList.append(mat)
                    for mat in iChild.Shaders:
                        deleteList.append(mat)

                    # Delete our Shaders
                    for node in deleteList:  
                        node.FBDelete() 
                    # Create our new Shaders        

                    # Add Unique Material
                    lMat = FBMaterial(iChild.Name + "Mat")
                    lMat.ShadingMode  = FBModelShadingMode.kFBModelShadingAll

                    # Bit shitty but i am fucking tired        
                    if iChild.Name.find("Strength_FarPlane") > 0:
                        lMat.Emissive = FBColor(0,1,1)
                        lMat.Diffuse = FBColor(0.8,0.8,1) 
                    elif iChild.Name.find("Strength_NearPlane") > 0:
                        lMat.Emissive = FBColor(1,0,1)
                        lMat.Diffuse = FBColor(1,0.8,0.8) 
                    elif iChild.Name.find(" FarPlane") > 0:
                        lMat.Emissive = FBColor(0,0,1)
                        lMat.Diffuse = FBColor(0.02,0.26,0.10) 
                    elif iChild.Name.find(" NearPlane") > 0:
                        lMat.Emissive = FBColor(0.8,0.28,0.01)
                        lMat.Diffuse = FBColor(0.8,0.28,0.01) 

                    # Add unique and instnaced Shader
                    iChild.Materials.append(lMat) 
                    iChild.Materials.append(lPlanesTrans) 
                # Check our Screen Fade, we only replace if its not there!!!
                # We also add a special Shader to it
                # Add a Shader Lib

                elif iChild.Name.find("ScreenFade") > 0:
                    bScreenfadeShader = False
                    bScreenfadeMat = False
                    for mat in iChild.Materials:            
                        if mat.Name.find("ScreenFadeMat") > 0:
                            bScreenfadeMat = True
                    for mat in iChild.Shaders:            
                        if mat.Name.find("ScreenFadeShader") > 0:
                            bScreenfadeShader = True    
                    if not bScreenfadeShader:
                        lFadePlaneTrans = FBShaderManager().CreateShader("FlatShader")
                        lFadePlaneTrans.Name = iCam.Name + " ScreenFadeShader"
                        lFadePlaneTrans.PropertyList.Find("Transparency Type").Data = 1
                        iChild.Shaders.append(lFadePlaneTrans)
                    if not bScreenfadeMat:
                        lFadePlaneMat = FBMaterial(iCam.Name + " ScreenFadeMat")
                        lFadePlaneMat.Diffuse = FBColor(0.0,0.0,0.0)
                        lFadePlaneMat.PropertyList.Find("Diffuse").SetAnimated(True)
                        lFadePlaneMat.TransparencyFactor = 1
                        lFadePlaneMat.PropertyList.Find("TransparencyFactor").SetAnimated(True) 
                        iChild.Materials.append(lFadePlaneMat)     
        return

    # This will Update the camera proeprteis, set them animatable
    def _updateCamProperties(self, iCam):

        # script v.16 updates url:bugstar:1733892
        # rename FOCUS to FOCUS (cm) - need to keep keys intact, but exporter now looks for (cm) in the name 
        # delete shallow and simple dofs - Derm wants them cleared of keys & turned off if the cams arent from v.16 (easier to just delete and rebuild)
        for i in iCam.PropertyList:
            if i.Name == 'FOCUS':
                i.Name = 'FOCUS (cm)'

            elif i.Name == 'Shallow_DOF':
                iCam.PropertyRemove( i )

            elif i.Name == 'Simple_DOF':
                iCam.PropertyRemove( i )

        # Default Properties
        if iCam.PropertyList.Find("FieldOfView"):
            iCam.PropertyList.Find("FieldOfView").SetAnimated(True)    


        # R* custom Properties
        for iProp in gCamCustomProperties:
            if iCam.PropertyList.Find(iProp[0]):
                if iProp[0] != "Script_Version":  # This is not animatable
                    iCam.PropertyList.Find(iProp[0]).SetAnimated(True)    


        #for iProp in lPropertyList:
            #if iCam.PropertyList.Find(iProp):
                #iCam.PropertyList.Find(iProp).SetAnimated(True)

        self.camFunctions.updateCameraProperties(iCam)     
        return


    def _rebuildConstraints(self, iCam):
        deleteList = []
        for iChild in iCam.Components:
            if iChild.ClassName() == "FBConstraintRelation":
                deleteList.append(iChild)        

        for iExportCamDelete in deleteList:        
            iExportCamDelete.FBDelete()

        lFarPlane = None
        lNearPlane = None    
        lDOFFarPlane = None
        lDOFNearPlane = None
        lFocusMarker = None

        for iChild in iCam.Children:
            if "FarPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFFarPlane = iChild
                else:
                    lFarPlane = iChild

            elif "NearPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFNearPlane = iChild
                else:
                    lNearPlane = iChild

            elif "Focus_Marker" in iChild.Name:
                lFocusMarker = iChild
                # update focus marker to new settings url:bugstar:1733892
                lFocusMarker.Size = float( self.FocusMarkerScale )
                lFocusMarker.Color = FBColor(0, 1, 0)       

        # script v.16 updates url:bugstar:1733892
        # remove old constraints, replace with new ones
        conDeleteList = []
        for con in RS.Globals.gConstraints:    
            for i in range( con.GetDstCount() ):  
                if con.GetDst( i ).ClassName() == 'FBCamera':
                    if con.PropertyList.Find('camera_macro_relation'):
                        None
                    else:
                        conDeleteList.append( con )

        if len( conDeleteList ) > 0:
            for i in conDeleteList:
                i.FBDelete()

        if lDOFFarPlane == None:
            print 'missing FAR DOF: ', iCam.Name
            print
            
        self.camFunctions.rs_CameraRelationConstraint(iCam, lFarPlane, lNearPlane, lDOFFarPlane, lDOFNearPlane, lFocusMarker) 


    def _updateCamConstraints(self, iCam):
        
        self.camFunctions.rs_Planes(iCam)                
        
        lFarPlane = None
        lNearPlane = None    
        lDOFFarPlane = None
        lDOFNearPlane = None
        lFocusMarker = None
        
        for iChild in iCam.Children:
            if "FarPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFFarPlane = iChild
                else:
                    lFarPlane = iChild

            elif "NearPlane" in iChild.Name:
                if "DOF" in iChild.Name:
                    lDOFNearPlane = iChild
                else:
                    lNearPlane = iChild

            elif "Focus_Marker" in iChild.Name:
                lFocusMarker = iChild    
         
        
        self.camFunctions.rs_CameraRelationConstraint(iCam, lFarPlane, lNearPlane, lDOFFarPlane, lDOFNearPlane, lFocusMarker)       

    """
    Set all the camera Frame Colors
    """
    def _setCamFrameColors_(self):
        for iCam in glo.gCameras:
            if not iCam.SystemCamera:
                if iCam.Name == "ExportCamera":
                    iCam.FrameColor = FBColor(1,0,0)
                elif "CamRock" in iCam.Name:
                    iCam.FrameColor = FBColor(0.5,0.45,0)    


        return




    """
    Public Methods
    """




    def updateCameraProperties(self):    
        #rs_print("Updating camera Propereis")
        for iCam in glo.gCameras:
            if not iCam.SystemCamera:
                #self.camFunctions.removeCameraProperties(iCam)        
                self.camFunctions.updateCameraProperties(iCam)#,propertyValues )
        return    

    ###############################################################################################################
    ## Description: Convert from Intrest Camera to Intrestless
    ###############################################################################################################    

    def ConvertIntrestCam(self, iCam):      
        lRotCamDeleteList = []
        lNewCamDeleteList = []

        #rs_print("Interest found on Cam: " + iCam.Name)

        lCamInt = iCam.Interest

        iCam.PropertyList.Find("Roll").SetAnimated(True)
        iCam.PropertyList.Find("Field Of View").SetAnimated(True)

        lTrnsNull = FBModelNull(iCam.Name + "_Null")

        lRotCamChildList = []
        lRotCam = self.camFunctions.CreateCustomCamera()

        lRotCam.PropertyList.Find("Field Of View").SetAnimated(True)

        RS.Utils.Scene.GetChildren(lRotCam, lRotCamChildList)
        #lRotCamChildList.extend(eva.gChildList)
        #del eva.gChildList[:]  

        for iChild in lRotCamChildList:
            iChild.Name = "ValueHolder_Cam"

        # Create a new camera and copy our Intrest values over to the new Intrestles camera
        lNewCam = self.camFunctions.CreateCustomCamera()
        lNewCam.PropertyList.Find("Field Of View").SetAnimated(True)

        lRotCam.Parent = lTrnsNull

        lVector = FBVector3d(0,0,0)
        iCam.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        lTrnsNull.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        iCam.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
        lTrnsNull.SetVector(lVector, FBModelTransformationType.kModelRotation, True)            
        FBSystem().Scene.Evaluate()

        lVector = FBVector3d(0,0,0)
        iCam.GetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        lRotCam.SetVector(lVector, FBModelTransformationType.kModelTranslation, True)
        iCam.GetVector(lVector, FBModelTransformationType.kModelRotation, True)
        lRotCam.SetVector(lVector, FBModelTransformationType.kModelRotation, True)    

        FBSystem().Scene.Evaluate()

        lParentConst = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        lParentConst.Name = ("Old_Translation_To_Null")
        lParentConst.ReferenceAdd (0,lTrnsNull)
        lParentConst.ReferenceAdd (1,iCam)
        lParentConst.Snap() 

        lRotRelCon = FBConstraintRelation("iCam_To_RotCam")
        lRotRelCon.Active = True

        lOldSender = lRotRelCon.SetAsSource(iCam)
        lRotRelCon.SetBoxPosition(lOldSender, 0, 100)
        lOldSender.UseGlobalTransforms = False

        lRotReceiver = lRotRelCon.ConstrainObject(lRotCam)
        lRotRelCon.SetBoxPosition(lRotReceiver, 800, 100)
        lRotReceiver.UseGlobalTransforms = False

        lNumVecConv = lRotRelCon.CreateFunctionBox('Converters', 'Number to Vector')
        lRotRelCon.SetBoxPosition(lNumVecConv, 400, 100)

        RS.Utils.Scene.ConnectBox(lOldSender, 'Roll', lNumVecConv, 'X')
        RS.Utils.Scene.ConnectBox(lNumVecConv, 'Result', lRotReceiver, 'Lcl Rotation') 

        lNewCamParentConst = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        lNewCamParentConst.Name = ("ValueCam_NewCam")
        lNewCamParentConst.ReferenceAdd (0,lNewCam)
        lNewCamParentConst.ReferenceAdd (1,lRotCam)
        lNewCamParentConst.Active = True



        # Select NOne
        self.rs_DeSelectComponents()
        lRotCam.Selected = True    
        lPlotPeriod = FBTime(0, 0, 0, 1)
        FBSystem().CurrentTake.PlotTakeOnSelected(lPlotPeriod)     
        lRotCam.Selected = False    
        FBSystem().Scene.Evaluate()

        self.rs_DeSelectComponents()
        lNewCam.Selected = True    
        lPlotPeriod = FBTime(0, 0, 0, 1)
        FBSystem().CurrentTake.PlotTakeOnSelected(lPlotPeriod)      
        lNewCam.Selected = False    
        FBSystem().Scene.Evaluate() 

        RS.Utils.Scene.GetChildren(lRotCam, lRotCamDeleteList)
        #lRotCamDeleteList.extend(eva.gChildList)
        #del eva.gChildList[:]  

        for iComp in lRotCam.Components:
            if iComp.ClassName() == "FBMaterial":
                lRotCamDeleteList.append(iComp)
            if iComp.ClassName() == "FBShaderLighted":
                lRotCamDeleteList.append(iComp)    
            if iComp.ClassName() == "FBShader":
                lRotCamDeleteList.append(iComp)   
            if iComp.ClassName() == "FBConstraintRelation":
                lRotCamDeleteList.append(iComp) 

        lRotCamDeleteList.append(lParentConst)
        lRotCamDeleteList.append(lRotRelCon)
        lRotCamDeleteList.append(lNewCamParentConst)
        lRotCamDeleteList.append(lTrnsNull)

        lRotCamDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lRotCamDeleteList) 

        #print "deleting"
        for iDelete in lRotCamDeleteList:
            if iDelete:
                iDelete.FBDelete()

        FBSystem().Scene.Evaluate()

        liCamParentConst = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        liCamParentConst.Name = ("NewCam_iCam")
        liCamParentConst.ReferenceAdd (0,lNewCam)
        liCamParentConst.ReferenceAdd (1,iCam)
        liCamParentConst.Active = True

        lCamInt.FBDelete()

        self.rs_DeSelectComponents()
        FBSystem().Scene.Evaluate()    
        iCam.Selected = True    
        lPlotPeriod = FBTime(0, 0, 0, 1)
        FBSystem().CurrentTake.PlotTakeOnSelected(lPlotPeriod)      
        iCam.Selected = False  

        RS.Utils.Scene.GetChildren(lNewCam, lNewCamDeleteList)
        #lNewCamDeleteList.extend(eva.gChildList)
        #del eva.gChildList[:]  

        for iComp in lNewCam.Components:
            if iComp.ClassName() == "FBMaterial":
                lNewCamDeleteList.append(iComp)
            if iComp.ClassName() == "FBShaderLighted":
                lNewCamDeleteList.append(iComp)    
            if iComp.ClassName() == "FBShader":
                lNewCamDeleteList.append(iComp)   
            if iComp.ClassName() == "FBConstraintRelation":
                lNewCamDeleteList.append(iComp) 

        lNewCamDeleteList.append(liCamParentConst)

        lNewCamDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lNewCamDeleteList) 

        for iDelete in lNewCamDeleteList:
            if iDelete:
                iDelete.FBDelete() 



        ###############################################################################################################
        ## Description: Update Interest Cams to Interestless Cams - for old cutscenes
        ###############################################################################################################
        #def UpdateIntrestCameras(self):
            #rs_print("Updating Intrest Cams to Interestless Cams")
            #for iCam in FBSystem().Scene.Cameras:        
                #if iCam.Interest:
                #print "Kilel it"
                #self.ConvertIntrestCam(iCam)    


    #
    # This will no Delete only Export camera branches and keep the camera itself.
    # BUG 1299306 - Removes screen overlay
    def deleteExportCamera(self):
        #rs_print("Deleting Old Export Camera")
        lExportCam = FBFindModelByLabelName("ExportCamera")


        lExportCamList = []
        lExportCamDeleteList = []

        if lExportCam:

            RS.Utils.Scene.GetChildren(lExportCam, lExportCamList)
            #lExportCamList.extend(eva.gChildList)
            #del eva.gChildList[:]   

            for iExpCamItem in lExportCamList:

                for iComp in iExpCamItem.Components:
                    if iComp.ClassName() == "FBMaterial":
                        lExportCamDeleteList.append(iComp)
                    if iComp.ClassName() == "FBShaderLighted":
                        lExportCamDeleteList.append(iComp)    
                    if iComp.ClassName() == "FBShader":
                        lExportCamDeleteList.append(iComp)    
                    if iComp.ClassName() == "FBConstraintRelation":
                        lExportCamDeleteList.append(iComp)
                # We don't want to delete our actual export cam because
                # We may have some proeprties from the exporter set o it
                # FUCK KNOWS WHY WE WOULD DO THAT, CRAZY!!!
                if iExpCamItem.Name != "ExportCamera":
                    lExportCamDeleteList.append(iExpCamItem)

        lExportCamDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lExportCamDeleteList)

        for iExportCamDelete in lExportCamDeleteList: 
            iExportCamDelete.FBDelete()    
        return   




    def DeleteCamera(self, iCam, RemoveCam=False):
        if iCam:
            lDeleteList = []

            if RemoveCam:
                lDeleteList.append(iCam)


            # Add our Shaders to the delete List       
            for iChild in iCam.Children:
                for iComp in iChild.Components:
                    if iComp.ClassName() == "FBMaterial":
                        lDeleteList.append(iComp)
                    if iComp.ClassName() == "FBShaderLighted":
                        lDeleteList.append(iComp)    
                    if iComp.ClassName() == "FBShader":
                        lDeleteList.append(iComp) 
            # Add our Constraints to the delete List
            for iComp in iCam.Components:  
                if iComp.ClassName() == "FBConstraintRelation":
                    lDeleteList.append(iComp)    
            # Add our camera planes to the delete List        
            for iChild in iCam.Children:
                # Skip Child Cameras - for rare case of parented cams
                if iChild.ClassName() == "FBCamera":
                    continue
                lDeleteList.append(iChild)
            # Remove our camera properties    
            self.camFunctions.removeCameraProperties(iCam)
            # Add our Plane Materials to the Delete List
            if iCam.PropertyList.Find(iCam.Name + " ScreenFadeMat.Diffuse"):
                for iMat in glo.gMaterials:
                    if iMat.Name == iCam.Name + " ScreenFadeMat":
                        lDeleteList.append(iMat)   

            if iCam.PropertyList.Find("ScreenFadeMaterial.Diffuse"):
                lPropDiff = iCam.PropertyList.Find("ScreenFadeMaterial.Diffuse")
                iCam.PropertyRemove(lPropDiff) 
            if iCam.PropertyList.Find("ScreenFadeMaterial.Opacity"):
                lPropOpac = iCam.PropertyList.Find("ScreenFadeMaterial.Opacity")
                iCam.PropertyRemove(lPropOpac)                                    
            if iCam.PropertyList.Find(iCam.Name + " ScreenFadeMat.Diffuse"):
                lPropCamDiff = iCam.PropertyList.Find(iCam.Name + " ScreenFadeMat.Diffuse")
                iCam.PropertyRemove(lPropCamDiff) 
            if iCam.PropertyList.Find(iCam.Name + " ScreenFadeMat.Opacity"):
                lPropCamOpac = iCam.PropertyList.Find(iCam.Name + " ScreenFadeMat.Opacity")
                iCam.PropertyRemove(lPropCamOpac)    
            # Remvoe duplicate entries from our delete List                      
            lDeleteList = RS.Utils.Collections.RemoveDuplicatesFromList(lDeleteList)                                        
            # Delete our camera object data,
            for iDelete in lDeleteList:
                try:
                    iDelete.FBDelete()        
                except:
                    pass
            
            # DELETE BAD MOCAP OBJECTS - author: Blake Buck
            # Calling FBDelete() on an RTE camera with a HUD or MultiCameraViewerData object attached
            # causes MotionBuilder to crash on the next save. This fix works for now, but we should
            # probably have a more comprehensive script to clean up leftover/broken data from mocap.
            deletedObjects = []
            
            # Delete CamRockTab HUD Objects
            deletedObjects += [eachHud.FBDelete() for eachHud in FBSystem().Scene.HUDs if eachHud.Name.lower() == "hud"]
            
            # Delete MultiCameraViewerData User Objects
            deletedObjects += [eachUserObj.FBDelete() for eachUserObj in FBSystem().Scene.UserObjects if eachUserObj.Name.lower() == "multicameraviewerdata"]
            
            #Switch To Producer Perspective - needed after deleting MCVD objects
            if deletedObjects:
                FBSystem().Scene.Renderer.UseCameraSwitcher = False
                FBSystem().Scene.Renderer.CurrentCamera = FBSystem().Scene.Cameras[0]

    ###############################################################################################################
    ## Description: Create Delete List of cams with less than 5 planes or does not have the new DOF Far/Near properties
    ###############################################################################################################   
    def _UpdateInvalidCameras_(self, force =False):
        #rs_print("Deleting Invalid Camera Data")
        # Disabled as MB locks up

        lHideDOFList = []

        lSceneList = FBModelList()
        FBGetSelectedModels (lSceneList, None, False)
        FBGetSelectedModels (lSceneList, None, True)  

        # Hides all the planes for some unknown reason
        for iScene in lSceneList:
            if 'NearPlane' in iScene.Name:
                lHideDOFList.append(iScene)
            elif 'FarPlane' in iScene.Name:
                lHideDOFList.append(iScene)
            elif 'DOF_Strength' in iScene.Name:
                lHideDOFList.append(iScene)

        for iPlane in lHideDOFList:
            iPlane.Show = False

        IntrestsCams = []    
        # Can't believe python as no Case Switch, this will check what type of update the camea
        for iCam in glo.gCameras:
            if not iCam.SystemCamera:

                # check if the camera has an FBCamera as a child - if it does we dont want to run
                # the update on the parent or child camera, we will just skip to the next camera
                # in the list url:bugstar:2343356
                continueScript = True
                if isinstance(iCam.Parent, FBCamera):
                    continue
                for iChild in iCam.Children:
                    if isinstance(iChild, FBCamera):
                        FBMessageBox('R* Warning', "'{0}' has a camera model as a child ({1}).\n\n'{0}' will therefore not be updated, otherwise you will lose '{1}'.\n\nMoving on to the next camera to update...".format(iCam.LongName, iChild.LongName), 'Ok')
                        continueScript = False
                        break
                if not continueScript:
                    continue

                self.camFunctions.RemoveInvalidProperties(iCam)

                '''                
                nUpdateType                
                ignore = 0
                interest = 1
                all = 2
                constraints = 3
                properties = 4
                shaders = 5
                '''
                
                checkRepeatUpdate = False
                nUpdateType = None
                if force == True:
                    print int( self.camFunctions.GetCameraVersion( iCam ) ), iCam.LongName
                    if int(self.camFunctions.GetCameraVersion(iCam)) < 16:
                        print iCam.LongName + ': Did not force update, camera version is less than version 16, which is when cameras were introduced.\nCamera needs a full update.\n'
                        nUpdateType = 0
                    else:    
                        print 'Force Update Ran: ', iCam.LongName
                        if iCam.LongName == 'ExportCamera':
                            nUpdateType = 2
                        else:
                            nUpdateType = 3                
                else:
                    checkRepeatUpdate = True
                    nUpdateType = self.camFunctions.CamUpdateType(iCam)
                    #print iCam.LongName, nUpdateType
                    
                if nUpdateType == self.camFunctions.upDateCam.shaders:
                    #print "== Updating Plane Shaders =="
                    self._rebuildCamShaders(iCam)
                    
                if nUpdateType == self.camFunctions.upDateCam.Ignore:
                    #print "== No Update Required for Camera =="
                    None

                if nUpdateType == self.camFunctions.upDateCam.Interest:
                    #print "== Convert Camera to Intrest ==\n" 
                    IntrestsCams.append(iCam)
                    self.ConvertIntrestCam(iCam) # Full Update

                if nUpdateType == self.camFunctions.upDateCam.Constraints:
                    #print "== Constraint Update ==\n"
                    self._updateCamProperties(iCam)
                    self._rebuildConstraints(iCam)

                if nUpdateType == self.camFunctions.upDateCam.Properties:
                    #print "== Properties Update ==\n"
                    self.camFunctions.updateCameraProperties(iCam)

                if nUpdateType == self.camFunctions.upDateCam.All:
                    #print "== Update All ==\n"
                    self.DeleteCamera(iCam)
                    self._updateCamProperties(iCam)    
                    self._updateCamConstraints(iCam)

                # reset focus_markers to have a 0,0,0 lcl rotation
                for child in iCam.Children:
                    if 'Focus_Marker' in child.Name:
                        lFocusMarker = child
                        lFocusMarkerProp = lFocusMarker.PropertyList.Find( 'Lcl Rotation' )
                        if lFocusMarkerProp:
                            lFocusMarkerProp.Data = FBVector3d( 0, 0, 0 )
                            
                # if the new updateType isn't 0 (which is None), run the update again as there are still items needing updated
                newUpdateType = self.camFunctions.CamUpdateType(iCam)                
                if checkRepeatUpdate == True and newUpdateType != 0:
                    # print 'Multiple Updates Ran: ', iCam.LongName
                    camUpdate._UpdateInvalidCameras_()

        # Update all cams Frame Colors
        self._setCamFrameColors_()






        return




    """    
    ###############################################################################################################
    ## Description: Update our Invalid Cameras and add in a new Export Camera
    ###############################################################################################################      
    def UpdateInvalidCameras(self):
    FBSystem().Scene.Evaluate()

    print 'FARTS'
    #self.lFbp.ProgressBegin()
    #self.lFbp.Percent = 0 
    # Add in a new Export Camera
    self.camFunctions.CreateExportCustomCamera() 

    #totalCount = len(glo.gCameras)
    #increment = 0    
    for iCam in glo.gCameras:
        print iCam.Name
        if not iCam.SystemCamera:
        if self.camFunctions.isCameraLateral(iCam):
            None    
        #if iCam.Name == "ExportCamera":
        #    None
        else:
            if self.camFunctions.GetRequiresUpdate(iCam):
            rs_print("Updating Camera Proeprties: "+iCam.Name)
            #self.lFbp.Caption = ("Updating Camera: "+iCam.Name)
            #self.lFbp.Text = ("Updating Camera: "+iCam.Name)
            self._updateCamProperties(iCam)    
            self._updateCamConstraints(iCam)
        #increment+=1
        #self.lFbp.Percent = (100 * increment / totalCount)

    #self.lFbp.ProgressDone()    
    return
    """

    def CreateCustomCamera(self):
        lNewCamera = self.camFunctions.CreateCustomCamera()
        return lNewCamera

    def CreateExportCamera(self):
        lNewExportCamera = self.camFunctions.CreateExportCustomCamera()
        return lNewExportCamera

    def rs_removeEmptyConstraintFolders(self):
        lEmptyList = []

        for iFolder in glo.gFolders:
            if iFolder.Name == "Constraints":
                for iComp in iFolder.Components:
                    if iComp.ClassName() == "FBFolder":
                        if len(iComp.Components)== 0: # its a empty Folder
                            #print iComp.Name
                            lEmptyList.append(iComp)
                        else:
                            for subComp in iComp.Components:
                                if len(subComp.Components)== 0:
                                    print subComp.Name

        #for iFolderDelete in lEmptyList:   
        #iFolderDelete.FBDelete()    

        print  lEmptyList    
        return




    ###############################################################################################################
    ## Definition: Remove Any Cameras Not Present In The Switcher - For UI
    ###############################################################################################################
    def rs_RemoveNonSwitcherCameras(self):    
        lCameraList = []
        lValueList = []
        lSwitcherCameraList = []
        lRemoveCameraList = []

        lCurrentTake = FBSystem().CurrentTake
        lCurrentTake.SetCurrentLayer(0)

        for iCamera in glo.gCameras:
            if not iCamera.SystemCamera:
                lCameraList.append(iCamera)

        lCameraSwitcher = FBCameraSwitcher()
        lKeys = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys

        for iKey in lKeys:
            lSwitcher = lCameraList[int(iKey.Value)-1].Name
            lSwitcherCameraList.append(lSwitcher)

        for iCamera in lCameraList:
            if iCamera.Name in lSwitcherCameraList:
                rs_print(iCamera.Name + " is in Switcher")
            else:
                if not iCamera.Name == "ExportCamera":
                    lRemoveCameraList.append(iCamera)

        for iRemoveCamera in lRemoveCameraList:
            self.DeleteCamera(iRemoveCamera, RemoveCam=True)

        if not (self.bSilent):
            FBMessageBox("Remove Unused Cameras", "Cameras not in the Switcher have been deleted.", "OK")




    ###############################################################################################################
    ## Description: Definition - Update Cameras - For UI
    ###############################################################################################################
    '''
        Need a smart update!
        If camera has less then 4 planes then needs full update
        if Camera has less number of properties then add new propertes and rebuild COnstraint system
        if Camera has same number of proeprtes tehn we just update the property values
    '''
    def rs_updateCameras(self, bSilent = False, force=False):
        self.bSilent = bSilent

        tstart = time.clock()

        #Disabled as doesn't do anything
        #self.rs_removeEmptyConstraintFolders()

        # Delete our Export Camera Anyway
        camUpdate.deleteExportCamera()

        # Then we rebuild them again
        # Delete Invalid Cameras
        if force == True:
            camUpdate._UpdateInvalidCameras_( force=True )
        else:
            camUpdate._UpdateInvalidCameras_()
        

        self.camFunctions.CreateExportCustomCamera(silent=True) 
        # Rebuild our Cameras with the new planes/properties if we need to?
        rs_print("---------------------------------------------------------------------")
        #camUpdate.UpdateInvalidCameras()

        #rs_removeEmptyConstraintFolders()

        tend = time.clock()
        ttotal = (tend-tstart)/100
        rs_print("Finished, Process took %0.3f ms" % (ttotal))

        #self.lFbp.Caption = "Done"

        FBSystem().Scene.Evaluate() 

        if not (self.bSilent):
            FBMessageBox("Update Cams", "Update Complete.", "OK")

    # This will actually update 1 select camera
    def rs_updateSelectedCamera(self, iCam):    
        #camUpdate.updateCameraProperties() 
        # If export camera delete it, else delete cameaa objects
        # then update our camera objects

        #deleteExportCamera
        if not (self.bSilent):
            FBMessageBox("Update Cams", "Updated Camera Properties Complete.", "OK")




    ###################################################
    ## Description: Generic show/hide function for cameras
    ###################################################
    def rs_ShowHideCameraPlanes(self, Show=False,SelectedModels=False, HideScreenFade=True):
        lCameraPlaneList = [] 
        lModelSelectedList = FBModelList()
        if not SelectedModels:
            for iCamera in glo.rs_NonSystemCameraList():
                lModelSelectedList.append(iCamera)

        else:
            FBGetSelectedModels(lModelSelectedList, None, True)


        if len(lModelSelectedList) > 0:
            for iSelect in lModelSelectedList:
                if len(iSelect.Children) > 0:
                    for iChild in iSelect.Children:
                        lCameraPlaneList.append(iChild)

        if len(lCameraPlaneList) > 0:
            for iPlane in lCameraPlaneList:
                if iPlane.Name.endswith('ScreenFade') and HideScreenFade:
                    iPlane.Show = False            
                else:
                    # Visibility is not the same as Show, so make sure its turned on!
                    iPlane.PropertyList.Find("Visibility").Data = True
                    iPlane.Show = Show

        return

    def rs_DeSelectComponents(self):
        lModelList = FBModelList()
        FBGetSelectedModels (lModelList)
        for lModel in lModelList:
            lModel.Selected = False

        #for iCam in glo.gCameras:
        #    iCam.Selected = False

    def rs_switchViewtoCamera(self, pCamera):
        if pCamera:
            FBSystem().Scene.Renderer.UseCameraSwitcher = False     # Make sure we disable the camera switcher first
            FBApplication().SwitchViewerCamera( pCamera )


    """
    Select camera by name
    """
    def rs_SelectCameraByName(self, pModelName, switchView=False, showPlanes=True):
        camerasLocked = False
        planesLocked = False
        # Deselect the camera switcher ??
        #lModelList = FBComponentList()
        #FBFindObjectsByName("Camera Switcher", lModelList, False, False)
        #if len(lModelList) == 1:
        #    lModelList[0].Selected = False

        lModel = None
        self.toggletSwitcherCamera(False) # Deselect our camera switcher
        self.rs_ShowHideCameraPlanes(False,False, True)

        # Just getting what are lock/unlock status are from the Scene:REFERENCE object are so I can work with them later
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            # Only need to do the Camera here and not the planes, because they are not automatically selected from the UI.        
            lCamProp = lReferenceNull.PropertyList.Find('LockCamera')
            if lCamProp:
                if lCamProp.Data == True:
                    camerasLocked = True        

        for iCam in glo.gCameras:    
            if iCam.Name == pModelName:
                iCam.Selected = True                        
                lModel = iCam
                if switchView == True:
                    self.rs_switchViewtoCamera(lModel)    
                break

        self.rs_ShowHideCameraPlanes(showPlanes,True, True)
        #If you select the row in the UI, this automatically selects the camera, so this de-selects the camera, if you do it earlier in this function it mess with Camera Planes showing.
        if camerasLocked:
            lCam = FBFindModelByLabelName(pModelName)
            lCam.Selected = False
        return lModel #  Return our found Model

    # Select the switcher Camera
    def toggletSwitcherCamera(self,bSelect):
        # Unselect Selected Cameras
        self.rs_DeSelectComponents()
        lModelList = FBComponentList()
        FBFindObjectsByName("Camera Switcher", lModelList, False, False)
        if len(lModelList) == 1:
            lModelList[0].Selected = bSelect
            # Opens Camera Switcher Settings in the Navigator if the selection is True
            if bSelect:
                lModelList[0].HardSelect()

    # Select the switcher Camera
    def getSwitcherCamera(self):
        # Unselect Selected Cameras
        self.rs_DeSelectComponents()
        lModelList = FBComponentList()
        FBFindObjectsByName("Camera Switcher", lModelList, False, False)
        if len(lModelList) == 1:
            return lModelList[0]

    # Simple fucntion to just hide all the models and show just the cameras
    def showOnlyCameras(self):
        lSceneList = FBModelList()
        FBGetSelectedModels (lSceneList, None, False)
        FBGetSelectedModels (lSceneList, None, True) 
        for iScene in lSceneList:
            if iScene.ClassName() == "FBModel":
                iScene.Show = False
            if iScene.ClassName() == "FBCamera":
                iScene.Show = True

    def lockCameras(self, pControl, pEvent):

        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            lCamProp = lReferenceNull.PropertyList.Find('LockCamera')
            if lCamProp == None:
                lCamProp = lReferenceNull.PropertyCreate('LockCamera', FBPropertyType.kFBPT_bool, "Bool", True, True, None)     
                if lCamProp:
                    lCamProp.Data = False           

        # No REFERENCE:Scene in the scene so let's create one so our lock tool can work, 
        # this shouldn't affect things later when you use the Ref Sys as there would already be a REFERENCE:Scene    
        elif lReferenceNull == None:
            lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", RS.Core.Reference.Manager.GetReferenceSceneNullName())             
            lCamProp = lReferenceNull.PropertyCreate('LockCamera', FBPropertyType.kFBPT_bool, "Bool", True, True, None)     
            if lCamProp:
                lCamProp.Data = False           

        # Let's lock/unlock the camera
        for iCamera in glo.gCameras:
            lPickable = iCamera.PropertyList.Find('Pickable')
            if lPickable:
                if pControl.Caption == 'All Cameras are Unlocked':         
                    if lPickable.Data:        
                        lPickable.Data = False
                else:
                    if lPickable.Data == False:
                        lPickable.Data = True    

        if pControl.Caption == 'All Cameras are Unlocked': 

            # Setting the property on the Scene:REFERENCE
            if lCamProp:
                if lCamProp.Data == False:
                    lCamProp.Data = True    

            # Workaround for button colours
            pControl.SetStateColor(FBButtonState.kFBButtonState0, self.RedButton)
            pControl.SetStateColor(FBButtonState.kFBButtonState1, self.RedButton)            
            # Set the button text to reflect the locked status
            pControl.Caption = 'All Cameras are Locked'
        else:

            # Setting the property on the Scene:REFERENCE        
            if lCamProp:
                if lCamProp.Data == True:
                    lCamProp.Data = False            

            # Workaround for button colours        
            pControl.SetStateColor(FBButtonState.kFBButtonState0, self.GreyButton)
            pControl.SetStateColor(FBButtonState.kFBButtonState1, self.GreyButton)              
            # Set the button text to reflect the unlocked status        
            pControl.Caption = 'All Cameras are Unlocked'

    def lockPlanes(self, pControl, pEvent):

        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            lPlaneProp = lReferenceNull.PropertyList.Find('LockPlane')
            if lPlaneProp == None:
                lPlaneProp = lReferenceNull.PropertyCreate('LockPlane', FBPropertyType.kFBPT_bool, "Bool", True, True, None)     
                if lPlaneProp:
                    lPlaneProp.Data = False            


        # No REFERENCE:Scene in the scene so let's create one so our lock tool can work, 
        # this shouldn't affect things later when you use the Ref Sys as there would already be a REFERENCE:Scene    
        elif lReferenceNull == None:
            lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", RS.Core.Reference.Manager.GetReferenceSceneNullName())             
            lPlaneProp = lReferenceNull.PropertyCreate('LockPlane', FBPropertyType.kFBPT_bool, "Bool", True, True, None)     
            if lPlaneProp:
                lPlaneProp.Data = False    


        # Let's lock/unlock the camera planes
        for iCamera in glo.gCameras: # Go through each camera
            for iChild in iCamera.Children: # Go through each plane
                lPlanePickable = iChild.PropertyList.Find('Pickable')
                if lPlanePickable:
                    if pControl.Caption == 'All Camera Planes are Unlocked':         
                        if lPlanePickable.Data:        
                            lPlanePickable.Data = False
                    else:
                        if lPlanePickable.Data == False:

                            lPlanePickable.Data = True    
        # Setting the property on the Scene:REFERENCE    
        if pControl.Caption == 'All Camera Planes are Unlocked': 
            if lPlaneProp:
                if lPlaneProp.Data == False:
                    lPlaneProp.Data = True    

            # Workaround for button colours
            pControl.SetStateColor(FBButtonState.kFBButtonState0, self.RedButton)
            pControl.SetStateColor(FBButtonState.kFBButtonState1, self.RedButton)            
            # Set the button text to reflect the locked status
            pControl.Caption = 'All Camera Planes are Locked'

        # Setting the property on the Scene:REFERENCE    
        else:

            if lPlaneProp:
                if lPlaneProp.Data == True:
                    lPlaneProp.Data = False            

            # Workaround for button colours        
            pControl.SetStateColor(FBButtonState.kFBButtonState0, self.GreyButton)
            pControl.SetStateColor(FBButtonState.kFBButtonState1, self.GreyButton)              
            # Set the button text to reflect the unlocked status        
            pControl.Caption = 'All Camera Planes are Unlocked'        

    def setCameraLock(self, state):
        """
        New Style method for locking or unlocking cameras
        """
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            lCamProp = lReferenceNull.PropertyList.Find('LockCamera')
            if lCamProp == None:
                lCamProp = lReferenceNull.PropertyCreate('LockCamera', FBPropertyType.kFBPT_bool, "Bool", True, True, None)
                if lCamProp:
                    lCamProp.Data = False

        # No REFERENCE:Scene in the scene so let's create one so our lock tool can work, 
        # this shouldn't affect things later when you use the Ref Sys as there would already be a REFERENCE:Scene
        elif lReferenceNull == None:
            lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", RS.Core.Reference.Manager.GetReferenceSceneNullName())
            lCamProp = lReferenceNull.PropertyCreate('LockCamera', FBPropertyType.kFBPT_bool, "Bool", True, True, None)
            if lCamProp:
                lCamProp.Data = False

        # Let's lock/unlock the camera
        for iCamera in glo.gCameras:
            lPickable = iCamera.PropertyList.Find('Pickable')
            if lPickable:
                lPickable.Data = not state

        # Setting the property on the Scene:REFERENCE
        if lCamProp:
            lCamProp.Data = not state

    def setPlanesLock(self, state):
        """
        New Style method for locking or unlocking Planes
        """
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lReferenceNull:
            lPlaneProp = lReferenceNull.PropertyList.Find('LockPlane')
            if lPlaneProp == None:
                lPlaneProp = lReferenceNull.PropertyCreate('LockPlane', FBPropertyType.kFBPT_bool, "Bool", True, True, None)
                if lPlaneProp:
                    lPlaneProp.Data = False


        # No REFERENCE:Scene in the scene so let's create one so our lock tool can work,
        # this shouldn't affect things later when you use the Ref Sys as there would already be a REFERENCE:Scene
        elif lReferenceNull == None:
            lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", RS.Core.Reference.Manager.GetReferenceSceneNullName())
            lPlaneProp = lReferenceNull.PropertyCreate('LockPlane', FBPropertyType.kFBPT_bool, "Bool", True, True, None)
            if lPlaneProp:
                lPlaneProp.Data = False

        # Let's lock/unlock the camera planes
        for iCamera in glo.gCameras: # Go through each camera
            for iChild in iCamera.Children: # Go through each plane
                lPlanePickable = iChild.PropertyList.Find('Pickable')
                if lPlanePickable:
                    if not state:
                        if lPlanePickable.Data:
                            lPlanePickable.Data = False
                    else:
                        if lPlanePickable.Data == False:

                            lPlanePickable.Data = True
        # Setting the property on the Scene:REFERENCE
        if not state: 
            if lPlaneProp:
                if lPlaneProp.Data == False:
                    lPlaneProp.Data = True

        # Setting the property on the Scene:REFERENCE
        else:
            if lPlaneProp:
                if lPlaneProp.Data == True:
                    lPlaneProp.Data = False


camUpdate = _camUpdate()
#lExportCam = FBFindModelByLabelName("SlateCam")
#camUpdate.ConvertIntrestCam(lExportCam)