###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: CameraRelocation.py
## Written And Maintained By: Kristine Middemiss
## Contributors:
## Description: This is a tool to relocate cameras from one stage to another, this is used with MoCap stages.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
from RS.Utils.Scene import Constraint

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Align Function
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Align( pModel, pAlignTo, pAlignTransX, pAlignTransY, pAlignTransZ, pAlignRotX, pAlignRotY, pAlignRotZ):

   lAlignTransPos = FBVector3d();
   lModelTransPos = FBVector3d();
   lAlignRotPos = FBVector3d();
   lModelRotPos = FBVector3d();   
   
   pAlignTo.GetVector(lAlignTransPos)
   pModel.GetVector(lModelTransPos)

   pAlignTo.GetVector(lAlignRotPos, FBModelTransformationType.kModelRotation )
   pModel.GetVector(lModelRotPos, FBModelTransformationType.kModelRotation)
   
   if pAlignTransX:
     lModelTransPos[0] = lAlignTransPos[0]
   if pAlignTransY:
     lModelTransPos[1] = lAlignTransPos[1]
   if pAlignTransZ:
     lModelTransPos[2] = lAlignTransPos[2]

   if pAlignRotX:
     lModelRotPos[0] = lAlignRotPos[0]
   if pAlignRotY:
     lModelRotPos[1] = lAlignRotPos[1]
   if pAlignRotZ:
     lModelRotPos[2] = lAlignRotPos[2]        
     
   pModel.SetVector(lModelTransPos)
   pModel.SetVector(lModelRotPos, FBModelTransformationType.kModelRotation)
 
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This is the function that will relocate the desired camera
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CameraRelocation(pControl, pEvent):
    
    # UI Prep Work to make sure everything is filled in correctly.
    if len(pControl.CameraListControl.Items) == 0:
        FBMessageBox("rs_CameraRelocation", "You have no cameras in this scene, how will you relocate them?!?!?", "OK")
        return
    
    elif len(pControl.CurrentStageControl.Items) == 0 or len(pControl.NewStageControl.Items) == 0:
        FBMessageBox("rs_CameraRelocation", "You need to fill in both the Current Stage and New Stage containers.", "OK")
        return    
        
    lCameraRelocateList = []

    for lCam in enumerate(pControl.CameraListControl.Items):
        if pControl.CameraListControl.IsSelected(lCam[0]):
            lCameraRelocateList.append(lCam[1])         
    if len(lCameraRelocateList) == 0:
        FBMessageBox("rs_CameraRelocation", "No cameras were selected.", "OK")
        return   
        
    # Start to Perform Camera Relocation
    lCurrentStage = FBFindModelByLabelName(pControl.CurrentStageControl.Items[0])        
    lNewStage = FBFindModelByLabelName(pControl.NewStageControl.Items[0])               
    
    if lCurrentStage and lNewStage:            
        
        # Make sure nothing is selected for plotting later
        lModelList = FBModelList()
        FBGetSelectedModels (lModelList)
        for lModel in lModelList:
                lModel.Selected = False
        
        # Create Master Null
        lMasterNull = FBModelNull("NullMaster")
        lMasterNull.Show = True 
        lMasterNull.Size = 1000        
        lMgr = FBConstraintManager()  
        lScene = FBSystem().Scene
        
        # Match up the NullMaster to the center of the current plane (Align > All)
        # This will align translation and rotation of the first one to second one in global space
        Align( lMasterNull, lCurrentStage, True, True, True, True, True, True)        
        
        # Create an array to store the nulls to easily delete them later
        lNullArray = []
        lConstraintArray = []
        lCurrentTake = FBSystem().CurrentTake
        lPlotPeriod = FBTime(0, 0, 0, 1)
        
        # Create child Nulls for Each Camera you would like to re-locate
        for lCamera in lCameraRelocateList:
            lCameraHandle = FBFindModelByLabelName(lCamera)
            
            if lCameraHandle:

                # Create Children Nulls for each camera
                lNull = FBModelNull("NullChild"+lCamera)
                lNull.Show = True                
                lNull.Size = 1000  
                lNullArray.append(lNull)
                
                # Set the Null translation to match the camera 
                # This will align translation of the first one to second one in global space                
                Align( lNull, lCameraHandle, True, True, True, True, True, True)        
                
                #Parent the child nulls to the masternull
                lNull.Parent = lMasterNull
                
                # Create Parent / Child constraint for each null and Camera
                lParentChildConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
                             
                lParentChildConstraint.Name = "Null_" +lCamera   
                # Constrainte Object - Child (null)
                lParentChildConstraint.ReferenceAdd (0,lNull)
                # Source Object - Parent (camera)
                lParentChildConstraint.ReferenceAdd (1,lCameraHandle)            
                lParentChildConstraint.Active = True     

                # Plot the selected nulls        
                lNull.Selected = True 
                lCurrentTake.PlotTakeOnSelected(lPlotPeriod)
                lNull.Selected = False      

                # Reverse the constraints parent child order reversed so the cameras are now driven by the nulls
                # Remove child(null) and parent(camera)
                lParentChildConstraint.ReferenceRemove (0,lNull)
                lParentChildConstraint.ReferenceRemove (1,lCameraHandle)
                # Swap Constrainte Object - add Child (camera)
                lParentChildConstraint.ReferenceAdd (0,lCameraHandle)
                # Swap Source Object - add parent (null)                
                lParentChildConstraint.ReferenceAdd (1,lNull) 
                                           
        # Now we need to move the whole setup to the new stage, so we snap the Master null of the current stage to the new stage
        Align( lMasterNull, lNewStage, True, True, True, True, True, True)  
        
        for lCamera in lCameraRelocateList:
            lCameraHandle = FBFindModelByLabelName(lCamera)
            if lCameraHandle:
                # Plot Cameras
                FBSystem().Scene.Evaluate() # I had to add this in, it was skipping the first camera 
                lCameraHandle.Selected = True    
                lCurrentTake.PlotTakeOnSelected(lPlotPeriod)     
                lCameraHandle.Selected = False   
                FBSystem().Scene.Evaluate()                      

        # Remove References from constraints - they weren't deleting properly if I didn't do this.     
        lConstraints = FBSystem().Scene.Constraints
        for lConstraint in lConstraints:
            if lConstraint.Name.startswith("Null_"):
                FBSystem().Scene.Evaluate()
                lChildModel = lConstraint.ReferenceGet (0)
                lConstraint.ReferenceRemove (0,lChildModel)
                lParentModel = lConstraint.ReferenceGet (1)
                lConstraint.ReferenceRemove (1,lParentModel)                                
                lConstraintArray.append(lConstraint) 
                
        for lCon in lConstraintArray:
            lCon.FBDelete()               
                            
        # Delete camera nulls
        for lNull in lNullArray:
            lNull.FBDelete()

        # Delete master null
        lMasterNull.FBDelete()           
          
        # Yay we finished.
        FBMessageBox("rs_CameraRelocation", "The selected camera's have been relocated.", "Ok")             
    
      
        