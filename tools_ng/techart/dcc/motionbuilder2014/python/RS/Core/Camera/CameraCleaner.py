from pyfbsdk import *

class FixCamNames():
    def __init__(self):
        self.standardModelNames = ['ScreenFade', 'FarPlane', 'NearPlane',
        'DOF_Strength_FarPlane', 'DOF_Strength_NearPlane', 'Focus_Marker']
        self.standardConstraintNames = ['Relation']
        self.standardMaterialNames = ['ScreenFadeMat', 'FarPlane', 'NearPlane', 'DOF_Strength_FarPlane', 'DOF_Strength_NearPlane']
        self.standardShaderNames = ['ScreenFadeShader', 'Transparency_Planes']
        
        self.cameraList = []
        self.modelNameDict = {}
        self.constraintNameDict = {}
        self.materialNameDict = {}
        self.shaderNameDict = {}
        
        self.badModelNames = []
        self.goodModelNames = []
        self.badConstraintNames = []
        self.goodConstraintNames = []
        self.badMaterialNames = []
        self.goodMaterialNames = []
        self.badShaderNames = []
        self.goodShaderNames = []
        
        self.main()
        
    def createCameraList(self):
        lSystem = FBSystem()
        #lSystem.CurrentTake.SetCurrentLayer(0)
        lScene = lSystem.Scene
        for camera in lScene.Cameras:
            self.cameraList.append(camera)
        self.cameraList = self.cameraList[7:]
        
    
    def validateName(self, itemName, camName, standardNameList, nameDict):        
        #CHECK ITEM FOR STANDARD NAMES - Only proceed if one is found.
        for eachStandardName in standardNameList:
            if eachStandardName in itemName:
                
                startText = itemName.split(eachStandardName)[0]
                endText = itemName.split(eachStandardName)[-1]
                
                #DETECT TRUNCATED TEXT - So we don't use shortened names for longer ones, such as FarPlane and again for DOF_Strength_Far_Plane.
                if (eachStandardName == "FarPlane" or "NearPlane") and (startText[-1] == "_"):
                    continue
                if endText != "":
                    if (eachStandardName == "ScreenFade") and (endText[0] == "S" or "M"):
                        continue
                    
                #CHECK ENDING - Make sure there's no extra ' 1' stuff at the end.
                if (camName not in itemName) or (endText != ""):
                    badName = itemName
                    if eachStandardName == "Focus_Marker":
                        goodName = camName + eachStandardName
                    else:
                        goodName = camName + " " + eachStandardName
                    nameDict[goodName] = badName
                    
    def checkCamsForNameIssues(self):
        #FLUSH NAME DICTS - For when we run this a second time to verify fixes at the end.
        self.modelNameDict = {}
        self.constraintNameDict = {}
        self.materialNameDict = {}
        self.shaderNameDict = {}
        
        #ONLY CONTINUE IF CAMS ARE FOUND
        if len(self.cameraList) != 0:
            for eachCam in self.cameraList:
                
                #CHECK MODEL AND CONSTRAINT NAMES
                camComponents = eachCam.Components
                if len(camComponents) != 0:
                    for eachComponent in camComponents:
                        self.validateName(eachComponent.Name, eachCam.Name, self.standardConstraintNames, self.constraintNameDict)
                        self.validateName(eachComponent.Name, eachCam.Name, self.standardModelNames, self.modelNameDict)
                        
                        #CHECK MATERIAL NAMES
                        try:
                            childMaterials = eachComponent.Materials
                        except AttributeError:
                            childMaterials = []
                        if len(childMaterials) != 0:
                            for eachMat in childMaterials:
                                self.validateName(eachMat.Name, eachCam.Name, self.standardMaterialNames, self.materialNameDict)
                        
                        #CHECK SHADER NAMES
                        try:
                            childShaders = eachComponent.Shaders
                        except AttributeError:
                            childShaders = []
                        if len(childShaders) != 0:
                            for eachShader in childShaders:
                                self.validateName(eachShader.Name, eachCam.Name, self.standardShaderNames, self.shaderNameDict)
    
    def repairCamObjNames(self):
        lSystem = FBSystem()
        #lSystem.CurrentTake.SetCurrentLayer(0)
        lScene = lSystem.Scene
        
        nameDictList = [ self.materialNameDict, self.shaderNameDict, self.constraintNameDict, self.modelNameDict ]
        lastDictIndex = None
        for eachNameDict in nameDictList:
            if len(eachNameDict) != 0:
                #DETECT DICT TYPE
                currentDictIndex = nameDictList.index(eachNameDict)
                if currentDictIndex != lastDictIndex:
                    lastDictIndex = currentDictIndex
                    
                    #SETUP MATERIAL SCENE DICT
                    if currentDictIndex == 0:
                        sceneObjDict = {}
                        for eachMaterial in lScene.Materials:
                            sceneObjDict[eachMaterial.Name] = eachMaterial
                    
                    #SETUP SHADER SCENE DICT
                    elif currentDictIndex == 1:
                        sceneObjDict = {}
                        for eachShader in lScene.Shaders:
                            sceneObjDict[eachShader.Name] = eachShader
                            
                    #SETUP CONSTRAINT SCENE DICT
                    elif currentDictIndex == 2:
                        sceneObjDict = {}
                        for eachConstraint in lScene.Constraints:
                            sceneObjDict[eachConstraint.Name] = eachConstraint
                            
                    #SETUP MODEL SCENE DICT
                    elif currentDictIndex == 3:
                        sceneObjDict = {}
                        modelObjList = [c for c in lScene.Components if isinstance(c, FBModel)]
                        for eachModelObj in modelObjList:
                            if (eachModelObj.Name in self.modelNameDict.keys()) and (eachModelObj.Name not in sceneObjDict.keys()):
                                sceneObjDict[eachModelObj.Name] = eachModelObj
                            if (eachModelObj.Name in self.modelNameDict.values()) and (eachModelObj.Name not in sceneObjDict.keys()):
                                sceneObjDict[eachModelObj.Name] = eachModelObj
                    else:
                        continue
                
                #GO THROUGH EACH ITEM IN OUR NAME DICTS
                renameDict = {}
                for eachItemName in eachNameDict.keys():
                    goodItemName = eachItemName
                    badItemName = eachNameDict[eachItemName]
                    
                    #CHECK RENAME DICT - To see if our bad name has been renamed previously (because of blocking issues).
                    if badItemName in renameDict.keys():
                        badItemName = renameDict[badItemName]
                    brokenItemObj = sceneObjDict[badItemName]
                    
                    #OBJECT BLOCKING RENAME DETECTED - We must either rename or delete it first
                    if goodItemName  in sceneObjDict.keys():
                            blockingItemObj = sceneObjDict[goodItemName]
                            
                            #RENAME TO TEMP - If blocker is on another cam.
                            if goodItemName in eachNameDict.values():
                                blockingItemObj.Name = "Temp"
                                
                                #Update our scene dict and add to the rename dict.
                                renameDict[goodItemName] = blockingItemObj.Name
                                sceneObjDict[blockingItemObj.Name] = blockingItemObj
                                del sceneObjDict[goodItemName]
                                
                            #DELETE OBJECT COMPLETELY - If it's just extra and not used on another cam.
                            else:
                                blockingItemObj.FBDelete()
                                del sceneObjDict[goodItemName]
                        
                    #RENAME ORIGINAL BROKEN OBJECT - Now that correct name is available.
                    brokenItemObj.Name = goodItemName
                    del sceneObjDict[badItemName]
                    sceneObjDict[brokenItemObj.Name] = brokenItemObj
        
    def main(self):
        self.createCameraList()
        self.checkCamsForNameIssues()
        nameIssueTotal = len(self.materialNameDict) + len(self.shaderNameDict) + len(self.modelNameDict) + len(self.constraintNameDict)
        
        #ONLY CONTINUE IF ISSUES ARE FOUND
        if nameIssueTotal > 0:
            messageText = str(nameIssueTotal) + " cam name issues were detected in this FBX. Do you want to repair them?"
            if FBMessageBox( "Camera Cleaner", messageText, "Yes", "No" ) == 1:
                    
                    #ONLY CONTINUE IF USER HITS YES
                    self.repairCamObjNames()
                    self.checkCamsForNameIssues()
                    remainingIssueTotal = len(self.materialNameDict) + len(self.shaderNameDict) + len(self.modelNameDict) + len(self.constraintNameDict)
                    
                    #LOOP TO RE-RUN THE SCRIPT - Only continues if subsequent runs are actually fixing issues.
                    while remainingIssueTotal > 0:
                        if remainingIssueTotal >= nameIssueTotal:
                            break
                        messageText = "The repair script fixed some issues, but  " + str(remainingIssueTotal) + " issues still remain.  Run repair script again?"
                        if FBMessageBox( "Camera Cleaner", messageText, "Yes", "No" ) == 1:
                            self.repairCamObjNames()
                            self.checkCamsForNameIssues()
                            nameIssueTotal = remainingIssueTotal #Reset nameIssueTotal, so we break out if the script isn't fixing things.
                            remainingIssueTotal = len(self.materialNameDict) + len(self.shaderNameDict) + len(self.modelNameDict) + len(self.constraintNameDict)
                        else:
                            break #Or break out if the user hits no
                            
                    #FINAL STATUS CHECK - Tells the user what happened.
                    if remainingIssueTotal == 0:
                        FBMessageBox( "Camera Cleaner", "All cam name issues have been fixed.", "Okay")
                    else:
                        messageText = "The repair script ran, but failed to fix all issues. " + str(nameIssueTotal) + " cam name issues remain."
                        FBMessageBox( "Camera Cleaner", messageText, "Damn.")

def Run():
    _fixCamObj_ = FixCamNames()

#Run()