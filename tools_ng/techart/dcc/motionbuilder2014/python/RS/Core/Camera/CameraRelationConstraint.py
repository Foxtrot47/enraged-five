'''

 Script Path: RS/Core/Camera/CameraRelationConstraint.py

 Written And Maintained By: Kathryn Bodey

 Created: 7 February 2014

 Description: New constraint setup for the Cameras.
              Keeping in a separate script as there is a lot here, should hopefully keep the Lib.py script tidier and
              easier to read.

'''


from pyfbsdk import *
import RS.Utils.Scene


def CameraConstraintSetup(pCamera):


    ########################################################################################
    # Constraint Folder
    ########################################################################################

    def Macro_RelationFolder():

        folder = None

        for i in RS.Globals.gFolders:
            macroFolderProp = i.PropertyList.Find('camera_macro_relation')

            if macroFolderProp:
                folder = i

        if folder == None:

            placeholder = FBConstraintRelation('Remove_Me')
            folder = FBFolder("Camera_Macros", placeholder)

            macroTag = folder.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
            macroTag.Data = "macroFolder"
            lTag = folder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
            lTag.Data = "rs_Folders"


            FBSystem().Scene.Evaluate()

            placeholder.FBDelete()

        return folder

    ########################################################################################
    # Defs for Relation Boxes
    ########################################################################################

    def MacroBox(relation, macroType, xPos, yPos, name=None):
        macroBox = relation.CreateFunctionBox('Macro Tools', macroType)
        relation.SetBoxPosition(macroBox, xPos, yPos)
        outNode = RS.Utils.Scene.FindAnimationNode(macroBox.AnimationNodeOutGet(), 'Input')
        if outNode:
            outNode.Label = name
        inNode = RS.Utils.Scene.FindAnimationNode(macroBox.AnimationNodeInGet(), 'Output')
        if inNode:
            inNode.Label = name
        return macroBox

    def SourceBox(relation, source, globalBool, xPos, yPos):
        sourceBox = relation.SetAsSource(source)
        sourceBox.UseGlobalTransforms = globalBool
        relation.SetBoxPosition(sourceBox , xPos, yPos)
        return sourceBox

    def TargetBox(relation, target, globalBool, xPos, yPos, writeDataLclRotX=None, writeDataLclRotY=None, writeDataLclRotZ=None):
        targetBox = relation.ConstrainObject(target)
        targetBox.UseGlobalTransforms = globalBool
        relation.SetBoxPosition(targetBox , xPos, yPos)
        if writeDataLclRotX != None and writeDataLclRotY != None and writeDataLclRotZ != None:
            dataLclRot = RS.Utils.Scene.FindAnimationNode(targetBox.AnimationNodeInGet(), 'Lcl Rotation')
            dataLclRot.WriteData([writeDataLclRotX, writeDataLclRotY, writeDataLclRotZ])
        return targetBox

    def ConverterBox(relation, converterType, xPos, yPos, writeDataX=None, writeDataY=None, writeDataZ=None):
        converterBox = relation.CreateFunctionBox('Converters', converterType)
        relation.SetBoxPosition(converterBox , xPos , yPos)
        if writeDataX != None:
            dataX = RS.Utils.Scene.FindAnimationNode(converterBox.AnimationNodeInGet(), 'X')
            dataX.WriteData([writeDataX])
        if writeDataY != None:
            dataY = RS.Utils.Scene.FindAnimationNode(converterBox.AnimationNodeInGet(), 'Y')
            dataY.WriteData([writeDataY])
        if writeDataZ != None:
            dataZ = RS.Utils.Scene.FindAnimationNode(converterBox.AnimationNodeInGet(), 'Z')
            dataZ.WriteData([writeDataZ])
        return converterBox

    def NumberBox(relation, numberType, xPos, yPos, writeDataA=None, writeDataB=None, writeDataC=None):
        numberBox = relation.CreateFunctionBox('Number', numberType)
        relation.SetBoxPosition(numberBox, xPos, yPos)
        if writeDataA != None:
            dataA = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'a')
            dataA.WriteData([writeDataA])
        if writeDataB != None:
            dataB = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'b')
            dataB.WriteData([writeDataB])
        if writeDataC != None:
            dataC = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'c')
            dataC.WriteData([writeDataC])
        return numberBox

    def OtherBox(relation, otherType, xPos, yPos):
        otherBox = relation.CreateFunctionBox('Other', otherType)
        relation.SetBoxPosition(otherBox, xPos, yPos)
        return otherBox

    def MyMacro(relation, macroType, xPos, yPos):
        myMacroBox = relation.CreateFunctionBox('My Macros', macroType)
        relation.SetBoxPosition(myMacroBox, xPos, yPos)
        return myMacroBox

    def RelationFCurveKeys(box, dictionary):

        valueEdit = RS.Utils.Scene.FindAnimationNode(box.AnimationNodeOutGet(), 'Value')

        curve = valueEdit.FCurve

        curve.EditBegin()
        for frame, value in dictionary.iteritems():

            # from MB13 FBTime no longer takes a float for the frame.  We had to work out the 'long' for 1 frame, then multiply that
            # by our desired frame, to be able to pass through the FBTime(long) to set the keys to the exact frames needed
            longFrameValue = long(1539538600 * frame)
            myKey = curve.KeyAdd(FBTime(longFrameValue), value[0])

            for key in curve.Keys:

                if round(key.Value, 2) == value[0]:

                    if value[1] != None:
                        key.Interpolation = value[1]

                    if value[2] != None:
                        key.FBTangentMode = value[2]
        curve.EditEnd()


    ##########################################################################################
    # Macro Relation Check
    ##########################################################################################

    macroDict = { 'Hyperfocal':(False, None),
                  'Near_Outer_Plane':(False, None),
                  'Near_Inner_Plane':(False, None),
                  'Far_Inner_Plane':(False, None),
                  'Far_Outer_Plane':(False, None),
                  'Lens_CoC_Focal_length':(False, None) }

    macroList = []

    # Check if the macro relations exist.
    for i in RS.Globals.gConstraints:

        macroProp = i.PropertyList.Find('camera_macro_relation')

        if macroProp:
            macroList.append(i)

    for key, value in macroDict.iteritems():

        for relCon in macroList:

            if relCon.Name == key:
                macroDict[ key ] = (True, relCon)


    ##########################################################################################
    # Macro Relation Constraints
    ##########################################################################################

    # only need to create the macro relations which are missing from the scene

    for key, value in macroDict.iteritems():

        # Hyperfocal Macro
        if key == 'Hyperfocal':
            if value[0] == False:

                hyperFocalRelation = FBConstraintRelation("Hyperfocal")
                hyperFocalRelation.Active = True
                lTag = hyperFocalRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = hyperFocalRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Hyperfocal'

                hyperFocalMacroIn = MacroBox(hyperFocalRelation, 'Macro Input Number', 0, 500, 'Focal_Length')
                hyperFocalMulti = NumberBox(hyperFocalRelation, 'Multiply (a x b)', 400, 400)
                hyperFocalMulti2 = NumberBox(hyperFocalRelation, 'Multiply (a x b)', 400, 600, 2.8, 0.03)
                hyperFocalDiv = NumberBox(hyperFocalRelation, 'Divide (a/b)', 800, 500)
                hyperFocalAdd = NumberBox(hyperFocalRelation, 'Add (a + b)', 1100, 500)
                hyperFocalMacroOut = MacroBox(hyperFocalRelation, 'Macro Output Number', 1400, 500, 'Hyperfocal')

                RS.Utils.Scene.ConnectBox(hyperFocalMacroIn, 'Input', hyperFocalMulti, 'a')
                RS.Utils.Scene.ConnectBox(hyperFocalMacroIn, 'Input', hyperFocalMulti, 'b')
                RS.Utils.Scene.ConnectBox(hyperFocalMulti, 'Result', hyperFocalDiv, 'a')
                RS.Utils.Scene.ConnectBox(hyperFocalMulti2, 'Result', hyperFocalDiv, 'b')
                RS.Utils.Scene.ConnectBox(hyperFocalDiv, 'Result', hyperFocalAdd, 'a')
                RS.Utils.Scene.ConnectBox(hyperFocalMacroIn, 'Input', hyperFocalAdd, 'b')
                RS.Utils.Scene.ConnectBox(hyperFocalAdd, 'Result', hyperFocalMacroOut, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(hyperFocalRelation)

            else:
                hyperFocalRelation = value[1]

        # Near_Outer_Plane Macro
        if key == 'Near_Outer_Plane':
            if value[0] == False:

                nearOuterPlaneRelation = FBConstraintRelation("Near_Outer_Plane")
                nearOuterPlaneRelation.Active = True
                lTag = nearOuterPlaneRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = nearOuterPlaneRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Near_Outer_Plane'

                nearOuterPlaneMacroIn = MacroBox(nearOuterPlaneRelation, 'Macro Input Number', 0, 600, 'Near_Outer_Override')
                nearOuterPlaneDiv = NumberBox(nearOuterPlaneRelation, 'Divide (a/b)', 500, 0, None, 100)
                nearOuterPlaneIsLess = NumberBox(nearOuterPlaneRelation, 'Is Less (a < b)', 500, 100, None, 0)
                nearOuterPlaneDistance = NumberBox(nearOuterPlaneRelation, 'Distance Numbers', 500, 200)
                nearOuterPlaneIsGreater = NumberBox(nearOuterPlaneRelation, 'is Greater (a > b)', 500, 300, None, 0)
                nearOuterPlaneIfCondElse = NumberBox(nearOuterPlaneRelation, 'IF Cond Then A Else B', 900, 0, None, 0)
                nearOuterPlaneDiv2 = NumberBox(nearOuterPlaneRelation, 'Divide (a/b)', 900, 150, None, 100)
                nearOuterPlaneMulti = NumberBox(nearOuterPlaneRelation, 'Multiply (a x b)', 1300, 0)
                nearOuterPlaneMulti2 = NumberBox(nearOuterPlaneRelation, 'Multiply (a x b)', 1300, 100, None, -100)
                nearOuterPlaneIfCondElse2 = NumberBox(nearOuterPlaneRelation, 'IF Cond Then A Else B', 1300, 200, None, 0)
                nearOuterPlaneMacroIn2 = MacroBox(nearOuterPlaneRelation, 'Macro Input Number', 1300, 500, 'Near_Inner_True_Pos')
                nearOuterPlaneSub = NumberBox(nearOuterPlaneRelation, 'Subtract (a - b)', 1700, 0)
                nearOuterPlaneMulti3 = NumberBox(nearOuterPlaneRelation, 'Multiply (a x b)', 1700, 100)
                nearOuterPlaneMulti4 = NumberBox(nearOuterPlaneRelation, 'Multiply (a x b)', 1700, 200, None, 100)
                nearOuterPlaneMulti5 = NumberBox(nearOuterPlaneRelation, 'Multiply (a x b)', 1700, 300, None, 0.5)
                nearOuterPlaneAdd = NumberBox(nearOuterPlaneRelation, 'Add (a + b)', 2000, 0)
                nearOuterPlaneMacroOut = MacroBox(nearOuterPlaneRelation, 'Macro Output Number', 2000, 500, 'Near_Outer_True_Pos')
                nearOuterPlaneNumVec = ConverterBox(nearOuterPlaneRelation, 'Number to Vector', 2300, 0)
                nearOuterPlaneMacroOut2 = MacroBox(nearOuterPlaneRelation, 'Macro Output Vector', 2600, 0, 'Near_Outer_Plane')

                RS.Utils.Scene.ConnectBox(nearOuterPlaneMacroIn, 'Input', nearOuterPlaneIsLess, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMacroIn, 'Input', nearOuterPlaneIsGreater, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMacroIn, 'Input', nearOuterPlaneMulti2, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMacroIn, 'Input', nearOuterPlaneMulti4, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneDiv, 'Result', nearOuterPlaneIfCondElse, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneIsLess, 'Result', nearOuterPlaneIfCondElse, 'Cond')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneDistance, 'Result', nearOuterPlaneDiv2, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneIsGreater, 'Result', nearOuterPlaneIfCondElse2, 'Cond')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneIfCondElse, 'Result', nearOuterPlaneMulti, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneDiv2, 'Result', nearOuterPlaneIfCondElse2, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti, 'Result', nearOuterPlaneSub, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti2, 'Result', nearOuterPlaneMulti, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneIfCondElse2, 'Result', nearOuterPlaneMulti3, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneSub, 'Result', nearOuterPlaneAdd, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti3, 'Result', nearOuterPlaneAdd, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti4, 'Result', nearOuterPlaneMulti3, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti5, 'Result', nearOuterPlaneDiv, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti5, 'Result', nearOuterPlaneDistance, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti5, 'Result', nearOuterPlaneSub, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti5, 'Result', nearOuterPlaneMacroOut, 'Output')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMacroIn2, 'Input', nearOuterPlaneMulti5, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneSub, 'Result', nearOuterPlaneAdd, 'b')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneMulti3, 'Result', nearOuterPlaneAdd, 'a')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneAdd, 'Result', nearOuterPlaneNumVec, 'X')
                RS.Utils.Scene.ConnectBox(nearOuterPlaneNumVec, 'Result', nearOuterPlaneMacroOut2, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(nearOuterPlaneRelation)

            else:
                nearOuterPlaneRelation = value[1]

        # Near_Inner_Plane Macro
        if key == 'Near_Inner_Plane':
            if value[0] == False:
                nearInnerPlaneRelation = FBConstraintRelation("Near_Inner_Plane")
                nearInnerPlaneRelation.Active = True
                lTag = nearInnerPlaneRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = nearInnerPlaneRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Near_Inner_Plane'

                nearInnerPlaneMacroIn = MacroBox(nearInnerPlaneRelation, 'Macro Input Number', 0, 600, 'Near_Outer_True_Pos')
                nearInnerPlaneMacroIn2 = MacroBox(nearInnerPlaneRelation, 'Macro Input Number', 0, 800, 'Near_Inner_Override')
                nearInnerPlaneMacroIn3 = MacroBox(nearInnerPlaneRelation, 'Macro Input Number', 0, 1000, 'Focal_Length')
                nearInnerPlaneMacroIn4 = MacroBox(nearInnerPlaneRelation, 'Macro Input Number', 0, 1200, 'Hyperfocal')
                nearInnerPlaneMacroIn5 = MacroBox(nearInnerPlaneRelation, 'Macro Input Number', 0, 1400, 'Focus')
                nearInnerPlaneDistance = NumberBox(nearInnerPlaneRelation, 'Distance Numbers', 500, 600)
                nearInnerPlaneMulti = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 500, 1400, None, 10)
                nearInnerPlaneDiv = NumberBox(nearInnerPlaneRelation, 'Divide (a/b)', 900, 500, None, 100)
                nearInnerPlaneIsLess = NumberBox(nearInnerPlaneRelation, 'Is Less (a < b)', 900, 650, None, 0)
                nearInnerPlaneDistance2 = NumberBox(nearInnerPlaneRelation, 'Distance Numbers', 900, 800)
                nearInnerPlaneIsGreater = NumberBox(nearInnerPlaneRelation, 'is Greater (a > b)', 900, 950, None, 0)
                nearInnerPlaneSub = NumberBox(nearInnerPlaneRelation, 'Subtract (a - b)', 900, 1100)
                nearInnerPlaneAdd = NumberBox(nearInnerPlaneRelation, 'Add (a + b)', 900, 1250)
                nearInnerPlaneMulti2 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 900, 1400, 2)
                nearInnerPlaneSub2 = NumberBox(nearInnerPlaneRelation, 'Subtract (a - b)', 900, 1550)
                nearInnerPlaneIfCondElse = NumberBox(nearInnerPlaneRelation, 'IF Cond Then A Else B', 1300, 575, None, 0)
                nearInnerPlaneDiv2 = NumberBox(nearInnerPlaneRelation, 'Divide (a/b)', 1300, 875, None, 100)
                nearInnerPlaneMulti3 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 1300, 1100)
                nearInnerPlaneSub3 = NumberBox(nearInnerPlaneRelation, 'Subtract (a - b)', 1300, 1315)
                nearInnerPlaneDiv3 = NumberBox(nearInnerPlaneRelation, 'Divide (a/b)', 1300, 1550, None)
                nearInnerPlaneMulti4 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 1700, 500)
                nearInnerPlaneMulti5 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 1700, 650, None, -100)
                nearInnerPlaneIfCondElse2 = NumberBox(nearInnerPlaneRelation, 'IF Cond Then A Else B', 1700, 800, None, 0)
                nearInnerPlaneDiv4 = NumberBox(nearInnerPlaneRelation, 'Divide (a/b)', 1700, 950, None, 10)
                nearInnerPlaneDiv5 = NumberBox(nearInnerPlaneRelation, 'Divide (a/b)', 1700, 1100, None, 10)
                nearInnerPlaneMacroOut = MacroBox(nearInnerPlaneRelation, 'Macro Output Number', 1700, 1550, 'Hyperfocal_Subject')
                nearInnerPlaneSub4 = NumberBox(nearInnerPlaneRelation, 'Subtract (a - b)', 2100, 500)
                nearInnerPlaneMulti6 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 2100, 800)
                nearInnerPlaneMulti7 = NumberBox(nearInnerPlaneRelation, 'Multiply (a x b)', 2100, 950, None, 100)
                nearInnerPlaneMacroOut2 = MacroBox(nearInnerPlaneRelation, 'Macro Output Number', 2100, 1550, 'Near_Inner_True_Pos')
                nearInnerPlaneAdd2 = NumberBox(nearInnerPlaneRelation, 'Add (a + b)', 2500, 575)
                nearInnerPlaneNumVec = ConverterBox(nearInnerPlaneRelation, 'Number to Vector', 2900, 575)
                nearInnerPlaneMacroOut3 = MacroBox(nearInnerPlaneRelation, 'Macro Output Vector', 3400, 575, 'Near_Inner_Plane')

                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn, 'Input', nearInnerPlaneDistance, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn2, 'Input', nearInnerPlaneIsLess, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn2, 'Input', nearInnerPlaneMulti5, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn2, 'Input', nearInnerPlaneMulti7, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn2, 'Input', nearInnerPlaneIsGreater, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn3, 'Input', nearInnerPlaneSub, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn3, 'Input', nearInnerPlaneMulti2, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn4, 'Input', nearInnerPlaneSub, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn4, 'Input', nearInnerPlaneAdd, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn4, 'Input', nearInnerPlaneSub2, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn5, 'Input', nearInnerPlaneDistance2, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMacroIn5, 'Input', nearInnerPlaneMulti, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDistance, 'Result', nearInnerPlaneDiv, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti, 'Result', nearInnerPlaneAdd, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti, 'Result', nearInnerPlaneMulti3, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti, 'Result', nearInnerPlaneSub2, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv, 'Result', nearInnerPlaneIfCondElse, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneIsLess, 'Result', nearInnerPlaneIfCondElse, 'Cond')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDistance2, 'Result', nearInnerPlaneDiv2, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneIsGreater, 'Result', nearInnerPlaneIfCondElse2, 'Cond')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneSub, 'Result', nearInnerPlaneMulti3, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneAdd, 'Result', nearInnerPlaneSub3, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti2, 'Result', nearInnerPlaneSub3, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneSub2, 'Result', nearInnerPlaneDiv3, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneIfCondElse, 'Result', nearInnerPlaneMulti4, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv2, 'Result', nearInnerPlaneIfCondElse2, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti3, 'Result', nearInnerPlaneDiv5, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti3, 'Result', nearInnerPlaneDiv3, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneSub3, 'Result', nearInnerPlaneDiv5, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv3, 'Result', nearInnerPlaneMacroOut, 'Output')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti4, 'Result', nearInnerPlaneSub4, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti5, 'Result', nearInnerPlaneMulti4, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneIfCondElse2, 'Result', nearInnerPlaneMulti6, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv4, 'Result', nearInnerPlaneDistance, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv4, 'Result', nearInnerPlaneDistance2, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv4, 'Result', nearInnerPlaneSub4, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv4, 'Result', nearInnerPlaneMacroOut2, 'Output')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneDiv5, 'Result', nearInnerPlaneDiv4, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneSub4, 'Result', nearInnerPlaneAdd2, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti6, 'Result', nearInnerPlaneAdd2, 'a')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneMulti7, 'Result', nearInnerPlaneMulti6, 'b')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneAdd2, 'Result', nearInnerPlaneNumVec, 'X')
                RS.Utils.Scene.ConnectBox(nearInnerPlaneNumVec, 'Result', nearInnerPlaneMacroOut3, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(nearInnerPlaneRelation)

            else:
                nearInnerPlaneRelation = value[1]

        # Far_Inner_Plane Macro
        if key == 'Far_Inner_Plane':
            if value[0] == False:

                farInnerPlaneRelation = FBConstraintRelation("Far_Inner_Plane")
                farInnerPlaneRelation.Active = True
                lTag = farInnerPlaneRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = farInnerPlaneRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Far_Inner_Plane'

                farInnerPlaneMacroIn = MacroBox(farInnerPlaneRelation, 'Macro Input Number', 0, 600, 'Hyperfocal_Subject')
                farInnerPlaneMacroIn2 = MacroBox(farInnerPlaneRelation, 'Macro Input Number', 0, 800, 'Far_Inner_Override')
                farInnerPlaneMacroIn3 = MacroBox(farInnerPlaneRelation, 'Macro Input Number', 0, 1000, 'Far_Outer_True_Pos')
                farInnerPlaneMacroIn4 = MacroBox(farInnerPlaneRelation, 'Macro Input Number', 0, 1200, 'Focus')
                farInnerPlaneDiv = NumberBox(farInnerPlaneRelation, 'Divide (a/b)', 400, 600, None, 10)
                farInnerPlaneIsGreater = NumberBox(farInnerPlaneRelation, 'is Greater (a > b)', 400, 750, None, 0)
                farInnerPlaneDistance = NumberBox(farInnerPlaneRelation, 'Distance Numbers', 400, 900)
                farInnerPlaneIsLess = NumberBox(farInnerPlaneRelation, 'Is Less (a < b)', 400, 1050, None, 0)
                farInnerPlaneDistance2 = NumberBox(farInnerPlaneRelation, 'Distance Numbers', 400, 1200)
                farInnerPlaneIfCondElse = NumberBox(farInnerPlaneRelation, 'IF Cond Then A Else B', 800, 750, None, 0)
                farInnerPlaneDiv2 = NumberBox(farInnerPlaneRelation, 'Divide (a/b)', 800, 900, None, 100)
                farInnerPlaneIfCondElse2 = NumberBox(farInnerPlaneRelation, 'IF Cond Then A Else B', 800, 1050, None, 0)
                farInnerPlaneDiv3 = NumberBox(farInnerPlaneRelation, 'Divide (a/b)', 800, 1200, None, 100)
                farInnerPlaneIsLess2 = NumberBox(farInnerPlaneRelation, 'Is Less (a < b)', 800, 600, None, 0)
                farInnerPlaneMulti = NumberBox(farInnerPlaneRelation, 'Multiply (a x b)', 1200, 750)
                farInnerPlaneMulti2 = NumberBox(farInnerPlaneRelation, 'Multiply (a x b)', 1200, 900, None, 100)
                farInnerPlaneMulti3 = NumberBox(farInnerPlaneRelation, 'Multiply (a x b)', 1200, 1050)
                farInnerPlaneMulti4 = NumberBox(farInnerPlaneRelation, 'Multiply (a x b)', 1200, 1200, None, -100)
                farInnerPlaneIfCondElse3 = NumberBox(farInnerPlaneRelation, 'IF Cond Then A Else B', 1600, 600, 100691.31)
                farInnerPlaneSub = NumberBox(farInnerPlaneRelation, 'Subtract (a - b)', 1600, 1050)
                farInnerPlaneAdd = NumberBox(farInnerPlaneRelation, 'Add (a + b)', 2000, 1050)
                farInnerPlaneMulti5 = NumberBox(farInnerPlaneRelation, 'Multiply (a x b)', 2400, 600, None, 10)
                farInnerPlaneNumVec = ConverterBox(farInnerPlaneRelation, 'Number to Vector', 2400, 1050)
                farInnerPlaneMacroOut = MacroBox(farInnerPlaneRelation, 'Macro Output Number', 2800, 600, 'Far_Inner_True_Pos')
                farInnerPlaneMacroOut2 = MacroBox(farInnerPlaneRelation, 'Macro Output Vector', 2800, 1050, 'Far_Inner_Plane')

                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn, 'Input', farInnerPlaneDiv, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn2, 'Input', farInnerPlaneIsGreater, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn2, 'Input', farInnerPlaneMulti2, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn2, 'Input', farInnerPlaneMulti4, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn2, 'Input', farInnerPlaneIsLess, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn3, 'Input', farInnerPlaneDistance, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMacroIn4, 'Input', farInnerPlaneDistance2, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDiv, 'Result', farInnerPlaneIfCondElse3, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDiv, 'Result', farInnerPlaneIsLess2, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIsGreater, 'Result', farInnerPlaneIfCondElse, 'Cond')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDistance, 'Result', farInnerPlaneDiv2, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIsLess, 'Result', farInnerPlaneIfCondElse2, 'Cond')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDistance2, 'Result', farInnerPlaneDiv3, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse, 'Result', farInnerPlaneMulti, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDiv2, 'Result', farInnerPlaneIfCondElse, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse2, 'Result', farInnerPlaneMulti3, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDiv3, 'Result', farInnerPlaneIfCondElse2, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIsLess2, 'Result', farInnerPlaneIfCondElse3, 'Cond')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMulti, 'Result', farInnerPlaneAdd, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMulti2, 'Result', farInnerPlaneMulti, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMulti3, 'Result', farInnerPlaneSub, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMulti4, 'Result', farInnerPlaneMulti3, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse3, 'Result', farInnerPlaneMulti5, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse3, 'Result', farInnerPlaneSub, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse3, 'Result', farInnerPlaneDistance, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIfCondElse3, 'Result', farInnerPlaneDistance2, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneSub, 'Result', farInnerPlaneAdd, 'b')
                RS.Utils.Scene.ConnectBox(farInnerPlaneAdd, 'Result', farInnerPlaneNumVec, 'X')
                RS.Utils.Scene.ConnectBox(farInnerPlaneMulti5, 'Result', farInnerPlaneMacroOut, 'Output')
                RS.Utils.Scene.ConnectBox(farInnerPlaneNumVec, 'Result', farInnerPlaneMacroOut2, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(farInnerPlaneRelation)

            else:
                farInnerPlaneRelation = value[1]

        # Far_Outer_Plane Macro
        if key == 'Far_Outer_Plane':
            if value[0] == False:

                farOuterPlaneRelation = FBConstraintRelation("Far_Outer_Plane")
                farOuterPlaneRelation.Active = True
                lTag = farOuterPlaneRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = farOuterPlaneRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Far_Outer_Plane'

                # dict for fcurve def
                farOuterPlaneFCurveDict = { 0:[0, FBInterpolation.kFBInterpolationLinear, None],
                                            1:[50, FBInterpolation.kFBInterpolationLinear, None],
                                            100:[50, FBInterpolation.kFBInterpolationLinear, None] }

                farOuterPlaneMacroIn = MacroBox(farOuterPlaneRelation, 'Macro Input Number', 0, 600, 'Hyperfocal_Subject')
                farOuterPlaneMacroIn2 = MacroBox(farOuterPlaneRelation, 'Macro Input Number', 0, 800, 'Far_Outer_Override')
                farOuterPlaneMacroIn3 = MacroBox(farOuterPlaneRelation, 'Macro Input Number', 0, 1000, 'Far_Inner_True_Pos')
                farOuterPlaneDiv = NumberBox(farOuterPlaneRelation, 'Divide (a/b)', 400, 600, None, 10)
                farOuterPlaneIsLess = NumberBox(farOuterPlaneRelation, 'Is Less (a < b)', 800, 500, None, 0)
                farInnerPlaneDistance = NumberBox(farOuterPlaneRelation, 'Distance Numbers', 800, 650)
                farInnerPlaneIsGreater = NumberBox(farOuterPlaneRelation, 'is Greater (a > b)', 800, 800, None, 0)
                farOuterPlaneDiv2 = NumberBox(farOuterPlaneRelation, 'Divide (a/b)', 800, 950, None, 100)
                farOuterPlaneFCurve = OtherBox(farOuterPlaneRelation, 'FCurve Number (%)', 800, 1100)
                RelationFCurveKeys(farOuterPlaneFCurve, farOuterPlaneFCurveDict)
                farOuterPlaneIfCondElse = NumberBox(farOuterPlaneRelation, 'IF Cond Then A Else B', 1200, 500, None, 0)
                farOuterPlaneDiv3 = NumberBox(farOuterPlaneRelation, 'Divide (a/b)', 1200, 650, None, 100)
                farOuterPlaneIfCondElse2 = NumberBox(farOuterPlaneRelation, 'IF Cond Then A Else B', 1200, 800, None, 0)
                farOuterPlaneMulti = NumberBox(farOuterPlaneRelation, 'Multiply (a x b)', 1200, 950, None, 100)
                farOuterPlaneMulti2 = NumberBox(farOuterPlaneRelation, 'Multiply (a x b)', 1600, 600)
                farOuterPlaneMulti3 = NumberBox(farOuterPlaneRelation, 'Multiply (a x b)', 1600, 750, None, -100)
                farOuterPlaneMulti4 = NumberBox(farOuterPlaneRelation, 'Multiply (a x b)', 1600, 900)
                farOuterPlaneMulti5 = NumberBox(farOuterPlaneRelation, 'Multiply (a x b)', 1600, 1050)
                farOuterPlaneSub = NumberBox(farOuterPlaneRelation, 'Subtract (a - b)', 2000, 600)
                farOuterPlaneAdd = NumberBox(farOuterPlaneRelation, 'Add (a + b)', 2400, 600)
                farOuterPlaneNumVec = ConverterBox(farOuterPlaneRelation, 'Number to Vector', 2800, 600)
                farOuterPlaneMacroOut = MacroBox(farOuterPlaneRelation, 'Macro Output Number', 2800, 500, 'Far_Outer_True_Pos')
                farOuterPlaneMacroOut2 = MacroBox(farOuterPlaneRelation, 'Macro Output Vector', 3200, 600, 'Far_Outer_Plane')

                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn, 'Input', farOuterPlaneDiv, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn2, 'Input', farOuterPlaneIsLess, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn2, 'Input', farOuterPlaneMulti3, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn2, 'Input', farInnerPlaneIsGreater, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn2, 'Input', farOuterPlaneMulti5, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn2, 'Input', farOuterPlaneFCurve, 'Position %')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn3, 'Input', farInnerPlaneDistance, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn3, 'Input', farOuterPlaneSub, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMacroIn3, 'Input', farOuterPlaneDiv2, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneDiv, 'Result', farInnerPlaneDistance, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneIsLess, 'Result', farOuterPlaneIfCondElse, 'Cond')
                RS.Utils.Scene.ConnectBox(farInnerPlaneDistance, 'Result', farOuterPlaneDiv3, 'a')
                RS.Utils.Scene.ConnectBox(farInnerPlaneIsGreater, 'Result', farOuterPlaneIfCondElse2, 'Cond')
                RS.Utils.Scene.ConnectBox(farOuterPlaneDiv2, 'Result', farOuterPlaneIfCondElse2, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneFCurve, 'Value', farOuterPlaneMulti, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneIfCondElse, 'Result', farOuterPlaneMulti2, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneDiv3, 'Result', farOuterPlaneIfCondElse, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneIfCondElse2, 'Result', farOuterPlaneMulti4, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMulti, 'Result', farOuterPlaneMulti5, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMulti2, 'Result', farOuterPlaneSub, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMulti3, 'Result', farOuterPlaneMulti2, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMulti4, 'Result', farOuterPlaneAdd, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneMulti5, 'Result', farOuterPlaneMulti4, 'b')
                RS.Utils.Scene.ConnectBox(farOuterPlaneSub, 'Result', farOuterPlaneAdd, 'a')
                RS.Utils.Scene.ConnectBox(farOuterPlaneAdd, 'Result', farOuterPlaneMacroOut, 'Output')
                RS.Utils.Scene.ConnectBox(farOuterPlaneAdd, 'Result', farOuterPlaneNumVec, 'X')
                RS.Utils.Scene.ConnectBox(farOuterPlaneNumVec, 'Result', farOuterPlaneMacroOut2, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(farOuterPlaneRelation)

            else:
                farOuterPlaneRelation = value[1]

        # Lens_CoC_Focal_length Macro
        if key == 'Lens_CoC_Focal_length':
            if value[0] == False:

                lensRelation = FBConstraintRelation("Lens_CoC_Focal_length")
                lensRelation.Active = True
                lTag = lensRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
                lTag.Data = 'rs_Constraints'
                tag = lensRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = 'Lens_CoC_Focal_length'

                # dicts for fcurve def
                lensFCurveDict = { 0:[3, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                   16.50:[4, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                   33.33:[6, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                   50:[9, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                   66.67:[11, FBInterpolation.kFBInterpolationLinear, None],
                                   83.33:[14, FBInterpolation.kFBInterpolationLinear, None],
                                   100:[14, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB] }

                lensFCurve2Dict = { 0:[2, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    16.50:[3, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    33.33:[5, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    50:[7, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    66.67:[8, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    83.33:[10, FBInterpolation.kFBInterpolationLinear, None],
                                    100:[10, FBInterpolation.kFBInterpolationLinear, None] }

                lensFCurve3Dict = { 0:[11.43, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    16.50:[17.97, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    33.33:[23.91, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    50:[35.72, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    66.67:[53.22, FBInterpolation.kFBInterpolationCubic, FBTangentMode.kFBTangentModeTCB],
                                    83.33:[91.45, FBInterpolation.kFBInterpolationLinear, None],
                                    100:[91.45, FBInterpolation.kFBInterpolationLinear, None] }

                lensMacroIn = MacroBox(lensRelation, 'Macro Input Number', 0, 600, 'Lens')
                lensMacroIn2 = MacroBox(lensRelation, 'Macro Input Number', 0, 900, 'FOV')
                lensMacroIn3 = MacroBox(lensRelation, 'Macro Input Number', 0, 1200, 'CoC_Night_Override')
                lensAbsolute = NumberBox(lensRelation, 'Absolute (|a|)', 400, 600)
                lensAbsolute2 = NumberBox(lensRelation, 'Absolute (|a|)', 400, 900)
                lensFCurve = OtherBox(lensRelation, 'FCurve Number (%)', 400, 1050)
                RelationFCurveKeys(lensFCurve, lensFCurveDict)
                lensIsGreater = NumberBox(lensRelation, 'is Greater (a > b)', 400, 1250, None, 0)
                lensMulti = NumberBox(lensRelation, 'Multiply (a x b)', 800, 400, None, 100.010)
                lensDiv = NumberBox(lensRelation, 'Divide (a/b)', 800, 500, None, 6)
                lensIfCondElse = NumberBox(lensRelation, 'IF Cond Then A Else B', 1200, 400, None, 6)
                lensIsBetween = NumberBox(lensRelation, 'Is Between A and B', 1200, 500, 0, 5)
                lensSub = NumberBox(lensRelation, 'Subtract (a - b)', 1200, 600, 59.98)
                lensScaleOffset = NumberBox(lensRelation, 'Scale And Offset (Number)', 1200, 900)
                dataMax = RS.Utils.Scene.FindAnimationNode(lensScaleOffset.AnimationNodeInGet(), 'Clamp Max')
                dataMax.WriteData([69.98])
                dataMin = RS.Utils.Scene.FindAnimationNode (lensScaleOffset.AnimationNodeInGet(), 'Clamp Min')
                dataMin.WriteData([10])
                lensIfCondElse2 = NumberBox(lensRelation, 'IF Cond Then A Else B', 1200, 1200, None, 6)
                lensFCurve2 = OtherBox(lensRelation, 'FCurve Number (%)', 1600, 1000)
                RelationFCurveKeys(lensFCurve2, lensFCurve2Dict)
                lensMacroIn4 = MacroBox(lensRelation, 'Macro Input Number', 1600, 1100, 'CoC_Override')
                lensInteger = NumberBox(lensRelation, 'Integer', 1600, 1200)
                lensFCurve3 = OtherBox(lensRelation, 'FCurve Number (%)', 2000, 400)
                RelationFCurveKeys(lensFCurve3, lensFCurve3Dict)
                lensPrecision = NumberBox(lensRelation, 'Precision Numbers', 2400, 300)
                dataPrecision = RS.Utils.Scene.FindAnimationNode(lensPrecision.AnimationNodeInGet(), 'Precision')
                dataPrecision.WriteData([0.01])
                lensMulti2 = NumberBox(lensRelation, 'Multiply (a x b)', 2000, 500, None, 100)
                lensDiv2 = NumberBox(lensRelation, 'Divide (a/b)', 2000, 600, None, 59.98)
                lensIsGreater2 = NumberBox(lensRelation, 'is Greater (a > b)', 2000, 700, None, 5)
                lensArcTan = NumberBox(lensRelation, 'arctan(a)', 2000, 800)
                lensDiv3 = NumberBox(lensRelation, 'Divide (a/b)', 2000, 900, 16)
                lensMulti3 = NumberBox(lensRelation, 'Multiply (a x b)', 2000, 1000, None, 2)
                lensIsGreater3 = NumberBox(lensRelation, 'is Greater (a > b)', 2000, 1100, None, 0)
                lensMacroOut = MacroBox(lensRelation, 'Macro Output Number', 2000, 1200, 'CoC_Night')
                lensMacroOut2 = MacroBox(lensRelation, 'Macro Output Number', 2400, 400, 'Focal_length')
                lensMulti4 = NumberBox(lensRelation, 'Multiply (a x b)', 2400, 800, 2)
                lensIfCondElse3 = NumberBox(lensRelation, 'IF Cond Then A Else B', 2400, 1100)
                lensIfCondElse4 = NumberBox(lensRelation, 'IF Cond Then A Else B', 2800, 800)
                lensInteger2 = NumberBox(lensRelation, 'Integer', 2800, 1100)
                lensMacroOut3 = MacroBox(lensRelation, 'Macro Output Number', 3200, 800, 'FOV_output')
                lensMacroOut4 = MacroBox(lensRelation, 'Macro Output Number', 3200, 1100, 'CoC')

                RS.Utils.Scene.ConnectBox(lensMacroIn, 'Input', lensAbsolute, 'a')
                RS.Utils.Scene.ConnectBox(lensMacroIn2, 'Input', lensAbsolute2, 'a')
                RS.Utils.Scene.ConnectBox(lensMacroIn3, 'Input', lensIfCondElse2, 'a')
                RS.Utils.Scene.ConnectBox(lensMacroIn3, 'Input', lensIsGreater, 'a')
                RS.Utils.Scene.ConnectBox(lensAbsolute, 'Result', lensDiv, 'a')
                RS.Utils.Scene.ConnectBox(lensAbsolute, 'Result', lensIsBetween, 'Value')
                RS.Utils.Scene.ConnectBox(lensAbsolute, 'Result', lensIsGreater2, 'a')
                RS.Utils.Scene.ConnectBox(lensAbsolute2, 'Result', lensIfCondElse4, 'a')
                RS.Utils.Scene.ConnectBox(lensAbsolute2, 'Result', lensScaleOffset, 'X')
                RS.Utils.Scene.ConnectBox(lensFCurve, 'Value', lensIfCondElse2, 'b')
                RS.Utils.Scene.ConnectBox(lensIsGreater, 'Result', lensIfCondElse2, 'Cond')
                RS.Utils.Scene.ConnectBox(lensMulti, 'Result', lensIfCondElse, 'a')
                RS.Utils.Scene.ConnectBox(lensDiv, 'Result', lensMulti, 'a')
                RS.Utils.Scene.ConnectBox(lensIfCondElse, 'Result', lensFCurve, 'Position %')
                RS.Utils.Scene.ConnectBox(lensIfCondElse, 'Result', lensFCurve2, 'Position %')
                RS.Utils.Scene.ConnectBox(lensIfCondElse, 'Result', lensFCurve3, 'Position %')
                RS.Utils.Scene.ConnectBox(lensIsBetween, 'Result', lensIfCondElse, 'Cond')
                RS.Utils.Scene.ConnectBox(lensSub, 'Result', lensDiv2, 'a')
                RS.Utils.Scene.ConnectBox(lensScaleOffset, 'Result', lensSub, 'b')
                RS.Utils.Scene.ConnectBox(lensIfCondElse2, 'Result', lensInteger, 'a')
                RS.Utils.Scene.ConnectBox(lensFCurve2, 'Value', lensIfCondElse3, 'b')
                RS.Utils.Scene.ConnectBox(lensMacroIn4, 'Input', lensIfCondElse3, 'a')
                RS.Utils.Scene.ConnectBox(lensMacroIn4, 'Input', lensIsGreater3, 'a')
                RS.Utils.Scene.ConnectBox(lensInteger, 'Result', lensMacroOut, 'Output')
                RS.Utils.Scene.ConnectBox(lensFCurve3, 'Value', lensMulti3, 'a')
                RS.Utils.Scene.ConnectBox(lensFCurve3, 'Value', lensPrecision, 'a')
                RS.Utils.Scene.ConnectBox(lensPrecision, 'Result', lensMacroOut2, 'Output')
                RS.Utils.Scene.ConnectBox(lensMulti2, 'Result', lensIfCondElse, 'b')
                RS.Utils.Scene.ConnectBox(lensDiv2, 'Result', lensMulti2, 'a')
                RS.Utils.Scene.ConnectBox(lensIsGreater2, 'Result', lensIfCondElse4, 'Cond')
                RS.Utils.Scene.ConnectBox(lensDiv3, 'Result', lensArcTan, 'a')
                RS.Utils.Scene.ConnectBox(lensArcTan, 'Result', lensMulti4, 'b')
                RS.Utils.Scene.ConnectBox(lensMulti3, 'Result', lensDiv3, 'b')
                RS.Utils.Scene.ConnectBox(lensIsGreater3, 'Result', lensIfCondElse3, 'Cond')
                RS.Utils.Scene.ConnectBox(lensMulti4, 'Result', lensIfCondElse4, 'b')
                RS.Utils.Scene.ConnectBox(lensIfCondElse3, 'Result', lensInteger2, 'a')
                RS.Utils.Scene.ConnectBox(lensIfCondElse4, 'Result', lensMacroOut3, 'Output')
                RS.Utils.Scene.ConnectBox(lensInteger2, 'Result', lensMacroOut4, 'Output')

                macroFolder = Macro_RelationFolder()
                macroFolder.Items.append(lensRelation)

            else:
                lensRelation = value[1]


    ##########################################################################################
    # Camera Relation Constraint
    ##########################################################################################

    farPlane = None
    nearPlane = None
    farStrengthPlane = None
    nearStrengthPlane = None
    focusMarker = None

    for child in pCamera.Children:
        if 'FarPlane' in child.Name and 'Strength' not in child.Name:
            farPlane = child
        if 'NearPlane' in child.Name and 'Strength' not in child.Name:
            nearPlane = child
        if 'FarPlane' in child.Name and 'Strength' in child.Name:
            farStrengthPlane = child
        if 'NearPlane' in child.Name and 'Strength' in child.Name:
            nearStrengthPlane = child
        if 'Focus_Marker' in child.Name:
            focusMarker = child

    cameraRelation = FBConstraintRelation(pCamera.Name + " Relation")
    cameraRelation.Active = True
    lTag = cameraRelation.PropertyCreate('camera_macro_relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = pCamera.Name
    lTag = cameraRelation.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = 'rs_Constraints'

    cameraSender = SourceBox(cameraRelation, pCamera, True, 0, 600)
    hyperfocalMacro = MyMacro(cameraRelation, hyperFocalRelation.Name, 500, 600)
    lensMacro = MyMacro(cameraRelation, lensRelation.Name, 500, 700)
    cameraNumVec = ConverterBox(cameraRelation, 'Number to Vector', 1100, 650)
    nearOuterPlaneMacro = MyMacro(cameraRelation, nearOuterPlaneRelation.Name, 1100, 300)
    nearInnerPlaneMacro = MyMacro(cameraRelation, nearInnerPlaneRelation.Name, 1100, 450)
    farInnerPlaneMacro = MyMacro(cameraRelation, farInnerPlaneRelation.Name, 1100, 1250)
    farOuterPlaneMacro = MyMacro(cameraRelation, farOuterPlaneRelation.Name, 1100, 1400)

    if nearStrengthPlane:
        nearStrengthPlaneReceiver = TargetBox(cameraRelation, nearStrengthPlane, False, 2000, 300, 0, 0, 90)
        RS.Utils.Scene.ConnectBox(nearOuterPlaneMacro, 'MacroOutput1', nearStrengthPlaneReceiver, 'Lcl Translation')
    if nearPlane:
        nearPlaneReceiver = TargetBox(cameraRelation, nearPlane, False, 2000, 450, 0, 0, 90)
        RS.Utils.Scene.ConnectBox(nearInnerPlaneMacro, 'MacroOutput2', nearPlaneReceiver, 'Lcl Translation')
    if focusMarker:
        focusMarkerReceiver = TargetBox(cameraRelation, focusMarker, False, 1700, 600)
        RS.Utils.Scene.ConnectBox(cameraNumVec, 'Result', focusMarkerReceiver, 'Lcl Translation')
    if farPlane:
        farPlaneReceiver = TargetBox(cameraRelation, farPlane, False, 2000, 1250, 0, 0, 90)
        RS.Utils.Scene.ConnectBox(farInnerPlaneMacro, 'MacroOutput1', farPlaneReceiver, 'Lcl Translation')
    if farStrengthPlane:
        farStrengthPlaneReceiver = TargetBox(cameraRelation, farStrengthPlane, False, 2000 , 1400, 0, 0, 90)
        RS.Utils.Scene.ConnectBox(farOuterPlaneMacro, 'MacroOutput1', farStrengthPlaneReceiver, 'Lcl Translation')

    cameraReceiver = TargetBox(cameraRelation, pCamera, True, 1700, 750)

    RS.Utils.Scene.ConnectBox(cameraSender, 'FOCUS (cm)', cameraNumVec, 'X')
    RS.Utils.Scene.ConnectBox(cameraSender, 'CoC Night Override', lensMacro, 'MacroInput2')
    RS.Utils.Scene.ConnectBox(cameraSender, 'CoC Override', lensMacro, 'MacroInput3')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Lens', lensMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(cameraSender, 'FieldOfView', lensMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Near Outer Override', nearOuterPlaneMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(cameraSender, 'FOCUS (cm)', nearInnerPlaneMacro, 'MacroInput4')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Near Inner Override', nearInnerPlaneMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Far Inner Override', farInnerPlaneMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(cameraSender, 'FOCUS (cm)', farInnerPlaneMacro, 'MacroInput3')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Far Outer Override', farOuterPlaneMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(cameraSender, 'Far Outer Override', farOuterPlaneMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(nearOuterPlaneMacro, 'MacroOutput0', nearInnerPlaneMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(nearInnerPlaneMacro, 'MacroOutput1', nearOuterPlaneMacro, 'MacroInput1')
    RS.Utils.Scene.ConnectBox(lensMacro, 'MacroOutput1', nearInnerPlaneMacro, 'MacroInput2')
    RS.Utils.Scene.ConnectBox(lensMacro, 'MacroOutput1', hyperfocalMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(lensMacro, 'MacroOutput0', cameraReceiver, 'CoC Night')
    RS.Utils.Scene.ConnectBox(lensMacro, 'MacroOutput3', cameraReceiver, 'CoC')
    RS.Utils.Scene.ConnectBox(lensMacro, 'MacroOutput2', cameraReceiver, 'FieldOfView')
    RS.Utils.Scene.ConnectBox(nearInnerPlaneMacro, 'MacroOutput0', farOuterPlaneMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(nearInnerPlaneMacro, 'MacroOutput0', farInnerPlaneMacro, 'MacroInput0')
    RS.Utils.Scene.ConnectBox(farInnerPlaneMacro, 'MacroOutput0', farOuterPlaneMacro, 'MacroInput2')
    RS.Utils.Scene.ConnectBox(farOuterPlaneMacro, 'MacroOutput0', farInnerPlaneMacro, 'MacroInput2')
    RS.Utils.Scene.ConnectBox(hyperfocalMacro, 'MacroOutput0', nearInnerPlaneMacro, 'MacroInput3')
