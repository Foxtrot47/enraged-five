import os
import shutil
import RS.Globals
import RS.Utils.Path as path
import RS.Core.Camera.AvidFrameProcessor as avidProcessor
from RS.Core.Automation.FrameCapture import VideoTools

from pyfbsdk import *

class MakeFbxRender():
    def __init__(self, pathText, noteText):
        self.fbxName = path.GetBaseNameNoExtension(FBApplication().FBXFileName)
        self.pathText = pathText
        self.noteText = noteText

        self.outputImageFolder = 'None'

        self.renderImageSeq()
        # self.processFramesForAvid()
        # VideoTools.CreateRdrAvidVideo(self.fbxName, inputPath, outputPath, noteText=noteText)

    def fixEndFrameAsterick(self):
        """ Detects a * in the endframe range
        and if found removes it completely. """
        endString = FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetTimeString()
        if endString.endswith('*'):
            fixedEndInt = int(endString[:-1])
            fixedTimeSpan = FBTimeSpan(FBTime(0,0,0,0),FBTime(0,0,0,fixedEndInt))
            FBSystem().CurrentTake.LocalTimeSpan = fixedTimeSpan

    def changeCamResolutions(self, camList, widthRes, heightRes):
        for camera in camList:
            camera.ResolutionWidth = widthRes
            camera.ResolutionHeight = heightRes
            camera.PixelAspectRatio = 1.0

            #If Cam Background Grey - Force to black
            if camera.BackGroundColor.Data == FBColor( 0.16, 0.16, 0.16):
                camera.BackGroundColor = FBColor( 0.0, 0.0, 0.0)

    def renderImageSeq(self):
        #BASIC SETUP
        lApp = FBApplication()
        lsys = FBSystem()
        lscene = lsys.Scene
        lscene.Renderer.UseCameraSwitcher = True

        #SET CAM RESOLUTION
        cameraList = lscene.Cameras[7:]
        self.changeCamResolutions(cameraList, 1280.00, 720.00)

        #SET RENDER OPTIONS
        lOptions = FBVideoGrabber().GetOptions()
        FBPlayerControl().SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapOnFrames
        VideoManager = FBVideoCodecManager()
        # lOptions.CameraResolution = lOptions.CameraResolution.kFBResolutionD1NTSC
        lOptions.CameraResolution = lOptions.CameraResolution.kFBResolutionCustom
        lOptions.RenderAudio = False
        lOptions.ShowTimeCode = True
        lOptions.ShowSafeArea = False
        lOptions.ShowCameraLabel = False
        lOptions.AntiAliasing = False
        lOptions.TimeSpan = FBTimeSpan(FBTime(0,0,0,0),FBTime(0,0,0,0))
        lOptions.TimeSteps = FBTime(0,0,0,1)

        #FILTER NOTES TEXT
        if self.noteText != "(optional)":
            noteString = "$" + self.noteText
            noteString = filter(lambda x: x!=' ', noteString)
            noteString = filter(lambda x: x!='.', noteString)
            noteString = filter(lambda x: x!='/', noteString)
            noteString = filter(lambda x: x!='\\', noteString)
            noteString = filter(lambda x: x!='#', noteString)
        else:
            noteString = ""

        #SETUP OUTPUT FOLDER AND IMAGE PATH
        ldestDir = self.pathText
        self.outputImageFolder = (ldestDir + '\\' + self.fbxName + noteString + '\\')
        if os.path.exists(self.outputImageFolder):
            shutil.rmtree(self.outputImageFolder)
        os.makedirs(self.outputImageFolder)
        outputImagePath = self.outputImageFolder + self.fbxName + noteString + "-.jpg"
        lOptions.OutputFileName = outputImagePath

        #TURN OFF STORY MODE
        #I'm disabling this for now, as it sometimes causes issues, and I don't remember why we ever needed it off.
        #formerStoryMuteState = RS.Globals.Story.Mute
        #RS.Globals.Story.Mute = True

        #START RENDER
        lApp.FileRender(lOptions)

        #RESTORE CAM RESOLUTION AND STORY STATE
        self.changeCamResolutions(cameraList, 1920.00, 1080.00)
        #RS.Globals.Story.Mute = formerStoryMuteState
        
        # HACKY QUICK FIX
        firstFramePath = self.outputImageFolder + self.fbxName + noteString + "-0000.jpg"
        videoOutputPath = os.path.split(self.outputImageFolder[:-1])[0] + "\\" + self.fbxName + noteString + ".mov"
        VideoTools.CreateRdrAvidVideo(self.fbxName, firstFramePath, videoOutputPath, noteText=self.noteText)
        shutil.rmtree(self.outputImageFolder)

    def processFramesForAvid(self):
        avidProcessor.ProcessImgSeqForAvid(self.outputImageFolder)
        FBMessageBox( "RENDER CREATED", "The fbx has been successfully rendered.", "OK" )
