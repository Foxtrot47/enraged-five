'''

 Script Path: RS/Tools/UI/Camera/Toolbox.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 26 March 2014
 
 Description: Launcher for the Camera Toolbox, for Kelly to assign as a hotkey

'''

from pyfbsdk import *

import RS.Tools.UI.Camera.Toolbox

#launch for cams
RS.Tools.UI.Camera.Toolbox.Run()