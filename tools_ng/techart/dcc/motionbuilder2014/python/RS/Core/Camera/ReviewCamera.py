"""
Main Lib Script for the Review CameraConstraintSetup
"""
import random
import pyfbsdk as mobu 

from RS.Core.Camera import Lib as CamLib
from RS.Utils.Scene import Constraint
from RS.Utils import Scene


class ReviewCamera(object):
    """
    Review Camera Class designed to create and manage the review cameras in motion builder
    
    Example Usage:
    
        from RS.Camera import ReviewCamera
        
        # Create a camera from a list of items
        cam1 = ReviewCamera.ReviewCamera(elementsToReviewAsAList)
        
        # Create another camera from a list of items, but move with the mover
        cam2 = ReviewCamera.ReviewCamera(differentElementsToReview, followMover=True)
        
        # Create yet another camera but this time using the current selection
        cam3 = ReviewCamera.ReviewCamera.CreateReviewFromSelection()
        
        # Update the camera focus with a list of items
        cam2.UpdateCameraFocus(elementsToReviewAsAList)
        
        # Iter through all review cameras
        for camera in ReviewCamera.ReviewCamera.GetReviewCameras():
            # Delete the cameras
            camera.DeleteCamera()
    """
    
    # Class strings to make sure we can identify review camera components
    REVIEW_CAMERA_STRING = "ReviewCamera"
    CAMERA_STRING = "Camera"
    CONSTRAINT_STRING = "Constraint"
    
    def __init__(self, listOfElements=None, reviewCamera=None, followMover=False):
        """
        Constructor
        
        Kwargs:
            listOfElements (list of FBModels): a list of elements to be the focus of the review
            reviewCamera (FBCamera)    : A Review camera, to wrap up
            followMover (bool)         : If the constraint should follow the mover or the elements
        
        """
        super(ReviewCamera, self).__init__()
        self._reviewCamera = None
        self._followMover = followMover
        self._listOfElements = listOfElements or []
        
        # handle wrapping a camera into the class or making a new camera
        if reviewCamera is not None and self._IsValidReviewCamera(reviewCamera):
            self._reviewCamera = reviewCamera
            self._listOfElements = self._GetElemetsFromConstraint()
        else:
            self._SetupCamera()

    @classmethod
    def CreateReviewFromSelection(cls, followMover=False):
        """
        Class Method
        
        Create a new review camera from the currently selected items in motion builder and return 
        the camera
        
        Kwargs:
            followMover (bool) : If the constraint should follow the mover or the elements
        
        Returns:
            ReviewCamera: Newly created review camera
        
        """
        modelList = Scene.GetSelection()
        
        ReviewCamera([item for item in modelList], followMover=followMover)
        
    @classmethod
    def GetReviewCameras(cls):
        """
        Class Method
        
        Get all the review cameras in the scene and return them as a list of wrapped ReviewCamera
        objects.
        
        Returns:
            list of ReviewCamera: All the review cameras in the scene
        
        """
        expectedName = "{0}.{1}.*".format(cls.REVIEW_CAMERA_STRING, cls.CAMERA_STRING, )
        cams = Scene.GetComponentsByNamePatternAndType(mobu.FBCamera, expectedName)
        return [ReviewCamera(reviewCamera=item) for item in cams]

    @classmethod
    def CreateReviewCamera(cls):
        """
        Class Method
        
        Create a new Review Camera with the correct name in Motion Builder and return it
        
        Returns:
            FBCamera: The newly created Review Camera
        """
        newName = "{0}.{1}.{2}".format(
                                       cls.REVIEW_CAMERA_STRING, 
                                       cls.CAMERA_STRING, 
                                       str(random.randint(1000000,9000000))
                                       )
        return mobu.FBCamera(newName)

    def UpdateCameraFocus(self, newListOfElements, followMover=None):
        """
        Update the focus of the review camera
        
        Args:
            listOfElements (list of FBModels): a list of elements to be the focus of the review
        
        Kwargs:
            followMover (bool) : If the constraint should follow the mover or the elements
        
        """
        if self._reviewCamera is None:
            self._reviewCamera = self.CreateReviewCamera()
            
        self._listOfElements = newListOfElements
        if followMover is not None:
            self._followMover = followMover
        self._SetupCamera()
        
    def DeleteCamera(self):
        """
        Delete the current Camera and its constraint
        """
        if self._reviewCamera is None:
            return
        constraint = self._GetOrCreateConstraint()
        self._reviewCamera.FBDelete()
        constraint.FBDelete()

    @classmethod
    def _IsValidReviewCamera(cls, cameraInQuestion):
        """
        Internal Method
        
        Class Method
        
        Given an object, will determine if it is a review camera or not
        
        Args:
            cameraInQuestion (FBCamera) : The input camera
            
        Returns:
            bool: If the given object is a review camera
        """
        if not isinstance(cameraInQuestion, mobu.FBCamera):
            return False
        return str(cameraInQuestion.Name).startswith(cls.REVIEW_CAMERA_STRING)
        
    @classmethod
    def _GetProducerPerspectiveCamera(cls):
        """
        Internal Method
        
        Get the Producer Perspective Camera from the scene
        """
        producerPerspectiveCameraName = 'Producer Perspective'
        scene = mobu.FBSystem().Scene
        cams = {cam.Name:cam for cam in scene.Cameras if cam.SystemCamera}
        return cams.get(producerPerspectiveCameraName)
    
    @classmethod
    def _CloneVector(cls, vectorToClone):
        """
        Internal Method
        
        Given an FBVector3d, this will make a copy of the vector and return it
        """
        newVector = mobu.FBVector3d()
        for channel in xrange(3):
            newVector[channel] = vectorToClone[channel]
        return newVector
    
    def _SetupCamera(self):
        """
        Internal Method
        
        Method to set up the camera, both the constraint and the view through the camera ect.
        This method should be called after updating the list of elements to focus on, or if the 
        driver is the mover 
        """
        # Remove any non-FBModel items from the list to follow
        self._listOfElements = [itm for itm in self._listOfElements if isinstance(itm, mobu.FBModel)]

        if len(self._listOfElements) == 0:
            raise ValueError("No Items give to follow")

        # Create a new camera if there isnt one
        if self._reviewCamera is None:
            self._reviewCamera = self.CreateReviewCamera()
        
        # Look through the new camera
        CamLib.camUpdate.rs_switchViewtoCamera(self._reviewCamera)

        # Get the constraint
        constraint = self._GetOrCreateConstraint()
        
        # Get Object (child) Object
        childProperty = constraint.PropertyList.Find("Constrained object (Child)")
        childProperty.removeAll()
        childProperty.append(self._reviewCamera)
        # Clear Source (parent) Objects
        parentProperty = constraint.PropertyList.Find("Source (Parent)")
        parentProperty.removeAll()
        
        # Make sure the constraint affects all the translation and rotation axis
        for propName in ["Affect Translation X", "Affect Translation Y", "Affect Translation Z", 
                         "Affect Rotation X", "Affect Rotation Y", "Affect Rotation Z"]:
            prop = constraint.PropertyList.Find(propName)
            prop.Data = True
        
        # If we're following the Mover, only use the Rotation Y axis, so turn the others off
        if self._followMover:
            for propName in ["Affect Rotation X", "Affect Rotation Z"]:
                prop = constraint.PropertyList.Find(propName)
                prop.Data = False
        
        # If we're not following the mover, we're following the elements
        if not self._followMover:
            for item in self._listOfElements:
                parentProperty.append(item)
        else:
            parentProperty.append(self._FindMover(self._listOfElements[-1]))

        # set the camera interests
        for item in self._listOfElements:
            self._reviewCamera.Interest = item
        
        constraint.Active = True

        perspectiveCam = self._GetProducerPerspectiveCamera()
        if perspectiveCam is None:
            return
        self._reviewCamera.Translation = self._CloneVector(perspectiveCam.Translation)

    def _GetElemetsFromConstraint(self):
        """
        Internal Method
        
        Get a list of objects that is used by the current constraint
        
        Returns:
            list of FBModels : A list of models which make up the drivers for the review camera 
                               constraint
        """
        # Get the constraint
        constraint = self._GetOrCreateConstraint()
        parentProperty = constraint.PropertyList.Find("Source (Parent)")
        return [item for item in parentProperty]
    
    def _FindMover(self, element):
        """
        Internal Method
        
        Method to find the mover component given a object to start looking at
        
        Args:
            element (FBModel) : Model to start looking for the mover
            
        Returns:
            None or FBModel: None if nothing is found, or the FBModel which is the mover object
        """
        parent = element.Parent
        if parent is None:
            return None
        if "mover" in str(parent.Name).lower():
            return parent
        return self._FindMover(parent) 
    
    def _GetOrCreateConstraint(self):
        """
        Internal Method
        
        Method to either get the correct constraint for the review camera, or create a new one with
        the correct name
        
        Returns:
            FBConstraint : The parent/child constraint to drive the current review camera
        """
        # Try to find the constraint
        item = Scene.GetComponentByNameAndType(mobu.FBConstraint, self._GetConstraintName())
        if item is not None:
            return item
        
        # Create the constraint
        return self._CreateConstraint()
    
    def _GetConstraintName(self):
        """
        Internal Method

        Method to generate the name of the constraint that drives the review camera
        
        Returns:
            string: The constraint name
        """
        newName = str(self._reviewCamera.Name).replace(self.CAMERA_STRING, self.CONSTRAINT_STRING)
        return newName
    
    def _CreateConstraint(self):
        """
        Internal Method

        Method to create a correctly named parent/child constraint to drive the review camera
        
        Returns:
            FBConstraint: The new constraint
        """
        newConstraint = Constraint.CreateConstraint(Constraint.PARENT_CHILD)
        newConstraint.Name = self._GetConstraintName()
        return newConstraint
