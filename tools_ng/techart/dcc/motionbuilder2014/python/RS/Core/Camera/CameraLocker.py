from pyfbsdk import *
from xml.dom import minidom
import RS.Config


class LockManager():
    def __init__(self):
        self.cameraList = []
        self.lockedCount = 0
        self.unlockedCount = 0
        
        self.excludedCamNames = ['producer perspective', 'producer front', 'producer back', 
                                'producer right', 'producer left', 'producer top', 'producer bottom',
                                '3lateral']
        self.propsToKey = ['Translation (Lcl)', 'Rotation (Lcl)', 'Roll', 'Lens']
        
        self.createCamList()
        self.detectLockCounts()
        
        #self.setAllCamLocks() #Pass nothing or True for lock, False for unlock
        #self.keyAllKeylessCams()
        
    def createCamList(self):
        """ Creates a list of all camera objects in the FBX,
        unless their names contain strings found in the excludedCamNames list. """
        
        #Get MB Objects
        lSystem = FBSystem()
        lScene = lSystem.Scene
        
        #Go Through Cameras
        for camera in lScene.Cameras:
            
            #Check Cam Name for All Invalid Names
            validCam = True
            for eachInvalidName in self.excludedCamNames:
                if eachInvalidName in camera.Name.lower():
                    validCam = False
                    break
            
            #Add Valid Cams To Cam List
            if validCam == True:
                self.cameraList.append(camera)
        
    def detectLockCounts(self):
        """ Counts the total number of locked and unlocked properties from the cameras in our cameraList. """
        
        #Reset Previous Counts
        self.lockedCount = 0
        self.unlockedCount = 0
        
        #Loop Through Each Camera
        for camera in self.cameraList:
            for eachProp in camera.PropertyList:
                
                #Count Each Property's Lock Status
                if eachProp.AllowsLocking():
                    if eachProp.IsLocked() == True:
                        self.lockedCount += 1
                    else:
                        self.unlockedCount += 1
        
        #Count Switcher Index Lock Status
        lCameraIndexProp = FBCameraSwitcher().PropertyList.Find("Camera Index")
        if lCameraIndexProp.IsLocked() == True:
            self.lockedCount += 1
        else:
            self.unlockedCount += 1
    
    def setAllCamLocks(self, locked=True):
        """ Sets the lock status of all properties on all cameras from the cameraList.
        Passing nothing or True will lock them, False will unlock. """
        
        #Loop Through Cams
        for camera in self.cameraList:
            for eachProp in camera.PropertyList:
                
                #Set Lock Status On Properties
                if eachProp.AllowsLocking() == True:
                    eachProp.SetLocked(locked)
                
        #Set Lock Status On Switcher
        lCameraIndexProp = FBCameraSwitcher().PropertyList.Find("Camera Index")
        lCameraIndexProp.SetLocked( locked )
    
    def getUserLayer(self):
        """ Gets the user's selected layer number,
        or simply uses the max if multiple are selected."""
        
        #Get MB Objects
        lSystem = FBSystem()
        layerCount = lSystem.CurrentTake.GetLayerCount()
        
        #Create List of Selected Layers
        selectedLayers = []
        for i in range(layerCount):
            layerSelected = lSystem.CurrentTake.GetLayer(i).Selected
            if layerSelected == True:
                selectedLayers.append(i)
                
        #Return Max Layers - or default to 0 if errors occur
        try:
            userLayer = max(selectedLayers)
        except ValueError:
            userLayer = 0
        return userLayer
    
    def keyAllKeylessCams(self):
        """ Keys any 'keyless' switcher camera found with a default key at frame 0. 
        We only key properties specified in our propsToKey list (and FOV if using the zoom lens). """
        
        #Detect Any Unlocked Props - and unlock all if found.
        self.detectLockCounts()
        if self.lockedCount != 0:
            self.setAllCamLocks(False)
        
        #Get User Layer - so we can restore later
        userLayer = self.getUserLayer()
        
        #Get MB Objects & Set Base Layer
        lSystem = FBSystem()
        lSystem.CurrentTake.SetCurrentLayer(0)
        
        #Set Default Keyframe Time
        keyTime = FBTime()
        keyTime.SetFrame( 0 )
        
        #Loop Through Each Camera
        for camera in self.cameraList:
            
            #Loop Through Each Prop
            for eachProp in self.propsToKey:                    
                currentCamProp = camera.PropertyList.Find(eachProp)
                
                #Skip Missing Properties
                if currentCamProp == None:
                    continue
                
                #Make Properties Animatable - if not already
                if not currentCamProp.IsAnimated():
                    currentCamProp.SetAnimated(True)
                
                #Roll Logic
                if (currentCamProp.Name == 'Roll'):
                    lensNode = currentCamProp.GetAnimationNode()
                    
                    #Check Count and Value - only key if empty and not set to 0.0
                    if (lensNode.KeyCount == 0) and (currentCamProp.Data != 0.0):
                        currentCamProp.KeyAt(keyTime)
                
                #Lens Logic
                elif currentCamProp.Name == 'Lens':
                    lensNode = currentCamProp.GetAnimationNode()
                    
                    #Key Empty Lens Property
                    if lensNode.KeyCount == 0:
                        currentCamProp.KeyAt(keyTime)
                        
                    #Check FOV Keyed - only for zoom lens (who's value is 6)
                    if currentCamProp.Data == 6:
                        fovProp = camera.PropertyList.Find('Field Of View')
                        fovProp.SetAnimated(True)
                        fovNode = fovProp.GetAnimationNode()
                        
                        #Key Empty FOV Property
                        if fovNode.KeyCount == 0:
                            fovProp.KeyAt(keyTime)
                
                #Translation Rotation Logic
                else:
                    
                    #Get XYZ SubNodes - and simply add a key if there are missing nodes
                    try:
                        propNodes = currentCamProp.GetAnimationNode().Nodes
                    except AttributeError:
                        currentCamProp.KeyAt(keyTime)
                        continue
                        
                    #Cheack Each SubNode - and key if any empty ones found
                    for eachNode in propNodes:
                        if eachNode.KeyCount == 0:
                            currentCamProp.KeyAt(keyTime)
                            break
        
        #Restore Previous User Layer
        lSystem.CurrentTake.SetCurrentLayer(userLayer)

class AutoLock():
    """ A lock reminder script setup as a callback on saving an FBX.
    Only runs for cam team members when unlocked cams are detected. """
    def __init__(self):
        self.validUserXmlPath = RS.Config.Tool.Path.TechArt + "\\etc\\config\\camera\\camTeam_config.xml"
        
        self.main()

    def createValidUserListFromXml(self):
        validUserList = []
        xmlFile = minidom.parse(self.validUserXmlPath)
        for node in xmlFile.getElementsByTagName("camTeamEmail"):
            tagValue = (str(node.firstChild.nodeValue)).lower()
            validUserList.append(tagValue)
        xmlFile.unlink()
        return validUserList

    def validateUser(self):
        try:
            userEmail = RS.Config.User.Email.lower()
        except:
            return False
        try:
            camTeamEmails = self.createValidUserListFromXml()
        except:
            return False
        if userEmail not in camTeamEmails:
            return False
        else:
            return True
        
    def main(self):
        # Import camKeyTasks - a bit shit, but prevents circular reference
        import RS.Core.Camera.SaveCameras as SaveCameras

        # Validate User First
        userStatus = self.validateUser()
        if userStatus:
            
            # Detected Unlocked Cams
            camLockObj = LockManager()
            if camLockObj.unlockedCount != 0:
                
                # Ask For Lock Save Choice
                messageText = "Unlocked cams detected. Lock or save cam data before saving the FBX?"
                userChoice = FBMessageBox("Cam Data Alert", messageText,
                                          "Lock Cameras", "Lock & Save Cams", "Save FBX Only")

                # Save Key Data - if user chooses
                if userChoice == 2:
                    SaveCameras.Run()

                # Lock Cams - if user chooses
                if userChoice in [1, 2]:
                    camLockObj.setAllCamLocks(True)


def Run():
    """ If triggered via the RS menu, simply detect any locked cams and automatically unlock them. """
    _camLockObj_ = LockManager()
    if _camLockObj_.lockedCount != 0:
        _camLockObj_.setAllCamLocks(False)