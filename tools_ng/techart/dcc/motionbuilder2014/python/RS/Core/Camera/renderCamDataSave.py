from pyfbsdk import *
from difflib import Differ
from xml.dom import minidom
import os
import subprocess
import time

class CreateDataFile():
    def __init__(self, frameNumbers, destFolder):
        self.frameNumbers = frameNumbers
        self.destFolder = destFolder
        
        self.fbxName = 'None'
        self.revNumber = '?'
        self.frameList = []
        self.translationList = []
        self.rotationList = []
        self.fovList = []
        
        self.main()
    
    def filterValues(self, valueList):
        resultsList = []
        for eachValue in valueList:
            resultsList.append("{:.6f}".format(float(eachValue)))
        return resultsList
        
    def getFbxInfo(self):
        self.fbxName = self.destFolder.split("/")[-3]
        
        revInfoXml = self.destFolder[:-8] + "currentRevisionInfo.xml"
        if not os.path.exists(revInfoXml):
            print "WARNING: No rev info file found."
        else:
            xmlFile = minidom.parse(revInfoXml)
            for node in xmlFile.getElementsByTagName('revision'):
                self.revNumber = str(node.firstChild.nodeValue)
        
    def main(self):
        
        self.getFbxInfo()
        
        #BASIC SETUP
        lTime = FBTime()
        lPlayerControl = FBPlayerControl()
        
        lsys = FBSystem()
        lscene = lsys.Scene
        lsys.Scene.Renderer.UseCameraSwitcher = True
        lCameraSwitcher = FBCameraSwitcher()
        
        #CREATE TRIMMED CAM LIST
        cameraList = []
        for camera in lscene.Cameras:
            cameraList.append(camera)
        cameraList = cameraList[6:]
        
        plottedCams = []
        
        for frame in self.frameNumbers:
            lTime.SetFrame( frame )
            lPlayerControl.Goto( lTime )
            
            FBSystem().Scene.Evaluate()
            
            lCurCameraIndex = lCameraSwitcher.CurrentCameraIndex
            lCurCamera = cameraList[lCurCameraIndex]
            
            # PLOT DOWN ALL CAM LAYERS
            if lCurCamera.Name not in plottedCams:
                
                #Select Base Layer
                lSystem = FBSystem()
                lSystem.CurrentTake.SetCurrentLayer(0)
                lSystem.CurrentTake.GetLayer(0).SelectLayer(True, True)
                
                #Select cam properties and plot all down to the base
                lCurCamera.Translation.SetFocus(True)
                lCurCamera.Rotation.SetFocus(True)
                lCurCamera.PropertyList.Find('Field Of View').SetFocus(True)
                lSystem.CurrentTake.MergeLayers(FBAnimationLayerMergeOptions.kFBAnimLayerMerge_AllLayers_SelectedProperties, True, FBMergeLayerMode.kFBMergeLayerModeAdditive)
                
                #Add this camera to our plotted list so we don't do it every frame
                plottedCams.append(lCurCamera.Name)
            
            #print '=== TRANSLATION ==='
            xValue = lCurCamera.Translation[0] / 100
            yValue = -(lCurCamera.Translation[2]) / 100
            zValue = lCurCamera.Translation[1] / 100
            tList = [xValue, yValue, zValue]
            
            #print '==== ROTATION ===='
            rNode = lCurCamera.Rotation.Data
            lVector4d = FBVector4d()
            FBRotationToQuaternion(lVector4d, rNode, FBRotationOrder.kFBYZX)
            lVector3d = FBVector3d()
            FBQuaternionToRotation(lVector3d, lVector4d, FBRotationOrder.kFBXYZ)
            rList = [lVector3d[0], lVector3d[1], lVector3d[2]-90]
            
            #print '==== FOV VALUE ===='
            fNode = lCurCamera.PropertyList.Find('Field Of View').Data
            fList = [ fNode ]
            
            tList = self.filterValues(tList)
            rList = self.filterValues(rList)
            fList = self.filterValues(fList)
            
            if len(str(frame)) < 4:
                if len(str(frame)) == 3:
                    frameNumber = '0' + str(frame)
                elif len(str(frame)) == 2:
                    frameNumber = '00' + str(frame)
                else:
                    frameNumber = '000' + str(frame)
            else:
                frameNumber = str(frame)
            
            self.frameList.append(frameNumber)
            self.translationList.append(str(tList))
            self.rotationList.append(str(rList))
            self.fovList.append(str(fList))
        
        latestDataPath = self.destFolder + "latestSwitcherData.txt"
        previousDataPath = self.destFolder + "previousSwitcherData.txt"
        if os.path.exists(latestDataPath):
            if os.path.exists(previousDataPath):
                os.unlink(previousDataPath)
            os.rename(latestDataPath, previousDataPath)
        
        camDataFile = open(latestDataPath, "w")
        
        camDataFile.write(self.fbxName + " - rev" + self.revNumber + "\n")
        
        camDataFile.write("Track[0]:\ttrack 7 id 0 type 0 cameraTranslation\n")
        for i in range(len(self.translationList)):
            filteredName = ((self.translationList[i]).replace(',', ''))[1:-1]
            filteredName = filteredName.replace("'", '')
            #camDataFile.write("Value[" + str(i) + "]:\t" + filteredName + "\n")
            camDataFile.write(self.frameList[i] + ":\t" + filteredName + "\n")
            
        camDataFile.write("\nTrack[1]:\ttrack 8 id 0 type 1 cameraRotation\n")
        for i in range(len(self.rotationList)):
            filteredName = ((self.rotationList[i]).replace(',', ''))[1:-1]
            filteredName = filteredName.replace("'", '')
            #camDataFile.write("Value[" + str(i) + "]:\t" + filteredName + "\n")
            camDataFile.write(self.frameList[i] + ":\t" + filteredName + "\n")
            
        camDataFile.write("\nTrack[2]:\ttrack 27 id 0 type 2 cameraFOV\n")
        for i in range(len(self.fovList)):
            filteredName = str(self.fovList[i])[2:-2]
            #camDataFile.write("Value[" + str(i) + "]:\t" + filteredName + "\n")
            camDataFile.write(self.frameList[i] + ":\t" + filteredName + "\n")
        camDataFile.close()

class ReadDataFile():
    def __init__(self, dataFilePath):
        self.dataFilePath = dataFilePath
        
        self.fbxName = 'None'
        self.revNumber = '?'
        self.frameList = []
        self.translationList = []
        self.rotationList = []
        self.fovList = []
        
        self.main()
        
    def convertLineToSimpleList(self, lineString):
        colonPosition = lineString.index(":") + 2
        currentLine = (lineString)[colonPosition:-1]
        rawValueList = currentLine.split(" ")
        simpleValueList = []
        for eachValue in rawValueList:
            simpleValueList.append("{:.2f}".format(float(eachValue)))
        return simpleValueList
    
    def main(self):
        if not os.path.exists(self.dataFilePath):
            print 'ERROR: No cam data file found at below path'
            print self.dataFilePath
        else:
            currentType = None
            camDataFile = open(self.dataFilePath, "r")
            lineList = camDataFile.readlines()
            for i in range(len(lineList)):
                
                if " - rev" in (lineList[i]):
                    dashPosition = lineList[i].index("-")
                    self.fbxName = (lineList[i])[:dashPosition-1]
                    self.revNumber = (lineList[i])[dashPosition+5:-1]
                
                
                if lineList[i].endswith("cameraTranslation\n"):
                    currentType = "translation"
                    continue
                if lineList[i].endswith("cameraRotation\n"):
                    currentType = "rotation"
                    continue
                if lineList[i].endswith("cameraFOV\n"):
                    currentType = "fov"
                    continue
                if len(lineList[i]) < 10:
                    currentType = None
                    continue
                if lineList[i].endswith("cameraDOF\n"):
                    break
                    
                
                if currentType != None:
                    simpleValueList = self.convertLineToSimpleList(lineList[i])
                    if currentType == "translation":
                        self.translationList.append(simpleValueList)
                        frameNumber = (lineList[i])[:4]
                        self.frameList.append(frameNumber)
                    if currentType == "rotation":
                        self.rotationList.append(simpleValueList)
                    if currentType == "fov":
                        self.fovList.append(simpleValueList)
                        
            camDataFile.close()

class CompareDataObjects():
    def __init__(self, latestDataObj, previousDataObj):
        self.latestDataObj = latestDataObj
        self.previousDataObj = previousDataObj
        
        self.uniqueLatestFrames = []
        self.uniquePreviousFrames = []
        self.matchingFrames = []
        
        self.translationChanges = []
        self.rotationChanges = []
        self.fovChanges = []
        
        self.main()
    
    def checkForUniqueFrames(self):
        for d in Differ().compare(self.latestDataObj.frameList, self.previousDataObj.frameList):
            
            #EXTRA FILTERING - Added some extra filtering, some complicated re-edits were generating multiple diff lines per frame.
            d = d.split("\n")[0]
            if d.startswith("?"):
                continue
            
            if "-" in d:
                self.uniqueLatestFrames.append(d[2:])
                lastD = "-"

            elif "+" in d:
                self.uniquePreviousFrames.append(d[2:])
                lastD = "+"
                
            else:
                self.matchingFrames.append(d[2:])
                
        if (len(self.uniqueLatestFrames) > 0) or (len(self.uniquePreviousFrames) > 0):
            print "WARNING: Frame differences found between revisions"
            print "Unique to latest fbx:"
            print self.uniqueLatestFrames
            print "Unique to previous fbx:"
            print self.uniquePreviousFrames
    
    def writeOutCompareFile(self):
        # SWITCHEROO - I've updated the section so we now see previous rev on the left and latest rev on the right, to match the before > after layout of the videos.
        compareFilePath = str(os.path.split(self.latestDataObj.dataFilePath)[0:-1])[2:-3] + "\\comparison.txt"
        compareFile = open(compareFilePath, "w")
        
        compareFile.write(self.latestDataObj.fbxName + " - Rev" + self.previousDataObj.revNumber + " vs Rev" + self.latestDataObj.revNumber + "\n")
        
        if len(self.uniquePreviousFrames) > 0:
            compareFile.write("FRAMES UNIQUE TO PREVIOUS FBX\n")
            for eachFrame in self.uniquePreviousFrames:
                compareFile.write(eachFrame + "\n")
            compareFile.write("\n")
        
        if len(self.uniqueLatestFrames) > 0:
            compareFile.write("FRAMES UNIQUE TO LATEST FBX\n")
            for eachFrame in self.uniqueLatestFrames:
                compareFile.write(eachFrame + "\n")
            compareFile.write("\n")
        
        if len(self.translationChanges) > 0:
            compareFile.write("TRANSLATION CHANGES\n")
            for eachString in self.translationChanges:
                compareFile.write(eachString + "\n")
            compareFile.write("\n")
        
        if len(self.rotationChanges) > 0:
            compareFile.write("ROTATION CHANGES\n")
            for eachString in self.rotationChanges:
                compareFile.write(eachString + "\n")
            compareFile.write("\n")
        
        if len(self.fovChanges) > 0:
            compareFile.write("FOV CHANGES\n")
            for eachString in self.fovChanges:
                compareFile.write(eachString + "\n")
            compareFile.write("\n")
            
        if (len(self.uniqueLatestFrames) == 0) and (len(self.uniquePreviousFrames) == 0):
            compareFile.write("REVISIONS EDIT FRAMES ARE IDENTICAL.")
            
        if (len(self.translationChanges) == 0) and (len(self.translationChanges) == 0) and (len(self.translationChanges) == 0):
            compareFile.write("REVISIONS ANIM DATA ARE IDENTICAL.")
        
        compareFile.close()
        
                
    def main(self):
        self.checkForUniqueFrames()
        
        for eachFrame in self.matchingFrames:
            previousIndex = self.previousDataObj.frameList.index(eachFrame)
            latestIndex = self.latestDataObj.frameList.index(eachFrame)
            
            previousFrameTranslation = self.previousDataObj.translationList[previousIndex]
            latestFrameTranslation = self.latestDataObj.translationList[latestIndex]
            for d in Differ().compare(previousFrameTranslation, latestFrameTranslation):
                if d.startswith("-"):
                    changeString = eachFrame + ":\t" + "Previous-\t" + str(previousFrameTranslation) + "\tLatest-\t" + str(latestFrameTranslation)
                    self.translationChanges.append(changeString)
                    break
                  
            
            previousFrameRotation = self.previousDataObj.rotationList[previousIndex]
            latestFrameRotation = self.latestDataObj.rotationList[latestIndex]
            for d in Differ().compare(previousFrameRotation, latestFrameRotation):
                if d.startswith("-"):
                    changeString = eachFrame + ":\t" + "Previous-\t" + str(previousFrameRotation) + "\tLatest-\t" + str(latestFrameRotation)
                    self.rotationChanges.append(changeString)
                    break
                    
            previousFrameFov = self.previousDataObj.fovList[previousIndex]
            latestFrameFov = self.latestDataObj.fovList[latestIndex]
            for d in Differ().compare(previousFrameFov, latestFrameFov):
                if d.startswith("-"):
                    changeString = eachFrame + ":\t" + "Previous-\t" + str(previousFrameFov) + "\tLatest-\t" + str(latestFrameFov)
                    self.fovChanges.append(changeString)
                    break
            
        self.writeOutCompareFile()    

def updateTimeLog(headerText, destFolder):
    localtime = time.localtime()
    timeString = time.strftime("%H:%M:%S", localtime)
    timeCode = time.time()
    timeLogPath = destFolder + "timeLog.txt"
    
    timeLog = open(timeLogPath, "r")
    currentText = timeLog.readlines()
    timeLog.close()
    
    timeLog = open(timeLogPath, "w")
    for eachLine in currentText:
        timeLog.write(eachLine)
    timeLog.write(headerText + ":\n")
    timeLog.write(timeString + "\n")
    timeLog.write(str(timeCode) + "\n")
    timeLog.close()