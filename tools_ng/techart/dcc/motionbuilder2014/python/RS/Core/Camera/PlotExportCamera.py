'''
   Script Name: rs_PlotExportCamera.py
   Re-written by: Mark Harrison-Ball
   Inital Version: - David Bailey
   Contributors: - Kathryn Bodey
   Description: This tool plots animation from switcher to ExportCamera

'''

'''
 TODOS:
 * Might need to set each property on each camera to Animatable before plottinng. Investigate
'''

from pyfbsdk import *

import RS.Globals as glo


import RS.Perforce
import time
import os

import RS
import RS.Core
import RS.Utils.Scene.Take





'''
         Description: Globals
'''


# WIll use a dicitoanry to hold the values if fins when plotting the camera swticher
# then we go through the dicaitoanry and plot back the attributes

debugLog = False


# Simple logger class
class logger:
    def __init__(self):
        self.filename = "{0}/tools/logs/CamPlot.log".format( RS.Config.Project.Path.Root )
        self.f = open(self.filename, 'w') 
        
    def write(self, sString):
        self.f.write(sString+"\n")
        
    def close(self):
        self.f.close()
    
if debugLog:
    log = logger()

# Private Reader Class  
class CamKeyReadDef: 
    # Class globals
    globalCamProperties = ("DiffuseColor",
                           "Transparency",
                           "Field Of View",
                           "Roll",
                           "FOCUS",
                           "DOF Expand",
                           "DOF Multiplier",
                           "DOF Strength",
                           "DOF Strength Override",
                           "Shallow_DOF",
                           "Motion_Blur",
                           "DOF NearPlane Override",
                           "DOF FarPlane Override",
                           "Translation",
                           "Rotation",
                           "Simple_DOF",
                           "CoC",
                           "CoC Override",
                           "CoC Night",                           
                           "Coc Night Override",
                           "Near Outer Override",
                           "Near Inner Override",
                           "Far Inner Override",
                           "Far Outer Override",
                           "focus (CM)")
    
    def __init__(self, lCamera, lScreenFadeMat, keyframe):
        self.lTimeFrame = keyframe
        self.lCam = lCamera
        self.lScreenFadeMat = lScreenFadeMat
        self.PropertyName = None
        self.lDict = dict()  # Stores a Key, Value pair for each property
        self.__collectKeys__()        
    
    def __collectKeys__(self):
        for lPropertyType in self.globalCamProperties:
            self.PropertyName = lPropertyType
            if lPropertyType == "Translation":self.lDict[lPropertyType] = self.__Translation__()                
            elif lPropertyType == "Rotation":
                self.lDict[lPropertyType] = self.__Rotation__()
            elif lPropertyType == "DiffuseColor":
                self.lDict[lPropertyType] = self.__Material__()
            elif lPropertyType == "Transparency":self.lDict[lPropertyType] = self.__Material__()               
            else:self.lDict[lPropertyType] = self.__Number__()
        return self.lDict
        
                
    def __Translation__(self, X = 0.0, Y = 0.0, Z = 0.0):
        if (self.lCam.Interest):
	    if self.lCam.Interest.Translation.GetAnimationNode():
		X = self.lCam.Interest.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
		Y = self.lCam.Interest.Translation.GetAnimationNode().Nodes[1].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
		Z = self.lCam.Interest.Translation.GetAnimationNode().Nodes[2].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
        else: 
	    if self.lCam.Translation.GetAnimationNode():
		X = self.lCam.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
		Y = self.lCam.Translation.GetAnimationNode().Nodes[1].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
		Z = self.lCam.Translation.GetAnimationNode().Nodes[2].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
        if debugLog:
            log.write("\tReading (%s) Translation X: %s Y: %s Z: %s" % (self.lTimeFrame, X, Y, Z))
        return [X,Y,Z]
    
       
    
        
    def __Rotation__(self, X = 0.0, Y = 0.0, Z = 0.0): 
        if (self.lCam.Rotation.GetAnimationNode()):
	    X = self.lCam.Rotation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
	    Y = self.lCam.Rotation.GetAnimationNode().Nodes[1].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
	    Z = self.lCam.Rotation.GetAnimationNode().Nodes[2].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))  
        if debugLog:
            log.write("\tReading (%s) Rotation X: %s Y: %s Z: %s" % (self.lTimeFrame, X, Y, Z))
        return [X,Y,Z]
    
    def __Number__(self):
        Value = self.__ReadProperty__()
        if debugLog:
            log.write("\tReading (%s) Number %s  = %s" % (self.lTimeFrame, self.PropertyName, Value))
        return Value
        
    # Crazy shit to only get return true if there is a actual set key
    # We do this for  setting the Screen Fade as requires 2 keys (in and out)
    def __bHasKey__(self, AnimNode):
        for Animkey in AnimNode.FCurve.Keys:
            if int(Animkey.Time.GetTimeString().replace("*", "")) == self.lTimeFrame:
                return True        
        return False
    
    # Read a material Property RGB Values
    def __Material__(self):
        value = None
        if (self.lScreenFadeMat):
            mat = self.lScreenFadeMat.PropertyList.Find(self.PropertyName)
            if (mat):
                if mat.IsAnimated(): 
                    if (self.__bHasKey__(mat.GetAnimationNode().Nodes[0])): # CHeck the first key as only want in and out                    
                        R = mat.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
                        G = mat.GetAnimationNode().Nodes[1].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
                        B = mat.GetAnimationNode().Nodes[2].FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
                        value = [R,G,B]
        return value
        
    # Read a property, if its valid return its value
    def __ReadProperty__(self):
        value = None
        hasProp = self.lCam.PropertyList.Find(self.PropertyName)
        if (hasProp):
            if hasProp.IsAnimated() == True:                
                value = hasProp.GetAnimationNode().FCurve.Evaluate(FBTime(0,0,0,(self.lTimeFrame)))
                #print "Is %s Animated: %s" % (self.PropertyName, value)
        return value
    
    
    
 
# Private Writer class  
class CamKeyWriteDef:
    def __init__(self, lCamera, lScreenFadeMat, CamKeyDef):
        self.keyframe = FBTime(0,0,0,CamKeyDef.lTimeFrame)
        self.lDict = CamKeyDef.lDict
        #print self.lDict["Field Of View"]
        self.lCam = lCamera
        self.lScreenFadeMat = lScreenFadeMat
        self.__WriteKeyInfo__()
        return
    
    def __WriteKeyInfo__(self):
        for key in self.lDict:            
            if key == "Translation":
                self.__WriteTranslation__(self.lDict[key])
            elif key == "Rotation":
                self.__WriteRotation__(self.lDict[key])  
            elif key == "DiffuseColor":
                self.__WriteMaterial__(key, self.lDict[key])  
            elif key == "Transparency":
                self.__WriteMaterial__(key, self.lDict[key])  
            else:
                self.__WriteNumber__(key, self.lDict[key]) 
            
                
    def __WriteTranslation__(self, key): 
        if self.lCam.Interest:  # Not sure if this will ever be the case
            for i in range(3):
		self.lCam.Interest.Translation.GetAnimationNode().Nodes[i].KeyAdd(self.keyframe, key[i])            
        else:        
            for i in range(3):
		self.lCam.Translation.GetAnimationNode().Nodes[i].KeyAdd(self.keyframe, key[i])
            
    def __WriteRotation__(self, key):
        for i in range(3):
	    self.lCam.Rotation.GetAnimationNode().Nodes[i].KeyAdd(self.keyframe, key[i]) 
	    
		
		
	    
	    
        
    def __WriteNumber__(self, key, value):
        if (value!=None):
            hasProp = self.lCam.PropertyList.Find(key)
            if (hasProp):
                hasProp.GetAnimationNode().KeyAdd(self.keyframe, value)
                if debugLog:
                    log.write("\tWriting Property %s Value: %s to %s camera" % (key, value, self.lCam.Name))
     
    # Only plot if there was a key            
    def __WriteMaterial__(self, key, value):
        if (self.lScreenFadeMat):
            if (value): # ONly plot if we have a value
                mat = self.lScreenFadeMat.PropertyList.Find(key)
                if (mat):
                    for i in range(3):
                        mat.GetAnimationNode().Nodes[i].KeyAdd(self.keyframe, value[i])   
         
         
class CamPlot:
    def __init__(self):
        self.lArray = []
        self.lCamera = None # Used to track what camera we are on
        self.lExportCamera = None
        self.lCameraList = []
        self.lTimeArray = []
        self.lValueArray = []        
        self.ScreenFadeMaterial = None        
        self.TimeDict = dict()
        
        self.bSilent = False
	self.bPlotAllTakes = False
        
       
       
     
    def GetScreenFadeMaterial(self, iCam):
        lMaterial = None
        for iChild in iCam.Children:
            if "ScreenFade" in iChild.Name:
                for iComp in iChild.Components:
                    if iComp.ClassName() == "FBMaterial":
                        lMaterial = iComp
                        lMaterial.PropertyList.Find("Transparency").SetAnimated(True)
                        lMaterial.PropertyList.Find("DiffuseColor").SetAnimated(True)                        
                        
        return lMaterial

    def GetCameraList(self):
        self.lCameraList = []
        for iCamera in glo.gCameras:
            if not "Producer" in iCamera.Name:
                self.lCameraList.append(iCamera) 
                
     
    # Record the Key properties for teh frame length         
    def recordKeys(self, i, lSwitchValue):
        
        # Get our Fade Material for the camera
        lScreenFadeMat = self.GetScreenFadeMaterial(self.lCamera)

        for iFrame in range((int(FBTime.GetTimeString(self.lTimeArray[i + 1])) - lSwitchValue)):
            lTimeFrame = (iFrame + lSwitchValue)            
            if debugLog:
                log.write("\nRecording Frame %s length %s for Camera: %s at Time %s on Layer:%s" % (iFrame, lSwitchValue , self.lCamera.Name, lTimeFrame, self.lLayer.Name))

            # Get our Property Keyframe values, bit crappy to add the screen the screen fade mat
            lCamKeyFrame = CamKeyReadDef(self.lCamera, lScreenFadeMat, lTimeFrame) 

            # Lets add our list of recorded properties to our keyframe Array
            self.lArray.append(lCamKeyFrame)   
            
    
    # Write our camera proeprteis back to the export Cam    
    def writeKeys(self, KeyDicInfo): 

        
        # Get our Fade Material for the current camera
        lScreenFadeMat = self.GetScreenFadeMaterial(self.lExportCamera)  
        
        CamKeyWriteDef(self.lExportCamera, lScreenFadeMat, KeyDicInfo)        
        
        
     
    # THis will got through each Camera in the switcher   
    def ReadSwitcherKeys(self):
	lCameraTimeRange = RS.Utils.Scene.Take.GetLocalTimeRange( FBSystem().CurrentTake )
	
	lStartFrame = lCameraTimeRange.Start
	lEndFrame = lCameraTimeRange.End
	        
        for iKey in self.lSwitcherKeys:
            
            self.lTimeArray.append(iKey.Time)
            self.lValueArray.append(iKey.Value)
        
        self.lTimeArray.append(lEndFrame) 

        
        self.TimeDict[lEndFrame] = 1
        lSwitchValue = 0
        
        
        # Go through each Camera in the switcher for the entire frame range        
        for i in range(len(self.lValueArray)):
            # Get each camera in the switcher       
            self.lCamera = self.lCameraList[int(self.lValueArray[i]) - 1]          
           
            # Record the keys for that camera
            self.recordKeys(i,lSwitchValue)
            
            # Step trhough to the next CAMERA
            lSwitchValue = int(FBTime.GetTimeString(self.lTimeArray[i + 1]))   
        
        # Write back key info back   
        self.lExportCamera.PropertyList.Find("Roll").SetAnimated(True)
	
	if len( self.lArray ) == 0:
	    if not self.bSilent and not self.bPlotAllTakes:    
		FBMessageBox( 'Rockstar', 'The camera switcher is empty!  Cannot plot.', 'OK' )		
		
	    glo.gUlog.logError( 'Camera switcher is empty!', context = 'Plotting Camera' )
	    
	    return False	
        
        lFrameRange = (int(FBTime.GetTimeString(lEndFrame)) - int(FBTime.GetTimeString(lStartFrame))) - 1  
        for iFrame in range(lFrameRange):
            idx = (iFrame + int(FBTime.GetTimeString(lStartFrame)))
            time = FBTime(0,0,0,iFrame) 
            
            if debugLog:
                log.write("\nWriting Frame %s length %s for Camera: %s at Time %s on Layer %s" % (iFrame, lFrameRange , self.lExportCamera.Name, time, self.lLayer.Name))
            

	    # Plot are keys back
            self.writeKeys(self.lArray[idx])
	    
	return True
            
            
        
    # This is called for each layer we find!   
    def rs_PlotExportCamera(self):
        self.lValueArray = []
        self.lTimeArray = []
        self.lArray = []
        # Collect our cameras
        self.GetCameraList()
        # Collect our switcher keys
        return self.ReadSwitcherKeys()        
        
        
    def rs_PlotMultiLayerExportCamera(self, bSilent = False, bPlotAllTakes = False):
        self.bSilent = bSilent
	self.bPlotAllTakes = bPlotAllTakes
        print "Starting Plot"
        
        # Deselct everything      
        RS.Utils.Scene.DeSelectAll()
        
        self.lExportCamera = FBFindModelByLabelName("ExportCamera")  
        if not (self.lExportCamera):
            print "NO Export Camera Found"
            return False, None
	Overlays = {}        
	# This is a weird one, we want to maintain the custom property keys of the the Export Camera > Overlay, however we don't want the rest of the properties 
	# to have animation, so first we need to save the key values on the Overlay property so we can put them back after.
	for lOverlayProp in self.lExportCamera.PropertyList:
	    if lOverlayProp.Name.endswith(':Overlay'):
		OverlayKeys = {}
		if lOverlayProp.IsAnimatable ():
		    lCameraOverlayPropAnimationNode = lOverlayProp.GetAnimationNode ()
		    # First let's see if the node has any keys
		    if lCameraOverlayPropAnimationNode.FCurve:
			
			for lKey in lCameraOverlayPropAnimationNode.FCurve.Keys:
			    OverlayKeys[lKey.Time.GetTimeString ()] = lKey.Value	
			Overlays[lOverlayProp.Name] = OverlayKeys
	
        self.lExportCamera.Selected = True	        

	# Removed all the animation from the Export Camera Properties, so that it is a blank canvas for the plot of the switcher to the Export Camera
        FBSystem().CurrentTake.ClearAllProperties(True)
        FBSystem().Scene.Evaluate()
        self.lExportCamera.Selected = False
        
        FBSystem().CurrentTake.SetCurrentLayer(0)  
        
        lPlayerControl =  FBPlayerControl()
        lPlayerControl.SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames 
    
        lCameraSwitcher = FBCameraSwitcher()
        self.lSwitcherKeys = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys
        
        
        # Add our camera keys time to thr check array  
        lCheckTimeArray = []
        for iKey in self.lSwitcherKeys:
            lCheckTimeArray.append(iKey.Time)        
        
    
        # [CHECK] GO through our time checktime array and see if any keys start with * indicating part keyframe placement          
        lBool = True
        for iTime in lCheckTimeArray:   
            if "*" in FBTime.GetTimeString(iTime):
                if not (self.bSilent) and not (self.bPlotAllTakes):
                    FBMessageBox('ERROR', 'The Switcher has keys placed on Part Frames (these show up as a number with an asterisk at the end e.g. 194*).\nYou will need to notify the camera team to get this changed, before proceeding to plot. All keys should be on full frames.', 'Ok')
                lBool = False        

        
        # If ok, continue
        if lBool == True:            
            for i in range(FBSystem().CurrentTake.GetLayerCount()): # Go through each layer
                FBSystem().CurrentTake.SetCurrentLayer(i)      
                # [CHECK] 
                lBool = False            
                
                self.lLayer = FBSystem().CurrentTake.GetLayer(i)  
                if debugLog:
                    log.write("Plotting down Cameras on Layer: %s" % self.lLayer.Name)
                
                # Find if the layer is locked and unlock until we have finished plotting
                if self.lLayer.PropertyList.Find("Lock").Data == True:
                    self.lLayer.PropertyList.Find("Lock").Data = False
                    lBool = True   
                    
                # Main Plot for switcher keys
                
                if not self.rs_PlotExportCamera():
		    return False, None
		
               
                
                # Unlock our layer if it was locked earlier
                if lBool == True:
                    self.lLayer.PropertyList.Find("Lock").Data = True
            
		FBSystem().Scene.Evaluate()
		 
            # Deselct everything        
            RS.Utils.Scene.DeSelectAll()
            
            # Select our camera
            # We need to select all planes
            self.lExportCamera.Selected = True
            for iChild in self.lExportCamera.Children:
                if "ExportCamera NearPlane" in iChild.Name:
                    iChild.Selected = True
                if "ExportCamera FarPlane" in iChild.Name:
                    iChild.Selected = True
		if "ExportCamera DOF_Strength_FarPlane" in iChild.Name:
		    iChild.Selected = True
		if "ExportCamera DOF_Strength_NearPlane" in iChild.Name:

		    iChild.Selected = True		
                FBSystem().Scene.Evaluate()
	    
            #plotting cam again to ensure the properties run via relation constraint become plotted down as well.
            FBSystem().CurrentTake.PlotTakeOnSelected(FBTime( 0, 0, 0, 1 ))
            
 
            return True, Overlays
                
       
        return False, None
        
class rs_PlotCameras():
    def __init__(self):
        self.CamPlot = CamPlot()
        self.bSilent = False
	self.bPlotAllTakes = False
    # Turn Stroy Mode Off when Plotting and tehn back on\
    # Check if user has stroy mode on and store as condition
    def SwitchStoryMode(bEnabled):
        lStory = FBStory() 
        lStory.Mute = True

    # Set frame start to 0
    def setValidFrameRange(self,START_FRAME):
        FBPlayerControl().LoopStart = FBTime(0,0,0,START_FRAME)
        return
        
    def plotCameras(self, bSilent=False, bPlotAllTakes=False):
	tstart = time.clock()
	# Seems sorty mode needs to be on and in actionmode mode        
	# Check if Stroy Mode is Enabled
	lStory = FBStory()
	bStoryWasEnabled = False
	if lStory.Mute == False:            
	    lStory.Mute = True
	    bStoryWasEnabled = True
	    if not self.bSilent:
		FBMessageBox("Camera Plot", "Story Mode is on, this will be disabled while the camera is plotted", "Continue")	
		

	lSystem = FBSystem()
	
	result = False
	
	if bPlotAllTakes:
	    for iTake in RS.System.Scene.Takes:
		RS.System.CurrentTake = iTake
		result = self.plotTake(bSilent, bPlotAllTakes)
	else:  	
	    result = self.plotTake(bSilent, bPlotAllTakes)
	    
	
	# Reset back story mode
	if bStoryWasEnabled:
	    lStory.Mute = False
	
	    
	tend = time.clock()
	ttotal = (tend-tstart)#/100
	print("Finished, Process took %0.3f seconds" % (ttotal))     	
	    
	if not self.bSilent :
	    if (result):
		FBMessageBox("Export Camera Plot", "Plot Complete", "OK")
		
		

    def plotTake(self, bSilent, bPlotAllTakes):
	lStartHardRange = None
	lEndHardRange = None
	
        self.bSilent = bSilent
	self.bPlotAllTakes = bPlotAllTakes


        # Check if Frame Range starts at 0
	lStartHardRange = int(FBPlayerControl().LoopStart.GetTimeString().replace("*", ""))
	
        if FBPlayerControl().LoopStart.GetTimeString() != "0":
            self.setValidFrameRange(0)
	    print self.bPlotAllTakes
            #if self.bSilent == False:
            #    FBMessageBox("Camera Plot", "Frame Range does not start at 0, this will be set to 0 before plotting", "Continue")	    
	    if not self.bPlotAllTakes and not self.bSilent:
		FBMessageBox("Camera Plot", "Frame Range does not start at 0, this will be set to 0 before plotting", "Continue")
	
            
	print 'bPlotAllTakes', bPlotAllTakes
        result, Overlays = self.CamPlot.rs_PlotMultiLayerExportCamera(self.bSilent, self.bPlotAllTakes )
        if debugLog:
            log.close()
	    
	result = True
                
        FBPlayerControl().GotoStart()
	FBSystem().CurrentTake.SetCurrentLayer(0) # Set back to base layer
	
	# Set back the Start Hard Range
	if lStartHardRange:
	    FBPlayerControl().LoopStart = FBTime(0,0,0,lStartHardRange)
        
	self.lExportCamera = FBFindModelByLabelName("ExportCamera")  
	if not (self.lExportCamera):
	    print "NO Export Camera Found"
	else:
	    #Putting the keys back on the Overlay properties after we plotted everything correctly
	    if Overlays != None:
		for lOverlayProp in self.lExportCamera.PropertyList:
		    for keyi, valuei in Overlays.iteritems():
			if keyi == lOverlayProp.Name:
			    print keyi, lOverlayProp.Name
			    if lOverlayProp.IsAnimatable ():			    
				lOverlayProp.GetAnimationNode().FCurve. EditClear ()                
				if Overlays != None:
				    for key, value in valuei.iteritems(): 
					lOverlayProp.GetAnimationNode().KeyAdd(FBTime(0,0,0,int(key)),value)
				else:
				    print "The 'Overlay' property keys were not retrieved properly, so they were not set"
			
   
	return result
		
		
            



# Class Initalized
rs_PlotCameras = rs_PlotCameras()

# Debug Only
#rs_PlotCameras.plotCameras()




# Key for compatability
def rs_PlotMultiLayerExportCamera(pControl, pEvent):
    rs_PlotCameras.rs_PlotMultiLayerExportCamera()
    

    