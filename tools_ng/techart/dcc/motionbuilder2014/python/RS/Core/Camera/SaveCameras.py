"""
Description:
    A wrapper for the camKeyTasks.SaveAndSubmitCameras function.
    Needed to avoid module conflicts when unpickling on the server.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

from RS.Core.Camera.KeyTools import *
import camKeyTasks


def Run():
    """
    Runs the camKeyTasks.SaveAndSubmitCameras function.
    Also put it in a try except just to be extra safe.
    Returns:
        A bool of the results - True for success, False otherwise.
    """

    try:
        results = camKeyTasks.SaveAndSubmitFbxCams()
    except:
        results = False
    return results