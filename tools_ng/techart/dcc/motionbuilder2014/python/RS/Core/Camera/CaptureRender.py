import os
from datetime import datetime
from xml.dom import minidom
import pyfbsdk as mobu

import RS.Config
import RS.Perforce
from RS.Core.Camera import CamValidationTools, SaveCameras, Lib as CamLib
from RS.Core.Automation.FrameCapture import CaptureModel, CapturePaths, FileTools, JobDbTools


def CheckCamRigs():
    camRigNames = []
    switcherCams = [cam.camera for cam in CamLib.Manager.GetSwitcherCameras()]
    print [cam.Name for cam in switcherCams]
    for camera in switcherCams:
        if camera.Parent and camera.Parent.LongName.lower().startswith("mc_"):
            camRigName = camera.Parent.LongName.split(":")[0]
            camRigNames.append("{0} : {1}".format(camera.Name, camRigName))
    return camRigNames


def QueueCaptureRender():
    # Get Job Paths and Data
    fbxPath = mobu.FBApplication().FBXFileName
    fbxName = os.path.splitext(os.path.basename(fbxPath))[0].upper()
    capPaths = CapturePaths.FastPath(fbxPath)
    projectName = capPaths.project
    capDataRoot = capPaths.capDataRoot
    dataCutXmlPath = os.path.join(capDataRoot, "CutFiles", "data.cutxml")
    dataLogFolder = os.path.join(capDataRoot, "LogFiles")
    jobXmlPath = os.path.join(capPaths.expJobQueue, "{}.xml".format(fbxName))
    userEmail = RS.Config.User.Email
    userName = userEmail.lower().split("@")[0].replace(".", " ").title()

    # Hacky GTA Fix
    if fbxPath.lower().startswith("x:\\gta5"):
        projectName = "GTA5"
        captureRoot = capPaths.captureRoot
        jobXmlPath = "{0}\\ExportQueue\\GTA5\\{1}.xml".format(captureRoot, fbxName)
        capDataRoot = "{0}\\CaptureData\\GTA5\\{1}".format(captureRoot, fbxName)
        dataCutXmlPath = os.path.join(capDataRoot, "CutFiles", "data.cutxml")
        dataLogFolder = os.path.join(capDataRoot, "LogFiles")

    # Detect Valid FBX
    fbxStatus = CamValidationTools.CheckFbxPath(fbxPath)
    if fbxStatus != "VALID":
        messageText = "Invalid FBX: " + fbxStatus
        mobu.FBMessageBox("Capture Render", messageText, "Okay")
        return "ERROR"

    # Detect Old EndFrame
    oldEndFrame = None
    if os.path.exists(dataLogFolder):
        jobXmls = [logFile for logFile in os.listdir(dataLogFolder) if os.path.splitext(logFile)[1].lower() == ".xml"]
        if jobXmls:
            jobRev = max([int(logFile.split("Capture")[1].split(".")[0]) for logFile in jobXmls])
            oldJobXml = os.path.join(dataLogFolder, "Capture{}.xml".format(jobRev))
            oldCap = CaptureModel.MobuCaptureModel(oldJobXml)
            oldEndFrame = getattr(oldCap, "endFrame", None)
            oldEndFrame = None if oldEndFrame.lower() == "none" else oldEndFrame

    # Check for EndFrame Change
    endFrame = str(mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame())
    if oldEndFrame and (int(oldEndFrame) != int(endFrame)):
        messageText = ["Altered endframe detected. Do you want to use the old endrange (",
                       oldEndFrame, ") or the new endrange (", endFrame, ")?\n\n\t",
                       "Warning: Changing endrange will break batch import."]
        messageText = "".join(messageText)
        userChoice = mobu.FBMessageBox("Capture Sender", messageText, "Keep Old Endrange", "Use New Endrange")
        if userChoice == 1:
            endFrame = oldEndFrame

    # Check for Cam Rigs - RDR3 only for now
    captureType = "FULL"
    if projectName == "RDR3":
        camRigs = CheckCamRigs()
        if camRigs:
            camRigs = camRigs[:10] if len(camRigs) > 10 else camRigs
            boxMessage = "Cameras are attached to a moving cutscene rig.\n\n"
            boxMessage += "    {0}\n\n".format("\n    ".join(camRigs))
            boxMessage += "  Would you like to capture using the cam rig?"
            userChoice = mobu.FBMessageBox("Capture Sender", boxMessage,
                                           "CAM RIG", "NORMAL", "CANCEL")
            if userChoice == 1:
                boxMessage = ("Cam Rig captures require the FBX to be saved submitted to P4 manually.\n\n"
                              "        Have you saved and finished submitting your latest changes?")
                hasSaved = mobu.FBMessageBox("Capture Sender", boxMessage, "YES", "NO")
                if hasSaved == 1:
                    captureType = "SIMPLE"
                else:
                    return
            elif userChoice == 3:
                return

    # Check for Fast Capture
    if captureType == "FULL" and oldEndFrame == endFrame and os.path.exists(dataCutXmlPath):
        lastCaptureTime = datetime.fromtimestamp(os.path.getctime(dataCutXmlPath)).strftime('%m/%d/%Y %H:%M:%S')
        userChoice = mobu.FBMessageBox("Capture Sender", "               SELECT CAPTURE TYPE\n\n" +
                                       "Full Capture - Latests cams & animations\n" +
                                       "Fast Capture - Latest cams only\n\n" +
                                       "Last full capture on " + lastCaptureTime,
                                       "FULL CAP", "FAST CAP", "CANCEL")
        if userChoice == 2:
            captureType = "FAST"
        elif userChoice == 3:
            return

    # Delete Existing Queue Xml
    FileTools.DeletePath(jobXmlPath)

    # Set Tag Lists of Saved Data
    tagNameList = ["projectName", "fbxName", "fbxPath", "endFrame", "captureType", "userEmail"]
    tagValueList = [projectName, fbxName, fbxPath, endFrame, captureType, userEmail]

    # Create Job on Database - and only save jobId if job was created
    jobId = None
    try:
        jobId = JobDbTools.CreateDbJob(fbxName, userName, fbxPath)
    except:
        print "Warning: Problem connecting to database. Skipping user-side job creation."
    if jobId:
        tagNameList.append("jobId")
        tagValueList.append(str(jobId))

    # Check Production Drive Access
    if not os.path.exists(capPaths.expJobQueue):
        mobu.FBMessageBox("Capture Render",
                          "Error: The job folder on the production drive could not be accessed.", "Okay")
        return "ERROR"

    # Create Job Xml From Tag Lists
    doc = minidom.Document()
    fbxInfo = doc.createElement("fbxInfo")
    doc.appendChild(fbxInfo)
    for i in range(len(tagNameList)):
        tagName = tagNameList[i]
        tagValue = tagValueList[i]
        if tagValue:
            tagNode = doc.createElement(tagName)
            textNode = doc.createTextNode(tagValue)
            tagNode.appendChild(textNode)
            fbxInfo.appendChild(tagNode)

    # Write Out Formatted Xml
    docPretty = doc.toprettyxml(indent="    ", encoding="utf-8")
    xmlFile = open(jobXmlPath, "w")
    xmlFile.write(docPretty)
    xmlFile.close()

    mobu.FBMessageBox("Capture Render",
                      "Your capture has been queued, and you will receive an email when finished.", "OK")
    return "SUCCESS"


def Run():
    camDataSaved = SaveCameras.Run()
    if camDataSaved:
        QueueCaptureRender()
