import os
import shutil

class ProcessImgSeqForAvid():
    def __init__(self, imageSeqFolder):
        """ Removes all 499s from an image sequence and renames them appropriately. """
        
        self.imageSeqFolder = imageSeqFolder
        self.sceneName = 'None'
        
        self.main()
        
    def convertFrameIntToString(self, intFrame):
        stringFrame = str(intFrame)
        while len(stringFrame) < 5:
            stringFrame = '0' + stringFrame
        return stringFrame
        
    def convertFrameStringToPath(self, stringFrame):
        stringFile = self.sceneName + '-' + stringFrame + '.jpg'
        stringPath = self.imageSeqFolder + '\\' + stringFile
        return stringPath
    
    def main(self):
        #EXTRA CHECK FOR TRAILING \\
        if self.imageSeqFolder.endswith('\\'):
            self.imageSeqFolder = self.imageSeqFolder[:-1]
        
        #DELETE ANY NON IMAGE FILES BEFORE STARTING
        fileList = os.listdir(self.imageSeqFolder)
        for eachFile in fileList:
            filePath = self.imageSeqFolder + '\\' + eachFile
            if not (filePath.lower().endswith('.jpg')):
                os.unlink(filePath)
        
        self.sceneName = self.imageSeqFolder.split("\\")[-1]
        imageList = os.listdir(self.imageSeqFolder)
        imageTotal = len(imageList)
        
        #MAIN LOOP
        for i in range(len(imageList)):
            filePath = self.imageSeqFolder + '\\' + imageList[i]
            fileFrame = (imageList[i].split('-')[-1])[:-4]
            
            #REMOVE ALL 499 FRAMES
            if fileFrame.endswith('499'):
                os.unlink(filePath)
                continue
                
            #DETECT CORRECT OFFSET
            intFrame = int(fileFrame)
            if (intFrame < 500):
                offset = 0
            elif (intFrame >= 500) and (intFrame < 1500):
                offset = 1
            else:
                offset = 1 + ((intFrame - 500) / 1000)
            fixedFrame = intFrame - offset
            
            #RENAME FRAMES
            fixedString = self.convertFrameIntToString(fixedFrame)
            fixedPath = self.convertFrameStringToPath(fixedString)
            os.rename(filePath, fixedPath)
            
            """ REMOVING - This is now handled inside of MB for more accurate endframes
            #DETECT LAST FRAME AND DUPLICATE
            if intFrame+1 == imageTotal:
                dupeFrame = fixedFrame + 1
                dupeString = self.convertFrameIntToString(dupeFrame)
                dupePath = self.convertFrameStringToPath(dupeString)
                shutil.copy(fixedPath, dupePath)
            """