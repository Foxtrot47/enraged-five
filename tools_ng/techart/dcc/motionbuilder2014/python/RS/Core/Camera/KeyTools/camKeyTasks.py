"""
Description:
    High level functions for creating, converting, submitting, and loading camera key data within MotionBuilder.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
from pyfbsdk import *
import RS.Config
import RS.Perforce as P4
import RS.Core.Camera.CamUtils as CamUtils
from RS.Core.Server import Client as rsServer
import RS.Core.Camera.CameraLocker as CameraLocker
import RS.Core.Camera.CamValidationTools as CamValidationTools
import RS.Core.Automation.FrameCapture.FileTools as FileTools

from RS.Core.Camera.KeyTools import *
import camKeyParser
import camKeySaver
import camKeyLoader
import camKeyPerforce


def GetLocalCamXmlPath():
    fbxPath = FBApplication().FBXFileName
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
    localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
    return localXmlPath


def ValidateBeforeSaveCams():
    """
    Separate module to check our current user, FBX, and p4 connection are valid before saving.
    Also handles error messages for user so the SaveAndSubmitFbxCams module doesn't have to.
    Returns:
        A bool - true if all checks are valid, false if any fail.
    """
    # Detect Valid FBX
    fbxStatus = CamValidationTools.CheckFbxPath(FBApplication().FBXFileName)
    if fbxStatus != "VALID":
        messageText = "Invalid FBX: " + fbxStatus
        FBMessageBox("Save Cam Data", messageText, "Okay")
        return False

    # Detect Valid Perforce Connection
    if not P4.Connected():
        messageText = "ERROR: Couldn't connect to perforce. Cam data not submitted."
        FBMessageBox("Save Cam Data", messageText, "Damn.")
        return False

    # All Checks Valid
    return True


def SaveAndSubmitFbxCams():
    """
    High level function to save out MB camera key data quickly,
    then submit it to perforce in the background via the server.
    """
    # Check User, FBX, and P4 Connection are Valid
    saveCamsValid = ValidateBeforeSaveCams()

    # Invalid Data - just return false
    if not saveCamsValid:
        return False

    # Unlock and Key Keyless Cams
    _camLockObj_ = CameraLocker.LockManager()
    _camLockObj_.keyAllKeylessCams()

    # Create camInfoObj
    camInfoObj = camKeySaver.CreateCamInfoFromFbx()

    # Pickle to temp file
    tempFilePath = "x:/temp.cams"
    camKeyParser.SerializeCamInfo(camInfoObj, tempFilePath)

    # Create Server Connection
    # rsServer.VISIBLE = True
    connection = rsServer.Connection("CamKeySaver")

    # Start Background Process on Server
    connection.Sync(camKeyPerforce)
    commandText = ("import camKeyPerforce" + "\n" +
                   "camKeyPerforce.backgroundProcessTempCams()")
    connection.Thread(commandText)

    # Notify User
    messageText = "Cam data saved."
    FBMessageBox("Save Cam Data", messageText, "Okay")

    return True


def ForceSubmitCamData():
    """
    Submits cam data locally (without using the server) without validation checks.
    Useful for the LayoutTools where we don't have access to the server.
    """
    # Unlock and Key Keyless Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.keyAllKeylessCams()

    # Get Local Xml Path
    localXmlPath = GetLocalCamXmlPath()

    # Verify Folders
    FileTools.CreateParentFolders(localXmlPath)
    FileTools.MakePathWriteable(localXmlPath)

    # Create camInfoObj and Write Out Xml
    camInfoObj = camKeySaver.CreateCamInfoFromFbx()
    camKeyParser.WriteCamInfoXml(camInfoObj, localXmlPath)


def DeleteAndLoadCamsFromXmlPath(xmlPath):
    """
    Mid level function for deleting and restoring key data from an xml path.
    Used in other scripts (like the crapExporter), but not intended for end users.
    """

    # Unlock Cameras
    _camLockObj_ = CameraLocker.LockManager()
    if _camLockObj_.lockedCount != 0:
        _camLockObj_.setAllCamLocks(False)

    # Delete Old Cam Data
    camKeyLoader.DeleteOldCamsFromFbx()

    # Create camInfoObj from Xml
    xmlInfoObj = camKeyParser.CreateCamInfoObjFromXmlPath(xmlPath)

    # Load Cams Into FBX
    camKeyLoader.LoadCamsIntoFbx(xmlInfoObj)

    # Load Switcher Into FBX
    camKeyLoader.LoadSwitcherIntoFbx(xmlInfoObj)

    return "SUCCESS"


def LoadCamsInterface():
    """
    High level UI function that allows users to delete and restore cams via a key xml.
    The user can either grab the latest perforce xml, or load a custom one via an MB file popup.
    """

    # Simple Message Box Interface
    messageText = "".join([" "*10, "Do you want to restore camera data from the most recent xml backup?",
                           "\t\n\n", " "*25, "WARNING: This will delete the current camera data."])
    restoreCamsChoice = FBMessageBox("Cam Data Loader", messageText, "Restore Latest", "Custom Load", "Cancel")
    results = None

    # LOAD LATEST XML
    if restoreCamsChoice == 1:

        # Validate Fbx Path
        fbxPath = FBApplication().FBXFileName.replace("\\", "/")
        if ("/" not in fbxPath) or (len(fbxPath) < 10):
            results = "Invalid FBX."
        else:

            # Detect Xml and Sync
            p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
            P4.Sync(p4XmlPath, force=True)

            # Validate Xml
            localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)
            if not os.path.exists(localXmlPath):
                results = "Cam data xml not found, or couldn't be synced from P4."
            else:

                # Delete and Load Cam Data From Xml
                results = DeleteAndLoadCamsFromXmlPath(localXmlPath)

    # LOAD CUSTOM XML
    elif restoreCamsChoice == 2:

        # Create MB FilePopup
        lFp = FBFilePopup()
        lFp.Caption = "FBFilePopup example: Select a file"
        lFp.Style = FBFilePopupStyle.kFBFilePopupOpen
        lFp.Filter = "*.xml"
        lFp.Path = os.path.join(RS.Config.Project.Path.Art, "animation", "resources", "cameras")
        lRes = lFp.Execute()
        if (lRes):

            # Delete and Load Cam Data From Xml
            results = DeleteAndLoadCamsFromXmlPath(lFp.FullFilename)

    # OUTPUT
    if results == "SUCCESS":

        # Move To Frame 0
        # weird bug after creating cams where keys
        # don't update until the frame is altered
        thumbnailTime = FBTime()
        thumbnailTime.SetFrame(-1)
        FBPlayerControl().Goto(thumbnailTime)
        thumbnailTime.SetFrame(0)
        FBPlayerControl().Goto(thumbnailTime)

        successText = "The cam data was successfully restored."
        FBMessageBox("Cam Data Loader", successText, "Okay")

    elif results != None:
        errorText = 'Error: ' + results
        FBMessageBox("Cam Data Loader", errorText, "Okay")


def LoadKeyXmlIntoEmptyFbx(fbxPath):
    """
    Detects the key xml path from a given fbxPath and loads it
    into an FBX via the DeleteAndLoadCamsFromXmlPath function.
    Arguments:
        fbxPath: A string of the local FBX path.
    """
    # Detect Xml and Sync
    p4XmlPath = camKeyPerforce.detectXmlPathFromFbxPath(fbxPath)
    localXmlPath = camKeyPerforce.convertPerforcePathToLocal(p4XmlPath)

    # Sync Latest Cam Data Xml - or return false on fail
    syncedFiles = P4.Sync(p4XmlPath, force=True)
    if not syncedFiles:
        return False

    # Load Cams and Switcher Into Blank FBX
    DeleteAndLoadCamsFromXmlPath(localXmlPath)

    return True


def CreateTransferCam():
    """
    Sets up the export camera in an empty FBX, renames it TransferCamera,
    rekeys the switcher with it, and returns a transCamData key object.
    Returns:
        A camKeyInfo object of the camera data to transfer.
    """
    # Setup Export Cam
    expCam = CamUtils.SetupExportCam()

    # Rename Export Cam to Transfer Cam
    expCam.Name = "TransferCamera"

    # Override Switcher Keys with TransferCamera
    CamUtils.OverrideSwitcherKeys(expCam.Name)

    # Create transCamData
    transCamData = camKeySaver.CreateCamInfoFromFbx()

    return transCamData


def LoadTransferCam(transCamData):
    """
    Loads a transCamData key object into a full FBX,
    then rekeys the switcher, and sets up export camera.
    Arguments:
        transCamData: A camKeyInfo object of the transferred cam data.
    """
    # Unlock Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.setAllCamLocks(False)

    # Delete Old Switcher Keys
    CamUtils.DeleteSwitcherKeys()

    # Load expCamObj into Full Fbx
    camKeyLoader.LoadCamsIntoFbx(transCamData)
    camKeyLoader.LoadSwitcherIntoFbx(transCamData)

    # Relock Switcher - prevents plotter from altering switcher
    switcherIndexProp = FBCameraSwitcher().PropertyList.Find("Camera Index")
    switcherIndexProp.SetLocked(True)

    # Setup Export Cam
    CamUtils.SetupExportCam()