"""

Shake Cam V 2.0
---------------

Author: Mark Harrison-Ball
Support: Ross.George@rockstarlondon.com
Email:  Mark.harrison-ball@rockstargames.com
Age:    Very Old

Revision History
----------------

V2.0
* Added Export cam feature so we can plug in any new cameras
* Added suppor tto bake down various translation data
* Added support to fade in / fade out cam shake data (TODO)

V1.0
* Inital hardoced 3 camera setup for adding shake cams


Additional Notes:
Save camera shake data to external meta file 
load in data and apply to keys

Uses external files in external folder
We need to alwasy export Z up!


"""
from pyfbsdk import *

import os

# New modules to support handling takes/aniamtions/nodes
import RS.Core
import RS.Utils.Scene.Model
import RS.Utils.Scene.AnimationNode
import RS.Utils.Scene.Property
import RS.Utils.Scene.Take

import RS.Globals as glo

import RS.Perforce
import RS.Config




#Frame Info Def
# We now will record all translation data too
# Changing this data will mean updating the parser to cgheck what export data is their
class FrameDefInfo:    
    frame = 0,
    TransX = 0,
    TransY = 0,
    TransZ = 0,
    RotX = 0,
    RotY = 0,
    RotZ = 0,
    FOV = 0
   
   
    """
    TODO: Set up shake data and parse in XML to define descrtiption, lenght, name etc
          Add beter UI visualerizer
          Add A shit load of penguins
    """
  

class HandShake:
    # Constructor
    def __init__(self):
        self.path = os.path.abspath( "{0}/etc/CameraData/".format( RS.Config.Tool.Path.TechArt ) )
        #self.path = "x:/wildwest/dcc/motionbuilder2014/python/etc/CameraData/"
        self.CameraShake = []
        self.Light = "Light Handheld"
        self.Medium = "MEdium Handheld"
        self.Heavy = "Heavy Handheld"
        
        #self.lTimeFrame = 0
        self.CameraRange = dict()
        
        self.lExportCamera = FBFindModelByLabelName("ExportCamera")  
        self.lSwitcherCamera = None
        
        self.gCamKeys = []
        
        self.ShakeLayer = None   
        
        # Used to store our frame start and end Ranges
        self.FrameStart = None
        self.FrameEnd = None
        
        # Used to disbale locla Transforms
        self.ExportLocal = True
        

        
        self.lCameraList = [] 
        self.Shakephase = 1 # USed for pahse start
        
        self.lSystem = FBSystem()
        
        # Default load light Shake, 1st nam ein our list
        
        self.readShakeFile(self.path+"Light.csf")
        

    """
    Private    
    """  

    # Go through our takes, if we find out take name then set it to our name
    def __setTakebyName__(self, takename):
        for lTakeIdx in range( len( self.lSystem.Scene.Takes )):
            if self.lSystem.Scene.Takes[lTakeIdx].Name == takename:  
                self.lSystem.CurrentTake = self.lSystem.Scene.Takes[lTakeIdx]
                return()
            
        FBMessageBox( "Information", "Failed to find a take which matches the selected camera name (the expected setup).\nUsing the current take as source for animation.", "OK" )
    
    # WHats this do I hear you ask, well it checks all the translation fcurves and 
    # will return the largest number so we only export the len of the camera animation
    # Also need to go through each layer as we don't know which layer has the aniamtion
    # Might in future check all vlaus are same len as this migth cause mismatch issues
    def __returnCamLength__(self, cam):
        maxlen = 0
        for i in range(self.lSystem.CurrentTake.GetLayerCount()): # Go through each layer
            self.lSystem.CurrentTake.SetCurrentLayer(i) 
            for x in range(3):
                AnimNode = cam.Rotation.GetAnimationNode()
                if AnimNode:                    
                    thisLen = AnimNode.Nodes[x].FCurve
                    if len(thisLen.Keys) > maxlen:
                        maxlen = len(thisLen.Keys) 
                AnimNode = cam.Translation.GetAnimationNode()
                if AnimNode:
                    thisLen = cam.Translation.GetAnimationNode().Nodes[x].FCurve
                    if len(thisLen.Keys) > maxlen:
                        maxlen = len(thisLen.Keys) 
        return maxlen
    
    
    
    # Get the camera
    # Check the Take
    # Make sure camera is at 0,0,0 and export data
    # read data out and write to csf file, 
    # Cam if exists has already been checked earlier
    def __ExportCamShake__(self, camName):
        # Get our Cam model
        cam = FBFindModelByLabelName(camName)
        
        # Set our take name if we have one to the camera name
        if not RS.Utils.Scene.Take.SetCurrentTakeByName( camName ):
            FBMessageBox( "Information", "Failed to find a take which matches the selected camera name (the expected setup).\nUsing the current take as source for animation.", "OK" )
     
        #Record camera information
        self.RecordSelectedCamera(cam)

    def __DeleteSelectedShake__(self, Cam):
        camModel = FBFindModelByLabelName(Cam)
        if camModel!=None:
            if self.FindLayer("ShakeCam"):
                self.SetLayer("ShakeCam")
                # Get the Cameras Fcurves and delete the shit out of them
                for i in range(3):
                    camModel.Rotation.GetAnimationNode().Nodes[i].FCurve.EditClear()
                    camModel.Translation.GetAnimationNode().Nodes[i].FCurve.EditClear()
                camModel.PropertyList.Find("Field Of View").GetAnimationNode().FCurve.EditClear()
                

    def run(self):
        self.main()
        
    
    """
    Public    
    """     
    # Load up and read in out costume Shake Data
    # Maybe at one point we coudl seralize this, but whats the point it will piss everyone off
    # NOTE!!! Added support for missing camera data you muppet!!!
    def readShakeFile(self, pFilename):
        self.CameraShake = []
        if os.path.exists(pFilename):        
            f = open(pFilename, 'r')
            for line in iter(f):
                FrameInfo = FrameDefInfo()
                tokens = line.split(',')
                FrameInfo.frame = int(tokens[0])
                if len(tokens) == 5:
                    FrameInfo.TransX = 0.0
                    FrameInfo.TransY = 0.0
                    FrameInfo.TransZ = float(tokens[1])
                    FrameInfo.RotX = float(tokens[2])
                    FrameInfo.RotY = float(tokens[3])
                    FrameInfo.RotZ = float(tokens[4])                    
                    
                else:                   
                
                    FrameInfo.TransX = float(tokens[1])
                    FrameInfo.TransY = float(tokens[2])
                    FrameInfo.TransZ = float(tokens[3])
                    FrameInfo.RotX = float(tokens[4])
                    FrameInfo.RotY = float(tokens[5])
                    FrameInfo.RotZ = float(tokens[6])
                    FrameInfo.FOV = float(tokens[7])
                self.CameraShake.append(FrameInfo)
            f.close()
        return
    
    # Write out our Shake Data, pretty straight forward
    def writeShakeFile(self, pFilename):
        
        # Make File Writable incase its not
        if os.path.exists(self.path+pFilename):
            os.chmod(self.path+pFilename, stat.S_IWRITE)
        
        print 'Saving Shake file to :{0}/{1}.csf'.format(self.path,pFilename)          
        
        f = open("{0}/{1}.csf".format(self.path,pFilename),'w')
        for fDef in self.CameraShake:
            f.write(str(fDef.frame) +","
                    +str(fDef.TransX) + ","
                    +str(fDef.TransY) + ","
                    +str(fDef.TransZ) + ","
                    +str(fDef.RotX) + ","
                    +str(fDef.RotY) + ","
                    +str(fDef.RotZ) + ","
                    +str(fDef.FOV) + "\n")
        f.close()
        
        
    # So this will automactially make the camera loop: SCARY!!   
    # We take start and end and take the difference and divide by 2
    # Not used yet
    def blendAnimationCurves(self, pCamera):
        fstart_TransX = pCamera.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(0)))
        fEnd_TransX = pCamera.Translation.GetAnimationNode().Nodes[0].FCurve.Evaluate(FBTime(0,0,0,(self.FrameEnd)))
        diff = fstart_TransX - fEnd_TransX
        diff = diff/2 # gets the offset

        return

    # RECORDING ONLY
    # read our camera data into a instaced class 'FrameInfo'  
    # GLOBAL MATRIX!!!
    #
    # Function Author: Ross.George@RockstarLondon.com
    #
    # C++ void FBGetGlobalMatrix        (FBMatrix &pMatrix, FBMatrix pMatrixParent, FBMatrix pLocalMatrix);
    #
    def ReadCamShakeKeys(self, pCamera):
        
        if ( pCamera ):
            print 'Processing Cam data'
            self.CameraShake = []
            
            #Set the base layer
            RS.Core.System.CurrentTake.SetCurrentLayer( 0 )
                        
            #Get time range on the current take for the animation nodes we are interested in
            lCameraTimeRange = RS.Utils.Scene.Model.GetTimeRange( pCamera, [ RS.Core.System.CurrentTake ], None, 
                                                      [ RS.Utils.Scene.Property.Translation, 
                                                        RS.Utils.Scene.Property.Rotation, 
                                                        RS.Utils.Scene.Property.FOV ] )
            
            #Check the range we have been returned has some valid results
            if not lCameraTimeRange.Valid():
                print 'Invalid Time Range'
                return()
            
            #Set time to first frame of the given camera
            RS.Core.Player.Goto( lCameraTimeRange.Start )
            
            #Get initial FOV data
            lCameraFOVInitialValue = pCamera.FieldOfView.Data

            lNullParent = None
            #Transform the camera into world space looking down the Z axis
            if( self.ExportLocal ):
                #Setup an inverse matrix 
                lCameraInverseMatrix = FBMatrix()            
                pCamera.GetMatrix( lCameraInverseMatrix )
                lCameraInverseMatrix.Inverse()  
                
                #Create a null object and align it to the origin
                lNullParent = FBModelNull("ShakeCameraExportParent")
                lNullParent.SetMatrix( FBMatrix() )
                RS.Core.System.Scene.Evaluate()
                
                #Parent the camera to the null and then pass in the cameras inverse 
                #matrix to bring it into the centre of the world
                pCamera.Parent = lNullParent
                lNullParent.SetMatrix( lCameraInverseMatrix )
                RS.Core.System.Scene.Evaluate()
            
            #Now we step through each frame and export the data
            for iFrame in range( lCameraTimeRange.GetStartFrame(), 
                                 lCameraTimeRange.GetEndFrame() ):

                #To keep the data as raw as possible we want to record everything even though we may not use it all
                FrameInfo = FrameDefInfo()
                FrameInfo.frame = iFrame
                
                lFrameTime = FBTime( 0,0,0,iFrame )
                
                #Move to the given frame
                RS.Core.Player.Goto( lFrameTime )
                 
                #Get the current frame matrix
                lCameraCurrentFrameMatrix = FBMatrix()
                pCamera.GetMatrix( lCameraCurrentFrameMatrix )
                       
                #Extract rotation components from the matrix and write out our FrameInfo
                lRotationVector = FBVector3d()
                FBMatrixToRotation(lRotationVector, lCameraCurrentFrameMatrix)
                
                FrameInfo.RotX = lRotationVector[0]
                FrameInfo.RotY = lRotationVector[1]
                FrameInfo.RotZ = lRotationVector[2]

                #Extract translation components from the matrix and write out our FrameInfo
                lTranslationVector = FBVector4d()
                FBMatrixToTranslation(lTranslationVector, lCameraCurrentFrameMatrix)
                
                FrameInfo.TransX = lTranslationVector[0]
                FrameInfo.TransY = lTranslationVector[1]
                FrameInfo.TransZ = lTranslationVector[2]
                
                #Extract FOV data
                FrameInfo.FOV = pCamera.FieldOfView - lCameraFOVInitialValue 
                
                self.CameraShake.append(FrameInfo)
            
            if( lNullParent ):
                #Delete the null to revert the camera back to where it was before.
                lNullParent.FBDelete()    
            
    
    
    def RecordSelectedCamera(self, pCamera):
        self.ReadCamShakeKeys(pCamera)
        
        (lRes, lShakeName ) = FBMessageBoxGetUserValue( "Save Camera", "Enter Shake Name for Camera: ", pCamera.Name, FBPopupInputType.kFBPopupString, "OK", "Cancel" )
        
        if lRes and lShakeName!= "":                     
            self.writeShakeFile(lShakeName)
        
        
        return

        
    # -----------------------------------------------------------------------    
    def delLayer(self, layerName):
        lTake = FBSystem().CurrentTake
        lCurrentLayer = lTake.GetCurrentLayer()
        lLayerNumber = lTake.GetLayerCount()        
         
        for iNumber in range(lLayerNumber):
            lLayer = lTake.GetLayer(iNumber)
            if lLayer.Name == layerName:
                #print "found layer %s " % lLayer.Name
                lTake.RemoveLayer(iNumber)
                return True
        return False        
       

    def SetLayer(self, layerName=None):

        if not (self.FindLayer(layerName)):            
            lSystem = FBSystem()
            lSystem.CurrentTake.CreateNewLayer()
            lCount = lSystem.CurrentTake.GetLayerCount()
            lSystem.CurrentTake.GetLayer(lCount-1).Name= layerName
            lSystem.CurrentTake.SetCurrentLayer(lCount-1)
           
            print "No %s Layer found, creating a new one"
            
    
    
    def FindLayer(self, layerName):
        
        lTake = FBSystem().CurrentTake
        lCurrentLayer = lTake.GetCurrentLayer()
        lLayerNumber = lTake.GetLayerCount()
        
        for iNumber in range(lLayerNumber):
            lLayer = lTake.GetLayer(iNumber)
            if lLayer.Name == layerName:
                lTake.SetCurrentLayer(iNumber) 
                #print "Set to layer %s" % (layerName)
                return True
           
        return False
     # -----------------------------------------------------------------------  
    
    def GetCameraList(self):
        self.lCameraList = []
        for iCamera in glo.gCameras:
            if not "Producer" in iCamera.Name:
                if not "ExportCamera" in iCamera.Name:
                    self.lCameraList.append(iCamera)       
    
    
    def plotShakeLayer(self):

        for lCamera in self.lCameraList :
            
            #Get time range on the current take for the animation nodes we are interested in        
            lCameraTimeRange = RS.Utils.Scene.Take.GetTimeRange( RS.Core.System.CurrentTake )

            #Check the properies we want to animate all exsist and 
            lTranslationProperty = lCamera.PropertyList.Find( RS.Utils.Scene.Property.Translation )
            lRotationProperty = lCamera.PropertyList.Find( RS.Utils.Scene.Property.Rotation )
            lFOVProperty = lCamera.PropertyList.Find( RS.Utils.Scene.Property.FOV )
            
            #Check everything we need is set to animate
            lTranslationProperty.SetAnimated( True )
            lRotationProperty.SetAnimated( True )
            lFOVProperty.SetAnimated( True )
            
            #Get our target animation nodes
            lTranslationNode = lTranslationProperty.GetAnimationNode()
            lRotationNode = lRotationProperty.GetAnimationNode()
            lFOVNode = lFOVProperty.GetAnimationNode()
                        
            #Clear all the old keys we don't need
            RS.Utils.Scene.AnimationNode.DeleteKeys( lTranslationNode, True )
            RS.Utils.Scene.AnimationNode.DeleteKeys( lRotationNode, True )
            RS.Utils.Scene.AnimationNode.DeleteKeys( lFOVNode, True )
                  
            lCamShakeIndex = int( len(self.CameraShake) * ( self.Shakephase / 100 )) # start Frame
            
            #Now we step through each frame and export the data
            for iFrame in range( lCameraTimeRange.GetStartFrame(), 
                                 lCameraTimeRange.GetEndFrame() ):     
                lKeyTime = FBTime(0,0,0,iFrame) 
    
                if lCamShakeIndex > len(self.CameraShake)-1:
                    lCamShakeIndex = 0 #We cycle through the shake camera
                    print "reset cam index to 0"

                # ROTATION#
                if( bool( self.UI.rotation_button_X.State ) ):
                    lRotationNode.Nodes[0].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotX )
                if( bool( self.UI.rotation_button_Y.State ) ):
                    lRotationNode.Nodes[1].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotY )                 
                if( bool( self.UI.rotation_button_Z.State ) ):
                    lRotationNode.Nodes[2].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].RotZ )                   

                # TRANSLATION
                if( bool( self.UI.translation_button_X.State ) ):
                    lTranslationNode.Nodes[0].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransX ) 
                if( bool( self.UI.translation_button_Y.State ) ):                
                    lTranslationNode.Nodes[1].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransY  ) 
                if( bool( self.UI.translation_button_Z.State ) ):
                    lTranslationNode.Nodes[2].KeyAdd(lKeyTime, self.CameraShake[lCamShakeIndex].TransZ  )

                # FOV (We work on a Additive layer so we need to add the diff)
                if( bool( self.UI.fov_button.State ) ):
                    lFOVNode.KeyAdd( lKeyTime, self.CameraShake[lCamShakeIndex].FOV )                

                # Move to next Value
                lCamShakeIndex+=1
                
                #Break while we work on the translation logic
                continue
            
                #Move to the frame so we get accurate matrix information
                RS.Core.Player.Goto( lKeyTime )
                RS.Core.System.Scene.Evaluate()
                
                #Now get the camera's matrix at this frame
                lCameraFrameMatrix = FBMatrix()
                lCamera.GetMatrix( lCameraFrameMatrix )
                
                #Create a matrix for the amount we need to offset the camera
                lCameraOffsetTranslationMatrix = FBMatrix()
                FBTranslationToMatrix( lCameraOffsetTranslationMatrix, FBVector4d( self.CameraShake[lCamShakeIndex].TransX, 
                                                                                   self.CameraShake[lCamShakeIndex].TransY, 
                                                                                   self.CameraShake[lCamShakeIndex].TransZ, 1  ) )
                #Get the pretranslated matrix
                lCameraPretranslatedFrameMatrix = FBMatrix()
                FBMatrixMult(lCameraPretranslatedFrameMatrix, lCameraFrameMatrix, lCameraOffsetTranslationMatrix )
                
                #Now we subtract the original matrix by the final 
                lCameraLocalMatrix = lCameraPretranslatedFrameMatrix - lCameraFrameMatrix
                
                #Extract the translation
                lTranslationVector = FBVector4d()
                FBMatrixToTranslation( lTranslationVector, lCameraLocalMatrix ) 
                
                #Finally we key this data
                lTranslationNode.Nodes[0].KeyAdd(lKeyTime, lTranslationVector[0] ) 
                lTranslationNode.Nodes[1].KeyAdd(lKeyTime, lTranslationVector[1] ) 
                lTranslationNode.Nodes[2].KeyAdd(lKeyTime, lTranslationVector[2] )
                
                # FOV (We work on a Additive layer so we need to add the diff)
                lFOVNode.KeyAdd( lKeyTime, self.CameraShake[lCamShakeIndex].FOV )
                    
                # Move to next Value
                lCamShakeIndex+=1

                RS.Core.Player.SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames
                
                # Get End Frame.
                RS.Core.Player.GotoStart()                

    
    """
    Public Methods
    """
    
    def plotShakeToCameras(self, Cam = None):
        
        #Resolve our shake layer and set it as active
        lShakeLayer = RS.Utils.Scene.Layer.ResolveLayer ( "ShakeCam" )
        RS.Utils.Scene.Layer.SetLayerActive( RS.Core.System.CurrentTake, lShakeLayer )
       
        if Cam != None:
            self.lCameraList = []
            camModel = FBFindModelByLabelName(Cam)
            if camModel!= None:
                self.lCameraList.append(camModel)
                self.plotShakeLayer() 
        else:
            self.GetCameraList()
            self.plotShakeLayer() 

        return
    
    

 
    def LoadShakeFile(self, shakeName):
        self.readShakeFile(self.path+shakeName)
        if len(self.CameraShake) > 0:
            print "Loaded %s file: " % (self.path+shakeName)
        else:
            print "Failed to Load %s " % (self.path+shakeName)
        return
    
    def DeleteShake(self):
        lResult = FBMessageBox("Warning" ,"This will remove ALL camera Shake Data" , "Cancel", "Continue")
        if lResult == 2:
            self.delLayer("ShakeCam")
        return
    
    def AddShake(self):
        lResult = FBMessageBox("Warning" ,"This will add Shake Data to ALL cameraa" , "Cancel", "Continue")
        if lResult == 2:        
            if len(self.CameraShake) > 0:
                self.delLayer("ShakeCam")
                self.SetLayer("ShakeCam") 
                self.plotShakeToCameras()
            else:
                print "No Shake Loaded"        
        return
    
    def AddShakeSelected(self, CameraName):
        if len(self.CameraShake) > 0:
            if not self.FindLayer("ShakeCam"):
                self.SetLayer("ShakeCam")                
            self.plotShakeToCameras(CameraName)
        else:
            print "No Shake Loaded"            
        return   
        
    def DeleteShakeSelected(self, CameraName):
        if CameraName!=None:
            self.__DeleteSelectedShake__(CameraName)
            return    
    
    def UpdateShakeWeight(self, shakeweight):
        self.Shakephase = shakeweight     
        return
    
    def ExportCamShake(self, CameraName):
        if CameraName != None:
            self.__ExportCamShake__(CameraName)
            return
        
    def DisableLocalExportTransforms(self, exportlocal = True):
        self.ExportLocal = exportlocal
        
        
        
    
    # NOt used
    def main(self):
        
        # Create a ShakeCam Layer
        
        
        #self.SetLayer("BaseAnimation")
        
        #self.RecordCamera()
        #return
        
        # Read Export Cam keys
        # MIght need to get teh absolute result from each layer...
        #self.ReadCamShakeKeys(self.lExportCamera)
        #self.gCamKeys = self.CameraShake 

        self.delLayer("ShakeCam")
        self.SetLayer("ShakeCam")        
        
        #self.readShakeFile(self.path+"Heavy.csf")
        
        self.plotCameras()
        
        
  
HandShake = HandShake()
#HandShake.LoadShakeFile("crazy")

#HandShake.__ExportCamShake__("EXTREME_SHAKE_CAM_2")

#HandShake.AddShakeSelected("gtaVCamera")
#HandShake.DeleteShake()