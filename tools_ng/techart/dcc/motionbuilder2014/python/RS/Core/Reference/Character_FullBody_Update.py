'''

 Script Path: RS/Core/Reference/Character_FullBody_Update.py.py

 Written And Maintained By: Kathryn Bodey

 Created: 04.12.2014

 Description: For fully updating a character - new one brought in and animation copied across from old version.
              Will be called from ref system.

'''

from pyfbsdk import *

import RS.Utils
import RS.Globals
import RS.Core.Reference.Lib
import RS.Utils.Scene.Component

reload( RS.Core.Reference.Lib )
reload( RS.Utils.Scene.Component )


class CharacterFullUpdate():

	def __init__( self ):

		self.CharacterList = []
		self.ThreeLateralReferenceList = []

		# populate character list
		for character in RS.Globals.Characters:
			if '_gs' in character.LongName or 'gs:' in character.LongName or character.LongName.startswith( 'gs ' ):
				None
			else:
				self.CharacterList.append( character ) 

		# populate 3L/character dictionary - Find any 3Lateral Facial and match them to potential characters
		sceneRefNull = RS.Core.Reference.Manager.GetReferenceSceneNull()

		refChildNullList = []
		if sceneRefNull:
			RS.Utils.Scene.GetChildren( sceneRefNull, refChildNullList )

		threeLateralNullList = []        
		if refChildNullList:
			for refChild in refChildNullList:
				assetTypeProperty = refChild.PropertyList.Find( 'rs_Asset_Type' )
				if assetTypeProperty:
					if assetTypeProperty.Data == '3Lateral':
						threeLateralNullList.append( refChild )

		if threeLateralNullList:
			for threeLateralNull in threeLateralNullList:
				
				pathProperty = None
				pathProperty = threeLateralNull.PropertyList.Find( 'Reference Path' )					
				
				characterMatchList = []
				# get 3L name
				if '-' in threeLateralNull.Name:
					splitName = threeLateralNull.Name.split( '-' )
					characterName = splitName[0]

					# find a character match to 3L ref by name
					for character in self.CharacterList:
						if 'IG_' in character.Name:
							None							
						elif characterName in character.Name:
							characterMatchList.append( character )
						elif characterName.lower() == 'michael' and character.Name == 'Player_Zero':
							characterMatchList.append( character )
						elif characterName.lower() == 'franklin' and character.Name == 'Player_One':
							characterMatchList.append( character )                    
						elif characterName.lower() == 'trevor' and character.Name == 'Player_Two':
							characterMatchList.append( character )  				

				if pathProperty and characterMatchList:
					self.ThreeLateralReferenceList.append( ThreeLateralReference( threeLateralNull, str( pathProperty.Data ), characterMatchList ) )


	def CharacterList():
		return self.CharacterList

	def PlotControlRig( self, character ):  
		# check if a control rig exists and if it is active
		ctrl_hips = character.GetCtrlRigModel( FBBodyNodeId.kFBHipsNodeId )

		# plot from ctrl rig to skeleton, delete control rig
		if ctrl_hips:
			if character.ActiveInput == True and character.InputType == FBCharacterInputType.kFBCharacterInputMarkerSet:

				plot_options = FBPlotOptions()
				plot_options.PlotAllTakes = True
				plot_options.PlotOnFrame = True
				plot_options.PlotPeriod = FBTime( 0, 0, 0, 1 )
				plot_options.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
				plot_options.UseConstantKeyReducer = False        

				character.PlotAnimation( FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plot_options )

				character.ActiveInput = False

				control_rig = character.GetCurrentControlSet()
				control_rig.FBDelete()      

			else:
				result = FBMessageBox( 'R* Warning', 'Cannot find an active Control Rig. Proceeding to replace character without plotting.', 'Ok', 'Cancel' )
				if result == 0:

					plot_options = FBPlotOptions()

					plot_options.PlotAllTakes = True
					plot_options.PlotOnFrame = True
					plot_options.PlotPeriod = FBTime( 0, 0, 0, 1 )
					plot_options.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll
					plot_options.UseConstantKeyReducer = False        

					character.PlotAnimation( FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plot_options )

					character.ActiveInput = False

					control_rig = character.GetCurrentControlSet()
					control_rig.FBDelete()   
		else:
			print 'control rig doesnt exist'



	def NamespaceReplace( self, scene_component_type, old_name, new_name ):
		for component in scene_component_type:            
			component_namespace = component.LongName.split(":")
			if component_namespace[0] == old_name:
				component.LongName = new_name + ":" + component.Name   


	def FacialComponents(self, namespace ):
		facialComponentIgnoreList = []
		faceRootNameList = [
			'FACIAL_facialRoot', 
			'faceControls_OFF', 
			'FacialAttrGUI', 
			'FaceFX',
			'Scaleoffset'                        
			'3LateralCustomAttributes', 
			'3LateralfacialRoot_Parent', 
			'Csb 3Lateral', 
			'Csb 3Lateral 1', 
			'Viseme Relation', 
			'FunctionList',
		]

		for component in RS.Globals.Components:
			for faceName in faceRootNameList:
				if component.LongName == namespace + ':' + faceName:
					if type( component ) == FBFolder:
						facialComponentIgnoreList.append( component )
						for i in component.Items:
							facialComponentIgnoreList.append( i )
					elif type( component ) in [ FBModelNull, FBModel, FBModelSkeleton ]:
						RS.Utils.Scene.GetChildren( component, facialComponentIgnoreList, "", True )
					elif type( component ) in [ FBConstraint, FBConstraintRelation ]:
						facialComponentIgnoreList.append( component )

		return facialComponentIgnoreList


	def CheckForFacial( self, character ):
		facialPresent = False
		threeLateralPath = None
		continueScript = True
		'''
		check if character has 3L
		'''
		if self.ThreeLateralReferenceList:
			for i in self.ThreeLateralReferenceList:
				if character == i.CharacterMatchList[0]:
					facialPresent = True
					threeLateralName = i.Null.Name
		
		if facialPresent == True:
			result = FBMessageBox( 'R* Warning', 'Warning:\n{0} has 3Lateral Facial attached to it - {1}.\n\nThe facial Reference will be deleted and a fresh one brought in for the updated\ncharacter.  If you choose to proceed with this update, a popup will appear at the\nend of the update asking you to verify the facial namespace and import options.\n\nIs it okay to proceed with the update?'.format( character.LongName, threeLateralName  ), 'Yes', 'No' )
			if result == 2:
				continueScript = False	
		
		return continueScript


	def CheckForToybox( self, namespace ):
		# setup list [0] = continue with script, [1] = re-add toybox constraint
		continueScriptList = [True, False]
		# check if a toybox constraint exists and if it is active
		toyboxConstraintName = namespace + ':mover_Control_SendToToyBox'
		for constraint in RS.Globals.Constraints:
			if constraint.LongName == toyboxConstraintName and constraint.Active == True:
				result = FBMessageBox('R* Warning', 'Toybox: {0} has an active toybox constraint.\nThis constraint will be deleted and re-added new, if you continue.\n\nHow would you like to proceed?'.format(namespace), 'Continue', 'Cancel')
				print 'result...', result
				if result == 1:
					continueScriptList = [True, True]
					continue
				else:
					continueScriptList = [False, False]
					continue
		
		return continueScriptList	
		
	def CharacterReplace( self, ref_null ):
		original_namespace = ref_null.Name
		temp_name = 'OLD_' + ref_null.Name

		path = None
		path_property = ref_null.PropertyList.Find( "Reference Path" )   
		if path_property:
			path = path_property.Data

		character = None

		for char in RS.Globals.Characters:
			if char.LongName.startswith( original_namespace + ':' ):
				character = char
				break

		if character and path:
			continueScript = True
			faceCheckResult = self.CheckForFacial( character )
			toyboxCheckResultList = self.CheckForToybox(original_namespace) 
			if faceCheckResult == True and toyboxCheckResultList[0] == True:
			
				''' 
				plot old character 
				'''
				self.PlotControlRig( character )              
	
				''' 
				rename old character 
				'''     
				scene = FBSystem().Scene
	
				for component in scene.Components:
					component.ProcessObjectNamespace ( FBNamespaceAction.kFBReplaceNamespace, original_namespace, temp_name, False )  
	
				for component in scene.Components:
					if not component.Is( FBNamespace.TypeInfo ):
						if component.Name == "Decklink Video Capture" or component.Name == "Blackmagic WDM Capture":
							pass
						else:
							namespace = component.LongName.split(":")     
							if namespace[0] == original_namespace:
								component.LongName = temp_name + ":" + component.Name                                 
	
				self.NamespaceReplace( scene.Groups, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Characters, original_namespace, temp_name )            
				self.NamespaceReplace( scene.CharacterExtensions, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Materials, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Folders, original_namespace, temp_name )                        
				self.NamespaceReplace( scene.Takes, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Cameras, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Textures, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Materials, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Shaders, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Deformers, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Devices, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Constraints, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Lights, original_namespace, temp_name )            
				self.NamespaceReplace( scene.AudioClips, original_namespace, temp_name )            
				self.NamespaceReplace( scene.VideoClips, original_namespace, temp_name )            
				self.NamespaceReplace( scene.MotionClips, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Notes, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Poses, original_namespace, temp_name )            
				self.NamespaceReplace( scene.ObjectPoses, original_namespace, temp_name )       
				self.NamespaceReplace( scene.Actors, original_namespace, temp_name )            
				self.NamespaceReplace( scene.ActorFaces, original_namespace, temp_name )            
				self.NamespaceReplace( scene.MarkerSets, original_namespace, temp_name )            
				self.NamespaceReplace( scene.ControlSets, original_namespace, temp_name )            
				self.NamespaceReplace( scene.CharacterFaces, original_namespace, temp_name )            
				self.NamespaceReplace( scene.CharacterPoses, original_namespace, temp_name )            
				self.NamespaceReplace( scene.UserObjects, original_namespace, temp_name )        
				self.NamespaceReplace( scene.Sets, original_namespace, temp_name )            
				self.NamespaceReplace( scene.Handles, original_namespace, temp_name )            
				self.NamespaceReplace( scene.ConstraintSolvers, original_namespace, temp_name )            
				self.NamespaceReplace( scene.PhysicalProperties, original_namespace, temp_name )        
	
				#Update RS_Null properties with the correct information
				ref_null.Name = temp_name    
	
				ref_null_namespace_property = ref_null.PropertyList.Find( "Namespace" )
				if ref_null_namespace_property:
					ref_null_namespace_property.Data = temp_name
	
				# need to manually remove the namespace from the Navigator > Namespaces, even though it's not used anymore.        
				for namespace in scene.Namespaces:
					if namespace.Name == original_namespace:
						namespace.FBDelete()   
	
	
				''' 
				bring in new character 
				'''
				FBMergeTransactionBegin()
				RS.Core.Reference.Lib.rs_CreateReference( path, path, quiet=True, returnReferenceNull=True, forcedNamespace=original_namespace )
				FBMergeTransactionEnd()
				
	
				''' 
				copy animation across
				'''          
				# create source and target component lists
				sourceFaceIgnoreList = self.FacialComponents( temp_name )
				targetFaceIgnoreList = self.FacialComponents( original_namespace )
				sourceComponentList = []
				targetComponentList = []
				for component in RS.Globals.Components:
					if component in sourceFaceIgnoreList or component in targetFaceIgnoreList :
						None
					else:
						if component.LongName.startswith( temp_name + ':' ):
							sourceComponentList.append( component )
						elif component.LongName.startswith( original_namespace + ':' ):
							targetComponentList.append( component )
	
				# match components
				componentMatchList = RS.Utils.Scene.Component.GetComponentMatchList( sourceComponentList, targetComponentList )     
	
				# copy animation
				for componentMatch in componentMatchList:
					componentMatch.CopyAnimationData()   
	
				# turn off active on new character
				new_character = None
				for char in RS.Globals.Characters:
					if char.LongName.startswith( original_namespace + ':' ):
						new_character = char
						break
	
				if new_character:
					new_character.ActiveInput = False
	
	
				'''
				delete old character
				'''
				temp_null = FBModelNull( ref_null.Name )
				RS.Core.Reference.Lib.rs_DeleteReference( temp_null, None )
				try:
					temp_null.FBDelete()
				except:
					pass
				
				
				'''
				delete 3L if it exists on character, and create a fresh one
				'''
				if self.ThreeLateralReferenceList:
					for reference in self.ThreeLateralReferenceList:
						if character == reference.CharacterMatchList[0]:
							# delete
							temp_ref_null = FBModelNull( reference.Null.Name )
							RS.Core.Reference.Lib.rs_DeleteReference( temp_ref_null, None )
							# create new 3L ref
							FBMergeTransactionBegin()
							RS.Core.Reference.Lib.rs_CreateReference( reference.Path, reference.Path, quiet=False, returnReferenceNull=True )							
							FBMergeTransactionEnd()
							break
						
				'''
				recreate toybox constraint if needed
				'''
				if toyboxCheckResultList[1] == True:
					# stupid shit... need to create a button so i can pass a 'Control' for the 'rs_MakeToyBoxRelations' fucntion to work
					button = FBButton()
					button.Name = original_namespace+':mover_Control'
					RS.Tools.ToyBox.rs_MakeToyBoxRelations(button, None)
							
							
class ThreeLateralReference:
	def __init__( self, Null, Path, CharacterMatchList ):
		self.Null = Null
		self.Path = Path
		self.CharacterMatchList = CharacterMatchList