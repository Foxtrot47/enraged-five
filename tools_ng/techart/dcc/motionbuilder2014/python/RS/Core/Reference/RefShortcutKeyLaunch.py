'''

 Script Path: RS/Core/Reference/RefShortcutKeyLaunch.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 09 December 2013
 
 Description: Launcher for the Ref System, for Kelly to assign as a hotkey

'''

from pyfbsdk import *

import RS.Tools.UI.ReferenceEditor

#launch for ref system
RS.Tools.UI.ReferenceEditor.Run()