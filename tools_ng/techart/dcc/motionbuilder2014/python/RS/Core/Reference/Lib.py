###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_RefSysCoreFunctions.py
## Written And Maintained By: David Bailey
## Contributors: Kristine Middlemiss
## Description: This tool allows Technical Artists To Set Up Scene That Will Alert Animators To Out Of Date
##              Resources In Their Scenes. It Will Also Sync The Latest Resounrces From Perforce And 
##              Automatically Merge Them Into The Scene.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
from pyfbsdk_additions import *

import datetime
import os
import difflib
import uuid

import RS.Utils.Creation as cre
import RS.Utils.Scene
import RS.Utils.Scene.Constraint
import RS.Utils.Scene.Property
import RS.Utils.Scene.Plug
import RS.Utils.Collections
import RS.Utils.Namespace
import RS.Core.Reference.ResetSetVectors as rsv
import RS.Globals as glo


import RS.Core.Face.AnimImporter
import RS.Perforce
import RS.Config
import RS.Utils.Widgets
import RS.Utils.Profiler
import RS.Core.Reference.Manager
import RS.Core.Face.Lib   
import RS.Core.Reference.RefSourceControlUtils as sc




###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gCharactersArray = []
gPropsArray = []
gSetsArray = []
gVehiclesArray = []
gRayFireArray = []
g3LateralArray = []
gAmbientFaceArray = []
gNamespaceArray = []
gProjRoot = RS.Config.Project.Path.Root
lReferenceNullList = []
lDeleteArray = []

class RsConfigDiffDialog( FBPopup ):
    '''
    Description:
        If the _Config.xml files are different you will get this dialogue allowing the user to choose
        what they would like to do, Replace, continue with old version or cancel.
    
    Author:
        Kristine Middlemiss <kristine.middlemiss@rockstartoronto.com>
        
    Example:
        
        # None is returned if the user clicked Cancel.
        result = core.RsConfigDiffDialog( 'XML Configuration Difference', version, lDiff )
        
        if result.value:
            # Do something.
    '''
    
    def __init__( self, title, version, diff1, diff2, size = [ 1410, 800 ] ):
        FBPopup.__init__( self )
        
        self.Caption = title
        self.Width = size[ 0 ]
        self.Height = size[ 1 ]
        
        # Position dialog in the middle of the screen.
        screenWidth, screenHeight, huh = FBSystem().DesktopSize.GetList()
        self.Top = int( ( screenHeight / 2.0 ) - ( self.Height / 2.0 ) )
        self.Left = int( ( screenWidth / 2.0 ) - ( self.Width / 2.0 ) )
        
        # Setup layout
        mainLyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 10, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( -10, FBAttachType.kFBAttachBottom, '' )
        
        self.AddRegion( 'main', 'main', x, y, w, h )
        self.SetControl( 'main', mainLyt )

        labelx = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        labely = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, '' )
        labelw = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        labelh = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, None )   
        mainLyt.AddRegion( "label", "label", labelx, labely, labelw, labelh )      
        
        label1x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        label1y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, '' )
        label1w = FBAddRegionParam( 685, FBAttachType.kFBAttachNone, None )
        label1h = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, None )   
        mainLyt.AddRegion( "label1", "label1", label1x, label1y, label1w, label1h )     

        list1x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        list1y = FBAddRegionParam( 60, FBAttachType.kFBAttachTop, '' )
        list1w = FBAddRegionParam( 685, FBAttachType.kFBAttachNone, None )
        list1h = FBAddRegionParam( -80, FBAttachType.kFBAttachBottom, '' )   
        mainLyt.AddRegion( "list1", "list1", list1x, list1y, list1w, list1h )                 
        
        label2x = FBAddRegionParam( 705, FBAttachType.kFBAttachLeft, '' )
        label2y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, '' )
        label2w = FBAddRegionParam( 685, FBAttachType.kFBAttachNone, None )
        label2h = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, None )   
        mainLyt.AddRegion( "label2", "label2", label2x, label2y, label2w, label2h ) 

        list2x = FBAddRegionParam( 705, FBAttachType.kFBAttachLeft, '' )
        list2y = FBAddRegionParam( 60, FBAttachType.kFBAttachTop, '' )
        list2w = FBAddRegionParam( 685, FBAttachType.kFBAttachNone, None )
        list2h = FBAddRegionParam( -80, FBAttachType.kFBAttachBottom, '' )   
        mainLyt.AddRegion( "list2", "list2", list2x, list2y, list2w, list2h )   
        
        reminderx = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        remindery = FBAddRegionParam(-65, FBAttachType.kFBAttachBottom, '' )
        reminderw = FBAddRegionParam( 800, FBAttachType.kFBAttachNone, None )
        reminderh = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, None )   
        mainLyt.AddRegion( "reminder", "reminder", reminderx, remindery, reminderw, reminderh )                                              

        replacex = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        replacey = FBAddRegionParam( -40, FBAttachType.kFBAttachBottom, '' )
        replacew = FBAddRegionParam( 453, FBAttachType.kFBAttachNone, None )
        replaceh = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, None )    
        mainLyt.AddRegion( "replace", "replace", replacex, replacey, replacew, replaceh )    

        continuex = FBAddRegionParam( 10, FBAttachType.kFBAttachRight, 'replace' )
        continuey = FBAddRegionParam( -40, FBAttachType.kFBAttachBottom, '' )
        continuew = FBAddRegionParam( 453, FBAttachType.kFBAttachNone, None )
        continueh = FBAddRegionParam(  30, FBAttachType.kFBAttachNone, None )    
        mainLyt.AddRegion( "continue", "continue", continuex, continuey, continuew, continueh )    
        
        cancelx = FBAddRegionParam( 10, FBAttachType.kFBAttachRight, 'continue' )
        cancely = FBAddRegionParam(  -40, FBAttachType.kFBAttachBottom, '' )
        cancelw = FBAddRegionParam( 453, FBAttachType.kFBAttachNone, None )
        cancelh = FBAddRegionParam( 30, FBAttachType.kFBAttachNone, None )    
        mainLyt.AddRegion( "cancel", "cancel", cancelx, cancely, cancelw, cancelh )    

        self.label = FBLabel()
        self.label.Caption = version
        self.label.Style = FBTextStyle.kFBTextStyleBold

        self.label1 = FBLabel()
        self.label1.Caption = "Skeleton version in current scene (OLD):"

        self.label2 = FBLabel()
        self.label2.Caption = "Skeleton in the new version (NEW):"   
        
        list1 = FBList()
        list1.Style = FBListStyle.kFBVerticalList
        for item in diff1.Items:
            list1.Items.append(item)

        list2 = FBList()
        list2.Style = FBListStyle.kFBVerticalList
        for item2 in diff2.Items:
            list2.Items.append(item2)
            
        self.reminder = FBLabel()
        self.reminder.Caption = "*The whole XML file is not shown above, only the parts that are different."                 
            
        replaceButton = FBButton()
        replaceButton.Caption = 'Rename/Replace Skeleton' 
        replaceButton.Hint = "This will finish the merge/sync with renaming and replacing your character with the most up-to-date version of the skeleton that you are using."
        replaceButton.OnClick.Add( self._onReplace_Clicked )      
        
        continueButton = FBButton()
        continueButton.Caption = 'Contine Without Updating'       
        continueButton.Hint = "This will finish the merge/sync with the older version of the skeleton that you are already using."
        continueButton.OnClick.Add( self._onContinue_Clicked )   
                        
        cancelButton = FBButton()
        cancelButton.Caption = 'Cancel Sync/Merge'
        cancelButton.Hint = "This will stop the merge/sync from executing anymore code."               
        cancelButton.OnClick.Add( self._onCancel_Clicked )
        
        mainLyt.SetControl("label", self.label)
        mainLyt.SetControl("label1", self.label1)
        mainLyt.SetControl("label2", self.label2)
        mainLyt.SetControl("list1", list1)
        mainLyt.SetControl("list2", list2)
        mainLyt.SetControl("reminder", self.reminder)        
        mainLyt.SetControl("replace", replaceButton) 
        mainLyt.SetControl("continue", continueButton)
        mainLyt.SetControl("cancel", cancelButton)        
        
        self.value = None        
        self.Show()
        
    def _onReplace_Clicked( self, control, event ):
        self.value = 1
        self.Close( True )
        
    def _onContinue_Clicked( self, control, event ):
        self.value = 2
        self.Close( True )        

    def _onCancel_Clicked( self, control, event ):
        self.value = None
        self.Close( True )

def rs_CheckForMultipleSceneReferences( silent = False ):
    '''
    Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
    
    This function checks for multiple instances of "REFERENCE: Scene" objects and asks the user
    if they'd like to be cleaned up.  In the case of a cutscene, the following scenario happened to Technicolor:
    
        + REFERENCE: Scene
        ++ RS_Null:Character_A
        
        + REFERENCE 1: Scene
        ++ RS_Null 1:Player_One
    
        + REFERENCE 2: Scene
        ++ RS_Null 2:Player_Zero    
    
    The above situation causes issues with the referencing system, so the goal of this function is to fix this up.    
    
    This function will first find the bad REFERENCE nodes.  Any RS_Null objects under each one will get re-parented to the
    correct REFERENCE node.  Additionally, their namespace will get fixed so that they point back to RS_Null:Something.
    Finally, the bad REFERENCE nodes will get deleted if they have no more children.
    
    For reference, this function is to fix B* 894941.
    '''
    
    # Find valid referencing system node.
    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()   

    if lReferenceNull:
        
        # Check for multiple REFERENCE nulls in the scene.  If found, prompt and potentially fixup.
        refObjs = []
        refObjComponents = RS.Utils.Scene.FindObjectsByNamespace( "REFERENCE *", namespaceOnly = True )
        
        # Collect only the bad reference scene nodes.
        for refObj in refObjComponents:

            try:
                namespace, name = str( refObj.LongName ).split( ':' )
                
            except ValueError:
                names = str( refObj.LongName ).split( ':' )
                name = names[ -1 ]
                namespace = names[ 0 ]
                namespaces = names[ 1 : -1 ]
                
                # Rebuild the namespace.
                for ns in namespaces:
                    namespace += ':{0}'.format( ns )
            
            if namespace != 'REFERENCE':
                if 'REFERENCE' in namespace and 'Scene' in name:
                    refObjs.append( refObj )
        
        if refObjs:
            result = 1
            
            if not silent:
                result = FBMessageBox( "Rockstar Referencing System", "Multiple 'REFERENCE: Scene' nodes were found!  This could cause an issue with referencing assets if the issue isn't fixed.\nWould you like for the referencing system to attempt to fix this issue?", "Yes", "No" )
            
            # Fixup the reference nodes.
            if result == 1:
                
                # Iterate over all of the found REFERENCE nodes.
                for refObj in refObjs:
                    try:
                        namespace, name = str( refObj.LongName ).split( ':' )
                        
                    except ValueError:
                        names = str( refObj.LongName ).split( ':' )
                        name = names[ -1 ]
                        namespace = names[ 0 ]
                        namespaces = names[ 1 : -1 ]
                        
                        # Rebuild the namespace.
                        for ns in namespaces:
                            namespace += ':{0}'.format( ns )
                    
                    # Skip the valid REFERENCE node, and only operate on the bad ones.
                    if namespace != 'REFERENCE':
                        for refChild in refObj.Children:
                            if isinstance( refChild, FBModelNull ):
                                try:
                                    rsNullNamespace, rsNullName = str( refChild.LongName ).split( ':' )
                                    if rsNullNamespace != 'RS_Null':
                                        
                                        # Fixup namespace
                                        refChild.LongName = 'RS_Null:' + rsNullName
                                        
                                    # Move it under the correct reference null.
                                    refChild.Parent = lReferenceNull
                                except:
                                    pass
                        FBSystem().Scene.Evaluate()
                                
                        # Delete the bad reference null.
                        if len( refObj.Children ) == 0:
                            refObj.FBDelete()
                            
                            
        del( refObjs )
        refObjChildren = []
        # There is scenario where you have a valid REFERNCE:Scene, but the RS_Null under it have multiple namespaces, need to provide a clean up for that too.
        refObjRSNulls = RS.Utils.Scene.FindObjectsByNamespace( "RS_Null *", namespaceOnly = True )

        if refObjRSNulls:
            result = 1
            
            if not silent:
                result = FBMessageBox( "Rockstar Referencing System", "Multiple 'RS_Null' namespaces were found!  This could cause an issue with referencing assets if the issue isn't fixed.\nWould you like for the referencing system to attempt to fix this issue?", "Yes", "No" )
            
            # Fixup the reference nodes.
            if result == 1:        

                # Collect only the bad RS_Null scene nodes.
                for refObjChild in refObjRSNulls:
                    rsNullNamespace, rsNullName = str( refObjChild.LongName ).split( ':' )
                    if rsNullNamespace != 'RS_Null':
                        
                        # Fixup namespace
                        refObjChild.LongName = 'RS_Null:' + rsNullName
        del( refObjChildren )
        return True
    
    else:
        return False

#This function will only have an effect on GTA. Added for [1838014] bit hacky having project specific code but can be removed after NG.
def rs_CheckFaceAssets():
    
    cgCutsceneDataPath = "x:\\gta5\\art\\animation\\cutscene"
    ngCutsceneDataPath = "x:\\gta5\\art\\ng\\animation\\cutscene"
    
    
    #Get fbxFilename and check we are working on a NG cutscene
    fbxFilename = glo.Application.FBXFileName.lower()
    if fbxFilename.startswith(ngCutsceneDataPath):
        
        refManager = RS.Core.Reference.Manager.RsReferenceManager()
        refManager.collectSceneReferences()        
        
        #For each face reference
        badFaceReferences = list()
        for faceReference in refManager.face:
            if faceReference.referencePath != None and faceReference.referencePath.lower().startswith(cgCutsceneDataPath):
                badFaceReferences.append(faceReference)
                
        #Fix face file paths
        if len(badFaceReferences) > 0:
            FBMessageBox("Info","Some face references that point to old generation assets have been found. They will be repointed to next generation assets.", "Ok")
            for badFaceReference in badFaceReferences:
                badFaceReference.referencePath = badFaceReference.referencePath.lower().replace( cgCutsceneDataPath, ngCutsceneDataPath )
                badFaceReference.p4Version = "0"
                
        #Check if any face references need updating
        for faceReference in refManager.face:
            if faceReference.p4Version == "0":
                FBMessageBox("Info","Some face references require updating - please do so at a convenient point.", "Ok")
                break
        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Needed for the function rs_CheckForToyBoxPropOnMover
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def FindAnimationNode( pParent, pName ):
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Need to add code for the scenario where you create a ToyBox constraint, but then re remerge the character via the Referencing system, everthing mostly 
##              works except, the Sent to ToyBox Property is no longer on the chassis_Control and Mover_Control, this seemed like the best place to add the code check,
##              because the reference system is so slow alrealy don't want to add more code there.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CheckForToyBoxPropOnMover():
    lToyBoxOverRideNull = FBFindModelByLabelName("ToyBoxOverRideNull")
    if lToyBoxOverRideNull:
        glo.gUlog.LogMessage( 'Checking for toy box prop on mover.' )
        
        lCreateToyBoxProperty = False
        
        for lCon in glo.gConstraints:
        
            if lCon.Name == "mover_Control_SendToToyBox":
                lFullName = lCon.LongName
                lNamespace = lFullName.split(":")
        
                lControlModel = FBFindModelByLabelName(lNamespace[0] + ":mover_Control")    
                if lControlModel:
                    if lControlModel.PropertyList.Find("Send to ToyBox") == None:                
                        lToyBoxProp = lControlModel.PropertyCreate("Send to ToyBox", FBPropertyType.kFBPT_int, "Number", True, True, None)
                        lToyBoxProp.SetMin(0)
                        lToyBoxProp.SetMax(1)
                        lToyBoxProp.SetAnimated(True)    
                        lCreateToyBoxProperty = True
                        for lBox in lCon.Boxes:                   
                            if lBox.Name.startswith("IF Cond Then A Else B"):
                                lConIn = FindAnimationNode( lBox.AnimationNodeInGet(), 'Cond' )
                            if lBox.Name.startswith("mover_Control"):
                                lSendtoToyBoxOut = FindAnimationNode( lBox.AnimationNodeOutGet(), 'Send to ToyBox' )
                                if lSendtoToyBoxOut and lConIn:
                                    FBConnect( lSendtoToyBoxOut, lConIn )
        
            if lCon.Name == "mover_Control_ToyBox":
                if lCreateToyBoxProperty == True:
                    for lBox in lCon.Boxes:
                        if lBox.Name.startswith("IF Cond Then A Else B"):
                            lResultOut = FindAnimationNode( lBox.AnimationNodeOutGet(), 'Result' )
                        if lBox.Name.startswith("mover_Control"):                    
                            lSendtoToyBoxIn = FindAnimationNode( lBox.AnimationNodeInGet(), 'Send to ToyBox' )                                    
                            if lResultOut and lSendtoToyBoxIn:
                                FBConnect( lResultOut, lSendtoToyBoxIn )
                                
        glo.gUlog.LogMessage( 'Finished checking for toy box prop on mover.' )

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Returns Files P4 Revision
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_P4_Revision( pFilePath, pRevision ): 
    fileInfo = RS.Perforce.GetFileState( sc.ToSafeP4Path( pFilePath ))
    
    if fileInfo:
        if pRevision == "haveRev":        
            return str( fileInfo.HaveRevision )
        
        elif pRevision == "headRev":    
            return str( fileInfo.HeadRevision )
        
    return None


def rs_CreateAmbientFaceRigReference( refNamespace = None, refFbxFilename = None, refreshUI = True, getLatest = True ):
    '''
    Description:
        Creates a reference for an ambient face rig.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        
    Keyword Arguments:
        refNamespace: The namespace of the reference to create.
        refFbxFilename: The fbx filename for where the reference comes from.
        refreshUI: Whether or not we update the Reference Editor UI after completion.
        getLatest: Whether or not to get latest on the reference before deleting.
    '''  
    
    # Create an instance of the referencing system manager and collect the scene references.
    mgr = RS.Core.Reference.Manager.RsReferenceManager()
    mgr.collectSceneReferences()
    
    # Path to let the user choose the namespace, fbx filename, etc.
    if refNamespace == None and refFbxFilename == None:
        
        # Choose the character for the ambient rig.
        charNames = [ ref.name for ref in mgr.characters ]
        charNames.sort()
        
        pickCharDialog = RS.Utils.Widgets.RsChoiceDialog( 'Choose a Character for the Ambient Rig', charNames, size = [ 220, 86 ] )
        characterName = pickCharDialog.value
        
        if characterName:
            
            # Enter a name for the ambient rig.
            result, ambientRigName = FBMessageBoxGetUserValue( 'Enter Name for the Ambient Rig', characterName, '', FBPopupInputType.kFBPopupString, 'OK', 'Cancel' )
            
            if result == 1:
                
                # Setup the namespace for the ambient rig.
                refNamespace = '{0}:{1}'.format( ambientRigName, characterName )
                
                # Ensure that this doesn't already exist in the scene.
                ambientUIModelNull = FBFindModelByLabelName( '{0}:Ambient_UI'.format( refNamespace ) )
                
                if ambientUIModelNull:
                    FBMessageBox( 'Rockstar', 'The character "{0}" is already using the ambient rig name "{1}"!  Please try again with a different rig name.'.format( characterName, ambientRigName ), 'OK' )
                    return False
        
                # Choose the _ANIM.fbx file.
                selectDialog = FBFilePopup()
                selectDialog.Caption = 'Please choose the animation FBX file...'
                selectDialog.Style = FBFilePopupStyle.kFBFilePopupOpen
                selectDialog.Filter = '*.fbx'
                selectDialog.Path = '{0}\\art\\animation\\cutscene\\'.format( RS.Config.Project.Path.Root )
                
                result = selectDialog.Execute()
                
                # Begin merging in the new reference.
                if result:
                    refFbxFilename = '{0}\\{1}'.format( selectDialog.Path, selectDialog.FileName )           
                
    # Create the ambient face rig reference.
    if refNamespace and refFbxFilename:
        if getLatest:
            animFileInfo = RS.Perforce.Sync( sc.ToSafeP4Path( refFbxFilename ))
            
        else:
            animFileInfo = RS.Perforce.GetFileState( sc.ToSafeP4Path( refFbxFilename ))
             
        # No 'REFERENCE:Scene' null, so create one.
        if not mgr.root:
            FBModelNull( '{0}:{1}'.format( RS.Core.Reference.Manager.STR_REFERENCE_SCENE_NAMESPACE, RS.Core.Reference.Manager.STR_REFERENCE_SCENE_NAME ) )
            mgr.collectSceneReferences()
            
        # Create reference null for the ambient rig.
        referenceNull = FBModelNull( '{0}:{1}'.format( RS.Core.Reference.Manager.STR_RS_NULL_NAMESPACE, refNamespace ) )
        referenceNull.Parent = mgr.root
        
        # Create custom properties.
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_REFERENCE_PATH, FBPropertyType.kFBPT_charptr, 'String', False, True, None )
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_NAMESPACE, FBPropertyType.kFBPT_charptr, 'String', False, True, None ) 
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_RS_ASSET_TYPE, FBPropertyType.kFBPT_charptr, 'String', False, True, None ) 
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_RELOAD, FBPropertyType.kFBPT_bool, 'Bool', True, True, None ) 
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_P4_VERSION, FBPropertyType.kFBPT_charptr, 'String', False, True, None ) 
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_UPDATE, FBPropertyType.kFBPT_bool, 'Bool', True, True, None )
        referenceNull.PropertyCreate( RS.Core.Reference.Manager.STR_TAKE_NAME, FBPropertyType.kFBPT_charptr, 'String', False, True, None ) 
        
        
        # Setup initial custom property values.
        prop = referenceNull.PropertyList.Find( RS.Core.Reference.Manager.STR_REFERENCE_PATH )
        prop.Data = refFbxFilename
        
        prop = referenceNull.PropertyList.Find( RS.Core.Reference.Manager.STR_NAMESPACE )
        prop.Data = refNamespace
        
        prop = referenceNull.PropertyList.Find( RS.Core.Reference.Manager.STR_RS_ASSET_TYPE )
        prop.Data = RS.Core.Reference.Manager.TYPE_FACE_AMBIENT
        
        prop = referenceNull.PropertyList.Find( RS.Core.Reference.Manager.STR_P4_VERSION )
        
        if animFileInfo:
            prop.Data = str( animFileInfo.HaveRevision )
            
        else:
            prop.Data = '1'
        
        # Setup merge options, leave take start/end alone.
        options = FBFbxOptions( True, refFbxFilename )
        options.SetAll( FBElementAction.kFBElementActionMerge, False )
        options.Story = FBElementAction.kFBElementActionDiscard
        options.ModelsAnimation = True
        options.TakeSpan = FBTakeSpanOnLoad.kFBLeaveAsIs
        options.NamespaceList = 'AMBIENT_FACE_RIG_MERGE'
        
        # Bug - 1195174 
        options.BaseCameras = False
        options.CameraSwitcherSettings = False
        options.CurrentCameraSettings = False
        options.CamerasAnimation = False
        options.TransportSettings = False
        options.Cameras = FBElementAction.kFBElementActionDiscard
        options.GlobalLightingSettings = False

        # Merge
        FBApplication().FileMerge( refFbxFilename, False, options )
        
        # Find Ambient_UI root.
        modelList = FBComponentList()
        FBFindObjectsByName( '{0}*:Ambient_UI'.format( 'AMBIENT_FACE_RIG_MERGE' ), modelList, True, True )
        
        if modelList:
            def recurseRenameNamespace( null, charNamespace ):
                '''
                Recursively renames the ambient ui nulls that are merged into the scene file.
                '''
                for child in null.Children:
                    child.LongName = '{0}:{1}'.format( charNamespace, child.Name )
                    
                    recurseRenameNamespace( child, charNamespace )                     

            ambientUIRoot = modelList[ 0 ]
            ambientUIRoot.LongName = '{0}:Ambient_UI'.format( refNamespace )
            
            recurseRenameNamespace( ambientUIRoot, refNamespace )
            
            # Fixup the ambient rig.
            RS.Core.Face.Lib.fixAmbientFacialRig( refNamespace )
            
            # Create a group for the ambient rig.
            
            if refreshUI:
                rs_RefreshReferenceUI()
            
        else:
            FBMessageBox( 'Rockstar', 'Could not find the Ambient_UI root null for "{0}"!  The scene is in a potentially unstable state, please contact Tech Art.'.format( refNamespace ), 'OK' )
            return False
                
def rs_UpdateAmbientFaceRigReference( charNamespace, getLatest = True ):
    '''
    Description:
        Updates an ambient face rig reference.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        
    Arguments:
        charNamespace: The namespace of the reference to update.
        
    Keyword Arguments:
        getLatest: Whether or not to get latest on the reference before updating.
    '''
    
    mgr = RS.Core.Reference.Manager.RsReferenceManager()
    mgr.collectSceneReferences()
    
    ref = mgr.getReferenceByName( charNamespace )
    
    if ref:
        refFilename = ref.referencePath
        
        rs_DeleteAmbientFaceRigReference( charNamespace, refreshUI = False )
        rs_CreateAmbientFaceRigReference( refNamespace = charNamespace, refFbxFilename = refFilename, refreshUI = False, getLatest = getLatest )

        rs_RefreshReferenceUI()
        
    else:
        FBMessageBox( "Warning", "The ambient face rig reference could not be found!", "OK")
        

def rs_DeleteAmbientFaceRigReference( charNamespace, refreshUI = True ):
    '''
    Description:
        Deletes an ambient face rig reference.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        
    Arguments:
        charNamespace: The namespace of the reference to delete.
        
    Keyword Arguments:
        refreshUI: Whether or not we update the Reference Editor UI after completion.
    '''  
    
    # Delete Ambient_UI nulls.
    ambientUINull = FBFindModelByLabelName( '{0}:Ambient_UI'.format( charNamespace ) )
    
    if ambientUINull:
        childrenNulls = []
        
        def recurseCollectChildren( null, children ):
            for child in null.Children:
                children.append( child )
                recurseCollectChildren( child, children )
                
        recurseCollectChildren( ambientUINull, childrenNulls )
        
        for child in childrenNulls:
            child.FBDelete()
            
        ambientUINull.FBDelete()
        
    # Delete reference null.
    referenceNull = FBFindModelByLabelName( 'RS_Null:{0}'.format( charNamespace ) )
    
    if referenceNull:
        referenceNull.FBDelete()
        
    if refreshUI:
        rs_RefreshReferenceUI()


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Renames "rs_Type" Of References 
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_CompareAndRename( pOriginalArray, pSceneElement, pNamespace, pAssetType, lGetLatestTexture, quiet = False):
        lReferencedArray = list(pSceneElement)
        lReferencedArray = RS.Utils.Collections.ReturnUnmatchedItems( pOriginalArray, lReferencedArray)
        lProp = None
        lAddToFolder = []
        
        lTextureArray = []
                    
        for iReference in lReferencedArray:
            lAddToFolder.append(iReference)
            for iDestination in range(iReference.GetDstCount()):
                if iReference.GetDst(iDestination).ClassName() == "FBFolder":
                    if lAddToFolder.count(iReference) > 0:
                        lAddToFolder.remove(iReference) 
            lProp = iReference.PropertyList.Find('rs_Type')
            
            if lProp:
                if lProp.Data != "rs_Characters":
                    # need to add a catcher for when the user multi-creates a reference url:bugstar:1981058
                    # it was adding 'REFERENCED:' data in for all irerations of the asset in each component (which was causing issues when stripping and deleteing)
                    if "REFERENCED:" in lProp.Data:
                        None
                    else:
                        lProp.Data = "REFERENCED:" + pAssetType + ":" + pNamespace + ":" + lProp.Data
                    
            if iReference.ClassName() == "FBTexture":
                
                lTextureArray.append(iReference)
        
        if pSceneElement == glo.gTextures and pAssetType != "Sets":
            # Do not sync in quiet mode (faster).
            #if quiet:
                #lGetLatestTexture = 0
                           
            if lGetLatestTexture == str(1):
                lTexturesToSync = []
                # Collect the textures
                for iVideoClip in lTextureArray:
                    if iVideoClip.Video != None:
                        if not os.path.isfile( iVideoClip.Video.Filename ):
                            lTexturesToSync.append( iVideoClip.Video.Filename )
                            
                        if not iVideoClip.Video.Filename in lTexturesToSync:
                            lTexturesToSync.append( iVideoClip.Video.Filename )
                
                RS.Perforce.Sync( lTexturesToSync )
                glo.gUlog.LogMessage( 'Synced textures: {0}.'.format( lTexturesToSync ) )
                for iVideoClip in lTextureArray:
                    
                    lVid = iVideoClip.Video
                    if lVid:
                        lPath = lVid.Filename
                        iVideoClip.Video.Filename = None
                        iVideoClip.Video.Filename = lPath
        
        del (lReferencedArray, lAddToFolder, lProp)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Is A Small UI To Get Reference Namespace From The User
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ReferenceNamespaceUI( pVal, pType ):
        
    lResult = FBMessageBoxGetUserValue( "Enter Namespace", "Namespace: ", pVal, pType, "Ok", "Cancel" ) 
    
    if lResult[0] == 1:
        return lResult[1]
    
    elif lResult[0] == 2:
        return None

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Is A Small UI Telling the user they need to refresh the UI.
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################        
        
def rs_RefreshReferenceUI():
    import RS.Tools.UI.ReferenceEditor
    RS.Tools.UI.ReferenceEditor.Run()

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Creates A Scene Reference
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

def rs_CreateReference( pControl, pPath, multiples=1, quiet = False, profileObj = None, returnReferenceNull = False, forcedNamespace = None ):  
    global gNamespaceArray
    global gProjRoot        

    lGetLatestTexture = None

    lIgDeleteArray = ["ShadowLight",
                      "AdditonalLight1",
                      "AdditonalLight2",
                      "AdditonalLight3",
                      "ShadowPlane",
                      "ShadowPlaneTexture",
                      "Live Shadow"]
        
    lLoadReference = 0
    lInputCharacter = 0
    
    #Stores the newly created references for return if needed
    newRefResults = []    
    
    # For newly created References and for batch Create Reference pControl == Path         
    if pControl == "REF" or "resources" in pControl:
        lGetLatestTexture = pPath 

    elif pControl == "LOAD":
        lLoadReference = 1
        
        ''' old script for comparing skeletons '''        
        #lConfigName = (pPath[2].rpartition(".")[0] + "_Config.xml")
        
        #if os.path.isfile(lConfigName):
            
            #lFStat = p4.rs_FStatFile(os.path.dirname(lConfigName), os.path.basename(lConfigName))
    
            #lHeadRev = 0
            
            #for iStat in lFStat:
                #if "headRev" in iStat:
                    #lHeadRev = int(iStat.rpartition(" ")[2])
            #if lHeadRev == 1:
                #None
            #else:
                
                #p4.rs_SyncFile(os.path.dirname(lConfigName), os.path.basename(lConfigName), (lHeadRev - 1))
                #lConfig1 = open(lConfigName, "r")
                #lConfig1_Lines = lConfig1.readlines()
                #del(lConfig1)
                
                ## I think this is forcing a sync before you can even cancel in the config Diff window...
                #p4.rs_SyncFile(os.path.dirname(lConfigName), os.path.basename(lConfigName), "Force")
                #lConfig2 = open(lConfigName, "r")
                #lConfig2_Lines = lConfig2.readlines()
                #del(lConfig2)
                
                #list1 = FBList()
                #list1.MultiSelect = True               
                #list2 = FBList()
                #list2.MultiSelect = True                       
                
                #for line in difflib.unified_diff(lConfig1_Lines, lConfig2_Lines, lineterm=''):
                    #if line.startswith("---"):
                        #list1.Items.append(line)
        
                    #elif line.startswith("+++"):
                        #list2.Items.append(line)
        
                    #elif line.startswith("-"):
                        #list1.Items.append(line)
                        #list2.Items.append("\n")
                              
                    #elif line.startswith("+"):
                        #list2.Items.append(line)
                        #list1.Items.append("\n")                
                    #else:
                        #list1.Items.append(line)
                        #list2.Items.append(line)        
               
                #if len(list1.Items) > 0 and len(list2.Items) > 0:                     

                    #version = "There is a Difference Between the Skeletons in Version " + str(lHeadRev - 1) + " and " + str(lHeadRev) + " in Reference " + pPath[0] + "\n"
                    
                    #result = RsConfigDiffDialog( 'Warning', version, list1, list2 )
                    
                    #if result.value == None:
                        #return result.value
                    
                    ## Replace/Rename
                    #elif result.value == 1:
                        #for iComponent in glo.gComponents:
                            
                            #if iComponent.LongName.partition(":")[0] == pPath[0]:
                                
                                #iComponent.LongName = "oLd*" + iComponent.LongName
                        
                        ## Removal of OLD
                        #lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", ("RS_Null:oLd*" + pPath[0]))            
                        #lReferenceNull.PropertyCreate('Reference Path', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                        #lReferenceNull.PropertyCreate('Namespace', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                        #lReferenceNull.PropertyCreate('rs_Asset_Type', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                        #lReferenceNull.PropertyCreate('Reload', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                        #lReferenceNull.PropertyCreate('Take_Name', FBPropertyType.kFBPT_charptr, 'String', False, True, None)
                        #lReferenceNull.PropertyCreate('Import_Option', FBPropertyType.kFBPT_charptr, 'String', False, True, None)
                        #lReferenceNull.PropertyCreate('Create_Missing_Takes', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                        #lProp = lReferenceNull.PropertyList.Find('Reference Path')
                        #lProp.Data = pPath[2]
                        #lProp = lReferenceNull.PropertyList.Find('Namespace')
                        #lProp.Data = "oLd*" + pPath[0]
                        #lProp = lReferenceNull.PropertyList.Find('rs_Asset_Type')
                        #lReferenceType = ""
                        #if "characters" in pPath[2]:
                            #lReferenceType = "Characters"
                        #elif "sets" in pPath[2]:
                            #lReferenceType = "Sets"
                        #elif "props" in pPath[2]:
                            #lReferenceType = "Props"
                        #elif "vehicles" in pPath[2]:
                            #lReferenceType = "Vehicles"
                        #elif "rayfire" in pPath[2]:
                            #lReferenceType = "Rayfire"
                        #elif "\\facial\\" in pPath[2].lower():
                            #lReferenceType = "3Lateral"
                        #elif "\\face\\" in pPath[2].lower():
                            #lReferenceType = "3Lateral"
                        #lProp.Data = lReferenceType
                        #lSceneNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
                        #if lSceneNull:
                            #lReferenceNull.Parent = lSceneNull    

                        #lInputCharacter = 1
                        
    import RS.Utils.Profiler    
    RS.Utils.Profiler.start( 'Converting lists', profileObj )
                    
    lActorFaceArray = list(glo.gActorFaces)
     
    lActorArray = list(glo.gActors)
      
    lAudioClipArray = list(glo.gAudioClips)
        
    lCameraArray = list(glo.gCameras)
    
    lCharacterFaceArray = list(glo.gCharacterFaces)
    
    lCharacterPoseArray = list(glo.gCharacterPoses)
    
    lCharacterArray = list(glo.gCharacters)
                
    lConstraintSolverArray = list(glo.gConstraintSlovers)
       
    lConstraintArray = list(glo.gConstraints)
    
    lControlSetArray = list(glo.gControlSets)
        
    lDeviceArray = list(glo.gDevices)
            
    lFolderArray = list(glo.gFolders)
      
    lGroupArray = list(glo.gGroups)
    
    lHandleArray = list(glo.gHandles)
    
    lLightArray = list(glo.gLights)
      
    lMarkerSetArray = list(glo.gMarkerSets)
      
    lMaterialArray = list(glo.gMaterials)
       
    lNotesArray = list(glo.gNotes)
    
    lObjectPoseArray = list(glo.gObjectPoses)
        
    lPhysicalPropertyArray = list(glo.gPhysicalProperties)
       
    lPoseArray = list(glo.gPoses)
    
    lSetsArray = list(glo.gSets)
               
    lShaderArray = list(glo.gShaders)
      
    lTakeArray = list(glo.gTakes)
            
    lTextureArray = list(glo.gTextures)
            
    lUserObjectArray = list(glo.gUserObjects)
                  
    lVideoClipArray = list(glo.gVideoClips)
    
    RS.Utils.Profiler.stop()
    
    RS.Utils.Profiler.start( 'Building lRootModelArray', profileObj )
    
    lRootModelArray = []
    
    for lRootModel in glo.gRoot.Components:
        RS.Utils.Scene.GetChildren(lRootModel, lRootModelArray)
        
    RS.Utils.Profiler.stop()
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Merge In Referenced Scene
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 
        
    lNamespace = None
    lAssetType = ""
    lImportOption = ""
    lTakeName = ""
    lCreateMissingTakes = ""
    lReferenceNull = None
    cancelAction = False

    # Only when creating a reference from scratch
    if lLoadReference == 0:
        # Need to add code for if parameter one is a path:
        if pControl == "REF":         
            lFilePopup = FBFilePopup();
            lFilePopup.Filter = '*.fbx'
            lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
            lFilePopup.Path = gProjRoot + "\\art\\animation\\resources"
            
            lFilePath = ""
            
            if lFilePopup.Execute():
                lFilePath = lFilePopup.FullFilename
            else:
                return
        # Scenario when you are doing Batch Create References
        else: 
            lFilePath = pControl              
        
        lSceneNull = None
        
        lSceneNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
        if lSceneNull == None:
            lSceneNull = FBCreateObject( "Browsing/Templates/Elements", "Null", RS.Core.Reference.Manager.GetReferenceSceneNullName() )
            glo.gUlog.LogMessage( 'Created "REFERENCE:Scene" null.' )
    

    for ver in range(multiples):
        if lLoadReference == 0: 
            # Only when creating a reference from scratch
            lFileName = RS.Utils.Path.GetBaseNameNoExtension(lFilePath)       
                
            RS.Perforce.Sync( sc.ToSafeP4Path( lFilePath ))
            glo.gUlog.LogMessage( 'Synced file: {0}'.format( lFilePath ) )        
            
            lReferenceNullList = []
            lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
            
            # From the root, get all the children which are the registered reference namespaces in the scene
            if lReferenceNull:
                RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)                
 
            lCount = 0
            
            if len(lReferenceNullList) >=1:
                for iRef in lReferenceNullList:   
                    if iRef.Name.startswith(lFileName):
                        lCount = lCount + 1
                        
                        
            if lCount != 0:
                lFileName = lFileName + "^" + str(lCount)
            
            
            
            # update filename/namespace handle if user changes it
            if quiet:
                lNamespace = lFileName
            else:
                lNamespace = rs_ReferenceNamespaceUI( lFileName, FBPopupInputType.kFBPopupString )
                if lNamespace:
                    lFileName = lNamespace 
                else:
                    cancelAction = True
    
            if cancelAction == False:
                
                if forcedNamespace:
                    lNamespace = forcedNamespace
                    lFileName = forcedNamespace
                    
                RS.Utils.Profiler.start( 'Creating custom attributes', profileObj )
                
                lReferenceNull = FBCreateObject( "Browsing/Templates/Elements", "Null", ("RS_Null:" + lFileName))            
                lReferenceNull.PropertyCreate('Reference Path', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                lReferenceNull.PropertyCreate('Namespace', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                lReferenceNull.PropertyCreate('rs_Asset_Type', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                lReferenceNull.PropertyCreate('Reload', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                lProp = lReferenceNull.PropertyList.Find('Reference Path')
                lProp.Data = lFilePath
                lProp = lReferenceNull.PropertyList.Find('Namespace')
                lProp.Data = lNamespace
                lProp = lReferenceNull.PropertyList.Find('rs_Asset_Type')
                lReferenceType = ""
                if "characters" in lFilePath.lower():
                    lReferenceType = "Characters"
                elif "sets" in lFilePath.lower():
                    lReferenceType = "Sets"
                elif "props" in lFilePath.lower():
                    lReferenceType = "Props"
                elif "vehicles" in lFilePath.lower():
                    lReferenceType = "Vehicles"
                elif "rayfire" in lFilePath.lower():
                    lReferenceType = "Rayfire"
                elif "\\facial\\" in lFilePath.lower():
                    lReferenceType = "3Lateral"
                elif "\\face\\" in lFilePath.lower():
                    lReferenceType = "3Lateral"
                lProp.Data = lReferenceType
                lAssetType = lReferenceType
                        
                lP4version = rs_P4_Revision( lFilePath, "haveRev" )          
                lReferenceNull.PropertyCreate('P4_Version', FBPropertyType.kFBPT_charptr, 'String', False, True, None) 
                lReferenceNull.PropertyCreate('Update', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                lProp = lReferenceNull.PropertyList.Find('P4_Version')
                
                if lReferenceType == "3Lateral":
                    lReferenceNull.PropertyCreate('Import_Option', FBPropertyType.kFBPT_charptr, 'String', False, True, None)
                    lReferenceNull.PropertyCreate('Take_Name', FBPropertyType.kFBPT_charptr, 'String', False, True, None)
                    lReferenceNull.PropertyCreate('Create_Missing_Takes', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                                    
                # Check to make sure None doesn't get written into the file - 1075797
                if lP4version != None:
                    lProp.Data = lP4version
                    
                else:
                    lProp.Data = '1'
                    
                RS.Utils.Profiler.stop()
                    
                lReferenceNull.Parent = lSceneNull
                newRefResults.append(lReferenceNull)
            else:
                FBMessageBox( "R* Cancel", "Create Reference action cancelled.", "Ok" )  
        
        else:
            lNamespace = pPath[0]
            lAssetType = pPath[1]
            lFilePath = pPath[2]
            lImportOption = pPath[3]
            lTakeName = pPath[4]
            lCreateMissingTakes = pPath[5]
            lGetLatestTexture = pPath[6]
         
                
        if cancelAction == False: 
            # Record current time range, in case we have to keep it after the merge.    
            keepTimeRange = False
            startTime = FBPlayerControl().LoopStart
            endTime = FBPlayerControl().LoopStop    
            
            # Record old fps.
            oldFpsMode = FBPlayerControl().GetTransportFps()
            oldFps = FBPlayerControl().GetTransportFpsValue()
            
            if oldFps != 30.0:
                if not quiet:
                    fpsResult = FBMessageBox( 'Rockstar', 'The FPS is currently set to {0}.  Would you like it to be set to 30 FPS?'.format( oldFps ), 'Yes', 'No' )
                    
                if quiet:
                    fpsResult = 1
                    
                if fpsResult == 1:
                    RS.Utils.Profiler.start( 'Set FPS to 30', profileObj )
                    
                    FBPlayerControl().SetTransportFps( FBTimeMode.kFBTimeMode30Frames, 30.0 )
                    
                    oldFpsMode = FBPlayerControl().GetTransportFps()
                    oldFps = FBPlayerControl().GetTransportFpsValue()
                    
                    RS.Utils.Profiler.stop()
                    glo.gUlog.LogMessage( 'Set FPS to 30.' )
            
            #Setup load options        
            lOptions = FBFbxOptions( True, lFilePath )
            lOptions.SetAll(FBElementAction.kFBElementActionMerge, False)
            for lTakeIndex in range( lOptions.GetTakeCount() ):
                lOptions.SetTakeSelect( lTakeIndex, False )
            
            # For Bug 731622 turn story merge OFF when sync/merging files
            lOptions.Story = FBElementAction.kFBElementActionDiscard
            lOptions.Lights = FBElementAction.kFBElementActionDiscard
            
            # Bug - 1195174 
            lOptions.BaseCameras = False
            lOptions.CameraSwitcherSettings = False
            lOptions.CurrentCameraSettings = False
            lOptions.CamerasAnimation = False         
            
            #1854762 - we must have animation on for constraint offsets to come through correctly (even if 
            #they are not animated - there is bug with autodesk about this because this is a change in behaviour 
            #from previous MB releases)
            lOptions.ConstraintsAnimation = True
            
            isNewRigThatNeedsUpdating = False
            rigRoot = FBFindModelByLabelName( '{0}:rig'.format( lNamespace ) )
            if rigRoot != None: 
                isNewRigThatNeedsUpdating = True
                
            # Import face animation.
            if lAssetType == "3Lateral":
                RS.Utils.Profiler.start( 'Import face animation', profileObj )            
                
                fileInfo = RS.Perforce.GetFileState( sc.ToSafeP4Path( lFilePath ))
                if fileInfo:                
                    if not RS.Perforce.IsLatest( sc.ToSafeP4Path( fileInfo )):
                        RS.Perforce.Sync( sc.ToSafeP4Path( lFilePath ))
                
                # Pop-up the import face animation dialog to choose options.
                if not quiet:
                    import RS.Core.Face.AnimImporterDialog
                    reload( RS.Core.Face.AnimImporterDialog )
                    
                    if not RS.Core.Face.AnimImporterDialog.show( lFilePath ):
                        return False
                    
                    importOptions = RS.Core.Face.AnimImporterDialog.getImportOptions()
                    
                    lImportOption =  lReferenceNull.PropertyList.Find('Import_Option')
                    lImportOption.Data = importOptions.importOnto
                    
                    lTakeName = lReferenceNull.PropertyList.Find('Take_Name')
                    lTakeName.Data = importOptions.currentTakeName
                    
                    lCreateMissingTakes = lReferenceNull.PropertyList.Find('Create_Missing_Takes')
                    lCreateMissingTakes.Data = importOptions.createMissingTakes
                    
                    glo.gUlog.LogMessage( 'Updated face animation.', context =  'Update Reference - {0}'.format( lNamespace ))
                    
                else:
                    import RS.Core.Face.AnimImporter
                    reload( RS.Core.Face.AnimImporter )
                    
                    faceAnimFileObj = RS.Core.Face.AnimImporter.fbxGetAnimationFromFile( lFilePath )
                    
                    if faceAnimFileObj:
                        importOptions = RS.Core.Face.AnimImporter.RsImportOptions()
                        
                        # Setup import options.
                        importOptions.clear()
                        
                        for takeName, takeObj in faceAnimFileObj.takes.iteritems():
                            for layerName, layerObj in takeObj.animationLayers.iteritems():
                                importOptions.addAnimationLayer( takeName, layerName )
                                
                        for characterName in faceAnimFileObj.characters:
                            importOptions.characters.append( characterName )
                        
                        # Get the Reference Null to help fill import options
                        importOptions.importOnto = lImportOption
                        importOptions.currentTakeName = lTakeName
                        importOptions.createMissingTakes = lCreateMissingTakes
                        
                        faceAnimSuccess = RS.Core.Face.AnimImporter.importFaceAnimation( faceAnimFileObj, importOptions, quiet = True )
                        
                        if not faceAnimSuccess:
                            return False
                        
                        glo.gUlog.LogMessage( 'Updated face animation.', context = 'Update Reference - {0}'.format( lNamespace ))
                        
                    else:
                        errorMsg = 'Could not load the FBX filename ({0})!  Cannot import face animation.'.format( lFilePath )
                        glo.gUlog.LogError( errorMsg )
                        
                        if not quiet:
                            FBMessageBox( 'Rockstar', errorMsg, 'OK' )
                            
                        
                        return False
                    # Keep existing time range.
                keepTimeRange = True 
                
                RS.Utils.Profiler.stop()
               
            #This is another hack *sigh* to move from different versions of a vehicle
            elif lAssetType == "Vehicles" and isNewRigThatNeedsUpdating:
                
                #We have to deactivate merge transation when it comes to updating the new vehicles - this is because the namespace functions don't react predictably
                mergeTransactionOn = FBMergeTransactionIsOn()
                if mergeTransactionOn:
                   FBMergeTransactionEnd()
                    
                #Merge in reference into a new namespace with a random UUID
                newNamespace = str( uuid.uuid1() )
                lOptions.NamespaceList = newNamespace
                lOptions.GlobalLightingSettings = False
                glo.gApp.FileMerge( lFilePath, False, lOptions)   
    
                
                #Copy data between namespaces
                glo.Scene.Evaluate() #Scene.Evaluates seem to be critical when using the namespace library functions
                RS.Utils.Namespace.CopyAnimationData( lNamespace, newNamespace )
                
                #Now we will go through each component and if the source component is driving something else in a constraint we 
                #will make sure it is swap in the target component appropriately
                componentMatchList = RS.Utils.Namespace.GetComponentMatchList( lNamespace, newNamespace, [ FBModelSkeleton, 
                                                                                                           FBModel, 
                                                                                                           FBModelMarker, 
                                                                                                           FBModelRoot, 
                                                                                                           FBModelNull ] )
                for componentMatch in componentMatchList:
                    for constraint in RS.Utils.Scene.Plug.GetDstPlugList( componentMatch.SourceComponent, FBConstraint ):
                        if RS.Utils.Scene.Constraint.IsConstraintType( constraint, [    RS.Utils.Scene.Constraint.PARENT_CHILD,
                                                                                        RS.Utils.Scene.Constraint.ROTATION, 
                                                                                        RS.Utils.Scene.Constraint.POSITION, 
                                                                                        RS.Utils.Scene.Constraint.SCALE ] ):
                            RS.Utils.Scene.Constraint.SwapSourceModel( constraint, 
                                                                       componentMatch.SourceComponent, 
                                                                       componentMatch.TargetComponent )  
    
                #Note: These Scene.Evaluates seem to be critical when deleting and renaming namespaces using these functions    
                #Delete old namespace and rename new namespace
                glo.Scene.Evaluate()
                glo.Scene.NamespaceDelete( lNamespace )
                glo.Scene.Evaluate()
                glo.Scene.NamespaceRename( newNamespace, lNamespace ) 
                glo.Scene.Evaluate()

                #If merge transaction was on then switch it back on                
                if mergeTransactionOn:
                    FBMergeTransactionBegin()                
    
            # Merge other reference types.    
            else:
                RS.Utils.Profiler.start( 'Merge', profileObj )
                lOptions.NamespaceList = lNamespace
                lOptions.GlobalLightingSettings = False
                
                glo.gApp.FileMerge( lFilePath, False, lOptions)
                    
                glo.gUlog.LogMessage( 'Merged file "{0}" for "{1}".'.format( lFilePath, lNamespace ), context = 'Update Reference - {0}'.format( lNamespace ) )
                                
                RS.Utils.Profiler.stop()
            
            # Fix FPS.
            RS.Utils.Profiler.start( 'Fix FPS and Time Range', profileObj )
            
            newFps = FBPlayerControl().GetTransportFpsValue()
            if newFps != oldFps:
                FBPlayerControl().SetTransportFps( oldFpsMode, oldFps )    
                    
            # Fixup time range.
            if keepTimeRange:
                FBPlayerControl().LoopStart = startTime
                FBPlayerControl().LoopStop = endTime
                
                glo.gUlog.LogMessage( 'Reset time range - Start:{0}  End:{1}'.format( startTime, endTime ) )
                
            RS.Utils.Profiler.stop()
                
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Compare And Rename 
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ############################################################################################################### 
            RS.Utils.Profiler.start( 'rs_CompareAndRename', profileObj )
            
            if lGetLatestTexture == None:
                lGetLatestTexture = False
            
            rs_CompareAndRename(lActorFaceArray, glo.gActorFaces, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lActorArray, glo.gActors, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lAudioClipArray, glo.gAudioClips, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lCameraArray, glo.gCameras, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lCharacterFaceArray, glo.gCharacterFaces, lNamespace, lAssetType, lGetLatestTexture, quiet) 
            
            rs_CompareAndRename(lCharacterPoseArray, glo.gCharacterPoses, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lConstraintSolverArray, glo.gConstraintSlovers, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lConstraintArray, glo.gConstraints, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lControlSetArray, glo.gControlSets, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lDeviceArray, glo.gDevices, lNamespace, lAssetType, lGetLatestTexture, quiet)
        
            rs_CompareAndRename(lFolderArray, glo.gFolders, lNamespace, lAssetType, lGetLatestTexture, quiet)
        
            rs_CompareAndRename(lGroupArray, glo.gGroups, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lHandleArray, glo.gHandles, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lLightArray, glo.gLights, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lMarkerSetArray, glo.gMarkerSets, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lMaterialArray, glo.gMaterials, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lNotesArray, glo.gNotes, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lObjectPoseArray, glo.gObjectPoses, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lPhysicalPropertyArray, glo.gPhysicalProperties, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lPoseArray, glo.gPoses, lNamespace, lAssetType, lGetLatestTexture, quiet)
                
            rs_CompareAndRename(lSetsArray, glo.gSets, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lShaderArray, glo.gShaders, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lTakeArray, glo.gTakes, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lTextureArray, glo.gTextures, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lUserObjectArray, glo.gUserObjects, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            rs_CompareAndRename(lVideoClipArray, glo.gVideoClips, lNamespace, lAssetType, lGetLatestTexture, quiet)
            
            RS.Utils.Profiler.stop()
            
        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Compare And Rename In Root Model
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ############################################################################################################### 
            
            RS.Utils.Profiler.start( 'Building lReferencedRootModelArray', profileObj )  
            
            lReferencedRootModelArray = []
        
            for iChild in glo.gRoot.Children:
                RS.Utils.Scene.GetChildren(iChild, lReferencedRootModelArray)
                
            RS.Utils.Profiler.stop()
            
            RS.Utils.Profiler.start( 'Comparing lRootModelArray and lReferencedRootModelArray', profileObj )
            
            refModelArray = []    
            
            for iReference in lReferencedRootModelArray:
                if iReference not in lRootModelArray:
                    refModelArray.append( iReference )
            
            RS.Utils.Profiler.stop()    
            
            lJawTranslation = None
                        
            RS.Utils.Profiler.start( 'Fixes', profileObj )
            
            for iReference in refModelArray:
                if iReference:            
                    lProp = iReference.PropertyList.Find('rs_Type')
                
                    if lProp:
                        if lProp.Data == "rs_Mesh":
                            lProp.Data = "REFERENCED:" + lAssetType + ":" + lNamespace + ":" + lProp.Data
                            pass
                
                    if iReference.Name == "RB_R_ThighRoll" or iReference.Name == "RB_L_ThighRoll":
                        if iReference.Parent.Name != "SKEL_Pelvis": 
                            if iReference.Translation.Data != FBVector3d(0,0,0):
                                iReference.Translation.Data = FBVector3d(0,0,0)
                                glo.gUlog.LogMessage( 'Set "{0}" translation to (0, 0, 0).'.format( iReference.LongName ) )
                
                    if iReference.Name == "facialRoot_C_OFF":
                        iReference.Scaling.Data = FBVector3d(40,40,40)
                        glo.gUlog.LogMessage( 'Set "{0}" scaling to (40, 40, 40).'.format( iReference.LongName ) )
                        
                    if iReference.Name == "FACIAL_jaw_Null":
                        iReference.Scaling.Data = FBVector3d(1,1,1)
                        iReference.Translation.Data = FBVector3d(0,0,0)
                        glo.gUlog.LogMessage( 'Set "{0}" translation to (0, 0, 0).'.format( iReference.LongName ) )
                        glo.gUlog.LogMessage( 'Set "{0}" scaling to (1, 1, 1).'.format( iReference.LongName ) )
                        
                        lJawTranslation = iReference.PropertyList.Find("RotationPivot").Data
                    
                    if iReference.Name == "FACIAL_jaw":
                        iReference.Scaling.Data = FBVector3d(1,1,1)
                        glo.gUlog.LogMessage( 'Set "{0}" scaling to (1, 1, 1).'.format( iReference.LongName ) )
                        
                        if lJawTranslation != None:
                            iReference.Translation.Data = lJawTranslation
                            glo.gUlog.LogMessage( 'Set "{0}" translation to {1}.'.format( iReference.LongName, lJawTranslation ) )
            
            if "!!scenes" in glo.gApp.FBXFileName:              
                if "ingame" in lFilePath or "Animals" in lFilePath:
                    for iConstraint in glo.gConstraints:
                        if iConstraint.LongName == lNamespace + ":mover_toybox_Control":
                            iConstraint.Active = True
                            glo.gUlog.LogMessage( 'Turned on constraint for "{0}".'.format( iConstraint.LongName ) )
                            
                            #glo.gScene.Evaluate()
                
                    lMover = FBFindModelByLabelName(lNamespace + ":mover")
                    if lMover:
                        lMover.RotationActive = True
                        lMover.RotationMinX = True
                        lMover.RotationMinY = True
                        lMover.RotationMaxX = True
                        lMover.RotationMaxY = True
                        glo.gUlog.LogMessage( 'Enabled rotation for "{0}".'.format( lMover.LongName ) )
                            
                for iDelete in lIgDeleteArray:
                    for iChild in glo.gRoot.Children:
                        if iDelete == iChild.Name:
                            try:
                                iChild.FBDelete()
                                #glo.gScene.Evaluate()
                                
                                glo.gUlog.LogMessage( 'Deleted "{0}".'.format( iChild.LongName ) )
                            except:    
                                pass
                                #glo.gScene.Evaluate()                                         
            
            RS.Utils.Profiler.stop()    
            
            ''' also part of old skeleton update script '''
            #if lInputCharacter == 1:       
                
                #lCharacter = None
                #lInputCharacter = None
                        
                #for iCharacter in glo.gCharacters:
                    
                    #if iCharacter.LongName == pPath[0] + ":" + pPath[0]:
                        
                        #lCharacter = iCharacter
                        
                    #if iCharacter.LongName == "oLd*" + pPath[0] + ":" + pPath[0]:
                        
                        #lInputCharacter = iCharacter
                    
                #if lCharacter and lInputCharacter:
                    
                    #lCharacter.InputCharacter = lInputCharacter
                    #lCharacter.Active = True
                    #lCharacter.InputType = FBCharacterInputType.kFBCharacterInputCharacter
                    
                #else:
                    
                    #if not quiet:
                        #FBMessageBox("Character Input", "Cannot Input OLD Character Into New Character", "OK")
            
            if "ingame" in lFilePath:
                for iGroup in glo.gGroups:
                    if iGroup.LongName == lNamespace + ":mover":
                        iGroup.Pickable = True
                        iGroup.Transformable = True
        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Clean Up
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

    if cancelAction == False: 
        glo.gUlog.LogMessage( 'Cleaning up arrays.' )
        
        del(lActorFaceArray, lActorArray, lAudioClipArray, lCameraArray, lCharacterFaceArray,
            lCharacterPoseArray, lConstraintSolverArray, lConstraintArray, lControlSetArray, lDeviceArray, lFolderArray,
            lGroupArray, lHandleArray, lLightArray, lMarkerSetArray, lMaterialArray, lNotesArray, lObjectPoseArray,
            lPhysicalPropertyArray, lPoseArray, lSetsArray, lShaderArray, lTakeArray, lTextureArray, lUserObjectArray,
            lVideoClipArray, lRootModelArray, lReferencedRootModelArray,)
        
        if pControl == "REF":
            rs_RefreshReferenceUI()
            
        
        if returnReferenceNull:
            return newRefResults
        
        else:
            return True   

        
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  Added by Kristine, for this function rs_RemoveOldBonesAndConstraints
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

lRollBoneExceptionList = ["MH_L_HairBack1",
                      "MH_L_HairBack2",
                      "MH_L_HairBack3",
                      "MH_L_HairFront1",
                      "MH_L_HairFront2",
                      "MH_L_HairFront3",
                      "MH_L_HairFront4",
                      "MH_R_HairBack1",                                            
                      "MH_R_HairBack2",
                      "MH_R_HairBack3",
                      "MH_R_HairFront1",
                      "MH_R_HairFront2",
                      "MH_R_HairFront3",                                                                                                              
                      "MH_R_HairFront4",
                      "MH_Top_Mid",
                      "MH_Top_R_1",
                      "MH_Top_L_1",
                      "MH_Top_L2",
                      "MH_Top_R2",
                      "MH_chain_L1",
                      "MH_chain_L2",
                      "MH_chain_L3",
                      "MH_chain_L4",                      
                      "MH_chain_L5",
                      "MH_chain_R1",
                      "MH_chain_R2",
                      "MH_chain_R3",
                      "MH_chain_R4",                      
                      "MH_chain_mid1",
                      "MH_chain_mid2"]  

def findChildren(Model):    
    for child in Model.Children:
        if len (child.Children) < 1:
            if child.Name.startswith("SM_") or child.Name.startswith("MH_") or child.Name.startswith("RB_")or child.Name.startswith("SPR_"): 
                if child.Name not in lRollBoneExceptionList:                
                    child.FBDelete()  
        else:
            findChildren(child)     


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: # This contains a list of bones that fall into these three categories:
#               1. RB bones (Rollbones)
#               2. MH bones (Mass Helper)
#               3. SM Bones (Secondary Motion)
#               
#               All of these bones are subject to change and carry no animation of their own these can all be 
#               removed when a character is stripped
##
##              Added by Kristine, for this bug 1056163
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
def rs_RemoveOldBonesAndConstraints(iRef):
    try:
        lProp = iRef.PropertyList.Find("rs_Asset_Type")
        if lProp:
            if lProp.Data == "Characters":            
    
                # Delete all the relation constraints associated with the old bones.
                for iConstraint in glo.gConstraints:
                    if iConstraint.LongName.startswith(iRef.Name + ":SM_") or iConstraint.LongName.startswith(iRef.Name + ":MH_") or iConstraint.LongName.startswith(iRef.Name + ":RB_") or iConstraint.LongName.startswith(iRef.Name + ":SPR_"):
                        if iConstraint.Name not in lRollBoneExceptionList:                    
                            lDeleteArray.append(iConstraint)         
    
                for iDeleteConstraint in lDeleteArray:
                    try:
                        iDeleteConstraint.FBDelete()                
                    except:
                        pass
    
                # Delete all the models/bones
                lSkelRoot = FBFindModelByLabelName(iRef.Name + ":SKEL_ROOT")            
                if lSkelRoot:
                    for i in range(8):
                        findChildren(lSkelRoot)
                        
                        
                # strip old facing direction
                keyFound = False
                facingDirection = FBFindModelByLabelName(iRef.Name + ":IK_FacingDirection")  
                if facingDirection:
                    trnsNodes = facingDirection.Rotation.GetAnimationNode().Nodes
                    for i in trnsNodes:
                        if i.KeyCount == 0:
                            keyFound = True
                            
                    if keyFound == True:
                        facingDirection.FBDelete()
                        print 'deleted facing direction for updating to new one.'

    except:
        FBMessageBox( "Cannot Continue", "Unable to remove the Old Bones and Constraints, the property has become unbound.", "OK", None, None )                        
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##  Added by Kristine, We were not looking for Group or entire scene strips, needed to fix this
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
def rs_StripSceneCore (pControl, lNamespace):

    stripSceneProfile = RS.Utils.Profiler.start( 'rs_StripScene - {0}'.format( lNamespace ) )
    
    lDeleteArray = []
        
    # Begin fix on offset object, which breaks the facial bones
    
    badOffsetModel = FBFindModelByLabelName( '{0}:ScaleOffset'.format( lNamespace ) )
    if badOffsetModel:
        badOffsetModel.FBDelete()                        
    # End fix on offset object, which breaks the facial bones

    # Begin fix jaw bone issue.
    glo.gUlog.LogMessage('Fixing jaw bone', context = 'Strip Scene')
    
    RS.Utils.Profiler.start( 'Fixing jaw bone', stripSceneProfile )    
    
    badJawBone = FBFindModelByLabelName( '{0}:FACIAL_jaw_Null'.format( lNamespace ) )
    
    nulls = FBComponentList()
    FBFindObjectsByName( '{0}:FACIAL_jaw_Null *'.format( lNamespace ), nulls, True, True )    
    
    if nulls:
        if len( nulls ) > 0 and badJawBone != None:
            try:
                badJawBone.FBDelete()
                #glo.gScene.Evaluate()
            except:
                pass
                #glo.gScene.Evaluate()              
            
            for null in nulls:
                if null.Parent:
                    if null.Parent.LongName == '{0}:FACIAL_facialRoot'.format( lNamespace ):
                        if len( null.Children ) == 0:
                            glo.gUlog.LogMessage( 'Deleted object ({0}) - Fixing jaw bone'.format( null.LongName ), context = 'Strip Scene' )
                            try:
                                null.FBDelete()
                                #glo.gScene.Evaluate()
                            except:
                                pass
                                #glo.gScene.Evaluate()
                        else:
                            glo.gUlogLogMessagee( 'Renamed object ({0}) to ({1}) - Fixing jaw bone'.format( null.LongName, '{0}:FACIAL_jaw_Null'.format( lNamespace ) ), context = 'Strip Scene' )
                            null.LongName = '{0}:FACIAL_jaw_Null'.format( lNamespace )
     
    nulls = FBComponentList()
    FBFindObjectsByName( '{0}:Null*'.format( lNamespace ), nulls, True, True )
    
    if nulls:
        for null in nulls:
            if null.Parent:
                if null.Parent.LongName == '{0}:FACIAL_facialRoot'.format( lNamespace ):
                    if len( null.Children ) == 0:
                        glo.gUlog.LogMessage( 'Deleted object ({0}) - Fixing jaw bone'.format( null.LongName ), context = 'Strip Scene' )
                        try:
                            null.FBDelete()
                            #glo.gScene.Evaluate()
                        except:
                            pass
                            #glo.gScene.Evaluate()                   
                    else:
                        glo.gUlog.LogMessage( 'Renamed object ({0}) to ({1}) - Fixing jaw bone'.format( null.LongName, '{0}:FACIAL_jaw_Null'.format( lNamespace ) ), context = 'Strip Scene' )
                        null.LongName = '{0}:FACIAL_jaw_Null'.format( lNamespace )
    
    RS.Utils.Profiler.stop()
    # End fix jaw bone issue.
    
    # Begin delete bad constraints.
    glo.gUlog.LogMessage('Deleting bad constraints', context = 'Strip Scene')
    
    badConstraints = [ '3lateralhead', 'facecontrols_off_null_parent',
                        'facialroot_c_off_parent', 'franklin 3lateral', 'franklin 3lateral 1',
                        '3lateralfacialrootparent', '3lateraljawparent', '3lateraltrevor 1', '3lateraltrevor',
                        'mover_toybox_control', 'rollbones', '3lateralcustomattributes', 'or - box trevor',
                        'or - box trevor 1' ]
    
    for i in FBSystem().Scene.Constraints:
        constraintName = str( i.Name ).lower()
        namespace = str( i.LongName ).split( ':' )[ 0 ]
        
        if namespace == lNamespace:
            if constraintName in badConstraints:
                glo.gUlog.LogMessage( 'Deleted constraint ({0}) - Deleting old constraint'.format( i.LongName ), context = 'Strip Scene')                
                try:                    
                    RS.Utils.Profiler.start( 'Deleting constraint: {0}'.format( i.LongName ), stripSceneProfile )
                    i.FBDelete()                    
                    RS.Utils.Profiler.stop()
                except:    
                    pass

                #glo.gScene.Evaluate()                    
    
    # End delete bad constraints.
    glo.gUlog.LogMessage('Deleting properties', context = 'Strip Scene')
    
    for iComponent in glo.gComponents:
        lProp = iComponent.PropertyList.Find("rs_Type")
        if lProp:
            if (pControl.Name + ":") in lProp.Data:
                lDeleteArray.append(iComponent)   
                
                #dummy property 'UDP3DSMAX' on an individual strip  url:bugstar:918686
                lControlDummy = None
                lControlDummy = FBFindModelByLabelName(pControl.Name + ":Dummy01")     
                if lControlDummy != None:
                    lProperty = None
                    lProperty = lControlDummy.PropertyList.Find("UDP3DSMAX")
                    if lProperty != None:
                        glo.gUlog.LogMessage( 'Removing property ({0}) on ({1})'.format( 'UDP3DSMAX', lControlDummy.LongName ) )
                        lControlDummy.PropertyRemove(lProperty)
                        
                        
        #dummy property 'UDP3DSMAX' on strip all url:bugstar:918686
        if pControl.Name == 'REFERENCED' or pControl.Name == 'Props':
            if iComponent.Name.startswith('Dummy'):    
                lProperty = None
                lProperty = iComponent.PropertyList.Find("UDP3DSMAX")
                if lProperty != None:
                    iComponent.PropertyRemove(lProperty)
                    glo.gUlog.LogMessage( 'Removing property ({0}) on ({1})'.format( 'UDP3DSMAX', iComponent.LongName ) )

        if iComponent.LongName != None:
            if iComponent.LongName.startswith(pControl.Name + ":oLd*"):
                if not iComponent in lDeleteArray:
                    lDeleteArray.append(iComponent)  
   
    glo.gUlog.LogMessage('Deleting', context = 'Strip Scene')    
    
    deleteClassTypes = ( "FBModel", "FBActor", "FBActorFace", "FBAudioClip",
                    "FBCamera", "FBCharacter", "FBCharacterPose", "FBCharacterFace",
                    "FBControlSet", "FBHandle", "FBFolder", "FBConstraintRelation",
                    "FBConstraint", "FBSet", "FBLight", "FBGroup", "FBMaterial", 
                    "FBNote", "FBPose", "FBShader", "FBShaderLighted", "FBTake", 
                    "FBTexture", "FBVideoClipImage" )
    
    deleteArrayProfile = RS.Utils.Profiler.start( 'Delete Array', stripSceneProfile )
    for iDelete in lDeleteArray:
        if iDelete.ClassName() in deleteClassTypes:
            glo.gUlog.LogMessage( 'Deleted ({0})'.format( iDelete.LongName ) )       
            
            try:
                RS.Utils.Profiler.start( 'FBDelete(): {0}'.format( iDelete.LongName ), deleteArrayProfile )
                iDelete.FBDelete()
                RS.Utils.Profiler.stop()                
            except:    
                pass
            
            #glo.gScene.Evaluate() 
            
            
    # Delete head skeleton (and on-face GUI to resolve other issues)
    
    lDeleteArrayFace = []
    lFacialRoots = ["FACIAL_facialRoot"] #This array contains the data we'll want externalized either via Globals or XML.
    
    for each in lFacialRoots:
        lDeleteStuff = []
        lSearchObj = FBFindModelByLabelName("{0}:{1}".format( lNamespace, each ) )
        
        if lSearchObj:
            RS.Utils.Scene.GetChildren( lSearchObj, lDeleteStuff )
        
        for item in lDeleteStuff:
            lDeleteArrayFace.append( item )
            
    deleteArrayFaceProfile = RS.Utils.Profiler.start( 'Delete Array Face', stripSceneProfile )    
    for each in lDeleteArrayFace:
        glo.gUlog.LogMessage( 'Deleted ({0})'.format( each.LongName ) )
        try:
            RS.Utils.Profiler.start( 'FBDelete(): {0}'.format( each.LongName ), deleteArrayFaceProfile )
            each.FBDelete()
            RS.Utils.Profiler.stop() 
        except:
            pass
    
    
    RS.Utils.Profiler.stop()
    
    del gNamespaceArray[:] 
    for iComponent in glo.gComponents:
        if ":" in iComponent.LongName:
            lNamespace = iComponent.LongName.partition(":")
            gNamespaceArray.append(lNamespace[0])
  
    del(lDeleteArray, lDeleteArrayFace, iComponent, lProp)
    RS.Utils.Profiler.stop()
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Strips The Scene Of A Specified Rs_Type
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_StripScene( pControl, pEvent, quiet = True ):
        
    global gCharactersArray
    global gPropsArray
    global gSetsArray
    global gVehiclesArray
    global gRayFireArray
    global gNamespaceArray
    global gProjRoot
    
    lArray = []
    lUpdateType = 0 
    lAllArray = [gCharactersArray, gPropsArray, gSetsArray, gVehiclesArray, gRayFireArray, g3LateralArray]
    lAssetArray = ["Characters", "Props", "Sets", "Vehicles", "RayFire", "3Lateral"]
    lAssetType = ""  
    
    #Deactivate ULog if we are running in quiet mode
    previousULogState = glo.gUlog.Active
    glo.gUlog.Active = not quiet
    
    # Legend
    # lStripType = 1 is Strip Group
    # lStripType = 2 is Strip Entire Scene
    # lStripType = 3 is Strip Individual Asset
    
    if pControl.Name == "Characters":
        lArray = gCharactersArray
        lStripType = 1
        lAssetType = "Characters"
    elif pControl.Name == "Sets":
        lArray = gSetsArray
        lStripType = 1
        lAssetType = "Sets"
    elif pControl.Name == "Props":
        lArray = gPropsArray
        lStripType = 1
        lAssetType = "Props"
    elif pControl.Name == "Vehicles":
        lArray = gVehiclesArray
        lStripType = 1
        lAssetType = "Vehicles"
    elif pControl.Name == "Rayfire":
        lArray = gRayFireArray
        lStripType = 1
        lAssetType = "RayFire"
    elif pControl.Name == "3Lateral":
        lArray = g3LateralArray
        lStripType = 1
        lAssetType = "3Lateral"
    elif pControl.Name == "REFERENCED":
        lStripType = 2
    else:
        lStripType = 3  

    #This will delay refreshes during strip    
    FBMergeTransactionBegin()
    
    if lStripType == 1: 
        for iItem in lArray:
            if iItem:              
                if not iItem.Name.startswith("oLd*"): 
                    lFile = iItem.PropertyList.Find("Reference Path").Data
                    lFile = gProjRoot + lFile[7:len(lFile)]
                    lNamespace = iItem.PropertyList.Find("Namespace").Data
                        
                    rs_StripSceneCore (pControl, lNamespace)

    elif lStripType == 2:
        for i in range(len(lAllArray)):
            for iItem in lAllArray[i]:
                if iItem:
                    try:
                        if not iItem.Name.startswith("oLd*"):                        
                            lFile = iItem.PropertyList.Find("Reference Path").Data
                            lFile = gProjRoot + lFile[7:len(lFile)]                
                            lNamespace = iItem.PropertyList.Find("Namespace").Data
                            rs_StripSceneCore (pControl, lNamespace)
                    except:
                        pass
                        
    elif lStripType == 3:    
        lNamespace = pControl.Name  
        print 'NAMESPACE: ', pControl.Name
        
        # check first if this is for an individual strip/delete, or for a full scene strip/update
        # ALL = Full scene strip/update
        if lNamespace != 'ALL':
            rs_StripSceneCore (pControl, lNamespace)
        
        else:
            print 'entire scene strip merge'
            
            refNullList = []
            lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
            if lReferenceNull:        
                RS.Utils.Scene.GetChildren(lReferenceNull, refNullList)
            
            if refNullList:
                for i in refNullList:
                    assetType = i.PropertyList.Find( "rs_Asset_Type" )
                    if assetType:
                        if assetType.Data == 'AmbientFace' or assetType.Data == '3Lateral':
                            None
                        else:
                            print 'STRIPPING:' + assetType.Data, i.Name
                            pControl = FBButton()
                            pControl.Name = assetType.Data
                            rs_StripSceneCore (pControl, i.Name) 

        
    ######## Delete Old Bones - 1056163 #######################################################################
    # Find the Namespace Root, inside it only does this on Characters
    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
    
    # From the root, get all the children which are the registered reference namespaces in the scene
    if lReferenceNull:        
        RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList)
        #lRootModelArray.extend(eva.gChildList)
        #del eva.gChildList[:]        
   
    if len(lReferenceNullList) >=1:
        for iRef in lReferenceNullList:
            if pControl.Name == "REFERENCED" or pControl.Name == "Characters" or pControl.Name == "ALL":
                rs_RemoveOldBonesAndConstraints(iRef)  
            #elif 
            try:
                #print iRef.Name
                if pControl.Name == iRef.Name:
                    rs_RemoveOldBonesAndConstraints(iRef)           
            except:
                pass
    #########################################################################################################                
    
    FBMergeTransactionEnd()

    if not quiet:
        glo.gUlog.Show( modal = False )
        
    glo.gUlog.Active = previousULogState


    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Strips The Scene Of A Specified Rs_Type
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_DeleteReference( pControl, pEvent ):
    rs_StripScene( pControl, pEvent )
    
    ###########################################################################################################
    ## So this is a bit of a hack, as when the control rig is created we never it give it the namepace of the character it is associate with.
    ## Potential solutions would be to delete based on children and we could get it then, or maybe setup a call back to assign the correct
    ## Namespace for it during the session with the scene, and then the delete would work, as if you assign the correct namespace to the control rig
    ## and then delete using the Reference System you will see everything is deleted beautifully. Ah well, here is my solution :)
    ###########################################################################################################
    for iChar in FBSystem().Scene.Characters:
        if iChar.LongName.partition(":")[0] == pControl.Name:
            for i in range(iChar.GetSrcCount()):                 
                if iChar.GetSrc(i).ClassName() == "FBControlSet":
                    print 'found controlset'
                    #iChar.GetSrc(i).FBDelete()
                    
    for iComponent in glo.gComponents:
        if "RS_Null:" + pControl.Name == iComponent.LongName:
            iComponent.FBDelete()
    
    glo.gScene.NamespaceDelete( pControl.Name )
            
    ##########################################################################################################
    # Need to delete ToyBox Setup for the object if it was setup:
    ##########################################################################################################
    lOverRideNull = FBFindModelByLabelName("ToyBoxOverRideNull")
    if lOverRideNull:
        lOverRideNullProp = lOverRideNull.PropertyList.Find((pControl.Name + '_OverRide_Bool'))
        if lOverRideNullProp != None:
            lOverRideNull.PropertyRemove(lOverRideNullProp)

    for lFolder in glo.gFolders:    
        if lFolder.Name == "ToyBoxFolder":
            if len(lFolder.Items) == 0:
                lFolder.FBDelete()
                lOverRideNull.FBDelete()
    ##########################################################################################################
    #Delete Empty Folders
    ##########################################################################################################
    for folder in FBSystem().Scene.Folders:
        for item in folder.Items:
            if isinstance(item,FBCharacter):
                folder.Items.remove(item)    
    
    lFolderList = []
    for lFolder in glo.gFolders:    
        if len(lFolder.Items) == 0:
            lFolderList.append(lFolder)
            
    map( FBComponent.FBDelete, lFolderList )  

    ##########################################################################################################
    # Delete REFERENCE:Scene if it has no children
    ##########################################################################################################       
    lReferenceNullListDelete = []
    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
    
    # From the root, get all the children which are the registered reference namespaces in the scene
    if lReferenceNull:
        RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullListDelete, "", False)
        #lReferenceNullListDelete.extend(eva.gChildList)
        #del eva.gChildList[:]  
        
        if len(lReferenceNullListDelete) == 0:
            lReferenceNull.FBDelete()    

    #rs_RefreshReferenceUI()
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Updates Groups Of References
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   
   
def rs_UpdateReference( pControl, pEvent, quiet = True ):
    global gCharactersArray
    global gPropsArray
    global gSetsArray
    global gVehiclesArray
    global gRayFireArray
    global gNamespaceArray
    global gProjRoot
    
    #Deactivate ULog if we are running in quiet mode
    previousULogState = glo.gUlog.Active
    glo.gUlog.Active = not quiet
    
    lArray = []
    lUpdateType = 0 
    lAllArray = [gCharactersArray, gPropsArray, gSetsArray, gVehiclesArray, gRayFireArray, g3LateralArray]
    lAssetArray = ["Characters", "Props", "Sets", "Vehicles", "RayFire", "3Lateral"]
    lAssetType = ""

    #This contains whether the person wants to get the latest textures and sync to the latest
    lCheckMarkInfo = pControl.split(";")  
    if lCheckMarkInfo[0] == "CHARACTERS":
        lArray = gCharactersArray
        lUpdateType = 1
        lAssetType = "Characters"
    elif lCheckMarkInfo[0] == "SETS":
        lArray = gSetsArray
        lUpdateType = 1
        lAssetType = "Sets"
    elif lCheckMarkInfo[0] == "PROPS":
        lArray = gPropsArray
        lUpdateType = 1
        lAssetType = "Props"
    elif lCheckMarkInfo[0] == "VEHICLES":
        lArray = gVehiclesArray
        lUpdateType = 1
        lAssetType = "Vehicles"
    elif lCheckMarkInfo[0] == "RAYFIRE":
        lArray = gRayFireArray
        lUpdateType = 1
        lAssetType = "RayFire"
    elif lCheckMarkInfo[0] == "3LATERAL":
        lArray = g3LateralArray
        lUpdateType = 1
        lAssetType = "3Lateral"
    elif lCheckMarkInfo[0] == "ALL":
        lUpdateType = 2
        lAssetType = "ALL"
        
    print lCheckMarkInfo
    #lImportOption = lCheckMarkInfo[4]
    #lTakeName = lCheckMarkInfo[5]
    #lCreateMissingTakes = lCheckMarkInfo[6]
    
    #Strip first
    lButton = FBButton()
    lButton.Name = lAssetType
    rs_StripScene( lButton, pEvent, quiet )
    
    lFile = ""
    global lOrgControlRigDic
    lOrgControlRigDic = {}
    lGetLatestTexture = lCheckMarkInfo[1]
    
    #This will delay refreshes during merging
    FBMergeTransactionBegin()        
    
    if lUpdateType == 1: 
        for iItem in lArray:
            if iItem:
                try:
                    if not iItem.Name.startswith("oLd*"):                    
                        lFile = iItem.PropertyList.Find("Reference Path").Data
                        lFile = gProjRoot + lFile[7:len(lFile)]
                        lNamespace = iItem.PropertyList.Find("Namespace").Data
                                                        
                        RS.Perforce.Sync( sc.ToSafeP4Path( lFile ))
                        lP4version = rs_P4_Revision( lFile, "haveRev" )
                        
                        # Check to make sure None doesn't get written into the file - 1075797
                        if lP4version != None:
                            iItem.PropertyList.Find("P4_Version").Data = lP4version
                        
                        lImportOption = "0"
                        lTakeName = ""
                        lCreateMissingTakes = False
                        
                        lImportOptionVar = iItem.PropertyList.Find("Import_Option")
                        if lImportOptionVar != None:
                            lImportOption = lImportOptionVar.Data
                            
                        lTakeNameVar = iItem.PropertyList.Find("Take_Name")
                        if lTakeNameVar != None:
                            lTakeName = lTakeNameVar.Data
                            
                        lCreateMissingTakesVar = iItem.PropertyList.Find("Create_Missing_Takes")
                        if lCreateMissingTakesVar != None:
                            lCreateMissingTakes = lCreateMissingTakesVar.Data
                            
                        lButtonInfo = [lNamespace, lAssetType, lFile, lImportOption, lTakeName, lCreateMissingTakes, lGetLatestTexture ]
                        lResult = rs_CreateReference("LOAD", lButtonInfo, 1, quiet )
                        if lResult == None:
                            break            
                except:
                    pass
                    
        # Need to track what the control rig setting were before we turn them off, so we can turn the appropriate ones back on at the end - 1222078        
        if lAssetType == "Characters":
            for iCharacter in glo.gCharacters:
                lOrgControlRigDic[iCharacter.Name] = iCharacter.Active
                iCharacter.Active = False    
        
    elif lUpdateType == 2:
        for i in range(len(lAllArray)):
            for iItem in lAllArray[i]:
                if iItem:
                    try:
                        if not iItem.Name.startswith("oLd*"):                        
                            lFile = iItem.PropertyList.Find("Reference Path").Data
                            lFile = gProjRoot + lFile[7:len(lFile)]                
                            lNamespace = iItem.PropertyList.Find("Namespace").Data

                            RS.Perforce.Sync( sc.ToSafeP4Path( lFile ))
                            lP4version = rs_P4_Revision( lFile, "haveRev" )          
                            # Check to make sure None doesn't get written into the file - 1075797
                            if lP4version != None:
                                iItem.PropertyList.Find("P4_Version").Data = lP4version
                            
                            lImportOption = "0"
                            lTakeName = ""
                            lCreateMissingTakes = False
                            
                            lImportOptionVar = iItem.PropertyList.Find("Import_Option")
                            if lImportOptionVar != None:
                                lImportOption = lImportOptionVar.Data
                                
                            lTakeNameVar = iItem.PropertyList.Find("Take_Name")
                            if lTakeNameVar != None:
                                lTakeName = lTakeNameVar.Data
                                
                            lCreateMissingTakesVar = iItem.PropertyList.Find("Create_Missing_Takes")
                            if lCreateMissingTakesVar != None:
                                lCreateMissingTakes = lCreateMissingTakesVar.Data
                                
                            lButtonInfo = [lNamespace, lAssetArray[i], lFile, lImportOption, lTakeName, lCreateMissingTakes, lGetLatestTexture ]
                            lResult = rs_CreateReference("LOAD", lButtonInfo, 1, quiet)
                            if lResult == None:
                                break                
                    except:
                        pass        
            if lAssetArray[i] == "Characters":
                for iCharacter in glo.gCharacters:
                    lOrgControlRigDic[iCharacter.Name] = iCharacter.Active
                    iCharacter.Active = False
                    
    FBMergeTransactionEnd()      

    del gCharactersArray[:] 
    del gPropsArray[:] 
    del gSetsArray[:] 
    del gVehiclesArray[:] 
    del gRayFireArray[:] 
    del gNamespaceArray[:] 
        
    for i in range(len(glo.gCameras)):
        if glo.gCameras[i].Name == "Producer Perspective":
            glo.gCameras[i].FarPlaneDistance = 400000
            
    lPlayerControl =  FBPlayerControl()
    lPlayerControl.SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames
    
    del gNamespaceArray[:] 
    for iComponent in glo.gComponents:
        if ":" in iComponent.LongName:
            lNamespace = iComponent.LongName.partition(":")
            gNamespaceArray.append(lNamespace[0])      

    lIgDeleteArray = ["ShadowLight",
                      "AdditonalLight1",
                      "AdditonalLight2",
                      "AdditonalLight3",
                      "ShadowPlane",
                      "ShadowPlaneTexture",
                      "Live Shadow"]
            
    if "!!scenes" in glo.gApp.FBXFileName:              
        if "ingame" in lFile or "Animals" in lFile:
            for iConstraint in glo.gConstraints:
                if iConstraint.LongName == lNamespace[0] + ":mover_toybox_Control":
                    iConstraint.Active = True
                    glo.gScene.Evaluate()
                    
            lMover = FBFindModelByLabelName(lNamespace[0] + ":mover")
            if lMover:
                lMover.RotationActive = TrueSo 
                lMover.RotationMinX = True
                lMover.RotationMinY = True
                lMover.RotationMaxX = True
                lMover.RotationMaxY = True
                    
        for iDelete in lIgDeleteArray:
            for iChild in glo.gRoot.Children:
                if iDelete == iChild.Name: 
                    try:
                        iChild.FBDelete()
                        glo.gScene.Evaluate()
                    except:    
                        glo.gScene.Evaluate()
    
    rsv.rs_ResetSetVectors()
    rs_CheckForToyBoxPropOnMover() 
    
    # We should be only turning off the Character constraint for the one we are updating, not turning them all off - 1222078        
    for iCharacter in glo.gCharacters:
        for key, value in lOrgControlRigDic.iteritems():
            if iCharacter.Name == key:  
                iCharacter.Active = value                              
                glo.gUlog.LogMessage( 'Marking the control rig on this character "{0}" as it was when it came into the merge.'.format( iCharacter.LongName ) )        

    rs_RefreshReferenceUI()

    if not quiet:
        glo.gUlog.Show( modal = False )
    glo.gUlog.Active = previousULogState
    

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: This Definition Updates Individual References
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################       
     
def rs_UpdateIndividualReference( pControl, pEvent, quiet = True ):    
    lButtonInfo = pControl.split(";")
    
    #Deactivate ULog if we are running in quiet mode
    previousULogState = glo.gUlog.Active
    glo.gUlog.Active = not quiet
    
    # Adding in the strip with the Merge for individual
    lButton = FBButton()
    lButton.Name = lButtonInfo[0]
    rs_StripScene( lButton, pEvent, quiet )  
    
    lRsNull = FBFindModelByLabelName("RS_Null:" + lButtonInfo[0])
    if lRsNull:
        profileUpdateRef = RS.Utils.Profiler.start( 'rs_UpdateIndividualReference - {0}'.format( lButtonInfo[ 0 ] ) )
        
        glo.gUlog.LogMessage( 'Starting...', context = 'Update Reference - {0}'.format( lButtonInfo[0] ) )      
        
        RS.Utils.Profiler.start( 'Sync file', profileUpdateRef )            
        RS.Perforce.Sync( lButtonInfo[2] )
        RS.Utils.Profiler.stop()
        
        glo.gUlog.LogMessage( 'Synced file: {0}'.format( lButtonInfo[2] ) )

        lP4version = rs_P4_Revision( lButtonInfo[2], "haveRev" )     
        
        # Check to make sure None doesn't get written into the custom property - 1075797
        if lP4version != None:
            lRsNull.PropertyList.Find("P4_Version").Data = lP4version
            glo.gUlog.LogMessage( 'Updated property "P4_Version" to {0}.'.format( lP4version ) )            
            
        profileCreateRef = RS.Utils.Profiler.start( 'rs_CreateReference', profileUpdateRef )            
            
        lResult = rs_CreateReference( "LOAD", lButtonInfo, 1, quiet, profileObj = profileCreateRef )
        
        RS.Utils.Profiler.stop()
        
        if lResult == None:
            return None
        
        if lButtonInfo[1] ==  "Characters":
            for iCharacter in glo.gCharacters:
                # We should be only turning off the Character constraint for the one we are updating, not turning them all off - 1222078
                if iCharacter.Name == lButtonInfo[0]:
                    lOrgControlRig = iCharacter.Active
                    iCharacter.Active = False
                    glo.gUlog.LogMessage( 'Marked character "{0}" as no longer being active.'.format( iCharacter.LongName ) )
        
        del gCharactersArray[:] 
        del gPropsArray[:] 
        del gSetsArray[:] 
        del gVehiclesArray[:] 
        del gRayFireArray[:] 
        del gNamespaceArray[:] 
        
        for i in range(len(glo.gCameras)):
            if glo.gCameras[i].Name == "Producer Perspective":
                glo.gCameras[i].FarPlaneDistance = 400000
                glo.gUlog.LogMessage( 'Set "Producer Perspective" camera far plane distance to 400000.' )
                
        lPlayerControl =  FBPlayerControl()
        lPlayerControl.SnapMode = FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames
        glo.gUlog.LogMessage( 'Enabled player control snap mode.' )
        
        RS.Utils.Profiler.start( 'rs_ResetSetVectors', profileUpdateRef )
        rsv.rs_ResetSetVectors()
        RS.Utils.Profiler.stop()
        
        RS.Utils.Profiler.start( 'rs_CheckForToyBoxPropOnMover', profileUpdateRef )
        rs_CheckForToyBoxPropOnMover()
        RS.Utils.Profiler.stop()

        # We should be only turning off the Character constraint for the one we are updating, not turning them all off - 1222078        
        RS.Utils.Profiler.start( 'Re-activating the Control Rig on the character if it was there to start', profileUpdateRef )
        if lButtonInfo[1] ==  "Characters":
            for iCharacter in glo.gCharacters:
                if iCharacter.Name == lButtonInfo[0]:
                    if lOrgControlRig:
                        iCharacter.Active = True
                    glo.gUlog.LogMessage( 'Marking the control rig on this character "{0}" as it was when it came into the merge.'.format( iCharacter.LongName ) )        
        RS.Utils.Profiler.stop()
        rs_RefreshReferenceUI()
        
        glo.gUlog.LogMessage( 'Finished updating individual reference.' )
        
        RS.Utils.Profiler.stop()
        
        if not quiet:
            glo.gUlog.Show( modal = False )      
        
    else:
        FBMessageBox( "Rockstar Referencing System", "Could not find the object named ({0})!  Cannot update the individual reference.".format( lButtonInfo[ 0 ] ), "OK" )
        
    glo.gUlog.Active = previousULogState

    #def DeleteChildren(model, top_node=True):


def Delete(*models):
    """
    Deletes the provided FBModels
    :param models: list
        a list of FBModel instances and/or names of object in the current motion builder scene
    :return: None
    """
    for each_model in models:
        if isinstance(each_model, basestring):
            each_model = FBFindModelByLabelName(each_model)
        if each_model in FBSystem().Scene.Components:
            DeleteChildren(each_model, True)

def DeleteChildren(model,is_child=False):
    """
    Deletes all the children of the FBModel instance provided

    :param model: FBModel Instance
        the object from motion builder who's childeren you want to delete
    :param is_child: Boolean
        flag to check if the current node is a child node. If true then the node will be deleted
    :return: None
    """
    # Always destroy from the last children to the first
    while len( model.Children ) > 0:
        DeleteChildren( model.Children[-1] )
    if is_child:
        model.FBDelete()
