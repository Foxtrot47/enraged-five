###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_ValidateCharacterSetup.py
## Written And Maintained By: Kathryn Bodey & Kristine Middlemiss
## Contributors:
## Description: Function to check the characters are setup correctly for ingame:
##              - ROM test, this would load an animation ROM onto the character to test for skinning issues 
##                (ideally this would be done on all geo vars). The ROM should be removed afterwards. This could be 
##                 loaded using motion file import.
##              - Floor contacts check - Ensure these are turned off
##              - Mover Group options - ensure 'show' is off and 'pick' and 'Trs' are on.
##              - Missing texture check
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *

import RS.Globals as glo
import RS.Core.Character.AnimateControlRig as ctrl
import RS.Core.Reference.Manager

import os

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: 1.    ROM test, this would load an animation ROM onto the character to test for skinning issues 
##                  (ideally this would be done on all geo vars). The ROM should be removed afterwards. This 
##                  could be loaded using motion file import (INGAME ONLY)
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
# Runs a series of aniamations on every joint in the CTRL RIG.
#
# NOTE: Save before Running this, as you dont want to check the file in with the rom still attached!"

def rs_LaunchROM(lChar, control, event):
    lFound = False
    # Set the character we want to run the ROM test to as the Current Character.
    if lChar:
        for lCharacter in glo.gCharacters:
            if lCharacter.Name == lChar.Name:
                lFound = True
                FBApplication().CurrentCharacter = lCharacter       
                ctrl.rs_RunROMTest(control, event)

    if lFound == True:
        control.ListData.Items.append("        ROM Check ................................................ ADDED (scrub timeline to verify the characters movement)")  
    else:
        control.ListData.Items.append("        ROM Check - Matching Character in Scene ............. NOT FOUND")          
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: 2.    Floor contacts check - Ensure these are turned off  (INGAME ANIM ONLY CHECK)
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FloorContactCheck(lChar, control, event):
    
    lFound = False
    for iCharacter in glo.gCharacters:
        lNamespace = iCharacter.LongName.split(":")
        if lNamespace[0] == lChar.Name:                
            lFound = True
            lFeet = iCharacter.PropertyList.Find("Feet Floor Contact")
            if lFeet:
                lFeetProp = lFeet.Data    
                # Should be off
                if lFeetProp:
                    control.ListData.Items.append("        Floor Contact - Feet ...................................... INCORRECT")      
                else:
                    control.ListData.Items.append("        Floor Contact - Feet ...................................... OK")  
                
            lToes = iCharacter.PropertyList.Find("Toes Floor Contact")
            if lToes:
                lToeProp = lToes.Data
                # Should be off
                if lToeProp:
                    control.ListData.Items.append("        Floor Contact - Toes ...................................... INCORRECT")      
                else:
                    control.ListData.Items.append("        Floor Contact - Toes ...................................... OK")          
    
            lHands = iCharacter.PropertyList.Find("Hands Floor Contact")
            if lHands:
                lHandProp = lHands.Data
                # Should be off
                if lHandProp:
                    control.ListData.Items.append("        Floor Contact - Hands .................................... INCORRECT")      
                else:
                    control.ListData.Items.append("        Floor Contact - Hands .................................... OK")  
            
            lFingers = iCharacter.PropertyList.Find("Fingers Floor Contact")
            if lFingers:
                lFingerProp = lFingers.Data
                # Should be off
                if lFingerProp:
                    control.ListData.Items.append("        Floor Contact - Fingers ................................... INCORRECT")      
                else:
                    control.ListData.Items.append("        Floor Contact - Fingers ................................... OK")          

    if lFound == False:
        control.ListData.Items.append("        Floor Contact - Matching Character in Scene .......... NOT FOUND")      
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: 3.    Mover Group options - ensure 'show' is off and 'pick' and 'Trs' are on.  
##                  (INGAME ANIM ONLY CHECK)
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_MoverGroupCheck(lChar, control, event):
    lFound = False

    for iGroup in glo.gGroups:
        
        if iGroup.LongName == lChar.Name + ':mover':
            lFound = True
            lShow = iGroup.Show
            # Should be off
            if lShow:
                control.ListData.Items.append("        Mover Group - Show ...................................... INCORRECT")      
            else:
                control.ListData.Items.append("        Mover Group - Show ...................................... OK")  

            # Should be on
            lPick = iGroup.Pickable
            if lPick:
                control.ListData.Items.append("        Mover Group - Pick ........................................ OK")    
            else:
                control.ListData.Items.append("        Mover Group - Pick ........................................ INCORRECT")    

            # Should be on            
            lTransform = iGroup.Transformable
            if lTransform:
                control.ListData.Items.append("        Mover Group - Transformable ............................ OK")    
            else:
                control.ListData.Items.append("        Mover Group - Transformable ............................ INCORRECT")                
          
    if lFound == False:
        control.ListData.Items.append("        Mover Group ................................................ NOT FOUND")  
            
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: 4.    Missing texture check (INGAME AND CUTSCENE)
##              CHECK ON THE PATH THE TEXTURE IS EXPECTED IN VS WHETHER A FILE ACTUALLY EXISTS ON DISK.
##              go through textures to get paths (lPath)
##    
##              os.isfile(lPath) - returns bool. True - exists. False - not on HD.
##    
##              re-ref to get textures if missing. if still missing add bug
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_TextureCheck(lChar, control, event):

    # Setup variables
    lTexturesToSync = []
                    
    for iVideoClip in glo.gTextures:
        lProp = iVideoClip.PropertyList.Find('rs_Type')        
        if lProp:
            if lChar.Name in lProp.Data:                           
                if iVideoClip.Video != None: 
                    try:
                        # If that Path has a forward slash, fix it.
                        lPathParts = iVideoClip.Video.Filename.split("/")
                        lPath = lPathParts[0] + "\\" + lPathParts[1]
                         
                    except:
                        # if everything is good leave it alone.
                        lPath = iVideoClip.Video.Filename   
                    
                     
                    # Appends texutures it does not find.                    
                    if not os.path.isfile( lPath ):
                        lTexturesToSync.append( lPath )
    
    # Need remove duplicates
    lRemoveDuplicates = list(set(lTexturesToSync))
                    
    for lTex in lRemoveDuplicates:
        control.ListData.Items.append("        Texture - " + lTex + " ............... NOT FOUND")
        
        
    '''lVid = lTex.Video
    if lVid:
        lFilePath = lVid.Filename
        iVideoClip.Video.Filename = None
        iVideoClip.Video.Filename = lFilePath'''                          

    if len(lTexturesToSync) < 1:
        control.ListData.Items.append("        Texture Check .............................................. OK")     
         
def rs_ValidateCharacterSetup(control, event):
    # This allows you to keep re-running the script in the same session without the list just appending on to the current list
    howMany= len(control.ListData.Items)
    while howMany>0:
        del control.ListData.Items[howMany-1]
        howMany -=1

    # The characters without control rigs are still valid, as it still contains correct characterization information 
    # and can generate a control rig if plotting from skeleton to control, if there is already a character with a control 
    # that is driving a skeleton, then I would imaagine this would be irrelevant
    
    lCSCharList = []
    lIGCharList = []
    lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull() 
    
    # This chunk of code is to gather up all the CS and IG character objects in the scene
    if lReferenceNull:
        for iChild in lReferenceNull.Children:
            RSNull = FBFindModelByLabelName(iChild.Name)
            if  RSNull:       
                rs_Asset_Type = RSNull.PropertyList.Find("rs_Asset_Type")
                if rs_Asset_Type:
                    if rs_Asset_Type.Data == "Characters":
                        # Okay, Only Dealing with Characters Now
                        lReference_Path = RSNull.PropertyList.Find("Reference Path")
                        if lReference_Path:
                            lReference_PathData = lReference_Path.Data
                            if "cutscene" in  lReference_PathData:
                                lCSCharList.append(RSNull)                                
                            elif "ingame" in  lReference_PathData:
                                lIGCharList.append(RSNull)
                                
    # For Cut Scene Characters
    control.ListData.Items.append("Checking CutScene Characters")    
    control.ListData.Items.append(" ")     
    if len(lCSCharList) > 0:
        for lCSChar in lCSCharList:
            control.ListData.Items.append("    " + lCSChar.Name)    
                
            # Start Cut Scene Character Checks
            rs_LaunchROM(lCSChar, control, event) # Check 1
            control.ListData.Items.append(" ")  
            rs_FloorContactCheck(lCSChar, control, event) # Check 2
            control.ListData.Items.append(" ")
            rs_MoverGroupCheck(lCSChar, control, event) # Check 3
            control.ListData.Items.append(" ")
            rs_TextureCheck(lCSChar, control, event) # Check 4
            control.ListData.Items.append(" ")            

    else:
        control.ListData.Items.append("    None")
        control.ListData.Items.append(" ")        

    # Spacer
    
    control.ListData.Items.append("---------------------------------------------------------------------------------------------------")        
    control.ListData.Items.append(" ") 
    
    # For In Game Characters
    control.ListData.Items.append("Checking  InGame Characters")    
    control.ListData.Items.append(" ")
    if len(lIGCharList) > 0:         
        for lIGChar in lIGCharList:
            control.ListData.Items.append("    " + lIGChar.Name)
           
            # Start In Game Character Checks
            rs_LaunchROM(lIGChar, control, event) # Check 1
            control.ListData.Items.append(" ")        
            rs_FloorContactCheck(lIGChar, control, event) # Check 2
            control.ListData.Items.append(" ")
            rs_MoverGroupCheck(lIGChar, control, event) # Check 3
            control.ListData.Items.append(" ")
            rs_TextureCheck(lIGChar, control, event) # Check 4
    else:
        control.ListData.Items.append("    None")  

