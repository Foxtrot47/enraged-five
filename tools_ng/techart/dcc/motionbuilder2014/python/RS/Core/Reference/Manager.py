'''
Module for interfacing with references in the currently loaded scene.

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>

Example usage:
    
    # Create the reference system manager.
    mgr = RsReferenceManager()
    
    # First thing you should run after creating the manager.  This
    # collects all of the references in the scene and store them internally
    # in the manager.
    mgr.collectSceneReferences()
    
    # Pointer to the 'REFERENCE:Scene' null.
    mgr.root            # Read-only
    
    # Dictionary for all of the found references in the scene.
    mgr.references      # Read-only
    
    # Easily access different reference types from the manager.
    mgr.characters      # Read-only
    mgr.vehicles        # Read-only
    mgr.bugs            # Read-only
    mgr.face            # Read-only
    mgr.props           # Read-only
    mgr.rayfire         # Read-only
    
    # Query just the referenced characters.
    chars = mgr.getReferencesByAssetType( TYPE_CHARACTERS )
    
    # Query the reference to Player_Zero.
    ref = mgr.getReferenceByName( 'Player_Zero' )
    
    # Reference attributes
    ref.modelNull       # Read-only
    ref.name            # Read-only
    ref.namespace
    ref.p4Version       # Read-only
    ref.update
    ref.reload
    ref.assetType       # Read-only
    ref.referencePath   # Read-only
    
    # Printing a reference will present a nicely formatted output.  Example below:
    >>> print ref
    
    Name:            Player_Zero
    Namespace:        Player_Zero
    Reference Path: <project>/art/animation/resources/characters/Models/cutscene/Player_Zero.fbx
    Asset Type:        Characters
    P4 Version:        59
    Reload:            False
    Update:            False
    Model Null:        <pyfbsdk.FBModelNull object at 0x0000000029A55C88> 
'''

import os
import types

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Config
import RS.Perforce
import RS.Core.Reference.Lib


## String name defines ##
STR_REFERENCE_SCENE_NAMESPACE   = 'REFERENCE'
STR_REFERENCE_SCENE_NAME        = 'Scene'
STR_RS_NULL_NAMESPACE           = 'RS_Null'

## Custom attribute names ##
STR_NAMESPACE       = 'Namespace'
STR_RS_TYPE         = 'rs_Type'
STR_RS_ASSET_TYPE   = 'rs_Asset_Type'
STR_RELOAD          = 'Reload'
STR_P4_VERSION      = 'P4_Version'
STR_UPDATE          = 'Update'
STR_BUG_NUMBERS     = 'Bug Numbers'
STR_REFERENCE_PATH  = 'Reference Path'
STR_UDP3DSMAX       = 'UDP3DSMAX'
STR_FACE_REFERENCE  = 'faceReference'
STR_TAKE_NAME       = 'Take_Name'
STR_IMPORT_OPTION   = 'Import_Option'
STR_CREATE_MISSING  = 'Create_Missing_Takes'

## Reference type defines ##
TYPE_CHARACTERS     = 'Characters'
TYPE_PROPS          = 'Props'
TYPE_SETS           = 'Sets'
TYPE_FACE           = '3Lateral'
TYPE_FACE_AMBIENT   = 'AmbientFace'
TYPE_RAYFIRE        = 'Rayfire'
TYPE_VEHICLES       = 'Vehicles'
TYPE_BUG            = 'Bug'


## Functions ##

def GetReferenceSceneNullName():
    global STR_REFERENCE_SCENE_NAMESPACE
    global STR_REFERENCE_SCENE_NAME
    
    return '{0}:{1}'.format( STR_REFERENCE_SCENE_NAMESPACE, STR_REFERENCE_SCENE_NAME )

def GetReferenceSceneNull():
    return FBFindModelByLabelName( GetReferenceSceneNullName() )

## Classes ##

class RsReference( object ):
    '''
    Represents a referenced object.
    '''
    
    def __init__( self, rsNull ):
        
        if not isinstance( rsNull, FBModelNull ):
            assert 0, 'Cannot create the RsReference!  You must supply a valid FBModelNull object.'
            return False
        
        # Pointer to the real null object.
        self.__null             = rsNull
        
        # Pointers to the property list objects for the custom properties we care about.
        self.__referencePath    = rsNull.PropertyList.Find( STR_REFERENCE_PATH )
        self.__namespace        = rsNull.PropertyList.Find( STR_NAMESPACE )
        self.__reload           = rsNull.PropertyList.Find( STR_RELOAD )
        self.__p4Version        = rsNull.PropertyList.Find( STR_P4_VERSION )
        self.__update           = rsNull.PropertyList.Find( STR_UPDATE )
        self.__faceReference    = rsNull.PropertyList.Find( STR_FACE_REFERENCE )
        
        self.__facialRoot       = None
        
        facialRootNames = ["FACIAL_facialRoot", "FACIAL_C_FacialRoot"]
        for headName in facialRootNames:
            tFacialRoot = None
            if self.__namespace:
                tFacialRoot = FBFindModelByLabelName( '{0}:{1}'.format( self.__namespace.Data, headName ) )
            if tFacialRoot != None:
                self.__facialRoot = tFacialRoot
            
            
        
        
        # Deal with a bug null type.
        bugProperty = rsNull.PropertyList.Find( STR_BUG_NUMBERS )
        
        if bugProperty:
            self.__assetType = TYPE_BUG
            self.__bugNumbers = bugProperty
            
        else:
            self.__assetType = rsNull.PropertyList.Find( STR_RS_ASSET_TYPE )
            
    
    ## Class Overrides ##

    def __repr__( self ):
        nullStr = 'Name:\t\t' + self.name + '\n'
        
        if self.namespace:
            nullStr += 'Namespace:\t' + self.namespace + '\n'
            
        if self.referencePath:
            nullStr += 'Reference Path:\t' + self.referencePath + '\n'
            
        if self.faceReference:
            nullStr += 'Face Reference:\t' + self.faceReference + '\n'
            
        if self.assetType:
            nullStr += 'Asset Type:\t' + self.assetType + '\n'
            
        if self.p4Version:
            nullStr += 'P4 Version:\t' + self.p4Version + '\n'
            
        if self.reload != None:
            nullStr += 'Reload:\t\t' + str( self.reload ) + '\n'
            
        if self.update != None:
            nullStr += 'Update:\t\t' + str( self.update ) + '\n'
            
        if self.bugNumbers:
            nullStr += 'Bug Numbers:\t' + str( self.bugNumbers ) + '\n'
            
        if self.modelNull:
            nullStr += 'Model Null:\t' + str( self.modelNull ) + '\n'
        
        return nullStr

    
    ## Properties ##
    
    @property
    def facialRoot( self ):
        return self.__facialRoot
    
    @property
    def faceReference( self ):
        if self.__faceReference:
            return self.__faceReference.Data
        
        return None
    
    @faceReference.setter
    def faceReference( self, refName ):
        if self.__faceReference:
            self.__faceReference.Data = refName
            
        else:
            assert 0, 'Cannot set face reference because that property does not exist on ({0})!'.format( self.name )

    @property
    def modelNull( self ):
        return self.__null
        
    @property
    def name( self ):
        return self.__null.Name
        
    @property
    def referencePath( self ):
        if self.__referencePath:
            return self.__referencePath.Data
        return None
    
    @referencePath.setter
    def referencePath( self, value ):
        if self.__referencePath != None:
            self.__referencePath.Data = str(value)
            
    @property
    def namespace( self ):
        if self.__namespace:
            return self.__namespace.Data
        
        return None
        
    @property
    def bugNumbers( self ):
        if self.__assetType == TYPE_BUG:
            bugs = []
            
            rawBugNumbers = str( self.__bugNumbers.Data ).split( ',' )
            
            for bugNum in rawBugNumbers:
                if bugNum != '':
                    bugs.append( int( bugNum ) )
                
            return bugs
            
        return None
    
    @bugNumbers.setter
    def bugNumbers( self, newBugNumbers ):
        raise NotImplementedError, 'Not yet implemented!'
        
    @property
    def assetType( self ):
        if self.__assetType:
            if self.__assetType == TYPE_BUG:
                return self.__assetType
            
            else:
                return self.__assetType.Data
        
        return None
        
    @property
    def reload( self ):
        if self.__reload:
            return self.__reload.Data
        
        return None
        
    @reload.setter
    def reload( self, state ):
        if self.__reload != None:
            self.__reload.Data = state
            
        else:
            assert 0, 'Cannot set value! The attribute ({0}) does not exist on ({1}).'.format( STR_RELOAD, self.name )
        
    @property
    def p4Version( self ):
        if self.__p4Version:
            if self.__p4Version.Data == 'None':
                return None
            
            else:
                return self.__p4Version.Data
        
        return None
    
    @p4Version.setter
    def p4Version( self, value ):
        if self.__p4Version != None:
            self.__p4Version.Data = str(value)
    
    @property
    def update( self ):
        if self.__update:
            return self.__update.Data
        
        return None
        
    @update.setter
    def update( self, state ):
        if self.__update != None:
            self.__update.Data = state
            
        else:
            assert 0, 'Cannot set value! The attribute ({0}) does not exist on ({1}).'.format( STR_UPDATE, self.name )
        

    ## Private Methods ##
    
    def __recurseCollectAnimatableFaceControls( self, currentControl, controls ):
        '''
        Recursively collects all animatable controls in the face control gui.
        '''
        for child in currentControl.Children:
            tagProp = child.PropertyList.Find( STR_UDP3DSMAX )
            rsType = child.PropertyList.Find( STR_RS_TYPE )
            
            if rsType:
                if rsType.Data == 'rs_3Lateral':
                    #if tagProp:
                    #    if ( 'BONETAG_{0}'.format( child.Name ) ) in tagProp.Data:
                    #        controls.append( child )
                        
                    if str( child.Name ).endswith( '_CTRL' ):
                        controls.append( child )
                        
            self.__recurseCollectAnimatableFaceControls( child, controls )
            
    def __recurseCollectAnimatableAttrFaceControls( self, currentControl, controls ):
        '''
        Recursively collects all animatable controls in the face attribute control gui.
        '''
        for child in currentControl.Children:
            rsType = child.PropertyList.Find( STR_RS_TYPE )
            
            if rsType:
                if rsType.Data == 'rs_3Lateral' and str( child.Name ).startswith( 'CIRC_' ):
                    controls.append( child )
                    
            self.__recurseCollectAnimatableAttrFaceControls( child, controls )
            
    def __recurseCollectAnimatableFaceBoneControls( self, currentControl, controls ):
        '''
        Recursively collects all animatable facial bone controls.
        '''
        for child in currentControl.Children:
            rsType = child.PropertyList.Find( STR_RS_TYPE )
            
            if rsType:
                if rsType.Data == 'rs_3Lateral' and str( child.Name ).endswith( '_CTRL' ):
                    controls.append( child )
                    
            self.__recurseCollectAnimatableFaceBoneControls( child, controls )        
    
    
    ## Public Methods ##
    
    def refresh( self ):
        '''
        Update the internal properties.
        '''
        
        if self.__null:
        
            # Pointers to the property list objects for the custom properties we care about.
            self.__referencePath    = self.__null.PropertyList.Find( STR_REFERENCE_PATH )
            self.__namespace        = self.__null.PropertyList.Find( STR_NAMESPACE )
            self.__reload           = self.__null.PropertyList.Find( STR_RELOAD )
            self.__p4Version        = self.__null.PropertyList.Find( STR_P4_VERSION )
            self.__update           = self.__null.PropertyList.Find( STR_UPDATE )
            self.__faceReference    = self.__null.PropertyList.Find( STR_FACE_REFERENCE )
            
            # Deal with a bug null type.
            bugProperty = self.__null.PropertyList.Find( STR_BUG_NUMBERS )
            
            if bugProperty:
                self.__assetType = TYPE_BUG
                self.__bugNumbers = bugProperty
                
            else:
                self.__assetType = self.__null.PropertyList.Find( STR_RS_ASSET_TYPE )        
    
    def getHintString( self ):
        '''
        For use in a control's .Hint property.
        '''
        nullStr = 'Name:\t' + self.name + '\n'
                
        if self.namespace:
            nullStr += 'Namespace:\t' + self.namespace + '\n'
            
        if self.referencePath:
            nullStr += 'Reference Path:\t' + self.referencePath + '\n'
            
        if self.faceReference:
            nullStr += 'Face Reference:\t' + self.faceReference + '\n'
            
        if self.assetType:
            nullStr += 'Asset Type:\t' + self.assetType + '\n'
            
        if self.p4Version:
            nullStr += 'P4 Version:\t' + self.p4Version + '\n'
            
        if self.reload != None:
            nullStr += 'Reload:\t' + str( self.reload ) + '\n'
            
        if self.update != None:
            nullStr += 'Update:\t' + str( self.update ) + '\n'
            
        if self.bugNumbers:
            nullStr += 'Bug Numbers:\t' + str( self.bugNumbers ) + '\n'
            
        if self.modelNull:
            nullStr += 'Model Null:\t' + str( self.modelNull ) + '\n'
        
        return nullStr      
    
    def exportFaceAnimation( self, fbxFilename ):
        if self.selectAnimatableFaceControls():       
            return FBApplication().FileExport( fbxFilename )
        
        else:
            FBMessageBox( 'Rockstar Referencing System', 'Could not export face animation for ({0}) because an unknown error\noccurred while attempting to select the animatable facial controls.'.format( self.name ), 'OK' )
            return False
        
    def importFaceAnimation( self, fbxFilename ):
        if self.selectAnimatableFaceControls():
            return FBApplication().FileImport( fbxFilename, True )
        
        else:
            FBMessageBox( 'Rockstar Referencing System', 'Could not import face animation for ({0}) because an unknown error\noccurred while attempting to select the animatable facial controls.'.format( self.name ), 'OK' )
            return False            
    
    def getAnimatableFaceControls( self ):
        '''
        Finds only the animatable face controls for this reference and returns them as a list.
        '''
        controls = []
                
        # Collect all of the face controls.
        faceControlsRoot = FBFindModelByLabelName( '{0}:faceControls_OFF'.format( self.namespace ) )
        
        if faceControlsRoot:
            rsType = faceControlsRoot.PropertyList.Find( STR_RS_TYPE )
                    
            if rsType:
                if rsType.Data == 'rs_3Lateral':
                    self.__recurseCollectAnimatableFaceControls( faceControlsRoot, controls )
                    
        # Collect all of the attribute gui face controls.
        faceAttrGuiRoot = FBFindModelByLabelName( '{0}:FacialAttrGUI'.format( self.namespace ) )
        
        if faceAttrGuiRoot:
            rsType = faceAttrGuiRoot.PropertyList.Find( STR_RS_TYPE )
                    
            if rsType:
                if rsType.Data == 'rs_3Lateral':
                    self.__recurseCollectAnimatableAttrFaceControls( faceAttrGuiRoot, controls )
                    
        # Collect the face bone controls.
        faceBoneRoot = FBFindModelByLabelName( '{0}:facialRoot_C_OFF'.format( self.namespace ) )
        
        if faceBoneRoot:
            rsType = faceBoneRoot.PropertyList.Find( STR_RS_TYPE )
                                
            if rsType:
                if rsType.Data == 'rs_3Lateral':
                    self.__recurseCollectAnimatableFaceBoneControls( faceBoneRoot, controls )            
        
        return controls
    
    def selectAnimatableFaceControls( self, keepCurrentSelection = False ):
        '''
        Selects all of the animatable facial controls for this reference.
        '''
        controls = self.getAnimatableFaceControls()
          
        if controls:
            
            # Deselect everything in the scene.
            if not keepCurrentSelection:
                modelList = FBModelList()
                FBGetSelectedModels( modelList, None, True )
                
                for model in modelList:
                    model.Selected = False
            
            # Now select them all.
            for control in controls:
                control.Selected = True
                
                # Bail if we somehow couldn't select the control.
                if not control.Selected:
                    assert 0, 'Could not select the control ({0})!'.format( control.Name )
                    return False
                
            return True
        
        return False
    
    def isLatestVersion( self ):
        '''
        Checks to see if the reference is the latest version by comparing the current p4 version attribute
        with the latest version in Perforce.
        '''
        fileInfo = RS.Perforce.GetFileState( self.referencePath )
        
        if fileInfo:
            if self.p4Version != None and fileInfo.HeadRevision != None:
                return int( self.p4Version ) == fileInfo.HeadRevision
            
        return False
    
    def validateName( self, quiet = False ):
        '''
        Checks to make sure that the objects in the scene match the referenced name.
        '''
        
        # Bail early if this is not a cutscene reference.
        cutsceneScenes = '{0}\\art\\animation\\cutscene\\!!scenes'.format( RS.Config.Project.Path.Root )
        cutsceneFacial = '{0}\\art\\animation\\cutscene\\facial'.format( RS.Config.Project.Path.Root )
        
        currentFbxFilename = str( FBApplication().FBXFileName ).lower()
        
        if cutsceneScenes.lower() not in currentFbxFilename and cutsceneFacial.lower() not in currentFbxFilename:
            return True
        
        
        objs = None
        
        # Hayes: Not exactly sure what types I should be checking against, so for now this is
        # limited to characters.
        if self.assetType == TYPE_CHARACTERS:
            objs = FBSystem().Scene.Characters
        
        if objs:
            for obj in objs:
                
                try:
                    namespace, name = str( obj.LongName ).split( ':' )
                
                # Found multiple ':' characters, so deal with that case.
                except ValueError:
                    names = str( obj.LongName ).split( ':' )
                    name = names[ -1 ]

                    namespace = names[ 0 ]
                    namespaces = names[ 1 : -1 ]
                    
                    # Rebuild the namespace.
                    for ns in namespaces:
                        namespace += ':{0}'.format( ns )

                # First, get the character belonging to this reference namespace.
                if self.namespace == namespace:
                    return True
                    
            # If we got here, then it means we were unable to find a matching character in the scene using the reference namespace.
            if not quiet:
                msg = 'There is a setup issue in the scene with the reference named "{0}"!\n\n'.format( self.name )
                msg += 'You are receiving this message because the reference name "{0}" does not match any of the character namespaces\n'.format( self.name )
                msg += 'listed under the "Characters" folder in the Navigator.  This typically happens when the character namespace has been changed\n'
                msg += 'to something like "{0} 1:{0}".\n\n'.format( self.name )
                msg += 'To fix this problem, follow these steps:\n'
                msg += '1. Find the character with the incorrect namespace under the "Characters" folder in the Navigator.\n'
                msg += '2. Rename the namespace for that character to match the reference name "{0}".\n'.format( self.name )
                msg += '3. Reopen the Reference Editor.  If correctly fixed, the reference should be listed.\n\n'
                msg += 'If the steps listed above do not fix the issue, please contact the Tech Art department.  For quicker turn-around, please\n'
                msg += 'include the filename and reference you are trying to update.'
                FBMessageBox( 'Rockstar Referencing System', msg, 'OK' )
                
            return False
        
        return True
    
    def validateReferencePath( self, quiet = False ):
        if not str( RS.Config.Project.Path.Root ).lower() in str( self.referencePath ).lower():
            if not quiet:
                result = FBMessageBox( 'Rockstar', 'The reference path for ({0}) does not exist under the project root!\nIt is currently mapped here:\n\n{1}\n\nWould you like to fix this now by choosing the correct refererence fbx file?'.format( self.name, self.referencePath ), 'Yes', 'No' )
                
            if quiet:
                result = 2
            
            if result == 1:
                raise NotImplementedError, 'Not yet implemented!'
            
            else:
                return False
            
        return True
    
    def validateAssetType( self, quiet = False ):
        if self.assetType == None or self.assetType == '':
            
            if not quiet:
                FBMessageBox( 'Rockstar', 'The reference ({0}) does not appear to be a valid reference!  It is missing an asset type.'.format( self.name ), 'OK' )
                
            return False
        
        return True
    
    def validateP4Version( self, quiet = False ):
        if self.p4Version == None or self.p4Version == '':
            
            if self.p4Version == '':
                self.__p4Version.Data = '1'
                return True
                
            if self.p4Version == None:
                self.__null.PropertyCreate( 'P4_Version', FBPropertyType.kFBPT_charptr, 'String', False, True, None )
                self.__p4Version = self.__null.PropertyList.Find( 'P4_Version' )
            
                if self.__p4Version != None:
                    self.__p4Version.Data = '1'
                    
                    return True
                
                else:
                    if not quiet:
                        FBMessageBox( 'Rockstar', 'The reference ({0}) does not appear to be valid!  It is missing a "P4_Version" attribute, and was unable to automatically create one.'.format( self.name ), 'OK' )
                        
                    return False
        
        return True
    
    def validate( self, quiet = False ):
        '''
        Function to validate various aspects of the reference.
        '''
    
        if not self.validateAssetType( quiet = quiet ):
            return False
        
        if not self.validateP4Version( quiet = quiet ):
            return False 
        
        if not self.validateReferencePath( quiet = quiet ):
            return False
        
        if not self.validateName( quiet = quiet ) :
            return False
        
        return True

    def updateReference( self, quiet = False ):
        
        # Hacky way to do this, but it's the way the referencing system was written.
        updateBtn = FBButton()
        updateBtn.Name = ( self.namespace + ";" + self.assetType + ";" + self.referencePath )
        updateBtn.Caption = 'Sync / Merge'
        updateBtn.customData = [ 1, 1 ] 
        
        stripBtn = FBButton()
        stripBtn.Name = self.namespace
        stripBtn.Caption = 'Strip'    
        
        RS.Core.Reference.Lib.rs_StripScene( stripBtn, None, quiet = quiet )
        
        refData = updateBtn.Name + ";" + str( updateBtn.customData[ 0 ] ) + ";" + str( updateBtn.customData[ 1 ] )
        RS.Core.Reference.Lib.rs_UpdateIndividualReference( refData, None, quiet = quiet )
        


class RsReferenceManager( object ):
    '''
    Manager for interfacing with references in the scene.  This contains RsReference objects, which are built 
    based on the references found in the currently loaded scene when .collectSceneReferences() is called.
    '''
    
    def __init__( self ):
        
        # Holds all of the found references in the scene.
        self.__references = {}
        
        # Pointer to the 'REFERENCE:Scene' object.
        self.__referenceRoot = None
     

    ## Properties ##    
    
    @property
    def root( self ):
        '''
        Returns pointer to the 'REFERENCE:Scene' object.
        '''
        return self.__referenceRoot
        
    @property
    def references( self ):
        '''
        Returns all references as a dictionary.
        '''
        return self.__references
    
    @property
    def characters( self ):
        return self.getReferencesByAssetType( TYPE_CHARACTERS ).values()
    
    @property
    def props( self ):
        return self.getReferencesByAssetType( TYPE_PROPS ).values()
    
    @property
    def rayfire( self ):
        return self.getReferencesByAssetType( TYPE_RAYFIRE ).values()
    
    @property
    def face( self ):
        return self.getReferencesByAssetType( TYPE_FACE ).values()
    
    @property
    def vehicles( self ):
        return self.getReferencesByAssetType( TYPE_VEHICLES ).values()
    
    @property
    def sets( self ):
        return self.getReferencesByAssetType( TYPE_SETS ).values()
    
    @property
    def bugs( self ):
        return self.getReferencesByAssetType( TYPE_BUG ).values()
    
    @property
    def ambientFace( self ):
        return self.getReferencesByAssetType( TYPE_FACE_AMBIENT ).values()
    

    ## Methods ##
    
    def propagateFaceAnimation( self, characters, exportOnly = False ):
        if not isinstance( characters, list ) and not isinstance( characters, tuple ):
            characters = [ characters ]
        
        faceFbxFiles = []
        charactersToPropagate = []
        
        # Validate we can propagate all of the supplied characters.
        for character in characters:
            if isinstance( character, RsReference ):
                if character.faceReference:
                    faceRef = self.getReferenceByName( character.faceReference )
                    
                    if faceRef:
                        faceFbxFiles.append( faceRef.referencePath )
                        charactersToPropagate.append( character )
                        
                    else:
                        FBMessageBox( 'Rockstar', 'Could not find a face reference for ({0})!  Is the property "faceReference" set?\n\nAborting face animation propagation for this character.'.format( characterRef.name ), 'OK' )
                    
                else:
                    FBMessageBox( 'Rockstar', 'Could not find the "faceReference" property for ({0}) or it does not have a value set!\n\nAborting face animation propagation for this character.'.format( characterRef.name ), 'OK' )
    
            else:
                assert 0, 'You must supply a RsReference object!  Got ({0}) instead.'.format( characterRef )
                    
        if charactersToPropagate:
            
            # Make sure that the fbx files are accessible.
            rs_CheckReadOnlyFiles.showDialog( faceFbxFiles )
            
            if rs_CheckReadOnlyFiles.result:
                
                # Store important reference data we will need later once we start opening files.  This is necessary
                # because the pointers in the current reference data will become invalid.
                refData = {}                
                
                # Export the face animation for each character.
                for character in charactersToPropagate:
                    faceRef = self.getReferenceByName( character.faceReference )
                    faceFbx = faceRef.referencePath
                    
                    propaFaceFbx = '{0}_propa.fbx'.format( os.path.join( os.path.dirname( faceFbx ), str( os.path.basename( faceFbx ) ).split( '.' )[ 0 ] ) )
                    character.exportFaceAnimation( propaFaceFbx )
                    
                    refData[ character.name ] = [ faceFbx, propaFaceFbx ]
                    
                # Open each face fbx file and import the exported face animation.
                for charName, data in refData.iteritems():
                    faceFbx, propaFaceFbx = data                    
                    
                    if not exportOnly:
                        try:
                            FBApplication().FileOpen( faceFbx )
                            
                        except RuntimeError:
                            FBMessageBox( 'Rockstar', 'An error occurred while trying to open the file:\n\n{0}\n\nPlease check to see if the file is possibly corrupt.'.format( faceFbx ), 'OK' )
                            return False

                        # Need to create a RsReference object for the newly opened scene, since they
                        # will be invalidated once we open a new file.
                        refNull = FBFindModelByLabelName( STR_RS_NULL_NAMESPACE + ':' + charName )
                        
                        if refNull:
                            ref = RsReference( refNull )
                            ref.importFaceAnimation( propaFaceFbx )
                            FBApplication().FileSave( faceFbx )
                            
                        else:
                            FBMessageBox( 'Rockstar', 'Could not find a reference for:\n\n{0}\n\nin the file:\n\n{1}\n\nCannot propagate face animation!'.format( charName, faceFbx ), 'OK' )
                    
                FBMessageBox( 'Rockstar', 'Completed propagating facial animation.', 'OK' )
                
                return True
            
        return False

    def removeReference( self ):
        raise NotImplementedError, 'Not yet implemented!'
    
    def removeAllReferences( self ):
        raise NotImplementedError, 'Not yet implemented!'
    
    def updateReference( self ):
        raise NotImplementedError, 'Not yet implemented!'
    
    def updateAllReferences( self ):
        raise NotImplementedError, 'Not yet implemented!'
    
    def createReference( self ):
        raise NotImplementedError, 'Not yet implemented!'
    
    def getReferenceByName( self, name ):
        '''
        Get a reference by the supplied name.
        '''
        if name in self.__references:
            return self.__references[ name ]
            
        return None
        
    def getReferencesByAssetType( self, assetType ):
        '''
        Returns RsReference objects of the supplied asset type as a dictionary.
        '''
        refs = {}
        
        for refName, ref in self.__references.iteritems():
            if ref.assetType == assetType:
                refs[ ref.name ] = ref
                
        return refs
                   
    def collectSceneReferences( self ):
        '''
        Queries the currently loaded scene for references, and updates the internal dictionary
        on the manager used to track references.
        '''
        self.__references = {}
        namespaces = []
        
        # Get the root reference scene node.
        refNull = FBFindModelByLabelName( STR_REFERENCE_SCENE_NAMESPACE + ':' + STR_REFERENCE_SCENE_NAME )
        
        if refNull:
            self.__referenceRoot = refNull
            
            # Iterate over the root reference scene node children and create an RsReference object for each one.
            for child in refNull.Children:
                ref = RsReference( child )
                
                if ref:
                    self.__references[ ref.namespace ] = ref
            
            return True
            
        return False
