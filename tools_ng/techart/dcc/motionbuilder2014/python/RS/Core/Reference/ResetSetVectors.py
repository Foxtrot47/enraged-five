###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_ResetSetVectors.py
## Written And Maintained By: David Bailey
## Contributors:
## Description: Resets Set Root In Cutscenes
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo

import RS.Core.Reference.Manager

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gSetScaleOneHundred =               ["Abbatoir_Int", "AH2B_Street_Ext", "AH_Outside_Bar_Ext", "Airport_Entrance_Ext",
                                     "Airport_Hangar_Int", "ArmyBaseBridge_Ext", "Bahama_Club_Int", "BankHeist_Int",
                                     "Beachhouse_CH1_Ext", "Beach_Gym_Ext", "BioTech_Lab_Ext", "BioTech_Lab_INT",
                                     "BS_AirStrip_EXT", "BS_FinaleBank_Ext", "BS_FinaleBank_Int", "BS_FinaleBank_Tunnel_Int",
                                     "BS_HomelessCamp_Ext", "BusStation_DT1_Ext", "Chinese_Theatre_Ext", "ChopShop_Ext",
                                     "Church_CS1_Ext", "CIA_Office_Int", "CoffeeShop_BT_Ext", "ComedyClub_Int",
                                     "Construction_Site_Ext", "Coroners_INT", "CountryBank_Int", "Country_Airport_Ext",
                                     "Country_Club_Ext", "EP_Countryside_Ext", "EP_Desert_Hideaway_Ext", "EP_EmptyShop_Ext",
                                     "EP_EpsilonBuilding_Ext", "ES_AirportEntrance_Ext", "EXL3_Beach_Ext", "FAM_1_Roadside_Ext",
                                     "FAM_BeanScene_Ext", "Farmhouse_Ext", "FBI_Lobby_Int", "FIB_Building_Ext",
                                     "FIB_Building_Int", "FIB_Roof_Ext", "FIN2_PowerPlant_Ext", "FIN2_WindFarm_Ext",
                                     "FIN_Industrial_Ext", "FIN_OilJacks_Ext", "Floyd_Apt_Ext", "Floyd_Apt_Int",
                                     "Foundry_Ext", "Foundry_Int", "FRA0_Alley_HW1_Ext", "FRA0_Trainyard2_Ext",
                                     "Franklins_House_Ext", "Franklins_House_Int", "Franklins_LargeHouse_CH2_Ext",
                                     "Franklins_LargeHouse_CH2_Int", "FRAS_Street_SC_Ext", "GenericStreet_Ext",
                                     "Graveyard", "Hairdresser_Int", "ID1_Street_Ext", "IG_CarMod_Int", 
                                     "IG_Countryside_Stream_Ext", "JanitorApt_Int", "Jewellers_Ext", "Jewellers_Int",
                                     "Kortz_Center_Ext", "Lake_Vinewood_Carpark_Ext", "Lesters_house_EXT", "Lesters_House_INT",
                                     "Life_Invader_Office_Ext", "Life_Invader_Office_Int", "Lost_TrailerPark", "LSDHS_Docks2",
                                     "MichaelSafeHouse_Ext", "MichaelSafeHouse_Int", "MICS_BevHills_Hotel_Ext", "Desert_Lockup_Ext",
                                     "Movie_Set_Ext", "Oilfield_Ext", "Ortega_Trailer", "PAP3_DrugScene_Ext", "PAP_ChateauMarmont_Ext",
                                     "PAP_DelPerro_Ext", "PAP_Hotel_Ext", "PAP_Morningwood_Ext", "PAP_Vinewood2_Ext",
                                     "PAP_Vinewood_Ext", "PeterDreyfuss_House_Ext", "Pier_VB_Ext", "PO1_CarPark_Ext",
                                     "PoliceHub_Int", "PRO_CashDepot_Int", "PRO_Road_Ext", "RailwayCrashArea_Ext",
                                     "Recycle_Plant_Ext", "Rec_Center_Ext", "Roadside_CS6_Ext", "RockClub_Int",
                                     "RPJosh_Motel_Ext", "Sand_Dunes_Ext", "Sharmoota_House_Ext", "Sharmoota_House_Int",
                                     "Shrinks_Office_Ext", "Shrinks_Office_Int", "SniperLoc_CH_Ext", "Solomons_Office_Int",
                                     "SOL_Tunnel_Road_Ext", "Stadium_Ext", "StripClub_SC1_Ext", "StripClub_SC1_Int",
                                     "SweatShop_Empty_Int", "Sweatshop_Ext", "SweatShop_Int", "Tattoo_Parlour_Int",
                                     "Trailer_Park_Ext", "TrevorDrive_End_Ext", "Trevors_Trailer_INT", "Trevors_Trailer_Tidy_Int",
                                     "TRVRAM_ArmyBase_Ext", "TRVRAM_LiquorMarket_Ext", "TRVRAM_SilverLake_Ext", "TRVRAM_Street_SC1_Ext",
                                     "TRVS_Dumpster_Ext", "TRVS_FastFood_SC_Ext", "TRVS_Monument_Ext", "TRVS_Pub_VB1_Ext",
                                     "Vinewood_Hills_Driveway_Ext", "Weapon_Shop_Int", "MICS_HookerMeet_Ext"]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gSetScaleOne =                      ["4Trail_old_young_gangbangers", "AH_3B_MCS_6", "Airport2_Ext", "Airport3_Ext",
                                     "Airport_Ext", "AirStrip_Ext", "AVB_Houses_Ext", "BioTech_Road_Ext", "BS_CarParkRoof_Ext",
                                     "BS_HijackTunnel_Ext", "BusStop_CH106_Ext", "CAR3_CountrySide_Ext", "CAR5_DockMeet_Ext",
                                     "CAR5_Docks_Ext", "CAR5_Garage_SC1_Ext", "Car_Dealership_Ext", "Coroners_Ext",
                                     "Depot_Ext", "Desert_Magenta1_Ext", "Driveway_HW1_Ext", "EF_Boardwalk_Ext",
                                     "EF_CyclePath_Ext", "EP6_CarPark_Ext", "EP_Dock_Ext", "EP_Woods_CS201_Ext",
                                     "ES_Dam_Ext", "ES_HighBuild_DT1_Ext", "ES_Woods_P1_CH1_Ext", "ES_Woods_P2_CH1_Ext",
                                     "EXL2_Countryside_Ext", "EXL_CargoPlane_Int", "Barbers_Int", "FAM_TramCrash_EXT",
                                     "Farmyard_Ext", "FBI1_CH311_Ext", "FIN_Cliff_Ext", "FRA0_Trainyard_Ext",
                                     "FRA1_Canal_SC1_Ext", "FRA1_GroveStreet_Ext", "FRA2_CarPark_SM_Ext", "FRAS_Club_Ext",
                                     "HollywoodBowl_Ext", "IG_AmbientCashReg_Ext", "IG_ARM1_Bank_Ext", "IG_CarPark_BH1_Ext",
                                     "IG_CourierBuilding_HW1_Ext", "IG_Garage_HW1_Ext", "IG_VB26_Ext", "JanitorApt_Ext",
                                     "JH_Canal_ID_Ext", "LAM0_Street_SC1", "Lamars_House_Ext", "LSDHS_Docks3",
                                     "Meth_Lab_Ext", "MICS_Bar_VB1_Ext", "MICS_Canal_Ext", "MICS_FancyClothesShop_Ext",
                                     "MICS_FancyHairSaloon_Ext", "MICS_FancyRestaurant_Ext", "MICS_Restaurant_HW1_Ext",
                                     "MICS_Restaurant_SS1_Ext", "MICS_Shop_Ext", "MICS_Street_VB_Ext", "MMB_Bakery_Ext",
                                     "MMB_Desert_Ext", "MMB_Hills_Ext", "Movie_Set_ActorTrailer_Ext", "Observatory_Ext",
                                     "PAP1_Road_DT1_Ext", "PAP3_CrashScene_Ext", "Planecrash_Area_Ext", "PRO_CashDepot_Ext",
                                     "RBH_Alley_Chase_Ext", "RBH_TrainTracks_Ext", "Riverside_CS2", "Roadside_CS1_Ext",
                                     "SAS_Woods_CS1", "StiltHouse_Ext", "Tequilala_Ext", "TRVRAM_BeerShop_Ext", "TRVS_Alley_Ext",
                                     "TRVS_Beach", "TRVS_Bridge2_PO_Ext", "TRVS_Bridge_Ext", "TRVS_Bridge_PO_Ext", "TRVS_Cafe_Ext", 
                                     "TRVS_Desert_Ext", "TRVS_Docks_EXT", "TRVS_Fountain_Ext", "TRVS_Pier_Ext", "TRVS_Yacht_Club_Ext"]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gSetScaleOneHundredNegativeNinety = ["Alleyway_CS105", "Bahama_Club_Ext", "Balcony_bug8393", "BAR_CityHall_Ext",
                                     "BAR_Downtown_Ext", "BAR_Park_Ext", "BeachHut_VB_Ext", "BikeRental_VB_Ext",
                                     "Boat_Family2_Ext", "BoneYard_Ext", "Bridge_CountrySide_Ext", "Brucie_Club_Ext",
                                     "BRU_Gym_BH104_Ext", "BurgerShot_VB_Ext", "ChickenFactory_Ext", "ChickenFactory_Int",
                                     "ChopShop_Int", "CropDust_CH1_Ext", "Derelict_Building_Int", "DES_Car_dealership_INT",
                                     "Deviant_Int", "Domestic_Apt_Ext", "DowntownFreeway_Bridge_Ext", "DriveThru_Hollywood_Ext",
                                     "DriveThru_SouthCentral_Ext", "EF_Beach_Ext", "ElSalvador_Alleyway_EXT",
                                     "ElSalvador_Alleyway_EXT1", "EP_Vinewood_Street_Ext", "ES_BuildTop_Ext", "ES_CraneBuild_DT1_Ext",
                                     "FarmHouse_Int", "FilmStudios_Ext", "FleecaBank_HW_Int", "FRAS_DrugShop_Ext",
                                     "FRAS_ParkingLot_Ext", "FRAS_Pub_SC1_Ext", "FRAS_Restaurant_SC1_Ext", "HollywoodSign_Ext",
                                     "Hotel_DT_Ext", "IG_Assassination_PayPhone_Ext", "IG_BackGarden_ID1_Ext",
                                     "IG_PoolPerv_HW1_Ext", "JH_Cutscene_Tower_Climb", "Laboratory_Int", "LSDHS_Docks1",
                                     "Magenta_House_Ext", "MalibuFreeway_Junction_Ext", "MICS_Marina_VB1_Ext",
                                     "MICS_ParkBench_CH2_Ext", "MICS_ParkBench_HW1_Ext", "MICS_Pharmacy_Ext",
                                     "MP_PB_Prison_Ext", "MP_PB_Prison_Ext_Section233275", "MP_PB_Prison_Ext_Section233281",
                                     "MP_PB_Prison_Ext_Section233287", "MP_PB_Prison_Ext_Section233288", "NMT_SantaMonica_Ext",
                                     "NMT_Vinewood_House_Ext", "NMT_Warehouse_Ext", "PAP_Restaurant_Ext", "PAP_Street_SS1_Ext",
                                     "PetrolStation_CS3_Ext", "PRO_FarmHouse_Ext", "Recycle_Plant_Int", "RPJosh_Mansion_Ext",
                                     "RuralBankHeist_Ext", "SniperBuild_DT102_Ext", "Stadium_AuditionRoom_Int",
                                     "StormDrain_ID_Ext", "StreetMug_DT1_9_Ext", "Torture_Warehouse_Ext", "Torture_Warehouse_Int",
                                     "Tower_CH4_Ext", "TRVS_Garden_HW1_Ext", "TRVS_HardwareShop_Ext", "TRVS_LingereShop_Ext",
                                     "TRVS_Mortuary_Ext"]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gSetArbitraryVector = ["Car_dealership_INT",
                       "Family_1_Boat",
                       "Hick_bar_INT",
                       "Hick_Bar_EXT",
                       "Meth_Lab_Int",
                       "MICS_Pier_Ext"]


gSetArbitraryVector = [[FBVector3d(0, 0, 0), FBVector3d(0, -0, -20), FBVector3d(100, 100, 100)],
                       [FBVector3d(-2.13163e-014, 5.96046e-007, 4.59732e-022), FBVector3d(1.27222e-014, -0, 0), FBVector3d(100, 100, 100)],
                       [FBVector3d(0, 0, 0), FBVector3d(7.59607e-015, -1.37433e-014, -57.86), FBVector3d(100, 100, 100)],
                       [FBVector3d(0, 0, 0), FBVector3d(-90, -27.622, 0), FBVector3d(100, 100, 100)],
                       [FBVector3d(40.1249, -36.95, 25.0237), FBVector3d(0, -0, 0), FBVector3d(100, 100, 100)],
                       [FBVector3d(-170547, 1652.56, 111758), FBVector3d(0, 0, 0), FBVector3d(100, 100, 100)]]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ResetSetVectors():
     
    lRefernceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
    
    if lRefernceNull:                  
        for iChild in lRefernceNull.Children:                      
            for model in glo.gRoot.Children:
                if model.LongName.startswith(iChild.Name):
                    if iChild.Name in gSetScaleOneHundred:
                        model.PropertyList.Find("Lcl Translation").Data =  FBVector3d(0, 0, 0)
                        model.PropertyList.Find("Lcl Rotation").Data =  FBVector3d(0, 0, 0)
                        model.PropertyList.Find("Lcl Scaling").Data = FBVector3d(100, 100, 100)
                        glo.gScene.Evaluate()
                    elif iChild.Name in gSetScaleOne:  
                        model.PropertyList.Find("Lcl Translation").Data =  FBVector3d(0, 0, 0)
                        model.PropertyList.Find("Lcl Rotation").Data =  FBVector3d(0, 0, 0)
                        model.PropertyList.Find("Lcl Scaling").Data = FBVector3d(1, 1, 1) 
                        glo.gScene.Evaluate()
                    elif iChild.Name in gSetScaleOneHundredNegativeNinety: 
                        model.PropertyList.Find("Lcl Translation").Data =  FBVector3d(0, 0, 0)
                        model.PropertyList.Find("Lcl Rotation").Data =  FBVector3d(-90, 0, 0)
                        model.PropertyList.Find("Lcl Scaling").Data = FBVector3d(100, 100, 100)
                        glo.gScene.Evaluate()  
                    elif iChild.Name in gSetArbitraryVector:
                        for i in range(len(gSetArbitraryVector)):
                            if iChild.Name == gSetArbitraryVector[i]:
                                model.PropertyList.Find("Lcl Translation").Data =  gSetArbitraryVector[i][0]
                                model.PropertyList.Find("Lcl Rotation").Data =  gSetArbitraryVector[i][1]
                                model.PropertyList.Find("Lcl Scaling").Data = gSetArbitraryVector[i][2]
                                     

