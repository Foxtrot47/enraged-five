from pyfbsdk import *
import RS.Tools.UI.Commands
import RS.Utils.UserPreferences as userPref


## Cutscene Menu Enums ##

# Reference Menu Defines
NEW_CUTSCENE                    = "R* Assembler New"
OPEN_CUTSCENE                   = "R* Assembler Open..."
SAVE_CUTSCENE                   = "R* Assembler Save - Place Holder"
IMPORT_ASSET                    = "R* Assembler Import Asset... - Place Holder"
CONVERT_SCENE_TO_ASSEMBLER      = "R* Convert Current Scene to Assembler"
LAUNCH_REFERNCE_DESIGNER        = "R* Launch R* Ref Designer - Place Holder"

def cutsceneMenuHandler( source, event ):
    '''
    Event handler for cutscene menu.
    '''
    if event.Name == OPEN_CUTSCENE:
        RS.Tools.UI.Commands.OpenCutscene()
        
    elif event.Name == SAVE_CUTSCENE:
        RS.Tools.UI.Commands.SaveCutscene()
        
    elif event.Name == IMPORT_ASSET:
        RS.Tools.UI.Commands.AddAssetsToCutscene()
        
    elif event.Name == CONVERT_SCENE_TO_ASSEMBLER:
        RS.Tools.UI.Commands.ConvertCurrentSceneToAssembler()
        
    elif event.Name == NEW_CUTSCENE:
        RS.Tools.UI.Commands.NewCutscene()

    elif event.Name == LAUNCH_REFERNCE_DESIGNER: # This will launch the standalone tool - TODO LATER        
        # This is a standalone tool for creating new reference layouts - Should easily pipe into offline Merge from FBX SDK
        FBMessageBox( "Reference Designer", "A freakin cool layout tool for the resource team......coming soon!", "Yes it will be cool!")         

def Run():
    if userPref.__Readini__("menu","Enable Stager", True):
        menuMgr = FBMenuManager()
        rsMenu = menuMgr.GetMenu( "File" )
        menuMgr.InsertBefore("File", "Send To Maya", NEW_CUTSCENE)
        menuMgr.InsertBefore("File", NEW_CUTSCENE, OPEN_CUTSCENE)
        #menuMgr.InsertBefore("File", CONVERT_SCENE_TO_ASSEMBLER, IMPORT_ASSET)
        #menuMgr.InsertBefore("File", IMPORT_ASSET, SAVE_CUTSCENE)
        menuMgr.InsertBefore("File", OPEN_CUTSCENE, CONVERT_SCENE_TO_ASSEMBLER)
        #menuMgr.InsertBefore("File", OPEN_CUTSCENE, NEW_CUTSCENE)
        #menuMgr.InsertAfter("File", LAUNCH_REFERNCE_DESIGNER, "") # Line Separator
        rsMenu.OnMenuActivate.Add( cutsceneMenuHandler )