###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_SceneConfigXML.py
## Written And Maintained By: David Bailey
## Contributors: -
## Description: This tool reads the current transforms of a character's skeleton and prints them into an xml file
##              The xml file with auto mark for add or check out, so it is easier for the user.  This xml is then 
##              used with the ref system to check differences between previous and current character updates, 
##              notifying the user that there has been a skel change.
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from pyfbsdk import *
import RS.Globals as glo
import RS.Perforce
import datetime
import os
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_RecurseChildNodes
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_RecurseChildNodes(pRoot, pXMLNode):
    
    lRoot = None
    
    lChildren = pRoot.Children
    
    if lChildren != None:
        
        for iChild in lChildren:
            
            if iChild.Name in glo.gSkelArray:

                lCurrentGroup = SubElement(pXMLNode, 'element', {'longName':iChild.LongName})
                
                if iChild.ClassName() == "FBModelSkeleton" or iChild.ClassName() == "FBModelNull":

                    lProperties = SubElement(lCurrentGroup, 'properties')
                    
                    for iProp in iChild.PropertyList:
                        
                        if "Lcl" in iProp.Name and not "Scaling" in iProp.Name and not "Rotation" in iProp.Name:
                            try:
                                if iProp.Data != None:
                                    
                                    lVector = FBVector3d()
                                    
                                    if "e" in str(iProp.Data[0]) or "-" in str(iProp.Data[0]):
                                        lVector[0] = 0
                                    else:
                                        if "." in str(iProp.Data[0]):
                                            lVector[0] = float( str(iProp.Data[0]).partition(".")[0] + "." + str(iProp.Data[0]).partition(".")[2][0:2] )
                                        else:
                                            lVector[0] = iProp.Data[0]
                                    
                                    
                                    if "e" in str(iProp.Data[1]) or "-" in str(iProp.Data[1]):
                                        lVector[1] = 0
                                    else:
                                        if "." in str(iProp.Data[1]):
                                            lVector[1] = float( str(iProp.Data[1]).partition(".")[0] + "." + str(iProp.Data[1]).partition(".")[2][0:2] )
                                        else:
                                            lVector[1] = iProp.Data[1]
                                    
                                    
                                    if "e" in str(iProp.Data[2]) or "-" in str(iProp.Data[2]):
                                        lVector[2] = 0
                                    else:
                                        if "." in str(iProp.Data[2]):
                                            lVector[2] = float( str(iProp.Data[2]).partition(".")[0] + "." + str(iProp.Data[2]).partition(".")[2][0:2] )
                                        else:
                                            lVector[2] = iProp.Data[2]
    
                                    lProperty = SubElement(lProperties, 'property', {'propertyName':iProp.Name, 'propertyData':str(lVector)})
                            except:
                                
                                None
                    
                    lClassName = SubElement(lProperties, 'property', {'className':iChild.ClassName()})
                
                rs_RecurseChildNodes(iChild, lCurrentGroup)
            
    else:
        
        return True

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_Prettify
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_Prettify(pElem):

    lRoughString = ElementTree.tostring(pElem, 'utf-8')
    lReparsed = minidom.parseString(lRoughString)
    return lReparsed.toprettyxml(indent="  ")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_RunConfigXML module
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_RunConfigXML():

    lFileName = glo.gApp.FBXFileName
    
    lCharacterName = os.path.basename(lFileName).partition(".")[0]
    lGeneratedOn = str(datetime.datetime.now())
    
    lConfig = Element(lCharacterName)
    
    lComment = Comment(lCharacterName + ' Config')
    lConfig.append(lComment)
    
    lScene = SubElement(lConfig, 'Scene'               )
    
    for iRoot in glo.gRoot.Children:
        
        if iRoot.Name == "Dummy01":
            
            for iChild in iRoot.Children:
                
                if iChild.Name == "mover":
            
                    lRoot = SubElement(lScene, "element", {'longName':iChild.LongName})
                    rs_RecurseChildNodes(iChild, lRoot)
        
    Actors              = SubElement(lConfig, 'Actors'              )
    ActorFaces          = SubElement(lConfig, 'ActorFaces'          )
    Cameras             = SubElement(lConfig, 'Cameras'             )
    Characters          = SubElement(lConfig, 'Characters'          )
    CharacterFaces      = SubElement(lConfig, 'CharacterFaces'      )
    CharacterExtensions = SubElement(lConfig, 'CharacterExtensions' )
    Constraints         = SubElement(lConfig, 'Constraints'         )
    Decks               = SubElement(lConfig, 'Decks'               )
    Devices             = SubElement(lConfig, 'Devices'             )
    Groups              = SubElement(lConfig, 'Groups'              )
    Sets                = SubElement(lConfig, 'Sets'                )
    Lights              = SubElement(lConfig, 'Lights'              )
    Materials           = SubElement(lConfig, 'Materials'           )
    Poses               = SubElement(lConfig, 'Poses'               )
    Shaders             = SubElement(lConfig, 'Shaders'             )
    Takes               = SubElement(lConfig, 'Takes'               )
    Textures            = SubElement(lConfig, 'Textures'            )
    Videos              = SubElement(lConfig, 'Videos'              )
    PhysicalProperties  = SubElement(lConfig, 'PhysicalProperties'  )
    Solvers             = SubElement(lConfig, 'Solvers'             )
    Systems             = SubElement(lConfig, 'Systems'             )
    
    lVar = rs_Prettify(lConfig)

    if os.path.isfile(lFileName.rpartition(".")[0] + "_Config.xml"):
        
        RS.Perforce.Edit( lFileName.rpartition(".")[0] + "_Config.xml" )
        lFile = open((lFileName.rpartition(".")[0] + "_Config.xml"), 'w')
        lFile.write(lVar)
        lFile.close()
    
    else:
        
        lFile = open((lFileName.rpartition(".")[0] + "_Config.xml"), 'w')
        lFile.write(lVar)
        lFile.close()
        RS.Perforce.Add( lFileName.rpartition(".")[0] + "_Config.xml" )
                
#rs_RunConfigXML()