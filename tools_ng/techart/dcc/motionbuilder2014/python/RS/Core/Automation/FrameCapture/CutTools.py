"""
Description:
    Functions for manipulating cutfile and zip data for capture purposes.
    Assets modified by this script SHOULD NOT be submitted to perforce.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import zipfile
from xml.etree import cElementTree as ET

import FileTools
import CapturePaths


def SendExportToNetwork(fbxPath):
    # Get Fbx Name
    fbxName = os.path.splitext(os.path.split(fbxPath)[1])[0]

    # Set Local Paths
    cutsPath = CapturePaths.GetCutsPath(fbxName)
    iasPath = CapturePaths.GetIasPath(fbxName)
    zipPath = CapturePaths.GetZipPath(fbxPath)

    # Validate Local Data
    if not os.path.exists(cutsPath) or not os.path.exists(iasPath) or not zipPath or not os.path.exists(zipPath):
        return "Error: Local export data not found for {}".format(fbxName)

    # Set Network Paths
    capDataRoot = CapturePaths.GetCapDataRoot(fbxName)
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")
    dataIasFolder = os.path.join(capDataRoot, "IasFiles")
    dataZipFolder = os.path.join(capDataRoot, "ZipFiles")

    # Validate Network Access
    if not os.path.exists(capDataRoot):
        return "Error: Access to network capture folder not found. Invalid user?"

    # Copy Cut Data
    FileTools.DeleteFolderContents(dataCutsFolder)
    FileTools.CopyFolderContents(cutsPath, dataCutsFolder)

    # Copy Ias Data
    FileTools.DeleteFolderContents(dataZipFolder)
    FileTools.CopyPath(zipPath, dataZipFolder)

    # Copy Zip Data
    FileTools.DeleteFolderContents(dataIasFolder)
    FileTools.CopyPath(iasPath, dataIasFolder)

    # Export Success
    return "Success: Export data copied successfully to network."


def ForceCutHideEventsToZero(xmlPath):
    """
    A function to search through a data.cutxml file, search for existing hide events, and set their time value to 0.0.
    Arguments:
        xmlPath: A string containing the path to the data.cuxml file.
    Returns:
        A bool - True if events were found and updated, False if nothing was found or updated
    """

    # Setup Basic Variables and Xml Root
    xmlUpdated = False
    hideObjectIdList = []
    tree = ET.parse(xmlPath)
    rootNode = tree.getroot()

    # Loop Through Hide Nodes - and storing their object Ids
    for eachNode in rootNode.findall('pCutsceneObjects/Item[@type="rage__cutfHiddenModelObject"]'):
        objectId = eachNode.find("iObjectId").attrib["value"]
        hideObjectIdList.append(objectId)

    # No Hide Nodes Found - simply exit early and return False
    if not hideObjectIdList:
        return False

    # Loop Through Event Nodes - and force the time value to 0.0
    for eachNode in rootNode.findall('pCutsceneEventList/Item[@type="rage__cutfObjectIdEvent"]'):
        if eachNode.find("iObjectId").attrib["value"] in hideObjectIdList:
            fTimeNode = eachNode.find("fTime")
            if float(fTimeNode.attrib["value"]):
                fTimeNode.attrib["value"] = "0.0"
                xmlUpdated = True

    # No Updates Made - simply exit early and return False
    if not xmlUpdated:
        return False

    # Updates Made - save changes and return true
    tree.write(xmlPath)
    return True


def BuildZipFromCutsPath(cutsPath, zipPath):
    """
    A function to build a cutscene zip file from a provided cutsPath and zipPath.
    Arguments:
        cutsPath (string): the path of the cut folder to build the zip from.
        zipPath (string): the path of the zip file to build.
    Returns:
        (bool): True if the zip built successfully, False if errors encountered.
    """
    # Delete Old Zip and Verify Folder
    FileTools.DeletePath(zipPath)
    FileTools.CreateParentFolders(zipPath)

    # Create New Zip
    newZipFile = zipfile.ZipFile(zipPath, "w")
    for root, dirs, files in os.walk(cutsPath):
        for eachFile in files:
            filePath = os.path.join(root, eachFile)
            inZipPath = filePath.replace(cutsPath, "", 1).replace("\\", "/")
            newZipFile.write(filePath, inZipPath)
    newZipFile.close()

    # Verify Zip Created Successfully
    if not os.path.exists(zipPath):
        return False
    else:
        return True


def GetProxyCharacterDict(fbxPath):
    """
    Creates a proxyDict for scene names matched in the proxyCharacterConfig.xml.
    Arguments:
        fbxPath (string): the local path of the FBX to search for
    Returns:
        proxyDict (dict): a dict of originalName: replacedName string items.
    """
    proxyDict = {}
    fbxName = os.path.splitext(os.path.basename(fbxPath))[0].lower()
    xmlPath = CapturePaths.proxyCharacterXmlPath

    if os.path.exists(xmlPath):
        tree = ET.parse(xmlPath)
        rootNode = tree.getroot()
        sceneNodes = [node for node in rootNode.iter() if node.tag.lower() == "scene" and node.attrib["Name"]]
        for sceneNode in sceneNodes:
            sceneName = sceneNode.attrib["Name"].lower()
            if fbxName in [sceneName, "*"] or (sceneName[-1] == "*" and fbxName.startswith(sceneName.replace("*", ""))):
                for characterNode in sceneNode.findall("character"):
                    originalName = characterNode.find("originalName")
                    replacedName = characterNode.find("replacedName")
                    if originalName is not None and replacedName is not None:
                        proxyDict[originalName.text] = replacedName.text
    return proxyDict


def GetMergeDict():
    """
    Searches through mergelist files and creates a merge dict of the data.
    Returns:
        mergeDict (dict): Made of merge names (keys) paired to a list of it's part names (values).
    """
    mergeDict = {}
    mergeListFolder = CapturePaths.mergeListRoot
    if os.path.exists(mergeListFolder):
        for root, dirs, files in os.walk(mergeListFolder):
            for filename in files:
                xmlPath = os.path.join(root, filename)
                rootNode = ET.parse(xmlPath).getroot()
                pathNode = rootNode.find("path")
                partNames = [os.path.split(node.text)[1].lower() for node in rootNode.findall(".//parts/Item/filename")]
                if pathNode is not None and len(partNames) > 0:
                    mergeName = os.path.split(pathNode.text)[1].lower()
                    mergeDict[mergeName] = partNames
    return mergeDict


def GetSceneOffsets(dataCutPath):
    """
    Reads a data.cutxml file and returns a scene's offset data if found.
    Arguments:
        dataCutPath (string): the path of the cutfile to search
    Returns:
        sceneOffset (list): a list of 4 floats (x, y, z, rotation)
    """
    if os.path.exists(dataCutPath):
        rootNode = ET.parse(dataCutPath)
        offsetNode = rootNode.find("vOffset")
        rotationNode = rootNode.find("fRotation")
        if offsetNode is not None and rotationNode is not None:
            sceneOffset = [offsetNode.get("x"), offsetNode.get("y"), offsetNode.get("z"), rotationNode.get("value")]
            if None not in sceneOffset:
                sceneOffset = [float(value) for value in sceneOffset]
                return sceneOffset
