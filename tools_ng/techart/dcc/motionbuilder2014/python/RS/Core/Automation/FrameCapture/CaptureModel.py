"""
Description:
    A class object that mirrors the data in a capture job xml file,
    and a few functions for updating that data during the capture process.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import time
import XmlTools


class MobuCaptureModel(object):
    """
    An object with properties and data that mirror the tags and
    values found in a capture job xml file at the provided path.
    """
    def __init__(self, jobXmlPath):
        self.jobXmlPath = jobXmlPath
        xmlDict = XmlTools.CreateTagValueDict(self.jobXmlPath)
        self.__dict__.update(xmlDict)

    def UpdateTag(self, propName, propValue):
        """
        Updates the object and job xml with a new property and value simultaneously.
        Arguments:
            propName: A string of the name of the property to update.
            propValue: The value of the property being updated.
        """
        setattr(self, propName, propValue)
        XmlTools.AddUpdateXmlFile(self.jobXmlPath, str(propName), str(propValue))

    def UpdateTime(self, stampName):
        """
        Creates / updates a timestamp property named stampName at the current time.
        Allows timestamps to be set easily without importing time elsewhere.
        Arguments:
            stampName: A string containing the name of the timestamp to update.
        """
        currentTime = time.time()
        self.UpdateTag(stampName, currentTime)