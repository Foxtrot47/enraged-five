"""
Description:
    Simple functions for copying, deleting, and modifying files and folders.
    Used in a variety of automation tasks, like cutfile manipulation.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import stat
import shutil


def CheckFileReadOnly(filePath):
    """
    Simple check to determine if a file is marked read-only.
    Arguments:
        filePath: A string of the filePath we want to check.
    Returns:
        A bool - True if the path is read-only, false if not.
    """
    fileAtt = os.stat(filePath)[0]
    return not fileAtt & stat.S_IWRITE


def MakeFileReadOnly(filePath):
    """
    Sets a filePath to read only if it isn't already.  Assumes file exists.
    Arguments:
        filePath: The path of the file we want to make writeable.
    """
    if not CheckFileReadOnly(filePath):
        os.chmod(filePath, stat.S_IREAD)


def MakeFileWriteable(filePath):
    """
    Sets a filePath to writeable if it isn't already.  Assumes file exists.
    Arguments:
        filePath: The path of the file we want to make writeable.
    """
    if CheckFileReadOnly(filePath):
        os.chmod(filePath, stat.S_IWRITE)


def MakePathWriteable(filePath):
    """
    Checks path exists, then sets a file or contents of a folder to writeable.
    Arguments:
        filePath: A string of the file or folder path to make writeable.
    """
    if os.path.exists(filePath):
        if os.path.isdir(filePath):
            for root, dirs, files in os.walk(filePath):
                for filename in files:
                    MakeFileWriteable(os.path.join(root, filename))
        else:
            MakeFileWriteable(filePath)


def CreateParentFolders(filePath):
    """
    Checks that all parent folders for a filePath exist, and creates them if they don't.
    Arguments:
        filePath: The path of the file we want to check/create the parent folders of.
    """
    # Standardize Path
    filePath = os.path.normpath(filePath)

    # Create Parent Folder List - And remove invalid folders
    pathParts = filePath.split(os.path.sep)
    if (pathParts[-1] == "") or ("." in pathParts[-1]):
        pathParts = pathParts[:-1]

    # Create Each Parent Folder - If it doesn't exist already
    currentPath = pathParts[0] + os.path.sep
    for eachFolder in pathParts[1:]:
        currentPath = os.path.join(currentPath, eachFolder)
        if not os.path.exists(currentPath):
            os.makedirs(currentPath)


def DeletePath(filePath):
    """
    Deletes a file or folder at the given filePath.
    Arguments:
        filePath: The path of the file or folder we want to delete.
    """
    if os.path.exists(filePath):
        MakePathWriteable(filePath)
        if os.path.isdir(filePath):
            shutil.rmtree(filePath)
        else:
            os.unlink(filePath)


def CopyPath(srcPath, destPath, moveMode=False):
    """
    Copies a file or folder from the srcPath to the destPath.
    Arguments:
        srcPath: A string of the source path.
        destPath: A string of the destination path.
    """
    # Folder Copy Logic
    if os.path.isdir(srcPath):
        destPath = os.path.join(destPath, os.path.split(srcPath)[1])
        DeletePath(destPath)
        CreateParentFolders(os.path.split(destPath)[0])
        if moveMode:
            shutil.move(srcPath, destPath)
        else:
            shutil.copytree(srcPath, destPath)

    # File Copy Logic
    else:
        if "." not in os.path.split(destPath)[1]:
            destPath = os.path.join(destPath, os.path.split(srcPath)[1])
        DeletePath(destPath)
        CreateParentFolders(destPath)
        if moveMode:
            shutil.move(srcPath, destPath)
        else:
            shutil.copy(srcPath, destPath)

def CopyFolderContents(srcFolder, destFolder):
    """
    Copies the contents of the srcFolder into the destFolder.
    Useful when renaming the srcFolder isn't possible because it's
    locked by another process (such as Mobu or the Rage Exporter).
    Arguments:
        srcFolder: A string of the source folder to copy files from.
        destFolder: A string of the destination folder to copy files to.
    """
    srcFolder = os.path.normpath(srcFolder)
    CreateParentFolders(destFolder)
    for (root, directories, files) in os.walk(srcFolder):
        for eachFile in files:
            currentFile = os.path.join(root, eachFile)
            currentFolder = os.path.split(currentFile)[0]
            if currentFolder == srcFolder:
                CopyPath(currentFile, destFolder)
            else:
                destSubFolder = destFolder + currentFolder.split(srcFolder)[1]
                CopyPath(currentFile, destSubFolder)


def DeleteFolderContents(folderPath):
    """
    Deletes the contents of a folder, and counts any locked files.
    Useful when the folder is locked by another process (such as Avid).
    Arguments:
        folderPath: A string of the folder to delete contents from.
    Returns:
        An int of the number of locked files or folders not deleted.
    """
    deleteList = []
    lockedCount = 0
    if not os.path.exists(folderPath):
        return lockedCount
    for (root, directories, files) in os.walk(folderPath):
        for eachFile in files:
            deleteList.append(os.path.join(root, eachFile))
        for eachDir in directories:
            deleteList.append(os.path.join(root, eachDir))
    for eachPath in deleteList:
        try:
            DeletePath(eachPath)
        except WindowsError:
            lockedCount += 1
    return lockedCount


def SearchFolderForFileName(folderPath, searchName):
    """
    Simple function for locating a file within a folder.
    Arguments:
        folderPath (string): path of the folder to search through
        searchName (string): file name to search for
    Returns:
        String of path if file is found, None otherwise.
    """
    for (root, dirs, files) in os.walk(folderPath):
        for fileName in files:
            if fileName.lower() == searchName.lower():
                return os.path.join(root, fileName)
