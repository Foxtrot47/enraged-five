"""
Description:
    Wrapper functions for interfacing with HB's DatabaseConnection module.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import uuid

try:
    # Mobu Import
    import RS.Core.DatabaseConnection as DatabaseConnection

except ImportError:
    # External Import - for the standalone capture tools
    import os
    import imp

    # Get Module Path Directory - using current directory or wildwest 2014 as a backup
    directory = os.path.split(__file__)[0]
    if directory == "":
        directory = "X:\\wildwest\\dcc\\motionbuilder2014\\python\\RS\\Core\\Automation\\FrameCapture"
    autoFolder = "{0}{1}{0}".format(os.path.sep, "Automation")
    modulePath = os.path.join(directory.split(autoFolder)[0], "DatabaseConnection.py")

    # Import Database Module via Imp
    DatabaseConnection = imp.load_source("DatabaseConnection", modulePath)

# Set Globals
DB_DRIVER = '{SQL Server}'
DB_SERVER = 'NYCW-MHB-SQL'
DB_DATABASE = 'CaptureRenders'
DB_CONN = DatabaseConnection.DatabaseConnection(DB_DRIVER, DB_SERVER, DB_DATABASE)


def CreateDbJob(fbxName, userName, jobId=None, jobPriority=0):
    """
    Creates a new job on the database with standard options.
    Arguments:
        fbxName: A string of the job's FBX name.
        userName: A string of the job's username.
    Keyword Arguments:
        jobId: An int of the job's ID (if we want to use a pre-existing ID)
        jobPriority: An int of the job's priority.
    Returns:
        An int (technically a long) of the jobID, or 0 if connection fails.
    """
    if jobId is None:
        jobId = uuid.uuid1().int
    cmdTxt = "exec CaptureMonitor_AddJob '{0}', '{1}', {2}, '{3}', '{4}'"
    cmdTxt = cmdTxt.format(userName, fbxName, 0, jobId, jobPriority)
    if DB_CONN.is_connected:
        DB_CONN.sql_command(cmdTxt)
        return jobId


def SubmitJobUpdate(jobId, statusInt, isCanceled):
    """
    Sends job update command to the database.
    Arguments:
        jobId: A long of the jobId to update.
        statusInt: An int of the job's status.
        isCanceled: An bool - True if job canceled, False otherwise.
    Returns:
        True if the command was sent, none otherwise.
    """
    cmdTxt = "exec CaptureMonitor_UpdateJob '{0}', {1}, {2}".format(jobId, statusInt, isCanceled)
    if DB_CONN.is_connected:
        DB_CONN.sql_command(cmdTxt)
        return True


def UpdateDbJob(jobId, statusInt):
    """
    Updates a job's status on the database without altering isCanceled.
    Arguments:
        jobId: A long of the jobId to update.
        statusInt: An int of the job's status.
    """
    if jobId:
        isCanceled = CheckJobTag(jobId, "IsCanceled")
        if isCanceled is not True:
            isCanceled = False
        SubmitJobUpdate(jobId, statusInt, isCanceled)


def CancelDbJob(jobId):
    """
    Sets a job's isCanceled value to True without altering status.
    Arguments:
        jobId: A long of the jobId to update.
    """
    if jobId:
        statusInt = CheckJobTag(jobId, "Status")
        if statusInt is None:
            statusInt = 0
        SubmitJobUpdate(jobId, statusInt, True)


def ReadDbJobs():
    """
    Reads all jobs on the database and returns the results.
    Returns:
        A list of row objects for the jobs on the database.
    """
    cmdTxt = "exec CaptureMonitor_ReadJobs"
    if DB_CONN.is_connected:
        tableResults = DB_CONN.sql_command(cmdTxt)
        return tableResults


def CheckJobTag(jobId, jobTag):
    """
    Looks up a specific value for a specific jobId on the database.
    Arguments:
        jobId: A long of the jobId to update.
        jobTag: A string of the job value name to lookup.
    Returns:
        The found jobTag's value - can be any type.
    """
    dbJobs = ReadDbJobs()
    for eachJob in dbJobs:
        if long(eachJob.JobID) == jobId:
            return getattr(eachJob, jobTag)

def DebugFlushJobs():
    """
    Debug command to set all jobs to complete and cancel to False.
    Useful for flushing out test data and old jobs.
    """
    dbJobs = ReadDbJobs()
    for eachJob in dbJobs:
        jobId = int(eachJob.JobID)
        SubmitJobUpdate(jobId, 5, False)