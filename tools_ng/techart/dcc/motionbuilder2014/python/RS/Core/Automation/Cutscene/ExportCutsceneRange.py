'''
Set Cutscene Options and Export. This is intially to be used with our capture process. (Extrenal process to load up MB and export cutscenes)
The purpose is that we can set up full range exports using a passed in range list and disable export options
USAGE:
setExportRanges( 10, 300 )
exportCutscene(enablePreview = False)

Author: Mark Harrison-Ball <mark.harrison-Ball@rockstargames.com>

'''

import pyfbsdk
from RS.Core.Export import *
import os



def openExporter():
    pyfbsdk.ShowToolByName("Rex Rage Cut-Scene Export")


def closeExporter():
    pyfbsdk.CloseToolByName("Rex Rage Cut-Scene Export")
    ''''''

def setExportRanges(Range_Start = 0, Range_End = 100):
    _setExportDefaults_()
    _setShotRange_(Range_Start , Range_End )


'''
   Export our Cutscene. 
   Export Cuts, Disable Perfore Intergration, Enable Preview
'''
def exportCutscene(enablePreview = True, p4Intergration=True):
    return RexExport.ExportBuildActiveCutscene( p4Intergration, enablePreview)
 
    

'''
   Set our frist shot to teh ranges we specifiy
   Turn off all post shots

'''	
def _setShotRange_(Range_Start , Range_End ):    
    ShotRange = RexExport.GetNumberCutsceneShots()
    for shotID in range(0, ShotRange): 
        if shotID == 0:
            RexExport.SetCutsceneShotStartRange( shotID, Range_Start )
            RexExport.SetCutsceneShotEndRange( shotID, Range_End )
            RexExport.SetCutsceneShotExportStatus( shotID, True )            
        else:
            RexExport.SetCutsceneShotExportStatus( shotID, False )    
    


'''
   Turn off DOF and Story Mode

'''	
def _setExportDefaults_():
    RexExport.SetCutsceneShotEnableDOFFirstCut(False)
    RexExport.SetCutsceneShotDisableDOF(True)
    RexExport.SetCutsceneStoryMode(False)