"""
Description:
    One main function for preparing an FBX for a capture export.

    Most prep steps happen within this function, but a few are handled by
    CamUtils (for simple MB stuff used frequently) and camKeyTasks (for the
    saving and loading of camera key data). Also and a few utility functions
    like ToyBox, ContentUpdate, and the Automation.FrameCapture's FileTools.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu

from RS.Core.Camera.KeyTools import camKeyTasks
from RS.Core.Automation.FrameCapture import CapturePaths, FileTools
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Camera import CamUtils


def PrepFbxForCapture(capObj):
    """
    Mid level function to set up a capture for export based on the capture type.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    """
    # Update Status
    capObj.UpdateTag("mobuStatus", "PREP FBX")

    # Set Basic Paths
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    capDataRoot = capPaths.capDataRoot
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")

    if capObj.captureType == "SIMPLE":
        # Open Full FBX
        mobu.FBApplication().FileOpen(capObj.fbxPath, True)
        capObj.UpdateTime("fbxOpenTime")

        # Detect Empty Full Fbx
        if mobu.FBApplication().FBXFileName == "":
            return "EXPORT ERROR: Full Fbx Is Empty - it needs to be resubmitted before full capture possible."

        # Run Capture Layout Steps
        LayoutTools.CaptureLayout()

        # Set Ranges
        endFrame = getattr(capObj, "endFrame", None)
        if endFrame is None:
            endFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
            capObj.UpdateTag("endFrame", str(endFrame))
        CamUtils.SetHardRanges(int(endFrame))

        # Plot Export Cam
        CamUtils.SetupExportCam()

    if capObj.captureType == "FULL":
        # Set Ranges in Empty FBX
        CamUtils.SetHardRanges(capObj.endFrame)

        # Load Cams in Empty FBX - via camKeyTasks
        camsLoaded = camKeyTasks.LoadKeyXmlIntoEmptyFbx(capObj.fbxPath)
        if not camsLoaded:
            return "EXPORT ERROR: Error loading cam data into empty Fbx during prep."

        # Create Transfer Cam Data - via camKeyTasks
        transCamData = camKeyTasks.CreateTransferCam()

        # Open Full FBX
        mobu.FBApplication().FileOpen(capObj.fbxPath, True)
        capObj.UpdateTime("fbxOpenTime")

        # Detect Empty Full Fbx
        if mobu.FBApplication().FBXFileName == "":
            return "EXPORT ERROR: Full Fbx Is Empty - it needs to be resubmitted before full capture possible."

        # Run Capture Layout Steps
        LayoutTools.CaptureLayout()

        # Set Ranges in Full FBX
        CamUtils.SetHardRanges(capObj.endFrame)

        # Load Transfer Cam Data - via camKeyTasks
        camKeyTasks.LoadTransferCam(transCamData)

    if capObj.captureType == "FAST":
        # Update fbxOpenTime
        capObj.UpdateTime("fbxOpenTime")

        # Set Ranges
        CamUtils.SetHardRanges(capObj.endFrame)

        # Load Cams in Empty FBX - via camKeyTasks
        camsLoaded = camKeyTasks.LoadKeyXmlIntoEmptyFbx(capObj.fbxPath)
        if not camsLoaded:
            return "EXPORT ERROR: Error loading cam data into empty Fbx during fast prep."

        # Setup Export Cam
        CamUtils.SetupExportCam()

        # Create Toybox
        LayoutTools.SetupToybox(showMessages=False)

        # Save CamOnly FBX - over the full fbx path
        FileTools.MakePathWriteable(capObj.fbxPath)
        mobu.FBApplication().FileSave(capObj.fbxPath)

        # Copy Cut Folder from Network
        FileTools.CopyFolderContents(dataCutsFolder, cutsPath)

    # Cleanup Bad Components
    # CamUtils.BadCompCleanup()

    # Set Thumbnail Frame
    thumbnailFrame = CamUtils.GetThumbnailFrame()
    capObj.UpdateTag("thumbnailFrame", str(thumbnailFrame))
    CamUtils.GoToFrameNumber(thumbnailFrame)

    # Open Export Window
    mobu.ShowToolByName("Rex Rage Cut-Scene Export")

    # Prep Success
    return "PREP SUCCESS"
