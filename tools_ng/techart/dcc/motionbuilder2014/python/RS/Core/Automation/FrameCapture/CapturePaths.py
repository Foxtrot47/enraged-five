import os


class FastPath(object):
    def __init__(self, fbxPath=None):
        fbxPath = fbxPath or ""
        pathParts = [part.lower() for part in fbxPath.split(os.sep)]
        self.fbxName = os.path.splitext(pathParts[-1])[0]

        # Get Project Paths
        self.project = "RDR3"
        if len(pathParts) > 2 and "gta5" in pathParts[1]:
            self.project = "GTA5"
        self.projectRoot = os.path.join("x:" + os.sep, self.project.lower())

        # Set Pack Root
        self.packRoot = self.projectRoot
        if self.project == "RDR3":
            self.packRoot = os.path.join(self.projectRoot, "dlc", "tuPacks", "tu_003", "mp003")
        elif self.project == "GTA5":
            artIndex = pathParts.index("art")
            self.packRoot = os.sep.join(pathParts[:artIndex])

        # Get Assets Root
        self.assetsRoot = os.path.join(self.packRoot, "assets")
        if self.project == "GTA5":
            self.assetsRoot += "_ng"
        elif self.project == "RDR3":
            self.assetsRoot = os.path.join(self.assetsRoot, "dev")

        # Get Cutscene/Audio Root
        self.cutsceneRoot = os.path.join(self.packRoot, "art", "animation", "cutscene", "!!scenes")
        self.audioRoot = os.path.join(self.projectRoot, "art", "animation", "resources", "audio")
        if self.project == "GTA5":
            self.audioRoot = os.path.join("x:" + os.sep, "gta5_dlc", "art", "animation", "resources", "audio")
        elif self.project == "RDR3":
            self.audioRoot = os.path.join(self.packRoot, "art", "animation", "resources", "audio")

        # Get Cuts/Ias Paths
        self.cutsPath = os.path.join(self.assetsRoot, "cuts", self.fbxName)
        self.cutListRoot = os.path.join(self.assetsRoot, "cuts", "!!Cutlists")
        self.mergeListRoot = os.path.join(self.assetsRoot, "cuts", "!!Mergelists")
        self.iasRoot = os.path.join(self.assetsRoot, "export", "anim", "anim_scenes", "cutscene")
        self.iasPath = os.path.join(self.iasRoot, "@{}.ias".format(self.fbxName))

        # Get Zip Paths
        self.zipRoot = os.path.join(self.assetsRoot, "export", "anim", "cutscene")
        missionName = self.fbxName
        if "!!scenes" in pathParts:
            missionIndex = pathParts.index("!!scenes") + 1
            missionName = pathParts[missionIndex]
        elif len(self.fbxName.split("_")) > 1 and len(self.fbxName.split("_")) > 2:
            missionName = self.fbxName.split("_")[0]
        self.zipFolder = os.path.join(self.zipRoot, missionName)
        self.zipPath = os.path.join(self.zipFolder, "{}.icd.zip".format(self.fbxName))

        # Get Tools Paths
        self.toolsRoot = os.path.join(self.projectRoot, "tools", "release")
        if self.project == "GTA5":
            self.toolsRoot = os.path.join(self.projectRoot, "tools_ng")
        self.frameDumpExePath = os.path.join(self.toolsRoot, "techart", "standalone", "FrameDumper", "FrameDumper.exe")

        # FFMPEG Paths - hardcoded for now, only supported in RDR
        self.ffmpegExePath = "x:\\rdr3\\tools\\release\\bin\\video\\ffmpeg\\3_0_1\\ffmpeg.exe"
        self.ffmpegFontPath = "x:\\rdr3\\tools\\release\\techart\\resources\\fonts\\rdr2lucid.ttf"

        # Get Mobu Exporter Paths
        self.mobuExePath = os.path.join(self.toolsRoot, "bin", "MotionbuilderLauncher", "MotionBuilderLauncher.exe")
        wildWestCaptureRoot = os.path.join("x:" + os.sep, "wildwest", "script", "python", "standalone", "CaptureRender")
        self.mobuScriptPath = os.path.join(wildWestCaptureRoot, "crapMBScript.py")
        self.mobuCapPath = os.path.join("x:" + os.sep, "mobuCap.txt")
        animPreProcessorName = "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess.meta"
        self.animPreProcessorPath = os.path.join(self.toolsRoot, "etc", "processors", animPreProcessorName)

        # Get Preview Path
        self.previewPath = os.path.join(self.packRoot, "build", "dev", "preview")
        if self.project == "GTA5":
            self.previewPath = "x:\\gta5\\build\\dev_ng\\preview"

        # Get Capture Paths
        self.captureRoot = os.path.join("N:" + os.sep, "RSGNYC", "Production", "CaptureRenders")
        self.capDataRoot = os.path.join(self.captureRoot, "CaptureData", self.project, self.fbxName)
        self.expJobQueue = os.path.join(self.captureRoot, "ExportQueue", self.project)
        self.capJobQueue = os.path.join(self.captureRoot, "CaptureQueue", self.project)
        self.expJobBackup = os.path.join(self.expJobQueue, "backups")
        self.capJobBackup = os.path.join(self.capJobQueue, "backups")

        # Get Range Paths
        self.trackerPath = os.path.join(self.projectRoot, "docs", "Production", "RDR3_MP_CAMERA_TRACKING.xlsm")
        self.pickleRoot = os.path.join(self.projectRoot, "art", "animation", "resources", "cameras", "Ranges")
        self.picklePath = os.path.join(self.pickleRoot, "mpPack01.pkl")
