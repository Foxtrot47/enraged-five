import os
import sys
import time
import traceback

from pyfbsdk import *

import RS.Globals
import RS.Perforce
import RS.Core.Reference.Manager
import RS.Core.Automation.Server
import RS.Core.Automation
import RS.Core.Camera.Lib
import RS.Core.Camera.PlotExportCamera
import RS.Core.EST.Edit
import RS.Core.Face.Lib


def updateReferencesByType( job, references ):
    for ref in references:
        if not ref.isLatestVersion():
            
            # Update status for the server to report.
            RS.Globals.gUlog.LogMessage( 'Updating "{0}" reference ({1})'.format( ref.assetType, ref.name ), context = 'Job ID {0}'.format( job.id ) )
            RS.Core.Automation.Server.updateJobStatus( job.id, 'Updating "{0}" reference ({1})'.format( ref.assetType, ref.name ) )
            
            try:
                ref.updateReference( quiet = True )
                
            except Exception:
                exc = traceback.format_exc()
                RS.Globals.gUlog.LogError( exc )
            
            if RS.Globals.gUlog.HasErrors:
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Errors reported for ({0})'.format( ref.name ), level = RS.Core.Automation.STATUS_LEVEL_FAILED )
                return False
                
            else:
                RS.Globals.gUlog.LogMessage( 'Updated {0} reference ({1})'.format( ref.assetType, ref.name ) )    
                return True

def updateReferences( job ):
    refMgr = RS.Core.Reference.Manager.RsReferenceManager()
    refMgr.collectSceneReferences()
    
    if updateReferencesByType( job, refMgr.characters ):
        if updateReferencesByType( job, refMgr.face ):
            if updateReferencesByType( job, refMgr.props ):
                if updateReferencesByType( job, refMgr.sets ):
                    return True
                    
    return False
    
def updateESTTracks( job ):
    RS.Globals.gUlog.LogMessage( 'Updating EST Tracks', context = 'Job ID {0}'.format( job.id )  )
    RS.Core.Automation.Server.updateJobStatus( job.id, 'Updating EST Tracks' )    

    try:
        RS.Core.EST.Edit.Est_edit.start( True )
        
    except:
        exc = traceback.format_exc()
        RS.Globals.gUlog.LogError( exc )
        
    if RS.Globals.gUlog.hasErrors:
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Errors reported during EST Track update.', level = RS.Core.Automation.STATUS_LEVEL_FAILED )
        return False
        
    else:
        RS.Globals.gUlog.LogMessage( 'Updated EST Tracks successfully.' )     
        return True
        
def updateCameras( job ):
    RS.Globals.gUlog.LogMessage( 'Updating cameras', context = 'Job ID {0}'.format( job.id )  )
    RS.Core.Automation.Server.updateJobStatus( job.id, 'Updating cameras' )
    
    try:
        RS.Core.Camera.Lib.camUpdate.rs_updateCameras( True )
        
        FBSystem().Scene.Evaluate()
        
        RS.Globals.gUlog.LogMessage( 'Plotting cameras' )
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Plotting cameras' )    

        RS.Core.Camera.PlotExportCamera.rs_PlotCameras.plotCameras( True )
        
    except Exception:
        exc = traceback.format_exc()
        RS.Globals.gUlog.LogError( exc )

    if RS.Globals.gUlog.GetErrors:
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Errors reported during camera update.', level = RS.Core.Automation.STATUS_LEVEL_FAILED )
        return False
        
    else:
        RS.Globals.gUlog.LogMessage( 'Updated cameras successfully.' )
        return True

def fixAmbientFaceRigs( job ):
    RS.Globals.gUlog.LogMessage( 'Fixing ambient face rigs', context = 'Job ID {0}'.format( job.id )  )
    RS.Core.Automation.Server.updateJobStatus( job.id, 'Fixing ambient face rigs' )
    
    try:
        ambientUINulls = RS.Core.Face.Lib.collectAllAmbientFacialRigRoots()
        
        if ambientUINulls:            
            for obj in ambientUINulls:
                try:
                    namespace, name = str( obj.LongName ).split( ':' )
                
                # Found multiple ':' characters, so deal with that case.
                except ValueError:
                    names = str( obj.LongName ).split( ':' )
                    name = names[ -1 ]
        
                    namespace = names[ 0 ]
                    namespaces = names[ 1 : -1 ]
                    
                    # Rebuild the namespace.
                    for ns in namespaces:
                        namespace += ':{0}'.format( ns )
                    
                RS.Core.Face.Lib.fixAmbientFacialRig( namespace, quiet = True )
                
    except:
        exc = traceback.format_exc()
        RS.Globals.gUlog.LogError( exc )
        
    if RS.Globals.gUlog.GetErrors:
        RS.Core.Automation.Server.updateJobStatus( job.id, 'Errors reported during ambient face rig fix.', level = RS.Core.Automation.STATUS_LEVEL_FAILED )
        return False
        
    else:
        RS.Globals.gUlog.LogMessage( 'Updated ambient face rigs successfully.' )
        return True    


def run( job ):
    '''
    Description:
        The entry point for the cutscene update automation system.
        
        Executes the supplied job.  Opens up a MotionBuilder file, and based on which modes are
        on the job, it will perform various actions.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    RS.Globals.gUlog.LogMessage( 'Job submitted by: {0}'.format( job.submittedBy ) , context = 'Job ID {0}'.format( job.id ) )
    RS.Globals.gUlog.LogMessage( 'Job machine: {0}'.format( job.machine ), context = 'Job ID {0}'.format( job.id )  )
    RS.Globals.gUlog.LogMessage( 'Job modes: {0}'.format( job.modes ), context = 'Job ID {0}'.format( job.id )  )
    RS.Globals.gUlog.LogMessage( 'Job filename: {0}'.format( job.filename ), context = 'Job ID {0}'.format( job.id )  )
    
    RS.Core.Automation.Server.updateJobStatus( job.id, 'Queueing job' )
    
    fileInfo = RS.Perforce.GetFileState( job.filename )
           
    if fileInfo:
        if RS.Perforce.IsOpenForEdit( fileInfo ):
            
            # Open the file.
            RS.Core.Automation.Server.updateJobStatus( job.id, 'Opening file' )
            
            FBApplication().FileOpen( job.filename )
            RS.Globals.gUlog.LogMessage( 'Opened file ({0}) at revision ({1}) of ({2})'.format( job.filename, fileInfo.HaveRevision, fileInfo.HeadRevision ) )
            
            # Perform actions on the open scene.
            updateReferencesSuccess = True
            updateCamerasSuccess = True
            updateESTTracksSuccess = True
            fixAmbientFaceRigsSuccess = True
            
            if RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES in job.modes:
                updateReferencesSuccess = updateReferences( job )
                
            if RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS in job.modes:
                updateCamerasSuccess = updateCameras( job )
                
            if RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS in job.modes:
                updateESTTracksSuccess = updateESTTracks( job )  
                
            if RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS in job.modes:
                fixAmbientFaceRigsSuccess = fixAmbientFaceRigs( job )
            
            # Pass if everything was successful.
            if updateESTTracksSuccess and updateCamerasSuccess and updateESTTracksSuccess and fixAmbientFaceRigsSuccess:
                
                # Save the file.
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Saving file' )
                FBApplication().FileSave( job.filename )
                
                RS.Globals.gUlog.LogMessage( 'Saved file ({0})'.format( job.filename ), context = 'Job ID {0}'.format( job.id ) )
                
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Shutting down MotionBuilder.', level = RS.Core.Automation.STATUS_LEVEL_COMPLETE )
                
            else:
                RS.Core.Automation.Server.updateJobStatus( job.id, 'Job encountered errors.  Please review the log.', level = RS.Core.Automation.STATUS_LEVEL_FAILED )                
    
        else:
            RS.Globals.gUlog.LogError( 'Could not check out the file ({0})!'.format( job.filename ) )
            RS.Core.Automation.Server.updateJobStatus( job.id, 'Could not process because the file could not be checked out!', level = RS.Core.Automation.STATUS_LEVEL_FAILED )
            
    else:
        RS.Globals.gUlog.LogError( 'File does not exist in Perforce! ({0})!'.format( job.filename ) )
        RS.Core.Automation.Server.updateJobStatus( job.id, 'File does not exist in Perforce!', level = RS.Core.Automation.STATUS_LEVEL_FAILED )        
        
    FBApplication().FileExit()