"""
Description:
    A QT gui for viewing, sorting, and cancelling capture jobs.  It should probably be
    broken into multiple files and moved to the Tools/UI folder eventually, but works
    well enough now for the guys to start using it to generate some feedback.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import sys
import time
import datetime
import getpass
from PySide import QtCore, QtGui

# Setup Sys Path for Mobu Python Tools
pathParts = os.path.realpath(__file__).split(os.path.sep)
mobuPythonPath = os.path.sep.join(pathParts[:pathParts.index("RS")])
sys.path.insert(0, mobuPythonPath)

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
import RS.Core.Automation.FrameCapture.JobDbTools as JobDbTools
import RS.Core.Automation.FrameCapture.ExportDataTools as ExportDataTools
import RS.Core.Server.RemoteServer as RemoteServer

# Constants
ADMINUSERS = ["bbuck", "wluna"]
ICONPATH = os.path.join(os.path.split(mobuPythonPath)[0], "images", "Rockstar_Games.png")
STATUSVALUES = ["Awaiting Export", "Exporting", "Awaiting Capture", "Capturing", "Finalizing", "Complete"]
MONITORSTYLESHEET = """
QTreeView { selection-background-color: navy;
            background-color: rgb(42,42,42);
            alternate-background-color: rgb(46,46,46);
            color: white; }

QTreeView::item:hover { background-color:grey; color:white; }
QTreeView::item:selected { background-color:darkBlue; color:white; }
QHeaderView::section { background-color:grey; }

QScrollBar::vertical { background: grey; }
QCheckBox { color: white; }
QLabel { color: white; }

QMessageBox { background-color: rgb(68,68,68); }
"""

class Worker(QtCore.QThread):
    """
    An qThread worker object that connects to the
    database and returns a list of all jobs on it.
    """
    Update = QtCore.Signal(list)

    def __init__(self):
        super(Worker, self).__init__()

    def run(self):
        while True:
            time.sleep(5)
            jobList = JobDbTools.ReadDbJobs()
            jobList = sorted(jobList, key=lambda x: datetime.datetime.strptime(x.Date, "%Y-%m-%d %H:%M:%S.%f0"))
            self.Update.emit(jobList)

    def stop(self):
        self.terminate()


class CapJobModelItem(baseModelItem.BaseModelItem):
    """
    A baseModelItem object that models a single job in the database.
    """
    def __init__(self, jobRow, parent=None):
        super(CapJobModelItem, self).__init__(parent=parent)
        self.fbxName = jobRow.Scene
        self.status = STATUSVALUES[jobRow.Status]
        if jobRow.IsCanceled:
            self.status = "Canceled"
        self.userName = jobRow.UserName
        self.date = "".join(jobRow.Date.split(".")[:-1])
        self.jobId = jobRow.JobID

    def data(self, index, role=QtCore.Qt.DisplayRole, column=None):
        if not column:
            column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.fbxName
            if column == 1:
                return self.status
            if column == 2:
                return self.userName
            if column == 3:
                return self.date
            if column == 4:
                return self.jobId
        if role == QtCore.Qt.BackgroundRole:
            if self.status == "Awaiting Export":
                return QtGui.QColor(0, 136, 122)
            if self.status == "Awaiting Capture":
                return QtGui.QColor(0, 116, 136)
            if self.status == "Exporting":
                return QtGui.QColor(150, 140, 0)
            if self.status == "Capturing":
                return QtGui.QColor(200, 105, 0)
            if self.status == "Finalizing":
                return QtGui.QColor(160, 100, 0)
            if self.status == "Complete":
                return QtGui.QColor(0, 80, 0)
            if self.status == "Canceled":
                return QtGui.QColor(150, 0, 0)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        column = index.column()
        if role == QtCore.Qt.EditRole:
            if column == 0:
                self.fbxName = value
                return True
            if column == 1:
                self.status = value
                return True
            if column == 2:
                self.userName = value
                return True
            if column == 3:
                self.date = value
                return True
            if column == 4:
                self.jobId = value
                return True
        return False


class CapJobModel(baseModel.BaseModel):
    """
    Our main baseModel object for storing all JobModelItem objects,
    controlling the worker, and adding / updating items.
    """
    def __init__(self, parent=None):
        super(CapJobModel, self).__init__(parent=parent)
        self.Worker = Worker()
        self.Worker.Update.connect(self.RefreshJobs)
        self.Worker.start()

    def getHeadings(self):
        return ["FBX", "Status", "User", "Date", "JobID"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def setupModelData(self, parent):
        jobList = JobDbTools.ReadDbJobs()
        jobList = sorted(jobList, key=lambda x: datetime.datetime.strptime(x.Date, "%Y-%m-%d %H:%M:%S.%f0"))
        if not jobList:
            print "Couldn't connect to Database"
            return
        for jobRow in jobList:
            parent.appendChild(CapJobModelItem(jobRow, parent=parent))

    def RefreshJobs(self, jobList):
        for jobIndex, jobRow in enumerate(jobList):
            if jobIndex >= len(self.rootItem.childItems):
                # Add A New Job - if our jobIndex >= the number of jobItems
                newJobItem = CapJobModelItem(jobRow, parent=self.rootItem)
                self.rootItem.appendChild(newJobItem)
                modelIndex = self.index(0, 0, self.index(jobIndex, 0))
                self.beginInsertRows(modelIndex, jobIndex, jobIndex)
                self.insertRows(0, 0, modelIndex)
                self.endInsertRows()
            else:
                # Job Already Exists - check status and update if changed
                newStatus = STATUSVALUES[jobRow.Status]
                if jobRow.IsCanceled:
                    newStatus = "Canceled"
                jobItem = self.rootItem.childItems[jobIndex]
                if jobItem.status != newStatus:
                    jobItem.setData(self.createIndex(jobIndex, 1), newStatus)
                    self.dataChanged.emit(self.createIndex(jobIndex, 0), self.createIndex(jobIndex, 4))


class CategoriesFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model to hide old jobs and allow sorting via headings.
    """
    def __init__(self, parent=None):
        super(CategoriesFilterProxyModel, self).__init__(parent=parent)
        self.setDynamicSortFilter(True)
        self.hideJobs = True

    def filterAcceptsRow(self, row, parent=None):
        if self.hideJobs:
            if self.sourceModel().rowCount() - row > 50:
                return False

        return True

    def FilterJobs(self, newVal):
        self.hideJobs = newVal
        self.reset()


class CaptureMonitor(QtGui.QWidget):
    """
    Main widget showing a list of all jobs, a filter recent checkbox,
    and a cancel capture button. Populated by the capJobModel via proxyModel.
    """
    def __init__(self, parent=None):
        super(CaptureMonitor, self).__init__(parent=parent)
        self.selectedJob = None
        self.SetupUi()

    def SetupUi(self):
        # Setup Main and Proxy Models
        self._mainModel = CapJobModel()
        self._proxyModel = CategoriesFilterProxyModel()
        self._proxyModel.setSourceModel(self._mainModel)
        self._proxyModel.sort(3, QtCore.Qt.DescendingOrder)

        # Setup Job Tree Layout
        self._treeView = QtGui.QTreeView()
        self._treeView.setModel(self._proxyModel)
        self._treeView.setAlternatingRowColors(True)
        self._treeView.setColumnWidth(0, 160)
        self._treeView.setColumnWidth(1, 100)
        self._treeView.setColumnWidth(2, 100)
        self._treeView.setIndentation(0)
        self._treeView.setColumnHidden(4, True)
        self._treeView.expandAll()
        self._treeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        # Button Box Layout
        buttonBox = QtGui.QHBoxLayout()

        # Filter Jobs Checkbox
        filterCheckBox = QtGui.QCheckBox("Recent Captures Only")
        filterCheckBox.toggle()
        filterCheckBox.stateChanged.connect(self.FilterJobList)
        buttonBox.addWidget(filterCheckBox)
        buttonBox.addStretch(1)

        # Sync Capture Button
        syncButton = QtGui.QPushButton("Sync")
        syncButton.setToolTip("Syncs the selected capture's preview data.")
        syncButton.setFixedWidth(60)
        syncButton.clicked.connect(self.SyncPreview)
        buttonBox.addWidget(syncButton)

        # Flush Preview Button
        syncButton = QtGui.QPushButton("Flush")
        syncButton.setToolTip("Deletes all preview files from your machine.")
        syncButton.setFixedWidth(60)
        syncButton.clicked.connect(self.FlushPreview)
        buttonBox.addWidget(syncButton)

        # Cancel Job Button
        cancelButton = QtGui.QPushButton("Cancel")
        cancelButton.setToolTip("Cancels the selected capture.")
        cancelButton.setFixedWidth(60)
        cancelButton.clicked.connect(self.CancelJob)
        buttonBox.addWidget(cancelButton)

        # Complete Job Button - for admins only
        completeButton = QtGui.QPushButton("Complete")
        completeButton.setFixedWidth(60)
        completeButton.clicked.connect(self.CompleteJob)
        if getpass.getuser() in ADMINUSERS:
            buttonBox.addWidget(completeButton)

        # Setup Main Layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self._treeView)
        layout.addLayout(buttonBox)
        self.setLayout(layout)
        self.setGeometry(500, 300, 510, 416)

        # Header Selection Handling
        headerSelection = self._treeView.header()
        headerSelection.setClickable(True)
        headerSelection.sectionClicked.connect(self.SwitchHeader)

        # Job Selection Handling
        selectionMode = self._treeView.selectionModel()
        selectionMode.selectionChanged.connect(self.GetSelectedJob)

        # Double Click Setup - Opens selected job's latest preview video
        self._treeView.doubleClicked.connect(self.OpenVideo)

    def GetSelectedJob(self):
        self.selectedJob = self._treeView.currentIndex()

    def SwitchHeader(self):
        self._treeView.header().setSortIndicatorShown(True)
        selectedColumn = self._treeView.header().sortIndicatorSection()
        selectedOrder = self._treeView.header().sortIndicatorOrder()
        self._proxyModel.sort(selectedColumn, selectedOrder)

    def FilterJobList(self, hideJobsBool):
        self._proxyModel.FilterJobs(hideJobsBool)

    def CancelJob(self):
        if self.selectedJob:
            jobStatus = self._proxyModel.index(self.selectedJob.row(), 1).data()
            if jobStatus in ["Complete", "Canceled", "Finalizing"]:
                self.PopUpMessage("Job {} - cannot cancel.".format(jobStatus))
            else:
                jobId = self._proxyModel.index(self.selectedJob.row(), 4).data()
                JobDbTools.CancelDbJob(jobId)
                self._mainModel.reset()

    def CompleteJob(self):
        if self.selectedJob:
            jobId = self._proxyModel.index(self.selectedJob.row(), 4).data()
            JobDbTools.SubmitJobUpdate(jobId, 5, False)
            self._mainModel.reset()

    def SyncPreview(self):
        if self.selectedJob:
            jobName = self._proxyModel.index(self.selectedJob.row(), 0).data()
            jobStatus = self._proxyModel.index(self.selectedJob.row(), 1).data()
            if jobStatus not in ["Capturing", "Finalizing", "Complete"]:
                self.PopUpMessage("Job {} - cannot sync data.".format(jobStatus))
            else:
                syncStatus = ExportDataTools.SyncPreview(jobName)
                if syncStatus is not True:
                    self.PopUpMessage("Error: Export data could not be synced.")

    def FlushPreview(self):
        ExportDataTools.FlushPreview()

    def PopUpMessage(self, topText, bottomText=None, buttonText=None):
        msgBox = QtGui.QMessageBox()
        mainWidgetPosition = self.geometry().center().toTuple()
        msgBox.setGeometry(mainWidgetPosition[0]-100, mainWidgetPosition[1]-50, 300, 300)
        msgBox.setWindowIcon(QtGui.QIcon(ICONPATH))
        msgBox.setWindowTitle("Warning")
        msgBox.setStyleSheet(MONITORSTYLESHEET)
        msgBox.setText("{}      ".format(topText))
        if bottomText:
            msgBox.setInformativeText(bottomText)
        if buttonText:
            msgBox.addButton(buttonText, msgBox.ButtonRole.AcceptRole)
        msgBox.exec_()

    def OpenVideo(self):
        jobName = self._proxyModel.index(self.selectedJob.row(), 0).data()
        projectName = os.environ.get("RS_PROJECT")
        dbVideoFolder = "\\\\NYCW-MHB-SQL\\Captures_NYC\\{0}\\{1}".format(projectName, jobName)
        if os.path.exists(dbVideoFolder):
            videoRoot = "http://NYCW-MHB-SQL/Captures_NYC/{0}/{1}".format(projectName, jobName)
            capRev = max([int(capRev) for capRev in os.listdir(dbVideoFolder)])
            videoPath = "{0}/{1}/{2}.mp4".format(videoRoot, capRev, jobName)
            os.startfile(videoPath)


def StartMonitor():
    """
    Function for creating the app widget and populating it with the CaptureMonitor widget.
    """
    app = QtGui.QApplication(sys.argv)
    wid = CaptureMonitor()

    wid.setWindowTitle("Capture Monitor")
    wid.setWindowIcon(QtGui.QIcon(ICONPATH))
    pal = QtGui.QPalette()
    pal.setColor(wid.backgroundRole(), QtGui.QColor(68, 68, 68))
    wid.setPalette(pal)
    wid.setStyleSheet(MONITORSTYLESHEET)

    wid.show()
    app.exec_()
    wid._mainModel.Worker.stop()

if __name__ == "__main__":
    """
    Launch behavior when run via command line.
    """
    StartMonitor()

if __name__ == "CaptureMonitor":
    """
    Launch behavior when run via Mobu server.
    Sends a command back to Mobu on close to kill the server.
    """
    StartMonitor()
    stopCommand = ("import RS.Core.Automation.FrameCapture.LaunchMonitor as LaunchMonitor;"
                   "LaunchMonitor.Stop()")
    RemoteServer.ExecuteInMotionBuilder(stopCommand)