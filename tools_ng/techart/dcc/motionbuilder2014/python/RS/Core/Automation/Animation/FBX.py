"""
Stores information about Ingame Animation FBX files into the Database

from RS.Core.Automation.Animation import FBX
FBX.StoreAnimData()
"""
import os
import re
import logging
import datetime
from xml.etree import cElementTree as xml

import pyfbsdk as mobu

from RS import Perforce, ProjectData, Config
from RS.Core import DatabaseConnection
from RS.Utils.Scene import Close
from RS.Utils.Logging import Universal
from RS.Core.Animation import Lib
from RS.Core.Automation.Animation import Render
from RS.Core.Animation.DataManager import Clip

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.CameraToolBox.PyCore.Decorators.memorize import memoized

LOG = Universal.UniversalLog("Motionbuilder_EST", False, True)
ANIMATION_DATABASE = DatabaseConnection.DatabaseConnection('{SQL Server}', 'NYCW-MHB-SQL', 'AnimationStats_RDR3', LOG)
CUTSCENE_DATABASE = DatabaseConnection.DatabaseConnection('{SQL Server}', 'NYCW-MHB-SQL', 'CutsceneStats_RDR3', LOG)

logging.basicConfig(filename=r"X:\render.log", level=logging.DEBUG, format='%(levelname)s %(asctime)s : %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


SKELETONS = {os.path.basename(eachFile): os.path.join(root, eachFile)
             for root, folders, files in os.walk(ProjectData.data.GetSkelFilesDirectory())
             for eachFile in files}


def AreSubmissionTimesDifferent(path, headRevisionTime):
    """
    Checks that the submission time being passed is not the same as the one on the database

    Arguments:
        path (string): path of the anim or fbx file whose last updated time needs to be checked
        headRevisionTime (DateTime): the time from perforce; the DateTime object must be from C#

    Return:
        boolean

    """
    table = ("FBX", "Anims")[path.endswith(".anim")]
    dateLastUpdated = ANIMATION_DATABASE.ExecuteQuery(("SELECT LastUpdated FROM {table} WHERE Path = '{path}' "
                                                       "OR DepotPath = '{path}'").format(path=path, table=table))

    if dateLastUpdated:
        databaseSubmissionDate = dateLastUpdated[0][0].strftime("%m/%d/%Y %I:%M:%S %p")

        date, time = headRevisionTime.ToString().strip().split(" ", 1)
        month, day, year = date.split("/")
        hour, minute, seconds = time.split(":")
        seconds, amPm = seconds.strip().split(" ")

        currentSubmissionsDate = "{0:02}/{1:02}/{2:02} {3:02}:{4:02}:{5:02} {6}".format(
            int(month), int(day), int(year), int(hour), int(minute), int(seconds), amPm)

        return currentSubmissionsDate != databaseSubmissionDate

    return True


@memoized
def GetUserIDFromChangelist(changelist):
    """
    Gets the username, fullname and email address of the user that submitted the provided change list and updates
    the database with their info if their info isn't already on it.

    When this method is ran, the return value is cached based on the input parameter. So when the same parameter value
    is used, the last result is returned without re-evalutaing the code for speed.

    Arguments:
        changelist (int): the changelist number

    Return:
        tuple(int)

    """

    description = Perforce.Run("describe", [str(changelist)])
    userData = Perforce.GetUserData(description.Records[0]["user"])

    user = userData.get("User", description.Records[0]["user"]).replace("'", "''")
    fullname = userData.get("FullName", "").replace("'", "''")
    email = userData.get("Email", "").replace("'", "''")

    # Add user data to the database if it is missing
    return CUTSCENE_DATABASE.ExecuteQuery("exec dbo.AddUser '{user}', '{fullname}', '{email}'".format(
        user=user, fullname=fullname, email=email))[0][0]


@memoized
def ExistsOnPerforce(path, bool):
    """
    Check if the given path exists on perforce
    """
    recordSet = Perforce.Run("files", [path.replace("@", "%40")])

    if bool:
        return len(recordSet.Records)

    return filter(None, [{key: record[key] for key in record.Fields.Keys} for record in recordSet.Records])


def GetClipData(path):
    """
    Uses the anim file to figure out the associated clip file which contains the data for the fbx file

    Arguments:
        path (string): path to the anim file

    Return:
        string: path to the associated fbx file
    """
    clipPath = path.replace(".anim", ".clip")
    Perforce.Sync(clipPath.replace("@", "%40"), force=True)

    clip = Clip.ClipObject(clipPath)
    if not hasattr(clip, "properties"):
        return
    return clip


def GetSubmissionsByDate(path, fileType="fbx", date=None):
    """
    Gets all the submissions on the given data that happened in the given path, filtered by file type

    Arguments:
        path (string): path to directory where the files to check live
        fileType (string): file extensions to filter by
        date (string): date to check submissions from. Date format should be year/month/day.

    """

    if not path.endswith("\\"):
        path = "{}\\".format(path)

    if date is None:
        date = datetime.date.today()
    date = str(date).replace("-", "/")

    recordSet = Perforce.Run("files",
                             ["{path}...{fileType}@{date}:0:00,@{date}:23:59".format(path=path,
                                                                                     fileType=fileType, date=date)])

    return [{key: record[key] for key in record.Fields.Keys} for record in recordSet.Records]


def StoreAnimDataFromDirectories(root, paths, force=False):
    """
    Store anim files to the database from the given paths

    Arguments:
        paths (list[string, etc.]): list of paths to store
        force (boolean): force anim to be stored even if it is already on the database
    """
    for path in paths:
        for record in Perforce.Run("files", ["{path}...anim".format(path=path.replace("@", "%40"))]):
            StoreAnimData(root, record, force=force)


def StoreAllAnimData():
    """ Stores all the anim data on P4 """
    for project, directory in ProjectData.data.GetAnimationDirectories():
        for record in Perforce.Run("files", ["{path}...anim".format(directory)]):
            StoreAnimData(directory, record)


def StoreRecentAnimData(close=False, force=False):
    """ Queries the latest submits from Perforce and updates the database with their info """

    today = datetime.date.today()
    yesterday = str(today - datetime.timedelta(1)).replace("-", "/")
    #yesterday = "2015/10/02"

    for project, directory in ProjectData.data.GetAnimationDirectories():
        for record in GetSubmissionsByDate(directory, "anim", yesterday):
            StoreAnimData(directory, record, force=force)

    if close:
        with open("x:\\ping.py", "w") as _file:
            _file.write("DONE = True\n")
        Close()


def StoreCoreAnimData(force=True):
    """ Stores the anim files listed on the core.xml file of the Anim2Fbx tool """
    path = os.path.join(Config.Script.Path.RockstarRoot, "Tools", "Animation", "Anim2Fbx", "Sets", "Core.xml")
    #path = r"X:\rdr3\tools\techart\dcc\motionbuilder2014\python\RS\Tools\Animation\Anim2Fbx\Sets\Core.xml"
    tree = xml.parse(path)
    root = tree.getroot()
    regexList = ["{}|".format(directory.replace("\\", "\\\\")) for project, directory in
                 ProjectData.data.GetAnimationDirectories()]
    directoryRegex = "".join(regexList)[:-1]

    for element in GetFileElements(root):
        path = element.text.strip()
        for record in Perforce.Run("files", [path.replace("@", "%40")]):
            StoreAnimData(re.search(directoryRegex, directory).group(), record, isCorePose=True, force=force)


def GetFileElements(element):
    """
    Recursively gets all the child elements whose tag is file

    Arguments:
        element (xml.etree.cElementTree.Element): element whose hierarchy should be checked

    Return:
        list

    """
    files = []
    if element.tag == "File":
        files = [element]

    for subelement in element:
        files.extend(GetFileElements(subelement))

    return files


def StoreAnimData(directory, record, force=False, isCorePose=False):
    """
    Stores the data of the anim files in the Perforce Record to the AnimDatabase

    Arguments:
        directory (string): animation directory
        record (Perforce.P4Record): P4 Record

    """

    changelist = record["change"]
    perforcePath = record["depotFile"]
    revision = record["rev"]

    basename = os.path.splitext(os.path.basename(perforcePath.replace("%40", "@")))[0]
    userId = GetUserIDFromChangelist(changelist)

    # Sync File
    Perforce.Sync(perforcePath, force=True)

    fileState = Perforce.GetFileState(perforcePath)
    animpath = fileState.ClientFilename.replace("%40", "@")
    date = fileState.HeadChangelistTime

    skip = not AreSubmissionTimesDifferent(animpath, date) if not force else False

    if skip:
        logging.debug(" --- SKIPPING {} ---".format(animpath))
        return

    logging.debug("Storing Anim Data: {}".format(animpath))

    sourceFbx = None
    skeletonPath = None
    clip = GetClipData(animpath)
    if clip and hasattr(clip, "properties"):
        sourceFbx = clip.properties.GetChildByName('FBXFile_DO_NOT_RESOURCE')
        sourceFbx = sourceFbx[0].string.replace("$(art)", Config.Project.Path.Art) if sourceFbx else None

        skeletonName = clip.properties.GetChildByName("Skelfile_DO_NOT_RESOURCE")
        skeletonName = skeletonName[0].string if skeletonName else None
        skeletonPath = SKELETONS.get(skeletonName, None)

    fbxId = 0
    skeletonId = 0
    if sourceFbx:
        fbxId = StoreFbxData(sourceFbx, force=force)
        skeletonId = StoreSkeletonData(clip.Path)

    renderResults = Render.CaptureAnimation(path=r"\\nycw-MHB-sql\Captures_Anim",
                                            animPaths=[animpath.replace("%40", "@")],
                                            rootPath=directory.replace("%40", "@"),
                                            skeletonPath=skeletonPath)

    renderPath, renderResult = renderResults[0]
    frameRange = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
    mover = mobu.FBFindModelByLabelName("CameraPivot")
    poseMoviePath = None

    if isCorePose and mover:
        currentTake = mobu.FBSystem().CurrentTake
        take = currentTake.CopyTake("TurnTable")

        # CopyTake copies the animation data too
        # We need to get rid of them.
        take.ClearAllProperties(False)
        mobu.FBSystem().CurrentTake = take

        # Renders a Turn Table of the first pose
        poseMoviePath = renderPath.replace(".mov", "_TurnTable.mov")
        take.LocalTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0, 0, 0, 0), mobu.FBTime(0, 0, 0, 96))

        Lib.PlotOnFrame(mover, properties=["Rotation"], frame=96)

        mobu.FBPlayerControl().GotoStart()
        mover.PropertyList.Find("Lcl Rotation").Data = mover.PropertyList.Find("Lcl Rotation").Data + mobu.FBVector3d(0, 0, 360)
        Lib.PlotOnFrame(mover, properties=["Rotation"], frame=0)

        Render.RenderTake(take, poseMoviePath)

    query = ("exec dbo.UpdateAnimData '{name}', '{path}', '{depotpath}', {fbx}, {frames}"
             ",{user}, {revision}, {changelist}, '{date}', '{moviepath}', "
             "'{renderresult}', {skeletonId}, {iscorepose}, '{coremoviepath}'").format(
    name=basename, path=animpath, depotpath=perforcePath.replace("%40", "@"), fbx=fbxId, frames=frameRange,
    user=userId, revision=revision, changelist=changelist, date=date, moviepath=renderPath,
    renderresult=int(renderResult), skeletonId=skeletonId, iscorepose=int(isCorePose), coremoviepath=poseMoviePath)

    ANIMATION_DATABASE.ExecuteQuery(query)


@memoized
def StoreFbxData(path, force=False):
    """
    Stores FBX Data to the database.

    When this method is ran, the return value is cached based on the input parameter. So when the same parameter value
    is used, the last result is returned without re-evalutaing the code for speed.

    Arguments:
        path (string): the path to the fbx

    """
    Perforce.Sync(path, force=False)
    fileState = Perforce.GetFileState(path)

    if not fileState:
        return 0

    name = os.path.splitext(os.path.basename(path))[0]
    localpath = fileState.ClientFilename.replace("%40", "@")
    depotpath = fileState.DepotFilename.replace("%40", "@")
    changelist = fileState.HeadChange
    revision = fileState.HeadRevision
    date = fileState.HeadChangelistTime
    userId = GetUserIDFromChangelist(changelist)

    storeReferenceData = AreSubmissionTimesDifferent(path, date) if not force else force

    query = ("exec dbo.UpdateFbxData '{name}', '{localpath}', '{depotpath}', {userId}, {changelist}, {revision}, "
             "'{date}'").format(name=name, localpath=localpath, depotpath=depotpath, userId=userId,
                                changelist=changelist, revision=revision, date=date)

    fbxId = ANIMATION_DATABASE.ExecuteQuery(query)[0][0]

    if storeReferenceData:
        StoreReferenceData(path, fbxId, force=force)

    # Open the fbx file
    return fbxId


@memoized
def StoreReferenceData(fbxPath, fbxId=None):
    """
    Stores the references from an FBX file into the database

    Arguments:
        fbxPath (string): path to the fbx file with references

    Return:
        list, list of the paths to the refernces

    """

    if fbxId is None:
        fbxId = ANIMATION_DATABASE.ExecuteQuery(
            "SELECT Id FROM FBX WHERE Path = '{path}' OR DepotPath = '{path}'".format(path=fbxPath))
        if not fbxId:
            return []
        fbxId = fbxId[0][0]

    logging.debug("Storing Reference Data: {}".format(fbxPath))

    mobu.FBApplication().FileOpen(str(fbxPath))

    manager = Manager()
    manager.SetSilent(True)

    # Get list of references
    references = [(os.path.splitext(os.path.basename(reference.Path))[0], reference.Path)
                  for reference in Manager().GetReferenceListAll()]

    # remove duplicates
    references = list(set(references))
    references.sort()

    for name, path in references:

        referenceId = ANIMATION_DATABASE.ExecuteQuery("exec dbo.AddReference '{name}', '{path}'".format(name=name.strip(),
                                                                                                        path=path.strip())
                                                      )[0][0]
        ANIMATION_DATABASE.ExecuteQuery("exec dbo.MapReference {referenceId}, {fbxId}  ".format(
            referenceId=referenceId, fbxId=fbxId))

    mobu.FBApplication().FileNew()

    return references


@memoized
def StoreSkeletonData(clipPath):
    """
    This stores skeleton data

    Arguments:
        clipPath (string): path to clip file

    """

    clip = GetClipData(clipPath)
    if not clip:
        return

    skeletonName = clip.properties.GetChildByName("Skelfile_DO_NOT_RESOURCE")[0].string
    skeletonPath = SKELETONS.get(skeletonName, SKELETONS["biped.skel"])

    sourceFbxPath = clip.properties.GetChildByName('FBXFile_DO_NOT_RESOURCE')[0].string
    sourceFbxPath = sourceFbxPath.replace("$(art)", Config.Project.Path.Art)

    fileState = Perforce.GetFileState(skeletonPath.replace("@", "%40"))
    if not fileState:
        return 0

    depotPath = fileState.DepotFilename.replace("%40", "@")
    references = StoreReferenceData(sourceFbxPath) or [("", "")]
    references = [path for name, path in references if "ingame" not in path or "cutscene" not in path]

    skeletonId = ANIMATION_DATABASE.ExecuteQuery("exec dbo.AddSkeleton '{name}', '{path}', '{depotPath}', '{fbxPath}'  ".format(
        name=skeletonName, path=skeletonPath, depotPath=depotPath, fbxPath=references[0]
    ))
    logging.debug("Storing Skeleton: {}".format(references[0]))

    return skeletonId[0][0]
