"""
Description:
    A few miscellaneous functions for working with export data from the network capture folders,
    and setting up a user's machine for playing it back (though I ended up not needing most of it).
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import copy
import FileTools
import CapturePaths
from xml.etree import ElementTree as ET

def VerifyContentXml(projectRoot, fbxName):
    """
    ! WARNING: The module is old and has been replace by a new version in RS.Utils.ContentUpdate
    Not sure if it would still work properly now or not. -BlakeBuck 10/15/15

    Module to check an RPF exists in a user's contentXml, and if it doesn't, add it automatically.
    Wrote this to work with the capture monitor's preview sync, and ended up not needing it. Oh well.
    Arguments:
        projectRoot: A string of the project root path.
        fbxName: A string of the fbxName to detect the RPF name from.
    Returns:
        A bool - false if the content xml already contained the rpf, True if and update was needed
        Or a string - containing an error message.
    """
    # Set contentXmlPath
    contentXmlPath = os.path.join(projectRoot, "build", "dev", "common", "data", "common_cutscene.meta")
    if not os.path.exists(contentXmlPath):
        return "Error: Content Xml not found."

    # Get Root Node
    tree = ET.parse(contentXmlPath)
    rootNode = tree.getroot()

    # Check Content Xml for Cutscene Rpf
    missionName = fbxName.split("_")[0].lower()
    rpfText = "platform:/anim/cutscene/cuts_{}.rpf".format(missionName)
    foundItems = [itemNode for itemNode in rootNode.findall("./Item/filename") if itemNode.text == rpfText]

    # Rpf Found - Return True and Exit
    if len(foundItems) > 0:
        print "matching rpf found in content xml"
        return True

    # Rpf Missing - Copy Last Item Node, Update Filename, and Add to Root
    newItemNode = copy.deepcopy(rootNode.findall("Item")[-1])
    newFilenameNode = newItemNode.find("filename")
    newFilenameNode.text = rpfText
    rootNode.append(newItemNode)

    # Save out Updated Content Xml
    FileTools.MakeFileWriteable(contentXmlPath)
    tree.write(contentXmlPath, encoding="UTF-8")
    FileTools.MakeFileReadOnly(contentXmlPath)

    # Rpf Added - Return False
    return False


def VerifyGameConfig(projectRoot):
    """
    Module to check a user's gameconfig is set to 30000, and if it isn't, up it automatically.
    Wrote this to work with the capture monitor's preview sync, and ended up not needing it. Oh well.
    Arguments:
        projectRoot: A string of the project root path.
    Returns:
        An int - 0 if gameConfig couldn't be found, 1 if no update needed, 2 if config was updated
    """
    # Set configAltered and contentXmlPath
    configAltered = False
    gameConfigPath = os.path.join(projectRoot, "build", "dev", "common", "data", "gameconfig.xml")

    # No GameConfig Found - Return 0
    if not os.path.exists(gameConfigPath):
        return 0

    # Get All PoolSize Nodes
    tree = ET.parse(gameConfigPath)
    clipStoreNodes = tree.getroot().findall(".//*[PoolName='ClipStore']/PoolSize")

    # Check PoolSize Values - and up their size if less than 30000
    for eachNode in clipStoreNodes:
        if int(eachNode.attrib["value"]) < 30000:
            eachNode.attrib["value"] = "30000"
            configAltered = True

    # GameConfig Unchanged - Return 1
    if configAltered is False:
        return 1

    # Save out Updated Game Config
    FileTools.MakeFileWriteable(gameConfigPath)
    tree.write(gameConfigPath, encoding="UTF-8")
    FileTools.MakeFileReadOnly(gameConfigPath)

    # Game Config Updated - Return 2
    return 2


def SyncPreview(fbxName):
    """
    Syncs preview files from the network capture data folder.
    Arguments:
        fbxName: A string of the fbxName to detect the RPF name from.
    Returns:
        Bool - True for successful sync, None otherwise.
    """
    localPreviewFolder = CapturePaths.previewPath
    capDataRoot = CapturePaths.GetCapDataRoot(fbxName)
    dataPreviewFolder = os.path.join(capDataRoot, "PreviewFiles")
    if os.path.exists(dataPreviewFolder) and len(os.listdir(dataPreviewFolder)) > 0:
        FileTools.CopyFolderContents(dataPreviewFolder, localPreviewFolder)
        return True


def FlushPreview():
    """
    Deletes all preview files from a user's local preview folder.
    Arguments:
        projectRoot: A string of the project root path to determine preview path.
    """
    localPreviewFolder = CapturePaths.previewPath
    FileTools.DeleteFolderContents(localPreviewFolder)
