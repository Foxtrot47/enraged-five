from RS.Core.Server import Client as rsServer
import RS.Core.Automation.FrameCapture.CaptureMonitor as CaptureMonitor
reload(CaptureMonitor)
# rsServer.VISIBLE = True

def Run():
    connection = rsServer.Connection("CaptureMonitor")
    connection.Sync(CaptureMonitor)
    connection.Thread("import CaptureMonitor")

def Stop():
    connection = rsServer.Connection("CaptureMonitor")
    connection.Run("Close()")