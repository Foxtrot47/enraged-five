"""
Description:
    A module for getting various data from Watchstar during Layout.
    Originally connected directly to the database, but now uses Geoff's AnimData.Context

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
from RS.Core.AnimData import Context

PROJECTDICT = {"RDR3": "Redemption2", "GTA5_DLC": "GTA 5 DLC"}


def GetWatchstarTrial(fbxPath, takeName):
    """
    Gets a Watchstar Trial object using an FBX path. We've put in a request for a new stored
    procedure in Watchstar that would get a trial by FBX path and eliminate this string search stuff.
    Arguments:
        fbxPath (string): the local path of the FBX to search for
    Returns:
        (AnimData.Context.MotionTrialBase): a watchstar trial object if found
    """
    # Detect Trial Via FbxPath
    p4Path = "/{}".format(fbxPath.replace("\\", "/")[2:])
    try:
        trials = Context.animData.getTrialsByFbxPath(p4Path)
    except:
        trials = None
    if trials:
        return trials[0]

    # Detect Composite Trial Via FbxPath
    try:
        compTrials = Context.animData.getCompositesByFbxPath(p4Path)
    except:
        compTrials = None
    if compTrials:
        return compTrials[0]

    # Detect Trial Via Cutscene String
    projectName = fbxPath.split(os.path.sep)[1].upper()
    fbxName = os.path.splitext(os.path.basename(fbxPath))[0]
    if projectName in PROJECTDICT.iterkeys():
        project = Context.animData.getProjectByName(PROJECTDICT[projectName])

        # Standard Name Check
        trials = []
        nameParts = fbxName.split("_")
        detectedPart = [part for part in nameParts if part[0] == "P" and len(part) > 1 and part[1] in "0123456789"]
        if detectedPart:
            partIndex = nameParts.index(detectedPart[0])
            searchText = "_".join(nameParts[:partIndex])
            trials = project.getCutsceneByName(searchText)

            # Irregular Name Checks - for various exceptions to the naming conventions
            if not trials and partIndex > 1:
                trials = project.getCutsceneByName("_".join(nameParts[:partIndex - 1]))
            if not trials and searchText[-1] not in "0123456789":
                trials = project.getCutsceneByName(searchText[:-1])

        # Brute Force Checks - last resort searches by removing letters from fbxName
        if not trials:
            for charIndex in xrange(len(fbxName)):
                if charIndex:
                    searchText = fbxName[:-charIndex]
                    trials = project.getCutsceneByName(searchText)
                    if trials:
                        break

        # Trials Found - search for matching fbxName
        for trial in trials.getAllTrials():
            trailFbxName = os.path.splitext(os.path.split(trial.getFbxScene())[-1])[0].upper()
            if trailFbxName == fbxName:
                return trial
            if trial.name == takeName:
                return trial


def GetCutsceneAction(fbxPath, takeName):
    """
    Searches watchstar for a matching trial's selected body action.
    Arguments:
        fbxPath (String): the local path of the FBX to search with
    Returns:
        (Int): The selected body action (is NOT and index)
    """
    matchedTrial = GetWatchstarTrial(fbxPath, takeName)
    if matchedTrial:
        matchedAction = [select.actionNumber() for select in matchedTrial.getSelects() if select.body() is True]
        if matchedAction:
            return matchedAction[0]


def GetCharacterDict(fbxPath, takeName):
    """
    Searches watchstar for a matching trial's character data.
    Arguments:
        fbxPath (String): the local path of the FBX to search with
    Returns:
        (Dict): made of performerRom : gameChar items.
    """
    matchedTrial = GetWatchstarTrial(fbxPath, takeName)
    if matchedTrial:
        return {character.performerROM(): character.gameChar() for character in matchedTrial.getCharacters()}


def GetPropDict(fbxPath, takeName):
    matchedTrial = GetWatchstarTrial(fbxPath, takeName)
    if matchedTrial:
        return {prop.mocapProp(): prop.gameProp() for prop in matchedTrial.getProps()}
