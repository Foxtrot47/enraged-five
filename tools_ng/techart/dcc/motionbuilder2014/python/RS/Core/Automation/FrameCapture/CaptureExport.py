"""
Description:
    Utility functions for common export processes in MotionBuilder.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu

import RS.Utils.Namespace as rsNamespace
from RS.Core.Export.Export import RexExport
from RS.Core.Automation.Layout import LayoutTools
from RS.Core.Automation.FrameCapture import CapturePaths, CutTools, FileTools


def GetAvailableName(newName, nameList):
    """
    Searches a list of existing names for an available dupe name (if dupes exist).
    Arguments:
         newName (string): the base name (non dupe version) to search for dupes with
         nameList (list): a list of strings to search through for an available name
    Returns:
        newName (string): an available name factoring in existing dupes
    """
    newName = newName.split("^")[0]
    dupeNames = [name for name in nameList if name.lower().startswith(newName.lower())]
    if dupeNames:
        if len(dupeNames) == 1:
            newName = "{}^1".format(newName)
        else:
            newName = "{0}^{1}".format(newName, len(dupeNames))
    return newName


def SwapProxyCharacters():
    """
    Swaps character namespace and exporter object any matched items in the proxyCharacterConfig.xml
    This is the new mobu version of the old CutTools module, which currently doesn't support animScenes.
    """
    # Create Proxy Dict and Export Object Names List
    fbxPath = mobu.FBApplication().FBXFileName
    proxyDict = CutTools.GetProxyCharacterDict(fbxPath)
    objectCount = RexExport.GetNumberCutsceneObjects()
    exportObjectNames = [RexExport.GetCutsceneObjectName(index) for index in xrange(objectCount)]

    # Check Each Export Object for Valid Swap
    for index, objectName in enumerate(exportObjectNames):
        proxyMatch = [proxyDict[name] for name in proxyDict.iterkeys() if name.lower() == objectName.lower()]
        modelMatch = mobu.FBFindModelByLabelName("{}:SKEL_ROOT".format(objectName))

        # Valid Swap Found - remove original from exporter and get new character name
        if proxyMatch and modelMatch:
            originalName = modelMatch.OwnerNamespace.Name
            RexExport.RemoveCutsceneObject("{}:SKEL_ROOT".format(originalName))
            newName = GetAvailableName(proxyMatch[0], exportObjectNames)

            # Check Conflicting Namespace - if one already exists we must rename it first
            existingNameSpaces = [nameSpace.Name for nameSpace in mobu.FBSystem().Scene.Namespaces]
            tempName = GetAvailableName(newName, existingNameSpaces)
            if tempName != newName:
                rsNamespace.Rename(newName, tempName)

            # Rename Character and Add to Exporter - and update our names list
            rsNamespace.Rename(originalName, newName)
            RexExport.AddCutsceneCharacter("{}:SKEL_ROOT".format(newName))
            exportObjectNames[index] = newName


def DeleteExtraZips(fbxPath):
    capPaths = CapturePaths.FastPath(fbxPath)
    zipFolder, capZipName = os.path.split(capPaths.zipPath)
    if os.path.exists(zipFolder):
        extraZips = [os.path.join(zipFolder, zipName) for zipName in os.listdir(zipFolder) if zipName.lower() != capZipName.lower()]
        [FileTools.DeletePath(zipPath) for zipPath in extraZips if os.path.getsize(zipPath) / 1024 < 5]


def ExportFbxForCapture(capObj):
    """
    Force our default shot options for capture exports.
    Setup for all capture types before type specific steps.
    Arguments:
        capObj: An instance of the CaptureModel object.
    Returns:
        A bool True after export success, or a string of an error message.
    """
    # Update Status and exportStartTime
    capObj.UpdateTag("mobuStatus", "PREP EXPORTER")
    capObj.UpdateTime("exportStartTime")

    # Run Full / Fast Cap Specific Logic
    if capObj.captureType != "FAST":
        exportResults = RunFullCap(capObj)
    else:
        exportResults = RunFastCap(capObj)

    # Return Export Results
    return exportResults


def RunFullCap(capObj):
    """
    Run full capture specific steps. Used for full and simple captures.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    Returns:
        A results string - "EXPORT SUCCESS", or a string of an error message.
    """
    # Reload or Populate Cut Files
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    cutXmlPath = os.path.join(cutsPath, "data.cutxml")
    cutPartPath = os.path.join(cutsPath, "data.cutpart")
    if os.path.exists(cutXmlPath) and os.path.exists(cutPartPath):
        RexExport.ImportCutFile(cutXmlPath)
        RexExport.ImportCutPart(cutPartPath)
    else:
        RexExport.AutoPopulateCutscene()

    # Setup Cut Files via Auto Layout
    LayoutTools.FixMissingHandles()
    LayoutTools.SetExporterShotData()
    LayoutTools.SetSafeLocation()

    # Force Frame Options - rarely old cutfiles with different ranges cause problems
    RexExport.SetCutsceneShotStartRange(0, 0)
    RexExport.SetCutsceneShotEndRange(0, int(capObj.endFrame))
    
    # GTA5 DOF Options
    if capPaths.project == "GTA5":
        RexExport.SetCutsceneShotEnableDOFFirstCut(0, True)

    # Make Cutfiles Writeable
    FileTools.MakePathWriteable(cutsPath)

    # Manual Export - exit early before export commands
    if capObj.exportMode == "Manual":
        return "MANUAL EXPORT"
    
    # Export Anims
    capObj.UpdateTag("mobuStatus", "BUILD ANIMS")
    try:
        animsBuilt = RexExport.ExportActiveCutscene(p4_Intergration=False, buildzip=False)
    except:
        animsBuilt = False
    if not animsBuilt:
        return "EXPORT ERROR: Issue during Export Anims/Cutfile"

    # Build Zip
    zipPath = capPaths.zipPath
    zipBuilt = CutTools.BuildZipFromCutsPath(cutsPath, zipPath)
    if not zipBuilt:
        return "EXPORT ERROR: Issue during Rebuild Zips (from tempCuts files)"

    # Delete Extra Zips
    DeleteExtraZips(capObj.fbxPath)

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    try:
        previewBuilt = RexExport.BuildActiveCutscene(p4_Intergration=False, preview=True)
    except:
        previewBuilt = False
    if not previewBuilt:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def RunFastCap(capObj):
    """
    Run fast capture specific steps. Used for fast captures only.
    Arguments:
        cabObj: An instance of the CaptureModel object.
    Returns:
        A bool True after export success, or a string of an error message.
    """
    # Setup Exporter Shot Data
    capPaths = CapturePaths.FastPath(capObj.fbxPath)
    cutsPath = capPaths.cutsPath
    cutPartPath = os.path.join(cutsPath, "data.cutpart")
    RexExport.ImportCutPart(cutPartPath)
    LayoutTools.SetExporterShotData()

    # Remove Extra Cutfiles and Add Export Camera
    RexExport.RemoveAllCutsceneObjects()
    RexExport.AddCutsceneCamera("ExportCamera", addFade=False)

    # Make Cutfiles Writeable
    FileTools.MakePathWriteable(cutsPath)

    # Export Camera Anim
    capObj.UpdateTag("mobuStatus", "BUILD CAM ANIMS")
    camAnimsBuilt = RexExport.ExportCameraAnim(p4_Intergration=False, buildzip=False)
    if not camAnimsBuilt:
        return "EXPORT ERROR: Issue during Build Cam Anims"

    # Update Status and Paths
    capObj.UpdateTag("mobuStatus", "REBUILD ZIP")
    capDataRoot = capPaths.capDataRoot

    # Set Temp Cuts Paths
    tempCutsRoot = os.path.join(os.path.split(cutsPath)[0], "tempCuts")
    tempCutFolder = os.path.join(tempCutsRoot, capObj.fbxName)
    expCamAnimPath = os.path.join(cutsPath, "ExportCamera.anim")
    expCamClipPath = os.path.join(cutsPath, "ExportCamera.clip")

    # Delete Previous Data and Copy Network Cuts Files
    FileTools.DeletePath(tempCutsRoot)
    FileTools.CreateParentFolders(tempCutsRoot)
    dataCutsFolder = os.path.join(capDataRoot, "CutFiles")
    FileTools.CopyFolderContents(dataCutsFolder, tempCutFolder)

    # Copy Export Cam Anim/Clip Files to TempCuts
    FileTools.CopyPath(expCamAnimPath, tempCutFolder)
    FileTools.CopyPath(expCamClipPath, tempCutFolder)

    # TODO - commented out ias copy below - why were we doing this?
    # Copy Previous IAS - if available
    # localIasFolder = CapturePaths.iasRoot
    # dataIasPath = os.path.join(capDataRoot, "IasFiles", "@{}.ias".format(capObj.fbxName))
    # if os.path.exists(dataIasPath):
    #     FileTools.CopyPath(dataIasPath, localIasFolder)

    # Build Zip
    zipPath = capPaths.zipPath
    zipBuilt = CutTools.BuildZipFromCutsPath(tempCutFolder, zipPath)
    if not zipBuilt:
        return "EXPORT ERROR: Issue during Rebuild Zips (from tempCuts files)"

    # Delete Extra Zips
    DeleteExtraZips(capObj.fbxPath)

    # Build Preview
    capObj.UpdateTag("mobuStatus", "BUILD PREVIEW")
    previewBuilt = RexExport.BuildActiveCutscene(p4_Intergration=False, preview=True)
    if not previewBuilt:
        return "EXPORT ERROR: Issue during Build Preview"

    # Scene Built Successfully
    return "EXPORT SUCCESS"


def SendLocalExportData():
    fbxPath = mobu.FBApplication().FBXFileName
    if fbxPath:
        fbxName = os.path.splitext(os.path.basename(fbxPath))[0]
        fbxEndFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        exportEndFrame = RexExport.GetCutsceneShotEndRange(0)
        if fbxEndFrame != exportEndFrame:
            msgTxt = "Error: The FBX endframe {0} does not match the exported endframe {1}".format(fbxEndFrame, exportEndFrame)
            mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Damn")
        else:
            msgTxt = "Send local export of {} to capture machines?".format(fbxName)
            userChoice = mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Send Data", "Cancel")
            if userChoice == 1:
                msgTxt = CutTools.SendExportToNetwork(fbxPath)
                mobu.FBMessageBox("Export Transfer Tool", msgTxt, "Okay")
