"""
Description:
    Various functions to automate layout steps, and a master AutoLayout function at the bottom to
    runs all of them at once.  For the moment this is only for cutscenes, but ingame is coming soon.

Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import re
import math
from itertools import izip_longest
from subprocess import Popen, PIPE
import pyfbsdk as mobu

import RS.Config
import RS.Globals
import RS.Perforce as P4
import RS.Core.Camera.KeyTools.camKeyTasks as CamKeyTasks
from RS.Core.Automation.Layout import WatchstarTools, ModelTools
from RS.Core.Camera import CamUtils, CameraLocker
from RS.Utils import ContentUpdate
from RS.Core.Audio import AudioTools
from RS.Core.Export.Export import RexExport
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Utils.Scene import Model
from RS import ProjectData
from RS.Tools import WorldPosition

def getCurrentProject():
    currentProj = ProjectData.data.GetAnimDataProjectName()
    return currentProj


def CleanSort(inputList):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key.lower())]
    inputList.sort(key=alphanum_key)


def RunTerminalCommand(cmdTxt):
    """
    Simple module to run a terminal command, making perforce commands easier than via the P4 module.
    Arguments:
        cmdTxt (string): the text of the termainal command to run
    Returns:
        (string): standard output returned by the command
    """
    process = Popen(cmdTxt, shell=True, stdout=PIPE)
    output = process.communicate()[0]
    return output


def VerifyCutsceneTake():
    """
    Verifies a cutscene FBX has the correct take selected.
    """
    # Check For More Than One Takes - no reason to swap otherwise
    lSystem = mobu.FBSystem()
    if len(lSystem.Scene.Takes) > 1:

        # Try Detecting Take Number
        fbxName = os.path.splitext(os.path.basename(mobu.FBApplication().FBXFileName))[0].upper()
        detectedTake = [part for part in fbxName.split("_") if part.startswith("T") and len(part) in [2, 3]]
        if detectedTake:

            # Set TakeNumber and SceneName
            sceneName = fbxName.split(detectedTake[0])[0][:-1]
            takeNumber = detectedTake[0][1:]
            if len(takeNumber) == 1:
                takeNumber = "0" + takeNumber

            # Mini Validator Function - for detecting valid take names
            def CheckValid(takeName):
                takeParts = takeName.split("_", 3)
                return len(takeParts) == 4 and takeParts[1] == takeNumber and takeParts[3] == sceneName

            # Validate Current Take
            currentTakeName = lSystem.CurrentTake.Name
            if CheckValid(currentTakeName) is False:

                # Current Take Invalid - search through other takes
                for index, eachTake in enumerate(lSystem.Scene.Takes):
                    if CheckValid(eachTake.Name):

                        # Correct Take Found - we select it and break out
                        lSystem.CurrentTake = lSystem.Scene.Takes[index]
                        break


def GetSlateKeys():
    """
    Looks for any slate models in the scene and if found returns their keys.
    If multiple valid slates are found we return the most recently added one.
    Returns:
        mobuKeys (list) - if slate keys found, otherwise returns None
    """
    validSlates = [comp for comp in RS.Globals.Components
                   if isinstance(comp, mobu.FBModel) and "slatetop_bone" in comp.Name.lower() and
                   getattr(comp, "Rotation") and comp.Rotation.GetAnimationNode()]
    if validSlates:
        return validSlates[-1].Rotation.GetAnimationNode().Nodes[2].FCurve.Keys


def SetSmartEndframe():
    """
    Sets our hard endframe to the last key on the slate (if found and is earlier)
    """
    slateKeys = GetSlateKeys()
    if slateKeys:
        lastKeyedFrame = int(math.ceil(slateKeys[-1].Time.GetSecondDouble() * 30))
        currentEndFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        if currentEndFrame > lastKeyedFrame:
            CamUtils.SetHardRanges(lastKeyedFrame)


def ZeroCutscene(showMessages=True):
    """
    Zeroes a cutscene to a selected body action (detected via Watchstar) and sets a smart endframe.
    """
    # Verify Correct Take
    VerifyCutsceneTake()

    # Verify Slate Has Keys and Not Zeroed Already
    slateKeys = GetSlateKeys()
    if slateKeys is None:
        errorMessage = "Error: No slate keys found in current FBX."
        if showMessages:
            mobu.FBMessageBox("Zero Cutscene", errorMessage, "Okay")
        return errorMessage
    if slateKeys[0].Time.GetFrame() < 0:
        messageText = "Warning: Slate appears to have been zeroed already. Skipping."
        if showMessages:
            mobu.FBMessageBox("Zero Cutscene", messageText, "Okay")
        return True

    # Detect Claps
    clapList = []
    clapDict = Clapper.GetClaps()
    [clapList.extend(value) for key, value in clapDict.iteritems() if type(key) == mobu.FBModelRoot]
    clapList.sort()
    if not clapList:
        errorMessage = "Error: Slate keys found, but not claps detected."
        if showMessages:
            mobu.FBMessageBox("Zero Cutscene", errorMessage, "Okay")
        return errorMessage

    # Detect Actions
    fbxPath = mobu.FBApplication().FBXFileName
    takeName = mobu.FBSystem().CurrentTake.Name
    actionList = WatchstarTools.GetCutsceneActions(fbxPath, takeName)
    if not actionList:
        errorMessage = "Error: Action not detected via Watchstar."
        if showMessages:
            mobu.FBMessageBox("Zero Cutscene", errorMessage, "Okay")
        return errorMessage

    # Check For Deleted Claps - if found assume already zeroed
    if len(actionList) > len(clapList):
        messageText = "Warning: Slate appears to have been zeroed already. Skipping."
        if showMessages:
            mobu.FBMessageBox("Zero Cutscene", messageText, "Okay")
        return True

    # Set Zero Frame and Zero Out
    actionIndex = [action.actionNumber()-1 for action in actionList if action.body() is True][0]
    zeroFrame = clapList[actionIndex]
    MocapCommands.ZeroOutScene(zeroFrame, showMessage=False)

    # Zeroing Successful
    return True


def DeleteSlates():
    """
    Deletes all slates from an FBX.
    """
    slateRootNames = ["slate_bone", "slatebone", "slate_text", "slatetext"]
    for model in list(mobu.FBSystem().Scene.RootModel.Children):
        modelName = model.Name.lower()
        for slateName in slateRootNames:
            if slateName in modelName:
                Model.DeleteChildren(model)
                model.FBDelete()
                break


def SetupToybox(showMessages=True):
    """
    Adds a Toybox object if one doesn't already exist.
    """
    toyboxFound = [comp for comp in RS.Globals.gComponents if comp.Name == "ToyBox"]
    if not toyboxFound:
        MocapCommands.CreateToybox()
    elif showMessages:
        mobu.FBMessageBox("Warning", "Warning: ToyBox object already exists. Skipping.", "Okay")


def SetupHandleFile():
    """
    Syncs, sorts, and updates handle file with any new handles in the current FBX.
    """
    # Sync and Read Handle File
    toolsRoot = RS.Config.Tool.Path.Root
    handleFilePath = os.path.join(toolsRoot, "etc", "config", "cutscene", "Cutscene_Model_Handles.csv")
    P4.Revert(handleFilePath)
    P4.Sync(handleFilePath, force=True)
    with open(handleFilePath, "r") as handleFile:
        inputLines = handleFile.readlines()

    # Create Dicts from Handle File
    characterDict, propDict, vehicleDict = {}, {}, {}
    specificStartIndex = None
    for handleLine in inputLines[1:]:
        if "cutscene specific" in handleLine.lower():
            specificStartIndex = inputLines.index(handleLine)
            break
        lineList = handleLine.strip().split(",")
        if len(lineList) > 2 and lineList[1] != "" and lineList[2] != "":
            characterDict[lineList[1]] = lineList[2]
        if len(lineList) > 4 and lineList[3] != "" and lineList[4] != "":
            propDict[lineList[3]] = lineList[4]
        if len(lineList) > 6 and lineList[5] != "" and lineList[6] != "":
            vehicleDict[lineList[5]] = lineList[6]

    # Detect New Handles to Add
    exportModelList = ModelTools.DetectExportModels()
    charactersToAdd = [character.split(":")[0] for character in exportModelList[0]]
    propsToAdd = [prop.split(":")[0] for prop in exportModelList[1] + exportModelList[2]]
    vehiclesToAdd = [vehicle.split(":")[0] for vehicle in exportModelList[3]]

    # Add New Handles to Dicts
    for character in charactersToAdd:
        if character not in characterDict:
            name = character[3:] if character[:3].lower() in ("cs_", "ig_") else character
            characterDict[character] = name
    propDict.update({prop: prop for prop in propsToAdd if prop not in propDict})
    vehicleDict.update({vehicle: vehicle for vehicle in vehiclesToAdd if vehicle not in vehicleDict})

    # Sort Handles
    sortedCharacters = characterDict.keys()
    CleanSort(sortedCharacters)
    sortedProps = propDict.keys()
    CleanSort(sortedProps)
    sortedVehicles = vehicleDict.keys()
    CleanSort(sortedVehicles)

    # Create Output Lines
    outputLines = [";,Ped Models,Ped handle,Prop Model,Prop Handle,Vehicle models,Vehicle handle\n"]
    for character, prop, vehicle in izip_longest(sortedCharacters, sortedProps, sortedVehicles, fillvalue=""):
        cHandle = characterDict[character] if character else ""
        pHandle = propDict[prop] if prop else ""
        vHandle = vehicleDict[vehicle] if vehicle else ""
        lineText = ",{0},{1},{2},{3},{4},{5}\n".format(character, cHandle, prop, pHandle, vehicle, vHandle)
        outputLines.append(lineText)
    outputLines[1] = "Common:{0}".format(outputLines[1])

    # Write Output Lines to File
    P4.Edit(handleFilePath)
    with open(handleFilePath, "w") as handleFile:
        for line in outputLines:
            handleFile.write(line)
        if specificStartIndex:
            for specificLine in inputLines[specificStartIndex:]:
                handleFile.write(specificLine)


def AddItemsToExporter():
    # TODO: Rewrite this old function to be less reliant on strings?
    # We now use RexExport.AutoPopulateCutscene instead, but a custom function would allow extra validation checks.
    """
    Detects models to export and adds them to the Rex Rage Cutscene Exporter.
    We used to add via RexExport.AutoPopulateObjects(), but doing it manually allows more control.
    """
    # Detect ExportModelList
    exportModelList = ModelTools.DetectExportModels()

    # Open Exporter, Remove Old Data, Add Export Camera
    mobu.ShowToolByName("Rex Rage Cut-Scene Export")
    RexExport.RemoveAllCutsceneObjects()
    RexExport.AddCutsceneCamera("ExportCamera", addFade=False)
    
    # Manual Add Audio - not needed if auto populate works
    fbxName = os.path.splitext(os.path.basename(mobu.FBApplication().FBXFileName))[0].upper()
    audioClips = [audio for audio in mobu.FBSystem().Scene.AudioClips if audio.Name == "{}.C.wav".format(fbxName)]
    if audioClips:
        audioName = audioClips[0].Name
        RexExport.AddCutsceneAudio(audioName)

    # Manual Add Model - not needed if auto populate works
    for typeIndex in xrange(len(exportModelList)):
        for eachModel in exportModelList[typeIndex]:
            if typeIndex == 0:
                RexExport.AddCutsceneCharacter(eachModel)
            if typeIndex == 1:
                RexExport.AddCutsceneProp(eachModel)
            if typeIndex == 2:
                RexExport.AddCutsceneWeapon(eachModel)
                pass
            if typeIndex == 3:
                RexExport.AddCutsceneVehicle(eachModel)


def FixMissingHandles():
    """
    Finds models missing their handle data and uses their model name instead.
    """
    for itemIndex in xrange(RexExport.GetNumberCutsceneObjects()):
        itemName = RexExport.GetCutsceneObjectName(itemIndex)
        itemHandle = RexExport.GetCutsceneObjectHandle(itemIndex)
        if itemHandle is None:
            RexExport.SetCutsceneObjectHandle(itemIndex, itemName)


def AutoPopulate():
    """
    Simple wrapper so we can run this via the UI without importing the RexExporter.
    I'll rewrite the UI down the road, but wanted to get testing on this for now.
    """
    if getCurrentProject() == "GTA 5":
        AddItemsToExporter()
    else:
        RexExport.AutoPopulateCutscene()


def ValidateSceneLocation(sceneLoc):
    if sceneLoc and sceneLoc not in ((0, 0, 0, 0), (-1, -1, -1, -1)):
        return True
    return False


def GetSceneLocationFromSet():
    """
    Detects a scene's export location from any sets found in the current FBX.
    """
    sets = [model for model in mobu.FBSystem().Scene.RootModel.Children if "setroot" in model.Name.lower()]
    if sets:
        pointOrigModel = [model for model in sets[-1].Children if "pointorig" in model.Name.lower()]
        if pointOrigModel:
            xOffset, yOffset, zOffset = pointOrigModel[0].Translation
            rotation = sets[-1].Rotation[2]
            rotation = 360 - abs(rotation) if rotation > 0 else abs(rotation)
            return xOffset, yOffset, zOffset, rotation


def GetSceneLocationFromExporter():
    """
    Detect's a scene's export location from the cutscene exporter.
    """
    x = RexExport.GetCutsceneOffsetsX()
    y = RexExport.GetCutsceneOffsetsY()
    z = RexExport.GetCutsceneOffsetsZ()
    rotation = RexExport.GetCutsceneOrientation()
    return x, y, z, rotation


def SetSceneLocation():
    """
    Detects if location data, and if a set is found, uses that data instead.
    """
    if getCurrentProject() == "GTA 5":
        sceneLoc = []
        excelSceneLoc = WorldPosition.GetWorldPositionFromExcel()
        listOfValues = [value for (key, value) in excelSceneLoc.items()]
        def getLocation(listOfValues):
            for value in listOfValues:
                for key, value in value.items():
                    sceneLoc.append(value)
                    
            return sceneLoc
        sceneLoc = getLocation(listOfValues)
    else:
        sceneLoc = GetSceneLocationFromSet()
    if ValidateSceneLocation(sceneLoc):
        RexExport.SetCutsceneOffsets(sceneLoc[0], sceneLoc[1], sceneLoc[2], sceneLoc[3])


def SetSafeLocation():
    """
    First checks the exporter location, then tries the set location, before defaulting to a backup location.
    """
    expLoc = GetSceneLocationFromExporter()
    if ValidateSceneLocation(expLoc) is False:
        setLoc = GetSceneLocationFromSet()
        if ValidateSceneLocation(setLoc) is True:
            RexExport.SetCutsceneOffsets(setLoc[0], setLoc[1], setLoc[2], setLoc[3])
        else:
            RexExport.SetCutsceneOffsets(-1125.541, -560.890, 82.868, 47.125)


def SetExporterShotData():
    """
    Detects scene offset and sets all shot options in the Rex Rage Cutscene Exporter.
    """
    # Backup Pre-Existing Location Data
    expLoc = GetSceneLocationFromExporter()

    # Set Shot Options
    RexExport.ClearCutsceneShots()
    RexExport.AddCutsceneShot("Shot_1")
    RexExport.SetCutsceneShotExportStatus(0, True)
    RexExport.SetCutsceneStoryMode(False)
    RexExport.SetCutsceneConcatMode(False)
    RexExport.SetCutsceneShotSectioning(0, 1)
    RexExport.SetCutsceneShotSectioningDuration(0, duration=4.0)

    # Restore Location Data - if not at zero
    if ValidateSceneLocation(expLoc):
        RexExport.SetCutsceneOffsets(expLoc[0], expLoc[1], expLoc[2], expLoc[3])


def LoadShotsFromEst():
    shotDict = {}
    for track in RS.Globals.Story.RootEditFolder.Tracks:
        if track.Name.endswith("_EST") and track.Name.count("_") == 2 and len(track.Clips):
            trackIndex = int(track.Name.split("_")[1]) - 1
            trackStart = track.Clips[0].Start.GetFrame()
            trackEnd = track.Clips[-1].Stop.GetFrame() - 1
            shotDict[trackIndex] = (trackStart, trackEnd)

    if shotDict:
        expLoc = GetSceneLocationFromExporter()
        RexExport.ClearCutsceneShots()
        for shotIndex in sorted(shotDict.keys()):
            shotRange = shotDict[shotIndex]
            RexExport.AddCutsceneShot("Shot_{0}".format(shotIndex + 1))
            RexExport.SetCutsceneShotStartRange(shotIndex, shotRange[0])
            RexExport.SetCutsceneShotEndRange(shotIndex, shotRange[1])

            RexExport.SetCutsceneShotExportStatus(shotIndex, True)
            RexExport.SetCutsceneStoryMode(True)
            RexExport.SetCutsceneShotSectioning(shotIndex, 1)
            RexExport.SetCutsceneShotSectioningDuration(shotIndex, duration=4.0)

        if ValidateSceneLocation(expLoc):
            RexExport.SetCutsceneOffsets(expLoc[0], expLoc[1], expLoc[2], expLoc[3])


def SaveBeforeExport(changeListNumber=None):
    """
    Saves (and locks) camera data and the FBX before export (using a changelist if provided).
    Fairly basic, but I split it off here to keep the AutoLayout function below clean and simple.
    """
    # Check Cam Data - if none exists we create it and add it to our AutoLayout changelist
    camDataPath = CamKeyTasks.GetLocalCamXmlPath()
    if P4.DoesFileExist(camDataPath) is False:
        CamKeyTasks.ForceSubmitCamData()
        if changeListNumber:
            cmdTxt = 'p4 add -c {0} {1}'.format(changeListNumber, camDataPath)
            RunTerminalCommand(cmdTxt)

    # Lock Cams
    camLockObj = CameraLocker.LockManager()
    camLockObj.setAllCamLocks(True)

    # Checkout, Save, and Add FBX to AutoLayout Changelist
    fbxPath = mobu.FBApplication().FBXFileName
    P4.Edit(fbxPath)
    mobu.FBApplication().FileSave(fbxPath)
    if changeListNumber:
        cmdTxt = "p4 reopen -c {0} {1}".format(changeListNumber, fbxPath)
        RunTerminalCommand(cmdTxt)


def ExportCutscene():
    """
    Runs cutscene export steps and checks for errors.
    Returns:
        True for successful export, or an error message string.
    """
    # Run Export Anims / Cut File
    animsBuilt = RexExport.ExportActiveCutscene(p4_Intergration=True, buildzip=True)
    if not animsBuilt:
        return "Error: Issue during Export Anims/Cutfile"

    # Run Build Dictionaries
    rpfBuilt = RexExport.BuildActiveCutscene(p4_Intergration=True, preview=False)
    if not rpfBuilt:
        return "Error: Issue during Build Preview"

    return True


def AutoLayout(showMessages=True, perforceSubmit=True, job=None):
    """
    Runs all Layout steps at once.
    """
    # TODO: Needs more checks the FBX is valid and not checked out
    if RS.Globals.Application.FBXFileName == "":
        return "Error: Invalid FBX."

    # Zero Out Cutscene - Disabled for now.
    # zeroSuccess = ZeroCutscene(showMessages)
    # if zeroSuccess is not True:
    #     return zeroSuccess

    # Set Smart End Frame
    SetSmartEndframe()

    # Delete Slates
    DeleteSlates()

    # Sync and Add Cutscene Audio
    AudioTools.SetupCutsceneAudio()

    # Add Toybox Object
    SetupToybox(showMessages)

    # Old Setup Steps - No longer needed?
    # ModelTools.SetupCharacters()
    # ModelTools.SetupHorses()
    # # ModelTools.SetupGameModels()
    # MocapCommands.DeleteBasePreviz()
    # MocapCommands.PlotMocapAssets()

    # Setup Export Camera
    CamUtils.DeleteInvalidCams()
    CamUtils.SetupExportCam()

    # TODO: Force Model Visibility - coming soon / is this still an issue?

    # Update Job Description
    _description = "AutoLayout"
    if job is not None:
        _description = "TechArt Automation AutoLayout\n" \
                       "Triggered by {}\n" \
                       "Trigger CL {}\n" \
                       "Job ID: {}\n" \
                       "Processed By {}\n" \
                       "".format(job.User,
                                 job.Changelist,
                                 job.ID,
                                 job.Machine,
                                 )

    # Create AutoLayout Changelist
    changeListNumber = P4.CreateChangelist(_description).Number

    # TODO: Below Sections Disabled During Initial Testing
    # # Setup Handle File
    # SetupHandleFile(changeListNumber)
    #
    # # Setup Cutscene Meta File
    # ContentUpdate.SetupGameContentFromMobuFbx()
    #
    # # Setup Content Xml Files
    # ContentUpdate.SetupToolsContent(changeListNumber)
    #
    # # AutoPopulate and Fix Handles
    # AutoPopulate()
    # FixMissingHandles()
    #
    # # Set Exporter Shot Data
    # SetExporterShotData()
    # SetSceneLocation()

    # Save Cams and FBX Before Export
    SaveBeforeExport(changeListNumber)

    # # Check Files in Default Changelist
    # beforeFiles = [str(record["depotFile"]) for record in P4.Run("opened", "").Records if record["change"] == "default"]
    #
    # # Export Cutscene
    # exportSuccess = ExportCutscene()
    # if exportSuccess is not True:
    #     return exportSuccess
    #
    # # TODO: Sometimes we aren't catching all updated files in afterFiles below. Maybe try a sleep or refresh?
    #
    # # Move New Default Changelist Files to AutoLayout Changelist
    # # time.sleep(3) might be needed here, as early tests of this would occasionally miss a few files
    # afterFiles = [str(record["depotFile"]) for record in P4.Run("opened", "").Records if record["change"] == "default"]
    # updatedFiles = [eachFile for eachFile in afterFiles if eachFile not in beforeFiles]
    # for eachFile in updatedFiles:
    #     cmdTxt = "p4 reopen -c {0} {1}".format(changeListNumber, eachFile)
    #     RunTerminalCommand(cmdTxt)

    # Submit AutoLayout Changelist
    if perforceSubmit:
        change = P4.Submit(changeListNumber)
        # We need to retrieve the actual changelist submission number as this could change
        for change_record in change.Records:
            changeListNumber = change_record['change']
            break

    print "Auto Layout Finished"
    return changeListNumber


def CaptureLayout():
    # Zero Out Cutscene
    # ZeroCutscene(showMessages=False)
    # SetSmartEndframe()

    # Setup Toybox and Audio
    SetupToybox(showMessages=False)
    # AudioTools.SetupCutsceneAudio()

    # Old Setup Steps - No longer needed?
    # ModelTools.SetupGameModels()
    # ModelTools.SetupCharacters()
    # ModelTools.SetupHorses()
    # MocapCommands.DeleteBasePreviz()
    # MocapCommands.PlotMocapAssets()
    # SetupHandleFile()

    # Setup Content Files
    # ContentUpdate.SetupGameContentFromMobuFbx()
    ContentUpdate.SetupToolsContent()

    # Capture Layout Success
    return True


def Run():
    AutoLayout()
