"""
Description:
    Functions for manipulating frames, syncing audio, and encoding them into video.
    Full documentation coming soon, but need to get this in now for Avid renders.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import re
import subprocess
import CapturePaths
import FileTools


def SyncVideoTools():
    videoToolPaths = [CapturePaths.ffmpegExePath, CapturePaths.ffmpegFontPath]
    for toolPath in videoToolPaths:
        cmdTxt = "p4 sync -q {0}".format(toolPath)
        subprocess.call(cmdTxt, shell=True, cwd="x:\\rdr3")


def SearchPerforceForAudio(audioName):
    cmdTxt = "p4 files -e {0}...{1}".format(CapturePaths.audioRoot, audioName)
    matchedAudio = subprocess.check_output(cmdTxt, shell=True, cwd="x:\\rdr3").splitlines()
    if matchedAudio:
        p4AudioPath = os.path.join(matchedAudio[0].split(audioName)[0], audioName)
        cmdTxt = "p4 sync -q {0}".format(p4AudioPath)
        subprocess.call(cmdTxt, shell=True, cwd="x:\\rdr3")
        cmdTxt = "p4 where {0}".format(p4AudioPath)
        mapString = subprocess.check_output(cmdTxt, shell=True, cwd="x:\\rdr3")
        audioPath = mapString.split(".wav ")[-1].strip()
        return audioPath


def ExtendAudioFile(srcPath, destPath, duration):
    inputTxt = '{0} -i {1} -f lavfi -i anullsrc=r=48000:cl=mono'.format(CapturePaths.ffmpegExePath, srcPath)
    filterTxt = '-filter_complex "[0:a][1:a]concat=n=2:v=0:a=1[a]" -map "[a]"'
    outputTxt = '-t {0} -loglevel error -y'.format(duration)
    cmdTxt = '{0} {1} {2} {3}'.format(inputTxt, filterTxt, outputTxt, destPath)
    subprocess.call(cmdTxt, shell=True)


def AddZeroesToNumber(number, zeroCount):
    numberString = str(number)
    while len(numberString) < zeroCount:
        numberString = "".join(["0", numberString])
    return numberString


def ConvertFrameIntToDropFrameInt(frameInt):
    offset = (frameInt + 500) / 1000
    return frameInt - offset


def GetFrameNameParts(inputFrame):
    frameText = ""
    frameDigit = ""
    inputFolder, frameName = os.path.split(inputFrame)
    fileName = os.path.splitext(frameName)[0]
    frameSplit = re.match("^(\d+)(.*)", fileName[::-1])
    if frameSplit:
        frameText = frameSplit.group(2)[::-1]
        frameDigit = frameSplit.group(1)[::-1]
    return frameText, frameDigit


def GetFrameListFromSequence(inputFrame):
    # Get Input Folder, Search Text, and Frame Index
    inputFolder, frameName = os.path.split(inputFrame)
    fileName, fileExt = os.path.splitext(frameName)
    frameText, frameDigit = GetFrameNameParts(inputFrame)
    searchText = "{0}(\d+){1}".format(re.escape(frameText), fileExt)
    frameIndex = int(frameDigit)

    # Create Frame Dict - so we only have one path for each frame number
    frameDict = {}
    for frame in os.listdir(inputFolder):
        nameMatch = re.match(searchText, frame, re.I)
        if nameMatch:
            frameString = nameMatch.group(1)
            frameNumber = int(frameString)
            if frameNumber not in frameDict.iterkeys():
                frameDict[frameNumber] = frame
            elif len(frame) > len(frameDict[frameNumber]):
                frameDict[frameNumber] = frame

    # Create Frame List - so we skip invalid frames
    frameList = []
    for frameNumber in sorted(frameDict.iterkeys()):
        if frameNumber == frameIndex:
            framePath = os.path.join(inputFolder, frameDict[frameNumber])
            frameList.append(os.path.join(inputFolder, framePath))
            frameIndex += 1
    return frameList


def DropFramesFromSequence(inputFrame):
    droppedFrames = 0
    inputFolder, frameName = os.path.split(inputFrame)
    fileName, fileExt = os.path.splitext(frameName)
    frameList = GetFrameListFromSequence(inputFrame)
    for frameIndex, framePath in enumerate(frameList):
        frameText, frameDigit = GetFrameNameParts(framePath)
        if str(frameIndex).endswith("499"):
            FileTools.DeletePath(framePath)
            droppedFrames += 1
        else:
            fixedDigit = AddZeroesToNumber(int(frameDigit) - droppedFrames, 5)
            fixedName = "".join([frameText, fixedDigit, fileExt])
            fixedPath = os.path.join(inputFolder, fixedName)
            if framePath != fixedPath:
                FileTools.DeletePath(fixedPath)
                os.rename(framePath, fixedPath)


def FixMissingStartFrames(inputFrame, missingFrames):
    inputFolder = os.path.split(inputFrame)[0]
    fileExt = os.path.splitext(inputFrame)[1]
    frameText, frameDigit = GetFrameNameParts(inputFrame)
    frameList = GetFrameListFromSequence(inputFrame)
    frameNumber = len(frameList) + missingFrames
    lastPath = ""
    while frameNumber:
        frameNumber -= 1
        fixedNumber = AddZeroesToNumber(frameNumber, len(frameDigit))
        fixedName = "".join([frameText, fixedNumber, fileExt])
        fixedPath = os.path.join(inputFolder, fixedName)
        if frameNumber >= missingFrames:
            framePath = frameList[frameNumber - missingFrames]
            os.rename(framePath, fixedPath)
        else:
            FileTools.CopyPath(lastPath, fixedPath)
        lastPath = fixedPath


def CreateRdrAvidVideo(sceneName, inputPath, outputPath, noteText="", jobLog=None):
    if jobLog:
        jobLog.LogMessage("Setting audio and ffmpeg options.")
    # Sync Video Tools
    SyncVideoTools()

    # Get Audio Path
    audioTemp = "x:/videoToolsTemp.wav"
    audioName = "{}.C.wav".format(sceneName)
    audioPath = SearchPerforceForAudio(audioName)
    if not audioPath:
        audioPath = FileTools.SearchFolderForFileName(CapturePaths.audioRoot, audioName)

    # Drop Frames
    DropFramesFromSequence(inputPath)

    # Get Frame Count and Frame Digit
    frameList = GetFrameListFromSequence(inputPath)
    frameCount = len(frameList)
    inputFolder, frameName = os.path.split(frameList[0])
    frameDigit = GetFrameNameParts(frameName)[1]

    # Get Fixed Input and Font Paths
    fixedName = frameName.replace(frameDigit, "%0{0}d".format(len(frameDigit)))
    fixedInputPath = os.path.join(inputFolder, fixedName)
    fixedFontPath = os.path.splitdrive(CapturePaths.ffmpegFontPath)[1].replace("\\", "/")

    # Set Default FFMPEG Options
    inputTxt = '{0} -r 29.97 -start_number {1} -i {2}'.format(CapturePaths.ffmpegExePath, frameDigit, fixedInputPath)
    filterTxt = ('-vf [in]scale=720:480,drawtext="fontfile={0}: text=\'{1}\': fontcolor=white: fontsize=24: '
                 'box=1: boxcolor=black@0.5: x=25: y=450"[out]').format(fixedFontPath, noteText)
    outputTxt = '-r 29.97 -f mov -vcodec dvvideo -pix_fmt yuv411p -shortest -loglevel error -y'

    # Verify FFMPEG Output Folder
    FileTools.CreateParentFolders(outputPath)

    # Audio Found - extend audio and update options
    if audioPath and os.path.exists(audioPath):
        ExtendAudioFile(audioPath, audioTemp, frameCount / 30 + 5)
        inputTxt += ' -i {0}'.format(audioTemp)
        outputTxt += ' -acodec copy'
    elif jobLog:
        jobLog.LogWarning("No audio found for this scene. Rendering video only.")

    # Create Video - via ridiculously long commandline call
    cmdTxt = '{0} {1} {2} {3}'.format(inputTxt, filterTxt, outputTxt, outputPath)
    if jobLog:
        jobLog.LogMessage("Rendering output video.")
    subprocess.call(cmdTxt, cwd="x:/rdr3", shell=True)

    # Delete Audio Temp
    FileTools.DeletePath(audioTemp)
