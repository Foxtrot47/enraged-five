"""
Description:
    Module for sending requests to be handled by a python executable outside of Motion Builder.
    Intended for performing tasks that need to perform asynchronous from Motion Builder.

Authors:
    David Vega <david.vega@rockstargames.com>

Wiki:
    https://devstar.rockstargames.com/wiki/index.php/Server

About:
    There are three methods to talk to the server:
    -send()
    -run()
    -thread()

    All three methods accept python code as strings, which is sent to a local python server to be executed.

    send() is the most basic method, all it does is send the string to the server inside a try statement to catch
    errors. It basically just wraps the socket.socket().send() method. It is decorated by the connect() method which
    makes sure that there is a local python server running. If there is no local server running and it can't start one,
    the send method will return None. If the byte_rate variable on the server is set to a value lower than the actual
    size of the string being sent, then the part of the string will be cut off. You can use the set_byte_rate() method
    to set the byte rate before sending the string to the server to resolve this problem. The run() method runs this
    check automatically.

    run() calls send() to send your string to the server and also accepts two more arguements, request_parameter, and
    log. request parameter accepts a string that has the name of a variable on the server that you want the run method
    to return. If it is on the server it will return it as a string. The log parameter accepts a string that it will use
    to log a message to the thread.log of the tool when the server is accessed.

    thread() calls run and accepts all the same parameters as run. This method takes the string with python code and
    wraps it in a method that gets sent to the server to be threaded. The method is added on a Queue in the server to
    make sure that no two methods accessing the same file/database conflict with each other. This method does not accept
    the request parameter and instead will return the thread id when successful.

    The code sent through run() or send() is executed on the main thread of the server. MotionBuilder will wait until
    the server sends it data back to continue running. If you don't want motion builder / dcc application to wait, use
    the thread() method. Any variables, methods, and classes not encapsulated in another method or class in the string
    you pass to the run() or send() will be stored in the global namespace of the server. the thread() method wraps the
    passed string into a method, so variables, methods and classes generated in that string will not be added to the
    global namespace of the server unless explicitly stated by the global statement.

    the start() method attempts to start a new server. It will fail if the port used by the server is in use.

    the kill() method closes the server. If there is no server open, it will open one and immediately close it.

Examples:

import sys
import RS.Core.Server as Server
import RS.Core.DatabaseConnection as DatabaseConnection

my_long_command='''
import time
time.sleep(3)
local_time = time.localtime()
print "Success"
'''

# Start the server, you can pass in any port number between 10000 to 10099, default is 10000
Server = Server.Connection()

# Just send data
command_size = sys.getsizeof(my_long_command)
Server.__SetByteRate(command_size)
Server.Send(my_long_command) #returns 'Successful Connection' if the server is available and executes on the server

# Lets ask for the value and set the log to say something relevant to our code0
print Server.Run(my_long_command, 'local_time', 'Getting the local time on the server')
# returns the value of local_time on the server

# Lets run this method in a thread as not to hang our dcc app
print Server.Thread(my_long_command, 'Getting the local time on the server')

# I've decided that I need to add access to the DatabaseConnection module to the server
Server.Sync(DatabaseConnection) #Adds the path to the folder containing the Database module to sys.path of the server

# Now we can call commands from that module on the server
database_command='''
import DatabaseConnection as db
database = db.DatabaseConnection('{SQL Server}','NYCW-MHB-SQL','gradingStats')
results = database.sql_command("SELECT * FROM AvidEdits")
'''
print Server.Run(database_command, 'results', 'Querying Database') #returns results as a string

# Lets close the server for now
Server.Close()

-----------------------------
"""

import re
import os
import sys
import time
import socket
import traceback
import subprocess

from functools import wraps

import Settings

SERVER_HOST = '127.0.0.1'
SERVER_SOCKET = None
SERVER_PORT = 10000

# External Python Interpreter
MOBU_PORT = 4242

# Is the python executable visible ?
VISIBLE = False

# This will retrieve the MB version we are working in.

VERSION = os.path.realpath(__file__).split('motionbuilder')[1].split('\\')[0]

# --- Decorators ---


def Connect(func):
    """
    decorator for connecting to the server, starts it if is not already running

    Arguments:
        host (string): ip address or name associated with an ip address ei. localhome
        port (int): number of the port that you want to open to talk to this server
        current_socket (socket.socket): an instance of a socket if you already have on open
    """
    @wraps(func)
    def wrapper(self, *args, **kwargs):

        # Local scope bug work around
        # Assign current_socket to a local variable so that we can use the if statement on it

        # If there is no socket or the socket is the same as the server socket
        # create a new socket to speak with the local server

        self.socket = socket.socket()

        # Try to connect to the local server and run the function
        try:
            self.socket.connect((self.host, self.port))
            return func(self, *args, **kwargs)

        except socket.error:

            # The connection failed so we are assuming the server isn't running
            # We are going to attempt to start it

            # Start Server
            if not self.Start():
                print "External server could not be started"
                return

            # This is a check to make sure that the newly spawned server has time to get associate the port number
            # to itself. There have been occasions were we try to connect to the server after it has been spawned BUT
            # has yet to hit the code that reserves the port number. This causes errors on the client side, making us
            # run Connection() again when we don't have to. Instead we are going to try to connect to the server
            # every .1 of a second until a second elapses or a connection is made.

            flag = 0
            start = time.time()
            success = False
            while not int(flag):
                try:
                    self.socket.connect((self.host, self.port))
                    success = True
                    break

                except:
                    flag += .1
                    print "Establishing connection to the server\nElapsed Time {}".format(time.time() - start)
                    time.sleep(.1)
            if success:
                return func(self, *args, **kwargs)

        finally:
            # After the func has finished , we want to close our socket to stop the connection
            self.socket.close()
            # We don't need to keep the socket in memory anymore, so we are just going to delete it.
            del self.socket

    return wrapper


class Connection(object):
    """
    Object for interacting with the remote server
    """
    __host = "127.0.0.1"

    def __init__(self, port="Default", force=True, useMotionBuilder=False):
        """
        Constructor

        Arguments:
            port (int or string): port to use for the local server. Int values must be between 11000 and 110099 and
                  string values must be default values stored in the settings xml
            force (boolean): when the instance is create , launch the server
        """

        self.socket = None
        self.port = Settings.GetPortNumber(port)
        self._useMotionBuilder = useMotionBuilder
        if not self.IsConnected() and force:
            self.Run(log="Server Started")

    def __str__(self):
        """ Host and Port """
        return "{}:{}".format(self.host, self.port)

    def __SetByteRate(self, byte_rate):
        """
        informs the server of the size in bytes of the string that is going to be sent to the server.
        This is done to ensure that all data sent to the server is received.

        Arguments:
            byte_rate (int): size of string in bytes that is going to be sent to the server

        Return:
            string
        """

        return self.Send('log="--- Preparing the server to run command ---"\nbyte_rate={byte_rate}'.format(
            byte_rate=byte_rate))

    def IsConnected(self):
        """ If the port is being used associated with server is in use"""
        temporarySocket = socket.socket()
        isConnected = False

        try:
            temporarySocket.bind(('127.0.0.1', self.port))
        except Exception:
            isConnected = True
        finally:
            temporarySocket.close()

        return isConnected  # self.port in Settings.GetOpenPorts()

    @property
    def host(self):
        """ the ip address of the host machine"""
        return self.__host

    def Start(self):
        """
        solves which project the tool is currently running in and launches the local python server

        Return:
            True
        """
        if self._useMotionBuilder:
            return self.StartMotionBuilder()
        return self.StartPython()

    def StartMotionBuilder(self):
        """
        Sends command to start up a mobu 'server'
        WARNING: Does not work from inside Motion Builder

        Returns:
            True
        """
        executableName = "motionbuilder_custom.exe"
        toolsPath = GetEnvironmentVariable("RS_TOOLSROOT")
        serverPyFile = os.path.join(os.path.dirname(__file__), 'MotionBuilderLauncher.py')
        with open(serverPyFile, "w+") as _:
            _.write("import os\n"
                    "os.environ['MotionBuilderBatchMode'] = 'True'\n"
                    "print os.environ['MotionBuilderBatchMode']\n"
                    "from RS.Core.Server import Settings\n"
                    "Settings.SetCurrentPort(11005)\n"
                    "from RS.Core.Server import RemoteServer\n")

        mobuLauncherPath = os.path.join(toolsPath, "techart", "bin",
                                        "MotionbuilderLauncher", "MotionbuilderLauncher.exe")

        launchCommand = ('{} -major_build "2016" -minor_build "CUT2" '
                         '-profile "TechArt" -native_args "console suspendMessages batch" {}').format(mobuLauncherPath,
                                                                                                      serverPyFile)

        previousMemory = 0
        existingProcesses = GetActiveProcess(executableName).keys()

        try:
            subprocess.Popen(launchCommand)
        except Exception:
            return False

        # Wait for approximately 10 seconds for Motion Builder to start running
        # Attempt to get the processId for the opened Motion Builder session
        for index in xrange(5):
            currentProcesses = GetActiveProcess(executableName)
            for processId, memory in currentProcesses.iteritems():
                if processId not in existingProcesses:
                    previousMemory = memory
                    break

            if previousMemory:
                break

            elif index >= 4:
                print "Could not find the new motion builder exec"
                return False

            else:
                print "Looking for the new motion builder session"
                time.sleep(5)

        print "Found the Motion Builder session. Id : ", processId
        # Wait until Motion Builder is finished opening by tracking memory usage
        # When memory usage stabilizes, motion builder has finished opening.
        while True:
            memory = GetActiveProcess(executableName).get(processId, 0)
            if memory < 1:
                print "Memory {}".format(memory)
                return False

            if previousMemory != memory:
                previousMemory = memory
                print "Waiting for Motion Builder to stabilize"
                time.sleep(2)
            else:
                break
        print "Motion Builder has stabilized"
        return True

    def StartPython(self):
        """ Starts external python executable """
        # Get path to the techart tools directory
        pythonExecutable = GetEnvironmentVariable('RS_TOOLSPYTHON')
        # Uncomment when finalized, it needs to look at WildWest while testing for now

        if not VISIBLE:
            pythonExecutable = pythonExecutable.replace('python.exe', 'pythonw.exe')

        Settings.SetCurrentPort(self.port)
        serverPyFile = os.path.join(os.path.dirname(__file__), 'RemoteServer.py')

        try:
            subprocess.Popen([pythonExecutable, serverPyFile], stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE).pid
        except WindowsError:
            return False

        return True

    def Close(self):
        """ Closes the Server """
        self.Run("Close(client_socket)", log="Server Closed")
        return True

    @Connect
    def Send(self, command='pass', request_parameter='', log='Connecting to the server'):
        """
        Sends the provided command to be executed on the server

        Argument:
            command (string): a python code to be executed on the server
            request_parameter (string): name of a parameter on the server that you want to be returned
            log (string): message that you want to print on the server/sent to log after a succesful connection

        Return:
            string
        """
        if not isinstance(command, basestring):
            command = str(command)

        self.socket.send(command)

        try:
            result = self.socket.recv(37)
            incoming_byte_size = 0
            if result:
                incoming_byte_size = int(result)

            return self.socket.recv(incoming_byte_size)

        except socket.error:
            print FullErrorStack()
            return None

        except Exception:
            print FullErrorStack()
            return None

    def Run(self, command='', request_parameter='', log='Connecting to the server'):
        """
        Sends the provided command to be executed on the server

        Argument:
            command (string): a python code to be executed on the server
            request_parameter (string): name of a parameter on the server that you want to be returned
            log (string): message that you want to print on the server/sent to log after a succesful connection

        Return:
            string
        """
        command = BuildCommand(command, request_parameter, log)
        self.__SetByteRate(sys.getsizeof(command))
        return self.Send(command, request_parameter, log)

    def Thread(self, command='pass', log='Threading Command'):
        """
        sends the command to the server and servers it

        Arguments:
            command (string): a python code to be executed on the server
            request_parameter (string): name of a parameter on the server that you want to be returned
            log (string): message that you want to print on the server/sent to log after a succesful connection

        Return:
            string
        """

        # Need to add Queue support for servers to ensure they all function properly
        server_command = ("def thread_command(*args, **kwargs):\n"
                          "{command}\n"
                          "thread_queue.add(thread_command)")

        command_lines = command.splitlines()
        command_lines.append("logging.debug('--- Thread Finished ---')")

        thread_command = ["\t\t{spaces}{each_line}".format(spaces=" " * (len(each_line) - len(each_line.strip())),
                                                           each_line=each_line.strip())
                          for each_line in command_lines if each_line.strip()]

        server_command = server_command.format(command="\n".join(thread_command))
        return self.Run(server_command, 'thread_queue.current_thread', log)

    def Sync(self, *modules, **kwargs):
        """
        syncs access to the modules you pass to the server or if nothing is provided, adds all paths from the
        sys.path of the client that are missing on the server

        Arguments:
            modules (list[module, etc.]): a list of modules that you want to pass to the server to use

        Keyword Arguments:
            paths (list[string, etc.]): list of paths to modules that you want the server to use

        Return:
            None
        """

        if not modules:
            modulePaths = sys.path

        else:
            modulePaths = []
            for each in modules:
                path = os.path.split(each.__file__)[0]
                if each.__file__.endswith('__init__.py') or each.__file__.endswith('__init__.pyc'):
                    path = os.path.split(path)[0]
                modulePaths.append(path)

        # Add optional paths if available
        modulePaths.extend(kwargs.get("paths", []))

        # remove duplicate paths
        modulePaths = list(set(modulePaths))

        modulePaths = str(modulePaths)
        command = '[sys.path.append(each) for each in {paths} if each not in sys.path]'.format(paths=modulePaths)
        # Sometimes getsizeof returns a number that is just a tad bit too low, so we extend it by a hundred bits
        # to ensure that we get back the entire strings in one go.
        sys_path_byte_size = sys.getsizeof(command) + 100
        self.__SetByteRate(sys_path_byte_size)

        self.Run(command, log='Adding client module paths to the server')
        return None


# --- Utility methods ---


def GetEnvironmentVariable(environment_var):
    """
    Gets the value associated with the environment variable passed

    Arguments:
         environment_var (string): the environment variable that you want to query

    Return:
        string
    """

    # Get the python tools executable
    osEnvironments = Settings.RunShellCommand('setenv')
    regularExpression = r"(?<={environment_var}:)[a-z0-9._:\t\b\\ ]+".format(environment_var=environment_var)
    searchResults = re.search(regularExpression, osEnvironments, re.M | re.I)

    if searchResults:
        tools_executable = searchResults.group().strip()
    else:
        raise "No system environment matching {environment_var} is setup".format(environment_var=environment_var)

    return tools_executable


def FullErrorStack():
    """
    gets the full error stack as string
    took from: http://stackoverflow.com/questions/6086976/how-to-get-a-complete-exception-stack-trace-in-python

    Return:
        string
    """
    exc = sys.exc_info()[0]
    stack = traceback.extract_stack()[:-1]  # last one would be full_stack()

    # i.e. if an exception is present
    # remove call of full_stack, the printed exception
    # will contain the caught exception caller instead

    if exc is not None:
        del stack[-1]

    trc = 'Traceback (most recent call last):\n'
    stackstr = trc + ''.join(traceback.format_list(stack))

    if exc is not None:
        stackstr += '  ' + traceback.format_exc().lstrip(trc)
    return stackstr


def BuildCommand(command, request_parameter='', log='Connecting to the server'):
    """
    creates the string that will be sent to the server to be executed

    Arguments:
        command (string): command to send to the server
        request_parameter (string): name of the variable whose value should be returned
        log (string): warning to log when the command is executed

    Return:
        string
    """
    command = "".join(("request_parameter = '{0}'\nlog='--- {log} ---'\nprint log\n".format(str(request_parameter),
                                                                                            log=log),
                      command))
    return command


def GetActiveProcess(process):
    """
    Gets a list of active process by their ids and the amount of memory they are consuming

    Arguments:
        process (string): name of the process to find

    return dictionary
    """
    output = subprocess.check_output('tasklist.exe',
                                  universal_newlines=True)
    processIds = {}
    for line in sorted(output.splitlines()[3:]):
        data = re.split("[ ]+", line)
        foundProcess, processId, memory = data[0], data[1], data[4]
        if foundProcess == process:
            processIds[processId] = memory
    return processIds


def CloseAll(force=False):
    """ Closes all the open python servers using ports 11000 through 11099 """
    if force:
        try:
            return subprocess.Popen("tskill pythonw"[:[None, -1][VISIBLE]], shell=True)

        except WindowsError:
            return os.system("tskill pythonw"[:[None, -1][VISIBLE]])

    for each_port in Settings.GetOpenPorts():
        Connection(each_port).Close()
