"""
Description:
    Server that lives outside motion builder and listens to commands sent from it.

Author:
    David Vega <david.vega@rockstargames.com>

About:

    The server is just a while loop that never ends, that has a socket associated with a port to listen for incoming
    requests from external applications. The open_port() method tries to associate a socket with a port and sets it to
    listen for incoming requests. The host and port are hardcoded to be '127.0.0.1' and 7331 respectively. If there is a
    port 7331 that is already associated with another application then the server won't start and commands from the
    client will fail.

    Whenever a request is accepted, data is received based on the current value of the byte_rate variable of the server.
    If the incoming string/data size is bigger than the current value of the byte_rate variable on the server, then part
    of the string/data will be cut off. The byte_rate will never be smaller than 1024.

    All data coming into the server gets executed through the exec() method.
    Every time data is successfully received and executed, it stamps the thread.log with a log based on the current
    value of the log variable of the server.

    If the request_parameter flag is set to some value other than '' or 'None', then the  server will attempt to find
    the value of a variable with that name and send it back to the client. If no value is set for the request_parameter
    variable than 'Connection Successful' is sent to the client. The server always sends to the client the size of the
    string it is about to send it before sending it the actual results to make sure that the client receives the entire
    string and not just a section of it.

    This server also has a ThreadQueue class for storing commands that are meant tobe threaded into a list. The server
    constantly calls the ThreadQueue's next() method to make sure it goes through all the commands it has queued up. It
    also keeps track if the Queue is waiting for a command to finish by check the busy variable/flag. We do this to make
    sure that no two methods are running that access the same file/database, which may lead to errors.

    The server has the methods send() and kill() that function similar to the same methods the __init__.py contains.
    send() sends data back to client and kill closes the server. These two methods take the additional argument
    client_socket, which is the socket that is connected to the client.

    It also has two utility methods, get_log_path() for getting the path of the log and modules_on_server() for getting
    a list of the current modules accessible on the server.

"""
import os
import sys
import thread
import logging
import traceback
import pkgutil


def ShowExceptionAndExit(exc_type, exc_value, tb):
    """
    Took from http://stackoverflow.com/questions/779675/stop-python-from-closing-on-error

    When an error occurs, wait for user input

    Arguments:
        exc_type (Exception): type of error
        exc_value (string): the error message
        tb (traceback): where the error occurred

    """
    import traceback
    traceback.print_exception(exc_type, exc_value, tb)
    raw_input("Press key to exit.")
    sys.exit(-1)

# Only add this as the exception hook if we are outside of Motion Builder
# This is to ensure that the python window doesn't close automatically if there is an error
if sys.excepthook.__module__ != "RS.Utils.Exception" and "pythonw" not in sys.executable:
    sys.excepthook = ShowExceptionAndExit

# Settings imports cElementTree, which can error out if the environment variables are not pointing to the
# libraries compiled against the version of python in the project tools bin.
import Settings

host = '127.0.0.1'
port = 7331

client_port = 4242
client_socket = None

log = '--- Connected ---'
byte_rate = 1024

motionBuilderBatchMode = os.environ.get("MotionBuilderBatchMode", "False")


class Borg(object):
    """ Singleton like pattern except that all instances share the same data """
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state


# Send data back to the client telnet commands
class ThreadQueue(Borg):
    """
    This class is for queueing up commands that have been requested to be threaded.

    It keeps track if the last thread has finished by keeping track of the busy flag.
    When the thread is busy and has commands queued up ,it will not start a new thread until the previous thread
    finishes. This is to ensure that no two commands that are threaded and trying to access the same file/database
    come into conflict with one another. We also use methods instead of assigning values directly on the object to
    ensure that the values are changing.

    Just setting the values in while the method was being threaded didn't cause the value to retain its updated status
    of busyness.
    """
    queue = []
    busy = False
    current_thread = 0

    def next(self):
        """ runs the next command in queue """
        if not self.busy and self.queue:
            self.busy = True
            self.current_thread = thread.start_new_thread(self.queue.pop(0), (self,))
        elif not self.busy:
            self.current_thread = 0

    def add(self, command):
        """ adds command to queue """

        def threadedCommand(self):
            try:
                command()
            except Exception:
                logging.error(FullErrorStack())
            self.setBusy(False)
            logging.debug("Thread {} Finished".format(self.current_thread))
        self.queue.append(threadedCommand)

    def isBusy(self):
        """ returns if the thread is busy """
        return self.busy

    def setBusy(self, is_busy=False):
        """ sets the thread to be busy """
        self.busy = is_busy
        return self.busy


def Send(value, client_socket):
    """
    Sends information back to the client

    First we send the client an int as a string to let it now how big the incoming message/value is.
    Then we send the actual message/value now that the client knows how much data to accept. We do
    this to make sure that all data is successfully received by the client and nothing gets cut off.

    Argument:
        value (string): string to send back to client
        client_socket (socket.socket): the client's socket
    """
    value_as_string = str(value)
    value_size = sys.getsizeof(value_as_string)
    client_socket.send(str(value_size))
    client_socket.send(str(value))


def ExecuteInMotionBuilder(code=""):
    """
    Send code to be executed in motion builder.
    This is a one trick pony as we are taking advantage of Motion Builders file watching callbacks for it to execute
    the code.

    Arguments:
        code (string): code that you want to execute in motion builder
    """
    # Convert code into a python file

    test = open("{}\ToMobu.py".format(os.path.split(__file__)[0]), 'w')
    test.writelines(code.splitlines())
    test.close()

    # edit the mobuRunRemoteScript to execute the recently created code
    test = open("x:\mobuRunRemoteScript.txt", 'w')
    test.write("-script -ignoreWing -silent;{}\ToMobu.py".format(os.path.split(__file__)[0]))
    test.close()


def OpenPort():
    """ opens port to listen for commands """
    # We have been running into an error where python tries to access the wrong socket module
    # Thus crashing stopping the server, this is to log that bug when it happens if it happens.

    try:
        import socket

        global port
        port = Settings.GetCurrentPort()

        # Create a port to talk to python
        python_socket = socket.socket()

        # Connect the local host to the port
        python_socket.bind((host, port))

        # Wait for a connection, up to five
        python_socket.listen(5)

        return python_socket

    except Exception:
        logging.error(traceback.format_exc())


def Close(client_socket=None):
    """ kills the server """

    if client_socket:
        Send("server closed", client_socket)
        client_socket.close()

    # Log that the server is closed
    logging.warning(log)
    # If there are any commands on the queue, have them finish up
    while thread_queue.queue:
        thread_queue.next()
    # kill the server
    sys.exit(0)


def ModulesOnServer():
    """
    A list of all modules accessible on the server

    Return:
        string
    """
    return "".join(["{0},".format(each[1]) for each in pkgutil.iter_modules()])


def GetLogPath():
    """
    Gets the path to the log
    Return:
        string
    """
    package_path = os.path.split(__file__)[0]
    return os.path.join(package_path, "thread.log")


def FullErrorStack():
    """
    gets the full error stack as string
    took from: http://stackoverflow.com/questions/6086976/how-to-get-a-complete-exception-stack-trace-in-python

    Return:
        string
    """
    exc = sys.exc_info()[0]
    stack = traceback.extract_stack()[:-1]  # last one would be full_stack()

    # i.e. if an exception is present
    # remove call of full_stack, the printed exception
    # will contain the caught exception caller instead

    if exc is not None:
        del stack[-1]

    trc = 'Traceback (most recent call last):\n'
    stackstr = trc + ''.join(traceback.format_list(stack))

    if exc is not None:
        stackstr += '  ' + traceback.format_exc().lstrip(trc)
    return stackstr


# Old function names for compatibility with old code


def get_log_path(*args, **kwargs):
    return GetLogPath(*args, **kwargs)


def modules_on_server():
    return ModulesOnServer()


def kill(*args):
    return Close(*args)


# Logging
# Cannot use Universal Log as clr module isn't compiled for the external python executable used during development

logging.basicConfig(filename=get_log_path(), level=logging.DEBUG, format='%(levelname)s %(asctime)s : %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

thread_queue = ThreadQueue()

# Set up connection to send commands back to mobu with telnet
# Creating sockets doesn't give us automatic reading of the data being sent

if __name__ == "__main__" or motionBuilderBatchMode == "True":

    python_socket = OpenPort()
    print "--- MotionBuilder Local Server: Port {} ---".format(port)
    while True:

        try:
            # Accept a connection coming in from the client
            client_socket, address = python_socket.accept()
            request_parameter = ''

            # Set the default byte rate accept size to 1024 if it is too small
            if byte_rate < 1024:
                byte_rate = 1024
            # Get the incoming data/command from the client
            command = client_socket.recv(byte_rate)

            if command:
                # If data is received, execute it
                exec command
                logging.debug(log)

            # Check the queue to execute any code on the queue list
            thread_queue.next()

            # If a parameter from the server (a.k.a. request parameter)
            # is asked by the client, return that value instead of the
            # default response "Connection Successful"
            if request_parameter and request_parameter != 'None':

                # If the request parameter is a nested parameter
                # split up the string to retrieve the value through the loop
                request_parameter_split = request_parameter.split(".")
                request_parameter = ""
                join_list = []

                current_module = sys.modules[__name__]
                value = ''

                for each_num, each in enumerate(request_parameter_split):
                    value = getattr(current_module, each)
                    current_module = value

                # Send it to the client
                Send(value, client_socket)

            else:
                Send('Connection Successful', client_socket)

        except Exception, e:
            error = FullErrorStack()
            logging.warning(error)
            Send(error, client_socket)

        finally:
            client_socket.close()
