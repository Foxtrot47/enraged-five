"""
Description:
    Methods for getting the settings from the Settings.xml file

Authors:
    David Vega <david.vega@rockstargames.com>
"""
import os
import re
import stat
import subprocess

# cElementTree can error out if the environment variables are not pointing to the
# libraries compiled against the version of python in the project tools bin.

try:
    import xml.etree.cElementTree as xml
except ImportError:
    import xml.etree.ElementTree as xml

PACKAGE_PATH = os.path.split(__file__)[0]
SETTINGS_PATH = os.path.join(PACKAGE_PATH, "Settings.xml")


def GetPortNumber(port):
    """
    Argument :
        port = int or string; number of the port you want the next server that starts up to use. If a string
                is passed, it tries to find a port number associated with that name in the Settings.xml.
                If no match is found, the port number defaults to 11000

    """
    xml_tree = xml.parse(SETTINGS_PATH)
    settings_element = xml_tree.getroot()
    port_element = settings_element.find("./Ports/{}".format(port))

    # For whatever reason, xml elements are not treated as true
    if port_element is not None:
        port = int(port_element.text)

    elif isinstance(port, basestring):
        port = 11000

    return port


def GetCurrentPort():
    """
    Reads the settings xml to determine which port should be opened
    """
    # Open XML
    xml_tree = xml.parse(SETTINGS_PATH)
    settings_element = xml_tree.getroot()
    current_port, ports = [each for each in settings_element]

    port = int(ports.find("Default").text)
    if current_port.text:
        port = int(current_port.text)
        current_port.text = ""

    return port


def SetCurrentPort(port):
    """
    Sets the port number to be used for the new server
    Argument :
        port = int or string; number of the port you want the next server that starts up to use. If a string
                is passed, it tries to find a port number associated with that name in the Settings.xml.
                If no match is found, the port number defaults to 11000
    """

    # Open XML
    xml_tree = xml.parse(SETTINGS_PATH)
    settings_element = xml_tree.getroot()
    current_port, ports = [each for each in settings_element]

    if not isinstance(port, basestring):
        port = str(port)

    if not port.isdigit():
        port = ""

    current_port.text = port
    fileStat = os.stat(SETTINGS_PATH)[0]
    if fileStat is not stat.S_IWRITE:
        os.chmod(SETTINGS_PATH, stat.S_IWRITE)
    xml_tree.write(SETTINGS_PATH)
    os.chmod(SETTINGS_PATH, fileStat)


def GetOpenPorts():
    """
    Gets the current ports between 10,000 and 10,099 that are open.
    These ports are meant to only be used by the server module

    :return: list
    """
    try:
        raw_output = subprocess.check_output(r'netstat -n -atp tcp | grep -i "listen" | findstr 127.0.0.1',
                                             stdin=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

    except WindowsError:
        results = os.popen(r'netstat -n -atp tcp | grep -i "listen" | findstr 127.0.0.1')
        raw_output = results.read()
        results.close()

    return [int(each) for each in re.findall("(?<=127.0.0.1:)110[0-9]{2}", raw_output, re.MULTILINE)]


def RunShellCommand(command):
    """
    Attempts to use subprocess to run the command but defaults to the os module if it runs into an error.
    There is a bug with the subprocess module when being ran in a windows application that is in interactive mode

    Argument:
        command: string; shell command to run

    Return:
        string
    """
    try:
        raw_output = subprocess.check_output(command, shell=True)

    except WindowsError:
        results = os.popen(command)
        raw_output = results.read()
        results.close()

    return raw_output