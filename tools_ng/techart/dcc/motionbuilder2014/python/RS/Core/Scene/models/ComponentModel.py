"""
Custom Model for getting a list of components in Motion Builder
"""

from PySide import QtCore

import pyfbsdk as mobu

from RS import Globals

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel
from RS.Core.Scene.models.modelItems import ComponentModelItem

COMPONENT_DICTIONARY = {
    mobu.FBModel: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModel)],
    mobu.FBModelSkeleton: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModelSkeleton)],
    mobu.FBModelNull: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModelNull)],
    mobu.FBModelMarker: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModelMarker)],
    mobu.FBModelPath3D: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModelPath3D)],
    mobu.FBModelCube: lambda *_:  [model for model in Globals.Components if isinstance(model, mobu.FBModelCube)],
    mobu.FBCamera: lambda *_: [camera for camera in Globals.Cameras if not camera.SystemCamera],
    mobu.FBComponent: lambda *_: Globals.Components,
    mobu.FBCharacter: lambda *_: Globals.Characters,
    mobu.FBCharacterExtension: lambda *_: Globals.CharacterExtensions,
    mobu.FBCharacterPose: lambda *_: Globals.CharacterPoses,
    mobu.FBConstraint: lambda *_: Globals.Constraints,
    mobu.FBConstraintSolver: lambda *_: Globals.ConstraintSlovers,
    mobu.FBTake: lambda *_: Globals.Takes,
    mobu.FBTexture: lambda *_: Globals.Textures,
    mobu.FBVideoClip: lambda *_: Globals.VideoClips,
    mobu.FBShader: lambda *_: Globals.Shaders,
    mobu.FBGroup: lambda *_: Globals.Groups,
    mobu.FBFolder: lambda *_: Globals.Folders,
    mobu.FBActor: lambda *_: Globals.Actors,
    mobu.FBActorFace: lambda *_: Globals.ActorFaces,
    mobu.FBControlSet: lambda *_: Globals.ControlSets,
    mobu.FBDevice: lambda *_: Globals.Devices,
    mobu.FBLight: lambda *_: Globals.Lights,
    mobu.FBMaterial: lambda *_: Globals.Materials,
    mobu.FBNote: lambda *_: Globals.Notes,
    mobu.FBSet: lambda *_: Globals.Sets,
    mobu.FBMotionClip: lambda *_: Globals.Scene.MotionClips,
    mobu.FBMarkerSet: lambda *_: Globals.MarkerSets,
    mobu.FBObjectPose: lambda *_: Globals.ObjectPoses}


class ComponentModel(baseModel.BaseModel):
    """
    model types generated and set to data for the the model item
    """

    def __init__(self, componentType=(mobu.FBComponent,), parent=None):
        """
        Constructor

        Arguments:
            component (pyfbsdk.FBComponent): component whose information should be stored
            parent (QtGui.QWidget): parent widget

        """
        self.componentTypes = componentType
        if not isinstance(self.componentTypes, (list, tuple)):
            self.componentTypes = [self.componentTypes]
        self._showLongName = False
        self._title = " ".join([component.__name__[2:] for component in self.componentTypes])
        super(ComponentModel, self).__init__(parent=parent)

    def headerData(self, section, orientation, role):
        """
        heading strings to be displayed in the view
        """
        if role == QtCore.Qt.DisplayRole:
            return [self._title]

    def setHeader(self, headerTitle):
        """
        Sets the header

        Arguments:
            headerTitle (string): value to set for the header
        """
        self._title = headerTitle

    def __components(self):
        if len(self.componentTypes) == 1 and self.componentTypes[0] in COMPONENT_DICTIONARY:
            return COMPONENT_DICTIONARY.get(self.componentTypes[0])()
        return [component for component in Globals.Components if isinstance(component, tuple(self.componentTypes))]

    def setupModelData(self, parent):
        """
        model type data sent to model item
        """
        self._components = {}

        # convert generators to lists so the values do not change 
        for component in self.__components():
            item = ComponentModelItem.ComponentItem(component, parent=parent)
            parent.appendChild(item)
            self._components[component] = item

    def data(self, index, role):
        """
        Overrides built-in method;
        Determines what stored information is returned based on the provided index and role

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get information from
            role (QtCore.Qt.QRole): role that is being requested by the data

        """
        if not index.isValid():
            return None
        item = index.internalPointer()
        if item == self:
            return None
        elif item is None:
            item = self.rootItem.child(index.row())

        if self._showLongName and role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            return item.data(index, QtCore.Qt.ToolTipRole)

        return item.data(index, role)

    def flags(self, index):
        """
        flags added to determine column properties

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get flag info from

        Return:
            QtGui.QFlags
        """
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        if not self.componentTypes:
            return QtCore.Qt.NoItemFlags

        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        return flags

    def reset(self, control=None, event=None):
        """
        Resets the model

        Arguments:
            control (pyfbsdk.FBComponent): object calling the event
            event (pyfbsdk.FBEvent): the event being triggered

        """
        # This is to limit the amount of times the model is updated
        if event is None or event.Type in [mobu.FBSceneChangeType.kFBSceneChangeDetach,
                                           mobu.FBSceneChangeType.kFBSceneChangeRenameUnique]:
            super(ComponentModel, self).reset()

    def getItemByComponent(self, component):
        """
        Gets the ComponentModelItem that corresponds to the given component

        Arguments:
            component (pyfbsdk.FBComponent): component whose corresponding index should be returned

        Return:
            RS.Tools.UI.MovingCutscene.Model.ComponentModelItem.ComponentItem()
        """
        return self._components.get(component, None)

    def setShowLongName(self, value):
        """
        Sets if the full name of the component should be shown

        Arguments:
            value (boolean): whether the long name should be shown or not
        """
        self._showLongName = value

    def clear(self):
        """ Clears the model """
        super(ComponentModel, self).reset()
