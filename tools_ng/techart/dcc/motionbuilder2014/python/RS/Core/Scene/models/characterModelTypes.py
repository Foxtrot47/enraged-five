from PySide import QtCore

from RS import Globals
from RS.Core.Scene.models.modelItems import characterModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class CharacterModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        self._checkable = True
        super(CharacterModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['Character Name',]

    def setCheckable(self, val):
        """
        Set if the model is checkable or not
        
        args: val (bool)
        """
        self._checkable = val
        
    def checkable(self):
        """
        Gets if the model is checkable or not
        
        Returns:
            bool
        """
        return self._checkable

    def _getCharacterDict(self):
        """
        Get list of characters in the scene
        
        Returns:
            Dictionary (String:FBCharacter) -
            namespaces of the character in the scene: FBCharacter object
        """
        characterDict = {}
        for character in Globals.Characters:
            nameSplit = character.LongName.split(":")
            namespace = nameSplit[0]
            characterDict[namespace] = character
        return characterDict

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        characterDict = self._getCharacterDict()
        # iterate through list and create dataItem for treeview
        for key, value in characterDict.iteritems():
            dataItem = characterModelItem.CharacterTextModelItem(key,
                                                                value,
                                                                parent=parent)
            # append tree view with item
            parent.appendChild(dataItem)

    def flags(self, index):
        '''
        flags added to determine column properties
        
        args: index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        
        if self._checkable:
            flags |= QtCore.Qt.ItemIsUserCheckable
        return flags
