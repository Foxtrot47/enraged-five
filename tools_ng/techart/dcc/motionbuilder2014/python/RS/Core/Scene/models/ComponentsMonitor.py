"""
Module for connecting code to be executed when an event from FBSceneChange or FBTakeChange is called on a component
of a given pyfbsdk class type.

Example:
import pyfbsdk as mobu

from RS.Core.Scene.models import ComponentsMonitor

cameraMonitor = ComponentsMonitor.ComponentsMonitor(mobu.FBCamera)

def printNewCameraName(event, countChanged):
    if countChanged:
        print event.ChildComponent.LongName

# Add method to be called when a new camera is created
cameraMonitor.attached.connect(printNewCameraName)

# Create Camera
mobu.FBCamera("My New Camera")

# Remove method that is called when a new camera is created
cameraMonitor.attached.disconnect(printNewCameraName)

"""
import pyfbsdk as mobu

from RS import Globals


class SceneChangeSignal(object):
    """
    Signal class that holds the functions/methods to execute.
    """
    def __init__(self, eventType, parent):
        """
        Constructor

        Arguments:
            eventType (pyfbsdk.FBEventName): The type of event this signal belongs to
            parent (ComponentsMonitor): The monitor class this signal belongs to
        """
        self._eventType = eventType
        self._parent = parent
        self._functions = []

    def __str__(self):
        """
        Overrides built-in method.
        Displays the object address and the event type it belongs to
        """
        return "{} of event {}".format(object.__str__(self), self._eventType)

    def eventType(self):
        """ The type of event this signal belongs to"""
        return self._eventType

    def connect(self, func):
        """ Connects/adds a method to be executed when the event this object belongs to is called """
        if func is not None:
            self._functions.append(func)

    def disconnect(self, func=None):
        """
        Removes the given function from this signal.
        If this signal is empty, then the parent monitor's cleanup method.

        Arguments:
            func (method/function): method/function to attach to this signal.

        """
        if func is not None and func in self._functions:
            self._functions.remove(func)
        else:
            # deleting contents of function just to be safe that we are removing everything
            del self._functions
            self._functions = []

        if not self._functions:
            self._parent.cleanup(self._eventType)

    def emit(self, event, countChanged):
        """
        Emits the signal, executing the stored methods/functions

        Arguments:
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method
            countChanged (boolean): the number of the components being monitored in the scene has changed

        """
        for function in self.functions():
            try:
                function(event, countChanged)
            except Exception, e:
                self.disconnect(function)

    def functions(self):
        """ the functions attached to this signal """
        return tuple(self._functions)


class ComponentsMonitor(object):
    """
    Class for registering commands that should go off when an object of given type has been selected, unselected,
    created, deleted, renamed, etc.
    """
    # --- Events ---

    _None = mobu.FBSceneChangeType.kFBSceneChangeNone
    # Unknown event
    TakeAdd = mobu.FBTakeChangeType.kFBTakeChangeAdded
    # Take Added
    TakeRemove = mobu.FBTakeChangeType.kFBTakeChangeRemoved
    # Take Removed
    TakeOpened = mobu.FBTakeChangeType.kFBTakeChangeOpened
    # Take Opened
    TakeClosed = mobu.FBTakeChangeType.kFBTakeChangeClosed
    # Take Closed
    TakeRenamed = mobu.FBTakeChangeType.kFBTakeChangeRenamed
    # Take Renamed
    TakeUpdated = mobu.FBTakeChangeType.kFBTakeChangeUpdated
    # Take Updated
    TakeMoved = mobu.FBTakeChangeType.kFBTakeChangeMoved
    # Take Moved
    TakeNone = mobu.FBTakeChangeType.kFBTakeChangeNone
    # Unknown Take event
    Destroy = mobu.FBSceneChangeType.kFBSceneChangeDestroy
    # Object destroyed.
    Attach = mobu.FBSceneChangeType.kFBSceneChangeAttach
    # Object attached.
    Detach = mobu.FBSceneChangeType.kFBSceneChangeDetach
    # Object detached.
    AddChild = mobu.FBSceneChangeType.kFBSceneChangeAddChild
    # Child added.
    RemoveChild = mobu.FBSceneChangeType.kFBSceneChangeRemoveChild
    # Child removed.
    Select = mobu.FBSceneChangeType.kFBSceneChangeSelect
    # Object selection.
    Unselect = mobu.FBSceneChangeType.kFBSceneChangeUnselect
    # Object deselection.
    Rename = mobu.FBSceneChangeType.kFBSceneChangeRename
    # Before object rename.
    RenamePrefix = mobu.FBSceneChangeType.kFBSceneChangeRenamePrefix
    # Before object rename prefix.
    RenameUnique = mobu.FBSceneChangeType.kFBSceneChangeRenameUnique
    # Before object rename unique.
    RenameUniquePrefix = mobu.FBSceneChangeType.kFBSceneChangeRenameUniquePrefix
    # Before object rename unique prefix.
    Renamed = mobu.FBSceneChangeType.kFBSceneChangeRenamed
    # After object rename.
    RenamedPrefix = mobu.FBSceneChangeType.kFBSceneChangeRenamedPrefix
    # After object rename prefix.
    RenamedUnique = mobu.FBSceneChangeType.kFBSceneChangeRenamedUnique
    # After object rename unique.
    RenamedUniquePrefix = mobu.FBSceneChangeType.kFBSceneChangeRenamedUniquePrefix
    # After object rename unique prefix.
    SoftSelect = mobu.FBSceneChangeType.kFBSceneChangeSoftSelect
    # Soft selection.
    SoftUnselect = mobu.FBSceneChangeType.kFBSceneChangeSoftUnselect
    # Soft deselection.
    HardSelect = mobu.FBSceneChangeType.kFBSceneChangeHardSelect
    # Hard selection.
    Activate = mobu.FBSceneChangeType.kFBSceneChangeActivate
    # Activate.
    Deactivate = mobu.FBSceneChangeType.kFBSceneChangeDeactivate
    # Deactivate.
    LoadBegin = mobu.FBSceneChangeType.kFBSceneChangeLoadBegin
    # Begin loading file.
    LoadEnd = mobu.FBSceneChangeType.kFBSceneChangeLoadEnd
    # End loading file.
    ClearBegin = mobu.FBSceneChangeType.kFBSceneChangeClearBegin
    # Begin clearing file (file new).
    ClearEnd = mobu.FBSceneChangeType.kFBSceneChangeClearEnd
    # End clearing file (file new).
    TransactionBegin = mobu.FBSceneChangeType.kFBSceneChangeTransactionBegin
    # Begin transaction.
    TransactionEnd = mobu.FBSceneChangeType.kFBSceneChangeTransactionEnd
    # End transaction.
    ReSelect = mobu.FBSceneChangeType.kFBSceneChangeReSelect
    # Re-selection.
    ChangeName = mobu.FBSceneChangeType.kFBSceneChangeChangeName
    # Object change name.
    ChangedName = mobu.FBSceneChangeType.kFBSceneChangeChangedName
    # Object changed name.
    PreParent = mobu.FBSceneChangeType.kFBSceneChangePreParent
    # Before object parenting.
    PreUnparent = mobu.FBSceneChangeType.kFBSceneChangePreUnparent
    # Before object unparenting.
    Focus = mobu.FBSceneChangeType.kFBSceneChangeFocus
    # Object have focus.
    ChangedParent = mobu.FBSceneChangeType.kFBSceneChangeChangedParent
    # Object changed parent.
    Reorder = mobu.FBSceneChangeType.kFBSceneChangeReorder
    # Object reorder.
    Reordered = mobu.FBSceneChangeType.kFBSceneChangeReordered
    # Object reordered.
    FileOpenCompleted = mobu.FBEventName.kFBEventFileOpenCompleted
    # File Open Completed

    _SHARED_EVENTS = {_None: TakeNone,
                      Attach: TakeAdd,
                      Detach: TakeRemove,
                      Renamed: TakeRenamed,
                      Reorder: TakeMoved}

    def __init__(self, componentType=mobu.FBComponent, parent=None):
        """
        Constructor

        Arguments:
            componentType(mobu.FBComponent): pyfbsdk component type to monitor in the current scene

        """

        self._functions = {}
        self._componentType = componentType
        self._previousCount = self.count()
        self._externalDisable = False
        self._internalDisable = False
        self._callbacksAdded = False
        self._parentMonitor = parent

    def _getSignal(self, eventType):
        """
        Gets the signal for the given FBEvent Type

        Arguments:
            eventType (mobu.FBEvent): FBEvent called by the SceneChange/TakeChange callbacks

        Return:
            SceneChangeSignal
        """

        if not self._functions:
            self.addMotionBuilderCallbacks()

        if not self._functions.get(eventType, None):
            self._functions[eventType] = SceneChangeSignal(eventType, parent=self)
        return self._functions[eventType]

    def _execute(self, control, event):
        """
        Executes all the stored functions.
        All stored functions have the event from the mobu callbacks
        passed in as an argument

        Arguments:
            control (object): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method

        """
        force = event.Type is self.FileOpenCompleted
        if not self.isEnabled() and not force:
            return

        self._internalDisable = True

        eventType = event.Type
        if self._componentType == mobu.FBTake:
            eventType = self._SHARED_EVENTS.get(event.Type, event.Type)

        # This particular if statement is here just to limit the amount of calls made to check the component scene count
        # as it may slow down mobu due to multiple callbacks getting triggered after a single click or user action.

        if self._functions.get(eventType, None):
            countChanged = (self.count() != self._previousCount) or force

            # emit signal
            self._functions[eventType].emit(event, countChanged)

            self._previousCount = self.count()
        self._internalDisable = False

    def connect(self, func=None, eventType=None):
        """
        Adds function to be executed when the contents of the scene changes

        Arguments:
            func (function): function to add to be executed

        """
        if self._componentType == mobu.FBTake:
            eventType = self._SHARED_EVENTS.get(eventType, eventType)

        if func is not None:
            self._getSignal(eventType).connect(func)
        
    def disconnect(self, func=None, eventType=None):
        """
        Removes function that is being executed when the contents of the scene changes

        Arguments:
            func (function): function to remove from being executed

        """
        if self._componentType == mobu.FBTake:
            eventType = self._SHARED_EVENTS.get(eventType, eventType)

        signal = self._getSignal(eventType)

        if func is not None and func in signal.functions():
            signal.disconnect(func)
        else:
            # deleting contents of function just to be safe that we are removing everything
            signal.disconnect()

    def cleanup(self, eventType):
        """
        Removes teh signal associated with the given event type and removes Motion Builder callbacks if monitor
        has no signals.

        Arguments:
            eventType (pyfbsdk.FBEventName): the type of event whose signal should be removed

        """
        self._functions.pop(eventType)
        if not self._functions:
            self.removeMotionBuilderCallbacks()

    def count(self):
        """
        Number of components in the scene of the type an instance of this class is monitoring

        Return:
            int
        """
        componentList = Globals.ComponentLists.get(self._componentType, None)

        if componentList is None:
            return len([component for component in Globals.Components if isinstance(component, self._componentType)])
        return len(componentList)

    def addMotionBuilderCallbacks(self):
        """ Adds methods to be triggered by Motion Builder """
        if self._callbacksAdded:
            return

        # Add callback to update the model whenever an object is created/deleted in the scene
        if self._componentType == mobu.FBTake:
            Globals.Callbacks.OnTakeChange.Add(self._execute)
        else:
            Globals.Callbacks.OnChange.Add(self._execute)
        Globals.Callbacks.OnFileOpenCompleted.Add(self._execute)
        self._callbacksAdded = True

    def removeMotionBuilderCallbacks(self):
        """ Removes methods to be triggered by Motion Builder """
        # Add callback to update the model whenever an object is created/deleted in the scene
        if self._componentType == mobu.FBTake:
            Globals.Callbacks.OnTakeChange.Remove(self._execute)
        else:
            Globals.Callbacks.OnChange.Remove(self._execute)
        Globals.Callbacks.OnFileOpenCompleted.Remove(self._execute)

        self._callbacksAdded = False

    def isEnabled(self):
        """ Is the component monitor enabled """
        return not (self._externalDisable or self._internalDisable)

    def setEnabled(self, enable):
        """
        Set the enable state of the monitor

        Arguments:
            enable (boolean): enable state to set for th monitor
        """
        self._externalDisable = enable is False

    def parent(self):
        """ The parent monitor """
        return self._parentMonitor

    def setParent(self, parent):
        """
        Sets the parent monitor

        Argument:
            parent (RS.Core.Scene.model.SceneMonitor.SceneMonitor): parent monitor

        """
        self._parentMonitor = parent

    @property
    def attached(self):
        """ component attached signal """
        return self._getSignal(self.Attach)

    @property
    def detached(self):
        """ component detached signal """
        return self._getSignal(self.Detach)

    @property
    def destroyed(self):
        """ component destroyed signal """
        return self._getSignal(self.Destroy)

    @property
    def childAdded(self):
        """ child added to a component signal """
        return self._getSignal(self.AddChild)

    @property
    def childRemoved(self):
        """ child removed from a component signal """
        return self._getSignal(self.RemoveChild)

    @property
    def selected(self):
        """ component selected signal """
        return self._getSignal(self.Select)

    @property
    def unselected(self):
        """ component deselected """
        return self._getSignal(self.Unselect)

    @property
    def preRename(self):
        """ component is about to be renamed signal """
        return self._getSignal(self.Rename)

    @property
    def preRenamePrefix(self):
        """ component's prefix is about to be renamed signal """
        return self._getSingal(self.RenamedPrefix)

    @property
    def preRenameUnique(self):
        """ component is about to be renamed to have an unique name signal """
        return self._getSignal(self.RenamedUnique)

    @property
    def renamed(self):
        """ component renamed signal """
        return self._getSignal(self.Renamed)

    @property
    def renamedPrefix(self):
        """ component prefix renamed signal """
        return self._getSignal(self.RenamedPrefix)

    @property
    def renamedUnique(self):
        """ component uniquely renamed signal """
        return self._getSignal(self.RenamedUnique)

    @property
    def renamedUniquePrefix(self):
        """ component prefix uniquely renamed signal """
        return self._getSignal(self.RenamedUniquePrefix)

    @property
    def softSelected(self):
        """ component soft selected signal """
        return self._getSignal(self.SoftSelect)

    @property
    def softDeselected(self):
        """ component soft deselected signal """
        return self._getSignal(self.SoftDeselect)

    @property
    def hardSelected(self):
        """ component hard selected signal """
        return self._getSignal(self.HardSelect)

    @property
    def activated(self):
        """ component has been activated signal """
        return self._getSignal(self.Active)

    @property
    def deactivated(self):
        """ component has been deactivated signal """
        return self._getSignal(self.Deactivate)

    @property
    def loadBeginning(self):
        """ load beginning signal """
        return self._getSignal(self.LoadBegin)

    @property
    def loadEnded(self):
        """ load bended signal """
        return self._getSignal(self.LoadEnd)

    @property
    def clearBeginning(self):
        """ clear beginning signal """
        return self._getSignal(self.ClearBegin)

    @property
    def clearEnded(self):
        """ clear ended signal """
        return self._getSignal(self.ClearEnd)

    @property
    def transactionBeginning(self):
        """ transaction beginning signal """
        return self._getSignal(self.TransactionBegin)

    @property
    def transactionEnded(self):
        """ transaction ended signal """
        return self._getSignal(self.TransactionEnd)

    @property
    def reselected(self):
        """ component reselected signal """
        return self._getSignal(self.ReSelect)

    @property
    def preChangeName(self):
        """ pree change name signal """
        return self._getSignal(self.ChangeName)

    @property
    def changedName(self):
        """ name changed signal """
        return self._getSignal(self.ChangedName)

    @property
    def preParent(self):
        """ before component is parented signal """
        return self._getSignal(self.PreParent)

    @property
    def preUnparent(self):
        """ before component is unparented signal """
        return self._getSignal(self.PreUnparent)

    @property
    def focused(self):
        """ component is focused """
        return self._getSignal(self.Focus)

    @property
    def changedParent(self):
        """ component parent changed signal """
        return self._getSignal(self.changedParent)

    @property
    def preReorder(self):
        """ pre - reordered signal """
        return self._getSignal(self.Reorder)

    @property
    def reordered(self):
        """ reordered signal """
        return self._getSignal(self.Reordered)

    @property
    def fileOpenCompleted(self):
        """ file open completed signal """
        return self._getSignal(self.FileOpenCompleted)

    @property
    def takeOpened(self):
        """ Take opened signal """
        return self._getSignal(self.TakeOpened)

    @property
    def takeClosed(self):
        """ Take closed signal """
        return self._getSignal(self.TakeOpened)

    @property
    def takeUpdated(self):
        """ Take updated signal """
        return self._getSignal(self.TakeUpdated)
