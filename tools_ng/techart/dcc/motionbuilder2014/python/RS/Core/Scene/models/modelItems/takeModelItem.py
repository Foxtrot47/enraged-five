"""
Model item class that represents an FBTake
"""
from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class TakeTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    Take = QtCore.Qt.UserRole

    def __init__(self, takeName, takeObject, parent=None):
        super(TakeTextModelItem, self).__init__([], parent=parent)
        self._takeName = takeName
        self._takeObject = takeObject
        self._checked = False
        self._enableCheckBox = True

    def SetCheckedStateOn(self):
        """
        Sets check state to False for item
        """
        self._checked = True

    def SetCheckedStateOff(self):
        """
        Sets check state to False for item
        """
        self._checked = False

    def GetCheckedState(self):
        """
        returns current Check state of item
        """
        return self._checked

    def EnableCheckbox(self, enable):
        """
        Sets whether the check box is enabled and visible or disabled and hidden
        
        Arguments: 
            enable (boolean): enable (True) or disable(False) the checkbox
        """
        self._enableCheckBox = enable

    def GetTakeNamespace(self):
        """
        returns take namespace for item
        """
        return self._takeName

    def GetTakeObject(self):
        """
        returns take node for item
        """
        return self._takeObject

    def columnCount(self):
        """
        returns the number of columns in view
        """
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        model data generated for view display
        """
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._takeName

        elif self._enableCheckBox and role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked

        elif role == QtCore.Qt.UserRole:
            return self._takeObject

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        model data updated based off of user interaction with the view
        """

        column = index.column()
        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True
        return False
