"""
QTreeView that shows the current components in the scene based on the component type and name filter
"""
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Core.Scene.models import ComponentModel, SceneMonitor, ComponentsMonitor
from RS.Core.Scene.models.modelItems import ComponentModelItem


class ComponentView(QtGui.QTreeView):
    """
    List Motion Builder components in the scene
    """
    def __init__(self, componentType=mobu.FBComponent,  parent=None):
        """
        Constructor

        Arguments:
            componentType (pyfbsdk.FBComponent): class of the Motion Builder component to list
            parent (QtGui.QWidget): parent widget

        """
        super(ComponentView, self).__init__(parent=parent)

        self.componentTypes = componentType
        if not isinstance(componentType, (list, tuple)):
            self.componentTypes = [componentType]

        self._enabled = True
        self._previousComponentCount = len(mobu.FBSystem().Scene.Components)

        self.setSelectionMode(self.ExtendedSelection)
        self._model = ComponentModel.ComponentModel(componentType=self.componentTypes)
        self._model.setShowLongName(True)

        self.filter = QtGui.QSortFilterProxyModel()
        self.filter.setSourceModel(self._model)
        self.setModel(self.filter)

        monitor = SceneMonitor.SceneMonitor()
        for componentType in self.componentTypes:
            monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
            monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Detach)
            monitor.connect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)

    def updateModel(self, event=None, countChanged=False):
        """
        Updates the model with the current contents of the scene

        Arguments:
            control (object): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method

        """
        if not countChanged:
            return

        # Call to isAnimated is just to trigger error that the TreeView has been destroyed to avoid
        # repopulating the model if the view has been closed.
        self.isAnimated()

        # store the current selection
        previousSelection = self.selectedComponents()

        # update model
        self._model.reset()
        self.filter.setSourceModel(self._model)

        # Select the components that were selected before reseting the model
        currentSelection = []
        for index in xrange(self.model().rowCount()):
            index = self.model().index(index, 0)
            if index.data(ComponentModelItem.ComponentItem.Component) in previousSelection:
                currentSelection.append(index)

        selectionModel = self.selectionModel()
        for index in currentSelection:
            selectionModel.select(index, selectionModel.Select)

    def selectedComponents(self):
        """
        Returns the components selected through the tree view
        """
        return [index.data(ComponentModelItem.ComponentItem.Component)
                for index in self.selectionModel().selection().indexes()
                if index.data(ComponentModelItem.ComponentItem.Component) is not None]

    def setHeader(self, headerTitle):
        """
        Sets the header

        Arguments:
            headerTitle (string): value to set for the header
        """
        self._model.setHeader(headerTitle)

    def setFilter(self, text, role=QtCore.Qt.DisplayRole):
        """
        Sets the Regex filter and which role to use the filter on when displaying the list of components

        Arguments:
            text (string): regex string to filter component names by
            role (QtCore.Qt.QRole): role that determines which data goes through the filter

        """
        self.filter.setFilterRegExp(text)
        self.filter.setFilterRole(role)

    def setFilterModel(self, model):
        self._model.reset()
        model.setSourceModel(self._model)
        self.setModel(model)
        self.filter = model

    def setShowLongName(self, value):
        """
        sets whether the full name of the components should be displayed

        Arguments:
            value (boolean): whether the long name should be shown or not

        """
        self._model.setShowLongName(value)

    def closeEvent(self, event):
        """
        Overrides built-in method.
        Removes callbacks when UI is closed

        Arguments:
            event (QtGui.QEvent): the event being triggered by Qt

        """
        monitor = SceneMonitor.SceneMonitor()
        for componentType in self.componentTypes:
            monitor.disconnect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Attach)
            monitor.disconnect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.Detach)
            monitor.disconnect(componentType, self.updateModel, ComponentsMonitor.ComponentsMonitor.FileOpenCompleted)
