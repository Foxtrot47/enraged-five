"""
Model that lists all the takes in the current Motion Builder file
"""
from collections import OrderedDict

from PySide import QtCore

import pyfbsdk as mobu

from RS import Globals
from RS.Core.Scene.models.modelItems import takeModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class TakeModel(baseModel.BaseModel):
    """
    model types generated and set to data for the the model item
    """
    def __init__(self, parent=None, register=False):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        self._enableCheckBox = True
        super(TakeModel, self).__init__(parent=parent)
        if register:
            self.registerCallback()
        
    def getHeadings(self):
        """
        heading strings to be displayed in the view
        """
        return ['Take Name']

    def _getTakeDict(self):
        """
        Get list of takes in the scene
        Returns:
            Dictionary (String:FBTake) -
            namespaces of the take in the scene: FBTake object
        """
        takeDict = OrderedDict()
        for take in Globals.Takes:
            nameSplit = take.LongName.split(":")
            namespace = nameSplit[0]
            takeDict[namespace] = take
        return takeDict

    def findIndexByChild(self, item):
        """
        returns index (int)
        """
        return self.childItems.index(item)

    def setupModelData(self, parent):
        """
        model type data sent to model item
        """
        takeDict = self._getTakeDict()
        # iterate through list and create dataItem for treeview
        for key, value in takeDict.iteritems():
            dataItem = takeModelItem.TakeTextModelItem(key, value, parent=parent)
            dataItem.EnableCheckbox(self._enableCheckBox)
            # append tree view with item
            parent.appendChild(dataItem)

    def enableCheckBox(self, enable):
        """
        Sets whether the check box for each item is enabled and visible or disabled and hidden

        Arguments:
            enable (boolean): enable (True) or disable(False) the checkbox
        """
        self._enableCheckBox = enable
        self.reset()

    def flags(self, index):
        """
        flags added to determine column properties

        args: index (int)
        """
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        if self._enableCheckBox:
            flags |= QtCore.Qt.ItemIsUserCheckable
        return flags

    def reset(self, control=None, event=None):
        """
        Overrides inherited method.
        Resets the model and unregisters the callback to update itself when the model no longer exists

        Arguments:
            control (obejct): Motion Builder object calling this method
            event (pyfbsdk.FBEvent): Motion Builder event that triggered this method

        """
        try:
            super(TakeModel, self).reset()
        except:
            self.unregisterCallback()

    def registerCallback(self):
        """
        Adds callback to update the model whenever the takes change in the scenee
        """
        mobu.FBSystem().Scene.OnTakeChange.Add(self.reset)

    def unregisterCallback(self):
        """
        Removes callback to update the model whenever the takes change in the scenee
        """
        mobu.FBSystem().Scene.OnTakeChange.Remove(self.reset)
