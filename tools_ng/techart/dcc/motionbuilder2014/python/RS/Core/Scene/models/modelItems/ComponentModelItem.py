"""
Custom Model for getting a list of components in Motion Builder
"""
import os
import re

from PySide import QtCore, QtGui

from RS import Config
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class ComponentItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    Name = QtCore.Qt.DisplayRole
    LongName = QtCore.Qt.ToolTipRole
    Type = QtCore.Qt.UserRole
    Selected = QtCore.Qt.UserRole + 1
    Namespace = QtCore.Qt.UserRole + 2
    Component = QtCore.Qt.UserRole + 3
    ShowShortName = QtCore.Qt.UserRole + 4

    def __init__(self, component, parent=None):
        """
        Constructor

        Arguments:
            component (pyfbsdk.FBCompenent): component to access data from
            parent (ComponentItem): parent item

        """
        super(ComponentItem, self).__init__([], parent=parent)
        self._component = component

    def columnCount(self):
        """
        returns the number of columns in view
        """
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        model data generated for view display

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get information from
            role (QtCore.Qt.QRole): role that is being requested by the data

        """
        if not index.isValid():
            return None

        # Check that component has not been deleted from the scene
        if self._component.__class__.__name__.endswith("_Unbound"):
            self._component = None

        if self._component is not None and role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            return self._component.Name

        elif self._component is not None and role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole, QtCore.Qt.ToolTipRole]:
            return self._component.LongName

        elif self._component is not None and role == QtCore.Qt.DecorationRole:
            # Removes FB at the start of the class name only if the name starts with FB
            componentType = re.sub("^FB", "", self._component.__class__.__name__).lower()
            path = os.path.join(Config.Script.Path.ToolImages,
                                "MotionBuilder", "{}.png".format(componentType))
            if not os.path.exists(path):
                path = os.path.join(Config.Script.Path.ToolImages, "MotionBuilder", "model.png")
            return QtGui.QIcon(path)

        elif self._component is not None and role == self.Type:
            return self._component.__class__

        elif self._component is not None and role == self.Selected:
            return self._component.Selected

        elif self._component is not None and role == self.Namespace:
            return self._component.LongName.split(":", 1)[0]

        elif self._component is not None and role == self.Component:
            return self._component

        elif role == self.ShowShortName:
            return self._showShortName

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        model data updated based off of user interaction with the view

        Arguments:
            index (QtGui.QModelIndex): modelIndex to get information from
            value (object): value to set
            role (QtCore.Qt.QRole): role that is being requested by the data

        """
        if not index.isValid():
            return None
        if role == self.ShowLongName:
            self._showShortName = value
        return False
