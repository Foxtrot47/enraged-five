'''
Provides some functions that are useful for debugging and development.

Author: Ross George, Geoff Samuel
'''

import sys
from RS.Tools.Reloader import reloader


def ReloadAllModules():
    '''
    Reloads all Rockstar modules that are currently registered. Useful for completely reloading 
    everything during development of tools. Note this does only reload Rockstar modules - it's 
    assumed that external and standard libraries are not going to be edited so there would be no 
    requirement to reload them.
    '''
    # Reload all R* Modules
    reloader.RsReloader.ReloadRsModules()
