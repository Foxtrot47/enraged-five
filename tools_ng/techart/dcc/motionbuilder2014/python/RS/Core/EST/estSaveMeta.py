import os
import RS
import RS.Core.EST.estPaths as estPaths
import RS.Core.Metadata as Metautils
import RS.Perforce as p4

"""
    EST Save Metadata Utiltiy
    
    * EST meta files could be called something else, they are taken from the export name.. need ability to read this somehow???
    ** Best to warn if there is not a zip file depending ??? Depends is user wants to get zip files
"""


"""
    TImecode Helper class
"""
class Timecode():
    def __init__(self):
        '''

        :return:
        '''
        self.hrs = 0
        self.mins = 0
        self.secs = 0
        self.frs = 0
        self.frameIdx = 0
        
        
"""
    EST SAVE META
"""

class ESTmeta:
    def __init__(self, lfilePath, gLog = None):
        '''

        :param lfilePath:
        :param gLog:
        :return:
        '''
        self.lFilename = RS.Utils.Path.GetBaseNameNoExtension( lfilePath ) 
        self.metaFilePath = None
        self.estMetaObject = None
        self.shotInfoFilePath = estPaths.returnShotMetaPath( lfilePath )
        self.gLog = gLog
        


    def fileExists( self ):
        '''
        Return bool value if file exists
        :return: bool
        '''
        return (os.path.isfile(self.shotInfoFilePath))

    def save(self ):
        '''
        Save our EST meta file
        :return:
        '''
        if self.gLog:
            self.gLog.LogMessage("Saving EST metafile {0}".format(self.shotInfoFilePath))
        if self.lFilename != "":
            self.estMetaObject.Save()
     
    """
    
    
    """   
    def convertToTimecode(self, frames):
        '''
        Willl convert frames into a time code bases on 30fps

        :param frames: number of Frames
        :return: Time code
        '''
        timecode = Timecode()
        timecode.hrs = frames/(3600*30)
        if timecode.hrs > 23:
            timecode.hrs = timecode.hrs % 24
        timecode.mins = (frames%(3600*30))/(60*30)
        timecode.secs = ((frames%(3600*30))%(60*30))/30
        timecode.frs  = ((frames%(3600*30))%(60*30))%30   
        timecode.frameIdx = frames
        return timecode
        ""
        

        
    def setMetaTimeFormat(self, timeCode, camName, shotDef):
        '''
        Write out timecode to a metadata format

        :param timeCode:
        :param camName:
        :param shotDef:
        :return:
        '''
        timeStartDef = shotDef.SourceTimeCode.Create()
        timeStartDef.Name = camName
        timeStartDef.Hour = timeCode.hrs
        timeStartDef.Minute = timeCode.mins
        timeStartDef.Second = timeCode.secs        
        timeStartDef.Frame = timeCode.frs
        timeStartDef.FrameIdx = timeCode.frameIdx
        
        
    def metaAddTimeCode(self, _rangeEdit, shotDef):
        '''
        Write out our Start and End timecodes to teh Meta file Object

        :param _rangeEdit:
        :param shotDef:
        :return:
        '''
        # Cam Name
        _camName = _rangeEdit.CameraName
        # Start
        _timeCode = self.convertToTimecode(_rangeEdit.RangeIn)
        self.setMetaTimeFormat(_timeCode, _camName, shotDef)
        # End
        _timeCode = self.convertToTimecode(_rangeEdit.RangeOut)
        self.setMetaTimeFormat(_timeCode, _camName, shotDef)
        
     
    def _setShotHardRanges(self, shotDef, shot):
        '''
        Write out MB Hard aniamoitn Ranges to Metafile Object

        :param shotDef:
        :param shot:
        :return:
        '''
        print len(shot.Ranges)
        if len(shot.Ranges) > 1:
            
            shotDef.RangeStart  = shot.Ranges[0].RangeIn
            shotDef.RangeEnd  = shot.Ranges[len(shot.Ranges)-1].RangeOut
            
        
        
    def addShot(self, shot, index):
        '''
        Add a Shot entry to our MetaData

        :param shot:
        :param index:
        :return:
        '''
        shotDef = self.estMetaObject.ReelList.Create()
        shotDef.ShotName = "Shot_{0}".format(index)
        
        self._setShotHardRanges(shotDef, shot)

        for rng in shot.Ranges:
            self.metaAddTimeCode( rng , shotDef )
        
        

    
    def setHeader(self):
        '''
        Write out the Header info to our Meta file

        :return:
        '''
        self.estMetaObject = RS.Core.Metadata.CreateMetaFile( "ShotDef" )
        self.estMetaObject.SetFilePath( self.shotInfoFilePath )
        self.estMetaObject.CutsceneName = self.lFilename
        self.estMetaObject.CutFile = "$(assets)/cuts/%s/data_master.cutxml" % (self.lFilename)    
    

            
    def _createMetaEdit( self, _exportRanges ):
        '''
        Write out a list of shots to our meta File Object

        :param _exportRanges:
        :return:
        '''
        self.setHeader()
        
        for idx, shot in enumerate(_exportRanges.Shots):
            self.addShot(shot, (idx+1)); # Shots start from 1
        
        self.save()
        

        
        
        
    def exportRanges( self, _exportRangeList ):
        '''
        Main function to check out and write out our Meta File
        
        :param _exportRangeList: RangeList Object
        :return:
        '''
        if self.lFilename == "":
            self.gLog.LogError("Error: This is not a valid cutscene file, Aborting!!") 
        else:         
            bNewFile = False  
            if not os.path.isfile(self.shotInfoFilePath):
                if self.gLog:
                    self.gLog.LogWarning("File {0} not found, generating a new EST Metafile".format(self.shotInfoFilePath))            
                bNewFile = True
                
            else:
                p4.Sync(self.shotInfoFilePath)
                p4.Edit(self.shotInfoFilePath)
            
               
                
            self._createMetaEdit( _exportRangeList )
            #if bNewFile:
            # NO harm in just adding file anyway as if reverted it won;t get added again
            if self.gLog:
                self.gLog.LogMessage("Adding file {0} to perforce, please check in".format(self.shotInfoFilePath))
            p4.Add(self.shotInfoFilePath)
        
        