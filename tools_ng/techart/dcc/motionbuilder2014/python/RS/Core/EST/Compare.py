"""
EST RANGE CHANGE COMPARE

Ok for now this is a bit specific but want to make this more generalised so moved track/clip objects into there own modules

I've taken all of Dave's good bits and probably made them worse.. sorry Dave

Will just keep this as simple as possoble for now

Prob bes tot have a report class

NOTE:

THIS iS USING OUR TRACK WRAPPER FOR COMPARRISON

"""


from pyfbsdk import *
import datetime, os
import RS.Perforce

import RS.Config
import RS.Utils.Email


# EST USERS NOTIFICATION. There this list is so going to expand!!
emailList = ['Tina.Chen@rockstargames.com',
             'Sam.Henman@rockstargames.com',
             'dermot.bailie@rockstarnorth.com',
             'geoffrey.fermin@rockstarnorth.com',
             'jenny.arnold@rockstarnorth.com',
             'matt.tempest@rockstargames.com',
             'Matthew.Rochester@rockstarnorth.com',
             'wallace.robinson@rockstartoronto.com',
             'nick.robey@rockstarleeds.com',
             'sean.letts@rockstarsandiego.com',
             'tim.webb@rockstarsandiego.com',
             'blake.buck@rockstargames.com',   
             'mark.harrison-ball@rockstargames.com'
             ]

# DEBUG
#emailList = ['mark.harrison-ball@rockstargames.com']
 

# HTML Tempaltes
gTemplateH = 'bgcolor="#5F7190"><font color="Black" size="2" face="Arial"><b>'   # Template Header
gTemplateBSH = 'bgcolor="#5F7190"><font color="White" size="2" face="Arial"><b>' # Tempalte Detail Sub Header Report 
gTemplateDH = 'bgcolor="#A5B4CE"><font color="Black" size="2" face="Arial"><b>'  # Default Bold
gTemplateW = 'bgcolor="#AC0F06"><font color="White" size="2" face="Arial"><b>'   # Tempalte Warning

gTemplateDHH = 'bgcolor="#5F7190"><font color="Black" size="1" face="Arial"><b>'   # Template Header
gTemplateDW = 'bgcolor="#AC0F06"><font color="White" size="1" face="Arial"><b>'   # Tempalte Warning
# EST OverVIew
gTemplateBH = 'bgcolor="#C6AA1D"><font color="Black" size="2" face="Arial"><b>'  # Tempalte Detail Header Report 

gTemplateB = 'bgcolor="#D5CDA5"><font color="Black" size="1" face="Arial"><b>'   # Tempalte Detail Report

# Detail Report
gTemplateSDB = 'bgcolor="#A5B4CE"><font color="Black" size="1" face="Arial">'    # Detail Small Blue
gTemplateSDB2 = 'bgcolor="#BDCAE1"><font color="Black" size="1" face="Arial">'   # Detail lighter blue

########################################################################
"""
Simple Function to Generate a table with optional tempaltes passed in.. NICE!!!
TODO: Add support to pass in a default template
Template Addition process
"""
def gentableCol(inputArray, bClose = True):  
    REFHTML = '<tr>\n'
    for dicName in inputArray:
        defaultTemplate = 'bgcolor="#A5B4CE"><font color="Black" size="2" face="Arial">'
        value = dicName
        if type(dicName) == tuple: # If we find a tempalte then set the bitch
            defaultTemplate = dicName[1]
            value = dicName[0]
        REFHTML += '<td {0}{1}</font></td>\n'.format( defaultTemplate, value)
    if bClose:
        REFHTML += '</tr>\n'
    return REFHTML

def genHtmlHeading(sValue, defaultTempalte = None):
    REFHTML = ''
    REFHTML += '<div><font color="Black" size="2" face="Arial"><b>{0}</span></div>'.format(sValue)
    return REFHTML
    
        
# Parse our htmlbuilder array
def parseHtmlData(testarry):
    htmlBody = ''
    for entry in testarry:
        htmlBody += gentableCol(entry, bClose=True)         
    return htmlBody

# Function to create a table from our Tracks
def genShotTable(REFHTML, pNewTrackArray,  headerName = None):
    if headerName:
        REFHTML += '<table width="400" border=0 cellspacing="0">\n'# New Table 
        testarry = [[(headerName,gTemplateBSH),('',gTemplateBSH),('',gTemplateBSH)]]
        REFHTML += parseHtmlData(testarry) 
        REFHTML += '</table>' #Close our table
        
    
    for track in pNewTrackArray:
        REFHTML += '<table width="400" border=0 cellspacing="0">\n'# New Table  
        testarry = [[(track.Name,gTemplateDHH),('camera',gTemplateDHH),('duration',gTemplateDHH), ('range', gTemplateDHH)]]
        REFHTML += parseHtmlData(testarry)
        testarry = []
        if len(track.Clips) == 0:
            testarry.append([('No Clip(s) Found',gTemplateSDB) , '', '' ])
        else:
            for clip in track.Clips:
                DefaultTemplate = gTemplateSDB
                if clip.Error:
                    DefaultTemplate = gTemplateDW
                    
                testarry.append(
                                [(clip.Name,DefaultTemplate),
                                 (clip.CameraName,gTemplateSDB2),
                                 (clip.Duration,gTemplateSDB),
                                 (str(clip.Start) + ' - ' + str(clip.End),gTemplateSDB2)]
                                )
        REFHTML += parseHtmlData(testarry)
        REFHTML += '</table><br>' #Close our table 
    return REFHTML
        
"""
The core HTML Report Creator
"""
def GenerateEmailHTMLReport(pOldTrackArray, pNewTrackArray, rangeWarning = ''):
    """
    # HTML TEMPLATES
    """
    global gTemplateH
    global gTemplateDHH
    global gTemplateDH
    global gTemplateW
    global gTemplateDW
    global gTemplateBH
    global gTemplateBSH
    global gTemplateB
    global gTemplateSDB
    global gTemplateSDB2
    
    
    
    """
    HTML START
    """
    REFHTML = '<body>\n'
    
    """
    # EEST HEADING INFO
    """    
    # Table Heading
    REFHTML += genHtmlHeading('EST Updated Report')    
    REFHTML += '<table width="800" border=1 cellspacing="0">' 
    
    # Table Contents  
    testarry = [[('User',gTemplateH),RS.Config.User.Name ],   
                [('Cutscene',gTemplateH),(os.path.basename(FBApplication().FBXFileName),gTemplateDH)],   
                [('Path:',gTemplateH),FBApplication().FBXFileName ],
                [('Time',gTemplateH),datetime.datetime.now().strftime( '%Y-%m-%d %H:%M:%S')],
                [('EST Change',gTemplateH),(rangeWarning,gTemplateW)]                
                ] 
    # Add P4 Info for FBX FIle
    try:
        fileInfo = perforce.getFileInfo( FBApplication().FBXFileName )
        if fileInfo:
            testarry.append([('P4 FBX File Rev:',gTemplateH),('rev:'+ str(fileInfo.haveRevision))])    
    except:
        testarry.append([('P4 FBX File Rev:',gTemplateH),'Unavaiable'])  
        
    # P4 info for Tracking DOc    
    try:
        fileInfo = perforce.getFileInfo( project.root +'/docs/production/GTAV_CAMERA_TRACKING.xlsm') 
        if fileInfo:
            testarry.append([('P4 Tracking Doc Rev:',gTemplateH),('rev:'+str(fileInfo.haveRevision))])    
    except:
        testarry.append([('P4 Tracking Doc Rev:',gTemplateH),'Unavaiable'])     
    
    REFHTML += parseHtmlData(testarry)
    # Tbale Close
    REFHTML += '</table><br>' #Close our table
    
    """
    # Range Info
    """
    # Table Heading
    REFHTML += genHtmlHeading('EST Change Overview')   
    REFHTML += '<table width="400" border=0 cellspacing="0">\n'# New Table   
    # Table Header (Optional)
    testarry = [[('',gTemplateBH),('Before',gTemplateBH),('After',gTemplateBH)]]
    REFHTML += parseHtmlData(testarry)
    # Table Contents  
    
    # RANGES CHECK
    errorTemplate = gTemplateB
    loldRange = returnDuration(pOldTrackArray)
    lnewRange = returnDuration(pNewTrackArray)
    
    if loldRange['range'] != lnewRange['range']:
        errorTemplate=gTemplateW
        
    testarry = [
        [('Range start/end',gTemplateB), (str(loldRange['start'])+' - '+str(loldRange['end']),gTemplateB), (str(lnewRange['start'])+' - '+str(lnewRange['end']),errorTemplate)],
        [('Range (frames)',gTemplateB), (loldRange['range'], gTemplateB), (lnewRange['range'], errorTemplate)]
       ]
    
    
    # SHOTS CHECK
    errorTemplate = gTemplateB # reset template    
    loldTracks = returnTrackCount(pOldTrackArray)
    lnewTracks = returnTrackCount(pNewTrackArray)
    
    if loldTracks != lnewTracks:
        errorTemplate = gTemplateW
    
    testarry.append([('Total Tracks',gTemplateB),(loldTracks,gTemplateB),(lnewTracks,errorTemplate)])    
    
    # CLIP CHECK
    errorTemplate = gTemplateB # reset template    
    loldClips = returnClipCount(pOldTrackArray)
    lnewclips = returnClipCount(pNewTrackArray)
    
    if loldClips != lnewclips:
        errorTemplate = gTemplateW
    
    testarry.append([('Total Clips',gTemplateB),(loldClips,gTemplateB),(lnewclips,errorTemplate)])
        
    
        
    
  
    
    REFHTML += parseHtmlData(testarry)
    # Tbale Close
    REFHTML += '</table><br>' #Close our table   
    
    """
    # Detailed Report
    """
    # Table Heading
    REFHTML += genHtmlHeading('EST Track Details')  
    
    # Table Contents  (Outer Table      
    REFHTML += '<table width="800" border=0 cellspacing="0">'
    # Created our fistr nest Table
    REFHTML += '<tr><td valign="top">'
    
    # Create Compare Track (old)
    REFHTML = genShotTable(REFHTML ,pOldTrackArray, 'Before')
        
    REFHTML += '<td valign="top">'
    REFHTML += '<table width="400" border=0 cellspacing="0">\n'# New Table  

    # Create Compare Track (New)
    REFHTML = genShotTable(REFHTML ,pNewTrackArray,  'After')

    
    # Close nest tables
    REFHTML += '</table></td>' #Close our table  
    REFHTML += '</tr></table>' #Close our table  
    
    

        
    # CLose Html Body
    REFHTML += '</body>\n'
    
        
    return REFHTML  






def EmailReport(pOldTrackArray = None, pNewTrackArray = None, gWarning = ''):  
    htmlContent = GenerateEmailHTMLReport(pOldTrackArray, pNewTrackArray, gWarning)  
    # DEBUG
    """
    f = open('x:/temp/html.html','w')
    f.writelines(htmlContent)
    f.close()
    """
    # Add user as well for report info
    if RS.Config.User.Email:
        if not RS.Config.User.Email in emailList:
            emailList.append(RS.Config.User.Email)
        
    
    if htmlContent:
        try:
            RS.Utils.Email.sendEmail( RS.Config.User.Email, emailList, 'EST Range Change Detected: ', htmlContent, asHtml = True )
        except:
            print 'Could not email the Report'
    
    return
    
    
    
    
    
# Compare Tracks
def CompareTracks(pOldTrackArray, pNewTrackArray):
    print 'Running EST Report'
    gWarning = ""
    gRangeWarning = ""
    if returnDuration(pOldTrackArray)['range'] != returnDuration(pNewTrackArray)['range']:
        gRangeWarning = "Range lengths have changed " 
        
    if returnClipCount(pOldTrackArray) == 0:
        gRangeWarning = "First time EST has been added to this scene" 
    else:
        # Number of Shot Tracks
        #if len(pOldTrackArray) != len(pNewTrackArray):
        if len(pOldTrackArray) > len(pNewTrackArray):
            gWarning = ": One or More Shot Tracks have been removed in latest EST"
        elif len(pOldTrackArray) < len(pNewTrackArray):
            gWarning = ": New Shot Track(s) have been added to latest EST" 
        else: 
            for iTrack in range(len(pOldTrackArray)):
                
                if len(pOldTrackArray[iTrack].Clips) > len(pNewTrackArray[iTrack].Clips):
                    # Clips Added
                    gWarning = ": One or More Clips have been removed in latest EST"
                elif len(pOldTrackArray[iTrack].Clips) < len(pNewTrackArray[iTrack].Clips):
                    gWarning = ": New Clip(s) have been added to latest EST" 
                else:
                    for indexClip in range(len(pOldTrackArray[iTrack].Clips)):
                        #check if camera names are the same in each clip
                        if pOldTrackArray[iTrack].Clips[indexClip].CameraName.lower() == pNewTrackArray[iTrack].Clips[indexClip].CameraName.lower():  #??
                            #check if clip names are the same for each clip
                            if pOldTrackArray[iTrack].Clips[indexClip].Name.lower()  != pNewTrackArray[iTrack].Clips[indexClip].Name.lower() :
                                gWarning = ": Names Don't Match"
                                pNewTrackArray[iTrack].Clips[indexClip].Error = True
                            
    # Range CHange Check
    gRangeWarning += gWarning

   
    if gRangeWarning != "":
        EmailReport(pOldTrackArray, pNewTrackArray, gRangeWarning)

    
    
    return

"""
MOVE to better lcoaiton
"""
def returnTrackCount(pTrackArray):
    return len(pTrackArray)
    
def returnClipCount(pTrackArray):
    clipCount = 0
    for track in pTrackArray:
        clipCount += len(track.Clips)        
    return clipCount

# To get the DUration we get the start Clip  and the end clip 
# If there are multipile Clips we collect min and Max Clip
def returnDuration(pTrackArray):    
    estStart = 1000000000 #BULLSHIT
    estEnd   = 0
    for track in pTrackArray:
        if track:
            if len(track.Clips) > 0:
                clipStart = track.Clips[0].Start
                if clipStart < estStart:
                    estStart = clipStart        
                clipEnd = track.Clips[len(track.Clips)-1].End
                if clipEnd > estEnd:
                    estEnd = clipEnd
    
    duration = estEnd - estStart
    # Check EST duration
    if duration < 0: # Means there are no shots
        duration = 0
    return {'start':estStart, 'end':estEnd, 'range':duration}



#EmailReport()


    
        
        
        
    
    

