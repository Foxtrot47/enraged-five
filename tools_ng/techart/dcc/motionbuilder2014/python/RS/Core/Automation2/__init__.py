import os
import sys
import pstats
import cProfile

from pyfbsdk import *

import RS.Utils.Logging.Universal
import RS.Core.Metadata
import RS.Perforce
import RS.Config
import RS.Utils.Instrument

class TaskResult( object ):
    '''
    Enumeration for the result of a task.
    '''
    COMPLETED	= 1
    SKIPPED	= 2
    FAILED	= 4
    

class AutomationTaskBase( object ):
    '''
    Base class for an automation task.
    '''
    
    def __init__( self, ownerJob, taskMetaStruct, taskID ):
        '''
        "taskMetaStruct" is the metadata struct object for this particular task.  For example, a task in the meta file 
        will look something like this:
        
            <Item type="UpdateCharacter">
                <Character>Player_Zero</Character>
                <MinimumRevision value="53" />
            </Item>
        
        The above XML is transformed into a metadata structure, and passed into the task.  To access data inside the struct, you 
        would simply do this for the above example:
        
            taskMetaStruct.Character
            taskMetaStruct.MinimumRevision
        '''
        self._id = taskID
        self._ownerJob = ownerJob
        self._taskMetaStruct = taskMetaStruct
        self._description = None
    
    @property
    def ID( self ):
        '''
        Returns the ID of the job
        '''
        return self._id        

    @property
    def Description( self ):
        '''
        Provides information about this task for the changelist description.
        '''
        return self._description
    
    @Description.setter
    def Description( self, value ):
        self._description = value
    
     ## Methods ##
     
    def Process( self, log, directory, changelist ):
        raise NotImplementedError, "You must implement this method!  Should return True on success, False on failure."
        
    def P4CheckoutFile( self, filePath, changelistNum, fileType = None, specialCharacters=False):
        if specialCharacters:    
            filePath = filePath.replace("@", "%40") 
            
        if RS.Perforce.DoesFileExist( filePath ):
            p4FileState = RS.Perforce.GetFileState( filePath )
            
            if RS.Perforce.IsOpenForEdit( p4FileState ):
                #If the file is already open for edit, lets skip the P4 stuff...
                return True
            
            else:
                #Make sure we have latest...
                if RS.Perforce.IsLatest( p4FileState ) == False:
                    RS.Perforce.Sync( filePath )           
                
                if RS.Perforce.Edit( filePath, changelistNum, fileType = fileType ):
                    return True
            
                else:
                    return False  
        else:
            if RS.Perforce.Add( filePath, changelistNum, fileType = fileType, force=True):
                return True
            else:
                return False
        
    def P4AddFile(self, filePath, changelistNum, fileType = None, specialCharacters=False):
        if specialCharacters:    
            filePath = filePath.replace("@", "%40") 
        
        if not RS.Perforce.DoesFileExist( filePath ):
            # File is not in P4, lets add it
            if RS.Perforce.Add( filePath, changelistNum, force=True, fileType = fileType):
                return True
            else:
                return False
        return False

    def P4SyncFileOrFolder(self, filePath, force = False):
        syncFile = filePath
        rootpath, ext = os.path.splitext(syncFile)
        if ext == '':
            if not rootpath.replace('\\','/').endswith('/'): 
                rootpath = str( rootpath + '/')
            syncInput = str(rootpath + "...")
        RS.Perforce.Sync( syncInput, force = force )
    
class AutomationJob( object ):
    '''
    Base class for an automation job.
    '''
    def __init__( self, jobID, jobCacheDirectory, jobLog, jobChangelist ):
        
        self._tasks = []        
        self._id = jobID
        self._changelist = jobChangelist
        self._cacheDirectory = jobCacheDirectory
        
        #Get a handle on our log
        self._log = jobLog
        self._log.LogMessage( "Motionbuilder process successfully picked up job {0}".format(self._id) ) 

        #Parse our metadata file
        self._log.LogMessage( "Parsing job metadata file.")
        self._metaFilePath = os.path.join( self._cacheDirectory, "process.meta" )
        self._metaFile = RS.Core.Metadata.ParseMetaFile( self._metaFilePath )
        
        # Collect and attach tasks to the job.
        self._log.LogMessage( "Parsing tasks.")
        taskID = 1
        for taskMeta in self._metaFile.Tasks:
            taskType = self._getAutomationTaskById( taskMeta.Type )
            self._tasks.append( taskType( self, taskMeta, taskID ) )
            taskID += 1
        self._log.LogMessage( "Parsed {0} task(s).".format( len( self._tasks ) ) )    
        
        #Set if we want to debug or profile job
        self._profile = self._metaFile.Profile
        self._debug = self._metaFile.Debug
 
    def _getAutomationTaskById( self, taskMetaType ):
        '''
        Find the appropriate automation task class given a task id.
        
        Returns:
        
            The found class that inherits from AutomationTaskBase.
        '''
        import RS.Core.Automation2.Tasks
        reload( RS.Core.Automation2.Tasks )
        
        for cls in AutomationTaskBase.__subclasses__():
            if cls.__name__ == taskMetaType:
                return cls
            
        raise Exception( "Fail to match the task by ID ensure your task inherits from \"RS.Core.Automation2.AutomationTaskBase\" and the ID is correct!" ) 
    
    @property
    def ID( self ):
        '''
        Returns the ID of the job
        '''
        return self._id
    
    def Process( self ):
        '''
        Processes each task assigned to this job.
        '''
	
        self._log.LogMessage( "Starting job process." )

        # Connect to Wing if debugging is set to True.
        if self._debug:
            self._log.LogMessage( "Setting up for debugging." )        
            import RS.Tools.UI.Commands
            RS.Tools.UI.Commands.ConnectToWing()        

        #If we are profiling set this up now
        if self._profile:
            self._log.LogMessage( "Setting up for profiling." )        
            import pyfbsdk
            RS.Utils.Instrument.InstrumentModule( pyfbsdk ) 

        # Perform tasks for this job.
        for task in self._tasks:

            #Push new log context for this task
            self._log.LogMessage( "Starting task - {0} ({1}).".format( task.ID, type(task).__name__ ) )
            taskName = "{0}_{1}".format( task.ID, type(task).__name__ )
            self._log.PushContext( taskName ) 		

            #Create working directory for the task
            taskDirectory = os.path.join( self._cacheDirectory, taskName )
                                          
            if not os.path.exists(taskDirectory):
                os.mkdir(taskDirectory)
            
            if self._profile:
                taskProfile = cProfile.Profile()
                taskProfile.enable() 

            #Run the task itself
            task.Process( self._log, taskDirectory, self._changelist )
            
            if self._profile:
                
                #Stop profiling on the task and dump information
                taskProfile.disable()
                taskProfileRawFilePath = os.path.join( self._cacheDirectory, ".profile.raw" )
                taskProfile.dump_stats( taskProfileRawFilePath )               
                
                #Process data to human readable information and attach to log for the task
                taskProfileFilePath = os.path.join( self._cacheDirectory, "profile" )
                taskProfileFileStream = open( taskProfileFilePath, 'w' )
                stats = pstats.Stats( taskProfileRawFilePath, stream = taskProfileFileStream )
                stats.strip_dirs()
                stats.sort_stats( 'time' )
                stats.print_stats()
                taskProfileFileStream.close()		
            
            self._log.LogMessage( "Task process successful" )
            self._log.PopContext()
          