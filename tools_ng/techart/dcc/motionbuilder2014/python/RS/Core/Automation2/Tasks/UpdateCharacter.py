'''
UpdateCharacter.py

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>

Description: 
Updates a character in the loaded scene file if the revision of the character is below the set
minimum revision of the task.
'''
import pyfbsdk as mobu

import RS.Core.Automation2

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character

class UpdateCharacterTask( RS.Core.Automation2.AutomationTaskBase ):
    
    def __init__( self, ownerJob, taskMetaStruct, taskID ):
        RS.Core.Automation2.AutomationTaskBase.__init__( self, ownerJob, taskMetaStruct, taskID )
        
        self.__characterNamespace = taskMetaStruct.Character
        self.__minimumRevision = int( taskMetaStruct.MinimumRevision )
        
    def Process( self, job ):
        mobu.FBPlayerControl().GotoStart()
        
        manager = Manager()
        
        characters = manager.GetReferenceListByType( Character )
        
        for character in characters:
            if character.Namespace == self.__characterNamespace:
                if int( character.CurrentVersion ) < self.__minimumRevision:
                    job.ULog.LogMessage( "Updating character: {0}".format( self.__characterNamespace ), context = job.Context )
                    job.ULog.LogMessage( "Minimum revision: {0}".format( self.__minimumRevision ), context = job.Context )                

                    manager.UpdateReferences( character, True )
		    
		    self.Description = "Performed \"{0}\" task. \"{1}\" was updated to revision \"{2}\".".format( UpdateCharacterTask.ID, self.__characterNamespace, int( character.CurrentVersion ) )
                    
                    return RS.Core.Automation2.TaskResult.COMPLETED
		
		else:
		    self.Description = "Skipped \"{0}\" task.  \"{1}\" is above the minimum revision \"{2}\" requirement.  Currently at revision \"{3}\".".format( UpdateCharacterTask.ID, self.__characterNamespace, self.__minimumRevision, int( character.CurrentVersion ) )
		    return RS.Core.Automation2.TaskResult.SKIPPED
		    
	
	# Don't fail the overall job if the character supplied isn't in the scene.	
	self.Description = "Skipped \"{0}\" task.  \"{1}\" does not exist in the scene.".format( UpdateCharacterTask.ID, self.__characterNamespace )
        return RS.Core.Automation2.TaskResult.SKIPPED