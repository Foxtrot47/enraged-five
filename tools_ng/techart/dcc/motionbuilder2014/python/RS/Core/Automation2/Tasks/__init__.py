'''
Automatically import any module living inside this directory.  This will allow us to dynamically
find subclasses of AutomationTask.
'''
import os

for module in os.listdir( os.path.dirname( __file__ )):
    if module == '__init__.py' or module[ -3: ] != '.py':
        continue
    
    result = __import__( module[ :-3 ], locals(), globals() )
    reload( result )
    
del module