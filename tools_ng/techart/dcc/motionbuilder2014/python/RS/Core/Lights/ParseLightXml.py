'''

 Script Path: RS/Core/Lights/ParseLightXml.py

 Written And Maintained By: Kathryn Bodey

 Created: 05.05.2014

 Description: Generate lights based off the lightxml selected by the user.
              The idea is to replicate game cutscene lights inside of Motionbuilder.
              url:bugstar:1720902

'''

from pyfbsdk import *

import os
import xml.etree.cElementTree

import RS.Config
import RS.Perforce
import RS.Globals
import RS.Utils.Scene
from RS.Utils.Scene import Constraint

from ctypes import *

'''

Store Light Properties in a Class

'''
class LightObjectInfo( object ):
    def __init__( self ):

        self.ConeAngle = 0.0
        self.Intensity = 0.0
        self.Colour = []
        self.Translation = []
        self.Rotation = []
        self.AutoClip = ''
        self.LightName = ''

'''

Create a dict with Camera index, Camera object and Autoclip Name

'''    
def CameraDictionary():
    cameraIndexDict = {}
    cameraDict = {}

    # get camera and index
    cameraList = RS.Globals.rs_NonSystemCameraList()

    cameraSwitcher = FBCameraSwitcher()
    switcherKeys = cameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys 
    for i, key in enumerate( switcherKeys ):

        camera = cameraList[int( key.Value )-1]
        index = key.Value

        cameraIndexDict[camera] = ( index )

    # get autoclips
    storyFolder = FBStory().RootEditFolder  
    for key, value in cameraIndexDict.iteritems():
        for track in storyFolder.Tracks:
            for clip in track.Clips:
                clipName = clip.Name
                shotCamera = clip.ShotCamera
                frameStart = clip.ShotActionStart.GetFrame()
                frameStop = clip.ShotActionStop.GetFrame()
                doubleStart = clip.ShotActionStart.GetSecondDouble()
                doubleStop = clip.ShotActionStop.GetSecondDouble()

                if key == shotCamera:
                    cameraDict[clipName] = ( shotCamera, value, doubleStart, doubleStop, frameStart, frameStop )

    return cameraDict


'''

Populate the Rel Constraint created in AddLightXMLLights()

'''    
def PopulateConstraint( cameraIndexDict, constraint, convertBox, autoclip, light, yPos ):


    for key, value in cameraIndexDict.iteritems():

        if autoclip == key:  
            
            numberBox2 = constraint.CreateFunctionBox( 'Number', 'Is Between A and B' )
            constraint.SetBoxPosition( numberBox2, 1000, yPos ) 
            dataA = RS.Utils.Scene.FindAnimationNode( numberBox2.AnimationNodeInGet(),'a' )
            dataA.WriteData( [value[2]] )   
            dataB = RS.Utils.Scene.FindAnimationNode( numberBox2.AnimationNodeInGet(),'b' )
            dataB.WriteData( [value[3]] ) 

            targetBox = constraint.ConstrainObject( light )
            constraint.SetBoxPosition( targetBox , 1400, yPos )

            RS.Utils.Scene.ConnectBox( convertBox, 'Result', numberBox2, 'Value' )
            RS.Utils.Scene.ConnectBox( numberBox2, 'Result', targetBox, 'Visibility' )


'''

Parse the Light XML file and generate the light properties

'''
def ParseLightXml( lightXmlFilename ):
    result = []

    doc = xml.etree.cElementTree.parse( lightXmlFilename )
    root = doc.getroot()

    if root:
        itemNodes = root.find( 'pCutsceneObjects' ).findall( 'Item' )

        for itemNode in itemNodes:
            itemType = itemNode.get( 'type' )

            if itemType == 'rage__cutfLightObject':
                info = LightObjectInfo()

                # light name - so we can filter out day, night and ped
                info.LightName = itemNode.find( 'cName' ).text

                # skip ped lights as these cannot be visualised 
                if info.LightName.find("_PED_") != -1:
                    continue
                
                # cone angle
                info.ConeAngle = float( itemNode.find( 'fConeAngle' ).get( 'value' ) )
                
                # intensity
                info.Intensity = float( itemNode.find( 'fIntensity').get( 'value' ) )        
                
                
                for i in itemNode.find( 'attributeList' ):
                    # translation
                    if i.tag == 'maxrelposition':
                        info.Translation = i.text
                    
                    # rotation
                    if i.tag == 'maxrelrotation':
                        info.Rotation = i.text
                    
                    # autoclip
                    if i.tag == 'camera':
                        info.AutoClip = i.text                        

                # colour
                vColourX = float( itemNode.find( 'vColour' ).get( 'x' ) )
                vColourY = float( itemNode.find( 'vColour' ).get( 'y' ) )
                vColourZ = float( itemNode.find( 'vColour' ).get( 'z' ) )
                info.Colour = [ vColourX, vColourY, vColourZ ]

                # translation
                vPositionX = float( itemNode.find( 'vPosition' ).get( 'x' ) )
                vPositionY = float( itemNode.find( 'vPosition' ).get( 'y' ) )
                vPositionZ = float( itemNode.find( 'vPosition' ).get( 'z' ) )                
                info.Position = [ vPositionX, vPositionY, vPositionZ ]    
                
                result.append( info )                

    return result


'''

Lights, from lightxml, are present in the scene - compare the perforce revisions to see if we need to update them

'''    
def CheckUpdateLightXMLLights( path, currentRevision ):

    if os.path.exists( path ):
        # compare the current lighhtxml P4 revision with what the scene has
        haveRevision = currentRevision
        headRevision = RS.Perforce.GetFileState( path ).HeadRevision

        # needs an update
        if int( haveRevision ) < int( headRevision ):
            print 'lights are present and out of date'
            deleteList = []

            for component in RS.Globals.gComponents:
                if component.PropertyList.Find('lightxml Path'):
                    deleteList.append( component )

            # it is easiest to just delete all of the lights and regenerate them from scratch
            for i in deleteList:
                try:
                    i.FBDelete()
                except:
                    pass

            AddLightXMLLights( path )

        else:
            FBMessageBox( 'R*', 'Lights are present and up to date already.', 'Ok' )


'''

Lights, from lightxml, are not present in the scene - we need to generate them

'''    
def AddLightXMLLights( pathKnown=None ):
    from RS.Core.Export import RexExport

    CameraDict = CameraDictionary()

    if pathKnown:
        print 'lights are updating...'
        # lightxml path already known
        selectedPath = pathKnown
    else:
        print 'adding lights for the first time'
        # get lights via exporter
        exportPath = RexExport.GetCutsceneExportPath()
        selectedPath = exportPath + '/data.lightxml'
        print '------------------------------'
        print selectedPath
        print '------------------------------'


    # set lights up
    if os.path.exists( selectedPath ):   

        split = selectedPath.split('cuts/')
        selectedFileName = split[-1]

        RS.Perforce.Sync( selectedPath, force = True, revision = None )    
        haveRevision = RS.Perforce.GetFileState( ( selectedPath ) ).HaveRevision

        lightResult = ParseLightXml( selectedPath )  
        
        # check the audioclips match
        # if they dont the user may have selected an incorrect xml file, or the cut file and MB file are out of sync.
        incorrectLightXML = False
        lightXMLAudioClipList = []
        for key, value in CameraDict.iteritems(): 
            lightXMLAudioClipList.append( key )

        autoClip = None
        for item in lightResult:
            autoClip = item.AutoClip

        if autoClip:
            if autoClip in lightXMLAudioClipList:
                None
            else:
                incorrectLightXML = True

        if incorrectLightXML != True:
            folderPlaceholderFile = FBLight( 'deleteme' )
    
            folder = FBFolder( 'lightxml_' + selectedFileName, folderPlaceholderFile )
    
            tag = folder.PropertyCreate('lightxml Name', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedFileName
    
            tag = folder.PropertyCreate('lightxml Path', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedPath
    
            tag = folder.PropertyCreate('lightxml Perforce Revision', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = str( haveRevision )
    
            folderPlaceholderFile.FBDelete()
    
            # day relation
            cameraDayRelation = Constraint.CreateConstraint(Constraint.RELATION)
            cameraDayRelation.Name = ("day_lightxml_Relation")    
    
            systemBox = cameraDayRelation.CreateFunctionBox( 'System', 'Local Time' )
            cameraDayRelation.SetBoxPosition( systemBox, 0, 300 ) 
    
            convertBoxDay = cameraDayRelation.CreateFunctionBox( 'Converters', 'Time to seconds' )
            cameraDayRelation.SetBoxPosition( convertBoxDay, 400, 300 )  
    
            RS.Utils.Scene.ConnectBox( systemBox, 'Result', convertBoxDay, 'Time' )        
    
            tag = cameraDayRelation.PropertyCreate('lightxml Name', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedFileName
    
            tag = cameraDayRelation.PropertyCreate('lightxml Path', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedPath
    
            tag = cameraDayRelation.PropertyCreate('lightxml Perforce Revision', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = str( haveRevision )
            
            # night relation
            cameraNightRelation = Constraint.CreateConstraint(Constraint.RELATION)
            cameraNightRelation.Name = ("night_lightxml_Relation")    
    
            systemBox = cameraNightRelation.CreateFunctionBox( 'System', 'Local Time' )
            cameraNightRelation.SetBoxPosition( systemBox, 0, 300 ) 
    
            convertBox = cameraNightRelation.CreateFunctionBox( 'Converters', 'Time to seconds' )
            cameraNightRelation.SetBoxPosition( convertBox, 400, 300 )  
    
            RS.Utils.Scene.ConnectBox( systemBox, 'Result', convertBox, 'Time' )        
    
            tag = cameraNightRelation.PropertyCreate('lightxml Name', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedFileName
    
            tag = cameraNightRelation.PropertyCreate('lightxml Path', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = selectedPath
    
            tag = cameraNightRelation.PropertyCreate('lightxml Perforce Revision', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = str( haveRevision )    
            
            cameraNightRelation.Active = False
            
            yPos = 300
            for item in lightResult:
    
                # variables
                autoClip = item.AutoClip
                translation = item.Translation
                rotation = item.Rotation
                colour = item.Colour    
                coneAngle = item.ConeAngle
                intesity = item.Intensity
                name = item.LightName
    
                for key, value in CameraDict.iteritems():
                    if key == autoClip:
                        startFrame = value[4]
                        stopFrame = value[5]
    
                # create a spot light
                light = FBLight( name )
    
                tag = light.PropertyCreate('lightxml Name', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = selectedFileName            
    
                tag = light.PropertyCreate('lightxml Path', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = selectedPath
    
                tag = light.PropertyCreate('lightxml Perforce Revision', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = str( haveRevision )        
    
                tag = light.PropertyCreate('AutoClip', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = str( autoClip )    
                        
                tag = light.PropertyCreate('Frame', FBPropertyType.kFBPT_charptr, "", False, True, None)
                tag.Data = str( startFrame ) + '-' + str( stopFrame )
    
                light.Show = True
                light.Visibility = False
    
                light.PropertyList.Find( 'Type' ).Data = 2        
                light.DiffuseColor = FBColor( colour )
                light.ConeAngle = coneAngle
                light.Intensity = intesity * 100
                light.FogIntensity = 0
    
                splitTrns = translation.split(',')
                xTrns = float( splitTrns[0] )*100 # scale translation by 100
                yTrns = float( splitTrns[1] )*100 # scale translation by 100
                zTrns = float( splitTrns[2] )*100 # scale translation by 100
    
                splitRot = rotation.split(',')
                xRot = float( splitRot[0] )
                yRot = float( splitRot[1] )
                zRot = float( splitRot[2] )
                wRot = float( splitRot[3] )
                
                light.Visibility.SetAnimated(True)
    
                folder.Items.append( light )
                
                if name.startswith('N_'):
                    PopulateConstraint( CameraDict, cameraNightRelation, convertBox, autoClip, light, yPos )
                else:
                    PopulateConstraint( CameraDict, cameraDayRelation, convertBoxDay, autoClip, light, yPos )            
                
                yPos = yPos + 150
    
                # Take the light translation/rotation and convert this into motionbuilder space and set the transform
                cdll.rexMBRage.SetLightTransformInMotionbuilderSpace_Py( name, c_float(xTrns), c_float(yTrns), c_float(zTrns), c_float(xRot), c_float(yRot), c_float(zRot), c_float(wRot) )
                
            FBMessageBox( 'R*', 'light setup complete', 'Ok' )
                
        else:
            FBMessageBox( 'R* Error', 'The Audio Clip names are not matching between\nthe data.lightxml and the MB Scene.\n\nYou have either selected an incorrect data.lightxml\nfile or they are just out of sync.\n\nPlease select the correct lightxml, or re-export\nyour scene to get them back into sync.', 'Ok' )
                     
    else:
        FBMessageBox( 'R*', 'Path does not exist:\n\n{0}'.format(selectedPath), 'Ok' )

'''

Check if Lights exist in the scene already - pass appropriate def based off results

'''    
def Lights( ):    

    # check if scene has lights present by checking the lights folder properties
    lightXmlPathProperty = None
    lightXmlPathRevision = None
    for i in RS.Globals.gFolders:
        lightXmlPathProperty = i.PropertyList.Find( 'lightxml Path' )
        lightXmlPathRevision = i.PropertyList.Find( 'lightxml Perforce Revision' )

    # double check - check the light properties as well, in case someone has deleted the folder
    if lightXmlPathProperty == None:
        for i in RS.Globals.gConstraints:
            lightXmlPathProperty = i.PropertyList.Find( 'lightxml Path' )
            lightXmlPathRevision = i.PropertyList.Find( 'lightxml Perforce Revision' )

    if lightXmlPathProperty != None:
        # lights found - check if they need updating
        CheckUpdateLightXMLLights( lightXmlPathProperty.Data, lightXmlPathRevision.Data )
    else:
        # lights not found - generate them
        AddLightXMLLights()
        
def ReferenceSystemFeedback():
    
    perforceInfoList = []
    
    # check if scene has lights present by checking the lights folder properties
    lightXmlPathProperty = None
    lightXmlPathRevision = None
    for i in RS.Globals.gFolders:
        lightXmlPathProperty = i.PropertyList.Find( 'lightxml Path' )
        lightXmlPathRevision = i.PropertyList.Find( 'lightxml Perforce Revision' )

    # double check - check the light properties as well, in case someone has deleted the folder
    if lightXmlPathProperty == None:
        for i in RS.Globals.gConstraints:
            lightXmlPathProperty = i.PropertyList.Find( 'lightxml Path' )
            lightXmlPathRevision = i.PropertyList.Find( 'lightxml Perforce Revision' )

    if lightXmlPathProperty != None:
        # lights found
        pathHeadRevision = RS.Perforce.GetFileState( lightXmlPathProperty.Data ).HeadRevision
        perforceInfoList = [ 'yes', lightXmlPathRevision.Data, pathHeadRevision ]
    else:
        # lights not found
        perforceInfoList = []
    
    return perforceInfoList