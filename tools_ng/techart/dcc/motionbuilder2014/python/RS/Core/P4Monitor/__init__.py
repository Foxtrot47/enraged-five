# Monitor p4 submits 

import sys, os, string, re, time, smtplib, traceback, logging, datetime
import uuid, base64
import ConfigParser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import RS.Config


useCL = False  #debug testing for reading from a static CL


debug = True
repeat  = True
UseConfigFile = True

useLog = True



class p4monitor:
    def __init__(self):  
        
        self.p4 = 'p4'
        self.jobsClient = '{0}/cache/automation/client/py_jobs/'.format( RS.Config.Project.Path.Root )
        self.p4path = '//depot/gta5/art/animation/cutscene/!!scenes/...'
        self.p4date = ''
        self.logfile = '{0}/cache/automation/client/p4monitor.log'.format( RS.Config.Project.Path.Root )
        self.configfile = '{0}/RS/Core/P4Monitor/config.cfg'.format( RS.Config.Script.Path.Root )

        # used to track from last CL. IF you add a number herw with useCL then it will track from here
        self.lastCL = 3852019
        self.comment = None
        
        self.FBXList = dict()
        self.localcllist = dict()
        
        
        self.__initSettings__()
        
    # Simple log file writer        
    def log(self, value):
        print(str(value)) # print our value to console
        if useLog: # if using log, then write to log
            now = datetime.datetime.now()
            tformat = '['+str(datetime.time(now.hour, now.minute, now.second)) +'] '
            fVal = tformat+str(value)+'\n'
            f = open((self.logfile),'a')
            f.write(fVal)
            f.close()    
        
        
    def __defaultConfig__(self):
        self.userlist = [
                  
                    ]
        self.studiolist = [
                    ]
        
        self.commentList = [
            ]
        
        self.restrictUsers = True        
        self.sleeptime = 60
        self.useLog = True
        self.useCommentExport =True
        sef.exchangeserver = ''
        
        
        
        
        
    # Startup fucntions 
    def __initSettings__(self):
        self.log('======================INITALISING P4 MONITOR===========================')
        # LOad in config filke
        if UseConfigFile:
            self.loadConfigFile()        

        # Display Modes:
        self.log('DebugMode:'+str(debug))
        self.log('Debug Use Static CL:'+ str(useCL) )
        self.log('\n[CONFIG SETTINGS]')
        
        self.log('restrictUsers:'+str(self.restrictUsers))
        self.log('users: '+str(self.userlist))
        self.log('Use Logging:'+str(self.useLog))
        self.log('Use Comment Filtering:'+str(self.useCommentExport))
        self.log('Check Interval Time:'+ str(self.sleeptime))
        self.log('')
        

        self.CreateJObFOlder()
        self.getLastCL()  
        if useLog:
            if not os.path.exists(self.logfile):
                f = open((self.logfile),'w')
                f.close()
        
    
    # Validate we have a good entry in teh config file
    def CFGvalidateUsersEntry(self,value):        
        subvalues = value.split('\n')
        userlist = []
        for user in subvalues:
            userlist.append(user.split(',')[0].strip())
            
        return userlist
        
        
        
        
        
            
    def loadConfigFile(self):
        if not os.path.exists(self.configfile):
            self.log('No Config File Found, Using default settings')            
            return
        else:
            self.log("Found Valid Config File: "+self.configfile)
            Config = ConfigParser.ConfigParser()
            Config.read(self.configfile)
            if len(Config.sections()) > 0:
                self.restrictUsers = Config.getboolean("P4Monitor", "RESTRICT_USERS")
                self.sleeptime = Config.getint("P4Monitor", "SLEEPTIME")
                self.useLog = Config.getboolean("P4Monitor", "LOG")                
                self.useCommentExport = Config.getboolean("P4Monitor", "COMMENT_EXPORT")
                self.userlist = self.CFGvalidateUsersEntry((Config.get("P4Monitor", "USERS")))
                self.exchangeserver = Config.get("P4Monitor","EXCHANGESERVER")
                
                if len(self.userlist) == 0:
                    self.restrictUsers = False
                
                
                
                

        return
        
    # Generate a unique GUID
    def get_a_Uuid(self):
        r_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
        return r_uuid.replace('=', '')
        
        
    
    # Creat Job FOlder    
    def CreateJObFOlder(self):
        if not os.path.exists(self.jobsClient):
            try:
                os.makedirs(self.jobsClient)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise                
                
             
    def CreateJobs(self):
	self.log('\nCreating Jobs.....')
        for k,v in self.FBXList.items():
            self.CreateSingleJob(k,v)  
            
        
        
        #for fbxFile in self.FBXList:
        #    self.CreateSingleJob(fbxFile)  
        self.log('Job(s) Created succuesfuly')
        return
    
    
    # for now I will creater a text file!! (ITS SHIT!!)
    def CreateSingleJob(self, fbxFile, user): 
     
        base = os.path.basename(fbxFile)
        uniqueName = self.get_a_Uuid()+base+'.job'
        
        if debug:
            self.log('\tCreating job '+ (self.jobsClient+uniqueName))

        f = open((self.jobsClient+uniqueName),'w')
        f.write(fbxFile)
        f.write('\n'+user)
        
        f.close()
        return
        
        
    """
    File Validation
    """    
    # Bool: Return if FBX file
    def isFBXfile(self, fName):
        if fName.endswith('fbx'):
            return True
        
        return False
    
    # Bool: Confirm if this is a fbx file we can process
    def isValidFBXfile(self, fPath):
        lastDir = os.path.split(os.path.dirname(fPath))[1]
        if lastDir.lower().endswith('stems'):
            return False               
        return True
    
    # Comment Filter --- coudl be clever here but i'm STUPID!
    def isExportFromComment(self):
	
        if (self.comment):
            if self.comment.lower().startswith("update"):
                return True
	    else:
		return False
        return False
       

    """
    """
    
    # get last submit and update CL number    
    def getLastCL(self): 
        if not useCL:
            cmd = (self.p4 + ' changes -m 1')
            self.parse_p4_Change(cmd)

    # Check for User
    def VerifyUsers(self, user = None):        
        if self.restrictUsers:            
            self.log('checking if user %s is valid for job creation' % user)            
            if (user):
                for sUser in self.userlist:
                    if sUser.lower() == user.lower():
                        if debug:
                            self.log('\tThis is a valid user:'+ user )
                        return True
            return False
        return True 
        
    def parse_p4_review(self):
        self.comment = None
        self.log('Scanning for changes since last CL'+str(self.lastCL-1))
        #self.log self.cmd
        self.localcllist= dict()
        self.FBXList = dict()
        
        
        cmd =(self.p4 + ' changes -s submitted '+self.p4path+'@'+str(self.lastCL)+',#head')
        if debug:
            self.log(cmd)
        
        self.parse_p4_Change(cmd)
               
            
        if len(self.localcllist) > 0:
            self.log('Checking submitted files in each changelist ...')
            self.getP4Files()           

        return

    # Go theough all the chnages in extract the changelist number
    def parse_p4_Change(self, cmd):
        for line in os.popen(cmd,'r').readlines():  
	    
            (change,user) = \
                re.match(r'Change.(\d+).+by.(\S+)@', line).groups()
            if debug:                
                self.log('\tChange:'+change+ ' User:'+user)
                
            if int(change) > self.lastCL:
                self.lastCL = int(change)+1 # plus one so we look for next cl
                # We then collect this CL for doing stuff
                
            # Filter our CL
            if (self.VerifyUsers(user)): 
                self.localcllist[int(change)] = user # Add a valid changelsit nymber
                
    def parse_p4_Commint(self,cmd):
        return
    
    
    def getP4Files(self):
        for key in self.localcllist:
	    self.pasreDescribe(key)
            
        self.CreateJobs()
        
        
    """
    def sendmail(self, from_addr, to_addrs, msg, mail_options=[],
                 rcpt_options=[]):
    """
    # Basic Error Email   
    def ErrorEmail(self, error):
        print self.exchangeserver
        if self.exchangeserver != '':           
        
            emailFrom = 'CutsceneUpdater@rockstargames.com'
            emailTo = 'mark.harrison-ball@rockstargames.com'
            emailSubject = 'Cutscene Monitor Has Crashed'
            
            machine = os.getenv('COMPUTERNAME') 
            user = os.environ.get( "USERNAME" )
            
            emailBody = ('Machine Name: '+machine+'\nUser:'+user+'\n'+str(e))

            body = MIMEText( emailBody )        
                 
            
            message = MIMEMultipart( 'alternative' )
            message[ 'Subject' ] =  emailSubject          
            message[ 'From' ] = emailFrom           
            message[ 'To' ] = emailTo
            message.attach( body )  
            # Have to use the London Exchange for email sent to me
            #server = smtplib.SMTP( self.exchangeserver )
            try:
                server = smtplib.SMTP("rsgldnexg1.rockstar.t2.corp")
                server.sendmail( emailFrom, emailTo, message.as_string() )
                server.quit()
                
            except:
                    print 'Could not send the email!'        
    
            return
            
        
    
    
    
    
    """
    Parse the CL and find the fbx files in the submit
    """
    def pasreDescribe(self, key):
	self.comment = None
        readedits = False # used to track where the file list is in the descritpon
        
        for sline in os.popen((self.p4+' describe -s ')+str(key),'r').readlines():
	    print sline
            if not readedits:   # We are reading the comment             
                if '#' in sline:
                    tokens= \
                        re.match(r'(.+)[#?](\S+)', sline).groups()               
                    self.comment = tokens[1]   
                
            if sline.startswith('Affected files'): # ok we find the edits path
                readedits = True
               
              
            if sline.startswith('...'):                
                (void, FBXfile) = \
                    re.match(r'(\S+) (\S.+)[#?]', sline).groups()           
                
                if (self.isFBXfile(FBXfile)):
		    
                    if (self.isValidFBXfile(FBXfile)):
                        lAddjob = 0
			

                        if (self.useCommentExport): # Comment Filter
			    
                            if (self.isExportFromComment()):
                                lAddjob = 1 
			    else:
				value = ('User:'+self.localcllist[key]+'\tNOT PROCESS (#update): '+FBXfile)
				self.log(value)				
				
				lAddjob = 0
				
                        else:
                            lAddjob = 1
			    
                        if lAddjob:  
                            self.log("\nFound valid file for a job")
                            self.FBXList[FBXfile] = self.localcllist[key]                            
                            value = ('User:'+self.localcllist[key]+'\tAdding files for processing:'+FBXfile)
                            self.log(value)
                
                
            
 


if __name__ == '__main__':
    
    p4mon = p4monitor()
    p4mon.log("\n[STARTING P4 MONITOR]")
    
    
    while(repeat):
        now = datetime.datetime.now()
        tformat = datetime.time(now.hour, now.minute, now.second)

        
        p4mon.log(str(tformat)+" Scanning p4 submits")
        if debug:
            p4mon.log(('Sleeping for %d seconds.' % p4mon.sleeptime))
        
        try:
	    p4mon.parse_p4_review()
                 
        except Exception, e:        
	    p4mon.log("Something went wring!!!!!!!!!!!")
	    p4mon.ErrorEmail(e)
            
        time.sleep(p4mon.sleeptime)
    else:
        #p4mon.parse_p4_review()
        if debug: p4mon.log('Done.')
	now = datetime.datetime.now()
	tformat = datetime.time(now.hour, now.minute, now.second)

	
	p4mon.log(str(tformat)+" Scanning p4 submits")
	if debug:
	    p4mon.log(('Sleeping for %d seconds.' % p4mon.sleeptime))	
	
	p4mon.parse_p4_review()
		 
	



