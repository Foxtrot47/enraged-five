
"""
Scan for jobs to pick up
"""
import ConfigParser
import RS.Core.P4Monitor.Ini
import RS.Core.CutsceneBatchTrigger as autobatch

import sys, os, string, re, time, traceback,  datetime, smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import RS.Config


debug = 1
repeat = 1

useLog = 1

RunBatchUpdate = 1
RunExport = 0
  
class jobMonitor:
    def __init__(self):
        self.jobsClient = "{0}\\cache\\automation\\client\\py_jobs\\".format( RS.Config.Project.Path.Root )
        self.logfile = '{0}/cache/automation/client/jobMonitor.log'.format( RS.Config.Project.Path.Root )
        self.activeJob = None
        self.configfile = '{0}/RS/Core/P4Monitor/config.cfg'.format( RS.Config.Script.Path.Root )
        self.p4 = 'p4'
        self.fbxFile = None
        self.fbxUser = None
        self.sleeptime = 60 # default
        self.emailjobList = dict() 
        self.__loadConfigFile__()   
        self.projroot = RS.Config.Project.Path.Root
        
        # Use this to check if there are jobs in our cache so lets not bother sleeping and get through hthem        
        self.jobsActive = 0
        #self.mbExport = mbExport()
        self.__initSettings__()
        
    
    def __loadConfigFile__(self):
        if not os.path.exists(self.configfile):
            self.log('No Config File Found, Using default settings')            
            return
        else:
            self.log("Found Valid Config File: "+self.configfile) 
            
            Config = ConfigParser.ConfigParser()
            Config.read(self.configfile)
            if len(Config.sections()) > 0:
                self.sleeptime = Config.getint("P4Monitor", "SLEEPTIME")
                self.__getuseremails__((Config.get("P4Monitor", "USERS")))       
        
        self.log(self.emailjobList)
        #self.emailjobList
        return
    
    def __getuseremails__(self, value):
        subvalues = value.split('\n')
        userlist = []
        for user in subvalues:
            tokens = user.split(',')
            if len(tokens) == 2:
                self.emailjobList[tokens[0].strip()] = tokens[1].strip()
        return
    
    
    def Sendemail(self, emailUser, subject):
        #if self.exchangeserver != '':           
                
        emailFrom = 'CutsceneUpdater@rockstargames.com'
        emailTo =  emailUser#'rob.elsworthy@rockstartoronto.com'
        emailSubject = 'Job has been sent to Cutscene update monitor'

        
        emailBody = (subject)

        body = MIMEText( emailBody )        
             
        
        message = MIMEMultipart( 'alternative' )
        message[ 'Subject' ] =  emailSubject          
        message[ 'From' ] = emailFrom           
        message[ 'To' ] = emailTo
        message.attach( body )  
        # Have to use the London Exchange for email sent to me
        #server = smtplib.SMTP( self.exchangeserver )
        try:
            server = smtplib.SMTP("rsgldnexg1.rockstar.t2.corp")
            server.sendmail( emailFrom, emailTo, message.as_string() )
            server.quit()
            
            
        except:
                print 'Could not send the email from JobMonitor!'        

        return          
        
    
    # Basic Error Email   
    def SendJobEmail(self, user, fbxfile):
        # look up user fisrt
        if self.emailjobList[user] != None:
            subject = 'file:'+fbxfile+'has been sent to be updated by the auto cutscene updater'
            self.Sendemail(self.emailjobList[user], subject)
        return
          
        
    # Startup fucntions 
    def __initSettings__(self): 
        os.chdir(self.projroot) # Change our working directory
        if useLog:
            if not os.path.exists(self.logfile):
                f = open((self.logfile),'w')
                f.close()        
 
 
# Supre simple log file writer        
    def log(self, value):
        print str(value)
        now = datetime.datetime.now()
        tformat = '['+str(datetime.time(now.hour, now.minute, now.second)) +'] '
        fVal = tformat+str(value)+'\n'
        f = open((self.logfile),'a')
        f.write(fVal)
        f.close()      

    def oldest_file_in_dir(self, extension=".job"):        
        rootfolder = (self.jobsClient)
        # check if folder is not empty
        if os.listdir(rootfolder) == []:
            return
        return min(
            (os.path.join(dirname, filename)
            for dirname, dirnames, filenames in os.walk(rootfolder)
            for filename in filenames
            if filename.endswith(extension)),
            key=lambda fn: os.stat(fn).st_mtime)    

    # check for files in the job    
    def checkforJobs(self):        
        self.activeJob = self.oldest_file_in_dir()
        if (self.activeJob): # FOund a job!
            self.jobsActive = 1
            self.log(('Processing job:',self.activeJob))
            self.getJobInfo() # Read the job info. WHatever crap i've written into the text file
            if self.runJob():
                self.jobComplete()
                
        else:
            print 'No jobs found'
            self.jobsActive = 0

        return
    
    # Return working Path
    def returnWorkPath(self):
        if (self.fbxFile):
            tokens = self.fbxFile.split('depot')
            if len(tokens) == 2:
                workPath = ('x:'+tokens[1]).split('\n')[0]
                
                return workPath  
            return None
        return None
            
            
        
    
    # Open the job info... fuck all info at teh momment you muppet!!
    def getJobInfo(self):
        if (self.activeJob):            
            f = open((self.activeJob),'r')
            lines = f.readlines()
            self.fbxFile = lines[0]
            self.fbxUser = lines[1]
            if debug:
                print self.fbxFile            
        
    
    def runJob(self):
        if debug:
            print 'Found job'        
        if self.jobSync(): # If we can syn to the fbc file then carry on else lets fuck off        
            # Start MB porcess and wait
            if RunBatchUpdate:
                
                
                ValidWorkPath = self.returnWorkPath()
                if (ValidWorkPath):                 
                    self.log('== Sending FBX File to batch update ==' ) 
                    self.log('\tUSER: '+self.fbxUser)
                    self.log('\tFBX PATH: '+ValidWorkPath)
                    self.log('\tFBX FILE: ' + self.fbxFile)

                    
                    autobatch.submitJob(ValidWorkPath)
                    
                    self.SendJobEmail(self.fbxUser,self.fbxFile)
                else:
                    print 'FILE NOT VALID', self.fbxFile
                    
            if RunExport:
                if RunExport:
                    print 'processing Job... ', self.activeJob
                    print 'Simulating moitonbuilder.exe process'                
           

                
                """
                Call MB and pasre the file name
                """
                
                
            time.sleep(10)
            if debug:
                print 'prcess completed'
            return True
        return
    
    # remove File
    def jobComplete(self):
        if (self.activeJob):
            try:
                if os.path.exists(self.activeJob):
                    os.remove(self.activeJob)
            except:  
                print 'Unable to delete job. Bollocks!!!'
        self.log('Completed Job:' +self.activeJob )
        return
    
    def jobFailed(self):
        return
    
    
    """
    perforce handling:
    ok so plan is, we run our mb export.- files are cehcked out automactically
    -- it creates a prieview dictionaries ( shevled but downlaoded)
    -- the export will checkout the relavant files:
    ----- Need to check if source file is checked out???
    ----- after we haev exported we want to move teh default changelist into a named changelist
    ----- Then we can shelve the bitch for test
    ----- SO would prefer to hook into the workbench and donwload the file instead.
    ------ MIghtbe better to ftp this back
    ----- Issue here is when someone is working on the file and the auto is exporting what then
    ------ ALl goes out of sync!!!
    ------ Best to mark a fiel as please export me in the comment
    
    
    cmd =(self.p4 + ' changes -s submitted '+self.p4path+'@'+str(self.lastCL)+',#head')
    """
    
    def shelveFiles(self, sFile):
        return
    
    # Prob best to add a timeout / check if we can not run
    def jobSync(self):
        # SO we can check here if file is cehcked out, if so then skip the job.. its failed!!
        #cmd =(self.p4,' sync ',self.fbxFile 
        return True
        #self.syncFile(self.fbxFile)
        #return True
        

    # Sync File
    def syncFile(self, sFile):
        cmd = self.p4+ ' sync '
        if debug:
            print cmd + sFile
        ret = os.popen(cmd + sFile).readline().strip() 
        print ret
        
        
    # OPen file for Edit          
    def OpenFileForEdit(self, file, changelist = ""):
        "Open a file for edit, if a changelist is passed in then open it in that list"
        cmd = self.p4+ ' edit '
        if changelist:
            cmd += " -c " + changelist + " "
        ret = os.popen(cmd + file).readline().strip()
        if not ret.endswith("opened for edit"):
            print "Couldn't open", file, "for edit:"
            print ret
            raise ValueError    
    
    
if __name__ == '__main__':
    print 'Looking for Jobs.........'
    jobMon = jobMonitor()
    jobMon.log("Looking for Jobs.........")
    while(repeat):
        now = datetime.datetime.now()
        tformat = datetime.time(now.hour, now.minute, now.second)
        
        print tformat," Scanning for jobs....."
        if debug:
            print('Sleeping for %d seconds.' % jobMon.sleeptime)
        
        #try:
        jobMon.checkforJobs()
                 
        #except:
        #    print "something is wrong"
        
        # SLeep a bit before checking for jobs again
        
        if not (jobMon.jobsActive):
            time.sleep(jobMon.sleeptime)
    else:
        jobMon.checkforJobs()
        if debug: print('Done.')