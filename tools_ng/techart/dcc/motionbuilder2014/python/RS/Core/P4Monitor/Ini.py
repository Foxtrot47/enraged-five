"""
To run any extrenal py scripts from outside MB we need to collect all teh scrips inside the MB pyhon folder
"""

import py_compile, sys, os, stat, time, decimal, socket
import xml.etree.cElementTree, contextlib, getpass, email.mime.text, imghdr, smtplib
import fnmatch, os, string
import _winreg
import RS.Config

def rs_Walk( pRoot, pRecurse=0, pPattern='*', pReturn_folders=0 ):
    
    
    
    lResult = []
    
    try:
        lNames = os.listdir(pRoot)
    except os.error:
        return lResult
    
    lPattern = pPattern or '*'
    lPat_list = string.splitfields( lPattern , ';' )
    
    for iName in lNames:
        lFullname = os.path.normpath(os.path.join(pRoot, iName))
        for iPattern in lPat_list:
            if fnmatch.fnmatch(iName, iPattern):
                if os.path.isfile(lFullname) or (pReturn_folders and os.path.isdir(lFullname)):
                    lResult.append(lFullname)
                continue
        if pRecurse:
            if os.path.isdir(lFullname) and not os.path.islink(lFullname):
                lResult = lResult + rs_Walk( lFullname, pRecurse, pPattern, pReturn_folders )        
    return lResult

def rs_RemoveLocalPythonPackages():
    pathsToRemove = []

    for path in sys.path:
        if 'c:\\python26' in str( path ).lower():
            pathsToRemove.append( path )
            
    for path in pathsToRemove:
        sys.path.remove( path )
        print 'Removed ({0}) from sys.path'.format( path )

def rs_SetupExternalPackages():
    print 'importing extreanl package'
    import site
    
    site.addsitedir( '{0}\\wildwest\\script\\MotionBuilderPython\\External'.format( RS.Config.Tool.Path.Root ) )

def rs_Ini():
    lDirectory =  RS.Config.Project.Path.Root
    lPath = "{0}\\tools\\wildwest\\script\\MotionBuilderPython\\...".format( lDirectory )
    lPaths = [ lDirectory + "\\tools\\wildwest\\script\\MotionBuilderPython\\GlobalUtilities",
               lDirectory + "\\tools\\wildwest\\script\\MotionBuilderPython\\CoreFunctions"]
    
    for iPath in lPaths:
            lPythonFiles = rs_Walk(iPath, 1, "*.py", 0)
            lPycFiles = rs_Walk(iPath, 1, "*.pyc", 0)
            iPy = ""
            
            for iPy in lPycFiles:
                lFileAtt = os.stat(iPy)[0]
        
                if (not lFileAtt & stat.S_IWRITE):  
                    os.chmod(iPy, stat.S_IWRITE)
            
            try:
                os.remove( iPy )
            
            except OSError:
                print 'ERROR: Could not remove the file ({0})!'.format( iPy )
           
            
            for iPy in lPythonFiles:
                py_compile.compile(iPy)
                #print iPy
    
                lNewPath = os.path.dirname(iPy)
                if not lNewPath in sys.path:
                    #print "Adding Path"
                    sys.path.append(lNewPath) 


def Run():
    rs_RemoveLocalPythonPackages()                       
    rs_SetupExternalPackages()                   
    rs_Ini()
    rs_RemoveLocalPythonPackages()
    





