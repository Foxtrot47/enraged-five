"""
Temp file to write out
"""

import os
#import perforce
import sys
import subprocess

import RS.Config

TestFile = "{0}/art/animation/cutscene/!!scenes/Family_5/Family_5_MCS_5/Family_5_MCS_5.fbx".format( RS.Config.Project.Path.Root )

debug = 0

class mbtempfile:
	def __init__(self):
		self.mbexe = "C:\\Program Files\\Autodesk\\MotionBuilder 2012 (64-bit)\\bin\\x64\\motionbuilder.exe"
		self.assetpath = "{0}/assets/cuts/".format( RS.Config.Project.Path.Root )
		self.args = r'x:\temp\myfile.py'
		self.fileName = None
		self.baseName = None
		self.datapart = None
		
		self.p4 = 'p4'
		return
	
	def writefile(self):
		
		f = open('x:\\temp\\myfile.py','w')
		
		f.write("""from ctypes import *
from pyfbsdk import *
from pyfbsdk_additions import *
import os
import RS.Config

try:
	logPath = os.path.join( RS.Config.Tool.Path.Root, "logs\cutscene\workbench_batch.log" )
	d = os.path.dirname(logPath)
	if not os.path.exists(d):
		os.makedirs(d)
	LOGFILE = open(logPath,"a")\n
""")
		print self.fileName
		f.write('\tif FBApplication().FileOpen( "%s" ) == True:\n' % self.fileName)
		f.write('\t\tif cdll.rexMBRage.ExportBuildCutscene_Py( "%s",False,False ) == False:\n' % (self.datapart))
		f.write('\t\t\tLOGFILE.write("%s - %s failed\\n")\n' % (self.fileName, self.datapart))
		f.write('\t\telse:\n')
		f.write('\t\t\tLOGFILE.write("%s - %s success\\n")\n' % (self.fileName, self.datapart))
		f.write('\telse:\n')
		f.write('\t\tLOGFILE.write("%s - Unable to open file\\n")\n' % (self.fileName))
		f.write("""
	LOGFILE.close()
except IOError:
  print "An error has occurred within the Python script."
FBApplication().FileExit()
		""")
		
		f.close()

		
	# So How the fcuk do we find the datapart to this scene?
	# We use our common inectopitn of file finding
	def getDatapart(self):
		base = os.path.basename(self.fileName).split('.fbx')
		self.baseName = base[0]
		# look for a folder called this in the assets dir.
		if os.path.exists(self.assetpath+base[0]):
			if os.path.isfile(self.assetpath+base[0]+'/data.cutpart'):
				self.datapart  = self.assetpath+base[0]+'/data.cutpart'
				return True
		return False
	
	
	
	def launchMotionbuilder(self):	
		# Launch MotionBuilder.
		proc = subprocess.Popen( '{0} {1} -suspendMessages'.format( MOBU_EXE, self.args) )
		proc.wait()
		
	def process(self, sFileName):
		self.fileName = sFileName
		if (self.getDatapart()):
			self.writefile()
			self.syncDependecies()
			if debug:
				print "Simulating launching Moitonbuilder and exporting"
			else:
				self.launchMotionbuilder()
	
	def syncDependecies(self):
		if debug:
			print 'syncing depedncies', self.assetpath+self.baseName
		basecmd = self.p4 + ' sync -f '+ self.assetpath+self.baseName
		os.popen(basecmd+'\...','r')
		os.popen(basecmd+'.cutbin','r')
		os.popen(basecmd+'.cutxml','r')
		return

if __name__ == '__main__':
	global MOBU_EXE
	MOBU_EXE = r'C:\Program Files\Autodesk\MotionBuilder 2012 (64-bit)\bin\x64\motionbuilder.exe'	       


	mbExport = mbtempfile()
	#test.process("test")
	mbExport.process(TestFile)