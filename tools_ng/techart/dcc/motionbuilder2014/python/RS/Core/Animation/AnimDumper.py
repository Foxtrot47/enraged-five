"""
Module for dumping body and or facial motions to a .txt file in the same format as the .anim dump text file output
"""

import os

import pyfbsdk as mobu
from RS import Globals
from RS.Utils import Scene


def generateOutputFileName():
    mainName = os.path.splitext(os.path.basename(mobu.FBApplication().FBXFileName))[0]
    takeName = mobu.FBSystem().CurrentTake.Name
    return "{0}.{1}.mobu.txt".format(mainName, takeName)


class AnimDumpIndex(object):
    boneTranslation = 0
    boneRotation = 1
    genericControl = 2

    @classmethod
    def getNameFromIndex(cls, index):
        idxDict = {
        cls.boneTranslation: "boneTranslation",
        cls.boneRotation: "boneRotation",
        cls.genericControl: "genericControl",
        }
        return idxDict.get(index, 'NoControlFound')


class AnimDumper(object):
    def __init__(self, itemsToExport=None, exportFile=None):
        self._internalModel = _AnimDataModel()
        self._trackNumber = 0
        if itemsToExport is None:
            itemsToExport = [] # Get character

        self._setupModel(itemsToExport)

        if exportFile is not None:
            self.dumpAnim(exportFile)

    def _preLoadBoneValues(self, currentLevel, frameNumber):
        for controller in currentLevel.children:
            self._preLoadBoneValues(controller, frameNumber)
        for channel in currentLevel.channels:
            channel.data(frameNumber)

    def _addChildrenBones(self, currentLevel):
        if not isinstance(currentLevel, mobu.FBModelSkeleton) and not isinstance(currentLevel, mobu.FBModelNull):
            return
        currentName = currentLevel.Name
        if not currentName.startswith("SKEL_"):
            return
        bannedWords = ["ROLL", "_NUB"]
        for word in bannedWords:
            if word.lower() in currentName.lower():
                return

        newController = AnimDumpController(None, currentLevel, self._trackNumber, AnimDumpIndex.boneRotation)
        if len(newController.channels) == 0 and len(newController.children) == 0:
            return

        self._internalModel.addController(newController)
        self._trackNumber += 1
        for child in [child for child in currentLevel.Children]:
            self._addChildrenBones(child)

    def _setupModel(self, items):
        self._trackNumber = 0

        mobySystem = mobu.FBSystem()
        # Get the Character (should only be one?!)
        characters = [char for char in Globals.Characters]
        character = None
        if len(characters) == 0:
            raise ValueError("No Character in scene")
        elif len(characters) == 1:
            character = characters[0]
        else:
            for char in characters:
                if char.Selected is True:
                    character = char
                    break
            if character is None:
                raise ValueError("Mutliple Characters in scene, please select one")

        # Start Building the tree
        topLevelNode = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)

        # Add the Translation
        newController = AnimDumpController(None, topLevelNode, self._trackNumber, AnimDumpIndex.boneTranslation)
        self._internalModel.addController(newController)
        self._trackNumber += 1

        # Add the children
        self._addChildrenBones(topLevelNode)

        # Set info dict
        # Number of Tracks
        self._internalModel.setInfo("NumTracks", len(self._internalModel.controllers))
        # Number of Frames
        endFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        startFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        self._internalModel.setInfo("InternalFrames", endFrame - startFrame)

        # Load up All the data
        player = mobu.FBPlayerControl()
        for frameIdx in xrange(startFrame, endFrame):
            player.Goto(mobu.FBTime(frameIdx))
            for controller in self._internalModel.controllers:
                self._preLoadBoneValues(controller, frameIdx)

    def _dumpAnimHeader(self, fileHandler):
        for key in self._internalModel.getInfoKeys():
            fileHandler.write("{0}:\t{1}\n".format(key, self._internalModel.getInfo(key)))
        fileHandler.write("\n")

    def _dumpAnimChannels(self, fileHandler, controller):
        mobySystem = mobu.FBSystem()
        startFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        endFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStop().GetFrame()

        for time in xrange(startFrame, endFrame):
            intro = "Value[{0}]:".format(time)
            data = " ".join([str(channel.data(time)) for channel in controller.channels])
            fileHandler.write("\t".join([intro, data]))
            fileHandler.write("\n")

    def _dumpAnimTrack(self, fileHandler, controller):
        intro = "Track[{0}]:".format(controller.track)
        trackInfo = "track {0}".format(controller.trackType) # not sure where this number comes from
        idInfo = "id {0}".format(controller.id)
        typeInfo = "type {0}".format(controller.trackType)
        name = controller.name

        fileHandler.write("{0}\t{1}".format(intro, " ".join([trackInfo, idInfo, typeInfo, name])))
        fileHandler.write("\n")
        if len(controller.channels) > 0:
            self._dumpAnimChannels(fileHandler, controller)
        fileHandler.write("\n")
        for child in controller.children:
            self._dumpAnimTrack(fileHandler, child)

    def dumpAnim(self, fileToExport):
        with open(os.path.normpath(fileToExport), 'w') as fileHandler:
            self._dumpAnimHeader(fileHandler)
            for controller in self._internalModel.controllers:
                self._dumpAnimTrack(fileHandler, controller)


class FacialDumper(AnimDumper):
    def __init__(self, itemsToExport=None, exportFile=None):
        super(FacialDumper, self).__init__(itemsToExport=itemsToExport, exportFile=exportFile)

    def _addChildrenBones(self, currentLevel):
        if not isinstance(currentLevel, mobu.FBModelSkeleton) and not isinstance(currentLevel, mobu.FBModelNull):
            return
        currentName = currentLevel.Name
        if not any([currentName.startswith("SKEL_"), currentName.startswith("FACIAL_"), currentName == "headSkeleton"]):
            return
        bannedWords = ["ROLL", "_NUB"]
        for word in bannedWords:
            if word.lower() in currentName.lower():
                return

        newController = AnimDumpController(None, currentLevel, self._trackNumber, AnimDumpIndex.boneRotation)
        if len(newController.channels) == 0 and len(newController.children) == 0:
            return

        self._internalModel.addController(newController)
        self._trackNumber += 1
        for child in [child for child in currentLevel.Children]:
            self._addChildrenBones(child)

    def _setupModel(self, items):
        self._trackNumber = 0

        topLevelNode = Scene.FindModelByName("SKEL_Neck1")
        if not topLevelNode:
            raise ValueError("Unable to root bone (SKEL_Neck1)")
        # Add the Translation
        newController = AnimDumpController(None, topLevelNode, self._trackNumber, AnimDumpIndex.boneTranslation)
        self._internalModel.addController(newController)
        self._trackNumber += 1
        mobySystem = mobu.FBSystem()
        # Add the children
        self._addChildrenBones(topLevelNode)

        # Set info dict
        # Number of Tracks
        self._internalModel.setInfo("NumTracks", len(self._internalModel.controllers))
        # Number of Frames
        endFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStop().GetFrame()
        startFrame = mobySystem.CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        self._internalModel.setInfo("InternalFrames", endFrame - startFrame)

        progressBar = mobu.FBProgress()
        progressBar.ProgressBegin()
        progressBar.Caption = "Dumping Animation"

        # Load up All the data
        player = mobu.FBPlayerControl()
        for frameIdx in xrange(startFrame, endFrame):
            # Stop the progress if user request cancellation by pressing and holding "ESC" key
            if (progressBar.UserRequestCancell()):
                break

            player.Goto(mobu.FBTime(frameIdx))
            for controller in self._internalModel.controllers:
                self._preLoadBoneValues(controller, frameIdx)
            progressBar.Percent = int((float(frameIdx) / (endFrame - startFrame)) * 100)
        progressBar.ProgressDone()


class _AnimDataModel(object):
    def __init__(self, name=None, filePath=None):
        super(_AnimDataModel, self).__init__()
        self._name = name or ""
        self._filePath = filePath or ""
        self._info = {}
        self._controllers = []

    def getControllerByName(self, controllerName):
        return self._getOrCreateControllerByName(controllerName, False)

    def getOrCreateControllerByName(self, controllerName):
        return self._getOrCreateControllerByName(controllerName, True)

    def _getOrCreateControllerByName(self, controllerName, createNew=False):
        for controller in self._controllers:
            if controller.name == controllerName:
                return controller
        if createNew:
            self.addNewController(controllerName)

        raise ValueError("Unable to find '{0}'".format(controllerName))

    def addNewController(self, name=None, track=None, trackId=None, trackType=None):
        newController = _AnimDataController(None, name=name, track=track, trackId=trackId, trackType=trackType)
        self._controllers.append(newController)
        return newController

    def addController(self, newController):
        self._controllers.append(newController)
        return newController

    @property
    def controllers(self):
        return self._controllers
    @property
    def animData(self):
        return self._controllers
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = newName

    @property
    def filePath(self):
        return self._filePath

    @filePath.setter
    def filePath(self, value):
        self._filePath = value

    def getInfoKeys(self):
        return self._info.keys()

    def getInfo(self, key):
        return self._info[key]

    def setInfo(self, key, value):
        self._info[key] = value

    def findChildByName(self, name):
        for controller in self._controllers:
            if controller.name == name:
                return controller
        return None

    def getChannelByPath(self, path):
        previousLevel = self
        for part in path:
            if isinstance(previousLevel, _AnimDataModel):
                childNode = self.findChildByName(part)
                if childNode is None:
                    return None
                previousLevel = childNode
            elif isinstance(previousLevel, _AnimDataController):
                childNode = previousLevel.findChildByName(part)
                if childNode is None:
                    return None
                previousLevel = childNode
        return previousLevel


class _AnimDataController(object):
    def __init__(self, parent, name=None, track=None, trackId=None, trackType=None):
        super(_AnimDataController, self).__init__()
        self._children = []
        self._values = []
        self._track = track
        self._id = trackId
        self._type = trackType
        self._name = name or ""
        self._parent = parent

    @property
    def parent(self):
        return self._parent

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, newName):
        self._name = newName

    @property
    def track(self):
        return self._track

    @track.setter
    def track(self, value):
        self._track = value

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def trackType(self):
        return self._type

    @trackType.setter
    def trackType(self, value):
        self._type = value

    def fullPath(self):
        if self._parent is None:
            paths = []
        else:
            paths = self._parent.fullPath()
        paths.append(self._name)
        return paths

    def addNewChildController(self, name=None, track=None, trackId=None, trackType=None):
        newController = _AnimDataController(self, name=name, track=track, trackId=trackId, trackType=trackType)
        self._children.append(newController)
        return newController

    def addChildController(self, newController):
        self._children.append(newController)
        return newController

    def _getOrCreateControllerByName(self, controllerName, createNew=False):
        for controller in self._children:
            if controller.name == controllerName:
                return controller
        if createNew:
            return self.addNewChildController(name=controllerName)
        raise ValueError("Unable to find '{0}' in controller '{1}'".format(controllerName, self.name))

    def getControllerByName(self, controllerName):
        return self._getOrCreateControllerByName(controllerName, False)

    def getOrCreateControllerByName(self, controllerName):
        return self._getOrCreateControllerByName(controllerName, True)

    def addNewChannel(self, channelName=None):
        newChannelValue = _AnimDataChannelValues(self, self, channelName=channelName)
        self._values.append(newChannelValue)
        return newChannelValue

    def addChannel(self, newChannelValue):
        self._values.append(newChannelValue)
        return newChannelValue

    def _getOrCreateChannel(self, channelName, createNew=False):
        for channel in self._values:
            if channel.channelName == channelName:
                return channel
        if createNew:
            return self.addNewChannel(channelName=channelName)
        raise ValueError("Unable to find '{0}' in controller '{1}'".format(channelName, self.name))

    def getChannelByName(self, channelName):
        return self._getOrCreateChannel(channelName, False)

    def getOrCreateChannelByName(self, channelName):
        return self._getOrCreateChannel(channelName, True)

    def setChannelValue(self, channelName, newValue, frameNumber):
        channel = self.getChannelByName(channelName)
        channel.setData(newValue, frameNumber)

    def setOrAddChannelValue(self, channelName, newValue, frameNumber):
        channel = self.getOrCreateChannelByName(channelName)
        channel.setData(newValue, frameNumber)

    @property
    def children(self):
        return self._children

    @property
    def channels(self):
        return self._values

    def findChildByName(self, name):
        for controller in self._children:
            if controller.name == name:
                return controller
        for channel in self._values:
            if channel.channelName == name:
                return channel
        return None


class _AnimDataChannelValues(object):
    def __init__(self, parent, channelName=None):
        super(_AnimDataChannelValues, self).__init__()
        self._channelName = channelName or ""
        self._channelValue = {}
        self._parent = parent

    @property
    def parent(self):
        return self._parent

    def fullPath(self):
        paths = self._parent.fullPath()
        paths.append(self._channelName)

    def data(self, frameNumber):
        return self._channelValue.get(frameNumber, 0.0)

    def setData(self, newData, frameNumber):
        self._channelName[frameNumber] = newData

    @property
    def channelName(self):
        return self._channelName

    @channelName.setter
    def channelName(self, newName):
        self._channelName = newName


class AnimDumpController(_AnimDataController):
    def __init__(self, parent, mobuObject, track, trackType):
        super(AnimDumpController, self).__init__(parent, track=track, trackType=trackType)
        self._mobuObject = mobuObject
        # Add Channels based on type and properties on object
        if trackType == AnimDumpIndex.boneTranslation:
            trans = self._mobuObject.PropertyList.Find("Lcl Translation")
            for channel in xrange(3):
                self.addChannel(AnimDumpChannelValues(self, trans, channel))

        elif trackType == AnimDumpIndex.boneRotation:
            rot = self._mobuObject.PropertyList.Find("Lcl Rotation")
            for channel in xrange(3):
                self.addChannel(AnimDumpChannelValues(self, rot, channel))

        elif trackType == AnimDumpIndex.genericControl:
            pass
        else:
            print "I have no clue what do with this."

    @property
    def name(self):
        return "{0}:{1}".format(AnimDumpIndex.getNameFromIndex(self.trackType), self._mobuObject.Name)

    @property
    def id(self):
        idProperty = self._mobuObject.PropertyList.Find("MaxHandle")
        if idProperty is None:
            return 0
        return idProperty.Data


class AnimDumpChannelValues(_AnimDataChannelValues):

    _player = mobu.FBPlayerControl()
    _system = mobu.FBSystem()

    @classmethod
    def _gotoFrame(cls, frameNumber):
        lTime = mobu.FBTime()
        lTime.SetFrame(frameNumber)
        if lTime == cls._system.LocalTime.Get():
            return
        cls._player.Goto(lTime)

    def __init__(self, parent, mobuObject, channelIdx):
        channelName = {0:"X", 1:"Y", 2: "Z"}.get(channelIdx)
        super(AnimDumpChannelValues, self).__init__(parent, channelName=channelName)
        self._channelValue = {}
        self._mobuObject = mobuObject
        self._channelIdx = channelIdx

    def data(self, frameNumber, usePreStore=True):
        value = None
        if usePreStore:
            value = self._channelValue.get(frameNumber)

        if value is None:
            self._gotoFrame(frameNumber)
            value = self._mobuObject.Data[self._channelIdx]
            self._channelValue[frameNumber] = value

        return value
