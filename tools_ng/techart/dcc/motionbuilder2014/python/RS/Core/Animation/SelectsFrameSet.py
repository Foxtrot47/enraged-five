'''

 Script Path: RS/Core/Animation/SelectsFrameSet.py

 Written And Maintained By: Kathryn Bodey

 Created: 14.05.2014

 Description:

'''


from pyfbsdk import *

import RS.Config
import RS.Globals
import RS.Utils.Scene
import RS.Utils.Scene.Plot
import RS.Core.Metadata
import RS.Core.Reference.HealthCheck as healthcheck
from RS.Utils.Scene import Constraint

import os


def CreateSelectsRangesNull():

    selectsDict = {}

    selectsNull = FBModelNull('selects_frame_ranges')

    tag = selectsNull.PropertyCreate('Frame Selects', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = selectsNull.Name

    tag = selectsNull.PropertyCreate('HUD Text Display' , FBPropertyType.kFBPT_enum, "", True, True, None)
    tag.SetAnimated(True)
    enumList = tag.GetEnumStringList(True)
    enumList.Add('None')

    return selectsNull


def AddFrameProperty(null, frameStart, frameStop, label):

    frameProperty = null.PropertyCreate('Select Range', FBPropertyType.kFBPT_charptr, "", False, True, None)
    frameProperty.Data = str(int(frameStart)) + '~' + str(int(frameStop)) + '~' + str(label)

    # create constraint
    CreateSelectsRangesConstraint(null)


def CreateSelectsRangesConstraint(null):

    # check if constraint exists already
    hudProperty = None
    for i in range(null.GetSrcCount()):
        if null.GetSrc(i).ClassName() == 'FBConstraintRelation':
            hudProperty = null.GetSrc(i).PropertyList.Find('Selects Relation')
            hudConstraint = null.GetSrc(i)

    if hudProperty:
        # constraint exists, delete it and re-populate a new one
        hudConstraint.FBDelete()
        SelectsRangesConstraint(null)

    else:
        # create a new constraint
        SelectsRangesConstraint(null)

    # connect and/or create HUD if it is not already connected
    FindHUD(null)


def SelectsRangesConstraint(null):

    if null:

        rangeDict = {}
        enumList = []

        hudPropertyEnum = null.PropertyList.Find('HUD Text Display')
        enumStringList = hudPropertyEnum.GetEnumStringList(True)
        for i in enumStringList:
            enumList.append(i)

        # get a dict of start and end frames
        for i in null.PropertyList:
            if 'Select Range' in i.Name:
                labelIndex = 0
                propertyData = i.Data
                split = propertyData.split('~')
                startFrame = split[0]
                endFrame = split[1]
                labelName = split[-1]
                for index in range(len(enumList)):
                    if enumList[index] == labelName:
                        labelIndex = index

                startFrame = int(startFrame)
                endFrame = int(endFrame)

                rangeDict[ i ] = (startFrame, endFrame, labelIndex)

        if len(rangeDict) > 0 and hudPropertyEnum:

            # create constraint
            hudConstraint = Constraint.CreateConstraint(Constraint.RELATION)
            hudConstraint.Name = ("hud_selects_relation")

            tag = hudConstraint.PropertyCreate('Selects Relation', FBPropertyType.kFBPT_charptr, "", False, True, None)
            tag.Data = hudConstraint.Name

            systemBox = hudConstraint.CreateFunctionBox('System', 'Local Time')
            hudConstraint.SetBoxPosition(systemBox, 0, 300)

            convertBox = hudConstraint.CreateFunctionBox('Converters', 'Time to seconds')
            hudConstraint.SetBoxPosition(convertBox, 400, 300)

            numberSumBox = hudConstraint.CreateFunctionBox('Number', 'Sum 10 numbers')
            hudConstraint.SetBoxPosition(numberSumBox , 2400, 300)

            targetBox = hudConstraint.ConstrainObject(null)
            hudConstraint.SetBoxPosition(targetBox , 2800, 300)

            RS.Utils.Scene.ConnectBox(systemBox, 'Result', convertBox, 'Time')

            yPos = 300
            counter = 0
            secondCounter = 0
            alphabetList = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' ]
            secondAlphabetList = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' ]
            numberSumBox2 = None

            for key, value in rangeDict.iteritems():

                propertyData = key.Data
                startFrame = value[0]
                endFrame = value[1]
                labelIndex = value[2]

                if counter <= 9:

                    alphabet = alphabetList[counter]

                    numberBox = hudConstraint.CreateFunctionBox('Number', 'Is Between A and B')
                    hudConstraint.SetBoxPosition(numberBox, 1000, yPos)
                    dataA = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'a')
                    dataA.WriteData([ float(startFrame) / 30])
                    dataB = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'b')
                    dataB.WriteData([ float(endFrame) / 30])

                    andBox = hudConstraint.CreateFunctionBox('Boolean', 'AND')
                    andBox.Name = propertyData
                    hudConstraint.SetBoxPosition(andBox, 1400, yPos)
                    dataB = RS.Utils.Scene.FindAnimationNode(andBox.AnimationNodeInGet(), 'b')
                    dataB.WriteData([ 1 ])

                    ifElseBox = hudConstraint.CreateFunctionBox('Number', 'IF Cond Then A Else B')
                    hudConstraint.SetBoxPosition(ifElseBox, 2000, yPos)
                    dataA = RS.Utils.Scene.FindAnimationNode(ifElseBox.AnimationNodeInGet(), 'a')
                    dataA.WriteData([ labelIndex ])
                    dataB = RS.Utils.Scene.FindAnimationNode(ifElseBox.AnimationNodeInGet(), 'b')
                    dataB.WriteData([ 0 ])

                    RS.Utils.Scene.ConnectBox(convertBox, 'Result', numberBox, 'Value')
                    RS.Utils.Scene.ConnectBox(numberBox, 'Result', andBox, 'a')
                    RS.Utils.Scene.ConnectBox(andBox, 'Result', ifElseBox, 'Cond')
                    RS.Utils.Scene.ConnectBox(ifElseBox, 'Result', numberSumBox, alphabet)
                    RS.Utils.Scene.ConnectBox(numberSumBox, 'Result', targetBox, hudPropertyEnum.Name)

                    yPos = yPos + 100
                    counter = counter + 1

                # if the user needs more than 10 selects, it will create a new sum box for an extra 10 entries.  Limit is now 20.
                else:
                    if secondCounter <= 9:

                        if numberSumBox2:
                            None
                        else:
                            numberSumBox2 = hudConstraint.CreateFunctionBox('Number', 'Sum 10 numbers')
                            hudConstraint.SetBoxPosition(numberSumBox2 , 2400, 1300)

                        secondAlphabet = secondAlphabetList[secondCounter]

                        numberBox = hudConstraint.CreateFunctionBox('Number', 'Is Between A and B')
                        hudConstraint.SetBoxPosition(numberBox, 1000, yPos)
                        dataA = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'a')
                        dataA.WriteData([ float(startFrame) / 30])
                        dataB = RS.Utils.Scene.FindAnimationNode(numberBox.AnimationNodeInGet(), 'b')
                        dataB.WriteData([ float(endFrame) / 30])

                        andBox = hudConstraint.CreateFunctionBox('Boolean', 'AND')
                        andBox.Name = propertyData
                        hudConstraint.SetBoxPosition(andBox, 1400, yPos)
                        dataB = RS.Utils.Scene.FindAnimationNode(andBox.AnimationNodeInGet(), 'b')
                        dataB.WriteData([ 1 ])

                        ifElseBox = hudConstraint.CreateFunctionBox('Number', 'IF Cond Then A Else B')
                        hudConstraint.SetBoxPosition(ifElseBox, 2000, yPos)
                        dataA = RS.Utils.Scene.FindAnimationNode(ifElseBox.AnimationNodeInGet(), 'a')
                        dataA.WriteData([ labelIndex ])
                        dataB = RS.Utils.Scene.FindAnimationNode(ifElseBox.AnimationNodeInGet(), 'b')
                        dataB.WriteData([ 0 ])

                        RS.Utils.Scene.ConnectBox(convertBox, 'Result', numberBox, 'Value')
                        RS.Utils.Scene.ConnectBox(numberBox, 'Result', andBox, 'a')
                        RS.Utils.Scene.ConnectBox(andBox, 'Result', ifElseBox, 'Cond')
                        RS.Utils.Scene.ConnectBox(ifElseBox, 'Result', numberSumBox2, secondAlphabet)
                        RS.Utils.Scene.ConnectBox(numberSumBox2, 'Result', targetBox, hudPropertyEnum.Name)

                        yPos = yPos + 100
                        secondCounter = secondCounter + 1

                    else:
                        None

def CreateHUD():

    scene = FBSystem().Scene

    hud = None

    hud = FBHUD("perspective_camera_selects_hud")
    tag = hud.PropertyCreate('Selects HUD', FBPropertyType.kFBPT_charptr, "", False, True, None)
    tag.Data = hud.Name

    scene.ConnectSrc(hud)

    scene.Cameras[0].ConnectSrc(hud)

    return hud


def CreateHUDTextElement(hud, null):

    nullProperty = null.PropertyList.Find("HUD Text Display")

    if nullProperty:
        HUDTextElement = FBHUDTextElement("selects_text_element")
        HUDTextElement.Content = ''
        HUDTextElement.PropertyList.Find('Justification').Data = 1
        HUDTextElement.PropertyList.Find('Horizontal Dock').Data = 1
        HUDTextElement.PropertyList.Find('Height').Data = 8
        HUDTextElement.PropertyAddReferenceProperty(nullProperty)
        tag = HUDTextElement.PropertyCreate('Selects HUD Text', FBPropertyType.kFBPT_charptr, "", False, True, None)
        tag.Data = HUDTextElement.Name

        hud.ConnectSrc(HUDTextElement)
    else:
        print 'not found hud prop'


def FindHUD(null):

    scene = FBSystem().Scene

    nullProperty = null.PropertyList.Find("HUD Text Display")

    if nullProperty:

        # check if a HUD exists already that is attached to the perspective camera
        hudFoundAndConnected = False
        selectTextElementFound = False
        textElementList = []

        if scene.HUDs:
            for i in scene.HUDs:
                # check that it is connected to the perspective camera
                for j in range(i.GetDstCount()):
                    if i.GetDst(j).ClassName() == 'FBCamera' and i.GetDst(j).Name == 'Producer Perspective':
                        hudFoundAndConnected = True
                        hud = i


        if hudFoundAndConnected == True:
            # check that a text element exists
            if hud.GetSrcCount() != 0:
                for i in range(hud.GetSrcCount()):
                    if hud.GetSrc(i).ClassName() == 'FBHUDTextElement':
                        selectsTextElementProperty = hud.GetSrc(i).PropertyList.Find('Selects HUD Text')
                        if selectsTextElementProperty:
                            selectTextElementFound = True

            if selectTextElementFound == True:
                None
            else:
                CreateHUDTextElement(hud, null)

        else:
            # create hud & text element
            hud = CreateHUD()
            CreateHUDTextElement(hud, null)


def CreateNewTake(label, startRange, endRange):
    # create new take, copied from current take
    system = FBSystem()
    newTake = system.CurrentTake.CopyTake(str(label))
    system.CurrentTake = newTake

    # change hard range to select ranges
    FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan(FBTime(0, 0, 0, startRange), FBTime(0, 0, 0, endRange))

    '''
    # plot characters
    characterNodeList = []
    plotOptions = FBPlotOptions()
    plotOptions.UseConstantKeyReducer = False
    plotOptions.PlotAllTakes = False
    plotOptions.PlotTranslationOnRootOnly = False
    plotOptions.RotationFilterToApply = FBRotationFilter.kFBRotationFilterUnroll

    for i in RS.Globals.Characters:


        if i.LongName.startswith( 'gs' ):
            None
        else:
            print "Plotting: {0}".format(i.LongName)

            i.PlotAnimation( FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions )
            if i.InputType == FBCharacterInputType.kFBCharacterInputMarkerSet:
                i.ActiveInput = True

            RS.Globals.Scene.Evaluate()
        # make a list of character nodes under the dummy
        hips = i.GetModel( FBBodyNodeId.kFBHipsNodeId )
        dummy = RS.Utils.Scene.GetParent( hips )
        RS.Utils.Scene.GetChildren( dummy, characterNodeList, "", True )

    RS.Utils.Scene.DeSelectAll()

    #print characterNodeList

    # plot everything else
    sceneNodeList = []

    for i in RS.Globals.Components:
        if i.ClassName() in [ 'FBComponent', 'FBMesh', 'FBModel', 'FBModelMarker', 'FBModelNull', 'FBModelRoot', 'FBModelSkeleton' ]:
            if 'dummy01' in i.LongName.lower():
                if i not in characterNodeList:
                    RS.Utils.Scene.GetChildren( i, sceneNodeList, "", True )

    for i in sceneNodeList:
        i.Selected = True

    RS.Utils.Scene.Plot.PlotCurrentTakeonSelected()

    RS.Utils.Scene.DeSelectAll()
    '''


def CreateMetaFile2(filename, rangesList, notes):
    definitionsFilePath = os.path.join(RS.Config.Project.Path.Root, 'assets', 'metadata', 'definitions', 'tools', 'motionbuilder', 'rangeSelects.psc')
    RS.Perforce.Sync(definitionsFilePath)

    if os.path.exists(definitionsFilePath):
        metaFile = RS.Core.Metadata.CreateMetaFile("RangeSelectsFile")
        metaFile.SceneName = "TestScene"
        metaFile.Notes = notes

        index = 0

        for item in rangesList:
            rangeSelects = metaFile.RangeSelectsList.Create()
            rangeSelects.Index = index
            rangeSelects.StartRange = item.Start
            rangeSelects.EndRange = item.End
            rangeSelects.Label = item.Label

            index += 1

        metaPath = filename
        metaFile.SetFilePath(metaPath)
        metaFile.Save()
        FBMessageBox('R*', 'Ranges Saved as .xml:\n\n{0}'.format(metaPath), 'Ok')

    else:
        FBMessageBox('R* Error', 'Unable to find .psc definitions file.\n\nCheck you have the following file:\n\n{0}'.format(definitionsFilePath), 'Ok')

def CreateMetaFile(rangesDict, filename):
    # make sure the user has the latest .psc definitions file
    definitionsFilePath = os.path.join(RS.Config.Project.Path.Root, 'assets', 'metadata', 'definitions', 'techart', 'motionbuilder', 'rangeSelects.psc')
    RS.Perforce.Sync(definitionsFilePath)

    if os.path.exists(definitionsFilePath):
        # create meta file
        metaFile = RS.Core.Metadata.CreateMetaFile("RangeSelectsFile")
        metaFile.SceneName = "TestScene"

        for key, value in rangesDict.iteritems():
            index = int(key)
            start = int(value[0])
            end = int(value[1])
            label = str(value[2])
            rangeSelects = metaFile.RangeSelectsList.Create()
            rangeSelects.Index = index
            rangeSelects.StartRange = start
            rangeSelects.EndRange = end
            rangeSelects.Label = label

        metaPath = filename
        metaFile.SetFilePath(metaPath)
        metaFile.Save()
        FBMessageBox('R*', 'Ranges Saved as .xml:\n\n{0}'.format(metaPath), 'Ok')

    else:
        FBMessageBox('R* Error', 'Unable to find .psc definitions file.\n\nCheck you have the following file:\n\n{0}'.format(definitionsFilePath), 'Ok')


def OpenMetaFile2(filename):
    result = []

    if (os.path.exists(filename)):
        try:
            metaFile = RS.Core.Metadata.ParseMetaFile(filename)

            result.append(getattr(metaFile, "Notes", ""))
            ranges = []

            for i in metaFile.RangeSelectsList:
                startFrame = int(i.StartRange)
                endFrame = int(i.EndRange)
                label = str(i.Label)

                ranges.append((startFrame, endFrame, label))

            result.append(ranges)

        except:
            pass

    return result


def OpenMetaFile(filename):
    SelectFrameRangeNull = None
    metaPath = filename

    if os.path.exists(metaPath):

        try:
            metaFile = RS.Core.Metadata.ParseMetaFile(metaPath)

            for i in metaFile.RangeSelectsList:
                index = str(i.Index) + '.'
                startFrame = str(i.StartRange)
                endFrame = str(i.EndRange)
                label = str(i.Label)

                if SelectFrameRangeNull:
                    enumProperty = SelectFrameRangeNull.PropertyList.Find('HUD Text Display')
                    if enumProperty:
                        enumList = enumProperty.GetEnumStringList(True)
                        enumList.Add(label)

                    # add new property range to existing null
                    propertyData = AddFrameProperty(SelectFrameRangeNull, startFrame, endFrame, label)
                else:
                    # create the selects_frame_ranges null and add initial property
                    SelectFrameRangeNull = CreateSelectsRangesNull()

                    enumProperty = SelectFrameRangeNull.PropertyList.Find('HUD Text Display')
                    if enumProperty:
                        enumList = enumProperty.GetEnumStringList(True)
                        enumList.Add(label)

                    propertyData = AddFrameProperty(SelectFrameRangeNull, startFrame, endFrame, label)

            FBMessageBox('R*', 'Ranges Loaded.', 'Ok')

        except:
            pass
    else:
        FBMessageBox('R* Error', 'Unable to find meta file to load Select Ranges.\n\n{0}'.format(metaPath), 'Ok')


def UpdateOldSetup(selectFrameRangeNull):
    # check if the file is using the old custom property layout
    oldSelectDict = {}
    if selectFrameRangeNull:
        for i in selectFrameRangeNull.PropertyList:
            if 'Select Range' in i.Name:
                if '~' in i.Data:
                    None
                    break
                else:
                    oldSelectDict[ i ] = i.Data

        if oldSelectDict:
            for key, value in oldSelectDict.iteritems():
                split = value.split('-')
                start = str(split[0])
                end = str(split[-1])

                newDataString = start + '~' + end + '~' + 'Select'

                for i in selectFrameRangeNull.PropertyList:
                    if key == i:
                        i.Data = newDataString

