"""
Library module for animation related tools and functions.

Author:
    * Jason Hayes <jason.hayes@rockstarsandiego.com>
    * Thanh Phan <thanh.phan@rockstarsandiego.com>
    * David Vega <david.vega@rockstargames.com>
"""

import os
import re
import shutil
import tempfile
import subprocess
from xml.etree import cElementTree as xml

from PySide import QtGui

import pyfbsdk as mobu

from RS import Config, Globals, Perforce, ProjectData
from RS.Utils import Path
from RS.Utils import Scene
from RS.Utils import Creation
from RS.Utils import Namespace
from RS.Utils.Scene import Component
PLOT_TYPE = {
    'rig'      :mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig, 
    'skeleton' :mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton
    }


class InputTypes(object):
    """
    Stores the input type constants from the pyfbsdk lib
    """
    Character = mobu.FBCharacterInputType.kFBCharacterInputCharacter
    Rig = mobu.FBCharacterInputType.kFBCharacterInputMarkerSet
    Stance = mobu.FBCharacterInputType.kFBCharacterInputStance

    _FindDictionary = {mobu.FBCharacterInputType.kFBCharacterInputCharacter: "Character",
                       mobu.FBCharacterInputType.kFBCharacterInputMarkerSet: "Rig",
                       mobu.FBCharacterInputType.kFBCharacterInputStance: "Stance"}

    @classmethod
    def GetNameFromType(cls, characterInputType):
        """
        Returns the name of the constant in this class that corresponds with the passed pyfbsdk FBCharacterInputType
        constant

        Arguments:
            characterInputType (pyfbsdk.FBCharacterInputType): motion builder character input type

        Return:
            string
        """
        return cls._FindDictionary.get(characterInputType, None)


def ThumbJointOrientationToggle(lNamespace):
    """
    Orients the thumb joints

    Arguments:
        lNamespace (string): namespace for the character whose thumbs you want to edit

    """
    
    # Finger Rotation Values
    rotationValues_IG = {"SKEL_R_Finger00":mobu.FBVector3d(10.344, 15.851, 30.603),
                         "SKEL_R_Finger01":mobu.FBVector3d(-0.404659, -1.43456, -16.5556),
                         "SKEL_R_Finger02":mobu.FBVector3d(-0.0504775, -1.16117, -13.269),
                         "SKEL_L_Finger00":mobu.FBVector3d(-10.3441, -15.8509, 30.6029),
                         "SKEL_L_Finger01":mobu.FBVector3d(0.4047, 1.43454, -16.5556),
                         "SKEL_L_Finger02":mobu.FBVector3d(0.0501915, 1.16085, -13.2691)
                         }
    
    rotationValues_CS = {"SKEL_R_Finger00":mobu.FBVector3d(90, 0, 0),
                         "SKEL_R_Finger01":mobu.FBVector3d(0, 0, 0),
                         "SKEL_R_Finger02":mobu.FBVector3d(0, 0, 0),
                         "SKEL_L_Finger00":mobu.FBVector3d(-90, 0, 0),
                         "SKEL_L_Finger01":mobu.FBVector3d(0, 0, 0),
                         "SKEL_L_Finger02":mobu.FBVector3d(0, 0, 0)
                         }
    
    # Find Mover Constraint
    lMoverConstraint = None
    for each in Globals.gConstraints:
        if each.LongName == "{0}:mover_toybox_Control".format(lNamespace):
            lMoverConstraint = each
            
    # Find Character
    lCharacter = None
    for each in Globals.gCharacters:
        if each.LongName.split(":")[0] == lNamespace:
            lCharacter = each

    if lMoverConstraint is not None and lCharacter is not None:
        
        # Store initial state and prepare
        lMoverState = lMoverConstraint.Active
        lMoverConstraint.Active = False
        lCharacter.SetCharacterizeOff()
        Globals.gScene.Evaluate()
        
        # Determine the finger state
        lArray = rotationValues_CS
        lFingerCheck = mobu.FBFindModelByLabelName("{0}:{1}".format(lNamespace, "SKEL_R_Finger00"))
        if 89.9 < lFingerCheck.Rotation[0] < 90.1:
            lArray = rotationValues_IG

        # Set to match the other state
        for each in lArray:
            lFinger = mobu.FBFindModelByLabelName("{0}:{1}".format(lNamespace, each))
            lFinger.Rotation = lArray[each]
            Globals.gScene.Evaluate()
            
        # Post Preparation
        lCharacter.SetCharacterizeOn(True)
        lMoverConstraint.Active = lMoverState
        Globals.gScene.Evaluate()


def CopyPropertyValues(source, destinations, properties=["Translation (Lcl)", "Rotation (Lcl)"]):

    if not isinstance(destinations, (tuple, list)):
        destinations = [destinations]

    propertyValues = {propertyName: source.PropertyList.Find(propertyName).Data for propertyName in properties
                      if source.PropertyList.Find(propertyName) is not None}
    [Component.SetProperties(destination, **propertyValues) for destination in destinations]


def CopyAnimation(source, destinations, properties=["Translation (Lcl)", "Rotation (Lcl)"], transfer=False):
    """
    Copies animation from the source component to the destination components

    Arguments:
        source (FBComponent): component to copy animation from
        destinations  (list[FBComponent, etc.]): components to copy animation to
        properties (list[string, etc.]): name of the properties whose animation should be copied
        transfer (boolean): if the animation from the source component should be deleted after copying it to the
                          destination objects
    """
    for propertyName in properties:
        sourceProperty = source.PropertyList.Find(propertyName)
        sourceAnimationNode = getattr(sourceProperty, "GetAnimationNode", lambda *args: None)()
        if not sourceProperty or not sourceAnimationNode:
            continue
            
        for destination in destinations:

            destinationProperty = destination.PropertyList.Find(propertyName)

            if type(sourceProperty) != type(destinationProperty):
                continue

            destinationProperty.SetAnimated(True)

            destinationAnimationNode = destinationProperty.GetAnimationNode()

            if destinationProperty is None or destinationAnimationNode is None:
                continue

            if destinationAnimationNode.Nodes:
                for index, node in enumerate(destinationAnimationNode.Nodes):

                    node.FCurve.KeyReplaceBy(sourceAnimationNode.Nodes[index].FCurve)
            else:
                destinationAnimationNode.FCurve.KeyReplaceBy(sourceAnimationNode.FCurve)

        if transfer:
            sourceAnimationNode.FCurve.EditClear()


def ClearAnimation(component, properties=[], allProperties=False):
    """
    Clear the animation from the provided properties of a component

    Arguments:
        component (pyfbsdk.FBComponent): component to clear animation from
        properties (list[pyfbsdk.FBProperty, etc.]): list of FBProperties to clear
        allProperties (boolean): clear all the properties of the component
    """
    if allProperties:
        properties = component.PropertyList

    for property in properties:
        ClearAnimationFromProperty(property)


def ClearAnimationFromProperty(property):
    """
    Clears the animation from a property

    Arguments:
        property (pyfbsdk.FBProperty): property with animation to clear
    """
    if property.IsAnimatable() and property.IsAnimated() and property.GetAnimationNode():

        animationNodes = property.GetAnimationNode().Nodes
        if not animationNodes:
            animationNodes = [property.GetAnimationNode()]

        for eachNode in animationNodes:
            eachNode.FCurve.EditClear()

# --- Get/Query Methods ---


def GetCurrentCharacter(warning=False):
    """
    Gets the current / last selected character

    Arguments:
        warning = (boolean):  When true it launches a dialog window when the command fails

    Return:
       mobu.FBCharacter() instance
    """
    lApp = mobu.FBApplication()
    lCurrentCharacter = lApp.CurrentCharacter

    if not lCurrentCharacter and warning:
        QtGui.QMessageBox.warning(None, "Anim2FBX Error", "No Character Selected")

    return lCurrentCharacter


def GetNamespace(character=None, warning=False):
    """
    Gets the Namespace of the provided character

    Arguments:
        character (mobu.FBComponent): The component whose namespace you want, defaults to the selected
                                                 FBCharacter
        warning (boolean): When true it launches a dialog window when the command fails

    Return:
        string
    """
    if not character:
        character = GetCurrentCharacter(warning)

    namespace = Namespace.GetNamespace(character)

    if not namespace and warning:
       QtGui.QMessageBox.warning(None, "Anim2FBX Error", "You did not use the reference system to load your character.")

    return namespace


# --- FBTake Methods ---


def CopyTake(path, use_take=False, warning=True):
    """
    Copies the current take
     
    Arguments: 
        path (string): path to an anim file. The name of the file is used as the name of the new take
        use_take (boolean): use the current take instead of creating a copy, this clears animation on the current take
        warning (boolean): if a warning should pop up that you are going to use the current take
    """
    # use current take instead of a copy if checkbox is checked
    lTake = mobu.FBSystem().CurrentTake

    if use_take and warning:
        # Throw a Messagebox warning to make sure this is what is desired
        use_take = QtGui.QMessageBox.warning(None, "WARNING WARNING WARNING!!!!!!!!!!!!!!!!!!",
                                   "This will overwrite an existing anim on the take."
                                   " Are you sure you want to do this?",
                                   QtGui.QMessageBox.Yes, QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes

    baseName = str(Path.GetBaseNameNoExtension(path))

    if not use_take:
        lTake = lTake.CopyTake(baseName)

        # CopyTake copies the animation data too
        # We need to get rid of them.
        lTake.ClearAllProperties(False)
    return lTake

# --- Set Properties Methods ---


def EnableRotationDOF(boneName, setting=True, namespace=None):
    """
    Enable Degrees of Freedom

    Arguments:
        boneName (string): name of the FBModelBone to enable degrees of freedom on
        setting (boolean): should rotation be active
        namespace (string): namespace of the bone to enable degrees of freedom on
    """
    if not namespace:
        namespace = GetNamespace()
    lBone = mobu.FBFindModelByLabelName("{}:{}".format(namespace, boneName))
    lBone.PropertyList.Find("RotationActive").Data = setting


def SetPlayerControl():
    """ Sets the player control to 30 FPS """
    lPlayerControl = mobu.FBPlayerControl()
    lPlayerControl.SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeSnapAndPlayOnFrames
    lPlayerControl.SetTransportFps(mobu.FBTimeMode.kFBTimeMode30Frames)


def Plot(character, plot_type, CreateControlRig=False, AllTakes=False, OnFrame=True, Period=(0, 0, 0, 1),
         RotationFilter= mobu.FBRotationFilter.kFBRotationFilterGimbleKiller, UseConstantKeyReducer=True,
         ConstantKeyReducerKeepOnKey=True, TranslationOnRootOnly=True, ForceInputActive=False):
    """
    Plots animation on a character. It will plot from the current input to the skeleton, current input to the rig, or
    if the current input is the rig, skeleton to the rig.

    Arguments:
        character (mobu.FBCharacter): character to plot
        plot_type (string): to plot to skeleton or rig
        CreateControlRig (boolean): create a control rig for the character
        AllTakes (boolean): plot the current animation to all the takes
        OnFrame (boolean): ?
        Period (list[int, int, int, int]): the number of frames between keyframes.
        RotationFilter (pyfbsdk.FBRotationFilter): Rotation filter to apply to the animation
        UseConstantKeyReducer (boolean): Removes keys whose values do not change over for consecutive frames
        ConstantKeyReducerKeepOnKey (boolean): keeps one frame keyed instead of removing all keys when the
                                               ConstantKeyReducer parameter is used
        TranslationOnRootOnly (boolean): only key translation values on the root node of the character
        ForceInputActive (boolean): activates the characters input if it isn't activated

    Return:
        boolean
    """
    plot_location = PLOT_TYPE.get(plot_type, PLOT_TYPE['skeleton'])

    # Create a control rig using Forward and Inverse Kinematics,
    # as specified by the "True" parameter.
    active = character.ActiveInput
    previousType = character.InputType

    # Create Control Rig and make sure that it isn't set to active
    if CreateControlRig:
        character.CreateControlRig(True)
        character.ActiveInput = False

    # If the character was being driven by another character make sure to set it back to being driven by that
    # character

    if previousType is InputTypes.Character:
        character.InputType = previousType
        character.ActiveInput = active

    # Turn on input if
    if ForceInputActive:
        # Set the control rig to active.
        character.ActiveInput = True

    plotOptions = mobu.FBPlotOptions()
    plotOptions.PlotAllTakes = AllTakes
    plotOptions.PlotOnFrame = OnFrame
    plotOptions.PlotPeriod = mobu.FBTime(*Period)
    plotOptions.RotationFilterToApply = RotationFilter
    plotOptions.UseConstantKeyReducer = UseConstantKeyReducer
    plotOptions.ConstantKeyReducerKeepOneKey = ConstantKeyReducerKeepOnKey
    plotOptions.PlotTranslationOnRootOnly = TranslationOnRootOnly

    character.PlotAnimation(plot_location, plotOptions)

    return True


def PlotCharacters(characters, plotLocations=('rig',), muteStory=True, takes=None, progressBar=None):
    """
    Plots the animation from characters to the skeleton or control rig

    Arguments:
        characters  (list[FBCharacters, etc]): list of characters to plot
        location (string): where the animation should be plotted to, ei. 'rig' for control rig or 'skeleton' for
                            skeleton.
        muteStory (boolean): mute Story
        progressBar (RS.Tools.UI.ExternalProgressBar.Launcher.ProgressBarController()): Progress Bar to update
    """
    system = mobu.FBSystem()

    currentTake = system.CurrentTake
    if not takes:
        takes = [currentTake]

    allCharacters = system.Scene.Characters
    storyTrackDictionary = {allCharacters[track.CharacterIndex - 1]: track for track in
                            mobu.FBStory().RootFolder.Tracks if track.CharacterIndex}
    for take in takes:
        system.CurrentTake = take
        for character in characters:

            # If a story track is unmuted, we force the control rig to be active
            # this is so the animation gets plotted correctly to the base skeleton

            ForceRigActive = storyTrackDictionary.get(character, False) and not storyTrackDictionary[character].Mute \
                             and character.InputType is InputTypes.Rig

            # Check if the control rig is the driving input
            # The default value is Stance, so it is stance then we assume that the rig should be driving it

            # If a character only has animation plotted to the bones , we plot the animation to the control rig first
            # and then the skeleton to make sure the animation plots correctly. Otherwise the control rig is created at
            # TPose and the TPose is plotted to the skeleton, killing the animation

            if plotLocations[0] == "skeleton" and not character.ActiveInput:
                plotLocations = ("rig", "skeleton")

            for plotLocation in plotLocations:
                # We check if the control rig exists again because it will be created during the loop if it was missing

                CreateControlRig = not HasControlRig(character)

                if progressBar is not None:
                    # Turn the story track of the character off it has a story track.
                    characterInput = "Skeleton"

                    if plotLocation in "skeleton":
                        characterInput = InputTypes.GetNameFromType(character.InputType)

                    text = "{} | Plotting {} from {} to {}".format(take.Name, character.LongName, characterInput,
                                                                   plotLocation.capitalize())

                    value = progressBar.Value + 1
                    progressBar.Update(text=text, value=value)

                    mobu.FBSystem().Scene.Evaluate()

                # Plot animation
                Plot(character, plotLocation, CreateControlRig=CreateControlRig, ForceInputActive=ForceRigActive)

                # Mute Story
                if (muteStory or character.InputType is InputTypes.Rig) and storyTrackDictionary.get(character, None):
                    storyTrackDictionary[character].Mute = True

    system.CurrentTake = currentTake


def PlotOnFrame(component, properties=("Translation", "Rotation"), frame=-100, children=True):
    """
    Sets a key with values from the current frame on the properties provided to the function.
    If recursive is set true, keys are set on all the children components as well.

    Arguments:
        component (pyfbsdk.FBModel): Any object that is considered a model by MB
                            (pyfbsdk.FBModel(), pyfbsdk.FBModelNull, pyfbsdk.FBLight, etc.)
        properties (list[string, etc.]): any property that you want to have keyed.
        frame (int): frame number to create keys on
        children (boolean): key properties of all children components as well

    Return:
        list(pyfbsdk.FBComponents, etc.)
    """
    if isinstance(component, basestring):
        component = mobu.FBFindModelByLabelName(component)

    if not component:
        return

    for eachProperty in properties:
        property = getattr(component, eachProperty, component.PropertyList.Find(eachProperty))

        # Check to make sure we get the FBProperty instance of the property
        # Some properties return just the value, not the FBProperty instance
        # PropertyList.Find returns the FBProperty instance

        if not isinstance(property, mobu.FBProperty):
            property = component.PropertyList.Find(eachProperty)

        if property is None or not property.IsAnimatable():
            continue

        value = property.Data
        property.SetAnimated(True)

        animationNode = property.GetAnimationNode()

        keyableNodes = animationNode.Nodes

        # Some animation nodes have sub nodes that you can key
        # If the animation node does not have keyable subnodes
        # key the animation node we already have.

        if not len(keyableNodes):
            keyableNodes = [animationNode]
            value = [value]

        # We just need the index values of xyz

        for index, keyableNode in enumerate(keyableNodes):
            keyableNode.KeyAdd(
               mobu.FBTime(0, 0, 0, frame), float(value[index]))

    keyedComponents = [component]
    if children:
        for eachChild in component.Children:
            keyedComponents.extend(PlotOnFrame(eachChild, properties, frame, children))

    return keyedComponents


def PlotTimeRange(components, properties=("Translation", "Rotation"), frameRange=(None, None), frameStep=1,
                  children=True):
    """
    Plots animation for the properties of the components passed based on the time range provided.
    Arguments:
            components (list[pyfbsdk.FBModel, etc]): list of FBModels to plot animation to
            properties (list[string, etc.]): any property that you want to have keyed.
            startFrame (list[int, int]): frame range to set keys on, defaults to the range of the time slider if no
                                         values are provided.
            frameStep (int): frame step number, only accepts integers higher than one
            children (boolean): key properties of all children components as well
    """

    frameRange = list(frameRange)

    #  Change the frameRange to the values of the time slider if frameRange values are None
    LocalTime = mobu.FBSystem().CurrentTake.LocalTimeSpan
    # LocalTime = mobu.FBSystem().LocalTime
    for index, frame in enumerate(frameRange):
        # Zero is considered false but it is a valid frame value, so we check if the value is None instead
        if frame is None:
            frameRange[index] = getattr(LocalTime, ["GetStart", "GetStop"][index])().GetFrame()

    startFrame, endFrame = frameRange
    currentFrame = mobu.FBSystem().LocalTime.GetFrame()
    if frameStep < 1:
        frameStep = 1

    # Set keys in each time frame
    PlayerControl = mobu.FBPlayerControl()
    for frame in xrange(startFrame-1, endFrame, frameStep):
        frame += 1
        PlayerControl.Goto(mobu.FBTime(0, 0, 0, frame))
        skipComponents = []
        for component in components:
            if component not in skipComponents:
                skipComponents.extend(PlotOnFrame(component, properties=properties, frame=frame, children=children))
    PlayerControl.Goto(mobu.FBTime(0, 0, 0, currentFrame))


def PlotProperties(components, properties=("Lcl Translation", "Lcl Rotation"), children=True, frameRange=(None, None),
                   frameStep=1, allTakes=False, onFrame=True,
                   rotationFilter= mobu.FBRotationFilter.kFBRotationFilterGimbleKiller, useConstantKeyReducer=True,
                   constantKeyReducerKeepOnKey=True, translationOnRootOnly=False,
                   maxDepth=None, depth=0):
    """
    Plots animation for the properties of the components passed based on the time range provided using the
    PlotTakeOnSelectedProperties method provided by motion builder.

    Arguments:
            components (list[pyfbsdk.FBModel, etc]): list of FBModels to plot animation to
            properties (list[string, etc.]): any property that you want to have keyed.
            startFrame (list[int, int]): frame range to set keys on, defaults to the range of the time slider if no
                                         values are provided.
            frameStep (int): frame step number, only accepts integers higher than one
            children (boolean): key properties of all children components as well
    """
    # Set focus to properties we want to plot
    for component in components:
        plotProperties = []
        for propertyName in properties:
            fbproperty = component.PropertyList.Find(propertyName)
            if fbproperty is not None:
                fbproperty.SetFocus(True)
                plotProperties.append(fbproperty)

        # Set focus to properties of child components
        if children and depth != maxDepth:
            for child in component. Children:
                PlotProperties([child], properties=properties, frameRange=frameRange, frameStep=frameStep,
                                children=children, depth=depth + 1)

    if depth == 0:
        # Set frame range for the take
        currentTake = mobu.FBSystem().CurrentTake

        # Change the frameRange to the values of the time slider if frameRange values aren't None
        if frameRange is not (None, None):
            localTime = currentTake.LocalTimeSpan
            ranges = []
            for index, frame in enumerate(frameRange):
                # Zero is considered false but it is a valid frame value, so we check if the value is None instead
                if frame is None:
                    frame = getattr(localTime, ("GetStart", "GetStop")[index])()
                else:
                    frame = mobu.FBTime(0, 0, 0, frame)
                ranges.append(frame)
            localTime.Set(*ranges)

        # Plot Options
        plotOptions = mobu.FBPlotOptions()
        plotOptions.UseConstantKeyReducer = useConstantKeyReducer
        plotOptions.ConstantKeyReducerKeepOneKey = constantKeyReducerKeepOnKey
        plotOptions.PlotAllTakes = allTakes
        plotOptions.PlotOnFrame = onFrame
        plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, frameStep)
        plotOptions.PlotTranslationOnRootOnly = translationOnRootOnly
        plotOptions.RotationFilterToApply = rotationFilter
        currentTake.PlotTakeOnSelectedProperties(plotOptions)

        [property.SetFocus(False) for property in plotProperties]


def MatchPosition(source, destinations):
    """
    Matches the position and orientation of the destination components to match that of the source component

    Arguments:
        source (pyfbsdk.FBComponent): component to get the position and orientation from
        destinations (list[pyfbsdk.FBComponent, etc.]): components whose positions and orientations should be set 

    """
    mobu.FBSystem().Scene.Evaluate()
    matrix = mobu.FBMatrix()
    source.GetMatrix(matrix)
    for destination in destinations:
        destination.SetMatrix(matrix)
        mobu.FBSystem().Scene.Evaluate()


def TransferMotion(parent, child):
    """
    Transfer translation and rotation from the child to the parent.

    Arguments:
        parent (pyfbsdk.FBComponent): parent component
        child (pyfbsdk.FBComponent): child component
    """
    # child is always first
    deleteConstraint = Creation.CreateConstraint("Parent/Child", "DELETE_ME", parent, child, Active=True, Lock=True)
    # Scene Evaluate is needed here to make sure that the attributes that have been set actually work
    mobu.FBSystem().Scene.Evaluate()

    PlotProperties([parent], children=False)
    deleteConstraint.FBDelete()


# --- Rig Utility Methods ---


def GetBonesFromCharacter(character):
    """
    skeleton components associated with the FBCharacter

    Arguments:
        character (pyfbsdk.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    return filter(None, [character.GetModel(getattr(mobu.FBBodyNodeId, bodyNodeIdName, None))
                         for bodyNodeIdName in dir(mobu.FBBodyNodeId) if "kFB" in bodyNodeIdName])


def GetControlsFromCharacter(character):
    """
    controls for the Control Rig associated with the FBCharacter

    Arguments:
        character (pyfbsdk.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    return GetFKControlsFromCharacter(character) + GetIKControlsFromCharacter(character)


def GetFKControlsFromCharacter(character):
    """
    fk controls for the Control Rig associated with theFBCharacter

    Arguments:
        character (pyfbsdk.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    ActivateControlRig(character, character.Active)
    ControlSet = character.GetCurrentControlSet()
    return filter(None, [ControlSet.GetFKModel(getattr(mobu.FBBodyNodeId, bodyNodeIdName, None))
                         for bodyNodeIdName in dir(mobu.FBBodyNodeId) if "kFB" in bodyNodeIdName
                         and not re.search("Translation|Invalid|Last", bodyNodeIdName)])


def GetIKControlsFromCharacter(character):
    """
    ik controls for the Control Rig associated with the FBCharacter

    Arguments:
        character (pyfbsdk.FBCharacter): Character to get components from

    Return:
        list[FBModel(), etc.]
    """
    ActivateControlRig(character, character.Active)
    ControlSet = character.GetCurrentControlSet()
    return filter(None, [ControlSet.GetIKEffectorModel(getattr(mobu.FBEffectorId, effectorNodeIdName, None), 0)
                         for effectorNodeIdName in dir(mobu.FBEffectorId) if "kFB" in effectorNodeIdName
                         and not re.search("Translation|Invalid|Last", effectorNodeIdName)])


def GetControlRigReferenceNull(character):
    """
    Gets the Reference Null that the control rig is parented under.

    Arguments:
        character (pyfbsdk.FBCharacter): character whose control rig reference null you want to get

    Return:
       FBModelNull
    """
    if not HasControlRig(character):
        ActivateControlRig(character)
    return character.GetCurrentControlSet().GetIKEffectorModel(mobu.FBEffectorId.kFBHipsEffectorId, 0).Parent


def HasControlRig(character):
    """
    Does a character have a control rig

    Arguments:
        character (pyfbsdk.FBCharacter): Character to check if it has a control rig

    Return:
        Boolean
    """
    hasControlRig = character.GetCurrentControlSet() is not None

    # GetCurrentControlSet will only work if the input is a control rig
    # So if the input type is something else, like Stance, we need to at attempt to switch it to control rig to get the
    # right value. If the character has not had a control rig generated, then switching to the control rig will just
    # fail
    if character.InputType is not InputTypes.Rig:

        previousInput = character.InputType
        isActive = character.ActiveInput

        character.InputType = InputTypes.Rig
        character.ActiveInput = True

        hasControlRig = character.GetCurrentControlSet() is not None

        character.InputType = previousInput
        character.ActiveInput = isActive

    return hasControlRig


def ActivateControlRig(character, active=True):
    """
    Activates or deactivates the Control Rig for selected characters in the scene
    Argument:
        character (pyfbsdk.FBCharacter): the character who needs their control rig activated
        active (boolean): set control rig to active
    """

    if not HasControlRig(character):
        # CreateControlRig accepts 0 and 1 as values, represent Ik and FK/IK respectively
        character.CreateControlRig(1)
    character.InputType = InputTypes.Rig
    character.Active = active


def SetControlRigVisibility(character, visible=True, active=True):
    """
    Activates or deactivates the Control Rig for selected characters in the scene
    Argument:
        character (pyfbsdk.FBCharacter): the character who needs their control rig visibility set
        visible (boolean): set control rig visibility
    """
    if not HasControlRig(character):
        ActivateControlRig(character, active=active)

    [setattr(control, property, visible) for control in GetControlsFromCharacter(character)
     for property in ("Show", "Visibility")]
    [setattr(bone, property, bool(visible - 1)) for bone in GetBonesFromCharacter(character)
     for property in ("Show", "Visibility") if "SKEL_ROOT" not in bone.Name]


# Old code that could possibly be removed
def ControlRigToSkeletonPlot(character):
    """
    Plots animation on the control rig to the skeleton

    Arguments:
        character (pyfbsdk.FBCharacter): character to plot
    """
    EnableRotationDOF(character, "SKEL_L_UpperArm")
    EnableRotationDOF(character, "SKEL_R_UpperArm")
    PlotCharacters(character, plotLocations=["skeleton"])
    DeleteControlRig(character)


def DeleteControlRig(character):
    """
    Delete Control Rig

    Arguments:
        character (pyfbsdk.FBCharacter): character with a control rig to delete
    """

    if HasControlRig(character):
        character.InputType = InputTypes.Rig
        character.GetCurrentControlSet().FBDelete()


def AddPoseToPoseControl(pose_name, pCharacter=None):
    """
    Adds the pose to the Pose Manager
    Arguments:
        pose_name (string): name for the pose
        pCharacter (pyfbsdk.FBCharacter): character whose pose we want to store

    Return:
        FBPose
    """
    if not pCharacter:
        pCharacter = GetCurrentCharacter()
    # Create Character Pose
    pose = mobu.FBCharacterPose(pose_name)
    pose.CopyPose(pCharacter)
    return pose


def ExportAllCharacterAnimations(path, AllTakes=True, Character=False, ControlSet=True, CharacterExtensions=False):
    """
    Export Characters Animations on takes as FBX files

    Arguments:
        path (string): directory to export tracks to
        AllTakes (boolean): export all takes in the scene
        Character (boolean): Save characters out to the FBX File
        ControlSet (boolean): Save control sets to the fbx file
        CharacterExtension (boolean): Save character extensions to the FBX file

    """
    application = mobu.FBApplication()

    options = mobu.FBFbxOptions()
    options.SaveCharacter = Character
    options.SaveControlSet = ControlSet
    options.SaveCharacterExtensions = CharacterExtensions

    # Get All takes + the current take
    takes = mobu.FBSystem().CurrentTake
    if AllTakes:
        takes = mobu.FBSystem().Scene.Takes

    filename = Scene.GetSceneName()

    if not path.endswith(filename):
        path = os.path.join(path, filename)

    if not os.path.exists(path):
        os.mkdir(path)

    for take in takes:
        takedirectory = os.path.join(path, take.Name)

        if not os.path.exists(takedirectory):
            os.mkdir(takedirectory)

        for character in mobu.FBSystem().Scene.Characters:
            application.SaveCharacterRigAndAnimation(os.path.join(takedirectory, "{}.fbx".format(character.Name)),
                                                     character, options)


# --- Anim2Fbx Utility Method ---


def PrepareMover(namespace, properties=("RotationActive", "TranslationActive", "Enable Translation DOF")):
    """
    Prepares Mover, this method can be deprecated\
    Arguments:
        namespace (string): namespace for mover
        properties [list(string, etc.)]: the name of attributes you want to set to False
    """
    lMover = mobu.FBFindModelByLabelName("{}:{}".format(namespace, "mover"))

    if lMover:
        [setattr(lMover.PropertyList.Find(each_property), "Data", False) for each_property in properties]


def GetControlRigXMLPath(character, sync=False):
    """
    Searches for, syncs and returns the controlrig.xml file related to the character passed as the
    arg.

    Args:
        FBCharacter

    Returns:
        String: file path to the character's controlrig.xml
    """
    path = ""
    rootDirectory = os.path.join(Config.Tool.Path.Root, "etc", "config", "characters", "skeleton")
    
    if sync: 
        Perforce.Sync(["-f", os.path.join(rootDirectory, "...")])

    controlRigXmlDictionary = {os.path.basename(folder): os.path.join(localpath, folder)
                               for localpath, folders, files in os.walk(rootDirectory) for folder in folders}

    directory = controlRigXmlDictionary.get(character.Name, "")
    if directory and os.path.exists(os.path.join(directory, "controlrig.xml")):
        path = os.path.join(directory, "controlrig.xml")
    return path


def RemoveAnimationLayersFromTakes(takes=[]):
    """
    Removes all the animation layers except the base layer from the given takes

    Arguments:
        takes (list[FBTake]): list of takes to clear animation layers from

    """
    for take in takes:
        # Start deleting layers starting with the highest index number to take into account that the index of a
        # layer dynamically changes
        for index in reversed(xrange(take.GetLayerCount())):
            if not index:
                continue
            layer = take.GetLayer(index)
            layer.FBDelete()


def SaveCharacterAnimationToFbx(path, character):
    """
    Saves out a character's animation to the given fbx file

    Arguments:
        path (string): fbx file to save animations to
        character (pyfbsdk.FBCharacter): character whose animation should be saved
    """

    options = mobu.FBFbxOptions(False)
    options.SaveCharacter = True
    options.SaveControlSet = False
    options.SaveCharacterExtensions = False

    Globals.Application.SaveCharacterRigAndAnimation(path, character, options)


def LoadCharacterAnimationFromFbx(path, character):
    """
    Loads out a character's animation from the given fbx file

    Arguments:
        path (string): fbx file to load animation from
        character (pyfbsdk.FBCharacter): character to apply animation to
    """
    options = mobu.FBFbxOptions(False)
    options.ReplaceControlSet = False
    options.CopyCharacterExtensions = False
    options.ProcessAnimationOnExtension = True
    options.IgnoreConflicts = True
    options.TransferMethod = mobu.FBCharacterLoadAnimationMethod.kFBCharacterLoadPlot
    options.ResetDOF = False
    options.ResetHierarchy = False
    options.RetargetOnBaseLayer = True
    options.RemoveConstraintReference = False

    plotOptions = mobu.FBPlotOptions()
    plotOptions.PlotAllTakes = False
    plotOptions.PlotOnFrame = True
    plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
    plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterGimbleKiller
    plotOptions.UseConstantKeyReducer = True
    plotOptions.ConstantKeyReducerKeepOneKey = True
    plotOptions.PlotTranslationOnRootOnly = True

    ActivateControlRig(character, True)
    Globals.Application.LoadAnimationOnCharacter(path, character, options, plotOptions)


def GetExportMetaFileFromFbx(fbxPath):
    """
    Gets the export metafile from the given fbx

    Arguments:
        fbxPath (string): path to the fbx to check for a meta file

    Return:
        string or None
    """
    filename, _ = os.path.splitext(os.path.basename(fbxPath).lower())
    filename = filename.rstrip("a")
    exportMetaFileDirectory = os.path.join(Config.Project.Path.Assets, "export", "characters", "metapeds",
                                           "component_models",
                                           filename[:2].lower())
    Perforce.Sync(os.path.join(exportMetaFileDirectory, "..."))

    if not os.path.exists(exportMetaFileDirectory):
        return

    for directory in os.listdir(exportMetaFileDirectory):
        if not directory.startswith(filename):
            continue
        for meshname in ("hand", "body", "uppr", "lowr", "head"):
            metaFilePath = os.path.join(exportMetaFileDirectory, directory, "{}.meta".format(directory))
            if meshname in directory and os.path.exists(metaFilePath):
                return metaFilePath


def GetSkeletonPathsFromMetaFile(metaFilePath):
    """
    Gets the bonelist path and paths to the skeleton files from the given meta file

    Arguments:
        metaFilePath (string): path to the metafile

    Returns:
        string, string, list[string, etc.]
    """
    tree = xml.parse(metaFilePath)
    root = tree.getroot()

    bonelistPath = root.find("MasterBoneList").text.replace(
        "$(assets)", Config.Project.Path.Assets) if root.find("MasterBoneList") is not None else None

    skeletonSetPath = "{}.skelset".format(root.find('SkeletonSet').text.replace(
        "$(assets)", Config.Project.Path.Assets)) if root.find("SkeletonSet") is not None else None
    skeletonPaths = []
    if skeletonSetPath:
        tree = xml.parse(skeletonSetPath)
        root = tree.getroot()

        mainSkeletonElement = root.find("MainSkeleton")
        skeletonsElement = root.find("Skeletons")
        skeletonPaths = [mainSkeletonElement.text.replace("$(assets)", Config.Project.Path.Assets)
                         ] if mainSkeletonElement is not None else skeletonPaths
        skeletonPaths += [element.text.replace("$(assets)", Config.Project.Path.Assets)
                          for element in skeletonsElement] if skeletonPaths else []
    return bonelistPath, skeletonSetPath, skeletonPaths


def ConvertMetapedToSkeleton(skeletonPaths, boneListPath):
    """
    Converts the metaped files into a skel file

    Arguments:
        skeletonPaths (list): list of skeletons paths
        boneListPath (string): path to the bonelist file

    Return:
        string or None

    """
    # Get the metaped file
    if not isinstance(skeletonPaths, (tuple, list)):
        skeletonPaths = [skeletonPaths]
    skeletonPath = skeletonPaths[0]
    executable = os.path.join(Config.Tool.Path.Bin, "anim", "skeletonmodularise.exe")
    temporaryDirectory = tempfile.mkdtemp()
    outputPath = os.path.join(temporaryDirectory, "skeleton.skel")

    commandList = [executable, "-inputFile", '"{}"'.format(skeletonPath), "-bonelist", '"{}"'.format(boneListPath),
                   "-outputdir", '"{}"'.format(temporaryDirectory)]

    if skeletonPaths[1:]:
        commandList += ["-additionalFiles", ",".join(['"{}"'.format(path) for path in skeletonPaths[1:]])]

    subprocess.call(commandList)

    fileExists = os.path.exists(outputPath)
    destinationPath = os.path.join(ProjectData.data.GetSkelFilesDirectory(), "skeleton.skel")
    if fileExists:
        shutil.move(outputPath, destinationPath)

    os.removedirs(temporaryDirectory)
    return destinationPath if fileExists else None


class Key(object):
    """
        Clone key value into a instance attribute.

        Args:
            curveKey (FBFCurveKey).

    """
    # Inspired from http://awforsythe.com/tutorials/pyfbsdk-7
    def __init__(self, curveKey=mobu.FBFCurveKey()):
        # Core values
        # In order to scale this value I use secondDouble type
        self.time = curveKey.Time.GetSecondDouble()
        self.value = curveKey.Value

        # Tangent values
        self.interpolation = curveKey.Interpolation
        self.tangentMode = curveKey.TangentMode
        self.constantMode = curveKey.TangentConstantMode
        self.leftDerivative = curveKey.LeftDerivative
        self.rightDerivative = curveKey.RightDerivative
        self.leftWeight = curveKey.LeftTangentWeight
        self.rightWeight = curveKey.RightTangentWeight
        self.tangentBreak = curveKey.TangentBreak
        self.tangentClampMode = curveKey.TangentClampMode

        # TCB values
        self.tension = curveKey.Tension
        self.continuity =  curveKey.Continuity
        self.bias = curveKey.Bias


class AnimationCurve(object):
    """
    Store keys included in a timeRange for an animationNode from motion builder.
    """
    def __init__(self):
        self.model = ''
        self.attribute  = ''
        self.animationNode  = ''
        self.curveNode  = ''
        self.timeRange = [0,1]
        self.keyInTimeRange = []
        self.keys = []


class AnimationBoundingBox(object):
    """
    Store global bounding box data as simple struct/enum.
    """
    def __init__(self):
        self.boundingBox = {}
        self.curveTriangleStrips = []
        self.keyPositions = []