"""
Main Logic for setting up, and dealing with IG to CS in and out keyframing and setting up
"""

import pyfbsdk as mobu

from RS.Utils import Scene, Namespace
from RS.Utils.Scene import RelationshipConstraintManager as rcm
from RS.Utils.Scene import AnimationNode, Constraint, Character, Layer
from RS.Core.Animation import Lib as animLib
from RS import Core


class MissingPropertyError(Exception):
    """
    Raises if a property is missing from a node
    """
    pass


class CharacterNotSetupError(Exception):
    """
    Rasies if a character is not setup and is trying to be used to either Key, Commit or Revert
    """
    pass


class CharacterNotCommitableError(Exception):
    """
    Errors if the character is trying to be commited when not in a commitable state such as if
    it is being driven by a Control Rig or another constraint or character/actor
    """
    pass


class CharacterNotReverableError(Exception):
    """
    Errors if the character is trying to be reverted when not in a revertable state, such is
    indicated when the "applyBump" property is turned on
    """
    pass


class MissingCharacterNode(Exception):
    """
    Raises if a character is missing a node such as Dummy or a Mover
    """
    pass


class IgToCs(object):
    """
    This class holds all the method for setting up, keying, commiting, reverting and checking the
    CS to IG animation system
    """

    _REFERENCE_CHARACTER_SCALE_PROPERTY_NAME = "_reference_ApplyCharacterScale"

    def __init__(self, character):
        """"
        Constructor

        Args:
            character (mobu.FBCharacter): Character to apply the IG to CS on
        """
        super(IgToCs, self).__init__()
        self._character = character

    @property
    def character(self):
        """
        Property

        Get the character for the IG/CS

        return:
            FBCharacter that is being applied the CS to IG on
        """
        return self._character

    def isSetup(self):
        """
        Check if the Setup has been applied to or not

        return:
            bool if the setup has been setup
        """
        return self._isSetup(self._character)

    def setup(self):
        """
        Sets up IG to CS system
        """
        if self.isSetup():
            return
        self._setupChracter(self._character)

    def canBeSetup(self):
        """
        Check if the character can be setup, checks for a mover and dummy node present in the character
        namespace

        return:
            bool if the character can be setup
        """
        return all([self._getDummy(self._character), self._getMover(self._character)])

    def canBeUnSetup(self):
        """
        Checks if the character's setup can be remove, checks for the constraint and any present 
        animation data
        """
        return self._canBeUnSetup(self._character)
    
    def removeSetup(self, finalPlot=True):
        """
        Removes the character's setup, reguardless of if animation is found or not
        """
        return self._removeSetup(self._character, finalPlot=finalPlot)

    def hasScaleUdps(self):
        """
        Check if the character has the UDPs needed to setup the system

        return:
            bool if the character has the scale UDP's
        """
        propDict = Character.ReadMaxProperties(self._getDummy(self._character))
        if propDict is None:
            return None

        for name in ["SCALEBUMPVAL", "IGTOCSSCALEVALUE"]:
            if not propDict.has_key(name):
                return False
        return True

    def canCommit(self):
        """
        Checks if the system is in a state where it can commit the motion

        return:
            bool if the motion can be committed
        """
        return self._canCommit(self._character)

    def canRevert(self):
        """
        Checks if the system is in a state where it can revert the motion

        return:
            bool if the motion can be reverted
        """
        return self._canRevert(self._character)

    def canKey(self):
        """
        Checks if the system is in a state where it can key the motion

        return:
            bool if the motion can be keyed
        """
        return self._canKey(self._character)

    def focusOnProperty(self, focus=True):
        """
        Focuses or DeFocuses on the Scale Animation property to show users the keys and allow for
        easier manipulation of it.

        kwargs:
            focus (bool): If the focus is given or taken
        """
        self._focusOnProperty(self._character, focus=focus)

    def lockProperty(self, lock=True):
        """
        Locks or Unlocks the Scale Animation property.

        kwargs:
            lock (bool): if the property is to be locked or unlocked
        """
        self._lockProperty(self._character, lock=lock)

    def setToIg(self):
        """
        Set the Character to the IG Position
        """
        self._setToIg(self._character)

    def setToCs(self):
        """
        Set the Character to the CS Position
        """
        self._setToCs(self._character)

    def getRefScaleValues(self):
        """
        Get the previous reference scale values
        """
        return self._getRefScaleValues(self._character)

    def getScaleValues(self, commitedOnly=True):
        """
        Get the previous reference scale values
        """
        return self._getScaleValues(self._character, commitedOnly=commitedOnly)

    def commitMotion(self):
        """
        Commit the motion of the IG/CS and bake it down so the system can
        recover and revert it later
        """
        self._commitMotion(self._character)

    def getKeysForCharacterScale(self):
        """
        Get a dict of scale nodes, with a dict for each node with its keys and values
        """
        return self._getKeysForCharacterScaleNode(self._character)

    def setKeysForCharacterScale(self, inputDict):
        """
        Set the keys on scale properties given an input dict with their names, and their key, values

        args:
            inputDict (dict): The dict with values to set for properties
        """
        self._setKeysForCharacterScaleNode(self._character, inputDict)

    def revertMotion(self):
        """
        Revert the commited motion of the IG/CS and bake it, as well as remove
        the scale transform animation as if the motion was never commited
        """
        self._revertMotion(self._character)

    @classmethod
    def _getKeysForCharacterScaleNode(cls, character):
        """
        Internal Method

        Get a dict of scale nodes, with a dict for each node with its keys and values

        args:
            character (FBCharacter): The character to get or create the scale node for
        """
        returnDict = {}
        scaleNode = cls._getOrCreateCharacterScaleNode(character)
        
        currentLayer = Layer.GetActiveLayer()
        currentTake = Core.System.CurrentTake
        Layer.SetLayerActive(currentTake, Layer.GetBaseAnimationLayer())

        for prop in [cls._getOrCreateCharacterScaleProperty(scaleNode), 
                     cls._getOrCreateCharacterRefScaleProperty(scaleNode)]:
            keys = prop.GetAnimationNode().FCurve.Keys
            returnDict[prop.Name] = {key.Time.Get() : key.Value for key in keys}

        Layer.SetLayerActive(currentTake, currentLayer)
        return returnDict

    @classmethod
    def _setKeysForCharacterScaleNode(cls, character, inputDict):
        """
        Internal Method

        Set the keys on scale properties given an input dict with their names, and their key, values
        
        args:
            character (FBCharacter): The character to get or create the scale node for
            inputDict (dict): The dict with values to set for properties
        """
        scaleNode = cls._getOrCreateCharacterScaleNode(character)
        
        currentLayer = Layer.GetActiveLayer()
        currentTake = Core.System.CurrentTake
        Layer.SetLayerActive(currentTake, Layer.GetBaseAnimationLayer())

        for prop in [cls._getOrCreateCharacterScaleProperty(scaleNode), 
                     cls._getOrCreateCharacterRefScaleProperty(scaleNode)]:
            data = inputDict.get(prop.Name)
            if data is None:
                continue
            
            curve = prop.GetAnimationNode().FCurve
            curve.EditClear()
            curve.EditBegin()

            for time, value in data.iteritems():
                curve.KeyAdd(mobu.FBTime(time), value)
            curve.EditEnd()

        Layer.SetLayerActive(currentTake, currentLayer)

    @classmethod
    def _getOrCreateCharacterScaleNode(cls, character):
        """
        Internal Method

        Get or Create the Character Scale Node for the given character

        args:
            character (FBCharacter): The character to get or create the scale node for
        """
        # See if we can grab it
        charScaleNodeName = "CharacterScale"
        if character is not None and Namespace.GetNamespace(character) is not None:
            charScaleNodeName = "{0}:{1}".format(Namespace.GetNamespace(character), charScaleNodeName)
        charScaleNode = Scene.FindModelByName(charScaleNodeName, True)
        if charScaleNode is None:
            # Guess we'll have to make it!
            dummyNode = cls._getDummy(character)
            if dummyNode is None:
                raise MissingCharacterNode("Cannot Create Character Scale Node, no Dummy Found!")

            charScaleNode = mobu.FBModelNull("Null")
            charScaleNode.LongName = charScaleNodeName
            charScaleNode.Parent = dummyNode

        cls._getOrCreateCharacterScaleProperty(charScaleNode)
        return charScaleNode

    @classmethod
    def _isSetup(cls, character):
        """
        Class Method

        Internal Method

        Check if the Setup has been applied to or not to the given character

        args:
            character (FBCharacter): The character to check if setup

        return:
            bool if the setup has been setup
        """
        for check in [cls._getSkelRoot(character),
                      cls._getOrCreateCharacterApplyBumpProperty(character, allowCreate=False),
                       cls._getOrCreateCharacterReverseBumpProperty(character, allowCreate=False)
                        ]:
            if check is None:
                return False
        return True

    @classmethod
    def _canCommit(cls, character):
        """
        Class Method

        Internal Method

        Checks if the system is in a state where it can commit the motion to the given character

        args:
            character (FBCharacter): The character to check

        return:
            bool if the motion can be committed
        """
        applyBumpProperty = cls._getOrCreateCharacterApplyBumpProperty(character, allowCreate=False)

        if applyBumpProperty is None:
            return False

        return applyBumpProperty.Data

    @classmethod
    def _canRevert(cls, character):
        """
        Class Method

        Internal Method

        Checks if the system is in a state where it can revert the motion to the given character

        args:
            character (FBCharacter): The character to check

        return:
            bool if the motion can be reverted
        """
        applyBumpProperty = cls._getOrCreateCharacterApplyBumpProperty(character, allowCreate=False)

        if applyBumpProperty is None:
            return False

        return not applyBumpProperty.Data

    @classmethod
    def _canKey(cls, character):
        """
        Class Method

        Internal Method

        Checks if the system is in a state where it can key the motion to the given character

        args:
            character (FBCharacter): The character to check

        return:
            bool if the motion can be key
        """
        if not cls._canCommit(character):
            return False

        return character.ActiveInput

    @classmethod
    def _canBeUnSetup(cls, character):
        """
        Class Method

        Internal Method

        Checks if the setup can be removed for the given character

        args:
            character (FBCharacter): The character
            
        return:
            bool if the setup can be removed
        """
        return AnimationNode.HasKeys(cls._getOrCreateCharacterScaleNode(character).AnimationNode)
        
    @classmethod
    def _removeSetup(cls, character, finalPlot=True):
        """
        Class Method

        Internal Method

        Remove the setup from the given character

        args:
            character (FBCharacter): The character
        """
        constraint = cls._getOrCreateConstraint(character, allowCreate=False)
        
        # Delete the constraint
        if constraint is not None:
            constraint.GetConstraint().FBDelete()
            
        # Delete the ScaleNode
        scaleNode = cls._getOrCreateCharacterScaleNode(character)
        if scaleNode is not None:
            scaleNode.FBDelete()
        
        #Remove the properties
        applyBumpProperty = cls._getOrCreateCharacterApplyBumpProperty(character, allowCreate=False)
        if applyBumpProperty is not None:
            owner = applyBumpProperty.GetOwner()
            owner.PropertyRemove(applyBumpProperty)
            
        reverseBumpProperty = cls._getOrCreateCharacterReverseBumpProperty(character, allowCreate=False)
        if reverseBumpProperty is not None:
            owner = reverseBumpProperty.GetOwner()
            owner.PropertyRemove(reverseBumpProperty)
        
        if finalPlot is False:
            return

        # plot to Control Rig then skeleton
        animLib.Plot(character, "rig", CreateControlRig=False, AllTakes=True)
        mobu.FBSystem().Scene.Evaluate()
        animLib.Plot(character, "skeleton", CreateControlRig=False, AllTakes=True)

    @classmethod
    def _focusOnProperty(cls, character, focus=True):
        """
        Class Method

        Internal Method

        Focuses or DeFocuses on the Scale Animation property to show users the keys and allow for
        easier manipulation of it on the given character

        args:
            character (FBCharacter): The character

        kwargs:
            focus (bool): If the focus is given or taken
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            return
        scaleProp = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        if scaleProp is None:
            return

        currentTake = Core.System.CurrentTake
        Layer.SetLayerActive(currentTake, Layer.GetBaseAnimationLayer())
        scaleProp.SetFocus(focus)

    @classmethod
    def _lockProperty(cls, character, lock=True):
        """
        Class Method

        Internal Method

        Locks or Unlocks the Scale Animation property on the given character

        args:
            character (FBCharacter): The character

        kwargs:
            lock (bool): if the property is to be locked or unlocked
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            return
        scaleProp = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        if scaleProp is None:
            return

        scaleProp.SetLocked(lock)

    @classmethod
    def _setupProperties(cls, character):
        """
        Class Method

        Internal Method

        Setup the properties needed on the given Character using the User Defined Properties that
        came across from 3ds Max

        args:
            character (FBCharacter): The character
        """
        propDict = Character.GetMaxProperties(character).get(cls._getDummy(character))
        if propDict is None:
            raise MissingPropertyError("Unable to find property dict on Character")
        moverNode = cls._getMover(character)
        for propName in ["SCALEBUMPVAL", "IGTOCSSCALEVALUE"]:
            propValue = propDict.get(propName)
            if propValue is None:
                raise MissingPropertyError("Missing Property '{0}' on character dict".format(propName))

            propValue = float(propValue)
            if propName == "SCALEBUMPVAL":
                propValue *= 100

            prop = moverNode.PropertyList.Find(propName)
            if prop is None:
                prop = moverNode.PropertyCreate(
                                             propName,
                                             mobu.FBPropertyType.kFBPT_double,
                                            'Number',
                                             True,
                                             True,
                                             None
                                             )
            prop.SetMax(propValue)
            prop.SetMin(propValue)
            prop.Data = propValue
            prop.SetAnimated(True)

    @classmethod
    def _setupChracter(cls, character=None):
        """
        Class Method

        Internal Method

        Sets up IG to CS system, from the Scale Nodes, the properties and the constraints

        args:
            character (FBCharacter): The character
        """
        if character is not None:
            # Add a control rig if there isnt one
            if animLib.HasControlRig(character) is False:
                character.CreateControlRig(1)
        
        cls._getOrCreateCharacterScaleNode(character)
        cls._setupProperties(character)
        cls._getOrCreateConstraint(character)

    @classmethod
    def _setToIg(cls, character):
        """
        Class Method

        Internal Method

        Set the given Character to the IG Position

        args:
            character (FBCharacter): The character
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        scaleProp = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        cls._keyOnBaseLayer(scaleProp, 0)

    @classmethod
    def _keyOnBaseLayer(cls, prop, value):
        """
        Internal Method

        Key the property with the given value, only on the base animation layer

        args:
            prop (FBProperty): The property to key
            value (object): The value to key the property with
        """
        currentLayer = Layer.GetActiveLayer()
        currentTake = Core.System.CurrentTake
        Layer.SetLayerActive(currentTake, Layer.GetBaseAnimationLayer())
        prop.Data = value
        prop.Key()
        Layer.SetLayerActive(currentTake, currentLayer)

    @classmethod
    def _setToCs(cls, character):
        """
        Class Method

        Internal Method

        Set the given Character to the CS Position

        args:
            character (FBCharacter): The character
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        scaleProp = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        cls._keyOnBaseLayer(scaleProp, 1)

    @classmethod
    def _commitMotion(cls, character):
        """
        Class Method

        Internal Method

        Commit the motion of the IG/CS on the given character and bake it down so the system can
        recover and revert it later

        args:
            character (FBCharacter): The character
        """
        if not cls._canCommit(character):
            raise CharacterNotCommitableError("Character not in commitable state!")
        # Set the Reverse Bump Property set to OFF
        removeBumpProperty = cls._getOrCreateCharacterReverseBumpProperty(character)
        applyBumpProperty = cls._getOrCreateCharacterApplyBumpProperty(character)

        removeBumpProperty.Data = False
        applyBumpProperty.Data = True
        mobu.FBSystem().Scene.Evaluate()

        # plot to Control Rig then skeleton
        animLib.Plot(character, "rig", CreateControlRig=False, AllTakes=True)
        # Turn off the Apply Bump property
        applyBumpProperty.Data = False
        mobu.FBSystem().Scene.Evaluate()

        animLib.Plot(character, "skeleton", CreateControlRig=False, AllTakes=True)
        cls._focusOnProperty(character, False)
        cls._lockProperty(character, True)

    @classmethod
    def _revertMotion(cls, character):
        """
        Class Method

        Internal Method

        Revert the commited motion of the IG/CS on the given character and bake it, as well as remove
        the scale transform animation as if the motion was never commited

        args:
            character (FBCharacter): The character
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        if not cls._canRevert(character):
            raise CharacterNotReverableError("Character not in revertable state!")
        root = cls._getSkelRoot(character)

        # Set the RemoveBump Property to 1 - ON
        reverseBumpProperty = cls._getOrCreateCharacterReverseBumpProperty(character)
        applyBumpProperty = cls._getOrCreateCharacterApplyBumpProperty(character)
        reverseBumpProperty.Data = True
        applyBumpProperty.Data = True

        # Plot Translation
        mobu.FBSystem().Scene.Evaluate()
        animLib.Plot(character, "rig", CreateControlRig=False)

        reverseBumpProperty.Data = False
        applyBumpProperty.Data = False

        mobu.FBSystem().Scene.Evaluate()
        animLib.Plot(character, "skeleton", CreateControlRig=False)

        # Set the RemoveBump Property to 0 - OFF
        mobu.FBSystem().Scene.Evaluate()
        applyBumpProperty.Data = True
        mobu.FBSystem().Scene.Evaluate()

        cls._lockProperty(character, False)

        # Clear the keys on the value on the Base Animaiton Layer
        currentLayer = Layer.GetActiveLayer()
        currentTake = Core.System.CurrentTake
        Layer.SetLayerActive(currentTake, Layer.GetBaseAnimationLayer())
        cls._copyScaleKeysToRefScale(character)
        animLib.ClearAnimationFromProperty(cls._getOrCreateCharacterScaleProperty(charScaleNode))
        Layer.SetLayerActive(currentTake, currentLayer)

        # plot to Control Rig then skeleton
        animLib.Plot(character, "rig", CreateControlRig=False, AllTakes=True)
        mobu.FBSystem().Scene.Evaluate()
        animLib.Plot(character, "skeleton", CreateControlRig=False, AllTakes=True)

    @classmethod
    def _getScaleValues(cls, character, commitedOnly=True):
        """
        Class Method
        
        Internal Method
        
        Get a list of frame, bool values for the scales
        
        args:
            character (FBCharacter): The character
            
        return:
            list of tuples of int, bool
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        
        scaleProperty = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        if commitedOnly is True and not scaleProperty.IsLocked():
            return [] 
        scaleAnim = scaleProperty.GetAnimationNode()
        return [(key.Time.GetFrame(), key.Value) for key in scaleAnim.FCurve.Keys]
        

    @classmethod
    def _getRefScaleValues(cls, character):
        """
        Class Method
        
        Internal Method
        
        Get a list of frame, bool values for the ref scales
        
        args:
            character (FBCharacter): The character
            
        return:
            list of tuples of int, bool
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        
        refScaleProperty = cls._getOrCreateCharacterRefScaleProperty(charScaleNode)
        refScaleAnim = refScaleProperty.GetAnimationNode()
        return [(key.Time.GetFrame(), key.Value) for key in refScaleAnim.FCurve.Keys]
        
    @classmethod
    def _copyScaleKeysToRefScale(cls, character):
        """
        Class Method
        
        Internal Method
        
        Copy the keys from the character Scale property to the Reference Scale property
        
        args:
            character (FBCharacter): The character
        """
        charScaleNode = cls._getOrCreateCharacterScaleNode(character)
        if charScaleNode is None:
            raise CharacterNotSetupError("Character Not Setup!")
        
        scaleProperty = cls._getOrCreateCharacterScaleProperty(charScaleNode)
        refScaleProperty = cls._getOrCreateCharacterRefScaleProperty(charScaleNode)
        
        scaleAnim = scaleProperty.GetAnimationNode()
        refScaleAnim = refScaleProperty.GetAnimationNode()
        
        refScaleProperty.SetLocked(False)
        
        refScaleAnim.FCurve.EditClear()
        
        for key in scaleAnim.FCurve.Keys:
            refScaleAnim.FCurve.KeyAdd(key.Time, key.Value)
        
        refScaleProperty.SetLocked(True)
        
    @classmethod
    def _getOrCreateConstraint(cls, character, allowCreate=True):
        """
        Class Method

        Internal Method

        Setup the IG/CS System constraint on the given character and activate it

        args:
            character (FBCharacter): The character
        """
        # Add Parent Child Constaint to keep offset to mover
        charNamespace = None
        if character is not None:
            charNamespace = Namespace.GetNamespace(character)
        constraintName = "MoverSetup"
        constraint = rcm.GetRelationShipConstraintByName(constraintName, namespace=charNamespace)
        if constraint is not None or allowCreate is False:
            return constraint
        
        if charNamespace is not None:
            constraintName = "{0}:{1}".format(charNamespace, constraintName)

        # Create the main relationship Constraint
        scaleNode = cls._getOrCreateCharacterScaleNode(character)
        applyScalePropertyName = cls._getOrCreateCharacterScaleProperty(scaleNode).Name
        removeBumpPropertyName = cls._getOrCreateCharacterReverseBumpProperty(character).Name
        applyBumpPropertyName = cls._getOrCreateCharacterApplyBumpProperty(character).Name
        bumpScalePropertyName = cls._getBumpValueProperty(character).Name
        scaleValuePropertyName = cls._getScaleValueProperty(character).Name

        newConstraint = rcm.RelationShipConstraintManager(name=constraintName)

        # Create Boxes - In's
        skelRootInBox = newConstraint.AddSenderBox(cls._getSkelRoot(character))
        scaleControlInBox = newConstraint.AddSenderBox(scaleNode)
        moverInBox = newConstraint.AddSenderBox(cls._getMover(character))

        # Create Boxes - Functions's
        scaleControlNotBox = newConstraint.AddFunctionBox("Boolean", "NOT")
        ifScaleBox = newConstraint.AddFunctionBox("Number", "IF Cond Then A Else B")
        subScaleBox = newConstraint.AddFunctionBox("Number", "Subtract (a - b)")
        toVectorScaleBox = newConstraint.AddFunctionBox("Converters", "Number to Vector")
        ifBumpBox = newConstraint.AddFunctionBox("Number", "IF Cond Then A Else B")
        toVectorBumpBox = newConstraint.AddFunctionBox("Converters", "Number to Vector")
        addBumpBox = newConstraint.AddFunctionBox("Vector", "Add (V1 + V2)")
        subBumpBox = newConstraint.AddFunctionBox("Vector", "Subtract (V1 - V2)")

        bumpLogicSwitchBox = newConstraint.AddFunctionBox("Vector", "IF Cond Then A Else B")
        applyBumpSwitchBox = newConstraint.AddFunctionBox("Vector", "IF Cond Then A Else B")

        # Create Boxes - Outs's
        skelRootOutBox = newConstraint.AddReceiverBox(cls._getSkelRoot(character))
        moverOutBox = newConstraint.AddReceiverBox(cls._getMover(character))

        # Configure Boxes
        ifScaleBox.SetReceiverValueByName("b", 1.0)
        subScaleBox.SetReceiverValueByName("a", 2)
        moverOutBox.SetGlobalTransforms(False)

        # Lay out the boxes nicely so Kat wont hit me!
        yIter = 150
        xIter = 300
        moverInBox.SetBoxPosition(xIter * 1, yIter * 1)
        scaleControlInBox.SetBoxPosition(xIter * 0, yIter * 4)
        skelRootInBox.SetBoxPosition(xIter * 1, yIter * 6)

        scaleControlNotBox.SetBoxPosition(xIter * 1, yIter * 4)
        ifScaleBox.SetBoxPosition(xIter * 2, yIter * 1)
        subScaleBox.SetBoxPosition(xIter * 2, yIter * 3)
        toVectorScaleBox.SetBoxPosition(xIter * 3, yIter * 1)
        ifBumpBox.SetBoxPosition(xIter * 2, yIter * 4)
        toVectorBumpBox.SetBoxPosition(xIter * 3, yIter * 4)
        addBumpBox.SetBoxPosition(xIter * 3, yIter * 6)
        subBumpBox.SetBoxPosition(xIter * 3, yIter * 5)
        bumpLogicSwitchBox.SetBoxPosition(xIter * 4, yIter * 6)
        applyBumpSwitchBox.SetBoxPosition(xIter * 5, yIter * 7)

        moverOutBox.SetBoxPosition(xIter * 4, yIter * 1)
        skelRootOutBox.SetBoxPosition(xIter * 6, yIter * 6)

        # Connect Boxes
        newConstraint.ConnectBoxes(scaleControlInBox, applyScalePropertyName, scaleControlNotBox, "a")
        newConstraint.ConnectBoxes(scaleControlNotBox, "Result", ifScaleBox, "Cond")
        newConstraint.ConnectBoxes(moverInBox, scaleValuePropertyName, subScaleBox, "b")
        newConstraint.ConnectBoxes(moverInBox, bumpScalePropertyName, ifBumpBox, "a")

        newConstraint.ConnectBoxes(subScaleBox, "Result", ifScaleBox, "a")
        newConstraint.ConnectBoxes(ifScaleBox, "Result", toVectorScaleBox, "X")
        newConstraint.ConnectBoxes(ifScaleBox, "Result", toVectorScaleBox, "Y")
        newConstraint.ConnectBoxes(ifScaleBox, "Result", toVectorScaleBox, "Z")
        newConstraint.ConnectBoxes(toVectorScaleBox, "Result", moverOutBox, "Lcl Scaling")

        newConstraint.ConnectBoxes(scaleControlNotBox, "Result", ifBumpBox, "Cond")
        newConstraint.ConnectBoxes(ifBumpBox, "Result", toVectorBumpBox, "Y")
        newConstraint.ConnectBoxes(skelRootInBox, "Translation", addBumpBox, "V1")
        newConstraint.ConnectBoxes(skelRootInBox, "Translation", subBumpBox, "V1")
        newConstraint.ConnectBoxes(toVectorBumpBox, "Result", addBumpBox, "V2")
        newConstraint.ConnectBoxes(toVectorBumpBox, "Result", subBumpBox, "V2")
        newConstraint.ConnectBoxes(skelRootInBox, removeBumpPropertyName, bumpLogicSwitchBox, "Cond")
        newConstraint.ConnectBoxes(addBumpBox, "Result", bumpLogicSwitchBox, "b")
        newConstraint.ConnectBoxes(subBumpBox, "Result", bumpLogicSwitchBox, "a")

        newConstraint.ConnectBoxes(bumpLogicSwitchBox, "Result", applyBumpSwitchBox, "a")
        newConstraint.ConnectBoxes(skelRootInBox, "Translation", applyBumpSwitchBox, "b")
        newConstraint.ConnectBoxes(skelRootInBox, applyBumpPropertyName, applyBumpSwitchBox, "Cond")
        newConstraint.ConnectBoxes(applyBumpSwitchBox, "Result", skelRootOutBox, "Translation")

        # Turn it on!
        newConstraint.SetActive(True)
        return newConstraint

    @classmethod
    def _getDummy(cls, character=None):
        """
        Class Method

        Internal Method

        Get the Dummy for the given character

        kargs:
            character (FBCharacter): The character

        return:
            FBObject of the dummy object for the character
        """
        charNodeName = "Dummy01"
        if character is not None  and Namespace.GetNamespace(character) is not None:
            charNodeName = "{0}:{1}".format(Namespace.GetNamespace(character), charNodeName)
        return Scene.FindModelByName(charNodeName, True)

    @classmethod
    def _getSkelRoot(cls, character=None):
        """
        Class Method

        Internal Method

        Get the SkelRoot for the given character

        kargs:
            character (FBCharacter): The character

        return:
            FBObject of the SkelRoot object for the character
        """
        charNodeName = "SKEL_ROOT"
        if character is not None and Namespace.GetNamespace(character) is not None:
            charNodeName = "{0}:{1}".format(Namespace.GetNamespace(character), charNodeName)
        return Scene.FindModelByName(charNodeName, True)

    @classmethod
    def _getScaleValueProperty(cls, character=None):
        """
        Class Method

        Internal Method

        Get the ScaleValue Property for the given character

        kargs:
            character (FBCharacter): The character

        return:
            FBProperty of the Scale Value for the given character
        """
        propName = "IGTOCSSCALEVALUE"
        prop = cls._getMover(character).PropertyList.Find(propName)
        if prop is None:
            raise MissingPropertyError("Missing property '{0}' on the Mover".format(propName))
        return prop

    @classmethod
    def _getBumpValueProperty(cls, character=None):
        """
        Class Method

        Internal Method

        Get the BumpValue Property for the given character

        kargs:
            character (FBCharacter): The character

        return:
            FBProperty of the Bump Value for the given character
        """
        propName = "SCALEBUMPVAL"
        prop = cls._getMover(character).PropertyList.Find(propName)
        if prop is None:
            raise MissingPropertyError("Missing property '{0}' on the Mover".format(propName))
        return prop

    @classmethod
    def _getMover(cls, character=None):
        """
        Class Method

        Internal Method

        Get the Mover for the given character

        kargs:
            character (FBCharacter): The character

        return:
            FBObject of the Mover object for the character
        """
        charNodeName = "mover"
        if character is not None and Namespace.GetNamespace(character) is not None:
            charNodeName = "{0}:{1}".format(Namespace.GetNamespace(character), charNodeName)
        return Scene.FindModelByName(charNodeName, True)

    @classmethod
    def _getOrCreateCharacterScaleProperty(cls, charScaleNode, allowCreate=True):
        """
        Class Method

        Internal Method

        Get or create the character scale property on the given character scale node.

        args:
            charScaleNode (FBObject): The character scale null

        kwargs:
            allowCreate (bool): Allow creating the property if not present

        return:
            FBProperty of the scale property
        """
        characterScalePropName = "ApplyCharacterScale"
        scaleProp = charScaleNode.PropertyList.Find(characterScalePropName)
        if scaleProp is not None or allowCreate is False:
            return scaleProp

        prop = charScaleNode.PropertyCreate(
                                     characterScalePropName,
                                     mobu.FBPropertyType.kFBPT_bool,
                                     'Bool',
                                     True,
                                     True,
                                     None
                                     )
        prop.SetAnimated(True)
        prop.Data = True
        return prop

    @classmethod
    def _getOrCreateCharacterRefScaleProperty(cls, charScaleNode, allowCreate=True):
        """
        Class Method

        Internal Method

        Get or create the reference character scale property on the given character scale node.

        args:
            charScaleNode (FBObject): The character scale null

        kwargs:
            allowCreate (bool): Allow creating the property if not present

        return:
            FBProperty of the ref scale property
        """
        refScaleProp = charScaleNode.PropertyList.Find(cls._REFERENCE_CHARACTER_SCALE_PROPERTY_NAME)
        if refScaleProp is not None or allowCreate is False:
            return refScaleProp

        prop = charScaleNode.PropertyCreate(
                                     cls._REFERENCE_CHARACTER_SCALE_PROPERTY_NAME,
                                     mobu.FBPropertyType.kFBPT_bool,
                                     'Bool',
                                     True,
                                     True,
                                     None
                                     )
        prop.SetAnimated(True)
        prop.Data = True
        return prop

    @classmethod
    def _getOrCreateCharacterReverseBumpProperty(cls, character, allowCreate=True):
        """
        Class Method

        Internal Method

        Get or create the character reverse bump property on the given character.

        args:
            character (FBCharacter): The character

        kwargs:
            allowCreate (bool): Allow creating the property if not present

        return:
            FBProperty of the reverse bump property
        """
        root = cls._getSkelRoot(character)
        if root is None:
            return None
        propName = "ReverseBump"
        prop = root.PropertyList.Find(propName)
        if prop is not None or allowCreate is False:
            return prop

        prop = root.PropertyCreate(
                                     propName,
                                     mobu.FBPropertyType.kFBPT_bool,
                                     'Bool',
                                     True,
                                     True,
                                     None
                                     )
        prop.SetAnimated(True)
        return prop

    @classmethod
    def _getOrCreateCharacterApplyBumpProperty(cls, character, allowCreate=True):
        """
        Class Method

        Internal Method

        Get or create the character apply bump property on the given character.

        args:
            character (FBCharacter): The character

        kwargs:
            allowCreate (bool): Allow creating the property if not present

        return:
            FBProperty of the apply bump property
        """
        root = cls._getSkelRoot(character)
        if root is None:
            return None
        propName = "ApplyBump"
        prop = root.PropertyList.Find(propName)
        if prop is not None or allowCreate is False:
            return prop

        prop = root.PropertyCreate(
                                     propName,
                                     mobu.FBPropertyType.kFBPT_bool,
                                     'Bool',
                                     True,
                                     True,
                                     None
                                     )
        prop.SetAnimated(True)
        prop.Data = True
        return prop

