import os
import re
import copy

from RS import Config, Perforce
from RS.Core.Export import Export

import pyfbsdk as mobu
from PySide import QtGui


class ExportPathTypes(object):
    AnimClip = 0

    @classmethod
    def GetExtentionsForType(cls, pathType):
        if pathType == cls.AnimClip:
            return [".anim", ".clip"]
        return []


class AudioExportPathGenerator(object):
    _pathsToReplace = [
                       ("/audio/dev/ASSETS/WAVES/", "/art/anim/export_for_audio/"),
                       ("\\\\audio\\\\DEV\\\\ASSETS\\\\Waves\\\\", "\\\\art\\\\anim\\\\export_for_audio\\\\"),
                    ]

    def __init__(self):
        super(AudioExportPathGenerator, self).__init__()
        self._cachedFileList = []
        self.fillP4Cahce()

    def findAudio(self, fileName, takeName):
        fileParts = fileName.split(os.path.sep)

        possibleFiles = [filePath for filePath in self._cachedFileList if os.path.splitext(os.path.basename(filePath))[0] == takeName]
        if len(possibleFiles) == 0:
            return None
        if len(possibleFiles) == 1:
            return possibleFiles[0]

        # Simple Checker #1 - Whole Name
        mainName = fileParts[-3]
        subFiles = []
        for filePath in possibleFiles:
            if mainName.lower() in filePath.lower():
                subFiles.append(filePath)

        if len(subFiles) == 1:
            return subFiles[0]
        elif len(subFiles) > 1:
            subFiles = [filePath for filePath in subFiles if not "placeholder" in filePath.lower()]
            if len(subFiles) == 1:
                return subFiles[0]

        # Simple Checker #2 - Char Name (1st Pass)
        mainName = fileParts[-3]
        charName = mainName.split("_")[0]
        subFiles = []
        for filePath in possibleFiles:
            if charName.lower() in filePath.lower():
                subFiles.append(filePath)
        if len(subFiles) == 1:
            return subFiles[0]
        elif len(subFiles) > 1:
            subFiles = [filePath for filePath in subFiles if not "placeholder" in filePath.lower()]
            if len(subFiles) == 1:
                return subFiles[0]

    def resolvePath(self, fileName, takeName, pathType, populateExport=True):
        depoAudioPath = self.findAudio(fileName, takeName)
        if depoAudioPath is None:
            return ["Unable to find Audio Path"]

        localAudioPath = Perforce.GetFileState(depoAudioPath).get_ClientFilename()

        allPaths = [(depoAudioPath, localAudioPath)]
        depoAudioPathClone = copy.copy(depoAudioPath)
        localAudioPathClone = copy.copy(localAudioPath)
        for subPath in self._pathsToReplace:
            oldPath, newPath = subPath
            depoAudioPathClone = re.sub(oldPath, newPath, depoAudioPathClone, flags=re.IGNORECASE)
            localAudioPathClone = re.sub(oldPath, newPath, localAudioPathClone, flags=re.IGNORECASE)

        for ext in ExportPathTypes.GetExtentionsForType(pathType):
            depoAudioPathParts = os.path.splitext(depoAudioPathClone)
            localAudioPathParts = os.path.splitext(localAudioPathClone)
            allPaths.append(
                            (
                              "{0}{1}".format(depoAudioPathParts[0], ext),
                              "{0}{1}".format(localAudioPathParts[0], ext)
                            )
                        )

        if populateExport:
            self._populateExport(takeName, allPaths)

        return allPaths

    def _populateExport(self, takeName, allPaths):
        localAnimPath = allPaths[1][1]
        dirName = os.path.dirname(localAnimPath)
        if not os.path.isdir(dirName):
            os.makedirs(dirName)
        Export.RexExport.AddTakeOutputPath(takeName, dirName)


    def fillP4Cahce(self):
        cmd = "files"
        resourcePath = os.path.join(Config.Project.Path.Root,
                                            "audio",
                                            "DEV",
                                            "ASSETS",
                                            "Waves",
                                            "...wav")

        args = ["-e", resourcePath]
        self._cachedFileList = []
        p4Files = Perforce.Run(cmd, args)
        if str('has been unloaded') in p4Files:
            None
        else:
            for record in p4Files.Records:
                path = record['depotFile']
                self._cachedFileList.append(str(path))

pathGenerator = AudioExportPathGenerator()


def Run(show=True):
    fileName = mobu.FBApplication().FBXFileName
    system = mobu.FBSystem()
    takes = [tk.Name for tk in system.Scene.Takes]

    for takeName in takes:
        pathGenerator.resolvePath(fileName, takeName, ExportPathTypes.AnimClip)

    if show:
        QtGui.QMessageBox.information(None, "Facial Export Path Manager", "Export Takes are setup")

