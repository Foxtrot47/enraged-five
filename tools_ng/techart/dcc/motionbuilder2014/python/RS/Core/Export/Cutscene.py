"""
Pythonic wrapper for the Cutscene Methods

author: <David Vega dvega@rockstargames.com>
"""
import Export
from ctypes import cdll, c_float

class Cutscene(Export.RexExport):
    pass

class Offsets(object):

    def __str__(self):
        return str((self.x, self.y, self.z))

    @property
    def xyz(self):
        return self.x, self.y, self.z

    @property
    def x(self):
        cdll.rexMBRage.GetCutsceneOffsetsX_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsX_Py()

    @x.setter
    def x(self, value):
        """
        :param value: float
        :return:
        """
        cdll.rexMBRage.SetCutsceneOffsets_Py(c_float(value), c_float(self.y), c_float(self.z), c_float(self.orientation))

    @property
    def y(self):
        cdll.rexMBRage.GetCutsceneOffsetsY_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsX_Py()

    @y.setter
    def y(self, value):
        """
        :param value: float
        :return:
        """
        cdll.rexMBRage.SetCutsceneOffsets_Py(c_float(self.x), c_float(value), c_float(self.z), c_float(self.orientation))

    @property
    def z(self):
        cdll.rexMBRage.GetCutsceneOffsetsZ_Py.restype = c_float
        return cdll.rexMBRage.GetCutsceneOffsetsX_Py()

    @y.setter
    def z(self, value):
        """
        :param value: float
        :return:
        """
        cdll.rexMBRage.SetCutsceneOffsets_Py(c_float(self.x), c_float(self.y), c_float(value), c_float(self.orientation))

