"""
Wrapper classes representing various animation data objects
"""

import fbx
import FbxCommon
import pyfbsdk as mobu

from RS import Globals


FACE_CONTROL_LIST = (
                   Globals.gAmbient2DSliders +
                   Globals.gAmbient1DSliders +
                   Globals.gMouthCtrls +
                   Globals.gEyeCtrls +
                   Globals.gBrowCtrls +
                   Globals.gOnFaceControls +
                   Globals.g3WaySliders +
                   Globals.qPosSliders +
                   Globals.qPosNegSliders +
                   Globals.qSquareSliders +
                   Globals.gBlushCtrl +
                   Globals.gUFCFacial_Ctrls + # New UFC Controls
                   Globals.gUFCFacial_Ctrls_Switches # New UFC Controls
               )


FBX_TO_MOBU_INTERPOLATIONS = {
                        fbx.FbxAnimCurveDef.eInterpolationConstant: mobu.FBInterpolation.kFBInterpolationConstant,
                        fbx.FbxAnimCurveDef.eInterpolationLinear : mobu.FBInterpolation.kFBInterpolationLinear,
                        fbx.FbxAnimCurveDef.eInterpolationCubic : mobu.FBInterpolation.kFBInterpolationCubic,
                        }


FBX_TO_MOBU_TANGENTS = {
                        fbx.FbxAnimCurveDef.eTangentAuto : mobu.FBTangentMode.kFBTangentModeClampProgressive,
                        fbx.FbxAnimCurveDef.eTangentTCB : mobu.FBTangentMode.kFBTangentModeTCB,
                        fbx.FbxAnimCurveDef.eTangentUser : mobu.FBTangentMode.kFBTangentModeUser,
                        fbx.FbxAnimCurveDef.eTangentGenericBreak : mobu.FBTangentMode.kFBTangentModeClampProgressive, # No Mobu Alternatives
                        fbx.FbxAnimCurveDef.eTangentBreak : mobu.FBTangentMode.kFBTangentModeBreak,
                        fbx.FbxAnimCurveDef.eTangentAutoBreak : mobu.FBTangentMode.kFBTangentModeClampProgressive, # No Mobu Alternatives
                        fbx.FbxAnimCurveDef.eTangentGenericClamp : mobu.FBTangentMode.kFBTangentModeClampProgressive, # No Mobu Alternatives
                        fbx.FbxAnimCurveDef.eTangentGenericTimeIndependent : mobu.FBTangentMode.kFBTangentModeTimeIndependent,
                        fbx.FbxAnimCurveDef.eTangentGenericClampProgressive : mobu.FBTangentMode.kFBTangentModeClampProgressive,
                        }


class RsKey(object):
    """
    Defaults set to:  tangentmode is clamp progressive, interpolation is cubic.
    To view other options, print mobu.FBTangentMode.values, or mobu.FBInterpolation.values
    """
    def __init__(self,
                 value,
                 time,
                 tangentmode=mobu.FBTangentMode.kFBTangentModeClampProgressive,
                 interpolation=mobu.FBInterpolation.kFBInterpolationCubic
                 ):
        self.value = value
        self.time = time
        self.tangentmode = tangentmode
        self.interpolation = interpolation

        self.hasBreak = False
        self.rightTangent = 0
        self.leftTangent = 0
        self.rightDerivative = 0
        self.leftDerivative = 0

        self.rightTangentWeighted = False
        self.rightTangentWeight = 0
        self.leftTangentWeighted = False
        self.leftTangentWeight = 0


class RsFCurve(object):
    def __init__(self, name):
        self.name = str(name)
        self.keys = []


class RsModel(object):
    def __init__(self, name):
        self.name = str(name)

        try:
            self.namespace = str(name).split(':')[0]

        # Multiple ':' in the namespace, so deal with that situation.
        except ValueError:
            namespaces = str(name).split(':')[:-1]

            self.namespace = ''

            for idx in namespaces:
                self.namespace += '{0}:'.format(idx)

            self.namespace = self.namespace.rstrip(':')

        self.translation = {}
        self.rotation = {}


class RsAnimationLayer(object):
    def __init__(self, name):
        self.name = str(name)
        self.models = {}


class RsTake(object):
    def __init__(self, name):
        self.name = str(name)
        self.animationLayers = {}
        self.startFrame = 0
        self.endFrame = 0


class RsFaceAnimationFile(object):
    def __init__(self, filename):
        self.filename = filename
        self.takes = {}
        self.characters = []

        # Perforce information about the file.
        self.fileInfo = None


class RsImportOptions(object):
    '''
    Description:
        Represents options for importing of animation.

    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    def __init__(self):
        # Whether or not the animation will be imported on the current take.
        self.useCurrentTake = True
        self.importOnto = "0"
        self.currentTakeName = ""
        self.createMissingTakes = False
        self.mergeLayers = False
        self.mergedLayerName = None
        self.targetNamespace = ""
        self.characters = []

        self._takes = {}

    def addAnimationLayer(self, takeName, layerName):
        '''
        Description:
            Add an animation layer to be imported or exported.

        Arguments:
            takeName:   The take name.
            layerName:  The animation layer name.
        '''

        if takeName in self._takes:
            if layerName not in self._takes[takeName]:
                self._takes[takeName].append(layerName)

        else:
            self._takes[takeName] = [layerName]

    def removeAnimationLayer(self, takeName, layerName):
        '''
        Description:
            Remove an animation layer to be imported or exported.

        Arguments:
            takeName:   The take name.
            layerName:  The animation layer name.
        '''
        if takeName in self._takes:
            if layerName in self._takes[takeName]:
                layerNames = self._takes[takeName]
                layerNames.remove(layerName)

                self._takes[takeName] = layerNames

    def clear(self):
        '''
        Description:
            Clear all of the options.
        '''
        self._takes = {}

    @property
    def takes(self):
        return self._takes


def fbxGetAnimationFromFile(filename):
    '''
    Description:
        Inspects the supplied fbx file for face animation and returns it.

    '''
    # Initialize the fbx sdk.
    sdkManager, scene = FbxCommon.InitializeSdkObjects()

    # Load the fbx file into memory.
    result = FbxCommon.LoadScene(sdkManager, scene, filename)

    if result:

        # Read animation data.
        rsAnimFileObj = RsFaceAnimationFile(filename)
        fbxGetAnimationFromTakes(rsAnimFileObj, scene)

        sdkManager.Destroy()

        return rsAnimFileObj

    return None


def fbxGetAnimationFromCurve(animCurve):
    '''
    Description:
        Retrieves keys on the supplied animation curve.

    Arguments:
        animCurve:  The fbx animation curve object to inspect.
    '''
    # Create our fcurve.
    rsCurve = RsFCurve(animCurve.GetName())

    # Get the number of keys on the curve and record the key data.
    numKeys = animCurve.KeyGetCount()

    for keyId in xrange(numKeys):
        keyValue = animCurve.KeyGetValue(keyId)
        keyTime = animCurve.KeyGetTime(keyId).Get()
        keyInterpolation = FBX_TO_MOBU_INTERPOLATIONS.get(animCurve.KeyGetInterpolation(keyId), mobu.FBInterpolation.kFBInterpolationCubic)
        keyTangent = FBX_TO_MOBU_TANGENTS.get(animCurve.KeyGetTangentMode(keyId), mobu.FBTangentMode.kFBTangentModeClampProgressive)

        # Create our key object and add it to our curve.
        rsKey = RsKey(keyValue, keyTime, tangentmode=keyTangent , interpolation=keyInterpolation)

        rsKey.hasBreak = animCurve.KeyGetBreak(keyId)
        if rsKey.hasBreak is True:
            rsKey.rightDerivative = animCurve.KeyGetRightDerivative(keyId)
            rsKey.leftDerivative = animCurve.KeyGetLeftDerivative(keyId)
            rsKey.rightTangent = animCurve.KeyGetRightAuto(keyId)
            rsKey.leftTangent = animCurve.KeyGetLeftAuto(keyId)

        rsKey.rightTangentWeighted = animCurve.KeyIsRightTangentWeighted(keyId)
        if rsKey.rightTangentWeighted is True:
            rsKey.rightTangentWeight = animCurve.KeyGetRightTangentWeight(keyId)
        rsKey.leftTangentWeighted = animCurve.KeyIsLeftTangentWeighted(keyId)
        if rsKey.leftTangentWeighted is True:
            rsKey.leftTangentWeight = animCurve.KeyGetLeftTangentWeight(keyId)

        rsCurve.keys.append(rsKey)

    return rsCurve


def fbxGetAnimationFromLayer(rsAnimLayer, animLayer, node):
    '''
    Description:
        Inspects the supplied fbx animation layer, and finds the animation for each model on the layer.

    Arguments:
        rsAnimLayer:    The RsAnimationLayer object.
        animLayer:      The fbx animation layer object.
        node:           The fbx object to inspect.
    '''
    # Iterate over each node and only look at meshes.
    for modelId in range(node.GetChildCount()):
        model = node.GetChild(modelId)

        if model.GetTypeName() in ['Mesh', 'Line']:
            rsModel = RsModel(model.GetName())

            try:
                namespace, name = str(rsModel.name).split(':')

            # Hit multiple : in the name.  For now, it isn't what we
            except ValueError:
                name = str(rsModel.name).split(':')[-1]

            if rsModel.name not in rsAnimLayer.models and name in FACE_CONTROL_LIST:

                # Add the model to our animation layer.
                rsAnimLayer.models[rsModel.name] = rsModel

                # Record the fcurve data.

                # Translation
                xPosCurve = model.LclTranslation.GetCurve(animLayer, 'X')
                yPosCurve = model.LclTranslation.GetCurve(animLayer, 'Y')
                zPosCurve = model.LclTranslation.GetCurve(animLayer, 'Z')

                if xPosCurve:
                    curve = fbxGetAnimationFromCurve(xPosCurve)
                    rsModel.translation['X'] = curve

                if yPosCurve:
                    curve = fbxGetAnimationFromCurve(yPosCurve)
                    rsModel.translation['Y'] = curve

                if zPosCurve:
                    curve = fbxGetAnimationFromCurve(zPosCurve)
                    rsModel.translation['Z'] = curve

                # Rotation
                xRotCurve = model.LclRotation.GetCurve(animLayer, 'X')
                yRotCurve = model.LclRotation.GetCurve(animLayer, 'Y')
                zRotCurve = model.LclRotation.GetCurve(animLayer, 'Z')

                if xRotCurve:
                    curve = fbxGetAnimationFromCurve(xRotCurve)
                    rsModel.rotation['X'] = curve

                if yRotCurve:
                    curve = fbxGetAnimationFromCurve(yRotCurve)
                    rsModel.rotation['Y'] = curve

                if zRotCurve:
                    curve = fbxGetAnimationFromCurve(zRotCurve)
                    rsModel.rotation['Z'] = curve

        fbxGetAnimationFromLayer(rsAnimLayer, animLayer, model)


def fbxGetAnimationFromTakes(rsAnimFileObj, scene):
    '''
    Description:
        Inspects the supplied fbx file for face animation and returns it.

    Arguments:
        scene:  The fbx scene object.
    '''
    # Get the number of takes from the fbx file, which are classified as an animation stack in the fbx file format.
    numTakes = scene.GetSrcObjectCount(fbx.FbxAnimStack.ClassId)

    # Iterate over each take.
    for takeId in xrange(numTakes):

        # Get the take object.
        take = scene.GetSrcObject(fbx.FbxAnimStack.ClassId, takeId)


        # Create our take object.
        rsTake = RsTake(take.GetName())

        # Get and store the start and end frames
        rsTake.startFrame = take.GetLocalTimeSpan().GetStart().GetFrameCount()
        rsTake.endFrame = take.GetLocalTimeSpan().GetStop().GetFrameCount()

        # Get the number of animation layers for this take.
        numAnimLayers = take.GetSrcObjectCount(fbx.FbxAnimLayer.ClassId)

        # Iterate over each animation layer.
        for animLayerId in xrange(numAnimLayers):

            # Get the animation layer object.
            animLayer = take.GetSrcObject(fbx.FbxAnimLayer.ClassId, animLayerId)

            # Create our animation layer object and add it to our take.
            rsAnimLayer = RsAnimationLayer(str(animLayer.GetName()).split(':')[-1])
            rsTake.animationLayers[rsAnimLayer.name] = rsAnimLayer

            # Recursively search through the model nodes on the animation layer and store off the animation data.
            fbxGetAnimationFromLayer(rsAnimLayer, animLayer, scene.GetRootNode())

            # Add our take to the return data.
            rsAnimFileObj.takes[rsTake.name] = rsTake

            for modelName, model in rsAnimLayer.models.iteritems():
                if model.namespace not in rsAnimFileObj.characters:
                    rsAnimFileObj.characters.append(model.namespace)


def importFaceAnimation(faceAnimFileObj, importOptions, quiet=False):
    '''
    Description:
        Imports animation from an fbx file onto face animation rigs in the scene.

    Arguments:
        faceAnimFileObj:    The RsFaceAnimationFile object to read.  Created by calling fbxGetAnimationFromFile().
        importOptions:      The RsImportOptions object to use, which dictate what takes and animation layers from the
                            fbx file to import.
    '''
    # Bug with mobu 2014 (doesn't occur in 2016) : need to test if any import options have been passed.
    if importOptions.targetNamespace == "" and importOptions.currentTakeName == "":
        mobu.FBMessageBox('Rockstar', 'Error: No import options have been passed. Recreate the Face Asset.', 'OK')
        return
    elif importOptions.targetNamespace == "":
        # The new targetNamespace property hasn't been introduced in the old ref system, this protects against the property being empty
        # Unfortunately assumes that the target namespace will always be the same as the namespace from the resource face file
        importOptions.targetNamespace = faceAnimFileObj.characters[0]

    currentTake = Globals.System.CurrentTake

    takePool = None
    if importOptions.importOnto == "0":
        takePool = importOptions.currentTakeName

    elif importOptions.importOnto == "1":
        takePool = importOptions.currentTakeName

    elif importOptions.importOnto == "2":
        takePool = faceAnimFileObj.takes

    # Bug 2197732. Sometimes a reference doesn't contain any information in the take and import option field.(Temporary fix, need to fix the creation of the reference)
    elif importOptions.importOnto == "":
        takePool = faceAnimFileObj.takes

    if importOptions.importOnto == "":
        print "Import onto : {0}".format("none")
        print "Current take : {0}".format(faceAnimFileObj.takes)
    else:
        print "Import onto : {0}".format(importOptions.importOnto)
        print "Current take : {0}".format(importOptions.currentTakeName)
        print "Target Character : {0}".format(importOptions.targetNamespace)

    # Iterate over each take in the import options.
    for takeName, layerNames in importOptions.takes.iteritems():

        # If the take name is in the face animation data, import the data into the scene.
        if takePool != None and takeName in takePool:
            animFileTakeObj = faceAnimFileObj.takes[takeName]

            # See if the take name already exists in the scene.
            takeAlreadyExists = False

            # Use the take name found in the fbx file.
            if importOptions.importOnto == "" or importOptions.importOnto == "0":
                takeAlreadyExists = True

            else:
                for take in Globals.System.Scene.Takes:
                    if take.Name == takeName:
                        Globals.System.CurrentTake = take
                        currentTake = take
                        takeAlreadyExists = True

                # The take doesn't exist, so create one and set it as the current one.
                if not takeAlreadyExists:
                    if importOptions.createMissingTakes:
                        newTake = mobu.FBTake(takeName)
                        Globals.Scene.Takes.append(newTake)
                        Globals.System.CurrentTake = newTake
                        currentTake = newTake
                        takeAlreadyExists = True

            if takeAlreadyExists:
                # Merge the animation layers.
                if importOptions.mergeLayers:

                    # The final result of iterating over each layer and merging the key data.
                    #
                    # key: The model name.
                    # value: The merged animation data for both the translation and rotation.
                    mergeData = {}

                    # Collect all of the keyframe data from each animation layer for each object.  The result of this
                    # loop is a collapsed set of key frame data.
                    for layerName in layerNames:

                        # Get the animation layer data from the fbx file.
                        animFileLayerObj = animFileTakeObj.animationLayers[layerName]

                        # Iterate over each model for the layer.
                        for modelNodeName, modelNode in animFileLayerObj.models.iteritems():

                            # Only look at the characters defined by the import options.
                            if modelNode.namespace in importOptions.characters:

                                # Get the real model in the scene.
                                model = mobu.FBFindModelByLabelName(modelNode.name)

                                if model:
                                    modelAnimData = None

                                    # Setup the initial merged keyframe data set for the model.
                                    if model.LongName not in mergeData:
                                        modelAnimData = { 'Lcl Translation' : { 'X' : {}, 'Y' : {}, 'Z' : {} },
                                                          'Lcl Rotation'    : { 'X' : {}, 'Y' : {}, 'Z' : {} } }

                                    # Merged keyframe data already exists for this model.
                                    else:
                                        modelAnimData = mergeData[model.LongName]

                                    if modelAnimData:

                                        # Merge translation.
                                        for subAnimNodeName, rsCurve in modelNode.translation.iteritems():

                                            # Get the merged keyframe data dictionary for the current sub-animation node.
                                            # Example: { 'X' : { time : value, time: value, ... } }
                                            subAnimData = modelAnimData['Lcl Translation'][subAnimNodeName]

                                            for key in rsCurve.keys:

                                                # Key at the time doesn't exist, so add it.
                                                if key.time not in subAnimData:
                                                    subAnimData[key.time] = key.value

                                                # Key already exists, so add it to the current time value.
                                                else:
                                                    subAnimData[key.time] += key.value

                                            modelAnimData['Lcl Translation'][subAnimNodeName] = subAnimData

                                        # Merge rotation.
                                        for subAnimNodeName, rsCurve in modelNode.rotation.iteritems():
                                            subAnimData = modelAnimData['Lcl Rotation'][subAnimNodeName]

                                            for key in rsCurve.keys:

                                                # Key at the time doesn't exist, so add it.
                                                if key.time not in subAnimData:
                                                    subAnimData[key.time] = key.value

                                                # Key already exists, so add it to the current time value.
                                                else:
                                                    delta = key.value - subAnimData[key.time]
                                                    subAnimData[key.time] += delta

                                            modelAnimData['Lcl Rotation'][subAnimNodeName] = subAnimData

                                        mergeData[model.LongName] = modelAnimData

                    # Create our new merged layer.
                    currentTake.CreateNewLayer()

                    numLayers = currentTake.GetLayerCount()
                    layer = currentTake.GetLayer(numLayers - 1)
                    layer.Name = importOptions.mergedLayerName

                    currentTake.SetCurrentLayer(numLayers - 1)
                    currentLayer = layer

                    # Iterate over the merged animation data for each model and create new fcurves for each.
                    for modelName, animData in mergeData.iteritems():

                        # Get the namespace of the model we want to transfer data onto
                        modelLongName = modelNode.name
                        modelNameSplit = modelNode.name.split(":")
                        if len(modelNameSplit) > 2:
                            continue
                        '''
                        if len(modelNameSplit) == 1:
                            mobu.FBMessageBox("R* Error", "Selected model has no namespace", "OK")
                            raise Exception("R* Error: Selected model has no namespace")
                        if len(modelNameSplit) > 2:
                            mobu.FBMessageBox("R* Error", "Too many namespaces for the targeted model", "OK")
                            raise Exception("R* Error: Too many namespaces for the targeted model")
                        '''
                        modelLongName = "{0}:{1}".format(importOptions.targetNamespace, modelNameSplit[1])

                        if modelNode.namespace in importOptions.characters:

                            model = mobu.FBFindModelByLabelName(modelLongName)

                            if model:
                                lclTranslationProp = model.PropertyList.Find('Lcl Translation')
                                lclRotationProp = model.PropertyList.Find('Lcl Rotation')

                                # Translation
                                if lclTranslationProp:
                                    lclTranslationSubAnimData = animData['Lcl Translation']
                                    lclRotationSubAnimData = animData['Lcl Rotation']

                                    for subAnimNodeName, keyData in lclTranslationSubAnimData.iteritems():

                                        # Get each sub-animation node that has animation in the file and replace it.
                                        subAnimNode = None

                                        if not lclTranslationProp.GetAnimationNode():
                                            lclTranslationProp.SetAnimated(True)

                                        animNodes = lclTranslationProp.GetAnimationNode().Nodes

                                        # Find the matching sub-anim node.
                                        for animNode in animNodes:
                                            if animNode.Name == subAnimNodeName:
                                                subAnimNode = animNode

                                        if subAnimNode:
                                            fcurve = subAnimNode.FCurve

                                            # Clear existing animation on model.
                                            fcurve.EditClear()

                                            # Begin adding the new keys.
                                            fcurve.EditBegin()

                                            # Iterate over the RsKey objects.
                                            keyNum = 0

                                            for keyTime, keyValue, keyTangentMode, keyInterpolation in keyData.iteritems():
                                                currentKey = None
                                                try:
                                                    currentKey = fcurve.KeyAdd(mobu.FBTime(keyTime), keyValue)
                                                    if currentKey:
                                                        fcurve.Keys[currentKey].TangentMode = mobu.FBTangentMode.values[keyTangentMode]
                                                        fcurve.Keys[currentKey].Interpolation = mobu.FBInterpolation.values[keyInterpolation]

                                                except OverflowError:
                                                    pass

                                                keyNum += 1

                                            fcurve.EditEnd()

                            # Rotation
                            if lclRotationProp:
                                for subAnimNodeName, keyData in lclRotationSubAnimData.iteritems():

                                    # Get each sub-animation node that has animation in the file and replace it.
                                    subAnimNode = None

                                    if not lclRotationProp.GetAnimationNode():
                                        lclRotationProp.SetAnimated(True)

                                    animNodes = lclRotationProp.GetAnimationNode().Nodes

                                    # Find the matching sub-anim node.
                                    for animNode in animNodes:
                                        if animNode.Name == subAnimNodeName:
                                            subAnimNode = animNode

                                    if subAnimNode:
                                        fcurve = subAnimNode.FCurve

                                        # Clear existing animation on model.
                                        fcurve.EditClear()

                                        # Begin adding the new keys.
                                        fcurve.EditBegin()

                                        # Iterate over the RsKey objects.
                                        keyNum = 0

                                        for keyTime, keyValue, keyTangentMode, keyInterpolation in keyData.iteritems():
                                            currentKey = None
                                            try:
                                                currentKey = fcurve.KeyAdd(mobu.FBTime(keyTime), keyValue)
                                                if currentKey:
                                                    fcurve.Keys[currentKey].TangentMode = mobu.FBTangentMode.values[keyTangentMode]
                                                    fcurve.Keys[currentKey].Interpolation = mobu.FBInterpolation.values[keyInterpolation]

                                            except OverflowError:
                                                pass

                                            keyNum += 1

                                        fcurve.EditEnd()

                # Normal import, keep animation layers intact.
                else:
                    for layerName in layerNames:
                        if layerName in animFileTakeObj.animationLayers:
                            animFileLayerObj = animFileTakeObj.animationLayers[layerName]

                            currentLayer = None

                            # Set the active layer.
                            numLayers = currentTake.GetLayerCount()

                            for layerId in range(0, numLayers):
                                layer = currentTake.GetLayer(layerId)

                                if layer.Name == layerName:

                                    # TODO: Ask to make sure the user wants to override existing animation on this layer.
                                    currentTake.SetCurrentLayer(layerId)
                                    currentLayer = layer

                            # No layer using the name found, so create a new one and set it as the active layer.
                            if not currentLayer:
                                currentTake.CreateNewLayer()

                                numLayers = currentTake.GetLayerCount()
                                currentLayer = currentTake.GetLayer(numLayers - 1)
                                currentLayer.Name = layerName

                                currentTake.SetCurrentLayer(numLayers - 1)

                            # Iterate over each model in the animation file.
                            for modelNodeName, modelNode in animFileLayerObj.models.iteritems():

                                modelLongName = modelNode.name
                                modelNameSplit = modelNode.name.split(":")

                                # use end of list for the name, so if the object doesnt have a
                                # namespace then using  modelNameSplit[-1] will still work
                                # url:bugstar:5495523
                                modelLongName = "{0}:{1}".format(importOptions.targetNamespace, modelNameSplit[-1])

                                if modelNode.namespace in importOptions.characters:
                                    model = mobu.FBFindModelByLabelName(str(modelLongName))

                                    # If the model exists in the scene...
                                    if model:
                                        lclTranslationProp = model.PropertyList.Find('Lcl Translation')
                                        lclRotationProp = model.PropertyList.Find('Lcl Rotation')

                                        # Translation
                                        if lclTranslationProp:
                                            for subAnimNodeName, rsCurve in modelNode.translation.iteritems():

                                                # Get each sub-animation node that has animation in the file and replace it.
                                                subAnimNode = None

                                                if not lclTranslationProp.GetAnimationNode():
                                                    lclTranslationProp.SetAnimated(True)

                                                animNodes = lclTranslationProp.GetAnimationNode().Nodes

                                                # Find the matching sub-anim node.
                                                for animNode in animNodes:
                                                    if animNode.Name == subAnimNodeName:
                                                        subAnimNode = animNode

                                                if subAnimNode:
                                                    fcurve = subAnimNode.FCurve

                                                    # Clear existing animation on model.
                                                    fcurve.EditClear()

                                                    # Begin adding the new keys.
                                                    fcurve.EditBegin()

                                                    # Iterate over the RsKey objects.
                                                    keyNum = 0
                                                    keyInterDict = {}
                                                    for keyNode in rsCurve.keys:
                                                        currentKey = None
                                                        try:
                                                            currentKey = fcurve.KeyAdd(mobu.FBTime(keyNode.time), keyNode.value)
                                                            keyInterDict[currentKey] = keyNode
                                                        except OverflowError:
                                                            pass

                                                        keyNum += 1

                                                    for keyInterNum, keyInterData in keyInterDict.iteritems():
                                                        fcurve.Keys[keyInterNum].Interpolation = keyInterData.interpolation
                                                        fcurve.Keys[keyInterNum].TangentMode = keyInterData.tangentmode

                                                        if keyInterData.hasBreak:
                                                            fcurve.Keys[keyInterNum].TangentBreak = True
                                                            fcurve.Keys[keyInterNum].RightDerivative = keyInterData.rightDerivative
                                                            fcurve.Keys[keyInterNum].LeftDerivative = keyInterData.leftDerivative

                                                        if keyInterData.rightTangentWeighted:
                                                            fcurve.Keys[keyInterNum].RightTangentWeight = keyInterData.rightTangentWeight

                                                        if keyInterData.leftTangentWeighted:
                                                            fcurve.Keys[keyInterNum].LeftTangentWeight = keyInterData.leftTangentWeight

                                                    fcurve.EditEnd()

                                        # Rotation
                                        if lclRotationProp:
                                            for subAnimNodeName, rsCurve in modelNode.rotation.iteritems():

                                                # Get each sub-animation node that has animation in the file and replace it.
                                                subAnimNode = None

                                                if not lclRotationProp.GetAnimationNode():
                                                    lclRotationProp.SetAnimated(True)

                                                animNodes = lclRotationProp.GetAnimationNode().Nodes

                                                # Find the matching sub-anim node.
                                                for animNode in animNodes:
                                                    if animNode.Name == subAnimNodeName:
                                                        subAnimNode = animNode

                                                if subAnimNode:
                                                    fcurve = subAnimNode.FCurve

                                                    # Clear existing animation on model.
                                                    fcurve.EditClear()

                                                    # Begin adding the new keys.
                                                    fcurve.EditBegin()

                                                    # Iterate over the RsKey objects.
                                                    keyInterDict = {}
                                                    for keyNode in rsCurve.keys:
                                                        currentKey = None
                                                        try:
                                                            currentKey = fcurve.KeyAdd(mobu.FBTime(keyNode.time), keyNode.value)
                                                            keyInterDict[currentKey] = keyNode

                                                        except OverflowError:
                                                            pass

                                                    for keyInterNum, keyInterData in keyInterDict.iteritems():
                                                        fcurve.Keys[keyInterNum].Interpolation = keyInterData.interpolation
                                                        fcurve.Keys[keyInterNum].TangentMode = keyInterData.tangentmode

                                                        if keyInterData.hasBreak:
                                                            fcurve.Keys[keyInterNum].TangentBreak = True
                                                            fcurve.Keys[keyInterNum].RightDerivative = keyInterData.rightDerivative
                                                            fcurve.Keys[keyInterNum].LeftDerivative = keyInterData.leftDerivative

                                                        if keyInterData.rightTangentWeighted:
                                                            fcurve.Keys[keyInterNum].RightTangentWeight = keyInterData.rightTangentWeight

                                                        if keyInterData.leftTangentWeighted:
                                                            fcurve.Keys[keyInterNum].LeftTangentWeight = keyInterData.leftTangentWeight

                                                    fcurve.EditEnd()

    if Globals.gUlog.HasErrors:
        if not quiet:
            mobu.FBMessageBox('Rockstar', 'Errors were encountered with the face animation import, which could result in bad facial animation.  Please check your work before saving!', 'OK')

            Globals.gUlog.Show(modal=False)

        return False

    return True
