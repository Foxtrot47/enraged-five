import os
import RS.Perforce

from pyfbsdk import *
from pyfbsdk_additions import *
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character

import RS.Core.Face.AnimImporter
reload( RS.Core.Face.AnimImporter ) 

global animFileContentsEditor
global importFaceAnimTool
global currentTakeName
global faceAnimFileObj
global etMergedLayerName
global lblMergedLayerName
global animIporterDialogClosed
global targetNamespace

animIporterDialogClosed= False

global importOptions
importOptions = RS.Core.Face.AnimImporter.RsImportOptions()


## Event Handlers ##

def onImport_Clicked( source, event ):
    global importFaceAnimTool
    global faceAnimFileObj
    global importOptions
    
    # Import animation
    RS.Core.Face.AnimImporter.importFaceAnimation( faceAnimFileObj, importOptions )
    
    importFaceAnimTool.Close( True )
    
def onTake_Changed( source, event ):
    global currentTakeName
    global importOptions
    
    currentTakeName = source.Items[ source.ItemIndex ]
    importOptions.currentTakeName = currentTakeName
    
    populateAnimFileContentsEditor()
    
def onCancel_Clicked( source, event ):
    global importFaceAnimTool
    global animIporterDialogClosed
    
    animIporterDialogClosed = True
    importFaceAnimTool.Close( True )
    
def onNamespace_Changed( source, event ):
    global targetNamespace
    global importOptions
    
    targetNamespace = source.Items[ source.ItemIndex ]
    importOptions.targetNamespace = targetNamespace    
    
def onAnimFileContentsEditor_Clicked( control, event ):
    global importOptions
    global currentTakeName
    
    val = control.GetCellValue( event.Row, event.Column )
    
    if event.Column == 0:
        layerName = control.GetCellValue( event.Row, 1 )
        
        if val == 0:
            importOptions.removeAnimationLayer( currentTakeName, layerName )
            
        else:
            importOptions.addAnimationLayer( currentTakeName, layerName )
            
def onCharactersEditor_Clicked( control, event ):
    global importOptions
    global currentTakeName
    
    val = control.GetCellValue( event.Row, event.Column )
    
    if event.Column == 0:
        characterName = control.GetCellValue( event.Row, 1 )
        
        if val == 0:
            if characterName in importOptions.characters:
                importOptions.characters.remove( characterName )
            
        else:
            importOptions.characters.append( characterName )
            
def onImportOntoCurrentTake_Changed( source, event ):
    global importOptions
    
    print source.Caption
    if 'current' in source.Caption.lower():
        importOptions.importOnto = "0"
    elif 'selected to matching' in source.Caption.lower():
        importOptions.importOnto = "1"
    elif 'all' in source.Caption.lower():
        importOptions.importOnto = "2"
            
def onMergeLayers_Changed( source, event ):
    global importOptions
    global etMergedLayerName
    global lblMergedLayerName
    
    if source.State == 0:
        importOptions.mergeLayers = False
        etMergedLayerName.Enabled = False
        lblMergedLayerName.Enabled = False
        
    else:
        importOptions.mergeLayers = True
        etMergedLayerName.Enabled = True
        lblMergedLayerName.Enabled = True
        FBMessageBox( 'Rockstar', 'Add Merge Animation Layer Name.'.format( faceAnimFileObj ), 'OK' ) 
        
def onCreateMissingTakes_Changed( source, event ):
    global importOptions
    if source.State == 0:
        importOptions.createMissingTakes = False
    else:
        importOptions.createMissingTakes = True

def onMergedLayerName_Changed( source, event ):
    global importOptions
   
    importOptions.mergedLayerName = source.Text


## Functions ##

def getImportOptions():
    global importOptions
    return importOptions

def populateAnimFileContentsEditor():
    global currentTakeName
    global faceAnimFileObj
    global importOptions
    global animFileContentsEditor
    global charactersEditor
    
    # Animation Layers Spreadsheet
    animFileContentsEditor.Clear()
    
    animFileContentsEditor.ColumnAdd( 'Import' )
    animFileContentsEditor.ColumnAdd( 'Animation Layer' )
    
    animFileContentsEditor.GetColumn( -1 ).Width = -10    
    
    animFileContentsEditor.GetColumn( 0 ).Width = 60
    animFileContentsEditor.GetColumn( 0 ).Style = FBCellStyle.kFBCellStyle2StatesButton
    
    animFileContentsEditor.GetColumn( 1 ).ReadOnly = True
    animFileContentsEditor.GetColumn( 1 ).Width = 250
    
    row = 0
    
    if currentTakeName in faceAnimFileObj.takes:
        takeObj = faceAnimFileObj.takes[ currentTakeName ]
        
        layerNames = takeObj.animationLayers.keys()
        
        for layerName in layerNames:
            animFileContentsEditor.RowAdd( currentTakeName, row )
            
            doImportAnimLayer = 0
            
            if layerName in importOptions.takes[ currentTakeName ]:
                doImportAnimLayer = 1
            
            animFileContentsEditor.SetCellValue( row, 0, doImportAnimLayer )
            animFileContentsEditor.SetCellValue( row, 1, layerName )
        
            row += 1
            
    # Characters Spreadsheet
    charactersEditor.Clear()
    
    charactersEditor.ColumnAdd( 'Import' )
    charactersEditor.ColumnAdd( 'Character Name' )
    
    charactersEditor.GetColumn( -1 ).Width = -10  
    
    charactersEditor.GetColumn( 0 ).Width = 60
    charactersEditor.GetColumn( 0 ).Style = FBCellStyle.kFBCellStyle2StatesButton
    
    charactersEditor.GetColumn( 1 ).ReadOnly = True
    charactersEditor.GetColumn( 1 ).Width = 250
    
    row = 0
        
    for characterName in faceAnimFileObj.characters:
        charactersEditor.RowAdd( '', row )
        
        charactersEditor.SetCellValue( row, 0, 1 )
        charactersEditor.SetCellValue( row, 1, characterName )
    
        row += 1    

def createToolLayout( mainLayout ):
    global animFileContentsEditor
    global charactersEditor
    global importOptions
    global currentTakeName
    global faceAnimFileObj
    global etMergedLayerName
    global lblMergedLayerName
    
    #contents = animLibMgr.getTakesAndLayersFromFile( animZipFilename )
    
    lyt = FBHBoxLayout()
    
    x = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, '' )
    y = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, '' )
    w = FBAddRegionParam( -6, FBAttachType.kFBAttachRight, '' )
    h = FBAddRegionParam( -6, FBAttachType.kFBAttachBottom, '' )
    mainLayout.AddRegion( 'main', 'main', x, y, w, h )
    mainLayout.SetControl( 'main', lyt )
    
    # Import Onto Label
    lblImportOnto = FBLabel()
    lblImportOnto.Caption = 'Import:'
    
    cbImportGroup = FBButtonGroup()
    
    # Import onto current take
    cbImportOntoCurrentTake = FBButton()
    cbImportOntoCurrentTake.Caption = 'Selected to Current take'
    cbImportOntoCurrentTake.Style = FBButtonStyle.kFBRadioButton
    if importOptions.importOnto == "0":
        cbImportOntoCurrentTake.State = True
    cbImportOntoCurrentTake.OnClick.Add( onImportOntoCurrentTake_Changed )
    cbImportGroup.Add( cbImportOntoCurrentTake )
    
    # Import onto Specified take
    cbImportOntoSpecifiedTake = FBButton()
    cbImportOntoSpecifiedTake.Caption = 'Selected to Matching take'
    cbImportOntoSpecifiedTake.Style = FBButtonStyle.kFBRadioButton
    if importOptions.importOnto == "1":
        cbImportOntoSpecifiedTake.State = True
    cbImportOntoSpecifiedTake.OnClick.Add( onImportOntoCurrentTake_Changed )
    cbImportGroup.Add( cbImportOntoSpecifiedTake )
    
    # Import all matching takes
    cbImportOntoAllTakes = FBButton()
    cbImportOntoAllTakes.Caption = 'ALL to matching takes'
    cbImportOntoAllTakes.Style = FBButtonStyle.kFBRadioButton
    if importOptions.importOnto == "2":
        cbImportOntoAllTakes.State = True
    cbImportOntoAllTakes.OnClick.Add( onImportOntoCurrentTake_Changed )
    cbImportGroup.Add( cbImportOntoAllTakes )
    
    # Blank label
    lblBlank = FBLabel()
    lblBlank.Caption = ''
    
    # Create Missing Takes
    cbCreateMissingTakes = FBButton()
    cbCreateMissingTakes.Caption = 'Create Missing Takes'
    cbCreateMissingTakes.Style = FBButtonStyle.kFBCheckbox
    cbCreateMissingTakes.State = importOptions.createMissingTakes
    cbCreateMissingTakes.OnClick.Add( onCreateMissingTakes_Changed )  
    
    # Creating drop down mist of pottential namespaces to import to
    lblNamespace = FBLabel()
    lblNamespace.Caption = 'Target Namespace:'
    
    # Take drop-down
    ddlblNamespace = FBList()
    ddlblNamespace.Style = FBListStyle.kFBDropDownList
    ddlblNamespace.OnChange.Add( onNamespace_Changed )

    
    # fill the list with data
    manager = Manager( ) 
    
    for character in manager.GetReferenceListByType( Character ):
        ddlblNamespace.Items.append( character.Namespace )
    
    
    # Take drop-down label
    lblTakes = FBLabel()
    lblTakes.Caption = 'Takes:'
    
    # Take drop-down
    ddlTakes = FBList()
    ddlTakes.Style = FBListStyle.kFBDropDownList
    ddlTakes.OnChange.Add( onTake_Changed )
    
    for takeName in faceAnimFileObj.takes.keys():
        ddlTakes.Items.append( takeName )
    
    currentTakeName = faceAnimFileObj.takes.keys()[ 0 ]
    importOptions.currentTakeName = currentTakeName
    
    # Merge layers
    cbMergeLayers = FBButton()
    cbMergeLayers.Caption = 'Merge Animation Layers'
    cbMergeLayers.Style = FBButtonStyle.kFBCheckbox
    cbMergeLayers.State = importOptions.mergeLayers
    cbMergeLayers.OnClick.Add( onMergeLayers_Changed ) 
    
    
    
    lblMergedLayerName = FBLabel()
    lblMergedLayerName.Caption = 'Merged Animation Layer Name:'
    etMergedLayerName = FBEdit()
    etMergedLayerName.OnChange.Add( onMergedLayerName_Changed )    
    
    # Import animation
    lytImportAnim = FBHBoxLayout( FBAttachType.kFBAttachRight )
    
    btnImportAnimation = FBButton()
    btnImportAnimation.Caption = 'Import'
    btnImportAnimation.OnClick.Add( onImport_Clicked )
    
    btnCancel = FBButton()
    btnCancel.Caption = 'Cancel'
    btnCancel.OnClick.Add( onCancel_Clicked )
    
    lytImportAnim.Add( btnCancel, 70 )
    lytImportAnim.Add( btnImportAnimation, 70 )
    
    # Layers spreadsheet
    lblAnimationLayers = FBLabel()
    lblAnimationLayers.Caption = 'Select Animation Layers to Import:'    

    animFileContentsEditor = FBSpread()
    animFileContentsEditor.Caption = 'Take'
    animFileContentsEditor.OnCellChange.Add( onAnimFileContentsEditor_Clicked )
    
    # Characters spreadsheet
    lblCharacters = FBLabel()
    lblCharacters.Caption = 'Select Characters to Import:' 

    charactersEditor = FBSpread()
    charactersEditor.Caption = 'Characters'
    charactersEditor.OnCellChange.Add( onCharactersEditor_Clicked )
    
    # Layouts
    optionsLyt = FBVBoxLayout()
    spreadLyt = FBVBoxLayout()
    
    optionsLyt.Add( lblTakes, 24 )
    optionsLyt.Add( ddlTakes, 24 )
    optionsLyt.Add( lblImportOnto, 24 )
    optionsLyt.Add( cbImportOntoCurrentTake, 24 )
    optionsLyt.Add( cbImportOntoSpecifiedTake, 24 )
    optionsLyt.Add( cbImportOntoAllTakes, 24 )
    optionsLyt.Add( lblBlank, 24 )
    optionsLyt.Add( cbCreateMissingTakes, 24 )
    optionsLyt.Add( cbMergeLayers, 24 )
    optionsLyt.Add( lblMergedLayerName, 24 )
    optionsLyt.Add( etMergedLayerName, 24 )
    optionsLyt.Add( lblNamespace, 24)
    optionsLyt.Add( ddlblNamespace, 24)    
    
    spreadLyt.Add( lblAnimationLayers, 24 )
    spreadLyt.AddRelative( animFileContentsEditor, 0.5 )
    spreadLyt.Add( lblCharacters, 24 )
    spreadLyt.AddRelative( charactersEditor, 0.5 )
    spreadLyt.Add( lytImportAnim, 24 )
    
    lyt.Add( optionsLyt, 200 )
    lyt.AddRelative( spreadLyt, 1.0 )
    
    # Setup initial import options
    importOptions.clear()
    
    for takeName, takeObj in faceAnimFileObj.takes.iteritems():
        for layerName, layerObj in takeObj.animationLayers.iteritems():
            importOptions.addAnimationLayer( takeName, layerName )
          
    for characterName in faceAnimFileObj.characters:
        importOptions.characters.append( characterName )
        for character in manager.GetReferenceListByType( Character ):
            if characterName == character.Namespace:
                importOptions.targetNamespace = character.Namespace
                
    # In case there were no matching face characters to characters in the scene we need to set the import option to the first one character in the list   
    if manager.GetReferenceListByType( Character ):
        if not importOptions.targetNamespace:
            importOptions.targetNamespace = ddlblNamespace.Items[0]  
        else:
            for i in range(len(ddlblNamespace.Items)):
                if ddlblNamespace.Items[i]== importOptions.targetNamespace:
                    ddlblNamespace.Selected(i, True)

    populateAnimFileContentsEditor()
    
    etMergedLayerName.Enabled = False
    lblMergedLayerName.Enabled = False
    

def show( fbxFilename = None, getLatest = True, refNull = None ):
    global importFaceAnimTool
    global faceAnimFileObj
    
    fileInfo = None
    
    if fbxFilename:
        if getLatest:
            if not os.path.isfile( fbxFilename ):
                fileInfo = RS.Perforce.Sync( fbxFilename )
               
            if not fileInfo:
                fileInfo = RS.Perforce.GetFileState( fbxFilename )
        
            if fileInfo:
                if not RS.Perforce.IsLatest( fileInfo ):
                    result = FBMessageBox( 'Rockstar', 'You do not have the latest version of ({0}).  You currently have version ({1}) of ({2}).\n\nWould you like to sync to the latest version?'.format( os.path.basename( fbxFilename ), fileInfo.HaveRevision, fileInfo.HeadRevision ), 'Yes', 'No' )
                    
                    if result == 1:
                        RS.Perforce.Sync( fbxFilename, force = True )
            
        if not os.path.isfile( fbxFilename ):
            FBMessageBox( 'Rockstar', 'The file ({0}) does not exist locally! Aborting.'.format( fbxFilename ), 'OK' )
            return False
    
    if not fbxFilename:
        openDialog = FBFilePopup()
        openDialog.Caption = "Choose a FBX File"
        openDialog.Style = FBFilePopupStyle.kFBFilePopupOpen
        openDialog.Filter = "*.fbx" 
        
        openResult = openDialog.Execute()
        
        if openResult:
            fbxFilename = '{0}\\{1}'.format( openDialog.Path, openDialog.FileName )
    
    if fbxFilename:
        faceAnimFileObj = RS.Core.Face.AnimImporter.fbxGetAnimationFromFile( fbxFilename )
        
        if faceAnimFileObj:
            
            if fileInfo:
                faceAnimFileObj.fileInfo = fileInfo            

            screenWidth, screenHeight, huh = FBSystem().DesktopSize.GetList()
            
            importFaceAnimTool = FBPopup()
            importFaceAnimTool.Caption = 'Import Face Animation Options'
            importFaceAnimTool.Width = 650
            importFaceAnimTool.Height = 400
            importFaceAnimTool.Top = 100 #int( ( screenHeight / 2.0 ) - ( importFaceAnimTool.Height / 2.0 ) )
            importFaceAnimTool.Left = 100 #int( ( screenWidth / 2.0 ) - ( importFaceAnimTool.Width / 2.0 ) )
            importFaceAnimTool.Modal = True            
            
            createToolLayout( importFaceAnimTool )
            
            importFaceAnimTool.Show()
            
            return True
            
        else:
            FBMessageBox( 'Rockstar', 'Could not read animation from ({0})!'.format( fbxFilename ), 'OK' )

