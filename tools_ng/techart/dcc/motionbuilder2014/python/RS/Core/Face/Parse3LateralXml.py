import RS.Utils.Collections
from xml.dom import minidom
import os
from pyfbsdk import *

import RS.Config

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Offset 
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FindLongestNameInArray(pArray):
    
    lInputLength = 0
    for iInput in pArray:
        if len(iInput) > lInputLength:
            lInputLength = len(iInput)
            
    return lInputLength

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression XML
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseExpressionXML(pXmlFileName):
    
    global gDriverArray
    
    lXmlFile = minidom.parse(pXmlFileName)
    for iNode in lXmlFile.childNodes:
        gExpression = ""
        for iNode1 in iNode.childNodes:
            if iNode1.nodeType != iNode1.TEXT_NODE: 
                if iNode1.nodeName == "driven":
                    
                    lDriven = (iNode1.attributes["expObj"].value + "-" + iNode1.attributes["expCont"].value)
                    
                    gDrivenArray.append(lDriven)
                    
                    lExpression = iNode1.attributes["expString"].value
                    
                    for iNode2 in iNode1.childNodes:
                        if iNode2.nodeType != iNode2.TEXT_NODE: 
                            if iNode2.nodeName == "driver":

                                    lDriver = iNode2.attributes["scalarObjects"].value + "_" + iNode2.attributes["scalarConts"].value
                                    
                                    lExpression = lExpression.replace(iNode2.attributes["scalarNames"].value, (iNode2.attributes["scalarObjects"].value + "_" + iNode2.attributes["scalarConts"].value), len(lExpression))
                                    
                                    gDriverArray.append(lDriver)
                                    gExpressionArray.append(lExpression)
                                    
    gDriverArray = RS.Utils.Collections.RemoveDuplicatesFromList(gDriverArray)   
    gDriverArray = sorted(gDriverArray) 

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Add Comments To Plug-In Code
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_AddHeader(pVectorType, pInputOutput):

    gFileArray.append("\n" + 
                      "//###############################################################################################################\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//##\n" +
                      "//## Description: " + pVectorType + " " + pInputOutput + "\n" +
                      "//##\n" +
                      "//##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" + 
                      "//###############################################################################################################"
                      "\n")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Splits Array At Indices
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def partition(alist, indices):
    return [alist[i:j] for i, j in zip([0]+indices, indices+[None])]

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Finds All Indices
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def allindices(string, sub, listindex=[], offset=0):
    i = string.find(sub, offset)
    while i >= 0:
        listindex.append(i)
        i = string.find(sub, i + 1)
    return listindex

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Renames All Axis As Vector Components
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def renameAxis(pString):

    variable = ""
    if "X_Position" in pString:
        variable = pString.replace("X_Position", "XYZ_Pos[0]", len(pString))
    elif "Y_Position" in pString:
        variable = pString.replace("Y_Position", "XYZ_Pos[1]", len(pString))
    elif "Z_Position" in pString:
        variable = pString.replace("Z_Position", "XYZ_Pos[2]", len(pString))
    elif "X_Rotation" in pString:
        variable = pString.replace("X_Rotation", "XYZ_Rot[0]", len(pString))
    elif "Y_Rotation" in pString:
        variable = pString.replace("Y_Rotation", "XYZ_Rot[1]", len(pString))
    elif "Z_Rotation" in pString:
        variable = pString.replace("Z_Rotation", "XYZ_Rot[2]", len(pString))
    elif "X_Scale" in pString:
        variable = pString.replace("X_Scale", "XYZ_Scale[0]", len(pString))
    elif "Y_Scale" in pString:
        variable = pString.replace("Y_Scale", "XYZ_Scale[1]", len(pString))
    elif "Z_Scale" in pString:
        variable = pString.replace("Z_Scale", "XYZ_Scale[2]", len(pString))
    else:
        variable = pString
        
    return variable

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parses Expression In XML Convert Maxscript To C++
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
 
def rs_FacialExpressions():
    
    rs_AddHeader("Facial", "Expressions")
    
    lTempExpressionArray = []
    lAllConditionals = []
    
    ###########################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Concatinate All Expression For Each Driven Joint
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###########################################################################################################
    
    lTempDrivenArray = RS.Utils.Collections.RemoveDuplicatesFromList(gDrivenArray) 
    
    for i in range(len(lTempDrivenArray)):
        lExpression =  ""
        for j in range(len(gDrivenArray)):
            if lTempDrivenArray[i] == gDrivenArray[j]:
                if lExpression == "":
                    lExpression = gExpressionArray[j]
                else:
                    lExpression = lExpression + " + " + gExpressionArray[j]
        
        lTempExpressionArray.append(lExpression)
    
    ###########################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Seperates Concatenated Expressions
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###########################################################################################################
    
    for i in range(len(lTempDrivenArray)):
        
        lController = lTempDrivenArray[i].partition("-")
        rs_AddHeader(lController[0], lController[2])

        lSplitExpressions = lTempExpressionArray[i].split(" + ")
        lSplitExpressions.sort()
        
        lCondVarArray = []
        lCondArray = []
        lIfArray = []
        
        lIfCount = 0
        
        for iSplitExpression in lSplitExpressions:
            
            ###################################################################################################
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ##
            ## Description: Find If And Nested If Statements And Parse String
            ##
            ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
            ###################################################################################################
            
            if "if" in iSplitExpression:
                
                lNestedIf = iSplitExpression.count("if")
                
                if lNestedIf > 1:
                    
                    iSplitExpression = iSplitExpression.replace("(", "", len(iSplitExpression))
                    iSplitExpression = iSplitExpression.replace(")", "", len(iSplitExpression))
                    iSplitExpression = iSplitExpression.replace(" ", "", len(iSplitExpression))
                    
                    lIfCount = lIfCount + 1
                    
                    lIndexList = []
                    lIndexList = allindices(iSplitExpression, "if", lIndexList)
                    
                    if lIndexList[0] == 0:
                        lIndexList.remove(lIndexList[0])
                    
                    lTempPartition = partition(iSplitExpression, lIndexList)
                    lCond = ""
                    lIndent = ""
                    
                    ###########################################################################################
                    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                    ##
                    ## Description: Cycle Through Nested If Statements
                    ##
                    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                    ###########################################################################################
                    
                    for i in range(len(lTempPartition)):
                        
                        if lTempPartition[i][-1] == ",":
                            lTempPartition[i] = lTempPartition[i][0:len(lTempPartition[i]) - 1]
                        
                        lIfTemp = lTempPartition[i].split(",")
                        
                        if i == 0:
                            
                            lCond = ""
                            
                            if "<" in lIfTemp[0]:
                                lCondVar = lIfTemp[0][2:lIfTemp[0].index("<")]
                                lAllConditionals.append(lCondVar)
                                lCondVarArray.append("double " + lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))+ " = 0;")
                                lCond = lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))
                                lCondArray.append(lCond)
                            elif ">" in lIfTemp[0]:
                                lCondVar = lIfTemp[0][2:lIfTemp[0].index(">")]
                                lAllConditionals.append(lCondVar)
                                lCondVarArray.append("double " + lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar)) + " = 0;") 
                                lCond = lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))
                                lCondArray.append(lCond)                           
                            
                            lIfTemp[0] = lIfTemp[0][2:len(lIfTemp[0])]
                            lIfArray.append("\nif(" + renameAxis(lIfTemp[0]) + ")")
                            lIfArray.append("{")
                            lIfArray.append("     " + lCond + " = " + renameAxis(lIfTemp[1]) + ";")
                            lIfArray.append("}else{")
                            
                        elif i == len(lTempPartition) - 1:
                            
                            
                            lIfTemp[0] = lIfTemp[0][2:len(lIfTemp[0])]
                            lIfArray.append(lIndent + "if(" + renameAxis(lIfTemp[0]) + ")")
                            lIfArray.append(lIndent + "{")
                            lIfArray.append(lIndent + "     " + lCond + " = " + renameAxis(lIfTemp[1]) + ";")
                            lIfArray.append(lIndent + "}else{")
                            lIfArray.append(lIndent + "     " + lCond + " = " + renameAxis(lIfTemp[2]) + ";")
                            
                        else:
                            
                            lIfTemp[0] = lIfTemp[0][2:len(lIfTemp[0])]
                            lIfArray.append( lIndent + "if(" + renameAxis(lIfTemp[0]) + ")")
                            lIfArray.append( lIndent + "{")
                            lIfArray.append( lIndent + "     " + lCond + " = " + renameAxis(lIfTemp[1]) + ";")
                            lIfArray.append( lIndent + "}else{")
                            
                        lIndent = lIndent + "     "

                    for i in range(len(lTempPartition)):
                        
                        lIndent = lIndent[0:len(lIndent) - 5]
                        lIfArray.append( lIndent + "}\n")
                
                ###################################################################################################
                ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                ##
                ## Description: Find Non Nested If Statements And Parse String
                ##
                ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                ###################################################################################################
                
                else:
                    
                    lIfCount = lIfCount + 1
                    
                    iSplitExpression = iSplitExpression.replace(" ", "", len(iSplitExpression))
                    
                    lIfTemp = iSplitExpression.split(",")
                    
                    lCond = ""
                    
                    lVariable = renameAxis(lIfTemp[0])

                    if "<" in lIfTemp[0]:
                        lCondVar = lIfTemp[0][4:lIfTemp[0].index("<")]
                        
                        if "Scale" in lCondVar:
                            lScaleVar == True
                        
                        lAllConditionals.append(lCondVar)
                        lCondVarArray.append("double " + lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))+ " = 0;")
                        lCond = lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))
                        lCondArray.append(lCond)
                    elif ">" in lIfTemp[0]:
                        lCondVar = lIfTemp[0][4:lIfTemp[0].index(">")]
                        
                        if "Scale" in lCondVar:
                            lScaleVar == True
                        
                        lAllConditionals.append(lCondVar)
                        lCondVarArray.append("double " + lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar)) + " = 0;") 
                        lCond = lCondVar + "_Cond" + str(lAllConditionals.count(lCondVar))
                        lCondArray.append(lCond)                
                    
                    lIfArray.append("\n" + lVariable[1:len(lVariable)] + ")")
                    lIfArray.append("{")
                    
                    lVariable = renameAxis(lIfTemp[1])
                    
                    lIfArray.append("    " + lCond + " = " + lVariable + ";")
                    lIfArray.append("}else{")
                    
                    lVariable = renameAxis(lIfTemp[2][0:len(lIfTemp[2]) - 2])
                    
                    lIfArray.append("    " + lCond + " = " + lVariable + ";")
                    lIfArray.append("}\n")
                        
        ###################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Add Conditinal Variables and If Statements To File 
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###################################################################################################
  
        for iCond in lCondVarArray:
            gFileArray.append(iCond)
        
        for iIf in lIfArray:
            gFileArray.append(iIf)
        
        ###################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Assign To Driven Vector 
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###################################################################################################
        
        lVecorType = lController[2].partition("_")

        lExpObj = ""
        
        if "X" in lVecorType[0]:
            lExpObj = lController[0] + "_XYZ_" + lVecorType[2] + "[0]"
        elif "Y" in lVecorType[0]:
            lExpObj = lController[0] + "_XYZ_" + lVecorType[2] + "[1]"
        elif "Z" in lVecorType[0]: 
            lExpObj = lController[0] + "_XYZ_" + lVecorType[2] + "[2]"
        
        gFileArray.append(lExpObj + " = \n")

        ###################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Is This A Rotation, If So We Need To Convert From Radians To Degrees
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###################################################################################################

        lIsRotation = False
        lIsScale = False
        
        if lVecorType[2] == "Rotation":
            lIsRotation = True
            gFileArray.append("    (")
        elif lVecorType[2] == "Scale":
            lIsScale = True
            gFileArray.append("    (")
        
            
        ###################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Add All Conditionals To Driven Assignment
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###################################################################################################
        
        lScaleVar = False
        
        for i in range(len(lCondArray)):
            
            if "Scale" in lCondArray[i]:
                lScaleVar = True
            
            if (len(lSplitExpressions) - lIfCount) > 0:
                gFileArray.append("    " + lCondArray[i] + " + ")
            elif i != len(lCondArray) - 1:
                gFileArray.append("    " + lCondArray[i] + " + ")
            else:
                if lIsRotation == True:
                    gFileArray.append("    " + lCondArray[i] + ") *  57.2957795; ")
                elif lIsScale == True:
                    if lScaleVar == False:  
                        gFileArray.append("    " + lCondArray[i] + ") + 1; ")
                    else:
                        gFileArray.append("    " + lCondArray[i] + "); ")
                else:
                    gFileArray.append("    " + lCondArray[i] + "; ")
        
        ###################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ##
        ## Description: Add Remaining Expressions To Driven Assignment
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        ###################################################################################################
                    
        for i in range(len(lSplitExpressions)):
                
            if not "if" in lSplitExpressions[i]:
                
                lVariable = renameAxis(lSplitExpressions[i])
                
                if "Scale" in lVariable:
                    lScaleVar = True
                
                if i != (len(lSplitExpressions) - lIfCount) - 1:
                    gFileArray.append("    " + lVariable + " + ")
                else:
                    if lIsRotation == True:
                        gFileArray.append("    " + lVariable + ") *  57.2957795; ")
                    elif lIsScale == True:  
                        if lScaleVar == False:  
                            gFileArray.append("    " + lVariable + ") + 1; ")
                        else:
                            gFileArray.append("    " + lVariable + "); ")
                    else:
                        gFileArray.append("    " + lVariable + ";")


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parse Transforms From Arrays Create Input / Output Nodes In Plug-In
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseVectors(pArray, pVectorType, pInputOutput):
    
    global gFileArray
    global gIter
    
    lDuplicateArray = []
    lInputVector3d = []
    
    lAnimationNodeType = None
    lPartition = None
    lVectorType = None
    lUnderScore = ""
    
    if pInputOutput == "Input":
        lPartition = "_"
        lUnderScore = "_"
        lAnimationNodeType = "AnimationNodeInCreate"
        if pVectorType == "Position":
            lVectorType = "Pos"
        elif pVectorType == "Rotation":
            lVectorType = "Rot"
        elif pVectorType == "Scale":
            lVectorType = "Scale"
    else:
        lPartition = "-"
        lUnderScore = ""
        lAnimationNodeType = "AnimationNodeOutCreate"
        if pVectorType == "Position":
            lVectorType = "_Position"
        elif pVectorType == "Rotation":
            lVectorType = "_Rotation"
        elif pVectorType == "Scale":
            lVectorType = "_Scale"
        
    rs_AddHeader(pVectorType, pInputOutput)
    
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    for i in range(len(pArray)):
        if "_X_" in pArray[i]:
            if not pArray[i].rpartition("_X_")[0] in lDuplicateArray:
                lDuplicateArray.append(pArray[i].rpartition("_X_")[0])
        elif "_Y_" in pArray[i]:
            if not pArray[i].rpartition("_Y_")[0] in lDuplicateArray:
                lDuplicateArray.append(pArray[i].rpartition("_Y_")[0])
        elif "_Z_" in pArray[i]:
            if not pArray[i].rpartition("_Z_")[0] in lDuplicateArray:
                lDuplicateArray.append(pArray[i].rpartition("_Z_")[0])
        elif lPartition in pArray[i]:
            if not pArray[i].rpartition(lPartition)[0] in lDuplicateArray:
                lDuplicateArray.append(pArray[i].rpartition(lPartition)[0])
       
    for iDuplicate in lDuplicateArray:  
        lTemporaryArray = []
        for iElement in pArray:
            if iElement.startswith(iDuplicate + "-"):
                
                lTemporaryArray.append(iElement.rpartition(lPartition)[2])
        
        gIter = gIter + 1
            
        if pInputOutput == "Input":
            gInputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        else:
            gOutputVector3d.append(iDuplicate + '_XYZ' + lUnderScore + lVectorType)
        
        lCode = '    m' + iDuplicate + '_XYZ' + lUnderScore + lVectorType
        for i in range(((lInputLength) - len(iDuplicate))):
            lCode = lCode + " "
        lCode = lCode + ' = ' + lAnimationNodeType + ' ( ' + str(gIter) + ', "' + iDuplicate + '_XYZ' + lUnderScore + lVectorType + '", ANIMATIONNODE_TYPE_VECTOR );'
        gFileArray.append(lCode)        

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Parse Custom Input / Outut Nodes
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseCustom(pArray, pVectorType, pInputOutput):
    
    global gIter
    
    rs_AddHeader(pVectorType, pInputOutput)
    lInputLength = rs_FindLongestNameInArray(pArray)
    
    lAnimationNodeType = None
    if pInputOutput == "Input":
        lAnimationNodeType = "AnimationNodeInCreate"
    else:
        lAnimationNodeType = "AnimationNodeOutCreate"
    
    for iElement in pArray:
        
        gIter = gIter + 1
        
        if pInputOutput == "Input":
            gInputDoubleArray.append(iElement)
        else:
            gOutputDoubleArray.append(iElement)
        
        lCode = "    m" + iElement 
        for i in range(((lInputLength + 9) - len(iElement))):
            lCode = lCode + " "
        lCode = lCode + ' = ' + lAnimationNodeType + ' ( ' + str(gIter) + ', "' + iElement + '", ANIMATIONNODE_TYPE_NUMBER );'
        gFileArray.append(lCode)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Create lStatus To Read Inputs For Plug-In
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_ParseVariables():
    gFileArray.append("\n" + 
                      "int lStatus[" + str(len(gInputVector3d) + len(gInputDoubleArray)) + "];\n" +
                      "\n")
    for i in range(len(gInputVector3d)):
        if i == 0:
            gFileArray.append("FBVector3d " + gInputVector3d[i] + ",")
        else:
           gFileArray.append("           " + gInputVector3d[i] + ",")
    for i in range(len(gOutputVector3d)):
           gFileArray.append("           " + gOutputVector3d[i] + ",")
    gFileArray.append(";\n")
    for i in range(len(gInputDoubleArray)):
        if i == 0:
            gFileArray.append("double " + gInputDoubleArray[i] + ",")
        else:
            gFileArray.append("       " + gInputDoubleArray[i] + ",")
    for i in range(len(gOutputDoubleArray)):
            gFileArray.append("       " + gOutputDoubleArray[i] + ",")
    gFileArray.append(";\n")
    gFileArray.append("\n")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Create lStatus To Read Inputs For Plug-In
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_lStatus():
    
    lTotal = 0
    
    for i in range(len(gInputVector3d)):
        
        gFileArray.append("lStatus[" + str(i) + "] = m" + gInputVector3d[i] + " -> ReadData( " + gInputVector3d[i] + ", pEvaluateInfo );")
        lTotal = i
        
    for i in range(len(gInputDoubleArray)):
        
        gFileArray.append("lStatus[" + str(i + (lTotal + 1)) + "] = m" + gInputDoubleArray[i] + " -> ReadData( &" + gInputDoubleArray[i] + ", pEvaluateInfo );")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Create Data Output For Plug-In
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_FacialExpressionsOutput():
    
    rs_AddHeader("Write", "Data")
    
    lDuplicateArray = []
    
    ltempArray = RS.Utils.Collections.RemoveDuplicatesFromList(gDrivenArray) 
    
    for i in range(len(ltempArray)):
        if not (ltempArray[i].rpartition("-")[0] + " " + ltempArray[i].rpartition("_")[2]) in lDuplicateArray:
            lDuplicateArray.append(ltempArray[i].rpartition("-")[0] + " " + ltempArray[i].rpartition("_")[2])
    
    for iDuplicate in lDuplicateArray:  
        for iElement in gDrivenArray:
            if iElement.startswith(iDuplicate.rpartition(" ")[0] + "-"):
                expObj = iDuplicate.rpartition(" ")[0] + "_XYZ_" + iDuplicate.rpartition(" ")[2]
            
        gFileArray.append("m" + expObj + " -> WriteData( &" + expObj + "[0], pEvaluateInfo );")

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Sort Input Types Into Position, Rotation, Scale and Custom
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_SortInputsType(pArray, pInputOutput):
    
    lPosArray = []
    lRotArray = []
    lScaleArray = []
    lCustomArray = []
    
    for iInput in pArray:
        if "Pos" in iInput:
            lPosArray.append(iInput)
        elif "Rot" in iInput:
            lRotArray.append(iInput)
        elif "Sca" in iInput:
            lScaleArray.append(iInput)
        else:
            lCustomArray.append(iInput)
            
    rs_ParseVectors(lPosArray, "Position", pInputOutput)
    
    rs_ParseVectors(lRotArray, "Rotation", pInputOutput)
    
    rs_ParseVectors(lScaleArray, "Scale", pInputOutput)
    
    lCustomArray = RS.Utils.Collections.RemoveDuplicatesFromList(lCustomArray)
    
    rs_ParseCustom(lCustomArray, "Custom", pInputOutput)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description:
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def rs_BoxCxxStart(pNamespace):

    lHeader = '''/*
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## File Name: orbox_''' + pNamespace.upper() + '''_box.cxx
## Written And Maintained By: David Bailey
## Contributors: -
## Description: This Is The Code File For ''' + pNamespace.upper() + ''''s 3Lateral Face Plug-in
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
*/

/* file    orbox_''' + pNamespace.upper() + '''_box.cxx
*/

//--- Class declaration
#include "orbox_''' + pNamespace.lower() + '''_box.h"

//--- Registration defines
#define ORBOX''' + pNamespace.upper() + '''__CLASS    ORBOX''' + pNamespace.upper() + '''__CLASSNAME
#define ORBOX''' + pNamespace.upper() + '''__NAME        ORBOX''' + pNamespace.upper() + '''__CLASSSTR
#define ORBOX''' + pNamespace.upper() + '''__LOCATION    "GTAV"
#define ORBOX''' + pNamespace.upper() + '''__LABEL    "''' + pNamespace.capitalize() + ''' 3Lateral"
#define ORBOX''' + pNamespace.upper() + '''__DESC        "Relation Box For ''' + pNamespace.capitalize() + ''' 3Lateral Face Rig"

//--- implementation and registration
FBBoxImplementation    (    ORBOX''' + pNamespace.upper() + '''__CLASS        );    // Box class name
FBRegisterBox    (    ORBOX''' + pNamespace.upper() + '''__NAME,            // Unique name to register box.
                              ORBOX''' + pNamespace.upper() + '''__CLASS,            // Box class name
                              ORBOX''' + pNamespace.upper() + '''__LOCATION,                  // Box location ('plugins')
                              ORBOX''' + pNamespace.upper() + '''__LABEL,            // Box label (name of box to display)
                        ORBOX''' + pNamespace.upper() + '''__DESC,            // Box long description.
                    FB_DEFAULT_SDK_ICON            );    // Icon filename (default=Open Reality icon)

/************************************************
 *    Creation
 ************************************************/
bool ORBox_''' + pNamespace.capitalize() + '''::FBCreate()
{
    /*
    *    Create the nodes for the box.
    */'''
         

    gFileArray.append(lHeader)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description:
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run():
    global gProjRoot
    global gFileName
    global gFileArray
    global gDrivenArray
    global gDriverArray
    global gExpressionArray
    global gInputVector3d
    global gOutputVector3d
    global gOutputDoubleArray
    global gInputDoubleArray
    global gZeroValueArray
    global gIter
    
    gProjRoot = RS.Config.Project.Path.Root
    
    lFilePopup = FBFilePopup();
    lFilePopup.Filter = '*.xml'
    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
    lFilePopup.Path = gProjRoot + "\\art"
    
    gFileName = ""
    
    if lFilePopup.Execute():
        gFileName = lFilePopup.FullFilename
    
    gFileArray = []
    gDrivenArray = []
    gDriverArray = []
    gExpressionArray = []
    gInputVector3d = []
    gOutputVector3d = []
    gOutputDoubleArray = []
    gInputDoubleArray = []
    gZeroValueArray = []
    gIter = 0    

    rs_ParseExpressionXML(gFileName)
    
    lResult = FBMessageBoxGetUserValue( "Enter Namespace", "Namespace: ", "", FBPopupInputType.kFBPopupString, "Ok" )
    
    rs_BoxCxxStart(lResult[1])
    rs_SortInputsType(gDriverArray, "Input")
    rs_SortInputsType(gDrivenArray, "Output")
    rs_ParseVariables()
    rs_lStatus()
    rs_FacialExpressions()
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Fix Implicit Scale In Expressions
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    global gScaleArray
    global gCompareScaleArray
    
    gScaleArray = []
    gCompareScaleArray = []
    
    rs_AddHeader("Implicit", "Scale")
    
    for line in gFileArray:
        if "_XYZ_Scale" in line and "] " in line:
            gScaleArray.append(line)
            gCompareScaleArray.append(line.partition("_XYZ_")[0])
    
    gCompareScaleArray = RS.Utils.Collections.RemoveDuplicatesFromList(gCompareScaleArray)
           
    for iCompare in gCompareScaleArray:
        lScales = []
        
        for iScale in gScaleArray:
            if iScale.startswith(iCompare):
                lScales.append(iScale) 
        
        if len(lScales) == 1:
            lNumber = int(lScales[0].partition("[")[2][0])
            if lNumber == 0:
                
                gFileArray.append("//\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[1] = 1;\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[2] = 1;\n")
                gFileArray.append("//\n")
                
            if lNumber == 1:
                
                gFileArray.append("//\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[0] = 1;\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[2] = 1;\n")
                gFileArray.append("//\n")
                
            if lNumber == 2:
                
                gFileArray.append("//\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[0] = 1;\n")
                gFileArray.append(lScales[0].partition("[")[0] + "[1] = 1;\n")
                gFileArray.append("//\n")
                
        if len(lScales) == 2:
            
            lNumbers = ""
            
            for iScale in lScales:
                lNumbers = lNumbers + iScale.partition("[")[2][0] + " "
                
            if "0" in lNumbers and "1" in lNumbers:
                gFileArray.append(lScales[0].partition("[")[0] + "[2] = 1;\n")
            elif "0" in lNumbers and "2" in lNumbers:
                gFileArray.append(lScales[0].partition("[")[0] + "[1] = 1;\n")
            elif "1" in lNumbers and "2" in lNumbers:
                gFileArray.append(lScales[0].partition("[")[0] + "[0] = 1;\n")
            
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Fix Implicit Scale In Expressions
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    rs_FacialExpressionsOutput()
    for line in gFileArray:
        print line

                
            
            
