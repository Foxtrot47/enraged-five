from pyfbsdk import *
import RS.Globals as glo

def Run():
    FBPlayerControl().GotoStart()  
    lStartFrame = int(FBSystem().LocalTime.GetTimeString())
    FBPlayerControl().GotoEnd()  
    lEndFrame = int(FBSystem().LocalTime.GetTimeString())
    
    lFrames = lEndFrame - lStartFrame
    
    lOutputsArray = ["mouth_CTRL",
                     "jaw_CTRL",
                     "tongueRoll_CTRL",
                     "tongueInOut_CTRL",
                     "eye_C_CTRL",
                     "lowerLip_L_CTRL",
                     "lowerLip_C_CTRL",
                     "lowerLip_R_CTRL",
                     "lipCorner_R_CTRL",
                     "upperLip_C_CTRL",
                     "upperLip_L_CTRL",
                     "lipCorner_L_CTRL",
                     "upperLip_R_CTRL"]
                     
    for iOutput in lOutputsArray:
        
        lOutput = FBFindModelByLabelName(iOutput)
        
        if lOutput:
            
            for iProp in lOutput.PropertyList:
                
                if iProp.IsUserProperty():
                    
                    if "CA" in iProp.Name:
                    
                        lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0])
                        
                        if not lCASlider:
                            
                            if iOutput == "lipCorner_R_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_A")
                                
                            elif iOutput == "upperLip_R_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_B")
                                
                            elif iOutput == "upperLip_C_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_C")
                                
                            elif iOutput == "upperLip_L_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_D")
                                
                            elif iOutput == "lipCorner_L_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_E")
                                
                            elif iOutput == "lowerLip_L_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_F")
                                
                            elif iOutput == "lowerLip_C_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_G")
                                
                            elif iOutput == "lowerLip_R_CTRL":
                                
                                lCASlider = FBFindModelByLabelName("CIRC_" + iProp.Name.rpartition("_")[0] + "_H")
                            
                        if lCASlider:
                            
                            for i in range(lFrames):
    
                                lValue = lOutput.PropertyList.Find(iProp.Name).GetAnimationNode().FCurve.Evaluate(FBTime(0,0,0,(i + lStartFrame)))
                                
                                if lCASlider.PropertyList.Find("Lcl Translation").GetAnimationNode():
                                    
                                    lCASlider.PropertyList.Find("Lcl Translation").GetAnimationNode().Nodes[0].KeyAdd(FBTime(0,0,0,i), lValue)
                                    
                                else:
                                    
                                    print lCASlider.Name