from pyfbsdk import *

import os
import xml.etree.cElementTree

import RS.Perforce
import RS.Config
import RS.Utils.Path


FIX_GUI_DIR = "{0}\\tools\\techart\\etc\\config\\characters\\face\\facebonedata".format( RS.Config.Project.Path.Root )


def getTabs( tabLevel ):
    tabStr = ''
    
    for i in range( 0, tabLevel ):
        tabStr += '\t'
        
    return tabStr

def recurceExportMinMax( currentCtrl, fstream, tabLevel ):
    '''
    Description:
        Recursively write out the Min/Max Translations for each ctrl in the hierarchy to the xml file stream.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        Thanh Phan <thanh.phan@rockstarsandiego.com>
    '''
    
    for child in currentCtrl.Children:
    
        # Gather Contoller Limit Data
        minX, minY, minZ = child.PropertyList.Find('TranslationMin')
        maxX, maxY, maxZ = child.PropertyList.Find('TranslationMax')
        
        # Gather Controller Limit On/Off
        tMinX = child.PropertyList.Find('TranslationMinX').Data
        tMinY = child.PropertyList.Find('TranslationMinY').Data
        tMinZ = child.PropertyList.Find('TranslationMinZ').Data
        tMaxX = child.PropertyList.Find('TranslationMaxX').Data
        tMaxY = child.PropertyList.Find('TranslationMaxY').Data
        tMaxZ = child.PropertyList.Find('TranslationMaxZ').Data
        
        # Gather Controller Limit On/Off
        tActive = child.PropertyList.Find('TranslationActive').Data
        
        # Gather Controller Position Data
        posX, posY, posZ = child.Translation.Data
        rotX, rotY, rotZ = child.Rotation.Data
        scaleX, scaleY, scaleZ = child.Scaling.Data
        
        # Gather PreRotation Data
        preRotX, preRotY, preRotZ = child.PreRotation
        
        # Write the Data
        fstream.write( '{0}<ctrl name="{1}" translationMin="{2} {3} {4}" translationMax="{5} {6} {7}" tMinX="{8}" tMinY="{9}" tMinZ="{10}" tMaxX="{11}" tMaxY="{12}" tMaxZ="{13}" tActive="{14}" posX="{15}" posY="{16}" posZ="{17}" rotX="{18}" rotY="{19}" rotZ="{20}" scaleX="{21}" scaleY="{22}" scaleZ="{23}" preRotX="{24}" preRotY="{25}" preRotZ="{26}" />\n'.format( getTabs( tabLevel ), child.Name, minX, minY, minZ, maxX, maxY, maxZ, tMinX, tMinY, tMinZ, tMaxX, tMaxY, tMaxZ, tActive, posX, posY, posZ, rotX, rotY, rotZ, scaleX, scaleY, scaleZ, preRotX, preRotY, preRotZ ) )
        recurceExportMinMax( child, fstream, tabLevel )
        
        
def exportMinMax( fbxFilename, quiet = False, rootCtrls = ('faceControls_OFF', 'FacialAttrGUI', 'FACIAL_facialRoot' ) ):
    '''
    Description:
        Exports Min/Max Translations for the skeleton hierarchy under the supplied root ctrls to an xml file.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        Thanh Phan <thanh.phan@rockstarsandiego.com>
    '''
    global FIX_GUI_DIR
    
    if rootCtrls == None:
        if not quiet:
            FBMessageBox( 'Rockstar', 'A root ctrl name must be supplied in order to export the pre-rotations!', 'OK' )
        return False
    
    # Didn't supply a fbx filename, so use the current scene.
    if fbxFilename == None:
        fbxFilename = FBApplication().FBXFileName
        
    # Supplied a fbx filename, so open that scene.
    else:
        fileinfo = RS.Perforce.Sync( fbxFilename )
        
        if not os.path.isfile( fbxFilename ):
            fbxFilename = None
            
    if fbxFilename:
        FBApplication().FileOpen( fbxFilename )
            
        # Find the pre-rotation file for the fbx filename supplied.
        filename = str( os.path.basename( fbxFilename ) ).split( '.' )[ 0 ]
    
        if not os.path.isdir( FIX_GUI_DIR ):
            os.mkdir( FIX_GUI_DIR )
        
        fixGUIFile = '{0}\\{1}.facebone'.format( FIX_GUI_DIR, filename )
        
        p4Check = False
        if not RS.Perforce.DoesFileExist( fixGUIFile ):
            RS.Perforce.Add( fixGUIFile )
            p4Check = True
        elif ( RS.Perforce.Edit( fixGUIFile ) ):
            p4Check = True
        else:
            pass
            
        if p4Check :
            fstream = open( fixGUIFile, 'w' )
            fstream.write( '<?xml version="1.0" encoding="utf-8"?>\n' )
            fstream.write( '<ctrls>\n' )
        
            for rootCtrl in rootCtrls : 
                therootCtrl = FBFindModelByLabelName( rootCtrl )
                
                if rootCtrl != 'facialRoot_C_OFF':
                    if therootCtrl:
                        recurceExportMinMax( therootCtrl, fstream, 1 )
                    elif quiet :
                        pass
                    else:
                        if not quiet:
                            result = FBMessageBox( 'Rockstar', 'Could not find the supplied root ctrl name {0}!  Cannot export pre-rotations for:\n\n{1}\n\nMaybe this file does not have a facial rig setup?'.format( rootCtrl, fbxFilename ), 'Continue', 'Cancel' )
                else:
                    print "Skipping facialRoot_C_OFF"
                    
            fstream.write( '</ctrls>\n' )
            fstream.close()
                    
        else:
            if not quiet:
                FBMessageBox( 'Rockstar', 'Could not export FaceBone Translations because the file cannot be checked out!\n\n{0}'.format( fbxFilename ), 'OK' )
            
                      
          
    else:
        if not quiet:
            FBMessageBox( 'Rockstar', 'Cannot export FaceBone Translations because the file does not exist.\n\n{0}'.format( fbxFilename ), 'OK' )
        
        
def applyMinMax( charNamespace, quiet = False ):
    '''
    Description:
        Imports and applies the Min/Max Translations to facial ctrls who match the supplied character namespace.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
        Thanh Phan <thanh.phan@rockstarsandiego.com>
    '''
    
    global FIX_GUI_DIR
    
    fixGUIFile = '{0}\\{1}.facebone'.format( FIX_GUI_DIR, charNamespace )

    if not RS.Perforce.DoesFileExist( fixGUIFile ):
        if not quiet:
            FBMessageBox( 'Rockstar', 'There is no FaceBone xml file for character ({0})!  Please export one.\n\nAborting FaceBone Translation application for this character.'.format( charNamespace ), 'OK' )
        
    else:
        RS.Perforce.Sync( fixGUIFile )
        
        # If file not in perforce this will crash
        if not os.path.exists( fixGUIFile ):
            return
        
        
        doc = xml.etree.cElementTree.parse( fixGUIFile )
        root = doc.getroot()
        
        if root:
            ctrls = root.findall( 'ctrl' )
            
            for ctrl in ctrls:
                ctrlName = ctrl.get( 'name' )
                if ctrlName != "facialRoot_C_OFF" :
                    ttMinX = ctrl.get( 'tMinX' )
                    ttMinY = ctrl.get( 'tMinY' )
                    ttMinZ = ctrl.get( 'tMinZ' )
                    ttMaxX = ctrl.get( 'tMaxX' )
                    ttMaxY = ctrl.get( 'tMaxY' )
                    ttMaxZ = ctrl.get( 'tMaxZ' )
                    ttActive = ctrl.get( 'tActive' )
                    minX, minY, minZ = str( ctrl.get( 'translationMin' ) ).split( ' ' )
                    maxX, maxY, maxZ = str( ctrl.get( 'translationMax' ) ).split( ' ' )
                    minTrans = FBVector3d( float( minX ), float( minY ), float( minZ ) )
                    maxTrans = FBVector3d( float( maxX ), float( maxY ), float( maxZ ) )
                    
                    posX = ctrl.get( 'posX' )
                    posY = ctrl.get( 'posY' )
                    posZ = ctrl.get( 'posZ' )
                    rotX = ctrl.get( 'rotX' )
                    rotY = ctrl.get( 'rotY' )
                    rotZ = ctrl.get( 'rotZ' )
                    scaleX = ctrl.get( 'scaleX' )
                    scaleY = ctrl.get( 'scaleY' )
                    scaleZ = ctrl.get( 'scaleZ' )
                    preRotX = ctrl.get( 'preRotX' )
                    preRotY = ctrl.get( 'preRotY' )
                    preRotZ = ctrl.get( 'preRotZ' )
                    thePos = FBVector3d( float( posX ), float( posY ), float( posZ ) )
                    theRot = FBVector3d( float( rotX ), float( rotY ), float( rotZ ) )
                    theScale = FBVector3d( float( scaleX ), float( scaleY ), float( scaleZ ) )
                    thePreRot = FBVector3d( float( preRotX ), float( preRotY ), float( preRotZ ) )
                    
                    sceneCtrl = FBFindModelByLabelName( '{0}:{1}'.format( charNamespace, ctrlName ) )
                    
                    if sceneCtrl:
                        #lclRotation = sceneCtrl.PropertyList.Find( 'Lcl Rotation' )
                        #rotationActive = sceneCtrl.PropertyList.Find( 'RotationActive' )
                        tMinX = sceneCtrl.PropertyList.Find( 'TranslationMinX' )
                        tMinY = sceneCtrl.PropertyList.Find( 'TranslationMinY' )
                        tMinZ = sceneCtrl.PropertyList.Find( 'TranslationMinZ' )
                        tMaxX = sceneCtrl.PropertyList.Find( 'TranslationMinX' )
                        tMaxY = sceneCtrl.PropertyList.Find( 'TranslationMinY' )
                        tMaxZ = sceneCtrl.PropertyList.Find( 'TranslationMinZ' )
                        tActive = sceneCtrl.PropertyList.Find( 'TranslationActive' )
                        minTranslation = sceneCtrl.PropertyList.Find( 'TranslationMin' )
                        maxTranslation = sceneCtrl.PropertyList.Find( 'TranslationMax' )
                        
                        if ("FACIAL_" in sceneCtrl.Name) or ("_OFF" in sceneCtrl.Name) :
                            print sceneCtrl.Name
                            if sceneCtrl.Translation.GetAnimationNode():
                                animNodes = sceneCtrl.Translation.GetAnimationNode().Nodes
                                for axisId in range( 0, len( animNodes ) ):
                                    animNodes[ axisId ].FCurve.EditClear()
                                    
                            if sceneCtrl.Rotation.GetAnimationNode():
                                animNodes = sceneCtrl.Rotation.GetAnimationNode().Nodes
                                for axisId in range( 0, len( animNodes ) ):
                                    animNodes[ axisId ].FCurve.EditClear()
                                    
                            if sceneCtrl.Scaling.GetAnimationNode():
                                animNodes = sceneCtrl.Scaling.GetAnimationNode().Nodes
                                for axisId in range( 0, len( animNodes ) ):
                                    animNodes[ axisId ].FCurve.EditClear()
                                    
                            sceneCtrl.Translation.Data = thePos
                            sceneCtrl.Rotation.Data = theRot
                            sceneCtrl.Scaling.Data = theScale
                            sceneCtrl.PreRotation.Data = thePreRot
                            
                        elif tMinX and tMaxX and minTranslation and maxTranslation:
                            
                            def str2bool(v):
                                return v.lower() in ("yes", "true", "t", "1")
                            tMinX.Data =  str2bool(ttMinX)
                            tMinY.Data =  str2bool(ttMinY)
                            tMinZ.Data =  str2bool(ttMinZ)
                            tMaxX.Data =  str2bool(ttMaxX)
                            tMaxY.Data =  str2bool(ttMaxY)
                            tMaxZ.Data =  str2bool(ttMaxZ)
                            tActive.Data = str2bool(ttActive)
                            minTranslation.Data = minTrans
                            maxTranslation.Data = maxTrans
                            
                            
                        else:
                            print 'An unknown error occurred while attempting to query the pre-rotation properties for {0}!'.format( sceneCtrl.Name )
                        
                    else:
                        print 'Could not apply the Translations to ctrl ({0}:{1}) because it could not be found!'.format( charNamespace, ctrlName )
                else:
                    print 'skipping facialRoot_C_OFF'
                    

                
def batchexportMinMax( fileList, ctrlName = 'FACIAL_facialRoot' ):
    '''
    Description:
        Batch export pre-rotations for the fbx filenames supplied.
        
    Author:
        Jason Hayes <jason.hayes@rockstarsandiego.com>
    '''
    
    RS.Perforce.Sync( fileList )
    
    result = None
    idx = 0
    
    while result == None:
        if idx <= ( len( fileList ) - 1 ):
            fbxFilename = fileList[ idx ]
            result = exportMinMax( fbxFilename, True )
            idx += 1
            
        else:
            result = False
            
#fixGUIFileList = ( '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\CS_AbigailRoberts.fbx'.format( RS.Config.Project.Path.Root ),
#                    '{0}\\art\\animation\\resources\\characters\\Models\\cutscene\\CS_CharlesSmith.fbx'.format( RS.Config.Project.Path.Root ) )
#
#faceBoneFolderName_Animal = '{0}\\art\\animation\\resources\\characters\\models\\Animals\\'.format( RS.Config.Project.Path.Root )
#faceBoneFolderName_CS = '{0}\\art\\animation\\resources\\characters\\models\\cutscene\\'.format( RS.Config.Project.Path.Root )
#faceBoneFolderName_IG = '{0}\\art\\animation\\resources\\characters\\models\\ingame\\'.format( RS.Config.Project.Path.Root )
#
#faceBoneFiles = RS.Utils.Path.Walk(faceBoneFolderName_CS, pRecurse = 1, pPattern = '*.fbx', pReturn_folders = 0 )