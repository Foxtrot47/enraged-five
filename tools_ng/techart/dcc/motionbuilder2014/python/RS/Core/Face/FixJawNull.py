from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils.Scene


def rs_FindCharacterName(tNS):
    
    if "townley" in tNS:
        Name = tNS.partition("townley")
        tNS = Name[0] + Name[2] 
    if "Dr" in tNS:
        tNS = tNS.replace("Dr","Dr_")
    if "Lester" in tNS:
        tNS = "Lester"
    if "Zero" in tNS:
        tNS = "Michael" 
    if "Tracy" in tNS:
        tNS = "Tracy"
    if "Two" in tNS:
        tNS = "OR - Box Trevor"
    if "One" in tNS:
        tNS = "OR - Box Franklin"
        
    return tNS


def rs_fixJawNull(tNamespace) :
    if tNamespace :
        
        tNS = rs_FindCharacterName(tNamespace)
        if "OR -" in tNS :
            None
        else :
            tNS = tNS + " 3Lateral"
        searchName = tNamespace + ":" + tNS
        
        jawCtrl = FBFindModelByLabelName(tNamespace+":FACIAL_jaw")
        if jawCtrl:
            jawCtrlParent = jawCtrl.Parent
        
            #
            # If Jaw Control parent is the facial root, create the parent.
            #
            
            
            lFacialRoot = FBFindModelByLabelName(tNamespace + ":FACIAL_facialRoot")
            if lFacialRoot:
                deleteMe = []
                nullNames = [ "facial_jaw_null", "null" ]
                for child in lFacialRoot.Children :
                    nullCheckName = ((child.Name.lower()).split(" ",1))[0]
                    if nullCheckName in nullNames :
                        if child != jawCtrlParent :
                            deleteMe.append(child)
                        
                for each in deleteMe : each.FBDelete()
                jawCtrlParent.Name = "FACIAL_jaw_Null"
            
            lRelConstraintGroup = []
            allConstraints = FBSystem().Scene.Constraints
            for constraint in allConstraints:
                if constraint.LongName.startswith(tNamespace + ":") and constraint.Name.endswith(" 3Lateral") and ("Ambient" not in constraint.Name) :
                    lRelConstraintGroup.append(constraint)
                elif (constraint.Name in ["OR - Box Trevor", "OR - Box Franklin"]) and (constraint.LongName.startswith(tNamespace + ":")) :
                    lRelConstraintGroup.append(constraint)
            
            lRelConstraint = None
            if len(lRelConstraintGroup) == 1 :
                lRelConstraint = lRelConstraintGroup[0]
            elif len(lRelConstraintGroup) == 0 :
                lRelConstraint == None
                FBMessageBox( "Failed", "Failed, no Constraints were found.\nPlease update the character.", "OK" )
            else :
                lRelConstraint == None
                FBMessageBox( "Failed", "Failed, too many Constraints were found.\nPlease update the character and/or contact a TA", "OK" )
            
            if lRelConstraint != None:
                
                deleteMe = []
                for controllerFound in lRelConstraint.Boxes:
                    if "FACIAL_jaw_Null" in controllerFound.LongName :
                        deleteMe.append(controllerFound)
                for each in deleteMe :
                    lRelConstraint.Boxes.remove(each)
            
                if jawCtrlParent.Translation.GetAnimationNode():
                    animNodes = jawCtrlParent.Translation.GetAnimationNode().Nodes
                    for axisId in range( 0, len( animNodes ) ):
                        animNodes[ axisId ].FCurve.EditClear()
                jawCtrlParent.Translation.Data = FBVector3d(0,0,0)
                
                if jawCtrlParent.Rotation.GetAnimationNode():
                    animNodes = jawCtrlParent.Rotation.GetAnimationNode().Nodes
                    for axisId in range( 0, len( animNodes ) ):
                        animNodes[ axisId ].FCurve.EditClear()
                jawCtrlParent.Rotation.Data = FBVector3d(0,0,0)
                
                if jawCtrlParent.Scaling.GetAnimationNode():
                    animNodes = jawCtrlParent.Scaling.GetAnimationNode().Nodes
                    for axisId in range( 0, len( animNodes ) ):
                        animNodes[ axisId ].FCurve.EditClear()
                jawCtrlParent.Scaling.Data = FBVector3d(1,1,1)
                
                for iBox in lRelConstraint.Boxes:
                    iBoxName = iBox.Name.lower()
                    relConstraintName = lRelConstraint.Name.lower()
                    if iBoxName.startswith(relConstraintName):
                        
                        lConstrainedObject = lRelConstraint.ConstrainObject(jawCtrlParent)
                        lConstrainedObject.UseGlobalTransforms = False
                        lRelConstraint.SetBoxPosition(lConstrainedObject, -1200, 0)
                        
                        lSourceObject = lRelConstraint.SetAsSource(jawCtrlParent)
                        lSourceObject.UseGlobalTransforms = False
                        lRelConstraint.SetBoxPosition(lSourceObject, -1900, 0)
                        
                        lAddBox = lRelConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                        lRelConstraint.SetBoxPosition(lAddBox, -1550, 50)
                        
                        lAddBox1 = lRelConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                        lRelConstraint.SetBoxPosition(lAddBox1, -1550, -50)
                        
                        # RS.Utils.Scene.ConnectBox(pNodeOut,pNodeOutPos, pNodeIn, pNodeInPos)
                        RS.Utils.Scene.ConnectBox(lSourceObject, 'Lcl Rotation', lAddBox, 'V2')
                        RS.Utils.Scene.ConnectBox(lSourceObject, 'Lcl Translation', lAddBox1, 'V2')
                        
                        RS.Utils.Scene.ConnectBox(iBox, 'FACIAL_jaw_XYZ_Rotation', lAddBox, 'V1')
                        RS.Utils.Scene.ConnectBox(iBox, 'FACIAL_jaw_XYZ_Position', lAddBox1, 'V1')
                        
                        RS.Utils.Scene.ConnectBox(lAddBox, 'Result', lConstrainedObject, 'Lcl Rotation')
                        RS.Utils.Scene.ConnectBox(lAddBox1, 'Result', lConstrainedObject, 'Lcl Translation')
                        
            FBMessageBox( "Success", "Fixed.", "OK" )
            
        else:
            FBMessageBox( "Rig not compatible", "This rig is not compatible.  Fix not applied", "OK" )
    else:
        FBMessageBox( "?", "No namespace supplied???", "OK" )
