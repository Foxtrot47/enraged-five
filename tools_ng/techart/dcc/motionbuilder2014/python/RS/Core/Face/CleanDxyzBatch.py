from pyfbsdk import *

import os.path
import RS.Core.Face.Lib
import RS.Core.Face.XMLImporter


def detectEndFrame(ctrl):
    ctrlAnimNode = ctrl.Translation.GetAnimationNode()
    ctrlKeys = []
    if ctrlAnimNode:
        ctrlKeys = ctrlAnimNode.Nodes[1].FCurve.Keys
    if len(ctrlKeys) > 0:
        lastFrame = ctrlKeys[ len(ctrlKeys) - 1 ].Time.GetFrame()
        return lastFrame
    return 0
    
def setEndFrame():
    
    endFrame = 0
    cl = FBComponentList()
    FBFindObjectsByName( "CTRL_*", cl, False, True )
    for ctrl in cl:
        endFrame = max(endFrame, detectEndFrame(ctrl))
    
    if endFrame > 0:
        FBSystem().CurrentTake.LocalTimeSpan = FBTimeSpan( FBTime(0), FBTime( 0,0,0,endFrame ) )

def setOffset():
    cl = FBComponentList()
    FBFindObjectsByName( "CTRL_*", cl, False, True )
    
    firstFrame = 0
    for ctrl in cl:
        ctrlAnimNode = ctrl.Translation.GetAnimationNode()
        if ctrlAnimNode:
            for ctrlAxis in ctrlAnimNode.Nodes:
                for ctrlKey in ctrlAxis.FCurve.Keys:
                    firstFrame = min( firstFrame, ctrlKey.Time.GetFrame() )
                    
    if not firstFrame < 0:
        for ctrl in cl:
            ctrlAnimNode = ctrl.Translation.GetAnimationNode()
            if ctrlAnimNode:
                for ctrlAxis in ctrlAnimNode.Nodes:
                    for ctrlKey in ctrlAxis.FCurve.Keys:
                        ctrlKey.Time = FBTime(0,0,0,(ctrlKey.Time.GetFrame() - 10))

def cleanTakes():
    
    deleteMe = []
    deleteTheseTakes = [
        "MaxFigPose",
        "MBStancePose",
        "Take 001"
    ]
    for take in FBSystem().Scene.Takes:
        FBSystem().CurrentTake = take
        setOffset()
        setEndFrame()
        
        if take.Name in deleteTheseTakes:
            deleteMe.append( take )
            
    for item in deleteMe:
        item.FBDelete()

def setupMoviePlanes( lOffset=0 ):
    
    # Remove all jpg sequences
    RS.Core.Face.XMLImporter.removeAllJpgs()
    
    # Get fbx filepath and asset root
    lFilePath = FBApplication().FBXFileName
    lAssetRoot = os.path.join( lFilePath.split(".")[0], "" )
    
    # Get all 00000000.jpg files into a list
    lImgSeq_List = []
    if os.path.isdir( lAssetRoot ):
        for lName in os.listdir( lAssetRoot ):
            lTakeRoot = os.path.join( lAssetRoot, lName )
            if os.path.isdir( lTakeRoot ):
                for lJpgName in os.listdir( lTakeRoot ):
                    if lJpgName == "00000000.jpg":
                        lImgSeq_List.append( os.path.join( lTakeRoot, lJpgName ) )
                        
    
    # Process each jpg sequence in the list
    for lJpgSeq in lImgSeq_List:
        lImgName = os.path.dirname( lJpgSeq ).rsplit( "\\", 1 )[1] + "_imgseq"
        RS.Core.Face.XMLImporter.createMoviePlane( lImgName, lJpgSeq, lOffset )
    
    # Setup visibility
    RS.Core.Face.XMLImporter.setupVisibleMoviePlanes()

def setupAudio( lOffset=0 ):
    
    # Remove all audio
    RS.Core.Face.XMLImporter.removeAllAudio()
    
    # Search for all audio files from 3 potential locations.
    lWav_List = []
    lFilePath = FBApplication().FBXFileName
    lAssetRoot = os.path.join( lFilePath.split(".")[0], "" )
    lAssetRoot2 = os.path.join( os.path.dirname( lFilePath ), "fwr" )
    
    if os.path.isdir( lAssetRoot ):
        for lName in os.listdir( lAssetRoot ):
            lTakeRoot = os.path.join( lAssetRoot, lName )
            
            # If it finds the audio folder, grab all wav files from there
            if lName == "RS_Audio":
                if os.path.isdir( lTakeRoot ):
                    for lWavName in os.listdir( lTakeRoot ):
                        lWav = os.path.join( lTakeRoot, lWavName )
                        if os.path.isfile( lWav ):
                            lWav_List.append( lWav )
            # if it's a wav, add it.
            elif os.path.isfile( lTakeRoot ):
                if lTakeRoot.lower().endswith(".wav"):
                    lWav_List.append( lTakeRoot )
    
    if os.path.isdir( lAssetRoot2 ):
        for lName in os.listdir( lAssetRoot2 ):
            lTakeRoot = os.path.join( lAssetRoot2, lName )
            if os.path.isfile( lTakeRoot ):
                if lTakeRoot.lower().endswith( ".wav" ):
                    lWav_List.append( lTakeRoot )
    
    # Search for all TAKE names
    lTakeNames = []
    for lTake in FBSystem().Scene.Takes:
        lTakeNames.append( lTake.Name )
    
    # Import all audio if there's a matching TAKE name
    for lWav in lWav_List:
        if os.path.basename( lWav ).rsplit(".", 1 )[0] in lTakeNames:
            RS.Core.Face.XMLImporter.importAudio( lWav, lOffset )
    
    # Match audio with take name
    for take in FBSystem().Scene.Takes:
        for audioFile in FBSystem().Scene.AudioClips:
            if audioFile.Name.split(".")[0] == take.Name:
                audioFile.CurrentTake = take
    
def cleanDxyzBatch():
    
    # Add Namespace
    dummyRoot = FBFindModelByLabelName("Dummy01")
    if dummyRoot:
        parentRoot = dummyRoot.Parent
        if parentRoot:
            parentRoot.ProcessNamespaceHierarchy(FBNamespaceAction.kFBConcatNamespace, "PedFacial")
            
    cleanTakes()
    
    # Get Offset from first key on CTRL_C_eye y translation
    lOffset = 0
    cl = FBComponentList()
    FBFindObjectsByName( "CTRL_C_eye", cl, False, True )
    if len( cl ) > 0:
        lOffset = cl[0].Translation.GetAnimationNode().Nodes[1].FCurve.Keys[0].Time.GetFrame()
    
    setupMoviePlanes( lOffset )
    
    setupAudio( lOffset )
    
    RS.Core.Face.Lib.addDelay_All()
    RS.Core.Face.Lib.reduceAllEyeAnimation()
    
    FBMessageBox( "Dxyz", "Dxyz Setup Completed!", "OK" )


        