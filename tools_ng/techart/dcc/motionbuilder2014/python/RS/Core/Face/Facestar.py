'''
Core\Face\Facestar.py

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>

Library for Facestar.
'''

import os
import sys
import xml.etree.cElementTree

import RS.Config
import RS.Core.Reference.Manager


## Constants ##

# XML Strings
XML_MARKER              = 'marker'
XML_MARKERS             = 'markers'
XML_OFF_FACE_MARKERS    = 'offFaceMarkers'
XML_ON_FACE_MARKERS     = 'onFaceMarkers'
XML_ID                  = 'id'
XML_NAME                = 'name'
XML_DISPLAY_NAME        = 'displayName'
XML_INFO                = 'info'
XML_NAMESPACE           = 'namespace'
XML_TEMPLATE            = 'facestarTemplate'
XML_POS                 = 'pos'
XML_ROT                 = 'rot'
XML_MIN                 = 'min'
XML_MAX                 = 'max'
XML_CONSTRAINT_WIDTH    = 'constraintWidth'
XML_ARROW_HEIGHT        = 'arrowHeight'
XML_DEFINITION          = 'facestarDefinition'
XML_ON_FACE_IMAGE       = 'onFaceImage'
XML_OFF_FACE_IMAGE      = 'offFaceImage'
XML_VERSION             = 'version'
XML_ATTRIBUTES          = 'attributes'
XML_ATTRIBUTE_GROUP     = 'attributeGroup'
XML_ATTRIBUTE           = 'attribute'
XML_VISIBLE             = 'visible'
XML_LOCAL_CONFIG        = 'facestarLocalConfig'
XML_SELECTION_SETS      = 'selectionSets'
XML_SELECTION_SET       = 'selectionSet'

# Directory where Facestar templates and definitions live.
CONFIG_DIR = '{0}\\etc\\config\\facestar'.format( RS.Config.Tool.Path.TechArt )

# Local configuration file for Facestar.
LOCAL_CONFIG_FILE = '{0}\\facestar.local'.format( CONFIG_DIR )

# File related constants.
TEMPLATE_FILE_EXTENSION = '.fst'
DEFINITION_FILE_EXTENSION = '.fsd'

TEMPLATE_FILE_VERSION = 0.1
DEFINITION_FILE_VERSION = 0.1


## Enums ##

class MarkerType:
    OnFace      = 1
    OffFace     = 2


## Functions ##

def GetSceneDefinitions():
    '''
    Get the definitions found in the currently open scene as a dictionary.
    
    Returns:
    
        Dictionary of Definition objects.  Key is the item namespace.
    '''
    definitions = {}
    
    refMgr = RS.Core.Reference.Manager.RsReferenceManager()
    refMgr.collectSceneReferences()
    
    # Find definitions for any character reference that has a facial root bone.
    for ref in refMgr.characters:
        if ref.facialRoot:
            definition = Definition( ref.namespace )
            definitions[ ref.namespace ] = definition
            
    return definitions

## Classes ##

class SaveOptions( object ):
    '''
    
    '''
    def __init__( self ):
        self.Namespace = None
        
class SelectionSet( object ):
    def __init__( self, name ):
        self.__name = name
        self.__offFaceMarkers = []
        self.__onFaceMarkers = []
        self.__attributes = []
        
    
    ## Properties ##
    
    @property
    def Name( self ):
        return self.__name
    
    @property
    def OffFaceMarkers( self ):
        return self.__offFaceMarkers
    
    @property
    def OnFaceMarkers( self ):
        return self.__onFaceMarkers
    
    @property
    def Attributes( self ):
        return self.__attributes
    
    
    ## Methods ##
    
    def HasItem( self, itemName ):
        return itemName in self.__attributes or itemName in self.__offFaceMarkers or itemName in self.__onFaceMarkers
    
    def RemoveItem( self, itemName ):
        if itemName in self.__attributes:
            self.__attributes.remove( itemName )
            
        elif itemName in self.__onFaceMarkers:
            self.__onFaceMarkers.remove( itemName )
            
        elif itemName in self.__offFaceMarkers:
            self.__offFaceMarkers.remove( itemName )
            
    def SetName( self, name ):
        self.__name = name
    
    def AddAttribute( self, attributeId ):
        if attributeId not in self.__attributes:
            self.__attributes.append( attributeId )
            
        else:
            print 'Attribute ({0}) already exists in the selection set!'.format( attributeId )
            
    def AddOffFaceMarker( self, offFaceMarkerId ):
        if offFaceMarkerId not in self.__offFaceMarkers:
            self.__offFaceMarkers.append( offFaceMarkerId )
            
        else:
            print 'Attribute ({0}) already exists in the selection set!'.format( offFaceMarkerId )
            
    def AddOnFaceMarker( self, onFaceMarkerId ):
        if onFaceMarkerId not in self.__onFaceMarkers:
            self.__onFaceMarkers.append( onFaceMarkerId )
            
        else:
            print 'Attribute ({0}) already exists in the selection set!'.format( onFaceMarkerId )
            
        
class LocalConfiguration( object ):
    def __init__( self ):
        self.__selectionSets = []
        
        
    ## Properties ##
    
    @property
    def SelectionSets( self ):
        return self.__selectionSets
    
        
    ## Private Methods ##
    
    def __LoadSelectionSets( self, xmlNode ):
        selectionSetNodes = xmlNode.findall( XML_SELECTION_SET )
                        
        for selectionSetNode in selectionSetNodes:
            name = selectionSetNode.get( XML_NAME )
            
            selectionSet = SelectionSet( name )
            
            # Add off-face markers.
            offFaceMarkerNode = selectionSetNode.find( XML_OFF_FACE_MARKERS )
            
            if offFaceMarkerNode:
                offFaceMarkerNodes = offFaceMarkerNode.findall( XML_MARKER )
                
                for offFaceMarkerNode in offFaceMarkerNodes:
                    selectionSet.AddOffFaceMarker( offFaceMarkerNode.get( XML_ID ) )
                
            # Add on-face markers.
            onFaceMarkerNode = selectionSetNode.find( XML_ON_FACE_MARKERS )
            
            if onFaceMarkerNode:
                onFaceMarkerNodes = onFaceMarkerNode.findall( XML_MARKER )
                                        
                for onFaceMarkerNode in onFaceMarkerNodes:
                    selectionSet.AddOnFaceMarker( onFaceMarkerNode.get( XML_ID ) )
                
            # Add attributes.
            attributeNode = selectionSetNode.find( XML_ATTRIBUTES )
            
            if attributeNode:
                attributeNodes = attributeNode.findall( XML_ATTRIBUTE )
                                        
                for attributeNode in attributeNodes:
                    selectionSet.AddAttribute( attributeNode.get( XML_ID ) )
                    
            self.__selectionSets.append( selectionSet )
        
    
    ## Methods ##
    
    def CreateNewSelectionSet( self, selectionSetName ):
        selectionSet = SelectionSet( selectionSetName )
        self.__selectionSets.append( selectionSet )
        
        return selectionSet
    
    def DeleteSelectionSet( self, selectionSetName ):
        ssItemIdx = None
        itemIdx = 0
        
        for selectionSet in self.__selectionSets:
            if selectionSet.Name == selectionSetName:
                ssItemIdx = itemIdx
            
            itemIdx += 1
            
        if ssItemIdx != None:
            self.__selectionSets.pop( ssItemIdx )
    
    def Load( self ):
        if not os.path.isfile( LOCAL_CONFIG_FILE ):
            self.Save()
            
        else:
            doc = xml.etree.cElementTree.parse( LOCAL_CONFIG_FILE )
            root = doc.getroot()
            
            if root:
                
                # Selection sets.
                selectionSetsNode = root.find( XML_SELECTION_SETS )
                
                if selectionSetsNode:
                    self.__LoadSelectionSets( selectionSetsNode )
                    
        
    def Save( self ):
        localConfigFile = open( LOCAL_CONFIG_FILE, 'w' )
        
        localConfigFile.write( '<?xml version="1.0" encoding="UTF-8"?>\n' )
        localConfigFile.write( '<{0}>\n'.format( XML_LOCAL_CONFIG ) )
        
        # Selection sets.
        localConfigFile.write( '\t<{0}>\n'.format( XML_SELECTION_SETS ) )
        
        for selectionSet in self.__selectionSets:
            # Selection set.
            localConfigFile.write( '\t\t<{0} name="{1}">\n'.format( XML_SELECTION_SET, selectionSet.Name ) )

            # Off-face markers.
            localConfigFile.write( '\t\t\t<{0}>\n'.format( XML_OFF_FACE_MARKERS ) )
            
            for offFaceMarkerId in selectionSet.OffFaceMarkers:
                localConfigFile.write( '\t\t\t\t<{0} id="{1}" />\n'.format( XML_MARKER, offFaceMarkerId ) )
                
            localConfigFile.write( '\t\t\t</{0}>\n'.format( XML_OFF_FACE_MARKERS ) )
            
            # On-face markers.
            localConfigFile.write( '\t\t\t<{0}>\n'.format( XML_ON_FACE_MARKERS ) )
            
            for onFaceMarkerId in selectionSet.OnFaceMarkers:
                localConfigFile.write( '\t\t\t\t<{0} id="{1}" />\n'.format( XML_MARKER, onFaceMarkerId ) )
                
            localConfigFile.write( '\t\t\t</{0}>\n'.format( XML_ON_FACE_MARKERS ) )
            
            # On-face markers.
            localConfigFile.write( '\t\t\t<{0}>\n'.format( XML_ATTRIBUTES ) )
            
            for attributeId in selectionSet.Attributes:
                localConfigFile.write( '\t\t\t\t<{0} id="{1}" />\n'.format( XML_ATTRIBUTE, attributeId ) )
                
            localConfigFile.write( '\t\t\t</{0}>\n'.format( XML_ATTRIBUTES ) )             
                        
            # End selection set.
            localConfigFile.write( '\t\t</{0}>\n'.format( XML_SELECTION_SET ) )
            
        # End selection sets.
        localConfigFile.write( '\t</{0}>\n'.format( XML_SELECTION_SETS ) )    
        
        # End local config.
        localConfigFile.write( '</{0}>\n'.format( XML_LOCAL_CONFIG ) )
        localConfigFile.close()
        

class Attribute( object ):
    '''
    Represents an off-face attribute.
    '''
    def __init__( self, name, displayName, minValue, maxValue ):
        self.__name = name
        self.__displayName = displayName.replace( "_", " " )
        self.__min = minValue
        self.__max = maxValue
        
        self.Favorite = False
        
    @property
    def Name( self ):
        return self.__name
    
    @property
    def DisplayName( self ):
        return self.__displayName
        
    @property
    def Min( self ):
        return self.__min
    
    @property
    def Max( self ):
        return self.__max    


class AttributeGroup( object ):
    '''
    A collection of Attribute objects.
    '''
    def __init__( self, name ):
        self.__name = name
        self.__attributes = []
        
    @property
    def Name( self ):
        return self.__name
        
    @property
    def Attributes( self ):
        return self.__attributes
    
    def AddAttribute( self, attributeObj ):
        self.__attributes.append( attributeObj )


class Marker( object ):
    '''
    Represents on off-face marker.
    '''
    def __init__( self, name, displayName, markerType, minValue = ( -1, -1 ), maxValue = ( 1, 1 ), constraintWidth = 60, arrowHeight = 60, visible = True ):
        self.__name = name
        self.__displayName = displayName.replace( "_", " " )
        self.__markerType = markerType
        self.__min = minValue
        self.__max = maxValue
        self.__constraintWidth = constraintWidth
        self.__arrowHeight = arrowHeight
        self.__visible = visible
        
        # GUI Transform
        self.Position = [ 0, 0 ]
        self.Rotation = 180.0
        
    @property
    def Type( self ):
        return self.__markerType
        
    @property
    def Name( self ):
        return self.__name
    
    @property
    def DisplayName( self ):
        return self.__displayName
    
    @property
    def Min( self ):
        return self.__min
    
    @property
    def Max( self ):
        return self.__max
    
    @property
    def ConstraintWidth( self ):
        return self.__constraintWidth
    
    @property
    def ArrowHeight( self ):
        return self.__arrowHeight
    
    @property
    def Visible( self ):
        return self.__visible
    

class Template( object ):
    '''
    The highest level representation of a face rig.  Definition's will inherit from the template.
    '''
    def __init__( self ):
        self.__templateDisplayName = None
        self.__filename = None
        self.__offFaceMarkers = {}
        self.__onFaceMarkers = {}
        self.__attributeGroups = []
        
        
    ## Properties ##
    
    @property
    def TemplateDisplayName( self ):
        return self.__templateDisplayName
    
    @property
    def TemplateFilename( self ):
        return self.__filename
    
    @property
    def OffFaceMarkers( self ):
        return self.__offFaceMarkers
    
    @property
    def OnFaceMarkers( self ):
        return self.__onFaceMarkers
    
    @property
    def AttributeGroups( self ):
        return self.__attributeGroups
    
    
    ## Methods ##
    
    def GetAttribute( self, attributeName ):
        for attributeGroup in self.__attributeGroups:
            for attribute in attributeGroup.Attributes:
                if attributeName == attribute.Name:
                    return ( attributeGroup, attribute )
                
        return None
        
    def AddMarker( self, marker ):
        if marker.Type == MarkerType.OffFace:
            self.__offFaceMarkers[ marker.Name ] = marker
            
        elif marker.Type == MarkerType.OnFace:
            self.__onFaceMarkers[ marker.Name ] = marker
           
    def GetMarker( self, markerName ):
        if markerName in self.__offFaceMarkers:
            return self.__offFaceMarkers[ markerName ]
        
        elif markerName in self.__onFaceMarkers:
            return self.__onFaceMarkers[ markerName ]
        
        else:
            return None
        
        #raise AttributeError( 'Marker using name "{0}" does not exist!'.format( markerName ) )
    
    def ClearMarkers( self ):
        self.__offFaceMarkers = {}
        self.__onFaceMarkers = {}
        
    def ClearAttributes( self ):
        self.__attributeGroups = []
    
    def LoadTemplate( self, filename ):
        if os.path.isfile( filename ):
            self.__filename = filename

            doc = xml.etree.cElementTree.parse( filename )
            root = doc.getroot()
        
            if root:
                
                self.ClearMarkers()                
                
                # Info
                self.__templateDisplayName = root.find( XML_INFO ).find( XML_DISPLAY_NAME ).text
                
                # On Face Markers
                markerNodes = root.find( XML_ON_FACE_MARKERS ).findall( XML_MARKER )
                                
                for markerNode in markerNodes:
                    name = markerNode.get( XML_NAME )
                    displayName = markerNode.get( XML_DISPLAY_NAME )
                    visible = eval( markerNode.get( XML_VISIBLE ) )
                    
                    marker = Marker( name, displayName, MarkerType.OnFace, visible = visible )
                    
                    self.AddMarker( marker )                
                
                # Off Face Markers
                markerNodes = root.find( XML_OFF_FACE_MARKERS ).findall( XML_MARKER )
                
                for markerNode in markerNodes:
                    name = markerNode.get( XML_NAME )
                    displayName = markerNode.get( XML_DISPLAY_NAME )
                    minValue = eval( markerNode.get( XML_MIN ) )
                    maxValue = eval( markerNode.get( XML_MAX ) )
                    constraintWidth = int( markerNode.get( XML_CONSTRAINT_WIDTH ) )
                    arrowHeight = int( markerNode.get( XML_ARROW_HEIGHT ) )
                    visible = eval( markerNode.get( XML_VISIBLE ) )
                    
                    marker = Marker( name, 
                                     displayName, 
                                     MarkerType.OffFace, 
                                     minValue = minValue, 
                                     maxValue = maxValue, 
                                     constraintWidth = constraintWidth, 
                                     arrowHeight = arrowHeight, 
                                     visible = visible )
                    
                    self.AddMarker( marker )
                    
                # Attributes
                attributeGroupNodes = root.find( XML_ATTRIBUTES ).findall( XML_ATTRIBUTE_GROUP )
                
                self.ClearAttributes()
                
                for attributeGroupNode in attributeGroupNodes:
                    name = attributeGroupNode.get( XML_NAME )
                    attributeGroup = AttributeGroup( name )
                    
                    attributeNodes = attributeGroupNode.findall( XML_ATTRIBUTE )
                    
                    for attributeNode in attributeNodes:
                        name = attributeNode.get( XML_NAME )
                        displayName = attributeNode.get( XML_DISPLAY_NAME )
                        minValue = float( attributeNode.get( XML_MIN ) )
                        maxValue = float( attributeNode.get( XML_MAX ) )
                        
                        attribute = Attribute( name, displayName, minValue, maxValue )
                        attributeGroup.AddAttribute( attribute )
                        
                    self.__attributeGroups.append( attributeGroup )
        
        else:
            raise WindowsError( 'The filename "{0}" does not exist!'.format( filename ) )        
        

class Definition( Template ):
    '''
    Represents the definition of a face rig for a specific character.  Each defintion will inherit from a
    template.  The definition mostly consists of data specific for the character.
    '''
    def __init__( self, definitionFilename ):
        Template.__init__( self )
                           
        self.__namespace = definitionFilename
        self.__displayName = definitionFilename.replace("_"," ")
        self.__definitionFilename = definitionFilename
        self.__definitionFilePath = '{0}\\definitions\\{1}{2}'.format( CONFIG_DIR, self.__definitionFilename, DEFINITION_FILE_EXTENSION )
        self.__offFaceImageFilePath = None
        self.__onFaceImageFilePath = None
        self.__templateName = None
        
        self.DefaultTemplatePath = '{0}\\definitions\\{1}{2}'.format( CONFIG_DIR, 'Default', DEFINITION_FILE_EXTENSION )
        
        self.TemplateName = None
        
        self.LoadDefinition()
        
    @property
    def Namespace( self ):
        return self.__namespace
    
    @property
    def DisplayName( self ):
        return self.__displayName
    
    @property
    def DefinitionFilename( self ):
        return self.__definitionFilename
    
    @property
    def DefinitionFilePath( self ):
        return self.__definitionFilePath
    
    @property
    def OffFaceImageFilename( self ):
        return self.__offFaceImageFilePath
    
    @property
    def OnFaceImageFilename( self ):
        return self.__onFaceImageFilePath
    
    @property
    def TemplateFileName( self ):
        return self.__templateName
    
    def LoadDefinition( self ):
        '''
        Load the definition from disk.
        '''
        
                
        # If Definition does not exist, use a Default.
        if os.path.isfile( self.__definitionFilePath ):
            pass
        elif "^" in self.__definitionFilePath:
            firstSplit = self.__definitionFilePath.split("^")
            secondSplit = firstSplit[1].split(".")
            noCarat = firstSplit[0] + "." + secondSplit[1]
            if os.path.isfile( noCarat ):
                self.__definitionFilename = self.__namespace.split("^")[0]
                self.__definitionFilePath = noCarat
            else:
                self.__definitionFilename = 'Default'
                self.__definitionFilePath = self.DefaultTemplatePath
        else:
            self.__definitionFilename = 'Default'
            self.__definitionFilePath = self.DefaultTemplatePath
        
        if os.path.isfile( self.__definitionFilePath ):
            doc = xml.etree.cElementTree.parse( self.__definitionFilePath )
            root = doc.getroot()
        
            if root:
                if self.TemplateName == None:
                    self.__templateName = root.find( XML_INFO ).find( XML_TEMPLATE ).text
                    self.TemplateName = root.find( XML_INFO ).find( XML_TEMPLATE ).text
                
                self.__offFaceImageFilePath = '{0}\\images\\{1}.png'.format( CONFIG_DIR, root.find( XML_INFO ).find( XML_OFF_FACE_IMAGE ).text )
                self.__onFaceImageFilePath = '{0}\\images\\{1}.png'.format( CONFIG_DIR, root.find( XML_INFO ).find( XML_ON_FACE_IMAGE ).text )
                
                templateFilePath = self.LoadTemplate( '{0}\\templates\\{1}{2}'.format( CONFIG_DIR, self.TemplateName, TEMPLATE_FILE_EXTENSION ) )
                
                # On AND Off Face Markers - Default Position       
                if os.path.isfile( self.DefaultTemplatePath ):
                    tDoc = xml.etree.cElementTree.parse( self.DefaultTemplatePath )
                    tRoot = tDoc.getroot()
                    tMarkerNodes = ( tRoot.find( XML_ON_FACE_MARKERS ).findall( XML_MARKER ) ) + ( tRoot.find( XML_OFF_FACE_MARKERS ).findall( XML_MARKER ) )
                    if tRoot:
                        for tMarkerNode in tMarkerNodes:
                            tID = tMarkerNode.get( XML_ID )
                            tPos = list( eval( tMarkerNode.get( XML_POS ) ) )
                            tMarker = self.GetMarker( tID )
                            if tMarker:
                                tMarker.Position = tPos
                                
                # On Face Markers
                markerNodes = root.find( XML_ON_FACE_MARKERS ).findall( XML_MARKER )
                for markerNode in markerNodes:
                    id = markerNode.get( XML_ID )
                    pos = list( eval( markerNode.get( XML_POS ) ) )
                    
                    marker = self.GetMarker( id )
                    
                    if marker:
                        marker.Position = pos
                
                # Off Face Markers
                markerNodes = root.find( XML_OFF_FACE_MARKERS ).findall( XML_MARKER )
                
                for markerNode in markerNodes:
                    id = markerNode.get( XML_ID )
                    pos = list( eval( markerNode.get( XML_POS ) ) )
                    rot = float( markerNode.get( XML_ROT ) )
                    
                    marker = self.GetMarker( id )
                    
                    if marker:
                        marker.Position = pos
                        marker.Rotation = rot
                        
                    else:
                        pass
                        #raise AttributeError( 'Facestar definition is using the marker id "{0}", but it could not be found in the template!'.format( id ) )
                
                                                     
                
                
                
                return templateFilePath
        
        else:
            sys.stderr.write( 'Facestar: The filename "{0}" does not exist!\n'.format( self.__definitionFilePath ) )
        
    def SaveAs( self ):
        pass
    
    def Save( self, filename = None ):
        '''
        Save the definition to disk as a .fsd file type.
        '''
        xml = '<?xml version="1.0" encoding="UTF-8"?>\n'
        
        xml += '<{0}>\n'.format( XML_DEFINITION )
        
        # Info block.
        xml += '\t<{0}>\n'.format( XML_INFO )
        if self.__definitionFilename == "Default":
            xmlNamespace = self.__definitionFilename
            xmlDisplayName = self.__definitionFilename
        else:
            xmlNamespace = self.__namespace
            xmlDisplayName = self.__displayName
        xml += '\t\t<{0}>{1}</{0}>\n'.format( XML_NAMESPACE, xmlNamespace )
        xml += '\t\t<{0}>{1}</{0}>\n'.format( XML_DISPLAY_NAME, xmlDisplayName )
        xml += '\t\t<{0}>{1}</{0}>\n'.format( XML_TEMPLATE, os.path.basename( self.TemplateName ).split( '.' )[ 0 ] )
        xml += '\t\t<{0}>{1}</{0}>\n'.format( XML_OFF_FACE_IMAGE, os.path.basename( self.__offFaceImageFilePath ).split( '.' )[ 0 ] )
        xml += '\t\t<{0}>{1}</{0}>\n'.format( XML_ON_FACE_IMAGE, os.path.basename( self.__onFaceImageFilePath ).split( '.' )[ 0 ] )
        xml += '\t</{0}>\n'.format( XML_INFO )
        
        # On Face Markers block.
        xml += '\t<{0}>\n'.format( XML_ON_FACE_MARKERS )
        for markerName, marker in self.OnFaceMarkers.iteritems():
            xml += '\t\t<{nodeName} {id}="{idVal}" {pos}="{posVal}" {rot}="{rotVal}" />\n'.format( nodeName = XML_MARKER, 
                                                                                                   id = XML_ID,
                                                                                                   idVal = markerName,
                                                                                                   pos = XML_POS,
                                                                                                   posVal = marker.Position,
                                                                                                   rot = XML_ROT,
                                                                                                   rotVal = marker.Rotation )
        
        xml += '\t</{0}>\n'.format( XML_ON_FACE_MARKERS )        
        
        # Off Face Markers block.
        xml += '\t<{0}>\n'.format( XML_OFF_FACE_MARKERS )
        
        for markerName, marker in self.OffFaceMarkers.iteritems():
            xml += '\t\t<{nodeName} {id}="{idVal}" {pos}="{posVal}" {rot}="{rotVal}" />\n'.format( nodeName = XML_MARKER, 
                                                                                                   id = XML_ID,
                                                                                                   idVal = markerName,
                                                                                                   pos = XML_POS,
                                                                                                   posVal = marker.Position,
                                                                                                   rot = XML_ROT,
                                                                                                   rotVal = marker.Rotation )
        
        xml += '\t</{0}>\n'.format( XML_OFF_FACE_MARKERS )
        
        xml += '</{0}>\n'.format( XML_DEFINITION )
    
        try:
            if filename == None:
                filename = self.__definitionFilePath
                
            fstream = open( filename, 'w' )
            fstream.write( xml )
            
        except IOError:
            print 'An error occurred while attempting to create the Facestar definition file "{0}"'.format( self.__definitionFilePath )
            
        finally:
            fstream.close()
