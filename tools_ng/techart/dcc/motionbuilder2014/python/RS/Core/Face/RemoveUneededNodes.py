#=========================================================
#  This will cleanup the lip controllers from the secondary relation constraint
#   This is to fix an issue with MB
#   Kyle Hansen
#=========================================================

from pyfbsdk import *
import RS.Globals as glo
import os


def testAgainstString(string1):
    foundBool = False
    for prefix in prefixs:
        for suffix in suffixs:
            for controllerName in controllerList:
                string2 = (prefix + controllerName + suffix)
                string2 = string2.lower()

                if string2.lower() in string1.lower():
                    foundBool = True                
    
    return foundBool


def testDisconProperty(string1):
    foundBool = False
    for prefix in prefixs:
        for disconnect in disconnectList:
            string2 = (prefix + disconnect)
            string2 = string2.lower()
            if string2.lower() in string1.lower():
                foundBool = True
                    
    return foundBool
    

def Run():
    prefixs = ["facial_r_", "facial_l_", "facial_"]
    suffixs = ["analog", "sdk"]
    controllerList = ["lipcorner", "lipupper", "liplower"]
    
    
    disconnectList = ["jaw "]
    disconnectProperty = ["lcl translation"]
    
    lCharacterName = os.path.basename(glo.gApp.FBXFileName).partition(".")[0].partition("_")[2].capitalize()
    searchName = (lCharacterName + " 3Lateral 1")
    
    relConstraint = None
    allConstraints = FBSystem().Scene.Constraints
    for constraint in allConstraints:
    
        if constraint.Name == searchName:
            relConstraint = constraint
            
    print relConstraint
    
    for controllerFound in relConstraint.Boxes:
        
        if testAgainstString(controllerFound.Name):
            print "DELETE THIS MOFO: ", controllerFound.Name
            relConstraint.Boxes.remove(controllerFound)
    
        elif testDisconProperty(controllerFound.Name):
            print "DISCONNECT THIS MOFO: ", controllerFound.Name
            lAnimationNodesIN = controllerFound.AnimationNodeInGet().Nodes
            for node in lAnimationNodesIN:
                if node.Name.lower() in disconnectProperty:
                    print node.Name
                    try:
                        out = node.GetSrc(0)
                        node.DisconnectSrc(out)
                    except:
                        print "Killed the node..."

                
               
                
                
