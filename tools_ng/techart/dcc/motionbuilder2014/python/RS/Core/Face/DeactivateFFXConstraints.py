from pyfbsdk import *
import RS.Globals

lFaceFXExpressions = ["mouth_CTRL-Y_Position",
                      "jaw_CTRL-Y_Position",
                      "tongueMove_CTRL-Y_Position",
                      "tongueRoll_CTRL-Y_Position",
                      "tongueInOut_CTRL-Y_Position",
                      "outerBrow_R_CTRL-Y_Position",
                      "innerBrow_L_CTRL-Y_Position",
                      "innerBrow_L_CTRL-X_Position",
                      "outerBrow_L_CTRL-Y_Position",
                      "innerBrow_R_CTRL-Y_Position",
                      "innerBrow_R_CTRL-X_Position",
                      "eye_C_CTRL-X_Position",
                      "eye_L_CTRL-Y_Position",
                      "eye_R_CTRL-X_Position",
                      "nose_L_CTRL-Y_Position",
                      "nose_L_CTRL-X_Position",
                      "nose_R_CTRL-Y_Position",
                      "nose_R_CTRL-X_Position",
                      "cheek_L_CTRL-Y_Position",
                      "cheek_R_CTRL-Y_Position",
                      "CIRC_press-X_Position",
                      "CIRC_narrowWide-X_Position",
                      "CIRC_smileR-X_Position",
                      "CIRC_smileL-X_Position",
                      "CIRC_scream-X_Position",
                      "CIRC_lipsNarrowWideR-X_Position",
                      "CIRC_lipsNarrowWideL-X_Position",
                      "CIRC_chinRaiseUpper-X_Position",
                      "CIRC_chinRaiseLower-X_Position",
                      "CIRC_closeOuterR-X_Position",
                      "CIRC_closeOuterL-X_Position",
                      "CIRC_puckerR-X_Position",
                      "CIRC_puckerL-X_Position",
                      "CIRC_oh-X_Position",
                      "CIRC_funnelUR-X_Position",
                      "CIRC_funnelDR-X_Position",
                      "CIRC_pressR-X_Position",
                      "CIRC_pressL-X_Position",
                      "CIRC_dimpleR-X_Position",
                      "CIRC_dimpleL-X_Position",
                      "CIRC_lipBite-X_Position",
                      "CIRC_funnelUL-X_Position",
                      "CIRC_funnelDL-X_Position",
                      "CIRC_blinkL-X_Position",
                      "CIRC_squeezeR-X_Position",
                      "CIRC_squeezeL-X_Position",
                      "CIRC_blinkR-X_Position",
                      "CIRC_squintInnerUR-X_Position",
                      "CIRC_squintInnerDR-X_Position",
                      "CIRC_squintInnerUL-X_Position",
                      "CIRC_squintInnerDL-X_Position"]

def rs_DeactivateFFX():
    
    for iConstraint in RS.Globals.gConstraints:
        
        if iConstraint.Name in lFaceFXExpressions:
            
            iConstraint.Active = False

    FBMessageBox("FFX Deactive", "Done", "OK")
    