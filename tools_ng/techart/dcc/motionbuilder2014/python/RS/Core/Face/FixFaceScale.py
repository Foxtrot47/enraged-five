from pyfbsdk import *
from ctypes import *
from xml.dom import minidom

import RS.Perforce as p4
import RS.Config
import os


def fixAllScale( ):
    for lDev in FBSystem().Scene.Devices:
        if lDev.ClassName() == "RSExprSolverDevice":
            fixScale( lDev )

def fixScale( lExprSolver ):
    setActiveDevice( lExprSolver.LongName )
    
    charName = lExprSolver.LongName.split( ":" )[0].split( "^" )[0].split( " " )[0]
    
    charScale = getScaleFromPedsMeta( charName )
    
    if charScale != 1 and charName.startswith( "CS_" ):
        print "Setting scale for {0} to {1}".format( charName, charScale )
        cdll.MBExprSolver.SetScale_Py(c_double( charScale ) )
    else:
        print "Setting scale for {0} to {1}".format( charName, "1.0" )
        cdll.MBExprSolver.SetScale_Py(c_double( 1.0 ) )
        

def setActiveDevice( deviceLongName ):
    '''
    Sets the current active device
    Args:
        String: Device LongName
    '''
    # set the argument type
    cdll.MBExprSolver.SetActiveDevice_Py.argtypes = [ c_char_p ]
    # carry out the method
    cdll.MBExprSolver.SetActiveDevice_Py( deviceLongName )


def getScaleFromPedsMeta( charName ):
    
    scaleValue = 1.0    
    gProjRoot = RS.Config.Project.Path.Root
    pXmlFileName = gProjRoot + "\\assets\\export\\data\\peds.pso.meta"
    p4.Sync( pXmlFileName )
    
    if(os.path.exists(pXmlFileName)):
        #charName = os.path.basename( FBApplication().FBXFileName ).split(".")[0]
        lXmlFile = minidom.parse( pXmlFileName )
        rootData = lXmlFile.getElementsByTagName("InitDatas")
        charData =  rootData[0].getElementsByTagName("Name")
        scaleData = rootData[0].getElementsByTagName("CutsceneScale")
        for char in charData:
            if char.childNodes[0].data == charName:
                for scale in scaleData:
                    if char.parentNode == scale.parentNode:
                        scaleValue = float( scale.getAttribute("value") )
                        
    else:
        print "{0} file does not exist locally.".format( pXmlFileName )
                        
    return float( scaleValue )
