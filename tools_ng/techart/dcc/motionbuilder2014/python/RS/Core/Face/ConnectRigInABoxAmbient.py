from pyfbsdk import *
import RS.Globals as glo
import os
import RS.Utils.Scene

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_ConstraintsFolder
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_IK_ConstraintsFolder():
	
	lFolder = None

	for iFolder in glo.gFolders:
		if iFolder.Name == 'Constraints 1':
			lFolder = iFolder
	    
	if lFolder == None:
		lPlaceholder = FBConstraintRelation('Remove_Me')
		lFolder = FBFolder("Constraints 1", lPlaceholder)
		lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
		lTag.Data = "rs_Folders"
		lConFolder = rs_IK_ConstraintsFolder()
		
		FBSystem().Scene.Evaluate()
		
		lPlaceholder.FBDelete()  
	
	return lFolder

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_FacialPreRotation
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################       

def rs_FacialPreRotation():

    for iChild in glo.gComponents:
        
        if iChild.Name.startswith("FB_"):
            if iChild.ClassName() == 'FBModelSkeleton':
                lRotation = iChild.PropertyList.Find("Lcl Rotation").Data
                if str(lRotation) != "FBVector3d(0, 0, 0)":
                    iChild.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,0)
                    iChild.PropertyList.Find("RotationActive").Data = True
                    iChild.PropertyList.Find("PreRotation").Data = lRotation
    
    for iComponent in glo.gComponents:     
        if iComponent.Name.startswith("CTRL"):
            for iProp in iComponent.PropertyList:
                if iProp.IsUserProperty():
                    try:
                        iProp.SetAnimated(True)
                    except:
                        print iProp.Name

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_ConnectAmbientFaceRig
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def rs_ConnectAmbientFaceRig():

    lCharacterName = os.path.basename(glo.gApp.FBXFileName).partition(".")[0].partition("_")[2].capitalize()
        
    if "Animals" in glo.gApp.FBXFileName and not "Chimp" in glo.gApp.FBXFileName:
    
        lRelConstraint = FBConstraintRelation('Ambient_canine 3Lateral')
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', ('Ambient_canine 3Lateral'))
    
    else:
        lRelConstraint = FBConstraintRelation('Ambient_anim 3Lateral')
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', ('Ambient_anim 3Lateral'))
    
    lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Constraints"   
    
    lRelConstraint.SetBoxPosition(lCharacterNode, -3000, 0)
    lRelConstraint.Active = True
    lConFolder = rs_IK_ConstraintsFolder()
    lConFolder.Items.append(lRelConstraint)
    
    Array = []
    
    for iComponent in glo.gComponents:     
        if "CTRL" in iComponent.Name or "CIRC" in iComponent.Name:
            for iProp in iComponent.PropertyList:
                if iProp.IsUserProperty():
                    try:
                        iProp.SetAnimated(True)
                    except:
                        print iProp.Name
    
    for iConstraint in glo.gConstraints:
        
        if iConstraint.Name == lRelConstraint.Name:
            for iBox in iConstraint.Boxes:
                if iBox.Name.startswith(lRelConstraint.Name):
                    i = 0
                    nodeArray = []
                    for iNode in iBox.AnimationNodeOutGet().Nodes:
    
                        nodeArray.append(iNode.Name)
                    
                    nodeArray = sorted(nodeArray)
                    
                    for node in nodeArray:
                        
                        modelName = node.partition("_XYZ_")[0]
                        
                        model = FBFindModelByLabelName(modelName)
                        
                        if not model:
                            
                            model = FBFindModelByLabelName(modelName[0:len(modelName) - 1] + "1")
                        
                        if model:
    
                            if node.partition("_XYZ_")[2] == "Rotation":
                                
                                lSend = iConstraint.SetAsSource(model)
                                
                                if lSend:
                                    iConstraint.SetBoxPosition(lSend, 200, 200 * i)
                                    lSend.UseGlobalTransforms = False
            
                                vAdd1 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                if vAdd1:
                                    iConstraint.SetBoxPosition(vAdd1, 500, 200 * i)
                                    
                                vAdd2 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                if vAdd2:
                                    iConstraint.SetBoxPosition(vAdd2, 500, 200 * i + 25)
                                    
                                vAdd3 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                if vAdd3:
                                    iConstraint.SetBoxPosition(vAdd3, 500, 200 * i + 50)
                                    
                                lReceive = iConstraint.ConstrainObject(model) 
    
                                if lReceive:
                                    iConstraint.SetBoxPosition(lReceive, 800, 200 * i)
                                    lReceive.UseGlobalTransforms = False
                                
                                i = i + 1
                                if lSend and lReceive:
                    
                                    vector = node.partition("_XYZ_")[2]
                                    if vector == "Position":
                                        vector = "Translation"
                                    
                                    RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Position", vAdd1, 'V1')
                                    RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Rotation", vAdd2, 'V1')
                                    RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Scale",    lReceive, "Lcl Scaling")
    
                                    RS.Utils.Scene.ConnectBox(lSend, "Lcl Translation", vAdd1, 'V2')
                                    RS.Utils.Scene.ConnectBox(lSend, "Lcl Rotation", vAdd2, 'V2')
                                    RS.Utils.Scene.ConnectBox(lSend, "Lcl Scaling", vAdd3, 'V2')
                                    RS.Utils.Scene.ConnectBox(vAdd1, 'Result', lReceive, "Lcl Translation")
                                    RS.Utils.Scene.ConnectBox(vAdd2, 'Result', lReceive, "Lcl Rotation")
                    
                    CtrlArray = []
                    for iNode in iBox.AnimationNodeInGet().Nodes:
                        Name = iNode.Name
                        if Name.startswith("c_"):
                            Name = iNode.Name[2:len(iNode.Name)]
                        
                        model = FBFindModelByLabelName(Name.partition("_XYZ_")[0])
                        
                        if model:
                            
                            if not model in CtrlArray:
                                CtrlArray.append(model)
                    i = 0       
                    for model in CtrlArray:
                        lSend = iConstraint.SetAsSource(model)
                        if lSend:
                            iConstraint.SetBoxPosition(lSend, -6000, 200 * i)
                            lSend.UseGlobalTransforms = False
                        i = i + 1
                        for iNode in iBox.AnimationNodeInGet().Nodes:
                            Name = iNode.Name
                            if Name.startswith("c_"):
                                Name = iNode.Name[2:len(iNode.Name)]
                            if Name.startswith(model.Name):
                                vector = None
                                if Name.partition("_XYZ")[2].partition("_")[2] == "Pos":
                                    vector = "Lcl Translation"
                                elif Name.partition("_XYZ")[2].partition("_")[2] == "Rot":
                                    vector = "Lcl Rotation"
                                elif Name.partition("_XYZ")[2].partition("_")[2] == "Scale":
                                    vector = "Lcl Scaling"
                                RS.Utils.Scene.ConnectBox(lSend, vector, iBox, Name)
                                if vector == None:
                                    RS.Utils.Scene.ConnectBox(lSend, Name[len(lSend.Name.partition(" ")[0]) + 1:len(Name)], iBox, Name)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## Description: rs_SetUpAmbientUI
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

def rs_SetUpAmbientUI():
     
    lFlipArray = ["RECT_R_Blink",
                  "RECT_L_Blink",
                  "TEXT_R_Blink",
                  "TEXT_L_Blink"] 
                        
    for iBorder in glo.gAmbientBorders:
        
        lModel = FBFindModelByLabelName(iBorder)
        
        if lModel:
            lModel.Pickable = False
            lModel.Transformable = False
            
            if lModel.Name in lFlipArray:
                lModel.Rotation.Data = FBVector3d(0,0,180)
            
    for iSlider in glo.gAmbient2DSliders:
        
        lModel = FBFindModelByLabelName(iSlider)
        
        if lModel:
            
            lModel.PropertyList.Find("TranslationActive").Data = True
            lModel.PropertyList.Find("TranslationMin").Data = FBVector3d(-1,-1,0)
            lModel.PropertyList.Find("TranslationMax").Data = FBVector3d(1,1,0)
            lModel.PropertyList.Find("TranslationMinX").Data = True
            lModel.PropertyList.Find("TranslationMinY").Data = True
            lModel.PropertyList.Find("TranslationMinZ").Data = True
            lModel.PropertyList.Find("TranslationMaxX").Data = True
            lModel.PropertyList.Find("TranslationMaxY").Data = True
            lModel.PropertyList.Find("TranslationMaxZ").Data = True
    
    lRelConstraint = FBConstraintRelation('Viseme Relation')
    lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
    lTag.Data = "rs_Constraints"   
    lRelConstraint.Active = True
    lConFolder = rs_IK_ConstraintsFolder()
    lConFolder.Items.append(lRelConstraint)
    
    i = 0
          
    for iSlider in glo.gAmbient1DSliders:
        
        lModel = FBFindModelByLabelName(iSlider)
        
        if lModel:
            
            lModel.PropertyList.Find("TranslationActive").Data = True
            lModel.PropertyList.Find("TranslationMin").Data = FBVector3d(0,-1,0)
            lModel.PropertyList.Find("TranslationMax").Data = FBVector3d(0,1,0)
            lModel.PropertyList.Find("TranslationMinX").Data = True
            lModel.PropertyList.Find("TranslationMinY").Data = True
            lModel.PropertyList.Find("TranslationMinZ").Data = True
            lModel.PropertyList.Find("TranslationMaxX").Data = True
            lModel.PropertyList.Find("TranslationMaxY").Data = True
            lModel.PropertyList.Find("TranslationMaxZ").Data = True
            
            lModel.PropertyCreate('viseme', FBPropertyType.kFBPT_double, 'Number',True, True, None) 
            
            lProp = lModel.PropertyList.Find("viseme")
            lProp.SetMin(-1)
            lProp.SetMax(1)
            lProp.Data = 0
            lProp.SetAnimated(True)
            
            lSend = lRelConstraint.SetAsSource(lModel)
            lRelConstraint.SetBoxPosition(lSend, 0, 200 * i)
            lSend.UseGlobalTransforms = False
            
            lVectorToNumber = lRelConstraint.CreateFunctionBox('Converters', 'Vector to Number')
            lRelConstraint.SetBoxPosition(lVectorToNumber, 400, 200 * i)
            
            lReceive = lRelConstraint.ConstrainObject(lModel) 
            lRelConstraint.SetBoxPosition(lReceive, 800, 200 * i)
            lReceive.UseGlobalTransforms = False
            
            RS.Utils.Scene.ConnectBox(lSend, "Lcl Translation", lVectorToNumber, 'V')
            RS.Utils.Scene.ConnectBox(lVectorToNumber, "Y", lReceive, 'viseme')
            
            i = i + 1
            
    lAmbientUI = FBFindModelByLabelName("Ambient_UI")
    lFaceFX = FBFindModelByLabelName("FaceFX")
    if lFaceFX:
        lFaceFX.Parent = lAmbientUI

