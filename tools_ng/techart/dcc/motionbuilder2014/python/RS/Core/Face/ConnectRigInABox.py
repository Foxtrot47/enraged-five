from pyfbsdk import *
import RS.Globals as glo
import os
import RS.Utils.Scene

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Find Rig Type By Number Of FACIAL bones
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_FaceRigType():

    lSkelHead = FBFindModelByLabelName("SKEL_Head")
    l3LateralList = []
    
    for iChild in lSkelHead.Children:
        
        if iChild.Name == "FACIAL_facialRoot":
            
            RS.Utils.Scene.GetChildren(iChild, l3LateralList)
            #l3LateralList.extend(eva.gChildList)
            #del eva.gChildList[:] 
            
    if len(l3LateralList) > 45 and len(l3LateralList) < 50:
        
        return "B_Rig"
    
    elif len(l3LateralList) > 155 and len(l3LateralList) < 160:
        
        return "3Lateral_Rig"
        
    else:
        
        return "Ambient_Rig"

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ConstraintsFolder
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_IK_ConstraintsFolder():
    
    lFolder = None
    
    for iFolder in glo.gFolders:
        if iFolder.Name == 'Constraints 1':
            lFolder = iFolder
    
    if lFolder == None:
        lPlaceholder = FBConstraintRelation('Remove_Me')
        lFolder = FBFolder("Constraints 1", lPlaceholder)   
        lTag = lFolder.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Folders"
        
        FBSystem().Scene.Evaluate()
        
        lPlaceholder.FBDelete()  
            
    return lFolder

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_OnFaceControls
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_OnFaceControls():
    
    gOnFaceControls = ["lowerLip_L_CTRL", "lowerLip_C_CTRL", "lowerLip_R_CTRL", "lipCorner_R_CTRL", "upperLip_C_CTRL",
                   "upperLip_L_CTRL", "lipCorner_L_CTRL", "upperLip_R_CTRL", "eyelidUpperOuter_L_CTRL", "eyelidUpperInner_L_CTRL",
                   "eyelidLowerOuter_L_CTRL", "eyelidLowerInner_L_CTRL", "eyelidLowerOuter_R_CTRL", "eyelidLowerInner_R_CTRL",
                   "eyelidUpperOuter_R_CTRL", "eyelidUpperInner_R_CTRL"]
    
    return gOnFaceControls
                      
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_FindCharacterName
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   
       
def rs_FindCharacterName():
    
    lCharacterName = os.path.basename(glo.gApp.FBXFileName).partition(".")[0].partition("_")[2].capitalize()
    
    lRigType = rs_FaceRigType()
    
    if "townley" in lCharacterName:
        Name = lCharacterName.partition("townley")
        lCharacterName = Name[0] + Name[2] 
    if "Dr" in lCharacterName:
        lCharacterName = lCharacterName.replace("Dr","Dr_")
    if "Lester" in lCharacterName:
        lCharacterName = "Lester"
    if "Zero" in lCharacterName:
        lCharacterName = "Michael" 
    if "Tracy" in lCharacterName:
        lCharacterName = "Tracy"
    if "Two" in lCharacterName:
        lCharacterName = "OR - Box Trevor"
    if "One" in lCharacterName:
        lCharacterName = "OR - Box Franklin" 
    if "CSB" in os.path.basename(glo.gApp.FBXFileName) or lRigType == "B_Rig":
        lCharacterName = "Csb" 
    if "Orleans" in lCharacterName:
        lCharacterName = "Sasq" 
        
    return lCharacterName

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ConnectRigInABox
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_ConnectRigInABox():
    
    pluginPresent = True
    
    expressionPluginPath = '{0}\\dcc\\current\\motionbuilder2014\\plugins\\x64\\expressions\\'.format( RS.Config.Tool.Path.Root )
    
    lCharacterNode = None
    lCharacterName = rs_FindCharacterName()
    gOnFaceControls = rs_OnFaceControls()
        
    if "OR - Box" in lCharacterName:
        lRelConstraint = FBConstraintRelation(lCharacterName)
        lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Constraints"   
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', lCharacterName)

    else:
        lRelConstraint = FBConstraintRelation(lCharacterName + ' 3Lateral')
        lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Constraints"   
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', (lCharacterName + ' 3Lateral'))

    
    if lCharacterNode == None:
        pluginPresent = False
        lRelConstraint.FBDelete()
        FBMessageBox( 'R* Error', 'This Character may not have a plugin available for 3L Face setup.\n\nMake sure you have latest on this file:\n{0}\n\nIf error persists after getting latest, please add a bug for Tech Art to\nbuild a plugin for this character.'.format( expressionPluginPath ), 'Ok' )
    
    else:   
        lRelConstraint.SetBoxPosition(lCharacterNode, -3000, 0)
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lRelConstraint)        
        
        Array = []
        
        for iComponent in glo.gComponents:     
            if "CTRL" in iComponent.Name or "CIRC" in iComponent.Name:
                for iProp in iComponent.PropertyList:
                    if iProp.IsUserProperty():
                        try:
                            iProp.SetAnimated(True)
                        except:
                            None
        
        for iConstraint in glo.gConstraints:
            
            if iConstraint.Name == lRelConstraint.Name:
                for iBox in iConstraint.Boxes:
                    if iBox.Name.startswith(lRelConstraint.Name):
                        i = 0
                        nodeArray = []
                        for iNode in iBox.AnimationNodeOutGet().Nodes:
        
                            nodeArray.append(iNode.Name)
                        
                        nodeArray = sorted(nodeArray)
                        
                        for node in nodeArray:
                            
                            model = FBFindModelByLabelName(node.partition("_XYZ_")[0])   
                            
                            if model:
                                
                                lReceive = iConstraint.ConstrainObject(model) 
          
                                if lReceive:
                                    
                                    lSend = iConstraint.SetAsSource(model)
                                    glo.gScene.Evaluate()
                                    
                                    if lSend:
                                        
                                        iConstraint.SetBoxPosition(lSend, 200, 200 * i)
                                        lSend.UseGlobalTransforms = False
                
                                    vAdd1 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd1:
                                        iConstraint.SetBoxPosition(vAdd1, 500, 200 * i)
                                        
                                    vAdd2 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd2:
                                        iConstraint.SetBoxPosition(vAdd2, 500, 200 * i + 25)
                                        
                                    vAdd3 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd3:
                                        iConstraint.SetBoxPosition(vAdd3, 500, 200 * i + 50)
                                        
                                    glo.gScene.Evaluate()
                                    
                                    if lReceive:
                                        print "lReceive: " + lReceive.Name
                                        iConstraint.SetBoxPosition(lReceive, 800, 200 * i)
                                        lReceive.UseGlobalTransforms = False
                                    
                                    i = i + 1
                                    if lSend and lReceive:
                        
                                        vector = node.partition("_XYZ_")[2]
                                        if vector == "Position":
                                            vector = "Translation"
                                        
                                        RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Position", vAdd1, 'V1')
                                        RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Rotation", vAdd2, 'V1')
                                        RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Scale",    lReceive, "Lcl Scaling")
    
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Translation", vAdd1, 'V2')
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Rotation", vAdd2, 'V2')
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Scaling", vAdd3, 'V2')
                                        if model.Name != "FACIAL_jaw":
                                            RS.Utils.Scene.ConnectBox(vAdd1, 'Result', lReceive, "Lcl Translation")
                                        RS.Utils.Scene.ConnectBox(vAdd2, 'Result', lReceive, "Lcl Rotation")
                        
                        CtrlArray = []
                        for iNode in iBox.AnimationNodeInGet().Nodes:
                            Name = iNode.Name
                            if Name.startswith("c_"):
                                Name = iNode.Name[2:len(iNode.Name)]
    
                            model = FBFindModelByLabelName(Name.partition("_XYZ_")[0])
                            
                            if model:
                                if not model in CtrlArray:
                                    CtrlArray.append(model)
                        
                        i = 0       
                        for model in CtrlArray:
                            lSend = iConstraint.SetAsSource(model)
                            if lSend:
                                iConstraint.SetBoxPosition(lSend, -6000, 200 * i)
                                lSend.UseGlobalTransforms = False
                            i = i + 1
                            for iNode in iBox.AnimationNodeInGet().Nodes:
                                Name = iNode.Name
                                if Name.startswith("c_"):
                                    Name = iNode.Name[2:len(iNode.Name)]
                                if Name.startswith(model.Name):
                                    if not model.Name in gOnFaceControls:
                                        vector = None
                                        if Name.partition("_XYZ")[2].partition("_")[2] == "Pos":
                                            vector = "Lcl Translation"
                                        elif Name.partition("_XYZ")[2].partition("_")[2] == "Rot": 
                                            vector = "Lcl Rotation"
                                        elif Name.partition("_XYZ")[2].partition("_")[2] == "Scale":
                                            vector = "Lcl Scaling"
                                        RS.Utils.Scene.ConnectBox(lSend, vector, iBox, Name)
                                        if vector == None:
                                            RS.Utils.Scene.ConnectBox(lSend, Name[len(lSend.Name.partition(" ")[0]) + 3:len(Name)] + "_CA", iBox, Name)
                                    else:
                                        if "Scale" in Name:
                                            if Name.partition("_XYZ")[2].partition("_")[2] == "Scale":
                                                vector = "Lcl Scaling"
                                            RS.Utils.Scene.ConnectBox(lSend, vector, iBox, Name)
                                        
    
                lFacialRoot = FBFindModelByLabelName("FACIAL_facialRoot")
                lFacialJaw = FBFindModelByLabelName("FACIAL_jaw")
                if lFacialRoot.ClassName() == 'FBModelNull':
                    lFacialRoot.PropertyList.Find("Look").Data = 1
                    lFacialRoot.PropertyList.Find("Size").Data = 10
                
                # Remove existing FACIAL_jaw_Null before making a new one.
                lFacialJawNulls = FBComponentList()
                FBFindObjectsByName("FACIAL_jaw_Null*",lFacialJawNulls,False,True)
                for iNulls in lFacialJawNulls :
                    iNulls.FBDelete()
                
                lFacialRootClone = lFacialRoot.Clone()
                lFacialRootClone.Name = "FACIAL_jaw_Null"
                lFacialRootClone.Parent = lFacialRoot
                
                lFacialRootClone.Translation.Data = FBVector3d(0,0,0)
                glo.gScene.Evaluate()
                lFacialRootClone.Rotation.Data = FBVector3d(0,0,0)
                glo.gScene.Evaluate()
                lFacialRootClone.Scaling.Data = FBVector3d(1,1,1)
                glo.gScene.Evaluate()
                lFacialRootClone.PropertyList.Find("RotationPivot").Data = lFacialJaw.Translation.Data
                glo.gScene.Evaluate()
                
                lFacialJaw.Parent = lFacialRootClone
                glo.gScene.Evaluate()
                
                ### Remove unneeded nodes
                deleteMe = []
                organizeList = (
                    'RS_eyelidLowerInner_L_CTRL',
                    'RS_eyelidLowerInner_R_CTRL',
                    'RS_eyelidLowerOuter_L_CTRL',
                    'RS_eyelidLowerOuter_R_CTRL',
                    'RS_eyelidUpperInner_L_CTRL',
                    'RS_eyelidUpperInner_R_CTRL',
                    'RS_eyelidUpperOuter_L_CTRL',
                    'RS_eyelidUpperOuter_R_CTRL',
                    'eyelidLowerInner_L_CTRL',
                    'eyelidLowerInner_R_CTRL',
                    'eyelidLowerOuter_L_CTRL',
                    'eyelidLowerOuter_R_CTRL',
                    'eyelidUpperInner_L_CTRL',
                    'eyelidUpperInner_R_CTRL',
                    'eyelidUpperOuter_L_CTRL',
                    'eyelidUpperOuter_R_CTRL'
                )
                for each in organizeList :
                    deleteIt = FBFindModelByLabelName(each)
                    if deleteIt :
                        if deleteIt.Parent == None :
                            deleteMe.append(deleteIt)
                for each in deleteMe :
                    each.FBDelete()
                
                
            ##    for iConstraint in glo.gConstraints:
            ##        if iConstraint.Selected == True:
            ##            lConstraint = iConstraint
            ##            break
                        
                if iConstraint != None:
                    
                    for iBox in iConstraint.Boxes:
                        if iBox.Name.startswith(iConstraint.Name):
                            
                            lConstrainedObject = iConstraint.ConstrainObject(lFacialRootClone)
                            lConstrainedObject.UseGlobalTransforms = False
                            iConstraint.SetBoxPosition(lConstrainedObject, -1200, 0)
                            
                            lSourceObject = iConstraint.SetAsSource(lFacialRootClone)
                            lSourceObject.UseGlobalTransforms = False
                            iConstraint.SetBoxPosition(lSourceObject, -1900, 0)
                            
                            lAddBox = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                            iConstraint.SetBoxPosition(lAddBox, -1550, 50)
                            
                            lAddBox1 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                            iConstraint.SetBoxPosition(lAddBox1, -1550, -50)
                            
                            RS.Utils.Scene.ConnectBox(lSourceObject, 'Lcl Rotation', lAddBox, 'V2')
                            RS.Utils.Scene.ConnectBox(lSourceObject, 'Lcl Translation', lAddBox1, 'V2')
                            
                            RS.Utils.Scene.ConnectBox(iBox, 'FACIAL_jaw_XYZ_Rotation', lAddBox, 'V1')
                            RS.Utils.Scene.ConnectBox(iBox, 'FACIAL_jaw_XYZ_Position', lAddBox1, 'V1')
                            
                            RS.Utils.Scene.ConnectBox(lAddBox, 'Result', lConstrainedObject, 'Lcl Rotation')
                            RS.Utils.Scene.ConnectBox(lAddBox1, 'Result', lConstrainedObject, 'Lcl Translation')
                            
        lRelConstraint.Active = True
    
    return pluginPresent

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_ConnectRigInABox1
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################  

def rs_ConnectRigInABox1():

    lCharacterNode = None
    lCharacterName = rs_FindCharacterName()
    gOnFaceControls = rs_OnFaceControls()
    
    if "OR - Box" in lCharacterName:
        lRelConstraint = FBConstraintRelation(lCharacterName)
        lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Constraints"   
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', lCharacterName)
    else:
        lRelConstraint = FBConstraintRelation(lCharacterName + ' 3Lateral')
        lTag = lRelConstraint.PropertyCreate('rs_Type', FBPropertyType.kFBPT_charptr, "", False, True, None)
        lTag.Data = "rs_Constraints"   
        lCharacterNode = lRelConstraint.CreateFunctionBox('GTAV', (lCharacterName + ' 3Lateral'))
    Array = []
    
    if lCharacterNode:
        lRelConstraint.SetBoxPosition(lCharacterNode, -3000, 0)
        lConFolder = rs_IK_ConstraintsFolder()
        lConFolder.Items.append(lRelConstraint)    
    
        for iComponent in glo.gComponents:     
            if "CTRL" in iComponent.Name or "CIRC" in iComponent.Name:
                for iProp in iComponent.PropertyList:
                    if iProp.IsUserProperty():
                        try:
                            iProp.SetAnimated(True)
                        except:
                            None
        
        for iConstraint in glo.gConstraints:
            
            if iConstraint.Name == lRelConstraint.Name:
                for iBox in iConstraint.Boxes:
                    if iBox.Name.partition(" ")[0].startswith(lRelConstraint.Name.partition(" ")[0]):
                        i = 0
                        nodeArray = []
                        for iNode in iBox.AnimationNodeOutGet().Nodes:
        
                            nodeArray.append(iNode.Name)
                        
                        nodeArray = sorted(nodeArray)
                        
                        for node in nodeArray:
                            
                            model = FBFindModelByLabelName(node.partition("_XYZ_")[0])   
                            
                            if model and node.partition("_XYZ_")[0] not in gOnFaceControls:
                                
                                lReceive = iConstraint.ConstrainObject(model) 
          
                                if lReceive:
                                    
                                    lSend = iConstraint.SetAsSource(model)
                                    glo.gScene.Evaluate()
                                    
                                    if lSend:
                                        
                                        iConstraint.SetBoxPosition(lSend, 200, 200 * i)
                                        lSend.UseGlobalTransforms = False
                
                                    vAdd1 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd1:
                                        iConstraint.SetBoxPosition(vAdd1, 500, 200 * i)
                                        
                                    vAdd2 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd2:
                                        iConstraint.SetBoxPosition(vAdd2, 500, 200 * i + 25)
                                        
                                    vAdd3 = iConstraint.CreateFunctionBox('Vector', 'Add (V1 + V2)')
                                    if vAdd3:
                                        iConstraint.SetBoxPosition(vAdd3, 500, 200 * i + 50)
                                        
                                    glo.gScene.Evaluate()
                                    
                                    if lReceive:
                                        print "lReceive: " + lReceive.Name
                                        iConstraint.SetBoxPosition(lReceive, 800, 200 * i)
                                        lReceive.UseGlobalTransforms = False
                                    
                                    i = i + 1
                                    if lSend and lReceive:
                        
                                        vector = node.partition("_XYZ_")[2]
                                        if vector == "Position":
                                            vector = "Translation"
                                        
                                        RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Position", vAdd1, 'V1')
                                        RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Rotation", vAdd2, 'V1')
                                        #RS.Utils.Scene.ConnectBox(iBox, node.partition("_XYZ_")[0] + node.partition("_XYZ_")[1] + "Scale",    lReceive, "Lcl Scaling")
        
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Translation", vAdd1, 'V2')
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Rotation", vAdd2, 'V2')
                                        RS.Utils.Scene.ConnectBox(lSend, "Lcl Scaling", vAdd3, 'V2')
                                        RS.Utils.Scene.ConnectBox(vAdd1, 'Result', lReceive, "Lcl Translation")
                                        RS.Utils.Scene.ConnectBox(vAdd2, 'Result', lReceive, "Lcl Rotation")
                        
                        CtrlArray = []
                        for iNode in iBox.AnimationNodeInGet().Nodes:
                            Name = iNode.Name
                            if Name.startswith("c_"):
                                Name = iNode.Name[2:len(iNode.Name)]
                            
                            if Name.partition("_XYZ_")[0] in gOnFaceControls:
                                
                                model = FBFindModelByLabelName(Name.partition("_XYZ_")[0])
                                
                                if model:
                                    if not model in CtrlArray:
                                        CtrlArray.append(model)
                        
                        i = 0       
                        for model in CtrlArray:
                            lSend = iConstraint.SetAsSource(model)
                            if lSend:
                                iConstraint.SetBoxPosition(lSend, -6000, 200 * i)
                                lSend.UseGlobalTransforms = False
                            i = i + 1
                            for iNode in iBox.AnimationNodeInGet().Nodes:
                                Name = iNode.Name
                                if Name.startswith("c_"):
                                    Name = iNode.Name[2:len(iNode.Name)]
                                if Name.startswith(model.Name):
                                    vector = None
                                    if Name.partition("_XYZ")[2].partition("_")[2] == "Pos":
                                        vector = "Lcl Translation"
                                    elif Name.partition("_XYZ")[2].partition("_")[2] == "Rot":
                                        vector = "Lcl Rotation"
                                    elif Name.partition("_XYZ")[2].partition("_")[2] == "Scale":
                                        vector = "Lcl Scaling"
                                    RS.Utils.Scene.ConnectBox(lSend, vector, iBox, Name)
                                    if vector == None:
                                        RS.Utils.Scene.ConnectBox(lSend, Name[len(lSend.Name.partition(" ")[0]) + 3:len(Name)] + "_CA", iBox, Name)
                                        
        lRelConstraint.Active = True
