import fbx
import FbxCommon

import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Utils import Namespace, Scene
from RS.Utils.Scene import Component


def FixHierarchyNameIssues(characterReference):
    '''
    Fixes naming issues e.g. 'Player_One:Player_Zero'
    bug 2568559
    '''
    #Get latest resource file first
    Perforce.Sync([characterReference.Path], True)

    #Get the resource name
    resourceName = characterReference.Path[characterReference.Path.rfind('\\') + 1 : len(characterReference.Path)- 4]

    if characterReference._getCharacterComponent().Name != resourceName:
        characterReference._getCharacterComponent().Name = resourceName

    dummy = mobu.FBFindModelByLabelName('{0}:{1}'.format(characterReference.Namespace, "Dummy01"))

    if dummy:
        for idx in range(len(dummy.Children)):
            if not dummy.Children[idx].Name in ['mover', 'Global_Contact', 'ToyBox Marker']:
                if dummy.Children[idx].Name != resourceName:
                    dummy.Children[idx].Name = resourceName

        sdkManager, scene = FbxCommon.InitializeSdkObjects()

        # Load the fbx file into memory.
        result = FbxCommon.LoadScene(sdkManager, scene, characterReference.Path)

        if result:
            for idx in range(scene.GetRootNode().GetChildCount()):
                childNode = scene.GetRootNode().GetChild(idx)
                for childNodeIdx in range(childNode.GetChildCount()):
                    if childNode.GetChild(childNodeIdx).GetName() == 'Dummy01':
                        dummy.Parent.Name = str(childNode.GetName())

            sdkManager.Destroy()


def FixOhScaleTranslateProblem(characterReference):
    '''
    It fixes a scale/translation problem that 3 model nulls(children of Zeroing_Null)
    after an update/swap from a version that doesn't have that Null in the hierachy, to a version that has it.
    ex. v4 Male_Skeleton to v17
    '''
    zeroingNull = Scene.FindModelByName('{0}:ZeroingNull'.format( characterReference.Namespace ), True)

    if not zeroingNull:
        mobu.FBMessageBox('Rockstar', "Zeroing Null does not exist in the hierachy. The nulls don't need fixing.", 'OK')
        return

    ohFacingDirection = Scene.FindModelByName('{0}:OH_FacingDirection'.format( characterReference.Namespace), True)
    ohUpperFixupDirection = Scene.FindModelByName('{0}:OH_UpperFixupDirection'.format( characterReference.Namespace), True)
    ohIndependentMoverDirection = Scene.FindModelByName('{0}:OH_IndependentMoverDirection'.format( characterReference.Namespace), True)

    ohModelList = [ohFacingDirection, ohUpperFixupDirection, ohIndependentMoverDirection]

    propertyList =[]
    for ohModel in ohModelList:
        #We need to enable DOF
        ohModel.PropertyList.Find("Enable Translation DOF").Data = False
        ohModel.PropertyList.Find("Enable Scaling DOF").Data = False

        propertyList.append( ohModel.PropertyList.Find('Lcl Translation'))
        propertyList.append( ohModel.PropertyList.Find('Lcl Scaling'))

    #Remove all previous keys on Translation and Scale
    Scene.ClearAnimationData(propertyList)

    #Scale vector is the same for all 3 items, translation differes
    s1 = mobu.FBVector3d(5.30, 3.50, 0.04)
    t1 = mobu.FBVector3d(0, 140, 0)
    t2 = mobu.FBVector3d(0, 100, 0)
    t3 = mobu.FBVector3d(0, 0, 0)

    currentTake = Globals.System.CurrentTake

    #We need to go through all the takes and all the layers and fix the translate/scale
    for take in Globals.System.Scene.Takes:
        Globals.System.CurrentTake = take
        for layerNumber in xrange( Globals.System.CurrentTake.GetLayerCount()):
            Globals.System.CurrentTake.GetLayer(layerNumber).SelectLayer(True, True)

            ohFacingDirection.SetVector(t1, mobu.FBModelTransformationType.kModelTranslation)
            ohUpperFixupDirection.SetVector(t2, mobu.FBModelTransformationType.kModelTranslation)
            ohIndependentMoverDirection.SetVector(t3, mobu.FBModelTransformationType.kModelTranslation)

            for ohModel in ohModelList:
                ohModel.SetVector(s1, mobu.FBModelTransformationType.kModelScaling)

                #Setting the Min/Max DOFs
                ohModel.PropertyList.Find("TranslationMinX").Data = True
                ohModel.PropertyList.Find("TranslationMinY").Data = True
                ohModel.PropertyList.Find("TranslationMinZ").Data = True
                ohModel.PropertyList.Find("TranslationMin").Data = mobu.FBVector3d(0,0,0)
                ohModel.PropertyList.Find("TranslationMaxX").Data = True
                ohModel.PropertyList.Find("TranslationMaxY").Data = True
                ohModel.PropertyList.Find("TranslationMaxZ").Data = True

                ohModel.PropertyList.Find("ScalingMinX").Data = True
                ohModel.PropertyList.Find("ScalingMinY").Data = True
                ohModel.PropertyList.Find("ScalingMinZ").Data = True
                ohModel.PropertyList.Find("ScalingMin").Data = mobu.FBVector3d(0,0,0)
                ohModel.PropertyList.Find("ScalingMaxX").Data = True
                ohModel.PropertyList.Find("ScalingMaxY").Data = True
                ohModel.PropertyList.Find("ScalingMaxZ").Data = True
                ohModel.PropertyList.Find("ScalingMax").Data = mobu.FBVector3d(5.30, 3.50, 0.04)

            ohFacingDirection.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,0,140)
            ohUpperFixupDirection.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,0,100)
            ohIndependentMoverDirection.PropertyList.Find("TranslationMax").Data = mobu.FBVector3d(0,0,0)

            #Disabling the DOFs
            for ohModel in ohModelList:
                ohModel.PropertyList.Find("Enable Translation DOF").Data = True
                ohModel.PropertyList.Find("Enable Scaling DOF").Data = True

            Globals.System.Scene.Evaluate()

    Globals.System.CurrentTake = currentTake

    #The 2 contraints: OH_IndependentMover_Constraint, OH_UpperFixup_Constraint need to be deactivated
    constraintList = Scene.Component.GetComponents(Namespace = characterReference.Namespace,
                                                            TypeList = [mobu.FBConstraint],
                                                            DerivedTypes = True,
                                                            IncludeRegex = '(?i)OH_IndependentMover_Constraint|OH_UpperFixup_Constraint')

    for contraint in constraintList:
        contraint.Active = False
    return


def GetProblemCharacterExtensions(characterReference):
    '''
    bug 2588122
    Some character extension sub objects don't have the default scale values of (1, 1, 1).
    This function returns them.
    '''
    modelList =[]

    extensionList = Scene.Component.GetComponents(Namespace = characterReference.Namespace,
                                                            TypeList = [mobu.FBCharacterExtension],
                                                            DerivedTypes = True,
                                                            ExcludeRegex = '(?i)OH')
    for extension in extensionList:
        for i in xrange(extension.GetSubObjectCount()):
            if not type(extension.GetSubObject(i))== mobu.FBPose:
                if extension.GetSubObject(i).PropertyList.Find( 'Lcl Scaling').Data != mobu.FBVector3d(1, 1, 1):
                    modelList.append(extension.GetSubObject(i))

    return modelList


def FixScaleOnCharacterExtensions(modelList):
    '''
    bug 2588122
    Sets the Scale to (1, 1, 1) on the elements from the provided list
    '''
    propertyList = []

    for model in modelList:
        if model.PropertyList.Find('Lcl Scaling'):
            propertyList.append(model.PropertyList.Find( 'Lcl Scaling'))

    #Remove all previous keys on Scale
    Scene.ClearAnimationData(propertyList)

    currentTake = Globals.System.CurrentTake

    #We need to go through all the takes and all the layers and fix the translate/scale
    for take in Globals.System.Scene.Takes:
        Globals.System.CurrentTake = take
        for layerNumber in xrange( Globals.System.CurrentTake.GetLayerCount()):
            Globals.System.CurrentTake.GetLayer(layerNumber).SelectLayer(True, True)

            #Set it to the new value
            for model in modelList:
                model.PropertyList.Find('Lcl Scaling').Data = mobu.FBVector3d(1, 1, 1)

            Globals.System.Scene.Evaluate()

    Globals.System.CurrentTake = currentTake


def switchOffDevices(self):
    '''
    ability to toggle expressions on and off for a character
    url:bugstar:2747962
    '''
    for device in Globals.Devices:
        if Component.GetNamespace(device) == self.Namespace:
            device.Online = False


def switchOnDevices(self):
    '''
    ability to toggle expressions on and off for a character
    url:bugstar:2747962
    '''
    for device in Globals.Devices:
        if Component.GetNamespace(device) == self.Namespace:
            device.Online = True

class ObjectSetData(object):
    def __init__(self,
                 targetProperty):
        self.targetModelFullName = targetProperty.GetOwner().FullName

        self.targetPropertyName = targetProperty.Name

    def connectMetaData(self,
                        inputObject,
                        objectProperty):
        inputObject.ConnectDst(objectProperty)

    def restoreConnection(self,
                          inputCharacter):
        if not isinstance(inputCharacter,
                          mobu.FBCharacter):
            return

        targetNode = mobu.FBFindObjectByFullName(self.targetModelFullName)
        if targetNode is None:
            return

        targetProperty = targetNode.PropertyList.Find(self.targetPropertyName)
        if targetProperty is None:
            return

        self.connectMetaData(inputCharacter,
                             targetProperty)


def StoreObjectSetConnections(inputNamespace):
    sceneNamespace = Namespace.GetFBNamespace(inputNamespace)
    currentCharacters = Namespace.GetContentsByType(sceneNamespace,
                                                    mobu.FBCharacter,
                                                    True)

    if not currentCharacters:
        return []

    objsetSetValues = []
    for index in xrange(currentCharacters[0].GetDstCount()):
        currentNode = currentCharacters[0].GetDst(index)

        if not isinstance(currentNode,
                          mobu.FBPropertyListObject):
            continue

        objsetSetValues.append(ObjectSetData(currentNode))

    return objsetSetValues