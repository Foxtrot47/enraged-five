import os

from RS import Config
from RS import ProjectData
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapGsSkeleton(MocapComplex):
    '''
    Represents a mocap gs Skeleton reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'MocapGsSkeleton'
    FormattedTypeName = 'Mocap gs Skeleton'
    TypePathList = [ProjectData.data.GetGsSkeletonPath()]
    FileType = 'fbx'

    @Logger("Mocap GS Skeleton - Delete")
    def _delete(self):
        '''
        This function is run when a gs skel is deleted
        '''
        # remove the double namespaces in handle objects
        self._log.LogMessage("Checking and fixing double namespaces in handles.")
        self.FixHandleDoubleNamespace()

        # run the rest of the delete
        super(MocapGsSkeleton, self)._delete()
