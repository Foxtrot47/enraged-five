import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapBasePreviz(MocapComplex):
    '''
    Represents a mocap base previz asset reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'MocapBasePreviz'
    FormattedTypeName = 'Mocap BasePreviz'
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz,'north', 'basePreviz', 'baseAssets'),
                    os.path.join(Config.VirtualProduction.Path.Previz,'nyc', 'basePreviz', 'baseAssets'),
                    os.path.join(Config.VirtualProduction.Path.Previz,'tor', 'basePreviz', 'baseAssets'),
                    os.path.join(Config.VirtualProduction.Path.Previz,'sd', 'basePreviz', 'baseAssets'),
                    ]
    FileType = 'fbx'

    def __init__(self, modelNull, log):
        '''
        Call base class constructor
        '''
        super(MocapBasePreviz, self).__init__(modelNull, log)

    @Logger("Mocap Base Previz - Create")
    def _create(self):
        '''
        This function gets run only when a character is created. Calls the base class and then
        fixes the UFC device.
        '''
        super(MocapBasePreviz, self)._create()

        # update groups
        self._log.LogMessage("Updating base previz groups with new base previz reference created.")
        self.UpdateGroups()