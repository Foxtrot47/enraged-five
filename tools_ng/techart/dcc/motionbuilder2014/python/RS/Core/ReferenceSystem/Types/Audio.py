'''
Usage:
    This contains logic to help interaction with AUDIO references
Author:
    Ross George
    Bianca Rudareanu
'''
import os
import re
import uuid

import pyfbsdk as mobu

from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Decorators import Logger
from RS import Perforce

class Audio(Complex):
    '''
    Represents a prop reference in a scene
    '''
    PropertyTypeName    = 'Audio'
    FormattedTypeName   = 'Audio'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'audio'),
                           os.path.join('art', 'dev','animation', 'resources', 'audio'),
                           ]
    FileType            = 'wav'

    def _create(self):
        '''
        Creates the audio asset
        '''
        self._update()

    @Logger("Audio - Updating")
    def _update(self, force=True):
        '''
        Updates the prop reference with latest data. Force does nothing here currently.
        '''
        self._log.LogMessage("Updating Audio: {0}".format(self.Namespace))

        audioPath = self.Path

        if os.path.exists(audioPath):
            audioClip = mobu.FBAudioClip(audioPath)
            audioClip.LongName = '{0}:{1}'.format(self.Namespace, audioClip.Name)
            audioClip.DstIn = mobu.FBTime(0, 0, 0, 0)

        #Update Reference P4 Version
        self._p4VersionProperty.Data = str(Perforce.GetFileState(self.Path).HaveRevision)

