'''
Usage:
    This contains logic to help interaction with AMBIENT FACE references
Author:
    Ross George
    Bianca Rudareanu
'''
import os

from RS.Core.ReferenceSystem.Types.Base import Base


class AmbientFace( Base ) :
    '''
    Represents a ambient face reference in a scene
    ''' 
    
    PropertyTypeName    = 'AmbientFace'
    FormattedTypeName   = 'AmbientFace'
    #ToDo - Need to finish face logic
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'face'),
                           os.path.join('art', 'dev','animation', 'resources', 'face'), 
                           ]
    FileType            = 'fbx'
    
    def _proxySetup ( self ):
        '''
        Turns the current reference into a proxy
        '''    