'''
Usage:
    This contains logic to help interaction with CHARACTER references
Author:
    Ross George
    Kat Bodey
    Bianca Rudareanu (KIA)
'''

import pyfbsdk as mobu

import fbx
import FbxCommon
import uuid
import os
import re
import operator
from ctypes import cdll

from RS import _ProjectsData, Config, Globals, Perforce, ProjectData
from RS.Core import Metadata, ReferenceSystem
import RS.Core.Face.Lib # as FaceLib
from RS.Core.Animation import IgToCs, Lib
from RS.Core.ReferenceSystem.Types.Base import Base
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem.Utils import Character as RefCharacter
from RS.Utils import ControlSet, FBXSDK, Namespace, Scene
from RS.Utils.Scene import Component, Constraint, Model, Plug
from RS.Tools import ToyBox
from RS.Core.ReferenceSystem import Exceptions
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Utils import Exception as RSException



class CharacterUpdateType:
    MINOR = 1
    MAJOR = 2

class CharacterOutfit(object):
    '''
    Represents an outfit that is tied to a character. An outfit maintains a list of model names
    that should be shown/hidden when the outfit is active. It also maintains a list of accessories
    which can be toggled for an outfit.
    '''

    def __init__(self, name):
        '''
        Initializes the character outfit with the passed name.
        '''
        self.Name = name
        self._accessoryDict = dict()
        self._modelNameSet = set()

    def AddModelName(self, modelName):
        '''
        Adds the passed model with name to the outfit
        '''
        self._modelNameSet.add(modelName)

    def AddAccessory(self, accessory):
        '''
        Creates and returns the accessory.
        '''
        self._accessoryDict[accessory.Name] = accessory

    def GetAccessory(self, accessoryName):
        '''
        Returns the accessory with the passed name.
        '''
        return self._accessoryDict[accessoryName]

    def GetAccessoryNameList(self):
        '''
        Gets a full sorted list of all the accessory names that relate to the outfit.
        '''
        return sorted(self._accessoryDict.keys())

    def GetAccessoryList(self):
        '''
        Gets a full list of all the accessories that relate to the outfit.
        '''
        return self._accessoryDict.values()

    def GetModelNameList(self):
        '''
        Gets a list of all the model names related to the outfit.
        '''
        return sorted(self._modelNameSet)


class CharacterOutfitAccessory(object):
    '''
    Represents an outfit accessory - an optional extra that can be toggled on/off.
    '''

    def __init__(self, name):
        '''
        Initializes the accessory with the passed name.
        '''
        self.Name = name
        self._modelNameSet = set()

    def AddModelName(self, modelName):
        '''
        Adds the passed model name to the accessory
        '''
        self._modelNameSet.add(modelName)

    def GetModelNameList(self):
        '''
        Gets a list of all the model names related to the accessory.
        '''
        return sorted(self._modelNameSet)

class Character(Complex):
    '''
    Represents a character reference in a scene
    '''
    PropertyTypeName = 'Characters'
    FormattedTypeName = 'Character'
    TypePathList = [os.path.join('art', 'animation', 'resources', 'characters'),
                    os.path.join('art', 'dev', 'animation', 'resources', 'characters')
                    ]
    FileType = 'fbx'

    def __init__(self, modelNull, log):
        '''
        Initialises the CHARACTER reference with the passed model null. Will call the base initializor as well.
        '''
        # Call base class constructor
        super(Character, self).__init__(modelNull, log)

        # List of supported constraint types when updating character using _majorUpdate
        self._supportedConstraints = [Constraint.PARENT_CHILD,
                                      Constraint.ROTATION,
                                      Constraint.POSITION,
                                      Constraint.SCALE]

        self._outfitMetaFile = self._getOutfitMetaFile()
        self._characterMetaFile = self._getCharacterMetaFile()
        self._parseOutfitData()
        self._log = log

    @Logger("Character - Create")
    def _create(self):
        '''
        This function gets run only when a character is created. Calls the base class and then
        fixes the UFC device.
        '''
        super(Character, self)._create()

        # Fix face and expressions.
        self._log.LogMessage("Fixing Expression Devices.")
        self._fixExpressionDevices()

        # Process outfit - will default to first outfit in list (if there are any)
        self._log.LogMessage("Processing Outfit.")
        self._processOutfit()

    def _getOutfitNameProperty(self):
        '''
        Gets or creates the outfit property for the character reference. So we can remember what
        the outfit was set too between scene saves.
        '''
        # To do - fix comment above
        outfitNameProperty = self._modelNull.PropertyList.Find(ReferenceSystem.STR_OUTFIT)

        # Setting up for first time.
        if outfitNameProperty == None:
            outfitNameProperty = self._modelNull.PropertyCreate(ReferenceSystem.STR_OUTFIT,
                                                                mobu.FBPropertyType.kFBPT_charptr,
                                                                'String',
                                                                False,
                                                                True,
                                                                None)

        # Verify that the outfit property is a valid outfit. If not set it to the first outfit or NONE
        outfitNameList = self.GetOutfitNameList()
        if outfitNameProperty.Data not in outfitNameList:
            if len(outfitNameList) == 0:
                outfitNameProperty.Data = "NONE"
            else:
                outfitNameProperty.Data = str(outfitNameList[0])
        return outfitNameProperty

    @Logger("Character - Get namespace property ")
    def _getAccessoryNameListProperty(self):
        '''
        Gets or creates the accessory name list FBProperty for the current outfit. Will return None if we are
        not currently on a valid outfit. This allow us to remember what accessories were set for an outfit
        between scene saves.
        '''
        # If we have an outfit active
        outfit = self.GetCurrentOutfit()
        if outfit is not None:

            # Check for or add the accessory property
            accessoryNameListProperty = self._modelNull.PropertyList.Find("{0}.Accessories".format(outfit.Name))
            if accessoryNameListProperty == None:
                accessoryNameListProperty = self._modelNull.PropertyCreate("{0}.Accessories".format(outfit.Name),
                                                                           mobu.FBPropertyType.kFBPT_stringlist,
                                                                           'stringlist',
                                                                           False,
                                                                           True,
                                                                           None)

            # Clean up accessory name list - remove ones tha aren't valid - not sure if this really needed
            accessoryNameList = outfit.GetAccessoryNameList()
            for accessoryName in list(accessoryNameListProperty):
                if accessoryName not in accessoryNameList:
                    accessoryNameListProperty.remove(accessoryName)

            return accessoryNameListProperty
        return None

    @Logger("Character - Remove Facial")
    def _removeFacial(self):
        '''
        This is run when a character type is going through a major update
        It remvoes all facial bones, before the new character is brought in.
        url:bugstar:2698423
        '''
        self._log.LogMessage("Removing facial bones for major update.")
        deleteComponentList = []

        # remove all facial bones
        deleteComponentList.extend(self.GetFaceBones())
        deleteComponentList.extend(self.GetFaceControls())

        # Execute the delete
        Component.Delete(deleteComponentList)

    @Logger("Character - Strip")
    def _strip(self):
        '''
        Completely strips the character back to it's bare essentials.
        All we should be left with are the core of the skeleton and the
        face controls.
        '''
        deleteComponentList = []

        # Add visual models
        deleteComponentList.extend(self.GetVisualModelList())

        # Add disposable bones list (roll bones, mass handlers etc...)
        deleteComponentList.extend(self.GetDisposableBonesList())

        # Add all facial constraints
        deleteComponentList.extend(self.GetFaceConstraints())

        # Add devices
        deleteComponentList.extend([component for component in Globals.Components
                                     if Component.GetNamespace(component) == self.Namespace
                                     and isinstance(component, mobu.FBDevice)])

        # Add expression constraints
        deleteComponentList.extend(self._getOldExpressionConstraintList())

        # Add old lookat rig
        deleteComponentList.extend(self._getOldLookAtRigList(self.Namespace))

        # Execute the delete
        self._log.LogMessage("Removing components for a strip")
        Component.Delete(deleteComponentList)

    def _getOldExpressionConstraintList(self):
        '''
        Get expression constraint list
        '''
        data = _ProjectsData.RDR.RdrData()

        expressionConstraints = []

        refConstraints = Component.GetComponents(Namespace=self.Namespace,
                                                 TypeList=[mobu.FBConstraint],
                                                 DerivedTypes=False)

        for constraint in refConstraints:
            if re.search(data.OldExpressionBoneNameRegex(), constraint.Name, re.IGNORECASE):
                expressionConstraints.append(constraint)

        return expressionConstraints

    @Logger("Character - Checking and fixing expressions")
    def _fixExpressionDevices(self):
        '''
        Fixes the RSExpressionSolver and UFC devices (sets up namespace and reactivates it).
        '''
        # Find devices in the namespace
        potentialDevices = [component for component in Globals.Components
                            if isinstance(component, mobu.FBDevice)
                            and Component.GetNamespace(component) == self.Namespace
                            ]

        # Should really only have one but we account for more (or less)
        targetDeviceList = []

        if(len(potentialDevices) == 1):
            targetDeviceList.append(potentialDevices[0])

        # If we have more than one then we should favour any RSExpressionSolver ones
        elif(len(potentialDevices) > 1):
            targetDeviceList = [device for device in potentialDevices if 'RSExpr' in device.Name]
            for device in potentialDevices:
                if 'RSExpr' in device.Name:
                    targetDeviceList.append(device)
                else:
                    device.FBDelete()

        if len(targetDeviceList) is 0:
            return

        for device in targetDeviceList:
            self._log.LogMessage("Activating and setting Namespace on RS Expression Device: {0}.".format(device.LongName))
            cdll.MBExprSolver.SetActiveDevice_Py(device.LongName)
            cdll.MBExprSolver.SetNamespace_Py(self.Namespace)

        # Fix UFC as well - we call this regardless of the device type due to global eye rig issues
        RS.Core.Face.Lib.fixFacial_UFC(self)

    def ReloadMetaFile(self):
        '''
        Reload the metadata file. So if user edited the .meta locally they can reload.
        '''
        self._outfitMetaFile = self._getOutfitMetaFile()
        self._characterMetaFile = self._getCharacterMetaFile()
        self._parseOutfitData()

    def _getCharacterMetaFile(self):
        '''
        Returns the metafile for the character. Or None if we don't have one.
        '''
        fbxFilePath = self.Path
        charName = os.path.splitext(os.path.basename(fbxFilePath))[0].lower()
        pathName = os.path.dirname(os.path.abspath(fbxFilePath)).lower()

        # Check character name first
        if str("ig_") in charName or str("ingame") in pathName:
            pathName = os.path.join(Config.Project.Path.Assets,
                                    'metadata',
                                    'animation',
                                    'resources',
                                    'characters',
                                    'ingame')
        elif str("cs_") in charName or str("cutscene") in pathName:
            pathName = os.path.join(Config.Project.Path.Assets,
                                    'metadata',
                                    'animation',
                                    'resources',
                                    'characters',
                                    'cutscene')
        elif str("animals") in pathName:
            pathName = os.path.join(Config.Project.Path.Assets,
                                    'metadata',
                                    'animation',
                                    'resources',
                                    'characters',
                                    'animals')

        metaFilePath = os.path.join(pathName, charName + ".meta")
        Perforce.Sync([metaFilePath], False)
        if os.path.exists(metaFilePath):
            try:
                return Metadata.ParseMetaFile(metaFilePath)
            except:
                self._log.LogWarning('Failed to parse metadata file {0}'.format(metaFilePath))
                return None
        else:
            return None

    def _getOutfitMetaFile(self):
        '''
        Returns a parsed metadata file from the outfit.meta.
        '''

        outfitMetaFilePath = os.path.join(Config.Project.Path.Export,
                                            'data',
                                            'metapeds',
                                            'outfits.pso.meta')

        Perforce.Sync(outfitMetaFilePath, False)
        if os.path.exists(outfitMetaFilePath):
            try:
                return Metadata.ParseMetaFile(outfitMetaFilePath)
            except:
                self._log.LogWarning('Failed to parse metadata file {0}'.format(outfitMetaFilePath))
                return None
        else:
            return None

    def _getOutfitModelRegex(self):
        '''
        Gets a regular expression that can be used to unhide models as appropriate with the current
        outfit and selected accessories.
        '''

        currentOutfit = self.GetCurrentOutfit()
        modelNameSet = set()
        if currentOutfit is not None:

            for modelName in currentOutfit.GetModelNameList():
                modelNameSet.add(modelName)

            for accessoryName in currentOutfit.GetAccessoryNameList():
                if self.GetAccessoryState(accessoryName):
                    accessory = currentOutfit.GetAccessory(accessoryName)
                    for modelName in accessory.GetModelNameList():
                        modelNameSet.add(modelName)

        return '|'.join(modelNameSet)

    def _processOutfit(self):
        '''
        Completely refreshes the state of the character's visual models against the current outfit
        by hiding/unhiding. Will do nothing if we don't have a valid outfit.
        '''

        self._log.LogMessage("Processing outfit.")

        currentOutfit = self.GetCurrentOutfit()
        if currentOutfit is not None:
            outfitModelRegex = self._getOutfitModelRegex()

            # Gather up matching models using regex
            visualModelMatchList = [visualModel for visualModel in self.GetVisualModelList() if
                                    re.search(outfitModelRegex, visualModel.Name, re.IGNORECASE)]

            if len(visualModelMatchList) > 0:
                for visualModel in self.GetVisualModelList():
                    if visualModel in visualModelMatchList:
                        visualModel.Visibility = True
                        visualModel.VisibilityInheritance = True
                        visualModel.Show = True
                    else:
                        visualModel.Show = False
            else:
                self._log.LogWarning("No models matched for the outfit {0}, possibly bad outfit data?".format(currentOutfit.Name))


    def _getOldLookAtRigList(self, namespace):
        '''
        creates a list of old components from the LookAt Rig
        url:bugstar:2575815

        Returns:
            List(FBComponent)
        '''
        # get oldRigDict[string] = list(FBType)
        componentDeleteList = []
        oldRigDict = ProjectData.data.OldLookAtRigDict()

        # find the components in the scene
        for key, valueList in oldRigDict.iteritems():
            for value in valueList:
                componentName = '{0}:{1}'.format(namespace, key)
                component = Scene.GetComponentByNameAndType(value, componentName)
                if component is not None:
                    componentDeleteList.append(component)

        return componentDeleteList

    @Logger("Character - Updating ")
    def _update(self, force=False):
        '''
        Performs an update on the character.
        '''

        # Validate reference and get update type (major or minor).
        # note that we do this even if the user has requested to force because it also does
        # some important validation.
        updateType = self._validateAndGetUpdateType()

        # Perform the update
        if force or updateType == CharacterUpdateType.MAJOR:
            self._majorUpdate()
        else:
            self._minorUpdate()

        if self.IsProxy:
            self._proxySetup()

        # Update Reference P4 Version of character. If the file is not in P4 we set it to -1
        fileState = RS.Perforce.GetFileState(self.Path)
        if fileState == []:
            self._setCurrentVersion(-1)
        else:
            self._setCurrentVersion(fileState.HaveRevision)

    def _checkForCharacterScale(self):
        '''
        If the source character has a 'CharacterScale' null present, we will need to ensure the
        target chaarcter also has this setup, so the data can be copied across.

        url:bugstar:2786055
        '''
        characterScale = [component for component in Globals.Components
                            if isinstance(component, mobu.FBModel)
                            and Component.GetNamespace(component) == self.Namespace
                            and component.Name == 'CharacterScale']

        if len(characterScale) == 0:
            return False
        return True

    @Logger("Character - Minor Update")
    def _minorUpdate(self):
        '''
        Performs a minor update on the reference. Essentially just brings in new visual geometry.
        If the heirarchy or resource file has changed too much this won't work that well - should
        then use the _majorUpdate. This function should not be triggered if there is more than one
        or not enough FBCharacters - it is defended against in the _getUpdateType.
        '''
        self._log.LogMessage("Starting Minor Update on reference {0}...".format(self.Namespace))
        # First we will strip (visual geometry and other elements)
        self._strip()

        # Get active state
        characterComponent = self._getCharacterComponent()
        activeInput = characterComponent.ActiveInput
        inputType = characterComponent.InputType

        # Check for Ig to Cs so it can be copied if needed
        orgIgToCs = IgToCs.IgToCs(characterComponent)
        igToCsKeyData = None
        igToCsWasCommited = False
        if orgIgToCs.isSetup():
            igToCsKeyData = orgIgToCs.getKeysForCharacterScale()
            # If the data has been commited, it needs to be reverted before it can be removed
            if orgIgToCs.canRevert() is True:
                igToCsWasCommited = True
                orgIgToCs.revertMotion()
            orgIgToCs.removeSetup(finalPlot=False)

        # Then we will merge in the new geometry - setup load options
        mergeOptions = ReferenceSystem.CreateBasicMergeOptions(self.Path)

        # Set target namespace correctly
        mergeOptions.NamespaceList = self.Namespace

        # Merge the file
        Globals.Application.FileMerge(self.Path, False, mergeOptions)

        # Push input type/state back - this gets lost otherwise and the character returns to stance
        characterComponent = self._getCharacterComponent()
        characterComponent.ActiveInput = activeInput
        characterComponent.InputType = inputType

        self._reconnectToybox()

        # check if character has character scale which needs to be re-applied
        if igToCsKeyData is not None:
            self._log.LogMessage("CharacterScale found on character")
            igToCs = IgToCs.IgToCs(characterComponent)
            igToCs.setup()
            igToCs.setKeysForCharacterScale(igToCsKeyData)
            if igToCsWasCommited is True:
                igToCs.commitMotion()

        # Set the DOF settings on mover. Not sure why this is here.
        mover = self.GetMover()
        mover.RotationActive = True
        mover.RotationMinX = True
        mover.RotationMinY = True
        mover.RotationMaxX = True
        mover.RotationMaxY = True

        # Reload current outfit
        self._processOutfit()

        # Fix expressions and face
        self._fixExpressionDevices()

        self._log.LogMessage("Minor Update finished.")

    def _IgToCS(self, sourceNamespace, targetNamespace):
        """
        Switches IG to CS characters and vice versa

        Arguments:
            sourceNamespace (pyfbsdk.FBNamespace): source namespace
            targetNamespace (pyfbsdk.FBNamespace): target namespace
        """
        # Ig to Cs Logic
        sourceCharacterComponent = self._getCharacterComponent()
        targetCharacterComponent = None

        # If the namespace does have characters, only use the first character found
        for character in Namespace.GetContentsByType(targetNamespace, mobu.FBCharacter, True)[:1]:
            targetCharacterComponent = character

        # Check for Ig to Cs so it can be copied if needed

        orgIgToCs = IgToCs.IgToCs(sourceCharacterComponent)
        igToCsKeyData = None
        igToCsWasCommited = False
        if orgIgToCs.isSetup():
            igToCsKeyData = orgIgToCs.getKeysForCharacterScale()
            # If the data has been commited, it needs to be reverted before it can be removed
            if orgIgToCs.canRevert() is True:
                igToCsWasCommited = True
                orgIgToCs.revertMotion()
            orgIgToCs.removeSetup(finalPlot=False)

        if igToCsKeyData is not None and targetCharacterComponent is not None:
            self._log.LogMessage("CharacterScale found on character")
            igToCs = IgToCs.IgToCs(targetCharacterComponent)
            igToCs.setup()
            igToCs.setKeysForCharacterScale(igToCsKeyData)
            if igToCsWasCommited is True:
                    igToCs.commitMotion()

    def _setToybox(self, sourceNamespace, targetNamespace):
        """
        Sets the toybox constraint from the source to match the target

        Arguments:
            sourceNamespace (pyfbsdk.FBNamespace): source namespace
            targetNamespace (pyfbsdk.FBNamespace): target namespace
        """
        # TODO: Need to pull this toybox code into tidier logic in separate module
        # Check if original character had a toybox relation
        self._log.LogMessage("Fixing toybox.")
        constraints = [
                       constraint for namespace in (sourceNamespace, targetNamespace)
                       for constraint in Namespace.GetContentsByType(namespace, mobu.FBConstraint, False)
                       if constraint.Name == 'mover_toybox_Control'
                       ]

        if len(constraints) == 2:
            oldMoverToyboxControl, newMoverToyboxControl = constraints
            oldMoverToyboxControlState = oldMoverToyboxControl.Active
            if oldMoverToyboxControlState is True:
                newMoverToyboxControl.Snap()
            newMoverToyboxControl.Active = oldMoverToyboxControlState

    def _copyAnimationSourceToTarget(self, sourceNamespace, targetNamespace):
        """
        copies animation from the source to the target

        Arguments:
            sourceNamespace (pyfbsdk.FBNamespace): source namespace
            targetNamespace (pyfbsdk.FBNamespace): target namespace
        """
        # Copy all property data between namespaces (non animation data)

        # Only copy on FBBox derived objects to avoid dialog boxes appearing [2630472]
        # We now no longer copy across information on the 'Dummy01' because we want the
        # UPD3DSMAX property to be maintained from import [2701439].
        components = []

        for namespace in (sourceNamespace, targetNamespace):
            components.append(Namespace.GetContentsByType(namespace, mobu.FBBox, False))
            for component in components[-1]:
                if component.Name == "Dummy01":
                    components[-1].remove(component)
                    break

        sourceComponents, targetComponents = components

        # TODO: Add a better dynamic way of resolving which properties should NOT be copied from the current character on file to the new character
        excludeList = ["GRP_R_ShoulderTwist", "GRP_L_ShoulderTwist"]
        componentMatchList = Component.GetComponentMatchList(sourceComponents, targetComponents, Exclude=excludeList)

        # Copy all property data between namespaces (non animation data)
        Component.CopyAnimationByNamespace(componentMatchList)

        for componentMatch in componentMatchList:
            if not isinstance(componentMatch.SourceComponent, mobu.FBModel):
                continue

            for constraint in Globals.Constraints:
                if Constraint.IsConstraintType(constraint, self._supportedConstraints):
                    # swap aim constraints
                    Constraint.SwapAimConstraintModels(constraint,
                                                       componentMatch.SourceComponent,
                                                       componentMatch.TargetComponent)

            for constraint in Plug.GetDstPlugList(componentMatch.SourceComponent, mobu.FBConstraint):
                if Constraint.IsConstraintType(constraint, self._supportedConstraints):
                    # Swap source models
                    Constraint.SwapSourceModel(constraint,
                                               componentMatch.SourceComponent,
                                               componentMatch.TargetComponent)

            for constraint in Plug.GetSrcPlugList(componentMatch.SourceComponent, mobu.FBConstraint):
                if Constraint.IsConstraintType(constraint, self._supportedConstraints):
                    # We also need to swap child objets(models)
                    Constraint.SwapTargetModel(constraint,
                                               componentMatch.SourceComponent,
                                               componentMatch.TargetComponent)

    def _mapCharacterSettings(self, sourceNamespace, targetNamespace):
        """
        Maps the values from the character from the source namespace to the one in the target namespace

        Arguments:
            sourceNamespace (pyfbsdk.FBNamespace): source namespace
            targetNamespace (pyfbsdk.FBNamespace): target namespace
        """
        # Get a handle on our source character component - note we have defended against no
        # characters being present earlier in the update process
        sourceCharacterComponent = self._getCharacterComponent(sourceNamespace, raiseError=False)
        targetCharacterComponent = self._getCharacterComponent(targetNamespace)

        # Remember character state
        self._log.LogMessage("Mapping over character settings.")
        sourceInputCharacter = sourceCharacterComponent.InputCharacter
        sourceActiveInput = sourceCharacterComponent.ActiveInput
        sourceInputType = sourceCharacterComponent.InputType

        # If the new character doesn't have a control rig - majority of cases. Sometimes animals have
        # control rigs embedded in the resource but we are seeing this less and less
        if not targetCharacterComponent.GetCurrentControlSet():

            # Try and connect any existing control rigs to the newly merged character
            existingControlRig = sourceCharacterComponent.GetCurrentControlSet()
            if existingControlRig is not None:
                # Standardise naming - protects against the rig being placed inside the namespace of the character
                # and being in danger of getting deleted.
                effectorList = ControlSet.GetAllEffectors(existingControlRig) or []
                for effector in effectorList:
                    Component.SetNamespace(effector, '{0}_Ctrl'.format(self.Namespace))
                Component.SetNamespace(existingControlRig, '{0}_Ctrl'.format(self.Namespace))

                # Disconnect rig from previous character component
                sourceCharacterComponent.DisconnectControlRig()

                # Connected to the one we just merged in
                targetCharacterComponent.ConnectControlRig(existingControlRig, True, False)

        # Set input type/state on the new character to match
        if sourceInputCharacter is not None:
            targetCharacterComponent.InputCharacter = sourceInputCharacter
        targetCharacterComponent.ActiveInput = sourceActiveInput
        targetCharacterComponent.InputType = sourceInputType

    @Logger("Character - Major Update")
    def _majorUpdate(self):
        '''
        Performs a major update on the reference. Will merge in latest resource into a new namespace
        and map animation across. This is used for large changes on the character reference, swapping
        characters, doing a reference refresh or if the character reference is going to be coming
        in with a control rig attached (animals).
        '''
        self._log.LogMessage("Starting Major Update on reference {0}...".format(self.Namespace))

        # Check if any unsupported constraints are linked to the character
        unsupportedConstraints = self.GetUnsupportedConstraints()
        if len(unsupportedConstraints) > 0:
            # some unsupported constraints found linked to Character
            raise Exceptions.UnsupportedConstraint()

        objsetSetValues = RefCharacter.StoreObjectSetConnections(self.Namespace)

        # Remove facial completely - face rigs can get fucked up on update if we leave the
        # facial rig and/or bones in place
        self._log.LogMessage("Removing facial.")
        self._removeFacial()

        # Setup load options
        mergeOptions = ReferenceSystem.CreateBasicMergeOptions(self.Path)

        # Create a holding/temporary namespace
        tempNamespace = '__temp__'
        mergeOptions.NamespaceList = tempNamespace

        # Merge the file
        self._log.LogMessage("Merging file {0}.".format(self.Path))
        Globals.Application.FileMerge(self.Path, False, mergeOptions)

        sourceNamespace = Globals.Scene.NamespaceGet(self.Namespace)
        targetNamespace = Globals.Scene.NamespaceGet(tempNamespace)

        # Ig to Cs Switch
        # TODO: Look into why in some cases Motion Builder becomes unstable and can't retrieve the correct properties
        # TODO: Leading to our custom errors raising even though the properties are correct in the scene.
        self._log.LogMessage("Mapping existing animation back")
        try:
            self._IgToCS(sourceNamespace, targetNamespace)
            self._copyAnimationSourceToTarget(sourceNamespace, targetNamespace)
        except RSException.PropertyError as exception:
            print exception.message

        # Check if original character had a toybox relation
        self._setToybox(sourceNamespace, targetNamespace)

        # Get map settings to the newly referenced character component
        self._mapCharacterSettings(sourceNamespace, targetNamespace)
        # Delete everything from the old namespace apart from the poses - we want to leave these [2576351]
        self._log.LogMessage("Delete Start: Deleting old reference components.")
        poses = []
        if sourceNamespace is not None:
            poses = [component for component in Namespace.GetAllContents(sourceNamespace)
                     if isinstance(component, mobu.FBCharacterPose)]

        Namespace.Add(targetNamespace, poses)

        # MergeTransactionActive not used for the following 2 Namespace methods as it was causing issues
        # with the namespace trying to update. (namespace was getting stuck as "__temp__", with no crashes).

        # delete namespace
        Namespace.Delete(sourceNamespace, deleteNamespaces=True)

        self._log.LogMessage("Delete End: Deleting old reference components.")

        # Rename the new namespace
        self._log.LogMessage("Renaming new reference components.")

        Namespace.Rename(tempNamespace, self.Namespace)

        # Reconnect toybox
        self._reconnectToybox()

        updatedCharacterComponent = self._getCharacterComponent()
        for objsetSet in objsetSetValues:
            objsetSet.restoreConnection(updatedCharacterComponent)

        self._log.LogMessage("Major Update finished.")


    def _parseOutfitData(self):
        '''
        Will parse the outfit data contained in either the outfits.meta or the characters .meta
        '''
        self._outfitDict = {}

        accessories = ('holster', 'scarf', 'bandolier', 'hat', 'satchel', 'knife', 'mask')

        # Check the outfit.meta first for outfits that relate to this PED
        accessoryList = []
        if self._outfitMetaFile is not None:
            try:
                for outfit in self._outfitMetaFile.outfits:
                    for customOutfit in outfit.customOutfits:
                        if customOutfit.pedName.lower().replace("cs_", "") in self.Path.lower():

                            # Parse as an outfit or an accessory
                            targetOutfitOrAccessory = None
                            if outfit.fullOutfit:
                                targetOutfitOrAccessory = CharacterOutfit(str(outfit.name))
                                self._outfitDict[str(outfit.name)] = targetOutfitOrAccessory

                            else:
                                targetOutfitOrAccessory = CharacterOutfitAccessory(str(outfit.name))
                                accessoryList.append(targetOutfitOrAccessory)

                            # Add the models to either the outfit or accessory
                            for asset in customOutfit.assets:
                                for propertyTag in asset.propertyTags:
                                    if (isinstance(targetOutfitOrAccessory, CharacterOutfit)):

                                        # Auto-detect accessories.
                                        isAccessory = False

                                        for accs in accessories:
                                            if accs.lower() in str(propertyTag.lower()):
                                                isAccessory = True
                                                break

                                        if isAccessory:
                                            item = CharacterOutfitAccessory(str(propertyTag))
                                            item.AddModelName(str(propertyTag))
                                            targetOutfitOrAccessory.AddAccessory(item)

                                        else:
                                            targetOutfitOrAccessory.AddModelName(str(propertyTag))

                                    else:
                                        targetOutfitOrAccessory.AddModelName(str(propertyTag))

            except Exception as e:
                self._log.LogWarning("Failed to transverse the outfit.pso.meta");
                self._log.LogWarning(e.message)


        # If we have some outfits then add in the accessories and return
        if len(self._outfitDict) > 0:
            for outfit in self._outfitDict.values():
                for accessory in accessoryList:
                    outfit.AddAccessory(accessory)
            # return

        # If we have reached this far we will try and collect information from the old character metadata file
        if self._characterMetaFile is not None:
            for metaOutfit in self._characterMetaFile.Outfits:
                if metaOutfit:
                    # Create the outfit and add it to our dict
                    outfit = CharacterOutfit(str(metaOutfit.Name))
                    self._outfitDict[str(metaOutfit.Name)] = outfit

                    # Add in the main models
                    for modelName in metaOutfit.Models:
                        outfit.AddModelName(str(modelName))

                    # Go through each of the accessories and add them as well
                    for metaAccessory in metaOutfit.Accessories:
                        accessory = CharacterOutfitAccessory(str(metaAccessory.FriendlyName))
                        accessory.AddModelName(str(metaAccessory.Name))
                        outfit.AddAccessory(accessory)

    def _validateAndGetUpdateType(self):
        '''
        Validates the chracter can be updated and returns update type based on various factors.
        '''
        # Check if we have the file path that we are about to update with - should have been
        # synced by the manager
        if not os.path.exists(self.Path):
            raise Exceptions.DeletedFile()

        # Get the character components that we have in our namespace
        characterComponents = [component for component in Globals.Characters
                               if Component.GetNamespace(component) == self.Namespace ]

        if len(characterComponents) == 0:
            raise Exceptions.CharacterComponentMissing()

        if len(characterComponents) > 1:
            self._log.LogWarning("Too many character components in the namespace - this is a sign that the character "
                                  "is in a broken state. Will attempt to continue. Setting update type to 'Major'")
            return CharacterUpdateType.MAJOR

        # First we will look in the fbx file
        sdkManager, scene = FbxCommon.InitializeSdkObjects()
        try:
            self._log.LogMessage("Analyzing resource FBX file.")
            FbxCommon.LoadScene(sdkManager, scene, self.Path)

            # Check for control rig - this means we will have to do a major update - motionbuilder
            # doesn't handle merging over the top of control rigs well
            if scene.GetControlSetPlugCount() > 0:
                self._log.LogWarning("Incoming resource has a control rig - setting update type to 'Major'")
                return CharacterUpdateType.MAJOR

            # Now check the incoming character component counts
            if scene.GetCharacterCount() == 0:
                raise Exception("No character component in the character resource file - resource "
                                "need to fix file.")
            if scene.GetCharacterCount() > 1:
                raise Exceptions.MultipleCharacterComponents()

            # Check out character component matches incoming component name
            if(str(scene.GetCharacter(0).GetName()).lower() != characterComponents[0].Name.lower()):
                self._log.LogWarning("The character component names don't match - fixing name and setting "
                                      "update type to 'Major'")
                characterComponents[0].Name = str(scene.GetCharacter(0).GetName())
                return CharacterUpdateType.MAJOR

        except:
            raise
        finally:
            # Makes sure we clean up
            sdkManager.Destroy()

        # Not all meta files have the Revision updated: in case we don't find a revision with the
        # perforce revision number that we are updating to, that has an UpdateType field. Or no
        # revisions at all, we do a minor update
        self._log.LogMessage("Looking at the metadata file to detect update type.")
        fileState = RS.Perforce.GetFileState(self.Path)
        if self._characterMetaFile and fileState != []:
            for revision in self._characterMetaFile.Revisions:
                if revision.Number >= self.CurrentVersion and revision.Number <= fileState.HaveRevision:
                    if revision.UpdateType == "Major":
                        return CharacterUpdateType.MAJOR

        # If we have reached this far then we'll go for a minor update
        self._log.LogMessage("Character in a healthy/clean state and not overriden by metadata - "
                              "setting update type to 'Minor'")
        return CharacterUpdateType.MINOR

    def _reconnectToybox(self):
        '''
        Recreate toybox constraints
        '''

        self._log.LogMessage("Reconnecting toybox.")

        toyboxExists = False
        for constraint in Globals.Constraints:
            if isinstance(constraint, mobu.FBConstraintRelation):
                if constraint.LongName == ('{0}:mover_Control_SendToToyBox'.format(self.Namespace)):
                    toyboxExists = True

        # Re-add in toybox relations, if they existed previously - ToDo: Need to pull this toybox code into tidier logic in seperate module
        if toyboxExists:
            button = mobu.FBButton() # we need to create a button so we can pass a 'Control' for the function to work, as it is using an older script
            button.Name = ('{0}:mover_Control'.format(self.Namespace))
            toyboxRelation, sendToToyboxRelation = ToyBox.rs_MakeToyBoxRelations(button, None, True)
            if toyboxRelation and sendToToyboxRelation:
                toyboxRelation.Active = False
                sendToToyboxRelation.Active = False

    @Logger("Character - Switch Devices Off")
    def switchOffDevices(self):
        '''
        ability to toggle expressions on and off for a character
        url:bugstar:2747962
        '''
        self._log.LogMessage("Switching Off Devices")
        RefCharacter.switchOffDevices(self)

    @Logger("Character - Switch Devices On")
    def switchOnDevices(self):
        '''
        ability to toggle expressions on and off for a character
        url:bugstar:2747962
        '''
        self._log.LogMessage("Switching On Devices")
        RefCharacter.switchOnDevices(self)

    @Logger("Character - Fix Hierarchy Name Issues")
    def FixHierarchyNameIssues(self):
        '''
        bug 2568559
        '''
        self._log.LogMessage("Fixing hierarchy name issues")
        RefCharacter.FixHierarchyNameIssues(self)

    @Logger("Character - Fix OH Scale")
    def FixOhScaleTranslateProblem(self):
        '''
        It fixes a scale/translation problem that 3 model nulls(children of Zeroing_Null)
        after an update/swap from a version that doesn't have that Null in the hierachy, to a version that has it.
        ex. v4 Male_Skeleton to v17
        '''
        self._log.LogMessage("Fixing OH scale translation problems")
        RefCharacter.FixOhScaleTranslateProblem(self)

    @Logger("Character - Fix Extensions")
    def GetProblemCharacterExtensions(self):
        '''
        bug 2588122
        Some character extension sub objects don't have the default scale values of (1, 1, 1).
        This function returns them.
        '''
        self._log.LogMessage("Fixing character extensions")
        return RefCharacter.GetProblemCharacterExtensions(self)

    @Logger("Character - Fix Scale on Extensions")
    def FixScaleOnCharacterExtensions(self, extensionList):
        '''
        bug 2588122
        Some character extension sub objects don't have the default scale values of (1, 1, 1).
        This function fixes that.
        '''
        self._log.LogMessage("Fixing scale on character extensions")
        RefCharacter.FixScaleOnCharacterExtensions(extensionList)

    @Logger("Character - get character component")
    def _getCharacterComponent(self, namespace=None, raiseError=True):
        """
        Gets the FBCharacter components associated with the namespace.
        By default it gets the character component associated with this reference object.

        Arguments:
            namespace (pyfbsdk.FBNamespace):  namespace to look for character in
            raiseError (bool): raise an error if no or too many characters are found

        Return:
            pyfbsdk.FBCharacter
        """
        if namespace is None:
            namespace = Namespace.GetFBNamespace(self.Namespace)

        # Get a handle on our source character component - note we have defended against no
        # characters being present earlier in the update process
        characters = Namespace.GetContentsByType(namespace, mobu.FBCharacter, True)
        if len(characters) == 1:
            return characters[0]

        elif not raiseError and characters:
            self._log.LogWarning("The reference has more than one character component - "
                                 "this is strange and a sign the scene is in a broken state. Will attempt to continue.")
            for character in characters:
                if character.Name == self.Name:
                    return character
            return characters[0]

    def GetOutfitList(self):
        '''
        Returns a list of outfits that are valid for the character.
        '''

        return self._outfitDict.values()

    def GetCurrentOutfit(self):
        '''
        Returns the current outfit
        '''

        outfitNameProperty = self._getOutfitNameProperty()
        if outfitNameProperty.Data == "NONE":
            return None
        else:
            return self._outfitDict[outfitNameProperty.Data]

    def GetOutfitByName(self, outfitName):
        '''
        Returns an outfit by name
        '''

        return self._outfitDict[outfitName]

    @Logger("Character - Switching outfit")
    def SetCurrentOutfit(self, outfit):
        '''
        Changes the current outfit to the one with the passed name
        '''
        if outfit in self._outfitDict.values():
            outfitNameProperty = self._getOutfitNameProperty()
            outfitNameProperty.Data = outfit.Name

            # Process the outfit which will show/hide the models
            self._processOutfit()
        else:
            self._log.LogWarning("Outfit passed in was not valid")

    def SetOutfitByName(self, outfitName):
        '''
        Changes the current outfit to the one with the passed name
        '''
        self.SetCurrentOutfit(self._outfitDict[outfitName])

    @Logger("Character - Set accessory state")
    def SetAccessoryState(self, accessoryName, state):
        '''
        Changes the passed accessory to the passed state (on/off) on the current outfit
        '''
        outfit = self.GetCurrentOutfit()
        if outfit is not None:
            if accessoryName in outfit.GetAccessoryNameList():
                accessoryNameListProperty = self._getAccessoryNameListProperty()
                if state:
                    if accessoryName not in accessoryNameListProperty:
                        accessoryNameListProperty.append(accessoryName)
                        self._processOutfit()
                else:
                    if accessoryName in accessoryNameListProperty:
                        accessoryNameListProperty.remove(accessoryName)
                        self._processOutfit()
            else:
                self._log.LogWarning("Passed accessory name is not valid for current outfit")
        else:
            self._log.LogWarning("Tried to set accessory state while not on a valid outfit.")

    @Logger("Character - Get accessory state")
    def GetAccessoryState(self, accessoryName):
        '''
        Gets the state for the passed accessoryName
        '''
        outfit = self.GetCurrentOutfit()
        if outfit is not None:
            accessoryNameListProperty = self._getAccessoryNameListProperty()
            if accessoryName in accessoryNameListProperty:
                return True
        return False

    def GetOutfitNameList(self):
        '''
        Returns a sorted list of outfit names that are valid for the current character.
        '''

        return sorted(self._outfitDict.keys())

    def GetDisposableBonesList(self):
        '''
        Returns a list of all the roll, mass handleres and secondary motion bones on the character
        '''
        disposableBonesList = Component.GetComponents(Namespace=self.Namespace,
                                                      TypeList=[mobu.FBModel],
                                                      DerivedTypes=True,
                                                      ExcludeRegex='(?i)openSmile|FACIAL',
                                                      IncludeRegex='(?i)RB|MH|SM')

        return disposableBonesList

    def GetVisualModelList(self):
        '''
        Returns a list of all the geometry elements that are currently being used as skin for the character
        '''
        models = Component.GetComponents(Namespace=self.Namespace,
                                         TypeList=[mobu.FBModel],
                                         DerivedTypes=False,
                                         ExcludeRegex='(?i)mover|text|rect|contact|ctrl|frm')

        visualModelList = list()
        for model in models:
            if type(model.Geometry) == mobu.FBMesh:
                if model.Geometry.PolygonCount() > 30:
                    visualModelList.append(model)
        return visualModelList

    def GetFaceBones(self):
        '''
        Returns a list of all facial bones and gui of the current Character
        '''
        faceBonesList = Component.GetComponents(Namespace=self.Namespace,
                                                TypeList=[mobu.FBModel],
                                                DerivedTypes=True,
                                                IncludeRegex='(?i)FACIAL_C_FacialRoot')

        headGuiList = Component.GetComponents(Namespace=self.Namespace,
                                                TypeList=[mobu.FBModel],
                                                DerivedTypes=True,
                                                IncludeRegex='(?i)headGui')

        return Model.GetChildren(list(faceBonesList), list(headGuiList))


    def GetFaceControls(self):
        '''
        Returns a list of the facial controls of the current Character
        '''
        faceControlsList = Component.GetComponents(Namespace=self.Namespace,
                                                TypeList=[mobu.FBModel],
                                                DerivedTypes=True,
                                                IncludeRegex='(?i)faceControls_OFF')

        facialAttrGUIList = Component.GetComponents(Namespace=self.Namespace,
                                                TypeList=[mobu.FBModel],
                                                DerivedTypes=True,
                                                IncludeRegex='(?i)FacialAttrGUI')

        return Model.GetChildren(list(faceControlsList), list(facialAttrGUIList))

    def GetFaceConstraints(self):
        '''
        Returns a list of the facial constraints of the current Character
        '''

        faceConstraints = Component.GetComponents(Namespace=self.Namespace,
                                                  TypeList=[mobu.FBConstraint],
                                                  DerivedTypes=True,
                                                  IncludeRegex='(?i)3lateral|viseme|_lookat|_eyedriver|_convergencegui|setup_relationconstraint')

        return faceConstraints

    def GetRootModel(self):
        '''
        Returns the root model
        '''
        return mobu.FBFindModelByLabelName('{0}:Dummy01'.format(self.Namespace))

    def GetUnsupportedConstraints(self):
        '''
        Checks the character for all constraint types constrained to it.
        Returns List of unsupported constraints when using _majorUpdate.
        '''
        # ToDo - recode/cleanup
        constraintList = mobu.FBComponentList()
        componentList = Component.GetComponents(self.Namespace)

        for componentItem in componentList:
            for constraint in Plug.GetDstPlugList(componentItem, mobu.FBConstraint):
                if not Constraint.IsConstraintType(constraint, self._supportedConstraints):
                    if constraint not in componentList:
                        constraintList.append(constraint)
            for constraint in Plug.GetSrcPlugList(componentItem, mobu.FBConstraint):
                if not Constraint.IsConstraintType(constraint, self._supportedConstraints):
                    if constraint not in componentList:
                        constraintList.append(constraint)

        return constraintList

    @Logger("Character - Hide Visual Models")
    def HideVisualModels(self):
        '''
        Hides all the skin models of the character.
        '''
        self._log.LogMessage("Hiding visual models")
        for visualModel in self.GetVisualModelList():
            visualModel.Show = False

    @Logger("Character - Unhide Visual Models")
    def UnhideVisualModels(self):
        '''
        Unhides all the skin models of the character.
        '''
        self._log.LogMessage("Unhiding visual models")
        for visualModel in self.GetVisualModelList():
            visualModel.Show = True

    def DeleteProblematicFacialBones(self):
        '''
        Removes facial bones that are known to cause problems (for gta5 )from the scene
        '''
        skelRoot = mobu.FBFindModelByLabelName("{0}:FaceRoot_Head_000_R".format(self.Namespace))
        if skelRoot:
            Model.DeleteChildren(skelRoot)

        skelRoot = mobu.FBFindModelByLabelName("{0}:FaceRoot_Head_001_R".format(self.Namespace))
        if skelRoot:
            Model.DeleteChildren(skelRoot)

    def Component(self):
        """
        Returns the character component

        Return:
            pyfbsdk.FBCharacter
        """
        return self._getCharacterComponent()

    def HideAllNonGeoGroups(self):
        '''
        goes through the character's group and hides all of the character's components, apart from
        the Geometry group.
        '''
        geoCharacterGroups = ([component for component in Globals.Components
                            if Component.GetNamespace(component) == self.Namespace
                            and isinstance(component, mobu.FBGroup)
                            and component.Name in ['Geometry', 'GeoVariations']])

        geoCharacterGroups.extend([groupItem for group in geoCharacterGroups
                               for groupItem in group.Items
                               if isinstance(groupItem, mobu.FBGroup)])

        characterGroups = ([component for component in Globals.Components
                            if Component.GetNamespace(component) == self.Namespace
                            and isinstance(component, mobu.FBGroup)
                            and component.Name.lower() != self.Namespace.lower()
                            and component not in geoCharacterGroups])

        for group in characterGroups:
            group.Show = False

    def _getCharacterNode(self):
        '''
        returns:
            FBCharacter
        '''
        for character in Globals.Characters:
            if Component.GetNamespace(character) == self.Namespace:
                characterNode = character
                return characterNode
        return

    def PlotToSkeletonCurrentTake(self, allTakes=False):
        '''
        plot to skeleton
        '''
        # get character node
        characterNode = self._getCharacterNode()
        if characterNode is None:
            return

        currentTake= mobu.FBSystem().CurrentTake

        Lib.PlotCharacters([characterNode], plotLocations=['skeleton'])

    def PlotToSkeletonAllTakes(self, allTakes=False):
        '''
        plot to skeleton
        '''
        # get character node
        characterNode = self._getCharacterNode()
        if characterNode is None:
            return

        Lib.PlotCharacters([characterNode], plotLocations=['skeleton'], takes=Globals.Takes)

    def PlotToControlRigCurrentTake(self, allTakes=False):
        '''
        plot to control rig
        '''
        # get character node
        characterNode = self._getCharacterNode()
        if characterNode is None:
            return

        Lib.PlotCharacters([characterNode], plotLocations=['rig'])

    def PlotToControlRigAllTakes(self, allTakes=False):
        '''
        plot to control rig
        '''
        # get character node
        characterNode = self._getCharacterNode()
        if characterNode is None:
            return

        Lib.PlotCharacters([characterNode], plotLocations=['rig'], takes=Globals.Takes)