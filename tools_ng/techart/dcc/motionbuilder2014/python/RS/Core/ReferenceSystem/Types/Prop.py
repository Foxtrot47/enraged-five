'''
Usage:
    This contains logic to help interaction with PROP references
Author:
    Ross George
    Bianca Rudareanu
'''
import os

from RS.Core.ReferenceSystem.Types.Complex import Complex


class Prop(Complex) :
    '''
    Represents a prop reference in a scene
    ''' 
    PropertyTypeName    = 'Props'
    FormattedTypeName   = 'Prop'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'props'),
                           os.path.join('art', 'dev','animation', 'resources', 'props'),
                           ]
    FileType            = 'fbx'

