import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapPropToy(MocapComplex):
    '''
    Represents a mocap proptoy reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'MocapPropToy'
    FormattedTypeName = 'Mocap PropToy'
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, 'north', 'propToys'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'nyc', 'propToys'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'nyc', 'props'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'tor', 'propToys'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'sd', 'propToys'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'globalAssets', 'propClones'),
                    ]
    FileType = 'fbx'

    @Logger("Mocap Prop Toy - Create")
    def _create(self):
        '''
        This function gets run only when a proptoy is created
        '''
        super(MocapPropToy, self)._create()

        # align assets to active stage
        self._log.LogMessage("Aligning proptoy to active stage.")
        self.AlignReferenceRootToActiveStage()

