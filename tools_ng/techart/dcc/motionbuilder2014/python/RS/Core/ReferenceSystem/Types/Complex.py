import uuid
import os
import re

import pyfbsdk as mobu

import RS.Core.ReferenceSystem as Core

from RS import Config, Globals, Utils, Perforce
from RS.Utils.Scene import Component, Constraint, Plug
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Core.ReferenceSystem.Types.Base import Base
from RS.Utils import Scene, Namespace


class Complex(Base):
    @Logger("Complex - Update")
    def _update(self, force=False):
        '''
        Updates the reference with latest data. Force does nothing here currently.
        '''
        sourceNamespace = Globals.Scene.NamespaceGet(self.Namespace)
        
        # Setup load options
        mergeOptions = Core.CreateBasicMergeOptions(self.Path)

        # Create a holding/temporary namespace
        tempNamespace = str(uuid.uuid1())
        mergeOptions.NamespaceList = tempNamespace

        self._log.LogMessage("Merging file: {0}".format(self.Path))
        Globals.Application.FileMerge(self.Path, False, mergeOptions)

        self._log.LogMessage("Mapping existing animation back")
        Namespace.CopyData(self.Namespace, tempNamespace)
        Namespace.CopyAnimationData(self.Namespace, tempNamespace)

        # Now we will go through each component and if the source component is driving something else in a constraint we
        # will make sure it is swap in the target component appropriately
        componentMatchList = Namespace.GetComponentMatchList(self.Namespace, tempNamespace, [mobu.FBModelSkeleton,
                                                                                             mobu.FBModel,
                                                                                             mobu.FBModelMarker,
                                                                                             mobu.FBModelRoot,
                                                                                             mobu.FBModelNull])
        for componentMatch in componentMatchList:
            for constraint in Plug.GetDstPlugList(componentMatch.SourceComponent, mobu.FBConstraint):
                if Constraint.IsConstraintType(constraint, [Constraint.PARENT_CHILD,
                                                            Constraint.ROTATION,
                                                            Constraint.POSITION,
                                                            Constraint.SCALE]):
                    Constraint.SwapSourceModel(constraint,
                                               componentMatch.SourceComponent,
                                               componentMatch.TargetComponent)

        # Delete namespace containing old reference
        self._log.LogMessage("Deleting old reference components.")
        componentsToDelete = mobu.FBComponentList()
        sourceNamespace.GetContentList(componentsToDelete)
        Component.Delete(componentsToDelete, deleteNamespaces=False)

        # Rename the new namespace
        self._log.LogMessage("Renaming new reference components.")
        componentsToRename = Component.GetComponents(Namespace=tempNamespace)
        prefix = '{}:'.format(self.Namespace)
        for component in componentsToRename:
            component.LongName = prefix + component.Name

        # Update Reference P4 Version
        self._p4VersionProperty.Data = str(Perforce.GetFileState(self.Path).HaveRevision)

        if self.IsProxy:
            self._proxySetup()

    @Logger("Complex - Stripping Reference")
    def _strip(self):
        '''
        Preforms a full strip of the reference.
        This will likely be overloaded by each of the inherited classes.
        '''

        # Delete visual models
        for visualModel in list(self.GetVisualModelList()):
            visualModel.FBDelete()

    @Logger("Complex - Reloading Textures")
    def _reloadTextures(self):
        '''
        Pushes a refresh to all textures on the reference. Called after the manager has sync'ed some
        textures the user didn't have or was out of date on. Manager will batch calls for efficency
        with syncing on p4.
        '''
        self._log.LogMessage("Reloading textures for reference: {0}".format(self.Namespace))
        textureComponents = [component for component in Globals.Components
                             if isinstance(component, mobu.FBTexture)
                             and Component.GetNamespace(component) == self.Namespace]
        for textureComponent in textureComponents:

            # Fist we check to see if the texture already has a video attached
            textureFilePath = None
            if textureComponent.Video is not None:
                textureFilePath = textureComponent.Video.Filename
                textureComponent.Video = None

            # If it doesn't we need to create one drawing information from backup property
            elif textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH) is not None:
                textureFilePath = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH).Data

            if textureFilePath is not None:

                #Check for tif version first - the more prefered option
                if os.path.exists(textureFilePath.replace('tga', 'tif')):
                    textureFilePath = textureFilePath.replace('tga', 'tif')
                if os.path.exists(textureFilePath):
                    if textureComponent.Video is None:
                        textureComponent.Video = mobu.FBVideoClipImage('{0}_video'.format(textureComponent.Name))
                    textureComponent.Video.Filename = textureFilePath

    @Logger("Complex - Strip Textures")
    def _stripTextures(self):
        '''
        Strips (deletes) textures from all the models for this reference. Called through
        the manager so calls to multiple refrences can be batched for efficency.
        '''
        self._log.LogMessage("Stripping textures for reference: {0}".format(self.Namespace))
        textureComponents = [component for component in Globals.Components
                             if isinstance(component, mobu.FBTexture)
                             and Component.GetNamespace(component) == self.Namespace]

        for textureComponent in textureComponents:
            if textureComponent.Video is not None:
                # Remember the texture name- this gests lost after we delete
                # the video and MOBU gaves it a generic name
                name = textureComponent.Name

                # Check to see if it has already a path property
                videoPathProperty = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH)
                if videoPathProperty is None:
                    videoPathProperty = textureComponent.PropertyCreate(Core.STR_VIDEO_PATH,
                                                                        mobu.FBPropertyType.kFBPT_charptr,
                                                                        "String", False, True, None)
                videoPathProperty.Data = str(textureComponent.Video.Filename)

                #Remove the video
                textureComponent.Video = None

                # Rename it for consistency
                textureComponent.Name = name

    @Logger("Complex - Proxy Setup")
    def _proxySetup(self):
        '''
        Turns the current reference into a proxy - a place holder asset colored pink in the scene
        '''
        self._log.LogMessage("Proxy setup for reference: {0}".format(self.Namespace))

        # strip textures and materials from asset
        textureList = Component.GetComponents(Namespace=self.Namespace,
                                              TypeList=[mobu.FBTexture,
                                                        mobu.FBMaterial,
                                                        mobu.FBVideo],
                                              DerivedTypes=False)

        for item in textureList:
            item.FBDelete()

        # iterate through list
        for model in self.GetVisualModelList():
            # get number of regions in mesh
            geometry = model.Geometry
            if geometry:
                geometryRegionList = set(geometry.GetMaterialIndexArray())
                numberRegions = 1
                if len(geometryRegionList) > 0:
                    numberRegions = max(geometryRegionList) + 1
                # create 1 material per region
                materialList = []
                for idx in range(numberRegions):
                    material = FBMaterial('{0}:ProxyMat_0'.format(model.Name))
                    material.Ambient = FBColor(1, 0, 1)
                    material.Diffuse = FBColor(1, 0, 1)

                    tag = material.PropertyCreate(
                        'ProxyAssetMaterial', FBPropertyType.kFBPT_charptr, "", False, True, None)
                    tag.Data = model.Name

                    materialList.append(material)

                # apply materials to each region
                for material in materialList:
                    if material not in model.Materials:
                        model.Materials.append(material)

                # Create mat folder
                placeholder = FBMaterial('Remove_Me')
                folder = mobu.FBFolder("{0}:ProxyMaterials".format(model.Name), placeholder)
                placeholder.FBDelete()

                # Add materials to a folder
                for component in Globals.Components:
                    proxyMatProperty = component.PropertyList.Find(
                        'ProxyAssetMaterial')
                    if proxyMatProperty:
                        folder.ConnectSrc(component)

    @Logger("Complex - Check Namespace")
    def CheckNamespaceConsistency(self):
        '''
        Sometimes models and textures obtain a different namespace to the reference(mobu bug perhaps)
        '''
        self._log.LogMessage("Checking Namespace for reference: {0}".format(self.Namespace))
        for texture in Scene.Model.GetTextureList(self.GetVisualModelList()):
            if Component.GetNamespace(texture) != self.Namespace:
                Component.SetNamespace(texture, self.Namespace)

    @Logger("Complex - Fix Handle Double Namespace")
    def FixHandleDoubleNamespace(self):
        '''
        the handles have double namespaces in an autodesk error.  They are causing crashes when
        deleting them and then attempting to save. So we need to scan for double namespaces before
        removing the asset from the scene.
        '''
        self._log.LogMessage("Checking for Duplicated Namespaces on Handles")
        for handle in Globals.Handles:
            namespace = Component.GetNamespace(handle)
            if namespace is None:
                continue
            namespaceList = namespace.split(":")
            # check if the handle has multiple namespace of the same name
            if len(namespaceList) > 1 and namespaceList[0] == namespaceList[1]:
                handleNamespace = "{0}:{1}".format(namespaceList[0], namespaceList[1])
                handle.ProcessObjectNamespace(mobu.FBNamespaceAction.kFBReplaceNamespace, handleNamespace, namespaceList[0], False)
                # delete the namespace related to the double handle namespace
                for namespace in Globals.Namespaces:
                    print namespace.LongName, handleNamespace
                    if namespace.LongName == handleNamespace:
                        namespace.FBDelete()

    def DisableRotDOF(self):
        '''
        Disable all Rotation DOFS
        '''
        if self.GetMover():
            dummyProp = self._modelNull.PropertyList.Find("Asset_Type")
            if dummyProp:
                rotDOFProp = self.GetMover().PropertyList.Find("Enable Rotation DOF")
                if rotDOFProp:
                    rotDOFProp.Data = False

    def GetMover(self):
        '''
        Retrieves the mover
        '''
        mover = mobu.FBFindModelByLabelName('{0}:mover'.format(self.Namespace))
        if mover == None:
            raise Exception("Missing mover model.")

        return mover
