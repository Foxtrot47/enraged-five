from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Types.MocapBlockingModel import MocapBlockingModel
from RS.Core.ReferenceSystem.Types.MocapGsSkeleton import MocapGsSkeleton
from RS.Core.ReferenceSystem.Types.MocapPropToy import MocapPropToy
from RS.Core.ReferenceSystem.Types.MocapStage import MocapStage
from RS.Core.ReferenceSystem.Types.MocapSlate import MocapSlate


class MocapEssential(MocapComplex):
    '''
    A master category which holds subcategories
    Essential is a category for the users to know they should not delete these assets at the end of
    mocap, for cleanup.
    '''
    PropertyTypeName = 'MocapEssential'
    FormattedTypeName = 'Mocap Essential'
    TypePathList = []
    FileType = ''
    SubCats = [MocapBlockingModel, MocapGsSkeleton, MocapPropToy, MocapSlate, MocapStage]

