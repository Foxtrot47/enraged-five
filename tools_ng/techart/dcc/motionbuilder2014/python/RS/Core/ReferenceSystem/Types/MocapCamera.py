import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex


class MocapCamera(MocapComplex):
    '''
    Represents a mocap camera reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'MocapCamera'
    FormattedTypeName = 'MocapCamera'
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz,'north', 'cameras'),
                     os.path.join(Config.VirtualProduction.Path.Previz,'nyc', 'cameras'),
                     os.path.join(Config.VirtualProduction.Path.Previz, 'tor', 'cameras'),
                     os.path.join(Config.VirtualProduction.Path.Previz, 'sd', 'cameras'),
                     ]
    FileType = 'fbx'
