import pyfbsdk as mobu

from PySide import QtGui

import time
import inspect
import itertools
import sys
import traceback
import pstats
import cProfile

from functools import wraps
from ctypes import cdll
from RS import Globals
from RS.Utils import Instrument
from RS.Utils.Scene import Component

from RS.Core.ReferenceSystem import Exceptions
import RS.Core.ReferenceSystem as Core


def IsListEmpty( func ):
    '''
    Decorator that checks if the second argument passed to the decorated function is an empty list
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):
        #Argument 0, argument 1 is a list
        if type(args[1])==list:
            if len(args[1])==0:
                QtGui.QMessageBox.warning(None,
                                          "Reference Editor Warning",
                                          "Selected reference list is empty.")
                return
        elif args[1]:
            return func(*args, **kwargs)
        return func(*args, **kwargs)
    return wrapper


def Profiler( func ):
    '''
    Decorator that returns the profile of a function
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):

        Instrument.InstrumentModule( mobu )

        prof = cProfile.Profile()
        prof.enable()

        result = func(*args, **kwargs)

        prof.disable()
        prof.dump_stats("X:\\temp\\profileDump.prof")

        stats = pstats.Stats("X:\\temp\\profileDump.prof")
        stats.strip_dirs()
        stats.sort_stats('time')
        stats.print_stats()

        return result
    return wrapper


def ExceptionMessage( func ):
    '''
    This decorator will show a message box with the exception is a
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):

        manager = args[0]
        try:
            return func(*args, **kwargs)
        except Exceptions.NotAnError:
            raise Exceptions.NotAnError()
        # user feedback for deleted file
        except Exceptions.DeletedFile:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "The Reference you are attempting to run a function on has been deleted from Perforce, or just cannot be sync'd for some reason. Please check the path in Perforce to find the issue.\n\nSpeak to Tech Art if you are unable to diagnose the issue.")
            raise Exceptions.DeletedFile()
        # user feedback for referencing from an unknown directory
        except Exceptions.IncorrectFileDirectory:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "Unable to determine the type of reference.\n\nThe file you are attempting to add is not from a known Reference Directory. Bug Tech Art if you wish for this directory to be known to the Ref Editor.\n\nA Reference has not been added to the scene.\n\nSpeak to Tech Art if you are unable to diagnose the issue.")
            raise Exceptions.IncorrectFileDirectory()
        # user feedback for character component namespace not matching that of a character reference
        except Exceptions.CharacterComponentMissing:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "There is no character component in the namespace. \n\nThis is a sign that the character is in a broken state and it is not possible to continue with update - please check over your scene.\n\nSpeak to Tech Art if you are unable to diagnose the issue.")
        # user feedback for character component namespace not matching that of a character reference
        except Exceptions.MultipleCharacterComponents:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "There are too many Character components with the same namespace, unable to determine which one to run this function on. \n\nPlease check over your scene as something is wrong with the namespaces, most likely a casing issue.\n\nSpeak to Tech Art if you are unable to diagnose the issue.")
        # user feedback when code is unable to find a reference to match the given namespace
        except Exceptions.MissingReference:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "There are no reference with the passed namespace.\n\nPlease check over your scene for potential issues with the reference nulls or namespaces.\n\nSpeak to Tech Art if you are unable to diagnose the issue.")
        # user feedback when an unsupported constraint is found.
        except Exceptions.UnsupportedConstraint:
            QtGui.QMessageBox.warning(None,
                                        "Reference Editor Warning",
                                        "The reference you are trying to update has an unsupported constraint(s).\n\nSupported Constraints include:\n- Parent/Child\n- Rotation\n- Position\n- Scale\n\nUnable to proceed with Update.")
        # error for TA to highlight a Reference Editor issue
        except Exception as e:
            if not manager._silent:
                QtGui.QMessageBox.warning(None,
                                          "Reference Editor Warning",
                                          e.message)
            raise
    return wrapper


class Logger( object ):
    '''
    A decorator that will act as log wrapper around the RS.Logging system.
    It takes optional arguments: context, debug and log( if used outside the Reference System you need to pass as an argument the RS.Universal.Logging)
    It raises full traceback of the occuring errors
    '''
    def __init__(self, context=None, state=True):
        self.Context = context
        self._state = state

    def __call__(self, func):

        @wraps(func)
        def wrapper(*args, **kwargs):

            manager = args[0]

            # set the progresswindowoutput property and run the log UI
            manager._log.ProgressWindowOutput = self._state

            # add log string to a list
            manager._log.PushContext(self.Context)

            try:
                return func(*args, **kwargs)

            except Exception as e:
                if not hasattr( e, '_handled' ):
                    manager._log.LogError( e.message )
                    e._handled = True
                raise

            finally:
                manager._log.PopContext()


        return wrapper


class Progress(object):
    '''
    '''
    def __init__(self, context= None):
        self.Context = context

    def __call__(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            manager = args[0]
            if manager._log.ProgressWindowOutput:
                manager._log.ProgressWindowStart()
            return func(*args, **kwargs)
        return wrapper


def CleanScene( func ):
    '''
    Decorator that rememebers scene settings and pushes them back. It will also call the
    Manager._cleanScene before and after running the target function. Bit of a strange design
    but will look at improving this at a later date. Also now handles turning of devices as well.
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):

        manager = args[0]

        #Remember information
        startTime = Globals.Player.LoopStart
        endTime = Globals.Player.LoopStop

        # Record current Take.
        oldTake = Globals.System.CurrentTake

        # Run it before the function
        manager._log.LogMessage("Preforming a pre pass of scene clean up.")
        manager._cleanScene()

        result = func(*args, **kwargs)

        #Also run it after - clean up anything that might have been left as a result of an update
        manager._log.LogMessage("Preforming a post pass of scene clean up.")
        manager._cleanScene()

        if oldTake != Globals.System.CurrentTake:
            Globals.System.CurrentTake = oldTake

        #Fixup time range
        Globals.Player.LoopStart = startTime
        Globals.Player.LoopStop = endTime

        #Go back to base layer
        Globals.System.CurrentTake.SetCurrentLayer( 0 )

        #Set FPS to 30fps
        Globals.Player.SetTransportFps( mobu.FBTimeMode.kFBTimeMode30Frames )

        return result

    return wrapper


class DeactivateFileOpenCallbacks( object ):
    '''
    A decorator that will temporarily deactivate any callbacks on FileOpen during a function's
    running.If we are passed 'False' as our state the decorator will do nothing (simply call the wrapped function directly)
    '''
    def __init__( self, State ):
        self._state = State

    def __call__(self, targetFunction):
        def inner( *args, **kwargs ):
            if self._state:
                fileOpenCallbackList = []
                for callback in Globals.Application.OnFileOpen.callbacks:
                    fileOpenCallbackList.append(callback)
                Globals.Application.OnFileOpen.RemoveAll()
                try:
                    return targetFunction( *args, **kwargs )
                except:
                    raise
                finally:
                    for fileOpenCallback in fileOpenCallbackList:
                        Globals.Application.OnFileOpen.Add(fileOpenCallback)
            else:
                return targetFunction( *args, **kwargs )
        return inner


class UseMergeTransaction( object ):
    '''
    A decorator that will turn merge transaction off during a function's running and turn it
    back on afterwards. If Merge transaction is already 'On' it will do nothing. If we are passed
    'False' as our state the decorator will do nothing (simply call the wrapped function directly)
    '''
    def __init__( self, State ):
        self._state = State

    def __call__(self, targetFunction):
        def inner( *args, **kwargs ):
            if not mobu.FBMergeTransactionIsOn() and self._state:
                mobu.FBMergeTransactionBegin()
                try:
                    return targetFunction( *args, **kwargs )
                except:
                    raise
                finally:
                    mobu.FBMergeTransactionEnd()
            else:
                return targetFunction( *args, **kwargs )
        return inner


