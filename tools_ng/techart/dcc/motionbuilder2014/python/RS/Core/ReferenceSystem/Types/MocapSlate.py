import os

from RS import Config
from RS.Core.ReferenceSystem.Types.MocapComplex import MocapComplex
from RS.Core.ReferenceSystem.Decorators import Logger


class MocapSlate(MocapComplex):
    '''
    Represents a mocap slate reference in a scene
    '''
    studio = ()
    PropertyTypeName = 'MocapSlate'
    FormattedTypeName = 'Mocap Slate'
    TypePathList = [os.path.join(Config.VirtualProduction.Path.Previz, 'north', 'slate'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'nyc', 'slate'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'tor', 'slate'),
                    os.path.join(Config.VirtualProduction.Path.Previz, 'sd', 'propToys'),
                    ]
    FileType = 'fbx'

    @Logger("Mocap Slate - Create")
    def _create(self):
        '''
        This function is run when a slate is created
        '''
        super(MocapSlate, self)._create()

        # update groups
        self._log.LogMessage("Updating base previz groups with new slate reference created.")
        self.UpdateGroups()