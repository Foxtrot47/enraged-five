from fbx import *
from FbxCommon import *

from pyfbsdk import *
import pyfbsdk as mobu

import os
import time
from functools import wraps

from RS import Globals, Perforce, Config
from RS.Utils import Collections, Path, Namespace, ControlSet, Email
from RS.Utils.Scene import Component
from RS.Utils.Logging import Universal

import RS.Core.ReferenceSystem as Core
from RS.Core.ReferenceSystem.Types.Character import Character
from RS.Core.ReferenceSystem.Types.Face import Face
from RS.Core.ReferenceSystem.Types.Audio import Audio
from RS.Core.ReferenceSystem.Types.Prop import Prop
from RS.Core.ReferenceSystem.Types.Set import Set
from RS.Core.ReferenceSystem.Types.AmbientFace import AmbientFace
from RS.Core.ReferenceSystem.Types.RayFire import RayFire
from RS.Core.ReferenceSystem.Types.Vehicle import Vehicle
from RS.Core.ReferenceSystem.Types.Complex import Complex
from RS.Core.ReferenceSystem import Decorators, Exceptions

from RS.Core.ReferenceSystem.Types.MocapEssential import MocapEssential
from RS.Core.ReferenceSystem.Types.MocapNonEssential import MocapNonEssential
from RS.Core.ReferenceSystem.Types.MocapBasePreviz import MocapBasePreviz
from RS.Core.ReferenceSystem.Types.MocapBlockingModel import MocapBlockingModel
from RS.Core.ReferenceSystem.Types.MocapCamera import MocapCamera
from RS.Core.ReferenceSystem.Types.MocapGsSkeleton import MocapGsSkeleton
from RS.Core.ReferenceSystem.Types.MocapPropToy import MocapPropToy
from RS.Core.ReferenceSystem.Types.MocapSlate import MocapSlate
from RS.Core.ReferenceSystem.Types.MocapStage import MocapStage


def Deprecated(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # TODO: Add code that updates a table on the database everytime deprecated methods are used.
        return func(*args, **kwargs)
    return wrapper


class Singleton(type):
    _instance = None

    def __call__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance


class Manager (object):
    '''
    Manages what references exist in the current scene.
    '''
    __metaclass__ = Singleton

    def __init__(self):
        '''
        Initialise the manager.
        '''

        self._referenceDictionary = {}
        self._rootNull = None
        self._p4SyncStatus = True

        # Setup our log
        self._log = Universal.UniversalLog("{0}_reference_system".format(os.getpid()))
        # Don't want to output to console for now
        self._log.ConsoleOutput = False

        self._debug = False
        self._silent = False
        self._ignoreProject = False

    @Decorators.Logger("Manager - Parsing Reference")
    def _parseReference(self, modelNull):
        '''
        Attempts to parse a reference from the passed model null
        '''
        # Match the type by NAME from the property
        typeName = modelNull.PropertyList.Find(Core.STR_RS_ASSET_TYPE).Data
        matchingType = self._resolveTypeByName(typeName)

        # Verify this matches the type derived from PATH
        filePath = modelNull.PropertyList.Find(Core.STR_REFERENCE_PATH).Data
        if(matchingType != self._resolveTypeByPath(filePath)):
            raise Exception("The type [{0}] isn't consistent with the location of the file [{1}].".format(
                typeName.FormattedTypeName, filePath))

        # Create a new instance of it by passing in the FBModelNull
        loadedReference = matchingType(modelNull, self._log)

        # Check to see if the name of the null is the same as the its namespace
        # property
        if self._checkNameConsistency(modelNull.Name, loadedReference.Namespace):
            raise Exception("Model null's name [{0}] does not match its namespace [{1}] property.".format(
                modelNull.LongName, loadedReference.Namespace))

        # Make sure the namespace isn't in use already
        if self._usingNamespace(loadedReference.Namespace):
            raise Exception("Namespace [{0}] collides with another reference.".format(
                loadedReference.Namespace))

        # Add it to our dictionary and return it
        self._referenceDictionary[loadedReference.Namespace] = loadedReference

        return loadedReference

    def _saveExportSettings(self):
        '''
        Temporary function that will save the root null to an empty file.Its purpose is to force
        save/create the userObjects related to the animation export (saving the settings of the exporter).
        When we merge or load a file the markup that was stored (if any) is loaded. So any changes that
        have been made by the user will be lost. We must preform a save operation to force save the markup.
        '''

        self._log.LogMessage("Saving export settings.")

        # Deselect all objects - ToDo - this should be changed to use the 'with' statement for
        # the begin change all models.
        mobu.FBBeginChangeAllModels()
        for component in Globals.Components:
            component.Selected = False
        mobu.FBEndChangeAllModels()

        self._rootNull.Selected = True

        tempFbxFilePath = os.path.join(
            Config.Tool.Path.Temp, 'markup_temp.fbx')

        saveOptions = FBFbxOptions(True, tempFbxFilePath)
        saveOptions.SetAll(FBElementAction.kFBElementActionDiscard, False)

        saveOptions.SaveSelectedModelsOnly = True
        saveOptions.UpdateRecentFiles = False

        Globals.Application.FileSave(tempFbxFilePath, saveOptions)

        self._rootNull.Selected = False

    def _checkNameConsistency(self, nullName, namespace):
        '''
        Checks to see if the name of the reference null is the same as it's namespace property
        '''
        if str(nullName) == str(namespace):
            return False
        else:
            return True

    def _usingNamespace(self, targetNamespace):
        '''
        Returns true if the passed namespace is in use.
        '''
        if self._referenceDictionary.has_key(targetNamespace):
            return True
        else:
            return False

    @Decorators.Logger("Manager - Resolving Namespace")
    def _resolveNamespace(self, fileName):
        '''
        Returns the correct namespace for the passed file path. Will derive this from the filename
        for the passed reference file path. It will add ^1 ^2 etc... for each subsequent reference.
        '''
        self._log.LogMessage("Resolving the Reference Namespace")

                # Remove the outfit name from the namespace, which uses a '-' as the separator.
        if ('-' in fileName):
            fileName = fileName.split('-')[0]

        baseNamespace = fileName
        returnNamespace = fileName
        iteration = 1
        while iteration < 50:
            if self._usingNamespace(returnNamespace):
                returnNamespace = "{0}^{1}".format(
                    baseNamespace, str(iteration))
                iteration += 1
            else:
                return returnNamespace

        raise Exception(
            "Detected over 50 references which are using the derived namespace!")

    @Decorators.Logger("Manager - Resolving Type by Path")
    def _resolveTypeByPath(self, filePath):
        '''
        Returns the type from the passed file path. Will raise an exception if the file is not
        rooted in one of the designated resource directories.
        '''
        for referenceType in self.GetReferenceTypeList():
            for typePath in referenceType.TypePathList:
                if typePath.lower() in filePath.lower():
                    if filePath.lower().startswith(Config.Project.Path.Root.lower()) or filePath.lower().startswith(Config.VirtualProduction.Path.Previz.lower()) or filePath.lower().startswith('x:\\projects\\'):
                        if referenceType.FileType in filePath.lower():
                            return referenceType
        return None

    @Decorators.Logger("Manager - Resolving Type by Name")
    def _resolveTypeByName(self, typeName):
        '''
        Returns the type from the passed passed string - from a ModelNull's property.
        '''
        self._log.LogMessage("Resolving the Reference Type by its Name")
        for referenceType in self.GetReferenceTypeList():
            if referenceType.PropertyTypeName.lower() == typeName.lower():
                return referenceType

        raise Exception("Unrecognised type [{0}].".format(typeName))

    def _checkReference(self, reference):
        '''
        Checks if given reference is a valid reference in the manager
        '''
        if not reference:
            raise Exception("No reference given.")

        if not reference in self.GetReferenceListAll():
            raise Exception("The reference does not exist in the manager.")

    @Decorators.IsListEmpty
    def _processReferences(self, referenceList):
        '''
        Checks to see if the references are valid. i.e are reference still in the scene and are
        in the reference dictionary
        '''
        if type(referenceList) != list:
            referenceList = [referenceList]

        currentList = list(self.GetReferenceListAll())

        for ref in referenceList:
            referenceFound = False
            for currentRef in currentList:
                if ref.Namespace.lower() == currentRef.Namespace.lower():
                    referenceFound = True
                    break

            if referenceFound is True:
                continue
            else:
                raise Exception("There are no reference with the passed namespace: {0}.\n\nPlease check over your scene for potential issues with the reference nulls or namespaces.".format(ref.Namespace))

        return referenceList

    @Decorators.Logger("Manager - Reloading data")
    def _reload(self, force=False):
        """
        Reloads the manager. Only if needed - so if the rootNull has changed. Which indicates that
        a new scene been loaded.
        """
        # Check if it is the same as the one we are currently working with - if it is we don't need a load
        rootNull = FBFindModelByLabelName("REFERENCE:Scene")
        if rootNull != self._rootNull or force:

            self._log.LogMessage("Scene has changed so preforming a reload.")

            # Unload before we attempt to load anything
            self._referenceDictionary = {}

            # Record the null we are working with
            self._rootNull = rootNull

            # If we don't have root null then there is nothing to load
            if self._rootNull == None:
                self._log.LogWarning(
                    "No reference root null - no data to load.")
            else:
                self._log.LogMessage(Globals.Application.FBXFileName)

                # For each child (should be a list of FBModelNull objects) of
                # the root parse
                for modelNull in self._rootNull.Children:
                    try:
                        reference = self._parseReference(modelNull)

                        self._log.LogMessage("Successfully parsed [{0}] as [{1}]".format(
                            reference.Namespace, type(reference).FormattedTypeName))
                    except Exception as e:
                        self._log.LogError(
                            "Error parsing - {0}".format(modelNull.LongName))
                        self._log.LogError(e.message)

    @Decorators.Logger("Manager - Clean scene")
    def _cleanScene(self):
        '''
        Cleans up the scene. Deletes things that are not connected to anything and fixes various
        problems.
        '''
        self._log.LogMessage("Start cleaning the scene...")
        componentDeleteList = []

        # Find and delete control sets that are not connected to a character
        for controlSet in [component for component in Globals.Components
                           if isinstance(component, (FBControlSet))]:

            # Control set is not connected to character
            if controlSet.GetDstCount() <= 1:

                # Add effectors and control rig to delete list
                componentDeleteList.extend(
                    ControlSet.GetAllEffectors(controlSet) or [])
                componentDeleteList.append(controlSet)

        # Delete videos, constraints, materials, textures and shaders that are
        # not attached to anything
        for component in [component for component in Globals.Components
                          if isinstance(component, (FBVideoClip,
                                                    FBConstraint,
                                                    FBTexture,
                                                    FBMaterial,
                                                    FBShader))]:
            if component.GetDstCount() <= 1: # Not connected to anything
                componentDeleteList.append(component)

        # Delete empty Sets, Groups and Folders
        for component in [component for component in Globals.Components
                          if isinstance(component, (FBSet,
                                                    FBGroup,
                                                    FBFolder))
                          # check for this particular group for Glenn, so it isnt deleted if empty
                          and component.Name != 'CHARACTERS']:
            if len(component.Items) == 0:
                componentDeleteList.append(component)

        # Delete any HIK solvers
        for constraintSolver in Globals.Scene.ConstraintSolvers:
            if constraintSolver.UniqueName is not None and 'HIK' in constraintSolver.UniqueName:
                componentDeleteList.append(constraintSolver)

        # Execute the delete of collected components
        if (len(componentDeleteList) > 0):
            self._log.LogMessage("Deleting {0} component(s)".format(len(componentDeleteList)))
            Component.Delete(componentDeleteList)

        # Get the visualmodel list from under the reference namespace and check if the namespace of the model
        # is corresponds to the namespace of the textures
        for reference in self.GetReferenceListAll():
            if issubclass(type(reference), Complex):
                reference.CheckNamespaceConsistency()

        # fix any incidents of double namespaces on handles - these particular double namespaces
        # seem to cause crashes.
        for reference in self.GetReferenceListAll():
            if issubclass(type(reference), Complex):
                reference.FixHandleDoubleNamespace()

        self._log.LogMessage("End cleaning the scene.")

    def SetConsoleOutputState(self, value):
        '''
        Sets the manager log to output to the console.
        '''
        self._log.ConsoleOutput = value

    def ShowLog(self):
        '''
        Shows the log.
        '''
        self._log.Show()

    def GetReferenceListByType(self, targetType):
        '''
        Returns a list of references of the passed type.
        '''
        self._reload()

        return list(reference for reference in self._referenceDictionary.values() if type(reference) == targetType)

    def IsReferenced(self, targetNamespace):
        '''
        Check to see if a passed namespace has a reference.

        Returns:
            bool
        '''
        self._reload()

        if self._usingNamespace(targetNamespace):
            return True
        else:
            return False

    def GetReferenceListAll(self, forceReload=False):
        '''
        Returns a list of all references in the scene.
        '''
        self._reload(force=forceReload)

        return self._referenceDictionary.values()

    def GetReferenceByNamespace(self, targetNamespace):
        '''
        Returns a reference that matches the passed namespace.
        '''
        self._reload()

        if self._usingNamespace(targetNamespace):
            return self._referenceDictionary[targetNamespace]
        else:
            self._log.LogError("No reference with the passed namespace")
            raise Exceptions.MissingReference()

    def GetNamespaceList(self, targetNamespace):
        '''
        Returns all namespaces that are in use within the scene.
        '''
        self._reload()

        return self._referenceDictionary.keys()

    def GetReferenceTypeList(self):
        '''
        Returns a list of all the reference types, including top categories and sub categories.
        '''
        typeList = [AmbientFace,
                    Audio,
                    Character,
                    Face,
                    Prop,
                    RayFire,
                    Set,
                    Vehicle,
                    MocapEssential,
                    MocapBlockingModel,
                    MocapCamera,
                    MocapGsSkeleton,
                    MocapPropToy,
                    MocapSlate,
                    MocapStage,
                    MocapNonEssential,
                    MocapBasePreviz,
                    ]

        return typeList

    def GetReferenceHierarchyList(self):
        '''
        Returns a list of all the top reference type categories.
        Does not include sub catergories.
        '''
        typeList = [AmbientFace,
                    Audio,
                    Character,
                    Face,
                    Prop,
                    RayFire,
                    Set,
                    Vehicle,
                    MocapEssential,
                    MocapNonEssential,
                    ]

        return typeList

    def GetTypeFromPath(self, path):
        """
        Get the reference type of an fbx by its path.

        Arguments:
            path (string): path to the fbx file

        Return:
            RS.Core.ReferenceSystem.Types.Base.Base or None
        """
        try:
            return self._resolveTypeByPath(path)
        except Exception:
            pass


    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Creating References")
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    def CreateReferences(self, filePathList):
        '''
        Creates a new reference from the passed filePathList
        '''
        self._reload()

        p4Sync = self.PerforceSync

        if type(filePathList) != list:
            filePathList = [filePathList]

        # Fix possible issue with the path
        for i, path in enumerate(filePathList):
            filePathList[i] = os.path.normpath(str(path))

        # Start our sync operation on the first path
        if p4Sync is True:
            currentSyncOperation = Perforce.SyncOperation(Collections.Peek(filePathList),
                                                          start=True)

        # List to store new references created
        newReferenceList = []
        while Collections.HasItems(filePathList):
            # Get the current path
            filePath = Collections.Pop(filePathList)

            # Wait until the sync operation for this reference is complete.
            if p4Sync is True:
                currentSyncOperation.WaitForCompletion()

            # If there is another reference on the list then queue the sync
            # operation
            if Collections.HasItems(filePathList) and p4Sync is True:
                currentSyncOperation = Perforce.SyncOperation(Collections.Peek(filePathList),
                                                              start=True)
            # Setup our log context
            self._log.LogMessage(
                "Starting creation of reference [{0}].".format(filePath))

            referenceType = self._resolveTypeByPath(filePath)
            if referenceType is None:
                raise Exceptions.IncorrectFileDirectory()

            self._log.LogMessage("Reference type detected as [{0}].".format(referenceType.FormattedTypeName))

            # If we don't have a root null we will have to create one at this
            # point
            if self._rootNull == None:
                self._log.LogMessage("Creating new root null object as this is fresh/empty scene.")
                self._rootNull = FBModelNull("REFERENCE:Scene")

            # Create our model null and associated properties
            self._log.LogMessage("Creating new reference null object and setting up properties.")

            # Get our namespace
            fileName = Path.GetBaseNameNoExtension(filePath)
            namespace = self._resolveNamespace(fileName)
            self._log.LogMessage("Namespace derived as [{0}].".format(namespace))

            # Create our modelNull and Link it up to the root null and set the
            # asset type correctly
            modelNull = FBModelNull("RS_Null:{0}".format(namespace))
            modelNull.Parent = self._rootNull

            # Create our properties.
            modelNull.PropertyCreate(
                Core.STR_REFERENCE_PATH, FBPropertyType.kFBPT_charptr, 'String', False, True, None)
            modelNull.PropertyCreate(
                Core.STR_NAMESPACE, FBPropertyType.kFBPT_charptr, 'String', False, True, None)
            modelNull.PropertyCreate(
                Core.STR_RS_ASSET_TYPE, FBPropertyType.kFBPT_charptr, 'String', False, True, None)
            modelNull.PropertyCreate(
                Core.STR_UPDATE, FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
            modelNull.PropertyCreate(
                Core.STR_P4_VERSION, FBPropertyType.kFBPT_charptr, 'String', False, True, None)
            modelNull.PropertyCreate(
                Core.STR_PROXY_ASSET, FBPropertyType.kFBPT_bool, 'Bool', False, True, None)

            modelNull.PropertyList.Find(
                Core.STR_RS_ASSET_TYPE).Data = referenceType.PropertyTypeName
            modelNull.PropertyList.Find(Core.STR_NAMESPACE).Data = namespace
            modelNull.PropertyList.Find(
                Core.STR_REFERENCE_PATH).Data = os.path.normpath(filePath)
            # We set this to zero to start with which will be used to indicate
            # a 'create' operation
            modelNull.PropertyList.Find(Core.STR_UPDATE).Data = 0
            modelNull.PropertyList.Find(Core.STR_PROXY_ASSET).Data = 0

            try:
                # Pass it to the parse reference function which handle the rest
                # of the creation and adding to list of references
                newReference = referenceType(modelNull, self._log)

                # Saving the UserObjects- export Rage- saves changes made to
                # the settings
                self._saveExportSettings()

                # If this was successful then create the new reference
                newReference._create()

                # Log that we created the reference correctly and return the
                # reference
                self._log.LogMessage("Successfully created new reference: {0}.".format(newReference.Namespace))

                newReferenceList.append(newReference)

                self._referenceDictionary[newReference.Namespace] = newReference

            except Exception as e:

                # log error and clean up the null and anything merged in so the scene
                # should be back to the state it was previously.
                deleteComponentList = []
                if modelNull is not None:
                    deleteComponentList.append(modelNull)
                if namespace is not None:
                    mergedComponentList = [component for component in Globals.Components
                                           if Component.GetNamespace(component) == namespace ]
                    deleteComponentList.extend(mergedComponentList)

                Component.Delete(deleteComponentList)
                self._log.LogError("Creation aborted - {0}".format(e.message))
                raise

        return newReferenceList

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Swapping References")
    def SwapReference(self, reference, theNewPath):
        '''
        Swaps one reference out for to a new file path.
        '''
        self._log.LogMessage("Begin Swap Reference...")

        self._checkReference(reference)

        originalNamespace = reference.Namespace
        originalPath = reference.Path

        newPath = os.path.normpath(str(theNewPath))
        fileName = Path.GetBaseNameNoExtension(newPath)
        newNamespace = self._resolveNamespace(fileName)

        referenceType = self._resolveTypeByPath(newPath)

        if(referenceType != type(reference)):
            self._log.LogError("Error can't swap references as they are different types.")
        else:
            # Change path and version - push through as a major update
            reference._referencePathProperty.Data = newPath
            reference._p4VersionProperty.Data = '0'

            fileName = Path.GetBaseNameNoExtension(newPath)
            self.UpdateReferences(reference, True)
            self.ChangeNamespace(reference, self._resolveNamespace(fileName))
            self._log.LogMessage("Swap Reference end.")

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Change Namespace")
    @Decorators.CleanScene
    def ChangeNamespace(self, reference, newNamespace):
        '''
        Changes the namespace of a single reference
        '''
        self._log.LogMessage("Begin Namespace Change...")
        self._checkReference(reference)

        # Check we aren't already using the namespace
        if self._usingNamespace(newNamespace):
            self._log.LogError("Namespace already in use")
            newNamespace = self._resolveNamespace(newNamespace)

        oldNamespace = reference.Namespace

        # this is on run for specific reference types - if it doesnt fall under a specific type it will pass
        reference.ChangeLabelName(oldNamespace, newNamespace)

        # continue with general namespace update
        reference._namespaceProperty.Data = newNamespace
        reference._modelNull.Name = newNamespace

        # rename
        Namespace.Rename(oldNamespace, newNamespace)

        # Change the device namespace
        if isinstance(reference, Character):
            reference._fixExpressionDevices()

        del self._referenceDictionary[oldNamespace]
        self._referenceDictionary[newNamespace] = reference

        # check handles for duplicated namespace
        reference.FixHandleDoubleNamespace()
        self._log.LogMessage("Namespace Change end.")

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Duplicating References")
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    def DuplicateReferences(self, referenceList):
        '''
        Duplicates the passed in reference.
        '''
        self._log.LogMessage("Begin Duplicating Reference...")
        referenceList = self._processReferences(referenceList)

        filePathList = []
        for ref in list(referenceList):
            filePathList.append(ref.Path)

        self.CreateReferences(filePathList)
        self._log.LogMessage("Successfully created a duplicate reference.")

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Deleting References")
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    def DeleteReferences(self, referenceList):
        '''
        Deletes the passed in references.
        '''
        referenceList = self._processReferences(referenceList)

        self._log.LogMessage("Deleting [{0}] reference(s)".format(len(referenceList)))

        # For reference in referenceList
        for reference in referenceList:

            # save the namespace
            refNamespace = reference.Namespace

            # Call delete on reference (removes namespace from scene)
            # Delete the null object
            reference._delete()

            # Remove from dictionary
            self._referenceDictionary.pop(refNamespace, None)
            self._log.LogMessage("Successfully deleted reference: {0}".format(refNamespace))

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Updating References")
    @Decorators.ExceptionMessage
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    @Decorators.DeactivateFileOpenCallbacks(True)
    def UpdateReferences(self, referenceList, force=False):
        '''
        Updates the passed references. If the user passes force then an update will be
        preformed regardless of version number. In the case of characters this will also force a full
        update of the character even if the version is not marked as 'Major'.
        '''
        p4Sync = self.PerforceSync

        referenceList = self._processReferences(referenceList)

        # Setup our log context
        self._log.LogMessage("Updating {0} reference(s) - Force:{1}".format(len(referenceList),
                                                                            str(force).upper()))
        # Start our sync operation on the first reference.
        if p4Sync is True:
            currentSyncOperation = Perforce.SyncOperation(Collections.Peek(referenceList).Path,
                                                          start=True)
        while Collections.HasItems(referenceList):

            # Get the current reference
            currentReference = Collections.Pop(referenceList)

            self._log.LogMessage("Updating reference - {0}".format(currentReference.Namespace))

            # Wait until the sync operation for this reference is complete.
            if p4Sync is True:
                currentSyncOperation.WaitForCompletion()

            # If there is another reference on the list then queue the sync
            # operation
            if Collections.HasItems(referenceList) and p4Sync is True:
                currentSyncOperation = Perforce.SyncOperation(Collections.Peek(referenceList).Path,
                                                              start=True)

            # If we are not forcing check if the revision we have is the
            # same as the one we are currently on
            fileState = Perforce.GetFileState(currentReference.Path)
            if fileState == []:
                self._log.LogWarning("File {0} not in P4 - skipping reference.".format(currentReference.Path))
                continue

            if not force and fileState.HeadRevision == currentReference.CurrentVersion:
                self._log.LogMessage(
                    "Skipping reference {0} as it's upto date.".format(currentReference.Namespace))
                self._log.LogMessage(
                    "Use the 'Force' option if you would like to push through the update and 'refresh' the reference.")
                continue

            else:
                # UserObject data is lost if we do not save before we merge
                # in other files
                self._saveExportSettings()

                currentReference._update(force)
                self._log.LogMessage("Successfully updated reference: [{0}]".format(currentReference.Namespace))

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Stripping References")
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    def StripReferences(self, referenceList):
        '''
        Strips the passed in references of their non essential components.
        '''
        referenceList = self._processReferences(referenceList)

        # Setup our log context
        self._log.LogMessage("Stripping [{0}] reference(s)".format(len(referenceList)))

        for reference in referenceList:
            try:
                self._log.LogMessage("Stripping reference [{0}] as [{1}]".format(
                    reference.Namespace, type(reference).FormattedTypeName))
                startTime = time.time()

                reference._strip()

                reference._setCurrentVersion(-1)
                endTime = time.time()

                self._log.LogMessage("Successfully stripped reference. Took {0:.2f} seconds".format(endTime - startTime))
            except Exception as e:
                self._log.LogError("Error stripping reference - {0}.".format(str(e)))

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Reloading Textures")
    @Decorators.IsListEmpty
    def ReloadTextures(self, referenceList, force=False):
        '''
        Syncs and calls a texture reload on all the references in the passed list.
        '''
        p4Sync = self.PerforceSync
        if p4Sync is False:
            self._log.LogWarning("P4 Syncing is turned off so textures will not be synced")
            return

        referenceList = self._processReferences(referenceList)
        complexReferenceList = [reference for reference in referenceList
                                if isinstance(reference, Complex)]

        if len(complexReferenceList) == 0:
            self._log.LogWarning("No valid references to reload texures on.")
            return
        else:
            self._log.LogMessage("Syncing and reloading textures for {0} reference(s)".format(len(complexReferenceList)))

        # Gather up a list of unique texture paths
        textureFilePathSet = set()
        for complexReference in complexReferenceList:

            # Collects the file paths of the textures
            textureComponents = [component for component in Globals.Components
                                 if isinstance(component, mobu.FBTexture)
                                 and Component.GetNamespace(component) == complexReference.Namespace]

            # Moving from TGA to Tifs, this code checks for both types of
            # texture extension
            for textureComponent in textureComponents:

                textureFilePath = None

                # Draw filepath from the video
                if textureComponent.Video is not None:
                    textureFilePath = textureComponent.Video.Filename

                # Draw filepath from the backup property - this gets added during strip
                elif textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH) is not None:
                    textureFilePath = textureComponent.PropertyList.Find(Core.STR_VIDEO_PATH).Data

                if textureFilePath is not None:
                    textureFilePathSet.add(textureFilePath)

                    # Also add a .tiff - new version of textures that we want to move to
                    textureDirectoryPath, textureFileName = os.path.split(textureFilePath)
                    textureFileName, textureFileExtension = os.path.splitext(textureFileName)
                    if textureFileExtension == '.tga':
                        textureFilePathSet.add(os.path.join(textureDirectoryPath,
                                                            '{0}.tif'.format(textureFileName)))

        # Exit script if no textures have been found
        if len(textureFilePathSet) == 0:
            self._log.LogMessage("No texture filepaths found to sync/reload")
        else:
            # Pass this to P4 in one big batch for syncing
            self._log.LogMessage("Syncing {0} texture file(s)".format(len(textureFilePathSet)))
            Perforce.Sync(list(textureFilePathSet), force)

        # Now reload the textures
        self._log.LogMessage("Starting refresh of textures...")
        for complexReference in complexReferenceList:
            complexReference._reloadTextures()

        self._log.LogMessage("Finished refresh of textures.")

    @Decorators.ExceptionMessage
    @Decorators.Logger("Manager - Stripping Textures")
    @Decorators.IsListEmpty
    @Decorators.CleanScene
    def StripTextures(self, referenceList):
        '''
        Actually strips the textures off the models for each of the passed in references.
        '''
        referenceList = self._processReferences(referenceList)
        complexReferenceList = [reference for reference in referenceList
                                if isinstance(reference, Complex)]

        # Strip textures
        for complexReference in complexReferenceList:
            self._log.LogMessage("Starting strip of textures for Reference: {0}...".format(complexReference))
            complexReference._stripTextures()
            self._log.LogMessage("Finished strip of textures.")

    @property
    def PerforceSync(self):
        '''
        Check if the user has P4 syncing turned on or off
        Returns:
            Boolean (True = On, False = Off)
        '''
        return self._p4SyncStatus

    @PerforceSync.setter
    @Decorators.ExceptionMessage
    @Decorators.Logger("PerforceSync - Setting P4 Sync Value (On/Off)")
    def PerforceSync(self, value):
        '''
        sets P4 syncing to be turned on, or off, depending on the arg value passed
        '''
        self._log.LogMessage("Setting P4 Sync Value to {0}".format(value))
        self._p4SyncStatus = value

    @property
    def IgnoreProject(self):
        """ Is the reference editor ignoring projects """
        return self._ignoreProject

    @IgnoreProject.setter
    def IgnoreProject(self, value):
        """ Set the reference editor to ignore projects """
        self._ignoreProject = value

    @property
    def Silent(self):
        """ Is the reference editor not bringing up warnings"""
        return self._silent

    @Silent.setter
    def Silent(self, value):
        """ Set the reference editor to not bringing up warnings"""
        self._silent = value

    @property
    def Debug(self):
        """ Is the references editor in debug mode """
        return self._debug

    @Debug.setter
    def Debug(self, value):
        """ Set the reference editor to debug mode"""
        self._debug = value

    # DEPRECATED METHODS
    @Deprecated
    def SetDebug(self, value):
        """ Set the reference editor to debug mode

        Arguments:
            value (bool): to activate debug mode (True) or not (False)
        """
        self.Debug = value

    @Deprecated
    def SetSilent(self, value):
        """
        Sets the manager to be in silent mode - prevents any message boxes appearing.

        Arguments:
            value (bool): to ignore message boxes (True) or not (False)
        """
        self.Silent = value

    @Deprecated
    def SetIgnoreProject(self, value):
        """
        Sets whether the manager should ignore the current project when looking for references in the scene

        Arguments:
            value (bool): to ignore the project (True) or not (False)
        """
        self.IgnoreProject = value