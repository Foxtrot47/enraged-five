
from PySide import QtGui

from RS.Utils import Exception as RSException


class NotAnError(RSException.SlientError):
    """
    Raises if not really an error, but aborting sliently for whatever reason
    """
    pass


class IncorrectFileDirectory(RSException.SlientError):
    """
    Raises if the user attempts to import a reference from an unknown directory

    Doesn't send an error, but will generate a popup notifying the user of the issue.
    """
    pass


class DeletedFile(RSException.SlientError):
    """
    Raises if the user attempts to import a reference from an unknown directory
    """
    pass


class CharacterComponentMissing(RSException.SlientError):
    """
    Raises if the user attempts to run a function on a character reference where the namespace is
    missing from the character component - either it has a different name or doesn't exist at all.
    """
    pass


class MultipleCharacterComponents(RSException.SlientError):
    """
    Raises if the user attempts to run a function on a character reference where the namespace is
    present in mulitple character components - there should only be one.
    Example:
    Player_Zero:Player_Zero
    Player_Zero:player_zero
    """
    pass


class MissingReference(RSException.SlientError):
    """
    Raises if the user attempts to run a function and a reference cannot be found with the passed
    namespace.
    """
    pass


class UnsupportedConstraint(RSException.SlientError):
    """
    Raises if the user attempts to run a function on a reference with a constraint we are unable to
    support (found in major updates where we have to copy anims from one character to another).
    """
    pass

