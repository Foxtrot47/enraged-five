'''
Usage:
    This contains logic to help interaction with RAYFIRE references
Author:
    Ross George
    Bianca Rudareanu
'''

from pyfbsdk import *

import re
import os

from RS.Core.ReferenceSystem.Types.Base import Base
from RS.Utils.Scene import Component
import RS.Config as Config
import RS.Utils.Path
import RS.Utils.Scene
import RS.Globals as Globals
from RS.Core.ReferenceSystem.Decorators import Logger


class RayFire(Base):
    '''
    Represents a rayfire reference in a scene
    '''
    PropertyTypeName    = 'RayFire'
    FormattedTypeName   = 'RayFire'
    TypePathList        = [os.path.join('art', 'animation', 'resources', 'rayfire'),
                           os.path.join('art', 'dev','animation', 'resources', 'rayfire'),
                           ]
    FileType            = 'fbx'

    @Logger("Rayfire - Create")
    def _create(self):
        self._log.LogMessage("Creating rayfire reference.")
        super(RayFire, self)._create()

        self._strip()

        rayFireList = []
        RS.Utils.Scene.GetChildren( self.GetRootModel() , rayFireList )

        for camera in Globals.gCameras:
            if camera.Name == "Producer Perspective":
                camera.PropertyList.Find("Far Plane").Data = 400000

        rayfireGroup = FBGroup( "Rayfire" )
        rayfireGroup.ConnectSrc( self.GetRootModel() )

        redMaterial = FBMaterial( "Rayfire_Material" )
        redMaterial.Diffuse = FBColor( 1,0,0.1 )

        for null in rayFireList:
            null.Materials.append(redMaterial)
            null.ShadingMode = FBModelShadingMode.kFBModelShadingAll
            rayfireGroup.ConnectSrc( null )

        for take in Globals.gTakes:
            if take.Name == "Take 001":
                take.Name = os.path.basename( self.Path )

        FBPlayerControl().LoopStart = FBTime(0,0,0,0)

        splitPathName = self.Path.split(".")
        newFilePath = splitPathName[0] + "_animation.FBX"

    @Logger("Rayfire - Strip")
    def _strip(self):
        self._log.LogMessage("Stripping rayfire reference.")

        detachList = []
        removeList = []
        rayFireMeshList = []

        rayFireList = []
        RS.Utils.Scene.GetChildren( self.GetRootModel() , rayFireList )

        for material in Component.GetComponents( self.Namespace, FBMaterial, False ):
            detachList.append( material )
            removeList.append( material )

        for component in detachList:
            for null in rayFireList:
                    null.DisconnectSrc( component )

        for component in removeList:
            if component.Name != "DefaultMaterial":
                component.FBDelete()


    def GetRootModel( self ):
        '''
        Returns the root model
        '''
        return FBFindModelByLabelName('{0}:Dummy01'.format( self.Namespace ))


    def _proxySetup ( self ):
        '''
        Turns the current reference into a proxy
        '''