'''
Usage:
    This contains logic for the BASE reference which is the abstract parent class for all other reference types
Author:
    Ross George
    Bianca Rudareanu
'''
from fbx import *
from FbxCommon import *

import pyfbsdk as mobu

import os
import re

from PySide import QtGui

import RS.Utils.Logging.Universal
import RS.Core.ReferenceSystem
from RS.Utils import Scene
from RS.Core.ReferenceSystem.Decorators import Logger
from RS.Utils.Scene import Component
from RS import Globals, Perforce
from RS.Core.ReferenceSystem import Exceptions


class Base(object):
    '''
    Represents a reference in a scene. Abstract class that doesn't get initialised directly.
    '''
    SubCats = None

    def __init__(self, modelNull, log):
        '''
        Update reference data from an existing model null object.
        '''
        self._modelNull = modelNull
        self._referencePathProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_REFERENCE_PATH)
        self._namespaceProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_NAMESPACE)
        self._updateProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_UPDATE)
        self._p4VersionProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_P4_VERSION)
        self._assetTypeProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_RS_ASSET_TYPE)
        self._proxyAssetProperty = modelNull.PropertyList.Find(
            RS.Core.ReferenceSystem.STR_PROXY_ASSET)
        self._latestVersion = None
        self._log = log

    @Logger("Base - Creating Reference")
    def _create(self):
        '''
        This is the function is called when a reference is created. The logic is slighly different
        when creating to when updating.
        '''
        mergeOptions = RS.Core.ReferenceSystem.CreateBasicMergeOptions(self.Path)
        mergeOptions.NamespaceList = self.Namespace

        self._log.LogMessage("Start merge of reference: {0}".format(self.Namespace))
        RS.Globals.Application.FileMerge(self.Path, False, mergeOptions)
        self._log.LogMessage("Merge reference end.")

        # Update Reference P4 Version of character
        if os.path.exists(self.Path):
            fileState = RS.Perforce.GetFileState(self.Path)
            if isinstance(fileState, list):
                # at the moment assets coming from GC's server cannot be found as we are unable to
                # switch servers just yet. This is a temp fix until we can start switching.
                if len(fileState) == 0:
                    self._p4VersionProperty.Data = '-1'
                    return
            self._p4VersionProperty.Data = str(RS.Perforce.GetFileState(self.Path).HaveRevision)
        else:
            # [R.G] should we log the error here or allow the error to be logged further up the stack (by the Logger?)
            self._log.LogError("Error creating reference - {0}".format(self.Namespace))
            raise Exceptions.DeletedFile()

    def _update(self, force=False):
        '''
        Preforms an update of the reference. This should be called via the manager
        as it will queue the sync commands and wrap the operation in some MergeTransaction calls.
        This is will likely be overloaded by each of the inherited class.
        '''

    def _delete(self):
        '''
        Deletes the reference. This should be called via the manager
        as it will also delete the null and wrap mutiple delete calls for efficency.
        '''
        RS.Utils.Namespace.Delete(self.Namespace, deleteNamespaces=True, additionalComponents=(self._modelNull,))

    @property
    def ModelNull(self):
        return self._modelNull

    @property
    def Name(self):
        '''
        Gets the name of the reference.
        '''
        return os.path.splitext(os.path.basename(self.Path))[0]

    @property
    def Path(self):
        '''
        Gets the path for the reference.
        '''
        return self._referencePathProperty.Data

    @property
    def Namespace(self):
        '''
        Gets the namespace for the reference.
        '''
        return self._namespaceProperty.Data

    @property
    def CurrentVersion(self):
        '''
        Gets the version number that is currently referenced in the scene.
        '''
        return int(self._p4VersionProperty.Data or -1)

    def _setCurrentVersion(self, value):
        '''
        Sets the CurrentVersion property on the modelNull
        '''
        self._p4VersionProperty.Data = str(value)

    def ChangeLabelName(self, oldNamespace, newNamespace):
        """
        Virtual Method - need to be reimplemented in sub-classes when needed.
        For updating a Handle's Label string
        """
        pass

    @property
    def LatestVersion(self):
        '''
        Gets the version number that is latest in perforce.
        '''
        if self._latestVersion == None:
            fileState = Perforce.GetFileState(self._referencePathProperty.Data)
            if not isinstance(fileState, list):
                self._latestVersion = int(fileState.HeadRevision)
            else:
                self._latestVersion = 0
        return self._latestVersion

    def IsDestroyed(self):
        """ Has the reference been destroyed """
        return Component.IsDestroyed(self._modelNull)

    def Validate(self):
        '''
        Validates the reference (make sure path to the asset is the same with the project and other things)
        '''
        # ToDo implement logic here.

    @property
    def IsProxy(self):
        '''
        Returns weather or not the asset is a proxy
        '''
        if not self._proxyAssetProperty:
            self._proxyAssetProperty = self._modelNull.PropertyCreate(
                RS.Core.ReferenceSystem.STR_PROXY_ASSET, mobu.FBPropertyType.kFBPT_bool, 'Bool', False, True, None)
            self._proxyAssetProperty.Data = 0
        return self._proxyAssetProperty.Data

    @IsProxy.setter
    def IsProxy(self, value):
        '''
        Turns the current asset into a proxy
        '''
        self._proxyAssetProperty.Data = value
        if value:
            self._proxySetup()

    def GetVisualModelList(self):
        '''
        Returns a list of all the geometry elements that are currently being used as skin for the vehicle
        '''
        models = Component.GetComponents(Namespace=self.Namespace,
                                         TypeList=[mobu.FBModel],
                                         DerivedTypes=False,
                                         ExcludeRegex='(?i)mover|text|rect|contact|ctrl|frm')

        visualModelList = list()
        for model in models:
            if type(model.Geometry) == mobu.FBMesh:
                if model.Geometry.PolygonCount() > 30:
                    visualModelList.append(model)

        return visualModelList

    def HideVisualModels(self):
        '''
        Hides all the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Show = False

    def UnhideVisualModels(self):
        '''
        Unhides all the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Show = True

    def SelectVisualModels(self):
        '''
        selects all of the skin models of the reference
        '''
        for visualModel in self.GetVisualModelList():
            visualModel.Selected = True