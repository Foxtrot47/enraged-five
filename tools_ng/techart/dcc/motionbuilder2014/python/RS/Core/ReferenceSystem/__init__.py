'''
Usage:
    Some core functions and constants related to the reference system
Author:
    Ross George
    Bianca Rudareanu
'''

import pyfbsdk as mobu
import RS.Core


#ModelNull core FBProperty names
STR_NAMESPACE = 'Namespace'
STR_RS_TYPE = 'rs_Type'
STR_RS_ASSET_TYPE = 'rs_Asset_Type'
STR_UPDATE = 'Update'
STR_P4_VERSION = 'P4_Version'
STR_BUG_NUMBERS = 'Bug Numbers'
STR_REFERENCE_PATH = 'Reference Path'
STR_UDP3DSMAX = 'UDP3DSMAX'
STR_FACE_REFERENCE = 'faceReference'
STR_TAKE_NAME = 'Take_Name'
STR_TARGET_NAMESPACE = 'Target_Namespace'
STR_IMPORT_OPTION = 'Import_Option'
STR_CREATE_MISSING = 'Create_Missing_Takes'
STR_OUTFIT = 'Outfit'
STR_PROXY_ASSET = 'ProxyAsset'
STR_VIDEO_PATH = 'Video Path'

def CreateBasicMergeOptions(FilePath):

    #Setup merge options
    mergeOptions = mobu.FBFbxOptions(True, FilePath)
    mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionMerge, False)
    for takeIndex in range(mergeOptions.GetTakeCount()):
        mergeOptions.SetTakeSelect(takeIndex, False)

    # For Bug 731622 turn story merge OFF when sync/merging files
    mergeOptions.Story  = mobu.FBElementAction.kFBElementActionDiscard
    mergeOptions.Lights = mobu.FBElementAction.kFBElementActionDiscard

    mergeOptions.BaseCameras = False #1195174
    mergeOptions.CameraSwitcherSettings = False
    mergeOptions.CurrentCameraSettings = False
    mergeOptions.CamerasAnimation = False
    mergeOptions.GlobalLightingSettings = False
    mergeOptions.ConstraintsAnimation = True #1854762
    mergeOptions.UpdateRecentFiles = False #This will stop the merged file getting added to the recent file list
    mergeOptions.CacheSize = 1572864000 #Set this to maximum (1500MB) as it does invariably help but never hinders
    mergeOptions.SetPropertyStaticIfPossible = False #2536974
    mergeOptions.EmbedMedia = False #2557110

    return mergeOptions