Bugstar --- :mod:`RS.Utils.Bugstar`
***********************************

Module for working with Bugstar.

.. note::

	This module interfaces with the .NET assembly **RSG.Model.Bugstar**.

Functions
=========
.. automodule:: RS.Utils.Bugstar
	:members:
	:undoc-members:
