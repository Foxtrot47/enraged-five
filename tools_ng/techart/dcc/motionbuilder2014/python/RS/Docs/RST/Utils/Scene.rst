Scene --- :mod:`RS.Utils.Scene`
*******************************

Helper utilities to make working with the MotionBuilder scene easier.

Functions
=========
.. automodule:: RS.Utils.Scene
    :members:
    :undoc-members: