Commands --- :mod:`RS.UI.Commands`
**********************************

The purpose of this module is to contain functions that can be called from the :doc:`../menu`.  This avoids
having to create separate code files for each menu item.

There are two requirements for this to work.

* Function defined in this module, taking no arguments.
* The function name is defined in the menu.config file.

For example: ::

  # Define a function in the commands module.
  def MyMenuCommand():
     print "Hello, world!"

  # Define the menu item in the menu.config file.
  <menu displayName="My Menu Command" command="MyMenuCommand" />

When the menu item "My Menu Command" is clicked on from the Rockstar menu inside of MotionBuilder, the "MyMenuCommand" function
defined in the commands module will be executed.
  


