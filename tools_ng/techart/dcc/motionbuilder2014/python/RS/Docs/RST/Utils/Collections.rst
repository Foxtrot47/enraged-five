Collections --- :mod:`RS.Utils.Collections`
*******************************************

Helper utilities to make working with lists, tuples and dictionaries easier.

.. warning::

	This module is only for performing operations on lists, tuples and dictionaries!

Functions
=========
.. autofunction:: RS.Utils.Collections.RemoveDuplicatesFromList

.. autofunction:: RS.Utils.Collections.ReturnUnmatchedItems
