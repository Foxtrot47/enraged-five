Tools Framework Overview
========================

This page provides an overview of our MotionBuilder Python tools framework.


Development Practice
--------------------

Our Python tools live in two different places.  The primary development branch is located here: ::

    //wildwest/dcc/motionbuilder2014/...
    
That is where all active development occurs, and then code is integrated down to the project here when it is ready to be published: ::

    <project>/tools/dcc/current/motionbuilder2014/...
    
.. warning::

    Code development should NEVER happen in the project branch.  The project branch should be a snapshot of
    what was developed in the global development depot.
    
    If development happens in the project branch, then code runs the risk of being stomped over.  Or worse, the code
    changes will only ever live in the project branch, never making their way back to the global development depot
    for other projects to use.
    
In order to setup your MotionBuilder environment to work out of the global development depot, you should run the following file: ::

    //wildwest/dcc/motionbuilder2014/Setup.bat
    
That will setup MotionBuilder to load our Python tools out of the global development depot.  When you need to test your changes in
the project, please run the following file, and then re-open MotionBuilder: ::

    <project>/tools/dcc/current/motionbuilder2014/Setup.bat
    
The project version of **Setup.bat** will ensure MotionBuilder is loading our Python tools from the project depot and not the global development
depot.
    

Python Tools
------------

Our tools use the Python package scheme, and live under here: ::

    # Global development depot.
    //wildwest/dcc/motionbuilder2014/python/...
    
    # Project depot.
    <project>/tools/dcc/current/motionbuilder2014/python/...
    
There are two different packages under the Python folder.  The primary one is labeled **RS**.  The other is **external**, which is reserved for
any external tools that we did not develop ourselves.  Everything else belongs under the **RS** package.

This means that when you import one of our tools inside of MotionBuilder, all of your imports will start with **RS**, unless it
is an external package.  In that case, you would just import the external package directly.  For example: ::

    # From the RS package.
    import RS.Utils.Bugstar
    import RS.Utils.Logging.Universal
    
    # From the external package.
    import PIL
    import pyodbc
    
.. seealso::

    `Python Coding Standards <https://devstar.rockstargames.com/wiki/index.php/Python_Coding_Standards>`_


.NET Framework
--------------
MotionBuilder uses CPython, which doesn't natively support usage of the .NET Framework.  However, we do
have this capability through the **clr** module, which is an external DLL.  This provides access to our tools pipeline
compiled DLL's, which are primarily built using the .NET framework.

Many of the utiltiy Python modules under RS.Utils use our .NET assemblies, allowing access to systems such as Bugstar, Configuration Settings
and the Universal Log.

Our .NET assemblies live in the project depot, and are located here: ::

    <tools>/dcc/current/motionbuilder2014/assemblies/x64/...
    
.. note::

    The .NET assemblies do NOT live in the Tech Art global development depot (//wildwest/...).  They live in the project depot.
    
Using the .NET assemblies is very straightforward.  For example: ::

    # This module allows us to load the .NET assemblies.
    import clr
    
    # Add the .NET assembly to clr.
    clr.AddReference( 'RSG.SourceControl.Perforce' )
    
    # Now import something from the assembly.
    from RSG.SourceControl.Perforce import P4
    
    # Use the imported assembly just like you would any other Python module.
    p4 = P4()
    p4.run( 'sync', filesToSync )
    
It can be difficult to determine how to use a .NET assembly, or even what's available.  In that case, it is recommended that you
get a license of `.NET Reflector <http://www.red-gate.com/products/dotnet-development/reflector/>`_ from your IT department.  It will allow you to inspect the DLL very easily without having to bug
a tools programmer.


MotionBuilder Setup
-------------------
In order for our tools to be loaded by MotionBuilder, we have several environment variables setup by the Rockstar tools installer.

+------------------------------+-----------------------------------------------------+
| Environment Variable         | Description                                         |
+==============================+=====================================================+
| MOTIONBUILDER_ASSEMBLY_PATH  | The location of the .NET assemblies.                |
+------------------------------+-----------------------------------------------------+
| MOTIONBUILDER_PLUGIN_PATH    | The location of the plugins.                        |
+------------------------------+-----------------------------------------------------+
| MOTIONBUILDER_PYTHON_STARTUP | The location of the Python startup directory.       |
+------------------------------+-----------------------------------------------------+

The entry point to setting up our tools is the Python startup directory, which will be located here: ::

    ../motionbuilder2014/python/startup/...
    
In that directory is a file named **!startup.py**, which initializes our Python tools framework.  After that script is run, all other scripts in that directory
will be executed as MotionBuilder is loading.

This is also where the Rockstar menu is setup inside of MotionBuilder, which you can read more about here :doc:`menu`.