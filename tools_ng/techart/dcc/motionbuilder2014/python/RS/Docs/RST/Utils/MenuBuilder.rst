Menu Builder --- :mod:`RS.Utils.MenuBuilder`
********************************************

This module is used to build the Rockstar menu inside of MotionBuilder.  For more information on how to work with
the menu system, please visit :doc:`../menu`.
