Email --- :mod:`RS.Utils.Email`
*******************************

Sends an email.  The supported attachment types are: ::

  xml, ulog, txt, ms, py, ini, png, tga, jpg, bmp

.. automodule:: RS.Utils.Email
	:members: Send
