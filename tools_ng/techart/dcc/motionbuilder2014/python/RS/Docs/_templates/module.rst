{moduleName} --- :mod:`{moduleFullName}`
{titleLine}

{moduleDocString}

Content
=========
.. automodule:: {moduleFullName}
   :members:
   :private-members:
   :special-members:
   :undoc-members:


