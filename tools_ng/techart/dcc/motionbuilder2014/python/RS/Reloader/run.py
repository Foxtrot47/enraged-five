import sys

from PySide import QtGui

from RS.Tools.Reloader import reloader


def Run(show=True):
    # Reload all R* Modules
    reloader.RsReloader.ReloadRsModules()
    QtGui.QMessageBox.information(None, "R* Reloader", "All R* Modules have been reloaded. See log for more details")
