"""
Code for reading the relevant xmls created by tools and techart for getting path information about to the project using
standard python libs.

For reference, the xmls being read by this module are:

<project>/tools/release/local.xml
<project>/tools/release/etc/local.xml
<project>/tools/release/etc/version.xml
<project>/tools/release/etc/project.xml
<project>/tools/release/etc/globals/studio.meta
<project>/tools/release/techart/motionbuilder/python/setup.config

"""
import os
import re

from xml.etree import cElementTree as xml

CORE_PROJECT = os.environ["RS_PROJECT"]
TOOLS_LIB = os.environ["RS_TOOLSROOT"]

_tree = xml.parse(os.path.join(TOOLS_LIB, "etc", "version.xml"))
_root = _tree.getroot()
TOOLS_VERSION = _root.attrib.get("baseLabel", "")


def getValue(item, attributes):
    """
    When a value is retrieved from the instance, it resolves any $ tags to the appropriate value.

    Arguments:
        item (string): attribute key
        attributes (dictionary): dictionary of paths with $ tags to resolve
    """

    value = attributes.get(item, None)
    if item not in attributes or value is None:
        return value

    pattern = re.compile('\$\((\w+)\)')
    matches = pattern.findall(value)

    for attribute_name in matches:

        if attributes.get(attribute_name):
            section = getValue(attribute_name, attributes)
            value = value.replace("$({0})".format(attribute_name), section).replace(os.altsep, os.sep)
            attributes[item] = value

    return value


class Branch(object):
    """Represents a branch within a project."""
    def __init__(self, element):
        """
        Constructor

        Arguments:
            element (xml.etree.ElementTree): branch element from the project xml

        """
        _Name = element.attrib["name"]
        self._Name = _Name
        self._IsDefault = False

        class Path:
            """Nested singleton which contains all paths related to the branch."""
            def __init__(self):
                attributes = element.attrib
                attributes["root"] = os.environ["RS_PROJROOT"]
                attributes["branch"] = _Name
                self._Common = getValue("common", attributes)
                self._Build = getValue("build", attributes)
                self._Audio = getValue("audio", attributes)
                self._Code = getValue("code", attributes)

            @property
            def Common(self):
                """Common root path."""
                return self._Common
            
            @property
            def Build(self):
                """Build root path."""
                return self._Build
            
            @property
            def Audio(self):
                """Audio root path."""
                return self._Audio
            
            @property
            def Code(self):
                """Code root path."""
                return self._Code

        self._Path = Path()
    
    @property
    def Name(self):
        """Name of the branch."""
        return self._Name
    
    @property
    def IsDefault(self):
        """Indicates i-f the branch is the default branch."""
        return self._IsDefault
    
    @property
    def Path(self):
        """Paths related to the branch"""
        return self._Path


class ProjectPath(object):
    """Nested singleton which contains all root level paths for the project."""

    def __init__(self, _project):
        """
        Constructor

        Arguments:
            _project(xml.etree.ElementTree.Element): root element of the project xml
        """
        _branch = os.path.join(TOOLS_LIB, 'etc', 'local.xml')
        _tree = xml.parse(_branch)
        _root = _tree.getroot()
        defaultBranch = _root.find("branches").attrib.get("default", "dev")

        element = _project.find("branches/branch[@name='{}']".format(defaultBranch))
        attributes = element.attrib
        attributes["root"] = os.environ["RS_PROJROOT"]
        attributes["branch"] = defaultBranch

        self._Cache = getValue("cache", attributes)
        self._Root = getValue("root", attributes)
        self._Art = getValue("art", attributes)
        self._Anim = getValue("anim", attributes)
        self._Assets = getValue("assets", attributes)
        self._Processed = getValue("processed", attributes)
        self._Export = getValue("export", attributes)
        self._Metadata = getValue("metadata", attributes)


class Project(object):
    """Provides access to project specific settings and configuration."""
    def __init__(self, _project, _isCore=False):
        """
        Constructor

        Arguments:
            _project(xml.etree.ElementTree.Element): root element of the project xml
            _isCore(boolean): is this the core project
        """
        root = _project

        _branch = os.path.join(TOOLS_LIB, 'etc', 'local.xml')
        _tree = xml.parse(_branch)
        _root = _tree.getroot()
        defaultBranch = _root.find("branches").attrib.get("default", "dev")

        self._Name = root.attrib["name"].upper()
        self._FriendlyName = self._Name
        self._isCore = _isCore

        self._DefaultBranch = None
        self._Branches = []

        for element in root.findall("branches/branch"):
            branch = Branch(element)
            self._Branches.append(branch)
            if branch.Name == defaultBranch:
                self._DefaultBranch = branch
                self._DefaultBranch._IsDefault = True


class User(object):
    def __init__(self):
        """
        Constructor
        """
        # Read the local.xml to get user data
        _local = os.path.join(TOOLS_LIB, 'local.xml')
        _tree = xml.parse(_local)
        _localroot = _tree.getroot()

        _user = _localroot.find("User")
        self._User = _user.find("Username").text.strip()
        self._Email = _user.find("EmailAddress").text.strip()
        _userType = int(_user.find("UserType").text.strip())
        _p4Server = _localroot.find("VersionControlSettings/Server").text

        # Read studio.meta to get studio data
        _studio = os.path.join(TOOLS_LIB, 'etc', 'globals', 'studios.meta')
        _tree = xml.parse(_studio)
        _studioroot = _tree.getroot()

        self._Domain = None
        self._Studio = None
        self._ExchangeServer = None

        for element in _studioroot.findall("Studios/Item[@type='Studio']"):
            _studioP4Server = element.find("PerforceServer").text.strip()
            if _p4Server == _studioP4Server:
                self._Domain = element.find("Domain").text.strip()
                self._Studio = element.find("FriendlyName").text.strip()
                self._ExchangeServer = element.find("ExchangeServer").text.strip()
                break

        self._IsExternal = True
        if self._Studio is not None and 'R*' in self._Studio:
            self._IsExternal = False

        # Read config xml to get usertypes
        _config = os.path.join(TOOLS_LIB, 'config.xml')
        _tree = xml.parse(_config)
        _configroot = _tree.getroot()

        self._Type = None
        self._Types = []

        # Determine user type.
        for element in _configroot.find("usertypes"):
            self._Types.append(element.attrib.get("name", "none"))
            # convert hexadecimal to decimal to compare values
            if int(element.attrib.get("flags", "0x0000"), 16) == _userType:
                self._Type = self._Types[-1]

        self._Types.sort()


class ToolsPath(object):
    """ Nested singleton which contains relevant paths for tools. """
    def __init__(self):
        self._Root = TOOLS_LIB
        self._Drive = os.path.splitdrive(TOOLS_LIB)
        
        self._Library = os.path.join(self._Root, "lib")
        self._Bin = os.path.join(self._Root, "bin")
        self._Logs = os.path.join(self._Root, "logs")
        self._Config = os.path.join(self._Root, "etc")
        self._Temp = os.path.join(self._Root, "temp")
        self._TechArt = os.path.join(self._Root, "techart")


def ConfigData(configPath):
    """
    Returns the data from the setup.config file

    Return:
        list(string, string, string, string, list(string, etc.))
    """
    tree = xml.parse(configPath)
    root = tree.getroot()

    _developmentMode = bool(root.find("DevelopmentMode").attrib.get("value", False))
    _targetBuild = root.find("TargetBuild").text.strip()
    _targetBuildVersion = root.find("TargetBuildVersion").text.strip()
    _targetBuildTitle = root.find("TargetBuildTitle").text.strip()
    _dependencyPathList = [element.text.format(pythonRoot=os.path.dirname(configPath),
                                               projRoot=os.environ["RS_PROJROOT"])
                           for element in root.find("DependencyPathList")]

    return _developmentMode, _targetBuild, _targetBuildVersion, _targetBuildTitle, _dependencyPathList


def DLCProjects(project):
    """
    DLC for the current project
    """
    return {}


def CreateProject(project):
    """
    Creates a project based on the Project class passed in

    Arguments:
        project (class): project class to instantiate

    return project()
    """
    tree = xml.parse(os.path.join(TOOLS_LIB, "etc", "project.xml"))
    return project(tree.getroot(), True)