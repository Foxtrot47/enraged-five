"""
Logic for getting project path information in Linux
"""
import os
import getpass

CORE_PROJECT = "Unknown"
TOOLS_LIB = os.environ.get("GIANT_DATA_DIR", "")

TOOLS_VERSION = "Unknown"


class Branch(object):
    """Represents a branch within a project."""
    def __init__(self, element):
        """
        Constructor

        Arguments:
            element (xml.etree.ElementTree): branch element from the project xml

        """
        self._Name = ""
        self._IsDefault = False

        class Path:
            """Nested singleton which contains all paths related to the branch."""
            def __init__(self):
                self._Common = ""
                self._Build = ""
                self._Audio = ""
                self._Code = ""

            @property
            def Common(self):
                """Common root path."""
                return self._Common
            
            @property
            def Build(self):
                """Build root path."""
                return self._Build
            
            @property
            def Audio(self):
                """Audio root path."""
                return self._Audio
            
            @property
            def Code(self):
                """Code root path."""
                return self._Code

        self._Path = Path()
    
    @property
    def Name(self):
        """Name of the branch."""
        return self._Name
    
    @property
    def IsDefault(self):
        """Indicates i-f the branch is the default branch."""
        return self._IsDefault
    
    @property
    def Path(self):
        """Paths related to the branch"""
        return self._Path


class ProjectPath(object):
    """Nested singleton which contains all root level paths for the project."""

    def __init__(self, _project):
        """
        Constructor

        Arguments:
            _project(xml.etree.ElementTree.Element): root element of the project xml
        """

        self._Cache = ""
        self._Root = ""
        self._Art = ""
        self._Anim = ""
        self._Assets = ""
        self._Processed = ""
        self._Export = ""
        self._Metadata = ""


class Project(object):
    """Provides access to project specific settings and configuration."""
    def __init__(self, _project, _isCore=False):
        """
        Constructor

        Arguments:
            _project(object): not used
            _isCore(boolean): is this the core project
        """

        self._Name = ""
        self._FriendlyName = ""
        self._isCore = _isCore

        self._DefaultBranch = None
        self._Branches = []


class User(object):
    def __init__(self):
        """
        Constructor
        """
        self._User = getpass.getuser()
        self._Email = ""

        # Read studio.meta to get studio data

        self._Domain = None
        self._Studio = None
        self._ExchangeServer = None


        self._IsExternal = True
        if self._Studio is not None and 'R*' in self._Studio:
            self._IsExternal = False

        # Read config xml to get usertypes

        self._Type = None
        self._Types = []


class ToolsPath(object):
    """ Nested singleton which contains relevant paths for tools. """
    def __init__(self):
        self._Root = ""
        self._Drive = ""
        
        self._Library = ""
        self._Bin = ""
        self._Logs = ""
        self._Config = ""
        self._Temp = ""
        self._TechArt = ""


def ConfigData(configPath):
    """
    Returns the data from the setup.config file

    Return:
        list(string, string, string, string, list(string, etc.))
    """
    _developmentMode = False
    _targetBuild = ""
    _targetBuildVersion = ""
    _targetBuildTitle = ""
    _dependencyPathList = []

    return _developmentMode, _targetBuild, _targetBuildVersion, _targetBuildTitle, _dependencyPathList


def DLCProjects(project):
    """
    DLC for the current project
    """
    return {}


def CreateProject(project):
    """
    Creates a project based on the Project class passed in

    Arguments:
        project (class): project class to instantiate

    return project()
    """
    return project(None, True)
