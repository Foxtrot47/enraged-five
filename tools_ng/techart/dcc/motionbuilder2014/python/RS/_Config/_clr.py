"""
Code for reading the relevant xmls created by tools and techart for getting path information about the project using
the toosl lib.
"""
import os
import clr
import inspect
import socket
import ipaddress

clr.AddReference("RSG.Base")
clr.AddReference("RSG.Configuration")

from RSG.Base import Logging
from RSG import Configuration

toolsconfig = Configuration.ToolsConfig()

_clrConfig = Configuration.ConfigFactory.CreateConfig(Logging.LogFactory.ApplicationLog)

def FixPath(path):
    return path.replace("/", "\\")

class Branch(object):
    """Represents a branch within a project."""
    def __init__(self, clrBranch ):

        self.Name = clrBranch.Name
        self.IsDefault = False

        class Path:
            def __init__(self):
                self.Common = FixPath(str(clrBranch.Common))
                self.Build = FixPath(str(clrBranch.Build))
                self.Audio = FixPath(str(clrBranch.Audio))
                self.Code = FixPath(str(clrBranch.Code))
                self.Art = FixPath(str(clrBranch.Art))
                self.Anim = FixPath(str(clrBranch.Anim))
                self.Assets = FixPath(str(clrBranch.Assets))
                self.Processed = FixPath(str(clrBranch.Processed))
                self.Export = FixPath(str(clrBranch.Export))
                self.Metadata = FixPath(str(clrBranch.Metadata))

        self.Path = Path()


class Project(object):
    """Provides access to project specific settings and configuration."""
    def __init__(self, clrProject, isCore=False):

        self.Name = clrProject.Name
        self.FriendlyName = clrProject.FriendlyName
        self.IsCore = isCore

        defaultBranch = None
        self.Branches = list()

        for clrBranch in clrProject.Branches:
            currentBranch = Branch( clrBranch.Value )
            self.Branches.append(currentBranch)
            if currentBranch.Name == clrProject.DefaultBranchName:
                defaultBranch = currentBranch
                defaultBranch.IsDefault = True

        if defaultBranch is None:
            raise Exception( "Missing default branch" )
        self.DefaultBranch = defaultBranch

        class Path:
            def __init__(self):
                self.Cache = FixPath(str(clrProject.CacheDirectory))
                self.Root = FixPath(str(clrProject.RootDirectory))
                self.Art = FixPath(defaultBranch.Path.Art)
                self.Anim = FixPath(defaultBranch.Path.Anim)
                self.Assets = FixPath(defaultBranch.Path.Assets)
                self.Processed = FixPath(defaultBranch.Path.Processed)
                self.Export = FixPath(defaultBranch.Path.Export)
                self.Metadata = FixPath(defaultBranch.Path.Metadata)

        self.Path = Path()


class User(object):
    def __init__(self, clrConfig):
        self.Name = str(clrConfig.Username)

        # IConfig has changed so altered script logic to take this into account without
        try:
            self.Domain = str(clrConfig.Domain)
        except:
            if clrConfig.CurrentStudio:
                self.Domain = str(clrConfig.CurrentStudio.Domain)
            else:
                self.Domain = None

        if clrConfig.CurrentStudio is not None:
            self.Studio = str(clrConfig.CurrentStudio.FriendlyName)
            self.P4ServerDefault = str(clrConfig.CurrentStudio.DefaultPerforceServer)
            self.P4ServerMocap = str(clrConfig.CurrentStudio.PerforceMocapServer)
            self.P4ServerSwarm = str(clrConfig.CurrentStudio.PerforceSwarmServer)
            self.ExchangeServer = str(clrConfig.CurrentStudio.ExchangeServer)
        else:
            self.Studio = None
            self.P4ServerDefault = None
            self.P4ServerMocap = None
            self.P4ServerSwarm = None
            self.ExchangeServer = None

        self.IpAddress = ipaddress.ip_address(unicode(socket.gethostbyname(socket.gethostname())))
        if clrConfig.EmailAddress:
            self.Email = clrConfig.EmailAddress.lower()
        else:
            self.Email = None

        self.IsExternal = True
        if self.Studio is not None and 'R*' in self.Studio:
            self.IsExternal = False
        self.IsInternal = not self.IsExternal

        if clrConfig.UserType is not None:
            self.Type = str(clrConfig.UserType.DisplayName)
        else:
            self.Type = "Unknown"

        self.ToolsConfig = toolsconfig


class Tool(object):
    """ Nested singleton which contains relevant paths for tools. """

    def __init__(self, clrConfig):

        class Path:

            def __init__(self):
                self.Bin = FixPath(str(clrConfig.ToolsBinDirectory))
                self.Logs = FixPath(os.path.join(str(clrConfig.ToolsLogsDirectory), 'Motionbuilder'))
                self.Root = FixPath(str(clrConfig.ToolsRootDirectory))
                self.Config = FixPath(str(clrConfig.ToolsConfigDirectory))
                self.Temp = FixPath(str(clrConfig.ToolsTempDirectory))
                self.TechArt = FixPath(os.path.join(self.Root, "techart"))
                currentFilePath = FixPath(str(os.path.abspath(inspect.getfile(inspect.currentframe()))).lower())
                if r'x:\wildwest' in currentFilePath:
                    self.TechArt = r'x:\wildwest'
                targetBuild = os.getenv('MOTIONBUILDER_MAJOR_VERSION', '2016')
                self.MobuRoot = os.path.join(self.Root,
                                             "dcc",
                                             "current",
                                             "motionbuilder{0}".format(targetBuild))
                self.Assemblies = os.path.join(self.MobuRoot, "assemblies", "x64")
                self.Plugins = os.path.join(self.MobuRoot, "plugins", "x64")

        self.Path = Path()

DLCProjects = dict()
for _dlcProject in _clrConfig.Project.DLCProjects:
    try:
        dlcProject = Project(_dlcProject.Value)
        DLCProjects[_dlcProject.Name] = dlcProject
    except:
        pass

Project = Project(_clrConfig.Project, True)
User = User(_clrConfig)
Tool = Tool(_clrConfig)
