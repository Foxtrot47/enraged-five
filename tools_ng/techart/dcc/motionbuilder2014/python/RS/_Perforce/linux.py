"""
Description:
    Helper functions for interfacing with Perforce through the P4 module provided by Perforce.
    This module mimics our Perforce module in Linux.

"""
import os
import re
import types
import marshal
import datetime
import platform
import subprocess

import P4

from PySide import QtGui


PERFORCE = P4.P4()
PERFORCE.cwd = os.environ.get("RS_PROJROOT")

# Sets max lock time to 10 seconds
PERFORCE.maxlocktime = 10000

# Sets P4 to ignore warnings and only raise exceptions
PERFORCE.exception_level = 1

FILE_ACTIONS = ('unknown', 'add', 'edit', 'delete', 'branch', 'move/add', 'move/delete', 'integrate', 'import', 'purge',
                'archive')


def Connection(func):
    """
    Decorator for establishing and disconnecting a connection to P4

    Arguments:
        func (function): function to decorate

    Return:
        function
    """
    def wrapper(*args, **kwargs):
        """
        Captures the arguments from the decorated function

        Arguments:
            *args (list): the positional arguments being passed to the decorated function
            **kwargs (dictionary): the positional arguments being passed to the decorated function

        Return:
            object
        """
        if PERFORCE.connected():
            PERFORCE.disconnect()

        with PERFORCE.connect():
            return func(*args, **kwargs)

    return wrapper


def User():
    """
    Gets the name of the user using perforce

    Return:
        string
    """
    return PERFORCE.user


def Client():
    """
    Gets the name of the client using perforce

    Return:
        string
    """
    return PERFORCE.client


def Port():
    """
    Gets the port used by perforce

    Return:
        string
    """
    return PERFORCE.port


def Host():
    """
    Gets the host for perforce

    Return:
        string
    """
    return PERFORCE.Host


def Connected():
    """ Connects to perforce """
    return PERFORCE.IsValidConnection(True, True)


def CreateConfig(directory, client=None, port=None, user=None):
    """
    Creates a .p4config file in the supplied directory.

    Arguments:

        directory (string): The directory to create the .p4config file.

    Keyword Arguments:

        client (string): The client workspace to set.
        port (string): The server and port to use.  Format would be: 'rsgsanp4p01:1999'
        user (string): The username to use.
    """
    if not os.path.isdir(directory):
        os.makedirs(directory)
    try:
        with open(os.path.join(directory, ".p4config"), "w") as config:
            config.write('P4CLIENT={0}\n'.format(client or Client()))
            config.write('P4PORT={0}\n'.format(port or Port()))
            config.write('P4USER={0}\n'.format(user or User()))
    except IOError, error:
        print 'Could not create the .p4config file due to the following error!\n', error


class FileState(object):
    """
    Wraps around the P4 fstat commands and returns its objects in a more accessible manner

    For Reference:
        http://www.perforce.com/perforce/doc.current/manuals/cmdref/fstat.html
    """

    def __init__(self, filepath):
        """
        Constructor

        Arguments:
            filepath (string): path to the file to get perforce information from
        """
        data = Run("fstat", [filepath])
        print data
        if data:
            fileState = data[0]
            self.ClientFilename = fileState.get("clientFile", "")
            self.DepotFilename = fileState.get("depotFile", "")
            self.IsMapped = "isMapped" in fileState
            self.IsShelved = "shelved" in fileState
            self.IsResolved = "resolved" in fileState
            self.IsUnResolved = "unresolved" in fileState

            headActionString = fileState.get("headAction", "unknown")
            self.HeadAction = FILE_ACTIONS.index(headActionString)
            self.HeadChange = fileState.get("headChange", "")
            self.HeadRevision = fileState.get("headRev", 0)
            self.HeadChangelistTime = fileState.get("headTime", "")
            self.HeadModifiedTime = fileState.get("headModTime", "")
            self.HeadFileType = fileState.get("headType", "")

            self.HaveRevision = fileState.get("haveRev", 0)
            self.OpenAction = FILE_ACTIONS.index(fileState.get("action", "unknown"))
            self.OpenFileType = fileState.get("type", "")
            self.Changelist = fileState.get("change", 0)
            self.OtherOpen = "otherOpen" in fileState
            self.OtherLocked = "otherLocked" in fileState or ("+l" in headActionString and self.OtherOpen)
            self.OtherOpenUsers = fileState.get("otherLock", []) or fileState.get("otherOpen", [])

            self.Locked = "ourLock" in fileState


def _AssertFileState(fileStateObj):
    """
    Asserts if the supplied object isn't of type RS.PyP4.FileState

    Arguments:

        fileStateObj (RS.Perforce.FileState): The RS.Perroce.FileState object to check.
    """
    assert isinstance(fileStateObj, FileState), ('Incorrect type: '
                                                 'Must supply a RS.Perroce.FileState object!')


def IsLatest(fileStateObj):
    """
    Checks to see if the supplied RS.PyP4.FileState object is the latest revision.

    Arguments:

        fileStateObj (RS.PyP4.FileState): The RS.PyP4.FileState object to check.

    Returns:

        True or False
    """
    _AssertFileState(fileStateObj)
    return fileStateObj.HaveRevision == fileStateObj.HeadRevision


def IsOpenForEdit(fileStateObj):
    """
    Checks whether or not the supplied file is opened for edit by the current user.

    Arguments:

        fileStateObj (RS.Perroce.FileState): The RS.Perroce.FileState object to check.

    Returns:

        True or False
    """
    _AssertFileState(fileStateObj)
    return FILE_ACTIONS[fileStateObj.OpenAction] == 'edit'


def CanOpenForEdit(fileStateObj):
    """
    Tests whether or not a file can be opened for edit by the current user.

    Arguments:

        fileStateObj (RS.Perroce.FileState): The RS.Perroce.FileState object to check.

    Returns:

        True or False
    """
    _AssertFileState(fileStateObj)
    return fileStateObj.Locked


def Exists(path):
    """
    Does the given path in P4 exist
    
    Arguments: 
        path (string): path to the file to check  

    """
    results = Run('fstat', [path])
    for result in results:
        if result.get("headAction", None) == "delete":
            return False
    return bool(len(results))


def GetFileState(filenames):
    """
    Runs fstat on the supplied filenames.

    Arguments:

        filenames (list[string, ect.]): The file names to test.

    Returns:

        A list of RS.PyP4.FileState objects.
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    # Fixing file paths
    filenames = FixFilePaths(filenames)

    fileStates = [FileState(filename) for filename in filenames]

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def GetFileInfo(localFilePath, revision, key):
    """
    Gets information about the file from Perforce based on the revision number and key

    Arguments:

        localFilePath (string): local perforce file path to test
        revision (int): revision you want to check
        key (string): the type of information you wish to return; acceptable values are
                      time, path, status, desc, client, change, changeType, and user.

    Returns:

        key result - could be int/string etc.
    """
    filePathState = GetFileState(localFilePath)

    if filePathState:
        recordSet = Run('changes', '{0}#{1}'.format(filePathState.DepotFilename, revision))
        if recordSet and key in recordSet[0]:
            return recordSet[0][key]


def GetAllMarkedDeleteDepotFiles(startingDirectory):
    """
    Gets ALL the files marked for delete within the supplied directory.

    Arguments:

        startingDirectory (string): directory to inspect
        lResourceDirectory = "//depot/gta5/art/animation/resources/...fbx"

    Returns:
        list[string, etc.];  A list of depot file names.
    """
    records = Run("opened", ["-a", startingDirectory])

    fileList = [record[key]
                for record in records
                for key in records
                if key == "depotFile" and record['action'] == 'delete']

    return fileList


def SwitchWorkspace(cwd):
    """
    Switches the current p4 connection to a new workspace.

    Arguments:
        cwd (string): Path to the .p4config file containing the client, workspace and server information.
    """
    global PERFORCE

    PERFORCE = P4.P4()
    PERFORCE.cwd = cwd


def getHostClients(server):
    """
    This will return the p4 hosts client name. There is a possibility a machine can have on or more clients so will
    return a list

     Return:
        List of Strings
    """

    # Using the global G option so we get back a python dictionary which is easier to work with
    cmd = 'p4 -G -p {0} clients -E *{1}*'.format(server, platform.node())
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    # because we may have multiple results we need to append them to a list
    clients = []
    try:
        while 1:
            output = marshal.load(proc.stdout)
            client = output.get('client', None)
            if client is not None:
                clients.append(client)
    except EOFError:
        pass
    finally:
        proc.stdout.close()

    return clients


def FixFilePath(filename):
    """
    Removes characters not recognized by P4 with their escapes.

    Arguments:
        filename (string): path to file

    Return:
        string; path to file with unrecognized characters by perforce removed.
    """
    for character, escape, regex in (("%", "%25", "%25|%23|%40"), ("#", "%23", "%23"), ("@", "%40", "%40")):
        if not re.search(regex, filename) and character in filename:
            filename = filename.replace(character, escape)

    # Not sure if this is needed
    return r"{}".format(filename)


def FixFilePaths(filenames):
    """
    Some file paths contain @, we need to convert it %
    """
    if not isinstance(filenames, (list, tuple)):
        filenames = [filenames]

    return [FixFilePath(filename) for filename in filenames]


def ValidatePaths(filenames, fixPaths=True):
    """
    Validates that the filenames are written in perforce safe way

    Arguments:
        filenames (list): list of files to validate. If a list/tuple isn't passed then the passed in object is put into
                          a list.
        fixPaths (boolean): removes invalid perforce characters when set to true

    Return:
        list
    """
    if type(filenames) not in (types.TupleType, types.ListType):
        filenames = [filenames]

    if fixPaths:
        return FixFilePaths(filenames)
    return [filename for filename in filenames]


@Connection
def Run(command, args=None):
    """
    Runs a P4 command

    Arguments:
        command (string): perforce command to run
        args (list): list of arguments to use

    Return:
        list
    """
    try:
        return PERFORCE.run(command, args)

    except P4.P4Exception, error:
        QtGui.QMessageBox.critical(None, "P4 Error", "P4 the following errors:\n{}".format(error.errors[0]))
        return []


def Add(filenames, changelistNum=None, force=False, fileType=None):
    """
    Adds the supplied filenames to Perforce.

    Arguments:

        filenames (string): The filenames to add.

    Keyword Arguments:

        changelistNum (int):
            Add files to the specified changelist.
        force (boolean):
            Indicates that the file should be force added
        fileType (string):
            The file type that we want to add the file as. Should be string that matches
            one of the following list of fle types;
            http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

    Returns:

        A single or list of RS.PyP4.FileState objects that were added.
    """

    # IMPORTANT: p4 add should NOT escape symbols in the file paths (@, etc).
    # The P4 Add command accepts characters normally ignored by the other P4 commands.
    # Replacing the characters with their escapes would add a file to perforce with the escapes in
    # the filename instead of the intended character.

    args = ValidatePaths(filenames, fixPaths=False)

    if fileType:
        args[0:0] = ['-t', fileType]

    if force:
        args[0:0] = ['-f']

    if changelistNum:
        args[0:0] = ['-c', str(changelistNum)]

    recordSet = Run('add', args)
    fileStates = [FileState(record['depotFile']) for record in recordSet]

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def Edit(filenames, changelistNum=None, fileType=None):
    """
    Opens for edit the supplied filenames.

    Arguments:

        filenames (list[string, etc.]): The filenames to open for edit.

    Keyword Arguments:

        changelistNum (int):
            Open for edit the files under this changelist.
        fileType:
            The file type that we want to add the file as. Should be string that matches
            one of the following list of fle types;
            http://www.perforce.com/perforce/r15.1/manuals/cmdref/file.types.html

    Returns:

        A single or list of RS.PyP4.FileState objects that were opened for edit.
    """

    args = ValidatePaths(filenames)

    # Add file type flag to the front of the args list
    if fileType:
        args[0:0] = ['-t', fileType]

    # Add changelist flag to the front of the args list
    if changelistNum:
        args[0:0] = ['-c', str(changelistNum)]

    recordSet = Run('edit', args)
    fileStates = [FileState(record['depotFile']) for record in recordSet]

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def Sync(filenames, force=False, revision=None):
    """
    Syncs the supplied filename(s).

    Arguments:

        filenames (list[string, etc.]): The filename(s) to sync.

    Keyword Arguments:

        force (boolean): Whether or not to force the sync.

    Returns:

        A single or list of RS.PyP4.FileState objects that were synced.
    """

    args = ValidatePaths(filenames)
    
    if revision:
        args = ['{}#{}'.format(filename, revision) for filename in args]
        
    if force:
        args[0:0] = ['-f']

    recordSet = Run('sync', args)
    fileStates = [FileState(record['depotFile']) for record in recordSet]

    if len(fileStates) == 1:
        return fileStates[0]

    return fileStates


def Submit(changelistNum):
    """
    Submit a changelist.

    Arguments:

        changelistNum (int): The changelist number to submit.

    Returns:

        P4API.P4RecordSet object.
    """
    return Run('submit', ['-c', str(changelistNum)])


def Revert(filenames):
    """
    Revert a list of files.

    Arguments:

        filenames (list[string, etc.]): The filenames to revert.

    Returns:

        P4API.P4RecordSet object.
    """

    return Run('revert', ValidatePaths(filenames))


def Shelve(filenames, changelistNum=None):
    """
    Shelve a list of files.

    Arguments:

        filenames (list[string, etc.]): The filenames to shelve.

    Keyword Arguments:

        changelistNum (int): The changelist number.

    Returns:

        P4API.P4RecordSet object.
    """

    args = ValidatePaths(filenames)

    if changelistNum:
        args[0:0] = ['-c', str(changelistNum), '-f']

    return Run('shelve', args)


def DeleteShelvedFiles(changelistNum):
    """
    Delete all shelved files in a changelist.

    Arguments:

        changelistNum (int): The changelist number to delete.

    Returns:

        P4API.P4RecordSet object.
    """
    return Run('shelve', ['-c', str(changelistNum), '-d'])


def ShelveChangelist(changelistNum):
    """
    Shelves the given changelist

    Arguments:
        changelistNum (int): changelist number

    Return:
        list
    """
    return Shelve([], changelistNum=changelistNum)


def DeleteChangelist(changelistNum, deleteShelvedFiles=True):
    """
    Delete a changelist.

    Arguments:

        changelistNum(int): The changelist number to delete.

    Keyword Arguments:

        deleteShelvedFiles(boolean): Delete shelved files in the changelist.

    Returns:

        P4API.P4RecordSet object.
    """

    if deleteShelvedFiles:
        DeleteShelvedFiles(changelistNum)

    return Run('change', ['-d', str(changelistNum)])


def RevertChangelist(changelistNum, deleteShelvedFiles=False):
    """
    Revert all files in a changelist.

    Arguments:

        changelistNum (int): The changelist number to delete.

    Keyword Arguments:

        deleteShelvedFiles (boolean): Delete any shelved files in the changelist.

    Returns:
        list
    """
    if deleteShelvedFiles:
        DeleteShelvedFiles(changelistNum)

    return Run('revert', ['-c', str(changelistNum), '//...'])


def GetDescription(changelistNumber):
    """
    Gets the description of the given changelist number

    Arguments:
        changelistNumber (int): the changelist number to get the description of

    """
    return Run("describe", [str(changelistNumber)])


def GetUserData(user):
    """
    Gets information on the user based on the perforce user name. The results are returned in a normal python dictionary

    Arguments:
        user (string): perforce username to get data from

    Return:
        dictionary
    """
    data = Run("users", [user])
    return data[0] if data else {}


def ConvertPerforceSecondsToDate(seconds):
    """
    Converts the seconds returned by perforce to a date and time

    Arguments:
        seconds (int): number of seconds grabbed from the perforce server

    Return:
        string
    """
    return str(datetime.datetime(1970, 1, 1) + datetime.timedelta(0, int(seconds)))


def ConvertRecordSetToDictionary(recordSet):
    """
    Converts a C# P4 Record into a dictionary, this method does nothing in Linux and is just here to catch
    it uses when porting Windows code straight to Linux.

    Arguments:
        recordSet (P4API.RecordSet): record set to convert into python dictionary

    Return:
        dictionary
    """
    return recordSet


# Class used by the Reference Editor

class SyncOperation(object):
    """
    Perforce p4.exe command line
    """

    def __init__(self, fileList, start=False, force=False):
        """
        Constructor

        Arguments:
            fileList (list): list of files
            start (boolean): start sync operation
            force (boolean): force sync even if files are already up-to-date
        """

        self.p4root = os.environ.get("RS_PROJROOT")
        self.fileList = FixFilePaths(fileList)
        self.force = force
        self.proc = None

        if start:
            self.Start()

    def Start(self):
        """
        Sync Operation
        """

        # send a request for a sync to perforce
        fileListString = ""
        for filepath in self.fileList:
            fileListString = '{}"{0}" '.format(fileListString, filepath)

        if self.force:
            fileListString = "-f {}".format(fileListString)

        commandText = 'p4 sync {} '.format(fileListString)

        # Remember the original working directory
        originaDir = os.environ.get("RS_PROJROOT")

        # Change it to the current working one so that we can use the p4 command
        os.chdir(self.p4root)
        try:
            self.proc = subprocess.Popen(commandText, shell=True, stdout=subprocess.PIPE)
        except:
            self.proc = subprocess.Popen(commandText, shell=True, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                         stdout=subprocess.PIPE)
            self.proc.stdin.close()
            self.proc.stderr.close()

        # Change it back to the original directory
        os.chdir(originaDir)

    def WaitForCompletion(self):
        """
        Wait for process to finish
        """

        self.proc.wait()