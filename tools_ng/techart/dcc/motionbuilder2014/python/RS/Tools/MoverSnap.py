from pyfbsdk import *

import RS.Utils.Scene

def moveSnap(pControl, pEvent):

    # get current character, mover and hips
    char = FBApplication().CurrentCharacter
    hips = char.GetModel( FBBodyNodeId.kFBHipsNodeId ) 
    
    if hips:
        # mover will always be the parent of the skel_root ( hips )
        if hips.Parent != None and hips.Parent.Name == 'mover':
            mover = hips.Parent
            if char.ActiveInput == True and char.InputType == ( FBCharacterInputType.kFBCharacterInputMarkerSet ):
                # align mover to hips on x & z and current on y            
                RS.Utils.Scene.Align( mover, hips, pAlignTransX = True, pAlignTransY = False, pAlignTransZ = True, pAlignRotX = False, pAlignRotY = True, pAlignRotZ = False  )
            else:
                FBMessageBox("R* Error", "Ensure your selected character has an active Ctrl Rig", "Ok")                
        else:
            FBMessageBox( "R* Error", "Unable to find the mover. \n\n'mover' should be a parent of 'Skel_Root', but cannot find this.", "Ok")
    else:
        FBMessageBox("R* Error", "Unable to find 'SKEL_ROOT' bone.", "Ok")              