import os

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Config
from RS.Core.AnimData import Context
from RS.Core.AnimData.Widgets import stageContextWidget
from RS.Tools.VirtualProduction import const
from RS.Core.Mocap import MocapCommands
from RS.Utils import Path, UserPreferences
from RS.Core.Camera import Lib
from RS.Core.Face import FacewareLive
from RS.Core.Mocap import AudioRecording


class DuringShootWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(DuringShootWidget, self).__init__(parent=parent)
        self._watchstarTrial = MocapCommands.WatchstarTrial()
        self.setupUi()

    def TakeNoLayering(self):
        self._watchstarTrial.grabTakeName(False)
        Lib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def TakeLayering(self):
        self._watchstarTrial.grabTakeName(True)
        Lib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

    def PrepForPlayback(self):
        # go offline with all devices
        MocapCommands.GiantDeviceOff()
        MocapCommands.NonGiantDevicesOff()
        # plot faceware and timeshift -5 frames
        FacewareClass = FacewareLive.FacewareConstraint()
        FacewareClass.TimeShift(-5)
        # bring in audio file that matches the take name into story mode and turn story mode on for playback
        fileFullPath = mobu.FBApplication().FBXFileName
        baseName = Path.GetBaseNameNoExtension(fileFullPath)
        extension = Path.GetFileExtension(fileFullPath)
        fileName = baseName + extension
        filePathSplit = fileFullPath.split(fileName)
        audioPathPrefix = filePathSplit[0]

        currentTakeName = mobu.FBSystem().CurrentTake.Name
        audioPath = audioPathPrefix + '\\' + currentTakeName + '.wav'

        if os.path.exists(audioPath):
            audioTrack = mobu.FBStoryTrack(mobu.FBStoryTrackType.kFBStoryTrackAudio, None)
            audioTrack.Name = currentTakeName
            audioClip = mobu.FBAudioClip(audioPath)
            mobu.FBStoryClip (audioClip, audioTrack, mobu.FBTime(0, 0, 0, 0))
            story = mobu.FBStory()
            story.Mute = False

        else:
            QtGui.QMessageBox.warning(None,
                                      "",
                                      "Unable to find an audio file that matches the current Take Name\n\n{0}".format(audioPath),
                                      QtGui.QMessageBox.Ok)

    def BackFromPlaybackTakeNoLayering(self):
        # delete the audio story track and delete the audio file from the scene
        MocapCommands.AudioClipAndTrackDelete()

        # create new take without animation
        self._watchstarTrial.grabTakeName(False)
        Lib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

        # go back online with all devices
        MocapCommands.NonGiantDevicesOn()
        MocapCommands.GiantDeviceOn()

    def BackFromPlaybackTakeLayering(self):
        # delete the audio story track and delete the audio file from the scene
        MocapCommands.AudioClipAndTrackDelete()

        # create new take without animation
        self._watchstarTrial.grabTakeName(True)
        Lib.camUpdate.rs_ShowHideCameraPlanes(False, False, False)

        # go back online with all devices
        MocapCommands.GiantDeviceOn()
        MocapCommands.NonGiantDevicesOn()

    def SetAudioRecordingFilePath(self):
        """
        Set the audio recording file path based off what is in watchstar
        """
        try:
            AudioRecording.SetAudioRecordingFilePath()
        except ValueError, expMsg:
            QtGui.QMessageBox.information(
                                          None,
                                          "Virtual Production Toolbox",
                                          str(expMsg)
                                          )
            return

    def _handleGrabTakeButton(self):
        self._currentTake.setText(self._watchstarTrial.grabTakeName(rawName=True) or "<No Active Take>")

    def _handleStageContextChange(self, newContext):
        self._watchstarTrial.stage = newContext
        self._setWatchstarButtonState(True)

    def _handleStageLocationChange(self, newLocation):
        self._setWatchstarButtonState(False)

    def _setWatchstarButtonState(self, val):
        self._grabTakeNameButton.setEnabled(val)
        self._layeringButton.setEnabled(val)
        self._noLayeringButton.setEnabled(val)
        self._setAudioFileLocButton.setEnabled(val)

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        operatorGroupBoxGridLayout = QtGui.QGridLayout()
        playbackGroupBoxGridLayout = QtGui.QGridLayout()
        currentTrialNameLayout = QtGui.QVBoxLayout()

        # create widgets
        operatorGroupBox = QtGui.QGroupBox()
        self._stageWidget = stageContextWidget.StageContextWidget()
        self._currentTake = QtGui.QLineEdit()
        self._grabTakeNameButton = QtGui.QPushButton("Get Current Take Name")

        playbackGroupBox = QtGui.QGroupBox()
        self._layeringButton = QtGui.QPushButton()
        self._noLayeringButton = QtGui.QPushButton()
        playbackButton = QtGui.QPushButton()
        layerPlaybackButton = QtGui.QPushButton()
        noLayeringPlayback = QtGui.QPushButton()
        self._setAudioFileLocButton = QtGui.QPushButton("Set Audio Recording path")
        emptyLabel = QtGui.QLabel()
        operatorSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        playbackSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        centerSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        # widget properties
        operatorGroupBox.setTitle("Watchstar")
        playbackGroupBox.setTitle("Playback")
        self._layeringButton.setText("Create Take with Layering")
        self._noLayeringButton.setText("Create Take without Layering")
        playbackButton.setText("Prepare for Playback")
        layerPlaybackButton.setText("Return from Playback with Layering")
        noLayeringPlayback.setText("Return from Playback without Layering")

        currentTrialNameLayout.setContentsMargins(0, 0, 0, 0)
        self._currentTake.setReadOnly(True)

        # add widgets to groupBox
        currentTrialNameLayout.addWidget(self._stageWidget)
        currentTrialNameLayout.addWidget(QtGui.QLabel("Current Trial:"))
        currentTrialNameLayout.addWidget(self._currentTake)

        operatorGroupBoxGridLayout.addLayout(currentTrialNameLayout, 1, 0)
        operatorGroupBoxGridLayout.addWidget(self._grabTakeNameButton, 2, 0)
        operatorGroupBoxGridLayout.addWidget(self._layeringButton, 5, 0)
        operatorGroupBoxGridLayout.addWidget(self._noLayeringButton, 6, 0)
        operatorGroupBoxGridLayout.addWidget(self._setAudioFileLocButton, 7, 0)
        operatorGroupBoxGridLayout.addItem(operatorSpacerItem, 8, 0)
        playbackGroupBoxGridLayout.addWidget(playbackButton, 0, 2)
        playbackGroupBoxGridLayout.addWidget(layerPlaybackButton, 1, 2)
        playbackGroupBoxGridLayout.addWidget(noLayeringPlayback, 2, 2)
        playbackGroupBoxGridLayout.addItem(playbackSpacerItem, 3, 2)

        # set groupBox layout
        operatorGroupBox.setLayout(operatorGroupBoxGridLayout)
        playbackGroupBox.setLayout(playbackGroupBoxGridLayout)

        # add to layout
        mainLayout.addWidget(operatorGroupBox, 0, 0)
        mainLayout.addWidget(playbackGroupBox, 0, 1)
        mainLayout.addItem(centerSpacerItem, 0, 1)

        # set layout
        self.setLayout(mainLayout)

        # handles
        self._grabTakeNameButton.pressed.connect(self._handleGrabTakeButton)
        self._stageWidget.locationSelected.connect(self._handleStageLocationChange)
        self._stageWidget.stageSelected.connect(self._handleStageContextChange)
        self._layeringButton.pressed.connect(self.TakeLayering)
        self._noLayeringButton.pressed.connect(self.TakeNoLayering)
        playbackButton.pressed.connect(self.PrepForPlayback)
        layerPlaybackButton.pressed.connect(self.BackFromPlaybackTakeLayering)
        noLayeringPlayback.pressed.connect(self.BackFromPlaybackTakeNoLayering)
        self._setAudioFileLocButton.pressed.connect(self.SetAudioRecordingFilePath)
        self._setWatchstarButtonState(False)

        self._stageWidget.setContext(self._watchstarTrial.stage)


