from PySide import QtCore, QtGui

import os
import collections

from RS import Config
from RS.Core.AnimData import Context


class PrevizFolder(object):
    '''
    directs the user's current location to the correct previz folder
    '''
    def __init__(self):
        '''
        Location ID Key:
        Glen Cove, NY :: 1
        Edinburgh, UK :: 2
        New York, NY :: 4
        Vancouver, BC :: 6
        San Diego, CA :: 7
        Toronto, ON :: 8
        Bethpage, NY :: 9
        '''
        # create dicted based off of the location id numbers and the folder names in virtual
        # production folders
        self.studioDict = {1:'nyc',
                           2:'north',
                           4:'nyc',
                           7:'sd',
                           8:'tor',
                           9:'nyc',
                           }
        self._defaultCity = 'nyc'

    def GetCurrentLocationID(self):

        '''
        defaults to 1 (nyc) if the location id cannot be found (e.g. London and Outsource)
        Returns:
            Int - unique number for location
        '''
        locationId = 1
        try:
            locationId = Context.animData.getCurrentLocation().locationID
        except:
            pass
        return locationId

    def GetStudioFolderName(self):
        '''
        Returns:
            String - user's location mocap path dir
        '''
        return self.studioDict.get(self.GetCurrentLocationID())

    def GetGlobalFolderName(self):
        '''
        Returns:
            String - global dir path
        '''
        return os.path.join(Config.VirtualProduction.Path.Previz, 'globalAssets')

    def GetCityLocation(self):
        '''
        Returns:
            String - name of user's city
        '''
        return Context.animData.getCurrentLocation().city()


class Icons(object):
    '''
    class for creating a dictionary of QIcons and Vectors (for color)
    '''
    # create qicons
    Blue = QtGui.QIcon()
    LightBlue = QtGui.QIcon()
    DarkBlue = QtGui.QIcon()
    Black = QtGui.QIcon()
    LightOrange = QtGui.QIcon()
    DarkOrange = QtGui.QIcon()
    Red = QtGui.QIcon()
    LightPurple = QtGui.QIcon()
    DarkPurple = QtGui.QIcon()
    LightYellow = QtGui.QIcon()
    DarkYellow = QtGui.QIcon()
    DarkGrey = QtGui.QIcon()
    LightGreen = QtGui.QIcon()
    Green = QtGui.QIcon()
    DarkGreen = QtGui.QIcon()
    Brown = QtGui.QIcon()
    Beige = QtGui.QIcon()
    Pink = QtGui.QIcon()
    DarkPink = QtGui.QIcon()
    White = QtGui.QIcon()

    Folder = QtGui.QIcon()
    File = QtGui.QIcon()

    # default image path
    mocapImageFolder = "{0}/Mocap/".format(Config.Script.Path.ToolImages)
    refEditorImageFolder = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)

    # add icon image to qicon
    Blue.addPixmap(QtGui.QPixmap("{0}actor_color_blue.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightBlue.addPixmap(QtGui.QPixmap("{0}actor_color_lightblue.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkBlue.addPixmap(QtGui.QPixmap("{0}actor_color_darkblue.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Black.addPixmap(QtGui.QPixmap("{0}actor_color_black.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightOrange.addPixmap(QtGui.QPixmap("{0}actor_color_lightorange.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkOrange.addPixmap(QtGui.QPixmap("{0}actor_color_darkorange.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Red.addPixmap(QtGui.QPixmap("{0}actor_color_red.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightPurple.addPixmap(QtGui.QPixmap("{0}actor_color_lightpurple.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkPurple.addPixmap(QtGui.QPixmap("{0}actor_color_darkpurple.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightYellow.addPixmap(QtGui.QPixmap("{0}actor_color_lightyellow.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkYellow.addPixmap(QtGui.QPixmap("{0}actor_color_darkyellow.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkGrey.addPixmap(QtGui.QPixmap("{0}actor_color_darkgrey.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    LightGreen.addPixmap(QtGui.QPixmap("{0}actor_color_lightgreen.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Green.addPixmap(QtGui.QPixmap("{0}actor_color_green.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkGreen.addPixmap(QtGui.QPixmap("{0}actor_color_darkgreen.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Brown.addPixmap(QtGui.QPixmap("{0}actor_color_brown.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Beige.addPixmap(QtGui.QPixmap("{0}actor_color_beige.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Pink.addPixmap(QtGui.QPixmap("{0}actor_color_pink.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DarkPink.addPixmap(QtGui.QPixmap("{0}actor_color_darkpink.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    White.addPixmap(QtGui.QPixmap("{0}actor_color_white.png".format(mocapImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    Folder.addPixmap(QtGui.QPixmap("{0}Folder.png".format(refEditorImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    File.addPixmap(QtGui.QPixmap("{0}File.png".format(refEditorImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    # add both qicons and color vectors to a dictionary
    # need to divide rgb by 256.0 to get mobu-friendly rgb
    # order dict based on url:bugstar:3043112
    colorDict = collections.OrderedDict()

    colorDict["Blue"] = (Blue, (0.0/256.0, 99.84/256.0, 253.44/256.0))
    colorDict["LightBlue"] = (LightBlue, (97.28/256.0, 184.32/256.0, 186.88/256.0))
    colorDict["DarkBlue"] = (DarkBlue, (23.04/256.0, 25.6/256.0, 112.64/256.0))
    colorDict["Green"] = (Green, (40.96/256.0, 199.68/256.0, 71.68/256.0))
    colorDict["LightGreen"] = (LightGreen, (115.2/256.0, 220.16/256.0, 30.72/256.0))
    colorDict["DarkGreen"] = (DarkGreen, (0/256.0, 64/256.0, 0/256.0))
    colorDict["LightYellow"] = (LightYellow, (255/256.0, 237/256.0, 105/256.0))
    colorDict["DarkYellow"] = (DarkYellow, (232/256.0, 186/256.0194, 0/256.0))
    colorDict["LightPurple"] = (LightPurple, (194/256.0, 153/256.0, 255/256.0))
    colorDict["DarkPurple"] = (DarkPurple, (97.28/256.0, 0/256.0, 153.6/256.0))
    colorDict["LightOrange"] = (LightOrange, (256/256.0, 128/256.0, 0/256.0))
    colorDict["DarkOrange"] = (DarkOrange, (256/256.0, 81.92/256.0, 0/256.0))
    colorDict["Pink"] = (Pink, (255/256.0, 0/256.0, 255/256.0))
    colorDict["DarkPink"] = (DarkPink, (166/256.0, 94/256.0, 105/256.0))
    colorDict["Red"] = (Red, (202.24/256.0, 35.84/256.0, 35.84/256.0))
    colorDict["Beige"] = (Beige, (225.28/256.0, 189.44/256.0, 128/256.0))
    colorDict["Brown"] = (Brown, (112.64/256.0, 48.64/256.0, 5.12/256.0))
    colorDict["DarkGrey"] = (DarkGrey, (82/256.0, 82/256.0, 82/256.0))
    colorDict["Black"] = (Black, (20/256.0, 20/256.0, 20/256.0))
    colorDict["White"] = (White, (256.0/256.0, 256.0/256.0, 256.0/256.0))

class ColorNameChange(object):
    '''
    some of the color names were changed, so we need a dictionary of the before and after names,
    to ensure we are using the latest names.
    url:bugstar:3043112

    Return:
    Dict [string]=string
    '''
    colorNameChangeDict = {'CornFlower':'LightBlue',
                           'NavyBlue':'DarkBlue',
                           'TexasOrange':'DarkOrange',
                           'Orange':'LightOrange',
                           'Purple':'DarkPurple',
                           'Yellow':'DarkYellow',
                           'Grey':'DarkGrey',
                           'MiddleGreen':'DarkGreen',
                           'NeonPink':'Pink',
                           'DustyRose':'DarkPink',
                           'Violet':'LightPurple',
                           }


class StageIcons(object):
    '''
    class for creating a dictionary of QIcons and Vectors (for color)
    '''
    # create qicons
    red = QtGui.QIcon()
    mintGreen = QtGui.QIcon()
    carolinaBlue = QtGui.QIcon()
    magenta = QtGui.QIcon()
    pink = QtGui.QIcon()
    green = QtGui.QIcon()
    purple = QtGui.QIcon()
    white = QtGui.QIcon()
    grey = QtGui.QIcon()
    blue = QtGui.QIcon()

    # default image path
    stageImageFolder = "{0}/Mocap/stages/".format(Config.Script.Path.ToolImages)

    # add icon image to qicon
    red.addPixmap(QtGui.QPixmap("{0}stage_color_red.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    mintGreen.addPixmap(QtGui.QPixmap("{0}stage_color_mintgreen.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    carolinaBlue.addPixmap(QtGui.QPixmap("{0}stage_color_carolinablue.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    magenta.addPixmap(QtGui.QPixmap("{0}stage_color_magenta.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    pink.addPixmap(QtGui.QPixmap("{0}stage_color_pink.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    green.addPixmap(QtGui.QPixmap("{0}stage_color_green.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    purple.addPixmap(QtGui.QPixmap("{0}stage_color_purple.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    white.addPixmap(QtGui.QPixmap("{0}stage_color_white.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    grey.addPixmap(QtGui.QPixmap("{0}stage_color_grey.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    blue.addPixmap(QtGui.QPixmap("{0}stage_color_blue.png".format(stageImageFolder)), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    # add both qicons and fbcolor values to a dictionary
    colorDict = {
            "Red":(red, (1, 0, 0)),
            "MintGreen":(mintGreen, (0.5, 1, 0.5)),
            "CarolinaBlue":(carolinaBlue, (0, 0.75, 1)),
            "Magenta":(magenta, (1, 0, 0.8)),
            "Pink":(pink, (1, 0.5, 0.75)),
            "Green":(green, (0, 0.4, 0)),
            "Purple":(purple, (0.65, 0.15, 1)),
            "White":(white, (1, 1, 0.8)),
            "Grey":(grey, (0.5, 0.5, 0.5)),
            "Blue":(blue, (0, 0.3, 1)),
            }

class StagesList(object):
    '''
    generates a list of stages the location's root directory contains

    Returns:
    list(string)
    '''
    stagesRootDir = os.path.join(Config.VirtualProduction.Path.Previz,
                                 PrevizFolder().GetStudioFolderName(),
                                 'stages'
                                 )

    stagesList = []
    for root, subFolders, files in os.walk(stagesRootDir):
        for fileName in files:
            nameNoExtension = fileName.split('.')[0]
            stagesList.append(nameNoExtension)

class BuildAssetsList(object):
    '''
    generates a list of build assets the location's root directory contains

    Returns:
    list(string)
    '''
    buildAssetsRootDir = os.path.join(Config.VirtualProduction.Path.Previz,
                                      'globalAssets',
                                      'preBuildAssets',
                                      )

    buildAssetsList = []
    for root, subFolders, files in os.walk(buildAssetsRootDir):
        for fileName in files:
            nameNoExtension = fileName.split('.')[0]
            buildAssetsList.append(nameNoExtension)

class GSModelTypes(object):
    '''
    class for defining what type of gs asset an object is
    '''
    Character = 'Character'
    Prop = 'Prop'
    Slate = 'Slate'


class PropModelDataIndex(object):
    selfReturn = QtCore.Qt.UserRole
    folderFileRole = QtCore.Qt.UserRole + 1
