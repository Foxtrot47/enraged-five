from PySide import QtGui, QtCore

import os
import glob

import pyfbsdk as mobu

from RS.Tools.VirtualProduction import const
from RS import Config, Globals, Perforce
from RS.Utils import Scene
from RS.Tools.VirtualProduction.models import propToyModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class PropToyModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropToyModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        # need 2 headers for this widget as the horizontal scrollbar seems to require it :(
        return ['', '']

    def _getPropPaths(self):
        '''
        Get propclone list

        Returns:
            List (Strings): list of path strings
        '''
        pathDict = {}
        # get prop paths
        propPathDirList =[os.path.join(Config.VirtualProduction.Path.Previz,
                                       const.PrevizFolder().GetStudioFolderName(),
                                       'propToys'
                                       ),
                        os.path.join(const.PrevizFolder().GetGlobalFolderName(),
                                     'propClones'
                                     ),
                        os.path.join(Config.VirtualProduction.Path.Previz,
                                      const.PrevizFolder().GetStudioFolderName(),
                                     'props',
                                     'animals',
                                     ),
                        os.path.join(Config.VirtualProduction.Path.Previz,
                                     const.PrevizFolder().GetStudioFolderName(),
                                     'props',
                                     'weapons',
                                     ),
                         ]

        # get a list of paths int he directories
        for propPathDir in propPathDirList:
            propToyFileList = []
            for root, subFolders, files in os.walk(propPathDir):
                for fileName in files:
                    propPath = os.path.join(propPathDir, fileName)
                    # sync path
                    Perforce.Sync(propPath)
                    # make sure path exists
                    if os.path.exists(propPath):
                        # add file paths to a list
                        propToyFileList.append(propPath)

            # apply folder name and type to a dict
            proptypeName = None
            for rootPath in propToyFileList:
                path = propToyFileList[0].split(os.path.sep)
                proptypeName = path[-2]
                pathDict[proptypeName] = propToyFileList
        return pathDict

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        parents = [parent]
        pathDict = self._getPropPaths()
        for key, value in pathDict.iteritems():
            parentNode = propToyModelItem.PropToyModelItem(key,
                                                           folderBool=True,
                                                           parent=parents[-1])
            parents[-1].appendChild(parentNode)
            if parentNode:
                for path in value:
                    pathbaseName = os.path.splitext(os.path.basename(path))[0]
                    childNode = propToyModelItem.PropToyModelItem(pathbaseName,
                                                                  fullPath=path,
                                                                  folderBool=False,
                                                                  parent=parentNode)
                    parentNode.appendChild(childNode)

    def flags(self, index):
        '''
        Flags are added to determine column properties

        Args:
            index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        column = index.column()
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

        return flags