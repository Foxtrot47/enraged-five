import re

import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS import Globals
from RS.Core.Mocap import MocapCommands
from RS.Utils.Scene import Component, Constraint
from RS.Tools.VirtualProduction import const
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.VirtualProduction.models import propToyModelTypes
from RS.Core.ReferenceSystem.Types.MocapPropToy import MocapPropToy


class PropToyWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropToyWidget, self).__init__(parent=parent)
        self.currentStage = None
        self.manager = Manager()
        self.setupUi()

    def setCurrentStage(self, stage):
        self.currentStage = stage

    def populateTreeView(self):
        # reset view
        self.propListTreeView.reset()
        # add model to tree widget
        self._mainModel =  propToyModelTypes.PropToyModel()
        self.propListTreeView.setModel(self._mainModel)

    def populateScenePropToyComboBoxList(self):
        self.availablePropToysComboBox.clear()
        proptoyList = self._getScenePropToys()
        self.availablePropToysComboBox.addItems(proptoyList)

    def populateConstraintTypeComboBoxList(self):
        self.constraintComboBox.clear()
        constraintList = ["Rotation", "Parent"]
        self.constraintComboBox.addItems(constraintList)

    def populateDriverTreeList(self):
        self.driversTree.clear()
        nonPropToyAssetDict = MocapCommands.GetNonPropToyAssets()
        for key, value in nonPropToyAssetDict.iteritems():
            root = QtGui.QTreeWidgetItem(self.driversTree)
            root.setText(0, key.LongName)
            for i in value:
                child = QtGui.QTreeWidgetItem(root)
                child.setText(0, i.LongName)

    def _getScenePropToys(self):
        '''
        Gathers a list of proptoys in the scene. Finds both Ref objects and ones not referenced.
        Reurns:
            list[str]
        '''
        propToyNameList = []
        # get a list of reference proptoy objects
        propToyReferences = self.manager.GetReferenceListByType(MocapPropToy)

        for reference in propToyReferences:
            propToyModelList = [component.LongName for component in Globals.Components
                                if isinstance(component, mobu.FBModel)
                                and reference.Namespace == Component.GetNamespace(component)
                                and re.search('_null|_bone', component.Name) is not None]
            propToyNameList.extend(propToyModelList)

        # get a list of non-reffed objects
        nonReferencedPropToysList = [component.Name for component in Globals.Components
                                    if isinstance(component, mobu.FBModel)
                                    and component.PropertyList.Find('Mocap PropToy') is not None]

        # add non ref proptoys to main list, if not already there
        for nonReference in nonReferencedPropToysList:
            if nonReference not in propToyNameList:
                propToyNameList.append(nonReference)

        return propToyNameList

    def _handleAddSelectedPropToys(self):
        # get list of selected indexes
        selectedIdxs = {idx.row():idx for idx in self.propListTreeView.selectedIndexes() if idx.column() == 0}.values()

        # if index is a file, add to list
        fileList = []
        for index in selectedIdxs:
            # 3 = string: path to the asset, that is in the tree row
            path = index.data(3)
            # is the selected idx a file, rather than a folder?
            isFile = index.data(const.PropModelDataIndex.folderFileRole)
            if isFile:
                fileList.append(str(path))

        # add selected prop(s) to scene and align to active stage
        referenceList = self.manager.CreateReferences(fileList) or []
        for reference in referenceList:
            reference.AlignReferenceRootToActiveStage()

        # clear selection
        self.propListTreeView.clearSelection()

        # re-populate proptoy combobox, with new prop toys int he scene
        self.populateScenePropToyComboBoxList()

    def _handleCreateConstraint(self):
        parent = str(self.availablePropToysComboBox.currentText())
        child = str(self.driversTree.currentItem().text(0))
        childComponent = mobu.FBFindModelByLabelName(child)
        # Force motion builder to evaluate the scene to ensure the matrix data is accurate
        mobu.FBSystem().Scene.Evaluate()
        matrix = mobu.FBMatrix()
        childComponent.GetMatrix(matrix)
        if child and parent:
            constraintType = self.constraintComboBox.currentIndex()
            if constraintType == 1:
                constraint = MocapCommands.CreateConstraint(child, parent, Constraint.PARENT_CHILD)
            elif constraintType == 0:
                constraint = MocapCommands.CreateConstraint(child, parent, Constraint.ROTATION)
        if self.snapCheckbox.checkState() == QtCore.Qt.CheckState.Checked:
            constraint.Snap()

        if self.retainPositionCheckbox.isEnabled() and self.retainPositionCheckbox.isChecked():
            childComponent.SetMatrix(matrix)
            # Force motion builder to evaluate the scene to ensure the matrix data is set
            mobu.FBSystem().Scene.Evaluate()

        # clear selection
        self.driversTree.clearSelection()
        # collapse tree
        self.driversTree.collapseAll()
        # reset snap checkbox
        self.snapCheckbox.setCheckState(QtCore.Qt.CheckState.Unchecked)


    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        proptoyGroupBoxGridLayout = QtGui.QGridLayout()
        constraintGroupBoxGridLayout = QtGui.QGridLayout()

        # create widgets
        self.propListTreeView = QtGui.QTreeView()
        proptoyGroupBox = QtGui.QGroupBox()
        emptyLabel = QtGui.QLabel()
        propToyLabel = QtGui.QLabel()
        addPropToyButton = QtGui.QPushButton()
        constraintGroupBox = QtGui.QGroupBox()
        driversLabel = QtGui.QLabel()
        self.driversTree = QtGui.QTreeWidget()
        availablePropToysLabel = QtGui.QLabel()
        self.availablePropToysComboBox = QtGui.QComboBox()
        constraintLabel = QtGui.QLabel()
        self.constraintComboBox = QtGui.QComboBox()
        self.snapCheckbox = QtGui.QCheckBox()
        self.retainPositionCheckbox = QtGui.QCheckBox()

        createConstraintButton = QtGui.QPushButton()

        # widget properties
        proptoyGroupBox.setTitle("Proptoy Create")
        propToyLabel.setText('Select one, or more, PropToys:')
        addPropToyButton.setText('Add Selected PropToys to the Scene')
        constraintGroupBox.setTitle("Proptoy Constraints")
        driversLabel.setText('Select one Driver:')
        self.propListTreeView.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        self.propListTreeView.header().hide()
        self.propListTreeView.header().setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.propListTreeView.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.propListTreeView.header().setStretchLastSection(False)
        self.driversTree.header().hide()
        self.driversTree.header().setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
        self.driversTree.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.driversTree.header().setStretchLastSection(False)
        self.populateDriverTreeList()
        availablePropToysLabel.setText('Scene PropToys:')
        self.populateScenePropToyComboBoxList()
        constraintLabel.setText('Type of Constraint:')
        self.populateConstraintTypeComboBoxList()
        self.snapCheckbox.setText('Snap (Activate) Constraint')
        self.retainPositionCheckbox.setText('Keep Child Position the Same')
        self.retainPositionCheckbox.setEnabled(False)
        self.snapCheckbox.toggled.connect(lambda checked: self.retainPositionCheckbox.setEnabled(checked))
        createConstraintButton.setText('Create Constraint')

        # add widgets to groupBox
        proptoyGroupBoxGridLayout.addWidget(propToyLabel, 0, 0)
        proptoyGroupBoxGridLayout.addWidget(self.propListTreeView, 1, 0, 7, 1)
        proptoyGroupBoxGridLayout.addWidget(addPropToyButton, 8, 0)
        constraintGroupBoxGridLayout.addWidget(driversLabel, 0, 0)
        constraintGroupBoxGridLayout.addWidget(self.driversTree, 1, 0)
        constraintGroupBoxGridLayout.addWidget(availablePropToysLabel, 2, 0)
        constraintGroupBoxGridLayout.addWidget(self.availablePropToysComboBox, 3, 0)
        constraintGroupBoxGridLayout.addWidget(constraintLabel, 4, 0)
        constraintGroupBoxGridLayout.addWidget(self.constraintComboBox, 5, 0)
        constraintGroupBoxGridLayout.addWidget(emptyLabel, 6, 0)
        constraintGroupBoxGridLayout.addWidget(self.snapCheckbox, 7, 0)
        constraintGroupBoxGridLayout.addWidget(self.retainPositionCheckbox, 7, 1)
        constraintGroupBoxGridLayout.addWidget(createConstraintButton, 8, 0)

        # set groupBox layout
        proptoyGroupBox.setLayout(proptoyGroupBoxGridLayout)
        constraintGroupBox.setLayout(constraintGroupBoxGridLayout)

        # add to layout
        mainLayout.addWidget(proptoyGroupBox, 0, 0)
        mainLayout.addWidget(constraintGroupBox, 0, 1)

        # set layout
        self.setLayout(mainLayout)

        # handles for callbacks
        addPropToyButton.pressed.connect(self._handleAddSelectedPropToys)
        createConstraintButton.pressed.connect(self._handleCreateConstraint)

        # populate proptoy list
        self.populateTreeView()