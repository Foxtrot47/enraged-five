import os

from PySide import QtCore, QtGui

from RS import Globals, Perforce
from RS.Core.AnimData import Context
from RS.Core.AnimData.Widgets import shotContextWidget, contextWidget, sessionContextWidget
from RS.Core.ReferenceSystem import Manager
from RS.Tools.VirtualProduction.models import watchstarModel


class WatchstarWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        self._manager = Manager.Manager()
        super(WatchstarWidget, self).__init__(parent=parent)
        self.currentStudio = None
        self.setupUi()

    def setupUi(self):
        # Layouts
        mainLayout = QtGui.QVBoxLayout()
        buttonLayout = QtGui.QHBoxLayout()

        # Widgets
        contextTab = QtGui.QTabWidget()
        self._shotsContextWidget = shotContextWidget.ShotContextComboxBoxView()
        self._trialContextWidget = contextWidget.ContextComboxBoxView()
        self._sessionContextWidget = sessionContextWidget.ContextSessionComboxBoxView()
        self._assetTreeView = QtGui.QTreeView()
        self._currentContextLabel = QtGui.QLabel("No Context Selected")
        self._buildSceneButton = QtGui.QPushButton("Build Whole Scene")
        self._importSelectedButton = QtGui.QPushButton("Import Selected")

        # Models
        self._model = watchstarModel.AssetWatchstarModel()

        # Configure
        self._currentContextLabel.setAlignment(QtCore.Qt.AlignCenter)
        contextTab.setMaximumHeight(80)
        contextTab.addTab(self._shotsContextWidget, "Shots")
        contextTab.addTab(self._trialContextWidget, "Trials")
        contextTab.addTab(self._sessionContextWidget, "Sessions")
        self._buildSceneButton.setEnabled(False)
        self._importSelectedButton.setEnabled(False)
        self._assetTreeView.setAlternatingRowColors(True)
        self._assetTreeView.setModel(self._model)
        self._assetTreeView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)

        # widgets to layouts
        buttonLayout.addWidget(self._buildSceneButton)
        buttonLayout.addWidget(self._importSelectedButton)
        mainLayout.addWidget(contextTab)
        mainLayout.addWidget(self._currentContextLabel)
        mainLayout.addWidget(self._assetTreeView)
        mainLayout.addLayout(buttonLayout)

        # layouts to widgets
        self.setLayout(mainLayout)

        # connections
        self._shotsContextWidget.shotSelected.connect(self._handleContexSelect)
        self._trialContextWidget.trialSelected.connect(self._handleContexSelect)
        self._sessionContextWidget.sessionSelected.connect(self._handleContexSelect)
        self._buildSceneButton.pressed.connect(self._handleBuildScene)
        self._importSelectedButton.pressed.connect(self._handleImportSelected)

    def setCurrentStudio(self, studio):
        self.currentStudio = studio

    def _handleContexSelect(self, newContext):
        self._model.setInputConext(newContext)
        if newContext is None:
            self._currentContextLabel.setText("No Context Selected")
            return
        self._currentContextLabel.setText("{0} ({1})".format(newContext.name, type(newContext).__name__))
        self._assetTreeView.expandAll()
        self._buildSceneButton.setEnabled(True)
        self._importSelectedButton.setEnabled(True)

    def _handleImportSelected(self):
        if self._model.getInputContext() is None:
            QtGui.QMessageBox.critical(self, "Watchstar", "No Shot/Trial Context selected")
            return
        items = list(set([idx.data(QtCore.Qt.UserRole) for idx in self._assetTreeView.selectedIndexes()]))
        for item in items:
            self._referenceAssetItem(item)
        self._updateView()

    def _handleBuildScene(self):
        if self._model.getInputContext() is None:
            QtGui.QMessageBox.critical(self, "Watchstar", "No Shot/Trial Context selected")
            return
        for refTypes in [self._model.audioItems(), self._model.characterItems(), 
                         self._model.enviromentItems(), self._model.propItems(), 
                         self._model.vehiclesItems()]:
            for item in refTypes:
                self._referenceAssetItem(item)
        self._updateView()
        
    def _updateView(self):
        self._model.update()
        self._assetTreeView.expandAll()

    def _referenceAssetItem(self, item):
        if not isinstance(item, watchstarModel.AssetWatchstarModelItem):
            return
        fileState = item.getFileState()
        if fileState is None:
            return

        # Handle Audio
        if fileState.ClientFilename.lower().endswith('.wav'):
            self._createAudio(fileState)
        # Handle FBX (reference)
        else:
            self._manager.CreateReferences(str(fileState.ClientFilename))

    def _createAudio(self, audioFile, showPopups=True):
        '''
        Creates Audio reference
        '''
        # Check for dupes - quiet mode optional
        audioDupe = None
        for each in Globals.gScene.AudioClips:
            if each.LongName == os.path.basename(str(audioFile.ClientFilename)):
                audioDupe = each
                break
            
        if showPopups:
            if audioDupe:
                flags = QtGui.QMessageBox.StandardButton.Yes | QtGui.QMessageBox.StandardButton.No
                question = "This audio file already exists within the scene.  Would you like to overwrite?"
                response = QtGui.QMessageBox.question(self, "Duplicate Audio", question, flags)
                if response == QtGui.QMessageBox.Yes:
                    audioDupe.FBDelete()
                else:
                    return
        else:
            if audioDupe:
                audioDupe.FBDelete()

        self._manager.CreateReferences(str(audioFile.ClientFilename))
