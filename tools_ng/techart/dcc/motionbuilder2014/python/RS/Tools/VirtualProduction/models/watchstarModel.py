"""
Base Context Model for the lazy loaded contexts
"""
import os

from PySide import QtGui, QtCore

from RS import Perforce
from RS.Core.AnimData._internal import contexts
from RS.Core.AnimData import Context
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem, textModelItem

PERFORCE_ROLE = QtCore.Qt.UserRole + 1


class AssetWatchstarModelItem(baseModelItem.BaseModelItem):
    """
    Depot File Item Model Type

    This represents an actual P4 File
    """
    def __init__(self, filePath, name=None, parent=None):
        """
        Constructor
        """
        super(AssetWatchstarModelItem, self).__init__(parent=parent)

        # handle the '^' if there is one
        if "^" in filePath:
            parts = filePath.split("^", 2)
            ext = parts[-1].split(".", 2)
            filePath = "{0}.{1}".format(parts[0], ext[-1])

        self._filePath = filePath
        self._name = name or os.path.splitext(os.path.split(filePath)[-1])[0]
        self._fileState = Perforce.GetFileState(filePath) or None

    def name(self):
        return self._name

    def getFileState(self):
        return self._fileState

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.name()
            if column == 1:
                if self._fileState is None:
                    return "No Version Found in Perforce"
                return "{0}/{1}".format(self._fileState.HaveRevision, self._fileState.HeadRevision)
            if column == 2:
                return self._filePath

        elif role == QtCore.Qt.UserRole:
            return self

        elif role == PERFORCE_ROLE:
            return self._fileState

        elif role == QtCore.Qt.ForegroundRole:
            if self._fileState is None:
                return QtGui.QBrush(QtGui.QColor(255, 0, 0))
            if self._fileState.HaveRevision != self._fileState.HeadRevision:
                return QtGui.QBrush(QtGui.QColor(255, 170, 0))
            return QtGui.QBrush(QtGui.QColor(0, 255, 0))

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False

    def rowCount(self, parent=None):
        return 0

    def hasChildren(self, parent=None):
        return False


class EnvVechAssetWatchstarModelItem(AssetWatchstarModelItem):
    """
    Depot File Item Model Type

    This represents an actual P4 File
    """
    def __init__(self, asset, name=None, parent=None):
        """
        Constructor
        """
        super(EnvVechAssetWatchstarModelItem, self).__init__(asset.fbxGameAssetPath(), name=name, parent=parent)
        self._asset = asset


class AssetWatchstarModel(baseModel.BaseModel):
    """
    Base Model Type
    """
    def __init__(self, parent=None):
        self._context = None
        super(AssetWatchstarModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Asset Name", "Version", "FileName"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def audioItems(self):
        """
        Get a list of all the Audio items in the model
        """
        return list(self._audioTop.childItems)

    def characterItems(self):
        """
        Get a list of all the Character items in the model
        """
        return list(self._charactersTop.childItems)

    def enviromentItems(self):
        """
        Get a list of all the Environments items in the model
        """
        return list(self._enviromentTop.childItems)

    def propItems(self):
        """
        Get a list of all the Props items in the model
        """
        return list(self._propsTop.childItems)

    def vehiclesItems(self):
        """
        Get a list of all the Vehicle items in the model
        """
        return list(self._vehiclesTop.childItems)

    def setInputConext(self, newContext):
        """
        Set the AnimData context to be used by the model
        """
        self.beginResetModel()
        self._audioTop.clearChildren()
        self._charactersTop.clearChildren()
        self._enviromentTop.clearChildren()
        self._propsTop.clearChildren()
        self._vehiclesTop.clearChildren()
        self._context = newContext

        if newContext is not None:
            if isinstance(newContext, contexts.Session):
                for shot in newContext.getAllShots():
                    self._populateModelFromTrial(shot)
            else:
                self._populateModelFromTrial(newContext)
        self.endResetModel()

    def _populateModelFromTrial(self, inputTrial):
        """
        Internal Method

        """
        if not isinstance(inputTrial, (contexts.CutSceneTrial, contexts.InGameTrial, contexts.Session, contexts.Shot)):
            return
        for char in inputTrial.getCharacters():
            gameChar = char.gameChar()
            if gameChar is None:
                gameCharName = "<NO CHARACTER SET>"
            else:
                gameCharName = gameChar.fbxGameAssetPath()

            scriptChar = char.scriptChar()
            if scriptChar is None:
                scriptCharName = "<NO SCRIPT CHARACTER SET>"
            else:
                scriptCharName = scriptChar.characterName()
            self._charactersTop.appendChild(AssetWatchstarModelItem(gameCharName, scriptCharName, parent=self._charactersTop))

        for prodAsset in inputTrial.getEnvironments():
            asset3d = prodAsset.getAsset3d()
            if asset3d is None:
                continue
            if isinstance(inputTrial, contexts.MotionTrialBase):
                self._enviromentTop.appendChild(EnvVechAssetWatchstarModelItem(asset3d, parent=self._enviromentTop))
            else:
                filePath = asset3d.fbxGameAssetPath()
                if filePath is None:
                    continue
                self._enviromentTop.appendChild(AssetWatchstarModelItem(filePath, parent=self._enviromentTop))

        for prodAsset in inputTrial.getVehicles():
            asset3d = prodAsset.getAsset3d()
            if asset3d is None:
                continue
            if isinstance(inputTrial, contexts.MotionTrialBase):
                self._vehiclesTop.appendChild(EnvVechAssetWatchstarModelItem(asset3d, parent=self._vehiclesTop))
            else:
                filePath = asset3d.fbxGameAssetPath()
                if filePath is None:
                    continue
                self._vehiclesTop.appendChild(AssetWatchstarModelItem(filePath, parent=self._vehiclesTop))

        for prodAsset in inputTrial.getProps():
            asset3d = prodAsset.getAsset3d()
            if asset3d is None:
                continue
            filePath = asset3d.fbxGameAssetPath()
            if filePath is None:
                continue
            self._propsTop.appendChild(AssetWatchstarModelItem(filePath, parent=self._propsTop))

        # Temporarily commented out until support is added for accessing files from the linked Feature.
        # if isinstance(inputTrial, contexts.MotionTrialBase):
        #     pathType = Context.settingsManager.consts.FilePathTypes.FBX_AUDIO
        #     for audioFile in inputTrial.getAllFiles(pathType=pathType):
        #         if audioFile is None:
        #             continue
        #         filePath = audioFile.rawFilePath()
        #         if not filePath:
        #             continue
        #         self._audioTop.appendChild(AssetWatchstarModelItem(filePath, parent=self._audioTop))

    def getInputContext(self):
        """"
        Get the current animData context used by the model
        """
        return self._context

    def update(self):
        """
        Forces an update of the model
        """
        self.setInputConext(self._context)

    def setupModelData(self, parent):
        """
        setup the model data

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        self._audioTop = textModelItem.TextModelItem(["Audio"], parent=parent)
        self._charactersTop = textModelItem.TextModelItem(["Characters"], parent=parent)
        self._enviromentTop = textModelItem.TextModelItem(["Environments"], parent=parent)
        self._propsTop = textModelItem.TextModelItem(["Props"], parent=parent)
        self._vehiclesTop = textModelItem.TextModelItem(["Vehicles"], parent=parent)

        parent.appendChild(self._audioTop)
        parent.appendChild(self._charactersTop)
        parent.appendChild(self._enviromentTop)
        parent.appendChild(self._propsTop)
        parent.appendChild(self._vehiclesTop)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    mainModel = AssetWatchstarModel()
    # win = QtGui.QColumnView()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
    sys.exit(app.exec_())
