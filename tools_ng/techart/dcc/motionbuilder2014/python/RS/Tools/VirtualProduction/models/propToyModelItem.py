from PySide import QtCore, QtGui

from RS.Tools.VirtualProduction import const
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class PropToyModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, baseName, fullPath=None, folderBool=False, parent=None):
        super(PropToyModelItem, self).__init__([], parent=parent)
        self.parentItem = parent
        self._columnNumber = 2
        self._baseName = baseName
        self._fullPath = fullPath
        self._isFolder = folderBool
        self.childItems = []
        self._itemIcon = None

        if self._isFolder:
            self._itemIcon = const.Icons.Folder
        else:
            self._itemIcon = const.Icons.File

    def appendChild(self, item):
        """
        add children to node
        """
        self.childItems.append(item)

    def child(self, row):
        """
        get child item at index row
        """
        return self.childItems[row]

    def childCount(self):
        """
        Get number of children
        """
        return len(self.childItems)

    def GetBaseName(self):
        '''
        Return:
            Type (string): the base name of the prop
        '''
        return self._baseName

    def GetFullPath(self):
        '''
        Return:
            Type (string): the full path of the prop
        '''
        return self._fullPath

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._baseName

        elif role == QtCore.Qt.DecorationRole:
            if column == 0:
                return self._itemIcon

        elif role == const.PropModelDataIndex.folderFileRole:
            if self._fullPath != None:
                return True

        elif role == QtCore.Qt.ToolTipRole:
            if column == 0:
                return self._fullPath

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()

        if role == QtCore.Qt.ToolTipRole:
            if column == 0:
                self._motionImportPath = _fullPath
                return True

        return False
