from PySide import QtGui, QtCore

import os
import re

import pyfbsdk as mobu

from RS import Globals
from RS.Tools.VirtualProduction import const
from RS.Tools.UI.Mocap.Models import motionImportModelTypes
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Utils import Scene
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.Manager import Manager


class MotionImportWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(MotionImportWidget, self).__init__(parent=parent)
        self.setupUi()

    def _handleSelectAll(self):
        for child in self._mainModel.rootItem.children():
            idx = self._mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self._mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainModel.dataChanged.emit(index0, index1)

    def _handleDeselectAll(self):
        for child in self._mainModel.rootItem.children():
            idx = self._mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self._mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainModel.dataChanged.emit(index0, index1)

    def _setEnableOnCheckBoxes(self):
        '''
        Sets the enable properties of the timeshift-related widgets, based off 
        of which checkbox is active.
        '''
        if self.autoShiftCheckbox.isChecked():
            self.slateListCombobox.setEnabled(True)
            self.frameCombobox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)
        else:
            self.slateListCombobox.setEnabled(False)
            self.frameCombobox.setEnabled(False)
            self.frameSpinBox.setEnabled(True)

    def _getGSAssetInfoDict(self):
        '''
        Generates the GS Asset info List
        
        Return:
            dict (FBModel:string): key=itemNull value=defaultPath
        '''
        assetInfoList = []
        gsSlateInfoDict = {}
        gsCharacterInfoDict = {}
        gsPropInfoDict = {}
        # get items from tree
        for child in self._mainModel.rootItem.children():
            # get checked state
            if child.GetCheckedState():
                # get index
                index = None
                idx = self._mainModel.rootItem.childItems.index(child)
                index1 = child.createIndex(idx, 1, child)
                # get item null
                itemRoot = child.GetGSAssetRoot()
                # get item default path
                defaultPath = child.GetDefaultPath()
                # get asset type
                assetType = child.GetGSType()
                # add info to dict
                if assetType == const.GSModelTypes.Slate:
                    gsSlateInfoDict[itemRoot] = defaultPath, index1, child, assetType
                if assetType == const.GSModelTypes.Character:
                    gsCharacterInfoDict[itemRoot] = defaultPath, index1, child, assetType
                if assetType == const.GSModelTypes.Prop:
                    gsPropInfoDict[itemRoot] = defaultPath, index1, child, assetType
                # reset checked state
                child.SetCheckedState(False)
        # add dicts to a list
        assetInfoList.append(gsSlateInfoDict)
        assetInfoList.append(gsCharacterInfoDict)
        assetInfoList.append(gsPropInfoDict)
        return assetInfoList

    def _getNonGSCharacters(self):
        '''
        Finds all of the non-gs characters in the scene
        
        Return:
            List (FBCharacter): list of character nodes found, which are not gs-related
        '''
        characterModelList = []
        for character in Globals.Characters:
            if 'gs' not in character.Name and not re.match("^M[0-9]+", character.Name):
                characterModelList.append(character)
        return characterModelList

    def _handleMotionImport(self):
        '''
        Motion import used to bring in clean data onto selected assets, from the UI.
        User selects the path to the clean data file for each asset, via a QFileDialog
        '''
        assetInfoList = self._getGSAssetInfoDict()
        try:
            for assetDictionary in assetInfoList:
                for key, value in assetDictionary.iteritems():
                    key.Name
        except:
            QtGui.QMessageBox.warning(None,
                                      "",
                                      "Please refresh the UI for the current scene's data.")
            return

        # deselect all
        Scene.DeSelectAll()

        # motion import for each selected item
        for assetDictionary in assetInfoList:
            for key, value in assetDictionary.iteritems():
                if value[3] == const.GSModelTypes.Slate:
                    # slate found, we merge this instead of using motion import
                    fileName, filePath = self.SlateMerge(defaultPath=value[0],
                                                        windowTitle="{0}".format(key.LongName.upper()),
                                                        nameFilters=["*.fbx", "*.FBX"])
                    # update slate combobox
                    self.populateSlateComboBox()
                else:
                    # select gs item's root/bone
                    key.Selected = True

                    # user selects the file to motion import to selected
                    fileName = None
                    filePath = None
                    filePopup = QtGui.QFileDialog()
                    filePopup.setWindowTitle("{0}".format(key.LongName.upper()))
                    filePopup.setViewMode(QtGui.QFileDialog.List)
                    filePopup.setNameFilters(["*.fbx", "*.FBX"])
                    filePopup.setDirectory(value[0])
                    if filePopup.exec_() :
                        if len(filePopup.selectedFiles()) > 0:
                            filePath = filePopup.selectedFiles()[0]
                            fileInfo = QtCore.QFileInfo(filePath)
                            fileName = str(fileInfo.baseName())
                            # normalise path and turn into a string
                            filePath = str(os.path.normpath(filePath))
                            # import motion
                            mobu.FBApplication().FileImport(filePath)

                    # deselect itemNull
                    key.Selected = False

                # create properties with user selected clean data path and name
                if fileName and filePath:
                    cleanDataPathProperty = key.PropertyCreate('Clean Data Path',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "", False,
                                                               True,
                                                               None)
                    cleanDataPathProperty.Data = filePath
                    cleanDataNameProperty = key.PropertyCreate('Clean Data Name',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "",
                                                               False,
                                                               True,
                                                               None)
                    cleanDataNameProperty.Data = fileName

                # set data (update view)
                if value[1] != None:
                    # set column 1 editrole data
                    dataValue = fileName
                    self._mainModel.setData(value[1], dataValue, QtCore.Qt.EditRole)
                    # set column 1 editrole data
                    dataValue = filePath
                    self._mainModel.setData(value[1], dataValue, QtCore.Qt.ToolTipRole)
                    ## set column 0 checkstate data
                    #dataValue = False
                    #self._mainModel.setData(value[1], dataValue, QtCore.Qt.CheckStateRole)

                # deselected any row which may be selected
                self.treeView.clearSelection()

    def _handlePlotCharactersToSkeleton(self):
        '''
        Plots all non-gs characters to their skeletons
        '''
        characterModelList = self._getNonGSCharacters()
        for character in characterModelList:
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = False
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

            if character.ActiveInput == True:
                character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions)
                character.ActiveInput == False

    def _handlePlotAll(self):
        '''
        Plots characters, props and vehicles.
        Will only find and plot the models that have references.
        '''
        # plot character models
        self._handlePlotCharactersToSkeleton()

        # plot everything else
        manager = Manager()
        nonCharacterReferenceList = []
        # deselect all
        Scene.DeSelectAll()
        # get references
        referenceList = manager.GetReferenceListAll()
        for reference in referenceList:
            # only plot for prop and vehicle references
            if reference.FormattedTypeName in ['Prop', 'Vehicle']:
                referenceNamespace = reference.Name
                for component in Globals.Components:
                    if '{0}:'.format(referenceNamespace) in component.LongName and isinstance(component, mobu.FBModel):
                        nonCharacterReferenceList.append(component)
                        break
        # iterate through found references, select all items in their heirarchy
        heirarchyList = []
        for reference in nonCharacterReferenceList:
            parent = Scene.GetParent(reference)
            Scene.GetChildren(parent, heirarchyList, "", False)
        # select items in heirarchy
        for item in heirarchyList:
            item.Selected = True
        # plot selected
        Scene.Plot.PlotCurrentTakeonSelected()
        # deselect all
        Scene.DeSelectAll()

    def SlateMerge(self, defaultPath=None, windowTitle="", nameFilters="*"):
        '''
        Clean slate is merged into the scene
        
        Return:
            FileName (string): the name of the slate file, selected by the user
            FilePath (String): the path to the slate file selected by the user
        '''
        fileName = None
        filePath = None

        # popup settings
        filePopup = QtGui.QFileDialog()
        filePopup.setWindowTitle(windowTitle)
        filePopup.setViewMode(QtGui.QFileDialog.List)
        filePopup.setNameFilters(nameFilters)
        filePopup.setDirectory(defaultPath)

        # launch popup
        if filePopup.exec_() :
            if len(filePopup.selectedFiles()) > 0:
                filePath = filePopup.selectedFiles()[0]
                fileInfo = QtCore.QFileInfo(filePath)
                fileName = str(fileInfo.baseName())
                # normalise path and turn into a string
                filePath = str(os.path.normpath(filePath))
                # set merge options
                options = mobu.FBFbxOptions(True, filePath)
                options.BaseCameras = False
                options.CameraSwitcherSettings = False
                options.CurrentCameraSettings = False
                options.GlobalLightingSettings = False
                options.TransportSettings = False
                # merge
                mobu.FBApplication().FileMerge(filePath, False, options)
        return fileName, filePath

    def ZeroToSlateCheckBoxes(self):
        if self.manualGenerateCheckBox.checkState() == QtCore.Qt.CheckState.Checked:
            self.frameSpinBox.setEnabled(True)
            self.frameComboBox.setEnabled(False)
            self.slateComboBox.setEnabled(False)
        else:
            self.frameComboBox.setEnabled(True)
            self.slateComboBox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)

    def populateSlateComboBox(self):
        self.slateComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key in clapsDict.iterkeys():
            self.slateComboBox.addItem(key.LongName)
        self.PopulateBeepsFromSlate()

    def PopulateBeepsFromSlate(self):
        """
        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        slateString = self.slateComboBox.currentText()
        if slateString == None:
            return
        self.frameComboBox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key, value in clapsDict.iteritems():
            if str(slateString) == key.LongName:
                for clap in value:
                    self.frameComboBox.addItem(str(clap))

    def ZeroToSlate(self):
        if self.autoGenerateCheckBox.isChecked() == True:
            frame = self.frameComboBox.currentText()
            if frame != "":
                MocapCommands.ZeroOutScene(int(frame))

        if self.manualGenerateCheckBox.isChecked() == True:
            frame = self.frameSpinBox.value()
            MocapCommands.ZeroOutScene(frame)

    def resetTree(self):
        self._mainModel.reset()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        timeShiftLayout = QtGui.QGridLayout()
        motionImportGroupBoxGridLayout = QtGui.QGridLayout()
        plotGroupBoxGridLayout = QtGui.QGridLayout()
        zeroGroupBoxGridLayout = QtGui.QGridLayout()

        # create widgets
        motionImportGroupBox = QtGui.QGroupBox()
        self._mainModel =  motionImportModelTypes.MotionImportModel()
        self.treeView = QtGui.QTreeView()
        self.treeView.setModel(self._mainModel)
        selectAllPushButton = QtGui.QPushButton()
        deselectAllPushButton = QtGui.QPushButton()
        plotGroupBox = QtGui.QGroupBox()
        motionImportPushButton = QtGui.QPushButton()
        plotPushButton = QtGui.QPushButton()
        plotCharactersPushButton = QtGui.QPushButton()
        plotSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        zeroGroupBox = QtGui.QGroupBox()
        self.autoGenerateCheckBox = QtGui.QCheckBox()
        self.slateComboBox = QtGui.QComboBox()
        self.frameComboBox = QtGui.QComboBox()
        self.manualGenerateCheckBox = QtGui.QCheckBox()
        self.frameSpinBox = QtGui.QSpinBox()
        timeShiftButton = QtGui.QPushButton()
        zeroButtonGroup = QtGui.QButtonGroup(self)

        # widget properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        self.treeView.header().resizeSection(0, 400)
        self.treeView.header().resizeSection(1, 400)
        motionImportPushButton.setText("Bring in Clean Data for Selected")
        plotPushButton.setText("Plot all")
        selectAllPushButton.setText("Select all")
        deselectAllPushButton.setText("Deselect all")
        plotPushButton.setToolTip("Plots all non-gs models")
        plotCharactersPushButton.setText("Plot all Characters to Skeleton")
        plotPushButton.setToolTip("Plots all character models (non-gs), props and vehicles to skeleton")
        motionImportGroupBox.setTitle("Motion Import")
        plotGroupBox.setTitle("Plotting")
        zeroGroupBox.setTitle("Zero to Slate")
        self.autoGenerateCheckBox.setText("Auto-generate frame(s)")
        self.manualGenerateCheckBox.setText("Manually set frame")
        timeShiftButton.setText("Timeshift Scene")
        self.frameSpinBox.setMinimum(0)
        self.frameSpinBox.setMaximum(10000.0)
        self.frameSpinBox.setValue(0)
        self.autoGenerateCheckBox.setCheckState(QtCore.Qt.CheckState.Checked)
        self.populateSlateComboBox()
        self.frameSpinBox.setEnabled(False)
        zeroButtonGroup.addButton(self.autoGenerateCheckBox)
        zeroButtonGroup.addButton(self.manualGenerateCheckBox)

        # add widgets to layout
        motionImportGroupBoxGridLayout.addWidget(self.treeView, 0, 0, 1, 6)
        motionImportGroupBoxGridLayout.addWidget(selectAllPushButton, 1, 0)
        motionImportGroupBoxGridLayout.addWidget(deselectAllPushButton, 1, 1)
        motionImportGroupBoxGridLayout.addWidget(motionImportPushButton, 2, 0, 1, 6)
        plotGroupBoxGridLayout.addWidget(plotCharactersPushButton, 0, 0)
        plotGroupBoxGridLayout.addWidget(plotPushButton, 1, 0)
        plotGroupBoxGridLayout.addItem(plotSpacerItem, 2, 0)
        zeroGroupBoxGridLayout.addWidget(self.autoGenerateCheckBox, 0, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.slateComboBox, 1, 0)
        zeroGroupBoxGridLayout.addWidget(self.frameComboBox, 1, 1)
        zeroGroupBoxGridLayout.addWidget(self.manualGenerateCheckBox, 2, 0, 1, 2)
        zeroGroupBoxGridLayout.addWidget(self.frameSpinBox, 3, 0)
        zeroGroupBoxGridLayout.addWidget(timeShiftButton, 4, 0, 1, 2)

        # set groupBox layout
        motionImportGroupBox.setLayout(motionImportGroupBoxGridLayout)
        plotGroupBox.setLayout(plotGroupBoxGridLayout)
        zeroGroupBox.setLayout(zeroGroupBoxGridLayout)

        # add groupbox to mainlayout
        mainLayout.addWidget(motionImportGroupBox, 0, 0, 2, 2)
        mainLayout.addWidget(zeroGroupBox, 0, 2)
        mainLayout.addWidget(plotGroupBox, 1, 2)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        motionImportPushButton.released.connect(self._handleMotionImport)
        plotCharactersPushButton.released.connect(self._handlePlotCharactersToSkeleton)
        plotPushButton.released.connect(self._handlePlotAll)
        selectAllPushButton.released.connect(self._handleSelectAll)
        deselectAllPushButton.released.connect(self._handleDeselectAll)
        self.manualGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
        self.autoGenerateCheckBox.stateChanged.connect(self.ZeroToSlateCheckBoxes)
        self.slateComboBox.currentIndexChanged.connect(self.PopulateBeepsFromSlate)
        timeShiftButton.released.connect(self.ZeroToSlate)