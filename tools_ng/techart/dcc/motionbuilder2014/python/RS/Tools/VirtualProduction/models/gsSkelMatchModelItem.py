from PySide import QtCore, QtGui

from RS.Tools.VirtualProduction import const
from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem



class gsSkelMatchTextModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, characterName, characterNode, gsName, gsPathList, actorName, actorColorString, parent=None):
        super(gsSkelMatchTextModelItem, self).__init__([], parent=parent)
        self._columnNumber = 4
        self._colorComboBoxString = actorColorString
        self._checked = False
        self._isItalic = True
        self._characterName = characterName
        self._characterNode = characterNode
        self._skelComboBoxString = gsName
        self._gsPathList = gsPathList
        self._existingActorName = actorName
        self._actorNameString = "enter actor name"
        if self._existingActorName:
            self._actorNameString = actorName

    def SetCheckedState(self):
        '''
        Sets check state to False for item
        '''
        self._checked = False

    def GetCheckedState(self):
        '''
        returns current Check state of item
        '''
        return self._checked

    def GetCharacterNamespace(self):
        '''
        returns character namespace for item
        '''
        return self._characterName

    def GetCharacterNode(self):
        '''
        returns character node for item
        '''
        return self._characterNode

    def GetGSSkelPath(self):
        '''
        returns current gs path node, for item
        gs path is found based off what gs Skel Name the user has selected
        '''
        for path in self._gsPathList:
            if '\{0}.fbx'.format(self._skelComboBoxString.lower()) in path.lower():
                
                return path
        return

    def GetUserActorString(self):
        '''
        returns current actor string
        '''
        return str(self._actorNameString)

    def GetUserColor(self):
        '''
        returns current color string
        '''
        return str(self._colorComboBoxString)

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role in [QtCore.Qt.DisplayRole, QtCore.Qt.EditRole]:
            if column == 0:
                return self._characterName
            if column == 1:
                return self._skelComboBoxString
            if column == 2:
                return self._actorNameString
            if column == 3:
                return self._colorComboBoxString

        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked

        elif role == QtCore.Qt.FontRole:
            newFont = QtGui.QFont()
            newFont.setPointSize(10)
            if column == 2:
                if not self._existingActorName:
                    newFont.setItalic(self._isItalic)
                return newFont

        elif role == QtCore.Qt.ToolTipRole:
            if column == 0:
                return 'Check box to select for merging'
            if column in [1, 2, 3]:
                return 'Double click cell to edit'
        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()
        if role == QtCore.Qt.EditRole:
            if column == 1:
                self._skelComboBoxString = value
                return True
            if column == 2:
                self._actorNameString = value
                self._isItalic = False
                return True
            if column == 3:
                self._colorComboBoxString = value
                return True

        elif role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True
        return False

