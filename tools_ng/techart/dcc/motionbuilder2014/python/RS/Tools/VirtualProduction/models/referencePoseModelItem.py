from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class ReferencePoseModelItem(textModelItem.TextModelItem):
    """
    Text Item to display text in columns for the model
    """
    def __init__(self, character, characterString, parent=None):
        super(ReferencePoseModelItem, self).__init__([], parent=parent)
        self.parentItem = parent
        self._columnNumber = 1
        self._character = character
        self._characterString = characterString
        self._checked = False

    def SetCheckedState(self, boolean):
        '''
        sets check state to False for item
        '''
        self._checked = boolean

    def GetCheckedState(self):
        '''
        Return:
            Checked State (Boolean): current check state of gs asset's checkbox in ui
        '''
        return self._checked

    def GetCharacter(self):
        '''
        Return:
            Type (string): the base name of the prop
        '''
        return self._character

    def GetCharacterString(self):
        '''
        Return:
            Type (string): the full path of the prop
        '''
        return self._characterString

    def columnCount(self):
        '''
        returns the number of columns in view
        '''
        return self._columnNumber

    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self._characterString

        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if self._checked:
                    return QtCore.Qt.CheckState.Checked
                else:
                    return QtCore.Qt.CheckState.Unchecked

        return None

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        model data updated based off of user interaction with the view
        '''
        column = index.column()

        if role == QtCore.Qt.CheckStateRole:
            if column == 0:
                if value == QtCore.Qt.CheckState.Checked:
                    self._checked = True
                else:
                    self._checked = False
                return True

        return False
