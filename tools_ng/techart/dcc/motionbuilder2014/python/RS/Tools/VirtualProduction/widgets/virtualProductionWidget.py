import pyfbsdk as mobu

from PySide import QtGui, QtCore

import os

from RS.Tools import UI
from RS import Config, Globals
from RS.Utils.Scene import Component
from RS.Core.Mocap import MocapCommands
from RS.Tools.VirtualProduction import const
from RS.Core.ReferenceSystem.Manager import Manager


from RS.Tools.VirtualProduction.widgets import watchstarWidget, stagingWidget, propToyWidget, gsSkelMatchWidget, duringShootWidget, referencePoseWidget, layoutWidget, trialNotesWidget, motionImportWidget


class VirtualProductionDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run ControlRigWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        self._settings = QtCore.QSettings("RockstarSettings", "VirtualProductionDialog")
        super(VirtualProductionDialog, self).__init__(parent=parent, title="Virtual Production", dockable=True)
        self._dock.closeEvent = self.closeEvent
        self._dock.resize(self._settings.value("size", QtCore.QSize(900, 500)))
        self._virtualProductionWidget = VirtualProductionWidget()

        # create layout and add widget
        self.setCentralWidget(self._virtualProductionWidget)

    def showEvent(self, event):
        self._dock.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self._dock.saveGeometry())
        self._settings.setValue("size", self._dock.size())
        return super(VirtualProductionDialog, self).closeEvent(event)


class VirtualProductionWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(VirtualProductionWidget, self).__init__(parent=parent)
        self._tabs = None
        self._watchstarTab = None
        self._stagingTab = None
        self._propToyTab = None
        self._gsSkelMatchTab = None
        self._duringShootTab = None
        self._referencePoseTab = None
        self._layoutTab = None
        #self._notesTab = None
        self._motionImportTab = None
        self.setupUi()
        self._currentlyRefreshing = False
        self.manager = Manager()

    def onStageChange(self):
        if self._currentlyRefreshing is True:
            return
        currentStageModel = None
        currentStage = self.activeStageComboBox.currentText()
        for stage in self.getStageList():
            if stage.Name == str(currentStage):
                currentStageModel = stage
                break
        self._stagingTab.setCurrentStage(currentStageModel)
        self._propToyTab.setCurrentStage(currentStageModel)

    def RefreshUI(self):
        # refresh tab info where applicable
        self.populateStageComboBox()
        # staging
        self._stagingTab.setDefaultStageName()
        # proptoys
        self._propToyTab.populateDriverTreeList()
        self._propToyTab.populateScenePropToyComboBoxList()
        # gs skel match
        self._gsSkelMatchTab.resetTree()
        # reference pose
        self._referencePoseTab.resetScrollArea()
        self._referencePoseTab.populateTreeView()
        # layout
        self._layoutTab.populateSlateComboBox()
        self._layoutTab.resetTree()
        # motion import
        self._motionImportTab.populateSlateComboBox()
        self._motionImportTab.resetTree()

    def getStageList(self):
        modelList = []
        stageList = []
        # model list is only up to 2 children - stages shouldnt be parented to anything, but we are
        # going to search a few children deep just in case
        for model in Globals.Scene.RootModel.Children:
            modelList.append(model)
            for subChild in [child for child in model.Children]:
                modelList.append(subChild)
                modelList.extend([child for child in subChild.Children])
        # iterate through model list to find stage(s)
        for model in modelList:
            stageProperty = model.PropertyList.Find('Stages')
            if stageProperty:
                stageList.append(model)
        return stageList

    def populateStageComboBox(self):
        self._currentlyRefreshing = True
        self.activeStageComboBox.clear()
        stageList = self.getStageList()
        activeStage = self.getActiveStage(stageList)
        comboBoxItemList = [item.LongName for item in stageList if item != activeStage]
        if activeStage is not None:
            activeStageName = activeStage.LongName
            comboBoxItemList = [activeStageName] + comboBoxItemList + ["Not Set"]
        else:
            comboBoxItemList = ["Not Set"] + comboBoxItemList
        self.activeStageComboBox.clear()
        self.activeStageComboBox.addItems(comboBoxItemList)
        self.activeStageComboBox.setCurrentIndex(0)
        self._currentlyRefreshing = False

    def setActiveStage(self):
        if self._currentlyRefreshing is True:
            return

        activeStage = None

        currentActiveStageSelection = str(self.activeStageComboBox.currentText())
        for stage in self.getStageList():
            activeProperty = stage.PropertyList.Find('Active Stage')
            if activeProperty != None:
                if stage.LongName == currentActiveStageSelection:
                    activeProperty.Data = True
                    activeStage = stage
                else:
                    activeProperty.Data = False

        if activeStage is None or self.manager.IsReferenced(Component.GetNamespace(activeStage)) is False:
            return

        reference = self.manager.GetReferenceByNamespace(Component.GetNamespace(activeStage))
        reference.SnapMocapConstraints()

    def getActiveStage(self, stageList):
        for stage in stageList:
            activeProperty = stage.PropertyList.Find('Active Stage')
            if activeProperty and activeProperty.Data == True:
                activeStage = stage
                return activeStage
        return None

    def _handleRenameStage(self):
        # get current take's name and active stage
        takeName = mobu.FBSystem().CurrentTake.Name
        stageList = self.getStageList()
        activeStage = self.getActiveStage(stageList)
        if activeStage is not None:
            activeStageNamespace = Component.GetNamespace(activeStage)
            if activeStageNamespace is None:
                activeStageNamespace = activeStage.Name
            reference = self.manager.IsReferenced(activeStageNamespace)
            if reference is not True:
                activeStageComponentList = []
                stageMaterial = None
                # find all component related to the stage and add to a list
                for idx in range(activeStage.GetSrcCount()):
                    activeStageComponentList.append(activeStage.GetSrc(idx))
                    if isinstance(activeStage.GetSrc(idx), mobu.FBMaterial):
                        stageMaterial = activeStage.GetSrc(idx)
                for idx in range(activeStage.GetDstCount()):
                    activeStageComponentList.append(activeStage.GetDst(idx))
                activeStageComponentList.append(activeStage)
                # get the components which arent picked up through a src/dst connection
                # handle
                for handle in Globals.Handles:
                    if handle.Name == activeStage.Name:
                        activeStageComponentList.append(handle)
                # folder
                for folder in Globals.Folders:
                    if folder.Name == activeStage.Name or folder.Name == '{0} 1'.format(activeStage.Name):
                        activeStageComponentList.append(folder)
                # textures
                if stageMaterial:
                    stageTexture = stageMaterial.GetTexture()
                    if stageTexture:
                        activeStageComponentList.append(stageTexture)
                # rename all components
                for component in activeStageComponentList:
                    component.Name = component.Name.replace(activeStageNamespace, takeName)
                # re activate stage
                activeProperty = activeStage.PropertyList.Find('Active Stage')
                activeProperty.Data = True
            else:
                self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(activeStageNamespace),
                                             takeName
                                             )
            # reset active stage list with new names
            self.populateStageComboBox()

    def _setupTabs(self):
        # create tab widgets/layout
        self._watchstarTab = watchstarWidget.WatchstarWidget()
        self._stagingTab = stagingWidget.StagingWidget()
        self._propToyTab = propToyWidget.PropToyWidget()
        self._gsSkelMatchTab = gsSkelMatchWidget.GSSkeletonMatchWidgets()
        self._duringShootTab = duringShootWidget.DuringShootWidget()
        self._referencePoseTab = referencePoseWidget.ReferencePoseWidget()
        self._layoutTab = layoutWidget.LayoutWidget()
        self._notesTab = trialNotesWidget.TrialNotesWidget()
        self._motionImportTab = motionImportWidget.MotionImportWidget()

        self._tabs.addTab(self._watchstarTab, "Watchstar")
        self._tabs.addTab(self._stagingTab, "Previz: Staging && Pre-Shoot")
        self._tabs.addTab(self._propToyTab, "Previz: PropToys")
        self._tabs.addTab(self._gsSkelMatchTab, "Previz: GS Skel Match")
        self._tabs.addTab(self._duringShootTab, "Shoot: During Shoot")
        self._tabs.addTab(self._referencePoseTab, "Shoot: Reference Pose")
        self._tabs.addTab(self._layoutTab, "Post: Layout")
        self._tabs.addTab(self._notesTab, "Post: Trial Notes")
        self._tabs.addTab(self._motionImportTab, "Post: Motion Import")

    def stageLockIcon(self):
        lockIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "lock.png"))
        return lockIcon

    def stageUnLockIcon(self):
        unlockIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "unlock.png"))
        return unlockIcon

    def setStageLockState(self):
        isLocked = self.getCurrentLockState()
        if isLocked:
            # stages are currently locked, we need to unlock them
            for stage in self.getStageList():
                transformProperty = stage.PropertyList.Find('Enable Transformation')
                if transformProperty:
                    transformProperty.Data = True
        else:
            # stages are currently unlocked, we need to lock them
            for stage in self.getStageList():
                transformProperty = stage.PropertyList.Find('Enable Transformation')
                if transformProperty:
                    transformProperty.Data = False
        self.setStageLockIcon()

    def getCurrentLockState(self):
        locked = False
        for stage in self.getStageList():
            transformProperty = stage.PropertyList.Find('Enable Transformation')
            if transformProperty and transformProperty.Data == False:
                locked = True
                break
        return locked

    def setStageLockIcon(self):
        isLocked = self.getCurrentLockState()
        if isLocked:
            self.stageLockButton.setIcon(self.stageLockIcon())
            self.stageLockButton.setToolTip("All Stages are Locked")
        else:
            self.stageLockButton.setIcon(self.stageUnLockIcon())
            self.stageLockButton.setToolTip("All Stages are Unlocked")

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        bannerLayout = QtGui.QGridLayout()
        studioLayout = QtGui.QGridLayout()

        # set layout properties
        bannerLayout.setVerticalSpacing(0)
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # create widgets
        banner = UI.BannerWidget(name='', helpUrl="")
        refreshButton = QtGui.QPushButton()
        activeStageLabel = QtGui.QLabel()
        self.activeStageComboBox = QtGui.QComboBox()
        stageLockLabel = QtGui.QLabel()
        self.stageLockButton = QtGui.QPushButton()
        stageRenamerButton = QtGui.QPushButton()
        self._tabs = QtGui.QTabWidget()

        # setup widget info
        refreshButton.setText('Refresh')
        activeStageLabel.setText(' Active Stage:')
        self.populateStageComboBox()
        stageLockLabel.setText(' Stages Locker:')
        self.setStageLockIcon()
        self.stageLockButton.setMaximumSize(QtCore.QSize(26, 20))
        self.stageLockButton.setIconSize(QtCore.QSize(22, 22))
        stageRenamerButton.setText('Rename to Stage to Current Take Name')
        self._setupTabs()
        self.onStageChange()

        # add widgets to layouts
        studioLayout.addWidget(activeStageLabel, 0, 0)
        studioLayout.addWidget(self.activeStageComboBox, 0, 1, 1, 3)
        studioLayout.addWidget(stageRenamerButton, 0, 4)
        studioLayout.addWidget(stageLockLabel, 1, 0)
        studioLayout.addWidget(self.stageLockButton, 1, 1)

        bannerLayout.addWidget(banner, 0, 0, 1, 2)
        bannerLayout.addWidget(refreshButton, 1, 0, 1 ,2)
        mainLayout.addLayout(bannerLayout, 2, 0, 1 ,2)
        mainLayout.addLayout(studioLayout, 4, 0)
        mainLayout.addWidget(self._tabs, 5, 0, 1, 2)

        # set layout
        self.setLayout(mainLayout)

        # callbacks
        self.activeStageComboBox.currentIndexChanged.connect(self.onStageChange)
        self.activeStageComboBox.currentIndexChanged.connect(self.setActiveStage)
        self.stageLockButton.pressed.connect(self.setStageLockState)
        self._stagingTab.stageSignal.connect(self.populateStageComboBox)
        stageRenamerButton.pressed.connect(self._handleRenameStage)
        refreshButton.pressed.connect(self.RefreshUI)
