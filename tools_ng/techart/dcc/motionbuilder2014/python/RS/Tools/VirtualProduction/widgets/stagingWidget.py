import pyfbsdk as mobu

from PySide import QtCore, QtGui

import os
import numpy as np

from RS import Config, Globals, Perforce
from RS.Tools import UI
from RS.Tools.UI.Mocap import PropSetSnapToolbox
from RS.Utils import Scene
from RS.Utils.Scene import Model
from RS.Core.Mocap import MocapCommands
from RS.Core.Mocap.Toolbox import BlockingModels, Stages
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Tools.VirtualProduction import const


class StagingWidget(QtGui.QWidget):
    stageSignal = QtCore.Signal()
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(StagingWidget, self).__init__(parent=parent)
        self.currentStage = None
        self.manager = Manager()
        self.setupUi()

    def setCurrentStage(self, stage):
        self.currentStage = stage

    def newYorkBlockingModelList(self):
        modelsList = ['Male',
                      'Female',
                      'Cube']
        return modelsList

    def modelSelectionList(self):
        createList = ['Stage',
                      'Blocking Models',]
        return createList

    def getCurrentName(self):
        currentName = str(self.nameEdit.text())
        return currentName

    def getCurrentColor(self):
        red = 1
        green = 1
        blue = 1
        color = self.colorDialog.currentColor()
        if color.isValid():
            rgbColor = color.getRgb()
            red = rgbColor[0]/256.0
            green = rgbColor[1]/256.0
            blue = rgbColor[2]/256.0
        return (red, green, blue)

    def blockingModelsList(self):
        # clear comboxbox and add default settings
        self.blockingModelsComboBox.setEnabled(True)
        modelsList = self.newYorkBlockingModelList()
        return modelsList

    def populateBlockingModelsList(self):
        modelCreateSelection = str(self.selectComboBox.currentText())
        if modelCreateSelection == 'Blocking Models':
            self.blockingModelsComboBox.clear()
            modelList = self.blockingModelsList()
            self.blockingModelsComboBox.addItems(modelList)
            self.blockingModelsComboBox.setEnabled(True)
        else:
            self.blockingModelsComboBox.clear()
            self.blockingModelsComboBox.setEnabled(False)

    def _handleCreateModel(self):
        # get user values
        userModelName = self.getCurrentName()
        modelCreateSelection = str(self.selectComboBox.currentText())
        blockingModelSelection = str(self.blockingModelsComboBox.currentText())
        currentColor = mobu.FBColor(self.getCurrentColor())

        # create stage
        if modelCreateSelection == 'Stage':
            stagePath = os.path.join(Config.VirtualProduction.Path.Previz,
                         const.PrevizFolder().GetStudioFolderName(),
                         'stages',
                         'stage01.fbx'
                         )
            Perforce.Sync(stagePath)
            if os.path.exists(stagePath):
                # create stage reference
                referenceList = self.manager.CreateReferences(stagePath)
                # we will only have created 1 reference, so lets get a variable on that
                reference = referenceList[0]
                # change color
                reference.ApplyColor(currentColor)
                # change namespace
                self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(reference.Name),
                                             userModelName
                                             )
                # fix double namespace issue - autodesk bug
                reference.FixHandleDoubleNamespace()
                # send signal that a stage has been created
                self.stageSignal.emit()

        # create blocking model
        elif modelCreateSelection == 'Blocking Models':
            # get path to selected blocking model
            blockingModelPath = os.path.join(const.PrevizFolder().GetGlobalFolderName(),
                                             'blockingModels',
                                             '{0}.fbx'.format(blockingModelSelection),
                                             )
            # create blocking model reference
            referenceList = self.manager.CreateReferences(blockingModelPath)
            # we will only have created 1 reference, so lets get a variable on that
            reference = referenceList[0]
            # change color
            reference.ApplyColor(currentColor)
            # change namespace
            self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(reference.Name),
                                         userModelName
                                         )
            # fix double namespace issue - autodesk bug
            reference.FixHandleDoubleNamespace()
            # align to stage
            reference.AlignReferenceRootToActiveStage()

        # reset lineEdit and color
        self.nameEdit.setText("")

    def _handleAddBasePreviz(self):
        '''
        Add in the base previz assets - everything under the base previz directory.
        Also adds in the slate, which is now a separate type from the base previz
        '''
        basePrevizRootDir = os.path.join(Config.VirtualProduction.Path.Previz,
                                         const.PrevizFolder().GetStudioFolderName(),
                                         'basePreviz',
                                         'baseAssets'
                                         )
        slateRootDir = os.path.join(Config.VirtualProduction.Path.Previz,
                                    const.PrevizFolder().GetStudioFolderName(),
                                    'slate'
                                    )
        camerasRootDir = os.path.join(Config.VirtualProduction.Path.Previz,
                                    const.PrevizFolder().GetStudioFolderName(),
                                    'cameras'
                                    )
        basePrevizFileList = []
        # get files in main folder directory
        for rootDir in [basePrevizRootDir, slateRootDir, camerasRootDir]:
            for root, subFolders, files in os.walk(rootDir):
                for fileName in files:
                    basePrevizPath = os.path.join(rootDir, fileName)
                    if os.path.exists(basePrevizPath):
                        Perforce.Sync(basePrevizPath)
                        basePrevizFileList.append(basePrevizPath)
        # create references
        self.manager.CreateReferences(basePrevizFileList)

    def _handleSnapPropsToSet(self):
        popup = MocapCommands.PropSetSnap()
        if popup == True:
            PropSetSnapToolbox.Run()
        else:
            QtGui.QMessageBox.information(None,
                                          "",
                                          "All found prop matches have been aligned.",
                                          QtGui.QMessageBox.Ok)

    def _handleRedMarkup(self):
        MocapCommands.TurnRedToggle()

    def _convertToInches(self,
                         distanceValue,
                         dimensionType=''):
        """
            Extract worldSpace boundingbox of an FBModel

            Args:
                distanceValue (double): value to convert to imperial system
                prefixInfo (str): distance prefix string

            returns dictionnary with keys:  feetValue       (double)
                                            inchesValue     (double)
                                            dimensionString (double)
        """
        #Source: http://www.metric-conversions.org/length/centimeters-to-feet.htm
        #ft =cm * 0.032808

        #original value in previous code was 0.3048
        feetToCm = 0.0328084
        feetValue = distanceValue * feetToCm

        feetRounded = np.modf(feetValue)[1]
        inchesValue = np.modf(feetValue)[0]
        inchesValue = np.around(inchesValue*12,0)

        dimensionType = "      {0}: ".format(dimensionType)
        dimensionString = "{0}{1} feet {2} inches".format(dimensionType,feetRounded,inchesValue)

        return (feetValue,
                inchesValue,
                distanceValue,
                dimensionString)

    def _handleDimensionOfSelected(self):
        '''
        Pop up window with the dimentions of the model you have selected
        '''
        components = mobu.FBModelList()
        # get the selected models
        mobu.FBGetSelectedModels( components )
        # show the list to the user
        if len( components ) == 0:
            QtGui.QMessageBox.information(None,
                                          "",
                                          "Nothing is selected.",
                                          QtGui.QMessageBox.Ok)
        else:
            message = ""
            for component in components:
                if type(component) == mobu.FBModel:
                    cleanUp_Model = Model.ProcessModelForBoundingBox(component,
                                                                     axisAligned=True)

                    boundingBox = Model.ComputeBoundingBox(cleanUp_Model)
                    boundingBox['sourceName'] = component.Name
                    cleanUp_Model.FBDelete()

                    # converting to feet and inches
                    lengthInImperial = self._convertToInches(boundingBox['length'],
                                                             dimensionType='Length')

                    widthInImperial = self._convertToInches(boundingBox['width'],
                                                            dimensionType='Width')

                    heightInImperial = self._convertToInches(boundingBox['height'],
                                                             dimensionType='Height')
                    #Create string for message
                    message += "\n{0}{1}{2}{3}".format(component.Name,
                                                       heightInImperial[3],
                                                       widthInImperial[3],
                                                       lengthInImperial[3])

            # bring up the pop-up
            QtGui.QMessageBox.information(None,
                                          "",
                                          message,
                                          QtGui.QMessageBox.Ok)

    def _handleLeadCams(self):
        MocapCommands.SetupLeadinLeadOutConstraints()

    def _handleResetCamFarPlane(self):
        for camera in Globals.Cameras:
            farPlaneProperty = camera.PropertyList.Find("Far Plane")
            gridProperty = camera.PropertyList.Find("Grid")
            if farPlaneProperty != None:
                farPlaneProperty.SetLocked(False)
                farPlaneProperty.Data = 4000000.0
                farPlaneProperty.SetLocked(True)
            if gridProperty != None:
                gridProperty.SetLocked(False)
                gridProperty.Data = False
                gridProperty.SetLocked(True)

    def _handleCamLabelVisibility(self):
        MocapCommands.TurnOffProducePerspectiveLabel()

    def setDefaultStageName(self):
        self.nameEdit.clear()
        filename = Scene.GetSceneName()
        self.nameEdit.setText(filename)

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        stagingGroupBoxGridLayout = QtGui.QGridLayout()
        preShootGroupBoxGridLayout = QtGui.QGridLayout()

        # create widgets
        stagingGroupBox = QtGui.QGroupBox()
        emptyLabel = QtGui.QLabel()
        createLabel = QtGui.QLabel()
        self.selectComboBox = QtGui.QComboBox()
        self.blockingModelsComboBox = QtGui.QComboBox()
        nameLabel = QtGui.QLabel()
        self.nameEdit = QtGui.QLineEdit()
        colorLabel = QtGui.QLabel()
        self.colorDialog = QtGui.QColorDialog()
        self.colorDialog.setOption(QtGui.QColorDialog.NoButtons)
        self.createButton = QtGui.QPushButton()
        preShootGroupBox = QtGui.QGroupBox()
        basePrevizButton = QtGui.QPushButton()
        snapPropButton = QtGui.QPushButton()
        redMarkupButton = QtGui.QPushButton()
        dimensionsButton = QtGui.QPushButton()
        multiCamButton = QtGui.QPushButton()
        leadInOutCamButton = QtGui.QPushButton()
        resetCamFarPlaneButton = QtGui.QPushButton()
        visibilityCheckBox = QtGui.QCheckBox()

        # widget properties
        self.setDefaultStageName()
        stagingGroupBox.setTitle("Staging")
        createLabel.setText('Select Model:')
        nameLabel.setText('Name:')
        colorLabel.setText('Color:')
        self.selectComboBox.addItems(self.modelSelectionList())
        self.createButton.setText('Create Model')
        self.populateBlockingModelsList()
        preShootGroupBox.setTitle("Pre-Shoot Options")
        basePrevizButton.setText('Add Base Previz')
        snapPropButton.setText('Snap Props to Set Object')
        redMarkupButton.setText('Red Markup')
        dimensionsButton.setText('Dimensions of Selected Model')
        multiCamButton.setText('Open Multi-Camera Viewer')
        leadInOutCamButton.setText('Lead Cams')
        resetCamFarPlaneButton.setText("Reset Camera FarPlanes && Grid")
        visibilityCheckBox.setText('Visibility for Perspective Camera Label')
        preshootSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        centerSpacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)

        # add widgets to groupBox
        stagingGroupBoxGridLayout.addWidget(createLabel, 0, 0)
        stagingGroupBoxGridLayout.addWidget(self.selectComboBox, 0, 1)
        stagingGroupBoxGridLayout.addWidget(self.blockingModelsComboBox, 1, 1)
        stagingGroupBoxGridLayout.addWidget(nameLabel, 2, 0)
        stagingGroupBoxGridLayout.addWidget(self.nameEdit, 2, 1)
        stagingGroupBoxGridLayout.addWidget(colorLabel, 3, 0)
        stagingGroupBoxGridLayout.addWidget(self.colorDialog, 3, 1)
        stagingGroupBoxGridLayout.addWidget(self.createButton, 4, 0, 1, 2)
        preShootGroupBoxGridLayout.addWidget(basePrevizButton, 0, 0)
        preShootGroupBoxGridLayout.addWidget(snapPropButton, 1, 0)
        preShootGroupBoxGridLayout.addWidget(redMarkupButton, 2, 0)
        preShootGroupBoxGridLayout.addWidget(dimensionsButton, 3, 0)
        preShootGroupBoxGridLayout.addWidget(multiCamButton, 4, 0)
        preShootGroupBoxGridLayout.addWidget(leadInOutCamButton, 5, 0)
        preShootGroupBoxGridLayout.addWidget(resetCamFarPlaneButton, 6, 0)
        preShootGroupBoxGridLayout.addWidget(visibilityCheckBox, 7, 0)
        preShootGroupBoxGridLayout.addItem(preshootSpacerItem, 8, 0)

        # set groupBox layout
        stagingGroupBox.setLayout(stagingGroupBoxGridLayout)
        preShootGroupBox.setLayout(preShootGroupBoxGridLayout)

        # add to layout
        mainLayout.addWidget(stagingGroupBox, 0, 0)
        mainLayout.addWidget(preShootGroupBox, 0, 1)
        mainLayout.addItem(centerSpacerItem, 0, 1)

        # set layout
        self.setLayout(mainLayout)

        # event callbacks
        self.selectComboBox.currentIndexChanged.connect(self.populateBlockingModelsList)
        self.createButton.pressed.connect(self._handleCreateModel)
        basePrevizButton.pressed.connect(self._handleAddBasePreviz)
        snapPropButton.pressed.connect(self._handleSnapPropsToSet)
        redMarkupButton.pressed.connect(self._handleRedMarkup)
        dimensionsButton.pressed.connect(self._handleDimensionOfSelected)
        multiCamButton.pressed.connect(MocapCommands.OpenMultiCameraViewer)
        leadInOutCamButton.pressed.connect(self._handleLeadCams)
        resetCamFarPlaneButton.pressed.connect(self._handleResetCamFarPlane)
        visibilityCheckBox.pressed.connect(self._handleCamLabelVisibility)
