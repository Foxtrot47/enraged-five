import socket


from RS import Databases
from RS.Tools.VideoTranscoder import VideoUtils

try:
    from RS.Core import DatabaseConnection
except ImportError:
    import DatabaseConnection

import platform, os, datetime
from collections import OrderedDict

DB = Databases.NYC.FaceAudioMocap()


class StageDoesNotExist(Exception):
    pass


def EnableDevServer(enable_Dev=False):
    """
    Allows us to use the dev server for testing or set to Rel Server
    :param status:
    :return:
    """
    global DB
    if enable_Dev:
        DB = Databases.NYCDEV.FaceAudioMocap()
    else:
        DB = Databases.NYC.FaceAudioMocap()


def AssignValidJobs(statusID=0):
    """

    Get a list of Pending jobs from the Database. Defaults to 0
    Also need to remove any duplicates

    :param statusID: satus ID of JOb to get, i.e pending, completed, transcoding etc
    :param devmode: set 1 if jobs are to only be used for testing
    :return:    Dictionary of FileID's with the video full Path
    """

    sqlcmd = ("exec [GetJobFilesByStatusID]"
              " {0}").format(statusID)
    result = _runQuery_(sqlcmd)

    if result == -1:
        return dict()

    # Construct a list of paths to return
    d = dict()
    for idx in result:
        d[idx[0]] = {
            "file":os.path.join(idx[1], idx[2]),
            "type":idx[3],
            "stageID":idx[4],
            "trialID":idx[5],
        }

    _sortedDict = OrderedDict(sorted(d.items(), key=lambda t: t[1]))
    return _sortedDict


def AddJobFileToJobTask(fileid, jobid, mediaInfo):
    """

    :param fileid:
    :param jobid:
    :return:
    """
    # Video Metadata Defaults
    timecode = '00:00:00:00'
    audioCodec = 'NULL'
    channels = -1
    videoCodec = 'NULL'
    frameRate = -1
    sourceFrames = -1
    duration = 'NULL'
    height = -1
    width = -1

    # Check if correct variable type has been passed in
    if type(mediaInfo).__name__ == 'GetVideoInfo':

        # Get teh media Info data
        if mediaInfo.streams.has_key('video'):
            videoCodec = mediaInfo.streams['video'].codecname
            height = mediaInfo.streams['video'].height
            width = mediaInfo.streams['video'].width
            frameRate = mediaInfo.streams['video'].frame_rate
            duration = str(datetime.timedelta(seconds=float(mediaInfo.streams['video'].duration)))
            sourceFrames = mediaInfo.streams['video'].frames

        if mediaInfo.streams.has_key('audio'):
            audioCodec = mediaInfo.streams['audio'].codecname
            channels = mediaInfo.streams['audio'].channels

        if mediaInfo.streams.has_key('data'):
            timecode = mediaInfo.streams['data'].timecode

    sqlcmd = ("exec [AddJobFileToTask]"
              " '{0}', '{1}', '{2}', {3}, '{4}', {5}, {6}, '{7}', '{8}', '{9}' ").format(jobid,
                                                                                        fileid,
                                                                                        duration,
                                                                                        sourceFrames,
                                                                                        timecode,
                                                                                        height,
                                                                                        width,
                                                                                        videoCodec,
                                                                                        audioCodec,
                                                                                        frameRate
                                                                                        )

    result = _runQuery_(sqlcmd, fetchall=False)


def GetJobsCountByStatus(statusID):
    """
    Get number of jobs
    :return:
    """
    sqlcmd = ("exec [GetJobCountByStatusID]"
              " {0}").format(statusID)
    result = _runQuery_(sqlcmd)
    if len(result) > 0:
        return result[0][0]
    else:
        return -1


def GetJobID(date, StageID):
    """
    Return the Job ID for the day/Stage.
    We may need to expand this for the studio and Machine host if needed
    :param date:    2015-10-13 (Year, Month, Day format)
    :param StageID: i.e -1 LocalTest, 0: PreViz, ....
    :return: JobID or 0
    """
    sqlcmd = ("exec [GetTranscodeJobIDFromDate]"
              " '{0}', {1}").format(date, StageID)

    result = _runQuery_(sqlcmd)
    if len(result) > 0:
        return result[0][0]
    else:
        return -1


def GetAllStatusIDs():
    """
    Returns a list of all the available status ID
    :return:
    """
    sqlcmd = ("exec [GetStatusIDs]")
    return  _runQuery_(sqlcmd)


def GetAllStages():
    """

    :return:
    """
    sqlcmd = ("exec [GetCurrentStages]")
    return  _runQuery_(sqlcmd)


def GetStageName(StudioID):
    """
    Returns the Stage Name from the Stage ID

    :param StageID:
    :return: Stage Name, if not valid will return empty
    """
    sqlcmd = ("exec [GetStageNameFromID]"
              " {0}").format(StudioID)
    result = _runQuery_(sqlcmd)
    if len(result) > 0:
        return result[0][0]
    else:
        return -1


def CreateJobsFromFolder(folderPath, studioID, recursive=False, trialID=0):
    """
    Create a load of jobs from files found in a passed in folder path
    :param folderPath:
    :param stageID:
    :param recursive:
    :param trialID:
    :return:
    """
    if recursive:
        for (path, dirs, files) in os.walk(folderPath):
            for name in files:
                fileName = os.path.join(path, name)
                CreateJob(fileName, studioID, trialID=trialID)
    else:
        for name in os.listdir(folderPath):
            fileName = os.path.join(folderPath, name)
            if os.path.isfile(fileName):
                CreateJob(fileName, studioID, trialID=trialID)


def DeleteJob(jobId):
    """
    Will delete teh job and the associated job files in the database

    :param jobId:
    :return: Fuck all
    """
    sqlcmd = ("exec [DeleteJob]"
              " '{0}'").format(jobId)
    result = _runQuery_(sqlcmd, fetchall=False)


def CreateJob(filepath, studioID=-1, typeID=0, trialID=0):
    """
    Add a new file to be processed
    This is called as teh
    :param filepath:
    :param stageID: -1 LocalTest, 0 = PreViz, 1 = Booth
    :param typeID:  0=Face, 1=Reference, 2=Unknown, 3=NoLTC
    :param trialID:
    :return:
    """
    if GetStageName(studioID) == -1:
        print ("'{0}' is not a valid Studio Index".format(studioID))
        for idx in GetAllStages(): print "{0}:\t{1}".format(idx[2], idx[3])
        return
        # raise StageDoesNotExist("'{0}' is not a valid Stage Index".format(stageID))

    if not os.path.exists(filepath):
        print ("File {0} does not exist").format(filepath)
        return

    fileName, dirPath = VideoUtils.dirFile(filepath)
    dirPath = VideoUtils.resolveFilerPath(dirPath, VideoUtils.WINDOWS_CONST)

    # Constrcut our SQL stored procedure cmd
    sqlcmd = ("exec [AddNewJobFile]"
                 " {0}, '{1}', '{2}', '{3}', '{4}', '{5}' ").format(
                studioID,
                dirPath,
                fileName,
                typeID,
                trialID,
                socket.gethostname(),
    )

    result = _runQuery_(sqlcmd)

    if result != 0 and result != None:
        return result
    else:
        print "Error: Parameters format is incorrect"
        return result


def CreateJobTask(projectID, machineName, files_count):
    """
    Creates a new Job task on the Databsae, we then would assign files to the job to be processed
    This is ran fromt eh JOb monitor which will look for files adn then create a new jobTask
    :param projectID:
    :param machineName:
    :param filescount:
    :return:
    """
    sqlcmd = ("exec [AddNewJobTask]"
                 "  {0}, '{1}', {2} ").format(
                projectID,
                machineName,
                files_count
    )

    result = _runQuery_(sqlcmd)

    if result != 0 and result != None:
        return result[0][0]


def UpdateJobMetrics(jobTask):
    """
    Parse in a jobtask and upload the metrics
    :param jobTask:
    :return:
    """
    sqlcmd = ("exec [UpdateTranscodeJobMetrics]"
                 " '{0}', {1}, '{2}', '{3}', '{4}'").format(
                                            jobTask.jobTaskID,
                                            str(len(jobTask.jobfiles)),
                                            jobTask.convertTime,
                                            jobTask.copyTime,
                                            jobTask.p4Time



    )
    return _runQuery_(sqlcmd, fetchall=False)


def UpdateJobStatus(jobID, statusID):
    """
    Set all the files under a JobId to the new Status

    :param jobID:       [GUID jib ID]
    :param statusID:    [0 = Pending, 1 = Transcoding 2 = Completed]
    :return:
    """
    statusList = dict(GetAllStatusIDs())
    if statusID in statusList.keys():
        sqlcmd = ("exec [UpdateTranscodeJobIDStatus]"
                     " '{0}', {1}").format(
                    jobID,
                    statusID
        )
        return _runQuery_(sqlcmd, fetchall=False)
    else:
        print "ERROR: Status Index not valid!\n{0}".format(statusList)
        return None


def GetFileTimecode(fileID):
    """

    :param fileID:
    :return:
    """
    sqlcmd = ("exec [GetTranscodeTimecode]"
                     " '{0}' ").format(
                    fileID

        )

    result = _runQuery_(sqlcmd)

    if result != None:
        return result[0][0]


def UpdateFileTimecode(fileID, timecode):
    """

    :param fileID:
    :param statusID:
    :return:
    """
    sqlcmd = ("exec [UpdateTranscodeFileTimecode]"
                     " '{0}', '{1}' ").format(
                    fileID,
                    timecode
        )
    return _runQuery_(sqlcmd, fetchall=False)


def UpdateFilenameTimecode(filename, timecode):
    """

    :param fileID:
    :param statusID:
    :return:
    """
    sqlcmd = ("exec [UpdateTranscodeTimeCodeByFilename]"
                     " '{0}', '{1}' ").format(
                    filename,
                    timecode
        )
    return _runQuery_(sqlcmd, fetchall=False)


def GetJobsByDate(date):
    """
    Checks to see if the filename exists in teh database
    :param filename:
    :return:
    """
    sqlcmd = ("exec [GetJobStatusFromeDate]"
                     " '{0}' ").format(
                    date

        )

    return _runQuery_(sqlcmd, fetchall=True)


def UpdateJobFileStatus(fileID, statusID):
    """

    :param fileID:
    :param statusID:
    :return:
    """
    statusList = dict(GetAllStatusIDs())
    if statusID in statusList.keys():
        sqlcmd = ("exec [UpdateJobFileIDStatus]"
                     " '{0}', {1}").format(
                    fileID,
                    statusID
        )
        return _runQuery_(sqlcmd, fetchall=False)
    else:
        print "ERROR: Status Index not valid!\n{0}".format(statusList)
        return None


def UpdateFileStatus(fileID, statusID):
    """
    Update a single FileID Status
    :param guid:
    :param statusID:
    :return:
    """
    statusList = dict(GetAllStatusIDs())
    if statusID in statusList.keys():
        sqlcmd = ("exec [UpdateTranscodeFileIDStatus]"
                     " '{0}', {1}").format(
                    fileID,
                    statusID
        )
        return _runQuery_(sqlcmd, fetchall=False)
    else:
        print "ERROR: Status Index not valid!\n{0}".format(statusList)
        return None


def UpdateJobTaskCompletion(jobID):
    """
`   Sets the Task Job completion TIme
    :param jobID:
    :return:
    """
    sqlcmd = ("exec [UpdateJobTaskCompletion]"
                     "'{0}'").format(
                    jobID

        )

    return _runQuery_(sqlcmd, fetchall=False)


def UpdateMachineStatus(statusID, machineName=platform.node(),):
    """

    :param machineName:
    :param statusID:
    :return:
    """
    sqlcmd = ("exec [UpdateMachineStatus]"
                     " '{0}', {1}").format(
                    machineName,
                    statusID
        )
    return _runQuery_(sqlcmd, fetchall=False)


def RemoveJobsDaysOld(daysOld, statusID=0):
    """
    Remove jobs form the database that are a certain number of days old
    :param daysOld:
    :param statusID:
    :return:
    """
    sqlcmd = ("exec [RemoveJobsByDaysOld]"
                     " '{0}', {1}").format(
                    daysOld,
                    statusID
        )
    return _runQuery_(sqlcmd, fetchall=False)


def _runQuery_(sqlcmd, fetchall=True):
    """
    Private

    :param sqlcmd:
    :return:
    """
    try:
        return DB.ExecuteQuery(sqlcmd, fetchall)
    except Exception, e:
        print e
        return []

