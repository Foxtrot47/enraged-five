"""
Utils
"""
__author__ = 'mharrison-ball'

import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import platform
import tempfile
import subprocess
import uuid
import socket
import re
from xml.dom import minidom

from RS.Core.Video import videoManager

LINUX_CONST = "Linux"
WINDOWS_CONST = "Windows"


def strToBool(inputStr):
    '''

    :param inputStr:
    :return:
    '''
    if inputStr.lower() == 'true':
         return True
    elif inputStr.lower() == 'false':
         return False
    else:
         raise ValueError


def dirFile(path):
    """
    Return dir and filename
    :param path:
    :return:
    """
    head, tail = os.path.split(path)
    return tail, head


def returnFrames(filePath):
    '''

    :param filePath:
    :return:
    '''
    hours, minutes, seconds, frames = videoManager.VideoManager.getVideoLengthAsTuple(filePath)
    hoursInSecs = (hours * 60) * 60
    minsInSecs = minutes * 60
    totalSecs = seconds + minsInSecs + hoursInSecs


def _sendEmail(buildGraph, emailList, startTime, endTime, videoTransactions):
    """

    :param buildGraph:
    :param emailList:
    :param startTime:
    :param endTime:
    :param videoTransactions:
    :return:
    """
    if not len(videoTransactions.files) > 0: return
    
    from RS import ProjectData
    from RS import Config
    templateFile = os.path.join(Config.Tool.Path.TechArt, "etc", "config", "notificationTemplates", "TranscodingTemplate.html")

    toStr = ", ".join(emailList)

    # Format the exception for email.
    message = MIMEMultipart('alternative')
    message['Subject'] = 'Transcoding Job Finished Report: {0}'.format(videoTransactions.inputPath)
    message['To'] = toStr
    message['From'] = Config.User.Email

    jobTime = endTime - startTime
    days, hours, minutes = (jobTime.days, jobTime.seconds // 3600, jobTime.seconds // 60 % 60)
    seconds = jobTime.seconds - ((minutes + (hours * 60)) * 60)

    with open(buildGraph.logFilePath, "r") as logFile:
        logText = logFile.read()

    resultStringRegex = re.compile("Build: \d succeeded, \d failed, \d up-to-date, \d skipped\n\Z")
    results = resultStringRegex.findall(logText)
    if len(results) == 1:
        results = results[0].replace("\n", "")
    else:
        results = "No Incredbuild Log results found"

    geenColour = "rgb(3,126,0)"
    blueColour = "rgb(55,112,157)"

    statGraphTemplate = "<td style=\"background-color: #COLOUR#\"></td>"
    statTemplate = """
    <tr>
        <td> #FILENAME# - #COMPRESSIONPERCENT#% -  #COMPRESSEDSIZE#MB/#UNCOMPRESSEDSIZE#Mb</td>
    </tr>
    <tr>
        <td>
        <table cellpadding="5" cellspacing="0"  width="100%">
        <tr>
            #STATGRAPH#
        </tr>
        </table>
        </td>
    </tr>
    """
    statText = ""
    convertedFiles = 0
    uncompressFileSize = 0
    compressedFileSize = 0
    for fileTrans in videoTransactions.files:
        if os.path.isfile(fileTrans.inputFilename) and os.path.isfile(fileTrans.outputFilename):
            convertedFiles += 1
            uncompressedSize = round((os.path.getsize(fileTrans.inputFilename) / 1024.0) / 1024.0, 2)
            compressedSize = round((os.path.getsize(fileTrans.outputFilename) / 1024.0) / 1024.0, 2)

            compPercent = int(((float(compressedSize)) / uncompressedSize) * 100)
            # Copy Text so we leave the template alone
            fileStatText = str(statTemplate)
            fileStatText = fileStatText.replace("#FILENAME#", os.path.basename(fileTrans.inputFilename))
            fileStatText = fileStatText.replace("#COMPRESSIONPERCENT#", str(compPercent))
            fileStatText = fileStatText.replace("#COMPRESSEDSIZE#", str(compressedSize))
            fileStatText = fileStatText.replace("#UNCOMPRESSEDSIZE#", str(uncompressedSize))

            statGraph = ""
            for idx in xrange(100):
                graphPoint = str(statGraphTemplate)
                colour = geenColour
                if idx >= compPercent:
                    colour = blueColour
                graphPoint = graphPoint.replace("#COLOUR#", colour)
                statGraph += graphPoint

            fileStatText = fileStatText.replace("#STATGRAPH#", statGraph)
            statText += fileStatText
            uncompressFileSize += uncompressedSize
            compressedFileSize += compressedSize

    with open(templateFile, 'r') as handler:
        body = handler.read()
        body = body.replace("#PROJECT#", ProjectData.data.Project())
        body = body.replace("#USERNAME#", Config.User.Name)
        body = body.replace("#TIMESTAMP#", startTime.strftime('%d/%m/%Y @ %H:%M:%S'))
        body = body.replace("#JOBTIME#", "{0}d {1}h {2}m {3}s".format(days, hours, minutes, seconds))
        body = body.replace("#FILESPROCESSED#", str(convertedFiles))
        body = body.replace("#UNCOMPRESSEDFILESIZE#", str(uncompressFileSize))
        body = body.replace("#COMPRESSEDFILESIZE#", str(compressedFileSize))
        body = body.replace("#INPUTDIR#", videoTransactions.inputPath)
        body = body.replace("#OUTPUTDIR#", videoTransactions.outputPath)
        body = body.replace("#SEGMENTSIZE#", str(videoTransactions.segmentSize) or "0")
        body = body.replace("#COMPSTATS#", statText)
        body = body.replace("#JOBRESULTS#", results)
        body = body.replace("#ERRORLIST#", "<br>".join([str(msg) for msg in videoTransactions.errors] or ["No Errors"]))
        body = body.replace("#BUILDCONFIG#", videoTransactions.config)

    htmlBody = MIMEText(body, 'html')
    message.attach(htmlBody)

    for attachment in [buildGraph.logFilePath, buildGraph.graphFilePath, buildGraph.outputFilePath]:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(attachment, 'r').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename={0}'.format(os.path.basename(attachment)))
        message.attach(part)
    '''
        Set to use c# libs to send emails until we fix the smtp issues.
    '''
    server = smtplib.SMTP(Config.User.ExchangeServer)
    try:
        server.sendmail(Config.User.Email, emailList, message.as_string())
    except Exception, e:
        print e
    server.quit()
    
    
def resolveFilerPath(filePath, platformOverride=None):
    '''
    takes in file path and resolves for host system
    
    EXAMPLE: 
        resloveFilerPath("/media/hdfacevideo/prores/dbtest/FUS1_INT.mov", platformOverride="Windows")
            #returns N:/rsglic/hdfacevideo/prores/dbtest/FUS1_INT.mov
    args: 
        filePath (str): input file path

    kwargs:
    	platformOverride (str): "Windows" or "Linux"
    	
    return:
        resolved host platform filepath
    '''
    compPlatform = platformOverride or platform.system()

    xmldoc = minidom.parse(os.path.join(os.path.dirname(__file__), "filerMappings.xml"))
    root = xmldoc.getElementsByTagName("mapping")[0]
    returnFilePath = str(filePath)

    for mapping in [pt for pt in root.childNodes if not isinstance(pt, minidom.Text)]:
        mappingDict = dict(mapping.attributes.items())

        # Linux Op
        if compPlatform == LINUX_CONST:
            if mappingDict["windowsPath"] in filePath:
                returnFilePath = filePath.replace(mappingDict["windowsPath"], mappingDict["linuxPath"])

        # Windows Op
        elif compPlatform == WINDOWS_CONST:
            if mappingDict["linuxPath"] in filePath:
                returnFilePath = filePath.replace(mappingDict["linuxPath"], mappingDict["windowsPath"])
    return str(returnFilePath)
