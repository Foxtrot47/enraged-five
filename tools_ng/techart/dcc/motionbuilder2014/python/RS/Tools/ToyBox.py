###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
##
## Script Name: Core\ToyBox.py
## Written And Maintained By: Kristine Middlemiss
## Contributors: David Bailey
## Description: Creates Constraints That Will Send Mover Nodes On Characters, Vehicles And Props To The ToyBox
##
## Rules: Definitions: Prefixed with "rs_"
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
###############################################################################################################

from pyfbsdk import *

import RS.Globals as glo
import RS.Utils.Scene

# This can be removed at the beginning of the production because then the vehicles will be originally setup properly.
import RS.Core.AssetSetup.Vehicles as rsAssetVehicles
# This can be removed at the beginning of the production because then the props will be originally setup properly.
import RS.Core.AssetSetup.Props as rsAssetProps
import RS.Config

######################################################################################
######################################################################################
def IdleCallback(pObject, pEventName):
    '''
    Callback function to make sure ToyBoxOverRideNull object and control rig are never true. This is a workaround solution for OnConnectionDataNotify not setting ToyBoxOverRideNull to false.

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    lOverRideNull = FBFindModelByLabelName("ToyBoxOverRideNull")
    if lOverRideNull:
        lPropList = lOverRideNull.PropertyList
        for lChar in FBSystem().Scene.Characters:
            if lChar:
                lFullName = lChar.LongName
                lNameList = lFullName.split(":")
                for lProp in lPropList:
                    if lProp.Name == lNameList[0]+":mover_Control_OverRide_Bool":
                        if lProp.Data == True and lChar.Active == True:
                            lProp.Data = False

######################################################################################
######################################################################################
def OnConnectionDataNotify(control, event):
    '''
    Callback function on the ToyBoxOverRideNull object, so that if you are going to send something to the toybox and it has it's control rig active, it asks you if you want to de-activate or cancel

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    lOverRideNull = FBFindModelByLabelName("ToyBoxOverRideNull")
    if lOverRideNull and event.Plug.Is(FBProperty_TypeInfo()):
        iControl = event.Plug.Name
        lProp = lOverRideNull.PropertyList.Find(iControl)
        if lProp:
            if lProp.Data == True:
                try:
                    lList = iControl.split(":")
                    if "mover_Control_OverRide_Bool" == lList[1]: # Yes our property is be affected.
                        for lChar in glo.gCharacters:
                            if lList[0] == lChar.Name:
                                lActive = lChar.PropertyList.Find("Active")
                                if lActive.Data == True: # Yes, we need ask the user if the want to de-actiavet it for the toybox to work
                                    val = FBMessageBox( "Warning: De-activate Control Rig onconnection?", "The control rig is active on %s, you are about to de-activate it, is that okay?" % lChar.Name, "Yes", "Cancel" )
                                    if val == 1: # Okay, they said they want to de-activate the control rig
                                        lActive.Data = False
                                    elif val == 2:
                                        lProp.Data = False #Why are you not working :( I think it's because we are still in the property that triggered the callback
                except:
                    pass

######################################################################################
######################################################################################
def FindAnimationNode( pParent, pName ):
    '''
    Helper Function for finding an animationNode

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    lResult = None
    for lNode in pParent.Nodes:
        if lNode.Name == pName:
            lResult = lNode
            break
    return lResult

######################################################################################
######################################################################################
def rs_CreateToyBox():
    '''
    This Definition Creates A ToyBox In Scene

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    lToyBox = FBFindModelByLabelName("ToyBox")
    if not lToyBox:

        lToyBox = FBModelCube("ToyBox")
        lToyBox.Show = True
        lToyBox.Scaling = FBVector3d(100,100,100)

        lToyBoxMaterial = FBMaterial("ToyBox_Material")
        lToyBoxMaterial.Diffuse = FBColor(1,0,1)
        lToyBoxMaterial.Opacity = 0.50
        lToyBox.Materials.append(lToyBoxMaterial)

        lToyBoxShader = FBShaderManager().CreateShader("FlatShader")
        lToyBoxShader.Name = "ToyBox_Shader"
        lToyBoxShader.PropertyList.Find("Transparency Type").Data = 1
        lToyBox.Shaders.append(lToyBoxShader)
        lToyBox.ShadingMode = FBModelShadingMode.kFBModelShadingAll

        lHandle = FBHandle("ToyBox_Handle")
        lHandle.Follow.append(lToyBox)
        lHandle.Manipulate.append(lToyBox)
        lHandle.PropertyList.Find("2D Display Color").Data = FBColor(1,0,1)
        lHandle.PropertyList.Find("Translation Offset").Data = FBVector3d(0,150,0)
        lHandle.PropertyList.Find("Size").Data = 40
        lHandle.PropertyList.Find("Label").Data = "ToyBox"

        lToyBoxGroup = FBGroup("ToyBox")
        lToyBoxGroup.ConnectSrc(lToyBox)
        lToyBoxGroup.Show = True
        lToyBoxGroup.Pickable = False
        lToyBoxGroup.Transformable = False

######################################################################################
######################################################################################
def rs_CreateToyBoxRelation( pToyBoxMarker, pMover, pCamera, pAxisNull, pToyBoxOverRide ):
    '''
    Creates ToyBox Relation Constraint

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    if pToyBoxMarker and pMover and pCamera and pAxisNull and pToyBoxOverRide:
        lRelConstraint = FBConstraintRelation(pToyBoxMarker.LongName + "_ToyBox")
        lRelConstraint.Active = False

        lCameraBox = lRelConstraint.SetAsSource(pCamera)
        lAxisNullBox = lRelConstraint.SetAsSource(pAxisNull)
        lMoverBox = lRelConstraint.SetAsSource(pMover)
        lOverRideBox = lRelConstraint.SetAsSource(pToyBoxOverRide)
        lToyBoxMarkerBox = lRelConstraint.ConstrainObject(pToyBoxMarker)
        lSubtract = lRelConstraint.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
        lSubtract1 = lRelConstraint.CreateFunctionBox("Vector", "Subtract (V1 - V2)")
        lDotProduct = lRelConstraint.CreateFunctionBox("Vector", "Dot Product (V1.V2)")
        lLength = lRelConstraint.CreateFunctionBox("Vector", "Length")
        lLength1 = lRelConstraint.CreateFunctionBox("Vector", "Length")
        lMultiply = lRelConstraint.CreateFunctionBox("Number", "Multiply (a x b)")
        lDivide = lRelConstraint.CreateFunctionBox("Number", "Divide (a/b)")
        lAdd = lRelConstraint.CreateFunctionBox("Number", "Add (a + b)")
        lArcCos = lRelConstraint.CreateFunctionBox("Number", "arccos(a)")
        lIsGreaterOrEqual = lRelConstraint.CreateFunctionBox("Number", "Is Greater or Equal (a >= b)")
        lIfCondThenAElseB = lRelConstraint.CreateFunctionBox("Number", "IF Cond Then A Else B")

        lRelConstraint.SetBoxPosition( lCameraBox, 0, -200)
        lRelConstraint.SetBoxPosition( lAxisNullBox, 0, -400)
        lRelConstraint.SetBoxPosition( lMoverBox, 0, 0)
        lRelConstraint.SetBoxPosition( lOverRideBox, 0, -600)
        lRelConstraint.SetBoxPosition( lToyBoxMarkerBox, 3200, -200)
        lRelConstraint.SetBoxPosition( lSubtract, 400, 0)
        lRelConstraint.SetBoxPosition( lSubtract1, 400, 200)
        lRelConstraint.SetBoxPosition( lDotProduct, 800, 0)
        lRelConstraint.SetBoxPosition( lLength, 800, 200)
        lRelConstraint.SetBoxPosition( lLength1, 800, 400)
        lRelConstraint.SetBoxPosition( lMultiply, 1200, 200)
        lRelConstraint.SetBoxPosition( lDivide, 1600, 0)
        lRelConstraint.SetBoxPosition( lAdd, 1600, -400)
        lRelConstraint.SetBoxPosition( lArcCos, 2000, 0)
        lRelConstraint.SetBoxPosition( lIsGreaterOrEqual, 2400, -200)
        lRelConstraint.SetBoxPosition( lIfCondThenAElseB, 2800, -200)

        RS.Utils.Scene.ConnectBox( lSubtract, "Result", lDotProduct, 'V1')
        RS.Utils.Scene.ConnectBox( lSubtract1, "Result", lDotProduct, 'V2')
        RS.Utils.Scene.ConnectBox( lSubtract, "Result", lLength, 'Vector')
        RS.Utils.Scene.ConnectBox( lSubtract1, "Result", lLength1, 'Vector')
        RS.Utils.Scene.ConnectBox( lLength, "Result", lMultiply, 'a')
        RS.Utils.Scene.ConnectBox( lLength1, "Result", lMultiply, 'b')
        RS.Utils.Scene.ConnectBox( lDotProduct, "Result", lDivide, 'a')
        RS.Utils.Scene.ConnectBox( lMultiply, "Result", lDivide, 'b')
        RS.Utils.Scene.ConnectBox( lDivide, "Result", lArcCos, 'a')
        RS.Utils.Scene.ConnectBox( lArcCos, "Result", lIsGreaterOrEqual, 'a')
        RS.Utils.Scene.ConnectBox( lAdd, "Result", lIsGreaterOrEqual, 'b')
        RS.Utils.Scene.ConnectBox( lCameraBox, "Translation", lSubtract, 'V1')
        RS.Utils.Scene.ConnectBox( lCameraBox, "Translation", lSubtract1, 'V1')
        RS.Utils.Scene.ConnectBox( lMoverBox, "Translation", lSubtract1, 'V2')
        RS.Utils.Scene.ConnectBox( lAxisNullBox, "Translation", lSubtract, 'V2')
        RS.Utils.Scene.ConnectBox( lCameraBox, "FieldOfView", lAdd, 'a')
        RS.Utils.Scene.ConnectBox( lIsGreaterOrEqual, "Result", lIfCondThenAElseB, 'b')
        RS.Utils.Scene.ConnectBox( lOverRideBox, (pToyBoxMarker.LongName + "_FOV_Increase"), lAdd, 'b')
        RS.Utils.Scene.ConnectBox( lOverRideBox, (pToyBoxMarker.LongName + "_OverRide_Bool"), lIfCondThenAElseB, 'a')
        RS.Utils.Scene.ConnectBox( lOverRideBox, (pToyBoxMarker.LongName + "_OverRide_Bool"), lIfCondThenAElseB, 'Cond')
        RS.Utils.Scene.ConnectBox( lIfCondThenAElseB, "Result", lToyBoxMarkerBox, 'Send to ToyBox')
        lRelConstraint.Active = True
        return lRelConstraint
    else:
        FBMessageBox("rs_CreateToyBoxRelation", "The ToyBox Constraint was not created. ", "OK")
        return

######################################################################################
######################################################################################
def rs_CreateSendToToyBoxRelation( pToyBox, pMover, pCharModel ):
    '''
    Creates SentToToyBox Relation Constraint

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    if pToyBox and pMover and pCharModel:
        lRelConstraint = FBConstraintRelation(pCharModel.LongName + "_SendToToyBox")

        lToyBoxBox = lRelConstraint.SetAsSource(pToyBox)
        lMoverControlBox = lRelConstraint.SetAsSource(pCharModel)
        lMoverBox = lRelConstraint.SetAsSource(pMover)
        lMoverBox.UseGlobalTransforms = True
        llMoverControInBox = lRelConstraint.ConstrainObject(pCharModel)
        lIfCondThenAElseB = lRelConstraint.CreateFunctionBox("Vector", "IF Cond Then A Else B")

        lRelConstraint.SetBoxPosition( lToyBoxBox, 0, 0)
        lRelConstraint.SetBoxPosition( lMoverControlBox, 0, 200)
        lRelConstraint.SetBoxPosition( lMoverBox, 0, 400)
        lRelConstraint.SetBoxPosition( llMoverControInBox, 800, 200)
        lRelConstraint.SetBoxPosition( lIfCondThenAElseB, 400, 200)

        RS.Utils.Scene.ConnectBox( lIfCondThenAElseB, "Result", llMoverControInBox, 'Translation')
        RS.Utils.Scene.ConnectBox( lMoverBox, "Translation", lIfCondThenAElseB, 'b')
        RS.Utils.Scene.ConnectBox( lToyBoxBox, "Translation", lIfCondThenAElseB, 'a')
        RS.Utils.Scene.ConnectBox( lMoverControlBox, "Send to ToyBox", lIfCondThenAElseB, 'Cond')
        lRelConstraint.Active = True
        return lRelConstraint

    else:
        FBMessageBox("rs_CreateSendToToyBoxRelations", "The Send To ToyBox Constraint was not created. ", "OK")
        return

######################################################################################
######################################################################################
def rs_ExpressionOnOff( pControl, pEvent):
    '''
    Turns On And Off The Expressions In ToyBoxFolder

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    buttonStatus = "On"

    if pControl.Caption == "Turn All ToyBox Constraints Off":
        buttonStatus = "Off"

    if buttonStatus == "On":
        for iFolder in glo.gFolders:
            if iFolder.Name == "ToyBoxFolder":
                for iConstraint in iFolder.Items:
                    iConstraint.Active = True
        pControl.Caption = "Turn All ToyBox Constraints Off"
    else:
        for iFolder in glo.gFolders:
            if iFolder.Name == "ToyBoxFolder":
                for iConstraint in iFolder.Items:
                    iConstraint.Active = False
        pControl.Caption = "Turn All ToyBox Constraints On"


######################################################################################
######################################################################################
def rs_MakeToyBoxRelations( pControl, pEvent, toyboxRefresh=False):
    '''
    Sets Up Selected Asset With ToyBox Constraints

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    if pControl.Name == "":
        FBMessageBox("rs_MakeToyBoxRelations", "Nothing was selected", "OK")
        return

    refresh = False
    lMover = None
    lToyBoxMarker = None

    lExportCamera = FBFindModelByLabelName("ExportCamera")
    lToyBoxOverRide = FBFindModelByLabelName("ToyBoxOverRideNull")
    lAxisNull = FBFindModelByLabelName("AxisNull")

    if lExportCamera:
        if not lToyBoxOverRide:
            lToyBoxOverRide = FBModelNull("ToyBoxOverRideNull")
            lOverRideGroup = FBGroup("ToyBoxOverRideGroup")
            lOverRideGroup.ConnectSrc(lToyBoxOverRide)
            lOverRideGroup.Show = False
            lOverRideGroup.Pickable = False
            lOverRideGroup.Transformable = False

        if not lAxisNull:
            lAxisNull = FBModelNull("AxisNull")
            lAxisNull.Parent = lExportCamera
            lAxisNull.PropertyList.Find("Lcl Translation").Data = FBVector3d(2000, 0, 0)
        for iControl in pControl.Name.split(","):
            iControl = iControl.replace("[", "", len(iControl))
            iControl = iControl.replace("]", "", len(iControl))
            iControl = iControl.replace("'", "", len(iControl))
            if iControl.startswith(" "):
                iControl = iControl[1:len(iControl)]

            #################################SPECIAL############################################################
            # This call to rs_AssetSetupVehicles can be removed at the start of next production, as all the
            # vehicles files will be set up correctly with the relation constraint called 'mover_toybox_Control'
            #################################SPECIAL############################################################
            lFound = False
            if "chassis_Control" in iControl:
                lList = iControl.split(":")
                for lConst in glo.gConstraints:
                    if lConst.LongName == lList[0]+":mover_toybox_Control":
                        lFound = True
                if lFound == False:
                    lDummy = FBFindModelByLabelName( lList[0]+":Dummy01")
                    if lDummy:
                        lMover = FBFindModelByLabelName( lList[0]+":mover")
                        if lMover:
                            lChassis = FBFindModelByLabelName(iControl)
                            if lChassis:
                                lMoverConstraint = rsAssetVehicles.rs_AssetSetupVehicleToyboxMoverControl(lList[0], lDummy, lMover, lChassis)
                                lMoverConstraint.Active = True
                            else:
                                FBMessageBox("rs_MakeToyBoxRelations", "Cannot find chassis control", "OK")
                                return
                        else:
                            FBMessageBox("rs_MakeToyBoxRelations", "Cannot find mover", "OK")
                            return
                    else:
                        FBMessageBox("rs_MakeToyBoxRelations", "Cannot find Dummy01", "OK")
                        return

            #################################SPECIAL############################################################
            # This call to rs_AssetSetupProps can be removed at the start of next production, as all the
            # props files will be set up correctly with the relation constraint called 'mover_toybox_Control'
            #################################SPECIAL############################################################
            lFound = False
            if "mover_Control" in iControl:
                lList = iControl.split(":")
                for lConst in glo.gConstraints:
                    if lConst.LongName == lList[0]+":mover_toybox_Control":
                        lFound = True
                # This is making the assumption if it has not found it then it's a prop...
                if lFound == False:
                    lDummy = FBFindModelByLabelName( lList[0]+":Dummy01")
                    if lDummy:
                        lMover = FBFindModelByLabelName( lList[0]+":mover")
                        if lMover:
                            lMover_Control = FBFindModelByLabelName(iControl)
                            if lMover_Control:
                                lMoverConstraint = rsAssetProps.AssetSetupPropToyboxMoverControl(lList[0], lDummy, lMover, lMover_Control)
                                lMoverConstraint.Active = True
                            else:
                                FBMessageBox("rs_MakeToyBoxRelations", "Cannot find mover control", "OK")
                                return
                        else:
                            FBMessageBox("rs_MakeToyBoxRelations", "Cannot find mover", "OK")
                            return
                    else:
                        FBMessageBox("rs_MakeToyBoxRelations", "Cannot find Dummy01", "OK")
                        return
            #################################SPECIAL############################################################

            if not "(DONE)" in iControl:
                refresh = True
                lControlModel = FBFindModelByLabelName(iControl)
                if lControlModel:
                    lMover = FBFindModelByLabelName(iControl.partition(":")[0] + ":mover")
                    lToyBoxMarker = FBFindModelByLabelName(iControl.partition(":")[0] + ":ToyBox Marker")
                    if lMover:
                        lToyBox = FBFindModelByLabelName("ToyBox")
                        if lToyBox == None:
                            rs_CreateToyBox()

                        # Need to create the property first before you connect it in the relation constraint.
                        if lControlModel.PropertyList.Find("Send to ToyBox") == None:
                            lToyBoxProp = lControlModel.PropertyCreate("Send to ToyBox", FBPropertyType.kFBPT_int, "Number", True, True, None)
                            lToyBoxProp.SetMin(0)
                            lToyBoxProp.SetMax(1)
                            lToyBoxProp.SetAnimated(True)

                        lSendToRelation = rs_CreateSendToToyBoxRelation(lToyBox, lMover, lControlModel)

                        # Need to create the property first before you connect it in the relation constraint.
                        if toyboxRefresh == False:
                            lProp = lToyBoxOverRide.PropertyCreate( iControl + '_OverRide_Bool', FBPropertyType.kFBPT_bool, 'Bool', True, True, None)
                            if lProp == None:
                                return
                            lProp.SetAnimated(True)

                            lProp2 = lToyBoxOverRide.PropertyCreate( iControl + '_FOV_Increase', FBPropertyType.kFBPT_double, 'Number', True, True, None)
                            if lProp2 == None:
                                return

                            lProp2.SetMin(-100)
                            lProp2.SetMax(100)
                            lProp2.Data = 20
                            lProp2.SetAnimated(True)

                        lToyBoxRelation = rs_CreateToyBoxRelation(lControlModel, lMover, lExportCamera, lAxisNull, lToyBoxOverRide)
                        if lToyBoxRelation == None or lSendToRelation == None:
                            return

                        lFoundFolder = False
                        for lToyBoxFolder in glo.gFolders:
                            if lToyBoxFolder.Name == "ToyBoxFolder":
                                lToyBoxRelation.ConnectDst(lToyBoxFolder)
                                lSendToRelation.ConnectDst(lToyBoxFolder)
                                lFoundFolder = True

                        if not lFoundFolder:
                            lToyBoxFolder = FBFolder("ToyBoxFolder", lSendToRelation)
                            lToyBoxRelation.ConnectDst(lToyBoxFolder)

                        # Callback for when the user is manipulating the ToyBoxOverRideNull  properties
                        #sys = FBSystem()
                        #sys.OnConnectionDataNotify.Add(OnConnectionDataNotify)
                        return lToyBoxRelation, lSendToRelation

                        # Idle callback to make sure the ToyBoxOverRideNull is never true when the control rig is true
                        #sys.OnUIIdle.Add(IdleCallback)

                    else:
                        FBMessageBox("rs_MakeToyBoxRelations", "Cannot find mover", "OK")
                        return
        if refresh:
            import RS.Tools.UI.ToyBox
            RS.Tools.UI.ToyBox.Run()
        else:
            FBMessageBox("rs_MakeToyBoxRelations", "Everything already has constraints created", "OK")
    else:
        FBMessageBox("rs_MakeToyBoxRelations", "Export Camera was not found, setup was not done", "OK")

######################################################################################
######################################################################################
def rs_DeleteToyBoxRelations( pControl, pEvent):
    '''
    Deletes Selected Asset With ToyBox Constraints

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    refresh = False
    if pControl.Name == "":
        FBMessageBox("rs_DeleteToyBoxRelations", "Nothing was selected", "OK")
        return

    for iControl in pControl.Name.split(","):
        iControl = iControl.replace("[", "", len(iControl))
        iControl = iControl.replace("]", "", len(iControl))
        iControl = iControl.replace("'", "", len(iControl))
        lCancel = False
        lDeleteArray = []
        if iControl.startswith(" "):
            iControl = iControl[1:len(iControl)]

        if "(DONE)" in iControl:
            iControl = iControl.replace(" (DONE)", "", len(iControl))
            lControlModel = FBFindModelByLabelName(iControl)
            lControlProp = lControlModel.PropertyList.Find("Send to ToyBox")
            if lControlProp != None:
                lControlModel.PropertyRemove(lControlProp)

            for i in range(lControlModel.GetSrcCount()):
                if lControlModel.GetSrc(i) != None:
                    if "ToyBox" in lControlModel.GetSrc(i).Name and lControlModel.GetSrc(i).ClassName() == "FBConstraintRelation":
                        lDeleteArray.append(lControlModel.GetSrc(i))

            lOverRideNull = FBFindModelByLabelName("ToyBoxOverRideNull")
            if lOverRideNull:
                lOverRideNullProp = lOverRideNull.PropertyList.Find((iControl + '_OverRide_Bool'))
                lFOVIncreaseProp = lOverRideNull.PropertyList.Find((iControl + '_FOV_Increase'))
                if lOverRideNullProp != None:
                    if lOverRideNullProp.IsAnimated():
                        lKeyCount = lOverRideNullProp.GetAnimationNode().KeyCount
                        if lKeyCount >=1:
                            val = FBMessageBox("rs_DeleteToyBoxRelations", "ToyBoxOverRidNull has animation on the property %s, do you want to delete it?" %lOverRideNullProp.Name , "Yes", "Cancel")
                            if val != 1: # Okay, they said they they do not want to delete the property
                                lCancel = True
                    if lCancel == False:
                        refresh = True
                        lOverRideNull.PropertyRemove(lOverRideNullProp)
                        map (FBComponent.FBDelete, lDeleteArray)
                if lFOVIncreaseProp != None:
                    lOverRideNull.PropertyRemove(lFOVIncreaseProp)

    for lFolder in glo.gFolders:
        if lFolder.Name == "ToyBoxFolder":
            if len(lFolder.Items) == 0:
                lFolder.FBDelete()
                lOverRideNull.FBDelete()
                sys = FBSystem()
                #sys.OnConnectionDataNotify.Remove(OnConnectionDataNotify)
    if refresh:
        import RS.Tools.UI.ToyBox
        RS.Tools.UI.ToyBox.Run()
    else:
        FBMessageBox("rs_DeleteToyBoxRelations", "Everything already has its constraints removed", "OK")


######################################################################################
######################################################################################
# This recursive function selects the children, it calls itself to inspect the whole hierarchy.
def SelectModels( pRoot):

    # Insure that we have a valid root and pattern.
    if pRoot:
        # For each children, tag it if necessary and see its own children.
        for lChild in pRoot.Children:
            for lMat in lChild.Materials:
                lList = [0.0,0.0,255.0]
                lBlue = FBColor(lList)
                lCurrentColor = lMat.Diffuse
                # WARNING: Due to a problem with the implementation of the class
                #          FBPropertyAnimatableColor, we have to create a tuple from the
                #          string representation of the BackGroundColor. This tuple is then
                #          used as a parameter in the constructor of FBColor.
                if  FBColor(eval(str(lCurrentColor))) == lBlue:
                    lMat.Diffuse = FBColor(1, 1, 1)
                else:
                    lMat.AmbientFactor = 0.0
                    lMat.Diffuse = lBlue
            # Recurse.
            SelectModels( lChild)

######################################################################################
######################################################################################
def rs_ChangeObjectBlue( pControl, pEvent):
    '''
    Turn the selected object(s) blue by changing the diffuse property

    *Arguments:*
        * ''pObject'' This is a handle to the control that triggered the callback.
        * ''pEventName'' This is a handle to the control that triggered the callback.

    *Keyword Arguments:*
        * ''keyword argument 1'' A description of the keyword argument.

    *Returns:*
        * ''None'' A description of the return value.

    '''
    if pControl.Name == "":
        FBMessageBox("rs_ChangeObjectBlue", "Nothing was selected", "OK")
        return

    for iControl in pControl.Name.split(","):
        iControl = iControl.replace("[", "", len(iControl))
        iControl = iControl.replace("]", "", len(iControl))
        iControl = iControl.replace("'", "", len(iControl))

        # Get rid of the word Done, just need the control name, don't care if the constraints are there or not to make the item blue
        if "(DONE)" in iControl:
            iControl = iControl.replace(" (DONE)", "", len(iControl))
        # Clear out the white space
        if iControl.startswith(" "):
            iControl = iControl[1:len(iControl)]

        lControlModel = FBFindModelByLabelName(iControl)
        if lControlModel:
            # Split out the name space from the mover control words
            lNamespace = iControl.split(":")[0]

            # Find valid referencing system node.
            lReferenceNullList = []
            lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()
            if lReferenceNull:
                RS.Utils.Scene.GetChildren(lReferenceNull, lReferenceNullList, "", False)
            if len(lReferenceNullList) >=1:
                for iRef in lReferenceNullList:
                    if iRef.Name == lNamespace:
                        lProp = iRef.PropertyList.Find('Reference Path')
                        if lProp:
                            lFilePath = lProp.Data
                            if lFilePath:
                                lPathParts = lFilePath.split("\\")
                                #Dissect the path so we can get the name of the file.
                                lFileName = ''
                                for lPart in lPathParts:
                                    if '.FBX' in lPart or '.fbx' in lPart:
                                        lFileName = lPart.split('.')[0]
                                        break
                                if lFileName != '':
                                    lDummy = FBFindModelByLabelName( lNamespace+":Dummy01")
                                    if lDummy:
                                        # We need to get the geometry object to turn things blue, and we can't assume the namespace is the same name as the geometry object, the geomety
                                        # object matches the name of the fbx file always.
                                        for lChild in lDummy.Children:
                                            # Cannot assume the Namespace and the object names are the same.
                                            if lChild.LongName.lower() == lNamespace.lower() + ":" + lFileName.lower():
                                                SelectModels( lChild)
                                                break

