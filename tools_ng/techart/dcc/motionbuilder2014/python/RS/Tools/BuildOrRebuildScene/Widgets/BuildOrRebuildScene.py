'''
Build or Rebuild Scene.
Util to create a new scene, extracting Reference models and mocap takes from a previz scene.
Another feature is to clean up an existing mobu file.
Can also be used to do simple range selects.

Simon Papp RSGLDS spring 2019
'''

import pyfbsdk as mobu
import os
import re

from PySide import QtGui, QtCore
import RS.Tools.UI
from RS import Config, Globals, Perforce
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Prop import Prop
from RS.Core.ReferenceSystem.Types.Set import Set
from RS.Core.ReferenceSystem.Types.Vehicle import Vehicle
from RS.Core.ReferenceSystem.Types.Character import Character

import webbrowser
import RS.Config
import RS.Utils.UserPreferences
from RS.Tools.UI import Application
from P4 import P4,P4Exception    # Import the module
import json

uiDirectory = os.path.join(RS.Config.Script.Path.Root, "RS\\Tools\\UI\\BuildOrRebuildScene\\QT\\")

RS.Tools.UI.CompileUi( uiDirectory + "BuildOrRebuildScene.ui",
                    uiDirectory + "BuildOrRebuildScene_compiled.py" )

import RS.Tools.UI.BuildOrRebuildScene.QT.BuildOrRebuildScene_compiled as BuildOrRebuildCompiled

class MainWindow(RS.Tools.UI.QtMainWindowBase, BuildOrRebuildCompiled.Ui_MainWindow):
    def __init__(self):
        # main window settings
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title = 'Build Or Rebuild Scene')
        
        self.setupUi(self)

        # setup the callbacks and UI elements
        # ~ Build Tab ~
        self.pushButton_useObjectDefaultOrigin.clicked.connect(self.useObjectDefaultOriginCallback)
        self.checkBox_useObjectDefaultOrigin.clicked.connect(self.useObjectDefaultOriginCbxCallback)
        self.checkBox_createNewScene.stateChanged.connect(self.createNewSceneCbxCallback)
        self.checkBox_mergeIntoScene.stateChanged.connect(self.mergeIntoSceneCbxCallback)
        self.checkBox_overwriteExistingScene.stateChanged.connect(self.overwriteExistingSceneCbxCallback)
        self.pushButton_newFileSearch.clicked.connect(self.searchFolderCallback)
        self.pushButton_createNewScene.clicked.connect(self.createNewSceneCallback)
        self.listWidget_sceneElements.clicked.connect(self.listWidgetCallback)
        
        self.label_headerIcon.setPixmap( uiDirectory + "BuildOrRebuild_logo.png")
        self.label_headerText.setPixmap( uiDirectory + "BuildOrRebuild_text.png")
        
        icon = QtGui.QPixmap( uiDirectory + "BuildOrRebuild_help.png")
        self.pushButton_headerIcon_Help.setIcon(icon)
        self.pushButton_headerIcon_Help.clicked.connect( self.help )

        # ~ Create Tab ~
        self.tableWidget_RangeSelects
        
        header = self.tableWidget_RangeSelects.horizontalHeader()
        header.setResizeMode(0, QtGui.QHeaderView.Stretch)
        header.setResizeMode(1, QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        
        self.spinBox_newRangeStartValue.setRange(0,1000000)
        self.spinBox_newRangeEndValue.setRange(0,1000000)

        self.tableWidget_RangeSelects.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        
        self.pushButton_createNewRange.clicked.connect(self.createNewRangeCallback)
        self.pushButton_deleteSelectedRange.clicked.connect(self.deleteSelectedRangeCallback)
        self.pushButton_newRangeStart.clicked.connect(self.newRangeStartCallback)
        self.pushButton_newRangeEnd.clicked.connect(self.newRangeEndCallback)
        
        self.checkBox_rebuildElements.clicked.connect(self.rebuildElementsCallback) # to just select all items in - rebuildElementsList

        # set some variables
        self.Manager = Manager()
        self.rangeSelectCount = 0
        self.ParentModel = None
        self.relocatorReferences = []
        self.exportModelList = [] 
        self.oldNameSpaceList = []
        
        # clear current selection
        for comp in RS.Globals.Scene.Components: 
            comp.Selected = False 
        
        # check for a reference node
        # if a reference node exists, we can populate listWidget_sceneElements
        if mobu.FBFindModelByLabelName(str("REFERENCE:Scene")):
            self.populateSceneElementsList()
        else:
            mobu.FBMessageBox( "Checking Scene...",
                            "\nNot a valid scene.\nPlease add some Reference Models and try again.",
                            "OK" ) 

    # do a check to see if we can start the export process
    def createNewSceneCallback( self ):
        errorCheck = False
        
        # quick check to see if anything is selectd in the reference list
        selectedIndexes = [x.row() for x in self.listWidget_sceneElements.selectedIndexes()]
        if not selectedIndexes:
            mobu.FBMessageBox( "Checking Selected Elements...",
                            "\nWe can't really have nothing selected to export.\nPlease Check and try again.",
                            "OK" )
            return

        if self.checkBox_createNewScene.isChecked() and self.lineEdit_newFileNameValue.text() != "": # using 'and not self.lineEdit_newFileNameValue.text()' doesn't seem to work?
            errorCheck = True
        elif self.checkBox_mergeIntoScene.isChecked() and self.lineEdit_newFileNameValue.text() != "":
            errorCheck = True
        elif self.checkBox_overwriteExistingScene.isChecked():
            errorCheck = True
        
        if errorCheck:
            errorCheckPopup = mobu.FBMessageBox( "Checking Everything is Plotted...", 
                                                "\nWe need to plot everything down on all the References we are using.\nIf your happy to proceed ress 'OK' otherwise 'Cancel'.",
                                                "OK", "Cancel" )
            if errorCheckPopup == 1:
                # need to check to see if everything exists on the depot first
                selectedIndexes = []
                selectedIndexes = [x.row() for x in self.listWidget_sceneElements.selectedIndexes()]
                if self.p4CheckLocalAssets( selectedIndexes ):
                    #self.Manager.UpdateReferences(self.Manager.GetReferenceListAll(), force=True)
                    self.createNewScene()
        else:
            if self.checkBox_createNewScene.isChecked():
                mobu.FBMessageBox( "Checking New Filename...",
                                "\nWe need to set a new filename.\nPlease Check and try again.",
                                "OK" )
            elif self.checkBox_mergeIntoScene.isChecked():
                mobu.FBMessageBox( "Checking Existing Filename...",
                                "\nWe need to set an existing filename to merge into.\nPlease Check and try again.",
                                "OK" )
        
    def createNewScene( self ):
        
        # clear current selection, as we're about to start selecting all the needed components ready to generate the new file
        self.deselectComponents()
        
        # try and make sure we have all the selected references locally, before we start the main process
        # grabbing latest references
        selectedIndexes = []
        selectedIndexes = [x.row() for x in self.listWidget_sceneElements.selectedIndexes()]
        
        self.Manager.Silent = True
        # grab all the selected references and make sure they are upadted to the lastest version
        self.Manager.UpdateReferences( self.Manager.GetReferenceByNamespace(self.listWidget_sceneElements.item(selectedIndexes[0]).text().encode("utf-8")), force=True )
          
        # check for vehicles first and plot down doors (otherwise we plot too much stuff), also identify the type of references we have, and treat them each in a specific way
        selectedItemsList = [item.text().encode("utf-8") for item in self.listWidget_sceneElements.selectedItems()] # note the '.encode("utf-8")' will give just the exact text (not a Unicode String)
        
        #selectedItemsListByType = []
        characters = []        
        props = []
        sets = []
        vehicles = []
        
        # populate lists to grab all the scenes (useful) items            
        characters = [sceneCharacter.Namespace for sceneCharacter in self.Manager.GetReferenceListByType(Character)]
        props = [sceneProp.Namespace for sceneProp in self.Manager.GetReferenceListByType(Prop)]
        sets = [sceneSet.Namespace for sceneSet in self.Manager.GetReferenceListByType(Set)]
        vehicles = [sceneVehicle.Namespace for sceneVehicle in self.Manager.GetReferenceListByType(Vehicle)]
             
        for item in selectedItemsList:
            if item in characters: # check for selected characters
                if not (item + ":mover") in self.relocatorReferences: 
                    self.relocatorReferences.append(item + ":mover")
                    # select the characters useful bits                  
                    selectionSet = []
                    HUMAN_NODES_LIST = ( RS.Globals.gSkelArray )
                 
                    for entry in HUMAN_NODES_LIST:
                        selectionSet.append( entry )
                    
                    selectionSet.append( 'mover' )
                    
                    for selection in range(len(selectionSet)):
                        selectedNode = mobu.FBFindModelByLabelName(str(item) + ':' + selectionSet[selection])
                        if selectedNode:
                            selectedNode.Selected = True
                        #else:
                        #    print 'Missing node', selectionSet[selection]    

            if item in props: # check for selected props
                propName = mobu.FBFindModelByLabelName(item + ":Dummy01")
                try: # without the try/except this could fail and error if there are no children
                    # Check if any children exist
                    children = propName.Children
                    if (len (children) != 0):
                        # Loop through the children
                        for child in children:
                            if "Control" in child.LongName:
                                if not child.LongName in self.relocatorReferences:
                                    self.relocatorReferences.append(child.LongName)
                                    propControl = mobu.FBFindModelByLabelName(child.LongName)
                                    #self.plotAnimToObj( child.LongName )
                                    self.selectChildren( propControl, True ) # calls function to select all the useful items
                except:
                    pass

            if item in sets: # check for selected sets
                setName = mobu.FBFindModelByLabelName(item + ":SetRoot")
                if setName:
                    if not setName in self.relocatorReferences:
                        self.relocatorReferences.append(setName.LongName)
                        self.plotAnimToObj( setName )
                        setName.Selected = True # just select the setroot   
            
            if item in vehicles:# check for selected vehicles 
                if not (item + ":Chassis_Control") in self.relocatorReferences:
                    vehicleName = item + ":Chassis_Control"
                    self.relocatorReferences.append(vehicleName)
                    self.plotVehicle( vehicleName.split(":")[0] )
                    vehicleControl = mobu.FBFindModelByLabelName(str(vehicleName))
                    vehicleControl.Selected = True # simply selects the control node of the vehicle (as there is only one node currently we're interested in) note:doors are already selected

        for item in range(len(selectedItemsList)):
            if selectedItemsList[item] + ":mover" == 'exportOrigin_Mover:mover':
                if not (selectedItemsList[item] + ":mover") in self.relocatorReferences: 
                    offsetOrigin = mobu.FBFindModelByLabelName(str('exportOrigin_Mover:mover'))
                    self.relocatorReferences.append('exportOrigin_Mover:mover')
                    offsetOrigin.Selected = True
                                
        # this will get the locations of the elements root and create a marker to use later to reposition
        if self.checkBox_useObjectDefaultOrigin.isChecked():
            self.createRelocatorModels( False )
               
        # generate a list of all the models we want to re-select in the new file we're about to create, from our current selection
        lModel = mobu.FBModelList()
        mobu.FBGetSelectedModels( lModel )
        for model in lModel:
            self.exportModelList.append(model.LongName)
        
        #takeList = []
        takeFilenames = []
        
        if self.checkBox_createNewScene.isChecked():        
            # prepare to export animation data
            file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text())
            filename = file_extension[0].encode("utf-8") + "_data.fbx"
            
            # export data to new 'data only' file (in the new location)
            RS.Globals.Application.FileExport( filename ) 
            
            # calls function that creates a txt file containing the required scene elements to be used by the reference editor
            self.createReferenceList()
            
            # Clear the scene (file new)
            RS.Globals.Application.FileNew()
                        
            # call reference editor to populate the blank scene with the required elements
            self.ReferenceInElements()
            
        elif self.checkBox_mergeIntoScene.isChecked():
            file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text())
            filename = file_extension[0].encode("utf-8") + "_data.fbx"
            # export data to new 'data only' file (in the new location) ######### NOTE: if a lot of merging is going into an existing scene, we may need new files each time?
            RS.Globals.Application.FileExport( filename ) 
            
            # Open the existing file to merge into
            RS.Globals.Application.FileOpen( self.lineEdit_newFileNameValue.text().encode("utf-8") , False )
            
        elif self.checkBox_overwriteExistingScene.isChecked():
            lDstFileName = RS.Globals.Application.FBXFileName
            file_extension = os.path.splitext(lDstFileName)
            for take in RS.Globals.Scene.Takes:
                mobu.FBSystem().CurrentTake = take
                
                # prepare to export animation data
                filename = file_extension[0] + "_" + str(take.Name) + ".fbx"
                takeFilenames.append(filename)

                # export data to new 'data only' file (in the new location)
                RS.Globals.Application.FileExport(filename)
                
            # calls function that creates a txt file containing the required scene elements to be used by the reference editor
            self.createReferenceList()
            
            # saves a blank scene in the desired location, with the new filename
            self.createFile()

            # call reference editor to populate the blank scene with the required elements
            self.ReferenceInElements()
    
        # this will get the locations of the elements root and create a marker to use later to reposition
        if self.checkBox_useObjectDefaultOrigin.isChecked():
            self.createRelocatorModels( True )

        # position a new scene origin locator to the default value of a prop if required 
        if self.checkBox_useObjectDefaultOrigin.isChecked():
            for i in range(len(self.relocatorReferences)):
                if self.lineEdit_useObjectDefaultOriginValue.text() in self.relocatorReferences[i]:
                    marker = mobu.FBModelMarker('new_origin')
                    
                    reference = mobu.FBFindModelByLabelName(str(self.relocatorReferences[i]))
            
                    lVecPos = mobu.FBVector3d()
                    reference.GetVector(lVecPos, mobu.FBModelTransformationType.kModelTranslation, True)
                    
                    lVecRot = mobu.FBVector3d()
                    reference.GetVector(lVecRot, mobu.FBModelTransformationType.kModelRotation, True)
                    
                    marker.Translation = lVecPos
                    marker.Rotation = lVecRot
                    
                    marker.Show = True
                    marker.Scaling = mobu.FBVector3d(100, 100, 100)
                    marker.Size = 15.00
                    marker.PropertyList.Find('Look').Data = 2
                    #marker.PropertyList.Find('ShowLabel').Data = True
                    marker.Selected = True
        
        # turn off STANCE mode on the newly referenced character
        for character in RS.Globals.Scene.Characters:
            character.ActiveInput = False
            
        # for Sets, we need to make the TRS active in the Group
        groups = RS.Globals.Scene.Groups
        for i in range(len(self.exportModelList)):
            if 'SetRoot' in self.exportModelList[i]:
                for group in groups:
                    if group.LongName == self.exportModelList[i]:
                        group.Transformable = True
                        
        # need to check namespaces (otherwise this wont work) ~~~~~~~~~~~~~~~~ temporary fix that will fix the namespace issue, I probably should do a remapping tool at some point in future iterations
        referenceList = self.Manager.GetReferenceListAll()
        newNameSpaceList = []
        
        for reference in referenceList:
            referenceNamespace = reference.Namespace
            newNameSpaceList.append(referenceNamespace)

        self.oldNameSpaceList.sort()
        newNameSpaceList.sort()

        for i in range(len(self.oldNameSpaceList)):
            if not self.oldNameSpaceList[i] == newNameSpaceList[i]: # check to see if they are the same name before, if they are we can ignore it
                if self.oldNameSpaceList[i].split("^")[0] == newNameSpaceList[i].split("^")[0]: # check to see if these models are a similar name but without the ^ denoting another model of the same type
                    reference = self.Manager.GetReferenceByNamespace(newNameSpaceList[i])
                    self.Manager.ChangeNamespace(reference, self.oldNameSpaceList[i])
        #self.manager.ChangeNamespace(oldName, newName) ## how the ChangeNamespace tool works ##
        
        # repopulate listWidget_SceneElements
        self.populateSceneElementsList()
        
        # reselect models so we can import data onto them
        for i in range(len(self.exportModelList)):
            model = mobu.FBFindModelByLabelName(str(self.exportModelList[i]))
            print str(self.exportModelList[i])
            model.Selected = True
        
        # import the data from the 'data only' file
        if not self.checkBox_overwriteExistingScene.isChecked():
            RS.Globals.Application.FileImport(filename, False, True)
        else:
            for take in range(len(takeFilenames)):
                RS.Globals.Application.FileImport(takeFilenames[take], False, True)
        
        # clear current selection
        self.deselectComponents()
            
        RS.Globals.Scene.Evaluate()
         
        # move all relocators to desired spot
        if self.checkBox_useObjectDefaultOrigin.isEnabled(): 
            if self.checkBox_useObjectDefaultOrigin.isChecked():
                for i in range(len(self.relocatorReferences)):
                    if self.lineEdit_useObjectDefaultOriginValue.text() in self.relocatorReferences[i]:
        
                        reference = mobu.FBFindModelByLabelName(str('new_origin'))
                        reference.Selected = True
                        mobu.FBPlayerControl().Key()
                        reference.Selected = False
                        
                        relocatorParent = mobu.FBFindModelByLabelName(str('marker:' + self.relocatorReferences[i]))
                        relocatorParent.Translation.SetAnimated(True) # make sure these are animatatable
                        relocatorParent.Rotation.SetAnimated(True) # make sure these are animatatable
                        
                        lVecPos = mobu.FBVector3d()
                        reference.GetVector(lVecPos, mobu.FBModelTransformationType.kModelTranslation, True)
                        
                        lVecRot = mobu.FBVector3d()
                        reference.GetVector(lVecRot, mobu.FBModelTransformationType.kModelRotation, True)
                        
                        relocatorParent.Selected = True
                        relocatorParent.Translation = lVecPos
                        relocatorParent.Rotation = lVecRot
                        
                        mobu.FBPlayerControl().Key() 
                        relocatorParent.Selected = False
                  
            # move elements to desired spot
            lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() # just make sure we're at start frame...
            mobu.FBPlayerControl().Goto(mobu.FBTime(0,0,0,(lStartFrame)))
    
            if self.checkBox_useObjectDefaultOrigin.isChecked():
                for i in range(len(self.relocatorReferences)):
                    RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
                    mobu.FBSleep(20)
                    ParentModel = mobu.FBFindModelByLabelName(str('marker:' + self.relocatorReferences[i]))
                    
                    lVecPos = mobu.FBVector3d()
                    ParentModel.GetVector(lVecPos, mobu.FBModelTransformationType.kModelTranslation, True)
                    lVecRot = mobu.FBVector3d()
                    ParentModel.GetVector(lVecRot, mobu.FBModelTransformationType.kModelRotation, True)
    
                    ChildModel = mobu.FBFindModelByLabelName(str(self.relocatorReferences[i]))
                    ChildModel.Selected = True
                    
                    oldScale = mobu.FBVector3d()
                    ChildModel.GetVector(oldScale, mobu.FBModelTransformationType.kModelScaling, True)
                    
                    lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
                    #TimeStart.Value = int(lStartFrame)
                    lStopFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetFrame()
                    #TimeStop.Value = int(lStopFrame)
    
                    mobu.FBKeyControl().MoveKeys(mobu.FBTimeSpan(mobu.FBTime(0,0,0,lStartFrame), mobu.FBTime(0,0,0,lStopFrame)), ChildModel, lVecPos, lVecRot, oldScale, mobu.FBTime(0,0,0,lStartFrame))
                    ChildModel.Selected = False
            
            RS.Globals.Scene.Evaluate() # got to add an evaluate and pause in there, just breaks otherwise
            mobu.FBSleep(20)
        
        # resave the file after all the changes
        if self.checkBox_createNewScene.isChecked():   
            filename = self.lineEdit_newFileNameValue.text().encode("utf-8")
            RS.Globals.Application.FileSave(filename) 
    
        # if we are to do range selects, we'll top and tail the new range selects here
        if not self.checkBox_overwriteExistingScene.isChecked():
            model = self.tableWidget_RangeSelects.model()
            index_list = [] 
            for row in range(model.rowCount()):
                index_list.append(row)
            if index_list:
                for index in range(len(index_list)):
                    item_0 = self.tableWidget_RangeSelects.item(index_list[index], 0)
                    item_1 = self.tableWidget_RangeSelects.item(index_list[index], 1)
                    item_2 = self.tableWidget_RangeSelects.item(index_list[index], 2)
                    
                    newTake = FBSystem().CurrentTake.CopyTake( item_0.text().encode("utf-8") )
                    lStartFrame = int( item_1.text() )
                    lStopFrame = int( item_2.text() )
                    lSetTimeSpan = mobu.FBTimeSpan(mobu.FBTime(0,0,0,lStartFrame),mobu.FBTime(0,0,0,lStopFrame))
                    mobu.FBSystem().CurrentTake.LocalTimeSpan = lSetTimeSpan
        
        # cleanup scene
        if self.checkBox_useObjectDefaultOrigin.isChecked():
            reference = mobu.FBFindModelByLabelName(str('new_origin'))
            reference.FBDelete()
            for i in range(len(self.relocatorReferences)):
                model = mobu.FBFindModelByLabelName(str('marker:' + self.relocatorReferences[i]))
                model.FBDelete()

        for take in RS.Globals.Scene.Takes:
            if take.Name == "Take 001":
                take.FBDelete()

        # if we have rebuilt the scene, lets delete all the exported take data files
        if takeFilenames:
            for take in range(len(takeFilenames)):
                if os.path.isfile(takeFilenames[take]):
                    os.remove(takeFilenames[take])
        else:
            file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text()) # or just the single data file
            filename = file_extension[0].encode("utf-8") + "_data.fbx"
            if os.path.isfile(filename):
                os.remove(filename)
        
        # finish tidyingup    
        file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text())
        filename = file_extension[0].encode("utf-8") + ".txt"
        if os.path.isfile(filename):
            os.remove(filename)
        
    def p4CheckLocalAssets( self, selectedIndexes ):
        #this is the process to read from the perforce depot using the P4Python Module (compatible with GTAV / motionbuilder 2014)
        p4 = P4()
        e = None
        currentDepotFiles = []
        fbxFiles = []
        for item in selectedIndexes:
            fileName = os.path.basename(self.sceneElementsPath[item])
            dirPath = os.path.dirname(self.sceneElementsPath[item])
            if 'gta5' in dirPath:
                dirPath = dirPath.replace( "X:\\", "//depot/" )
                dirPath = dirPath.replace( "x:\\", "//depot/" )
                dirPath = dirPath.replace( "\\", "/" )
            else:
                dirPath = dirPath.replace( "X:\\", "//" )
                dirPath = dirPath.replace( "x:\\", "//" )
                dirPath = dirPath.replace( "\\", "/" )
 
            p4.connect()                     # Connect to the Perforce server
            
            perforceDepotFile = dirPath + "/" + fileName
            
            try:                             # lets see if we're logged into perforce
                PerforceFbxFiles = p4.run("files", "-e", dirPath + "/...")
            except P4Exception:              # NO!
              for e in p4.errors:            # Lets grab the error
                  print e
            if not e:
                for fbxFile in range(len(PerforceFbxFiles)):
                    try: # doing this catch just in case there isn't any files that might cause this to drop out
                        fbxFileStr = json.dumps(PerforceFbxFiles[fbxFile])
                        fbxFileStr = fbxFileStr.split('"')[19]
                        if "fbx" in fbxFileStr or "FBX" in fbxFileStr:
                            currentDepotFiles.append(os.path.basename(fbxFileStr))
                    except:
                        pass
            else:
                mobu.FBMessageBox( "Perforce Login Failed.", "\n" + e + "\n\nTry re-logging into Perfoce and try again.", "OK" ) # messagebox showing the perforce login error     
            
            p4.disconnect()
            
        perforceCheck = True 
        
        for i in selectedIndexes:
            if not os.path.basename(self.sceneElementsPath[i]) in currentDepotFiles:
                mobu.FBMessageBox( "Error.", "\n" + str( os.path.basename(self.sceneElementsPath[i]) ) + "\nDoes not exist in the perforce Depot?", "OK" ) # messagebox showing the perforce login error
                perforceCheck = False
        
        if perforceCheck:
            return True
        else:
            return False

    def deselectComponents( self ):
        for comp in RS.Globals.Scene.Components: 
            comp.Selected = False 

    def populateSceneElementsList( self ):
        # clear out listWidget_sceneElements if it is populated
        itemList = [self.listWidget_sceneElements.item(i) for i in range(self.listWidget_sceneElements.count())]
        for item in range(len(itemList)):
            self.listWidget_sceneElements.takeItem(self.listWidget_sceneElements.row(itemList[item]))       

        self.sceneElementsPath = []
        
        # populate the listWidget_sceneElements
        self.deselectComponents()
        
        self.ParentModel = mobu.FBFindModelByLabelName(str("REFERENCE:Scene"))
        self.selectChildren( self.ParentModel, False )

        lModel = mobu.FBModelList()
        mobu.FBGetSelectedModels( lModel )
        
        lModel = mobu.FBModelList()
        mobu.FBGetSelectedModels( lModel )        
            
        for model in lModel:                          # fill the list with all models that have been referenced in  
            name = model.Name
            self.listWidget_sceneElements.addItem(str(name))
            elementPath = model.PropertyList.Find( 'Reference Path' )
            self.sceneElementsPath.append(elementPath.Data)
            model.Selected = False
    
    def plotAnimToObj( self, Obj ):
        Obj.Translation.SetAnimated(True) # make sure these are animatatable
        Obj.Rotation.SetAnimated(True) # make sure these are animatatable
        Obj.Selected = True
        plottoMarker = mobu.FBPlotOptions()
        plottoMarker.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterGimbleKiller
        plottoMarker.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
        plottoMarker.PlotOnFrame = True
        plottoMarker.UseConstantKeyReducer = True
        plottoMarker.ConstantKeyReducerKeepOneKey  = True
        mobu.FBSystem().CurrentTake.PlotTakeOnSelected(plottoMarker) 

    def plotVehicle( self, vehicleName ):
        #vehcileRoot = mobu.FBFindModelByLabelName(str(vehicleName + ":chassis"))
        for comp in RS.Globals.Scene.Components:
            if vehicleName in comp.LongName:
                if comp.LongName == vehicleName + ":" + "Chassis_Control":
                    comp.Selected = True
                    lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() # just make sure we're at start frame...
                    mobu.FBPlayerControl().Goto(mobu.FBTime(0,0,0,(lStartFrame)))
                    comp.Translation.SetAnimated(True) # make sure these are animatatable
                    comp.Rotation.SetAnimated(True) # make sure these are animatatable
                    #mobu.FBPlayerControl().Key()
                    plottoVehicle = mobu.FBPlotOptions()
                    plottoVehicle.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll          
                    plottoVehicle.UseConstantKeyReducer = True
                    plottoVehicle.ConstantKeyReducerKeepOneKey  = True
                    plottoVehicle.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
                    plottoVehicle.PlotOnFrame = True
                    mobu.FBSystem().CurrentTake.PlotTakeOnSelected(plottoVehicle)  
                    #comp.Selected = False
                    
        for comp in RS.Globals.Scene.Components: 
            if vehicleName in comp.LongName:                  
                if 'door_' in comp.LongName:
                    comp.Selected = True
                    comp.Translation.SetAnimated(True) # make sure these are animatatable
                    comp.Rotation.SetAnimated(True) # make sure these are animatatable
                    plottoVehicle = mobu.FBPlotOptions()
                    plottoVehicle.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll          
                    plottoVehicle.UseConstantKeyReducer = True
                    plottoVehicle.ConstantKeyReducerKeepOneKey  = True
                    plottoVehicle.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
                    plottoVehicle.PlotOnFrame = True
                    mobu.FBSystem().CurrentTake.PlotTakeOnSelected(plottoVehicle)    
        
    def createRelocatorHierarchy( self ):
        lPlayer = mobu.FBPlayerControl()
        lSystem = mobu.FBSystem()
        lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame()
        lPlayer.Goto(mobu.FBTime(0,0,0,(lStartFrame)))
        child = []
        parent = None
    
        for i in range(len(self.relocatorReferences)):
            if self.lineEdit_useObjectDefaultOriginValue.text() in self.relocatorReferences[i]:
                #marker = FBModelMarker('marker:' + relocatorReferences[i])
                parent = self.relocatorReferences[i]
            else:
                #marker = FBModelMarker('marker:' + relocatorReferences[i])
                child.append(self.relocatorReferences[i])
        for i in range(len(child)):
            parentModel = mobu.FBFindModelByLabelName(str('marker:' + parent))
            childModel = mobu.FBFindModelByLabelName(str('marker:' + child[i]))
            childModel.Parent = parentModel
            childModel.Selected = True
            lPlayer.Key()

    def createRelocatorModels( self, stage1 ):
        lPlayer = mobu.FBPlayerControl()
        lSystem = mobu.FBSystem()
        
        lStartFrame = mobu.FBSystem().CurrentTake.LocalTimeSpan.GetStart().GetFrame() # just make sure we're at start frame...
        lPlayer.Goto(mobu.FBTime(0,0,0,(lStartFrame)))
        
        if not stage1:
            for i in range(len(self.relocatorReferences)):
                marker = mobu.FBModelMarker('marker:' + self.relocatorReferences[i])
                
                reference = mobu.FBFindModelByLabelName(str(self.relocatorReferences[i]))
        
                lVecPos = mobu.FBVector3d()
                reference.GetVector(lVecPos, mobu.FBModelTransformationType.kModelTranslation, True)
                
                lVecRot = mobu.FBVector3d()
                reference.GetVector(lVecRot, mobu.FBModelTransformationType.kModelRotation, True)
                
                marker.Translation = lVecPos
                marker.Rotation = lVecRot
                
                marker.Show = True
                marker.Scaling = mobu.FBVector3d(100, 100, 100)
                marker.Size = 15.00
                marker.PropertyList.Find('Look').Data = 2
                marker.PropertyList.Find('ShowLabel').Data = True
                
                if not self.checkBox_useObjectDefaultOrigin.isChecked():
                    marker.Selected = True
                    lPlayer.Key()    
        else:
            for i in range(len(self.relocatorReferences)):
                marker = mobu.FBModelMarker('marker:' + self.relocatorReferences[i])
                marker.Show = True
                marker.Scaling = mobu.FBVector3d(100, 100, 100)
                marker.Size = 15.00
                marker.PropertyList.Find('Look').Data = 2
                marker.PropertyList.Find('ShowLabel').Data = True
        
        self.createRelocatorHierarchy()

    def motionBuilderBuildCheck( self ):
        if Globals.System.Version <= 14000.0:
            mobu.FBMessageBox( "MotionBuilder Build too old.",
                            "\nYou are currently on MotionBuilder 2014 or below.\nThis feature is unavaliable.",
                            "OK" )
            return False
        else:
            return True
        
    def useObjectDefaultOriginCbxCallback( self ):
        if not self.checkBox_useObjectDefaultOrigin.isChecked():
            self.lineEdit_useObjectDefaultOriginValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
        
        if not self.motionBuilderBuildCheck():
            self.lineEdit_useObjectDefaultOriginValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
            self.checkBox_useObjectDefaultOrigin.setChecked(False)
       
    def useObjectDefaultOriginCallback( self ):
        if not self.motionBuilderBuildCheck():
            self.lineEdit_useObjectDefaultOriginValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
            self.checkBox_useObjectDefaultOrigin.setChecked(False)
        elif len(self.listWidget_sceneElements.selectedItems()) == 1:
            text = [item.text() for item in self.listWidget_sceneElements.selectedItems()]
            self.lineEdit_useObjectDefaultOriginValue.setText(QtGui.QApplication.translate("MainWindow", str(text[0]), None, QtGui.QApplication.UnicodeUTF8))
            self.checkBox_useObjectDefaultOrigin.setChecked(True)
  
    def selectChildren( self, parentModel, selectParent ):
        # Get models children
        children = parentModel.Children
    
        # Check if any children exist
        if (len (children) != 0):
            # Loop through the children
            for child in children:
                # Select the child
                child.Selected = True
                
                # Get any children
                self.selectChildren( child, False )
        
        # Select parent
        if selectParent:
            parentModel.Selected = True
        
        # Return status
        return True

    def createReferenceList( self ):
        file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text())
        filename = file_extension[0] + ".txt"
        f = open(filename, 'w')
        
        selectedIndexes = [x.row() for x in self.listWidget_sceneElements.selectedIndexes()]
        
        if selectedIndexes:
            for item in selectedIndexes:
                f.write(self.sceneElementsPath[item] + "\n")
                self.oldNameSpaceList.append(self.listWidget_sceneElements.item(item).text().encode("utf-8")) # grabbing the old References that are selected now, ready to do the namespace check later
            f.close
    
    def createFile( self ):
        if not self.checkBox_overwriteExistingScene.isChecked():
            filename = self.lineEdit_newFileNameValue.text().encode("utf-8")
            saveFile = filename
            saveFolder = os.path.split(saveFile)[0]
            if not os.path.exists(saveFolder):
                os.makedirs(saveFolder)  
            RS.Globals.Application.FileSave(saveFile)
        else:
            # overwrite the old file so we got a blank canvas to work from
            lDstFileName = RS.Globals.Application.FBXFileName
            
            # Clear the scene (file new)
            RS.Globals.Application.FileNew()
            
            # Save over the old file with a blank scene
            RS.Globals.Application.FileSave(lDstFileName)     

    def ReferenceInElements( self ):
        file_extension = os.path.splitext(self.lineEdit_newFileNameValue.text())
        filename = file_extension[0] + ".txt"

        with open(filename, "r") as txtFile:
            txtFileDataList = txtFile.readlines()
            referenceList = []
            for path in txtFileDataList:
                pathSplit = path.split('\n')
                path = pathSplit[0]
                referenceList.append(path)
            if len(referenceList) > 0:
                self.Manager.CreateReferences(referenceList)
        
        self.Manager.UpdateReferences(self.Manager.GetReferenceListAll()) ## ~~~~~~~~~~~ does not work? ~~~~~~~~~~~~~~ ###
            
    def searchFolderCallback( self ):
        # popup for user to select new export folder
        lDstFileName = RS.Globals.Application.FBXFileName
        dirPath = os.path.dirname(lDstFileName)
        popup = QtGui.QFileDialog()
        #popup.setFileMode(QtGui.QFileDialog.Directory)
        popup.setViewMode(QtGui.QFileDialog.List)
        popup.setOption(QtGui.QFileDialog.ShowDirsOnly)
        popup.setDirectory(dirPath)
        selected_directory = QtGui.QFileDialog.getSaveFileName(None, "Choose a New or Existing FileName", "", "FBX files (*.fbx *.FBX)")
        if str(selected_directory[0]) != "":
            self.lineEdit_newFileNameValue.setText(QtGui.QApplication.translate("MainWindow", str(selected_directory[0]), None, QtGui.QApplication.UnicodeUTF8))
            #self.pushButton_newFileSearch.setText(QtGui.QApplication.translate("MainWindow", "New File Name:", None, QtGui.QApplication.UnicodeUTF8))
        else:
            self.lineEdit_newFileNameValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
            #self.pushButton_newFileSearch.setText(QtGui.QApplication.translate("MainWindow", "New File Name:", None, QtGui.QApplication.UnicodeUTF8))

    def createNewSceneCbxCallback( self ):
        if self.checkBox_createNewScene.isChecked():
            self.checkBox_mergeIntoScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_overwriteExistingScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_useObjectDefaultOrigin.setEnabled(True)
            self.pushButton_useObjectDefaultOrigin.setEnabled(True)
            self.lineEdit_useObjectDefaultOriginValue.setEnabled(True)
            self.pushButton_newFileSearch.setEnabled(True)
            self.lineEdit_newFileNameValue.setEnabled(True)
            self.pushButton_newFileSearch.setText(QtGui.QApplication.translate("MainWindow", "New File Name:", None, QtGui.QApplication.UnicodeUTF8))
            self.pushButton_createNewScene.setText(QtGui.QApplication.translate("MainWindow", "Create New Scene", None, QtGui.QApplication.UnicodeUTF8))
                            
    def mergeIntoSceneCbxCallback( self ):
        if self.checkBox_mergeIntoScene.isChecked():
            self.checkBox_createNewScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_overwriteExistingScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_useObjectDefaultOrigin.setEnabled(True)
            self.pushButton_useObjectDefaultOrigin.setEnabled(True)
            self.lineEdit_useObjectDefaultOriginValue.setEnabled(True)
            self.pushButton_newFileSearch.setEnabled(True)
            self.lineEdit_newFileNameValue.setEnabled(True)
            self.pushButton_newFileSearch.setText(QtGui.QApplication.translate("MainWindow", "File to Merge into:", None, QtGui.QApplication.UnicodeUTF8))
            self.pushButton_createNewScene.setText(QtGui.QApplication.translate("MainWindow", "Merge Into Scene", None, QtGui.QApplication.UnicodeUTF8))
            
    def overwriteExistingSceneCbxCallback( self ):
        if self.checkBox_overwriteExistingScene.isChecked():
            self.checkBox_createNewScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_mergeIntoScene.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_useObjectDefaultOrigin.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)
            self.checkBox_useObjectDefaultOrigin.setEnabled(False)
            self.pushButton_useObjectDefaultOrigin.setEnabled(False)
            self.lineEdit_useObjectDefaultOriginValue.setEnabled(False)
            self.pushButton_newFileSearch.setEnabled(False)
            self.lineEdit_newFileNameValue.setEnabled(False)
            self.lineEdit_newFileNameValue.setText(QtGui.QApplication.translate("MainWindow", "", None, QtGui.QApplication.UnicodeUTF8))
            self.pushButton_createNewScene.setText(QtGui.QApplication.translate("MainWindow", "Rebuild and Overwrite current scene", None, QtGui.QApplication.UnicodeUTF8))        
        
    def help( self ):
        webbrowser.open('https://hub.rockstargames.com/display/ANIM/Motionbuilder+Tool%3A+Create+New+Scene+from+Previz')
        
    def createNewRangeCallback( self ):
        if self.lineEdit_newRangeNameValue.text() != "":
            # create a blank row
            rowPosition = self.tableWidget_RangeSelects.rowCount()# gets the last row
            self.tableWidget_RangeSelects.insertRow(rowPosition)
            
            # add data to it            
            self.tableWidget_RangeSelects.setItem(rowPosition , 0, QtGui.QTableWidgetItem( self.lineEdit_newRangeNameValue.text() ))
            self.tableWidget_RangeSelects.setItem(rowPosition , 1, QtGui.QTableWidgetItem( self.spinBox_newRangeStartValue.text() ))
            self.tableWidget_RangeSelects.setItem(rowPosition , 2, QtGui.QTableWidgetItem( self.spinBox_newRangeEndValue.text() ))

            # re-justufy the range numbers
            item = self.tableWidget_RangeSelects.item(rowPosition , 1)
            item.setTextAlignment(QtCore.Qt.AlignCenter)# got various other options - AlignHCenter , AlignVCenter, AlignBottom 
            item = self.tableWidget_RangeSelects.item(rowPosition , 2)
            item.setTextAlignment(QtCore.Qt.AlignCenter)

    def deleteSelectedRangeCallback( self ):
        # credit to = https://stackoverflow.com/questions/37786299/how-to-delete-row-rows-from-a-qtableview-in-pyqt
        index_list = []                                                          
        for model_index in self.tableWidget_RangeSelects.selectionModel().selectedRows():       
            index = QtCore.QPersistentModelIndex(model_index)         
            index_list.append(index)                                             
        
        for index in index_list:                                      
             self.tableWidget_RangeSelects.removeRow(index.row()) 
              
    def newRangeStartCallback( self ):
        lCurrentFrame = mobu.FBSystem().LocalTime.GetFrame()
        self.spinBox_newRangeStartValue.setValue( int(lCurrentFrame) )

        if int(self.spinBox_newRangeEndValue.text()) < int(self.spinBox_newRangeStartValue.text()):
            self.spinBox_newRangeEndValue.setValue( int(lCurrentFrame) + 1 )
                    
    def newRangeEndCallback( self ):
        lCurrentFrame = mobu.FBSystem().LocalTime.GetFrame()
        if int(lCurrentFrame) > int(self.spinBox_newRangeStartValue.text()):
            self.spinBox_newRangeEndValue.setValue( int(lCurrentFrame) )
        else:
            self.spinBox_newRangeEndValue.setValue( int(self.spinBox_newRangeStartValue.text()) + 1 )

    def rebuildElementsCallback( self ):
        if self.checkBox_rebuildElements.isChecked():
            self.listWidget_sceneElements.selectAll()
            
    def listWidgetCallback( self ):
        if self.checkBox_rebuildElements.isChecked():
            self.checkBox_rebuildElements.setCheckState(QtCore.Qt.CheckState(False)) # works better than - .setChecked(bool)    

def Run():
    form = MainWindow()
    form.setWindowTitle('Build Or Rebuild Scene')
    form.show()