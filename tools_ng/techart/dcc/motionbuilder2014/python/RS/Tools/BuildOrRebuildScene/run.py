from RS.Tools.BuildOrRebuildScene.Widgets import BuildOrRebuildScene


def Run( show = True ):
    toolsDialog = BuildOrRebuildScene.MainWindow()
    
    if show:
        toolsDialog.show()

    return toolsDialog