import time


class progBar():

    def __init__(self, pBar, pBarLabel):
        self.__pBar = pBar
        self.__pBarLabel = pBarLabel

    def setValue(self, val):
        self.__pBar.setValue(val)

    def setVisible(self, vis):
        self.__pBar.setVisible(vis)
        self.__pBarLabel.setVisible(vis)

    def reset(self):
        self.__pBar.reset()
        self.__pBarLabel.setText('')

    def text(self, txt):
        self.__pBarLabel.setText(txt)

    def simProgress(self):

        numIterations = 10
        timeDelay = 1

        for i in range( numIterations ):

            # Determine a progress value.
            progress = 100 * ( i + 1 ) / numIterations

            self.__pBar.setValue(progress)

            time.sleep( timeDelay )

        self.__pBar.setVisible(False)