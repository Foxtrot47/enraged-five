"""
Tool UI for the saveing and loading of XML relationship constraints
"""
import os

from PySide import QtCore, QtGui

import pyfbsdk as mobu
from RS.Tools import UI
from RS.Utils.Scene import RelationshipConstraintManager as rcm


class RelationShipConstraintTool(UI.QtMainWindowBase):
    """
    Main Dialog for the relationship Constraint Tool
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._toolName = "RelationShip Constraint Manager"
        super(RelationShipConstraintTool, self).__init__(title=self._toolName, dockable=True, size=(350,150))
        self.SetupUi()

    def SetupUi(self):
        """
        Setup UI
        """
        tabs = QtGui.QTabWidget()
        tabs.addTab(self._setupLoadTab(), "Load")
        tabs.addTab(self._setupSaveTab(), "Save")

        self.setCentralWidget(tabs)

    def _setupLoadTab(self):
        """
        Setup the Load Tab of the UI
        """
        tabWidget = QtGui.QWidget()
        layout = QtGui.QGridLayout()

        self._loadTextField = QtGui.QLineEdit()
        self._namespaceTextField = QtGui.QLineEdit()
        loadBrowseButton = QtGui.QPushButton("...")
        loadButton = QtGui.QPushButton("Load Relationship Constraint")

        loadBrowseButton.setMaximumWidth(50)

        layout.addWidget(self._loadTextField, 0, 0)
        layout.addWidget(loadBrowseButton, 0, 1)
        layout.addWidget(QtGui.QLabel("Namespace:"), 1, 0, 1, 2)
        layout.addWidget(self._namespaceTextField, 2, 0, 1, 2)
        layout.addWidget(loadButton, 3, 0, 1, 2)

        tabWidget.setLayout(layout)

        loadButton.pressed.connect(self._handleLoadConstraint)
        loadBrowseButton.pressed.connect(self._handleBrowseForFile)

        return tabWidget

    def _setupSaveTab(self):
        """
        Setup the Save Tab of the UI
        """
        tabWidget = QtGui.QWidget()
        layout = QtGui.QGridLayout()

        self._relationConstraintsInScene = QtGui.QComboBox()
        refreshButton = QtGui.QPushButton("Refresh")
        saveButton = QtGui.QPushButton("Save Relationship Constraint")

        refreshButton.setMaximumWidth(50)

        layout.addWidget(self._relationConstraintsInScene, 0, 0)
        layout.addWidget(refreshButton, 0, 1)
        layout.addWidget(saveButton, 1, 0, 1, 2)

        tabWidget.setLayout(layout)

        saveButton.pressed.connect(self._handleSaveConstraint)
        refreshButton.pressed.connect(self._handleConstraintRefresh)

        self._handleConstraintRefresh()

        return tabWidget

    def _handleBrowseForFile(self):
        """
        Internal Method

        For the browse for file button is pressed
        """
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, 'Load Constraint', '/home', "Relation Constraint files (*.xml)")
        if not notCan:
            return
        self._loadTextField.setText(filePath)

    def _handleLoadConstraint(self):
        """
        Internal Method

        Handle the loading of a filepath as a constraint
        """
        if os.path.isfile(self._loadTextField.text()):
            try:
                namespace = self._namespaceTextField.text() or None 
                name = os.path.splitext(os.path.basename(self._loadTextField.text()))[0]
                rcm.LoadInConstraint(self._loadTextField.text(), name, namespace=namespace)
            except ValueError, exp:
                QtGui.QMessageBox.critical(self, "Nodes Missing", "{0}".format(exp))
        else:
            QtGui.QMessageBox.critical(self, "File not found", "{0} not found".format(self._loadTextField.text()))
            return
        self._loadTextField.setText("")
        self._namespaceTextField.setText("")

    def _handleSaveConstraint(self):
        """
        Internal Method

        Handle the saving of the currently selected relationship constraint
        """
        con = rcm.GetRelationShipConstraintByName(self._relationConstraintsInScene.currentText())
        if con is None:
            QtGui.QMessageBox.critical(self, "Constraint not found", "{0} not found".format(self._relationConstraintsInScene.currentText()))
            return

        filePath, notCan = QtGui.QFileDialog.getSaveFileName(self, 'Save Constraint', '{0}.xml'.format(con.Name), "Relation Constraint files (*.xml)")
        if not notCan:
            return
        con.SaveConstaintToFile(filePath)

    def _handleConstraintRefresh(self):
        """
        Internal Method

        Handle the refreshing of the list of relationship constraints in the scene
        """
        self._relationConstraintsInScene.clear()
        self._relationConstraintsInScene.addItems([con.Name for con in rcm.GetAllRelationshipConstraints()])


def Run():
    return UI.Run(RelationShipConstraintTool())


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    win = RelationShipConstraintTool()
    win.show()
    sys.exit(app.exec_())
