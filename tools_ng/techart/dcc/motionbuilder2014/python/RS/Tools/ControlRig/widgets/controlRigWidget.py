from PySide import QtGui, QtCore

import pyfbsdk as mobu

import os

from RS import Globals
from RS.Tools import UI
from RS.Utils import Scene
from RS.Core.Animation import Lib

from RS.Core.AssetSetup.Parser.Organize import Organize


class ControlRigWidgetDialog(UI.QtMainWindowBase):
    """
    Dialogue to run ControlRigWidget and retain size settings for opening and closing of the widget
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._settings = QtCore.QSettings( "RockstarSettings", "ControlRigWidgetDialog" )
        super(ControlRigWidgetDialog, self).__init__(parent=parent, title="R* Control Rig Manager", dockable=True)
        self._dock.closeEvent = self.closeEvent
        self._dock.resize(self._settings.value("size", QtCore.QSize(400, 500)))
        controlRigWidget = ControlRigWidget()

        # create layout and add widget
        self.setCentralWidget(controlRigWidget)

    def showEvent(self, event):
        self._dock.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self._dock.saveGeometry())
        self._settings.setValue("size", self._dock.size())
        return super(ControlRigWidgetDialog, self).closeEvent(event)


class ControlRigWidget(QtGui.QWidget):
    """
    Class to generate the widgets, and their functions, for the Toolbox UI
    """
    
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ControlRigWidget, self).__init__(parent=parent)
        self._character = None
        self.setupUi()

    def _getCharacterList(self):
        """
        Get list of characters in the scene
        Returns:
            List (Str) - naemspaces of the characters in the scene
        """
        characterNamespaceList = []
        for character in Globals.Characters:
            nameSplit = character.LongName.split(":")
            namespace = nameSplit[0]
            characterNamespaceList.append(namespace)
        return characterNamespaceList

    def _createControlRig(self):
            characterXMLPath = Lib.GetControlRigXMLPath(self._character, True)
            if characterXMLPath:
                organizeXMLPath = os.path.join(os.path.dirname(characterXMLPath), "organize.xml")
                Organize(characterXMLPath, namespace=self._getNamespace(), create=True)
                Organize(organizeXMLPath, namespace=self._getNamespace(), create=False)
                return True
            else:
                QtGui.QMessageBox.warning(
                                    None,
                                    "Warning",
                                    "An xml for the selected character does not exist.\n\n"
                                    "Please see rigging to get this generated.",
                                    QtGui.QMessageBox.Ok)

    def _returnSourceDict(self):
        """
        Dictionary containing the source option, character input type and method
        
        Returns:
            Dictionary - String: InputType, Method, index
        """
        return {"Stance": ("kFBCharacterInputStance", self._setStanceMode, 2),
                "Control Rig": ("kFBCharacterInputMarkerSet", self._setControlRigOn, 1)}

    def _populateComboBoxes(self):
        """
        Populate the combo boxes with strings
        """
        self.characterCombo.addItem("None")
        self.sourceCombo.addItem("None")
        if not self._getCharacterList():
            return
        sourceStringList = self._returnSourceDict().keys()
        sourceStringList.sort()
        self.characterCombo.addItems(self._getCharacterList())
        self.sourceCombo.addItems(sourceStringList)
        self.sourceCombo.setEnabled(False)

    def _setControlRigOn(self):
        """
        Create control rig if it doesnt exist, or activate control rig if it does.
        """
        if self._getCharacterName():
            # check if character has a mobu control rig attached first
            if self._character.GetCtrlRigModel(mobu.FBBodyNodeId.kFBHipsNodeId):
                self._character.InputType = mobu.FBCharacterInputType.kFBCharacterInputMarkerSet
                self._character.ActiveInput = True
            # check if character has a complete custom control rig
            else:
                # create a control rig if there isn't one already for the character
                self._createControlRig()
                self.sourceCombo.setCurrentIndex(0)

    def _setStanceMode(self):
        """
        Sets the current character into stance mode
        """
        currentCharacterText = str(self.characterCombo.currentText())
        if currentCharacterText != "None":
            self._character.InputType = mobu.FBCharacterInputType.kFBCharacterInputStance
            self._character.ActiveInput = True

    def _plotToSkeleton(self):
        """
        Plot down the skeleton
        """
        currentCharacterText = str(self.characterCombo.currentText())
        if currentCharacterText != "None":
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = True
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

            self._character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton,
                                          plotOptions)
            self._character.ActiveInput == False
        else:
            QtGui.QMessageBox.warning(
                                        None,
                                        "Warning",
                                        "A character has not been selected.\n\nPlease select a character before selecting options to plot.",
                                        QtGui.QMessageBox.Ok
                                     )

    def _getCharacterName(self):
        currentCharacterText = str(self.characterCombo.currentText())
        return currentCharacterText if currentCharacterText != "None" else None

    def _getNamespace(self):
        currentCharacterText = str(self.characterCombo.currentText())
        if currentCharacterText != "None":
            namespaces = [namespace.Name for namespace in mobu.FBSystem().Scene.Namespaces]
            return currentCharacterText if currentCharacterText in namespaces else ""

    def _plotToControlRig(self):
        """
        Plot the skeleton to the control rig
        """

        if self._getCharacterName():
            # plot options
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = True
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

            # check if character has a mobu control rig attached first
            if self._character.GetCtrlRigModel(mobu.FBBodyNodeId.kFBHipsNodeId):
                self._character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig,
                                              plotOptions)
            else:
                # character does not have a control rig attached, we will need to create a new one and
                # then plot
                self._createControlRig()
                self._character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig, plotOptions)

            # set current source as control rig
            self.sourceCombo.setCurrentIndex(1)
        else:
            QtGui.QMessageBox.warning(
                                        None,
                                        "Warning",
                                        "A character has not been selected.\n\nPlease select a character before selecting options to plot.",
                                        QtGui.QMessageBox.Ok
                                     )

    def _characterComboBoxChanged(self):
        """
        Select current character as characterCombo's current index
        """
        # get current text
        currentCharacterText = str(self.characterCombo.currentText())
        self.sourceCombo.setCurrentIndex(0)

        # if set to None disable sourceCombo
        if currentCharacterText == "None":
            self.sourceCombo.setEnabled(False)
            return

        # enable combo
        self.sourceCombo.setEnabled(True)

        # deselect all
        Scene.DeSelectAll()

        # select character as current
        for character in Globals.Characters:
            nameSplit = character.LongName.split(":")
            namespace = nameSplit[0]
            if namespace == currentCharacterText:
                character.Selected = True
                self._character = character
                break

        # check if a mobu control rig is already active on the character
        if self._character and self._character.ActiveInput:
            inputType = str(self._character.InputType)
            for value in self._returnSourceDict().itervalues():
                if str(value[0]) == inputType:
                    self.sourceCombo.setCurrentIndex(value[2])

    def _sourceComboBoxChanged(self):
        """
        Assign the correct method for whatever the user has selected as the current index for
        sourceCombo
        """
        sourceDict = self._returnSourceDict()
        currentSourceText = str(self.sourceCombo.currentText())

        if currentSourceText != "None":
            for key, value in sourceDict.iteritems():
                if currentSourceText == key:
                    value[1]()

    def setupUi(self):
        """
        Sets up the widgets, their properties and launches call events
        """
        # deselect all
        Scene.DeSelectAll()

        # create layouts
        mainLayout = QtGui.QGridLayout()

        # widgets
        self.characterLabel = QtGui.QLabel()
        self.sourceLabel = QtGui.QLabel()
        self.characterCombo = QtGui.QComboBox()
        self.sourceCombo = QtGui.QComboBox()
        horizontalLine = QtGui.QFrame()
        self.skelButton = QtGui.QPushButton()
        self.rigButton = QtGui.QPushButton()

        # widget properties
        self.characterLabel.setText("Character:")
        self.sourceLabel.setText("Source:")
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        self.skelButton.setText("Plot To Skeleton")
        self.rigButton.setText("Plot To Control Rig")

        # populate comboboxes
        self._populateComboBoxes()

        # add widgets to layout
        mainLayout.addWidget(self.characterLabel, 0, 0)
        mainLayout.addWidget(self.characterCombo, 0, 1, 1, 2)
        mainLayout.addWidget(self.sourceLabel, 1, 0)
        mainLayout.addWidget(self.sourceCombo, 1, 1, 1, 2)
        mainLayout.addWidget(horizontalLine, 2, 1, 1, 2)
        mainLayout.addWidget(self.skelButton, 3, 1, 1, 2)
        mainLayout.addWidget(self.rigButton, 4, 1, 1, 2)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        self.skelButton.released.connect(self._plotToSkeleton)
        self.rigButton.released.connect(self._plotToControlRig)
        self.characterCombo.currentIndexChanged.connect(self._characterComboBoxChanged)
        self.sourceCombo.currentIndexChanged.connect(self._sourceComboBoxChanged)
