from PySide import QtCore, QtGui
import RS.Config

class DlcProjectListModel(QtCore.QAbstractListModel):
    def __init__(self, projectList = [], parent = None):
        ### call the superclass constructor and pass the parent class to it ###
        QtCore.QAbstractListModel.__init__(self, parent)

        ### private attributes ###
        self.__projectList = projectList



    ### View needs to know how many items this model contains ###
    def rowCount(self, parent):
        return len(self.__projectList)

    ### QModelIndex and role ###
    def data(self, index, role):


        if role == QtCore.Qt.DisplayRole:
            row = index.row()
            value = self.__projectList[row]
            return value

    ### Making the lists editable, need two more methods, flags and setData ###
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable