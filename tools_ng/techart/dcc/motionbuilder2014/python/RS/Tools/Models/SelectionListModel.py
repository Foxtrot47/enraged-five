from PySide import QtCore, QtGui
import RS.Config
import icons_rc
import RS.Utils.Path as rsPath

class animSelectionNode(object):

    def __init__(self, name, typeInfo = None, nodePath = None):

        self._name = name
        self._typeinfo = typeInfo		
        self._path = nodePath

    def typeInfo(self):
        return self._typeinfo
       

    # getters	
    def name(self):
        return self._name


    def path(self):
        return self._path
    
    def getIndex(self):
        return index.internalPointer()

    

class animFolderSelectionNode(animSelectionNode):

    def __init__(self, name, typeInfo = None, nodePath = None):
        super(animFolderSelectionNode, self).__init__(name, typeInfo, nodePath)

    def typeInfo(self):
        return "File Folder"
        
class animFileSelectionNode(animSelectionNode):

    def __init__(self, name, parent = None, nodePath = None):
        super(animFileSelectionNode, self).__init__(name, parent, nodePath)

    def typeInfo(self):
        return "anim depot File"

class animFileWorkspaceSelectionNode(animSelectionNode):

    def __init__(self, name, parent = None, nodePath = None):
        super(animFileWorkspaceSelectionNode, self).__init__(name, parent, nodePath)

    def typeInfo(self):
        return "anim workspace File"


class SelectionListModel(QtCore.QAbstractListModel):
    def __init__(self, nodeList = [], parent = None):
        ### call the superclass constructor and pass the parent class to it ###
        QtCore.QAbstractListModel.__init__(self, parent)

        ### private attributes ###
        self.__nodeList = nodeList


    def getList(self):
        return self.__nodeList


    ### View needs to know how many items this model contains ###
    def rowCount(self, parent = QtCore.QModelIndex()):
        return len(self.__nodeList)


    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent = QtCore.QModelIndex()):	
        return 4	

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "Folders/Files"
                elif section == 1:
                    return "Type"
                else:
                    return "Path"	

    ### QModelIndex and role ###
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()		

        if role == QtCore.Qt.DisplayRole:
            row = index.row()
            value = self.__nodeList[row]

            if index.column() == 0:
                return value.name()
            if index.column() == 1:
                return value.typeInfo()
            if index.column() == 2:
                return value.path()

        if role == QtCore.Qt.DecorationRole:

            row = index.row()
            value = self.__nodeList[row]

            if unicode(value.typeInfo()) == "File Folder":
                return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
            elif unicode(value.typeInfo()) == "anim depot File":
                return QtGui.QIcon(QtGui.QPixmap(":/depotFile_icon.png"))
            else:
                return QtGui.QIcon(QtGui.QPixmap(":/workspaceFile_icon.png"))	
            
            """ Inputs: QModelIndex"""
        
    #def getRow(self, index):
        #for 

    ### Making the lists editable, need two more methods, flags and setData ###
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    #=====================================================================#
    # INSERTING AND REMOVING
    #=====================================================================#
    def insertRows(self, position, rows, node, parent = QtCore.QModelIndex()):
        #self.beginInsertRows(index, first, last)
        self.beginInsertRows(parent, position, position + rows - 1)

        # DO INSERTING HERE
        self.__nodeList.insert(position, node) 

        self.endInsertRows()

        return True

    def removeRows(self, position, rows, parent = QtCore.QModelIndex()):

        self.beginRemoveRows(parent, position, position + rows - 1)

        # DO Removing HERE
        for i in range(rows):
            value = self.__nodeList[position]
            self.__nodeList.remove(value)

        self.endRemoveRows()	

        return True

