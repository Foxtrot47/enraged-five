"""
Interface for the culling of geo based off an input sphere
"""
from PySide import QtCore, QtGui

import pyfbsdk as mobu
import mobuExtender

from RS.Tools import UI
from RS.Utils import Scene, Vector
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive


class MeshCullingUI(UI.QtMainWindowBase):
    """
    GUI Tool for the Mehs Cull tool
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Mesh Culling Tool"
        super(MeshCullingUI, self).__init__(title=self._toolName, dockable=True)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setupUi()
        self._previousPosition = mobu.FBVector3d(0, 0, 0)

    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        internalLayout = QtGui.QGridLayout()
        cullItemsLayout = QtGui.QGridLayout()

        # Widgets
        banner = UI.BannerWidget(self._toolName)
        self._enableToolButton = QtGui.QPushButton("")
        self._internalToolWidget = QtGui.QWidget()
        self._sizeSlider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self._sizeSpinner = QtGui.QSpinBox()

        cullGeoButton = QtGui.QPushButton("Cull Geometry outside of sphere!")

        cullItemeWidget = QtGui.QWidget()
        self._cullItemsListBox = QtGui.QListWidget()
        cullItemsAddFromSelection = QtGui.QPushButton("Add Items from Selection")
        cullItemsRemoveFromSelection = QtGui.QPushButton("Remove Items from Selection")
        cullItemsClear = QtGui.QPushButton("Clear all Items")

        # Configure Widgets
        self._sizeSlider.setValue(1)
        self._sizeSlider.setMinimum(1)
        self._sizeSlider.setMaximum(999)
        self._sizeSpinner.setValue(1)
        self._sizeSpinner.setMinimum(1)
        self._sizeSpinner.setMaximum(999)

        # Add Widgets to Layouts
        layout.addWidget(banner)
        layout.addWidget(self._enableToolButton)
        layout.addWidget(self._internalToolWidget)

        internalLayout.addWidget(QtGui.QLabel("Safe Sphere Radius:"), 0, 0)
        internalLayout.addWidget(self._sizeSlider, 1, 0)
        internalLayout.addWidget(self._sizeSpinner, 1, 1)
        internalLayout.addWidget(cullItemeWidget, 2, 0, 1, 2)
        internalLayout.addWidget(cullGeoButton, 3, 0, 1, 2)

        cullItemsLayout.addWidget(QtGui.QLabel("Items to Cull:"), 0, 0, 1, 3)
        cullItemsLayout.addWidget(self._cullItemsListBox, 1, 0, 1, 3)
        cullItemsLayout.addWidget(cullItemsAddFromSelection, 2, 0,)
        cullItemsLayout.addWidget(cullItemsClear, 2, 1)
        cullItemsLayout.addWidget(cullItemsRemoveFromSelection, 2, 2)

        # Set Layouts to Widgets
        self._internalToolWidget.setLayout(internalLayout)
        cullItemeWidget.setLayout(cullItemsLayout)
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

        # Hook up connections
        self._enableToolButton.pressed.connect(self._handleEnableDisableButton)
        cullGeoButton.pressed.connect(self._handleCullGeo)
        cullItemsAddFromSelection.pressed.connect(self._handleAddCullItems)
        cullItemsRemoveFromSelection.pressed.connect(self._handleRemoveCullItems)
        cullItemsClear.pressed.connect(self._handleClearCullItems)

        self._sizeSlider.valueChanged.connect(self._handleSizeSliderChange)
        self._sizeSpinner.valueChanged.connect(self._handleSizeSpinnerChange)

        # Final the UI step
        self._updateToolState()

    def focusInEvent(self, event):
        """
        Reimplemented Focus Event

        Force the UI to update its button states
        """
        self._updateToolState()

    def _getOrCreateCullingSphere(self, allowCreate=False):
        """
        Internal Method

        Get or Create a culling sphere and returns it if found

        args:
            allowCreate (bool): Create the culling sphere if one was not found

        return:
            The FBModel of Culling Sphere, or None if none is found
        """
        cullSphereName = "MOBU_CULLING_SPHERE"
        cullSphere = Scene.FindModelByName(cullSphereName)
        if cullSphere is None and allowCreate is True:
            cullSphere = mobu.FBCreateObject("Browsing/Templates/Elements/Primitives", "Sphere", "Sphere")
            cullSphere.Name = cullSphereName
            cullSphere.Show = True
            cullSphere.ShadingMode = mobu.FBModelShadingMode.kFBModelShadingWire
            cullSphere.Translation = self._previousPosition
            cullSphere.Selected = True
            self._setCullingSphereSize(self._sizeSlider.value())
        return cullSphere

    def _removeCullingSphere(self):
        """
        Internal Method

        Remove the a culling sphere and returns it if found
        """
        cullSphere = self._getOrCreateCullingSphere()
        if cullSphere is not None:
            self._previousPosition = Vector.CloneVector(cullSphere.Translation)
            cullSphere.FBDelete()

    def _getAllItemsToBeCulled(self):
        """
        Internal Method

        Get a list of all the FBModels that are in the list box

        returns:
            list of FBModels
        """
        return [self._cullItemsListBox.item(idx).data(QtCore.Qt.UserRole) for idx in xrange(self._cullItemsListBox.count())]

    def _setCullingSphereSize(self, newSize):
        """
        Internal Method

        Set the size of the culling sphere, as it has to be uniformed scaled

        args:
            newSize (int): The size to set
        """
        cullSphere = self._getOrCreateCullingSphere()
        cullSphere.Scaling.SetLocked(False)
        cullSphere.Scaling = mobu.FBVector3d(newSize, newSize, newSize)
        cullSphere.Scaling.SetLocked(True)

    def _updateToolState(self):
        """
        Internal Method

        Update the tool status, enableing or disabling the tool buttons based of the state of
        the scene
        """
        if self._getOrCreateCullingSphere() == None:
            self._internalToolWidget.setEnabled(False)
            self._enableToolButton.setText("Enable Culling Tool")
        else:
            self._internalToolWidget.setEnabled(True)
            self._enableToolButton.setText("Disable Culling Tool")

    @noRecursive.noRecursive
    def _handleSizeSliderChange(self, newValue):
        """
        Internal Method

        Handle the size slider change
        """
        self._sizeSpinner.setValue(newValue)
        self._setCullingSphereSize(newValue)

    @noRecursive.noRecursive
    def _handleSizeSpinnerChange(self, newValue):
        """
        Internal Method

        Handle the size spinner change
        """
        self._sizeSlider.setValue(newValue)
        self._setCullingSphereSize(newValue)

    def _handleClearCullItems(self):
        """
        Internal Method

        Handle the Clear Cull Item Button being pressed
        """
        self._cullItemsListBox.clear()

    def _handleAddCullItems(self):
        """
        Internal Method

        Handle the Add Cull Item Button being pressed
        """
        selected = Scene.GetSelection(mobu.FBModel)
        allCurrentItems = self._getAllItemsToBeCulled()
        cullSphere = self._getOrCreateCullingSphere()
        for item in selected:
            if item in allCurrentItems or item == cullSphere:
                continue
            newItem = QtGui.QListWidgetItem(item.Name)
            newItem.setData(QtCore.Qt.UserRole, item)
            self._cullItemsListBox.addItem(newItem)

    def _handleRemoveCullItems(self):
        """
        Internal Method

        Handle the Remove Cull Items Button being pressed
        """
        selected = Scene.GetSelection(mobu.FBModel)
        allCurrentItems = self._getAllItemsToBeCulled()
        counterOffset = 0
        for _idx in xrange(self._cullItemsListBox.count()):
            idx = _idx - counterOffset
            item = self._cullItemsListBox.item(idx).data(QtCore.Qt.UserRole)
            if item in selected:
                item = self._cullItemsListBox.takeItem(idx)
                del item
                counterOffset += 1

    def _handleEnableDisableButton(self):
        """
        Internal Method

        Handle the Enable/Disable Tool Button being pressed
        """
        cullSphere = self._getOrCreateCullingSphere()
        if cullSphere is None:
            self._getOrCreateCullingSphere(True)
        else:
            self._removeCullingSphere()
        self._updateToolState()

    def _handleCullGeo(self):
        """
        Internal Method

        Handle the cull geo button being pressed
        """
        allCurrentItems = self._getAllItemsToBeCulled()
        if len(allCurrentItems) == 0:
            QtGui.QMessageBox.critical(self, "Mesh Cull Tool", "No meshes given to cull against")
            return
        cullSphere = self._getOrCreateCullingSphere()
        if cullSphere is None:
            QtGui.QMessageBox.critical(self, "Mesh Cull Tool", "Unable to get CullSphere! Aborting")
            return

        radius = mobuExtender.GetRadiusOfMesh(cullSphere)
        for model in allCurrentItems:
            mobuExtender.CullMeshOutsideOfRadius(model, cullSphere.Translation.Data, radius, True)

        # Turn off the tool after use!
        self._handleEnableDisableButton()
