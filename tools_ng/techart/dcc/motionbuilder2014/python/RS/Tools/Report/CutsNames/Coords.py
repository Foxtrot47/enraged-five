###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_GetSceneInformation
## Written And Maintained By: Mike Wilson
## Contributors: Kathryn Bodey
## Description: Functions to extract infomartion from cut.xml files
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################


from ctypes import *
from pyfbsdk import *
import os

import RS.Config
import RS.Utils.Path

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Definition - Get Offsets
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

def Run():
    cdll.rexMBRage.GetCutsceneRotationOffset_Py.restype = c_float
    cdll.rexMBRage.GetCutscenePositionOffsetX_Py.restype = c_float
    cdll.rexMBRage.GetCutscenePositionOffsetY_Py.restype = c_float
    cdll.rexMBRage.GetCutscenePositionOffsetZ_Py.restype = c_float
    
    lFiles = RS.Utils.Path.Walk("{0}/assets/cuts/".format( RS.Config.Project.Path.Root ), 1, pPattern = 'data.cutxml')
    
    lOutput = open('c:/CutsceneOffset.csv', 'w')
    
    lString = ""
    
    for iFile in lFiles:
        
        lFileArray = iFile.split('/')
        lSceneName = lFileArray[len(lFileArray)-2]
        
        try:
            if "Shot_" in lSceneName:
                None
            else:
                lString = (lString + 
                          lSceneName + 
                          "," +
                          str(cdll.rexMBRage.GetCutscenePositionOffsetX_Py(iFile)) +
                          "," +
                          str(cdll.rexMBRage.GetCutscenePositionOffsetY_Py(iFile)) +
                          "," +
                          str(cdll.rexMBRage.GetCutscenePositionOffsetZ_Py(iFile)) +
                          "," +
                          str(cdll.rexMBRage.GetCutsceneRotationOffset_Py(iFile)) +
                          "\n")
        except:
            
            print "This data.cutxml does not work: " + lSceneName
        
        
    lOutput.write(lString)
    
    lOutput.close()
