###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_WatchTower_AssetList
## Written And Maintained By: Kathryn Bodey
## Description: Functions to extract information from P4 Depot
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from time import strftime
import os 
import csv 


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: rs_GetNamesFromP4Depot
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################   

def rs_GetNamesFromP4Depot(p4Dir):
    lNameArray = []
    lResult = os.popen('p4 files {0}....fbx'.format(p4Dir))
    
    for iLine in lResult:
        
        lFirst, lLast = str(iLine).split( '#' )
        
        if lFirst.lower().endswith( '.fbx' ) and not ( 'delete' in lLast ):
            
            lParts = lFirst.split( '/' )
            lName = lParts[-2] + ":" + lParts[-1]
            lNameSplit = lName.lower().split(".fbx")
        
            if "test" not in lNameSplit[0]:
                if "cockpit" not in lNameSplit[0]:
                    if "mocap stages" not in lNameSplit[0]:
                        if "rag_environment_grab" not in lNameSplit[0]:
                            if "dlc" not in lNameSplit[0]:
                                if "subref_complete_weapons" not in lNameSplit[0]:
                                    if ".bck" not in lNameSplit[0]:
                                        lNameArray.append(lNameSplit[0]) 

    return lNameArray

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Extract Information & Output to CSV
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
############################################################################################################### 

date = strftime('%d_%m')

lAssetArray = []
lMasterArray = []

lPathList = ["//depot/gta5/art/animation/resources/characters/Models/cutscene/", 
             "//depot/gta5/art/animation/resources/characters/Models/ingame/",
             "//depot/gta5/art/animation/resources/sets/",
             "//depot/gta5/art/animation/resources/characters/Models/Animals/",
             "//depot/gta5/art/animation/resources/props/", 
             "//depot/gta5/art/animation/resources/vehicles/"]

for iPath in lPathList:

    lAssetArray = rs_GetNamesFromP4Depot(iPath)

    for iAsset in lAssetArray:
        
        lAssetTypeArray = []
        lAssetNameArray = []
        
        lNameSplit = iAsset.split(":")
        lAssetType = lNameSplit[0]
        lAssetName = lNameSplit[-1]
                
        if lAssetType.lower() == "cutscene":
            lAssetType = "character:cutscene"

        elif lAssetType.lower() == "ingame":
            lAssetType = "character:ingame"

        elif lAssetType.lower() == "attachments":
            lAssetType = "weapons:attachment"
            
        elif lAssetType.lower() == "vehicles":
            None       

        elif lAssetType.lower() == "weapons":
            None   

        elif lAssetType.lower() == "sets":
            None   

        elif lAssetType.lower() == "animals":
            None 
                    
        else:
            lAssetType = "prop:" + lAssetType
        
        lAssetTypeArray.append(lAssetType)
        lAssetNameArray.append(iPath + lAssetName + '.fbx')

        lCSVFile = (open("C:\WatchTower_AssetLists_%s" %date + ".csv", "a"))
        lWriter = csv.writer(lCSVFile)
        lWriter.writerow(lAssetTypeArray + lAssetNameArray)    
        lCSVFile.close()    
