###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: rs_CutsNames_VFX
## Written And Maintained By: Kathryn Bodey
## Description: Functions to extract information from cut.xml files
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

from xml.dom import minidom
from time import strftime
import xml.dom.minidom
from subprocess import Popen, PIPE, call
import csv 
import os 

import RS.Config
import RS.Utils.Path
import RS.Perforce

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Define Directory
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################    

def Run():
    date = strftime('%d_%m')
    
    lDirectory = RS.Utils.Path.Walk("{0}//assets//cuts//".format( RS.Config.Project.Path.Root ), 1, pPattern = 'data.cutxml')
    
    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ##
    ## Description: Parse XML and Extract VFX Info. Output info into a text file.
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
    ###############################################################################################################
    
    for iFile in lDirectory:
    
        RS.Perforce.Sync( iFile )
        
        lLogFileFull = (open("C:\CUTFILE_VFX_FULL_%s" %date + ".txt", "a"))
        lLogFileVFX = (open("C:\CUTFILE_VFX_ONLY_%s" %date + ".txt", "a"))
    
        lAssetList = []
        
        lSceneNameSplit = iFile.split("\\")
        lSceneName = lSceneNameSplit[-2]
        lSceneName = "\n " + lSceneName
        
        try: 
            
            if "shot_" in lSceneName.lower():
                None
                
            else:
                
                lLogFileFull.write(lSceneName)
                
                lDoc = minidom.parse(iFile)
                       
                lCutObj = lDoc.getElementsByTagName("pCutsceneObjects")
                for iNodes in lCutObj:
                    for iNodeChildren in iNodes.childNodes:
                        if iNodeChildren.nodeType != iNodes.TEXT_NODE:
                            if iNodeChildren.attributes["type"].value == "rage__cutfAnimatedParticleEffectObject": 
                                lLogFileVFX.write(lSceneName)
                                for Node2 in iNodeChildren.childNodes:
                                    if Node2.nodeType != iNodeChildren.TEXT_NODE:
                                        if Node2.nodeName == "cName":
                                            lName = Node2.childNodes[0].nodeValue
                                            if ":" in lName:
                                                lNameSplit = lName.split(":")
                                                lAssetName = lNameSplit[0]
                                                lAssetName = "\n " + lAssetName
                                                lAssetList.append(str(lAssetName)) 
                                                    
                                                lLogFileVFX.write(lAssetName)
                                                lLogFileFull.write(lAssetName)
                                
                                
                if len(lAssetList) == 0:
                    lNoneString = "\n " + "none"
                    lLogFileFull.write("\n none")
                    
            lLogFileVFX.close()
            lLogFileFull.close()
        except:
            print "This data.cutxml does not work: " + lSceneName
                   