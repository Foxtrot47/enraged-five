import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Globals
from RS.Utils import Scene
from RS.Tools import UI
from RS.Core.Animation import Lib

from RS.Core.Scene.models import characterModelTypes, takeModelTypes


class OHPelvisSetupForExportDialog(UI.QtMainWindowBase):
    """
    Dialogue to run ControlRigWidget and retain size settings for opening and closing of the widget
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        self._settings = QtCore.QSettings( "RockstarSettings", "OHPelvisSetupForExportDialog" )
        super(OHPelvisSetupForExportDialog, self).__init__(parent=parent, title="OH_Pelvis Export Setup", dockable=True)
        self._dock.closeEvent = self.closeEvent
        self._dock.resize(self._settings.value("size", QtCore.QSize(600, 300)))
        controlRigWidget = OHPelvisSetupForExportWidget()

        # create layout and add widget
        self.setCentralWidget(controlRigWidget)

    def showEvent(self, event):
        self._dock.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self._dock.saveGeometry())
        self._settings.setValue("size", self._dock.size())
        return super(OHPelvisSetupForExportDialog, self).closeEvent(event)


class OHPelvisSetupForExportWidget(QtGui.QWidget):
    """
    Class to generate the widgets, and their functions, for the Toolbox UI
    """

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(OHPelvisSetupForExportWidget, self).__init__(parent=parent)
        self._mainCharacterModel = None
        self._mainTakeModel = None
        self.setupUi()

    def _handleCheckAllCharacters(self):
        for child in self._mainCharacterModel.rootItem.children():
            idx = self._mainCharacterModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self._mainCharacterModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainCharacterModel.dataChanged.emit(index0, index1)

    def _handleUncheckAllCharacters(self):
        for child in self._mainCharacterModel.rootItem.children():
            idx = self._mainCharacterModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self._mainCharacterModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainCharacterModel.dataChanged.emit(index0, index1)

    def _handleCheckAllTakes(self):
        for child in self._mainTakeModel.rootItem.children():
            idx = self._mainTakeModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self._mainTakeModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainTakeModel.dataChanged.emit(index0, index1)

    def _handleUncheckAllTakes(self):
        for child in self._mainTakeModel.rootItem.children():
            idx = self._mainTakeModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self._mainTakeModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
            self._mainTakeModel.dataChanged.emit(index0, index1)

    def _handleRunPelvisSetup(self):
        '''
        Generates the Character info List, using the 'CharacterInformation' class

        returns: List
        '''
        characterList = []
        takeList = []
        # generate character list
        for child in self._mainCharacterModel.rootItem.children():
            if child.GetCheckedState() is True:
                # get character object
                characterName = child.GetCharacterNamespace()
                if characterName is not None:
                    characterList.append(characterName)

        # generate character list
        for child in self._mainTakeModel.rootItem.children():
            if child.GetCheckedState() is True:
                # get character object
                takeObject = child.GetTakeObject()
                if takeObject is not None:
                    takeList.append(takeObject)

        self._handleUncheckAllTakes()
        self._handleUncheckAllCharacters()

        self.OHPelvisSetupForExport(characterList, takeList)

    def OHPelvisSetupForExport(self, characterList, takeList):
        if len(takeList) <= 0:
            takeList = [mobu.FBSystem().CurrentTake]

        # user selects take, or all takes
        for take in takeList:
            mobu.FBSystem().CurrentTake = take
            for characterName in characterList:
                # get required nodes
                pelvisControl = Scene.FindModelByName('{0}:OH_Pelvis_Control'.format(characterName), True)
                offsetNode = Scene.FindModelByName('{0}:Offset_Node'.format(characterName), True)
                offsetParentNode = Scene.FindModelByName('{0}:Offset_Parent_Node'.format(characterName), True)
                moverTracer = Scene.FindModelByName('{0}:Mover_Tracer'.format(characterName), True)
                zeroNode = Scene.FindModelByName('{0}:Zero_Node'.format(characterName), True)

                # find lock and export constraints and swap active
                lockConstraint = None
                exportConstraint = None
                for constraint in Globals.Constraints:
                    if constraint.LongName == '{0}:OH_Pelvis_Lock'.format(characterName):
                        lockConstraint = constraint
                    elif constraint.LongName == '{0}:OH_Pelvis_Export'.format(characterName):
                        exportConstraint = constraint

                if all([pelvisControl, offsetNode, offsetParentNode, moverTracer, zeroNode, lockConstraint, exportConstraint]):
                    # swap lock and export constraint active states
                    lockConstraint.Active = False
                    exportConstraint.Active = True

                    # remove keys from Control nulls
                    pelvisControlNullList = []
                    Scene.GetChildren(pelvisControl, pelvisControlNullList, "", True)
                    for control in pelvisControlNullList:
                        for nodeTransform in control.AnimationNode.Nodes:
                            for nodeAxis in nodeTransform.Nodes:
                                nodeAxis.FCurve.EditClear()

                    # go to first hard frame
                    mobu.FBPlayerControl().SnapMode = mobu.FBTransportSnapMode.kFBTransportSnapModeSnapOnFrames
                    startFrame = mobu.FBPlayerControl().LoopStart.GetTimeString()
                    mobu.FBPlayerControl().Goto(mobu.FBTime(0,0,0,int(startFrame)))
                    # double check offset node is zero'd - turn off constraint, so we can rotate
                    # offset node
                    pelvisConstraint = None
                    for constraint in Globals.Constraints:
                        if constraint.LongName == '{0}:pelvisOffset_parentChild'.format(characterName):
                            pelvisConstraint = constraint
                            break
                    if pelvisConstraint:
                        pelvisConstraint.Active = False
                        pelvisConstraint.Lock = False
                        Scene.Align(offsetNode, zeroNode, True, True, True, True, True, True)
                        mobu.FBSystem().Scene.Evaluate()
                        pelvisConstraint.Snap()
                        pelvisConstraint.Lock = True

                    # align zero node to mover tracer (rotation)
                    Scene.Align(zeroNode, moverTracer, False, False, False, True, True, True)
                    mobu.FBSystem().Scene.Evaluate()
                    # add a key
                    zeroNode.Translation.SetAnimated(True)
                    zeroNode.Rotation.SetAnimated(True)
                    animTrnsNodes = zeroNode.Translation.GetAnimationNode().Nodes
                    animRotNodes = zeroNode.Rotation.GetAnimationNode().Nodes

                    for idx in range(len(animTrnsNodes)):
                        fcurve = animTrnsNodes[idx].FCurve
                        fcurve.KeyAdd(mobu.FBTime(0,0,0,int(startFrame)), zeroNode.Translation[idx])
                    for idx in range(len(animRotNodes)):
                        fcurve = animRotNodes[idx].FCurve
                        fcurve.KeyAdd(mobu.FBTime(0,0,0,int(startFrame)), zeroNode.Rotation[idx])

                    # align offset parent to offset node (rotation and translation)
                    Scene.Align(offsetParentNode, offsetNode, True, True, True, True, True, True)
                    mobu.FBSystem().Scene.Evaluate()
                    # add a key
                    offsetParentNode.Translation.SetAnimated(True)
                    offsetParentNode.Rotation.SetAnimated(True)
                    animTrnsNodes = offsetParentNode.Translation.GetAnimationNode().Nodes
                    animRotNodes = offsetParentNode.Rotation.GetAnimationNode().Nodes

                    for idx in range(len(animTrnsNodes)):
                        fcurve = animTrnsNodes[idx].FCurve
                        fcurve.KeyAdd(mobu.FBTime(0,0,0,int(startFrame)), offsetParentNode.Translation[idx])
                    for idx in range(len(animRotNodes)):
                        fcurve = animRotNodes[idx].FCurve
                        fcurve.KeyAdd(mobu.FBTime(0,0,0,int(startFrame)), offsetParentNode.Rotation[idx])

    def setupUi(self):
        """
        Sets up the widgets, their properties and launches call events
        """
        # create layout
        mainLayout = QtGui.QGridLayout()

        # widgets
        self.characterTree = QtGui.QTreeView()
        self.takeTree = QtGui.QTreeView()
        checkAllChar = QtGui.QPushButton()
        clearAllChar = QtGui.QPushButton()
        checkAllTake = QtGui.QPushButton()
        clearAllTake = QtGui.QPushButton()
        horizontalLine = QtGui.QFrame()
        setupButton = QtGui.QPushButton()

        # widget properties
        checkAllChar.setText("Check All")
        checkAllChar.setMaximumHeight(15)
        clearAllChar.setText("Uncheck All")
        clearAllChar.setMaximumHeight(15)
        checkAllTake.setText("Check All")
        checkAllTake.setMaximumHeight(15)
        clearAllTake.setText("Uncheck All")
        clearAllTake.setMaximumHeight(15)
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        setupButton.setText("Setup OH_Pelvis for Export")

        # tree for characters
        self._mainCharacterModel =  characterModelTypes.CharacterModel()
        self.treeViewCharacter = QtGui.QTreeView()
        self.treeViewCharacter.setModel(self._mainCharacterModel)
        # tree properties
        self.treeViewCharacter.setAlternatingRowColors(True)
        self.treeViewCharacter.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeViewCharacter.setSortingEnabled(True)
        self.treeViewCharacter.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)

        # tree for takes
        self._mainTakeModel =  takeModelTypes.TakeModel()
        self.treeViewTake = QtGui.QTreeView()
        self.treeViewTake.setModel(self._mainTakeModel)
        # tree properties
        self.treeViewTake.setAlternatingRowColors(True)
        self.treeViewTake.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeViewTake.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)

        # add widgets to layout
        mainLayout.addWidget(self.treeViewCharacter, 0, 0, 1, 3)
        mainLayout.addWidget(checkAllChar, 1, 1)
        mainLayout.addWidget(clearAllChar, 1, 2)
        mainLayout.addWidget(self.treeViewTake, 0, 3, 1, 3)
        mainLayout.addWidget(checkAllTake, 1, 4)
        mainLayout.addWidget(clearAllTake, 1, 5)
        mainLayout.addWidget(horizontalLine, 2, 0, 1 ,6)
        mainLayout.addWidget(setupButton, 3, 0, 1 ,6)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        setupButton.released.connect(self._handleRunPelvisSetup)
        checkAllChar.released.connect(self._handleCheckAllCharacters)
        clearAllChar.released.connect(self._handleUncheckAllCharacters)
        checkAllTake.released.connect(self._handleCheckAllTakes)
        clearAllTake.released.connect(self._handleUncheckAllTakes)

