from pyfbsdk import *
import RS.Globals as glo



def rs_PreviewHighHeels(pControl, pEvent):
        
    lCharacter = glo.gApp.CurrentCharacter
    
    for iConstataint in glo.gConstraints:
        
        if iConstataint.Name == "HighHeel_Relation":
            
            print iConstataint.LongName
            
            iConstataint.Active = True
            glo.gScene.Evaluate()
        
    if iConstataint and iConstataint.Active == True:   
        # Only continue if the constraint is present and active 
        for iGroup in glo.gGroups:
            
            if iGroup.Name == "HighHeel_Slider" or iGroup.Name == "HighHeel_Proxies":
                
                iGroup.Show = True
                glo.gScene.Evaluate()
            
        for iCamera in FBSystem().Scene.Cameras:
            iCamera.PropertyList.Find("BackgroundColor").Data = FBColor(0.3, 0.0, 0.0)
            
            
def rs_DeactivateHighHeels(pControl, pEvent):
    
    lCharacter = glo.gApp.CurrentCharacter
    
    for iConstataint in glo.gConstraints:
        
        if iConstataint.Name == "HighHeel_Relation":
            
            print iConstataint.LongName
            
            iConstataint.Active = False
            glo.gScene.Evaluate()
    
    for iGroup in glo.gGroups:
        
        if iGroup.Name == "HighHeel_Slider" or iGroup.Name == "HighHeel_Proxies":
            
            iGroup.Show = False
            glo.gScene.Evaluate()
            
    for iCamera in FBSystem().Scene.Cameras:
        
        iCamera.PropertyList.Find("BackgroundColor").Data = FBColor(0.6, 0.6, 0.6)
        
        foundBackplate = False
        for iVid in FBSystem().Scene.VideoClips:
            if iVid.Name == "HeelsBackdrop.jpg":
                try:
                    iVid.FBDelete()
                    #iVid.Selected = True
                    break
                except:
                    print "Unable to delete backplate image"


        



