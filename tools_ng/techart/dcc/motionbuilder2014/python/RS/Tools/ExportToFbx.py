"""
Module for moving components in a scene to another fbx file
"""
import os
import re
import shutil
import tempfile
import time
from PySide import QtGui

import pyfbsdk as mobu

from RS import Globals, Config
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem import Manager
from RS.Tools.UI import ExternalProgressBar


def Dialog(func):
    """
    Decorator for creating a Qt Dialog that prompts the user to confirm that the shown path is the correct path to save
    the moved file to after they have been moved.

    Arguments:
        func (method): method that is wrapped by this function

    """
    def wrapper(*args, **kwargs):

        exportPath = ""
        promptDialog = kwargs.pop("dialog", False)
        multiplier = 1

        if not promptDialog:
            func(*args, **kwargs)
            return

        path = Globals.Application.FBXFileName
        path = ResolvePath(path)

        messageBox = QtGui.QMessageBox()
        messageBox.setWindowTitle("Save {} to New File".format(func.__name__))
        messageBox.setText(("Save {} to the following file:\n"
                            "{}").format(func.__name__.lower(), path))
        messageBox.setIcon(QtGui.QMessageBox.Question)
        messageBox.addButton("No", QtGui.QMessageBox.RejectRole)
        messageBox.addButton("Yes", QtGui.QMessageBox.AcceptRole)

        if path:
            messageBox.addButton("Save As ...", QtGui.QMessageBox.DestructiveRole)

        elif not path or "previz" not in path:
            messageBox.setIcon(QtGui.QMessageBox.Warning)
            messageBox.setText(("\t"
                                "This is NOT a previz file !\n"
                                "Do you still wish to save {} in this scene to the another file?\n"
                                ).format(func.__name__.lower()))
            multiplier = 2

        messageBox.exec_()
        result = messageBox.result() * multiplier

        if not result:
            return

        # 1 is the result returned when 'Yes' is pressed on a valid previz file
        elif result == 1:
            exportPath = path

        # 2 is the result returned when 'Save As ...' is pressed or Yes is pressed on a non-previz file
        elif result == 2:
            exportPath, _ = QtGui.QFileDialog.getSaveFileName(None, "Save Characters to ...", "*.fbx")
            exportPath = str(exportPath)

        if exportPath:
            func(*args, **kwargs)
            exportDirectory = os.path.dirname(exportPath)
            if not os.path.exists(exportDirectory):
                os.makedirs(exportDirectory)
            # False sets the default save options for Mobu
            Globals.Application.FileSave(exportPath, mobu.FBFbxOptions(False))

    return wrapper


def ResolvePath(path):
    """
    Takes the given path and attempts to resolve the path where it most likely should be saved out to.
    Only works for files with _gc_ in the name.

    Arguments:
        path (string): path to resolve

    Return:
        tuple(string, list); a tuple where the first value is the resolved path and the second value is a list of
                            directories missing from the local machine that are part of the resolved path.
    """
    # TODO: Add support for linux

    root = os.path.join("X:\\", str(Config.Project.Name).lower(),"art", "animation", "ingame","source", "cnv_camp")
    _, filename = os.path.split(path)
    finalPath = ""

    if "_gc_" in filename.lower():
        _, name = filename.lower().split("_gc_")
        finalPath = root
        for section in name.split("_"):
            section, _ = os.path.splitext(section)
            if re.search("P[0-9]+", section, re.I):
                finalPath = "{}_{}".format(finalPath, section)
            else:
                finalPath = os.path.join(finalPath, "@{}".format(section))
        finalPath = os.path.join(finalPath, "CNV.fbx")

    return finalPath


@Dialog
def Characters(dialog=False, useProgressBar=True):
    """
    Exports animations from control rigs using Mobu's native animation saving/loading logic and then in a new
    empty scene re-references the rigs and loads in the animations.

    Arguments:
        dialog (boolean): prompt user for file path to save scene to. This parameter is used by the Dialog decorator
        progressBar (boolean): shows progress of the save/load process

    """
    progressBar = None
    if useProgressBar:
        progressBar = ExternalProgressBar.Run()

    tempDirectory = tempfile.mkdtemp()

    manager = Manager.Manager()
    manager.SetSilent(True)
    paths = []
    referencedCharacters = manager.GetReferenceListByType(Manager.Character)

    if useProgressBar:
        # The '+2' is to account for the 'Opening New Scene' & 'Done' messages

        progressBar.Maximum = (len(referencedCharacters) * 2) + (len(referencedCharacters) * len(Globals.Takes)) + 3
        print progressBar.Maximum
    for referencedCharacter in referencedCharacters:
        character = referencedCharacter.Component()

        if useProgressBar:
            progressBar.Update(text="Saving Animations for {}".format(character.LongName), value=progressBar.Value + 1)

        path = os.path.join(tempDirectory, "{}.fbx".format(character.Name))

        # Pausing Motion Builder prevents it from crashing
        time.sleep(.1)

        Lib.SaveCharacterAnimationToFbx(path, character)
        paths.append((path, referencedCharacter.Path))

    if useProgressBar:
        progressBar.Update(text="Opening New Scene", value=progressBar.Value + 1)

    Globals.Application.FileNew()
    characters = []
    for animationPath, referencePath in paths:
        newReferencedCharacter = manager.CreateReferences(referencePath)[0]
        character = newReferencedCharacter.Component()

        if useProgressBar:
            progressBar.Update(text="Loading Animations for {}".format(character.LongName),
                               value=progressBar.Value + 1)

        Lib.LoadCharacterAnimationFromFbx(animationPath, newReferencedCharacter._getCharacterComponent())
        for take in Globals.Takes:
            Globals.System.CurrentTake = take
            if useProgressBar:
                progressBar.Update(text="Plotting Animation to Skeleton for {} Take {}".format(character.LongName, take.Name),
                                   value=progressBar.Value + 1)

            Lib.Plot(character, "skeleton")
        os.remove(animationPath)

    Globals.Scene.Evaluate()

    Lib.PlotCharacters(characters, plotLocations=["skeleton"], muteStory=True, takes=Globals.Takes, progressBar=progressBar)
    shutil.rmtree(tempDirectory)

    if useProgressBar:
        progressBar.Update(text="Done", value=progressBar.Maximum)
