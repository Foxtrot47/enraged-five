"""
Main ShortcutKeys Dialog
"""
import os

from PySide import QtGui, QtCore

from RS.Core.ShortcutKeys import core
from RS.Tools import UI
from RS.Tools.ShortcutKeys.Widgets import codeWidget, keyAssignWidget


class ShortcutKeysExportDialog(QtGui.QDialog):
    """
    Dialog to give the user the option of what shortcuts to export
    """
    def __init__(self, parent=None):
        super(ShortcutKeysExportDialog, self).__init__(parent=parent)
        self._shortcutManager = core.ShortcutKeysManager()
        self.setupUi()
    
    def setupUi(self):
        """
        Set up the Ui
        """
        # Layouts
        mainLayout = QtGui.QGridLayout()
        
        # Widgets
        self._keysTree = QtGui.QTreeWidget()
        exportButton = QtGui.QPushButton("Export Selected")
        cancelButton = QtGui.QPushButton("Cancel")
        
        # Widgets to Layouts
        mainLayout.addWidget(self._keysTree, 0, 0, 1, 2)
        mainLayout.addWidget(exportButton, 1, 0)
        mainLayout.addWidget(cancelButton, 1, 1)
        
        self.setLayout(mainLayout)
        
        # Signals
        exportButton.pressed.connect(self._handleExportButtonPress)
        cancelButton.pressed.connect(self.reject)
        
    def _populateTree(self):
        """
        Internal method
        
        Populate the tree with the categories and commands
        """
        self._keysTree.clear()
        for category in self._shortcutManager.getAllCategories():
            catItem = QtGui.QTreeWidgetItem([category])
            catItem.setFlags(catItem.flags() | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsTristate)
            for command in self._shortcutManager.getShortcutsInCategory(category):
                if isinstance(command, core.MotionBuilderBuiltInShortcutHotkey):
                    continue
                commandItem = QtGui.QTreeWidgetItem([command.name()])
                commandItem.setFlags(commandItem .flags() | QtCore.Qt.ItemIsUserCheckable)
                commandItem.setCheckState(0, QtCore.Qt.Checked)
                commandItem.setData(0, QtCore.Qt.UserRole, command)
                catItem.addChild(commandItem)
            if catItem.childCount() > 0:
                self._keysTree.addTopLevelItem(catItem)
        
        self._keysTree.expandAll()

    def showEvent(self, event):
        """
        Re-Implemented
        
        show event, to populate the tree incase there has been updates
        """
        self._populateTree()
        return super(ShortcutKeysExportDialog, self).showEvent(event)

    def _handleExportButtonPress(self):
        """
        Internal method
        
        Handle the export process, getting the checked items and the outfile path and passing that
        to the shortcutManager
        """
        keysToExport = []
        for catIdx in xrange(self._keysTree.topLevelItemCount()):
            catItem = self._keysTree.topLevelItem(catIdx)
            for cmdIdx in xrange(catItem.childCount()):
                commandItem = catItem.child(cmdIdx)
                if not commandItem.checkState(0) == QtCore.Qt.Checked:
                    continue
                keysToExport.append(commandItem.data(0, QtCore.Qt.UserRole))
        
        filePath, ok = QtGui.QFileDialog.getSaveFileName(self, "Export shortcut keys as...", filter="Mobu Shortcut Keys (*.xml)")
        if ok:
            self._shortcutManager.exportShortcutKeys(filePath, keysToExport)
            self.accept()


class ShortcutKeysDialog(UI.QtMainWindowBase):
    """
    Ui for the shortcut keys tool
    """
    def __init__(self):
        self._currentShortcut = None
        self._toolName = "Shortcut Keys Editor"
        self._shortcutManager = core.ShortcutKeysManager()
        self._shortcutManager.loadKeys()
        super(ShortcutKeysDialog, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def _setupActions(self):
        """
        Internal Method

        Setup all the actions, with the connections and icons
        """
        # File actions
        self.ac_fileExportKeys = QtGui.QAction('Export Shortcut Keys', self)
        self.ac_fileExportKeys.triggered.connect(self._handleFileExportKeys)

        self.ac_fileImportKeys = QtGui.QAction('Import Shortcut Keys', self)
        self.ac_fileImportKeys.triggered.connect(self._handleFileImportKeys)
    
    def _addActionsToMenu(self, menuToAdd, actions):
        """
        Internal Method

        Helper method to add a list of actions to a menuw
        if an action in the list of actions is None, then a separator is added
        """
        for action in actions:
            if action is None:
                menuToAdd.addSeparator()
                continue
            menuToAdd.addAction(action)

    def _setupMenu(self):
        """
        Internal Method

        Setup the menu's using the actions which were created
        """
        menubar = QtGui.QMenuBar()
        self.layout().setMenuBar(menubar)

        # File Menu
        fileMenu = menubar.addMenu('File')
        self._addActionsToMenu(fileMenu, [self.ac_fileImportKeys, self.ac_fileExportKeys])

    def setupUi(self):
        """
        setup the ui
        """
        # Layouts
        mainLayout = QtGui.QGridLayout()
        categoriesLayout = QtGui.QGridLayout()
        infoLayout = QtGui.QGridLayout()
        buttonLayout = QtGui.QVBoxLayout()
        dialogButtonsLayout = QtGui.QHBoxLayout()
        radioButtonLayout = QtGui.QHBoxLayout()
        codeStackedLayout = QtGui.QVBoxLayout()
        fileStackedLayout = QtGui.QHBoxLayout()
        
        # Widgets
        mainWidget = QtGui.QWidget()
        categoryWidget = QtGui.QWidget()
        self._categoriesList = QtGui.QTreeWidget()
        self._commandsList = QtGui.QTreeWidget()
        
        descriptionLabel = QtGui.QLabel("Description:")
        nameLabel = QtGui.QLabel("Name:")
        categoryLabel = QtGui.QLabel("Category:")
        self._newButton = QtGui.QPushButton("New")
        self._updateButton = QtGui.QPushButton("Edit")
        self._testButton = QtGui.QPushButton("Test")
        self._deleteButton = QtGui.QPushButton("Delete")
        self._helpButton = QtGui.QPushButton("Help")
        
        self._infoWidget = QtGui.QWidget()
        self._keyAssignWidget = keyAssignWidget.KeyAssignWidget()
        self._nameTextBox = QtGui.QLineEdit()
        self._descriptionTextBox = QtGui.QLineEdit()
        self._categoryCombo = QtGui.QComboBox()
        addNewCategory = QtGui.QPushButton("+")
        self._codeRadio = QtGui.QRadioButton("Code")
        self._fileRadio = QtGui.QRadioButton("File")
        self._executeStacked = QtGui.QStackedWidget()
        
        self._codeEditor = codeWidget.CodeEditor()
        self._fileToRunTextBox = QtGui.QLineEdit()
        browseForFileButton = QtGui.QPushButton("...")
        
        closeButton = QtGui.QPushButton("Close")
        
        # Configure Widgets
        for but in [self._newButton, self._updateButton, self._deleteButton, self._helpButton]:
            but.setMaximumWidth(70)
        categoryWidget.setMaximumHeight(250)
        addNewCategory.setMaximumWidth(30)
        self._categoriesList.setHeaderHidden(True)
        self._commandsList.setHeaderHidden(True)
        
        browseForFileButton.setMaximumWidth(30)
        self._codeRadio.setChecked(True)
        for stackedLay in [codeStackedLayout, fileStackedLayout]:
            newWid = QtGui.QWidget()
            newWid.setLayout(stackedLay)
            self._executeStacked.addWidget(newWid)
        
        for wid in [nameLabel, descriptionLabel, categoryLabel]:
            wid.setMaximumWidth(70)
        self._keyAssignWidget.setMaximumWidth(200)
        self._helpButton.setEnabled(False)
        
        # Assign Widgets to Layouts
        categoriesLayout.addWidget(QtGui.QLabel("Categories:"), 0, 0)
        categoriesLayout.addWidget(self._categoriesList, 1, 0)
        categoriesLayout.addWidget(QtGui.QLabel("Commands:"), 0, 1)
        categoriesLayout.addWidget(self._commandsList, 1, 1)
        
        buttonLayout.addWidget(self._newButton)
        buttonLayout.addWidget(self._updateButton)
        buttonLayout.addWidget(self._testButton)
        buttonLayout.addWidget(self._deleteButton)
        buttonLayout.addWidget(self._helpButton)
        
        radioButtonLayout.addWidget(self._codeRadio)
        radioButtonLayout.addWidget(self._fileRadio)

        infoLayout.addWidget(nameLabel, 0, 0)
        infoLayout.addWidget(self._nameTextBox, 0, 1, 1, 2)
        infoLayout.addWidget(descriptionLabel, 1, 0)
        infoLayout.addWidget(self._descriptionTextBox, 1, 1, 1, 2)
        infoLayout.addWidget(categoryLabel, 2, 0)
        infoLayout.addWidget(self._categoryCombo, 2, 1)
        infoLayout.addWidget(self._keyAssignWidget, 0, 3, 3, 1)
        infoLayout.addWidget(addNewCategory, 2, 2)
        infoLayout.addLayout(radioButtonLayout, 3, 0, 1, 4)
        infoLayout.addWidget(self._executeStacked, 4, 0, 1, 4)
        
        codeStackedLayout.addWidget(QtGui.QLabel("Code:"))
        codeStackedLayout.addWidget(self._codeEditor)
        
        fileStackedLayout.addWidget(self._fileToRunTextBox)
        fileStackedLayout.addWidget(browseForFileButton)
        
        dialogButtonsLayout.addWidget(closeButton )
        
        mainLayout.addWidget(categoryWidget, 0, 0, 1, 2)
        mainLayout.addLayout(buttonLayout, 1, 1)
        mainLayout.addWidget(self._infoWidget, 1, 0)
        mainLayout.addLayout(dialogButtonsLayout, 3, 0, 1, 2)
        
        categoryWidget.setLayout(categoriesLayout)
        self._infoWidget.setLayout(infoLayout)
        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)

        # Signals
        self._codeRadio.clicked.connect(self._handleCodeRadioButtonChange)
        self._fileRadio.clicked.connect(self._handleCodeRadioButtonChange)
        self._categoriesList.itemClicked.connect(self._handleCategorySelected)
        self._commandsList.itemClicked.connect(self._handleCommandSelected)
        self._newButton.pressed.connect(self._handleNewShortcutButton)
        self._updateButton.pressed.connect(self._handleUpdateCurrentShortcutButton)
        self._deleteButton.pressed.connect(self._handleDeleteCurrentShortcutButton)
        self._testButton.pressed.connect(self._handleTestButton)
        addNewCategory.pressed.connect(self._handleAddNewCategory)
        closeButton.pressed.connect(self.close)
        
        self.populateCategories()
        self.setCurrentShortcut(None)
        self._setMayaKeyboardScheme()
        self._setMobuKeyboardScheme()
    
        # Add Menu Bar
        self._setupActions()
        self._setupMenu()
    
    def populateCategories(self):
        """
        Populate the categories from the keyboard shortcut core
        """
        selected = self._categoriesList.selectedItems()
        currentText = None
        if len(selected) > 0:
            currentText = selected[0].text(0)
        self._categoriesList.clear()
        
        for item in self._shortcutManager.getAllCategories():
            newItem = QtGui.QTreeWidgetItem([item])
            self._categoriesList.addTopLevelItem(newItem)
            if currentText == item:
                newItem.setSelected(True)
    
    def _populateCommands(self):
        """
        Internal Method
        
        Populate the commands for the currently selected category
        """
        self._commandsList.clear()
        
        selected = self._categoriesList.selectedItems()
        currentText = None
        if len(selected) == 0:
            return
        cat = selected[0].text(0)
        
        for item in self._shortcutManager.getShortcutsInCategory(cat):
            newItem = QtGui.QTreeWidgetItem([item.name()])
            newItem.setData(0, QtCore.Qt.UserRole, item)
            self._commandsList.addTopLevelItem(newItem)

    def _setMayaKeyboardScheme(self):
        """
        Internal Method
        
        Sets the currently keyboard scheme to match Maya's scheme.
        """
        shortcutName = 'Maya Keyboard'
        allShortcutNames = [item._name for item in self._shortcutManager.getShortcutsInCategory('viewer')]
        if shortcutName not in allShortcutNames:
            self._shortcutManager.addNewShortcut(shortcutName, 'F11', 'mocapCommand.ChangeKeyboardConfiguration("Maya")', None, True, category='viewer', description='Keyboard scheme for Maya')

    def _setMobuKeyboardScheme(self):
        """
        Internal Method
        
        Sets the currently keyboard scheme to match Motionbuilder's scheme.
        """
        shortcutName = 'Mobu Keyboard'
        allShortcutNames = [item._name for item in self._shortcutManager.getShortcutsInCategory('viewer')]
        if shortcutName not in allShortcutNames:
            self._shortcutManager.addNewShortcut(shortcutName, 'F12', 'mocapCommand.ChangeKeyboardConfiguration("Motionbuilder")', None, True, category='viewer', description='Keyboard scheme for Motionbuilder')

    def _handleAddNewCategory(self):
        """
        Internal Method
        
        Handle the new add new category Button click
        """
        value, ok = QtGui.QInputDialog.getText(self, "New Category", "Enter new Category Name:")
        if ok is False:
            return
        self._categoryCombo.addItem(value)
        self._categoryCombo.setCurrentIndex(self._categoryCombo.count()-1)
    
    def _handleCommandSelected(self):
        """
        Internal Method
        
        Handle a command being selected
        """
        self.setCurrentShortcut(self._commandsList.selectedItems()[0].data(0, QtCore.Qt.UserRole))
        
    def _handleCategorySelected(self):
        """
        Internal Method
        
        Handle a category being selected
        """
        self._populateCommands()
    
    def _handleNewShortcutButton(self):
        """
        Internal Method
        
        Handle the new shortcut Button click
        """
        newShortcut = core.ShortcutHotkey("<No Name>", "", "", "")
        self.setCurrentShortcut(newShortcut)
        self._infoWidget.setEnabled(True)
        self._updateButton.setText("Save")
    
    def _handleTestButton(self):
        """
        Internal Method
        
        Handle the test shortcut Button click
        """
        self._updateCurrentShortcut()
        self._currentShortcut.execute()
    
    def _handleDeleteCurrentShortcutButton(self):
        """
        Internal Method
        
        Handle the delete shortcut Button click
        """
        self._shortcutManager.removeShortcut(self._currentShortcut)
        self.populateCategories()
        self._populateCommands()
        self.setCurrentShortcut(None)
    
    def _handleUpdateCurrentShortcutButton(self):
        """
        Internal Method
        
        Handle the edit/save shortcut Button click
        """
        if self._infoWidget.isEnabled():
            self._updateCurrentShortcut()
            self._infoWidget.setEnabled(False)
            self._updateButton.setText("Edit")
        else:
            self._infoWidget.setEnabled(True)
            self._updateButton.setText("Save")

    def _handleCodeRadioButtonChange(self):
        """
        Internal Method
        
        Handle the toggeling of code to file radio buttons
        """
        if self._codeRadio.isChecked():
            self._executeStacked.setCurrentIndex(0)
            self._currentShortcut.setRunCodeWhenExecuted(True)
        else:
            self._executeStacked.setCurrentIndex(1)
            self._currentShortcut.setRunCodeWhenExecuted(False)

    def _updateCurrentShortcut(self):
        """
        Internal Method
        
        Update the core with the new shortcut details, such as name, keys, description, code etc
        """
        self._currentShortcut.setName(self._nameTextBox.text())
        self._currentShortcut.setDescription(self._descriptionTextBox.text())
        self._currentShortcut.setCategory(self._categoryCombo.currentText())
        self._currentShortcut.setRunCodeWhenExecuted(self._codeRadio.isChecked())
        self._currentShortcut.setFileToRun(self._fileToRunTextBox.text())
        self._currentShortcut.setCodeToRun(self._codeEditor.toPlainText())
        self._currentShortcut.setKeys(self._keyAssignWidget.generateKeySequence())
        self._shortcutManager.updateShortcut(self._currentShortcut)
        
        self.populateCategories()
        self._populateCommands()
    
    def _clearDetails(self):
        """
        Internal Method
        
        Clear all the details from the info widget
        """
        self._nameTextBox.clear()
        self._categoryCombo.clear()
        self._descriptionTextBox.clear()
        self._codeEditor.clear()
        self._fileToRunTextBox.clear()
        self._codeRadio.setChecked(True)
        self._keyAssignWidget.clear()
    
    def setCurrentShortcut(self, shortcut):
        """
        Set the current shortcut to be shown in the info widget
        
        args:
            shortcut (ShortcutHotkey): The shortcut to be active for editing and viewing
        """
        self._clearDetails()
        self._currentShortcut = shortcut
        self._updateButton.setText("Edit")
        self._infoWidget.setEnabled(False)

        if shortcut is None:
            self._updateButton.setEnabled(False)
            self._testButton.setEnabled(False)
            self._deleteButton.setEnabled(False)
            return

        self._updateButton.setEnabled(shortcut.editable())
        self._testButton.setEnabled(shortcut.editable())
        self._deleteButton.setEnabled(shortcut.editable())
        
        self._categoryCombo.addItems(self._shortcutManager.getAllCategories())
        
        self._nameTextBox.setText(self._currentShortcut.name())
        self._descriptionTextBox.setText(self._currentShortcut.description())
        idx = self._categoryCombo.findText(self._currentShortcut.category())
        if idx >= 0:
            self._categoryCombo.setCurrentIndex(idx)
        else:
            self._categoryCombo.setCurrentIndex(-1)
        
        self._keyAssignWidget.setCurrentShortcut(self._currentShortcut)
        
        if self._currentShortcut.runCodeWhenExecuted():
            self._codeRadio.setChecked(True)
            self._fileRadio.setChecked(False)
        else:
            self._codeRadio.setChecked(False)
            self._fileRadio.setChecked(True)
            
        self._fileToRunTextBox.setText(self._currentShortcut.fileToRun())
        self._codeEditor.setPlainText(self._currentShortcut.codeToRun())
        
        self._handleCodeRadioButtonChange()

    def _handleFileExportKeys(self):
        """
        Internal Method
        
        handle the Export action being triggered
        """
        ShortcutKeysExportDialog().exec_()
    
    def _handleFileImportKeys(self):
        """
        Internal Method
        
        handle the Import action being triggered
        """
        filePath, ok = QtGui.QFileDialog.getOpenFileName(self, "Import shortcut keys", filter="Mobu Shortcut Keys (*.xml)")
        if ok:
            self._shortcutManager.importShortcutKeys(filePath)
            self.populateCategories()
            self._populateCommands()


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wid = ShortcutKeysDialog()
    wid.show()
    sys.exit(app.exec_())
