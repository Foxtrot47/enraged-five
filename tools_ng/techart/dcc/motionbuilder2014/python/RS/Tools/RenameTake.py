'''Script Name RenameCurrentTake.py
Original Author Stephen Sloper, Rockstar Toronto.
Modified Author Kristine Middlemiss, Rockstar Toronto.

Description     Loads UI to quickly rename the selected take.'''

from pyfbsdk import *
from pyfbsdk_additions import *

def rs_renametake(control, event):
    # Error Handling    
    if control.Name == "":
        FBMessageBox("rs_renametake", "Nothing was selected", "OK")
        return      
    if control.SearchValue == "" or control.ReplaceValue == "":
        FBMessageBox("rs_renametake", "You need a value in the search and replace fields.", "OK")
        return 
    lSystem = FBSystem()            
    for iControl in control.Name.split(","):
        iControl = iControl.replace("[", "", len(iControl))
        iControl = iControl.replace("]", "", len(iControl))
        iControl = iControl.replace("'", "", len(iControl))
        if iControl.startswith(" "):
            iControl = iControl[1:len(iControl)]
        for iTake in lSystem.Scene.Takes:
            if iTake.Name == iControl: 
                if control.SearchValue in iTake.Name:
                    iTake.Name = iTake.Name.replace(control.SearchValue,control.ReplaceValue)

