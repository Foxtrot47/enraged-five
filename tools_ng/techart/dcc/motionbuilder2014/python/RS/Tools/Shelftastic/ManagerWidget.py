"""
Manager Widget for the shelf tool
"""
__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded

from PySide import QtCore, QtGui


class ShelftasticManagerWindow(QtGui.QMainWindow):
    """
    Dialog to allow users to add, rename, delete, save and load shelfs from the shelf tool
    """
    def __init__(self, dataBinding, parent=None):
        """
        Constructor
        
        Args:
            dataBinding (Shelftastic): The shelf widget to bind too.
        
        Kwargs:
            parent (QWidet): The parent of the dialog window
        """
        super(ShelftasticManagerWindow, self).__init__(parent=parent)
        self._dataBinding = dataBinding
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.setupUi()
        
    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QGridLayout()
        shelfGroupBoxLayout = QtGui.QVBoxLayout()
        buttonGroupBoxLayout = QtGui.QVBoxLayout()
        
        # Widgets
        self._shelfsWidget = QtGui.QTreeWidget()
        self._buttonsWidget = QtGui.QTreeWidget()
        shelfGroupBox = QtGui.QGroupBox("Shelf Options")
        buttonGroupBox = QtGui.QGroupBox("Button Options")
        
        renameShelfButton = QtGui.QPushButton("Rename Selected Shelf")
        deleteShelfButton = QtGui.QPushButton("Delete Selected Shelves")
        newShelfButton = QtGui.QPushButton("New Shelf")
        loadShelfButton = QtGui.QPushButton("Load Shelves")
        saveShelfButton = QtGui.QPushButton("Share Selected Shelves")
        
        newButtonButton = QtGui.QPushButton("New Button")
        editButtonButton = QtGui.QPushButton("Edit Selected Button")
        deleteButtonButton = QtGui.QPushButton("Delete Selected Button")
        
        # Configure Widgets
        self._shelfsWidget.setHeaderLabels(["Shelf Names", "Sourced from", "Locked?"])
        self._shelfsWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self._shelfsWidget.setAlternatingRowColors(True)
        self._shelfsWidget.setSortingEnabled(False)
        
        self._buttonsWidget.setHeaderLabels(["Button Names"])
        self._buttonsWidget.setAlternatingRowColors(True)
        self._buttonsWidget.setSortingEnabled(False)
        
        # Assign Widgets to Layotus
        shelfGroupBox.setLayout(shelfGroupBoxLayout)
        for item in [renameShelfButton, deleteShelfButton, newShelfButton, 
                     loadShelfButton,saveShelfButton]:
            shelfGroupBoxLayout.addWidget(item)
        
        buttonGroupBox.setLayout(buttonGroupBoxLayout)
        for item in [newButtonButton, editButtonButton, deleteButtonButton]:
            buttonGroupBoxLayout.addWidget(item)
        
        layout.addWidget(self._shelfsWidget, 0, 0)
        layout.addWidget(self._buttonsWidget, 0, 1)
        layout.addWidget(shelfGroupBox, 1, 0)
        layout.addWidget(buttonGroupBox, 1, 1)

        # Setup Connections
        saveShelfButton.pressed.connect(self._handleSaveShelfButton)
        renameShelfButton.pressed.connect(self._handleRenameShelfButton)
        deleteShelfButton.pressed.connect(self._handleDeleteShelfButton)
        newShelfButton.pressed.connect(self._handleNewShelfButton)
        loadShelfButton.pressed.connect(self._handleLoadShelfButton)
        self._shelfsWidget.currentItemChanged.connect(self._populateButtonData)
        
        newButtonButton.pressed.connect(self._handleNewButtonButton)
        editButtonButton.pressed.connect(self._handleEditButtonButton)
        deleteButtonButton.pressed.connect(self._handleDeleteButtonButton)

        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

    def _handleNewButtonButton(self):
        tab = self._getCurrentTab()
        if tab is None:
            QtGui.QMessageBox.warning(self, "New Button", "No Shelves selected to add button to")
            return
        if tab.IsLocked():
            QtGui.QMessageBox.warning(self, "New Button", "Not able to add new buttons on locked Shelves")
            return
        newButton = tab.AddNewButton("", "", "")
        newButton.buttonToBeEdited.emit(newButton)
        self._populateButtonData()

    def _handleEditButtonButton(self):
        """
        Internal Method
        
        Handle the delete button button press
        """
        button = self._getCurrentButton()
        if button is None:
            QtGui.QMessageBox.warning(self, "Edit Button", "No button selected to edit")
            return
        parent = button.GetTabParent()
        if parent.IsLocked():
            QtGui.QMessageBox.warning(self, "Delete Buttons", "Not able to edit buttons on locked Shelves")
            return
        button.buttonToBeEdited.emit(button)
        self._populateButtonData()

    def _handleDeleteButtonButton(self):
        """
        Internal Method
        
        Handle the delete button button press
        """
        buttons = self._getCurrentButtons()
        if len(buttons) == 0:
            QtGui.QMessageBox.warning(self, "Delete Buttons", "No buttons selected to delete")
            return
        parent = buttons[0].GetTabParent()
        if parent.IsLocked():
            QtGui.QMessageBox.warning(self, "Delete Buttons", "Not able to delete buttons on locked Shelves")
            return
        for button in buttons:
            parent.RemoveButton(button)
        self._populateButtonData()

    def _handleSaveShelfButton(self):
        """
        Internal Method
        
        Handle the save button press
        """
        tabs = self._getCurrentTabs()
        if len(tabs) == 0:
            QtGui.QMessageBox.warning(self, "Share Shelfs", "No Shelves selected to save")
            return
        
        filePath, notCan = QtGui.QFileDialog.getSaveFileName(self, 'Save shelf','/home', "Shelf Files(*.xml)")
        if not notCan:
            return
        self._dataBinding.SaveTabsToFile(filePath, tabs)
        
        self._populateData()
    
    def _handleRenameShelfButton(self):
        """
        Internal Method
        
        Handle the rename button press
        """
        tab = self._getCurrentTab()
        if tab is None:
            QtGui.QMessageBox.warning(self, "Rename Shelf", "No Shelf selected to rename")
            return
        if tab.IsLocked():
            QtGui.QMessageBox.warning(self, "Rename Shelf", "Not able to rename locked shelfs")
            return
        (shelfName, notCan) = QtGui.QInputDialog.getText(self,"Rename Shelf","Shelf Name:",QtGui.QLineEdit.Normal, tab.Name())
        if not notCan:
            return
        self._dataBinding.RenameTab(tab, shelfName)
        self._populateData()
    
    def _handleDeleteShelfButton(self):
        """
        Internal Method
        
        Handle the delete shelf button press
        """
        tabs = self._getCurrentTabs()
        if len(tabs) == 0:
            QtGui.QMessageBox.warning(self, "Delete Shelves", "No Shelves selected to delete")
            return
        if any([tab.IsLocked() for tab in tabs]):
            QtGui.QMessageBox.warning(self, "Delete Shelves", "Not able to delete locked Shelves")
            return
        for tab in tabs:
            self._dataBinding.DeleteTab(tab)
        self._populateData()
    
    def _handleNewShelfButton(self):
        """
        Internal Method
        
        Handle the add new shelf button press
        """
        (shelfName, notCan) = QtGui.QInputDialog.getText(self,"New Shelf","Shelf Name:",QtGui.QLineEdit.Normal,"New Self")
        if not notCan:
            return
        self._dataBinding.AddNewTab(shelfName)
        self._populateData()

    def _handleLoadShelfButton(self):
        """
        Internal Method
        
        Handle the load shelf button press
        """
        filePath, notCan = QtGui.QFileDialog.getOpenFileName(self, 'Open Shelves','/home', "Shelf Files(*.xml)")
        if not notCan:
            return
        self._dataBinding.LoadTabsFromFile(filePath)
        self._populateData()
        
    def _populateData(self):
        """
        Internal Method
        
        Populate the treeview with the tabs
        """
        self._shelfsWidget.clear()
        for tab in self._dataBinding.GetTabs():
            newItem = QtGui.QTreeWidgetItem()

            # Name
            newItem.setText(0, tab.Name())
            newItem.setText(1, tab.GetSourceLocation())
            newItem.setText(2, str(tab.IsLocked()))
            newItem.setData(0, QtCore.Qt.UserRole, tab)
            
            self._shelfsWidget.addTopLevelItem(newItem)

    def _getCurrentTab(self):
        """
        Internal Method
        
        Get the currently selected tab
        
        Return:
            ScriptTab: The selected tab item
        """
        currentItem = self._shelfsWidget.currentItem()
        if currentItem is None:
            return None
        
        return currentItem.data(0, QtCore.Qt.UserRole)

    def _getCurrentButton(self):
        """
        Internal Method
        
        Get the currently selected button
        
        Return:
            ScriptButton: The selected button item
        """
        currentItem = self._buttonsWidget.currentItem()
        if currentItem is None:
            return None
        
        return currentItem.data(0, QtCore.Qt.UserRole)

    def _getCurrentTabs(self):
        """
        Internal Method
        
        Get the currently selected tabs
        
        Return:
            list of ScriptTab: The currently selected tabs
        """
        return [item.data(0, QtCore.Qt.UserRole) for item in self._shelfsWidget.selectedItems()]
    
    def _getCurrentButtons(self):
        """
        Internal Method
        
        Get the currently selected buttons
        
        Return:
            list of ScriptButtons: The currently selected tabs
        """
        return [item.data(0, QtCore.Qt.UserRole) for item in self._buttonsWidget.selectedItems()]
    
    def _populateButtonData(self):
        """
        Internal Method
        
        Populate the button treeview with the all the buttons in the selected tab
        """
        self._buttonsWidget.clear()
        
        tab = self._getCurrentTab()
        if tab is None:
            return
        
        for button in tab.GetButtons():
            newItem = QtGui.QTreeWidgetItem()
            # Name
            newItem.setText(0, button.Name())
            newItem.setIcon(0, button.GetIcon())
            newItem.setData(0, QtCore.Qt.UserRole, button)
            
            self._buttonsWidget.addTopLevelItem(newItem)

    def show(self):
        """
        Re-Implemented
        
        Show event, and populate the tab treeview
        """
        super(ShelftasticManagerWindow, self).show()
        self._populateData()