"""
Flow layout to allow widgets to be moved based around the size of the layout
"""

__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded

from PySide import QtGui, QtCore


class FlowLayout(QtGui.QLayout):
    """
    Flow Layout is a port of an Qt layout example. it has the ability to resize and move its widgets
    depending on its size

    Very Cude examples

    given a flowlayout with 5 widgets in it, when its lenght is reduced it would go from
    ===================
    | A B C D E       |
    ==================

    to:

    ========
    | A B C|
    | D E  |
    ========
    """
    def __init__(self, parent=None, margin=-1, hSpacing=-1, vSpacing=-1):
        super(FlowLayout, self).__init__()
        """
        Constrcutor
        """
        self.setContentsMargins(margin, margin, margin, margin)
        self._margin = margin

        self._itemList = []
        self._hSpace = hSpacing
        self._mSpace = vSpacing

    def addItem(self, item):
        """
        Reimplemented

        Core Qt method for layouts need an addItem method. adds the item to the layout

        Args:
            item (QWidget, QLayout, QSpacerItem): Item to add to layout
        """
        self._itemList.append(item)

    def horizontalSpacing(self):
        """
        ReImplemented
        """
        if self._hSpace >= 0:
            return self._hSpace
        return self.smartSpacing(QtGui.QStyle.PM_LayoutHorizontalSpacing)

    def verticalSpacing(self):
        """
        ReImplemented
        """
        if self.vSpace >= 0:
            return self.vSpace
        return self.smartSpacing(QtGui.QStyle.PM_LayoutVerticalSpacing)

    def count(self):
        """
        The amount of widgets in the layout

        Return:
            Int the amount of widgets in the layout
        """
        return len(self._itemList)

    def itemAt(self, index):
        """
        Get the widget/layout/spacer at the given index

        Args:
            index (index): the index number of the widget to get

        Return:
            (QWidget, QLayout, QSpacerItem) at that index
        """
        if index >= 0 and index < len(self._itemList):
            return self._itemList[index]
        return None

    def takeAt(self, index):
        """
        pop the widget/layout/spacer at the given index

        Args:
            index (index): the index number of the widget to get

        Return:
            (QWidget, QLayout, QSpacerItem) at that index
        """
        if index >= 0 and index < len(self._itemList):
            return self._itemList.pop(index)
        return 0

    def expandingDirections(self):
        """
        ReImplemented
        """
        return QtCore.Qt.Orientations(QtCore.Qt.Orientation(0))

    def hasHeightForWidth(self):
        """
        ReImplemented
        """
        return True

    def heightForWidth(self, width):
        """
        ReImplemented
        """
        return self.doLayout(QtCore.QRect(0, 0, width, 0), True)

    def maxWidth(self):
        """
        ReImplemented
        
        Get the max width of the layout with everything on a single line
        """
        x = 0
        for item in self._itemList:
            wid = item.widget()
            spaceX = self.horizontalSpacing()
            if (spaceX == -1):
                spaceX = wid.style().layoutSpacing(QtGui.QSizePolicy.ButtonBox, QtGui.QSizePolicy.PushButton, QtCore.Qt.Horizontal)
            x += item.sizeHint().width() + spaceX
        return x
            
    def setGeometry(self, rect):
        """
        ReImplemented
        """
        super(FlowLayout, self).setGeometry(rect)
        self.doLayout(rect, False)

    def sizeHint(self):
        """
        ReImplemented
        """
        return self.minimumSize()

    def minimumSize(self):
        """
        ReImplemented
        """
        width = self.geometry().width()
        height = self.doLayout(QtCore.QRect(0, 0, width, 0), True)
        return QtCore.QSize(width + 2 * self._margin, height + 2 * self._margin)

    def doLayout(self, rect, testOnly=False):
        """
        Main Layout logic, where we position the widgets in the flow layout where we want them to go

        args:
            rect (QRect): The widget space to work with
            testOnly (bool): If the widgets will get set in layout of if just calculated

        return:
            the total height of the layout once all the widgets have been placed
        """
        left, top, right, bottom = self.getContentsMargins()
        effectiveRect = rect.adjusted(+left, +top, -right, -bottom)

        x = effectiveRect.x()
        y = effectiveRect.y()
        lineHeight = 0

        for item in self._itemList:
            wid = item.widget()
            spaceX = self.horizontalSpacing()
            if (spaceX == -1):
                spaceX = wid.style().layoutSpacing(QtGui.QSizePolicy.ButtonBox, QtGui.QSizePolicy.PushButton, QtCore.Qt.Horizontal)

            spaceY = self.horizontalSpacing()
            if (spaceY == -1):
                spaceY = wid.style().layoutSpacing(QtGui.QSizePolicy.ButtonBox, QtGui.QSizePolicy.PushButton, QtCore.Qt.Vertical)

            nextX = x + item.sizeHint().width() + spaceX
            if (nextX - spaceX > effectiveRect.right() and lineHeight > 0):
                x = effectiveRect.x()
                y = y + lineHeight + spaceY
                nextX = x + item.sizeHint().width() + spaceX
                lineHeight = 0

            if not testOnly:
                item.setGeometry(QtCore.QRect(QtCore.QPoint(x, y), item.sizeHint()))

            x = nextX
            lineHeight = max([lineHeight, item.sizeHint().height()])

        return y + lineHeight - rect.y() + bottom

    def smartSpacing(self, pixelMetric):
        """
        method for doing smarter spacing based of the pixel data
        """
        parent = self.parent()
        if not parent:
            return -1
        elif isinstance(parent, QtGui.QWidget):
            return parent.style().pixelMetric(pixelMetric)
        else:
            return parent.spacing()
