"""
The main shelf widget code. This can work in Motion Builder or standalone with standard QT
The shelf should auto dock to the top of the motion builder main window

Note:

    Could be subclassed and utlizied in another applications such as NuanceTools or Max
"""
import os
from xml.dom import minidom

from PySide import QtCore, QtGui

from RS import Config
from RS.Tools import UI
from RS.Tools.Shelftastic import ShelfComponents, ManagerWidget, EditorWidget


class Shelftastic(UI.QtMainWindowBase):
    """
    Main Gui for the shelf widget
    """
    def __init__(self):
        """
        Constructor
        """
        self._sizeOptionSmallIcons = False
        self._sizeOptionIconText = False
        self._sizeOptionHScrollBar = False
        self._layoutOrientation = QtCore.Qt.Vertical
        self._settings = QtCore.QSettings("RockstarSettings", "ShelftasticPrefences")
        super(Shelftastic, self).__init__(title='Shelftastic', dock=True, dockable=True)
        self._tabList = []
        self.setupUi()
        self._shelfManager = ManagerWidget.ShelftasticManagerWindow(self, self)
        self._setupShelfManagerMenuBar()
        self._editButtonDialog = EditorWidget.ShelftasticEditorWidget(self)

    def showEvent(self, event):
        """
        ReImplement
        Show event, where we read the qsettings to get the last set settings
        """
        self._openLastShelf()

        self._sizeOptionSmallIcons = self._strToBool(self._settings.value("useSmallIcons", "False"))
        self._sizeOptionIconText = self._strToBool(self._settings.value("useIconText", "False"))
        orientationChecked = self._strToBool(self._settings.value("scrollingOrientation", "True"))

        self._switchLayoutOrientationAction.setChecked(orientationChecked)
        self._switchSmallButtonsAction.setChecked(self._sizeOptionSmallIcons)
        self._switchLabelButtonsAction.setChecked(self._sizeOptionIconText)

        self._handleSwitchLayoutOrientationAction()
        self._handleSwitchLabelButtonsAction()
        self._handleSwitchSmallButtonsAction()

        super(Shelftastic, self).showEvent(event)

    def _setupShelfManagerMenuBar(self):
        """
        Internal Method

        Add options to the shelf manager widget to allow for the switching layout and button sizes
        and options.
        """
        menubar = self._shelfManager.menuBar()
        cameraMenu = menubar.addMenu('Options')
        cameraMenu.addAction(self._switchLayoutOrientationAction)
        cameraMenu.addAction(self._switchSmallButtonsAction)
        cameraMenu.addAction(self._switchLabelButtonsAction)

    def _strToBool(self, inputString):
        """
        Helper method to convert a string into a bool value.

        Example:
            _strToBool('True') # Return True
            _strToBool('true') # Return True
            _strToBool('False') # Return False
            _strToBool('false') # Return False
            _strToBool('derp') # Will Raise
        """
        if inputString.lower() == 'true':
            return True
        elif inputString.lower() == 'false':
            return False
        raise ValueError("Value {0} cannot be converted to bool".format(inputString))

    def _openLastShelf(self):
        """
        Internal Method

        set the tab to the last tab opened, if it can be found or Custom if that is available.
        """
        lastTabOpen = self._settings.value("lastShelfOpen", "Custom")
        lastTab = [self._tabList.index(shelf) for shelf in self._tabList if shelf.Name() == lastTabOpen]
        if len(lastTab) != 0:
            self._tabs.setCurrentIndex(lastTab[0])
            return
        customTab = [self._tabList.index(shelf) for shelf in self._tabList if shelf.Name() == "Custom"]
        if len(customTab) != 0:
            self._tabs.setCurrentIndex(customTab[0])
            return
        return

    def setupUi(self):
        """
        Setup the main widget, the menu button and the tab widget
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QHBoxLayout()

        # Widgets
        self._optionsButton = QtGui.QPushButton()
        self._tabs = QtGui.QTabWidget()

        # Configure Widgets
        iconPath = os.path.join(Config.Script.Path.ToolImages, "Shelftastic", "menuDropdown.png")
        self._optionsButton.setIcon(QtGui.QIcon(iconPath))
        self._optionsButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self._optionsButton.setMinimumWidth(20)
        self._optionsButton.setMaximumWidth(20)
        self._optionsButton.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        lockIconPath = os.path.join(Config.Script.Path.ToolImages, "Shelftastic", "locked.png")
        self._lockIcon = QtGui.QIcon(lockIconPath)
        self._tabs.setIconSize(QtCore.QSize(10, 10))
        # Configure top (self) widget
        self.layout().setContentsMargins(0, 0, 0, 0)

        self._setMaxShelfSize()

        # Assign Widgets to Layouts
        layout.addWidget(self._optionsButton)
        layout.addWidget(self._tabs)

        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

        # Setup connections
        self._optionsButton.pressed.connect(self._handleShelfManagerAction)

        # Build the tabs
        self._setupTabs()

        # Setup the right click context menu
        # Add new shelf menu option
        newShelfAction = QtGui.QAction(self._optionsButton)
        newShelfAction.setText("Add new shelf")
        newShelfAction.triggered.connect(self._handleNewShelfAction)
        self._optionsButton.addAction(newShelfAction)

        # Open the shelf manager button
        shelfManagerAction = QtGui.QAction(self._optionsButton)
        shelfManagerAction.setText("Open Shelf Manager")
        shelfManagerAction.triggered.connect(self._handleShelfManagerAction)
        self._optionsButton.addAction(shelfManagerAction)

        # Remove the current shelf button
        removeCurrentAction = QtGui.QAction(self._optionsButton)
        removeCurrentAction.setText("Remove current shelf")
        removeCurrentAction.triggered.connect(self._handleRemoveShelfAction)
        self._optionsButton.addAction(removeCurrentAction)

        # Seperator
        sep = QtGui.QAction(self._optionsButton)
        sep.setSeparator(True)
        self._optionsButton.addAction(sep)

        # Remove the current shelf button
        self._switchLayoutOrientationAction = QtGui.QAction(self._optionsButton)
        self._switchLayoutOrientationAction.setText("Vertical shelf scroll")
        self._switchLayoutOrientationAction.triggered.connect(self._handleSwitchLayoutOrientationAction)
        self._switchLayoutOrientationAction.setCheckable(True)
        self._switchLayoutOrientationAction.setChecked(self._layoutOrientation != QtCore.Qt.Vertical)
        self._optionsButton.addAction(self._switchLayoutOrientationAction)

        self._switchSmallButtonsAction = QtGui.QAction(self._optionsButton)
        self._switchSmallButtonsAction.setText("Use Small Buttons")
        self._switchSmallButtonsAction.triggered.connect(self._handleSwitchSmallButtonsAction)
        self._switchSmallButtonsAction.setCheckable(True)
        self._switchSmallButtonsAction.setChecked(self._sizeOptionSmallIcons)
        self._optionsButton.addAction(self._switchSmallButtonsAction)

        self._switchLabelButtonsAction = QtGui.QAction(self._optionsButton)
        self._switchLabelButtonsAction.setText("Show Button Labels")
        self._switchLabelButtonsAction.triggered.connect(self._handleSwitchLabelButtonsAction)
        self._switchLabelButtonsAction.setCheckable(True)
        self._switchLabelButtonsAction.setChecked(self._sizeOptionIconText)
        self._optionsButton.addAction(self._switchLabelButtonsAction)

        self._tabs.currentChanged.connect(self._handleTabChange)

    def _handleSwitchSmallButtonsAction(self):
        """
        Internal Method

        Handle the using of small or large button icons
        """
        value = self._switchSmallButtonsAction.isChecked()
        self._sizeOptionSmallIcons = value
        for shelf in self._tabList:
            shelf.SetSmallButton(value)
        self._setMaxShelfSize()
        self._settings.setValue("useSmallIcons", value)

    def _handleSwitchLabelButtonsAction(self):
        """
        Internal Method

        Handle the showing or hiding of the lables under the buttons
        """
        value = self._switchLabelButtonsAction.isChecked()
        self._sizeOptionIconText = value
        for shelf in self._tabList:
            shelf.SetButtonLabels(value)
        self._setMaxShelfSize()
        self._settings.setValue("useIconText", value)

    def _handleSwitchLayoutOrientationAction(self):
        """
        Internal Method

        Handle the Vertical to Horizontal scrolling layout change for the options
        """
        value = self._switchLayoutOrientationAction.isChecked()
        if value:
            self._layoutOrientation = QtCore.Qt.Vertical
        else:
            self._layoutOrientation = QtCore.Qt.Horizontal

        for shelf in self._tabList:
            shelf.SetScrollingOrientation(self._layoutOrientation)
        self._setMaxShelfSize()

        self._settings.setValue("scrollingOrientation", value)

    def _setMaxShelfSize(self):
        """
        Internal Method

        Set the max size of the shelf based on what 'extras' are on such as the small buttons, the
        horizontal scroll bar or the text under the icon
        """
        baseSize = 62

        if self._sizeOptionSmallIcons:
            baseSize -= 10
        if self._sizeOptionIconText:
            baseSize += 18
        if self._sizeOptionHScrollBar:
            baseSize += 17

        self.setMinimumHeight(baseSize)
        self.setMaximumHeight(baseSize)

    def _handleTabChange(self):
        """
        Internal Method

        Update the current tab in the settings so it can be opened when the shelf is next opened
        """
        tabIdx = self._tabs.currentIndex()
        currentShelfName = self._tabList[tabIdx].Name()
        self._settings.setValue("lastShelfOpen", currentShelfName)

    def AddNewTab(self, shelfName=None):
        """
        Add a new shelf with the given name, if no name is given, a dialog will ask the user for
        the new tab name

        Kwargs:
            shelfName (str): Name of the tab to be created
        """
        if shelfName is None:
            (shelfName, notCan) = QtGui.QInputDialog.getText(
                                                             self,
                                                             "New Shelf",
                                                             "Shelf Name:",
                                                             QtGui.QLineEdit.Normal,
                                                             "Untitled Shelf"
                                                             )
            if not notCan:
                return
        newTab = ShelfComponents.ScriptTab(shelfName)
        self._addTab(newTab)

    def GetTabs(self):
        """
        Get the list of tab widgets that make up the shelf

        Return:
            list of ScriptTab: All the tabs that make up the shelf widget
        """
        return self._tabList

    def RenameTab(self, tab, newName):
        """
        Rename the given tab with the new name

        Args:
            tab (ScriptTab): The tab to rename
            newName (str): The new name for the tab
        """
        tab.SetName(newName)
        tabIdx = self._tabs.indexOf(tab.GetTab())
        self._tabs.setTabText(tabIdx, newName)
        self.SaveAllTabs()

    def AddTab(self, tabName):
        """
        Create a new tab with the given name

        Args:
            tabName (str): The name of the tab to create

        Return:
            ScriptTab: The newly created tab
        """
        newTab = ShelfComponents.ScriptTab(tabName)
        self._addTab(newTab)
        return newTab

    def DeleteTab(self, tab):
        """
        Delete the given tab from the shelf

        Args:
            tab (ScriptTab): The tab to remove from the shelf
        """
        tabIdx = self._tabs.indexOf(tab.GetTab())
        self._tabs.removeTab(tabIdx)
        self._tabList.pop(tabIdx)

    def SaveAllTabs(self):
        """
        Save all the tabs in the shelf to their respective locations
        """
        # Compile a dict to combine all tabs with the same file path so they can be saved together
        tabsToPath = {}
        for tab in self._tabList:
            if tab.IsLocked():
                continue
            # Get the list of tabs with the given file name, or create a new list
            tabs = tabsToPath.get(tab.GetSourceLocation(), [])
            tabs.append(tab)
            tabsToPath[tab.GetSourceLocation() or self.GetCustomMenuFile()] = tabs

        # Save the files
        for filePath, tabs in tabsToPath.iteritems():
            self.SaveTabsToFile(filePath, tabs)

    def SaveTabsToFile(self, filePath, tabs):
        """
        Save the given tabs to the given file path

        Args:
            filePath(str): The file location to save the files
            tabs (list of ScriptTab): The tabs to save
        """
        # Open the file for writting
        fileHandle = open(filePath, "wb")
        implementation = minidom.getDOMImplementation()
        baseTabs = implementation.createDocument(None, "tabs", None)
        baseTabsElement = baseTabs.documentElement

        for tab in tabs:
            # Write the tab name and buttons
            tabNode = baseTabs.createElement("tab")
            tabName = baseTabs.createElement("name")
            tabName.appendChild(baseTabs.createTextNode(tab.Name()))
            tabButtons = baseTabs.createElement("buttons")

            # Go through the buttons
            for button in tab.GetButtons():

                # Write the button's Name, Icon and code, if there is any
                scriptButton = baseTabs.createElement("button")
                buttonName = button.Name()
                if buttonName != "":
                    scriptName = baseTabs.createElement("name")
                    scriptName.appendChild(baseTabs.createTextNode(buttonName))
                    scriptButton.appendChild(scriptName)

                buttonIcon = button.IconPath()
                if buttonIcon != "":
                    scriptIcon = baseTabs.createElement("icon")
                    scriptIcon.appendChild(baseTabs.createTextNode(buttonIcon))
                    scriptButton.appendChild(scriptIcon)

                buttonCode = button.Code()
                if buttonCode != "":
                    scriptCode = baseTabs.createElement("code")
                    scriptCode.appendChild(baseTabs.createTextNode(buttonCode))
                    scriptButton.appendChild(scriptCode)

                buttonCodeFile = button.CodeFilepath()
                if buttonCodeFile != "":
                    scriptCode = baseTabs.createElement("codeFile")
                    scriptCode.appendChild(baseTabs.createTextNode(buttonCodeFile))
                    scriptButton.appendChild(scriptCode)

                runTypeIdx = button.RunType()
                if runTypeIdx != "":
                    scriptCode = baseTabs.createElement("runTypeIdx")
                    scriptCode.appendChild(baseTabs.createTextNode(str(runTypeIdx)))
                    scriptButton.appendChild(scriptCode)

                tabButtons.appendChild(scriptButton)

            tabNode.appendChild(tabName)
            tabNode.appendChild(tabButtons)

            baseTabsElement.appendChild(tabNode)
        baseTabs.writexml(fileHandle)
        fileHandle.close()

        # Update the source locations for the tabs
        for tab in tabs:
            tab.SetSourceLocation(filePath)

    def _handleShelfManagerAction(self):
        """
        Internal Method

        Show the shelf manager dialog
        """
        self._shelfManager.show()

    def _handleNewShelfAction(self):
        """
        Internal Method

        Add a new shelf to the toolbar
        """
        self.AddNewTab()

    def _handleRemoveShelfAction(self):
        """
        Internal Method

        Remove the current shelf, if a shelf is selected
        """
        tabIdx = self._tabs.currentIndex()
        if tabIdx == -1:
            return
        self._tabs.removeTab(tabIdx)
        self._tabList.pop(tabIdx)

    def GetDeaultMenuFile(self):
        """
        Get the file path for the "default" shelf xml

        Return:
            filePath (str): Path to default.xml
        """
        return os.path.join(Config.Tool.Path.TechArt, "etc", "config", "motionbuilder", "shelftastic", "default.xml")

    def GetCustomMenuFile(self):
        """
        Get the file path for the "custom" shelf xml

        Return:
            filePath (str): Path to custom.xml
        """
        return os.path.join(os.getenv('APPDATA'), "rockstar", "motionbuilder",
                            "shelftastic", "custom.xml")

    def _setupTabs(self):
        """
        Internal Method

        Setup the tabs using the Default and Custom files
        """
        self.LoadTabsFromFile(self.GetDeaultMenuFile())
        customFile = self.GetCustomMenuFile()
        if not os.path.isfile(customFile):
            self._initlizeCustomXmlFile()
        self.LoadTabsFromFile(self.GetCustomMenuFile())

    def _initlizeCustomXmlFile(self):
        """
        Internal Method

        Method to create the folder structure and the initial custom xml file to store any custom
        shelf data.
        """
        customFile = self.GetCustomMenuFile()
        if not os.path.exists(os.path.dirname(customFile)):
            os.makedirs(os.path.dirname(customFile))

        fileHandle = open(customFile, "w+")
        implementation = minidom.getDOMImplementation()
        baseTabs = implementation.createDocument(None, "tabs", None)
        baseTabsElement = baseTabs.documentElement

        # Write the tab name and buttons
        tabNode = baseTabs.createElement("tab")
        tabName = baseTabs.createElement("name")
        tabName.appendChild(baseTabs.createTextNode("Custom"))
        tabButtons = baseTabs.createElement("buttons")

        tabNode.appendChild(tabName)
        tabNode.appendChild(tabButtons)

        baseTabsElement.appendChild(tabNode)
        baseTabs.writexml(fileHandle)

    def LoadTabsFromFile(self, filePath):
        """
        Load a shelf from a given xml file

        Args:
            filePath (str): The xml file path to load in
        """
        if not os.path.isfile(filePath):
            return

        newShelfs = self._GetMenu(filePath)
        for shelf in newShelfs:
            self._addTab(shelf)

    def _addTab(self, newTab):
        """
        Internal Method

        Add a ScriptTab to the shelf widget

        Args:
            newTab (ScriptTab): The tab to add to the shelf
        """
        self._tabList.append(newTab)
        self._tabs.addTab(newTab.GetTab(), newTab.Name())
        newTab.dataChanged.connect(self._handleTabDataChange)
        newTab.buttonToBeEdited.connect(self._handleButtonToBeEdited)
        newTab.lockStatusChanged.connect(self._handleLockStatusChange)
        newTab._tabWidget.horizontalScrollShown.connect(self._handleHorizontalScrollShown)
        newTab._tabWidget.horizontalScrollHidden.connect(self._handleHorizontalScrollHidden)
        newTab.IsLocked()

    def _handleHorizontalScrollShown(self):
        """
        Internal Method

        The Horizontal Scroll bar is now showing, recalculate the proper hight of the widget and
        set the max hight so the scroll bar isnt cut off the bottom and isnt cutting into the shelf
        """
        self._sizeOptionHScrollBar = True
        self._setMaxShelfSize()

    def _handleHorizontalScrollHidden(self):
        """
        Internal Method

        The Horizontal Scroll bar is now hidden, recalculate the proper hight of the widget and
        set the max hight
        """
        self._sizeOptionHScrollBar = False
        self._setMaxShelfSize()

    def _handleButtonToBeEdited(self, button):
        """
        Internal Method

        Display the Editor Widget with the given button to edit

        Args:
            button (ScriptButton): The button to edit
        """
        self._editButtonDialog.setButton(button)
        self._editButtonDialog.show()

    def _handleLockStatusChange(self, shelf, value):
        """
        Internal Method

        Method to add, or remove the lock icon of a shelf.

        Args:
            shelf (ScriptTab): Shelf to add/remove lock icon
            value (bool): If the shelf is locked or not
        """
        idx = self._tabList.index(shelf)
        icon = QtGui.QIcon()
        if value:
            icon = self._lockIcon
        self._tabs.setTabIcon(idx, icon)

    def _handleTabDataChange(self):
        """
        Internal Method

        Method to save all the tabs, used for when a tab is edited, so any changes such as adding
        new buttons, editing buttons or removing buttons are saved
        """
        self.SaveAllTabs()

    @classmethod
    def _GetMenu(cls, filePath):
        """
        Class Method

        Internal Method

        Given a file path, construct a list of ScriptTabs with ScriptButtons from the xml file

        Args:
            filePath (str): The xml file to use to build shelfs to add to the widget

        ReturnL
            list of ScriptTab: The scriptTabs generated from the given xml file
        """
        xmldoc = minidom.parse(filePath)

        tabs = xmldoc.getElementsByTagName("tab")
        newShelfs = []
        # Build the tabs
        for tab in tabs:
            expanded = {str(node.nodeName): node for node in tab.childNodes
                                                            if not isinstance(node, minidom.Text)}
            name = str(expanded.get("name").childNodes[0].nodeValue)
            newShelf = ShelfComponents.ScriptTab(name, sourceLocation=filePath)
            buttons = expanded.get("buttons")
            expandedButs = [node for node in buttons.childNodes if not isinstance(node, minidom.Text)]
            # Build the buttons
            for button in expandedButs:
                expButton = {str(node.nodeName): node.childNodes[0].nodeValue for node in
                                            button.childNodes if not isinstance(node, minidom.Text)}
                scrButton = ShelfComponents.ScriptButton(
                                         None,
                                         expButton.get("name", "NoName"),
                                         expButton.get("icon"),
                                         expButton.get("code", ""),
                                         expButton.get("codeFile", ""),
                                         int(expButton.get("runTypeIdx", "0"))
                                         )
                newShelf.AddButton(scrButton)
            newShelfs.append(newShelf)
        return newShelfs


def Run():
    return UI.Run(Shelftastic())


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    win = Shelftastic()
    win.show()
    sys.exit(app.exec_())
