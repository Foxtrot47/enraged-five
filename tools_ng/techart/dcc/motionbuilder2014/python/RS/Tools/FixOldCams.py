from pyfbsdk import *
import RS.Globals as glo
import RS.Core.Camera.Lib
from RS.Utils.Scene import Constraint


#ConnectBox
def ConnectBox(nodeOut,nodeOutPos, nodeIn, nodeInPos):
	mynodeOut = FindAnimationNode(nodeOut.AnimationNodeOutGet(), nodeOutPos )
	mynodeIn = FindAnimationNode(nodeIn.AnimationNodeInGet(), nodeInPos )
	if mynodeOut and mynodeIn :
		FBConnect(mynodeOut, mynodeIn)
	else:
		None


#FindAnimationNode
def FindAnimationNode(pParent, pName):
	lResult = None
	for lNode in pParent.Nodes:
		if lNode.Name == pName:
			lResult = lNode
			break
		else:
			None
	return lResult


def transferCameraAnimation( sourceCam, targetCam ):
	numAnimLayers = FBSystem().CurrentTake.GetLayerCount()

	for animLayer in range( 1, numAnimLayers ):
		FBSystem().CurrentTake.SetCurrentLayer( animLayer - 1 )

		for prop in sourceCam.PropertyList:
			propName = prop.GetName()
			propType = prop.GetPropertyType()

			# Ignore string types, since you can't animate those.
			if propType != FBPropertyType.kFBPT_charptr:
				sourceProp = sourceCam.PropertyList.Find( propName )
				targetProp = targetCam.PropertyList.Find( propName )

				if sourceProp and targetProp:
					if hasattr( sourceProp, 'GetAnimationNode' ):
						sourceAnimNode = sourceProp.GetAnimationNode()
						targetAnimNode = targetProp.GetAnimationNode()

						if sourceAnimNode and targetAnimNode:
							if targetAnimNode.FCurve and sourceAnimNode.FCurve:              
								targetAnimNode.FCurve.KeyReplaceBy( sourceAnimNode.FCurve )


def createNewCam():
	#Create Camera
	cam = FBCamera('CAMERATOREPLACETHEOLD')
	cam.Show = True
	cam.Translation = FBVector3d(0,0,0)
	cam.Rotation = FBVector3d(0,-90,0)
	cam.ResolutionMode = FBCameraResolutionMode.kFBResolutionCustom
	cam.ResolutionWidth = 1920
	cam.ResolutionHeight = 1080
	cam.FrameColor = FBColor(0,0,0)
	cam.UseFrameColor = True
	cam.SafeAreaMode = FBCameraSafeAreaMode.kFBSafeAreaSquare 
	cam.ViewDisplaySafeArea = True
	cam.BackGroundImageFit = False
	cam.BackGroundPlaneDistance = 100
	cam.ForeGroundMaterialThreshold = 50
	cam.ForeGroundPlaneDistanceMode = FBCameraDistanceMode.kFBDistModeAbsoluteFromCamera
	cam.FilmBackType = FBCameraFilmBackType.kFBFilmBackCustom
	cam.FarPlaneDistance = 40000


	#---Adding DOF Plane Elements      
	#ScreenFade Plane
	fadePlane = FBModelPlane(cam.Name + " ScreenFade")
	fadePlane.Show = True
	fadePlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(50.76,0,0)
	fadePlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,90)
	FBSystem().Scene.Evaluate()
	fadePlane.PropertyList.Find("Enable Transformation").Data = False
	fadePlane.PropertyList.Find("Enable Selection").Data = False       
	#Far Plane
	farPlane = FBModelPlane(cam.Name + " FarPlane")
	farPlane.Show = True
	farPlane.PropertyList.Find("Enable Translation DOF").Data = True
	farPlane.PropertyList.Find("Enable Min X").Data = True
	farPlane.PropertyList.Find("Enable Min Y").Data = True
	farPlane.PropertyList.Find("Enable Min Z").Data = True
	farPlane.PropertyList.Find("Max Freedom").Data = FBVector3d(20000,0,0)
	farPlane.PropertyList.Find("Enable Max X").Data = True
	farPlane.PropertyList.Find("Enable Max Y").Data = True
	farPlane.PropertyList.Find("Enable Max Z").Data = True
	farPlane.PropertyList.Find("Enable Rotation DOF").Data = True
	farPlane.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,90)
	farPlane.PropertyList.Find("RotationMinX").Data = True
	farPlane.PropertyList.Find("RotationMinY").Data = True
	farPlane.PropertyList.Find("RotationMinZ").Data = True
	farPlane.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,90)
	farPlane.PropertyList.Find("RotationMaxX").Data = True
	farPlane.PropertyList.Find("RotationMaxY").Data = True
	farPlane.PropertyList.Find("RotationMaxZ").Data = True
	farPlane.PropertyList.Find("Default Manipulation Mode").Data = 2
	farPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
	farPlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,90)
	FBSystem().Scene.Evaluate()
	#Near Plane
	nearPlane = FBModelPlane(cam.Name + " NearPlane")
	nearPlane.Show = True
	nearPlane.PropertyList.Find("Enable Translation DOF").Data = True
	nearPlane.PropertyList.Find("Min Freedom").Data = FBVector3d(6,0,0)
	nearPlane.PropertyList.Find("Enable Min X").Data = True
	nearPlane.PropertyList.Find("Enable Min Y").Data = True
	nearPlane.PropertyList.Find("Enable Min Z").Data = True
	nearPlane.PropertyList.Find("Enable Max X").Data = False
	nearPlane.PropertyList.Find("Enable Max Y").Data = True
	nearPlane.PropertyList.Find("Enable Max Z").Data = True
	nearPlane.PropertyList.Find("Enable Rotation DOF").Data = False
	nearPlane.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,90)
	nearPlane.PropertyList.Find("RotationMinX").Data = True
	nearPlane.PropertyList.Find("RotationMinY").Data = True
	nearPlane.PropertyList.Find("RotationMinZ").Data = True
	nearPlane.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,90)
	nearPlane.PropertyList.Find("RotationMaxX").Data = True
	nearPlane.PropertyList.Find("RotationMaxY").Data = True
	nearPlane.PropertyList.Find("RotationMaxZ").Data = True
	nearPlane.PropertyList.Find("Default Manipulation Mode").Data = 2
	nearPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(-75,0,0)
	nearPlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(180,0,90)
	FBSystem().Scene.Evaluate()
	#Far Blend Plane
	farBlendPlane = FBModelPlane(cam.Name + " FarBlendPlane")
	farBlendPlane.Show = True
	farBlendPlane.PropertyList.Find("Enable Translation DOF").Data = True
	farBlendPlane.PropertyList.Find("Enable Min X").Data = True
	farBlendPlane.PropertyList.Find("Enable Min Y").Data = True
	farBlendPlane.PropertyList.Find("Enable Min Z").Data = True
	farBlendPlane.PropertyList.Find("Max Freedom").Data = FBVector3d(20000,0,0)
	farBlendPlane.PropertyList.Find("Enable Max X").Data = True
	farBlendPlane.PropertyList.Find("Enable Max Y").Data = True
	farBlendPlane.PropertyList.Find("Enable Max Z").Data = True
	farBlendPlane.PropertyList.Find("Enable Rotation DOF").Data = True
	farBlendPlane.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,90)
	farBlendPlane.PropertyList.Find("RotationMinX").Data = True
	farBlendPlane.PropertyList.Find("RotationMinY").Data = True
	farBlendPlane.PropertyList.Find("RotationMinZ").Data = True
	farBlendPlane.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,90)
	farBlendPlane.PropertyList.Find("RotationMaxX").Data = True
	farBlendPlane.PropertyList.Find("RotationMaxY").Data = True
	farBlendPlane.PropertyList.Find("RotationMaxZ").Data = True
	farBlendPlane.PropertyList.Find("Default Manipulation Mode").Data = 2
	farBlendPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(0,0,0)
	farBlendPlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(0,0,90)
	farBlendPlane.PropertyList.Find("Lcl Scaling").Data = FBVector3d(0.5,0.5,0.5)
	FBSystem().Scene.Evaluate()
	#Near Blend Plane
	nearBlendPlane = FBModelPlane(cam.Name + " NearBlendPlane")
	nearBlendPlane.Show = True
	nearBlendPlane.PropertyList.Find("Enable Translation DOF").Data = True
	nearBlendPlane.PropertyList.Find("Min Freedom").Data = FBVector3d(6,0,0)
	nearBlendPlane.PropertyList.Find("Enable Min X").Data = True
	nearBlendPlane.PropertyList.Find("Enable Min Y").Data = True
	nearBlendPlane.PropertyList.Find("Enable Min Z").Data = True
	nearBlendPlane.PropertyList.Find("Enable Max X").Data = False
	nearBlendPlane.PropertyList.Find("Enable Max Y").Data = True
	nearBlendPlane.PropertyList.Find("Enable Max Z").Data = True
	nearBlendPlane.PropertyList.Find("Enable Rotation DOF").Data = False
	nearBlendPlane.PropertyList.Find("RotationMin").Data = FBVector3d(0,0,90)
	nearBlendPlane.PropertyList.Find("RotationMinX").Data = True
	nearBlendPlane.PropertyList.Find("RotationMinY").Data = True
	nearBlendPlane.PropertyList.Find("RotationMinZ").Data = True
	nearBlendPlane.PropertyList.Find("RotationMax").Data = FBVector3d(0,0,90)
	nearBlendPlane.PropertyList.Find("RotationMaxX").Data = True
	nearBlendPlane.PropertyList.Find("RotationMaxY").Data = True
	nearBlendPlane.PropertyList.Find("RotationMaxZ").Data = True
	nearBlendPlane.PropertyList.Find("Default Manipulation Mode").Data = 2
	nearBlendPlane.PropertyList.Find("Lcl Translation").Data = FBVector3d(-75,0,0)
	nearBlendPlane.PropertyList.Find("Lcl Rotation").Data = FBVector3d(180,0,90)
	nearBlendPlane.PropertyList.Find("Lcl Scaling").Data = FBVector3d(0.5,0.5,0.5)
	#Create Heirarchy
	cam.ConnectSrc(fadePlane)
	cam.ConnectSrc(nearPlane)
	cam.ConnectSrc(farPlane)
	cam.ConnectSrc(farBlendPlane)
	cam.ConnectSrc(nearBlendPlane)

	#---Setting Materials to the DoF Planes
	nearMat = FBMaterial(cam.Name + " DOF_NearPlane")
	nearMat.Emissive = FBColor(0.8,0.28,0.01)
	nearMat.Diffuse = FBColor(0.8,0.28,0.01)

	farMat = FBMaterial(cam.Name + " FarPlane")
	farMat.Emissive = FBColor(0,0,1)
	farMat.Diffuse = FBColor(0.02,0.26,0.10)

	nearBlendMat = FBMaterial(cam.Name + " NearBlendPlane")
	nearBlendMat.Emissive = FBColor(0.8,0.5,0.01)
	nearBlendMat.Diffuse = FBColor(0.2,0.125,0.005)

	farBlendMat = FBMaterial(cam.Name + " FarBlendPlane")
	farBlendMat.Emissive = FBColor(0,0,0.5)
	farBlendMat.Diffuse = FBColor(0.01,0.13,0.05)

	fadeMat = FBMaterial(cam.Name + " ScreenFadeMaterial")
	fadeMat.Diffuse = FBColor(0.0,0.0,0.0)
	#fadeMat.Diffuse.SetAnimated(True)
	fadeMat.Opacity = 0
	#fadeMat.Opacity.SetAnimated(True)

	#---Setting Shaders to the DoF Planes
	planeTrnsp = FBShaderLighted(cam.Name + " Transparency_Planes")
	planeTrnsp.Transparency = FBAlphaSource.kFBAlphaSourceAccurateAlpha
	planeTrnsp.Alpha  = 0.46
	fadeTrnsp = FBShaderManager().CreateShader("FlatShader")
	fadeTrnsp.Name = cam.Name + " ScreenFadeShader"
	fadeTrnsp.PropertyList.Find("Transparency Type").Data = 1

	#---Add Mats and Shaders to Geo
	nearPlane.Materials.append(nearMat)
	farPlane.Materials.append(farMat)
	nearBlendPlane.Materials.append(nearBlendMat)
	farBlendPlane.Materials.append(farBlendMat)
	fadePlane.Materials.append(fadeMat)
	nearPlane.Shaders.append(planeTrnsp)
	nearPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
	farPlane.Shaders.append(planeTrnsp)
	farPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
	nearBlendPlane.Shaders.append(planeTrnsp)
	nearBlendPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
	farBlendPlane.Shaders.append(planeTrnsp)
	farBlendPlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll
	fadePlane.Shaders.append(fadeTrnsp)
	fadePlane.ShadingMode  = FBModelShadingMode.kFBModelShadingAll

	#---Organising Mats and Shaders into Folders
	myMatFolder = FBFolder(cam.Name + "Mats:Materials", nearMat)
	myMatFolder.ConnectSrc(farMat)
	myMatFolder.ConnectSrc(fadeMat)
	myShadFolder = FBFolder(cam.Name + "Shader:Shaders", planeTrnsp)
	myShadFolder.ConnectSrc(fadeTrnsp)

	#---Creating Custom Properties and adding them to the Cameras
	#FOCUS
	custprop2 = cam.PropertyCreate('FOCUS', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop2.SetMin(0, True)
	custprop2.SetMax(5000, True)
	custprop2.Data = 150
	#custprop2.SetAnimated(True)
	#DOF Multiplier
	custprop3 = cam.PropertyCreate('DOF Multiplier', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop3.SetMin(1, True)
	custprop3.SetMax(100, True)
	custprop3.Data = 1
	#custprop3.SetAnimated(True)
	#DOF Expand
	custprop4 = cam.PropertyCreate('DOF Expand', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop4.SetMin(-31, True)
	custprop4.SetMax(500, True)
	custprop4.Data = 0
	#custprop4.SetAnimated(True)
	#DOF Strength
	custprop5 = cam.PropertyCreate('DOF Strength', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop5.SetMin(0.001, False)
	custprop5.SetMax(1, False)
	#custprop5.SetAnimated(True)
	#DOF Strength Override
	custprop8 = cam.PropertyCreate('DOF Strength Override', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop8.SetMin(0.001, False)
	custprop8.SetMax(1, False)
	custprop8.Data = 0
	#custprop8.SetAnimated(True)

	#====== NEW BLEND PLANES  =====        
	#DOF NEAR Blend PLANE Override
	custprop9 = cam.PropertyCreate('Near Blend', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop9.SetMin(1, False)
	custprop9.SetMax(500, False)
	custprop9.Data = 1
	#custprop9.SetAnimated(True)
	#DOF Near Blend Multi
	custprop10 = cam.PropertyCreate('Near Blend Multi', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop10.SetMin(1, False)
	custprop10.SetMax(100, False)
	custprop10.Data = 1
	#custprop10.SetAnimated(True)

	#DOF FR Blend PLANE Override
	custprop11 = cam.PropertyCreate('Far Blend', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop11.SetMin(1, False)
	custprop11.SetMax(500, False)
	custprop11.Data = 1
	#custprop11.SetAnimated(True)
	#DOF Far Blend Multi
	custprop12 = cam.PropertyCreate('Far Blend Multi', FBPropertyType.kFBPT_float, 'Number', True, True, None)
	custprop12.SetMin(1, False)
	custprop12.SetMax(100, False)
	custprop12.Data = 1
	#custprop12.SetAnimated(True)
	#====== ==================  ===== 

	#Script_Version
	customtag = cam.PropertyCreate('Script_Version', FBPropertyType.kFBPT_charptr, "", False, True, None)
	customtag.Data = "5"      
	#ScreenFade - Diffuse
	refProp = fadeMat.PropertyList.Find('Diffuse')
	custprop6 = cam.PropertyCreate("ScreenFadeMaterial.Diffuse", FBPropertyType.kFBPT_Reference, '', True, True, refProp)
	#ScreenFade - Opacity
	refProp2 = fadeMat.PropertyList.Find('Opacity')
	custprop7 = cam.PropertyCreate("ScreenFadeMaterial.Opacity", FBPropertyType.kFBPT_Reference, '', True, True, refProp2)
	#Field of View
	#cam.FieldOfView.SetAnimated(True)

	#---Creating Relation Constraint
	#-------------------------------- INSERT CONSTRAINT HERE
	relConst = Constraint.CreateConstraint(Constraint.RELATION)
	FBSystem().Scene.Constraints.append(relConst)
	relConst.Name = (cam.Name + " Relation")
	relConst.Active = True




	#Camera Sender
	camSend = relConst.SetAsSource(cam)
	relConst.SetBoxPosition(camSend, 0, 400)
	camSend.UseGlobalTransforms = False      


	#log(a)
	log = relConst.CreateFunctionBox('Number', 'log(a)')
	relConst.SetBoxPosition(log, 400, 750)
	#Multiply (a x b)
	Mult = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(Mult, 700, 750)
	#Multiply (a x b)2
	Mult2 = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(Mult2, 1000, 750)        
	#Number to Vector2
	NumVec2 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec2, 1400, 750)
	#Number to Vector4
	NumVec4 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec4, 1400, 500)
	#Number to Vector5
	NumVec5 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec5, 1400, 600)
	#Number to Vector6
	NumVec6 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec6, 1400, 500)
	#Number to Vector7
	NumVec7 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec7, 1400, 600)
	#Vector to Number
	VecNum = relConst.CreateFunctionBox('Converters', 'Vector to Number')
	relConst.SetBoxPosition(VecNum, 1700, 700)
	#Vector to Number2
	VecNum2 = relConst.CreateFunctionBox('Converters', 'Vector to Number')
	relConst.SetBoxPosition(VecNum2, 1700, 800)        
	#Vector to Number3
	VecNum3 = relConst.CreateFunctionBox('Converters', 'Vector to Number')
	relConst.SetBoxPosition(VecNum3, 1700, 900)
	#Number to Vector3
	NumVec3 = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec3, 2300, 750)
	#Number to Vector
	NumVec = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(NumVec, 1700, 1000)
	#Multiply (a x b)3
	Mult3 = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(Mult3, 2000, 750)
	Mult3Val = FindAnimationNode(Mult3.AnimationNodeInGet(),'b')
	Mult3Val.WriteData([-1])
	#Add (V1 + V2)
	AddVec = relConst.CreateFunctionBox('Vector', 'Add (V1 + V2)')
	relConst.SetBoxPosition(AddVec, 2100, 650)
	#Subtract (V1 - V2)
	SubVec = relConst.CreateFunctionBox('Vector', 'Subtract (V1 - V2)')
	relConst.SetBoxPosition(SubVec, 2100, 850)
	#Add(a + b)
	add = relConst.CreateFunctionBox('Number', 'Add (a + b)')
	relConst.SetBoxPosition(add, 1000, 600)
	#Add(a + b)2
	add2 = relConst.CreateFunctionBox('Number', 'Add (a + b)')
	relConst.SetBoxPosition(add2, 1000, 680)

	#FarPlane Receiver
	farplaneReceiver = relConst.ConstrainObject(farPlane)
	relConst.SetBoxPosition(farplaneReceiver, 1700, 550)
	farplaneReceiver.UseGlobalTransforms = False

	#NearPlane Receiver
	nearplaneReceiver = relConst.ConstrainObject(nearPlane)
	relConst.SetBoxPosition(nearplaneReceiver, 1700, 450)
	nearplaneReceiver.UseGlobalTransforms = False

	#Connect Boxes
	ConnectBox(camSend, 'FieldOfView', log, 'a')
	ConnectBox(camSend, 'DOF Multiplier', Mult, 'b')
	ConnectBox(camSend, 'DOF Expand', NumVec, 'X')
	ConnectBox(camSend, 'FieldOfView', Mult2, 'a')
	ConnectBox(camSend, 'FOCUS', add, 'a')
	ConnectBox(camSend, 'FOCUS', add2, 'a')
	ConnectBox(log, 'Result', Mult, 'a')
	ConnectBox(Mult, 'Result', Mult2, 'b')
	ConnectBox(Mult2, 'Result', NumVec2, 'X')
	ConnectBox(NumVec2, 'Result', VecNum, 'V')
	ConnectBox(NumVec2, 'Result', AddVec, 'V1')
	ConnectBox(VecNum, 'X', Mult3, 'a')
	ConnectBox(Mult3, 'Result', NumVec3, 'X')
	ConnectBox(NumVec3, 'Result', SubVec, 'V1')
	ConnectBox(SubVec, 'Result', VecNum2, 'V')
	ConnectBox(VecNum2, 'X', add, 'b')
	ConnectBox(add, 'Result', NumVec4, 'X')
	ConnectBox(NumVec4, 'Result', nearplaneReceiver, 'Lcl Translation')
	ConnectBox(NumVec, 'Result', SubVec, 'V2')
	ConnectBox(NumVec, 'Result', AddVec, 'V2')
	ConnectBox(AddVec, 'Result', VecNum3, 'V')
	ConnectBox(VecNum3, 'X', add2, 'b')
	ConnectBox(add2, 'Result', NumVec5, 'X')
	ConnectBox(NumVec5, 'Result', farplaneReceiver, 'Lcl Translation')

	#ConnectBox(NumVec6, 'Result', farblendplaneReceiver, 'Lcl Translation')
	#ConnectBox(NumVec7, 'Result', farblendplaneReceiver, 'Lcl Translation')


	#===============================
	# Create FAR Blend
	#FarPlane Receiver
	farBlendReceiver = relConst.ConstrainObject(farBlendPlane)
	relConst.SetBoxPosition(farBlendReceiver, 1300, 1200)
	farBlendReceiver.UseGlobalTransforms = False

	#Set Far Plane as source
	#Camera Sender
	farPlaneSend = relConst.SetAsSource(farPlane)
	relConst.SetBoxPosition(farPlaneSend, 0, 1200)
	farPlaneSend.UseGlobalTransforms = False

	#multiply for the blend multi
	#Multiply (a x b)
	farMulti = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(farMulti, 400, 1200)
	ConnectBox(camSend, 'Far Blend', farMulti, 'a')
	ConnectBox(camSend, 'Far Blend Multi', farMulti, 'b')

	#Vector to num for farplane.x
	#Vector to Number3
	farPlaneVectToNum = relConst.CreateFunctionBox('Converters', 'Vector to Number')
	relConst.SetBoxPosition(farPlaneVectToNum, 400, 1300)
	ConnectBox(farPlaneSend, 'Lcl Translation', farPlaneVectToNum, 'V')

	#Add numbers farblend multi + farPlane.x
	#Add(a + b)
	addFarBlend = relConst.CreateFunctionBox('Number', 'Add (a + b)')
	relConst.SetBoxPosition(addFarBlend, 800, 1300)
	ConnectBox(farMulti, 'Result', addFarBlend, 'a')
	ConnectBox(farPlaneVectToNum, 'X', addFarBlend, 'b')

	#Num to Vector for far blend plane
	#Number to Vector
	farBlendPlaneNumToVector = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(farBlendPlaneNumToVector, 1000, 1200)
	ConnectBox(addFarBlend, 'Result', farBlendPlaneNumToVector, 'X')

	#connect the output to the far blend plane
	ConnectBox(farBlendPlaneNumToVector, 'Result', farBlendReceiver, 'Lcl Translation')



	#===============================
	# Create NEAR Blend
	#FarPlane Receiver
	nearBlendReceiver = relConst.ConstrainObject(nearBlendPlane)
	relConst.SetBoxPosition(nearBlendReceiver, 1300, 1500)
	nearBlendReceiver.UseGlobalTransforms = False

	#Set near Plane as source
	#Near plane sender
	nearPlaneSend = relConst.SetAsSource(nearPlane)
	relConst.SetBoxPosition(nearPlaneSend, 0, 1500)
	nearPlaneSend.UseGlobalTransforms = False

	#multiply for the blend multi inversion
	#Multiply (a x b)
	nearBlendInver = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(nearBlendInver, 400, 1500)

	#send the cams near blend to the inverter
	ConnectBox(camSend, 'Near Blend', nearBlendInver, 'a')
	#set the b slot to -1
	nearBlendInverB = FindAnimationNode(nearBlendInver.AnimationNodeInGet(),'b')
	nearBlendInverB.WriteData([-1])

	#multiply for the blend multi
	#Multiply (a x b)
	nearMulti = relConst.CreateFunctionBox('Number', 'Multiply (a x b)')
	relConst.SetBoxPosition(nearMulti, 600, 1500)
	#connect the inverse to this multi
	ConnectBox(nearBlendInver, 'Result', nearMulti, 'b')
	#connect the cam.near blend multi to the multi
	ConnectBox(camSend, 'Near Blend Multi', nearMulti, 'a')

	#Vector to num for nearplane.x
	#Vector to Number3
	nearPlaneVectToNum = relConst.CreateFunctionBox('Converters', 'Vector to Number')
	relConst.SetBoxPosition(nearPlaneVectToNum, 400, 1800)
	ConnectBox(nearPlaneSend, 'Lcl Translation', nearPlaneVectToNum, 'V')

	#Add numbers nearblend multi + nearPlane.x
	#Add(a + b)
	addNearBlend = relConst.CreateFunctionBox('Number', 'Add (a + b)')
	relConst.SetBoxPosition(addNearBlend, 800, 1800)
	ConnectBox(nearMulti, 'Result', addNearBlend, 'a')
	ConnectBox(nearPlaneVectToNum, 'X', addNearBlend, 'b')

	#Num to Vector for near blend plane
	#Number to Vector
	nearBlendPlaneNumToVector = relConst.CreateFunctionBox('Converters', 'Number to Vector')
	relConst.SetBoxPosition(nearBlendPlaneNumToVector, 1000, 1500)
	ConnectBox(addNearBlend, 'Result', nearBlendPlaneNumToVector, 'X')

	#connect the output to the near blend plane
	ConnectBox(nearBlendPlaneNumToVector, 'Result', nearBlendReceiver, 'Lcl Translation')    
	#===============================================================       
	#connect the output to the near blend plane
	ConnectBox(nearBlendPlaneNumToVector, 'Result', nearBlendReceiver, 'Lcl Translation')  

	#--------------------------------
	return cam


def Run():
	cameraList = []
	
	frameRange = FBSystem().CurrentTake.LocalTimeSpan.GetDuration().GetFrame(1)
	
	exportBool = 0
	exportCamera = None
	
	FBSystem().CurrentTake.SetCurrentLayer(0)
	cameraList = []
	dofList = []
	exportDofs = {}	

	result = FBMessageBox( 'Rockstar', 'This tool will find and replace old GTA cameras with new ones. The purpose of this tool is to fix MotionBuilder crashes with missing custom attributes on older gta cameras.\nIf you have 10+ cameras in the file, expect this process to take hours to complete.  You can thank MotionBuilder for that...\n\nDo you want to continue?', 'Yes', 'No' )
	
	if result == 1:
		print 'Replacing old GTA cameras...'
	
		switcher = FBFindModelByLabelName('Camera Switcher')
		for cam in FBSystem().Scene.Cameras:
			if 'Producer' not in cam.Name and "Export" not in cam.Name:
				#this is not a system cam nor an export cam nor switcher based on name...
				cameraList.append(cam)
				children = cam.Children
				camDofPlanes = {}
				for child in children:
					name = child.Name.lower()
					if "plane" in name:
						tok = name.split(" ")
						camDofPlanes[tok[1]] = child
	
				dofList.append(camDofPlanes)
	
		propertyList = ['Translation','Rotation','FOCUS','DOF Multiplier','DOF Expand','DOF Strength','FieldOfView','Near Blend', 'Far Blend', 'Near Blend Multi', 'Far Blend Multi']
	
		for cam in cameraList:
			newCamera = createNewCam()
	
			relConst = Constraint.CreateConstraint(Constraint.RELATION)
			camSend = relConst.SetAsSource(cam)
			relConst.SetBoxPosition(camSend, 0, 0)
			camSend.UseGlobalTransforms = True
	
			camDest = relConst.ConstrainObject(newCamera)
			relConst.SetBoxPosition(camDest, 500, 0)
			camDest.UseGlobalTransforms = True
	
			for property in propertyList:
				ConnectBox(camSend, property, camDest, property)
	
	
			oldCamName = cam.Name    
			cam.Name = "OLD_CAM_" + oldCamName
			newCamera.Name = oldCamName
	
			#rename the old cameras dof planes
			for child in cam.Children:
				newName = child.Name.replace(newCamera.Name, cam.Name)
				child.Name = newName
				#child.FBDelete()
	
			#rename the new cameras dof planes
			for child in newCamera.Children:
				newName = child.Name.replace('CAMERATOREPLACETHEOLD', newCamera.Name)
				child.Name = newName
	
			# Transfer FOV settings
			newCamera.FieldOfView = cam.FieldOfView
			newCamera.FieldOfViewX = cam.FieldOfViewX
			newCamera.FieldOfViewY = cam.FieldOfViewY
			newCamera.FilmAspectRatio = cam.FilmAspectRatio
			newCamera.FilmSizeHeight = cam.FilmSizeHeight
			newCamera.FilmSizeWidth = cam.FilmSizeWidth
	
			transferCameraAnimation( cam, newCamera )
	
			# Hide DOF planes  
			lCameraPlaneHideList = [] 
	
			for iCamera in glo.rs_NonSystemCameraList():
				if len(iCamera.Children) > 0:
					for iChild in iCamera.Children:
						lCameraPlaneHideList.append(iChild)
	
			if len(lCameraPlaneHideList) > 0:
				for iPlane in lCameraPlaneHideList:
					iPlane.Show = False
	
	


