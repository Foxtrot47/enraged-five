

class BedDict(object):
    bedDict = {'Low Height 0.45m':45,
               'High Height 0.8m':80,
                }


class ChairDict(object):
    chairDict = {'Standard Height 0.45m':45,
                }


class CounterDict(object):
    counterDict = {'Standard Height 1.05m ':80,
                }


class TableDict(object):
    tableDict = {'Standard Height 0.8m':80,
                 'Coffee Table Height 0.45m':45,
                }


class PropStringDict(object):
    '''
    '''
    propStringDict = {'Bed':BedDict.bedDict,
                      'Chair':ChairDict.chairDict,
                      'Counter':CounterDict.counterDict,
                      'Table':TableDict.tableDict,
                      }