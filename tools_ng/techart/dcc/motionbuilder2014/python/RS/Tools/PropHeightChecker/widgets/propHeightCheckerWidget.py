import pyfbsdk as mobu

import os

from PySide import QtGui, QtCore

from RS import Config, Perforce
from RS import Globals
from RS.Tools import UI
from RS.Tools.PropHeightChecker import const


class PropHeightCheckerDialog(UI.QtMainWindowBase):
    '''
    Dialogue to run PropHeightCheckerWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropHeightCheckerDialog, self).__init__(parent=parent,
                                                      title='Prop Height Checker Dialog',
                                                      size=(800, 250),
                                                      store=False,
                                                      dockable=True,
                                                      )

        # create layout and add widget
        self.setCentralWidget(PropHeightCheckerWidget())


class PropHeightCheckerWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(PropHeightCheckerWidget, self).__init__(parent=parent)
        self._imagePath = os.path.join(Config.Script.Path.ToolImages,
                                        "PropHeightChecker",
                                        "Prop_Lineup_1.jpg")
        self._setupUi()

    def _populatePropComboBox(self):
        '''
        populating comboBox for prop strings, using const to generate the string items.
        '''
        # clear boxes to populate from fresh
        self._propComboBox.clear()

        # get prop and prop types
        for key, value in const.PropStringDict().propStringDict.iteritems():
            self._propComboBox.addItem(key, value)

        # populate the propMeasurementComboBox
        self._populatePropMeasurementComboBox()

    def _populatePropMeasurementComboBox(self):
        '''
        using the user data of the current index of the propComboBox, we can get the related prop
        dict to populate the propMeasurementComboBox
        '''
        # clear boxes to populate from fresh
        self._propMeasurementComboBox.clear()

        # using the user data, we can get the related constraint object to the current index
        currentPropComboBoxIndex = self._propComboBox.currentIndex()
        propMeasurementDict = self._propComboBox.itemData(currentPropComboBoxIndex)

        # add items to the _propMeasurementComboBox
        for key, value in propMeasurementDict.iteritems():
            self._propMeasurementComboBox.addItem(key, value)

    def _populatePrimitivesComboBox(self):
        '''
        populating comboBox for primitives existing in the scene
        '''
        # clear box to populate from fresh
        self._primitivesComboBox.clear()

        # get primitives in the scene
        primitiveList = self._getScenePrimitiveList()
        if primitiveList is None:
            return

        # add to combobox
        for primitive in primitiveList:
            self._primitivesComboBox.addItem(primitive.LongName, primitive)

    def _getScenePrimitiveList(self):
        '''
        Returns:
            list(FBModelCube)
        '''
        primitiveList = [component for component in Globals.Components if isinstance(component, mobu.FBModelCube)
                         and component.PropertyList.Find('Prop Checker Primitive') is not None]

        if not len(primitiveList) > 0:
            return None

        return primitiveList

    def _propComboBoxIndexChanged(self):
        '''
        user has changed the index of the _propComboBox, so we need to refresh the
        _propMeasurementComboBox list
        '''
        self._populatePropMeasurementComboBox()

    def _createPrimitive(self):
        '''
        based on the user's selection, a primitive will be created and snap to a selected object.

        only 1 object may be selected at a time.
        '''
        # get prop type
        currentPropComboBoxString = self._propComboBox.currentText()
        currentMeasurementComboBoxString = self._propMeasurementComboBox.currentText()
        # get measurement
        propMeasurementComboBoxIndex = self._propMeasurementComboBox.currentIndex()
        propMeasurement = self._propMeasurementComboBox.itemData(propMeasurementComboBoxIndex)

        # create primitive
        if not isinstance(propMeasurement, (float, int)):
            return

        primitiveModel = mobu.FBModelCube('{0}_{1}'.format(currentPropComboBoxString, currentMeasurementComboBoxString))
        # move the pivot tot he base of the model cube
        geoOffsetProperty = primitiveModel.PropertyList.Find('GeometricTranslation')
        if geoOffsetProperty is not None:
            geoOffsetProperty.Data = mobu.FBVector3d(0, 1, 0)
        # set scale - divide by 2, not sure why, but thats what works!
        primitiveModel.Scaling = mobu.FBVector3d(1, propMeasurement/2, 1)
        primitiveModel.Show = True
        # add custom property for easy finding
        customProperty = primitiveModel.PropertyCreate('Prop Checker Primitive', mobu.FBPropertyType.kFBPT_charptr, "", False, True, None)
        customProperty.Data = str(currentPropComboBoxString)

        # update primitiveComboBox List
        self._populatePrimitivesComboBox()

        # change current index to the last index
        primitiveCount = self._primitivesComboBox.count()
        self._primitivesComboBox.setCurrentIndex(primitiveCount - 1)

        # snap primitive to selected object, only is user has 1 object selected
        self._snapPrimitive()

    def _snapPrimitive(self):
        '''
        snaps selected primitive to selected object.

        only 1 object may be selected at a time.
        '''
        # get current primitive
        primitiveComboBoxIndex = self._primitivesComboBox.currentIndex()
        currentPrimitive = self._primitivesComboBox.itemData(primitiveComboBoxIndex)

        if currentPrimitive is None:
            return

        # get scene selected item
        modelSelectedList = mobu.FBModelList()
        mobu.FBGetSelectedModels(modelSelectedList, None, True)

        if not len(modelSelectedList) == 1:
            return

        selectedModel = modelSelectedList[0]

        # snap to selected
        selectedTrns = mobu.FBVector3d()
        selectedModel.GetVector(selectedTrns, mobu.FBModelTransformationType.kModelTranslation, True)
        currentPrimitive.SetVector(selectedTrns, mobu.FBModelTransformationType.kModelTranslation, True)

    def _clearAllPrimitives(self):
        '''
        deletes all primitives in the scene.

        primitives are found via a custom property.
        '''
        # get primitives in the scene
        primitiveList = self._getScenePrimitiveList()
        if primitiveList is None:
            return

        # delete
        for primitive in primitiveList:
            primitive.FBDelete()

        # update primitive combobox
        self._populatePrimitivesComboBox()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # create widgets
        banner = UI.BannerWidget(name='Prop Height Checker')
        self._propComboBox = QtGui.QComboBox()
        self._propMeasurementComboBox = QtGui.QComboBox()
        self._primitivesComboBox = QtGui.QComboBox()
        createButton = QtGui.QPushButton('Create and Snap Primitive')
        snapButton = QtGui.QPushButton('Snap Primitive to Selected')
        clearButton = QtGui.QPushButton('Clear all Primitives')
        propImage = QtGui.QLabel()

        # widget properties
        createButton.setToolTip("Upon creation, primitive will be snapped to any object selected.\n\nIf no objects are selected, or more than 1 object is selected,\nthe primitive will appear at the origin.")
        snapButton.setToolTip("Primitive can only be snapped to 1 selected object.\n\nIf more than 1 objects are sleected, nothing will happen.")
        clearButton.setToolTip("All primivitives in the scene will be deleted.")
        Perforce.Sync(self._imagePath)
        propImage.setPixmap(QtGui.QPixmap(self._imagePath))
        propImage.setScaledContents(True)
        propImage.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Ignored)

        # populate comboBoxes
        self._populatePropComboBox()
        self._populatePrimitivesComboBox()

        # set widgets to layout
        mainLayout.addWidget(banner, 0 , 0, 1, 3)
        mainLayout.addWidget(self._propComboBox, 1 , 0)
        mainLayout.addWidget(self._propMeasurementComboBox, 1 , 1)
        mainLayout.addWidget(createButton, 1, 2)
        mainLayout.addWidget(snapButton, 2, 2)
        mainLayout.addWidget(self._primitivesComboBox, 2, 1)
        mainLayout.addWidget(clearButton, 3, 2)
        mainLayout.addWidget(propImage, 4, 0, 1, 3)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        createButton.pressed.connect(self._createPrimitive)
        snapButton.pressed.connect(self._snapPrimitive)
        clearButton.pressed.connect(self._clearAllPrimitives)
        self._propComboBox.currentIndexChanged.connect(self._propComboBoxIndexChanged)
