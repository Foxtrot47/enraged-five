'''

 Script Path: RS/Core/Reference/RTCCamRockSnapToolbox.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 26 November 2013
 
 Description: UI to display list of Scene cameras and camrock offsets to snap 
              and constrain

'''

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Core.Mocap.MocapCommands
reload( RS.Core.Mocap.MocapCommands )

###############################################################################################################
## UI Populate
###############################################################################################################

class RTCCamRockSnapTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'RTC Toolbox', size = [ 415, 350 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Creating_a_Lattice' )
        
    def Create( self, main ):
        
        tabControl = FBTabControl()
        tabControlX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        tabControlY = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        tabControlW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        tabControlH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        main.AddRegion( "tabControl", "tabControl", tabControlX, tabControlY, tabControlW, tabControlH )
        main.SetControl( "tabControl", tabControl )

        ###############################################################################################################
        ## RTCOffset Tab
        ###############################################################################################################

        rtcTab = FBLayout()                
        rtcTabX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        rtcTabY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        rtcTabW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        rtcTabH = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        rtcTab.AddRegion("rtcTab","rtcTab", rtcTabX, rtcTabY, rtcTabW, rtcTabH)
        tabControl.Add( "RTC Offset", rtcTab )

        camSnapLabel = FBLabel()
        camSnapLabel.Caption = "Pick your CamRock Offset and which Camera you wish to snap its position to:"
        camSnapLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        camSnapLabelY = FBAddRegionParam(5,FBAttachType.kFBAttachTop,"")
        camSnapLabelW = FBAddRegionParam(450,FBAttachType.kFBAttachNone,"")
        camSnapLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "camSnapLabel","camSnapLabel", camSnapLabelX, camSnapLabelY, camSnapLabelW, camSnapLabelH )
        rtcTab.SetControl( "camSnapLabel",camSnapLabel )
       
        offsetDropDown = FBList()
        offsetDropDown.Style = FBListStyle.kFBDropDownList
        offsetDropDownX = FBAddRegionParam( 10,FBAttachType.kFBAttachLeft,"" )
        offsetDropDownY = FBAddRegionParam( 5,FBAttachType.kFBAttachBottom, "camSnapLabel" )
        offsetDropDownW = FBAddRegionParam( 150,FBAttachType.kFBAttachNone,"" )
        offsetDropDownH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )            
        rtcTab.AddRegion( "offsetDropDown","offsetDropDown", offsetDropDownX, offsetDropDownY, offsetDropDownW, offsetDropDownH )
        rtcTab.SetControl( "offsetDropDown", offsetDropDown )
        
        #list camera rocks
        cameraRockList = []
        
        for component in RS.Globals.gComponents:
            if 'camrock' in component.Name.lower() and 'offset' in component.Name.lower():
                if component.ClassName() == 'FBModelNull':
                    cameraRockList.append( component )
            
        if len( cameraRockList ) > 0:
            for i in cameraRockList:
                offsetDropDown.Items.append( i.LongName )


        cameraDropDown = FBList()
        cameraDropDown.Style = FBListStyle.kFBDropDownList
        cameraDropDownX = FBAddRegionParam( 10,FBAttachType.kFBAttachRight,"offsetDropDown" )
        cameraDropDownY = FBAddRegionParam( 5,FBAttachType.kFBAttachBottom, "camSnapLabel" )
        cameraDropDownW = FBAddRegionParam( 150,FBAttachType.kFBAttachNone,"" )
        cameraDropDownH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )            
        rtcTab.AddRegion( "cameraDropDown","cameraDropDown", cameraDropDownX, cameraDropDownY, cameraDropDownW, cameraDropDownH )
        rtcTab.SetControl( "cameraDropDown", cameraDropDown )
        
        #list scene cameras
        cameraList = []
        
        for camera in RS.Globals.gCameras:
            if camera.SystemCamera:
                None
            elif camera.Name.lower() == 'movercamera' or '3lateral' in camera.Name.lower() or 'headcam' in camera.Name.lower()or 'gamestream' in camera.Name.lower():
                None
            elif camera.Parent == None:
                cameraList.append( camera )
        
        if len( cameraList ) > 0:        
            for i in cameraList:
                cameraDropDown.Items.append( i.LongName )
            
        
        constraintLabel = FBLabel()
        constraintLabel.Caption = "Select your constraint option:"
        constraintLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        constraintLabelY = FBAddRegionParam(20,FBAttachType.kFBAttachBottom,"offsetDropDown")
        constraintLabelW = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
        constraintLabelH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "constraintLabel","constraintLabel", constraintLabelX, constraintLabelY, constraintLabelW, constraintLabelH )
        rtcTab.SetControl( "constraintLabel",constraintLabel )
        
        
        noneRadioButton = FBButton()
        noneRadioButton.Caption = "None"
        noneRadioButton.State = True
        noneRadioButton.Style = FBButtonStyle.kFBRadioButton
        noneRadioButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        noneRadioButtonY = FBAddRegionParam(10,FBAttachType.kFBAttachBottom,"constraintLabel")
        noneRadioButtonW = FBAddRegionParam(50,FBAttachType.kFBAttachNone,"")
        noneRadioButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "noneRadioButton", "noneRadioButton", noneRadioButtonX, noneRadioButtonY, noneRadioButtonW, noneRadioButtonH )
        rtcTab.SetControl( "noneRadioButton", noneRadioButton )    
            
        pcRadioButton = FBButton()
        pcRadioButton.Caption = "Parent Child Constraint"
        pcRadioButton.Style = FBButtonStyle.kFBRadioButton
        pcRadioButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        pcRadioButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachBottom,"noneRadioButton")
        pcRadioButtonW = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
        pcRadioButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "pcRadioButton", "pcRadioButton", pcRadioButtonX, pcRadioButtonY, pcRadioButtonW, pcRadioButtonH )
        rtcTab.SetControl( "pcRadioButton", pcRadioButton )               
            
        positionRadioButton = FBButton()
        positionRadioButton.Caption = "Position Constraint"
        positionRadioButton.Style = FBButtonStyle.kFBRadioButton
        positionRadioButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        positionRadioButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachBottom,"pcRadioButton")
        positionRadioButtonW = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
        positionRadioButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "positionRadioButton", "positionRadioButton", positionRadioButtonX, positionRadioButtonY, positionRadioButtonW, positionRadioButtonH )
        rtcTab.SetControl( "positionRadioButton", positionRadioButton )    
        
        buttonGroup = FBButtonGroup()
        buttonGroup.Add(noneRadioButton)
        buttonGroup.Add(pcRadioButton)
        buttonGroup.Add(positionRadioButton)

        
        snapButton = FBButton()
        snapButton.Caption = "Snap"
        snapButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        snapButtonY = FBAddRegionParam(20,FBAttachType.kFBAttachBottom,"positionRadioButton")
        snapButtonW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        snapButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        rtcTab.AddRegion( "snapButton", "snapButton", snapButtonX, snapButtonY, snapButtonW, snapButtonH )
        rtcTab.SetControl( "snapButton", snapButton )            


        ###############################################################################################################
        ## RTCOffset Callback
        ###############################################################################################################
        
        def dropDownCallBack( control, event ):
            
            global noneConstraintBool
            global pcConstraintBool
            global positionConstraintBool
            
            noneConstraintBool = False
            pcConstraintBool = False
            positionConstraintBool = False
            
            selectedOffset = None
            selectedCamera = None
            
            for i in range(len(offsetDropDown.Items)):
                if offsetDropDown.IsSelected(i):
                    selectedOffset =  offsetDropDown.Items[i]
            
            for i in range(len(cameraDropDown.Items)):
                if cameraDropDown.IsSelected(i):
                    selectedCamera = cameraDropDown.Items[i]
                    
            if noneRadioButton.State == True:
                noneConstraintBool = True
            
            if pcRadioButton.State == True:
                pcConstraintBool = True            
                                   
            if positionRadioButton.State == True:
                positionConstraintBool = True                
              
            RS.Core.Mocap.MocapCommands.RTCOffset( selectedOffset, selectedCamera, noneConstraintBool, pcConstraintBool, positionConstraintBool )
        
        snapButton.OnClick.Add(dropDownCallBack)

        
        
def Run():
    tool = RTCCamRockSnapTool()  
    tool.Show()
