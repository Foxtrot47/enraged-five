"""
UI interface for layering previz animation
"""
import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS.Tools import UI
from RS import Globals
from RS.Core.Mocap import Clapper
from RS.Utils.Scene import Character, Groups
from RS.Core.Mocap.Previz import LayeringTool


class LayeringToolUI(UI.QtMainWindowBase):
    """
    GUI Tool for the layering tool
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Previz Animation Layering Tool"
        super(LayeringToolUI, self).__init__(title=self._toolName, dockable=True)

        self.setupUi()

    def setupUi(self):
        """
        Set up the UI and attach the connections
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        zeroControlLayout = QtGui.QGridLayout()

        # Widgets
        banner = UI.BannerWidget(
                                 self._toolName,
                                 )
        refreshButton = QtGui.QPushButton("Refresh Characters")
        self._characterList = QtGui.QTreeWidget()
        self._propList = QtGui.QTreeWidget()
        self._layerNameTextbox = QtGui.QLineEdit()
        createLayerButton = QtGui.QPushButton("Create Layer with Selected Characters")
        zeroControlsBox = QtGui.QGroupBox("Zero Scene Controls")
        self._clapperBeepsRadio = QtGui.QRadioButton("Clapper Beeps")
        self._clapperBeepsDrop = QtGui.QComboBox()
        self._manualFrameRadio = QtGui.QRadioButton("Manual Frame Number")
        self._manualFrameSpinner = QtGui.QSpinBox()
        self._noZeroing = QtGui.QRadioButton("No Zeroing")

        # Configure Widgets
        self._characterList.setHeaderLabels(["Character Name", "Is Hidden"])
        self._characterList.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self._characterList.setAlternatingRowColors(True)
        self._characterList.setSortingEnabled(True)
        self._propList.setHeaderLabels(["Prop Toy Name", "Is Hidden"])
        self._propList.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self._propList.setAlternatingRowColors(True)
        self._propList.setSortingEnabled(True)
        banner.setMaximumHeight(30)
        self._clapperBeepsRadio.setChecked(True)
        self._manualFrameSpinner.setEnabled(False)
        self._manualFrameSpinner.setMaximum(999999)
        self._manualFrameSpinner.setMinimum(-999999)

        # Add Widgets to Layouts
        layout.addWidget(banner)
        layout.addWidget(refreshButton)
        layout.addWidget(self._characterList)
        layout.addWidget(self._propList)
        layout.addWidget(QtGui.QLabel("Layer Name:"))
        layout.addWidget(self._layerNameTextbox)
        layout.addWidget(zeroControlsBox)
        layout.addWidget(createLayerButton)

        zeroControlLayout.addWidget(self._clapperBeepsRadio, 0, 0)
        zeroControlLayout.addWidget(self._clapperBeepsDrop, 0, 1)
        zeroControlLayout.addWidget(self._manualFrameRadio, 1, 0)
        zeroControlLayout.addWidget(self._manualFrameSpinner, 1, 1)
        zeroControlLayout.addWidget(self._noZeroing, 2, 0)

        # Set Layouts to Widgets
        zeroControlsBox.setLayout(zeroControlLayout)
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

        # Hook up connections
        refreshButton.pressed.connect(self._handleRefreshButton)
        createLayerButton.pressed.connect(self._handleCreateLayerButton)
        self._clapperBeepsRadio.toggled.connect(self._handleRadioButtonStateChanged)
        self._manualFrameRadio.toggled.connect(self._handleRadioButtonStateChanged)
        self._noZeroing.toggled.connect(self._handleRadioButtonStateChanged)

        # Populate the UI
        self.RePopulateModels()

    def _handleRadioButtonStateChanged(self):
        """
        Internal Method

        Handle the state change on the radio buttons
        """
        self._clapperBeepsDrop.setEnabled(self._clapperBeepsRadio.isChecked())
        self._manualFrameSpinner.setEnabled(self._manualFrameRadio.isChecked())

    def _handleRefreshButton(self):
        """
        Internal Method

        Handle the character refresh button being pressed
        """
        self.RePopulateModels()

    def RePopulateModels(self):
        """
        Force an Update all the models in the UI such as the Characters, Props, Clapper index and
        Layer Name
        """
        self._PopulateModel()
        self._PopulatePropsModel()
        self._PopulateBleeps()
        self._PopulateLayerName()

    def _handleCreateLayerButton(self):
        """
        Internal Method

        Handle the layer button being pressed
        """
        if Clapper.GetTakeSlateGroup() is None:
            QtGui.QMessageBox.critical(self, self._toolName, "No Slate Group Detected. Please add the slate to the previz slate group. Aborting")
            return

        # Get the offset frame number
        offsetFrame = None
        if self._clapperBeepsRadio.isChecked():
            currentText = self._clapperBeepsDrop.currentText()
            if currentText == "":
                QtGui.QMessageBox.warning(self, self._toolName, "No Clapper Frame Choosen")
                return
            offsetFrame = int(currentText)
        elif self._manualFrameRadio.isChecked():
            offsetFrame = self._manualFrameSpinner.value()

        # Get the selected Characters
        characters = []
        for item in self._characterList.selectedItems():
            characters.append(item.data(0, QtCore.Qt.UserRole))

        # Get the selected Props
        props = []
        for item in self._propList.selectedItems():
            props.append(item.data(0, QtCore.Qt.UserRole))

        if len(characters) == 0 and len(props) == 0:
            QtGui.QMessageBox.warning(self, self._toolName, "No Characters Selected")
            return

        # Get the layer name text
        layerName = self._layerNameTextbox.text()

        # Run the library method
        LayeringTool.AnimationLayering(characters, offsetFrame, layerName=layerName, props=props)

        # Refresh with changes and alert the user the job is complete
        self.RePopulateModels()
        self._noZeroing.setChecked(True)
        QtGui.QMessageBox.information(self, self._toolName, "Layering Complete")

    def _PopulateModel(self):
        """
        Internal Method

        Method to handle the adding or removing of characters from the treeView
        """
        currentChars = {}
        # Get all the current Characters in the treeview
        for idx in xrange(self._characterList.topLevelItemCount()):
            item = self._characterList.topLevelItem(idx)
            char = item.data(0, QtCore.Qt.UserRole)
            currentChars[char] = idx

        takeNames = [take.Name for take in mobu.FBSystem().Scene.Takes]
        namespacesToIgnore = ["REF_POSE"]
        allChars = []
        for char in Globals.gCharacters:
            if not any([ns in char.LongName for ns in takeNames + namespacesToIgnore]):
                allChars.append(char)

        # Delete any characters in the treeview which dont exists in the scene anymore
        preDelete = 0
        for char, item in currentChars.iteritems():
            if char not in allChars:
                self._characterList.takeTopLevelItem(idx - preDelete)
                preDelete += 1

        # Add any new characters from the scene into the treeview
        for char in allChars:
            if char in currentChars.keys():
                continue
            newItem = QtGui.QTreeWidgetItem()
            # Name
            newItem.setText(0, char.LongName)
            # If the character is hidden
            newItem.setText(1, str(Character.CharacterHidden(char)))
            # The actual mobu character object
            newItem.setData(0, QtCore.Qt.UserRole, char)

            self._characterList.addTopLevelItem(newItem)

    @classmethod
    def _getPropToys(cls):
        """
        Class method

        Internal Method

        Get all the proptoy groups in the scene

        Return:
            List of FBGroups: All the proptoy groups
        """
        allGroups = []
        # Get by property
        for obj in [comp for comp in mobu.FBSystem().Scene.Components if isinstance(comp, mobu.FBModel) and comp.PropertyList.Find("propSetup") is not None]:
            grps = Groups.GetGroupsFromObject(obj)
            if len(grps) > 0 and grps[0] in allGroups:
                continue
            allGroups.append(grps[0])

        # Get by group
        for grp in Globals.gGroups:
            if str(grp.Name).lower().startswith("prop") and not grp in allGroups:
                allGroups.append(grp)

        return allGroups

    def _PopulatePropsModel(self):
        """
        Internal Method

        Method to handle the adding or removing of props from the treeView
        """
        currentProps = {}
        # Get all the current Characters in the treeview
        for idx in xrange(self._propList.topLevelItemCount()):
            item = self._propList.topLevelItem(idx)
            prop = item.data(0, QtCore.Qt.UserRole)
            currentProps[prop] = idx

        takeName = mobu.FBSystem().CurrentTake.Name
        allProps = [prop for prop in self._getPropToys() if takeName not in prop.LongName]

        # Delete any characters in the treeview which dont exists in the scene anymore
        preDelete = 0
        for prop, item in currentProps.iteritems():
            if prop not in allProps:
                self._propList.takeTopLevelItem(idx - preDelete)
                preDelete += 1

        # Add any new characters from the scene into the treeview
        for prop in allProps:
            if prop in currentProps.keys():
                continue
            newItem = QtGui.QTreeWidgetItem()
            # Name
            newItem.setText(0, prop.LongName)
            # If the character is hidden
            newItem.setText(1, str(not prop.Show))
            # The actual mobu character object
            newItem.setData(0, QtCore.Qt.UserRole, prop)

            self._propList.addTopLevelItem(newItem)

    def _PopulateLayerName(self):
        """
        Internal Method

        Get the current take name and set it to the new layer name edit text box
        """
        takeName = mobu.FBSystem().CurrentTake.Name
        self._layerNameTextbox.setText(takeName)

    def _PopulateBleeps(self):
        """
        Internal Method

        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        self._clapperBeepsDrop.clear()

        # Get the current Take Slate Group
        slateGroup = Clapper.GetTakeSlateGroup()
        if slateGroup is None:
            return

        # See which slate is in the current take slate group
        for slateObj, beeps in Clapper.GetClaps().iteritems():
            if slateGroup in Groups.GetGroupsFromObject(slateObj):
                self._clapperBeepsDrop.addItems([str(beep) for beep in beeps])
                self._clapperBeepsDrop.setCurrentIndex(self._clapperBeepsDrop.count() - 1)
                return


def Run(show=True):
    layeringTool = LayeringToolUI()

    if show:
        layeringTool.show()

    return layeringTool

