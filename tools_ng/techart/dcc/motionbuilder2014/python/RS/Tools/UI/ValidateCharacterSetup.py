###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
## 
## Script Name: ValidateCharacterSetup
## Written And Maintained By: Kristine Middlemiss
## Contributors: 
## Description: Function to check the characters are setup correctly for ingame:
##              - ROM test, this would load an animation ROM onto the character to test for skinning issues 
##                (ideally this would be done on all geo vars). The ROM should be removed afterwards. This could be 
##                 loaded using motion file import.
##              - Floor contacts check - Ensure these are turned off
##              - Mover Group options - ensure 'show' is off and 'pick' and 'Trs' are on.
##              - Missing texture check
##
## Rules: Definitions: Prefixed with "rs_" 
##        Global Variables: Prefixed with "g"
##        Local Variables: Prefixed with "l"
##        Iteration Variables: Prefixed with "i"
##        Arguments: Prefixed with "p"
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################
from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils.Creation as cre
import RS.Core.Reference.ValidateCharacterSetup as core
import RS.Tools.UI


class ValidateCharacterSetupTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Validate Character Setup Tool', size = [ 695, 400 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Validate_Character_Setup' )
        
    def Create( self, pMain ):    
        # Warning
        lWarning = FBLabel()
        lWarning.Justify = FBTextJustify.kFBTextJustifyCenter
        lWarning.Caption = "REMINDER:\nFor ROM Check, DO NOT SAVE this file with the ROM in it! Save first then run 'Validate Scene'"
        
        lReferenceListX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lReferenceListY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        lReferenceListW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lReferenceListH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")  
        pMain.AddRegion("warning","warning", lReferenceListX,lReferenceListY,lReferenceListW,lReferenceListH)   
        pMain.SetControl("warning",lWarning)     
        
        # Tool Information
        sceneSetup = FBLabel()
        sceneSetup.Caption = "Check the validity of your Cutscene and In-Game character:"
        lTitlex = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lTitley = FBAddRegionParam(65,FBAttachType.kFBAttachTop,"")
        lTitlew = FBAddRegionParam(5,FBAttachType.kFBAttachRight,"")
        lTitleh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("setupTitle","setupTitle", lTitlex,lTitley,lTitlew,lTitleh)
        pMain.SetControl("setupTitle", sceneSetup)        
           
        #global gCheck
        lCheck = FBList()
        lCheck.Style = FBListStyle.kFBVerticalList  
        lCheckx = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lChecky = FBAddRegionParam(95,FBAttachType.kFBAttachTop,"")
        lCheckw = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lCheckh = FBAddRegionParam(-35,FBAttachType.kFBAttachBottom,"")
        pMain.AddRegion("Check","Check", lCheckx,lChecky,lCheckw,lCheckh)        
        pMain.SetControl("Check", lCheck)       
    
        lCheckButton = FBButton()
        lCheckButton.Hint = "Click this button to check the validity of your cutscene/ingame character."
        lCheckButton.Caption = "Validate Scene"
        lCheckButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lCheckButton.ListData = lCheck
        lCheckButton.Name = "Validate"
        lCheckButtonx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        lCheckButtony = FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,"")
        lCheckButtonw = FBAddRegionParam(120,FBAttachType.kFBAttachNone,None)
        lCheckButtonh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)  
        pMain.AddRegion("lCheckButton","lCheckButton", lCheckButtonx,lCheckButtony,lCheckButtonw,lCheckButtonh)    
        pMain.SetControl("lCheckButton",lCheckButton)  
        lCheckButton.OnClick.Add(core.rs_ValidateCharacterSetup)
    
        lFixButton = FBButton()
        lFixButton.Hint = "Click this button to check the validity of your cutscene/ingame character."
        lFixButton.Caption = "Fix Scene"
        lFixButton.Justify = FBTextJustify.kFBTextJustifyCenter
        
        lFixButton.ListData = lCheck
        lFixButtonx = FBAddRegionParam(130,FBAttachType.kFBAttachLeft,"")
        lFixButtony = FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,"")
        lFixButtonw = FBAddRegionParam(120,FBAttachType.kFBAttachNone,None)
        lFixButtonh = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)  
        pMain.AddRegion("lFixButton","lFixButton", lFixButtonx,lFixButtony,lFixButtonw,lFixButtonh)    
        pMain.SetControl("lFixButton",lFixButton)  
        lFixButton.Enabled = False
    
###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Define Tool and Launch UI
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################       

def Run( show = True ):   
     
    t = ValidateCharacterSetupTool()
    
    if show:
        t.Show()
        
    return t
