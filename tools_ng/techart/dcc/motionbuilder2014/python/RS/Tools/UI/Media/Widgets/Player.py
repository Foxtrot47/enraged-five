"""
Video player that uses the Phonon module to play videos through Qt using video backends that local machine has installed
"""
import os
import math

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.UI import Run
from RS.Tools.UI.Media.Widgets import Error

ERROR = ""
try:
    from PySide import phonon
    from RS.Tools.UI.Media.Widgets import Video

except ImportError, error:
    Video = None
    phonon = None
    ERROR = error


class VideoPlayer(QtGui.QWidget):
    """
    A widget for playing videos and scrubbing through them
    """
    __iconSize = QtCore.QSize(40, 40)
    __policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
    Buttons = ["Start", "Previous", "Play", "Next", "End"]

    def __init__(self, instanceKey="default", parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            instanceKey (string): dictionary key used to store/get a particular video player. The default value is
                                  'default'.

        """
        super(VideoPlayer, self).__init__(parent=parent)
        self.mainLayout = QtGui.QVBoxLayout()
        self.video = Video.Video(self, instanceKey=instanceKey)
        self.video.QWidget.hasVideoChanged = self.hasVideoChanged
        self.video.setSizePolicy(self.__policy)
        self.media = self.video.mediaObject()
        self._loop = False
        self.sliderLayout = QtGui.QHBoxLayout()
        self.slider = phonon.Phonon.SeekSlider(self.media, self)

        self.frameDisplay = QtGui.QSpinBox()
        self.frameDisplay.setAlignment(QtCore.Qt.AlignRight)
        self.frameDisplay.setFixedSize(50, 30)
        self.frameDisplay.setMinimum(0)
        self.frameDisplay.setMaximum(0)

        self.media.setPrefinishMark(250)
        [self.sliderLayout.addWidget(each) for each in [self.slider, self.frameDisplay]]

        numberOfButtons = float(len(self.Buttons))

        self.buttonLayout = QtGui.QHBoxLayout()
        for index, each in enumerate(self.Buttons):
            multiplier = abs(math.sin(math.radians((index/(numberOfButtons - 1)) * 180)))
            multiplier = 1.3 ** multiplier
            eachButton = QtGui.QPushButton(self)
            eachButton.setIcon(QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages,
                                           "VideoPlayer",
                                           "{}.png".format(each.lower()))))

            eachButton.setIconSize(self.iconSize * multiplier)
            eachButton.setFixedWidth(self.iconSize.width() * multiplier)

            self.buttonLayout.addWidget(eachButton)
            setattr(self, "{}Button".format(each), eachButton)

        self.buttonLayout.setSpacing(0)

        self.mainLayout.addWidget(self.video.QWidget)
        self.mainLayout.addLayout(self.sliderLayout)
        self.mainLayout.addLayout(self.buttonLayout)
        self.setLayout(self.mainLayout)
        
        self.connect(self.media, QtCore.SIGNAL("tick(qint64)"), self.updateFrameNumber)
        self.media.hasVideoChanged.connect(self.hasVideoChanged)
        self.media.stateChanged.connect(self.togglePlayPauseIcons)
        self.media.prefinishMarkReached.connect(self._loopVideo)

        self.StartButton.clicked.connect(self.start)
        self.PlayButton.clicked.connect(self.togglePlayPause)
        self.PreviousButton.clicked.connect(self.previous)
        self.NextButton.clicked.connect(self.next)
        self.EndButton.clicked.connect(self.end)

        self.frameDisplay.editingFinished.connect(self.updateFrame)

        # Set the look of our widget, to be incorporated later
        self.setStyleSheet("""
        QPushButton{
            border-style: outset;
            }
        Phonon--SeekSlider > QSlider {background-color: #252526;
                                      border: 1px solid #595959;
                                      border-radius: 5px;

                             }

        Phonon--SeekSlider > QSlider::handle:horizontal {
                     background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5c91d7, stop:1 #15498d);
                     border: 1px solid #0c457e;
                     width: 18px;
                     margin: -2px 0;
                     border-radius: 3px;
 }
        """)

    def play(self, path=""):
        """
        Plays the current video
        Arguments:
            path (string): path to the video to be played, if no path is passed the current video will be played
        """
        self.media.hasVideo()

        if not path and self.video.path:
            self.video.play()

        elif path and not self.video.path:
            self.video.play(path)
            self.video.mediaObject().setTickInterval(1000 / float(self.video.framesPerSecond))

    def previous(self):
        """ steps back one frame"""
        self.video.seek(self.video.currentTime() - (1000 / float(self.video.framesPerSecond)))

    def next(self):
        """ steps forward one frame"""
        self.video.seek(self.video.currentTime() + (1000 / float(self.video.framesPerSecond)))

    def pause(self):
        """ pauses video """
        self.video.pause()
        if self._loop:
            self.media.finished.emit()

    def start(self):
        """ go to the start of the video """
        self.video.seek(0)

    def end(self):
        """ go to the end of the video """
        self.video.seek(self.video.totalTime())

    def goToFrame(self, frame):
        """
        Goes to the given frame

        Arguments:
            frame (int): frame to go to

        """
        self.video.seek((1000 / float(self.video.framesPerSecond) * frame))

    def updateFrameNumber(self, time):
        """
        Updates the frame number displayed by the widget based on the given milliseconds

        Arguments:
            time = int/float; milliseconds to update the frame counter by
        """
        self.frameDisplay.setValue(int(round(time/(1000/self.video.framesPerSecond))))

    def updateFrame(self):
        """ Updates the video to go the frame on the qspinbox """

        frame = self.frameDisplay.value()

        milliseconds = float(self.video.totalTime())

        if not frame > self.video.totalFrames:
            milliseconds = frame * (1000/self.video.framesPerSecond)

        self.video.seek(round(milliseconds))

    def togglePlayPause(self, *args):
        """ Toggles between playing and pausing the video"""
        state = self.media.state()

        if state in [phonon.Phonon.PausedState, phonon.Phonon.StoppedState]:
            self.play()

        elif state == phonon.Phonon.PlayingState:
            self.pause()

    def togglePlayPauseIcons(self, state):
        """
        Toggles the icons of the Play/Pause button
        Arguments:
            state (phonon.Phonon.State): the state of the video (Playing, Paused, Stopped, etc.)
        """
        if state == phonon.Phonon.PausedState:
            self.PlayButton.setIcon(QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages,
                                                             "VideoPlayer", "play.png")))
            self.PlayButton.setIconSize(self.iconSize * 1.3)

        elif state == phonon.Phonon.PlayingState:
            self.PlayButton.setIcon(QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages,
                                                             "VideoPlayer", "pause.png")))
            self.PlayButton.setIconSize(self.iconSize * 1.3)

    def hasVideoChanged(self, value):
        """
        Overloaded method; sets the max number the frame qspinbox can be edited
        to the total number of frames in the video
        """
        self.frameDisplay.setMaximum(self.video.totalFrames)

    def loop(self):
        """
        Is video looping

        Return:
            boolean
        """
        return self._loop

    def setLoop(self, loop):
        """
        Set the video to loop

        Arguments:
            loop (boolean): should the video loop
        """
        self._loop = loop

    def _loopVideo(self, msecToEnd=0):
        """
        Loops the video if the widget is set to loop

        Arguments:
            msecToEnd) (int): milliseconds left of playback

        """
        if self._loop:
            # Forces video to go back to the beginning before it ends so it loops endlessly
            self.goToFrame(0)

    @property
    def path(self):
        """
        Path to the video to stream

        Return:
            string
        """
        return self.video.path

    @path.setter
    def path(self, path):
        """
        Sets the path to the video to stream

        Return:
            string
        """
        self.video.path = path

        # We set the path twice to force the video player to load the rest of the data of the video
        # ei. how long the video is
        # normally we override the hasVideoChanged method but it isn't getting called when the video changes
        # By forcing it to set the video twice we make sure that it has the latest information of the video.

        self.video.path = path

        self.frameDisplay.setMaximum(self.video.totalFrames)

    @property
    def iconSize(self):
        """
        Size of the icon

        Return:
            QtCore.QSize
        """
        return self.__iconSize

    @iconSize.setter
    def iconSize(self, width, height):
        """
        Sets the size of the icon

        Return:
            QtCore.QSize
        """
        self.__iconSize = QtCore.QSize(width, height)

    def codecErrorEnabled(self):
        """
        Is the codec error enabled

        Return:
            boolean
        """
        return self.video.CodecErrorEnabled()

    def setCodecErrorEnabled(self, enabled):
        """
        Set the enabled state of the codec error

        Arguments:
            enabled (boolean): should the codec error be raised
        """
        self.video.setCodecErrorEnabled(enabled)

if ERROR:
    VideoPlayer = Error.Error
    VideoPlayer.error = ERROR

@Run("Player")
def Run():
    return VideoPlayer()
