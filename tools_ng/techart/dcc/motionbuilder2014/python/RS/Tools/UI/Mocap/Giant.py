import pyfbsdk as mobu
import pyfbsdk_additions as mobuAdditions

from RS.Tools import UI
from RS.Core.Mocap.Giant import PrepAssets
from RS.Core.Mocap import MocapCommands


# container for user to only add one component
def ContainerDragAndDropSingle(control, event):
    if event.State == mobu.FBDragAndDropState.kFBDragAndDropDrag:
        event.Accept()
    elif event.State == mobu.FBDragAndDropState.kFBDragAndDropDrop:
        if event.Components[0]:
            if len(control.Items) == 0:
                control.Items.append(event.Components[0].LongName)
            else:
                control.Items.removeAll()
                control.Items.append(event.Components[0].LongName)

# container for user to add mulitple components
def ContainerDragAndDropMulti(control, event):
    if event.State == mobu.FBDragAndDropState.kFBDragAndDropDrag:
        event.Accept()
    elif event.State == mobu.FBDragAndDropState.kFBDragAndDropDrop:
        if event.Components[0]:
            for i in range(len(event.Components)):
                control.Items.append(event.Components[i].LongName)

# clear container fields
def DblClickClear(control, event):
    control.Items.removeAll()


def CheckPrepLattice(control, event):
    """
    Ask the user if the set has been updated or not
    """
    if mobu.FBMessageBox("Prep Lattice Check", "Has the set been updated?", "Yes, Continue", "No") == 1:
        MocapCommands.PrepLattice(control, event)


class GiantsTools(UI.Base):
    def __init__(self):
        super(GiantsTools, self).__init__("Giant Toolbox", size=(250, 330), helpUrl="https://devstar.rockstargames.com/wiki/index.php/Giant")

    def Create(self, pMain):
        lTabControl = mobuAdditions.FBTabControl()
        lTabControlX = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachLeft, "")
        lTabControlY = mobu.FBAddRegionParam(self.BannerHeight, mobu.FBAttachType.kFBAttachTop, "")
        lTabControlW = mobu.FBAddRegionParam(-5, mobu.FBAttachType.kFBAttachRight, "")
        lTabControlH = mobu.FBAddRegionParam(-5, mobu.FBAttachType.kFBAttachBottom, "")
        pMain.AddRegion("lTabControl", "lTabControl", lTabControlX, lTabControlY, lTabControlW, lTabControlH)
        pMain.SetControl("lTabControl", lTabControl)

        # First tab
        lAssetPrepContainer = mobu.FBLayout()
        lAssetPrepContainer.ItemIndex = 0
        lAssetPrepContainerX = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachLeft, "")
        lAssetPrepContainerY = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachBottom, "")
        lAssetPrepContainerW = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachRight, "")
        lAssetPrepContainerH = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachTop, "")
        lAssetPrepContainer.AddRegion("lAssetPrepContainer", "lAssetPrepContainer", lAssetPrepContainerX, lAssetPrepContainerY, lAssetPrepContainerW, lAssetPrepContainerH)
        lTabControl.Add("Asset Prep", lAssetPrepContainer)

        # content for first tab
        lAnimalPrepButton = mobu.FBButton()
        lAnimalPrepButton.Hint = "Sets up Animals to be taken into Giant, for giant setup."
        lAnimalPrepButton.Caption = "Animal Giant Prep"
        lAnimalPrepButton.Look = mobu.FBButtonLook.kFBLookPush
        lAnimalPrepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        lAnimalPrepButtonY = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachTop, "")
        lAnimalPrepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        lAnimalPrepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lAssetPrepContainer.AddRegion("lAnimalPrepButton", "lAnimalPrepButton", lAnimalPrepButtonX, lAnimalPrepButtonY, lAnimalPrepButtonW, lAnimalPrepButtonH)
        lAssetPrepContainer.SetControl("lAnimalPrepButton", lAnimalPrepButton)
        lAnimalPrepButton.OnClick.Add(PrepAssets.PrepGiantAnimals)

        lCharacterPrepButton = mobu.FBButton()
        lCharacterPrepButton.Hint = "Sets up Characters to be taken into Giant, for giant setup."
        lCharacterPrepButton.Caption = "Character Giant Prep"
        lCharacterPrepButton.Look = mobu.FBButtonLook.kFBLookPush
        lCharacterPrepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        lCharacterPrepButtonY = mobu.FBAddRegionParam(40, mobu.FBAttachType.kFBAttachTop, "lAnimalPrepButton")
        lCharacterPrepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        lCharacterPrepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lAssetPrepContainer.AddRegion("lCharacterPrepButton", "lCharacterPrepButton", lCharacterPrepButtonX, lCharacterPrepButtonY, lCharacterPrepButtonW, lCharacterPrepButtonH)
        lAssetPrepContainer.SetControl("lCharacterPrepButton", lCharacterPrepButton)
        lCharacterPrepButton.OnClick.Add(PrepAssets.PrepGiantCharacters)

        lSetPrepButton = mobu.FBButton()
        lSetPrepButton.Hint = "Sets up Environments to be taken into Giant, for giant setup."
        lSetPrepButton.Caption = "Environment Giant Prep"
        lSetPrepButton.Look = mobu.FBButtonLook.kFBLookPush
        lSetPrepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        lSetPrepButtonY = mobu.FBAddRegionParam(40, mobu.FBAttachType.kFBAttachTop, "lCharacterPrepButton")
        lSetPrepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        lSetPrepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lAssetPrepContainer.AddRegion("lSetPrepButton", "lSetPrepButton", lSetPrepButtonX, lSetPrepButtonY, lSetPrepButtonW, lSetPrepButtonH)
        lAssetPrepContainer.SetControl("lSetPrepButton", lSetPrepButton)
        lSetPrepButton.OnClick.Add(PrepAssets.PrepGiantSets)

        lPropPrepButton = mobu.FBButton()
        lPropPrepButton.Hint = "Sets up Props to be taken into Giant, for giant setup."
        lPropPrepButton.Caption = "Prop Giant Prep"
        lPropPrepButton.Look = mobu.FBButtonLook.kFBLookPush
        lPropPrepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        lPropPrepButtonY = mobu.FBAddRegionParam(40, mobu.FBAttachType.kFBAttachTop, "lSetPrepButton")
        lPropPrepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        lPropPrepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lAssetPrepContainer.AddRegion("lPropPrepButton", "lPropPrepButton", lPropPrepButtonX, lPropPrepButtonY, lPropPrepButtonW, lPropPrepButtonH)
        lAssetPrepContainer.SetControl("lPropPrepButton", lPropPrepButton)
        lPropPrepButton.OnClick.Add(PrepAssets.PrepGiantProps)

        lVehiclePrepButton = mobu.FBButton()
        lVehiclePrepButton.Hint = "Sets up Vehicles to be taken into Giant, for giant setup."
        lVehiclePrepButton.Caption = "Vehicle Giant Prep"
        lVehiclePrepButton.Look = mobu.FBButtonLook.kFBLookPush
        lVehiclePrepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        lVehiclePrepButtonY = mobu.FBAddRegionParam(40, mobu.FBAttachType.kFBAttachTop, "lPropPrepButton")
        lVehiclePrepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        lVehiclePrepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lAssetPrepContainer.AddRegion("lVehiclePrepButton", "lVehiclePrepButton", lVehiclePrepButtonX, lVehiclePrepButtonY, lVehiclePrepButtonW, lVehiclePrepButtonH)
        lAssetPrepContainer.SetControl("lVehiclePrepButton", lVehiclePrepButton)
        lVehiclePrepButton.OnClick.Add(PrepAssets.PrepGiantVehicles)

        # second tab
        lLatticePrepContainer = mobu.FBLayout()
        lLatticePrepContainer.ItemIndex = 2
        lLatticePrepContainerX = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachLeft, "")
        lLatticePrepContainerY = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachBottom, "")
        lLatticePrepContainerW = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachRight, "")
        lLatticePrepContainerH = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachTop, "")
        lLatticePrepContainer.AddRegion("lAssetPrepContainer", "lAssetPrepContainer", lLatticePrepContainerX, lLatticePrepContainerY, lLatticePrepContainerW, lLatticePrepContainerH)
        lTabControl.Add("Lattice Prep", lLatticePrepContainer)

        # content for second tab
        stageLabel = mobu.FBLabel()
        stageLabel.Caption = "Add One Stage"
        stageLabel.Hint = 'You can only add ONE stage object.\nSingle click to see full object name.\nDouble click to clear.'
        stageLabelX = mobu.FBAddRegionParam(70, mobu.FBAttachType.kFBAttachLeft, "")
        stageLabelY = mobu.FBAddRegionParam(0, mobu.FBAttachType.kFBAttachTop, "")
        stageLabelW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        stageLabelH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lLatticePrepContainer.AddRegion("stageLabel", "stageLabel", stageLabelX, stageLabelY, stageLabelW, stageLabelH)
        lLatticePrepContainer.SetControl("stageLabel", stageLabel)

        stageContainer = mobu.FBVisualContainer()
        stageContainer.ItemWidth = 100
        stageContainer.Hint = 'You can only add ONE stage object.\nSingle click to see full object name.\nDouble click to clear.'
        stageContainerX = mobu.FBAddRegionParam(8, mobu.FBAttachType.kFBAttachLeft, "")
        stageContainerY = mobu.FBAddRegionParam(5, mobu.FBAttachType.kFBAttachBottom, "stageLabel")
        stageContainerW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        stageContainerH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lLatticePrepContainer.AddRegion("stageContainer", "stageContainer", stageContainerX, stageContainerY, stageContainerW, stageContainerH)
        lLatticePrepContainer.SetControl("stageContainer", stageContainer)
        stageContainer.OnDragAndDrop.Add(ContainerDragAndDropSingle)
        stageContainer.OnDblClick.Add(DblClickClear)

        groundLabel = mobu.FBLabel()
        groundLabel.Caption = "Add Ground Object(s)"
        groundLabel.Hint = 'You may add more than 1 item of ground geo if you wish.\nSingle click to see full object name.\nDouble click to clear.'
        groundLabelX = mobu.FBAddRegionParam(60, mobu.FBAttachType.kFBAttachLeft, "")
        groundLabelY = mobu.FBAddRegionParam(5, mobu.FBAttachType.kFBAttachBottom, "stageContainer")
        groundLabelW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        groundLabelH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lLatticePrepContainer.AddRegion("groundLabel", "groundLabel", groundLabelX, groundLabelY, groundLabelW, groundLabelH)
        lLatticePrepContainer.SetControl("groundLabel", groundLabel)

        groundContainer = mobu.FBVisualContainer()
        groundContainer.ItemWidth = 100
        groundContainer.Hint = 'You may add more than 1 item of ground geo if you wish.\nSingle click to see full object name.\nDouble click to clear.'
        groundContainerX = mobu.FBAddRegionParam(8, mobu.FBAttachType.kFBAttachLeft, "")
        groundContainerY = mobu.FBAddRegionParam(5, mobu.FBAttachType.kFBAttachBottom, "groundLabel")
        groundContainerW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        groundContainerH = mobu.FBAddRegionParam(100, mobu.FBAttachType.kFBAttachNone, "")
        lLatticePrepContainer.AddRegion("groundContainer", "groundContainer", groundContainerX, groundContainerY, groundContainerW, groundContainerH)
        lLatticePrepContainer.SetControl("groundContainer", groundContainer)
        groundContainer.OnDragAndDrop.Add(ContainerDragAndDropMulti)
        groundContainer.OnDblClick.Add(DblClickClear)

        prepButton = mobu.FBButton()
        prepButton.Caption = "Prepare Lattice for Giant"
        prepButton.StageControl = stageContainer
        prepButton.GroundGeoControl = groundContainer
        prepButtonX = mobu.FBAddRegionParam(10, mobu.FBAttachType.kFBAttachLeft, "")
        prepButtonY = mobu.FBAddRegionParam(15, mobu.FBAttachType.kFBAttachBottom, "groundContainer")
        prepButtonW = mobu.FBAddRegionParam(200, mobu.FBAttachType.kFBAttachNone, "")
        prepButtonH = mobu.FBAddRegionParam(25, mobu.FBAttachType.kFBAttachNone, "")
        lLatticePrepContainer.AddRegion("prepButton", "prepButton", prepButtonX, prepButtonY, prepButtonW, prepButtonH)
        lLatticePrepContainer.SetControl("prepButton", prepButton)
        prepButton.OnClick.Add(CheckPrepLattice)


def Run(show=True):
    tool = GiantsTools()

    if show:
        tool.Show()

    return tool
