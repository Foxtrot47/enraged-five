"""
Watchstar Tool UI From Virtual Production
"""
from RS.Tools import UI
from RS.Tools.VirtualProduction.widgets import watchstarWidget


class WatchstarTool(UI.QtMainWindowBase):
    """
    Tool UI
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Watchstar Tool"
        super(WatchstarTool, self).__init__(title=self._toolName, dockable=True)
        self.setupUi()

    def setupUi(self):
        """
        Setup the UI Components
        """
        self._mainWidget = watchstarWidget.WatchstarWidget()
        self.setCentralWidget(self._mainWidget)


def Run(show=True):
    tool = WatchstarTool()

    if show:
        tool.show()

    return tool