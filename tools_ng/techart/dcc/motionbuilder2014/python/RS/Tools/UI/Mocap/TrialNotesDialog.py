"""
Dialog for the Mocap Trial Notes Widget
"""
import pyfbsdk as mobu

from PySide import QtCore, QtGui

from RS.Tools import UI
from RS.Core.AnimData.Widgets import onSetNotesTool


class TrialNotesDialog(UI.QtMainWindowBase):
    """
    GUI Tool for the trial Notes
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "Mocap Trial Notes"
        super(TrialNotesDialog, self).__init__(title=self._toolName, dockable=True)

        self.setupUi()

    def setupUi(self):
        """
        Set up the UI
        """
        # Layouts
        mainWidget = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()

        # Widgets
        banner = UI.BannerWidget(self._toolName)
        notesWidget = onSetNotesTool.MobuOnSetMocapNotesTool()

        layout.addWidget(banner)
        layout.addWidget(notesWidget)

        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)


def Run(show=True):
    notesDialog = TrialNotesDialog()

    if show:
        notesDialog.show()

    return notesDialog

