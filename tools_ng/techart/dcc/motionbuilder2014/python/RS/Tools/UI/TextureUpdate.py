from RS.Core.ReferenceSystem.Manager import Manager

from RS.Core.ReferenceSystem.Types.Character import Character

def Run():
        manager = Manager()
        manager.ReloadTextures(manager.GetReferenceListByType(Character), True)