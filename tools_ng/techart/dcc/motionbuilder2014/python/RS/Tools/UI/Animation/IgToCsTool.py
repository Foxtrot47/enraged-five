"""
UI Front end for adding CS/IG Keyframe animations
"""

from PySide import QtGui, QtCore

from RS.Tools import UI
from RS.Core.Animation import IgToCs
from RS.Core.Scene.models import characterModelTypes
from RS.Core.Scene.models.modelItems import characterModelItem


class IgToCsCharacterTextModelItem(characterModelItem.CharacterTextModelItem):
    """
    Extend the Character model item to allow for colours to show the state of the character
    """
    def __init__(self, characterName, characterObject, parent=None):
        super(IgToCsCharacterTextModelItem, self).__init__(characterName, characterObject, parent=parent)
        self._igToCsData = IgToCs.IgToCs(characterObject)
        characterObject.OnUnbind.Add(self._handleUnbind)

    def _handleUnbind(self, _, __):
        self._igToCsData = None
        
    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''
        model data generated for view display
        '''
        column = index.column()
        if self._igToCsData is None:
            return "No Data"
        if role == QtCore.Qt.DecorationRole:
            if self._igToCsData.isSetup():
                return QtGui.QColor.fromRgb(61,255,61)
            else:
                return QtGui.QColor.fromRgb(255,0,0)
        elif role == QtCore.Qt.CheckStateRole:
            return None
        return super(IgToCsCharacterTextModelItem, self).data(index, role=role)


class IgToCsCharacterModel(characterModelTypes.CharacterModel):
    """
    Extend the Character model item to allow for colours to show the state of the character
    """
    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        self._checkable = False
        characterDict = self._getCharacterDict()
        # iterate through list and create dataItem for treeview
        for key, value in characterDict.iteritems():
            dataItem = IgToCsCharacterTextModelItem(key,value,parent=parent)
            # append tree view with item
            parent.appendChild(dataItem)


class IgToCsDialog(UI.QtMainWindowBase):
    """
    Tool UI
    """
    def __init__(self):
        """
        Constructor
        """
        self._toolName = "IG To CS Tool"
        super(IgToCsDialog, self).__init__(title=self._toolName, dockable=True)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self._characterDict = {}
        self.setupUi()

    def setupUi(self):
        """
        Setup the UI Components
        """
        # layout
        mainLayout = QtGui.QVBoxLayout()

        # widgets
        charLabel = QtGui.QLabel("Current Character:")
        mainWidget = QtGui.QWidget()
        self._characterCombo = QtGui.QComboBox()
        self._setupButton = QtGui.QPushButton("Setup Character")
        self._unsetupButton = QtGui.QPushButton("Remove Setup on Character")
        self._selectPropetyButton = QtGui.QPushButton("Select Animation Property")
        self._toCsButton = QtGui.QPushButton("Key as CS")
        self._toIgButton = QtGui.QPushButton("Key as IG")
        self._commitButton = QtGui.QPushButton("Commit Motion")
        self._revertButton = QtGui.QPushButton("Revert Motion")
        self._statusText = QtGui.QLineEdit("")
        self._currentStates = QtGui.QTreeWidget()
        self._previousStates = QtGui.QTreeWidget()
        self._characterModel = IgToCsCharacterModel()

        # Configure Widgets
        self._characterCombo.setModel(self._characterModel)
        charLabel.setMaximumHeight(50)
        self._statusText.setReadOnly(True)
        self._statusText.setAlignment(QtCore.Qt.AlignCenter)
        self._currentStates.setHeaderLabels(["Frame", "State"])
        self._currentStates.setAlternatingRowColors(True)
        self._previousStates.setHeaderLabels(["Frame", "State"])
        self._previousStates.setAlternatingRowColors(True)

        # assign widgets to layouts
        mainLayout.addWidget(charLabel)
        mainLayout.addWidget(self._characterCombo)

        mainLayout.addWidget(self._setupButton)
        mainLayout.addWidget(self._unsetupButton)
        mainLayout.addWidget(self._selectPropetyButton)
        mainLayout.addWidget(self._toCsButton)
        mainLayout.addWidget(self._toIgButton)
        mainLayout.addWidget(self._commitButton)
        mainLayout.addWidget(self._revertButton)
        mainLayout.addWidget(QtGui.QLabel("Currently commited States:"))
        mainLayout.addWidget(self._currentStates)
        mainLayout.addWidget(QtGui.QLabel("Previous commited States:"))
        mainLayout.addWidget(self._previousStates)
        mainLayout.addWidget(self._statusText)

        # connections
        self._characterCombo.currentIndexChanged.connect(self._handleCharacterSelectIndexChange)
        self._selectPropetyButton.pressed.connect(self._handleSelectProperty)
        self._setupButton.pressed.connect(self._handleSetupCharacter)
        self._unsetupButton.pressed.connect(self._handleUnSetupCharacter)
        self._toCsButton.pressed.connect(self._handleToCs)
        self._toIgButton.pressed.connect(self._handleToIg)
        self._commitButton.pressed.connect(self._handleCommit)
        self._revertButton.pressed.connect(self._handleRevert)

        mainWidget.setLayout(mainLayout)
        self.setCentralWidget(mainWidget)

        self._handleCharacterSelectIndexChange(0)

    def focusInEvent(self, event):
        """
        Reimplemented Focus Event

        Force the UI to update its button states
        """
        self._updateChracterModel()
        self._changeButtonState()

    def _setStatusText(self, text, error=False):
        """
        Internal Method

        Set the status text in the messagebox at the bottom of the UI. Can be set to be an error
        with a RED colour background

        args:
            text (str): The text to display

        kwargs:
            error (bool): If the message is an error or not.
        """
        style = "background-color: rgb(0, 255, 0);\ncolor: rgb(0, 0, 0);"
        if error is True:
            style = "background-color: rgb(255, 0, 0);\ncolor: rgb(255, 255, 255);"

        self._statusText.setText(text)
        self._statusText.setStyleSheet(style)

    def _changeButtonState(self):
        """
        Internal Method

        Logic for enabling or disabling the buttons based on certain conditions, and displaying
        message text to guide the user
        """
        char = self._getCurrentCharacter()
        self._updateCurrentStates()
        self._updatePreviousStates()
        if char is None:
            for but in [self._setupButton, self._toCsButton, self._toIgButton,
                        self._commitButton, self._revertButton, self._selectPropetyButton]:
                but.setDisabled(True)
                self._setStatusText("Select Character!", error=True)
            return

        char.canKey()
        if not char.isSetup():
            if not char.canBeSetup():
                self._setupButton.setDisabled(True)
                self._setStatusText("Character is not a valid Rockstar Character", error=True)

            elif not char.hasScaleUdps():
                self._setupButton.setDisabled(True)
                self._setStatusText("Character is missing UDP, Please re-resource", error=True)
            else:
                self._setupButton.setDisabled(False)
                self._setStatusText("Character needs to be setup")

            self._selectPropetyButton.setDisabled(True)
            self._unsetupButton.setDisabled(True)
            self._toCsButton.setDisabled(True)
            self._toIgButton.setDisabled(True)
            self._commitButton.setDisabled(True)
            self._revertButton.setDisabled(True)

            return

        self._setupButton.setDisabled(True)
        if char.canCommit():
            if char.canKey():
                self._selectPropetyButton.setDisabled(True)
                self._toCsButton.setDisabled(True)
                self._toIgButton.setDisabled(True)
                self._commitButton.setDisabled(True)
                self._setStatusText("Character is being driven!", error=True)
            else:
                self._selectPropetyButton.setDisabled(False)
                self._toCsButton.setDisabled(False)
                self._toIgButton.setDisabled(False)
                self._commitButton.setDisabled(False)
                self._setStatusText("Add CS/IG Keys")
            self._unsetupButton.setDisabled(False)
            self._revertButton.setDisabled(True)

        else:
            self._unsetupButton.setDisabled(True)
            self._selectPropetyButton.setDisabled(True)
            self._toCsButton.setDisabled(True)
            self._toIgButton.setDisabled(True)
            self._commitButton.setDisabled(True)
            self._revertButton.setDisabled(False)
            self._setStatusText("CS/IG Applied!")

    def _getCurrentCharacter(self):
        """
        Internal Method

        Handle the revert motion button press
        """
        char = self._characterCombo.itemData(self._characterCombo.currentIndex(), QtCore.Qt.UserRole)
        if char is None:
            return None
        riggedChar = self._characterDict.get(char)
        if riggedChar is None:
            riggedChar = IgToCs.IgToCs(char)
            self._characterDict[char] = riggedChar
        return riggedChar

    def _updateChracterModel(self):
        """
        Internal Method

        Update the characters in the drop down, maintaining the same selected character, if it still
        is present
        """
        char = self._getCurrentCharacter()
        self._characterModel.reset()
        if char is None:
            return
        for idx in xrange(self._characterModel.rowCount()):
            if self._characterCombo.itemData(idx, QtCore.Qt.UserRole) == char.character:
                self._characterCombo.setCurrentIndex(idx)
                return

    def _updateCurrentStates(self):
        """
        Internal Method
        
        Update the UI with the currently commited states stored in the character scale node
        """
        self._currentStates.clear()
        
        character = self._getCurrentCharacter()
        if character is None:
            return
        
        if character.canBeSetup() is False:
            return
        
        for states in character.getScaleValues():
            frame, value = states
            newItem = QtGui.QTreeWidgetItem()
            newItem.setText(0, str(frame))
            newItem.setText(1, "CS" if value == 1.0 else "IG")
            self._currentStates.addTopLevelItem(newItem)

    def _updatePreviousStates(self):
        """
        Internal Method
        
        Update the UI with the previous states stored in the character scale node
        """
        self._previousStates.clear()
        
        character = self._getCurrentCharacter()
        if character is None:
            return
        
        if character.canBeSetup() is False:
            return
        
        for states in character.getRefScaleValues():
            frame, value = states
            newItem = QtGui.QTreeWidgetItem()
            newItem.setText(0, str(frame))
            newItem.setText(1, "CS" if value == 1.0 else "IG")
            self._previousStates.addTopLevelItem(newItem)

    def _handleCharacterSelectIndexChange(self, newIndex):
        """
        Internal Method

        Handle the character selection change on the character dropdown
        """
        self._changeButtonState()

    def _handleSetupCharacter(self):
        """
        Internal Method

        Handle the setup character button press
        """
        self._getCurrentCharacter().setup()
        self._changeButtonState()

    def _handleUnSetupCharacter(self):
        """
        Internal Method

        Handle the remove setup character button press
        """
        currentChar = self._getCurrentCharacter()
        if currentChar.canBeUnSetup():
            answer = QtGui.QMessageBox.question(
                                        self, 
                                       "CS to IG tool", 
                                       "There are currently keys on the IG/CS attribute. Are you sure you wish to remove the setup from the character", 
                                       QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, 
                                       QtGui.QMessageBox.No
                                       )
            if answer == QtGui.QMessageBox.No:
                return
        self._getCurrentCharacter().removeSetup()
        self._changeButtonState()

    def _handleSelectProperty(self):
        """
        Internal Method

        Handle the select property button press
        """
        self._getCurrentCharacter().focusOnProperty()

    def _handleToCs(self):
        """
        Internal Method

        Handle the key CS button press
        """
        self._getCurrentCharacter().setToCs()

    def _handleToIg(self):
        """
        Internal Method

        Handle the key IG button press
        """
        self._getCurrentCharacter().setToIg()

    def _handleCommit(self):
        """
        Internal Method

        Handle the commit motion button press
        """
        self._getCurrentCharacter().commitMotion()
        self._changeButtonState()

    def _handleRevert(self):
        """
        Internal Method

        Handle the revert motion button press
        """
        self._getCurrentCharacter().revertMotion()
        self._changeButtonState()


def Run(show=True):
    tool = IgToCsDialog()

    if show:
        tool.show()

    return tool
