'''

 Script Path: RS/Tools/UI/NonReferenceItemView.py

 Written And Maintained By: Kathryn Bodey

 Created: 2013

 Description: Displays the information, gathered from NonRefItemViewerDict.py,
              In a UI to allow the user to select/delete objects they no longer want.

'''

from pyfbsdk import *
from pyfbsdk_additions import *

import RS
import RS.Globals as glo
import RS.Utils.Scene

from RS.Tools.UI.NonReferenceItemView import NonRefItemViewerDict


##########
#UI Layout
##########
class NonRefItemViewTool( RS.Tools.UI.Base ):

    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Rockstar - Non-Referenced Item View', size = [ 810, 400 ] )

    def Create( self, mainLyt ):

        global lSpreadViewer
        global lNonRefItemDict

        #generate non ref item list
        x = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        mainLyt.AddRegion( 'main', 'main', x, y, w, h )

        lSpreadViewer = FBSpread()
        lSpreadViewer.Caption = "Item Name"

        #add columns
        lSpreadViewer.ColumnAdd( '' )
        lSpreadViewer.ColumnAdd( '' )
        lSpreadViewer.ColumnAdd( 'Has Anims?' )
        lSpreadViewer.ColumnAdd( 'Notes' )

        #set column widths
        lSpreadViewer.GetColumn( 0 ).Width = 50
        lSpreadViewer.GetColumn( 0 ).ReadOnly = False

        lSpreadViewer.GetColumn( 1 ).Width = 50
        lSpreadViewer.GetColumn( 1 ).ReadOnly = False

        lSpreadViewer.GetColumn( 2 ).Width = 60
        lSpreadViewer.GetColumn( 2 ).ReadOnly = True

        lSpreadViewer.GetColumn( 3 ).Width = 400
        lSpreadViewer.GetColumn( 3 ).ReadOnly = True

        mainLyt.SetControl( 'main', lSpreadViewer )

        #populate grid. Don't populate on MB startup.  The call to build class array kills MB.
        if not RS.MOBU_IS_STARTING:
            row = 0

            multiClassArray = NonRefItemViewerDict.BuildClassArray()

            for i in multiClassArray:

                if "Yes" in str( i.hasAnim ) and "FBCharacter" in str( i.note ):
                    None

                else:
                    #item name
                    lSpreadViewer.RowAdd( str( i.sceneNode.LongName ), row )

                    #create select button
                    lSelectCell = lSpreadViewer.GetSpreadCell( row, 0 )
                    lSelectCell.Style = FBCellStyle.kFBCellStyleButton
                    lSpreadViewer.SetCellValue( row, 0, 'Select' )

                    #create delete button
                    lDeleteCell = lSpreadViewer.GetSpreadCell( row, 1 )
                    lDeleteCell.Style = FBCellStyle.kFBCellStyleButton
                    lSpreadViewer.SetCellValue( row, 1, 'Delete' )

                    #has anims
                    lSpreadViewer.SetCellValue( row, 2, str( i.hasAnim ) )
                    lSelectCell = lSpreadViewer.GetSpreadCell( row, 2 )
                    lSelectCell.Style = FBCellStyle.kFBCellStyleString

                    #notes
                    lSpreadViewer.SetCellValue( row, 3, str( i.note ) )
                    lSelectCell = lSpreadViewer.GetSpreadCell( row, 3 )
                    lSelectCell.Style = FBCellStyle.kFBCellStyleString

                row += 1

        lSpreadViewer.OnCellChange.Add( self.lButtonCallback )


    #########
    #Callback
    #########
    def lButtonCallback( self, control, event ):

        global lSpreadViewer
        global lNonRefItemDict

        #'select' button triggered
        if event.Column == 0:

            #get cell value
            lCell = lSpreadViewer.GetRow( event.Row ).Caption

            #unselect scene items
            RS.Utils.Scene.DeSelectAll()

            #counter to see if the spread item was matched to dict item
            lBool = False

            #iterate
            multiClassArray = NonRefItemViewerDict.BuildClassArray()

            for i in multiClassArray:

                #match spread item to dict item
                if lCell == i.sceneNode.LongName:

                    lBool = True

                    lHierarchyList = []

                    #find all Children of key
                    RS.Utils.Scene.GetChildren( i.sceneNode, lHierarchyList, "", True )

                    #select hierarchy of item
                    for item in lHierarchyList:
                        item.Selected = True

                    break

            #spreadsheet item couldnt be matched to the sceneNode
            if lBool == False:
                FBMessageBox( 'Error', 'Something has gone wrong, This Item cannot be found in your scene.\n Please contact Tech Art', 'ok' )


        #'delete' button triggered
        if event.Column == 1:

            #double check deleting is what the user wants
            lResult = FBMessageBox( "Delete Warning", "Are you sure you want to Delete this item?", "Yes", "Cancel" )
            if lResult == 1:

                lCell = lSpreadViewer.GetRow( event.Row ).Caption

                #counter to see if the spread item was matched to dict item
                lBool = False

                #iterate
                multiClassArray = NonRefItemViewerDict.BuildClassArray()

                for i in multiClassArray:

                    #match spread item to dict item
                    if lCell == i.sceneNode.LongName:

                        lBool = True

                        lHierarchyList = []

                        #find all children of key
                        RS.Utils.Scene.GetChildren( i.sceneNode, lHierarchyList, "", True )

                        #check if sceneNode has an FBCharacter, delete this first.
                        gsSkelRoot = None
                        for item in lHierarchyList:
                            if item.Name == 'SKEL_ROOT':
                                    gsSkelRoot = item

                        if gsSkelRoot != None:
                            for character in RS.Globals.gCharacters:
                                if character.GetModel( FBBodyNodeId.kFBHipsNodeId ) == gsSkelRoot:
                                    character.FBDelete()

                        #delete items in hierarchy
                        for item in lHierarchyList:
                            item.FBDelete()

                        #user visual feedback
                        lSpreadViewer.SetCellValue( event.Row, 3, 'DELETED' )
                        lDeleteCell = lSpreadViewer.GetSpreadCell( event.Row, 3 )
                        lDeleteCell.Enabled = False
                        lDeleteCell = lSpreadViewer.GetSpreadCell( event.Row, 2 )
                        lDeleteCell.Enabled = False
                        lDeleteCell = lSpreadViewer.GetSpreadCell( event.Row, 1 )
                        lDeleteCell.Enabled = False
                        lDeleteCell = lSpreadViewer.GetSpreadCell( event.Row, 0 )
                        lDeleteCell.Enabled = False

                        break

                #spread item couldnt be matched to the dict key
                if lBool == False:
                    FBMessageBox( 'Error', 'Something has gone wrong, This Item cannot be found in your scene.\n Please contact Tech Art', 'ok' )


def Run( show = True ):
    tool = NonRefItemViewTool()

    if show:
        tool.Show()

    return tool
