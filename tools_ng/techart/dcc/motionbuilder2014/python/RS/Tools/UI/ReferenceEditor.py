"""
This tool allows Technical Artists To Set Up Scene That Will Alert Animators To Out Of Date
Resources In Their Scenes. It Will Also Sync The Latest Resounrces From Perforce And
Automatically Merge Them Into The Scene.

Authors:
    David Bailey
    Kristine Middlemiss

Image:
    $TechArt/Shelftastic/referenceEditor.png

"""
from pyfbsdk import *
from pyfbsdk_additions import *

import os
import webbrowser
import inspect

import RS.Tools.UI
import RS.Globals as glo
import RS.Config
import RS.Perforce
import RS.Utils.Creation as cre
import RS.Utils.Scene
import RS.Utils.Collections
import RS.Utils.Widgets
import RS.Utils.UserPreferences
import RS.Core.Reference.Lib as core
import RS.Core.Reference.Manager
import RS.Core.Reference.HealthCheck as hea
import RS.Core.Reference.RefNullEdit as refnull
import RS.Core.Reference.SaveSceneReferences as saveref
import RS.Core.Lights.ParseLightXml
import RS.Core.Reference.RefSourceControlUtils as sc
import RS.Core.Reference.GSPairing

try:
    import RS.Tools.UI.CapturePlayer
    useCapturePlayer = True

except:
    useCapturePlayer = False

reload (RS.Core.Reference.Manager)
reload ( RS.Core.Lights.ParseLightXml )
reload (refnull)
reload (core)
reload (hea)
reload(sc)
reload( RS.Core.Reference.GSPairing )

lProjectRoot = RS.Config.Tool.Path.Root

gAllArray = [core.gCharactersArray, core.gSetsArray, core.gPropsArray, core.gVehiclesArray, core.gRayFireArray, core.g3LateralArray, core.gAmbientFaceArray]
gAllTypeArray = ["CHARACTERS", "SETS", "PROPS", "VEHICLES", "RAYFIRE", "3LATERAL", 'AMBIENT FACE'] 

class ReferenceEditor( RS.Tools.UI.Base ):

    def __init__( self ):
        try:
            ## Tool Version Code
            lRefSysCoreVer = float(core.rs_P4_Revision( lRefSysCorePath, "haveRev" ))
            lRefSysManagerVer = float(core.rs_P4_Revision( lRefSysManagerPath, "haveRev" ))
            lRefSysUIVer = float(core.rs_P4_Revision( lRefSysUIPath, "haveRev" ))

            ##    Forumla Example:
            ##    version = ( 73.0 / 100.0 ) + ( 11.0 / 100.0 ) + ( 32.0 / 100.0 )
            ##    rs_RefSysCoreFunctions2012.py #73/74
            ##    rs_RefSysManager.py #11/11
            ##    rs_ReferencingSystemAdminUI2012.py #32/33

            # The 1. is for Reference System 1.0
            lVersionNumber = 1.0 + ( lRefSysCoreVer / 100.0 ) + ( lRefSysManagerVer / 100.0 ) + ( lRefSysUIVer / 100.0 )
            toolNameWithVersion = "Reference Editor" + " (Version 1." + str(lVersionNumber) + ")"
        except:
            toolNameWithVersion = "Reference Editor"

        # variable to use in markforadd changes - url:bugstar:1856125
        self.markedForDeleteControlList = []

        # gs pairing variables url:bugstar:1174885
        self.ActorAnimManager = RS.Core.Reference.GSPairing.ActorAnimManager()
        self.gsPairingLabel = None
        self.SpreadSheet = FBSpread()
        self.ManagedMenuSpreadCellDict = {}
        super(ReferenceEditor, self).__init__(
                                              toolNameWithVersion, 
                                              size=(650, 670), 
                                              helpUrl='https://devstar.rockstargames.com/wiki/index.php/Referencing_System'
                                              )

        # Check for multiple scene references, and potentially fix them.
        core.rs_CheckForMultipleSceneReferences()
        core.rs_CheckFaceAssets()

        # Find valid referencing system node.
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()

        if lReferenceNull:
            for iChild in lReferenceNull.Children:

                # Hayes: Added to deal with ambient face rigs, who have multiple : characters in their namespace.
                try:
                    namespace, name = str( iChild.LongName ).split( ':' )

                # Found multiple ':' characters, so deal with that case.
                except ValueError:
                    names = str( iChild.LongName ).split( ':' )
                    name = names[ -1 ]

                    namespace = names[ 0 ]
                    namespaces = names[ 1 : -1 ]

                    # Rebuild the namespace.
                    for ns in namespaces:
                        namespace += ':{0}'.format( ns )

                if 'RS_Null' not in namespace:
                    iChild.LongName = "{0}:{1}".format( namespace, iChild.Name )

        RS.Utils.Scene.EditEmbedMediaOptions()

        core.gNamespaceArray = []
        for namespace in glo.gScene.Namespaces:
            core.gNamespaceArray.append(namespace.Name)

    ## Event Handlers ##

    def Refresh( self, pControl, pEvent ):

        self.markedForDeleteControlList = []

        core.rs_CheckForMultipleSceneReferences()
        core.rs_CheckFaceAssets()

        del core.gCharactersArray[:]
        del core.gPropsArray[:]
        del core.gSetsArray[:]
        del core.gVehiclesArray[:]
        del core.gRayFireArray[:]
        del core.g3LateralArray[:]
        del core.gAmbientFaceArray[:]

        # refresh gs pairing variables url:bugstar:1174885
        self.ActorAnimManager = RS.Core.Reference.GSPairing.ActorAnimManager()
        self.gsPairingLabel = None
        self.SpreadSheet = FBSpread()
        self.ManagedMenuSpreadCellDict = {}

        self.Show( True )

    def UpdateIndividualReference( self, pControl, pEvent ):
        glo.gUlog.Flush()
        core.rs_UpdateIndividualReference(pControl.Name + ";" + str(pControl.TextureData), pEvent)

    def CreateReference( self, pControl, pEvent ):
        global lCreateAmbientFaceRigCtrl

        glo.gUlog.Flush()

        if lCreateAmbientFaceRigCtrl.State == True:
            core.rs_CreateAmbientFaceRigReference()

        else:
            createRefProfileObj = RS.Utils.Profiler.start( 'rs_CreateReference' )
            # Because multiple references are being created, we will not ask for namespace when create reference is greater then one.
            multiples =  int(pControl.Multiples.Value)
            if multiples == 1:
                core.rs_CreateReference(pControl.Name, str(pControl.TextureData), 1, profileObj = createRefProfileObj )
            else:
                core.rs_CreateReference(pControl.Name, str(pControl.TextureData), multiples, True, profileObj = createRefProfileObj )
            RS.Utils.Profiler.stop()

    def UpdateReference( self, pControl, pEvent):
        glo.gUlog.Flush()
        if pControl.Name == "ALL":
            lResult = FBMessageBox( "Update Warning", "Are you sure you want to update the entire scene?", "Yes", "Cancel" )
            if lResult == 1:
                core.rs_UpdateReference(pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "CHARACTERS":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "SETS":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "PROPS":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "VEHICLES":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "RAYFIRE":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

        elif pControl.Name == "3LATERAL":
            core.rs_UpdateReference( pControl.Name + ";" + str(pControl.TextureData), pEvent)

    def UpdateAmbientFaceReference( self, source, event ):
        glo.gUlog.Flush()

        # You do not need to strip an Ambient Facial Reference
        core.rs_UpdateAmbientFaceRigReference( source.referenceNamespace, True )

    def UpdateAllAmbientFaceReferences( self, source, event ):
        glo.gUlog.Flush()

        # You do not need to strip an Ambient Facial Reference
        mgr = RS.Core.Reference.Manager.RsReferenceManager()
        mgr.collectSceneReferences()

        for ref in mgr.ambientFace:
            core.rs_UpdateAmbientFaceRigReference( ref.namespace, True )

    def StripScene( self, source, event ):
        glo.gUlog.Flush()

        lResult = FBMessageBox( "Strip Warning", "Are you sure you want to strip the entire scene?", "Yes", "Cancel" )
        if lResult == 1:
            core.rs_StripScene( source, event )

    def DeleteAmbientFaceReference( self, source, event ):
        glo.gUlog.Flush()

        lResult = FBMessageBox( "Delete Warning", "Are you sure you want to delete this item\nand its animation?", "Yes", "Cancel" )
        if lResult == 1:
            core.rs_DeleteAmbientFaceRigReference( source.referenceNamespace )

    def DeleteReference( self, source, event ):
        glo.gUlog.Flush()

        lResult = FBMessageBox( "Delete Warning", "Are you sure you want to delete this item\nand its animation?", "Yes", "Cancel" )
        if lResult == 1:
            core.rs_DeleteReference( source, event )

    def SettingCustomTextureData( self, pControl, pEvent ):
        pControl.syncMergeControl.TextureData = pControl.State

    def CharacterhCheck_Run( self, pControl, pEvent ):
        import RS.Tools.UI.ValidateCharacterSetup
        RS.Tools.UI.ValidateCharacterSetup.Run()

    def NonRefItemCheck_Run(self, pControl, pEvent):
        import RS.Tools.UI.NonReferenceItemView
        RS.Tools.UI.NonReferenceItemView.Run()

    # mark for delete addition - url:bugstar:1856125
    def MarkedForDeleteCallback( self, control, event ):

        if control.State == True:
            self.markedForDeleteControlList.append( control )

        if control.State == False:
            self.markedForDeleteControlList.remove( control )

    # mark for delete addition - url:bugstar:1856125
    def DeleteMarkedCallback( self, control, event ):

        if len( self.markedForDeleteControlList ):

            for control in self.markedForDeleteControlList:

                try:
                    print 'Deleting: {0}'.format( control.Name )
                    print
                    core.rs_DeleteReference( control, None )
                except:
                    print 'cannot delete'

            self.Refresh( None, None )

        else:
            FBMessageBox( 'R* Error', 'Nothing is marked for Delete.', 'Ok' )

    # parse light addition - url:bugstar:1720902
    def ParseLights( self, control, event ):

        RS.Core.Lights.ParseLightXml.Lights()

        self.Refresh( None, None )

    ## Layout ##

    def Create( self, pMain ):
        refMgr = RS.Core.Reference.Manager.RsReferenceManager()
        refMgr.collectSceneReferences()

        lChildArray = []
        lImageSyncArray = []
        lPathArray = []
        lStateArray = []
        lPerforceArray = []
        lNullArray = []
        lRSNullCount = 0

        for iChild in glo.gRoot.Children:
            RS.Utils.Scene.GetChildren(iChild, lChildArray)

        for iChild in lChildArray:
            if iChild.LongName.startswith("RS_Null:"):
                lRsAssetType = iChild.PropertyList.Find("rs_Asset_Type")
                if lRsAssetType != None:

                    ref = refMgr.getReferenceByName( iChild.Name )

                    if ref:
                        if ref.validate():
                            if lRsAssetType.Data == "Characters":
                                core.gCharactersArray.append(iChild)
                            elif lRsAssetType.Data == "Props":
                                core.gPropsArray.append(iChild)
                            elif lRsAssetType.Data == "Sets":
                                core.gSetsArray.append(iChild)
                            elif lRsAssetType.Data == "Vehicles":
                                core.gVehiclesArray.append(iChild)
                            elif lRsAssetType.Data == "RayFire":
                                core.gRayFireArray.append(iChild)
                            elif lRsAssetType.Data == "3Lateral":
                                core.g3LateralArray.append(iChild)
                            elif lRsAssetType.Data == RS.Core.Reference.Manager.TYPE_FACE_AMBIENT:
                                core.gAmbientFaceArray.append( iChild )

                lRSNullCount = lRSNullCount + 1
        lVariable = 1

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Refresh UI Button
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")# Allows for the Rockstar Banner which is 30 high
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        pMain.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        pMain.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add( self.Refresh )

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Scroll Window
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lScrollX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lScrollY = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        lScrollW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lScrollH = FBAddRegionParam(-270,FBAttachType.kFBAttachBottom,"") # allows for the tab controls at the bottom
        pMain.AddRegion("lScroll","lScroll", lScrollX, lScrollY, lScrollW, lScrollH)

        #I want the scrollbox content to actually attatch to the scrollbox on the left and right
        lScrollContentX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"lScroll")
        lScrollContentW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"lScroll")
        lScroll = FBScrollBox()
        pMain.SetControl("lScroll",lScroll)

        # This is why there is so much space at the bottom of the scoll, the problem is I think once you create the scoll it is static
        # would need to look into if you can create a dynamic scroll based on what is in it...
        lScroll.SetContentSize(565, ((lRSNullCount + 1) * 80) + 410)

        #This is the layout inside of the scrollbox, then things get put in the layout.
        lLayoutContentX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"lScroll")
        lLayoutContentY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"lScroll")
        lLayoutContentW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"lScroll")
        lLayoutContentH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lScroll")

        lScroll.Content.AddRegion("lLayoutContent", "lLayoutContent", lLayoutContentX, lLayoutContentY, lLayoutContentW, lLayoutContentH)
        lLayoutContent = FBLayout()
        lScroll.Content.SetControl("lLayoutContent", lLayoutContent)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Reference Drop Downs In Scroll Box
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        for iArrays in range(len(gAllArray)):

            if len(gAllArray[iArrays]) > 0:
                # This actually does the heigth of the groups
                lRollOut = "lRollOut" + str(iArrays)
                lRollOutX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"lLayoutContent")
                if gAllTypeArray[iArrays] == "3LATERAL":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    elif len(gAllArray[iArrays - 2]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 2)))
                    elif len(gAllArray[iArrays - 3]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 3)))
                    elif len(gAllArray[iArrays - 4]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 4)))
                    elif len(gAllArray[iArrays - 5]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 5)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                elif gAllTypeArray[iArrays] == "AMBIENT FACE":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    elif len(gAllArray[iArrays - 2]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 2)))
                    elif len(gAllArray[iArrays - 3]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 3)))
                    elif len(gAllArray[iArrays - 4]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 4)))
                    elif len(gAllArray[iArrays - 5]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 5)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                elif gAllTypeArray[iArrays] == "RAYFIRE":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    elif len(gAllArray[iArrays - 2]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 2)))
                    elif len(gAllArray[iArrays - 3]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 3)))
                    elif len(gAllArray[iArrays - 4]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 4)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                elif gAllTypeArray[iArrays] == "VEHICLES":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    elif len(gAllArray[iArrays - 2]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 2)))
                    elif len(gAllArray[iArrays - 3]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 3)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                elif gAllTypeArray[iArrays] == "PROPS":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    elif len(gAllArray[iArrays - 2]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 2)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                elif gAllTypeArray[iArrays] == "SETS":
                    if len(gAllArray[iArrays - 1]) > 0:
                        lRollOutY = FBAddRegionParam(30,FBAttachType.kFBAttachBottom, ("lRollOut" + str(iArrays - 1)))
                    else:
                        lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
                else:
                    lRollOutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")

                lRollOutW = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"lLayoutContent")
                lRollOutH = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
                lLayoutContent.AddRegion( lRollOut ,lRollOut , lRollOutX, lRollOutY, lRollOutW, lRollOutH )

                lRollOutButton = FBArrowButton()
                lLayoutContent.SetControl( lRollOut , lRollOutButton )

                lLayout = FBLayout()
                lLayoutName = "lLayout"
                lLayoutNameX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"lLayoutContent")
                lLayoutNameY = FBAddRegionParam(5,FBAttachType.kFBAttachTop,"lLayoutContent")
                lLayoutNameW = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"lLayoutContent")
                lLayoutNameH = FBAddRegionParam((len(gAllArray[iArrays]) * 90) + 10,FBAttachType.kFBAttachNone,"")
                lLayout.AddRegion( lLayoutName, lLayoutName, lLayoutNameX, lLayoutNameY, lLayoutNameW, lLayoutNameH)

                # This content from the Arrow Button seems hard coded based on the width
                lRollOutButton.SetContent( gAllTypeArray[iArrays], lLayout, 605, (len(gAllArray[iArrays]) * 95) + 5 )

                lInSync = 0

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Loop Through Multidemsional Array To Populate Scroll Box
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

                for i in range(len(gAllArray[iArrays])):
                    lFoundPathInPerforce = 0
                    lReferencePath = gAllArray[iArrays][i].PropertyList.Find("Reference Path").Data
                    lP4version = core.rs_P4_Revision( lReferencePath, "headRev" )
                    lP4CurrentVersion = gAllArray[iArrays][i].PropertyList.Find("P4_Version").Data
                    lFBXSDKCharacter = gAllArray[iArrays][i].PropertyList.Find("GeoVarDeletedGroupNames")


                    #
                    lReferenceBorder = FBLayout()

                    lReferenceBorderX = FBAddRegionParam(3,FBAttachType.kFBAttachLeft,"")
                    lReferenceBorderY = FBAddRegionParam((i * 95) + 5,FBAttachType.kFBAttachTop,"")
                    lReferenceBorderW = FBAddRegionParam(-50,FBAttachType.kFBAttachRight,"")
                    #This is the actual height of inside the the expande borders.
                    lReferenceBorderH = FBAddRegionParam(80,FBAttachType.kFBAttachNone,"")

                    lLayout.AddRegion(("lReferenceBorder" + str(i)),("lReferenceBorder" + str(i)), lReferenceBorderX, lReferenceBorderY, lReferenceBorderW, lReferenceBorderH)
                    lLayout.SetControl(("lReferenceBorder" + str(i)),lReferenceBorder)
                    lLayout.SetBorder(("lReferenceBorder" + str(i)),FBBorderStyle.kFBEmbossSmoothBorder,False, True,2,2,90,0)

                    lSyncMergeButtonX = FBAddRegionParam(-190,FBAttachType.kFBAttachRight,"")
                    lSyncMergeButtonY = FBAddRegionParam(-25,FBAttachType.kFBAttachBottom,"")
                    lSyncMergeButtonW = FBAddRegionParam(90,FBAttachType.kFBAttachNone,"")
                    lSyncMergeButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")


                        # Check if the file is still in Perforece.
                    if RS.Perforce.DoesFileExist(sc.ToSafeP4Path(lReferencePath)):
                        lFoundPathInPerforce = 1
                    else:
                        lFoundPathInPerforce = 0

                    if not gAllArray[iArrays][i].Name.startswith("oLd*"):
                        # First Sync/Merge Button for individual reference update
                        lSyncMergeButton = FBButton()
                        lSyncMergeButton.referenceNamespace = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        lSyncMergeButtonX = FBAddRegionParam(-190,FBAttachType.kFBAttachRight,"")
                        lSyncMergeButtonY = FBAddRegionParam(-25,FBAttachType.kFBAttachBottom,"")
                        lSyncMergeButtonW = FBAddRegionParam(90,FBAttachType.kFBAttachNone,"")
                        lSyncMergeButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")

                        var0 = var1 = var2 = var3 = var4 = var5 = ""
                        var0 = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        var1 = gAllArray[iArrays][i].PropertyList.Find("rs_Asset_Type").Data
                        var2 = gAllArray[iArrays][i].PropertyList.Find("Reference Path").Data

                        lImportOption = gAllArray[iArrays][i].PropertyList.Find("Import_Option")
                        if lImportOption != None:
                            var3 = lImportOption.Data

                        lTakeName = gAllArray[iArrays][i].PropertyList.Find("Take_Name")
                        if lTakeName != None:
                            var4 = lTakeName.Data

                        lCreateMissingTakes = gAllArray[iArrays][i].PropertyList.Find("Create_Missing_Takes")
                        if lCreateMissingTakes != None:
                            var5 = lCreateMissingTakes.Data

                        lSyncMergeButton.Name = ( "{0};{1};{2};{3};{4};{5}" ).format( var0, var1, var2, var3, var4, var5 )

                        lSyncMergeButton.TextureData = 0
                        if lFoundPathInPerforce == 0:
                            lSyncMergeButton.ReadOnly = True
                        lSyncMergeButton.Caption = "Strip & Update"
                        lReferenceBorder.AddRegion("Update","Update", lSyncMergeButtonX, lSyncMergeButtonY, lSyncMergeButtonW, lSyncMergeButtonH)
                        lReferenceBorder.SetControl("Update", lSyncMergeButton)

                        if gAllTypeArray[iArrays] == "AMBIENT FACE":
                            lSyncMergeButton.OnClick.Add( self.UpdateAmbientFaceReference )

                        else:
                            lSyncMergeButton.OnClick.Add(self.UpdateIndividualReference)

                    if gAllTypeArray[iArrays] == "AMBIENT FACE":
                        lDeleteButton = FBButton()
                        lDeleteButton.referenceNamespace = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        lDeleteButtonX = FBAddRegionParam(-96,FBAttachType.kFBAttachRight,"")
                        lDeleteButton.Name = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        lDeleteButton.Caption = "Delete"
                        lReferenceBorder.AddRegion("Delete","Delete", lDeleteButtonX, lSyncMergeButtonY, lSyncMergeButtonW, lSyncMergeButtonH)
                        lReferenceBorder.SetControl( "Delete", lDeleteButton )

                        lDeleteButton.OnClick.Add( self.DeleteAmbientFaceReference )

                    else:
                        # mark for delete addition - url:bugstar:1856125
                        lDeleteButton = FBButton()
                        lDeleteButton.Style = FBButtonStyle.kFBCheckbox
                        lDeleteButton.State = False
                        lDeleteButton.Caption = 'Mark for Delete'
                        lDeleteButton.Hint = "This will mark the asset for delete.\n To delete items press the 'Delete Marked Asset(s)' button"
                        lDeleteButton.referenceNamespace = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        lDeleteButtonX = FBAddRegionParam(-98,FBAttachType.kFBAttachRight,"")
                        lDeleteButtonW = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
                        lDeleteButton.Name = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                        lReferenceBorder.AddRegion("Delete","Delete", lDeleteButtonX, lSyncMergeButtonY, lDeleteButtonW, lSyncMergeButtonH)
                        lReferenceBorder.SetControl( "Delete", lDeleteButton )

                        lDeleteButton.OnClick.Add( self.MarkedForDeleteCallback )

                    lReferenceVersionX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
                    lReferenceVersionW = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
                    lReferenceBorder.AddRegion("lReferenceVersionImage","lReferenceVersionImage", lReferenceVersionX, lSyncMergeButtonY, lReferenceVersionW, lSyncMergeButtonH)
                    lReferenceVersionImage = FBImageContainer()

                    lP4CurrentVersion = lP4CurrentVersion.rstrip('\r\n')
                    if gAllArray[iArrays][i].Name.startswith("oLd*"):
                        lReferenceVersionImage.Filename = "{0}\\InSync.jpg".format( RS.Config.Script.Path.ToolImages )
                    else:
                        # Is it found in perforce?
                        if lFoundPathInPerforce:
                            if lP4CurrentVersion == None:
                                lReferenceVersionImage.Filename = "{0}\\NoSync.jpg".format( RS.Config.Script.Path.ToolImages )
                                if lInSync < 2:
                                    lInSync = 2
                            elif lP4CurrentVersion != lP4version:
                                lReferenceVersionImage.Filename = "{0}\\OutSync.jpg".format( RS.Config.Script.Path.ToolImages )
                                if lInSync < 1:
                                    lInSync = 1
                            elif lP4CurrentVersion == lP4version:
                                lReferenceVersionImage.Filename = "{0}\\InSync.jpg".format( RS.Config.Script.Path.ToolImages )
                        else:
                            lReferenceVersionImage.Filename = "{0}\\NoSync.jpg".format( RS.Config.Script.Path.ToolImages )
                            if lInSync < 2:
                                lInSync = 2

                    lRevisionLabel = FBLabel()


                    if lFoundPathInPerforce:
                        if sc.IsMarkedForDelete(lReferencePath):
                            lRevisionLabel.Caption = str(lP4CurrentVersion) + " / " + str(lP4version) + " - Marked For Delete"
                        else:
                            lRevisionLabel.Caption = str(lP4CurrentVersion) + " / " + str(lP4version)
                    else:
                        lRevisionLabel.Style = FBTextStyle.kFBTextStyleBold
                        lRevisionLabel.Caption = "Deleted in Perforce"

                    lRevisionLabelX = FBAddRegionParam(30,FBAttachType.kFBAttachLeft,"")
                    lRevisionLabelW = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
                    lReferenceBorder.AddRegion("lRevisionLabel","lRevisionLabel", lRevisionLabelX, lSyncMergeButtonY, lRevisionLabelW, lSyncMergeButtonH)
                    lReferenceBorder.SetControl("lRevisionLabel",lRevisionLabel)

                    lReferenceBorder.SetControl("lReferenceVersionImage",lReferenceVersionImage)

                    lNamespaceLabel = FBLabel()
                    lNamespaceLabel.Caption = "Namespace: "
                    lNamespaceLabelX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
                    lNamespaceLabelY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
                    lNamespaceLabelW = FBAddRegionParam(65,FBAttachType.kFBAttachNone,"")
                    lReferenceBorder.AddRegion("lNamespaceLabel","lNamespaceLabel", lNamespaceLabelX, lNamespaceLabelY, lNamespaceLabelW, lSyncMergeButtonH)
                    lReferenceBorder.SetControl("lNamespaceLabel",lNamespaceLabel)

                    lNamespaceDataLabel = FBLabel()
                    lNamespaceDataLabel.Caption = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                    lNamespaceDataLabelX = FBAddRegionParam(70,FBAttachType.kFBAttachLeft,"")
                    lNamespaceDataLabelY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
                    lNamespaceDataLabelW = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
                    lReferenceBorder.AddRegion("lNamespaceDataLabel","lNamespaceDataLabel", lNamespaceDataLabelX, lNamespaceDataLabelY, lNamespaceDataLabelW, lSyncMergeButtonH)
                    lReferenceBorder.SetControl("lNamespaceDataLabel",lNamespaceDataLabel)

                    if lFBXSDKCharacter:
                        lGoVarReducedLabel = FBLabel()
                        lGoVarReducedLabel.Style = FBTextStyle.kFBTextStyleBold
                        lGoVarReducedLabel.Caption = "*GeoVar Reduced*"
                        lGoVarReducedLabelX = FBAddRegionParam(-110,FBAttachType.kFBAttachRight,"")
                        lGoVarReducedLabelY = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
                        lGoVarReducedLabelW = FBAddRegionParam(110,FBAttachType.kFBAttachNone,"")
                        lReferenceBorder.AddRegion("lGoVarReducedLabel","lGoVarReducedLabel", lGoVarReducedLabelX, lGoVarReducedLabelY, lGoVarReducedLabelW, lSyncMergeButtonH)
                        lReferenceBorder.SetControl("lGoVarReducedLabel",lGoVarReducedLabel)

                    lPathDataLabel = FBLabel()
                    lPathDataLabel.Caption = lReferencePath
                    lPathDataLabelX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
                    lPathDataLabelY = FBAddRegionParam(25,FBAttachType.kFBAttachTop,"")
                    lPathDataLabelW = FBAddRegionParam(600,FBAttachType.kFBAttachNone,"")
                    lReferenceBorder.AddRegion("lPathDataLabel","lPathDataLabel", lPathDataLabelX, lPathDataLabelY, lPathDataLabelW, lSyncMergeButtonH)
                    lReferenceBorder.SetControl("lPathDataLabel",lPathDataLabel)

                    global lTexturesCheckButton

                    if not gAllArray[iArrays][i].Name.startswith("oLd*"):
                        if gAllTypeArray[iArrays] == "3LATERAL" or gAllTypeArray[iArrays] == "AMBIENT FACE":
                            pass

                        else:
                            lTexturesCheckButton = FBButton()
                            lTexturesCheckButton.Style = FBButtonStyle.kFBCheckbox
                            lTexturesCheckButton.State = False
                            lTexturesCheckButton.Caption = 'Sync Textures'
                            lTexturesCheckButton.Hint = 'This will automatically sync the textures for this item'
                            lTexturesCheckButton.Name = (gAllArray[iArrays][i].PropertyList.Find("Namespace").Data + ";" + gAllArray[iArrays][i].PropertyList.Find("rs_Asset_Type").Data + ";" + gAllArray[iArrays][i].PropertyList.Find("Reference Path").Data)
                            lTexturesCheckButton.syncMergeControl = lSyncMergeButton
                            lTexturesCheckButtonX = FBAddRegionParam(-385,FBAttachType.kFBAttachRight,"")
                            lTexturesCheckButtonY = FBAddRegionParam(-25,FBAttachType.kFBAttachBottom, "")
                            lTexturesCheckButtonW = FBAddRegionParam(95,FBAttachType.kFBAttachNone,"")
                            lReferenceBorder.AddRegion("lTexturesCheckButton", "lTexturesCheckButton", lTexturesCheckButtonX, lTexturesCheckButtonY, lTexturesCheckButtonW, lSyncMergeButtonH)
                            lReferenceBorder.SetControl("lTexturesCheckButton", lTexturesCheckButton)
                            lTexturesCheckButton.OnClick.Add(self.SettingCustomTextureData)

                            lStripButton = FBButton()
                            lStripButtonX = FBAddRegionParam(-284,FBAttachType.kFBAttachRight,"")
                            if lFoundPathInPerforce == 0:
                                lStripButton.ReadOnly = True
                            lStripButton.Name = gAllArray[iArrays][i].PropertyList.Find("Namespace").Data
                            lStripButton.Caption = "Strip Only"
                            lReferenceBorder.AddRegion("Strip","Strip", lStripButtonX, lSyncMergeButtonY, lSyncMergeButtonW, lSyncMergeButtonH)
                            lReferenceBorder.SetControl( "Strip", lStripButton )
                            lStripButton.OnClick.Add(core.rs_StripScene)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Grouped Sync / Merge And Strip Buttons
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

                # Second Sync/Merge Button for assest group reference update
                lUpdateGroup = FBButton()
                lUpdateGroup.Caption = "Update Group"
                lUpdateGroup.Look = FBButtonLook.kFBLookColorChange
                lUpdateGroup.SetStateColor(FBButtonState.kFBButtonState0,FBColor(190, 190, 190))
                lUpdateGroup.SetStateColor(FBButtonState.kFBButtonState1,FBColor(190, 190, 190))
                lUpdateGroupX = FBAddRegionParam(474,FBAttachType.kFBAttachLeft,"")
                lUpdateGroupY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, lRollOut)
                lUpdateGroupW = FBAddRegionParam(90,FBAttachType.kFBAttachNone,"")
                lUpdateGroupH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
                lLayoutContent.AddRegion(("lUpdateGroup" + str(iArrays)), ("lUpdateGroup" + str(iArrays)), lUpdateGroupX, lUpdateGroupY, lUpdateGroupW, lUpdateGroupH)
                lLayoutContent.SetControl(("lUpdateGroup" + str(iArrays)),lUpdateGroup)
                lUpdateGroup.Name = gAllTypeArray[iArrays]
                lUpdateGroup.TextureData = 0

                if gAllTypeArray[iArrays] == "AMBIENT FACE":
                    lUpdateGroup.OnClick.Add( self.UpdateAllAmbientFaceReferences )

                else:
                    lUpdateGroup.OnClick.Add( self.UpdateReference )

                global lTexturesCheckButtonGroup

                if gAllTypeArray[iArrays] == "3LATERAL" or gAllTypeArray[iArrays] == "AMBIENT FACE":
                    pass

                else:
                    lTexturesCheckButtonGroup = FBButton()
                    lTexturesCheckButtonGroup.Name = "lTexturesCheckButtonGroup" + str(iArrays)
                    lTexturesCheckButtonGroup.Style = FBButtonStyle.kFBCheckbox
                    lTexturesCheckButtonGroup.syncMergeControl = lUpdateGroup
                    lTexturesCheckButtonGroup.State = False
                    lTexturesCheckButtonGroup.Caption = 'Sync Textures'
                    lTexturesCheckButtonGroup.Hint = 'This will automatically sync the textures for this item'
                    lTexturesCheckButtonGroupX = FBAddRegionParam(280,FBAttachType.kFBAttachLeft,"")
                    lTexturesCheckButtonGroupY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, lRollOut)
                    lTexturesCheckButtonGroupW = FBAddRegionParam(95,FBAttachType.kFBAttachNone,"")
                    lLayoutContent.AddRegion(("lTexturesCheckButtonGroup" + str(iArrays)),("lTexturesCheckButtonGroup" + str(iArrays)), lTexturesCheckButtonGroupX, lTexturesCheckButtonGroupY, lTexturesCheckButtonGroupW, lUpdateGroupH)
                    lLayoutContent.SetControl(("lTexturesCheckButtonGroup" + str(iArrays)), lTexturesCheckButtonGroup)
                    lTexturesCheckButtonGroup.OnClick.Add(self.SettingCustomTextureData)

                    lStripGroup = FBButton()
                    lStripGroup.Caption = "Strip Group"
                    lStripGroup.Look = FBButtonLook.kFBLookColorChange
                    lStripGroup.SetStateColor(FBButtonState.kFBButtonState0,FBColor(190, 190, 190))
                    lStripGroup.SetStateColor(FBButtonState.kFBButtonState1,FBColor(190, 190, 190))
                    lStripGroupX = FBAddRegionParam(380,FBAttachType.kFBAttachLeft,"")
                    lLayoutContent.AddRegion(("lStripGroup" + str(iArrays)),("lStripGroup" + str(iArrays)), lStripGroupX, lUpdateGroupY, lUpdateGroupW, lUpdateGroupH)
                    lLayoutContent.SetControl(("lStripGroup" + str(iArrays)),lStripGroup)
                    lStripGroup.Name = gAllTypeArray[iArrays].lower().capitalize()
                    lStripGroup.OnClick.Add(core.rs_StripScene)

                lGroupVersionImageX = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
                lGroupVersionImageW = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
                lLayoutContent.AddRegion(("lGroupVersionImage" + str(iArrays)),("lGroupVersionImage" + str(iArrays)), lGroupVersionImageX, lUpdateGroupY, lGroupVersionImageW, lSyncMergeButtonH)
                lGroupVersionImage = FBImageContainer()
                if lInSync == 0:
                    lGroupVersionImage.Filename = "{0}\\InSync.jpg".format( RS.Config.Script.Path.ToolImages )
                elif lInSync == 1:
                    lGroupVersionImage.Filename = "{0}\\OutSync.jpg".format( RS.Config.Script.Path.ToolImages )
                elif lInSync == 2:
                    lGroupVersionImage.Filename = "{0}\\NoSync.jpg".format( RS.Config.Script.Path.ToolImages )

                lLayoutContent.SetControl(("lGroupVersionImage" + str(iArrays)),lGroupVersionImage)

                lGroupTerminator = FBLabel()
                lGroupTerminator.Caption = "------------------------------------------------------------------------------------------------------------------------------------------------"
                lGroupTerminatorX = FBAddRegionParam(2,FBAttachType.kFBAttachLeft,"")
                lGroupTerminatorY = FBAddRegionParam(25,FBAttachType.kFBAttachBottom, lRollOut)
                lGroupTerminatorW = FBAddRegionParam(566,FBAttachType.kFBAttachNone,"")
                lGroupTerminatorH = FBAddRegionParam(5,FBAttachType.kFBAttachNone,"")
                lLayoutContent.AddRegion(("lGroupTerminator" + str(iArrays)),("lGroupTerminator" + str(iArrays)), lGroupTerminatorX, lGroupTerminatorY, lGroupTerminatorW, lGroupTerminatorH)
                lLayoutContent.SetControl(("lGroupTerminator" + str(iArrays)),lGroupTerminator)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Tab Layout
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lTabLayout = FBTabControl()
        lTabLayoutX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lTabLayoutY = FBAddRegionParam(-270,FBAttachType.kFBAttachBottom,"")
        lTabLayoutW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        pMain.AddRegion("lTabLayout", "lTabLayout", lTabLayoutX, lTabLayoutY, lTabLayoutW, lTabLayoutH)
        pMain.SetControl("lTabLayout", lTabLayout)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Reference Editor Tab
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lReferanceEditorTabName = "Reference Editor"

        lReferanceEditorTabLayout = FBLayout()
        lReferanceEditorTabLayoutX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lReferanceEditorTabLayoutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        lReferanceEditorTabLayoutW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lReferanceEditorTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        lReferanceEditorTabLayout.AddRegion(lReferanceEditorTabName, lReferanceEditorTabName, lReferanceEditorTabLayoutX, lReferanceEditorTabLayoutY, lReferanceEditorTabLayoutW, lReferanceEditorTabLayoutH)
        lReferanceEditorTabLayout.SetBorder(lReferanceEditorTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        # Third Sync/Merge Button for all reference updates in the scene
        lSyncMergeAllButton = FBButton()
        lSyncMergeAllButton.Caption = "----------------------------- STRIP AND UPDATE ENTIRE SCENE  -----------------------------"
        lSyncMergeAllButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lSyncMergeAllButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lSyncMergeAllButtonY = FBAddRegionParam(45,FBAttachType.kFBAttachTop,"")
        lSyncMergeAllButtonW = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        lSyncMergeAllButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lReferanceEditorTabLayout.AddRegion("lSyncMergeAllButton","lSyncMergeAllButton", lSyncMergeAllButtonX, lSyncMergeAllButtonY, lSyncMergeAllButtonW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl("lSyncMergeAllButton",lSyncMergeAllButton)
        lSyncMergeAllButton.Name = "ALL"
        lSyncMergeAllButton.TextureData = 0
        lSyncMergeAllButton.OnClick.Add( self.UpdateReference )

        global lTexturesCheckButtonAll
        lTexturesCheckButtonAll = FBButton()
        lTexturesCheckButtonAll.Style = FBButtonStyle.kFBCheckbox
        lTexturesCheckButtonAll.State = False
        lTexturesCheckButtonAll.Caption = 'Sync Textures (excluding 3Lateral)'
        lTexturesCheckButtonAll.Hint = 'This will automatically sync the textures for this item'
        lTexturesCheckButtonAll.syncMergeControl = lSyncMergeAllButton
        lTexturesCheckButtonAllX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lTexturesCheckButtonAllY = FBAddRegionParam(160,FBAttachType.kFBAttachTop,"")
        lTexturesCheckButtonAllW = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lReferanceEditorTabLayout.AddRegion(("lTexturesCheckButtonAll" + str(iArrays)),("lTexturesCheckButtonAll" + str(iArrays)), lTexturesCheckButtonAllX, lTexturesCheckButtonAllY, lTexturesCheckButtonAllW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl(("lTexturesCheckButtonAll" + str(iArrays)), lTexturesCheckButtonAll)
        lTexturesCheckButtonAll.OnClick.Add( self.SettingCustomTextureData )

        lStripAllButton = FBButton()
        lStripAllButton.Caption = "--------------------------------  STRIP ONLY ENTIRE SCENE  --------------------------------"
        lStripAllButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lStripAllButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        lReferanceEditorTabLayout.AddRegion("lStripAllButton","lStripAllButton", lSyncMergeAllButtonX, lStripAllButtonY, lSyncMergeAllButtonW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl("lStripAllButton",lStripAllButton)
        lStripAllButton.Name = "REFERENCED"
        lStripAllButton.OnClick.Add( self.StripScene )

        # mark for delete addition - url:bugstar:1856125
        lDeleteMarkedButton = FBButton()
        lDeleteMarkedButton.Caption = "--------------------------------  DELETE MARKED ASSET(S)  --------------------------------"
        lDeleteMarkedButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lDeleteMarkedButtonY = FBAddRegionParam(6,FBAttachType.kFBAttachBottom,"lSyncMergeAllButton")
        lReferanceEditorTabLayout.AddRegion("lDeleteMarkedButton","lDeleteMarkedButton", lSyncMergeAllButtonX, lDeleteMarkedButtonY, lSyncMergeAllButtonW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl("lDeleteMarkedButton",lDeleteMarkedButton)
        lDeleteMarkedButton.Name = "DELETE"
        lDeleteMarkedButton.OnClick.Add( self.DeleteMarkedCallback )

        # save references - url:bugstar:1860982
        lSaveRefsButton = FBButton()
        lSaveRefsButton.Caption = "--------------------------------  SAVE SCENE REFERENCES  --------------------------------"
        lSaveRefsButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lSaveRefsButtonY = FBAddRegionParam(6,FBAttachType.kFBAttachBottom,"lDeleteMarkedButton")
        lReferanceEditorTabLayout.AddRegion("lSaveRefsButton","lSaveRefsButton", lSyncMergeAllButtonX, lSaveRefsButtonY, lSyncMergeAllButtonW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl("lSaveRefsButton",lSaveRefsButton)
        lSaveRefsButton.Name = "SAVE"
        lSaveRefsButton.OnClick.Add( saveref.SaveSceneReferences )

        lCaptureButton = FBButton()
        lCaptureButton.Caption = "--------------------------------  VIEW CAPTURE RENDER  --------------------------------"
        lCaptureButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lCaptureButtonY = FBAddRegionParam(6,FBAttachType.kFBAttachBottom,"lSaveRefsButton")
        lReferanceEditorTabLayout.AddRegion("lCaptureButton","lCaptureButton", lSyncMergeAllButtonX, lCaptureButtonY, lSyncMergeAllButtonW, lSyncMergeAllButtonH)
        lReferanceEditorTabLayout.SetControl("lCaptureButton",lCaptureButton)
        lCaptureButton.Name = "VIDEO"
        if useCapturePlayer:
            lCaptureButton.OnClick.Add( RS.Tools.UI.CapturePlayer.Run )
        else:
            lCaptureButton.Enabled = False
        # label for warning if gs pairings are incomplete.  Caption is added in the gs pairing section. url:bugstar:1174885
        self.gsPairingLabel = FBLabel()
        X = FBAddRegionParam( 15,FBAttachType.kFBAttachLeft,"" )
        Y = FBAddRegionParam( 190,FBAttachType.kFBAttachTop,"" )
        W = FBAddRegionParam( 500,FBAttachType.kFBAttachNone,"" )
        H = FBAddRegionParam( 15,FBAttachType.kFBAttachNone,"" )
        lReferanceEditorTabLayout.AddRegion( "self.gsPairingLabel", "self.gsPairingLabel", X, Y, W, H )
        lReferanceEditorTabLayout.SetControl( "self.gsPairingLabel", self.gsPairingLabel )



        lTabLayout.Add(lReferanceEditorTabName,lReferanceEditorTabLayout)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Create Reference Tab
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lCreateReferenceTabName = "Create Reference"

        lCreateReferenceTabLayout = FBLayout()
        lCreateReferenceTabLayoutX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lCreateReferenceTabLayoutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        lCreateReferenceTabLayoutW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lCreateReferenceTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        lCreateReferenceTabLayout.AddRegion(lCreateReferenceTabName,lCreateReferenceTabName, lCreateReferenceTabLayoutX, lCreateReferenceTabLayoutY, lCreateReferenceTabLayoutW, lCreateReferenceTabLayoutH)
        lCreateReferenceTabLayout.SetBorder(lCreateReferenceTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        # Multiples Option
        lMultiplesLabel = FBLabel()
        lMultiplesLabel.Caption = "Number of References to Create:"
        lMultiplesLabelX = FBAddRegionParam(20,FBAttachType.kFBAttachLeft,"")
        lMultiplesLabelY = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        lMultiplesLabelW = FBAddRegionParam(175,FBAttachType.kFBAttachNone,"")
        lMultiplesLabelH = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        lCreateReferenceTabLayout.AddRegion("lMultiplesLabel","lMultiplesLabel", lMultiplesLabelX,lMultiplesLabelY,lMultiplesLabelW,lMultiplesLabelH)
        lCreateReferenceTabLayout.SetControl("lMultiplesLabel",lMultiplesLabel)

        lEditNum = FBEditNumber()
        lEditNum.Max = 50
        lEditNum.Min = 1
        lEditNum.Value = 1
        lEditNum.Precision = 0 # so the number is an integer and not a float.
        lEditNumX = FBAddRegionParam(190,FBAttachType.kFBAttachLeft,"")
        lEditNumY = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        lEditNumW = FBAddRegionParam(50,FBAttachType.kFBAttachNone,"")
        lEditNumH = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        lCreateReferenceTabLayout.AddRegion("lEditNum","lEditNum", lEditNumX,lEditNumY,lEditNumW,lEditNumH)
        lCreateReferenceTabLayout.SetControl("lEditNum",lEditNum)


        lCreateReferenceButton = FBButton()
        lCreateReferenceButton.Caption = "-------------------------- CREATE REFERENCE(S) ---------------------------"
        lCreateReferenceButton.Name = "REF"
        lCreateReferenceButton.TextureData = 0
        lCreateReferenceButton.Multiples = lEditNum
        lCreateReferenceButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lCreateReferenceButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lCreateReferenceButtonY = FBAddRegionParam(40,FBAttachType.kFBAttachTop,"")
        lCreateReferenceButtonW = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        lCreateReferenceButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lCreateReferenceTabLayout.AddRegion("lCreateReferenceButton","lCreateReferenceButton", lCreateReferenceButtonX, lCreateReferenceButtonY, lCreateReferenceButtonW, lCreateReferenceButtonH)
        lCreateReferenceTabLayout.SetControl("lCreateReferenceButton",lCreateReferenceButton)
        lCreateReferenceButton.OnClick.Add( self.CreateReference )


        lTexturesCheckButton = FBButton()
        lTexturesCheckButton.Style = FBButtonStyle.kFBCheckbox
        lTexturesCheckButton.State = False
        lTexturesCheckButton.Caption = 'Sync Textures (excluding 3Lateral)'
        lTexturesCheckButton.Hint = 'This will automatically sync the textures for this item'
        lTexturesCheckButton.syncMergeControl = lCreateReferenceButton
        lTexturesCheckButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lTexturesCheckButtonY = FBAddRegionParam(70,FBAttachType.kFBAttachTop,"")
        lTexturesCheckButtonW = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lUpdateGroupH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateReferenceTabLayout.AddRegion(("lTexturesCheckButton" + str(iArrays)),("lTexturesCheckButton" + str(iArrays)), lTexturesCheckButtonX, lTexturesCheckButtonY, lTexturesCheckButtonW, lUpdateGroupH)
        lCreateReferenceTabLayout.SetControl(("lTexturesCheckButton" + str(iArrays)), lTexturesCheckButton)
        lTexturesCheckButton.OnClick.Add( self.SettingCustomTextureData )

        global lCreateAmbientFaceRigCtrl
        lCreateAmbientFaceRigCtrl = FBButton()
        lCreateAmbientFaceRigCtrl.Style = FBButtonStyle.kFBCheckbox
        lCreateAmbientFaceRigCtrl.State = False
        lCreateAmbientFaceRigCtrl.Caption = 'Create Ambient Face Rig'
        lCreateAmbientFaceRigCtrl.Hint = ''
        lCreateAmbientFaceRigCtrlX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lCreateAmbientFaceRigCtrlY = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        lCreateAmbientFaceRigCtrlW = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lCreateReferenceTabLayout.AddRegion(("lCreateAmbientFaceRigCtrl" + str(iArrays)),("lCreateAmbientFaceRigCtrl" + str(iArrays)), lCreateAmbientFaceRigCtrlX, lCreateAmbientFaceRigCtrlY, lCreateAmbientFaceRigCtrlW, lUpdateGroupH)
        lCreateReferenceTabLayout.SetControl(("lCreateAmbientFaceRigCtrl" + str(iArrays)), lCreateAmbientFaceRigCtrl)

        lTabLayout.Add(lCreateReferenceTabName,lCreateReferenceTabLayout)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Edit Namespace Tab
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        lEditNamespaceTabName = "Edit Namespace"
        lEditNamespaceTabLayout = FBLayout()
        lEditNamespaceTabLayoutX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lEditNamespaceTabLayoutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        lEditNamespaceTabLayoutW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lEditNamespaceTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        lEditNamespaceTabLayout.AddRegion(lEditNamespaceTabName,lEditNamespaceTabName, lEditNamespaceTabLayoutX, lEditNamespaceTabLayoutY, lEditNamespaceTabLayoutW, lEditNamespaceTabLayoutH)
        lEditNamespaceTabLayout.SetBorder(lEditNamespaceTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        # Warning Message About Skeletons
        lWarning = FBLabel()
        lWarning.Justify = FBTextJustify.kFBTextJustifyCenter
        lWarning.Caption = "WARNING: Only change the Path for identical skeletons, if they are not the Ref Editor will be mad at you ;)"
        lWarnMsgX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lWarnMsgY = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        lWarnMsgW = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        lWarnMsgH = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("lWarnMsg","lWarnMsg", lWarnMsgX,lWarnMsgY,lWarnMsgW,lWarnMsgH)
        lEditNamespaceTabLayout.SetControl("lWarnMsg",lWarning)

        lab= FBLabel()
        lab.Caption = "Select Namespace: "
        x = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(35,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(125,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("Label","Label", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("Label",lab)

        lyt = FBVBoxLayout()
        x = FBAddRegionParam(140,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(30,FBAttachType.kFBAttachNone,None)
        lEditNamespaceTabLayout.AddRegion("main","main", x,y,w,h)
        lEditNamespaceTabLayout.SetControl("main",lyt)

        l = FBLabel()
        l.Caption = "Update Namespace: "
        x = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(120,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("Label1","Label1", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("Label1",l)

        editNamespace = FBEdit()
        editNamespace.Visible = True
        editNamespace.ReadOnly = False
        editNamespace.Enabled = True
        editNamespace.PasswordMode = False
        x = FBAddRegionParam(140,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(65,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("editNamespace","editNamespace", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("editNamespace", editNamespace)

        l = FBLabel()
        l.Caption = "Update Path: "
        x = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(85,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("Label2","Label2", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("Label2",l)

        editPath = FBEdit()
        editPath.Visible = True
        editPath.ReadOnly = False
        editPath.Enabled = True
        editPath.PasswordMode = False
        x = FBAddRegionParam(140,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(95,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(-60,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("editPath","editPath", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("editPath", editPath)

        editPathButton = FBButton()
        editPathButton.Caption = "..."
        editPathButton.Justify = FBTextJustify.kFBTextJustifyCenter
        editPathButton.editNamespace = editNamespace
        editPathButton.editPath = editPath
        x = FBAddRegionParam(-55,FBAttachType.kFBAttachRight,"")
        y = FBAddRegionParam(95,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(40,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("editPathButton","editPathButton", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("editPathButton", editPathButton)
        editPathButton.OnClick.Add(refnull.FillPath)

        controls = FBList()
        controls.editNamespace = editNamespace
        controls.editPath = editPath
        controls.OnChange.Add(refnull.ListCallback)
        controls.Style = FBListStyle.kFBDropDownList
        controls.Selected(0, False)
        lReferenceNull = RS.Core.Reference.Manager.GetReferenceSceneNull()

        if lReferenceNull:
            for iChild in lReferenceNull.Children:
                lName = iChild.Name
                controls.Items.append(lName)

        if len(controls.Items) == 1:
            controls.Selected(0, True)
            refnull.ListCallback(controls, event=None) #Need to fill everything up when there is only one item.
        else:
            controls.Selected(-1, True)

        lyt.Add(controls, 25)

        if len(controls.Items) == 1:
            RSNull = FBFindModelByLabelName(controls.Items[0])
            if  RSNull:
                lPropNamespace = RSNull.PropertyList.Find("Namespace")
                lPropPath = RSNull.PropertyList.Find("Reference Path")
                if lPropNamespace:
                    Namespace = RSNull.PropertyList.Find("Namespace").Data
                    editNamespace.Text = Namespace
                if lPropPath:
                    Path = RSNull.PropertyList.Find("Reference Path").Data
                    editPath.Text = Path

        # Needs to be a the bottom because it uses the varialbes editNamespace, editPath and RSNull
        commitButton = FBButton()
        commitButton.Caption = "Update Namespace and/or Path"
        commitButton.Justify = FBTextJustify.kFBTextJustifyCenter
        commitButton.editNamespace = editNamespace
        commitButton.editPath = editPath
        commitButton.controls = controls
        x = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        y = FBAddRegionParam(125,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lEditNamespaceTabLayout.AddRegion("commitButton","commitButton", x, y, w, h)
        lEditNamespaceTabLayout.SetControl("commitButton", commitButton)
        commitButton.OnClick.Add(refnull.CommitChange)

        lTabLayout.Add(lEditNamespaceTabName,lEditNamespaceTabLayout)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Health Check Tab
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        def rs_HealthCheck_CheckBox_Run(pControl, pEvent):

            if lSetsCheckButton.State == True:
                hea.rs_RotationScale_Check()
                lSetsCheckDone = '\n- Environment Rotation/Scaling: This has been set to Default'
            else:
                lSetsCheckDone = '\n- Environment Rotation/Scaling: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lFPSCheckButton.State == True:
                hea.rs_TimeMode_Check()
                lFPSCheckDone = '\n\n- Time Mode (FPS): This has been set to Default'
            else:
                lFPSCheckDone = '\n\n- Time Mode (FPS): *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteAnimLayerButton.State == True:
                hea.rs_DeleteAnimLayer_Check()
                lDeleteAnimLayerDone = '\n\n- Delete Empty Animation Layer: All empty animation layers have been deleted'
            else:
                lDeleteAnimLayerDone = '\n\n- Delete Empty Animation Layer: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDisableRotDOFButton.State == True:
                hea.rs_DisableRotDOF_Check()
                lDisableRotDOFDone = "\n\n- Disable Rotation DOF: All rotation DOF's in props and weapons have been disabled"
            else:
                lDisableRotDOFDone = '\n\n- Disable Rotation DOF: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteHIKSolverButton.State == True:
                hea.rs_DeleteHIKSolver_Check()
                lDeleteHIKSolverDone = "\n\n- Delete HIK Solver: All HIK 4.5 Solvers have been deleted"
            else:
                lDeleteHIKSolverDone = '\n\n- Delete HIK Solver: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteFacialBonesButton.State == True:
                hea.rs_DeleteFacialBone_Check()
                lDeleteFacialBonesDone = "\n\n- Delete Facial Bones: Deletes facial bones named FaceRoot_Head_000_R abd FaceRoot_Head_001_R, and the children"
            else:
                lDeleteFacialBonesDone = '\n\n- Delete Facial Bones: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteUnUsedCtrlButton.State == True:
                hea.rs_DeleteUnusedControlRigs_Check()
                lUnUsedCtrlCheckDone = '\n\n- Unused Control Rigs: Unused Control Rigs have now been deleted.'
            else:
                lUnUsedCtrlCheckDone = '\n\n- Unused Control Rigs: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteDupeExprButton.State == True:
                hea.rs_FixBadCharExt()
                lDupeExprCheckDone = '\n\n- Duplicated/Unused Character Extensions: Duplicated/Unused Character Extensions have now been deleted.'
            else:
                lDupeExprCheckDone = '\n\n- Duplicated/Unused Character Extensions: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteheadCamLightButton.State == True:
                hea.rs_DeleteHeadCamLightObjects()
                lDeleteheadCamLightCheckDone = '\n\n- Delete headCam & headLight Objects: Any headCam & headLight objects in the scene have now been deleted.'
            else:
                lDeleteheadCamLightCheckDone = '\n\n- Delete headCam & headLight Objects: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lFixHighHeelButton.State == True:
                hea.rs_FixHighHeel_Check()
                lFixHighHeelDone = '\n\n- Fixed Broken/Missing High Heel: All High Heels have been fixed/added.'
            else:
                lFixHighHeelDone = '\n\n- Fixed Broken/Missing High Heel: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteNamespacesButton.State == True:
                hea.rs_DeleteUnusedNamespace_Check()
                lDeleteNamespacesDone = '\n\n- Removed Namespaces that were not assigned to any object in the scene.'
            else:
                lDeleteNamespacesDone = '\n\n- Delete Unused Namespaces: *Check box Disabled*'

            FBSystem().Scene.Evaluate()



            if lDeleteTextureVideoClipButton.State == True:
                hea.rs_DeleteTextureVideoClip()
                lDeleteTextureVideoClipDOne = '\n\n- Delete Floating Textures and VideoClips: All Floating items have been deleted.'
            else:
                lDeleteTextureVideoClipDOne = '\n\n- Delete Floating Textures and VideoClips: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            if lDeleteFoldersButton.State == True:
                hea.rs_DeleteFolders_Check()
                lDeleteFoldersDone = '\n\n- Delete Folders: All empty folders have been deleted'
            else:
                lDeleteFoldersDone = '\n\n- Delete Folders: *Check box Disabled*'

            FBSystem().Scene.Evaluate()

            FBMessageBox('Health Check Report', lSetsCheckDone + lFPSCheckDone + lDeleteFoldersDone + lDisableRotDOFDone + lDeleteHIKSolverDone + lDeleteFacialBonesDone + lUnUsedCtrlCheckDone + lDupeExprCheckDone + lDeleteheadCamLightCheckDone + lDeleteAnimLayerDone + lFixHighHeelDone + lDeleteNamespacesDone + lDeleteTextureVideoClipDOne, 'Ok')

        lCreateHealthTabName = "Health Check"
        lCreateHealthTabLayout = FBLayout()
        lCreateHealthTabLayoutX = FBAddRegionParam(3,FBAttachType.kFBAttachLeft,"")
        lCreateHealthTabLayoutY = FBAddRegionParam(3,FBAttachType.kFBAttachTop,"")
        lCreateHealthTabLayoutW = FBAddRegionParam(-3,FBAttachType.kFBAttachRight,"")
        lCreateHealthTabLayoutH = FBAddRegionParam(-3,FBAttachType.kFBAttachBottom,"")
        lCreateHealthTabLayout.AddRegion(lCreateHealthTabName,lCreateHealthTabName, lCreateHealthTabLayoutX, lCreateHealthTabLayoutY, lCreateHealthTabLayoutW, lCreateHealthTabLayoutH)
        lCreateHealthTabLayout.SetBorder(lCreateHealthTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        lHealthButton = FBButton()
        lHealthButton.Caption = "---------------------------------  HEALTH CHECK ---------------------------------"
        lHealthButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lHealthButtonX = FBAddRegionParam(6,FBAttachType.kFBAttachLeft, "")
        lHealthButtonY = FBAddRegionParam(6,FBAttachType.kFBAttachTop,"")
        lHealthButtonW = FBAddRegionParam(-6,FBAttachType.kFBAttachRight,"")
        lHealthButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lHealthButton","lHealthButton", lHealthButtonX, lHealthButtonY, lHealthButtonW, lHealthButtonH)
        lCreateHealthTabLayout.SetControl("lHealthButton",lHealthButton)
        lHealthButton.OnClick.Add(rs_HealthCheck_CheckBox_Run)

        lSetsCheckButton = FBButton()
        lSetsCheckButton.Style = FBButtonStyle.kFBCheckbox
        lSetsCheckButton.State = True
        lSetsCheckButton.Caption = 'Environment Rotation/Scaling set to Default'
        lSetsCheckButton.Hint = 'default: Rot:(-90,[any],0) Scale:(100,100,100)'
        lSetsCheckButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lSetsCheckButtonY = FBAddRegionParam(3,FBAttachType.kFBAttachBottom, "lHealthButton")
        lSetsCheckButtonW = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lSetsCheckButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lSetsCheckButton", "lSetsCheckButton", lSetsCheckButtonX, lSetsCheckButtonY, lSetsCheckButtonW, lSetsCheckButtonH)
        lCreateHealthTabLayout.SetControl("lSetsCheckButton", lSetsCheckButton)

        lFPSCheckButton = FBButton()
        lFPSCheckButton.Style = FBButtonStyle.kFBCheckbox
        lFPSCheckButton.State = True
        lFPSCheckButton.Caption = 'Time Mode (FPS) set to Default'
        lFPSCheckButton.Hint = 'default: 30FPS'
        lFPSCheckButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lFPSCheckButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, "lSetsCheckButton")
        lFPSCheckButtonW = FBAddRegionParam(300,FBAttachType.kFBAttachNone,"")
        lFPSCheckButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lFPSCheckButton", "lFPSCheckButton", lFPSCheckButtonX, lFPSCheckButtonY, lFPSCheckButtonW, lFPSCheckButtonH)
        lCreateHealthTabLayout.SetControl("lFPSCheckButton", lFPSCheckButton)

        lDeleteFoldersButton = FBButton()
        lDeleteFoldersButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteFoldersButton.State = True
        lDeleteFoldersButton.Caption = 'Delete Empty Folders in the Navigator'
        lDeleteFoldersButton.Hint = 'Tired of empty folders clogging up your work space?'
        lDeleteFoldersButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lDeleteFoldersButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, "lFPSCheckButton")
        lDeleteFoldersButtonW = FBAddRegionParam(300,FBAttachType.kFBAttachNone,"")
        lDeleteFoldersButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteFoldersButton", "lDeleteFoldersButton", lDeleteFoldersButtonX, lDeleteFoldersButtonY, lDeleteFoldersButtonW, lDeleteFoldersButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteFoldersButton", lDeleteFoldersButton)

        lDisableRotDOFButton = FBButton()
        lDisableRotDOFButton.Style = FBButtonStyle.kFBCheckbox
        lDisableRotDOFButton.State = True
        lDisableRotDOFButton.Caption = "Disable Rotation DOF's in all Props and Weapons"
        lDisableRotDOFButton.Hint = 'This disable the property Rotation on asset types of Props Skinned, Props, Props Weapon and Vehicles'
        lDisableRotDOFButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDisableRotDOFButtonY = FBAddRegionParam(3,FBAttachType.kFBAttachBottom, "lHealthButton")
        lDisableRotDOFButtonW = FBAddRegionParam(275,FBAttachType.kFBAttachNone,"")
        lDisableRotDOFButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDisableRotDOFButton", "lDisableRotDOFButton", lDisableRotDOFButtonX, lDisableRotDOFButtonY, lDisableRotDOFButtonW, lDisableRotDOFButtonH)
        lCreateHealthTabLayout.SetControl("lDisableRotDOFButton", lDisableRotDOFButton)

        lDeleteHIKSolverButton = FBButton()
        lDeleteHIKSolverButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteHIKSolverButton.State = True
        lDeleteHIKSolverButton.Caption = "Delete HIK 4.5 Solvers - Not used in GTA5"
        lDeleteHIKSolverButton.Hint = 'This Deletes all the HIK 4.5 Solvers that are in the Navigator under Solvers, they are not used in the game and were introduced by a Hot Fix'
        lDeleteHIKSolverButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDeleteHIKSolverButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, "lDisableRotDOFButton")
        lDeleteHIKSolverButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteHIKSolverButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteHIKSolverButton", "lDeleteHIKSolverButton", lDeleteHIKSolverButtonX, lDeleteHIKSolverButtonY, lDeleteHIKSolverButtonW, lDeleteHIKSolverButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteHIKSolverButton", lDeleteHIKSolverButton)

        lDeleteFacialBonesButton = FBButton()
        lDeleteFacialBonesButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteFacialBonesButton.State = False
        lDeleteFacialBonesButton.Caption = "Delete Bad Facial Bones due to Broken Face"
        lDeleteFacialBonesButton.Hint = 'This deletes the facial bones that are problematic after you strip and sync/merge, it will delete  FaceRoot_Head_000_R and FaceRoot_Head_001_R, then you can strip and sync/merge again'
        lDeleteFacialBonesButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDeleteFacialBonesButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, "lDeleteHIKSolverButton")
        lDeleteFacialBonesButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteFacialBonesButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteFacialBonesButton", "lDeleteFacialBonesButton", lDeleteFacialBonesButtonX, lDeleteFacialBonesButtonY, lDeleteFacialBonesButtonW, lDeleteFacialBonesButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteFacialBonesButton", lDeleteFacialBonesButton)

        lDeleteheadCamLightButton = FBButton()
        lDeleteheadCamLightButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteheadCamLightButton.State = True
        lDeleteheadCamLightButton.Caption = "Delete headCam & headLight Objects"
        lDeleteheadCamLightButton.Hint = "This deletes all objects in the scene called 'headCam and 'headLight'.'"
        lDeleteheadCamLightButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lDeleteheadCamLightButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lDeleteFoldersButton")
        lDeleteheadCamLightButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteheadCamLightButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteheadCamLightButton", "lDeleteheadCamLightButton", lDeleteheadCamLightButtonX, lDeleteheadCamLightButtonY, lDeleteheadCamLightButtonW, lDeleteheadCamLightButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteheadCamLightButton", lDeleteheadCamLightButton)

        lDeleteDupeExprButton = FBButton()
        lDeleteDupeExprButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteDupeExprButton.State = False
        lDeleteDupeExprButton.Caption = "Delete Duplicate & Unused Character Extensions"
        lDeleteDupeExprButton.Hint = 'This deletes any duplicated or unused character extensions. It will also fix any broken namespaces on remaining extensions.'
        lDeleteDupeExprButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDeleteDupeExprButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lDeleteFacialBonesButton")
        lDeleteDupeExprButtonW = FBAddRegionParam(275,FBAttachType.kFBAttachNone,"")
        lDeleteDupeExprButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteDupeExprButton", "lDeleteDupeExprButton", lDeleteDupeExprButtonX, lDeleteDupeExprButtonY, lDeleteDupeExprButtonW, lDeleteDupeExprButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteDupeExprButton", lDeleteDupeExprButton)

        lDeleteUnUsedCtrlButton = FBButton()
        lDeleteUnUsedCtrlButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteUnUsedCtrlButton.State = False
        lDeleteUnUsedCtrlButton.Caption = "Delete Unused Control Rigs"
        lDeleteUnUsedCtrlButton.Hint = 'This deletes any control rigs not hooked up to a character.'
        lDeleteUnUsedCtrlButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lDeleteUnUsedCtrlButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lDeleteheadCamLightButton")
        lDeleteUnUsedCtrlButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteUnUsedCtrlButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteUnUsedCtrlButton", "lDeleteUnUsedCtrlButton", lDeleteUnUsedCtrlButtonX, lDeleteUnUsedCtrlButtonY, lDeleteUnUsedCtrlButtonW, lDeleteUnUsedCtrlButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteUnUsedCtrlButton", lDeleteUnUsedCtrlButton)

        lDeleteAnimLayerButton = FBButton()
        lDeleteAnimLayerButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteAnimLayerButton.State = False
        lDeleteAnimLayerButton.Caption = 'Delete Empty Animation Layers from Takes'
        lDeleteAnimLayerButton.Hint = 'Tired of empty animation layers clogging up your work space?'
        lDeleteAnimLayerButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDeleteAnimLayerButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom, "lDeleteDupeExprButton")
        lDeleteAnimLayerButtonW = FBAddRegionParam(250,FBAttachType.kFBAttachNone,"")
        lDeleteAnimLayerButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteAnimLayerButton", "lDeleteAnimLayerButton", lDeleteAnimLayerButtonX, lDeleteAnimLayerButtonY, lDeleteAnimLayerButtonW, lDeleteAnimLayerButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteAnimLayerButton", lDeleteAnimLayerButton)

        lFixHighHeelButton = FBButton()
        lFixHighHeelButton.Style = FBButtonStyle.kFBCheckbox
        lFixHighHeelButton.State = True
        lFixHighHeelButton.Caption = "Fix Broken/Missing High Heels"
        lFixHighHeelButton.Hint = 'This fixes any broken or missing high heels, if they are not broken or missing then nothing happens.'
        lFixHighHeelButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lFixHighHeelButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lDeleteUnUsedCtrlButton")
        lFixHighHeelButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lFixHighHeelButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lFixHighHeelButton", "lFixHighHeelButton", lFixHighHeelButtonX, lFixHighHeelButtonY, lFixHighHeelButtonW, lFixHighHeelButtonH)
        lCreateHealthTabLayout.SetControl("lFixHighHeelButton", lFixHighHeelButton)

        lDeleteNamespacesButton = FBButton()
        lDeleteNamespacesButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteNamespacesButton.State = False
        lDeleteNamespacesButton.Caption = "Delete Unused Namespaces from the Scene"
        lDeleteNamespacesButton.Hint = "Some times MotionBuilder doesn't remove namespaces that are not associated with anything, this will remove them"
        lDeleteNamespacesButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        lDeleteNamespacesButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lFixHighHeelButton")
        lDeleteNamespacesButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteNamespacesButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteNamespacesButton", "lDeleteNamespacesButton", lDeleteNamespacesButtonX, lDeleteNamespacesButtonY, lDeleteNamespacesButtonW, lDeleteNamespacesButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteNamespacesButton", lDeleteNamespacesButton)


        lDeleteTextureVideoClipButton = FBButton()
        lDeleteTextureVideoClipButton.Style = FBButtonStyle.kFBCheckbox
        lDeleteTextureVideoClipButton.State = False
        lDeleteTextureVideoClipButton.Caption = "Delete Floating Textures and VideoClips"
        lDeleteTextureVideoClipButton.Hint = 'This deletes any textures or videoclips that are not connected to a reference.'
        lDeleteTextureVideoClipButtonX = FBAddRegionParam(275,FBAttachType.kFBAttachLeft,"")
        lDeleteTextureVideoClipButtonY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lDeleteAnimLayerButton")
        lDeleteTextureVideoClipButtonW = FBAddRegionParam(235,FBAttachType.kFBAttachNone,"")
        lDeleteTextureVideoClipButtonH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lCreateHealthTabLayout.AddRegion("lDeleteTextureVideoClipButton", "lDeleteTextureVideoClipButton", lDeleteTextureVideoClipButtonX, lDeleteTextureVideoClipButtonY, lDeleteTextureVideoClipButtonW, lDeleteTextureVideoClipButtonH)
        lCreateHealthTabLayout.SetControl("lDeleteTextureVideoClipButton", lDeleteTextureVideoClipButton)

        lTabLayout.Add(lCreateHealthTabName,lCreateHealthTabLayout)

        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        ##
        ## Description: Advanced Functions/Checks Tab
        ##
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        ###############################################################################################################

        lAdvanceTabName = "Advanced"
        lAdvanceTabLayout = FBLayout()
        lAdvanceTabLayoutX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lAdvanceTabLayoutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        lAdvanceTabLayoutW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lAdvanceTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        lAdvanceTabLayout.AddRegion(lAdvanceTabName,lAdvanceTabName, lAdvanceTabLayoutX, lAdvanceTabLayoutY, lAdvanceTabLayoutW, lAdvanceTabLayoutH)
        lAdvanceTabLayout.SetBorder(lAdvanceTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        lCharHealthButton = FBButton()
        lCharHealthButton.Caption = "------------------------  VALIDATE CHARACTER SETUP  ------------------------"
        lCharHealthButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lCharHealthButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft, "")
        lCharHealthButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        lCharHealthButtonW = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        lCharHealthButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lAdvanceTabLayout.AddRegion("lCharHealthButton","lCharHealthButton", lCharHealthButtonX, lCharHealthButtonY, lCharHealthButtonW, lCharHealthButtonH)
        lAdvanceTabLayout.SetControl("lCharHealthButton",lCharHealthButton)
        lCharHealthButton.OnClick.Add(self.CharacterhCheck_Run)

        lNonRefCheckButton = FBButton()
        lNonRefCheckButton.Caption = "----------------------  NON-REFERENCED ITEM VIEWER  ----------------------"
        lNonRefCheckButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lNonRefCheckButtonX = FBAddRegionParam(15,FBAttachType.kFBAttachLeft, "")
        lNonRefCheckButtonY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCharHealthButton")
        lNonRefCheckButtonW = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        lNonRefCheckButtonH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lAdvanceTabLayout.AddRegion("lNonRefCheckButton","lNonRefCheckButton", lNonRefCheckButtonX, lNonRefCheckButtonY, lNonRefCheckButtonW, lNonRefCheckButtonH)
        lAdvanceTabLayout.SetControl("lNonRefCheckButton",lNonRefCheckButton)
        lNonRefCheckButton.OnClick.Add(self.NonRefItemCheck_Run)

        lightCreateButton = FBButton()
        lightCreateButton.Justify = FBTextJustify.kFBTextJustifyCenter
        lightCreateButtonX = FBAddRegionParam( 15,FBAttachType.kFBAttachLeft, "" )
        lightCreateButtonY = FBAddRegionParam( 5,FBAttachType.kFBAttachBottom,"lNonRefCheckButton" )
        lightCreateButtonW = FBAddRegionParam( 450,FBAttachType.kFBAttachNone,"" )
        lightCreateButtonH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )
        lAdvanceTabLayout.AddRegion( "lightCreateButton","lightCreateButton", lightCreateButtonX, lightCreateButtonY, lightCreateButtonW, lightCreateButtonH )
        lAdvanceTabLayout.SetControl( "lightCreateButton",lightCreateButton )
        lightCreateButton.OnClick.Add( self.ParseLights )

        lightVersionImage = FBImageContainer()
        lightVersionImageX = FBAddRegionParam( 480,FBAttachType.kFBAttachLeft, "" )
        lightVersionImageY = FBAddRegionParam( 8,FBAttachType.kFBAttachBottom,"lNonRefCheckButton" )
        lightVersionImageW = FBAddRegionParam( 20,FBAttachType.kFBAttachNone,"" )
        lightVersionImageH = FBAddRegionParam( 20,FBAttachType.kFBAttachNone,"" )
        lAdvanceTabLayout.AddRegion( "lightVersionImage","lightVersionImage", lightVersionImageX, lightVersionImageY, lightVersionImageW, lightVersionImageH )
        lAdvanceTabLayout.SetControl("lightVersionImage",lightVersionImage)

        revisionLabel = FBLabel()
        perforceLightInfo = RS.Core.Lights.ParseLightXml.ReferenceSystemFeedback()
        if perforceLightInfo:
            haveRevision = perforceLightInfo[1]
            headRevision = perforceLightInfo[2]
            revisionLabel.Caption = str( haveRevision ) + " / " + str( headRevision )
            if int( haveRevision ) < int( headRevision ):
                lightCreateButton.Caption = "----------------------  UPDATE LIGHTS FROM LIGHTXML  ----------------------"
                lightCreateButton.Enabled = True
                lightVersionImage.Filename = "{0}\\OutSync.jpg".format( RS.Config.Script.Path.ToolImages )
                lightVersionImage.Hint = "lights are out of sync, hit the button to update them"
            else:
                lightCreateButton.Caption = "----------------------  LIGHTS FROM LIGHTXML  ----------------------"
                lightCreateButton.Enabled = False
                lightVersionImage.Filename = "{0}\\InSync.jpg".format( RS.Config.Script.Path.ToolImages )

        else:
            lightCreateButton.Caption = "----------------------  ADD LIGHTS FROM LIGHTXML  ----------------------"
            lightCreateButton.Enabled = True
            revisionLabel.Caption = " 0 / 0 "
            lightVersionImage.Filename = "{0}\\blankSync.jpg".format( RS.Config.Script.Path.ToolImages )
            revisionLabel.Hint = "lights have not been generated for this scene yet"
        revisionLabelX = FBAddRegionParam( 510,FBAttachType.kFBAttachLeft,"" )
        revisionLabelY = FBAddRegionParam( 5,FBAttachType.kFBAttachBottom,"lNonRefCheckButton" )
        revisionLabelW = FBAddRegionParam( 50,FBAttachType.kFBAttachNone,"" )
        revisionLabelH = FBAddRegionParam( 25,FBAttachType.kFBAttachNone,"" )
        lAdvanceTabLayout.AddRegion( "revisionLabel","revisionLabel", revisionLabelX, revisionLabelY, revisionLabelW, revisionLabelH )
        lAdvanceTabLayout.SetControl( "revisionLabel",revisionLabel )

        lTabLayout.Add(lAdvanceTabName,lAdvanceTabLayout)

    ###############################################################################################################
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ##
    ## Description: Batch Create Reference
    ##
    ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ###############################################################################################################

        def BrowseCallback(control, event):
            lFilePopup = FBFilePopup();
            lFilePopup.Filter = '*.txt'
            lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
            lFilePopup.Path = r"{0}\wildwest\script\MotionBuilderPython\ToolImages".format( RS.Config.Tool.Path.Root )

            lFileName = ""

            if lFilePopup.Execute():
                lFileName = lFilePopup.FullFilename
            else:
                return
            control.Path.Text = lFileName

        def BatchCallback(control, event):

            if control.Path.Text == "":
                FBMessageBox( "Error", "Please add a Path to your batch file!", "OK")
            else:
                if os.path.exists(control.Path.Text):
                    # Updates the UI
                    lBatchPath = control.Path.Text
                    #Open and read in file data
                    lFile = open(lBatchPath, "r")
                    lFileData = lFile.readlines()
                    for lEachFile in lFileData:
                        lResourcePath = lEachFile.strip()
                        core.rs_CreateReference(lResourcePath, str(control.TextureData), 1, control.AcceptNamespace)
                    self.Show( True )
                else:
                    FBMessageBox( "Error", "Please add a valid path on the disk!", "OK")

        def OpenSampleFile  ( pControl, pEvent ):
            #Launch a txt file in Notepad
            lSampleFilePath = r"{0}\wildwest\script\MotionBuilderPython\ToolImages\Mix.txt".format( RS.Config.Tool.Path.Root )
            os.system('notepad.exe ' + lSampleFilePath)

        def SettingAcceptDefaultNamespace( pControl, pEvent ):
            pControl.createReference.AcceptNamespace = pControl.State

        lBatchCreateTabName = "Batch Create"
        lBatchCreateTabLayout = FBLayout()
        lBatchCreateTabLayoutX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lBatchCreateTabLayoutY = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        lBatchCreateTabLayoutW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lBatchCreateTabLayoutH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        lBatchCreateTabLayout.AddRegion(lBatchCreateTabName,lBatchCreateTabName, lBatchCreateTabLayoutX, lBatchCreateTabLayoutY, lBatchCreateTabLayoutW, lBatchCreateTabLayoutH)
        lBatchCreateTabLayout.SetBorder(lBatchCreateTabName,FBBorderStyle.kFBStandardBorder,False, True,1,0,90,0)

        l = FBLabel()
        l.Caption = "Browse to your 'Batch File', click '? - Sample' button to review 'Batch File' layout:"
        x = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(15,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(400,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("lab","lab", x, y, w, h)
        lBatchCreateTabLayout.SetControl("lab",l)

        lSample = FBButton()
        lSample.Caption = "? - Sample"
        lSample.Style = FBButtonStyle.kFBPushButton
        lSampleX = FBAddRegionParam(490, FBAttachType.kFBAttachLeft, "")
        lSampleY = FBAddRegionParam(15, FBAttachType.kFBAttachTop, "")
        lSampleW = FBAddRegionParam(70,FBAttachType.kFBAttachNone,"")
        lSampleH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("lSample", "lSample",lSampleX,lSampleY,lSampleW,lSampleH)
        lBatchCreateTabLayout.SetControl("lSample",lSample)
        lSample.OnClick.Add(OpenSampleFile)

        e = FBEdit()
        x2 = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y2 = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lab")
        w2 = FBAddRegionParam(470,FBAttachType.kFBAttachNone,"")
        h2 = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("path","path", x2, y2, w2, h2)
        lBatchCreateTabLayout.SetControl("path",e)

        b3 = FBButton()
        b3.Caption = "Create Reference(s) from Batch"
        b3.Justify = FBTextJustify.kFBTextJustifyCenter
        b3.Path = e
        b3.TextureData = 0
        b3.AcceptNamespace = 1
        b3.Style = FBButtonStyle.kFBPushButton
        x5 = FBAddRegionParam(15,FBAttachType.kFBAttachLeft,"")
        y5 = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lAcceptDefaultNamespace")
        w5 = FBAddRegionParam(-15,FBAttachType.kFBAttachRight,"")
        h5 = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("shots","shots", x5, y5, w5, h5)
        lBatchCreateTabLayout.SetControl("shots",b3)
        b3.OnClick.Add(BatchCallback)

        b2 = FBButton()
        b2.Caption = "Browse..."
        b2.Path = e
        b2.Style = FBButtonStyle.kFBPushButton
        b2.Justify = FBTextJustify.kFBTextJustifyCenter
        x3 = FBAddRegionParam(475,FBAttachType.kFBAttachLeft,"path")
        y3 = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lab")
        w3 = FBAddRegionParam(70,FBAttachType.kFBAttachNone,"")
        h3 = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("but","but", x3, y3, w3, h3)
        lBatchCreateTabLayout.SetControl("but",b2)
        b2.OnClick.Add(BrowseCallback)

        cb = FBButton()
        cb.Style = FBButtonStyle.kFBCheckbox
        cb.Caption = "Sync All Textures"
        cb.syncMergeControl = b3
        x4 = FBAddRegionParam(15, FBAttachType.kFBAttachLeft, "")
        y4 = FBAddRegionParam(5, FBAttachType.kFBAttachBottom, "path")
        w4 = FBAddRegionParam(300,FBAttachType.kFBAttachNone,"")
        h4 = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("backp", "backp",x4,y4,w4,h4)
        lBatchCreateTabLayout.SetControl("backp",cb)
        cb.OnClick.Add(self.SettingCustomTextureData)

        lAcceptDefaultNamespace = FBButton()
        lAcceptDefaultNamespace.Style = FBButtonStyle.kFBCheckbox
        lAcceptDefaultNamespace.Caption = "Accept Default Refence Namespace(s)"
        lAcceptDefaultNamespace.State = True
        lAcceptDefaultNamespace.createReference = b3
        lAcceptDefaultNamespaceX = FBAddRegionParam(15, FBAttachType.kFBAttachLeft, "")
        lAcceptDefaultNamespaceY = FBAddRegionParam(0, FBAttachType.kFBAttachBottom, "backp")
        lAcceptDefaultNamespaceW = FBAddRegionParam(300,FBAttachType.kFBAttachNone,"")
        lAcceptDefaultNamespaceH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        lBatchCreateTabLayout.AddRegion("lAcceptDefaultNamespace", "lAcceptDefaultNamespace",lAcceptDefaultNamespaceX,lAcceptDefaultNamespaceY,lAcceptDefaultNamespaceW,lAcceptDefaultNamespaceH)
        lBatchCreateTabLayout.SetControl("lAcceptDefaultNamespace",lAcceptDefaultNamespace)
        lAcceptDefaultNamespace.OnClick.Add(SettingAcceptDefaultNamespace)

        lTabLayout.Add(lBatchCreateTabName,lBatchCreateTabLayout)

        ###############################################################################################################
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        ##
        ## Description: GS Pairing Tab
        ## url:bugstar:1174885
        ##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        ###############################################################################################################

        gsPairingTabName = "GS Pairing"
        self.gsPairingTabLayout = FBLayout()
        X = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        W = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        H = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")
        self.gsPairingTabLayout.AddRegion( gsPairingTabName, gsPairingTabName, X, Y, W, H )

        lTabLayout.Add( gsPairingTabName, self.gsPairingTabLayout )

        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachBottom,"" )
        w = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"" )
        h = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        self.gsPairingTabLayout.AddRegion( "mainLayout", "mainLayout", x, y, w, h )

        X = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        Y = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        W = FBAddRegionParam( 700,FBAttachType.kFBAttachNone,"" )
        H = FBAddRegionParam( 202,FBAttachType.kFBAttachNone,"" )
        self.gsPairingTabLayout.AddRegion( "self.SpreadSheet","self.SpreadSheet", X, Y, W, H )
        self.gsPairingTabLayout.SetControl( "self.SpreadSheet", self.SpreadSheet )
        self.SpreadSheet.OnCellChange.Add( self.OnDropDownCellChange )

        lockCheckbox = FBButton()
        lockCheckbox.Style = FBButtonStyle.kFBCheckbox
        lockCheckbox.Style = FBButtonStyle.kFBCheckbox
        lockCheckbox.Caption = 'Pairing Editing Enable/Disable'
        lockCheckbox.Hint = 'safety feature to prevent people\naccidentally editing the pairs.'
        X = FBAddRegionParam(370,FBAttachType.kFBAttachLeft,"")
        Y = FBAddRegionParam(207,FBAttachType.kFBAttachTop,"")
        W = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
        H = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        self.gsPairingTabLayout.AddRegion( "lockCheckbox","lockCheckbox", X, Y, W, H )
        self.gsPairingTabLayout.SetControl( "lockCheckbox", lockCheckbox )
        lockCheckbox.State = True
        lockCheckbox.OnClick.Add( self.LockCallback )

        self.SpreadSheet.ColumnAdd( 'Actor Anim' )
        self.SpreadSheet.ColumnAdd( 'Character' )
        self.SpreadSheet.GetColumn( -1 ).Width = 0
        self.SpreadSheet.GetColumn( 0 ).Width = 360
        self.SpreadSheet.GetColumn( 1 ).Width = 250
        self.SpreadSheet.GetColumn( 0 ).ReadOnly = True

        self.PopulateSpreadSheet()

        lTabLayout.SetContent( 0 )
        lTabLayout.TabPanel.TabStyle = 0

    def PopulateSpreadSheet( self ):
        if self.ActorAnimManager.ActorAnimList:
            # fill spread
            rowIndex = 0
            for anm in self.ActorAnimManager.ActorAnimList:

                self.SpreadSheet.RowAdd( None, rowIndex )
                self.SpreadSheet.Justify = FBTextJustify.kFBTextJustifyRight

                actorAnimCell = self.SpreadSheet.GetSpreadCell( rowIndex, 0 )
                actorAnimCell.Style = FBCellStyle.kFBCellStyleString
                self.SpreadSheet.SetCellValue( rowIndex, 0, str( anm.AnimName ) )

                characterCell = self.SpreadSheet.GetSpreadCell( rowIndex, 1 )
                characterCell.Style = FBCellStyle.kFBCellStyleMenu

                characterCell.Enabled = False

                self.ManagedMenuSpreadCellDict[anm] = RS.Core.Reference.GSPairing.ManagedMenuSpreadCell( self.SpreadSheet, 1, rowIndex )

                rowIndex += 1

            # populate the dropdowns
            self.RefreshDropDownCells()


    def RefreshDropDownCells( self ):
        if self.ActorAnimManager.ActorAnimList:
            for anm in self.ActorAnimManager.ActorAnimList:
                # reference properties
                propertyFound = False
                for prop in self.ActorAnimManager.PropertyReferenceList:
                    if anm.AnimName == prop.AnimReferenceProperty.Name:
                        prop.AnimReferenceProperty.SetLocked( False )
                        prop.AnimReferenceProperty.Data = anm.CharacterName
                        prop.AnimReferenceProperty.SetLocked( True )
                        propertyFound = True
                if propertyFound == False:
                    anmProperty = self.ActorAnimManager.ReferenceScene.PropertyCreate( anm.AnimName, FBPropertyType.kFBPT_charptr, "", False, True, None )
                    anmProperty.Data = anm.CharacterName
                    anmProperty.SetLocked( True )

                # refresh cells
                cellValueList = ["None"]

                if anm.CharacterName != "None":
                    cellValueList.append( anm.CharacterName )

                for unpairedChar in self.ActorAnimManager.GetUnpairedCharacterNames():
                    cellValueList.append( unpairedChar )

                self.ManagedMenuSpreadCellDict[anm].SetCellValues( cellValueList )
                self.ManagedMenuSpreadCellDict[anm].SetCurrentCellValue( anm.CharacterName )

        # re-populate reference list
        self.ActorAnimManager.PopulateReferenceProperty()

        # check if pairs are complete, if not, add a label in the 'Reference Editor' tab
        if self.IncompletePairLabel() == False:
            self.gsPairingLabel.Caption = "NOTE: The GS-Character Pairings are incomplete. Please check the 'GS Pairing' Tab."
        else:
            self.gsPairingLabel.Caption = ' '


    def OnDropDownCellChange( self, control, event ):
        if self.ActorAnimManager.ActorAnimList:
            # get character name
            for anm in self.ActorAnimManager.ActorAnimList:
                currentCellValue = self.ManagedMenuSpreadCellDict[anm].GetCurrentCellValue( )
                anm.CharacterName = currentCellValue

            # refresh lists
            self.RefreshDropDownCells()


    def IncompletePairLabel( self ):
        for prop in self.ActorAnimManager.PropertyReferenceList:
            if prop.CharacterReferenceData == "None":
                return False


    def LockCallback( self, control, event ):
        for i in range( len( self.ActorAnimManager.ActorAnimList ) ):
            dropdownCell = self.SpreadSheet.GetSpreadCell( i, 1 )
            if control.State == 1:
                    dropdownCell.Enabled = False
            else:
                dropdownCell.Enabled = True

def Run( show = True ):
    del core.gCharactersArray[:]
    del core.gPropsArray[:]
    del core.gSetsArray[:]
    del core.gVehiclesArray[:]
    del core.gRayFireArray[:]
    del core.g3LateralArray[:]
    del core.gAmbientFaceArray[:]

    tool = ReferenceEditor()

    if show:
        #For the moment the old ref system and the new one will work side by side, to keep things consistent get the scene null
        #and force reload the new ref manager(temporary code)
        import RS.Core.ReferenceSystem.Manager
        from RS.Core.ReferenceSystem.Manager import Manager        
        
        manager = Manager()
        #Reset the manager
        manager._rootNull = None       
        
        tool.Show()
        
    return tool