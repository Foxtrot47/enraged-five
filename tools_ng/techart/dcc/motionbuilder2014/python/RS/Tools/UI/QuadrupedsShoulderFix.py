# Copyright 2009 Autodesk, Inc.  All rights reserved.
# Use of this software is subject to the terms of the Autodesk license agreement 
# provided at the time of installation or download, or which otherwise accompanies
# this software in either electronic or hard copy form.
#
# Script description:
# Create a tool with 2 different kinds of list and shows how to register callbacks.
# 
# Topic: FBVBoxLayout, FBListStyle, FBList
#

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils
import RS.Tools.UI
import RS.Core.AssetSetup.Quadrupeds.ShoulderFix

reload( RS.Core.AssetSetup.Quadrupeds.ShoulderFix ) 

class CustomAnimalControlRig( RS.Tools.UI.Base ):
    
    def __init__( self ):
        
        RS.Tools.UI.Base.__init__( self, 'Quadrupeds Shoulders Manipulators', size = [ 220, 130 ]  )
        
        self.CharacterDropdown = None        
    
        
    def BtnCallback( self, control, event ):
        
        selectedIndex = None
        for i in range( len( self.CharacterDropdown.Items ) ):
            if self.CharacterDropdown.IsSelected( i ):
                selectedIndex = i     
        
        if selectedIndex != None:
            characterName = RS.Globals.Characters[ selectedIndex ].LongName            
            RS.Core.AssetSetup.Quadrupeds.ShoulderFix.MergeControlRig( characterName )

    def Create(self, mainLyt):
        x = FBAddRegionParam(2,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(200,FBAttachType.kFBAttachNone,"")
        h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        mainLyt.AddRegion("main","main", x,y,w,h)
        
        lyt = FBVBoxLayout()
        mainLyt.SetControl("main",lyt)

        # List creation
        self.CharacterDropdown = FBList()

        # fill the list with characters
        for char in FBSystem().Scene.Characters:
            name = char.LongName
            self.CharacterDropdown.Items.append(name)        
        
        # list properties
        self.CharacterDropdown.Style = FBListStyle.kFBDropDownList
        lyt.Add(self.CharacterDropdown, 25)

        
        '''
        self.CharacterDropdown[1].Style = FBListStyle.kFBVerticalList
        lyt.Add(self.CharacterDropdown[1], 200)
        self.CharacterDropdown[1].Selected(7, True)
        self.CharacterDropdown[1].MultiSelect = True
        '''
        b = FBButton()
        b.Caption = "Deploy Shoulders System"
        b.Justify = FBTextJustify.kFBTextJustifyLeft
        lyt.Add(b,40)
        b.OnClick.Add(self.BtnCallback)


def Run( show = True ):
    tool = CustomAnimalControlRig()

    if show:
        tool.Show()

    return tool

