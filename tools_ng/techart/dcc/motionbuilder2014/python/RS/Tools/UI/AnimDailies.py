'''
Script Name     RSGTOR_GTA5_AnimDailies_2012.py
Author          Stephen Sloper, Rockstar Toronto. 2012.

Description     Creates and saves renders of takes in the GTA5 anim dailies folder.

HISTORY         v1.00 beta      First revision. In testing, features still to add.
                v1.01           Added UI options to include last frame and audio.
                                Custom Render Mode now works.
                v1.02           Adding 'override' to Render Destination.
                v2.00           Added ScrollBox to bottom of window to display 'Existing Daily Renders'.
                v2.10           Added .mov (h.264) convertion using ffmpeg (<project>/tools/bin/video/ffmpeg.exe).
                                Made the script usable to those unrecognized by the script. Custom Path only.
                v2.11           Removed audio codec flag (-acodec libmp3lame) from FFMPEG as it was causing errors before playback in QT.
                v2.20           Added some lines to create daily folders for new users. Both when rendering and when opening their daily folder.
                v2.21           Added RSGTOR users to user list.

To Run          Drag Drop file into viewport, click 'Execute'.
'''

import getpass, os, sys, shutil, datetime, subprocess, ctypes

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Config
import RS.Tools.UI


class AnimDailiesTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Animation Dailies", size = [ 555, 475 ] )
        self.userNameLabel = None
        self.defaultPathDestLabel = None       

    def UserDictionary( self ):
        dailyUpdateRoot = 'N:\RSGEDI\Reference\Animation\In-Game\Daily Updates\\'
        
        userDictionary = {'ssloper': ('Stephen Sloper', dailyUpdateRoot),
        'preynolds': ('Peter Reynolds', dailyUpdateRoot),
        'triggs': ('Tristan Riggs', dailyUpdateRoot),
        'jcarroll': ('Jeff Carroll', dailyUpdateRoot),
        'Mtempest': ('Matt Tempest', dailyUpdateRoot),
        'mtempest': ('Matt Tempest', dailyUpdateRoot),
        'jhoaglund': ('Josh Hoaglund', dailyUpdateRoot),
        'mtennant': ('Mark Tennant', dailyUpdateRoot),
        'bchue': ('Benny Chue', dailyUpdateRoot),
        'BChue': ('Benny Chue', dailyUpdateRoot),
        'jkim': ('John Kim', dailyUpdateRoot),
        'mpinnock': ('Mark Pinnock', dailyUpdateRoot),
        'mfonseca': ('Matt Fonseca', dailyUpdateRoot),
        'relsworthy': ('Rob Elsworthy', dailyUpdateRoot),
        'aahmed': ('Abe Ahmed', dailyUpdateRoot),
        'abeahmed': ('Abe Ahmed', dailyUpdateRoot),
        'spencer.jones': ('Spencer Jones', dailyUpdateRoot),
        'Kirkc': ('Kirk Cumming', dailyUpdateRoot),
        'Bharris': ('Brett Harris', dailyUpdateRoot),
        'mpeterson': ('Michael Peterson', dailyUpdateRoot),
        'rstratton': ('Randy Stratton', dailyUpdateRoot),
        'Eliot': ('Eliot Tokoroyama', dailyUpdateRoot),
        'eliot': ('Eliot Tokoroyama', dailyUpdateRoot),
        'simon.papp': ('Simon Papp', dailyUpdateRoot),
        'Simon.Papp': ('Simon Papp', dailyUpdateRoot),
        'lisa.stapleton': ('Lisa Stapleton', dailyUpdateRoot),
        'myork': ('Mike York', dailyUpdateRoot),
        'kmiddlemiss': ('Kristine Middlemiss', dailyUpdateRoot),
        'mblanchette': ('Mike Blanchette', dailyUpdateRoot),
        'awelihozkiy': ('Andy Welihozkiy', dailyUpdateRoot),
        'jries': ('Joe Ries', dailyUpdateRoot),         
        'adavies': ('Andy Davies', dailyUpdateRoot)}
        
        return userDictionary
    
    def SetUser( self ):
        userList = self.UserDictionary()
        user = getpass.getuser()
        userInfo = userList.get(user)
        #print type(userInfo)
        if user not in userList:
            message = "AnimDailies does not recognise you - Default Path cannot be set. \nContact stephen.sloper@rockstartoronto.com stating username '%s' to add your Default Path. \n\nOnly 'Custom Path' renders will be available. \n\n- Thanks!" % user
            FBMessageBox('GTA5_AnimDailies - UNKNOWN USER', message, 'OK')
            self.userNameLabel = 'UNKNOWN USER - only "Custom Path" renders will be available to you.'
            self.defaultPathDestLabel = 'UNKNOWN USER - no default path available.'               
            
        else:
            #set user info in UI
            self.userNameLabel = userInfo[0]
            self.defaultPathDestLabel = ('%s%s\\' % (userInfo[1], userInfo[0]))              
            #print ('RSGTOR_GTA5_AnimDailies setup for %s' % userInfo[0])
    
    
    def FileBrowser(self, control, event):
        fileBrowser = FBFolderPopup()
        fileBrowser.Caption = 'Select Custom folder for Daily Renders...'
        
        userList = self.UserDictionary()
        user = getpass.getuser()
        userInfo = userList.get(user)
        #try to procedureally create desktop path - C:\Users\USER\Desktop
        defaultPath = ('C:\Users\%s\Desktop\\' % user)       #('%s%s\\' % (userInfo[1], userInfo[0]))
        fileBrowser.Path = defaultPath
        fileBrowser.Style = FBFilePopupStyle.kFBFilePopupOpen
        fileBrowser.Execute()
        g_animDailies_custDirField.Text = fileBrowser.Path
    
    def CreateDaily(self, control, event):
        #run SetUser - shortcut for producing unknown user error message.   #removed as script can now be used by unknown users.
        #RSGTOR_GTA5_AnimDailies_SetUser()
    
        #get all mode states
        currentMode = g_animDailies_currentRadioBut.State
        allMode = g_animDailies_allRadioBut.State
        customMode = g_animDailies_customRadioBut.State
        
        if currentMode == 1:
            currentTake = FBSystem().CurrentTake
            self.DoRender(currentTake)
        if allMode == 1:
            allTakes = FBSystem().Scene.Takes
            for take in allTakes:
                self.DoRender(take)
        if customMode == 1:
            allTakesUI = g_animDailies_takeList.Items        
            for x in range(0, len(allTakesUI)):
                if g_animDailies_takeList.IsSelected(x):
                    #print allTakesUI[x]
                    allTakesScene = FBSystem().Scene.Takes
                    for i in allTakesScene:
                        if i.Name == allTakesUI[x]:
                            self.DoRender(i)
        #refresh dailies file list
        self.ProcessDailies('', '')
    
    
    def DoRender(self, takeToRender):
        #get all variables
        if g_animDailies_custDirOptBut.State == True:
            destDir = g_animDailies_custDirField.Text
        else:
            destDir = self.defaultPathDestLabel
            if os.path.exists(destDir) == False:
                os.mkdir(destDir)          
        
        #error check to see if path exists
        pathValid = os.path.exists(destDir)
        
        if pathValid == False:
            message = "Custom Path entered is not valid. Please enter a valid path name."
            FBMessageBox('GTA5_AnimDailies - PATH ERROR', message, 'OK')
            
        else:
    
            prefix = g_animDailies_prefixEdit.Text
            suffix = g_animDailies_suffixEdit.Text
            startFrame = takeToRender.LocalTimeSpan.GetStart().GetFrame()
            endFrame = takeToRender.LocalTimeSpan.GetStop().GetFrame()
            todaysDate = datetime.date.today()
            
            #add slash to end of path if one is not present.
            if destDir[len(destDir)-1] != '//':
                destDir = destDir + '//'
            dailyDir = destDir + str(todaysDate) + '\\'
            
            #negate 1 from last frame if 'Include Last Frame' is not checked.
            incLastFrame = g_animDailies_incLastFrameBut.State
            if incLastFrame == False:
                endFrame = endFrame - 1
            
            #create/check for anim daily folder
            dailyDirExist = os.path.exists(dailyDir)
            if dailyDirExist == False:
                os.mkdir(dailyDir)
                print ('Creating folder for todays daily: ' + dailyDir)
            else:
                print ('Adding renders to daily folder ' + dailyDir)
            
            mgr = FBVideoCodecManager()
            # By specifying Codec stored, the first time we render a scene, the codec dialog
            # will be available if user presses on configure
            # the second time a scene is rendered, the same settings will be used.
            mgr.VideoCodecMode = FBVideoCodecMode.FBVideoCodecStored
            app = FBApplication()
            videoOptions = FBVideoGrabber().GetOptions()
            videoOptions.BitsPerPixel = FBVideoRenderDepth.FBVideoRender32Bits
            videoOptions.ShowTimeCode = True
            videoOptions.RenderAudio = False
            captureRange = FBTimeSpan()
            captureRange.Set(FBTime(0,0,0,startFrame), FBTime(0,0,0,endFrame))
            videoOptions.TimeSpan = captureRange
            
            #check 'Include Audio' button from UI to specify whether audio should be rendered. 
            incAudio = g_animDailies_incAudioBut.State
            if incAudio == True:
                videoOptions.RenderAudio = True
            
            if prefix > 0:
                saveName = dailyDir + prefix
            saveName = saveName + takeToRender.Name
            if suffix > 0:
                saveName = saveName + suffix
            saveName = saveName + '.avi'
            
            videoOptions.OutputFileName = saveName
            
            #delete any files with existing name
            renderExist = os.path.exists(saveName)
            if renderExist == True:
                os.remove(saveName)
            
            #render take
            #must make take to render current take or videos will not render properly.
            FBSystem().CurrentTake = takeToRender
            app.FileRender(videoOptions)
            print ('Render created for "%s" in %s' % (takeToRender.Name, dailyDir))
            
            #convert render using ffmpeg
            ffmpeg = r'{0}\bin\video\ffmpeg.exe'.format( RS.Config.Tool.Path.Root )
            sourceMovie = saveName        
            finalMovie = sourceMovie.replace('.avi', '.mp4')
            
            if os.path.exists(ffmpeg) == False:
                message = "ffmpeg library does not exist, unable to convert to .mov. Get latest from Perforce to enable convertion ('<project>/tools/bin/video/ffmpeg.exe').\nVideo rendered as .avi."
                FBMessageBox('GTA5_AnimDailies - ffmpeg ERROR', message, 'OK')
                #raise TypeError(message)
            else:
                #use convertion settings;
                #audio codec = MP3 (This was removed as ffmpeg would create playback errors. command was '-acodec libmp3lame')
                #number of audio tracks = 2 (stereo)
                #audio rate 44100 (cd quality)
                #video codec = H.264
                #GOP (group of pictures - helps looping) = 10
                #overwrite existing file = yes
                os.system('%s -i "%s" -ac 2 -ar 44100 -vcodec libx264 -g 10 -y "%s"' % (ffmpeg, sourceMovie, finalMovie))
                os.remove(sourceMovie)
                print ('Successfully converted %s to .mp4 using H.264 codec.' % sourceMovie)
        
    
    def BuildDaily( self ):
        #this build daily info (video links) to paste into email
        dailyDir = self.defaultPathDestLabel
        dailyRenderList =  os.listdir(dailyDir)
        print [(dailyDir + x) for x in dailyRenderList]
    
    
    def ClearTakeList(self, control, event):
        g_animDailies_takeList.Items.removeAll()
    
    
    def GenerateTakeList(self, control, event):
        allTakes = FBSystem().Scene.Takes
        for x in allTakes:
            g_animDailies_takeList.Items.append(x.Name)
    
    
    def OpenDailyDir(self, control, event):
        dailyDir = self.defaultPathDestLabel
        if os.path.exists(dailyDir) == False:
                os.mkdir(dailyDir)
        subprocess.Popen('explorer %s' % dailyDir)
        
    
    def OpenCustomDailyDir(self, control, event):
        dailyDir = g_animDailies_custDirField.Text
        if os.path.exists(dailyDir):
            subprocess.Popen('explorer %s' % dailyDir)
        else:
            message = "Custom Path entered is not valid. Please enter a valid path name."
            FBMessageBox('GTA5_AnimDailies - PATH ERROR', message, 'OK')
            #raise TypeError(message)
    
    
    def ToggleCustomDailyDir(self, control, event):
        buttonState = g_animDailies_custDirOptBut.State
        if buttonState == 1:
            g_animDailies_custDirField.ReadOnly = False
            custDirBut.ReadOnly = False
            custDirBut.Caption = '<<<'
            custDirBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.38, 0.38, 0.38))
            custDirOpenBut.ReadOnly = False
            custDirOpenBut.Caption = 'open'
            custDirOpenBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.38, 0.38, 0.38))
            
        else:
            g_animDailies_custDirField.ReadOnly = True
            custDirBut.ReadOnly = True
            custDirBut.Caption = ''
            custDirBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.28, 0.28, 0.28))
            custDirOpenBut.ReadOnly = True
            custDirOpenBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.28, 0.28, 0.28))
            custDirOpenBut.Caption = ''
    
    
    def ProcessDailies(self, content, user):
        destDir = self.defaultPathDestLabel
        todaysDate = datetime.date.today() 
        dailyDir = destDir + str(todaysDate) + '\\'
        
        global g_animDailies_scrollMainLyt
        global g_animDailies_todaysDailiesLabel        
    
        if os.path.exists(dailyDir):
            #get folder contents and prep data
            content = os.listdir(dailyDir)
            fileCount = len(content)
            startString = ("Today's Dailies (%s files):" % fileCount)
            contentString = ''
            for x in content:
                contentString = '\n - %s%s' % (x, contentString)
            
            #remove and rebuild layout using expanded size
            g_animDailies_scrollMainLyt.Remove(g_animDailies_todaysDailiesLabel)
            yGrow = 13
            
            g_animDailies_scrollMainLyt = FBVBoxLayout()
            g_animDailies_scrollLyt.Content.SetControl("content", g_animDailies_scrollMainLyt)
            newValue = (yGrow * fileCount + 100)
            g_animDailies_scrollLyt.SetContentSize(470,newValue)
            
            g_animDailies_todaysDailiesLabel = FBLabel()
            newValue = (yGrow * fileCount + 40)
            g_animDailies_scrollMainLyt.Add(g_animDailies_todaysDailiesLabel,newValue)
            
            refreshDailiesBut = FBButton()
            refreshDailiesBut.Caption = 'Refresh'
            refreshDailiesBut.OnClick.Add(self.ProcessDailies)
            g_animDailies_scrollMainLyt.Add(refreshDailiesBut,20)
            
            copyDailyLinksBut = FBButton()
            copyDailyLinksBut.Caption = 'Copy as links to clipboard'
            copyDailyLinksBut.OnClick.Add(self.PrepareDataForClipboard)
            g_animDailies_scrollMainLyt.Add(copyDailyLinksBut,20)
            
            g_animDailies_todaysDailiesLabel.Caption = startString + contentString
        else:
            #remove and rebuild layout using default size
            g_animDailies_scrollMainLyt.Remove(g_animDailies_todaysDailiesLabel)
            
            g_animDailies_scrollMainLyt = FBVBoxLayout()
            g_animDailies_scrollLyt.Content.SetControl("content", g_animDailies_scrollMainLyt)
            g_animDailies_scrollLyt.SetContentSize(470, 100)
            
            g_animDailies_todaysDailiesLabel = FBLabel()
            g_animDailies_todaysDailiesLabel.Caption = "Today's Dailies: \n "
            g_animDailies_scrollMainLyt.Add(g_animDailies_todaysDailiesLabel, 40)
            
            refreshDailiesBut = FBButton()
            refreshDailiesBut.Caption = 'Refresh'
            refreshDailiesBut.OnClick.Add(self.ProcessDailies)
            g_animDailies_scrollMainLyt.Add(refreshDailiesBut,20)
            
            copyDailyLinksBut = FBButton()
            copyDailyLinksBut.Caption = 'Copy as links to clipboard'
            copyDailyLinksBut.OnClick.Add(self.PrepareDataForClipboard)
            g_animDailies_scrollMainLyt.Add(copyDailyLinksBut,20)
            
    
    def CopyToClipboard(self, txt):
        ctypes.windll.user32.OpenClipboard(None)
        ctypes.windll.user32.EmptyClipboard()
        hcd = ctypes.windll.kernel32.GlobalAlloc(0x2000, len(bytes(txt))+1)
        pd = ctypes.windll.kernel32.GlobalLock(hcd)
        ctypes.cdll.msvcrt.strcpy(ctypes.c_char_p(pd), bytes(txt))
        ctypes.windll.kernel32.GlobalUnlock(hcd)
        ctypes.windll.user32.SetClipboardData(1, hcd)
        ctypes.windll.user32.CloseClipboard()
    
    
    def PrepareDataForClipboard(self, content, user):
        destDir = self.defaultPathDestLabel
        todaysDate = datetime.date.today() 
        dailyDir = destDir + str(todaysDate) + '\\'
        if os.path.exists(dailyDir):
            content = os.listdir(dailyDir)
            singleString = ''
            for x in content:
                #print dailyDir + x
                singleString = (singleString + dailyDir + x + '\n')
            print 'Daily Links copied to clipboard successfully.'
            self.CopyToClipboard(singleString)
        else:
            message = "There are no videos in todays dailies folder. No links were copied."
            FBMessageBox('GTA5_AnimDailies - FILE WARNING', message, 'OK')
            #raise TypeError(message)
    
    
    def Create(self, ui):
        
        
        #create the main adjustable layout --------------------------------------------------
        al = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"")
        ui.AddRegion("main","main", al, at, ar, ab)
        mainLyt = FBVBoxLayout()
        ui.SetControl("main",mainLyt)
        
        #create the user layout -------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(10,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        userLyt = FBHBoxLayout()
        ui.AddRegion("user","user", al, at, ar, ab)
        ui.SetControl("user",userLyt)
    
        userLabel = FBLabel()
        userLabel.Caption = 'User:'
        userLyt.Add(userLabel,80)
        
        g_animDailies_userNameLabel = FBLabel()
        g_animDailies_userNameLabel.Caption = self.userNameLabel 
        userLyt.Add(g_animDailies_userNameLabel,350)
        
        #create the dir layout -------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(33,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        dirLyt = FBHBoxLayout()
        ui.AddRegion("dir","dir", al, at, ar, ab)
        ui.SetControl("dir",dirLyt)
    
        defaultPathLabel = FBLabel()
        defaultPathLabel.Caption = 'Default Path:'
        dirLyt.Add(defaultPathLabel,80)
    
        g_animDailies_defaultPathDestLabel = FBLabel()
        g_animDailies_defaultPathDestLabel.Caption = self.defaultPathDestLabel
        dirLyt.Add(g_animDailies_defaultPathDestLabel,375)
        
        openDefaultPathBut = FBButton()
        openDefaultPathBut.Caption = 'open'
        dirLyt.Add(openDefaultPathBut,50)
        openDefaultPathBut.OnClick.Add(self.OpenDailyDir)
        
        
        #create the custom dir layout -------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(57,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        custDirLyt = FBHBoxLayout()
        ui.AddRegion("custDir","custDir", al, at, ar, ab)
        ui.SetControl("custDir",custDirLyt)
    
        custDirLabel = FBLabel()
        custDirLabel.Caption = 'Custom Path:'
        custDirLyt.Add(custDirLabel,76)
        
        global g_animDailies_custDirOptBut
        g_animDailies_custDirOptBut = FBButton()
        g_animDailies_custDirOptBut.Style = FBButtonStyle.kFBCheckbox
        custDirLyt.Add(g_animDailies_custDirOptBut,20)
        g_animDailies_custDirOptBut.OnClick.Add(self.ToggleCustomDailyDir)
    
        global g_animDailies_custDirField
        g_animDailies_custDirField = FBEdit()
        g_animDailies_custDirField.ReadOnly = True
        custDirLyt.Add(g_animDailies_custDirField,300)
        
        global custDirBut
        custDirBut = FBButton()
        custDirBut.ReadOnly = True
        custDirBut.Look = FBButtonLook.kFBLookColorChange
        custDirBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.28, 0.28, 0.28))
        custDirLyt.Add(custDirBut,50)
        custDirBut.OnClick.Add( self.FileBrowser )
        
        global custDirOpenBut
        custDirOpenBut = FBButton()
        custDirOpenBut.ReadOnly = True
        custDirOpenBut.Look = FBButtonLook.kFBLookColorChange
        custDirOpenBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0.28, 0.28, 0.28))
        custDirLyt.Add(custDirOpenBut,50)
        custDirOpenBut.OnClick.Add(self.OpenCustomDailyDir)
        
        #create the mode layout -------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        modeLyt = FBHBoxLayout()
        ui.AddRegion("mode","mode", al, at, ar, ab)
        ui.SetControl("mode",modeLyt)
        
        RenderModeLabel = FBLabel()
        RenderModeLabel.Caption = 'Render Mode:'
        modeLyt.Add(RenderModeLabel,80)
        
        #placing all radios in this group will allow single selections only
        butGroup = FBButtonGroup()
        
        global g_animDailies_currentRadioBut
        g_animDailies_currentRadioBut = FBButton()
        g_animDailies_currentRadioBut.Caption = 'Current'
        g_animDailies_currentRadioBut.Style = FBButtonStyle.kFBRadioButton
        g_animDailies_currentRadioBut.State = True
        g_animDailies_currentRadioBut.OnClick.Add(self.ClearTakeList)
        butGroup.Add(g_animDailies_currentRadioBut)
        
        al = FBAddRegionParam(90,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(-5,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(100,FBAttachType.kFBAttachRight,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        modeLyt.AddRegion("g_animDailies_currentRadioBut","g_animDailies_currentRadioBut", al, at, ar, an)
        modeLyt.SetControl("g_animDailies_currentRadioBut",g_animDailies_currentRadioBut)
        
        global g_animDailies_allRadioBut
        g_animDailies_allRadioBut = FBButton()
        g_animDailies_allRadioBut.Caption = 'All'
        g_animDailies_allRadioBut.Style = FBButtonStyle.kFBRadioButton
        g_animDailies_allRadioBut.OnClick.Add(self.ClearTakeList)
        butGroup.Add(g_animDailies_allRadioBut)
        
        al = FBAddRegionParam(190,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(-5,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(100,FBAttachType.kFBAttachRight,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        modeLyt.AddRegion("g_animDailies_allRadioBut","g_animDailies_allRadioBut", al, at, ar, an)
        modeLyt.SetControl("g_animDailies_allRadioBut",g_animDailies_allRadioBut)
        
        global g_animDailies_customRadioBut
        g_animDailies_customRadioBut = FBButton()
        g_animDailies_customRadioBut.Caption = 'Custom'
        g_animDailies_customRadioBut.Style = FBButtonStyle.kFBRadioButton
        g_animDailies_customRadioBut.OnClick.Add(self.GenerateTakeList)
        butGroup.Add(g_animDailies_customRadioBut)
        
        al = FBAddRegionParam(270,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(-5,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(100,FBAttachType.kFBAttachRight,"")
        an = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")
        modeLyt.AddRegion("g_animDailies_customRadioBut","g_animDailies_customRadioBut", al, at, ar, an)
        modeLyt.SetControl("g_animDailies_customRadioBut",g_animDailies_customRadioBut)
        
        #create the list layout -------------------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(110,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(150,FBAttachType.kFBAttachNone,"")
        listLyt = FBVBoxLayout()
        ui.AddRegion("list","list", al, at, ar, ab)
        ui.SetControl("list",listLyt)
        
        global g_animDailies_takeList
        g_animDailies_takeList = FBList()
        g_animDailies_takeList.Style = FBListStyle.kFBVerticalList
        g_animDailies_takeList.MultiSelect = True
        listLyt.Add(g_animDailies_takeList,100)
        
        #create the prefix suffix layout -----------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(230,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(15,FBAttachType.kFBAttachNone,"")
        presufLyt = FBHBoxLayout()
        ui.AddRegion("presuf","presuf", al, at, ar, ab)
        ui.SetControl("presuf",presufLyt)
        
        prefixLabel = FBLabel()
        prefixLabel.Caption = 'Prefix:'
        presufLyt.Add(prefixLabel,30)
        
        global g_animDailies_prefixEdit
        g_animDailies_prefixEdit = FBEdit()
        presufLyt.Add(g_animDailies_prefixEdit,70)
        
        dummy1Label = FBLabel()
        dummy1Label.Caption = ' '
        presufLyt.Add(dummy1Label,15)
    
        suffixLabel = FBLabel()
        suffixLabel.Caption = 'Suffix:'
        presufLyt.Add(suffixLabel,30)
        
        global g_animDailies_suffixEdit
        g_animDailies_suffixEdit = FBEdit()
        presufLyt.Add(g_animDailies_suffixEdit,70)
        
        dummy2Label = FBLabel()
        dummy2Label.Caption = ' '
        presufLyt.Add(dummy2Label,30)
        
        incLastFrameLabel = FBLabel()
        incLastFrameLabel.Caption = 'Include Last Frame:'
        presufLyt.Add(incLastFrameLabel,95)
    
        global g_animDailies_incLastFrameBut    
        g_animDailies_incLastFrameBut = FBButton()
        g_animDailies_incLastFrameBut.Style = FBButtonStyle.kFBCheckbox
        presufLyt.Add(g_animDailies_incLastFrameBut,40)
        
        incAudioLabel = FBLabel()
        incAudioLabel.Caption = 'Include Audio:'
        presufLyt.Add(incAudioLabel,70)
        
        global g_animDailies_incAudioBut
        g_animDailies_incAudioBut = FBButton()
        g_animDailies_incAudioBut.Style = FBButtonStyle.kFBCheckbox
        presufLyt.Add(g_animDailies_incAudioBut,40)
        
        #create the render button layout -----------------------------------------------------
        al = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(255,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(50,FBAttachType.kFBAttachNone,"")
        renderLyt = FBVBoxLayout()
        ui.AddRegion("render","render", al, at, ar, ab)
        ui.SetControl("render",renderLyt)
        
        renderBut = FBButton()
        renderBut.Caption = '- RENDER -'
        renderBut.OnClick.Add(self.CreateDaily)
        renderBut.Look = FBButtonLook.kFBLookColorChange
        renderBut.SetStateColor(FBButtonState.kFBButtonState0,FBColor(0, 0.5, 0.5))
        renderLyt.Add(renderBut,40)
        
        #create the daily info layout ---------------------------------------------------------
        al = FBAddRegionParam(20,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(320,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(-20,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(-20,FBAttachType.kFBAttachBottom,"")
        dailiesLyt = FBVBoxLayout()
        ui.AddRegion("daily","daily", al, at, ar, ab)
        ui.SetControl("daily",dailiesLyt)
        ui.SetBorder("daily",FBBorderStyle.kFBStandardBorder,False, True,2,7,90,0)
        
        al = FBAddRegionParam(3,FBAttachType.kFBAttachLeft,"")
        at = FBAddRegionParam(-8,FBAttachType.kFBAttachTop,"")
        ar = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        ab = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        
        global g_animDailies_scrollLyt
        g_animDailies_scrollLyt = FBScrollBox()
        ui.SetControl("daily",g_animDailies_scrollLyt)
        g_animDailies_scrollLyt.Content.AddRegion("content", "content", al, at, ar, ab)
        
        global g_animDailies_scrollMainLyt
        g_animDailies_scrollMainLyt = FBVBoxLayout()
        g_animDailies_scrollLyt.Content.SetControl("content", g_animDailies_scrollMainLyt)
        g_animDailies_scrollLyt.SetContentSize(470,100)
        
        global g_animDailies_todaysDailiesLabel
        g_animDailies_todaysDailiesLabel = FBLabel()
        g_animDailies_todaysDailiesLabel.Caption = "Today's Dailies: \n "
        g_animDailies_scrollMainLyt.Add(g_animDailies_todaysDailiesLabel,50)
        
        refreshDailiesBut = FBButton()
        refreshDailiesBut.Caption = 'Refresh'
        refreshDailiesBut.OnClick.Add(self.ProcessDailies)
        g_animDailies_scrollMainLyt.Add(refreshDailiesBut,20)
        
        copyDailyLinksBut = FBButton()
        copyDailyLinksBut.Caption = 'Copy as links to clipboard'
        g_animDailies_scrollMainLyt.Add(copyDailyLinksBut,20)

def Run( show = True ):
    tool = AnimDailiesTool()
    
    if show:
        tool.SetUser()
        tool.Show()
    
        tool.ProcessDailies('', '')
        
    return tool