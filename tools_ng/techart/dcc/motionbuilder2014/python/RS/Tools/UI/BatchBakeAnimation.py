"""
Description:
    UI for selecting FBCharacters and plotting animation from the skeleton to control rig and vice versa of multiple
    FBCharacters.

Author:
    David Vega <david.vega@rockstargames.com>
"""
import ast
from functools import partial
from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS.Tools.UI import ExternalProgressBar, Run
from RS.Utils.Scene import UndoBlock
from RS.Core.Animation import Lib
from RS.Core.Scene.widgets import ComponentView


class BatchBakeAnimation(QtGui.QWidget):
    """
    UI for selecting FBCharacters and plotting animation from the skeleton to control rig and vice versa of multiple
    FBCharacters.
    """
    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(BatchBakeAnimation, self).__init__(parent=parent)

        self.takeView = ComponentView.ComponentView(componentType=mobu.FBTake)
        self.characterView = ComponentView.ComponentView(componentType=mobu.FBCharacter)

        self.splitter = QtGui.QSplitter()
        self.splitter.addWidget(self.takeView)
        self.splitter.addWidget(self.characterView)
        self.characterView.setShowLongName(False)

        skeletonButton = QtGui.QPushButton("Plot To Skeleton")
        controlRigButton = QtGui.QPushButton("Plot To Control Rig")
        bothButton = QtGui.QPushButton("Plot To Both")

        skeletonSwitchButton = QtGui.QPushButton("Switch To Skeleton")
        controlRigSwitchButton = QtGui.QPushButton("Switch To Control Rig")

        gridLayout = QtGui.QGridLayout()
        gridLayout.addWidget(skeletonButton, 0, 0)
        gridLayout.addWidget(controlRigButton, 0, 1)
        gridLayout.addWidget(skeletonSwitchButton, 1, 0)
        gridLayout.addWidget(controlRigSwitchButton, 1, 1)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.splitter)
        layout.addWidget(bothButton)
        layout.addLayout(gridLayout)

        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

        skeletonButton.clicked.connect(partial(self.PlotCharacters, ["skeleton"]))
        controlRigButton.clicked.connect(partial(self.PlotCharacters, ["rig"]))
        bothButton.clicked.connect(partial(self.PlotCharacters, ["skeleton", "rig"]))

        skeletonSwitchButton.clicked.connect(partial(self.ActivateControlRig, False))
        controlRigSwitchButton.clicked.connect(self.ActivateControlRig)

    def CreateMenu(self):
        """
        Creates the menu bar and its contents

        Return:
            QtGui.QMenuBar
        """

        self.menuBar = QtGui.QMenuBar(self)
        settings = QtGui.QMenu(("Settings"))

        self.removeLayers = QtGui.QAction(self.tr("Remove animation layers on takes"), self)

        self.removeLayers.setCheckable(True)

        self.menuBar.addMenu(settings)
        settings.addAction(self.removeLayers)
        return self.menuBar

    def PlotCharacters(self, plotLocations=('rig',)):
        """
        Plots animation to the selected characters

        Arguments:
            plotLocations: (list[string, etc]): list of locations to plot animation, list can only contain 'rig' and/or
                                             'skeleton'
        """

        with UndoBlock("Plot") as _:
            characters = self.characterView.selectedComponents()
            takes = self.takeView.selectedComponents()

            error = []
            if not characters:
                error.append("No character selected\n")
            if not takes:
                error.append("No take selected\n")
            if error:
                QtGui.QMessageBox.warning(None, "Bake Animation Error", "".join(error))
                return

            plotNumber = len(characters) * len(plotLocations) * len(takes)

            if characters:
                progressBar = ExternalProgressBar.Run("Starting Character Plotting", minimum=0, maximum=plotNumber + 1)
                Lib.PlotCharacters(characters, plotLocations=plotLocations, takes=takes,
                                   progressBar=progressBar)
                progressBar.Update(text="Plotting Finished", value=plotNumber + 1)

            if self.removeLayers.isChecked():
                Lib.RemoveAnimationLayersFromTakes(takes=takes)

    def ActivateControlRig(self, active=True):
        """
        Turns on Control Rig and sets the visibility for the controls
        Arguments:
            active (boolean): should the control rig be active
        """
        [Lib.SetControlRigVisibility(character, visible=active, active=active)
         for character in self.characterView.selectedComponents()]

    def storeWindowState(self, qsetting):
        """
        Stores settings into the QSettings when the tool closes

        Arguments:
            qsetting (QtGui.QSettings): object to store settings in

        """
        qsetting.setValue("sizes", self.splitter.sizes())
        qsetting.setValue("clearLayers", self.removeLayers.isChecked())

    def restoreWindowState(self, qsetting):
        """
        Restores settings from the QSettings when the tool opens

        Arguments:
            qsetting (QtGui.QSettings): object to restore settings from

        """
        values = [ast.literal_eval(value) for value in qsetting.value("sizes", [self.width()/2, self.width()/2])]
        self.splitter.setSizes(values)
        self.removeLayers.setChecked("true" == qsetting.value("clearLayers", "false"))

    def closeEvent(self, event):
        """
        Overrides built-in method

        Removes motion builder callbacks

        Arguments:
            event (QtGui.QEvent): event being called
        """

        super(self.__class__, self).closeEvent(event)
        self.takeView.closeEvent(event)
        self.characterView.closeEvent(event)


@Run(title="Batch Bake Characters", size=[168, 200], dockable=True)
def Run(show=True):
    """
    Show tool in Motion Builder
    Argument:
        show (Boolean): should the window be visible

    Return:
        RS.Tools.UI.QtBannerWindowBase()
    """
    return BatchBakeAnimation()
