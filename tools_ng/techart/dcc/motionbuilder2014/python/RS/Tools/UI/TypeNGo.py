"""
Tool for finding tools from the R* Menu by typing their name

Image:
    $TechArt/Tools/TypeAndGo.png
"""
from RS.Utils import MenuBuilder
from RS.Tools import TypeNGo

from RS.Tools.UI import Run as Run_

from PySide import QtGui, QtCore

__WIKI__ = r'https://devstar.rockstargames.com/wiki/index.php/Type%27n%27Go_Tool'


class TypeNGoTool(QtGui.QLineEdit):
    """
    Tool for finding the tools in the Rockstar menu by typing their names
    """

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget

        """
        super(TypeNGoTool, self).__init__(parent=parent)

        self.registeredTools = MenuBuilder.GetRegisteredTools()

        # The heart of the tool, the completer is what finds which names match the current text
        completer = QtGui.QCompleter(sorted(self.registeredTools.keys()))
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.setCompleter(completer)

        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.setStyleSheet("font : 16px;")
        self.returnPressed.connect(self.OpenTool)

    def OpenTool(self):
        """
        Opens the tool whose name best matches the current text
        """
        text = self.text()
        toolFound = True
        errorMessage = "Cannot find a Rockstar tool with the name: {} \n\nPlease try again!".format(text)

        if text:
            self.completer().setCompletionPrefix(text)
            toolName = self.completer().currentCompletion()
            toolFound, errorMessage = TypeNGo.Search(toolName, self.registeredTools)

        if not toolFound:
            QtGui.QMessageBox.warning(self, "Rockstar", errorMessage)


@Run_(title="Type'n'Go", size=[200, 50], url=__WIKI__)
def Run():
    """
    Launches the UI

    Return:
        Motion Builder Window
    """
    widget = QtGui.QWidget()
    widget.setLayout(QtGui.QVBoxLayout())
    widget.layout().addWidget(QtGui.QLabel("  Start typing the name of the tool you need and press enter when done  "))
    widget.layout().addWidget(TypeNGoTool())
    widget.layout().setContentsMargins(0, 5, 0, 0)
    widget.setStyleSheet("QLabel{font: 12px};")
    return widget
