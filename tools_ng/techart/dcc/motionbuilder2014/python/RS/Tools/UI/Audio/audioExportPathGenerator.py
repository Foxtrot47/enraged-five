from PySide import QtGui, QtCore
from RS.Core.Audio import exportPathManager
from RS.Tools import UI

import pyfbsdk as mobu


class AudioExportPathGeneratorWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(AudioExportPathGeneratorWidget, self).__init__(parent=parent)        
        self.setupUI()
        
    def setupUI(self):
        layout = QtGui.QVBoxLayout()
        
        refreshP4Button = QtGui.QPushButton("Refresh P4 Cache")
        self._takeNameLineEdit = QtGui.QLineEdit()
        runButton = QtGui.QPushButton("Run!")
        self._outputLog = QtGui.QPlainTextEdit()        
        
        layout.addWidget(refreshP4Button)
        layout.addWidget(QtGui.QLabel("Take Name (leave empty for all takes):"))
        layout.addWidget(self._takeNameLineEdit)
        layout.addWidget(runButton)
        layout.addWidget(QtGui.QLabel("Output:"))
        layout.addWidget(self._outputLog)
        
        self.setLayout(layout)
        
        refreshP4Button.pressed.connect(self._handleP4Refresh)
        runButton.pressed.connect(self._handleRunButton)
        
    def _handleRunButton(self):
        # Get File Name
        fileName = mobu.FBApplication().FBXFileName
        
        takes = self._takeNameLineEdit.text()
        if takes == "":
            system = mobu.FBSystem()
            takes = [tk.Name for tk in system.Scene.Takes]
        else:
            takes = [takes]
        
        # Get Takes (from mobu or scene)
        outputString = ""
        for takeName in takes:
            output = exportPathManager.pathGenerator.resolvePath(fileName, takeName, exportPathManager.ExportPathTypes.AnimClip)
            if len(output) == 1:
                outputString += "Take: {0}\n\t{1}\n\n\n".format(takeName, output[0])
            else:
                outputString += "Take: {0}\n\tAudio Path:\n\t\tP4:{1}\n\t\tLocal:{2}\n\tAnim Path:\n\t\tP4:{3}\n\t\tLocal:{4}\n\tClip Path:\n\t\tP4:{5}\n\t\tLocal:{6}\n\n\n".format(takeName, output[0][0],output[0][1], output[1][0], output[1][1], output[2][0], output[2][1])
            
        self._outputLog.setPlainText(outputString)
            
    def _handleP4Refresh(self):
        exportPathManager.pathGenerator.fillP4Cahce()


@UI.Run(title="Audio Export Path Generator")
def Run(show=True):
    return AudioExportPathGeneratorWidget()
