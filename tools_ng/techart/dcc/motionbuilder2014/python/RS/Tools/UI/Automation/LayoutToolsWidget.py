from PySide import QtCore, QtGui
from RS.Tools.UI import Run

import RS.Core.Automation.Layout.LayoutTools as LayoutTools
import RS.Core.Audio.AudioTools as AudioTools
import RS.Core.Camera.CamUtils as CamUtils
import RS.Utils.ContentUpdate as ContentUpdate

from functools import partial

def RunCommand(function):
    function()
    QtGui.QMessageBox.information(None, "Notification", "Finished.")

@Run("Cutscene Layout Tools")
def ShowTool():
    wid = QtGui.QWidget()
    mainLayout = QtGui.QVBoxLayout()
    gridLayout = QtGui.QGridLayout()

    buttonData = [["Zero Cutscene", "Zeros a the current cutscene FBX.",  LayoutTools.ZeroCutscene],
                  ["Set Hard Ranges", "Sets the hard ranges to the slate keys.",  LayoutTools.SetSmartEndframe],
                  ["Setup Audio", "Syncs and adds audio to a cutscene FBX.",  AudioTools.SetupCutsceneAudio],
                  ["Add Toybox", "Adds a ToyBox object to the FBX.",  LayoutTools.SetupToybox],
                  ["Setup Export Cam", "Adds and plots an export cam.",  CamUtils.SetupExportCam],
                  ["Auto Populate Exporter", "Populates export data.",  LayoutTools.AutoPopulate],
                  ["Fix Missing Handles", "Fix missing handles data.",  LayoutTools.FixMissingHandles],
                  ["Update Handle File", "Adds missing handles to the handle file.",  LayoutTools.SetupHandleFile],
                  ["Update Cutscene Meta File", "Updates cutscene meta file with this scene.",  ContentUpdate.SetupGameContent],
                  ["Update Content Xml Files", "Updates content xml files with this scene.",  ContentUpdate.SetupToolsContent],
                  ["Set Exporter Shot", "Sets shot options in the exporter.",  LayoutTools.SetExporterShotData],
                  ["Set Scene Location", "Sets scene location in the exporter.",  LayoutTools.SetSceneLocation],
                  ["Auto Layout", "Runs all above steps automatically.",  LayoutTools.AutoLayout]]

    for index, eachButton in enumerate(buttonData):
        function = partial(RunCommand, eachButton[-1])
        newButton = QtGui.QPushButton(eachButton[0])
        newButton.setToolTip(eachButton[1])
        newButton.clicked.connect(function)

        if index < 6:
            newButton.setFixedWidth(150)
            gridLayout.addWidget(newButton, index, 0)
        elif index < 12:
            newButton.setFixedWidth(150)
            gridLayout.addWidget(newButton, index-6, 1)
        else:
            newButton.setFixedHeight(60)
            mainLayout.addLayout(gridLayout)
            mainLayout.addWidget(newButton)

    wid.setLayout(mainLayout)
    return wid

def Run():
    ShowTool()