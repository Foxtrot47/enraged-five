import sys
import os
import inspect
import clr
import time
import string


from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

from collections import OrderedDict

import RS.Config
import RS.Tools.AnimToFBX as anim2fbx
import RS.Utils.UserPreferences as userPref


import RS.Tools.UI
import RS.Utils.Creation

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory

#import Models classes
import RS.Tools.Models.DepotAnimListModel as animListModel
import RS.Tools.Models.DlcProjectListModel as dlcBranch
import RS.Tools.Models.SelectionListModel as selListModel
import RS.Tools.Models.SelectionSetModel as selSetModel

import RS.Tools.ModelViews.AnimToFBX as depotFileData

_Config = ConfigFactory.CreateConfig()

reload (animListModel)
reload (anim2fbx)
reload (dlcBranch)
reload (depotFileData)
reload (selSetModel)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)
gDevelopment = False

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\QT\\AnimToFBX_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\AnimToFBX_QT.py'.format(RS.Config.Script.Path.Root))
RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\QT\\AnimToFBXDataWidget_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\AnimToFBXDataWidget_QT.py'.format(RS.Config.Script.Path.Root))


import RS.Tools.UI.AnimToFBX_QT as anim2fbxQt
import RS.Tools.UI.AnimToFBXDataWidget_QT as mapper

reload(anim2fbxQt)
reload(mapper)




class progBar():
    def __init__(self, pBar, pBarLabel):
	self.__pBar = pBar
	self.__pBarLabel = pBarLabel
	
    def setValue(self, val):
	self.__pBar.setValue(val)
	
    def setVisible(self, vis):
	self.__pBar.setVisible(vis)
	self.__pBarLabel.setVisible(vis)
	
    def reset(self):
	self.__pBar.reset()
	self.__pBarLabel.setText('')
	
    def text(self, txt):
	self.__pBarLabel.setText(txt)
	
    def simProgress(self):

	numIterations = 10
	timeDelay = 1
	
	for i in range( numIterations ):
		
		# Determine a progress value.
		progress = 100 * ( i + 1 ) / numIterations
		
		self.__pBar.setValue(progress)
		
		time.sleep( timeDelay )        
	
	self.__pBar.setVisible(False) 
	
class DataMapperWidget( RS.Tools.UI.QtMainWindowBase,  mapper.Ui_Form ):
	def __init__(self, parent = None):
		RS.Tools.UI.QtMainWindowBase.__init__(self, parent = None)
		self.setupUi(self)
		
		self._dataMapper = QtGui.QDataWidgetMapper()
		
	def setModel(self, proxyModel):
		self._proxyModel = proxyModel
		try:
		    self._dataMapper.setModel(proxyModel.sourceModel())
		except:
		    self._dataMapper.setModel(proxyModel)	
		
		self._dataMapper.addMapping(self.lItemName, 0)
		self._dataMapper.addMapping(self.lItemPath, 2)	

	
	"""INPUTS: QModelIndex, QModelIndex"""
	def setSelection(self, current, old):
	    
	    current = self._proxyModel.mapToSource(current)
	    
	    parent = current.parent()
	    self._dataMapper.setRootIndex(parent)
	    
	    self._dataMapper.setCurrentModelIndex(current)
    
		

class AnimToFbxTool( RS.Tools.UI.QtMainWindowBase, anim2fbxQt.Ui_Form  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Anim to FBX')
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
	#dock the ui's to the MainWindowBase
	self._mapper = DataMapperWidget(self)	
	
	# Map them to the correct groupboxes
	self.lytDataMapper.addWidget(self._mapper) 	
	
        # Add the Rockstar Banner
        self._banner = RS.Tools.UI.BannerWidget(name = 'Anim to FBX', helpUrl ="https://rsgediwiki1/bob/index.php/Editing_and_converting_exported_.anim_files_back_into_FBX")
	self.lytBanner.addWidget(self._banner)
	
	
        self.treeLoadingProgress.setVisible(False)
        # Hide the Dev debug section if not in development mode
        if not gDevelopment:
            self.animSearch_hidden.setVisible(False)
            self.devOnly.setVisible(False)
            #self.selectedBranch_hidden.setVisible(False)
	    self.lImportOriginAndOffsetPos.setVisible(False)
	    self.lRunGimbleFilterButton.setVisible(False)

        # List of DLC Branches for project root (from configfactory assembly)
        self.projectList = self.getAnimsFolderList()

        self.selectionList = []     #List of paths and files to be imported  
	self.selSetDict	= {}	    #dicyionary of set names
	
        self.selectedItem = None    # selected item in the selection list
        self.treeItem = None        # selected item in the anim tree

        self._treeModel = None      # Model for treeView
        self._projModel = None      # Model for dlc drop down list
        self._selModel = None       # Model for selection list 
	self._selSetModel = None    # Model for Selection Set list	
	self._SetsRootnode = None   #Root node of self._selSetModel
	
	
	
	
	self._dlcBranch	= None	    # current dlcBranch

        #List of skeleton files in perforce depot
        self.skelList = None        

        #Facial/Prop/Camera import buttons
	self.lLoadCharButton.pressed.connect(self.browseAnims)
        self.lLoadFacialButton.pressed.connect(anim2fbx.rs_LoadFacialAnim)
        self.lLoadFacialFolderButton.pressed.connect(anim2fbx.rs_LoadFacialFolderAnims)
        self.lLoadPropButton.pressed.connect(anim2fbx.rs_LoadPropAnim)
        self.lLoadPropFolderButton.pressed.connect(anim2fbx.rs_LoadPropFolderAnims) 
        self.lLoadCameraAnimButton.pressed.connect(anim2fbx.rs_LoadCameraFileAnim)
        
        # Import Pose tab buttons
        self.lPoseAnimButton.pressed.connect(anim2fbx.rs_PoseFileAnim)
        self.lPoseAnimFolderButton.pressed.connect(anim2fbx.rs_PoseFolderAnims)  
        self.lFemalePoseAnimButton.pressed.connect(anim2fbx.rs_PoseFileAnim)
        self.lFemalePoseAnimFolderButton.pressed.connect(anim2fbx.rs_PoseFolderAnims) 

        # start filter
        self.filterNowButton.pressed.connect( self.applyFilterText )
        
        #expand and collapse tree buttons
        self.btnExpandAll.pressed.connect( self.expandTree )
        self.btnCollapseAll.pressed.connect( self.collapseTree )
        
        # add/remove to Selection List
        self.addToSelectionButton.pressed.connect(self.addToList)
        self.removeFromSelectionButton.pressed.connect(self.removeFromList)
        self.clearList.pressed.connect(self.clearSelectionList)
        
        # Import buttons
        self.lLoadSelectionList.pressed.connect(self.lLoadSelection)
        self.lLoadSelected.pressed.connect(self.LoadAnim)
	
	# Selection set buttons
	
	self.lSaveSelSetList.pressed.connect(self.SaveSelList)
	self.lLoadSelSetList.pressed.connect(self.LoadSelList)
	self.lDelSelSetList.pressed.connect(self.DeleteSelectionSet)
	self.lDeleteAllSets.pressed.connect(self.DeleteAllSetNodes)
        
        # Gimbal checkbox
        self.lRunGimbleFilterButton.pressed.connect(self.SettingCustomGimbalData) 

        # setting the source models to their views
        self.connectDlcModelToView()
        self.selList = self.connectSelModelToSelView()


        # create the model based on dlc selection and setting the model to the tree view
        self.lDlcCombo.activated[str].connect(self.connectAnimListModelToView)

        #setting the treeView QTreeView Selection Mode and Behavior
        self.treeView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)
        self.treeView.setSelectionBehavior(QtGui.QTreeView.SelectRows)
	
	#setting the selectionlistview SelectionMode
	self.selectionListView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)
        
        # Populate the skeleton combo box
        self.LoadSkels()
	
	# Create the Selection Set Model and set to view
	self.CreateSelectionSetModel()
	
	#Load Selection sets from mbtools.ini
	self.LoadSelSets()
	

	
#=====================================================================#
# Selection Set Methods
#=====================================================================#	
    def CreateSelectionSetModel(self):
	# Create a Selection Set Model instance
	self._SetsRootnode = selSetModel.SelectionSetNode("Root")
	
	#Create a Root Node
	self._selSetModel = selSetModel.SelectionSetModel(self._SetsRootnode)
	
	#Set the Model on the Tree View
	self.SetListView.setModel(self._selSetModel)
	

	return	
	
    def SaveSelSet(self):
	# Delete all sets first
	self.DeleteHistory(False)
	
	# Resave all sets
	self.SaveSetHistory()
	
    
    def SaveSetHistory(self):
	
	#Get number of sets from model root childcount
	numSets = self._SetsRootnode.childCount()
	
	# print the section and setcount
	userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName, "setcount", numSets)
	
	# Loop through children
	for i in range(numSets):
	    
	    #Get the child node (list Item)
	    node = self._SetsRootnode.child(i)
	    
	    # print the set alias (i.e. [AnimToFbx Selection Sets_1])
	    userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listcount", node.childCount())
	    
	    # print the setname
	    userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "setname", node.name())
	    
	    # print the branch
	    userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "branch", node.branch())
	    
	    # Get the Set Node child count
	    nodeChildCount = node.childCount()
	    
	    # Loop through items (setNode children)
	    for j in range(nodeChildCount):

		#create the listitem data
		lItemText = node.child(j).name() + "|" + node.child(j).typeInfo() + "|" + node.child(j).path()
		    
		# print the list item alias (listitem_1)
		userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listitem_" + str(j), lItemText)
    
    def LoadSelList(self):
	# get current index from the selection list (same as delete set)
	setNode = self._selSetModel.getNode(self.SetListView.currentIndex())
		
	# check if it is typeInfo "Selection Set"
	# if it is not, get the parent, and check if it is typeInfo "Selection Set"
	while setNode.typeInfo() != "Selection Set":
	    setNode = setNode.parent()	
	    
	# create new SelectionListModel
	self.connectSelModelToSelView()	


	# get the childcount of the set node
	childcount = setNode.childCount()
	
	#create a nodeList
	nodeList = []
	
	# for each childnode, create a slectionlist node
	for i in range(childcount):
	    # Get number of rows, should be 0
	    rows = self._selModel.rowCount()
	    
	    # Get the child node from the selection set children
	    node = setNode.child(i)
	    
	    # use the info to create a new node and add it to the list	    
	    if node.typeInfo() == "File Folder":
		selNode = selListModel.animFolderSelectionNode(node.name(), nodePath = node.path())
	    elif node.typeInfo() == "anim depot File":
		selNode = selListModel.animFileSelectionNode(node.name(), nodePath = node.path())
		
	    self._selModel.insertRows(rows, 1, node)
	    
	# load the branch
	test = self._dlcBranch
	if test != setNode.branch():
	    self.connectAnimListModelToView(setNode.branch())
	
	
    def SaveSelList(self):
	# get the number of items in the selection list, and return if 0
	numItems = self._selModel.rowCount()
	
	# get the number of sets from ini
	setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue = True))
	
	# ask for name of set
	setname = depotFileData.GetSelectionSetNameFromUser()
	
	# check the name is not a dup
	
	# create a node for the set Node and parent to the rootnode
	selSetNode = selSetModel.SelectionSetNode(setname, animBranch = self._dlcBranch)
	
	# insert to the model
	self._selSetModel.insertRows(self._SetsRootnode.childCount(), 1, selSetNode)
	
	# get a list of the nodes
	nodeList = self._selModel.getList()
	
	# Loop through list Items
	for i in range(len(nodeList)):
	    # create a node for each item, and parent to the setNode
	    if nodeList[i].typeInfo() == "File Folder":
		node = selSetModel.SelSetFolderNode(nodeList[i].name(), selSetNode, nodeList[i].path())
	    elif nodeList[i].typeInfo() == "anim depot File":
		node = selSetModel.SelSetFileNode(nodeList[i].name(), selSetNode, nodeList[i].path())
	
	# saveSelSet()
	self.SaveSelSet()
	
	self.adjustSelectionSetsColumnwidth()
    
    def DeleteSelectionSet(self):
	# Get the current selected item from the tree
	setNode = self._selSetModel.getNode(self.SetListView.currentIndex())
		
	# check if it is typeInfo "Selection Set"
	# if it is not, get the parent, and check if it is typeInfo "Selection Set"
	while setNode.typeInfo() != "Selection Set":
	    setNode = setNode.parent()	    	
	
	# Messagebox confirmation
	result = FBMessageBox("Delete Set Confirmation", "Are you sure?", "Delete Set", "Cancel")
	
	if result == 2:
	    return
	
	# remove node
	row = setNode.row()
	self._selSetModel.removeRows(row, 1)	                  
	
	# saveSelSet()
	self.SaveSelSet()
	
	
    
    def LoadSelSets(self):
	
	#Read in the # of Selection Sets
	setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue = True))
	
	#Loop Through each selection set
	for i in range(setCount):
	
	    # Read in the selection set branch
	    branch = str(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "branch", returnValue = True))
	    
	    # iS THE SELECTION SET FROM A VALID BRANCH IN THE CONFIG
	    found = False
	    dlcList = _Config.AllProjects()
	    for dlc in dlcList.Values:
		if branch == dlc.get_Name() or branch == 'rdr3 old location':	
		    found = True	    
	    
	    if found:
		# Get the Selection Set Name 
		setname = userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "setname", returnValue = True)
		
		# Gte the selection set listcount
		listCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listcount", returnValue = True))
		
		# Create a SelectionSetNode
		selSetRoot = selSetModel.SelectionSetNode(setname, self._SetsRootnode, animBranch = branch)
		
		# Loop through list Items
		for j in range(listCount):
		
		    # Read in the list item
		    item = userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listitem_" + str(j), returnValue = True)
		    itemDict = item.split("|")		    

		    # Create a listitemNode for each item
		    if itemDict[1] == "File Folder":
			node = selSetModel.SelSetFolderNode(itemDict[0], parent = selSetRoot, nodePath = itemDict[2])
		    elif itemDict[1] == "anim depot File":
			node = selSetModel.SelSetFileNode(itemDict[0], parent = selSetRoot, nodePath = itemDict[2])
			
	self.adjustSelectionSetsColumnwidth()
	

	
    def DeleteHistory(self, warn = True):
	if warn:
	    #Prompt a warning before deleting all
	    result = FBMessageBox("Confirm Deleting All Selection Sets", "Are you sure?", "Delete All", "Cancel")
	else:
	    result = 1
	
	if result == 1:
	    #Read in the # of Selection Sets
	    setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue = True))
	    
	    #Loop Through each selection set
	    for i in range(setCount):
		
		#Delete section, only if exists in branch
		
		userPref.__RemoveSection__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i))
	return result
		
    def DeleteAllSetNodes(self, delete = True):	
	# Delete all sets first
	delete = self.DeleteHistory(True)
	
	if delete == 1:
	    # Re-create model and rootnode
	    self.CreateSelectionSetModel()	
	    
	    userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName, "setcount", str(0))
	    
   
#=====================================================================#
# 
#=====================================================================#	
    
    def checkForCharacter(self):
        lApp = FBApplication()
        lCurrentCharacter = lApp.CurrentCharacter
        
        if not lCurrentCharacter:
	    return False
	return True
    
    def lLoadSelection(self):    
	# Add a loading bar
	if self.checkForCharacter():
	    mList = self._selModel.getList()
	    
	    if self.useSkelCheckbox.isChecked():
		skelFile = str(self.useSkelCombo.currentText())
	    else:
		skelFile = None
   
	    if len(mList):
		for path in mList:
		    if path.typeInfo() == u"File Folder":
			anim2fbx.rs_LoadFolder(path.path(), skelFile, self.syncCheckbox.isChecked())
		    else:
			anim2fbx.rs_LoadFile(path.path(), skelFile, self.syncCheckbox.isChecked())
	else:
	    FBMessageBox( "Error", "No Character is selected in the scene. Please select one in the Character Controls, and try again!", "OK")
 



    def clearSelectionList(self):
        mList = self._selModel.getList()

        if len(mList):
            self._selModel.removeRows(0, len(mList))

    def LoadSkels(self):

        self.skelList = depotFileData.getDepotSkelFiles()

        skelIndex = 0
        defPlayerSkelIndex  = 0
	defBipedSkelIndex = 0
	defSkelIndex = 0
        for skelFile in self.skelList:
            self.useSkelCombo.addItem(skelFile)
            if skelFile == 'biped.skel':
                defBipedSkelIndex = skelIndex
            elif skelFile == 'player.skel':
                defPlayerSkelIndex = skelIndex
            skelIndex += 1
	if defBipedSkelIndex:
	    defSkelIndex = defBipedSkelIndex
	elif defPlayerSkelIndex:
	    defSkelIndex = defPlayerSkelIndex
	self.useSkelCombo.setCurrentIndex(defSkelIndex)   
	
    def browseAnims(self):
	lFilePopup = FBFilePopup()
	lFilePopup.Filter = '*.anim'
	lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
	lFilePopup.Path = "{0}".format( RS.Config.Tool.Path.Root )
	
	gFileName = ""
	if lFilePopup.Execute():
	    gFileName = lFilePopup.FullFilename
	    self.LoadAnim(gFileName)
	else:
	    return
	


    def LoadAnim(self, singleAnim = None):
	
	# get the state of the cbImportToCurrentTake
	cbUseTake= self.cbImportToCurrentTake.isChecked()
        #what is the name, typeinfo , and path of selected
	if self.checkForCharacter():	
	    if singleAnim:
		
		if self.useSkelCheckbox.isChecked():
			skelFile = str(self.useSkelCombo.currentText())
		else:
		    skelFile = None
		    
		anim2fbx.rs_LoadFile(singleAnim, skelFile, self.syncCheckbox.isChecked(), useTake = self.cbImportToCurrentTake.isChecked())
	    else:
		listIndexes = self.treeView.selectedIndexes()
		
		if len(listIndexes) > 1:
		    cbUseTake = False
		
		for selection in listIndexes:
		    item = self._proxyModel.mapToSource(selection)
		    #mitem = self._proxyModel.getNode(selection)
		    if self.useSkelCheckbox.isChecked():
			skelFile = str(self.useSkelCombo.currentText())
		    else:
			skelFile = None		
    
		    if item.data(33) == u"File Folder":
			anim2fbx.rs_LoadFolder(item.data(34), skelFile, self.syncCheckbox.isChecked())
		    else:
			anim2fbx.rs_LoadFile(item.data(34), skelFile, self.syncCheckbox.isChecked(), useTake = cbUseTake)		

	else:
	    FBMessageBox( "Error", "No Character is selected in the scene. Please select one in the Character Controls, and try again!", "OK")
	
    def addToList(self):
	# Get the tree selections
	listIndexes = self.treeView.selectedIndexes()
	removeList = []
	appendList = {}	# needs to be a key dictionary
	# loop through the tree selections
	for selection in listIndexes: # selection is a LeafFilterProxyModel
	    
	    appendList.update({selection:True})
	    
	    # Get the source model QModelIndex of the selected item (instead of the proxymodel QModelIndex)
	    index = self._proxyModel.mapToSource(selection) # selection is a DepotAnimListModel
	    
	    rowNum = 0

	   # Get the current selection list
	    for listItem in self._selModel.getList():
		
		# is selection is the same as item in list
		if listItem.path() == index.data(34):
		    FBMessageBox("Message", "{0} is already in your list".format(index.data(32)), "OK")
		    appendList[selection] = False
		    rowNum += 1
		    #continue
		
		# selection is a child of the folder? this one needs work
		elif listItem.path() in index.data(34):
		    lTempList = index.data(34).replace(listItem.path(), "").split("/")
		    
		    # looking for length 2 because the first item will be  u""
		    if len(lTempList)==2:
			
			lFileName = str(listItem.path())	    
			liName = lFileName.replace('%40','@')		    
    
			choice = FBMessageBox("Warning", "{0} is already included in the selection list under the\n{1} folder".format(str(lTempList[1]), liName), "Add anyway", "Skip")

			if choice == 2:
			    appendList[selection] = False
			    rowNum += 1
			    #continue
			    
		# selected item is a folder and an item in the list is a child (anim file)
		elif listItem.typeInfo() != "File Folder" and index.data(34) in listItem.path():
		    lFileName = str(index.data(32))	    
		    iName = lFileName.replace('%40','@')		
		    choice = FBMessageBox("Message", "{1} is in the \n{0} folder. ".format(iName, listItem.name()), "Remove Anim", "Do Not Remove Anim")
		    
		    if choice == 1:
			# remove listItem
			removeList.append(rowNum)
			rowNum += 1
			#continue
		else:
		    rowNum += 1
		    #continue
		
	#remove all items flagged for remove	
	if len(removeList)>0:
	    for i in range(len(removeList)-1, -1, -1):
		self._selModel.removeRows(removeList[i], 1)		
	
	# add all items flagged for add
	if len(appendList)>0:
	    for k, v in appendList.items():
		if v == True:
		    index = self._proxyModel.mapToSource(k)
		    rows = self._selModel.rowCount()
		    if index.data(33) == "File Folder":
				
			node = selListModel.animFolderSelectionNode(index.data(32), index.data(33), index.data(34))
			
		    elif index.data(33) == "anim depot File":
			
			node = selListModel.animFileSelectionNode(index.data(32), index.data(33), index.data(34))
	    
		    else:
			node = selListModel.animFileWorkspaceSelectionNode(index.data(32), index.data(33), index.data(34))
	    
		    self._selModel.insertRows(rows, 1, node)		
    
    def removeFromList(self):
	removalDict = {}
	listIndexes = self.selectionListView.selectedIndexes()
        #row = 0
        lList = self._selModel.getList()
        
        for selection in listIndexes: # selection is a SelectionListModel    
        # Go through the list , looking for the matching name
	    row = 0

	    for node in lList:
    
		if node.name() == selection.data():
		    removalDict.update({selection:row})
		    break
		else:
		    row += 1
		    
	#sort dict by value
	sorted_removalDict = OrderedDict(sorted(removalDict.items(), key=lambda x: x[1], reverse = True))
	#remove here
	for k, v in sorted_removalDict.items():
	    self._selModel.removeRows(v,1)


    def connectAnimListModelToView(self, index):
        #Create the Data List
        if index == u'Select a Branch to Load Character Animation From:':
            return
        
        #setting up loading bar from QtDesigner ui
	self.handleProgress = progBar(self.treeLoadingProgress, self.lblLoadingBar)
	self.handleProgress.reset()
	self.handleProgress.setVisible(True)
	#self.handleProgress.simProgress()	
	
        rootNode = depotFileData.setupModelData( unicode(index), self.handleProgress)
	self._dlcBranch = str(index)
	
	if rootNode:
	    self._treeModel = animListModel.DepotAnimListModel(rootNode)
	    self._treeModel.setbranchRootPath( unicode(index))
    
	    """ VIEW <---------> PROXY MODEL <----------> DATA MODEL"""
	    ##proxy model for filtering
	    self._proxyModel = animListModel.LeafFilterProxyModel()	
    
	    self._proxyModel.setSourceModel(self._treeModel)	
    
	    self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
	    self._proxyModel.setFilterRole(animListModel.DepotAnimListModel.filterRole)
    
	    self.treeView.setModel(self._proxyModel)
    
	    QtCore.QObject.connect(self.animSearch_hidden, QtCore.SIGNAL ("textChanged(QString)"), self._proxyModel.setFilterRegExp)
    
	    self.treeView.resizeColumnToContents(0)
	    self.treeView.setColumnWidth(1, 100)
	    
	    self._mapper.setModel(self._proxyModel)
	    
	    QtCore.QObject.connect(self.treeView.selectionModel(), QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._mapper.setSelection)

    def connectDlcModelToView(self):

        self._projModel = dlcBranch.DlcProjectListModel(self.projectList)

        self.lDlcCombo.setModel(self._projModel)
	if len(self.projectList)== 2:
	    self.connectAnimListModelToView(self.projectList[1])
	    

    def connectSelModelToSelView(self):

        self.nodeList = []
        self._selModel = selListModel.SelectionListModel(self.nodeList)

        self.selectionListView.setModel(self._selModel)	
	
	#self._mapper.setModel(self._selModel)
	
	#QtCore.QObject.connect(self.selectionListView.selectionModel(), QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._mapper.setSelection)	
	

    def applyFilterText(self):
	self.collapseTree()
        # I am sure there is a better and faster way to filter, but for now, this is the best solution
        self.animSearch_hidden.setText(self.animSearch.text())

    def expandTree(self):
        self.treeView.expandAll()
    
    def collapseTree(self):
        self.treeView.collapseAll()


    def SettingCustomGimbalData( self, pControl, pEvent ): 
        pControl.LoadAnimButton.TextureData = pControl.State   
        pControl.LoadAnimFolderButton.TextureData = pControl.State


    def getAnimsFolderList(self):

        self.projectList = ["Select a Branch to Load Character Animation From:"]
	if gProjRoot == "x:\\rdr3":
	    self.projectList.append("rdr3 old location")
        # Get a list of the current local project branches
	
        dlcList = _Config.AllProjects()
        for dlc in dlcList.Values:
            name = "%s" % (str(dlc.get_Name()))
            self.projectList.append(name)

        return self.projectList  
    def adjustSelectionSetsColumnwidth(self):
	self.SetListView.expandAll()
	width = self.SetListView.sizeHintForColumn(0) + 5
	self.SetListView.setColumnWidth(0, width)
	self.SetListView.collapseAll()	
	
def Run( show = True ):
    if gDevelopment:
        import RS.Tools.UI.Commands as cmds
        cmds.ConnectToWing()

    tool = AnimToFbxTool()

    if show:
        tool.show()

    return tool