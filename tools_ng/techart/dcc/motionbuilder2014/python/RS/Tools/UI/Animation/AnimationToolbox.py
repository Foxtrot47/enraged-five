from pyfbsdk import *
from pyfbsdk_additions import *

import inspect

import RS.Tools.UI
import RS.Core.Animation.Lib as anim
import RS.Utils.Creation

reload (anim)


class AnimationToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Cutscene Facial Toolbox", size = [ 200, 250 ] )
        

    def Create( self, getCharsLyt ):
        
        # The object to search for in the scene to find cutscene characters
        idControl = "SKEL_ROOT"
        
        # GUI Element Shared Properties
        lButtonLook = FBButtonLook.kFBLookNormal
        
        # The main Horizontal FBox Layout
        lytHoriz = FBHBoxLayout()
        x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
        getCharsLyt.AddRegion("main","main", x,y,w,h)
        getCharsLyt.SetControl("main",lytHoriz)
        
        # The main Vertical FBox Layout
        lyt = FBVBoxLayout()
        lytHoriz.Add(lyt,175)
        
        # LABEL - Cutscene Characters
        label1 = FBLabel()
        label1.Caption = "Select a Cutscene Character:"
        lyt.Add(label1,20)
        
        # List creation
        global ctrlsList
        ctrlsList = FBList()
        ctrlsList.Style = FBListStyle.kFBVerticalList
        ctrlsList.MultiSelect = False
        lyt.Add(ctrlsList, 100)
        
        # fill the list with data
        lChars = []
        
        lCharCtrls = FBComponentList()
        FBFindObjectsByName(idControl, lCharCtrls,False, False)
        for iEach in lCharCtrls :
            lName = iEach.LongName.split(":",1)
            lChars.append(lName[0])
            
        for each in lChars :
            ctrlsList.Items.append(each)
        ctrlsList.Selected(0, True)
        
        
        # Button -- Refresh Tool
        but_close = FBButton()
        but_close.Caption = "^ Refresh Character List ^"
        but_close.Hint = "Update the list above"
        but_close.Look = lButtonLook
        lyt.Add(but_close,20)
        
        # Spacer
        spacer_00 = FBLabel()
        lyt.Add(spacer_00,1)
        
        # Button -- Toggle Thumb Joint Orientation
        but_toggleThumbs = FBButton()
        but_toggleThumbs.Caption = "Toggle Thumb Joint Orientation"
        but_toggleThumbs.Hint = "This will toggle between the two thumb orientation settings used on V (IG vs CS)."
        but_toggleThumbs.Look = lButtonLook
        lyt.Add(but_toggleThumbs,25)
        #but_toggleThumbs.Enabled = False
        
        # Spacer
        spacer_01 = FBLabel()
        lyt.Add(spacer_01,1)
        
        # Spacer
        spacer_02 = FBLabel()
        lyt.Add(spacer_02,1)
        
        # Spacer
        spacer_03 = FBLabel()
        lyt.Add(spacer_03,1)
        
        # Spacer
        spacer_04 = FBLabel()
        lyt.Add(spacer_04,1)
        
            
        #-------------#
        # Definitions #
        #-------------#
        
        
        # On CLick - Refresh Toolbox
        def but_close_Clicked (control, event) :
            CloseTool(getCharsLyt)
            tool = AnimationToolbox()
            tool.Show()
            
        but_close.OnClick.Add(but_close_Clicked)
        
            
        # On Click - Fix Lumpy Face
        def but_toggleThumbs_Clicked( control, event ):
            tNamespace = ctrlsList.Items[ctrlsList.ItemIndex]
            if tNamespace :
                anim.ThumbJointOrientationToggle( tNamespace )
            
        but_toggleThumbs.OnClick.Add( but_toggleThumbs_Clicked )
        
            

def Run( show = True ):
    global tool
    tool = AnimationToolbox()
    
    if show:
        tool.Show()
        
    return tool
