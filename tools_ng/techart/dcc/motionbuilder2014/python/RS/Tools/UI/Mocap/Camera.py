"""
Description:
    UI for tool to edit cameras lenses while on set

Authors:
    David Vega <david.vega@rockstargames.com>
"""

from PySide import QtGui, QtCore

import RS.Globals
import RS.Tools.UI
import RS.Core.Mocap.MocapCommands
from RS.Tools.UI import Application
from functools import partial


class MocapCamera(RS.Tools.UI.QtBannerWindowBase):

    _LensValues = {16: 69.98,
                   25: 47.85,
                   35: 36.99,
                   50: 25.24,
                   75: 17.10,
                   120: 10.00}

    def __init__(self, parent=None, *args, **kwargs):
        if not parent:
            parent = RS.Tools.UI.RS.Tools.UI.MotionBuilderWindow()

        super(MocapCamera, self).__init__(parent, *args, **kwargs)
        self.__originalSize = True
        self.__tabletMode = False

        policy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.infoField = QtGui.QLineEdit()
        self.infoField.setAlignment(QtCore.Qt.AlignCenter)
        self.infoField.setEnabled(False)
        self._mainLayout.addWidget(self.infoField)

        self.bakeButton = QtGui.QPushButton("Set Camera")
        self.bakeButton.setSizePolicy(policy)
        self.bakeButton.clicked.connect(self.setCamera)
        self._mainLayout.addWidget(self.bakeButton)
        self._mainLayout.setSpacing(0)
        self.setMinimumWidth(300)
        for index, each in enumerate([16, 25, 35, 50, 75, 120]):

            button = QtGui.QPushButton()
            button.setText( "{}mm Lens".format(each))
            button.setSizePolicy(policy)
            button.clicked.connect(partial(self.SetLens, each))
            self._mainLayout.addWidget(button)

            setattr(self, "{}mmLensButton".format(each), button)

        self.setStyleSheet("""
             QLineEdit{
                color: white;
                }
             QPushButton{
             background-color: #2d2d30;
             font: bold 12px;}
                           """)
        RS.Core.Mocap.MocapCommands.RockCamTabCheck()
        Application.AddShortCut("shift+x", self.ToggleTabletMode)
        Application.AddShortCut("shift+c", self.FullScreen)

    def setCamera(self):
        try:
            camera = RS.Core.Mocap.MocapCommands.CamRockBake(False, False)
            info = "{} has been created".format(camera.Name)
        except:
            info = "No camera created"

        finally:
            self.infoField.setText(info)

    def SetLens(self, value):
        """
        Sets the custom lens camera for selected cameras that have the Lens Property

        Argument:
            value = int; numbers 0 through 5, the lens attribute is an enum with the following values corresponding
                         to a lens size, 0 = 16, 1 = 25 , 2=35, 3=50, 4=75, 5=120.
        """

        if self.CamRock: self.CamRock.PropertyList.Find("Field Of View").Data = self._LensValues[value]

    def ToggleTabletMode(self):
        self.SetTabletMode(bool(self.__tabletMode - 1))

    def SetTabletMode(self, onOff):

        self.__tabletMode = onOff

        mainwindow = Application.GetMainWindow()

        showHide = ["show", "hide"]

        windowMode = [QtCore.Qt.Window, QtCore.Qt.FramelessWindowHint]

        mainwindow.setWindowFlags(windowMode[onOff])
        mainwindow.show()

        self.__originalSize = False
        self.FullScreen()

        [getattr(child, showHide[onOff])() for child in mainwindow.children()
         if hasattr(child, showHide[onOff]) and getattr(child, "windowTitle", lambda *args: "")() not in
         [self.windowTitle(), "Viewer", "Python Editor"]]


    def FullScreen(self):
        if not self.__tabletMode:
            return

        mainwindow = Application.GetMainWindow()
        viewer = Application.GetChildrenByWindowTitle(mainwindow, "Viewer")[0]
        children = Application.GetChildrenByClass(viewer, QtGui.QWidget, True)[0]
        displayParent = children.children()[0].children()[0]
        size = viewer.size()

        if self.__originalSize:
            viewer.move(0, -52)
            viewer.resize(size.width() * 1.04, size.height() * 1.1)
            [child.hide() for child in displayParent.children()
             if hasattr(child, "hide") and getattr(child, "windowTitle", lambda *args: "")() not in ["KtOpenGL"]]

        else:
            viewer.move(0, 0)
            viewer.resize(size.width() * (1/1.04), size.height() * (1/1.1))
            [child.show() for child in displayParent.children()
             if hasattr(child, "show") and getattr(child, "windowTitle", lambda *args: "")() not in ["KtOpenGL"]]

        self.__originalSize = bool(self.__originalSize-1)

        return True

    def closeEvent(self, event):
        Application.RemoveShortCut("shift+x")
        Application.RemoveShortCut("shift+c")

    @property
    def CamRock(self):
        camera = filter(lambda camera: camera.Name == "CamRockTab_cam", RS.Globals.gCameras)
        if camera:
            return camera[0]


def Run(show=True):

    tool = MocapCamera(title=__name__.split(".")[-1], size=[174, 400], dockable=True, dock=True,
                       dockArea=QtCore.Qt.RightDockWidgetArea)
    if show:
        try:
            tool.show_data()
        except Exception, e:
            #print_this(e)
            pass
        tool.show()
    return tool