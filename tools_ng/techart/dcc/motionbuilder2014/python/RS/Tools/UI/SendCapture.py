__author__ = 'mharrison-ball'

'''
Capture Framework Testing

Prequeistes:
Sync latest on
a.    //depot/gta5/tools_ng/script/automation/
b.    //depot/gta5/tools_ng/ironlib/lib/
c.    //depot/gta5/tools_ng/etc/automation/

Batch File
x:\gta5\tools_ng\script\automation\FrameCapture - Console.bat

Add extra commands for Rag here
x:\gta5\tools_ng\etc\automation\rag_scripts\CutSceneExtras.meta

Usage:
x:\gta5\tools_ng\ironlib\lib\RSG.Pipeline.Automation.Console.exe --host EDIW-CAPTREND --nopopups --script X:\gta5\tools_ng\script\automation\scene_script.txt

ScriptFile:
frame_capture x:\gta5_dlc\mpPacks\mpHeist\art\animation\cutscene\!!scenes\CHICKEN_HEIST\CHICKEN_HEIST_INT\CHICKEN_HEIST_INT_T07.fbx
'''

import subprocess, tempfile
import RS.Config
from pyfbsdk import *


EXE = "RSG.Pipeline.Automation.Console.exe"
ARG = "--host EDIW-CAPTREND --nopopups --script"

DEFAULT_SETTINGS_PATH = tempfile.gettempdir() + "\\captureTemp.txt"



def writeScript(mFile):
    f = open(DEFAULT_SETTINGS_PATH, 'w')
    f.write(mFile)
    f.close()

    ''''''

def runCommand():
    exe = "{0}\\ironlib\\lib\\{1}".format(RS.Config.Tool.Path.Root, EXE)
    arg = "{0} {1}".format(ARG, DEFAULT_SETTINGS_PATH)
    cmd = "{0} {1}".format(exe, arg)
    temp=subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)


def sendCapture():
    if FBApplication().FBXFileName != '':
    
        string = "frame_capture {0}".format(FBApplication().FBXFileName)
        # get Scenname
        writeScript(string);
        # Run our script command
        runCommand()
        lMessage = "Capture has been sent"
        FBMessageBox( "Message", lMessage, "OK", None, None )


def Run(  ):
    sendCapture()
    

   




