import RS.Config
from pyfbsdk import *

import RS.Core.EST.estEdit

def validateUser():
    validUsers = ['blake.buck@rockstargames.com', 'mark.harrison-ball@rockstargames.com', 'joe.rubino@rockstargames.com',
                    'gethin.aldous@rockstargames.com', 'luke.howard@rockstargames.com', 'forrest.karbowski@rockstargames.com',
                    'perry.katz@rockstargames.com', 'jason.barnes@rockstargames.com', 'rod.edge@rockstargames.com',
                    'wilmar.luna@rockstargames.com', 'geoffrey.fermin@rockstarnorth.com']
    userEmail = RS.Config.User.Email.lower()
    if userEmail in validUsers:
        return "VALID"
    else:
        return "INVALID"

def Run():
    #VALIDATE USER - ONLY CAM TEAM MEMBERS SUPPORTED CURRENTLY
    # userStatus = validateUser()
    # if userStatus != "VALID":
        # FBMessageBox( "EST Script", "The EST currently breaks autoclip names for lighting, and has been temporarily disabled.\n\n\tContact blake.buck@rockstargames.com for more info.", "OK" )
    # else:
        # #RUN SCRIPT FOR VALIDATED USER
        # RS.Core.EST.estEdit.genShotTrack()
    RS.Core.EST.estEdit.genShotTrack()