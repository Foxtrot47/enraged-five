"""
Description:
    UI for visually displaying features, tools and commands that artists can set for our shelves and hotkeys

Author:
    David Vega <david.vega@rockstargames.com>
"""

import os
import xml.etree.cElementTree as xml
from functools import partial

from PySide import QtGui

from RS import Config
from RS.Tools.UI import Run
from RS.Tools.UI.FeaturePicker.Widget import DescriptionWidget, ToolIconWidget, DisplayWidget


class FeaturePicker(QtGui.QWidget):
    """
    UI that displays information about the menu items under the Rockstar Menu so they can be drag-and-dropped into
    the R* shelf and hotkey editor
    """
    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(FeaturePicker, self).__init__(parent)
        layout = QtGui.QHBoxLayout()
        tab = QtGui.QTabWidget()

        self._InformationWidget = DescriptionWidget.Description()

        [tab.addTab(display, display.windowTitle()) for display in self.ReadRockstarMenu()]
        layout.addWidget(tab)
        layout.addWidget(self._InformationWidget)

        tab.setTabPosition(QtGui.QTabWidget.East)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

    def ReadRockstarMenu(self):
        """ Reads the menu.config xml and populates the UI with buttons that represent its contents """
        menuXmlPath = os.path.join(os.path.dirname(Config.Script.Path.RockstarRoot), "menu.config")
        menuTree = xml.parse(menuXmlPath)
        menuRoot = menuTree.getroot()

        displayWidgets = [DisplayWidget.Display()]
        displayWidgets[0].setWindowTitle("Rockstar")

        for child in menuRoot:
            if "sub" in child.tag:
                displayWidgets.append(DisplayWidget.Display())
                displayWidgets[-1].setWindowTitle(child.attrib['displayName'])
                [displayWidgets[-1].addWidget(icon) for icon in self.BuildSubMenuDisplay(child)]

            else:
                icon = ToolIconWidget.ToolIcon()
                icon.storeMenuInformation(child)
                icon.pressed.connect(partial(self.UpdateDescriptionWidget, icon))
                displayWidgets[0].addWidget(icon)

        return displayWidgets

    def BuildSubMenuDisplay(self, element):
        """
        Builds the icons for each tab in the Feature Picker

        Arguments:
            element (xml.etree.elementTree.Element): element from the menu.config xml

        Return:
            list[RS.Tools.UI.FeaturePicker.Widget.ToolsIconWidget.ToolsIcon, etc.]
        """
        icons = []
        for child in element:
            if not list(child):
                icons.append(ToolIconWidget.ToolIcon())
                icons[-1].storeMenuInformation(child)

                icons[-1].pressed.connect(partial(self.UpdateDescriptionWidget, icons[-1]))
            icons.extend(self.BuildSubMenuDisplay(child))

        return icons

    def UpdateDescriptionWidget(self, toolIcon):
        """
        Updates the content of the description based on the selected button

        Arguments:
            toolIcon (RS.Tools.UI.FeaturePicker.Widget.ToolsIconWidget.ToolsIcon): the button to get the information
                from.

        """
        self._InformationWidget.setToolTitle(toolIcon.title)
        self._InformationWidget.setDescription(toolIcon.description)
        self._InformationWidget.setIcon(toolIcon.imagePath)
        self._InformationWidget.setModule(toolIcon.module)
        self._InformationWidget.setRevision(toolIcon.revision)

        nameParts = toolIcon.module.split(".")
        nameParts[-1] = "{}.py".format(nameParts[-1])
        self._InformationWidget.PerforceWidget.setPath(os.path.join(Config.Script.Path.RockstarRoot, *nameParts[1:]))
        self._InformationWidget.PerforceWidget.setChangelist(toolIcon.changelist)
        self._InformationWidget.PerforceWidget.setChangelistDescription(toolIcon.changelistDescription)
        self._InformationWidget.PerforceWidget.setDate(toolIcon.date)
        self._InformationWidget.PerforceWidget.setContributors(*toolIcon.contributors)
        self._InformationWidget.PerforceWidget.setDependencies(*toolIcon.dependencies)
        self._InformationWidget.setIconText(["", "No Icon"][toolIcon.imagePath.endswith("default.png")])


@Run(title="Feature Picker", size=(500, 500))
def Run():
    """
    Shows the Feature Picker

    Return:
        RS.Tools.UI.FeaturePicker.Widget.FeaturePickerWidget.FeaturePicker()
    """
    return FeaturePicker()
