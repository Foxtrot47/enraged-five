"""
Description:
    UI for setting up Moving Cutscene Setup

Author:
    David Vega <david.vega@rockstargames.com>
"""
import os

import pyfbsdk as mobu

from PySide import QtGui

from RS import Globals
from RS.Utils import LocalSettings
from RS.Tools import MovingCutscene
from RS.Tools.UI import Run
from RS.Core.Animation import Lib
from RS.Core.Scene.models import SceneMonitor
from RS.Core.Scene.widgets import ComponentView, ComponentTextField


ROTATION_FILTERS = {"None": mobu.FBRotationFilter.kFBRotationFilterNone,
                    "Gimble Killer": mobu.FBRotationFilter.kFBRotationFilterGimbleKiller,
                    "Unroll": mobu.FBRotationFilter.kFBRotationFilterUnroll}


SCENE_MONITOR = SceneMonitor.SceneMonitor()


class SetupWidget(QtGui.QWidget):

    def __init__(self, parent=None, f=0):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
            f (QtCore.Qt.WindowFlag): window flags
        """
        super(SetupWidget, self).__init__(parent=parent, f=f)

        self.settings = LocalSettings.Settings(directory=os.path.dirname(os.path.dirname(__file__)))

        layout = QtGui.QVBoxLayout()

        self.driverTextField = ComponentTextField.ComponentLineEdit()

        self.cameraView = ComponentView.ComponentView(mobu.FBCamera)
        self.characterView = ComponentView.ComponentView(mobu.FBCharacter)
        self.propView = ComponentView.ComponentView(mobu.FBModel)
        self.propView.setHeader("Prop")
        self.propView.setFilter("_Control$")
        self.propView.update()

        setupButton = QtGui.QPushButton("Setup Vehicles")

        self.splitter = QtGui.QSplitter()
        self.splitter.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.splitter.addWidget(self.cameraView)
        self.splitter.addWidget(self.characterView)
        self.splitter.addWidget(self.propView)

        setupButton.pressed.connect(self.setupVehicle)

        layout.addWidget(QtGui.QLabel("Type name of the component that will drive the setup"))
        layout.addWidget(self.driverTextField)
        layout.addWidget(QtGui.QLabel("Select components to be driven"))
        layout.addWidget(self.splitter)
        layout.addWidget(setupButton)

        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

        # Connect
        SCENE_MONITOR.component.selected.connect(self.updateTextField)

        self.setStyleSheet("QLabel {"
                           "qproperty-alignment: AlignCenter;"
                           "}")

    def CreateMenu(self):
        """
        Creates the menu bar for the tool
        """
        self.menuBar = QtGui.QMenuBar(self)

        self.settingsMenu = QtGui.QMenu("Settings")
        self.plotOptionsMenu = QtGui.QMenu("Plot Options")

        self.menuBar.addMenu(self.settingsMenu)
        self.menuBar.addMenu(self.plotOptionsMenu)

        self.showNamespaces = QtGui.QAction("Show Namespaces", self)
        self.plotComponents = QtGui.QAction("Plot Components", self)

        self.showNamespaces.setCheckable(True)
        self.plotComponents.setCheckable(True)

        self.showNamespaces.toggled.connect(self.useLongNames)

        self.showNamespaces.setChecked(self.settings.Get("showNamespaces", True))
        self.plotComponents.setChecked(self.settings.Get("plotComponents", True))

        self.settingsMenu.addAction(self.showNamespaces)
        self.settingsMenu.addAction(self.plotComponents)

        self.plotOnFrame = QtGui.QAction("On Frame", self)
        self.rotationFilter = QtGui.QActionGroup(self)
        self.useConstantKeyReducer = QtGui.QAction("Use Constant Key Reducer", self)
        self.constantKeyReducerKeepOneKey = QtGui.QAction("Keep One Key", self)
        self.plotTranslationOnRootOnly = QtGui.QAction("Translation On Root Only", self)
        self.rotationFilterGroup = QtGui.QActionGroup(self)

        rotationFilter = self.settings.Get("Rotation Filter", "Gimble Killer")

        for name in ("Gimble Killer", "Unroll", "None"):
            action = QtGui.QAction(name, self)
            action.setCheckable(True)
            self.rotationFilterGroup.addAction(action)
            if rotationFilter == name:
                action.setChecked(True)

        self.plotOnFrame.setCheckable(True)
        self.useConstantKeyReducer.setCheckable(True)
        self.constantKeyReducerKeepOneKey.setCheckable(True)
        self.plotTranslationOnRootOnly.setCheckable(True)

        # Set state of the buttons
        self.plotOnFrame.setChecked(self.settings.Get("plotOnFrame", True))
        self.useConstantKeyReducer.setChecked(self.settings.Get("useConstantKeyReducer", True))
        self.constantKeyReducerKeepOneKey.setChecked(self.settings.Get("keepOneKey", True))
        self.plotTranslationOnRootOnly.setChecked(self.settings.Get("translationOnRootOnly", False))

        self.plotOptionsMenu.addSeparator()
        self.plotOptionsMenu.addSeparator()
        self.plotOptionsMenu.addAction(self.useConstantKeyReducer)
        self.plotOptionsMenu.addAction(self.constantKeyReducerKeepOneKey)
        self.plotOptionsMenu.addSeparator().setText("Plot ...")
        self.plotOptionsMenu.addAction(self.plotOnFrame)
        self.plotOptionsMenu.addAction(self.plotTranslationOnRootOnly)
        self.plotOptionsMenu.addSeparator().setText("Rotation Filter:")
        for action in self.rotationFilterGroup.actions():
            self.plotOptionsMenu.addAction(action)

        return self.menuBar

    def useLongNames(self, value):
        """
        Use Long Names

        Arguments:
            value (boolean): Toogle the display of long names in the tree views
        """
        self.cameraView.setShowLongName(value)
        self.characterView.setShowLongName(value)
        self.propView.setShowLongName(value)

        # Get the top level parent
        parent = None
        _parent = self.parent()
        while _parent:
            parent = _parent
            _parent = parent.parent()

        # Toggle active window to have the TreeViews update the contents of their models
        if parent:
            QtGui.QApplication.setActiveWindow(parent)
        QtGui.QApplication.setActiveWindow(self)

    def setupVehicle(self):
        """
        Setups the vehicle system
        """
        SCENE_MONITOR.disableCallback()
        driver = mobu.FBFindModelByLabelName(str(self.driverTextField.text()))
        if not driver:
            QtGui.QMessageBox.warning(None, "Warning", "Invalid Driver")
            return

        plot = self.plotComponents.isChecked()

        characters = {}
        _characters = []

        rotationFilterAction = self.rotationFilterGroup.checkedAction()
        for character in self.characterView.selectedComponents():

            if plot:
                _characters.append(character)

            if not Lib.ActivateControlRig(character, True) and character.GetCurrentControlSet() is not None:
                characters["{}_NULL".format(character.Name)] = character.GetCurrentControlSet().GetReferenceModel()

        cameras = self.GetDriverDictionary(self.cameraView.selectedComponents())
        props = self.GetDriverDictionary(self.propView.selectedComponents())

        MovingCutscene.SetupMovingCutscene(driver=driver, transferMotion=self.plotComponents.isChecked(),
                                           CHARACTERS=characters, CAMERAS=cameras, PROPS=props)
        Globals.Scene.Evaluate()

        for character in _characters:
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotAllTakes = False
            plotOptions.PlotOnFrame = self.plotOnFrame.isChecked()
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)
            plotOptions.RotationFilterToApply = ROTATION_FILTERS[rotationFilterAction.text()]
            plotOptions.UseConstantKeyReducer = self.useConstantKeyReducer.isChecked()
            plotOptions.ConstantKeyReducerKeepOneKey = self.constantKeyReducerKeepOneKey.isChecked()
            plotOptions.PlotTranslationOnRootOnly = self.plotTranslationOnRootOnly.isChecked()

            character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnControlRig, plotOptions)
        SCENE_MONITOR.enableCallback()

    def GetDriverDictionary(self, components):
        """
        Creates a python dictionary where the name of the component with "_Null" as the suffix is the key to each
        component

        Arguments:
            components (list[pyfbsdk.FBComponent, etc.]): list of components to make a dictionary out of

        Return:
            dictionary
        """
        driverDictionary = {}
        for component in components:
            if not driverDictionary.get("{}_NULL".format(component.Name), None):
                driverDictionary["{}_NULL".format(component.Name)] = component
            else:
                driverDictionary["{}_NULL".format(component.LongName.split(":")[0])] = component
        return driverDictionary

    def updateTextField(self, event, countChange):
        """
        Updates the text field to have the name of the last object selected.

        Arguments:
            event (pyfbsdk.FBEvent): event being called
            countChange (int): the number of components of the type the Component Monitor track has changed

        """
        if isinstance(event.Component, mobu.FBModel):
            self.driverTextField.setText(event.Component.LongName)

    def closeEvent(self, event):
        """
        Overrides built-in method

        Remove callbacks

        Arguments:
            event(QtGui.QEvent): event being called
        """
        super(SetupWidget, self).closeEvent(event)
        self.propView.closeEvent(event)
        self.cameraView.closeEvent(event)
        self.characterView.closeEvent(event)
        self.driverTextField.closeEvent(event)

        SCENE_MONITOR.component.selected.disconnect(self.updateTextField)

    def storeWindowState(self, qsetting):
        """
        Stores settings into the QSettings when the tool closes

        Arguments:
            qsetting (QtGui.QSettings): object to store settings in

        """
        self.settings.SplitterSizes = self.splitter.sizes()

    def restoreWindowState(self, qsetting):
        """
        Restores settings from the QSettings when the tool opens

        Arguments:
            qsetting (QtGui.QSettings): object to restore settings from

        """

        self.splitter.setSizes(self.settings.Get("SplitterSizes", [self.width()/3, self.width()/3, self.width()/3]))


@Run(title="Moving Cutscene", dockable=True)
def Run():
    """
    Shows the UI

    Arguments:
        show = boolean;
    """
    return SetupWidget()
