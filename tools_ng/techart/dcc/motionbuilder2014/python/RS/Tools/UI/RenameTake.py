'''Script Name RenameCurrentTake.py
Original Author Stephen Sloper, Rockstar Toronto.
Modified Author Kristine Middlemiss, Rockstar Toronto.

Description     Loads UI to quickly rename the selected take.'''

from pyfbsdk import *
from pyfbsdk_additions import *
import RS.Tools.UI
import RS.Utils.Creation
import RS.Tools.RenameTake

reload(RS.Tools.RenameTake)

global gTakesArray
gTakesArray = []

import sys

class RenameTakes( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Batch Rename Takes', size = [ 300, 550 ], minSize = [ 300, 400 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Rename_Take_-_Batch_Tool' )

    def Refresh( self, pControl, pEvent ):
        del gTakesArray[:]
        self.Show( True )   

    def AllTakesClearOutSelectionTakes( self, pControl, pEvent ):
        global b
        del gTakesArray[:]        
        for iTake in FBSystem().Scene.Takes:
            gTakesArray.append(iTake.Name)
        b.Name = str(gTakesArray)                   
        #gTakesList.ReadOnly = True
        gTakesList.Selected(0, False)

    def SelectedTakesAddSelectionTakes( self, pControl, pEvent ):
        #gTakesList.ReadOnly = False
        gTakesList.Selected(0, False)
        gTakesList.MultiSelect = True 

        gTakesList.Items.removeAll() 
        for iTake in FBSystem().Scene.Takes:
            gTakesList.Items.append(iTake.Name)

    def ListCallback( self, control, event ):
        global b
        del gTakesArray[:]
        
        for i in range(len(gTakesList.Items)):
            if gTakesList.IsSelected(i):
                gTakesArray.append(gTakesList.Items[i])
        
        b.Name = str(gTakesArray)  
        print b.Name

    def SearchFieldCallback( self, control, event ):                                                        
        b.SearchValue = control.Text
        
    def ReplaceFieldCallback( self, control, event ):     
        b.ReplaceValue = control.Text 
        
    def rs_renametake(self,control, event):
        global gTakesArray, b
        del gTakesArray[:]
        # Only if All takes are selected, then we populate every take to be update, other wise this is done in ListCallback for selected takes
        if b.AllTakes.State == False:
            for iTake in FBSystem().Scene.Takes:
                gTakesArray.append(iTake.Name)
            b.Name = str(gTakesArray)            
        RS.Tools.RenameTake.rs_renametake(control, event)
        self.Show( True )               

    def Create(self, mainLyt):
        # Refresh UI
        lRefreshUIButton = FBButton()
        lRefreshUIButton.Caption = "Click to Refresh UI"
        lRefreshUIButton.Justify = FBTextJustify.kFBTextJustifyCenter  
        lRefreshUIButtonX = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        lRefreshUIButtonY = FBAddRegionParam(30,FBAttachType.kFBAttachTop,"")# Allows for the Rockstar Banner which is 30 high
        lRefreshUIButtonW = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        lRefreshUIButtonH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("lRefreshUIButton","lRefreshUIButton", lRefreshUIButtonX, lRefreshUIButtonY, lRefreshUIButtonW, lRefreshUIButtonH)
        mainLyt.SetControl("lRefreshUIButton",lRefreshUIButton)
        lRefreshUIButton.OnClick.Add( self.Refresh )    

        ToolInstructions = FBLabel()
        ToolInstructions.Caption = "Which take(s) would you like to rename:"
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(60,FBAttachType.kFBAttachTop,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)        
        mainLyt.AddRegion("ToolInstructions","ToolInstructions", butx,buty,butw,buth)
        mainLyt.SetControl("ToolInstructions",ToolInstructions)   

        #placing all radios in this group will allow single selections only
        butGroup = FBButtonGroup()

        AllTakesRadioBut = FBButton()
        AllTakesRadioBut.Caption = 'All Takes'
        AllTakesRadioBut.Style = FBButtonStyle.kFBRadioButton
        AllTakesRadioBut.State = True
        butGroup.Add(AllTakesRadioBut)
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        butw = FBAddRegionParam(-200,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)  
        mainLyt.AddRegion("AllTakesRadioBut","AllTakesRadioBut", butx,buty,butw,buth)
        mainLyt.SetControl("AllTakesRadioBut",AllTakesRadioBut)   
        AllTakesRadioBut.OnClick.Add(self.AllTakesClearOutSelectionTakes)           

        SelectedTakesRadioBut = FBButton()
        SelectedTakesRadioBut.Caption = 'Selected Takes'
        SelectedTakesRadioBut.Style = FBButtonStyle.kFBRadioButton
        SelectedTakesRadioBut.State = False
        butGroup.Add(SelectedTakesRadioBut)
        butx = FBAddRegionParam(100,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(90,FBAttachType.kFBAttachTop,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)  
        mainLyt.AddRegion("SelectedTakesRadioBut","SelectedTakesRadioBut", butx,buty,butw,buth)
        mainLyt.SetControl("SelectedTakesRadioBut",SelectedTakesRadioBut)     
        SelectedTakesRadioBut.OnClick.Add(self.SelectedTakesAddSelectionTakes)
        
        takesx = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        takesy = FBAddRegionParam(120,FBAttachType.kFBAttachTop,"")
        takesw = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        takesh = FBAddRegionParam(-140,FBAttachType.kFBAttachBottom,"")
        mainLyt.AddRegion("takes","takes", takesx,takesy,takesw,takesh)

        SearchLabel = FBLabel()
        SearchLabel.Caption = "Search:"
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-130,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(15,FBAttachType.kFBAttachNone,None)        
        mainLyt.AddRegion("Search","Search", butx,buty,butw,buth)
        mainLyt.SetControl("Search",SearchLabel)        
        
        SearchField = FBEdit()
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-110,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("SearchField","SearchField", butx,buty,butw,buth)
        mainLyt.SetControl("SearchField",SearchField) 
        SearchField.OnChange.Add(self.SearchFieldCallback)

        ReplaceLabel = FBLabel()
        ReplaceLabel.Caption = "Replace:"
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-80,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(15,FBAttachType.kFBAttachNone,None)        
        mainLyt.AddRegion("ReplaceLabel","ReplaceLabel", butx,buty,butw,buth)
        mainLyt.SetControl("ReplaceLabel",ReplaceLabel)             
                        
        ReplaceField = FBEdit()
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-60,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("ReplaceField","ReplaceField", butx,buty,butw,buth)
        mainLyt.SetControl("ReplaceField",ReplaceField) 
        ReplaceField.OnChange.Add(self.ReplaceFieldCallback) 

        global b
        #Passing the list of takes to do the work on.
        b = FBButton()       
        b.Caption = "Rename Take(s)"
        b.SearchValue = SearchField.Text
        b.ReplaceValue = ReplaceField.Text 
        b.AllTakes = SelectedTakesRadioBut     
        b.Hint = "This will rename all the selected takes with above text."        
        butx = FBAddRegionParam(5,FBAttachType.kFBAttachLeft,"")
        buty = FBAddRegionParam(-30,FBAttachType.kFBAttachBottom,"")
        butw = FBAddRegionParam(-5,FBAttachType.kFBAttachRight,"")
        buth = FBAddRegionParam(25,FBAttachType.kFBAttachNone,None)
        mainLyt.AddRegion("b","b", butx,buty,butw,buth)
        mainLyt.SetControl("b",b)      

        global gTakesList
        gTakesList = FBList()
        for iTake in FBSystem().Scene.Takes:
            gTakesList.Items.append(iTake.Name)

        b.Name = str(gTakesArray)        
        gTakesList.Style = FBListStyle.kFBVerticalList
        #gTakesList.ReadOnly = True
        gTakesList.Selected(0, False)
        gTakesList.OnChange.Add(self.ListCallback)                       
                        
        # Now we want to show the list in the UI   
        mainLyt.SetControl("takes", gTakesList)  
        b.OnClick.Add(self.rs_renametake)        
      
def Run( show = True ):
    tool = RenameTakes()
    
    if show:
        tool.Show()
        
    return tool