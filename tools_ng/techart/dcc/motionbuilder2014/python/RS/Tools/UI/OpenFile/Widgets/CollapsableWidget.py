"""
Widget for adding a button that hides a widget
"""

from PySide import QtGui, QtCore


class CollapsableWidget(QtGui.QWidget):
    """ Widget that has a button for hiding/showing another widget """

    clicked = QtCore.Signal(bool)

    Up = QtCore.Qt.UpArrow
    Down = QtCore.Qt.DownArrow
    Left = QtCore.Qt.LeftArrow
    Right = QtCore.Qt.RightArrow
    NoArrow = QtCore.Qt.NoArrow

    __DirectionDictionary = {
        QtCore.Qt.UpArrow:
            {
             "Position": (2, 1),
             "SizePolicy": (QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed),
             "Alignment": QtCore.Qt.AlignTop,
             "Reverse": QtCore.Qt.DownArrow,
             "Size": (16777215, 26)
            },
        QtCore.Qt.DownArrow:
            {
             "Position": (0, 1),
             "SizePolicy": (QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed),
             "Alignment": QtCore.Qt.AlignBottom,
             "Reverse": QtCore.Qt.UpArrow,
             "Size": (16777215, 26)
            },
        QtCore.Qt.LeftArrow:
            {
             "Position": (1, 2),
             "SizePolicy": (QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding),
             "Alignment": QtCore.Qt.AlignRight,
             "Reverse": QtCore.Qt.RightArrow,
             "Size": (26, 16777215)
            },
        QtCore.Qt.RightArrow:
            {
             "Position": (1, 0),
             "SizePolicy": (QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding),
             "Alignment": QtCore.Qt.AlignLeft,
             "Reverse": QtCore.Qt.LeftArrow,
             "Size": (26, 16777215)
            }
    }

    def __init__(self, parent=None, direction=QtCore.Qt.DownArrow):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            direction (QtCore.Qt.Arrow): direction to put button at

        """
        super(CollapsableWidget, self).__init__(parent=parent)

        self._direction = direction
        self._stack = QtGui.QStackedWidget()

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(self._stack)
        self.setLayout(layout)

        layout = QtGui.QGridLayout()
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setColumnMinimumWidth(0, 0)
        layout.setColumnMinimumWidth(1, 0)
        layout.setColumnMinimumWidth(2, 0)

        self.gridLayout = layout

        self._topButton = QtGui.QToolButton()
        self._topButton.setMinimumSize(QtCore.QSize(0, 0))
        self._topButton.clicked.connect(self.click)

        widget = QtGui.QWidget()
        widget.setLayout(self.gridLayout)

        self._stack.addWidget(self._topButton)
        self._stack.addWidget(widget)

        self._button = QtGui.QToolButton()
        self._button.setMinimumSize(QtCore.QSize(0, 0))
        self._button.clicked.connect(self.click)
        self.setMinimumSize(0, 0)

        self.setDirection(direction=self._direction)

    def setDirection(self, direction):
        """
        Sets the direction that the button should be at

        Arguments:
            direction (QtCore.Qt.Arrow): direction to put button at

        """
        self._direction = direction
        self.gridLayout.addWidget(self._button, *self.__DirectionDictionary[direction]["Position"],
                                  alignment=self.__DirectionDictionary[direction]["Alignment"])
        self._button.setSizePolicy(*self.__DirectionDictionary[direction]["SizePolicy"])

        centerWidget = self.centerWidget()

        if not centerWidget or (centerWidget and self._stack.currentIndex()):
            self._topButton.setArrowType(direction)
            self._button.setArrowType(direction)
            self._topButton.setVisible(True)
            self._stack.setCurrentIndex(0)
            self.setMaximumSize(*self.__DirectionDictionary[direction]["Size"])
            self.setSizePolicy(*self.__DirectionDictionary[direction]["SizePolicy"])

        elif centerWidget and not self._stack.currentIndex():
            self._button.setArrowType(self.__DirectionDictionary[direction]["Reverse"])
            self._stack.setCurrentIndex(1)
            self._topButton.setVisible(False)
            self.setMaximumSize(16777215, 16777215)
            self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

    def addWidget(self, widget, visible=False):
        """
        Adds a widget to be controlled by the Collapsable widget. The collapsable widget may only have one widget at a
        time

        Arguments:
            widget (QtGui.QWidget): widget to add to the collapsable widget
            visible (boolean): should the widget being added be visible

        """
        self.gridLayout.addWidget(widget, 1, 1)
        widget.setVisible(visible)

    def centerWidget(self):
        """
        The current widget under the collapsable widget

        Return:
            QtGui.QWidget or None
        """
        item = self.gridLayout.itemAtPosition(1, 1)
        if item:
            return item.widget()

    def click(self):
        """ Toggles visibility for the widget under the collapsable widget """
        visible = bool(self._stack.currentIndex())
        centerWidget = self.centerWidget()

        if centerWidget:
            visible = bool(visible - 1)
            centerWidget.setVisible(visible)
            self.setDirection(self._direction)

        self.clicked.emit(visible)

    def setToolTip(self, text):
        """
        Sets the tool tip for the collapse button

        Arguments:
            text (string): text to display when mouse hovers over the collapse buttons

        """
        self._topButton.setToolTip(text)
        self._button.setToolTip(text)

    def isCenterWidgetVisible(self):
        """ Is the center widget visible """
        if self.centerWidget():
            return self.centerWidget().isVisible()
        return False
