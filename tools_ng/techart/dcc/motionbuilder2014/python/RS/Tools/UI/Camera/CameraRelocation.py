'''

 Script Path: RS/Tools/UI/Mocap/Toolbox.py
 
 Written And Maintained By: Kathryn Bodey and Kristine Middlemiss
 
 Created: 
 
 Description: Toolbox UI to hold mocap tools:
              Stages, Decks, Blocking Models, Camera Relocate


'''

import inspect
from decimal import *

from pyfbsdk import *
from pyfbsdk_additions import *

import RS
import RS.Tools.UI
import RS.Core.Camera.CameraRelocation as cam
import RS.Utils.Creation as cre

###############################################################################################################
## Description: Container Callbacks
###############################################################################################################           

#container for user to add only one item
def ContainerDragAndDrop(control, event):
    if event.State == FBDragAndDropState.kFBDragAndDropDrag:
        event.Accept()
    elif event.State == FBDragAndDropState.kFBDragAndDropDrop:
        if event.Components[0]:
            #Allows for multiple items to be added to one container, this ensures only one is added
            if len(control.Items) == 0:
                control.Items.append(event.Components[0].Name)
            else:
                control.Items.removeAll()    
                control.Items.append(event.Components[0].Name)            

#clear container of any items
def DblClickClear(control, event):
    control.Items.removeAll()     


###############################################################################################################
## Description: UI
###############################################################################################################

class CameraRelocation( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Camera Relocation", size = [ 440, 530 ], helpUrl = 'https://devstar.rockstargames.com/wiki/index.php/Camera_Relocation' )
        
    def Create( self, pMain ):
       
 
        stageList = []

        if not RS.MOBU_IS_STARTING:
            for component in RS.Globals.gComponents:
                stageProperty = component.PropertyList.Find( 'Stages' )
                if stageProperty:
                    stageList.append( component.LongName )

        if len( stageList ) == 0:
            stageList.append( 'No Stages Present' )
            
        lInstructionLabel = FBLabel()
        lInstructionLabel.Caption = "Drag and drop the stage planes into the containers. Double click to clear."
        lInstructionLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lInstructionLabelY = FBAddRegionParam(35,FBAttachType.kFBAttachTop,"")
        lInstructionLabelW = FBAddRegionParam(420,FBAttachType.kFBAttachNone,"")
        lInstructionLabelH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lInstructionLabel","lInstructionLabel", lInstructionLabelX, lInstructionLabelY, lInstructionLabelW, lInstructionLabelH)
        pMain.SetControl("lInstructionLabel",lInstructionLabel)
    
        lCurrentStageLabel = FBLabel()
        lCurrentStageLabel.Caption = "Current Stage:"
        lCurrentStageLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lCurrentStageLabelY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lInstructionLabel")
        lCurrentStageLabelW = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        lCurrentStageLabelH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lCurrentStageLabel","lCurrentStageLabel", lCurrentStageLabelX, lCurrentStageLabelY, lCurrentStageLabelW, lCurrentStageLabelH)
        pMain.SetControl("lCurrentStageLabel",lCurrentStageLabel)
        
        lCurrentStageContainer = FBVisualContainer()
        lCurrentStageContainer.ItemWidth = 100
        lCurrentStageContainerX = FBAddRegionParam(100,FBAttachType.kFBAttachLeft,"")
        lCurrentStageContainerY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lInstructionLabel")
        lCurrentStageContainerW = FBAddRegionParam(285,FBAttachType.kFBAttachNone,"")
        lCurrentStageContainerH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"") 
        pMain.AddRegion("lCurrentStageContainer","lCurrentStageContainer", lCurrentStageContainerX, lCurrentStageContainerY, lCurrentStageContainerW, lCurrentStageContainerH)
        pMain.SetControl("lCurrentStageContainer",lCurrentStageContainer)     
        lCurrentStageContainer.OnDragAndDrop.Add(ContainerDragAndDrop)
        lCurrentStageContainer.OnDblClick.Add(DblClickClear)      
        
        lNewStageLabel = FBLabel()
        lNewStageLabel.Caption = "New Stage:"
        lNewStageLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lNewStageLabelY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCurrentStageLabel")
        lNewStageLabelW = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        lNewStageLabelH = FBAddRegionParam(20,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lNewStageLabel","lNewStageLabel", lNewStageLabelX, lNewStageLabelY, lNewStageLabelW, lNewStageLabelH)
        pMain.SetControl("lNewStageLabel",lNewStageLabel)
        
        lNewStageContainer = FBVisualContainer()
        lNewStageContainer.ItemWidth = 100
        lNewStageContainerX = FBAddRegionParam(100,FBAttachType.kFBAttachLeft,"")
        lNewStageContainerY = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"lCurrentStageContainer")
        lNewStageContainerW = FBAddRegionParam(285,FBAttachType.kFBAttachNone,"")
        lNewStageContainerH = FBAddRegionParam(25,FBAttachType.kFBAttachNone,"")    
        pMain.AddRegion("lNewStageContainer","lNewStageContainer", lNewStageContainerX, lNewStageContainerY, lNewStageContainerW, lCurrentStageContainerH)
        pMain.SetControl("lNewStageContainer",lNewStageContainer)     
        lNewStageContainer.OnDragAndDrop.Add(ContainerDragAndDrop)
        lNewStageContainer.OnDblClick.Add(DblClickClear)                
        
        lCameraLabel = FBLabel()
        lCameraLabel.Caption = "Select the cameras you would like to relocate:\n\nNOTE: Camera switcher cameras are automatically selected"
        lCameraLabelX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lCameraLabelY = FBAddRegionParam(15,FBAttachType.kFBAttachBottom,"lNewStageLabel")
        lCameraLabelW = FBAddRegionParam(420,FBAttachType.kFBAttachNone,"")
        lCameraLabelH = FBAddRegionParam(40,FBAttachType.kFBAttachNone,"")
        pMain.AddRegion("lCameraLabel","lCameraLabel", lCameraLabelX, lCameraLabelY, lCameraLabelW, lCameraLabelH)
        pMain.SetControl("lCameraLabel",lCameraLabel)
    
        lCamList = FBList()
        lCamList.Style = FBListStyle.kFBVerticalList
        lCamList.MultiSelect = True
        lCamListX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lCamListY = FBAddRegionParam(5,FBAttachType.kFBAttachBottom,"lCameraLabel")
        lCamListW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lCamListH = FBAddRegionParam(-50,FBAttachType.kFBAttachBottom,"")
       
        lCameraList = []
        lSwitcherCameraList = []
        
        for lCam in FBSystem().Scene.Cameras:
            if not lCam.SystemCamera:            
                lCameraList.append(lCam)
                lCamList.Items.append(lCam.Name)
        
        lCameraSwitcher = FBCameraSwitcher()
        lKeys = lCameraSwitcher.PropertyList.Find("Camera Index").GetAnimationNode().FCurve.Keys
        
        for iKey in lKeys:
            lSwitcher = lCameraList[int(iKey.Value)-1].Name
            lSwitcherCameraList.append(lSwitcher)
            
        # Select the Camera Switcher Camera's in the UI
        lCount = 0
        for iCamera in lCameraList:
            lCount = lCount + 1
            if iCamera.Name in lSwitcherCameraList:
                lCamList.Selected(lCount-1, True)   
                
        pMain.AddRegion("lCamList","lCamList", lCamListX,lCamListY,lCamListW,lCamListH)
        pMain.SetControl("lCamList",lCamList)
    
        lPerformRelocationButton = FBButton()
        lPerformRelocationButton.Hint = "Creating relations for Characters, Props and Vehicles to work with ToyBox"
        lPerformRelocationButton.Caption = "Perform Camera Relocation"
        lPerformRelocationButton.CurrentStageControl = lCurrentStageContainer
        lPerformRelocationButton.NewStageControl = lNewStageContainer
        lPerformRelocationButton.CameraListControl = lCamList    
        lPerformRelocationButtonX = FBAddRegionParam(10,FBAttachType.kFBAttachLeft,"")
        lPerformRelocationButtonY = FBAddRegionParam(15,FBAttachType.kFBAttachBottom,"lCamList")
        lPerformRelocationButtonW = FBAddRegionParam(-10,FBAttachType.kFBAttachRight,"")
        lPerformRelocationButtonH = FBAddRegionParam(-10,FBAttachType.kFBAttachBottom,"")    
        pMain.AddRegion("lPerformRelocationButton","lPerformRelocationButton", lPerformRelocationButtonX,lPerformRelocationButtonY,lPerformRelocationButtonW,lPerformRelocationButtonH)
        pMain.SetControl("lPerformRelocationButton",lPerformRelocationButton)
        lPerformRelocationButton.OnClick.Add(cam.rs_CameraRelocation)    
        


def Run( show = True ):
    tool = CameraRelocation() 
    
    if show:
        tool.Show()
        
    return tool
