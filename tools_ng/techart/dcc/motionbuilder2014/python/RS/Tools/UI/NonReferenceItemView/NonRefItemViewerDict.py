'''

 Script Path: RS/Core/Reference/NonRefItemViewerDict.py
 
 Written And Maintained By: Kathryn Bodey
 
 Created: 2013
 
 Description: Gathers information on objects not referenced, and whether they have keys present
              Information is displayed via a UI: RS/Tools/UI/NonReferenceItemView.py

'''

from pyfbsdk import *

import RS.Globals as glo
import RS.Utils.Scene
import RS.Utils.Scene.AnimationNode


#####################
# Class for item info
#####################

class NonReferenceItem:

    def __init__( self ):
        
        self.sceneNode = None
        self.hasAnim = "      No"
        self.note = None

####################
# BuildClassArray()
####################
def BuildClassArray():
    
    multiClassArray = []
    
    sceneParentDict = {}
    
    for component in RS.Globals.gComponents:
        
        #only get the class-types we need
        if component.ClassName() == 'FBModel' or component.ClassName() == 'FBModelNull' or component.ClassName() == 'FBMarker'  or component.ClassName() == 'FBModelSkeleton' or component.ClassName() == 'FBModelPath3D':
            if component.Parent == None and component.Name != None:       
                
                #add component to a dictionary. Bool = True ( default bool status for items we want to populate our UI with )
                sceneParentDict[ component ] = ( True )

    for key, value in sceneParentDict.iteritems():
        #get full hierarchy to iterate through to find if the asset has been referenced
        heirarchyList = []
        RS.Utils.Scene.GetChildren( key, heirarchyList, "", True )            
            
        for item in heirarchyList:
            
            parentNull = None
            refProperty = None
            refProperty = item.PropertyList.Find("rs_Type")
            
            if refProperty != None:
                if 'REFERENCED' in refProperty.Data:
                    parentNull = RS.Utils.Scene.GetParent( item )
                    
                    #change bool to a 'false' is item has been referenced, so we know to ignore these
                    if key.LongName == parentNull.LongName:
                        sceneParentDict[ key ] = ( False )
                    
            #change bool to a 'false' is item is in our ignore list
            if 'RECT_ScaleOffset' in key.LongName or 'FacialAttrGUI' in key.LongName or 'WRINKLES_TEXT' in key.LongName or 'faceControls_OFF' in key.LongName or 'TEXT_Normals' in key.LongName or 'Facial_UI' in key.LongName or 'Ambient_UI' in key.LongName or 'Text001' in key.LongName or 'FaceRoot_head_000_r' in key.LongName or 'FaceFX' in key.LongName or 'REFERENCE:Scene' in key.LongName or 'ToyBox' in key.LongName or 'Motion Blend Result' in key.LongName or 'slate_text' in  key.LongName or 'Player_Zero:RECT_HairScale' in key.LongName:
                sceneParentDict[ key ] = ( False )
                
            #change bool to a 'false' is item is a stage
            if key.PropertyList.Find('Stages'):
                sceneParentDict[ key ] = ( False )
                    
                                           
    for key, value in sceneParentDict.iteritems():
        
        #fill info into class. bool = true, so we want to populate our UI with these items
        if value == True:
            
            nonRefClassList = NonReferenceItem()
            
            #sceneNode
            nonRefClassList.sceneNode = key 
            #classType
            nonRefClassList.classType = str( key.ClassName() )
        
            #check if item has anims or not
            hierarchyList = []
            RS.Utils.Scene.GetChildren( key, hierarchyList, "", True )
            
            for item in hierarchyList:
                hasKeys = RS.Utils.Scene.AnimationNode.HaveKeys( item.AnimationNode, 1 )            
                
                #if one or more key is present, append the dictionary with 'yes'
                if hasKeys == True:
                    #hasAnim
                    nonRefClassList.hasAnim = '     Yes'
                    break
                    
            #if key is a character, check if it has an input  
            
            gsSkelRoot = None
            for item in hierarchyList:
                    if item.Name == 'SKEL_ROOT':
                        gsSkelRoot = item
                    
            if gsSkelRoot != None:
                for character in RS.Globals.gCharacters:
                    if character.GetModel(FBBodyNodeId.kFBHipsNodeId) == gsSkelRoot:
                        if character.ActiveInput == True and character.InputType == ( FBCharacterInputType.kFBCharacterInputCharacter ):
                            #note
                            nonRefClassList.note = 'FBCharacter - Active Character Input:    ' + character.InputCharacter.LongName
                        else:
                            nonRefClassList.note = 'FBCharacter - No active Character Input'   

            multiClassArray.append( nonRefClassList )


    #new list to order the lists by hasAnim data
    orderedMultiClassArray = []
    tempArray = []
    
    for i in multiClassArray:
        if "No" in i.hasAnim:
            orderedMultiClassArray.append( i )
        else:
            tempArray.append(i)
            
    orderedMultiClassArray.extend( tempArray )


    return orderedMultiClassArray                
                   