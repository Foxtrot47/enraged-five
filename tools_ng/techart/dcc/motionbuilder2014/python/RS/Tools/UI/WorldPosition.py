"""
Description:
    Outputs world position tool in Motion Builder

Author:
    David Vega <david.vega@rockstargames.com>
"""

import re

import pyfbsdk as mobu

from PySide import QtGui, QtCore

import RS.Tools.WorldPosition
import RS.Tools.UI
import RS.Config

from RS.Tools.UI import Run, Application
DEVELOPMENT = False
WARNING = True


class WorldPositionWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        super(WorldPositionWindow, self).__init__(parent=parent)

        self.Layout = QtGui.QVBoxLayout()

        # create widgets
        self.OffsetTreeWidget = QtGui.QTreeWidget()
        label = QtGui.QLabel("Type name of origin object & press Enter\n"
                             "\tor\n"
                             "Select mover in the scene and press Ctrl+D")
        self.ObjectNameTextField = QtGui.QLineEdit()
        resultLabel = QtGui.QLabel("Offsets")
        self.PositionOutputTextField = QtGui.QTextEdit()
        self.RefreshButton = QtGui.QPushButton()
        self.ExcelCheckBox = QtGui.QCheckBox("Use Excel Sheet")

        # Set widget values
        self.OffsetTreeWidget.headerItem().setData(0, 0, "Ingame Offsets")
        label.setAlignment(QtCore.Qt.AlignCenter)
        resultLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.ObjectNameTextField.setText("mover")
        self.PositionOutputTextField.setReadOnly(True)
        self.RefreshButton.setText("Refresh")

        # add widget to layouts
        self.Layout.addWidget(self.OffsetTreeWidget)
        self.Layout.addWidget(label)
        self.Layout.addWidget(self.ObjectNameTextField)
        self.Layout.addWidget(resultLabel)
        self.Layout.addWidget(self.PositionOutputTextField)
        self.Layout.addWidget(self.ExcelCheckBox)
        self.Layout.addWidget(self.RefreshButton)

        self.RefreshButton.clicked.connect(self.ShowData)
        self.ObjectNameTextField.returnPressed.connect(self.CalculatePosition)
        self.ShowData()

        self.Layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.Layout)

    def showEvent(self, event):
        """
        overrides built-in method; adds custom hotkey on close

        Arguments:
            event (QtGui.QCloseEvent): event called by the UI
        """
        Application.AddShortCut("Ctrl+D", self.CalculatePositionFromSelection)

    def closeEvent(self, event):
        """
        overrides built-in method; removes custom hotkey on close

        Arguments:
            event (QtGui.QCloseEvent): event called by the UI
        """
        Application.RemoveShortCut("Ctrl+D")

    def GetData(self):
        """  Retrieves data about the current scene from the appropriate source as dictated by the user/project """

        if self.ExcelCheckBox.isChecked():
            self.GetExcelData()
        else:
            self.GetDatabaseData()
        return self.data

    def GetExcelData(self):
        """ Calls the WorldPosition.GetWorldPosition function and stores the result in self.database_data """
        self.data = RS.Tools.WorldPosition.GetWorldPositionFromExcel(warning=WARNING)
        return self.data

    def GetDatabaseData(self):
        """ Calls the WorldPosition.GetWorldPositionFromDatabase function and stores the result in self.database_data"""
        self.data = RS.Tools.WorldPosition.GetWorldPositionFromDatabase(warning=WARNING)
        return self.data

    def ShowData(self):
        """
        Updates the offset tree widget with offset information based on the current set referenced into the scene
        """

        self.OffsetTreeWidget.clear()
        self.GetData()

        for each_set, each_set_data in self.data.iteritems():

            # Create Null/Set Widget
            null_set_widget_name = '{0} : {1}'.format(each_set,each_set_data["null_name"])
            null_set_widget = QtGui.QTreeWidgetItem(self.OffsetTreeWidget, [null_set_widget_name])

            # Get keys that only deal with Translation and Rotation Data based on the current names used in the database
            # ei. Trans_X, Rotate_X, etc.
            data_keys_ordered_list = [each for each in each_set_data.keys() if re.search("(?<=_)X|Y|Z$", each)]
            data_keys_ordered_list.sort()

            for each_property in data_keys_ordered_list:

                property_widget = QtGui.QTreeWidgetItem(null_set_widget, [each_property])
                QtGui.QTreeWidgetItem(property_widget, [str(each_set_data[each_property])])

        self.OffsetTreeWidget.expandAll()

    def CalculatePosition(self):
        """
        Figures out the offset world position values for the Origin Object typed in the UI and shows the results
        in the In Game Position Output Text Edit window
        """
        # Check if origin object exists in the scene
        origin_object = self.ObjectNameTextField.text()
        origin_object = mobu.FBFindModelByLabelName(str(origin_object))

        if origin_object:
            # Set the world game coordinates in the QT UI
            self.PositionOutputTextField.clear()
            feedback_list = RS.Tools.WorldPosition.rs_CalculatePosition(origin_object)
            feedback_list = ["{0}\n".format(each.strip()) for each in feedback_list]
            self.PositionOutputTextField.insertPlainText("".join(feedback_list))
        else:
            error_message = "Origin object does not exist\nCheck spelling or reference a new origin object."
            spaces = " "*16
            QtGui.QMessageBox.warning(None, "Error", spaces + error_message)

    def CalculatePositionFromSelection(self):
        models = [selection for selection in mobu.FBSystem().Scene.Components if selection.Selected]
        if models:
            self.ObjectNameTextField.setText(models[0].LongName)
            self.CalculatePosition()


@Run(title='Ingame World Position', size=(200, 400), dockable=True)
def Run(show=True):
    """
    Shows the widget

    Arguments:
        show (boolean): shows widget
    """
    return WorldPositionWindow()
