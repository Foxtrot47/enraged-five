"""
Description:
    UI module for specifying user options when creating renders of ingame animations.
Author:
    Blake Buck <blake.buck@rockstargames.com>
"""

import os
import pyfbsdk as mobu
from PySide import QtCore, QtGui
import RS.Config
import RS.Utils.UserPreferences as userPref
from RS.Core.Animation import IgaRenderCore
from RS.Core.Scene.widgets import ComponentView
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem
from RS.Core.Scene.models.modelItems.ComponentModelItem import ComponentItem
from RS.Tools.UI import Application, Run


ICONSROOT = RS.Config.Script.Path.ToolImages
RENDERROOT = os.path.join(RS.Config.Project.Path.Art, "animation", "ingame", "Renders")
FOLDERICON = os.path.join(ICONSROOT, "ReferenceEditor", "Folder.png")


def GetDefaultOutputPath():
    """
    Gets a saved output path from a user's preferences, or uses a default.
    """
    outputPath = RENDERROOT
    savedPath = userPref.__Readini__("renderPaths", "igrender", notFoundValue=None)
    if savedPath:
        outputPath = os.path.split(savedPath)[0] if savedPath.lower().endswith(".mov") else savedPath
    return outputPath


def GetAudioTrackText(audioTrack):
    """
    Creates a string containing an audio track with it's clips names.
    Arguments:
        audioTrack (mobu.FBStoryTrack): audio track to generate names from
    Returns:
        trackText (string): the formatted track text
    """
    trackClips = [clip.Name for clip in audioTrack.Clips]
    trackText = "{0}: {1}".format(audioTrack.Name, ", ".join(trackClips))
    return trackText


def GetRenderWindows():
    """
    Gets all of Mobu's Render windows and retuns them in a list.
    Returns:
        renderWindows (list): a list of Mobu Window objects
    """
    mobuWindow = Application.GetMainWindow()
    windowList = Application.GetChildrenByWindowTitle(mobuWindow, "Render")
    renderWindows = [window for window in windowList if window.windowTitle() == "Render"]
    return renderWindows


def CloseRenderWindows():
    """
    Searches for any Mobu Render windows and closes them.
    """
    renderWindows = GetRenderWindows()
    for window in renderWindows:
        if window.windowTitle() == "Render":
            window.close()


class RenderItem(baseModelItem.BaseModelItem):
    """
    Simple baseModelItem for storing and displaying the user's render items.
    """
    mobuRole = QtCore.Qt.UserRole

    def __init__(self, data, parent=None):
        self._data = data
        super(RenderItem, self).__init__(parent=parent)

    def data(self, index, role=QtCore.Qt.DisplayRole, column=None):
        if role == QtCore.Qt.DisplayRole:
            if type(self._data) == mobu.FBStoryTrack:
                return GetAudioTrackText(self._data)
            return self._data.Name
        elif role == self.mobuRole:
            return self._data
        elif role == QtCore.Qt.DecorationRole:
            if type(self._data) == mobu.FBTake:
                path = os.path.join(ICONSROOT, "MotionBuilder", "take.png")
                return QtGui.QIcon(path)
            elif type(self._data) == mobu.FBStoryTrack:
                path = os.path.join(ICONSROOT, "MotionBuilder", "audio.png")
                return QtGui.QIcon(path)


class RenderModel(baseModel.BaseModel):
    """
    Simple baseModel for storing and displaying the user's render items.
    """
    def __init__(self, parent=None):
        self._dict = {}
        super(RenderModel, self).__init__(parent=parent)

    def setupModelData(self, parent):
        for take, trackList in self._dict.iteritems():
            takeItem = RenderItem(take, parent)
            for track in trackList:
                trackItem = RenderItem(track, takeItem)
                takeItem.appendChild(trackItem)
            parent.appendChild(takeItem)

    def getHeadings(self):
        return ["Renders"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.getHeadings())

    def flags(self, index):
        if index.parent().data() == None:
            return self.rootItem.flags(index)
        else:
            return QtCore.Qt.NoItemFlags

    def setDict(self, value):
        self._dict.update(value)
        self.reset()


class FileProxyModel(QtGui.QSortFilterProxyModel):
    """
    ProxyModel to allow users to see files in the file dialog, but only select folders.
    """
    def __init__(self, parent=None):
        super(FileProxyModel, self).__init__(parent=parent)

    def flags(self, index):
        index = self.mapToSource(index)
        if not self.sourceModel().isDir(index):
            return QtCore.Qt.ItemIsEnabled
        return self.sourceModel().flags(index)


class TrackProxyModel(QtGui.QSortFilterProxyModel):
    """
    ProxyModel to filter out non-audio story tracks and display track names with clip info.
    """
    def __init__(self, parent=None):
        super(TrackProxyModel, self).__init__(parent=parent)

    def filterAcceptsRow(self, row, parent=None):
        # index = self.index(row, 0, parent)
        index = self.sourceModel().index(row, 0, parent)
        storyTrack = index.data(ComponentItem.Component)  # QtCore.Qt.UserRole + 3
        if storyTrack and storyTrack.Type == mobu.FBStoryTrackType.kFBStoryTrackAudio and len(storyTrack.Clips) > 0:
            return True
        return False

    def data(self, index, role):
        sourceIndex = self.mapToSource(index)
        component = sourceIndex.data(ComponentItem.Component)
        if component is not None and role == QtCore.Qt.DisplayRole:
            return GetAudioTrackText(component)
        elif component is not None and role == QtCore.Qt.DecorationRole:
            path = os.path.join(ICONSROOT, "MotionBuilder", "audio.png")
            return QtGui.QIcon(path)
        return super(TrackProxyModel, self).data(index, role)


class IgRenderMenu(QtGui.QWidget):
    """
    Main widget for setting options and creating renders for ingame animations.
    """
    def __init__(self, parent=None):
        super(IgRenderMenu, self).__init__(parent=parent)
        self.takeViewer = ComponentView.ComponentView(mobu.FBTake)
        self.trackViewer = ComponentView.ComponentView(mobu.FBStoryTrack)
        self.trackViewer.setFilterModel(TrackProxyModel())
        self.renderModel = RenderModel()
        self.renderViewer = QtGui.QTreeView()
        self.proxyModel = QtGui.QSortFilterProxyModel()
        self.renderFolder = GetDefaultOutputPath()
        self.renderFolderText = QtGui.QLabel(self.renderFolder)
        self.manualRenderButton = QtGui.QPushButton("Manual Render")
        self.customOptions = userPref.__Readini__("igRender", "customOptions", notFoundValue=False)
        self.SetupUi()

    def SetupUi(self):
        # Setup Basic UI Elements
        self.setWindowTitle("IG Render Menu")
        mainLayout = QtGui.QVBoxLayout()
        mobuLayout = QtGui.QHBoxLayout()
        renderLayout = QtGui.QHBoxLayout()
        itemButtonLayout = QtGui.QVBoxLayout()
        renderFolderLayout = QtGui.QHBoxLayout()
        renderOptionsLayout = QtGui.QHBoxLayout()
        renderButtonLayout = QtGui.QHBoxLayout()

        # Setup Mobu Viewers
        selectTakesText = QtGui.QLabel("Select Takes to Render:")
        self.takeViewer.setSelectionMode(self.takeViewer.MultiSelection)
        self.takeViewer.setRootIsDecorated(False)
        self.takeViewer.setHeader("Takes")
        self.trackViewer.setSelectionMode(self.trackViewer.MultiSelection)
        self.trackViewer.setRootIsDecorated(False)
        self.trackViewer.setHeader("Audio Tracks")
        takeSelection = self.takeViewer.selectionModel()
        takeSelection.selectionChanged.connect(self.ResetAudioViewer)

        # Setup Render Viewer
        self.proxyModel.setSourceModel(self.renderModel)
        self.renderViewer.setModel(self.proxyModel)
        self.renderViewer.setSelectionMode(self.trackViewer.MultiSelection)
        self.renderViewer.setAnimated(True)

        # Setup Buttons
        addItemsButton = QtGui.QPushButton("Add Renders")
        addItemsButton.clicked.connect(self.AddItemsPressed)
        removeItemsButton = QtGui.QPushButton("Remove Renders")
        removeItemsButton.clicked.connect(self.RemoveItemsPressed)
        browseFolderButton = QtGui.QPushButton()
        browseFolderIcon = QtGui.QIcon(FOLDERICON)
        browseFolderButton.setIcon(browseFolderIcon)
        browseFolderButton.setMaximumWidth(30)
        browseFolderButton.clicked.connect(self.BrowseFolderPressed)
        self.manualRenderButton.clicked.connect(self.LocalRenderPressed)
        self.manualRenderButton.setEnabled(False)
        autoRenderButton = QtGui.QPushButton("Automated Render")
        autoRenderButton.setEnabled(False)
        autoRenderButton.setToolTip("Coming soon.")

        # Setup Render Options Box
        renderOptionsLabel = QtGui.QLabel("Video Options:")
        renderOptionsGroup = QtGui.QGroupBox()
        defaultOptionsButton = QtGui.QRadioButton("Default")
        defaultOptionsButton.clicked.connect(self.DefaultOptionsPressed)
        customOptionsButton = QtGui.QRadioButton("Custom")
        customOptionsButton.clicked.connect(self.CustomOptionsPressed)
        if self.customOptions:
            customOptionsButton.setChecked(True)
        else:
            defaultOptionsButton.setChecked(True)
        renderOptionsGroup.setChecked(True)

        # Setup Output Folder Box
        folderLabelText = QtGui.QLabel("Render Folder:")
        self.renderFolderText = QtGui.QLabel(self.renderFolder)
        self.renderFolderText.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
        self.renderFolderText.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)

        # Add Elements to Layout
        mainLayout.addWidget(selectTakesText)
        mobuLayout.addWidget(self.takeViewer)
        mobuLayout.addWidget(self.trackViewer)
        mainLayout.addLayout(mobuLayout)
        itemButtonLayout.addWidget(addItemsButton)
        itemButtonLayout.addWidget(removeItemsButton)
        renderLayout.addWidget(self.renderViewer)
        renderLayout.addLayout(itemButtonLayout)
        mainLayout.addLayout(renderLayout)
        renderOptionsLayout.setAlignment(QtCore.Qt.AlignLeft)
        renderOptionsLayout.addWidget(renderOptionsLabel)
        renderOptionsLayout.addWidget(defaultOptionsButton)
        renderOptionsLayout.addWidget(customOptionsButton)
        renderFolderLayout.addWidget(folderLabelText)
        renderFolderLayout.addWidget(self.renderFolderText, stretch=1)
        renderFolderLayout.addWidget(browseFolderButton)
        mainLayout.addLayout(renderOptionsLayout)
        mainLayout.addLayout(renderFolderLayout)
        renderButtonLayout.addWidget(self.manualRenderButton)
        renderButtonLayout.addWidget(autoRenderButton)
        mainLayout.addLayout(renderButtonLayout)
        self.setLayout(mainLayout)

    def RefreshRenderViewer(self):
        self.proxyModel.setSourceModel(self.renderModel)
        self.proxyModel.sort(0, QtCore.Qt.AscendingOrder)
        self.renderViewer.expandAll()

    def ResetAudioViewer(self):
        self.trackViewer._model.reset()

    def AddItemsPressed(self):
        renderDict = {}
        takes = self.takeViewer.selectedComponents()
        tracks = self.trackViewer.selectedComponents()
        for take in takes:
            renderDict[take] = tracks
        self.renderModel.setDict(renderDict)
        self.RefreshRenderViewer()
        self.trackViewer._model.reset()
        self.takeViewer._model.reset()
        self.manualRenderButton.setEnabled(True)

    def RemoveItemsPressed(self):
        selectedIndexes = self.renderViewer.selectionModel().selection().indexes()
        for index in selectedIndexes:
            take = index.data(RenderItem.mobuRole)
            self.renderModel._dict.pop(take, None)
        self.renderModel.reset()
        self.RefreshRenderViewer()
        if len(self.renderModel._dict) == 0:
            self.manualRenderButton.setEnabled(False)

    def BrowseFolderPressed(self):
        fileDialog = QtGui.QFileDialog()
        fileProxy = FileProxyModel()
        fileDialog.setProxyModel(fileProxy)
        fileDialog.setWindowTitle("Select Render Folder")

        widgetCenter = self.window().frameGeometry().center()
        frameOffset = (self.window().frameGeometry().y() - self.window().geometry().y()) / 2
        fileDialog.move(widgetCenter.x() - fileDialog.width() / 2,
                        widgetCenter.y() - fileDialog.height() / 2 + frameOffset)

        fileDialog.setFileMode(QtGui.QFileDialog.Directory)
        fileDialog.setDirectory(self.renderFolder)
        if fileDialog.exec_():
            self.renderFolder = fileDialog.selectedFiles()[0]
            self.renderFolderText.setText(self.renderFolder)
        del fileDialog

    def LocalRenderPressed(self):
        # Save Prefrences and get Render Dict
        userPref.__Saveini__("renderPaths", "igrender", self.renderFolder)
        userPref.__Saveini__("igRender", "customOptions", self.customOptions)
        renderDict = self.renderModel._dict

        # Render Each Take
        for take, audioTracks in renderDict.iteritems():
            cleanName = IgaRenderCore.CleanTakeName(take)
            outputPath = "{0}/{1}.mov".format(self.renderFolder, cleanName)
            IgaRenderCore.RenderIgVideo(take, outputPath, audioTracks, self.customOptions)

        # Close Render Window
        CloseRenderWindows()
        mainWindow = QtGui.QApplication.instance()
        mainWindow.setActiveWindow(self)
        msgBox = QtGui.QMessageBox()
        widgetCenter = self.window().frameGeometry().center()
        msgBox.setGeometry(widgetCenter.x() - 100, widgetCenter.y() - 50, 300, 300)
        msgBox.setWindowTitle("IG Render")
        msgBox.setText("Renders complete.")
        msgBox.exec_()
        mainWindow.setActiveWindow(self)

        # Update Render Database
        IgaRenderCore.UpdateRenderDatabase(renderDict)

    def DefaultOptionsPressed(self):
        self.customOptions = False
        CloseRenderWindows()

    def CustomOptionsPressed(self):
        self.customOptions = True
        CloseRenderWindows()
        mobu.ShowToolByName("Render")


@Run("IG Render Menu")
def Run():
    wid = IgRenderMenu()
    wid.resize(540, 550)
    return wid
