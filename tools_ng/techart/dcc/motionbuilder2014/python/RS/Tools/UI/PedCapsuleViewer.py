
#1.2: capsule tool into a class
#1.3: more presets corresponding to the following xml data
##      <Radius value="0.250000" />
##      <RadiusRunning value="0.450000" />
##      <RadiusCrouched value="0.400000" />
##      <RadiusInCover value="0.200000" />
##      <RadiusCrouchedinCover value="0.300000" />
##      <RadiusGrowSpeed value="0.300000" />

import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Utils.Creation as cre


class CapsuleViewerTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Capsule Viewer", size = [ 220, 165 ] )
           
            
        #defaults
        self.buttonHeight = 20
        self.capName = "capsule"
        self.sizeValues = (25, 45, 40, 20, 30, 30)   #according to the radius values in header
        #nb: a further extension would be to read these values in from the xml file directly
        self.lengthValues = []
        
        #calculate length to correspond to size where y = 200 - 2x
        #for some reason, this is the proportional relationship between length and size!
        
        for i in range(len(self.sizeValues)):
            self.lengthValues.append(200 - (self.sizeValues[i] * 2))
        
        self.sizeNames = ("Default", "Running", "Crouched", "In Cover", "Crouched In Cover", "Grow Speed")
        
        #controls
        self.infoLabel = FBLabel()
        self.slider = FBSlider()
    
    #functions
    def DeleteIfExists(self, objName):
        item = FBFindModelByLabelName(objName)
        if (item != None):
            item.FBDelete()
            
    def CreateNull(self, name, position):
        newNull = FBModelNull(name)
        newNull.Translation = position
        newNull.Show = True
        newNull.Size = 10
        return newNull
    
    def ToggleMoverCapsule(self, control, event):
        capsule = FBFindModelByLabelName(self.capName)
        mover = FBFindModelByLabelName("mover")
    
        if mover and capsule:
            if (mover.Visibility.Data == True):
                #create capsule
                value = int(self.slider.Value)
                capsule = FBModelMarker(self.capName)
                capsule.Look = FBMarkerLook.kFBMarkerLookCapsule
                capsule.Show = True
                capsule.Size = self.sizeValues[value]
                capsule.Length = self.lengthValues[value]
                
                #get transforms
                mover = FBFindModelByLabelName("mover")
                dummy = mover.Parent
                mover.Parent = None
                mtr = mover.Translation #this is the location for the capsule
                mro = mover.Rotation
                msc = mover.Scaling
                
                #create helper
                self.DeleteIfExists("capsuleHelper")
                nullParent = self.CreateNull("capsuleHelper", FBVector3d(0,0,0))
                capsule.Parent = nullParent
                nullParent.Translation = FBVector3d(mtr)
                nullParent.Rotation = FBVector3d(mro)
                nullParent.Scaling = FBVector3d(msc)
                
                mover.Parent = dummy
                nullParent.Parent = dummy
                mover.Visibility = False
                return capsule
                
            else:
                self.DeleteIfExists("capsule")
                self.DeleteIfExists("capsuleHelper")
                mover.Visibility = True
                return None
            
        else:
            FBMessageBox( 'Rockstar', 'Could not find an object named "mover" and capsule named "{0}"!'.format( self.capName ), 'OK' )
            return None
    
    def CapsuleSize(self, control, event):
        capsule = FBFindModelByLabelName(self.capName)
        value = int(self.slider.Value)
        if (capsule != None):
            capsule.Size = self.sizeValues[value]
            capsule.Length = self.lengthValues[value]
        self.infoLabel.Caption = "Capsule size edited to " + self.sizeNames[value]
        
    #GUI
    def Create(self, mainLyt):
        
        # Rockstar Banner
        #lHeaderBanner = cre.rs_CreateBanner( mainLyt, toolName, emailTo, toolPath, cc, wikipage )
        
        x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(100,FBAttachType.kFBAttachNone,"")
        mainLyt.AddRegion("main","main", x, y, w, h)
        lyt = FBVBoxLayout()
        mainLyt.SetControl("main",lyt)
        
        b = FBButton()
        b.Caption = "Toggle mover/capsule"
        b.Justify = FBTextJustify.kFBTextJustifyCenter
        lyt.Add(b, self.buttonHeight)
        b.OnClick.Add(self.ToggleMoverCapsule)
        
        l = FBLabel()
        l.Caption = "Capsule Size:"
        lyt.Add(l, self.buttonHeight)
        
        self.slider.Orientation = FBOrientation.kFBHorizontal
        self.slider.Min = 0
        self.slider.Max = len(self.sizeValues) - 1
        self.slider.Value = 0
        lyt.Add(self.slider, 20)
        self.slider.OnChange.Add(self.CapsuleSize)    
    
        self.infoLabel.Caption = "Create a capsule and modify the size"
        self.infoLabel.Justify = FBTextJustify.kFBTextJustifyLeft
        lyt.Add(self.infoLabel, self.buttonHeight)
    
def Run( show = True ):
    tool = CapsuleViewerTool()
    
    if show:
        tool.Show()
        
    return tool
