import os
import socket
from pyfbsdk import *
import RS.Config


def AppTxtChangeNeedToRestart():
    lResult = FBMessageBox( "Configuration file update confirmation", "The configuration file has been updated.\nYou must restart MotionBuilder before launching Faceware.", "Restart", "Restart Later"  )
    if lResult == 1:
        lResult2 = FBMessageBox( "Save changes?", "Save changes before closing? \nIf you don't save, your changes will be lost.", "Save", "Don't Save", "Cancel"  )
        if lResult2 == 1:
            # Save            
        try:
        os.startfile(r"C:\Program Files\Autodesk\MotionBuilder 2014\bin\x64\motionbuilder.exe")                        
        FBApplication().FileExit (True) 
        except Exception, err:
        value = str(err)
        FBMessageBox( "Error Launching", value, "Ok")            
            
        elif lResult2 == 2:
            # Don't Save
            try:
        os.startfile(r"C:\Program Files\Autodesk\MotionBuilder 2014\bin\x64\motionbuilder.exe")                       
        FBApplication().FileExit (False)  
        except Exception, err:
        value = str(err)
        FBMessageBox( "Error Launching", value, "Ok")           

def CheckAppTxtAndLaunch():
    
    # Remove boost interprocess folder. - TPhan 4/May/2015
    # This folder seems to be some kind of memory management cache for the retargeter, and occasionally gets corrupted causing issues starting the fwr app.
    #RS.Utils.Process.KillAll( "FacewareRetargeter_GUI" )    
    #boostPath = "C:\\ProgramData\\boost_interprocess"
    #if ( os.path.isdir( boostPath ) ):
    #    shutil.rmtree( boostPath )
    #    print "Removed boost interprocess folder."
     
    # Turn on the Auto key (url:bugstar:2093430)
    FBKeyControl().AutoKey = True
    
    lApplication = FBApplication()
    lSystem = FBSystem()
    lconfig = FBConfigFile("@Task.txt")
    
    lvalue = lconfig.Get("Display", "FastIdleOnDeactivate")
    
    if lvalue == None:        
            
        # The setting isn't there at all we need to add it.
        apptxt = r'C:\Users\{0}\Documents\MB\2014-x64\config\{1}.Task.txt'.format( os.environ.get( "USERNAME" ), socket.gethostname() )
        if os.path.isfile( apptxt ):
            fstream = open( apptxt, 'a' )
    
            # Write the Data
            fstream.write( '\n[Display]\n' )
            fstream.write( 'FastIdleOnDeactivate = Yes ; Yes or No (viewer refresh when the application is out of focus\n\n' )
    
            fstream.close()

            AppTxtChangeNeedToRestart()
        
    
    elif lvalue == 'Yes' or lvalue == 'yes':
        try:
        os.startfile(r"C:\Program Files\Faceware\Retargeter 4.0\MotionBuilder\2014(64)\FacewareRetargeter_GUI.exe")        
        except Exception, err:
        value = str(err)
        FBMessageBox( "Error Launching", value, "Ok")
             
    elif lvalue == 'No' or lvalue == 'no':
        # Updating the NoFastIdleOnDeactivate value to No 
        lconfig.Set("Display", "FastIdleOnDeactivate", "Yes", "Default is Yes, it will always refresh")
        AppTxtChangeNeedToRestart()    


def Run():
    CheckAppTxtAndLaunch()        
