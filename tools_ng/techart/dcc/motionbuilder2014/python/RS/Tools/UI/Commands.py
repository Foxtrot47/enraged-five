"""
Description:
    Commands module that are called from the Rockstar menu.
    
Author:
    Jason Hayes <jason.hayes@rockstarsandiego.com>
"""

import os
import sys
import py_compile
import webbrowser
import pythonidelib

import pyfbsdk as mobu

import RS.Config
import RS.Globals
import RS.Perforce
import RS.Utils.Report.CodingStandards
import RS.Core.Reference.New.Scene
import RS.Tools.AddHeelControl
import RS.Utils.Scene

from RS import ProjectData
from RS.Utils import Wing

def CharacterReviewApproveCurrentFile():
	import RS.Core.Character.Review
	RS.Core.Character.Review.ApproveCurrentFile()


def StartUFC():
    if ProjectData.data.UseUFC():
        from RS.Core.Face import UFC
        UFC.Start()

def RunAddHeelControlTool():
    """ Launches the Add Heel Control Tool """
    RS.Tools.AddHeelControl.Run()


def OpenHowToBuildToolsDocumentation():
    """ Opens the how to build tools documentation wiki page"""
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/Building_Python_Tools_Documentation")


def OpenPythonToolsFrameworkOverview():
    """ Opens the Python Tools Framework wiki page"""
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/"
                    "MotionBuilder_2014_-_Python_Tools_Framework_Overview")


def OpenPythonCodingStandards():
    """ Opens the python coding standards wiki page"""
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/Python_Coding_Standards")


def OpenDebuggingPythonInMotionBuilder():
    """ Opens the debugging python in motion builder wiki page"""
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/Debugging_Python_in_MotionBuilder")


def OpenRemotelyExecuteScriptsInMotionBuilder():
    """ Opens the wiki page on how to remotely execute scripts in motion builder"""
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/Remotely_Execute_Scripts_in_MotionBuilder")

def OpenAnimationDatabasePage():
    """ Opens the animation database page """
    webbrowser.open("http://nycw-mhb-sql/")

def GenerateCodingStandardsReport():
    """ Generate the coding standard report for the RS library """
    RS.Utils.Report.CodingStandards.GenerateReport()


def UpdateTools():
    """ Updates the RS Library to the latest version and removes old .pyc files """
    RS.Perforce.Sync('{0}\\...'.format(RS.Config.Script.Path.Root))  
    
    # Delete all .pyc files, could be some old ones in there.
    scriptDirs = ['{0}\\RS'.format(RS.Config.Script.Path.Root),
                  '{0}\\startup'.format(RS.Config.Script.Path.Root)]
    
    for scriptDir in scriptDirs:
        for directory, dirNames, filenames in os.walk(scriptDir):
            for filename in filenames:
                if filename.endswith('.pyc'):
                    os.remove(os.path.join(directory, filename))
                    
                if filename.endswith('.py'):
                    try:
                        py_compile.compile(os.path.join(directory, filename))
                        
                    except py_compile.PyCompileError, err:
                        sys.stderr.write(err)


def BuildDocumentation():
    """ Builds the documentation for the RS Module in a separate Motion Builder process"""
    from RS.Docs import Build
    Build.MotionBuilderBatch()


def OpenDeveloperHelp():
    """ Opens the tool help documentation for the Tech Art RS Libray """
    # The help is not currently being integrated to the project.  Only TA's need to view it, so doesn't seem
    # necessary to integrate to the projects.
    helpDir = os.path.abspath('x:\\wildwest\\dcc\\motionbuilder{0}\\help'.format(RS.Config.Script.TargetBuild))
    
    RS.Perforce.Sync('{0}\\...'.format(helpDir))

    indexHtml = '{0}\\index.html'.format(helpDir)

    if os.path.isfile(indexHtml):
        webbrowser.open(indexHtml)

    else:
        mobu.FBMessageBox('Rockstar', 'You do not have the tools documentation.  '
                                      'Was expecting it here:\n\n{0}'.format(indexHtml), 'OK')


def OpenToolsHelp():
    """ Opens the wikipage for the Tech Art RS Libray """
    webbrowser.open("https://devstar.rockstargames.com/wiki/index.php/MotionBuilder_Rockstar_TechArt_Tools")


def ContactUs():
    """ Composes an email to the Tech Art team for help """
    emailTo = "TECHART_SUPPORT"
    toolName = "General Tools Help"
    os.startfile("mailto:" + emailTo + "&Subject= "+ toolName)


def OpenULog():
    """
    Opens the U Log

    Image:
        $TechArt\Tools\ULog.png
    """
    RS.Globals.gUlog.Show(modal=False)
    

def ConnectToWing():
    """ Connect to Wing IDE for debugging python scripts """
    if not Wing.ConnectToWing():
        mobu.FBMessageBox('Rockstar',   'Could not remotely connect to Wing IDE!  \n'
                                        'Do you have it open and that you have Enable Passive Listen on under\n'
                                        'preferences -> Debugger -> External/Remote \n',
                                        'OK')


def ConnectToPycharm():
    """ Connect to Pycharm for debugging python scripts """
    # This is hardcoded , may not work for every user, will need to be tested if it works for different versions
    # of Pycharm

    pydev_paths = [r'X:\wildwest\etc\Pycharm\pycharm-debug',
                   r'X:\wildwest\etc\Pycharm\helpers']

    for pydev_path in pydev_paths:
        if pydev_path not in sys.path:
            sys.path.append(pydev_path)

    try:
        import pydevd
        # pydevd seems to use a singleton that will not connect to the server
        reload(pydevd)
        pydevd.settrace('localhost', port=21100, suspend=False)


    except ValueError:
        mobu.FBMessageBox('Rockstar',
                          'Could not remotely connect to Pycharm!  \n'
                          'Do you have it open and have the port set to 21100',
                          'OK')


def DebugInWing(pDevelopment, *parameters):
    """ Decorator to automatically connect to wing if the gDevelopment variable is True """

    def parameter_keywords(func):
        def wrapper(*args, **kwargs):
            if pDevelopment:
                ConnectToWing()
            return func(*args, **kwargs)
        return wrapper
    return parameter_keywords


def DebugInPycharm(pDevelopment, *parameters):
    """
    Decorator to automatically connect to pycharm if the gDevelopment variable is True
    NOTE: Pycharm Debug Server must be open to for it to work
    """

    def parameter_keywords(func):
        def wrapper(*args, **kwargs):

            try:
                if pDevelopment:
                    ConnectToPycharm()
                    return func(*args, **kwargs)

            except Exception:
                    return func(*args, **kwargs)

            finally:
                if pDevelopment:
                    import pydevd
                    pydevd.stoptrace()

        return wrapper
    return parameter_keywords


def FileDialog(type="File", caption="Select", path="", filter="*", style="Open"):
    """
    Opens a file dialog to get a file path

    Argument:
        type (string): file or directory
        caption (string): button caption
        path (string): path to look for files in
        filter (string): filter files by
        style (string): the style of the dialog
    """
    type =type.capitalize()
    dialog = getattr(sys.modules["pyfbsdk"],
                     "FB{}Popup".format(type))()
    dialog.Caption = caption

    # Setup Path
    path = os.path.abspath(path)

    if not os.path.exists(path):
        path = os.path.abspath("")

    dialog.Path = path
    # Set extra values associated with the file dialog
    if type == "File":
        style = style.capitalize()
        dialog.Style = getattr(mobu.FBFilePopupStyle, "kFBFilePopup{}".format(style),
                               mobu.FBFilePopupStyle.kFBFilePopupOpen)
        dialog.Filter = "*.{}".format(filter)

    result = dialog.Execute()

    return getattr(dialog, "FullFilename", dialog.Path), result


# # Referencing System 2.0 Commands # #

def ConvertCurrentSceneToAssembler():
    """ Converts the current cuscene to an assembler """
    cutsceneFilePath = NewCutscene()
    if cutsceneFilePath is not None:
        RS.Core.Reference.New.Scene.convertSceneToAssembler(cutsceneFilePath)


def NewCutscene():
    """ Creates a cutscene """
    saveDialog = mobu.FBFilePopup()
    saveDialog.Caption = "Create a New Cutscene"
    saveDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
    saveDialog.Filter = "*.cutscene" 
    
    saveResult = saveDialog.Execute()
    
    if saveResult:
        cutsceneFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
        
        try:
            fstream = open(cutsceneFilename, 'w')
            
            # Create the assets directory.
            os.makedirs('{0}\\assets'.format(saveDialog.Path))
            
        except:
            pass
        
        # Ensure the file handle closes no matter what.    
        finally:
            fstream.close()
        return saveDialog.Path
    else:    
        return None


def OpenCutscene():
    """ Opens a cutscene """
    openDialog = mobu.FBFilePopup()
    openDialog.Caption = "Choose a Cutscene File"
    openDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen
    openDialog.Filter = "*.cutscene" 
    openDialog.Path = r"{0}\art\animation\cutscene\!!scenes\Test\Staging_Prototype".format(RS.Config.Project.Path.Root)
    
    openResult = openDialog.Execute()
    
    if openResult:
        cutsceneFilename = '{0}\\{1}'.format(openDialog.Path, openDialog.FileName)
        RS.Core.Reference.New.Scene.loadCutscene(cutsceneFilename)


def SaveCutscene():
    """ Saves the current cutscene """
    RS.Core.Reference.New.Scene.saveCutscene()


def SaveCutsceneAs():
    """ Save the current cutscene under a different name """
    saveDialog = mobu.FBFilePopup()
    saveDialog.Caption = "Choose a Cutscene File"
    saveDialog.Style = mobu.FBFilePopupStyle.kFBFilePopupSave
    saveDialog.Filter = "*.cutscene" 
    
    saveResult = saveDialog.Execute()
    
    if saveResult:
        cutsceneFilename = '{0}\\{1}'.format(saveDialog.Path, saveDialog.FileName)
        RS.Core.Reference.New.Scene.saveCutsceneAs(cutsceneFilename)


def AddAssetsToCutscene():
    """ Adds assets to the cutscene. UNDER DEVELOPMENT. DOES NOT WORK"""
    pass


def ActivateFFXConstraints():
    """ Activates the FFX constraints """
    import RS.Core.Face.ActivateFFXConstraints
    RS.Core.Face.ActivateFFXConstraints.rs_ActivateFFX()


def DeactivateFFXConstraints():
    """ Deactivates the FFX Constraints """
    import RS.Core.Face.DeactivateFFXConstraints
    RS.Core.Face.DeactivateFFXConstraints.rs_DeactivateFFX()

# Mocap Commands
# I will delete these once we get our Mocap Toolset UI working
# Need this in the meantime so Kelly can easily find all the scripts


def AddBasePrevizProperty():
    """ Adds 'basepreviz' custom property to all objects in file """
    if 'RS.Core.Mocap.Previz.AddBasePrevizProperty' not in sys.modules:
        import RS.Core.Mocap.Previz.AddBasePrevizProperty

    else:
        module = sys.modules['RS.Core.Mocap.Previz.AddBasePrevizProperty']
        reload(module)


def Play():
    """ Play player controls """
    if 'RS.Core.Mocap.Previz.Play' not in sys.modules:
        import RS.Core.Mocap.Previz.Play

    else:
        module = sys.modules['RS.Core.Mocap.Previz.Play']
        reload(module)


def RecordOverwrite():
    """ Record & overwrite player controls """
    if 'RS.Core.Mocap.Previz.RecordOverwrite' not in sys.modules:
        import RS.Core.Mocap.Previz.RecordOverwrite

    else:
        module = sys.modules['RS.Core.Mocap.Previz.RecordOverwrite']
        reload(module)


def RecordOverwritePlay():
    """ Record, overwrite & play player controls """

    if 'RS.Core.Mocap.Previz.RecordOverwritePlay' not in sys.modules:
        import RS.Core.Mocap.Previz.RecordOverwritePlay

    else:
        module = sys.modules['RS.Core.Mocap.Previz.RecordOverwritePlay']
        reload(module)


def AddGiantDevice():
    """ Adds the giant device to the scene """

    if 'RS.Core.Mocap.Previz.AddGiantDevice' not in sys.modules:
        import RS.Core.Mocap.Previz.AddGiantDevice

    else:
        module = sys.modules['RS.Core.Mocap.Previz.AddGiantDevice']
        reload(module)


def AddSpaceBallDevice():
    """
    -- DEPRECATED --
    Adds the space ball device
    """
    if 'RS.Core.Mocap.Previz.AddSpaceBallDevice' not in sys.modules:
        import RS.Core.Mocap.Previz.AddSpaceBallDevice

    else:
        module = sys.modules['RS.Core.Mocap.Previz.AddSpaceBallDevice']
        reload(module)        


def GiantDeviceOn():
    """ Turns on Giant Device Sets 'Online', 'Recording' and 'Live' options to ON """
    if 'RS.Core.Mocap.Previz.GiantDeviceOn' not in sys.modules:
        import RS.Core.Mocap.Previz.GiantDeviceOn

    else:
        module = sys.modules['RS.Core.Mocap.Previz.GiantDeviceOn']
        reload(module)


def GiantDeviceOff():
    """ 
    Turns off the Giant Device and sets 'Online', 'Recording' and 'Live' options to OFF for all 'giant studios device'
    """
    if 'RS.Core.Mocap.Previz.GiantDeviceOff' not in sys.modules:
        import RS.Core.Mocap.Previz.GiantDeviceOff

    else:
        module = sys.modules['RS.Core.Mocap.Previz.GiantDeviceOff']
        reload(module)


def NonGiantDevicesOn():
    """ Sets 'Online', 'Recording' and 'Live' options to ON for all devices, apart from the 'giant studios device' """
    if 'RS.Core.Mocap.Previz.NonGiantDevicesOn' not in sys.modules:
        import RS.Core.Mocap.Previz.NonGiantDevicesOn

    else:
        module = sys.modules['RS.Core.Mocap.Previz.NonGiantDevicesOn']
        reload(module)


def NonGiantDevicesOff():
    """ Sets 'Online', 'Recording' and 'Live' options to OFF for all devices, apart from the 'giant studios device' """
    if 'RS.Core.Mocap.Previz.NonGiantDevicesOff' not in sys.modules:
        import RS.Core.Mocap.Previz.NonGiantDevicesOff

    else:
        module = sys.modules['RS.Core.Mocap.Previz.NonGiantDevicesOff']
        reload(module)


def DeleteGiantDevice():
    """ Turns off and deletes the giant device from the scene """
    if 'RS.Core.Mocap.Previz.DeleteGiantDevice' not in sys.modules:
        import RS.Core.Mocap.Previz.DeleteGiantDevice

    else:
        module = sys.modules['RS.Core.Mocap.Previz.DeleteGiantDevice']
        reload(module)


def DeleteDevices():
    """ Turns off and deletes all devices from the scene """
    if 'RS.Core.Mocap.Previz.DeleteDevices' not in sys.modules:
        import RS.Core.Mocap.Previz.DeleteDevices

    else:
        module = sys.modules['RS.Core.Mocap.Previz.DeleteDevices']
        reload(module)


def CleanMocapPreviz():
    """
    Deletes items with a particular custom property and launches a UI displaying what objects are not part of
    ref system and if they have animation or not
    """
    if 'RS.Core.Mocap.Previz.CleanMocapPreviz' not in sys.modules:
        import RS.Core.Mocap.Previz.CleanMocapPreviz

    else:
        module = sys.modules['RS.Core.Mocap.Previz.CleanMocapPreviz']
        reload(module)


def CreateToybox():
    """ Creates Toybox object. Sets materials, shaders and group. """
    if 'RS.Core.Mocap.Previz.CreateToybox' not in sys.modules:
        import RS.Core.Mocap.Previz.CreateToybox

    else:
        module = sys.modules['RS.Core.Mocap.Previz.CreateToybox']
        reload(module)


def PrepareLattice():
    """ Prepares the ground and stage, which the user selects, for the lattice setup in giant """
    if 'RS.Core.Mocap.Previz.PrepareLattice' not in sys.modules:
        import RS.Core.Mocap.Previz.PrepareLattice

    else:
        module = sys.modules['RS.Core.Mocap.Previz.PrepareLattice']
        reload(module)


def SaveSelectedCameras():
    """ Saves out cameras, along with their linked components, as a separate file """
    if 'RS.Core.Mocap.Previz.SaveSelectedCameras' not in sys.modules:
        import RS.Core.Mocap.Previz.SaveSelectedCameras

    else:
        module = sys.modules['RS.Core.Mocap.Previz.SaveSelectedCameras']
        reload(module)


def CreateExportCamera():
    """  Creates an export camera, using HB's CameraCore functions """
    if 'RS.Core.Mocap.Previz.CreateExportCamera' not in sys.modules:
        import RS.Core.Mocap.Previz.CreateExportCamera

    else:
        module = sys.modules['RS.Core.Mocap.Previz.CreateExportCamera']
        reload(module)


def gsSkelMatchMerge():
    """ Matches characters with existing gs skeletons in mocap P4, merges them in and activates inputs """
    if 'RS.Core.Mocap.Previz.gsSkelMatchMerge' not in sys.modules:
        import RS.Core.Mocap.Previz.gsSkelMatchMerge

    else:
        module = sys.modules['RS.Core.Mocap.Previz.gsSkelMatchMerge']
        reload(module)


def CamRockBake():
    """ Plotting CamRocks to a new camera """
    if 'RS.Core.Mocap.Previz.CamRockBake' not in sys.modules:
        import RS.Core.Mocap.Previz.CamRockBake

    else:
        module = sys.modules['RS.Core.Mocap.Previz.CamRockBake']
        reload(module)


def RTCRangeSave():
    """ Outputs current hard and soft ranges to a .txt file """
    if 'RS.Core.Mocap.Previz.RTCRangeSave' not in sys.modules:
        import RS.Core.Mocap.Previz.RTCRangeSave

    else:
        module = sys.modules['RS.Core.Mocap.Previz.RTCRangeSave']
        reload(module)


def RTCRangeLoad():
    """ Uploads existing .txt output (fromRTCRangeSave) and sets the hard and soft ranges based on the .txt info """
    if 'RS.Core.Mocap.Previz.RTCRangeLoad' not in sys.modules:
        import RS.Core.Mocap.Previz.RTCRangeLoad

    else:
        module = sys.modules['RS.Core.Mocap.Previz.RTCRangeLoad']
        reload(module)


def PlotMocapAssets():
    """ Plots all assets part of the Ref System Null """
    if 'RS.Core.Mocap.Previz.PlotMocapAssets' not in sys.modules:
        import RS.Core.Mocap.Previz.PlotMocapAssets

    else:
        module = sys.modules['RS.Core.Mocap.Previz.PlotMocapAssets']
        reload(module)


def RTEBaseSetup():
    """ Creates RTE cameras based on the number given by the user """
    if 'RS.Core.Mocap.Previz.RTEBaseSetup' not in sys.modules:
        import RS.Core.Mocap.Previz.RTEBaseSetup

    else:
        module = sys.modules['RS.Core.Mocap.Previz.RTEBaseSetup']
        reload(module)


def GrabWatchstarTakeName():
    """
    User sets the Watchstar Profile they want to get information from,
    then they can crete takes based off this XML information inside of MotionBuilder.
    """
    if 'RS.Core.Mocap.Previz.GrabWatchstarTakeName' not in sys.modules:
        import RS.Core.Mocap.Previz.GrabWatchstarTakeName

    else:
        module = sys.modules['RS.Core.Mocap.Previz.GrabWatchstarTakeName']
        reload(module)


def GrabWatchstarSetCoordinates():
    """ Get the Watchstar Set Co-ordinates. """
    if 'RS.Core.Mocap.Previz.GrabWatchstarSetCoordinates' not in sys.modules:
        import RS.Core.Mocap.Previz.GrabWatchstarSetCoordinates

    else:
        module = sys.modules['RS.Core.Mocap.Previz.GrabWatchstarSetCoordinates']
        reload(module)        


def PlotFacewareLiveHeads():
    """
    DEPRECATED
    Plots Faceware Live Heads
    """
    if 'RS.Core.Mocap.Previz.PlotFacewareLiveHeads' not in sys.modules:
        import RS.Core.Mocap.Previz.PlotFacewareLiveHeads

    else:
        module = sys.modules['RS.Core.Mocap.Previz.PlotFacewareLiveHeads']
        reload(module)           


def TurnoffLabel():
    """ Turns off the label name in the Viewer for the Producer Perspective, it overlaps the new HUD """
    if 'RS.Core.Mocap.Previz.TurnOffProducePerspectiveLabel' not in sys.modules:
        import RS.Core.Mocap.Previz.TurnOffProducePerspectiveLabel

    else:
        module = sys.modules['RS.Core.Mocap.Previz.TurnOffProducePerspectiveLabel']
        reload(module)  


# Resource Weekly Changes
def WeeklyResourceChanges():
    """
    Generates a report of changes to the <project>\\art\\animation\\resources\\ directory that happened in the last
    week.
    """
    if 'RS.Core.AssetSetup.Resource.WeeklyResourceChanges' not in sys.modules:
        import RS.Core.AssetSetup.Resource.WeeklyResourceChanges

    else:
        module = sys.modules['RS.Core.AssetSetup.Resource.WeeklyResourceChanges']
        reload(module)

# Empty Group cleanup
def CleanGroups():
    """ Removes empty groups from the current motion builder scene """
    if 'RS.Core.AssetSetup.Resource.GroupCleanup' not in sys.modules:
        import RS.Core.AssetSetup.Resource.GroupCleanup

    else:
        module = sys.modules['RS.Core.AssetSetup.Resource.GroupCleanup']
        reload(module)

# reset rollbones url:bugstar:
def ResetRollbones():
    """ Removes empty groups from the current motion builder scene """
    if 'RS.Core.Animation.resetRollbones' not in sys.modules:
        import RS.Core.Animation.resetRollbones

    else:
        module = sys.modules['RS.Core.Animation.resetRollbones']
        reload(module)


# remove py scripts from scene
def RemovePyScripts():
    """
    Removes components in the scene named after python scripts and removes empty folders
    """
    if 'RS.Core.AssetSetup.Resource.RemovePyScripts' not in sys.modules:
        import RS.Core.AssetSetup.Resource.RemovePyScripts

    else:
        module = sys.modules['RS.Core.AssetSetup.Resource.RemovePyScripts']
        reload(module)        


# FPS Camera
def FPSCamera():
    """ Sets up First Person Cameras """
    if 'RS.Core.AssetSetup.FPS_Cam_Setup' not in sys.modules:
        import RS.Core.AssetSetup.FPS_Cam_Setup

    else:
        module = sys.modules['RS.Core.AssetSetup.FPS_Cam_Setup']
        reload(module)


# FPS Camera Patch
def FPSCameraPatch():
    """ First Person Camera Patch """
    if 'RS.Core.AssetSetup.FPS_Cam_Patch' not in sys.modules:
        import RS.Core.AssetSetup.FPS_Cam_Patch

    else:
        module = sys.modules['RS.Core.AssetSetup.FPS_Cam_Patch']
        reload(module)


# Turn Off Grid on All Cameras
def GridOffForAllCameras():
    """ Turns off the grid for all the cameras in the scene """
    if 'RS.Core.Camera.GridOffForAllCameras' not in sys.modules:
        import RS.Core.Camera.GridOffForAllCameras

    else:
        module = sys.modules['RS.Core.Camera.GridOffForAllCameras']
        reload(module)


# Hoove Pivot
def RunCreateHoovePivot():
    if 'RS.Core.Character.hooves.runCreateHoovePivot' not in sys.modules:
        import RS.Core.Character.hooves.runCreateHoovePivot

    else:
        module = sys.modules['RS.Core.Character.hooves.runCreateHoovePivot']
        reload(module)

def RunDeleteHoovePivot():
    if 'RS.Core.Character.hooves.runDeleteHoovePivot' not in sys.modules:
        import RS.Core.Character.hooves.runDeleteHoovePivot

    else:
        module = sys.modules['RS.Core.Character.hooves.runDeleteHoovePivot']
        reload(module)


# QT Utility Commands
def QPrint(*args):
    """
    Workaround to have print statements output to the console when working
    inside code that creates/manipulates Qt UI

    Arguments:
        *args (list): anything that you want to see printed out to the console
    """
    print "".join([str(each) for each in args])
    pythonidelib.FlushOutput()