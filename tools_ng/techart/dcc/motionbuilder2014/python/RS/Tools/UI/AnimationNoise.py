from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.AnimationNoise
import RS.Tools.UI

## Event Handlers ##

class AnimationNoiseTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Animation Noise", size = [ 410, 940 ] )
        
        
    ## Layouts ##
    
    def CreateFrameRangeLayout( self, animNoiseOptions ):
        '''
        Sets up the frame range controls.
        '''
        
        frameRangeLyt = FBLayout()
        
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 10, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 66, FBAttachType.kFBAttachNone, '' )
        
        frameRangeCtrlLyt = FBVBoxLayout()
        
        frameRangeLyt.AddRegion( 'main', 'Frame Range', x, y, w, h )
        frameRangeLyt.SetBorder( 'main', FBBorderStyle.kFBStandardBorder, False, True, 1, 0, 90, 0 )
        frameRangeLyt.SetControl( 'main', frameRangeCtrlLyt )
        
        # Use active frame range
        useActiveFrameRangeBtn = FBButton()
        useActiveFrameRangeBtn.options = animNoiseOptions
        useActiveFrameRangeBtn.Caption = 'Use active frame range'
        useActiveFrameRangeBtn.Style = FBButtonStyle.kFBCheckbox
        useActiveFrameRangeBtn.OnClick.Add( self.OnUseActiveFrameRange_Clicked )
    
        # Frame start
        frameStartEndLyt = FBHBoxLayout()
        
        frameStartLbl = FBLabel()
        frameStartLbl.Caption = 'Frame Start:'
        
        frameStartBtn = FBEditNumber()
        frameStartBtn.Precision = 1.0
        frameStartBtn.options = animNoiseOptions
        frameStartBtn.OnChange.Add( self.OnFrameStart_Changed )
        
        frameEndLbl = FBLabel()
        frameEndLbl.Caption = 'Frame End:'
        
        frameEndBtn = FBEditNumber()
        frameEndBtn.Precision = 1.0
        frameEndBtn.options = animNoiseOptions
        frameEndBtn.OnChange.Add( self.OnFrameEnd_Changed )
        
        useActiveFrameRangeBtn.controls = [ frameStartLbl, frameStartBtn, frameEndLbl, frameEndBtn ]
        
        frameStartEndLyt.Add( frameStartLbl, 74 )
        frameStartEndLyt.Add( frameStartBtn, 48, space = -8 )
        frameStartEndLyt.Add( frameEndLbl, 74, space = 30 )
        frameStartEndLyt.Add( frameEndBtn, 48, space = -12 )
        
        frameRangeCtrlLyt.Add( useActiveFrameRangeBtn, 24 )
        frameRangeCtrlLyt.Add( frameStartEndLyt, 24 )
        
        return frameRangeLyt
        
    def CreateTrackGroup( self, animNoiseOptionsTransformComponent, tabLyt, regionName, attachTo = '' ):
        '''
        Sets up a group of controls for each track, i.e. X, Y, Z for
        translation, rotation and scale.
        '''
        
        lyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 4, FBAttachType.kFBAttachLeft, '' )
        
        if attachTo == '':
            y = FBAddRegionParam( 4, FBAttachType.kFBAttachTop, attachTo )
            
        else:
            y = FBAddRegionParam( 16, FBAttachType.kFBAttachBottom, attachTo )
            
        w = FBAddRegionParam( -4, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 204, FBAttachType.kFBAttachNone, '' )
        
        tabLyt.AddRegion( regionName, regionName[ -1 ], x, y, w, h )
        tabLyt.SetBorder( regionName, FBBorderStyle.kFBStandardBorder, False, True, 1, 0, 90, 0 )
        tabLyt.SetControl( regionName, lyt )
        
        # Enabled
        enabledCtrl = FBButton()
        enabledCtrl.State = animNoiseOptionsTransformComponent.enabled
        enabledCtrl.options = animNoiseOptionsTransformComponent
        enabledCtrl.Caption = regionName[ -1 ]
        enabledCtrl.Style = FBButtonStyle.kFBCheckbox
        
        # Amplitude Min / Max
        amplitudeLyt = FBHBoxLayout()
        
        amplitudeMinLbl = FBLabel()
        amplitudeMinLbl.Caption = 'Amplitude Min:'
        
        amplitudeMinCtrl = FBEditNumber()
        amplitudeMinCtrl.Value = animNoiseOptionsTransformComponent.amplitudeMin
        amplitudeMinCtrl.options = animNoiseOptionsTransformComponent
        amplitudeMinCtrl.OnChange.Add( self.OnAmplitudeMin_Changed )
    
        amplitudeMaxLbl = FBLabel()
        amplitudeMaxLbl.Caption = 'Amplitude Max:'
        
        amplitudeMaxCtrl = FBEditNumber()
        amplitudeMaxCtrl.Value = animNoiseOptionsTransformComponent.amplitudeMax
        amplitudeMaxCtrl.options = animNoiseOptionsTransformComponent
        amplitudeMaxCtrl.OnChange.Add( self.OnAmplitudeMax_Changed )
        
        amplitudeLyt.Add( amplitudeMinLbl, 70 )
        amplitudeLyt.Add( amplitudeMinCtrl, 40 )
        amplitudeLyt.Add( amplitudeMaxLbl, 72, space = 30 )
        amplitudeLyt.Add( amplitudeMaxCtrl, 40, space = 8 )
        
        # Frame Step
        frameStepLyt = FBHBoxLayout()
        
        frameStepLbl = FBLabel()
        frameStepLbl.Caption = 'Frame Step:'
        
        frameStepCtrl = FBEditNumber()
        frameStepCtrl.Value = animNoiseOptionsTransformComponent.frameStep
        frameStepCtrl.Precision = 1
        frameStepCtrl.Min = 1
        frameStepCtrl.options = animNoiseOptionsTransformComponent
        frameStepCtrl.OnChange.Add( self.OnFrameStep_Changed )
        
        frameStepLyt.Add( frameStepLbl, 70 )
        frameStepLyt.Add( frameStepCtrl, 40 )
        
        # Hold
        holdLyt = FBHBoxLayout()
        
        enableHoldCtrl = FBButton()
        enableHoldCtrl.Caption = 'Apply Holds'
        enableHoldCtrl.Style = FBButtonStyle.kFBCheckbox
        enableHoldCtrl.State = animNoiseOptionsTransformComponent.hold
        enableHoldCtrl.OnClick.Add( self.OnApplyHold_Clicked )
        enableHoldCtrl.options = animNoiseOptionsTransformComponent
        
        holdPctLbl = FBLabel()
        holdPctLbl.Caption = 'Percentage:'
        
        holdPctCtrl = FBEditNumber()
        holdPctCtrl.Precision = 1
        holdPctCtrl.Min = 0
        holdPctCtrl.Max = 100
        holdPctCtrl.Value = animNoiseOptionsTransformComponent.holdPercentage
        holdPctCtrl.options = animNoiseOptionsTransformComponent
        holdPctCtrl.OnChange.Add( self.OnHoldPercentage_Changed )
        
        holdDurationLyt = FBHBoxLayout()
        
        holdDurationMinLbl = FBLabel()
        holdDurationMinLbl.Caption = 'Duration Min:'
        
        holdDurationMinCtrl = FBEditNumber()
        holdDurationMinCtrl.Precision = 1
        holdDurationMinCtrl.Min = 1
        holdDurationMinCtrl.Value = animNoiseOptionsTransformComponent.holdDurationMin
        holdDurationMinCtrl.options = animNoiseOptionsTransformComponent
        holdDurationMinCtrl.OnChange.Add( self.OnHoldDurationMin_Changed )
        
        holdDurationMaxLbl = FBLabel()
        holdDurationMaxLbl.Caption = 'Duration Max:'
        
        holdDurationMaxCtrl = FBEditNumber()
        holdDurationMaxCtrl.Precision = 1
        holdDurationMaxCtrl.Min = 1
        holdDurationMaxCtrl.Value = animNoiseOptionsTransformComponent.holdDurationMax
        holdDurationMaxCtrl.options = animNoiseOptionsTransformComponent
        holdDurationMaxCtrl.OnChange.Add( self.OnHoldDurationMax_Changed )    
        
        holdDurationLyt.Add( holdDurationMinLbl, 70 )
        holdDurationLyt.Add( holdDurationMinCtrl, 40 )
        holdDurationLyt.Add( holdDurationMaxLbl, 70, space = 34 )
        holdDurationLyt.Add( holdDurationMaxCtrl, 40 )     
        
        enableHoldCtrl.controls = [ holdPctLbl, holdPctCtrl, holdDurationMinLbl, holdDurationMinCtrl, holdDurationMaxLbl, holdDurationMaxCtrl ]
        self.EnableControls( enableHoldCtrl.controls, enableHoldCtrl.State )
    
        holdLyt.Add( holdPctLbl, 70 )
        holdLyt.Add( holdPctCtrl, 40 )
        
        curveLyt = FBHBoxLayout()
        
        curveTypeLbl = FBLabel()
        curveTypeLbl.Caption = 'Curve Type:'
        
        curveTypeCtrl = FBList()
        curveTypeCtrl.Style = FBListStyle.kFBDropDownList
        curveTypeCtrl.options = animNoiseOptionsTransformComponent
        curveTypeCtrl.OnChange.Add( self.OnCurveType_Changed )
        
        curveLyt.Add( curveTypeLbl, 69 )
        curveLyt.Add( curveTypeCtrl, 140 )
        
        curveTypes = RS.Tools.AnimationNoise.CURVE_TYPES.keys()
        curveTypes.sort()
        
        idx = 0
        
        for curveType in curveTypes:
            curveTypeCtrl.Items.append( curveType )
            
            if curveType == 'None':
                curveTypeCtrl.Selected( idx, True )
            
            idx += 1
            
        # Set default
        
        
        # Setup control references
        enabledCtrl.OnClick.Add( self.OnTrackEnable_Clicked )
        
        enabledCtrl.controls = [ amplitudeMinLbl,
                                 amplitudeMinCtrl,
                                 amplitudeMaxLbl,
                                 amplitudeMaxCtrl,
                                 frameStepLbl,
                                 frameStepCtrl,
                                 enableHoldCtrl,
                                 holdPctLbl,
                                 holdPctCtrl,
                                 holdDurationMinLbl,
                                 holdDurationMinCtrl,
                                 holdDurationMaxLbl,
                                 holdDurationMaxCtrl,
                                 curveTypeLbl,
                                 curveTypeCtrl ]
                                 
        self.EnableControls( enabledCtrl.controls, enabledCtrl.State )                    
    
        # Main layout
        lyt.Add( enabledCtrl, 24 )
        lyt.Add( amplitudeLyt, 24 )
        lyt.Add( frameStepLyt, 24 )
        lyt.Add( curveLyt, 24 )
        lyt.Add( enableHoldCtrl, 24 )
        lyt.Add( holdLyt, 24 )
        lyt.Add( holdDurationLyt, 24 )
         
    def CreateTabPage( self, animNoiseOptionsTransform, tabCtrl, tabName ):
        '''
        Creates a tab page for translation, rotation and scale.
        '''
        
        tabLyt = FBLayout()
        
        x = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 0, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        
        tabRegionName = 'tabPage{0}'.format( tabName )
        
        xRegionName = '{0}X'.format( tabRegionName )
        yRegionName = '{0}Y'.format( tabRegionName )
        zRegionName = '{0}Z'.format( tabRegionName )
        
        # Create a group of controls for each track component.
        self.CreateTrackGroup( animNoiseOptionsTransform.x, tabLyt, xRegionName )
        self.CreateTrackGroup( animNoiseOptionsTransform.y, tabLyt, yRegionName, xRegionName )
        self.CreateTrackGroup( animNoiseOptionsTransform.z, tabLyt, zRegionName, yRegionName )
        
        tabCtrl.Add( tabName, tabLyt )
        
    def CreateTabs( self, animNoiseOptions ):
        tabCtrl = FBTabControl()
        self.CreateTabPage( animNoiseOptions.translation, tabCtrl, 'Translation' )
        self.CreateTabPage( animNoiseOptions.rotation, tabCtrl, 'Rotation' )
        self.CreateTabPage( animNoiseOptions.scale, tabCtrl, 'Scale' )
        tabCtrl.SetContent( 0 )
        
        return tabCtrl
        
    def CreateAnimationLayerLayout( self, animNoiseOptions ):
        lyt = FBLayout()
        
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( 10, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 66, FBAttachType.kFBAttachNone, '' )
        
        ctrlLyt = FBVBoxLayout()
        
        lyt.AddRegion( 'main', '', x, y, w, h )
        lyt.SetBorder( 'main', FBBorderStyle.kFBStandardBorder, False, True, 1, 0, 90, 0 )
        lyt.SetControl( 'main', ctrlLyt )
        
        # Create new animation layer
        createAnimLayerLyt = FBHBoxLayout()
        
        createAnimLayerCtrl = FBButton()
        createAnimLayerCtrl.Caption = 'Create New Animation Layer:'
        createAnimLayerCtrl.Style = FBButtonStyle.kFBCheckbox
        createAnimLayerCtrl.State = animNoiseOptions.createAnimationLayer
        createAnimLayerCtrl.options = animNoiseOptions
        createAnimLayerCtrl.OnClick.Add( self.OnCreateAnimationLayer_Clicked )
        
        createAnimLayerNameCtrl = FBEdit()
        createAnimLayerNameCtrl.Text = animNoiseOptions.animationLayerName
        createAnimLayerNameCtrl.options = animNoiseOptions
        createAnimLayerNameCtrl.OnChange.Add( self.OnAnimationLayerName_Changed )
        
        createAnimLayerCtrl.animLayerNameCtrl = createAnimLayerNameCtrl    
        
        createAnimLayerLyt.Add( createAnimLayerCtrl, 166, space = 0 )
        createAnimLayerLyt.AddRelative( createAnimLayerNameCtrl, 1.0 )
        
        createAnimLayerCtrl.controls = [ createAnimLayerNameCtrl ]
        self.EnableControls( createAnimLayerCtrl.controls, createAnimLayerCtrl.State ) 
        
        # Apply to animation layer
        applyToAnimLayerLyt = FBHBoxLayout()
        
        applyToAnimLayerLbl = FBLabel()
        applyToAnimLayerLbl.Caption = 'Apply to Animation Layer:'
        
        global applyToAnimLayerCtrl
        applyToAnimLayerCtrl = FBList()
        applyToAnimLayerCtrl.Style = FBListStyle.kFBDropDownList
        applyToAnimLayerCtrl.options = animNoiseOptions
        applyToAnimLayerCtrl.OnChange.Add( self.OnAnimLayerList_Changed )
    
        self.RefreshAnimLayerListCtrl()
        
        # Set initial to first one in the list.
        animNoiseOptions.animationLayerName = applyToAnimLayerCtrl.Items[ 0 ]
        
        animLayerListRefreshCtrl = FBButton()
        animLayerListRefreshCtrl.Caption = 'Refresh'
        animLayerListRefreshCtrl.OnClick.Add( self.OnRefreshAnimLayerList_Clicked )
        
        createAnimLayerCtrl.animLayerListCtrl = applyToAnimLayerCtrl
        createAnimLayerCtrl.refreshAnimListCtrl = animLayerListRefreshCtrl
        createAnimLayerCtrl.animLayerListLbl = applyToAnimLayerLbl
        
        applyToAnimLayerLyt.Add( applyToAnimLayerLbl, 160 )
        applyToAnimLayerLyt.AddRelative( applyToAnimLayerCtrl, 1.0 )
        applyToAnimLayerLyt.Add( animLayerListRefreshCtrl, 50 )
        
        ctrlLyt.Add( createAnimLayerLyt, 24 )
        ctrlLyt.Add( applyToAnimLayerLyt, 24 )
        
        return lyt
        
    def Create( self, mainLyt ):
        '''
        Primary layout for the tool.
        '''
        
        # Animation noise options.
        global animNoiseOptions
        animNoiseOptions = RS.Tools.AnimationNoise.AnimationNoiseOptions()
        
        lyt = FBVBoxLayout()
         
        x = FBAddRegionParam( 4, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( self.BannerHeight, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -4, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        mainLyt.AddRegion( 'main', 'main', x, y, w, h )
        mainLyt.SetControl( 'main', lyt )
        
        # Apply Noise
        applyNoiseCtrl = FBButton()
        applyNoiseCtrl.Caption = 'Apply Noise to Selection'
        applyNoiseCtrl.options = animNoiseOptions
        applyNoiseCtrl.OnClick.Add( self.OnApplyNoiseToSelection_Clicked )
        
        # Frame range
        frameRangeLyt = self.CreateFrameRangeLayout( animNoiseOptions )
        
        # Animation layer
        animLayerLyt = self.CreateAnimationLayerLayout( animNoiseOptions )
        
        # Tab pages
        tabCtrl = self.CreateTabs( animNoiseOptions )
        
        lyt.Add( applyNoiseCtrl, 32 )
        lyt.Add( frameRangeLyt, 80 )
        lyt.Add( animLayerLyt, 80 )
        lyt.AddRelative( tabCtrl, 1.0 )
        
        
    ## Functions ##
        
    def EnableControls( self, controls, state ):
        holdControl = None
        
        for control in controls:
            control.Enabled = state
            
            if hasattr( control, 'controls' ) and control.Caption == 'Apply Holds':
                holdControl = control
                
        # Gotta treat the hold controls special.
        if holdControl:
            holdControl.Enabled = state
            
            if state == False:
                for control in holdControl.controls:
                    control.Enabled = False
                    
            else:
                for control in holdControl.controls:
                    control.Enabled = holdControl.State
    
    def RefreshAnimLayerListCtrl( self ):
        global applyToAnimLayerCtrl
        
        numItems = len( applyToAnimLayerCtrl.Items )
                
        for idx in range( numItems ):
            applyToAnimLayerCtrl.Items.pop()
        
        animLayerNames = self.GetAnimationLayerNames()
        
        for animLayerName in animLayerNames:
            applyToAnimLayerCtrl.Items.append( animLayerName )
    
    def GetAnimationLayerNames( self ):
        animLayerNames = []
        
        numLayers = FBSystem().CurrentTake.GetLayerCount()
        
        for layerIdx in reversed( range( numLayers ) ):
            animLayerNames.append( FBSystem().CurrentTake.GetLayer( layerIdx ).Name )
            
        return animLayerNames
    
        
    ## Event Handlers ##

    
    def OnApplyNoiseToSelection_Clicked( self, source, event ):
        modelList = FBModelList()
        FBGetSelectedModels( modelList )
        
        for model in modelList:
            RS.Tools.AnimationNoise.addNoise( model, source.options )
            
        self.RefreshAnimLayerListCtrl()
    
    
    def OnUseActiveFrameRange_Clicked( self, source, event ):
        self.EnableControls( source.controls, not source.State )
        source.options.useActiveFrameRange = True if source.State == 1 else False
    
    
    def OnFrameEnd_Changed( self, source, event ):
        source.options.frameEnd = source.Value
            
    
    def OnFrameStart_Changed( self, source, event ):
        source.options.frameStart = source.Value

           
    def OnTrackEnable_Clicked( self, source, event ):
        self.EnableControls( source.controls, source.State )
        source.options.enabled = True if source.State == 1 else False
        
    
    def OnApplyHold_Clicked( self, source, event ):
        self.EnableControls( source.controls, source.State )
        source.options.hold = True if source.State == 1 else False
        
    
    def OnFrameStep_Changed( self, source, event ):
        source.options.frameStep = source.Value
        
    
    def OnAmplitudeMin_Changed( self, source, event ):
        source.options.amplitudeMin = source.Value
    
    
    def OnAmplitudeMax_Changed( self, source, event ):
        source.options.amplitudeMax = source.Value
        
    
    def OnHoldDurationMin_Changed( self, source, event ):
        source.options.holdDurationMin = source.Value
        
    
    def OnHoldDurationMax_Changed( self, source, event ):
        source.options.holdDurationMax = source.Value
        
    
    def OnHoldPercentage_Changed( self, source, event ):
        source.options.holdPercentage = source.Value
        
    
    def OnCreateAnimationLayer_Clicked( self, source, event ):
        self.EnableControls( source.controls, source.State )
        source.options.createAnimationLayer = True if source.State == 1 else False
        
        if source.State == 1:
            source.options.animationLayerName = source.animLayerNameCtrl.Text
            
        else:
            source.options.animationLayerName = source.animLayerListCtrl.Items[ source.animLayerListCtrl.ItemIndex ]
            
        source.animLayerListCtrl.Enabled = not source.State
        source.refreshAnimListCtrl.Enabled = not source.State
        source.animLayerListLbl.Enabled = not source.State
        
    
    def OnAnimationLayerName_Changed( self, source, event ):
        source.options.animationLayerName = source.Text

       
    def OnRefreshAnimLayerList_Clicked( self, source, event ):
        self.RefreshAnimLayerListCtrl()

           
    def OnAnimLayerList_Changed( self, source, event ):
        source.options.animationLayerName = source.Items[ source.ItemIndex ]

           
    def OnCurveType_Changed( self, source, event ):
        curveType = source.Items[ source.ItemIndex ]
        source.options.curveType = RS.Tools.AnimationNoise.CURVE_TYPES[ curveType ]
        

def Run( show = True ):
    tool = AnimationNoiseTool()
    
    if show:
        tool.Show()
        
    return tool