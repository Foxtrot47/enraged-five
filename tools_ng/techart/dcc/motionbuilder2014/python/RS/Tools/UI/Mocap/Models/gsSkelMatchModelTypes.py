from PySide import QtCore, QtGui

import pyfbsdk as mobu

import os
import glob

from RS import Config, Globals
from RS.Tools.UI.Mocap import const
from RS.Tools.UI.Mocap.Models import gsSkelMatchModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel
from RS import ProjectData


class GSSkelInfoModel(baseModel.BaseModel):
    '''
    model types generated and set to data for the the model item
    '''
    def __init__(self, parent=None):
        self._defaultGSSkelPath = None
        self._defaultGSSkelName = ProjectData.data.GetDefaultGSSkelName()
        self._gsSkeletonNameList = []
        self._gsSkeletonPathList = []
        super(GSSkelInfoModel, self).__init__(parent=parent)

    def getHeadings(self):
        '''
        heading strings to be displayed in the view
        '''
        return ['Character Name', 'GS Skeleton Match', 'Actor Name', 'Actor Color']

    def _getCharacterList(self):
        '''
        returns: a list of FBCharacter nodes in the scene
        '''
        characterList = []
        for character in Globals.Characters:
            if '_gs' in character.Name or character.Name.startswith('M'):
                continue
            characterList.append(character)
        return characterList

    def _getGSSkelLists(self):
        '''
        returns: a dict of gsSkel Names and paths
        '''        
        GSSkelNameList = []
        GSSkelPathList = []
        # get project name
        project = str(Config.Project.Name).lower()
        gsPath = None
        
        # paths to gs skel files
        try:
            gsPath = ProjectData.data.GetGsSkeletonPath()
        except:
            pass
        
        if not gsPath:
            return

        # using local files for this. Having problems accessing depot files with the mocap server.
        tempFileList = glob.glob(gsPath)
        for filePath in tempFileList:
            #if '_gs' in filePath: WAITING ON BUG 2329722
            # make a list of all gs skel path names
            itemSplitName = str(filePath).split('\\')
            fbxNameSplit = itemSplitName[-1].split('.')
            gsSkelString = fbxNameSplit[0]
            # append gsSkeleton lists
            if gsSkelString.lower().endswith('_gs') or gsSkelString.lower().startswith('m'):
                self._gsSkeletonNameList.append(gsSkelString)
                self._gsSkeletonPathList.append(filePath)

        return GSSkelNameList

    def GetGsSkelNameList(self):
        '''
        returns a list of strings
        '''
        return self._gsSkeletonNameList

    def findIndexByChild(self, item):
        '''
        returns index (int)
        '''
        return self.childItems.index(item)

    def setupModelData(self, parent):
        '''
        model type data sent to model item
        '''
        gsSkeletonInformationDict = {}
        self._getGSSkelLists()
        characterList = self._getCharacterList()

        for character in characterList:
            gsSkelMatched = False
            getCharacterNamespace = character.LongName.split(':')
            characterNamespace = getCharacterNamespace[0]
            # check if a gs skel has already been matched with this character and has a color
            actorName = None
            actorColorString = 'Black'
            characterHips = character.GetModel(mobu.FBBodyNodeId.kFBHipsNodeId)
            if characterHips:
                actorNamespaceProperty = characterHips.PropertyList.Find('gs match namespace')
                actorColorStringProperty = characterHips.PropertyList.Find('actor color string')
                if actorNamespaceProperty:
                    actorNamespace = actorNamespaceProperty.Data
                    actorNamespaceSplit = actorNamespace.split(':')
                    actorName = actorNamespaceSplit[0]
                if actorColorStringProperty:
                    actorColorString = actorColorStringProperty.Data

            # get matching gs name
            for gsSkelName in self._gsSkeletonNameList:
                if '{0}_'.format(character.Name) in gsSkelName:
                    gsSkelMatched = True
                    # create dataItem for treeview
                    dataItem = gsSkelMatchModelItem.gsSkelMatchTextModelItem(
                                                                             characterNamespace,
                                                                             character,
                                                                             gsSkelName,
                                                                             self._gsSkeletonPathList,
                                                                             actorName,
                                                                             actorColorString,
                                                                             parent=parent
                                                                            )
                    # append tree view with item
                    parent.appendChild(dataItem)
                    break
            if not gsSkelMatched:
                # if character name doesnt match any gs skel name, use default name 'M01'
                dataItem = gsSkelMatchModelItem.gsSkelMatchTextModelItem(
                                                                        characterNamespace,
                                                                         character,
                                                                         self._defaultGSSkelName,
                                                                         self._gsSkeletonPathList,
                                                                         actorName,
                                                                         actorColorString,
                                                                         parent=parent
                                                                        )
                # append tree view with item
                parent.appendChild(dataItem)

    def flags(self, index):
        '''
        flags added to determine column properties
        
        args: index (int)
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        column = index.column()
        flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        if column == 0:
            return flags | QtCore.Qt.ItemIsUserCheckable
        elif column in [1, 2, 3]:
            return flags | QtCore.Qt.ItemIsEditable
        else:
            return flags
