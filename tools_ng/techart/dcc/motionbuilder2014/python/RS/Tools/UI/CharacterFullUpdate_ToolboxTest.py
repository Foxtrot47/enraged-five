'''

 Script Path: RS/Tools/UI/CharacterFullUpdate_ToolboxTest.py

 Written And Maintained By: Kathryn Bodey

 Created: 04.12.2014

 Description: temp ui for testing - modules will be merged into ref system when confirmed working.

'''

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils
import RS.Globals
import RS.Tools.UI

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character


class TemporaryCharacterUpdateUI( RS.Tools.UI.Base ):
    
    def __init__( self ):

        RS.Tools.UI.Base.__init__( self, 'Character Update - Temp', size = [ 230, 130 ]  )

        self.CharacterDropdown = None
        self.CharacterList = []

        self.manager = Manager()

        self.characters = self.manager.GetReferenceListByType( Character )


    def Create( self, mainLayout ):
        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachBottom,"" )
        w = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"" )
        h = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        mainLayout.AddRegion( "mainLayout", "mainLayout", x, y, w, h )

        self.CharacterDropdown = FBList()
        self.CharacterDropdown.Style = FBListStyle.kFBDropDownList 
        self.CharacterDropdown.Hint = 'Select character to update'
        X = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 40, FBAttachType.kFBAttachTop, "" )
        W = FBAddRegionParam( 200, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "self.CharacterDropdown", "self.CharacterDropdown", X, Y, W, H )
        mainLayout.SetControl( "self.CharacterDropdown", self.CharacterDropdown )

        for i in self.characters:
            self.CharacterDropdown.Items.append( i.Namespace )

        updateCharacterButton = FBButton()
        updateCharacterButton.Caption = 'Update Character'
        updateCharacterButton.Hint = 'Fully updates the character\n\nNew one is brought in,\nanimation copied across and\nold one deleted.'
        X = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, "" )
        Y = FBAddRegionParam( 15, FBAttachType.kFBAttachBottom, "self.CharacterDropdown" )
        W = FBAddRegionParam( 190, FBAttachType.kFBAttachNone, "" )
        H = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( "updateCharacterButton", "updateCharacterButton", X, Y, W, H )
        mainLayout.SetControl( "updateCharacterButton", updateCharacterButton )  
        updateCharacterButton.OnClick.Add( self.UpdateCharacterButtonCallback )

    def UpdateCharacterButtonCallback( self, control, event ):
        selectedCharacter = None
        for i in range( len( self.CharacterDropdown.Items ) ):
            if self.CharacterDropdown.IsSelected( i ):
                for character in self.characters:
                    if self.CharacterDropdown.Items[i] == character.Namespace:
                        selectedCharacter = character
                        break

        if selectedCharacter:
            self.manager.UpdateReferences( selectedCharacter, True )


def Run( show = True ):
    tool = TemporaryCharacterUpdateUI()

    if show:
        tool.Show()

    return tool
