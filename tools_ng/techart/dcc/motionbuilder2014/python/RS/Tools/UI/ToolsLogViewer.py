import os
import sys

from PySide import QtCore, QtGui

import RS.Tools.UI
import RS.Utils.Logging


class LoggingTableModel( QtCore.QAbstractTableModel ):
    def __init__( self, parent, header, data, *args ):
        QtCore.QAbstractTableModel.__init__( self, parent, *args )
        
        self.__header = header
        self.__data = data
        
    def rowCount( self, parent ):
        return len( self.__data )
    
    def columnCount( self, parent ):
        return len( self.__header )
    
    def data( self, index, role ):
        if not index.isValid():
            return None
        
        elif role != QtCore.Qt.DisplayRole:
            return None
        
        return self.__data[ index.row() ][ index.column() ]
    
    def headerData( self, col, orientation, role ):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.__header[ col ]
        
        return None
    
    def sort( self, col, order ):
        self.emit( QtCore.SIGNAL( "layoutAboutToBeChanged()" ) )
        self.__data = sorted( self.__data,
            key = operator.itemgetter( col ) )
        
        if order == QtCore.Qt.DescendingOrder:
            self.__data.reverse()
            
        self.emit( QtCore.SIGNAL( "layoutChanged()" ) )

class MainWidget( QtGui.QWidget ):
    def __init__( self ):
        QtGui.QWidget.__init__( self )
        
        mainLayout = QtGui.QVBoxLayout()
        self.setLayout( mainLayout )
        
        columns = RS.Utils.Logging.GetColumns()
        allRecords = RS.Utils.Logging.GetAllRecords()
        
        records = []
        
        for record in allRecords:
            data = []
            
            for column in columns:
                if column == RS.Utils.Logging.Column.Application:
                    data.append( RS.Utils.Logging.ApplicationDisplayName[ record.Application ] )                
                    
                elif column == RS.Utils.Logging.Column.Type:
                    data.append( RS.Utils.Logging.TypeDisplayName[ record.Type ] )
                    
                elif column == RS.Utils.Logging.Column.Timestamp:
                    data.append( str( record.Timestamp ) )
                    
                elif column == RS.Utils.Logging.Column.TotalTime:
                    data.append( str( ( record.TotalTime - record.Timestamp ).total_seconds() ) )
                    
                else:
                    data.append( getattr( record, column ) )
                
            records.append( data )
        
        self.__tableModel = LoggingTableModel( self, columns, records )
        self.__table = QtGui.QTableView()
        self.__table.setModel( self.__tableModel )
        
        mainLayout.addWidget( self.__table )

class TechArtToolsLoggingViewer( RS.Tools.UI.QtMainWindowBase ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'Rockstar - Tech Art Tools Logging Viewer', size = [ 800, 450 ] )
        
        mainWidget = MainWidget()
        self.setCentralWidget( mainWidget )
        
        self.show()
        

def Run():
    tool = TechArtToolsLoggingViewer()
    
    