"""
Open File Dialog for opening cutscenes and ingame animations

Author:
    David Vega <david.vega@rockstargames.com>
"""

import os
import clr
import time
import base64
import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Perforce, Config, ProjectData
from RS.Core import DatabaseConnection
from RS.Core.Server import Client
from RS.Utils.Logging import Universal
from RS.Utils.LocalSettings import Settings
from RS.Tools.UI import Run, Application, QtMainWindowBase, QPrint
from RS.Tools.UI.ExternalProgressBar import Launcher
from RS.Tools.UI.OpenFile.Widgets import FileTree
from RS.Tools.UI.OpenFile.Widgets import DescriptionWidget
from RS.Tools.UI.OpenFile.Widgets import CollapsableWidget

SETTINGS_DIRECTORY = os.path.join(Config.Tool.Path.TechArt, "etc", "config", "motionbuilder")

GLOG = Universal.UniversalLog("Motionbuilder Open Dialog", False, True)

if Config.User.IsInternal: 
    # Check to make sure we don't try to access the database from India (They don't have access)
    CUTSCENE_DATABASE = DatabaseConnection.DatabaseConnection(
        '{SQL Server}', 'NYCW-MHB-SQL', 'CutsceneStats_{}'.format(Config.Project.Name), GLOG)
    CAPTURE_DATABASE = DatabaseConnection.DatabaseConnection(
        '{SQL Server}', 'NYCW-MHB-SQL', 'CaptureRenders', GLOG)
    SAN_DIEGO_DATABASE = DatabaseConnection.DatabaseConnection(
        '{SQL Server}', 'RSGSANSQL1\PRODUCTION', 'RockstarTools', GLOG, username='tools',
        password=base64.b64decode('cm9ja3N0YXIxQQ=='))


class Dialog(QtGui.QSplitter):
    """ Dialog window for opening up cutscenes and animation files """

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent for the widget

        """
        super(Dialog, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout()

        self._filepath = ""
        self._lock = False
        self._lockedUser = None
        self._currentUser = None
        self._seconds = 0
        self._settings = Settings(directory=SETTINGS_DIRECTORY)
        self._syncToPerforce = True
        self._size = (450, 900)

        cutscenePath, dlcCutscenePaths, ingamePath, dlcIngamePaths = ProjectData.data.GetFbxDirectories()

        self.trees = []
        self.trees.append(FileTree.FileTreeWidget(rootDirectories=[cutscenePath] + dlcCutscenePaths))
        self.trees.append(FileTree.FileTreeWidget(
            title="Open InGame Files",
            rootDirectories=[ingamePath] + dlcIngamePaths)
        )

        self.tab = QtGui.QTabWidget()
        self.tab.addTab(self.trees[0], "Cutscene")
        self.tab.addTab(self.trees[1], "Ingame")

        for tree in self.trees:
            tree.selectionChangedSignal.connect(self.UpdateDescription)
        self.setAutoFiltering(not self._settings.Get("disableAutoFilter", True))

        self.description = DescriptionWidget.Description()
        self.descriptionToggle = CollapsableWidget.CollapsableWidget(direction=CollapsableWidget.CollapsableWidget.Right)
        self.descriptionToggle.addWidget(self.description)
        self.descriptionToggle.clicked.connect(self.ShowDescription)
        self.descriptionToggle.setToolTip("Show/Hide File Stats")

        self.openButton = QtGui.QPushButton(QtGui.QIcon(
            os.path.join(Config.Script.Path.ToolImages, "motionbuilder_Icon.png")), "Open Scene in Motion Builder")
        self.openInGameButton = QtGui.QPushButton(QtGui.QIcon(
            os.path.join(Config.Script.Path.ToolImages, "Remote", "camera_Controller.png")), "Open Scene in Game")

        self.openButton.setEnabled(False)
        self.openButton.setIconSize(QtCore.QSize(40, 40))
        self.openInGameButton.setEnabled(False)
        self.openInGameButton.setIconSize(QtCore.QSize(40, 40))

        self.openButton.clicked.connect(self.OpenFile)
        self.openInGameButton.clicked.connect(self.OpenSceneInGame)

        splitter = QtGui.QSplitter()
        splitter.addWidget(self.tab)
        splitter.addWidget(self.descriptionToggle)

        openLayout = QtGui.QHBoxLayout()
        openLayout.addWidget(self.openButton)
        openLayout.addWidget(self.openInGameButton)
        openLayout.setContentsMargins(0, 0, 0, 0)
        openLayout.setSpacing(0)

        layout.addWidget(splitter)
        layout.addLayout(openLayout)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        self.splitter = splitter

        self.setLayout(layout)

    def CreateMenu(self):
        """
        Creates a menu for the widget.
        This method is called by the Run decorator.

        Returns:
            QtGui.QMenuBar

        """
        self.menuBar = QtGui.QMenuBar(self)
        self.settingsMenu = QtGui.QMenu("Settings")
        self.openFile = QtGui.QMenu("File")

        self.menuBar.addMenu(self.settingsMenu)
        self.menuBar.addMenu(self.openFile)

        self.showFaceFiles = QtGui.QAction("Include Face Files in Search", self)
        self.checkoutFiles = QtGui.QAction("Check Out Files from Perforce", self)
        self.showLoadingBar = QtGui.QAction("Show Loading Bar", self)
        self.standardOpen = QtGui.QAction("Standard File Open", self)
        self.disableAutoFilter = QtGui.QAction("Disable Automatic Filtering", self)

        self.showFaceFiles.setCheckable(True)
        self.checkoutFiles.setCheckable(True)
        self.showLoadingBar.setCheckable(True)
        self.disableAutoFilter.setCheckable(True)

        self.showFaceFiles.setChecked(self._settings.Get(key="showFaceFiles", default=False))
        self.checkoutFiles.setChecked(self._settings.Get(key="checkoutFiles", default=True))
        self.showLoadingBar.setChecked(self._settings.Get(key="showLoadingBar", default=False))
        self.disableAutoFilter.setChecked(self._settings.Get(key="disableAutoFilter", default=False))

        self.showFaceFiles.triggered.connect(self.setFilter)
        self.showFaceFiles.triggered.connect(lambda *args:
                                             setattr(self._settings, "showFaceFiles", self.showFaceFiles.isChecked()))
        self.checkoutFiles.triggered.connect(lambda *args:
                                             setattr(self._settings, "checkoutFiles", self.checkoutFiles.isChecked()))
        self.showLoadingBar.triggered.connect(lambda *args:
                                              setattr(self._settings, "showLoadingBar", self.showLoadingBar.isChecked()))

        self.disableAutoFilter.triggered.connect(lambda *args:
                                                 self.setAutoFiltering(not self.disableAutoFilter.isChecked()))

        self.disableAutoFilter.triggered.connect(lambda *args:
                                                 setattr(self._settings, "disableAutoFilter",
                                                         self.disableAutoFilter.isChecked()))
        self.standardOpen.triggered.connect(self.FileDialog)
        self.standardOpen.triggered.connect(self.Close)

        self.settingsMenu.addAction(self.showFaceFiles)
        self.settingsMenu.addAction(self.checkoutFiles)
        self.settingsMenu.addAction(self.showLoadingBar)
        self.settingsMenu.addAction(self.disableAutoFilter)

        self.openFile.addAction(self.standardOpen)

        self.setFilter()

        return self.menuBar

    def ContextMenu(self, position):
        """
        Custom context menu for opening files

        Arguments:
            position (QtCore.QPos): position of pointer

        """
        menu = QtGui.QMenu()

        menu.addAction(self.tr("Open File in Motion Builder"), self.OpenFile)
        menu.addAction(self.tr("Open Scene InGame"), self.OpenSceneInGame)

        # menu.exec_(self.Tree.FileTree.Tree.viewport().mapToGlobal(position))

    def setFilter(self):
        """ Updates filter proxy model to show/hide Face files """
        filters = ("Face", "face")
        for tree in self.trees:
            for filter in filters:
                if not self.showFaceFiles.isChecked():
                    tree.addFilter(filter)
                else:
                    tree.removeFilter(filter)

    def UpdateDescription(self):
        """ Retrieves relevant information from Perforce and Databases to updates the Description Widget with  """

        # Get the TreeWidget that is currently active
        tree = self.tab.currentWidget()
        visible = self.descriptionToggle.isCenterWidgetVisible()
        filepath = ""

        if tree.selectedItem and tree.selectedItem.data().lower().endswith(".fbx") and visible:

            # Build filepath and basename for the selected file
            item = tree.Model.itemFromIndex(tree.Filter.mapToSource(tree.selectedItem))
            filepath = item.data(QtCore.Qt.UserRole)
            basename, _ = os.path.splitext(os.path.basename(filepath))

            # Get the current state of the file from perforce to check if the file is locked
            filestate = Perforce.GetFileState(filepath.replace("@", "%40"))
            description = Perforce.GetDescription(filestate.HeadChange)["desc"]

            isLocked = [user for user in filestate.OtherOpenUsers]
            lockedUser = ""

            color = None
            if isLocked:
                lockedUser = isLocked[0]

            # when the current user has the selected file open for editing, it is considered locked
            # the background of the text box however is set to blue that it is safe to open
            elif not isLocked and Perforce.IsOpenForEdit(filestate):
                isLocked = True
                lockedUser = str(Perforce.User())
                color = "#3a609a" # Light Navy Blue

            # Get dependencies
            dependencies = [str(each[0]) for each in CUTSCENE_DATABASE.ExecuteQuery(
                "exec [ROCKSTAR\dvega].[getCharacterNamesFromCut] '/cuts/{}/Shot_1.cutxml'".format(basename))]

            # Get list of users
            # FbxFileNames stored on the database alternate between their perforce path and local paths
            # When the local path doesn't return any results, we try using the perforce path

            databaseFilepath = filepath.replace("\\", "/")
            results = CAPTURE_DATABASE.ExecuteQuery("exec [ROCKSTAR\dvega].[GetCaptureSubmitters_{}] '{}'".format(
                Config.Project.Name[:-1], databaseFilepath))

            if not results:
                results = CAPTURE_DATABASE.ExecuteQuery("exec [ROCKSTAR\dvega].[GetCaptureSubmitters_{}] '{}'".format(
                    Config.Project.Name[:-1], "/{}".format(databaseFilepath[2:])) or [])

            # Remove duplicate user names while keeping the order they were returned at
            users = []
            users_append = users.append
            [users_append(username) for usernameTuple in results for username in usernameTuple if username not in users]

            # Run custom query to retrieve the full names and emails associated with each user name
            usernameQuery = (
                "SET NOCOUNT ON;"
                "DECLARE @UserNames [ROCKSTAR\dvega].[UserTable];"
                "{}"
                "SET NOCOUNT OFF;"
                "EXEC [ROCKSTAR\dvega].[GetNameAndEmailFromTable] @UserNames")

            usernameQuery = usernameQuery.format("".join(["INSERT INTO @UserNames VALUES ('{}');".format(username)
                                                          for username in users]))

            users = ["{} <{}>".format(username, email) for username, email in
                     SAN_DIEGO_DATABASE.ExecuteQuery(usernameQuery)]

            # Get estimated load time for the file
            loadtime = SAN_DIEGO_DATABASE.ExecuteQuery(
                "exec [ROCKSTAR\mharrison-ball].[TechArtLogging_GetSceneAvgLoadTime] "
                "'{}'".format(filepath.replace("\\", "/"))) or ((None, None, "00:00:00"),)

            # Get full name and email of the user who has the selected file locked if it is locked
            lockedUserInfo = ""
            if lockedUser:
                nameAndEmail = SAN_DIEGO_DATABASE.ExecuteQuery(
                    "exec [ROCKSTAR\dvega].[GetNameAndEmailFromUserName] '{}'".format(lockedUser.split("@")[0]))
                if nameAndEmail:
                    lockedUserInfo = "{} <{}>".format(*nameAndEmail[0])

            # Update the description widget
            self.description.setPath(filepath)
            self.description.setLocked(bool(isLocked), lockedUserInfo, color=color)
            self.description.setRevision(filestate.HeadRevision)
            self.description.setChangelist(filestate.HeadChange)
            self.description.setDateTime(filestate.HeadChangelistTime)
            self.description.setVideo(filename=os.path.splitext(os.path.basename(filepath))[0])
            self.description.setDescription(description)
            self.description.setDependencies(dependencies)
            self.description.setUsers(users)
            self.description.setTimeEstimate(str(loadtime[0][2]))

            # Update internal variables
            self._filepath = filepath
            self._lock = isLocked
            self._lockedUser = lockedUser
            self._currentUser = str(Perforce.User())
            self._seconds = [int(_) for _ in loadtime[0][2].split(":")]
            self._seconds = (self._seconds[0] * 3600) + (self._seconds[1] * 60) + self._seconds[2]

        elif tree.selectedItem and tree.selectedItem.data().lower().endswith(".fbx"):
            # Build filepath and basename for the selected file
            item = tree.Model.itemFromIndex(tree.Filter.mapToSource(tree.selectedItem))
            filepath = item.data(QtCore.Qt.UserRole)
            self._filepath = filepath
        # Enable the open button
        self.openButton.setEnabled(filepath.lower().endswith(".fbx"))
        self.openInGameButton.setEnabled("!!scenes" in filepath)

    def event(self, event):
        """
        Overrides the built-in event method, when the window is shown, it queries perforce to populate the tree view

        Arguments:
            event (QtCore.QEvent): event called by the window

        Return:
            True
        """

        if event.type() == QtCore.QEvent.Type.WindowActivate and self._syncToPerforce:
            [tree.PopulateView() for tree in self.trees]
            self._syncToPerforce = False

            # Hide/Show Video
            if not self._settings.Get("showVideo", True):
                self.description.collapsableWidget.click()

            self.description.collapsableWidget.clicked.connect(lambda *args:
                                                   setattr(self._settings, "showVideo",
                                                           self.description.collapsableWidget.centerWidget().isVisible()))

            if self._settings.Get("descriptionVisibility", False):
                parent = self.topParent()
                if self.width() - 632 > 20:
                    parent.resize(parent.width() - 600, parent.height())
                self.descriptionToggle.click()

        return True

    def topParent(self):
        if self.parent() and isinstance(self.parent().parent(), QtMainWindowBase):
            return self.parent().parent()._parent
        return self.parent()

    def Close(self):
        """ Only way to properly close the window """
        self.parent().parent().parent().close()

    def setAutoFiltering(self, value):
        [tree.setAutoFilter(value) for tree in self.trees]

    def ShowDescription(self, visible):
        """
        Shows the description widget and resizes the dialog to a minimum required size to view everything
        Arguments:
            visible (boolean): the collapesed widget's visibility

        """
        parent = self.topParent()

        width, height = parent.width(), parent.height()

        if visible:
            self._size = (width, height)
            width = self.tab.width() + 632
            tabSize = (self.tab.width(), self.tab.height())
            parent.resize(width, height)
            self.tab.resize(*tabSize)
            self.splitter.moveSplitter(self.tab.width(), 1)

            # Update description when opened if a different file is selected

            tree = self.tab.currentWidget()
            if tree.selectedItem:
                item = tree.Model.itemFromIndex(tree.Filter.mapToSource(tree.selectedItem))
                filepath = item.data(QtCore.Qt.UserRole)
                if self.description.path() != filepath:
                    self.UpdateDescription()

        elif self.parent():
            parent.resize(self.tab.width() + 32, height)

        self._settings.descriptionVisibility = visible

    def OpenFile(self):
        """
        Syncs and opens the file
        """
        # Warn the user that the current file is locked by someone else

        if self._lock and self._lockedUser and self._lockedUser != self._currentUser and \
           QtGui.QMessageBox.warning(None, "Warning", "The file is currently checked out by "
                                                      "another user. You won't be able to commit "
                                                      "the file until they have checked in their "
                                                      "changes. \n"
                                                      " Do you still want to open this file?",
                                     QtGui.QMessageBox.Open | QtGui.QMessageBox.Cancel) == QtGui.QMessageBox.Cancel:

            return

        filepath = str(self._filepath.replace("@", "%40"))

        # Sync the latest file and open it for edit if it isn't locked
        Perforce.Sync(filepath)
        if not self._lock and self.checkoutFiles.isChecked():
            Perforce.Run("edit", [self._filepath])

        filepath = str(self._filepath.replace("%40", "@"))

        # Start a progress bar outside motion builder to countdown the time it takes to open the file only if
        # the estimated time to open said file is more than 5 seconds
        self.description.previewVideo.pause()
        if self._seconds > 5 and self.showLoadingBar.isChecked():

            progressBar = Launcher.ProgressBarController(maximum=self._seconds * 4)
            progressBar.Maximum = self._seconds * 4
            command = (
                "import Launcher\n"
                "import time\n"
                "control = Launcher.ProgressBarController(useExistingProgressBar={portNumber})\n"
                "for index in xrange({time}):\n"
                "\tcontrol.Update(value=index)\n"
                "\ttime.sleep(.25)"
            ).format(portNumber=progressBar.portNumber, time=self._seconds * 4)

            # Start thread to update the progressbar
            server = Client.Connection("OpenFile")
            server.Sync(Launcher)
            server.Sync(Launcher.ProgressBar_)
            server.Thread(command)

        # Open File

        mobu.FBApplication().FileOpen(filepath)

        settings = Settings(directory=SETTINGS_DIRECTORY)
        settings.Get("RecentFilesOpened", [])
        settings.RecentFilesOpened[0:0] = [str(filepath)]
        settings.RecentFilesOpened = settings.RecentFilesOpened[:29]

        if self._seconds > 5 and self.showLoadingBar.isChecked():
            # Close server updating the progress bar and close the progressbar itself
            server.Close()
            progressBar.Value = progressBar.Maximum

        # Not the best solution but it closes properly
        self.Close()

    def OpenSceneInGame(self):
        """ Opens a Scene in the game if the game is already on """

        clr.AddReference("RSG.Rag.Clients")
        clr.AddReference("RSG.TechArt.Remote")

        from RSG.TechArt.Remote import Remote
        from RSG.Rag.Clients import RagClientFactory

        tree = self.tab.currentWidget()

        # Build filepath and basename for the selected file
        item = tree.Model.itemFromIndex(tree.Filter.mapToSource(tree.selectedItem))
        filepath = item.data(QtCore.Qt.UserRole)
        scenename, _ = os.path.splitext(os.path.basename(filepath))

        remote = Remote()
        remote.EnsureConnection()
        try:
            client = RagClientFactory.CreateWidgetClient(remote.GameConnection)
        except Exception:
            # Not entirely sure how to get a handle for the error that the c code throws out
            QtGui.QMessageBox.warning(None, "Open Error", "The game is currently not turned on.\n"
                                                          "Close Motion Builder, start the game and try again")
            return

        if Config.Project.Name == "GTA5":

            widgetBankRefCutsceneDebug = 'Cut Scene Debug'
            widgetCutsceneSceneList = 'Cut Scene Debug/Cut Scenes'
            widgetSearchCutscene = 'Cut Scene Debug/Search:'
            widgetCutsceneBank = 'Cut Scene Debug/Toggle Cut Scene Debug bank'
            widgetStartEndCutscene = 'Cut Scene Debug/Start or End Selected Cut scene'

            client.AddBankReference(widgetBankRefCutsceneDebug)

            if not client.WidgetExists(widgetCutsceneSceneList):
                client.PressButton(widgetCutsceneBank)

            # Insert empty string to clear search box ?
            if not client.WidgetExists(widgetSearchCutscene):
                time.sleep(1)
            client.WriteStringWidget(widgetSearchCutscene, '')

            client.SendSyncCommand(0)

            # Look for cutscene name in the enum/combo box/list
            client.WriteStringWidget(widgetCutsceneSceneList, scenename.lower())
            client.SendSyncCommand(0)

            # Need to check here to see if the cutscene exits: We read the combo list
            if client.ReadStringWidget(widgetCutsceneSceneList).lower():
                client.PressButton(widgetStartEndCutscene)
            else:
                QtGui.QMessageBox.warning(None, "Open Error", "The cutscene has not been integrated into the game.")
        else:

            widgetBankAnimSceneToggle = 'Anim Scenes/Toggle Anim Scenes bank'
            widgetBankAnimSelectAnimScene  = 'Anim Scenes/Load scene/Scene name selector: /Select animscene'
            widgetBankAnimSearchAnimScene  = 'Anim Scenes/Load scene/Scene name selector: /Enter search text:'
            widgetBankAnimLoadAnimScene  = 'Anim Scenes/Load scene/Load scene from rpf'
            widgetBankAnimSceneEditor  = 'Anim Scenes/Activate scene editor'
            widgetBankAnimPlayAnimScene = 'Anim Scenes/Current Anim Scene/Playback'

            client.AddBankReference('Anim Scenes')

            if not client.WidgetExists(widgetBankAnimSelectAnimScene):
                client.PressButton(widgetBankAnimSceneToggle)

            client.SendSyncCommand(5)

            client.WriteStringWidget(widgetBankAnimSearchAnimScene, scenename.lower())
            client.PressButton(widgetBankAnimLoadAnimScene)
            client.SendSyncCommand(0)

            time.sleep(2)
            counter = 5
            while counter:
                try:
                    client.PressVCRButton(widgetBankAnimPlayAnimScene, 1)
                    client.WriteBoolWidget(widgetBankAnimSceneEditor, False)
                    client.SendSyncCommand(0)
                    break
                except:
                    time.sleep(1)
                    counter -= 1


    @staticmethod
    def FileDialog():
        """ Launches a file dialog to open fbx files in Motion Builder """
        actions = Application.GetMainMenuActions()
        for menuText, menuDictionary in actions["File"].iteritems():
            if menuText.startswith("&Open..."):
                menuDictionary["__QAction"].trigger()
                return

    @staticmethod
    def StoreCallback(*_):
        """
        Stores list of the last 30 files that have recently been opened

        Arguments:
            _ (list): Anything, this parameter is just here to catch any values that might be thrown at it
        """
        path = mobu.FBApplication().FBXFileName
        if path:
            path = str(path)
            settings = Settings(directory=SETTINGS_DIRECTORY)
            settings.Get("RecentFilesOpened", [Config.Project.Path.Art])
            settings.RecentFilesOpened.remove(path) if path in settings.RecentFilesOpened else None
            settings.RecentFilesOpened[0:0] = [path]
            settings.RecentFilesOpened = settings.RecentFilesOpened[:29]


@Run("Open Cutscenes/Animations", size=(450, 900), dialog=True,
     url="https://hub.rockstargames.com/pages/viewpage.action?pageId=44664855")
def Run():
    """ Shows the Widget in Motion Builder """
    return Dialog()
