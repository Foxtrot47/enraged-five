"""
Description:
    Tool for setting up cameras for moving cutscenes

Author:
    David Vega <david.vega@rockstargames.com>
"""
from RS.Tools.UI.MovingCutscene.Widget.SetupWidget import Run
