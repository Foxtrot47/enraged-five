"""
Description:
    UI for displaying a selection from Motion Builder

Author:
    David Vega <david.vega@rockstargames.com>
"""

import pyfbsdk as mobu

from functools import partial
from PySide import QtGui, QtCore
from RS.Tools.UI import QPrint
from RS.Tools.UI.MovingCutscene.Model import AbstractTree


class SelectionTree(AbstractTree.AbstractTreeView):

    def __init__(self, parent=None, f=0, title="Components", filterByTypes=(mobu.FBComponent,), excludeByTypes=(),
                 filterByName=""):
        """
        Constructor

        Arguments:
            parent: QWidget; QWidget parent
            f: QtCore.Qt.WindowFlag; window flags
            title: string; title for the tree view
            filterType: class or list(class, etc.); class from pyfbsdk that you want to filter selection by. The class
                        should be FBComponent or a class that inherits FBComponent.
        """
        super(SelectionTree, self).__init__(parent=parent, f=f)
        horizontalLayout = QtGui.QHBoxLayout()

        self.SelectionButton = self.RefreshButton
        self.RemoveButton = QtGui.QPushButton("Minus")
        self.ClearButton = QtGui.QPushButton("Clear")

        horizontalLayout.addWidget(self.SelectionButton)
        horizontalLayout.addWidget(self.RemoveButton)
        horizontalLayout.addWidget(self.ClearButton)
        self.layout().addLayout(horizontalLayout)

        self.SelectionButton.setText("Add")

        self.SelectionButton.clicked.connect(self.UpdateTree)
        self.RemoveButton.clicked.connect(partial(self.UpdateTree, remove=True))
        self.ClearButton.clicked.connect(self.ClearTree)

        # Convert data into tuples
        if not isinstance(filterByTypes, (list, tuple)):
            filterByTypes = (filterByTypes,)
        elif isinstance(filterByTypes, list):
            filterByTypes = tuple(filterByTypes)

        if not isinstance(excludeByTypes, (list, tuple)):
            excludeByTypes = (excludeByTypes,)
        elif isinstance(excludeByTypes, list):
            excludeByTypes = tuple(excludeByTypes)

        self.filterByTypes = filterByTypes
        self.excludeByTypes = excludeByTypes
        self.filterByName = filterByName
        self.title = title
        self._selection = []
        self.layout().setSpacing(0)

    @property
    def Selection(self):
        """ Returns the stored selection """
        self._selection = self.CleanSelection()
        self._selection += [each for each in self.StoreSelection() if each not in self._selection]
        return self._selection

    def StoreSelection(self):
        """ Stores the current motion builder selection based on the filterType"""
        components = mobu.FBSystem().Scene.Components

        # To avoid iterating over every component every time, we try to get the
        # list of components from FBSystem().Scene
        if len(self.filterByTypes) == 1 and self.filterByTypes[0] is not mobu.FBModel:
            components = getattr(mobu.FBSystem().Scene, "{}s".format(self.filterByTypes[0].__name__[2:]))

        return [component for component in components if component.Selected and
                isinstance(component, self.filterByTypes) and not isinstance(component, self.excludeByTypes)
                and self.filterByName in component.LongName]

    def RemoveSelection(self):
        """ Removes selected objects """
        removeSelection = self.StoreSelection()
        [setattr(each, "Selected", False) for each in removeSelection]
        self._selection = [each for each in self._selection if each not in removeSelection]

    def CleanSelection(self):
        """ Removes stored selected objects that have been destroyed"""
        return [each for each in self._selection if "Unbound" not in each.__class__.__name__]

    def ClearTree(self, clearSelection=True):
        """ Clears the Tree View and stored selection """
        self.Model.clear()
        self.Model.setColumnCount(1)
        self.Model.setHeaderData(0, QtCore.Qt.Horizontal, self.title)

        if clearSelection:
            self._selection = []

    def UpdateTree(self, remove=False):
        """ Updates the TreeView to reflect the current selection. """
        if remove:
            self.RemoveSelection()
        self.ClearTree(clearSelection=False)
        [self.Model.appendRow(self.CreateItemFromComponent(eachComponent)) for eachComponent in self.Selection]
        QPrint(self._selection)

    @staticmethod
    def CreateItemFromComponent(component):
        """
        Create a QStandardItem from the provided FBComponent

        Arguments:
            component: FBComponent(); component to base new QStandardItem from
        """
        item = QtGui.QStandardItem(component.LongName)
        item.setData(component.LongName)
        return item

