
from ctypes import *
from pyfbsdk import *
from pyfbsdk_additions import *
import sys
import os
os.system("call setenv")
sys.path.append(os.environ["RS_TOOLSROOT"] + "\dcc\current\motionbuilder2010\scripts\util")
print(os.environ["RS_TOOLSROOT"] + "\dcc\current\motionbuilder2010\scripts\util")

# My python enum equiv
class PathColumnCode:
    kOperate = 0

class ScriptColumnCode:
    kExecute = 0

class BatchToolManager:
    pyScripts = []
    fbxFiles = []
    
    # Controls
    mainLyt = None
    pathSpread = None
    scriptSpread = None
    
    addScriptButton = None
    addPathButton = None
    flushScriptsButton = None
    flushPathsButton = None
    checkAllFbxButton = None
    checkAllScriptsButton = None
    uncheckAllFbxButton = None
    uncheckAllScriptsButton = None
    
    executeButton = None
    
    exportCheckbox = None
    
    grid = None
    
    def OnClickAddPathButtonCallBack(self, control, event):
        
        dirPicker = FBFolderPopup()
        dirPicker.Path = os.environ["RS_PROJROOT"] + "\\art\\anim\\source_fbx\\"
        if(dirPicker.Execute()):
            fbxFileCount = len(self.fbxFiles)
            print fbxFileCount
            fileList = [f for f in os.listdir(dirPicker.Path) if f.lower().endswith('.fbx')]
            fbxFileSet = set(self.fbxFiles)
            for index in range(len(fileList)):
                if fileList[index] not in fbxFileSet:
                    self.fbxFiles.append(fileList[index])
                    self.pathSpread.RowAdd(fileList[index], fbxFileCount+index)
                    self.pathSpread.SetCellValue(fbxFileCount+index, PathColumnCode.kOperate, "On")
                
    def OnClickFlushPathsButtonCallBack(self, control, event):
        self.pathSpread.Clear()
        self.pathSpread.ColumnAdd("Operate") 
        self.pathSpread.GetColumn(0).Justify = FBTextJustify.kFBTextJustifyCenter
        self.fbxFiles = []
            
    
    def OnClickAddScriptButtonCallBack(self, control, event): 
        filePicker = FBFilePopup()
        filePicker.Filter = "*.py"
        filePicker.Path =os.environ["RS_TOOLSROOT"] + "\\dcc\\current\\motionbuilder2009\\scripts\\"
        if(filePicker.Execute()):
            self.scriptSpread.RowAdd(filePicker.FullFilename, len(self.pyScripts))
            self.pyScripts.append(filePicker.FullFilename)
            self.scriptSpread.SetCellValue(len(self.pyScripts)-1, ScriptColumnCode.kExecute, "On")
            
    def OnClickFlushScriptsButtonCallBack(self, control, event):
        self.scriptSpread.Clear()
        self.scriptSpread.ColumnAdd("Execute")
        self.scriptSpread.GetColumn(0).Justify = FBTextJustify.kFBTextJustifyCenter
        self.pyScripts = []
        
    def OnClickCheckAllFbxButtonCallBack(self, control, event):
        for index in range(len(self.fbxFiles)):
            self.pathSpread.SetCellValue(index, PathColumnCode.kOperate, "On")
        
    def OnClickUncheckAllFbxButtonCallBack(self, control, event):
        for index in range(len(self.fbxFiles)):
            self.pathSpread.SetCellValue(index, PathColumnCode.kOperate, "Off")
        
    def OnClickCheckAllScriptsButtonCallBack(self, control, event):    
        for index in range(len(self.pyScripts)):
            self.scriptSpread.SetCellValue(index, ScriptColumnCode.kExecute, "On")
        
    def OnClickUncheckAllScriptsButtonCallBack(self, control, event):
        for index in range(len(self.pyScripts)):
            self.scriptSpread.SetCellValue(index, ScriptColumnCode.kExecute, "Off")
       
    def OnClickExecuteButtonCallBack(self, control, event):
        app = FBApplication()
        for index in range(len(self.fbxFiles)):
            try:
                if(self.pathSpread.GetCellValue(index, PathColumnCode.kOperate) == "On"):
                    app.FileOpen(self.fbxFiles[index])
                    for index2 in range(len(self.pyScripts)):
                        app.ExecuteScript (self.pyScripts[index2])
                    if self.exportCheckbox.State == 1:
                        cdll.rexMBRage.Export_Py()
            except IOError as (errno, strerror):
                print "I/O error({0}): {1}".format(errno, strerror)
            except:
                print "Unexpected error:", sys.exc_info()[0]
                raise
            
       
    def OnColClickEvent(self, control, event):
       pass
        
    def OnRowClickEvent(self, control, event):
       pass
        
    def OnCellChangedEvent(self, control, event):
       pass
        
    def OnDragAndDropEvent(self, control, event):
       pass
        
    def initialise(self):
        self.mainLyt = FBCreateUniqueTool("Batch Processor")
        self.mainLyt.StartSizeX = 800
        self.mainLyt.StartSizeY = 800
        
        x = FBAddRegionParam(0,FBAttachType.kFBAttachLeft,"")
        y = FBAddRegionParam(0,FBAttachType.kFBAttachTop,"")
        w = FBAddRegionParam(0,FBAttachType.kFBAttachRight,"")
        h = FBAddRegionParam(0,FBAttachType.kFBAttachBottom,"")
    
        self.mainLyt.AddRegion("main","main", x, y, w, h)
        
      
        self.grid = FBGridLayout()
    
        
        self.grid.SetRowRatio(0, 3.0)
        
        # Path spreadsheet
        self.pathSpread = FBSpread()
        self.pathSpread.Caption = "FBX Files"
        self.pathSpread.OnColumnClick
        self.grid.AddRange(self.pathSpread,0,6,0,5)
        #self.grid.SetRowHeight(0, 200)
        #self.grid.SetColWidth(-1, 600)
        
        self.pathSpread.ColumnAdd("Operate")
        #self.pathSpread.GetColumn(0).Style = FBCellStyle.kFBCellStyle2StatesButton 
        self.pathSpread.GetColumn(0).Justify = FBTextJustify.kFBTextJustifyCenter
        self.pathSpread.OnColumnClick.Add(self.OnColClickEvent)
        self.pathSpread.OnRowClick.Add(self.OnRowClickEvent)
        self.pathSpread.OnCellChange.Add(self.OnCellChangedEvent)
        self.pathSpread.OnDragAndDrop.Add(self.OnDragAndDropEvent)
        self.pathSpread.GetColumn(-1).Width = 450
        
        # Script spreadhsheet
        self.scriptSpread = FBSpread()
        self.scriptSpread.Caption = "Scripts"
        self.grid.AddRange(self.scriptSpread,8,14,0,5)
        
        self.scriptSpread.ColumnAdd("Execute")
        #self.scriptSpread.GetColumn(0).Style = FBCellStyle.kFBCellStyle2StatesButton
        self.scriptSpread.GetColumn(0).Justify = FBTextJustify.kFBTextJustifyCenter
        self.scriptSpread.GetColumn(-1).Width = 450
        
        
        # Buttons
        self.addPathButton = FBButton()
        self.addPathButton.Caption = "Add Path"
        self.addPathButton.OnClick.Add(self.OnClickAddPathButtonCallBack)
        self.grid.Add(self.addPathButton,5,6)
        
        self.addScriptButton = FBButton()
        self.addScriptButton.Caption = "Add Script"
        self.addScriptButton.OnClick.Add(self.OnClickAddScriptButtonCallBack)
        self.grid.Add(self.addScriptButton,13,6)
        
        self.flushPathsButton = FBButton()
        self.flushPathsButton .Caption = "Flush Paths"
        self.flushPathsButton .OnClick.Add(self.OnClickFlushPathsButtonCallBack)
        self.grid.Add(self.flushPathsButton ,6,6)
        
        self.flushScriptsButton = FBButton()
        self.flushScriptsButton .Caption = "Flush Scripts"
        self.flushScriptsButton .OnClick.Add(self.OnClickFlushScriptsButtonCallBack)
        self.grid.Add(self.flushScriptsButton ,14,6)
        
        self.checkAllFbxButton = FBButton()
        self.checkAllFbxButton.Caption = "Check All"
        self.checkAllFbxButton.OnClick.Add(self.OnClickCheckAllFbxButtonCallBack)
        self.grid.Add(self.checkAllFbxButton,7,4)
        
        self.uncheckAllFbxButton = FBButton()
        self.uncheckAllFbxButton.Caption = "Uncheck All"
        self.uncheckAllFbxButton.OnClick.Add(self.OnClickUncheckAllFbxButtonCallBack)
        self.grid.Add(self.uncheckAllFbxButton,7,5)
        
        self.checkAllScriptButton = FBButton()
        self.checkAllScriptButton.Caption = "Check All"
        self.checkAllScriptButton.OnClick.Add(self.OnClickCheckAllScriptsButtonCallBack)
        self.grid.Add(self.checkAllScriptButton,15,4)
        
        self.uncheckAllScriptButton = FBButton()
        self.uncheckAllScriptButton.Caption = "Uncheck All"
        self.uncheckAllScriptButton.OnClick.Add(self.OnClickUncheckAllScriptsButtonCallBack)
        self.grid.Add(self.uncheckAllScriptButton,15,5)
        
        self.executeButton = FBButton()
        self.executeButton.Caption = "Execute"
        self.executeButton.OnClick.Add(self.OnClickExecuteButtonCallBack)
        self.grid.Add(self.executeButton,15,0)
        
        self.exportCheckbox = FBButton()
        self.exportCheckbox.Caption = "Export"
        self.exportCheckbox.Style = FBButtonStyle.kFBCheckbox
        self.grid.Add(self.exportCheckbox,15,1)
        
        # Fixed the height of row 0 and row 2 so the label and the buttons
        # have a normal height
        #grid.SetRowHeight(0, 160)
        #grid.SetRowHeight(2, 160)
        
        # To ensure our memo can be stretched, ensure row 1 and column 0 have a ratio
        #self.grid.SetColRatio( 0, 1.0 )
        #self.grid.SetRowRatio( 1, 1.0 )
        
        self.mainLyt.SetControl("main", self.grid)
    
        ShowTool(self.mainLyt)
       
        
    def add_execution_unit(unit):
        scriptExecutionUnits.append(unit)
        
    def add_fbx_cluster(cluster):
        fbxClusters.append(cluster)
        
    def execute_batch_operations(self):
        print "Executing batch operations"
        

class ScriptExecutionUnit:
    scriptPath = ""
    active = False
    parameters = ""
    
    def execute():
        print "executing"
        
class FbxCluster:
    fbxFiles = [] 
  
  
def CreateTool():    
    toolManager = BatchToolManager()
    toolManager.initialise()

CreateTool()