'''

 Script Path: RS/Tools/UI/VehicleControls.py

 Written And Maintained By: Kathryn Bodey

 Created: 30 April 2014

 Description: Car Rig UI

'''

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Utils.UserPreferences

import RS.Config
import RS.Tools.UI


'''
Radio Button Definition
'''

def CreateRotationButton( button, left, top, container, containerFollow, description ):
    image = "{0}\\VehicleControls\\rotation.png".format( RS.Config.Script.Path.ToolImages )
    imageSelected = "{0}\\VehicleControls\\rotation_selected.png".format( RS.Config.Script.Path.ToolImages )

    rotationButton = FBButton()
    rotationButton.Name = button
    rotationButton.Hint = description
    rotationButton.Style = FBButtonStyle.kFBBitmap2States
    rotationButton.SetImageFileNames( image, imageSelected )
    rotationButtonX = FBAddRegionParam( left, FBAttachType.kFBAttachLeft, "")
    rotationButtonY = FBAddRegionParam( top, FBAttachType.kFBAttachTop, containerFollow)
    rotationButtonW = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    rotationButtonH = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    container.AddRegion( button, button, rotationButtonX, rotationButtonY, rotationButtonW, rotationButtonH )
    container.SetControl( button, rotationButton )

    return rotationButton


def CreateTranslationButton( button, left, top, container, containerFollow, description ):
    image = "{0}\\VehicleControls\\translation.png".format( RS.Config.Script.Path.ToolImages )
    imageSelected = "{0}\\VehicleControls\\translation_selected.png".format( RS.Config.Script.Path.ToolImages )

    translationButton = FBButton()
    translationButton.Name = button
    translationButton.Hint = description
    translationButton.Style = FBButtonStyle.kFBBitmap2States
    translationButton.SetImageFileNames( image, imageSelected )
    translationButtonX = FBAddRegionParam( left, FBAttachType.kFBAttachLeft, "")
    translationButtonY = FBAddRegionParam( top, FBAttachType.kFBAttachTop, containerFollow)
    translationButtonW = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    translationButtonH = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    container.AddRegion( button, button, translationButtonX, translationButtonY, translationButtonW, translationButtonH )
    container.SetControl( button, translationButton )

    return translationButton


def CreateTransformButton( button, left, top, container, containerFollow, description ):
    image = "{0}\\VehicleControls\\transform.png".format( RS.Config.Script.Path.ToolImages )
    imageSelected = "{0}\\VehicleControls\\transform_selected.png".format( RS.Config.Script.Path.ToolImages )

    transformButton = FBButton()
    transformButton.Name = button
    transformButton.Hint = description
    transformButton.Style = FBButtonStyle.kFBBitmap2States
    transformButton.SetImageFileNames( image, imageSelected )
    transformButtonX = FBAddRegionParam( left, FBAttachType.kFBAttachLeft, "")
    transformButtonY = FBAddRegionParam( top, FBAttachType.kFBAttachTop, containerFollow)
    transformButtonW = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    transformButtonH = FBAddRegionParam( 15, FBAttachType.kFBAttachNone, "")
    container.AddRegion( button, button, transformButtonX, transformButtonY, transformButtonW, transformButtonH )
    container.SetControl( button, transformButton )

    return transformButton


'''
Scene Vehicle Dropdown
'''
def SceneVehicleList():        
    # get a list of all vehicles brought in via ref system
    referenceScene = None
    refNullList = []
    vehicleList = []
    dropdownList = []

    for component in RS.Globals.gComponents:
        if component.LongName == 'REFERENCE:Scene' and component.ClassName() == 'FBModelNull':
            referenceScene = component

    if referenceScene != None:
        RS.Utils.Scene.GetChildren( referenceScene, refNullList, "", False )

        if len( refNullList ) > 0:
            for asset in refNullList:
                if asset.PropertyList.Find( 'rs_Asset_Type' ):
                    typeProperty = asset.PropertyList.Find( 'rs_Asset_Type' )
                    if typeProperty.Data == 'Vehicles':
                        vehicleList.append( asset )

    for i in vehicleList:
        dropdownList.append( i )

    return dropdownList


'''
Create UI
'''
class VehicleControlsToolbox( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Vehicle Controls', size = [ 465, 660 ] )

        # private variable
        self.__VehicleTypeList = [ '4_door', '2_door', 'van', 'motorbike' ]

        # public variables
        self.ButtonList = []
        self.SceneVehicleList = []
        self.ImageContainer = None
        self.VehicleImage = None
        self.ImageLayout = None
        self.VehicleSelectedIndex = None
        self.VehicleTypeSelected = None
        self.VehicleTypeSelectedIndex = None


        self.SceneVehicleList = SceneVehicleList()

        self.VehicleSelectedIndex = RS.Utils.UserPreferences.__Readini__( "VehicleControlToolbox","VehicleSelectedIndex", self.VehicleSelectedIndex )
        self.VehicleTypeSelected = RS.Utils.UserPreferences.__Readini__( "VehicleControlToolbox","VehicleTypeSelected", self.VehicleTypeSelected )
        self.VehicleTypeSelectedIndex = RS.Utils.UserPreferences.__Readini__( "VehicleControlToolbox","VehicleTypeSelectedIndex", self.VehicleTypeSelectedIndex )

        if self.VehicleSelectedIndex == None:
            self.VehicleSelectedIndex = 0
        if self.VehicleTypeSelected == None:
            self.VehicleTypeSelected = self.__VehicleTypeList[0]
        if self.VehicleTypeSelectedIndex == None:
            self.VehicleTypeSelectedIndex = 0

    '''
    Callback: Refresh
    '''
    def RefreshCallback( self, control, event ):
        currentItemList = self.SceneVehicleDropDown.Items
        
        for item in currentItemList:
            self.SceneVehicleDropDown.Items.remove(item)

        self.SceneVehicleList = SceneVehicleList()

        for i in self.SceneVehicleList:
            self.SceneVehicleDropDown.Items.append( i.Name )

        if len( self.SceneVehicleList ) >= ( self.VehicleSelectedIndex + 1 ):
            self.SceneVehicleDropDown.Selected( self.VehicleSelectedIndex, True )

        for button in self.ButtonList:
            button.State = 0

    def Create( self, mainLayout ):

        x = FBAddRegionParam( 0,FBAttachType.kFBAttachLeft,"" )
        y = FBAddRegionParam( 0,FBAttachType.kFBAttachBottom,"" )
        w = FBAddRegionParam( 0,FBAttachType.kFBAttachRight,"" )
        h = FBAddRegionParam( 0,FBAttachType.kFBAttachTop,"" )
        mainLayout.AddRegion( "mainLayout", "mainLayout", x, y, w, h )


        '''
        Refresh Button
        '''
        button = FBButton()
        button.Caption = 'Refresh Scene Vehicle list'
        button.Hint = 'Reloads the UI'
        buttonX = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, "" )
        buttonY = FBAddRegionParam( 30, FBAttachType.kFBAttachTop, "" )
        buttonW = FBAddRegionParam( 449, FBAttachType.kFBAttachNone,"" )
        buttonH = FBAddRegionParam( 17, FBAttachType.kFBAttachNone,"" )
        mainLayout.AddRegion( 'button', 'button', buttonX, buttonY, buttonW, buttonH )
        mainLayout.SetControl( 'button', button )

        button.OnClick.Add( self.RefreshCallback )


        '''
        Dropdowns
        '''
        # label: vehicle from scene
        sceneVehicleLabel = FBLabel()
        sceneVehicleLabel.Caption = ' Scene Vehicle:'
        labelX = FBAddRegionParam( 5 , FBAttachType.kFBAttachLeft, "" )
        labelY = FBAddRegionParam( 50, FBAttachType.kFBAttachTop, "" )
        labelW = FBAddRegionParam( 90, FBAttachType.kFBAttachNone,"" )
        labelH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )
        mainLayout.AddRegion( 'sceneVehicleLabel', 'sceneVehicleLabel', labelX, labelY, labelW, labelH )
        mainLayout.SetControl( 'sceneVehicleLabel', sceneVehicleLabel )

        # dropdown: vehicle from scene
        self.SceneVehicleDropDown = FBList()
        self.SceneVehicleDropDown.Style = FBListStyle.kFBDropDownList
        self.SceneVehicleDropDown.Hint = ''
        dropDownX = FBAddRegionParam( 100, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 50, FBAttachType.kFBAttachTop, "" )
        dropDownW = FBAddRegionParam( 345, FBAttachType.kFBAttachNone,"" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )
        mainLayout.AddRegion( 'self.SceneVehicleDropDown', 'self.SceneVehicleDropDown', dropDownX, dropDownY, dropDownW, dropDownH )
        mainLayout.SetControl( 'self.SceneVehicleDropDown', self.SceneVehicleDropDown )

        for i in self.SceneVehicleList:
            self.SceneVehicleDropDown.Items.append( i.Name )

        print len( self.SceneVehicleList )
        if len( self.SceneVehicleList ) >= ( self.VehicleSelectedIndex + 1 ):
            self.SceneVehicleDropDown.Selected( self.VehicleSelectedIndex, True )

        self.SceneVehicleDropDown.OnChange.Add( self.SelectVehicleDropdownCallback )

        # label: vehicle type
        vehicleTypeLabel = FBLabel()
        vehicleTypeLabel.Caption = ' Vehicle Type:'
        labelX = FBAddRegionParam( 5, FBAttachType.kFBAttachLeft, "" )
        labelY = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "sceneVehicleLabel" )
        labelW = FBAddRegionParam( 90, FBAttachType.kFBAttachNone,"" )
        labelH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone,"" )
        mainLayout.AddRegion( 'vehicleTypeLabel', 'vehicleTypeLabel', labelX, labelY, labelW, labelH )
        mainLayout.SetControl( 'vehicleTypeLabel', vehicleTypeLabel ) 

        # dropdown: vehicle type
        vehicleTypeDropDown = FBList()
        vehicleTypeDropDown.Style = FBListStyle.kFBDropDownList
        vehicleTypeDropDown.Hint = ''
        dropDownX = FBAddRegionParam( 100, FBAttachType.kFBAttachLeft, "" )
        dropDownY = FBAddRegionParam( 5, FBAttachType.kFBAttachBottom, "self.SceneVehicleDropDown" )
        dropDownW = FBAddRegionParam( 345, FBAttachType.kFBAttachNone, "" )
        dropDownH = FBAddRegionParam( 20, FBAttachType.kFBAttachNone, "" )
        mainLayout.AddRegion( 'vehicleTypeDropDown', 'vehicleTypeDropDown', dropDownX, dropDownY, dropDownW, dropDownH )
        mainLayout.SetControl( 'vehicleTypeDropDown', vehicleTypeDropDown )   

        for i in self.__VehicleTypeList:
            vehicleTypeDropDown.Items.append( i )  
        vehicleTypeDropDown.OnChange.Add( self.SelectVehicleTypeCallback )
        vehicleTypeDropDown.Selected( self.VehicleTypeSelectedIndex, True )
        self.SelectVehicleTypeCallback( vehicleTypeDropDown, None )

        '''
        Plot
        '''
        plotButton = FBButton()
        plotButton.Caption = 'Plot Vehicle'
        plotButton.Hint = 'Plots animation to skeleton'
        buttonX = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, "" )
        buttonY = FBAddRegionParam( -16, FBAttachType.kFBAttachBottom, "" )
        buttonW = FBAddRegionParam( 449, FBAttachType.kFBAttachNone,"" )
        buttonH = FBAddRegionParam( 17, FBAttachType.kFBAttachNone,"" )
        mainLayout.AddRegion( 'plotButton', 'plotButton', buttonX, buttonY, buttonW, buttonH )
        mainLayout.SetControl( 'plotButton', plotButton )        

        plotButton.OnClick.Add( self.PlotCallback )


    '''
    Callback: Select Null Callback
    '''
    def SelectNullCallback( self, control, event ):   
        # get vehicle selected name
        selectedIndex = None
        selectedVehicle = None
        for i in range( len( self.SceneVehicleDropDown.Items ) ):
            if self.SceneVehicleDropDown.IsSelected( i ):
                selectedVehicle = self.SceneVehicleDropDown.Items[i]

        vehicleRigList = []
        nullFound = False

        nullSelectedName = control.Name

        # find null and select it
        if selectedVehicle:
            for component in RS.Globals.gComponents:
                if component.LongName == selectedVehicle + ":" + nullSelectedName:
                    nullFound = True
                    if control.State == 1:
                        component.Selected = True
                    else:
                        component.Selected = False

            if nullFound == True:
                None
            else:
                control.State = 0
                FBMessageBox( 'R* Error', "Cannot find [{0}] on [{1}]\nUI will not work for this vehicle.".format( control.Hint, selectedVehicle ), 'Ok' )
        else:
            control.State = 0
            FBMessageBox( 'R* Error', 'No Vehicle has been selected from the dropdown.', 'Ok' )


    '''
    Callback: Plots vehicle
    '''        
    def PlotCallback( self, control, event ):   

        #Store current selection so we can put it back after we have plotted
        previouslySelectedComponents = list()
        for component in RS.Globals.Components:
            if component.Selected: 
                previouslySelectedComponents.append(component)
        RS.Utils.Scene.DeSelectAll()    

        # get selected vehicle  name
        selectedIndex = None
        selectedVehicleName = None
        for i in range( len( self.SceneVehicleDropDown.Items ) ):
            if self.SceneVehicleDropDown.IsSelected( i ):
                selectedVehicleName = self.SceneVehicleDropDown.Items[i]

        # get vehicle mover 
        selectedVehicleMover = FBFindModelByLabelName( "{0}:mover".format( selectedVehicleName ) )

        # get children of the mover
        if selectedVehicleMover:
            selectedVehicleMover.Selected = True

            childModels = list()
            RS.Utils.Scene.GetChildren( selectedVehicleMover, childModels )

            # go through each one and if it's a bone then select it
            for childModel in childModels:

                if type( childModel ) in [ FBModelSkeleton, FBModelNull ]:
                    childModel.Selected = True

            # now plot selected on current take
            options = FBPlotOptions()
            options.PlotOnFrame = True
            options.UseConstantKeyReducer = False
            RS.Globals.System.CurrentTake.PlotTakeOnSelected(options)

        # reselect old selection
        RS.Utils.Scene.DeSelectAll()
        for component in previouslySelectedComponents:
            component.Selected = True


    '''
    Callback: Select Vehicle Dropdown
    '''
    def SelectVehicleDropdownCallback( self, control, event ):
        RS.Utils.Scene.DeSelectAll()

        for i in self.ButtonList:
            i.State = 0

        RS.Utils.Scene.DeSelectAll()

        for i in range( len( control.Items ) ):
            if control.IsSelected( i ):
                self.VehicleSelectedIndex = i

        RS.Utils.UserPreferences.__Saveini__( "VehicleControlToolbox", "VehicleSelectedIndex", self.VehicleSelectedIndex )


    '''
    Callback: Select Vehicle Type Dropdown
    '''            
    def SelectVehicleTypeCallback(self, control, event ):
        rotationImageDisabled = "{0}\\VehicleControls\\rotation_disabled.png".format( RS.Config.Script.Path.ToolImages )
        imageSelected = "{0}\\VehicleControls\\rotation_selected.png".format( RS.Config.Script.Path.ToolImages )
        image = "{0}\\VehicleControls\\rotation.png".format( RS.Config.Script.Path.ToolImages )

        for i in range( len( control.Items ) ):
            if control.IsSelected( i ):
                self.VehicleTypeSelected = control.Items[i]
                self.VehicleTypeSelectedIndex = i

        RS.Utils.UserPreferences.__Saveini__( "VehicleControlToolbox", "VehicleTypeSelected", self.VehicleTypeSelected ) 
        RS.Utils.UserPreferences.__Saveini__( "VehicleControlToolbox", "VehicleTypeSelectedIndex", self.VehicleTypeSelectedIndex ) 


        self.VehicleImage = "{0}\\VehicleControls\\{1}.png".format( RS.Config.Script.Path.ToolImages, self.VehicleTypeSelected )

        '''
		Image Container
		'''
        if self.ImageLayout != None:
            self.MainLayout.RemoveRegion( "self.ImageLayout" )

        self.ImageLayout = FBHBoxLayout()
        X = FBAddRegionParam( -45, FBAttachType.kFBAttachLeft,"" )
        Y = FBAddRegionParam( 100, FBAttachType.kFBAttachTop,"" )
        W = FBAddRegionParam( 560, FBAttachType.kFBAttachNone,"" )
        H = FBAddRegionParam( 520, FBAttachType.kFBAttachNone,"" )
        self.MainLayout.AddRegion("self.ImageLayout","self.ImageLayout", X, Y, W, H)
        self.MainLayout.SetControl( "self.ImageLayout", self.ImageLayout )	

        self.ImageContainer = FBVisualContainer()
        self.ImageContainer.ItemWidth = 530
        self.ImageContainer.ItemHeight = 540
        self.ImageContainer.Items.append( "" )
        self.ImageContainer.ItemIconSet( 0, self.VehicleImage )

        self.ImageLayout.AddRelative( self.ImageContainer )

        '''
		Buttons 
		'''	
        for button in self.ButtonList:
            try:
                self.MainLayout.RemoveRegion(button.Name)
            except:
                pass

        if self.VehicleTypeSelected == '4_door':
            # left
            wheelPlaneLF = CreateTranslationButton( "lf_suspension_plane", 103, 410, self.MainLayout, "", "Left - Front wheel suspension")
            wheelPlaneLR = CreateTranslationButton( "lr_suspension_plane", 337, 410, self.MainLayout, "", "Left - Rear wheel suspension")         
            doorDSideFControl = CreateRotationButton( "door_dside_f_handle", 220, 332, self.MainLayout, "", "Driver side - Front door handle")        
            doorDSideRControl = CreateRotationButton( "door_dside_r_handle", 301, 330, self.MainLayout, "", "Driver side - Rear door handle") 
            manipulateLF = CreateTranslationButton( "lf_chassis_manipulate", 120, 282, self.MainLayout, "", "Left - Front chassis suspension")        
            manipulateLR = CreateTranslationButton( "lr_chassis_manipulate", 380, 280, self.MainLayout, "", "Left - Rear chassis suspension") 
            overrideLF = CreateRotationButton( "lf_override", 103, 378, self.MainLayout, "", "Left - Front wheel override" ) 
            overrideLR = CreateRotationButton( "lr_override", 337, 378, self.MainLayout, "", "Left - Rear wheel override" ) 
            # right
            wheelPlaneRF = CreateTranslationButton( "rf_suspension_plane", 337, 595, self.MainLayout, "", "Right - Front wheel suspension")
            wheelPlaneRR = CreateTranslationButton( "rr_suspension_plane", 103, 595, self.MainLayout, "", "Right - Rear wheel suspension")
            doorPSideFControl = CreateRotationButton( "door_pside_f_handle", 221, 518, self.MainLayout, "", "Passenger side - Front door handle")        
            doorPSideRControl = CreateRotationButton( "door_pside_r_handle", 138, 517, self.MainLayout, "", "Passenger side - Rear door handle") 
            manipulateRF = CreateTranslationButton( "rf_chassis_manipulate", 330, 472, self.MainLayout, "", "Right - Front chassis suspension") 
            manipulateRR = CreateTranslationButton( "rr_chassis_manipulate", 60, 470, self.MainLayout, "", "Right - Rear chassis suspension")  
            overrideRF = CreateRotationButton( "rf_override", 337, 565, self.MainLayout, "",  "Right - Front wheel override" ) 
            overrideRR = CreateRotationButton( "rr_override", 103, 565, self.MainLayout, "",  "Right - Rear wheel override" ) 

            # front
            positioningRoot = CreateTransformButton( "rig", 108, 106, self.MainLayout, "", "Rig root" )
            steeringWheel = CreateRotationButton( "steering_wheel", 142, 155, self.MainLayout, "", "Steering wheel" )
            # back
            bootControl = CreateRotationButton( "boot_handle", 332, 171, self.MainLayout, "", "Boot handle")

            self.ButtonList = [ wheelPlaneLF, wheelPlaneLR, doorDSideFControl, doorDSideRControl, manipulateLF, manipulateLR, 
                                wheelPlaneRF, wheelPlaneRR, doorPSideRControl, doorPSideFControl, manipulateRF, manipulateRR, 
                                positioningRoot, bootControl, steeringWheel, overrideLF, overrideLR, overrideRF, overrideRR ]

        elif self.VehicleTypeSelected == '2_door':
            # left
            wheelPlaneLF = CreateTranslationButton( "lf_suspension_plane", 103, 410, self.MainLayout, "", "Left - Front wheel suspension")
            wheelPlaneLR = CreateTranslationButton( "lr_suspension_plane", 337, 410, self.MainLayout, "", "Left - Rear wheel suspension")         
            doorDSideFControl = CreateRotationButton( "door_dside_f_handle", 301, 332, self.MainLayout, "", "Driver side - Front door handle")        
            manipulateLF = CreateTranslationButton( "lf_chassis_manipulate", 120, 282, self.MainLayout, "", "Left - Front chassis suspension")        
            manipulateLR = CreateTranslationButton( "lr_chassis_manipulate", 380, 280, self.MainLayout, "", "Left - Rear chassis suspension") 
            overrideLF = CreateRotationButton( "lf_override", 103, 378, self.MainLayout, "", "Left - Front wheel override" ) 
            overrideLR = CreateRotationButton( "lr_override", 337, 378, self.MainLayout, "", "Left - Rear wheel override" ) 
            # right
            wheelPlaneRF = CreateTranslationButton( "rf_suspension_plane", 337, 595, self.MainLayout, "", "Right - Front wheel suspension")
            wheelPlaneRR = CreateTranslationButton( "rr_suspension_plane", 103, 595, self.MainLayout, "", "Right - Rear wheel suspension")
            doorPSideFControl = CreateRotationButton( "door_pside_f_handle", 138, 518, self.MainLayout, "", "Passenger side - Front door handle")        
            manipulateRF = CreateTranslationButton( "rf_chassis_manipulate", 330, 472, self.MainLayout, "", "Right - Front chassis suspension") 
            manipulateRR = CreateTranslationButton( "rr_chassis_manipulate", 60, 470, self.MainLayout, "", "Right - Rear chassis suspension")  
            overrideRF = CreateRotationButton( "rf_override", 337, 565, self.MainLayout, "",  "Right - Front wheel override" ) 	    
            overrideRR = CreateRotationButton( "rr_override", 103, 565, self.MainLayout, "",  "Right - Rear wheel override" ) 
            # front
            positioningRoot = CreateTransformButton( "rig", 108, 106, self.MainLayout, "", "Rig root" )
            steeringWheel = CreateRotationButton( "steering_wheel", 142, 155, self.MainLayout, "", "Steering wheel" )
            # back
            bootControl = CreateRotationButton( "boot_handle", 332, 171, self.MainLayout, "", "Boot handle")        

            self.ButtonList = [ wheelPlaneLF, wheelPlaneLR, doorDSideFControl, manipulateLF, manipulateLR, 
                                wheelPlaneRF, wheelPlaneRR, doorPSideFControl, manipulateRF, manipulateRR, 
                                positioningRoot, bootControl, steeringWheel, overrideLF, overrideLR, overrideRF, overrideRR ]

        elif self.VehicleTypeSelected == 'van':
            # left
            wheelPlaneLF = CreateTranslationButton( "lf_suspension_plane", 97, 430, self.MainLayout, "", "Left - Front wheel suspension")
            wheelPlaneLR = CreateTranslationButton( "lr_suspension_plane", 334, 430, self.MainLayout, "", "Left - Rear wheel suspension")         
            doorDSideFControl = CreateRotationButton( "door_dside_f_handle", 181, 349, self.MainLayout, "", "Driver side - Front door handle")        
            manipulateLF = CreateTranslationButton( "lf_chassis_manipulate", 120, 302, self.MainLayout, "", "Left - Front chassis suspension")        
            manipulateLR = CreateTranslationButton( "lr_chassis_manipulate", 390, 300, self.MainLayout, "", "Left - Rear chassis suspension") 
            overrideLF = CreateRotationButton( "lf_override", 97, 405, self.MainLayout, "", "Left - Front wheel override" ) 
            overrideLR = CreateRotationButton( "lr_override", 334, 405, self.MainLayout, "", "Left - Rear wheel override" ) 
            # right
            wheelPlaneRF = CreateTranslationButton( "rf_suspension_plane", 345, 600, self.MainLayout, "", "Right - Front wheel suspension")
            wheelPlaneRR = CreateTranslationButton( "rr_suspension_plane", 110, 600, self.MainLayout, "", "Right - Rear wheel suspension")
            doorPSideFControl = CreateRotationButton( "door_pside_f_handle", 261, 518, self.MainLayout, "", "Passenger side - Front door handle")        
            manipulateRF = CreateTranslationButton( "rf_chassis_manipulate", 325, 472, self.MainLayout, "", "Right - Front chassis suspension") 
            manipulateRR = CreateTranslationButton( "rr_chassis_manipulate", 55, 470, self.MainLayout, "", "Right - Rear chassis suspension")  
            overrideRF = CreateRotationButton( "rf_override", 345, 575, self.MainLayout, "",  "Right - Front wheel override" ) 			
            overrideRR = CreateRotationButton( "rr_override", 110, 575, self.MainLayout, "",  "Right - Rear wheel override" ) 
            # front
            positioningRoot = CreateTransformButton( "rig", 108, 102, self.MainLayout, "", "Rig root" )
            steeringWheel = CreateRotationButton( "steering_wheel", 130, 155, self.MainLayout, "", "Steering wheel" )
            # back
            doorDSideRControl = CreateRotationButton( "door_dside_r_handle", 320, 180, self.MainLayout, "", "Driver side - Rear door handle")             
            doorPSideRControl = CreateRotationButton( "door_pside_r_handle", 352, 180, self.MainLayout, "", "Passenger side - Rear door handle") 

            self.ButtonList = [ wheelPlaneLF, wheelPlaneLR, doorDSideFControl, doorDSideRControl, manipulateLF, manipulateLR, 
                                wheelPlaneRF, wheelPlaneRR, doorPSideRControl, doorPSideFControl, manipulateRF, manipulateRR, 
                                positioningRoot, steeringWheel, overrideLF, overrideLR, overrideRF, overrideRR ]            

        elif self.VehicleTypeSelected == 'motorbike':
            # right
            wheelPlaneF = CreateTranslationButton( "f_suspension_plane", 340, 538, self.MainLayout, "", "Front wheel suspension")
            overrideF = CreateRotationButton( "b_f_Master", 340, 475, self.MainLayout, "", "Front wheel override" ) 
            wheelPlaneR = CreateTranslationButton( "r_suspension_plane", 115, 538, self.MainLayout, "", "Rear wheel suspension")
            overrideR = CreateRotationButton( "b_r_Master", 115, 475, self.MainLayout, "",  "Rear wheel override" ) 	

            chassisControl = CreateTranslationButton( "chassis_Control", 220, 418, self.MainLayout, "", "Chassis control")
            swivelF = CreateTranslationButton( "f_swivel", 340, 418, self.MainLayout, "", "Front swivel")
            swivelR = CreateTranslationButton( "mover_swivel", 115, 418, self.MainLayout, "", "Mover swivel")

            # front
            positioningRoot = CreateTransformButton( "rig", 106, 120, self.MainLayout, "", "Rig root" )
            frontHandle = CreateTransformButton( "Front_Handle", 106, 300, self.MainLayout, "", "Front rig handle" )
            # back
            handlebarsControl = CreateRotationButton( "Handlebar_Controller", 331, 150, self.MainLayout, "", "Handlebar control") 
            rearHandle = CreateTransformButton( "Rear Handle", 331, 300, self.MainLayout, "", "Front rig handle" )

            self.ButtonList = [ wheelPlaneF, overrideF, wheelPlaneR, overrideR, chassisControl, swivelF, swivelR, 
                                positioningRoot, frontHandle, handlebarsControl, rearHandle ]

        else:
            None

        for i in self.ButtonList:
            i.OnClick.Add( self.SelectNullCallback )

'''
Run UI
'''
def Run( show = True ):
    tool = VehicleControlsToolbox()

    if show:
        tool.Show()

    return tool