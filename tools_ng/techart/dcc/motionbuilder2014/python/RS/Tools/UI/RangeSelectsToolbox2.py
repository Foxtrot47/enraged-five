import os

import pyfbsdk as mobu
from PySide import QtCore, QtGui

from RS import Config
from RS.Tools import UI
from RS.Core.Animation import SelectsFrameSet as core
from RS.Core.Mocap import MocapCommands
from RS.Core.Mocap import Clapper


START_FRAME_COLUMN = 0
END_FRAME_COLUMN = 1
SOFT_RANGE_COLUMN = 2
LABEL_COLUMN = 3


class Range(object):
    def __init__(self, label, start, end):
        self.Label = label
        self.Start = start
        self.End = end

    def ColumnData(self, columnIdx):
        if columnIdx == START_FRAME_COLUMN:
            return self.Start

        elif columnIdx == END_FRAME_COLUMN:
            return self.End

        elif columnIdx == SOFT_RANGE_COLUMN:
            return "{0}~{1}".format(self.Start, self.End)

        elif columnIdx == LABEL_COLUMN:
            return self.Label

        return None


class RangeTableModel(QtCore.QAbstractTableModel):
    def __init__(self, parent, header):
        super(RangeTableModel, self).__init__(parent)

        # A collection of Range() objects.
        self.__ranges = []
        self.__header = header

    # # Properties ##
    @property
    def Ranges(self):
        return self.__ranges

    # # Methods ##
    def ClearRanges(self):
        self.__ranges = []

    def GetRange(self, label):
        for item in self.__ranges:
            if item.Label.lower() == label.lower():
                return item
        return None

    def AddNewRange(self, startFrame, endFrame, label):
        if label == '':
            QtGui.QMessageBox.critical(None, "Rockstar", "A range label cannot be empty!", QtGui.QMessageBox.Ok)
            return False

        existingRange = self.GetRange(label)

        if (existingRange == None):
            newRange = Range(label, startFrame, endFrame)
            self.__ranges.append(newRange)
            self.layoutChanged.emit()
            return True

        else:
            QtGui.QMessageBox.critical(None, "Rockstar", "A range with that label already exists!", QtGui.QMessageBox.Ok)

        return False

    def DeleteRange(self, label):
        item = self.GetRange(label)

        if (item != None):
            self.__ranges.remove(item)
            self.layoutChanged.emit()
            return

        return False

    # # Overrides ##
    def rowCount(self, parent=None):
        return len(self.__ranges)

    def columnCount(self, parent=None):
        return len(self.__header)

    def data(self, index, role):
        if role == QtCore.Qt.EditRole or role == QtCore.Qt.DisplayRole:
            return self.__ranges[index.row()].ColumnData(index.column())

        if role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignCenter

        return None

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.__header[col]

        return None

    def flags(self, index=None):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable


class RangesTableItemDelegate(QtGui.QStyledItemDelegate):
    def __init__(self, parent=None):
        super(RangesTableItemDelegate, self).__init__(parent=parent)
        self.__index = None

    def createEditor(self, parent, option, index):
        self.__index = index
        editor = None

        if index.column() != LABEL_COLUMN:
            editor = QtGui.QPushButton(str(index.data()), parent)
            editor.pressed.connect(self.OnClick)

        else:
            # editor = QtGui.QLineEdit(parent)
            # editor.text = index.data
            # editor.textChanged.connect(self.OnLabelEdit)
            pass

        return editor

    def OnLabelEdit(self):
        print self.__index.data()

    def OnClick(self):
        if self.__index.column() == SOFT_RANGE_COLUMN:
            start, end = str(self.__index.data()).split('~')
            startFrame = mobu.FBTime(0, 0, 0, int(start))
            endFrame = mobu.FBTime(0, 0, 0, int(end))
            startTime = startFrame.Get()
            endTime = endFrame.Get()
            playerControl = mobu.FBPlayerControl()
            playerControl.ZoomWindowStart = mobu.FBTime(startTime)
            playerControl.ZoomWindowStop = mobu.FBTime(endTime)

        elif self.__index.column() == START_FRAME_COLUMN or self.__index.column() == END_FRAME_COLUMN:
            frame = mobu.FBTime(0, 0, 0, self.__index.data())
            playerControl = mobu.FBPlayerControl()
            playerControl.Goto(frame)


class MainWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MainWidget, self).__init__(parent=parent)

        # Setting main layout to vertical box layout
        mainLayout = QtGui.QVBoxLayout()
        self.setLayout(mainLayout)
        self.setAutoFillBackground(True)

        # Zero scene
        zeroSceneGroupBox = QtGui.QGroupBox("Time Shift Scene")
        zeroSceneLayout = QtGui.QVBoxLayout()

        zeroSceneSpinnerLayout = QtGui.QHBoxLayout()
        zeroSceneLabel = QtGui.QLabel("Frame:")
        self.zeroSceneFrameSpinner = QtGui.QSpinBox()
        self.zeroSceneFrameSpinner.setRange(-999999, 999999)
        zeroSceneSpinnerLayout.addWidget(zeroSceneLabel)
        zeroSceneSpinnerLayout.addWidget(self.zeroSceneFrameSpinner, 0, QtCore.Qt.AlignLeft)
        zeroSceneSpinnerLayout.addStretch()

        zeroScene = QtGui.QPushButton("Apply")
        zeroScene.clicked.connect(self.OnZeroScene_Clicked)

        zeroSceneLayout.addLayout(zeroSceneSpinnerLayout)
        zeroSceneLayout.addWidget(zeroScene)
        zeroSceneGroupBox.setLayout(zeroSceneLayout)

        # Create Range
        createRangeGroupBox = QtGui.QGroupBox("Create Range")
        createRangeLayout = QtGui.QVBoxLayout()

        # Start Frame
        startEndFrameLayout = QtGui.QHBoxLayout()
        startLabel = QtGui.QLabel("Start Frame:")
        self.startSpinner = QtGui.QSpinBox()
        self.startSpinner.setRange(-999999, 999999)
        currentFrameStartButton = QtGui.QPushButton("<")

        startEndFrameLayout.addWidget(startLabel)
        startEndFrameLayout.addWidget(self.startSpinner)
        startEndFrameLayout.addWidget(currentFrameStartButton)

        # End Frame
        endLabel = QtGui.QLabel("End Frame:")
        self.endSpinner = QtGui.QSpinBox()
        self.endSpinner.setRange(-999999, 999999)
        currentFrameEndButton = QtGui.QPushButton("<")

        startEndFrameLayout.addSpacing(10)
        startEndFrameLayout.addWidget(endLabel)
        startEndFrameLayout.addWidget(self.endSpinner)
        startEndFrameLayout.addWidget(currentFrameEndButton)

        startEndFrameLayout.addStretch()

        currentFrameStartButton.pressed.connect(self.OnAddCurrentFrameStart_Clicked)
        currentFrameEndButton.pressed.connect(self.OnAddCurrentFrameEnd_Clicked)

        # Range Label
        rangeLabelLayout = QtGui.QHBoxLayout()
        rangeLabel = QtGui.QLabel("Label:")
        self.rangeLabelText = QtGui.QLineEdit()
        self.rangeLabelText.setPlaceholderText("Enter label name (30 character limit)")
        self.rangeLabelText.setMaxLength(30)
        rangeLabelLayout.addWidget(rangeLabel)
        rangeLabelLayout.addWidget(self.rangeLabelText)

        # Add Range
        addRange = QtGui.QPushButton("Add Range")
        addRange.width = 200
        addRange.clicked.connect(self.OnAddRange_Clicked)

        createRangeLayout.addLayout(startEndFrameLayout)
        createRangeLayout.addLayout(rangeLabelLayout)
        createRangeLayout.addWidget(addRange, 0, QtCore.Qt.AlignRight)

        createRangeGroupBox.setLayout(createRangeLayout)

        # Ranges Groupbox
        rangesGroupbox = QtGui.QGroupBox("Ranges")
        rangesLayout = QtGui.QVBoxLayout()
        rangesGroupbox.setLayout(rangesLayout)

        header = ['Start Frame', 'End Frame', 'Soft Range', 'Label']
        self.rangesModel = RangeTableModel(self, header)

        self.rangesTable = QtGui.QTableView()

        rangesTableDelegate = RangesTableItemDelegate()
        self.rangesTable.setItemDelegate(rangesTableDelegate)
        self.rangesTable.horizontalHeader().setDefaultAlignment(QtCore.Qt.AlignCenter)
        self.rangesTable.horizontalHeader().setStretchLastSection(True)
        self.rangesTable.setSelectionBehavior(QtGui.QTableView.SelectRows)
        self.rangesTable.setModel(self.rangesModel)
        self.rangesTable.setEditTriggers(QtGui.QAbstractItemView.AllEditTriggers)
        rangesLayout.addWidget(self.rangesTable)

        deleteRange = QtGui.QPushButton("Delete Selected")
        deleteRange.clicked.connect(self.OnDeleteSelected_Clicked)

        # Notes
        notesLabel = QtGui.QLabel("Notes:")
        self.notesField = QtGui.QPlainTextEdit()

        # Save / Load
        saveLoadLayout = QtGui.QHBoxLayout()

        saveRanges = QtGui.QPushButton("Save Ranges")
        saveRanges.clicked.connect(self.SaveRanges)
        loadRanges = QtGui.QPushButton("Load Ranges")
        loadRanges.clicked.connect(self.LoadRanges)

        saveLoadLayout.addWidget(saveRanges)
        saveLoadLayout.addWidget(loadRanges)

        rangesLayout.addWidget(deleteRange, 0, QtCore.Qt.AlignRight)
        rangesLayout.addWidget(notesLabel)
        rangesLayout.addWidget(self.notesField)
        rangesLayout.addLayout(saveLoadLayout)

        # Create / Delete Takes
        takesGroupbox = QtGui.QGroupBox("Takes")
        takesLayout = QtGui.QVBoxLayout()
        takesGroupbox.setLayout(takesLayout)

        self.takesList = QtGui.QComboBox()

        takesButtonsLayout = QtGui.QHBoxLayout()
        createAllTakes = QtGui.QPushButton("Create All")
        createAllTakes.clicked.connect(self.OnCreateAll_Clicked)
        createTake = QtGui.QPushButton("Create New Take")
        createTake.clicked.connect(self.OnCreateNewTake_Clicked)

        takesButtonsLayout.addWidget(createAllTakes)
        takesButtonsLayout.addWidget(createTake)
        takesLayout.addWidget(self.takesList)
        takesLayout.addLayout(takesButtonsLayout)

        mainLayout.addWidget(zeroSceneGroupBox)
        mainLayout.addWidget(createRangeGroupBox)
        mainLayout.addWidget(rangesGroupbox)
        mainLayout.addWidget(takesGroupbox)

        self.SetSlateFrame()

    # # Methods ##
    def SetSlateFrame(self):
        clapsDict = Clapper.GetClaps()

        if (len(clapsDict) != 0):
            frame = clapsDict.values()[0][0]
            self.zeroSceneFrameSpinner.setValue(frame)

    def GetSelectedRanges(self):
        result = []
        indices = set([idx.row() for idx in self.rangesTable.selectionModel().selectedIndexes() if idx.row() > -1])
        
        for idx in indices:
            item = self.rangesModel.Ranges[idx]
            result.append(item)

        return result

    def Refresh(self):
        self.RefreshTakesCombobox()

    # # Event Handlers ##
    def OnZeroScene_Clicked(self):
        MocapCommands.ZeroOutScene(self.zeroSceneFrameSpinner.value())

    def OnAddCurrentFrameStart_Clicked(self):
        self.startSpinner.setValue(mobu.FBSystem().LocalTime.GetFrame())

    def OnAddCurrentFrameEnd_Clicked(self):
        self.endSpinner.setValue(mobu.FBSystem().LocalTime.GetFrame())

    def OnAddRange_Clicked(self):
        start = self.startSpinner.value()
        end = self.endSpinner.value()
        label = self.rangeLabelText.text()

        self.rangesModel.AddNewRange(start, end, label)
        self.Refresh()

    def OnDeleteSelected_Clicked(self):
        ranges = self.GetSelectedRanges()

        for item in ranges:
            self.rangesModel.DeleteRange(item.Label)

        self.Refresh()

    def OnCreateAll_Clicked(self):
        labels = [self.takesList.itemText(i) for i in range(self.takesList.count())]

        for label in labels:
            item = self.rangesModel.GetRange(label)

            if (item != None):
                core.CreateNewTake(item.Label, item.Start, item.End)

    def OnCreateNewTake_Clicked(self):
        item = self.rangesModel.GetRange(self.takesList.currentText())

        if (item != None):
            core.CreateNewTake(item.Label, item.Start, item.End)

    def RefreshTakesCombobox(self):
        self.takesList.clear()

        for item in self.rangesModel.Ranges:
            self.takesList.addItem(item.Label)

    def SaveRanges(self):
        if (len(self.rangesModel.Ranges) > 0):
            fileName, fileType = QtGui.QFileDialog.getSaveFileName(self, "Pick Name", Config.Project.Path.Root, "Meta File (*.xml)")

            if (fileName != ''):
                core.CreateMetaFile2(str(fileName), self.rangesModel.Ranges, self.notesField.toPlainText())

        else:
            QtGui.QMessageBox.critical(None, "Rockstar", "No ranges to save!", QtGui.QMessageBox.Ok)

    def LoadRanges(self):
        fileName, fileType = QtGui.QFileDialog.getOpenFileName(self, "Pick Meta", Config.Project.Path.Root, "Meta File (*.xml)")
        if (fileName != ''):
            result = core.OpenMetaFile2(fileName)

            if (len(result) > 0):
                notes, ranges = result

                self.notesField.setPlainText(notes)
                self.rangesModel.ClearRanges()

                for item in ranges:
                    start, end, label = item
                    self.rangesModel.AddNewRange(start, end, label)

                self.Refresh()


@UI.Run('Range Selects Toolbox', size=[500, 800], dockable=True,
        url=r'https://hub.rockstargames.com/display/ANIM/Range+Selects+Toolbox')
def Run():
	return MainWidget()
