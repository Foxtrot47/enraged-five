"""
Description:
    ProgressBar Qt Widget

Author:
    David Vega <david.vega@rockstargames.com>
"""

import os
import json
import time
import datetime

from PySide import QtGui, QtCore

# Temporary solution until we move python independent code to its own directory structure and start using the
# RS Hook to resolve the correct library to use
try:
    from RS.Core.Server import Settings
except ImportError:
    import Settings

# HTML Colors for the progress bar. This is so if multiple progres bars are going off at once, max 10, they
# have different colors to identify them. Could be namedTuple
HTML_COLORS = [('#78d', '#238'), ('#ff8080', '#ff0000'), ('#7db07d', '#008000'), ("#ffc080", "#ff8000"),
               ("#80627f", "#80227f"), ("#e3cc71", "#e3b500"), ("#80c1ff", "#0082ff"), ("#552a2a", "#550000"),
               ("#cccccc", "#666666"), ("#7f007f", "#7f407f")]


class ProgressBarWidget(QtGui.QWidget):
    """
    Progress Bar Widget
    """

    _StyleSheet = """
                    QWidget{
                    background:transparent;
                    }

                    QLCDNumber{
                    background-color: #2b2b2b;
                    border: 1px solid white;
                    border-bottom-right-radius: 7px;
                    }

                    QProgressBar {
                    border: 1px solid white;
                    text-align: center;
                    border-bottom-right-radius: 0px;
                    border-bottom-left-radius: 7px;
                    background-color: #2b2b2b;
                    width: 15px;
                    color: white;
                    font: bold 11px;
                    }

                    QProgressBar::chunk {
                    background: QLinearGradient( x1: 0, y1: 0, x2: 1, y2: 0,
                    stop: 0 %s,
                    stop: 1 %s);
                    border-bottom-right-radius: 7px;
                    border-bottom-left-radius: 7px;
                    }
                """

    def __init__(self, text="", value=0, minimum=0, maximum=100, size=(500, 50), portNumber=11091, parent=None):
        """
        Constructor

        Arguments:

            text (string): text for the progress bar
            value (int): value of the progressbar
            minimum (int): minimum value the progress bar takes
            maximum (int): maximum value the progress bar takes
            size ([int, int]): size of the window
            portNumber (int): port number for the python executable running the UI
            parent (QWidget): parent widget

        """
        super(ProgressBarWidget, self).__init__(parent, QtCore.Qt.FramelessWindowHint)

        # Get root folder of the tool
        basepath = os.path.dirname(__file__)
        basepath = os.path.dirname(basepath)

        # Get path to the json file the progress bar monitors
        self.MONITOR_FILE_PATH = os.path.join(basepath, "monitor{0:03}.json".format(portNumber - 11090))

        # Create/Reset the file the tool monitors
        if not os.path.exists(self.MONITOR_FILE_PATH):
            with open(self.MONITOR_FILE_PATH, "r") as monitorFile:
                json.dump({"text": text, "value": value, "minimum": minimum, "maximum": maximum}, monitorFile)

        # Set the color of the progress bar
        self._StyleSheet = self._StyleSheet % HTML_COLORS[portNumber-11091]

        # build UI
        self.setupUi()

        # resize UI
        self.bar.resize(QtCore.QSize(*size))
        self.resize(QtCore.QSize(*size))
        self.time = QtCore.QTime()
        self.time.start()

        # Setup Thread that monitors changes in file the tool monitors
        self.worker = Worker(self.MONITOR_FILE_PATH)
        self.worker.Update.connect(self.Update)
        self.worker.Close.connect(self.Close)
        self.worker.start()

    def setupUi(self):
        """
        Sets up the UI
        """
        layout = QtGui.QGridLayout()
        self.setWindowTitle("Progress Bar")

        self.bar = QtGui.QProgressBar()

        # widget = QtGui.QFrame()
        # widgetLayout = QtGui.QVBoxLayout()
        self.timeLCD = QtGui.QLCDNumber()
        # widgetLayout.addWidget(self.timeLCD)
        # widget.setLayout(widgetLayout)
        # widget.setFrameShape(widget.Panel)

        layout.addWidget(self.bar, 0, 0)
        layout.addWidget(self.timeLCD, 0, 1)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.bar.setTextVisible(True)

        # High value so the progressbar scales
        self.bar.setMaximumHeight(1000)

        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setLayout(layout)
        self.setStyleSheet(self._StyleSheet)

    def mousePressEvent(self, event):
        """
        Overrides the mousePressEvent of QWidget, registers the position of the UI on the screen when the mouse clicks
        it

        Argument:
            event (Core.Qt.Event): event registered by Qt
        """
        if event.button() == QtCore.Qt.LeftButton:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()
            event.accept()

    def mouseMoveEvent(self, event):
        """
        Overrides the mousePressEvent of QWidget, changes the position of the UI to where the mouse is dragging the UI

        Argument:
            event (Core.Qt.Event): event registered by Qt
        """
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)
            event.accept()

    def Update(self, text="", value=0, minimum=None, maximum=None):
        """
        Updates the values of the Progress Bar

        Arguments:
            text (string): text for the progress bar
            value (int): value of the progressbar
            minimum (int): minimum value the progress bar takes
            maximum (int): maximum value the progress bar takes
        """
        # Get current values for minimum and maximum if None is passed

        minimum, maximum = [minimum or self.bar.minimum(), maximum or self.bar.maximum()]
        self.bar.setRange(minimum, maximum)
        self.bar.setValue(value)

        # Set Text
        percent = round(value * (1/float(maximum - minimum)) * 100)
        finalText = "{}%".format(percent)
        if text:
            finalText = "{} : {}%".format(text, percent, 2)

        self.bar.setFormat(finalText)
        if value >= maximum:
            self.worker.finish = True

        elapsedTime = self.time.elapsed()
        self.timeLCD.display(str(datetime.timedelta(0, elapsedTime/1000)))

    def Close(self):
        """ Closes the Progress Bar """
        QtCore.QCoreApplication.instance().quit()


class Worker(QtCore.QThread):
    """ Thread that watches json file so it can update the Progress Bar Widget """

    Update = QtCore.Signal(basestring, int, int, int)
    Close = QtCore.Signal()

    def __init__(self, monitorFilePath):
        """
        Constructor

        Arguments:
            monitorFilePath (string): path to json file to monitor
        """
        super(Worker, self).__init__()
        self.monitorFilePath = monitorFilePath
        self.finish = False

    def run(self):
        """
        Overrides QThreads run method
        Updates the progress bar based on the contents of the file the thread monitors. The thread exists and closes
        the UI when the finished variable is True or the file that was being monitored is removed/deleted from its
        directory
        """
        while os.path.isfile(self.monitorFilePath) and not self.finish:
            self.Monitor()
            self.Update.emit(self.text, self.value, self.minimum, self.maximum)
            time.sleep(0.1)
        time.sleep(1)
        # Emit close signal to close the UI
        self.Close.emit()

    def Monitor(self):
        """ Reads the contents of the file being monitored """

        data = {}

        # Convert the data from the json file into a dictionary
        monitorfile = open(self.monitorFilePath, "r")
        content = monitorfile.read()
        if content.strip():
            data = json.loads(content)
        monitorfile.close()

        self.__dict__.update(data)
