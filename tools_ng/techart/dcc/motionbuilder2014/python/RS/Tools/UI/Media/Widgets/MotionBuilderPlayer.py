from PySide import QtGui, phonon

import pyfbsdk as mobu

from RS.Utils.Scene import Time
from RS.Tools.UI.Media.Widgets import VideoPlayer


class MotionBuilderPlayer(VideoPlayer.VideoPlayer):
    """ Video Player that can be synced to the motion builder time line """
    def __init__(self, *args, **kwargs):
        """
        Constructor

        :param args:
        :param kwargs:
        :return:
        """
        super(MotionBuilderPlayer, self).__init__(*args, **kwargs)
        self.__synched = False
        self.Suppress = False
        
        self.SyncButton = QtGui.QPushButton("Sync Motion Builder")
        self.OffsetBox = QtGui.QSpinBox()
        self.OffsetBox.setEnabled(False)
        self.OffsetBox.setMaximum(10000)
        
        horizontalLayout = QtGui.QHBoxLayout()
        horizontalLayout.setSpacing(0)
        [horizontalLayout.addWidget(each) for each in [self.SyncButton, self.OffsetBox]]

        self.SyncButton.setMinimumWidth(480)
        self.SyncButton.clicked.connect(self.Sync)
        self.OffsetBox.editingFinished.connect(lambda *arguments: self.totalTimeChanged(self.video.totalTime()))

        self.SyncButton.setStyleSheet("""
            border-style: outset;
            border-width: 1px;
            border-color: beige;
            background-color: green;
            """)

        self.video._Video__singleinstance.mediaObject().stateChanged.connect(self.stateChange)
        self.video._Video__singleinstance.mediaObject().totalTimeChanged.connect(self.totalTimeChanged)

        self.mainLayout.addLayout(horizontalLayout)

        self._timelineMonitor = Time.TimelineMonitor()
        self._timelineMonitor.TimelineMethod = self.TimeLineMethod

    def Seek(self, frame):
        """
        Goes to the specified frame

        Arguments:
            frame = int; frame to go to
        """
        time = super(MotionBuilderPlayer, self).Seek(frame, isFrame=True)
        frame = time / self.video.secondsPerFrame
        if self.__synched:
            self.Suppress = True
            mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0, int(round((frame + self.OffsetBox.value())))))
            self.Suppress = False

    def Sync(self):
        """ Sync slider to MB time"""
        self.__synched = bool(1 - self.__synched)
        self.SyncButton.setText("{} Motion Builder".format(["Sync", "Unsync"][self.__synched]))
        self.SyncButton.setStyleSheet("""
            border-style: outset;
            border-width: 1px;
            border-color: beige;
            background-color: {};
            """.format(["green", "red"][self.__synched]))

        self.OffsetBox.setEnabled(self.__synched)

        if self.__synched:
            endFrame = int(round(self.video.totalFrames + self.OffsetBox.value()))
            mobu.FBSystem().CurrentTake.LocalTimeSpan = mobu.FBTimeSpan(
                mobu.FBTime(0, 0, 0, 0, 0),  mobu.FBTime(0, 0, 0, endFrame, 0))
            self._timelineMonitor.Register()

        elif not self.__synched:
            mobu.FBPlayerControl().Stop()
            self._timelineMonitor.Unregister()

        if self.__synched and self.video._Video__singleinstance.mediaObject().state() == phonon.Phonon.PlayingState:
            frame = int(self.video.currentFrame) + self.OffsetBox.value()
            mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0, frame))
            mobu.FBPlayerControl().Play()

    def TimeLineMethod(self, currentFrame, previousFrame):
        """
        Updates the player when the time changes in motion builder
        Arguments:
            currentFrame: int; the current frame
            previousFrame: int; the previous frame
        """
        difference = abs(currentFrame - previousFrame)

        check1 = mobu.FBPlayerControl().IsPlaying and difference > 1
        check2 = not mobu.FBPlayerControl.IsPlaying and difference

        if not self.Suppress and (check1 or check2):
            super(MotionBuilderPlayer, self).Seek(currentFrame, isFrame=True)

    def stateChange(self, currentState, newState):
        """ Code to run whenever the video changes states between playing, paused, loading and stopped"""
        super(MotionBuilderPlayer, self).stateChange(currentState, newState)

        if self.__synched:
            if currentState in [phonon.Phonon.PausedState, phonon.Phonon.StoppedState]:
                mobu.FBPlayerControl().Stop()

            elif currentState == phonon.Phonon.PlayingState:
                frame = int(round(self.video.currentFrame) + self.OffsetBox.value())
                mobu.FBPlayerControl().Goto(mobu.FBTime(0, 0, 0,frame))
                mobu.FBPlayerControl().Play()

    def totalTimeChanged(self, newTotalTime):
        """ Code to run whenever the lenght of a new video has been determined """
        if self.__synched:
            totalframes = (float(newTotalTime) / self.video.secondsPerFrame) + self.OffsetBox.value()
            mobu.FBSystem().CurrentTake.LocalTimeSpan = mobu.FBTimeSpan(
                mobu.FBTime(0, 0, 0, self.OffsetBox.value(), 0), mobu.FBTime(0, 0, 0, int(totalframes), 0))