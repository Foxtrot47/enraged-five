"""
Tree Widget for displaying files from Perforce
"""
import os
from collections import OrderedDict

from PySide import QtGui, QtCore

from RS import Config
from RS import Perforce


class TreeFilterModel(QtGui.QSortFilterProxyModel):
    """
    Custom QSortFilterProxyModel so that when a match is found it also finds the parents
    """

    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        Took from http://stackoverflow.com/questions/250890/using-qsortfilterproxymodel-with-a-tree-model
        If a child matches the filter string, show the parents

        Arguments:
            sourceRow (int): location of the row
            sourceParent (QtGui.QModelIndex): index of the parent

        Return:
            boolean
        """
        if not self.filterRegExp().isEmpty():
            sourceIndex = self.sourceModel().index(sourceRow, self.filterKeyColumn(), sourceParent)
            if sourceIndex.isValid():
                for index in xrange(self.sourceModel().rowCount(sourceIndex)):
                    if self.filterAcceptsRow(index, sourceIndex):
                        return True
                currentString = str(self.sourceModel().data(sourceIndex, self.filterRole()))

                return self.filterRegExp().pattern().lower() in currentString.lower()

        return super(TreeFilterModel, self).filterAcceptsRow(sourceRow, sourceParent)


class FileTreeWidget(QtGui.QWidget):
    """
    Tree widget for displaying files from perforce
    """
    selectionChangedSignal = QtCore.Signal()

    def __init__(self, parent=None, title="Open Cutscene", rootDirectories=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
            title (string): title for the widget
            rootDirectory (string): root directory where the files that should be displayed live
        """
        super(FileTreeWidget, self).__init__(parent=parent)
        rootDirectories = rootDirectories or []

        self._block = False
        self._enableAutoFilter = True
        self._excludeFilters = []

        self.Model = QtGui.QStandardItemModel()
        self.Filter = TreeFilterModel()
        self.Filter.setSourceModel(self.Model)
        self.Filter.setFilterRole(QtCore.Qt.UserRole)
        self.FilterBox = QtGui.QLineEdit()
        self.FilterBox.textChanged.connect(self.textChanged)
        self.FilterBox.returnPressed.connect(lambda *args: self.filterText(self.FilterBox.text()))
        self.Tree = QtGui.QTreeView()
        self.Tree.setModel(self.Filter)
        self.Tree.selectionChanged = self.selectionChanged

        self._currentSelection = None
        self.rootDirectories = rootDirectories

        self.setWindowTitle(title)

        self.Files = OrderedDict()
        self.Files.QStandardItem = self.Model

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.FilterBox)
        layout.addWidget(self.Tree)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.Tree.expanded.connect(self.expanded)
        self.Tree.collapsed.connect(self.expanded)

        self.Tree.setSelectionMode(self.Tree.SingleSelection)
        self.Tree.setMouseTracking(False)
        self.Tree.setDragEnabled(False)
        self.Tree.setAcceptDrops(False)
        self.setLayout(layout)

    def PopulateView(self):
        """ Populates the tree view """
        for rootDir in self.rootDirectories:
            basePathName = os.path.basename(rootDir)
            p4Path = os.path.join(rootDir, "...fbx")
            for filepath in Perforce.Run("files", ["-e", p4Path.replace("@", "%40")]):
                self.BuildPathItems(rootDir, filepath['depotFile'].split(basePathName)[-1][1:].replace("%40", "@"))
        self.Model.setHeaderData(0, QtCore.Qt.Horizontal, self.windowTitle())

    def BuildPathItems(self, rootDir, path):
        """
        Creates the standard items for the tree view
        Arguments:
            path (string): path to the file
        """
        pathParts = path.split("/")
        parent = self.Files
        for index, eachPart in enumerate(pathParts):
            if not parent.get(eachPart, None):
                filename = eachPart
                fullpath = os.path.join(rootDir, *path.split("/"))
                parent[eachPart] = OrderedDict()
                parent[eachPart].QStandardItem = QtGui.QStandardItem(filename)
                parent[eachPart].QStandardItem.setToolTip(fullpath)
                parent[eachPart].QStandardItem.setData(fullpath, QtCore.Qt.UserRole)
                parent.QStandardItem.appendRow(parent[eachPart].QStandardItem)

                icon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Watchstar",
                                   ["folder.png", "media.png"][eachPart.lower().endswith(".fbx")]))
                parent[eachPart].QStandardItem.setIcon(icon)
            parent = parent[eachPart]

    def textChanged(self, text):
        """
        Overrides built-in method.
        When the text in the text field is greater than two, expand the children of all the
        folders.

        Arguments:
            text (string): text from the text field

        """
        if self._enableAutoFilter:
            self.filterText(text)

    def autoFilter(self):
        """ is the automatic filter option on """
        return self._enableAutoFilter

    def setAutoFilter(self, value):
        """
        Sets the value of the automatic filter option

        Argument:
            value (boolean): value to set for the auto filter option

        """
        self._enableAutoFilter = bool(value)

    def filterText(self, text):
        self.Filter.setFilterFixedString(text)
        if len(text) > 2:
            # First visible item
            self._block = True
            modelIndex = self.Tree.indexAt(self.Tree.rect().topLeft())
            while modelIndex.isValid():
                self.expandChildren(modelIndex)
                modelIndex = self.Tree.indexBelow(modelIndex)

            self._block = False

        else:
            self.Tree.collapseAll()

    def sizeHint(self):
        """ Overrides built-in method. Returns the size hint for this widget """
        return QtCore.QSize(300, 900)

    def selectionChanged(self, selectedItems, _):
        """
        Overrides built-in method.
        When the selection changes store the new selection as the current selection and emit a signal

        Arguments:
            selectedItems (QtCore.QSelectedIndexes): the newly selected items

        """
        try:
            self._currentSelection = selectedItems[0].indexes()[0]
            self.selectionChangedSignal.emit()
        except:
            pass

    def expanded(self, modelIndex):
        """
        When shift is selected, expand all the children in one go

        Arguments:
            modelIndex (QtGui.QModelIndex): the item that was just expanded

        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ShiftModifier and self.Tree.isExpanded(modelIndex) and not self._block:
            self._block = True
            self.expandChildren(modelIndex, 0)
            self._block = False

    def expandChildren(self, modelIndex, depth=0):
        """
        Recursively expands/collapses the children of an item in the tree view

        Arguments:
            modelIndex (QtGui.QModelIndex): model index that corresponds to the item that should have its children
                                            expanded
            depth (int): the depth level of the current recursion
            ignoreValid (boolean): get the children of the model index even if the modelIndex is invalid.
        """

        if not modelIndex.isValid():
            return
        elif modelIndex.data() in self._excludeFilters:
            self.Tree.collapse(modelIndex)
            return

        index = 0
        child = modelIndex.child(0, 0)
        while child.isValid():
            self.expandChildren(child, depth=depth + 1)

            index += 1
            child = modelIndex.child(index, 0)

        if self.Tree.isExpanded(modelIndex) and depth:
            self.Tree.collapse(modelIndex)

        elif not self.Tree.isExpanded(modelIndex):
            self.Tree.expand(modelIndex)

    def addFilter(self, filter):
        """
        Adds word to exclude filter list.
        When a word is found in the exclude list that folder/file isn't shown.

        Arguments:
            filter (string): word to add to the exclude filter list

        """
        if filter not in self._excludeFilters:
            self._excludeFilters.append(filter)

    def removeFilter(self, filter):
        """
        Removes word from the exclude filter list.
        When a word is found in the exclude list that folder/file isn't shown.

        Arguments:
            filter (string): word to remove from the exclude filter list

        """
        if filter in self._excludeFilters:
            self._excludeFilters.remove(filter)

    @property
    def selectedItem(self):
        """ Returns the last item that was selected """
        if self._currentSelection is not None:
            return self._currentSelection
