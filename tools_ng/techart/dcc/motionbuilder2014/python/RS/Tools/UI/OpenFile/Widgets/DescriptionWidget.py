"""
Description of the FBX File based on perforce information
"""
import os

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.UI import Run
from RS.Tools.UI.OpenFile.Widgets import CollapsableWidget
from RS.Tools import CapturePlayer

from RS.Tools.UI.Media.Widgets import Player


class Description(QtGui.QWidget):
    """
    Shows information and latest capture of the file selected
    """
    __defaultImage = os.path.join(Config.Script.Path.ToolImages, "VideoPlayer", "nopreview.png")
    __defaultVideo = os.path.join(Config.Script.Path.ToolImages, "VideoPlayer", "error.mp4")
    _lockedImage = os.path.join(Config.Script.Path.ToolImages, "AssetSetup", "locked.png")
    _unlockedImage = os.path.join(Config.Script.Path.ToolImages, "AssetSetup", "unlocked.png")

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent(QtGui.QWidget): parent widget

        """
        super(Description, self).__init__(parent=parent)

        if not CapturePlayer.DATABASE.IsConnected:
            CapturePlayer.DATABASE.Connect(force=True)

        self.__videoPaths = {os.path.splitext(os.path.basename(path))[0]: path for path, project in
                             CapturePlayer.GetAllVideoPaths()}
        self.__path = ""

        self.pathBox = QtGui.QLineEdit()
        self.revisionBox = QtGui.QLineEdit()
        self.changelistBox = QtGui.QLineEdit()
        self.dateBox = QtGui.QLineEdit()
        self.timeBox = QtGui.QLCDNumber()
        self.amPmBox = QtGui.QLabel()
        self.lockedImage = QtGui.QLabel()

        frame = QtGui.QFrame()
        self.collapsableWidget = CollapsableWidget.CollapsableWidget()
        self.collapsableWidget.setToolTip("Show/Hide Video")
        self.previewVideo = Player.VideoPlayer()

        self.previewVideo.setCodecErrorEnabled(False)
        self.previewVideo.setMinimumHeight(100)
        self.changelistTextEdit = QtGui.QTextEdit()
        self.estimatedTimeBox = QtGui.QLCDNumber()

        self.lockedByBox = QtGui.QLineEdit()
        self.authorList = QtGui.QListWidget()
        self.dependenciesList = QtGui.QListWidget()

        self.previewVideo.path = self.__defaultVideo
        self.setLocked()

        self.pathBox.setProperty("Bold", "True")
        self.pathBox.setReadOnly(True)
        self.lockedByBox.setReadOnly(True)
        self.changelistTextEdit.setReadOnly(True)
        self.revisionBox.setReadOnly(True)

        self.timeBox.setSegmentStyle(self.timeBox.Flat)
        self.timeBox.setDigitCount(8)
        self.timeBox.display("00:00:00")
        self.estimatedTimeBox.setDigitCount(8)
        self.estimatedTimeBox.display("00:00:00")
        self.estimatedTimeBox.setSegmentStyle(self.estimatedTimeBox.Flat)

        self.amPmBox.setText("AM")
        self.dateBox.setMaximumWidth(75)
        self.revisionBox.setMaximumWidth(25)
        self.changelistBox.setMaximumWidth(60)

        layout = QtGui.QVBoxLayout()
        toplayout = QtGui.QHBoxLayout()
        frameLayout = QtGui.QVBoxLayout()
        lockLayout = QtGui.QHBoxLayout()
        authorDependenciesLayout = QtGui.QGridLayout()

        self.collapsableWidget.addWidget(self.previewVideo, visible=True)
        self.collapsableWidget.clicked.connect(self.videoVisibilitySwitched)

        toplayout.addWidget(QtGui.QLabel("Revision "))
        toplayout.addWidget(self.revisionBox)
        toplayout.addWidget(QtGui.QLabel(" Changelist "))
        toplayout.addWidget(self.changelistBox)
        toplayout.addWidget(QtGui.QLabel(" Date Submitted "))
        toplayout.addWidget(self.dateBox)
        toplayout.addWidget(QtGui.QLabel(" Time Submitted "))
        toplayout.addWidget(self.timeBox)
        toplayout.addWidget(self.amPmBox)

        frameLayout.addWidget(self.collapsableWidget)
        frameLayout.setSpacing(0)
        frameLayout.setContentsMargins(0, 0, 0, 0)

        lockLayout.addWidget(self.lockedImage)
        lockLayout.addWidget(QtGui.QLabel(" Locked By: "))
        lockLayout.addWidget(self.lockedByBox)
        lockLayout.addWidget(QtGui.QLabel(" Approx. Load Time "))
        lockLayout.addWidget(self.estimatedTimeBox)
        lockLayout.addWidget(QtGui.QLabel(" Min "))

        authorDependenciesLayout.addWidget(QtGui.QLabel("Authors"), 0, 0)
        authorDependenciesLayout.addWidget(self.authorList, 1, 0)
        authorDependenciesLayout.addWidget(QtGui.QLabel("References"), 0, 1)
        authorDependenciesLayout.addWidget(self.dependenciesList, 1, 1)

        layout.addWidget(self.pathBox)
        layout.addWidget(frame)
        layout.addLayout(toplayout)
        layout.addWidget(self.changelistTextEdit)
        layout.addLayout(authorDependenciesLayout)
        layout.addLayout(lockLayout)

        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.setAlignment(QtCore.Qt.AlignCenter)

        toplayout.setContentsMargins(0, 0, 0, 0)
        toplayout.setSpacing(0)

        frame.setLayout(frameLayout)
        self.setLayout(layout)
        self.setStyleSheet('QLabel{'
                           'qproperty-alignment: AlignCenter;'
                           'font: bold 12px'
                           '}'
                           'QLineEdit{'
                           'qproperty-alignment: AlignCenter;'
                           'font: 12px'
                           '}'
                           'QLineEdit[Bold="True"]{'
                           'font: bold 12px'
                           '}'
                           'QPushButton{'
                           'font: bold 20px;'
                           'font-family: Comicsan;'
                           '}'
                           'QToolButton{'
                           'border-radius: 6px;'
                           '}'
                           )
        frame.setStyleSheet(
                           'QFrame{'
                           'border: 1px solid white;'
                           'border-radius: 6px;'
                           'padding: 0px;'
                           '}')

    def videoVisibilitySwitched(self, visible):
        """
        When the video is hidden it stops pauses playback and when it is shown it restarts the playback

        Arguments:
            visible (True): visibility of the collapsable widget

        """
        if visible:
            self.previewVideo.play()
        else:
            self.previewVideo.pause()

    def path(self):
        """ The current path being displayed by the tool """
        return self.__path

    def setPath(self, path):
        """
        Updates the text field that displays the file path and stores the path internally

        Arguments:
            path (string): file path

        """
        self.__path = path
        self.pathBox.setText(path)

    def setImage(self, path):
        """
        Sets an image to be displayed. Currently Not Implemented

        Arguments:
            path (string): path to the image

        """
        self.previewImage.setPixmap(QtGui.QPixmap(path))

    def setVideo(self, path=None, filename=None):
        """
        sets the video to be displayed

        Arguments:
            path (string): path to the video to display
            filename (string): base filename of an fbx that has a capture. The path parameter takes precendences over
                                this parameter
        """

        if not path and filename:
            path = self.__videoPaths.get(os.path.splitext(filename)[0], None)

        self.previewVideo.path = path or self.__defaultVideo
        if self.previewVideo.isVisible():
            self.previewVideo.play()

    def setLocked(self, isLocked=False, owner="", color=None):
        """
        Displays if the file is locked and by whom

        Argument:
            isLocked (boolean): is the file locked
            owner (string): name of the person who currently has this file locked
            color (string): color to display as the background of the text field.

        """
        if not color and isLocked:
            color = "#4d0d0e"

        elif not color:
            color = "green"

        self.lockedByBox.setText(owner)
        self.lockedImage.setPixmap([self._unlockedImage, self._lockedImage][isLocked])
        self.lockedByBox.setStyleSheet("background-color: {};".format(color))

    def setDescription(self, text):
        """
        Updates the description that is displayed
        Arguments:
            text (string): description to display

        """
        self.changelistTextEdit.setText(text)

    def setRevision(self, revision):
        """
        Updates the revision number that is displayed

        Arguments:
            revision (string/int): revision number to display

        """
        self.revisionBox.setText(str(revision))

    def setChangelist(self, changelist):
        """
        Updates the changelist that is displayed

        Arguments:
            changelist (string/int): changelist number to display

        """
        self.changelistBox.setText(str(changelist))

    def setDateTime(self, datetime):
        """
        Updates the date, time, and am/pm that is displayed

        Arguments:
            datetime (string): time to display in '00/00/0000 00:00:00 AM' format

        """
        # Safety check to makes sure that we have at least 3 items in the results list
        results = str(datetime).strip().split(" ") + ['', '', '']
        date, time, amPm = results[:3]

        # Set data
        self.dateBox.setText(date)
        self.timeBox.display(time)
        self.amPmBox.setText(amPm)

    def setTimeEstimate(self, estimate):
        """
        Updates the time estimate for opening a file that is diplayed

        Arguments:
            estimate(string): time to display in '00:00:00' format

        """
        self.estimatedTimeBox.display(estimate)

    def setDependencies(self, dependencies):
        """
        Updates the list of references contained in the file

        Arguments:
            dependencies (list[string, etc.]): list of objects referenced in the scene

        """
        self.dependenciesList.clear()
        self.dependenciesList.addItems(dependencies)

    def setUsers(self, users):
        """
        Updates the list of users that is displayed who have worked on this file

        Arguments:
            users(list[string, etc.]): list of users to display

        """
        self.authorList.clear()
        self.authorList.addItems(users)


@Run("Description")
def Run():
    """
    Shows Widget in Motion Builder
    """
    return Description()
