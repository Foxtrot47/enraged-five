"""
Description:
    Displays Icons

"""

from PySide import QtGui
from RS.Tools.UI import Run


class Display(QtGui.QWidget):
    """ Displays the ToolIcons that represent each tool in the R* menu """
    _RowNumber = 5

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        super(Display, self).__init__(parent=parent)
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        self.Scroll = QtGui.QScrollArea()
        self.Frame = QtGui.QWidget()
        self.frameLayout = QtGui.QGridLayout()

        # self.Frame.setFrameStyle(QtGui.QFrame.StyledPanel)
        self.Frame.setLayout(self.frameLayout)

        self.Scroll.setWidget(self.Frame)
        self.Scroll.setWidgetResizable(True)
        layout.addWidget(self.Scroll)

        self.Frame.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.setLayout(layout)

    def addWidget(self, widget):
        """
        adds a widget to the display

        Arguments:
            widget (QtGui.QWidget): widget to add to the display
        """
        count = self.frameLayout.count()
        widget.setFixedSize(54, 54)
        self.frameLayout.addWidget(widget, count / self._RowNumber, count % self._RowNumber)


@Run(title="Feature Picker Display")
def Run():
    """
    Shows the Display as an indepedent window

    Return:
        RS.Tools.UI.FeaturePicker.Widget.DisplayWidget.Display()
    """
    return Display()


