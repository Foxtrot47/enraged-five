import os

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Core.Automation.Server
import RS.Core.Automation
import RS.Config

global fbxFilename
global jobModes

jobModes = [ RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES, 
             RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS, 
             RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS,
             RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS ]

class SubmitJobTool( RS.Tools.UI.Base ):
    def __init__( self, title ):
        RS.Tools.UI.Base.__init__( self, title, size = [ 400, 210 ] )
        

    ## Event Handlers ##
    
    def OnSubmit_Clicked( self, source, event ):
        global fbxFilename
        global jobModes
    
        if fbxFilename.endswith( '.batchxml' ):
            RS.Core.Automation.Server.addJobsFromFile( fbxFilename, modes = jobModes )
            
        else:
            RS.Core.Automation.Server.addJob( fbxFilename, jobModes )
        
        self.Close( True )
        
    def OnUpdateReferences_Changed( self, source, event ):
        global jobModes
        
        if source.State == 0:
            if RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES in jobModes:
                jobModes.remove( RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES )
                
        else:
            if RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES not in jobModes:
                jobModes.append( RS.Core.Automation.JOB_MODE_UPDATE_REFERENCES )
    
    def OnUpdateCameras_Changed( self, source, event ):
        global jobModes
        
        if source.State == 0:
            if RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS in jobModes:
                jobModes.remove( RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS )
                
        else:
            if RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS not in jobModes:
                jobModes.append( RS.Core.Automation.JOB_MODE_UPDATE_CAMERAS )
                
    def OnUpdateESTTracks_Changed( self, source, event ):
        global jobModes
            
        if source.State == 0:
            if RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS in jobModes:
                jobModes.remove( RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS )
                
        else:
            if RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS not in jobModes:
                jobModes.append( RS.Core.Automation.JOB_MODE_UPDATE_EST_TRACKS )    
            
    def OnFixAmbientFaceRigs_Changed( self, source, event ):
        global jobModes
                
        if source.State == 0:
            if RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS in jobModes:
                jobModes.remove( RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS )
                
        else:
            if RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS not in jobModes:
                jobModes.append( RS.Core.Automation.JOB_MODE_FIX_AMBIENT_FACE_RIGS )     
            
    def OnCancel_Clicked( self, source, event ):
        self.Close( True )
    
    def Create( self, mainLayout ):
        lyt = FBVBoxLayout()
            
        x = FBAddRegionParam( 6, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( self.BannerHeight, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -6, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( -6, FBAttachType.kFBAttachBottom, '' )
        mainLayout.AddRegion( 'main', 'main', x, y, w, h )
        mainLayout.SetControl( 'main', lyt )
    
        
        # Update References
        cbUpdateReferences = FBButton()
        cbUpdateReferences.Caption = 'Update References'
        cbUpdateReferences.Style = FBButtonStyle.kFBCheckbox
        cbUpdateReferences.State = True
        cbUpdateReferences.OnClick.Add( self.OnUpdateReferences_Changed )
        
        # Update Cameras
        cbUpdateCameras = FBButton()
        cbUpdateCameras.Caption = 'Update Cameras'
        cbUpdateCameras.Style = FBButtonStyle.kFBCheckbox
        cbUpdateCameras.State = True
        cbUpdateCameras.OnClick.Add( self.OnUpdateCameras_Changed )
        
        # Update EST Tracks
        cbUpdateESTTracks = FBButton()
        cbUpdateESTTracks.Caption = 'Update EST Tracks'
        cbUpdateESTTracks.Style = FBButtonStyle.kFBCheckbox
        cbUpdateESTTracks.State = True
        cbUpdateESTTracks.OnClick.Add( self.OnUpdateESTTracks_Changed )
        
        # Fix ambient face rigs
        cbFixAmbientFaceRigs = FBButton()
        cbFixAmbientFaceRigs.Caption = 'Fix Ambient Face Rigs'
        cbFixAmbientFaceRigs.Style = FBButtonStyle.kFBCheckbox
        cbFixAmbientFaceRigs.State = True
        cbFixAmbientFaceRigs.OnClick.Add( self.OnFixAmbientFaceRigs_Changed )     
        
        
        # Submit / Cancel
        lytSubmitCancel = FBHBoxLayout( FBAttachType.kFBAttachRight )
            
        btnSubmitJob = FBButton()
        btnSubmitJob.Caption = 'Submit'
        btnSubmitJob.OnClick.Add( self.OnSubmit_Clicked )
        
        btnCancel = FBButton()
        btnCancel.Caption = 'Cancel'
        btnCancel.OnClick.Add( self.OnCancel_Clicked )
        
        lytSubmitCancel.Add( btnCancel, 70 )
        lytSubmitCancel.Add( btnSubmitJob, 70 )
        
        lyt.Add( cbUpdateReferences, 24 )
        lyt.Add( cbUpdateCameras, 24 )
        lyt.Add( cbUpdateESTTracks, 24 )
        lyt.Add( cbFixAmbientFaceRigs, 24 )
        
        lyt.Add( lytSubmitCancel, 24 )

def Run():   
    global fbxFilename
    
    if RS.Config.User.Studio == None:
        FBMessageBox( 'Rockstar', 'This system is only supported for internal Rockstar studios!', 'OK' )
        
    else:
        openDialog = FBFilePopup()
        openDialog.Caption = "Choose a FBX or BatchXML File to Submit"
        openDialog.Style = FBFilePopupStyle.kFBFilePopupOpen
        openDialog.Filter = "*.fbx;*.batchxml" 
        
        openResult = openDialog.Execute()
        
        if openResult:
            fbxFilename = '{0}\\{1}'.format( openDialog.Path, openDialog.FileName )
            
            tool = SubmitJobTool( 'Cutscene Update Job Options - {0}'.format( os.path.basename( fbxFilename ) ) )
            tool.Show()