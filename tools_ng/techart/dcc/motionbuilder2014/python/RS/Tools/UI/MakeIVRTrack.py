"""
Description: 
    UI to generate IVR Tracks
     
Author: 
    David Vega <david.vega@rockstargames.com>
"""


import os

import pyfbsdk as mobu 

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools import UI, MakeIVRTrack, WorldPosition

WARN = True
PROJECT = Config.Project.Path.Root
DEVELOPMENT = False


class IvrTrackTool(QtGui.QWidget):
    """ UI for generating and moving IVR Tracks """

    def __init__(self, parent=None, f=0):
        """
        Constructor

        Arguments:
            parent: QWidget();
        """
        super(IvrTrackTool, self).__init__(parent=parent, f=f)
        layout = QtGui.QVBoxLayout()
        label = QtGui.QLabel()
        label.setText("\n      TO PLACE THE IVR CORRECTLY IN THE SCENE\n   OFFSET USING THE INGAME WORLD COORDINATES")

        self.importButton = QtGui.QPushButton()
        self.importButton.setText("Import IVR")
        self.importButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        self.coordinatesLabel = QtGui.QLabel()
        self.coordinatesLabel.setText("Coordinates")
        self.coordinatesLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.coordinatesLabel.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.offsetLayout = QtGui.QHBoxLayout()

        for each in "XYZ":
            label = QtGui.QLabel()
            label.setText("   {}:   ".format(each))
            label.setAlignment(QtCore.Qt.AlignCenter)
            label.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
            setattr(self, "{}Box".format(each), QtGui.QDoubleSpinBox())
            getattr(self, "{}Box".format(each)).setRange(-10000, 10000)
            getattr(self, "{}Box".format(each)).setButtonSymbols(QtGui.QAbstractSpinBox.NoButtons)
            getattr(self, "{}Box".format(each)).setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
            self.offsetLayout.addWidget(label)
            self.offsetLayout.addWidget(getattr(self, "{}Box".format(each)))

        self.rotateLabel = QtGui.QLabel("RAG Z Rotation")
        self.rotateLabel.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.rotateBox = QtGui.QDoubleSpinBox()
        self.rotateBox.setRange(-1000, 1000)

        self.moveButton = QtGui.QPushButton()
        self.moveButton.setText("Move to Coordinates")
        self.moveButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        layout.addWidget(self.coordinatesLabel)
        layout.addLayout(self.offsetLayout)
        layout.addWidget(self.rotateLabel)
        layout.addWidget(self.rotateBox)
        layout.addWidget(self.importButton)
        layout.addWidget(self.moveButton)

        self.nullA = None

        self.UpdateXYZCoordinates()

        self.importButton.pressed.connect(self.ImportIVR)
        self.moveButton.pressed.connect(self.GetOffsetData)
        self.rotateBox.valueChanged.connect(self.GetRotationValue)

        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.setStyleSheet("""
            QLabel{
                color: white;
                qproperty-alignment: AlignCenter;
                }

            QLabel[text="Coordinates"]{
                        font: bold 12px;
            }

            QLabel[text="RAG Z Rotation"]{
                        border: 1px solid #0c457e;
                        margin: -2px 0;
                        border-radius: 3px;;
            }

            """)
     
    def ImportIVR(self):
        """ Imports the IVR Track from the ivr file selected by the user """
        filePopup = mobu.FBFilePopup();
        filePopup.Filter = '*.ivr'
        filePopup.Style = mobu.FBFilePopupStyle.kFBFilePopupOpen

        path = os.path.join(PROJECT, r"\assets\recordings\car")
        if "GTA" in Config.Project.Name:
            path = os.path.join(PROJECT, r"\assets_ng\recordings\car")

        filePopup.Path = path

        filePath = None
        if filePopup.Execute():
            filePath = filePopup.FullFilename

        if not filePath:
            return False

        IVRData = MakeIVRTrack.IVRFile(filePath)
        if IVRData.ValidData:
            self.nullA = IVRData.GenerateComponents()
            self.GetOffsetData()
            self.GetRotationValue()

    def GetOffsetData(self):
        """ Applies the translation offset data from the UI to the IVR Track """
        lX = float(self.XBox.value()) * -100.00
        lZ = float(self.YBox.value()) * 100
        lY = float(self.ZBox.value()) * -100.00

        MakeIVRTrack.IVRFile.SceneAdjustment(self.nullA, lX, lY, lZ, "Translation")
        
    def GetRotationValue(self):
        """ Applies the rotate z offset data from the UI to the IVR Track """
        rotY = float(self.rotateBox.value())
        if DEVELOPMENT:
            UI.QPrint("Z-Axis Rot {}".format(rotY))

        if self.nullA:
            MakeIVRTrack.IVRFile.SceneAdjustment(self.nullA.Parent, 0, rotY, 0, "Rotation")

    def UpdateXYZCoordinates(self):
        """ Updates the UI Offset information with the data from the database"""
        SceneInfo = WorldPosition.GetWorldPositionFromDatabase(warning=WARN)
        if SceneInfo:
            SceneCoordinates = SceneInfo.values()[0]

            self.XBox.setValue(float(SceneCoordinates.get("Trans_X", 0)))
            self.YBox.setValue(float(SceneCoordinates.get("Trans_Y", 0)))
            self.ZBox.setValue(float(SceneCoordinates.get("Trans_Z", 0)))
            self.rotateBox.setValue(float(SceneCoordinates.get("Rotation_X", 0)))


@UI.Run(title="Make IVR Track", size=[250, 160], url="https://devstar.rockstargames.com/wiki/index.php/Importing_Vehicle_Recordings")
def Run():
    return IvrTrackTool()