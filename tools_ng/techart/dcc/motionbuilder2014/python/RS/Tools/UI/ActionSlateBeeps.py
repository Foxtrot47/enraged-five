from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Core.Mocap.Audio

import RS.Core.Mocap.MocapCommands as mocapCommand
reload(mocapCommand)

global gTCArray
gTCArray = []

class ActionSlateBeepsTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Rockstar - Find Action Slate Beeps', size = [ 400, 400 ] )
     
    def Create( self, mainLayout ):
        ctrlLyt = FBVBoxLayout()
            
        x = FBAddRegionParam( 10, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( self.BannerHeight, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( -10, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( -5, FBAttachType.kFBAttachBottom, '' )
        
        mainLayout.AddRegion( 'main', 'main', x, y, w, h )
        mainLayout.SetControl( 'main', ctrlLyt )
        
        # Lower Decibel Level
        lowerDecibelLevelLyt = FBHBoxLayout()
        
        lowerDecibelLevelLbl = FBLabel()
        lowerDecibelLevelLbl.Caption = 'Lower Level (Db):'
        
        self.lowerDecibelLevelCtrl = FBEditNumber()
        self.lowerDecibelLevelCtrl.Precision = 1
        self.lowerDecibelLevelCtrl.Value = 10
        
        lowerDecibelLevelLyt.Add( lowerDecibelLevelLbl, 86 )
        lowerDecibelLevelLyt.Add( self.lowerDecibelLevelCtrl, 40, space = 4 )
        
        # Upper Decibel Level
        upperDecibelLevelLyt = FBHBoxLayout()
        
        upperDecibelLevelLbl = FBLabel()
        upperDecibelLevelLbl.Caption = 'Upper Level (Db):'
        
        self.upperDecibelLevelCtrl = FBEditNumber()
        self.upperDecibelLevelCtrl.Precision = 1
        self.upperDecibelLevelCtrl.Value = 70
        
        upperDecibelLevelLyt.Add( upperDecibelLevelLbl, 86 )
        upperDecibelLevelLyt.Add( self.upperDecibelLevelCtrl, 40, space = 4 )
        
        # Upper Decibel Level
        maxBeepDurationLyt = FBHBoxLayout()
        
        maxBeepDurationLbl = FBLabel()
        maxBeepDurationLbl.Caption = 'Maximum Beep Duration (seconds):'
        
        self.maxBeepDurationCtrl = FBEditNumber()
        self.maxBeepDurationCtrl.Precision = 0.1
        self.maxBeepDurationCtrl.Value = 0.3
        
        maxBeepDurationLyt.Add( maxBeepDurationLbl, 170 )
        maxBeepDurationLyt.Add( self.maxBeepDurationCtrl, 40, space = 4 )        
        
        # Choose Audio File
        chooseAudioBtn = FBButton()
        chooseAudioBtn.Caption = 'Choose Audio File...'
        chooseAudioBtn.OnClick.Add( self.OnChooseAudioFile_Clicked )
        
        # Enter Frame        
        self.lTimeFrame = FBEditNumber()   
        self.lTimeFrame.Hint = "Don't add a negative to the Frame Value."

        # Zero Out Button
        lZeroOutButton = FBButton()  
        lZeroOutButton.Hint = "Zero Out to First Selected TimeCode"
        lZeroOutButton.Caption = "Zero Out to First Selected TimeCode"
        lZeroOutButton.TC = gTCArray
        lZeroOutButton.FrameValue = self.lTimeFrame
        lZeroOutButton.OnClick.Add( self.ZeroOutScene_Clicked )
        
        # Timecodes
        self.lTimeCodeList = FBList()
        self.lTimeCodeList.Style = FBListStyle.kFBVerticalList
        self.lTimeCodeList.Selected(0, False)
        self.lTimeCodeList.TC = lZeroOutButton
        self.lTimeCodeList.MultiSelect = True  
        self.lTimeCodeList.OnChange.Add(self.ListCallback) 
        
        # Label        
        lTimeFrameLabel = FBLabel()
        lTimeFrameLabel.Caption = "Enter Frame for Zero Out:"
        lTimeFrameLabel.Hint = "Don't add a negative to the Frame Value."
    
        
        ctrlLyt.Add( lowerDecibelLevelLyt, 24 )
        ctrlLyt.Add( upperDecibelLevelLyt, 24 )
        ctrlLyt.Add( maxBeepDurationLyt, 24 )
        ctrlLyt.Add( chooseAudioBtn, 32 )
        ctrlLyt.AddRelative( self.lTimeCodeList, 1 )
        ctrlLyt.Add( lTimeFrameLabel, 32 )  
        ctrlLyt.Add( self.lTimeFrame, 32 )  
        ctrlLyt.Add( lZeroOutButton, 32 )        
        
    def ListCallback( self, control, event ):
        del gTCArray[:]        
        for i in range(len(control.Items)):
            if control.IsSelected(i):
                gTCArray.append(control.Items[i])
        
        control.TC = gTCArray
        
    def ZeroOutScene_Clicked(self, source, event):
        mocapCommand.ZeroOutScene( self.lTimeFrame.Value )

    def OnChooseAudioFile_Clicked( self, source, event ):
        dlg = FBFilePopup()
        dlg.Caption = 'Select a .wav file'
        dlg.Style = FBFilePopupStyle.kFBFilePopupOpen
        dlg.Filter = '*.wav'
        
        result = dlg.Execute()
        
        if result:
            audioFilename = '{0}\\{1}'.format( dlg.Path, dlg.FileName )
        
            lowerDb = self.lowerDecibelLevelCtrl.Value
            upperDb = self.upperDecibelLevelCtrl.Value
            maxBeepDuration = self.maxBeepDurationCtrl.Value
            
            timeCodes = RS.Core.Mocap.Audio.FindActionSlateBeeps( audioFilename, upperDecibelLevel = upperDb, lowerDecibelLevel = lowerDb, maxBeepDuration = maxBeepDuration )                        
            
            for timeCode in timeCodes:
                self.lTimeCodeList.Items.append(timeCode)   
                     
        
def Run( show = True ):
    tool = ActionSlateBeepsTool()
    
    if show:
        tool.Show()
        
    return tool