'''
OutfitSelection.py

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
Description: Provides the ability to switch between a set of outfits/accessories for a character.
'''

import os
from PySide import QtCore, QtGui
from pyfbsdk import *

import RS.Tools.UI

from RS import Globals

from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character     

class OutfitSelectionTool( RS.Tools.UI.QtMainWindowBase ):
    def __init__( self ):
        self.manager = Manager()         

        RS.Tools.UI.QtMainWindowBase.__init__( self, title = 'Outfit Selection', size = [ 250, 350 ] )
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)

        self.__centralWidget = QtGui.QWidget()
        self.__centralWidget.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)       
        mainLayout = QtGui.QVBoxLayout()
        self.__centralWidget.setLayout( mainLayout )
        
        self.__centralWidget.__charactersCtrl = QtGui.QComboBox( self.__centralWidget)
        self.__centralWidget.__charactersCtrl.currentIndexChanged.connect( self.OnCharacterSelected )
        
        self.__centralWidget.__outfitsLabel = QtGui.QLabel( "Outfits", self.__centralWidget )
        self.__centralWidget.__outfitsCtrl = QtGui.QListWidget( self.__centralWidget )
        self.__centralWidget.__outfitsCtrl.itemClicked.connect( self.OnOutfitSelected )

        self.__centralWidget.__accessoriesLabel = QtGui.QLabel( "Accessories", self.__centralWidget)        
        self.__centralWidget.__accessoriesCtrl = QtGui.QListWidget( self.__centralWidget )
        self.__centralWidget.__accessoriesCtrl.itemChanged.connect( self.OnAccessoryChanged )
                
        mainLayout.addWidget( self.__centralWidget.__charactersCtrl )
        mainLayout.addWidget( self.__centralWidget.__outfitsLabel )
        mainLayout.addWidget( self.__centralWidget.__outfitsCtrl )
        mainLayout.addWidget( self.__centralWidget.__accessoriesLabel )
        mainLayout.addWidget( self.__centralWidget.__accessoriesCtrl )             
        
        #Callbacks on the tool- should be destroyed after a File New or File Open
        Globals.Application.OnFileOpenCompleted.Add( self.checkSceneCallback )
        Globals.Application.OnFileNewCompleted.Add( self.checkSceneCallback ) 
                
        self._refreshingAccessoryList = False
        
        self.RefreshCharactersList()    
        
        self.setCentralWidget( self.__centralWidget )
        
    ## Methods ##
        
    def RefreshCharactersList( self ):
        self.__centralWidget.__charactersCtrl.clear()
        
        if (self.manager.GetReferenceListByType( Character ))> 0:
            for character in self.manager.GetReferenceListByType( Character ):
                character.ReloadMetaFile()
                self.__centralWidget.__charactersCtrl.addItem( str(character.Namespace) )
        else:
            QtGui.QMessageBox.warning( self, "Outfit Selection", "There are no references in the scene! You must have at least one character reference available." )
                 
    def RefreshOutfitsList( self ):
        self.__centralWidget.__accessoriesCtrl.clear()        
        self.__centralWidget.__outfitsCtrl.clear()
        
        character = self.GetCurrentCharacter()        
        
        if character != None:  
            currentOutfit = character.GetCurrentOutfit()
            for outfit in character.GetOutfitNameList():
                item = QtGui.QListWidgetItem( outfit, self.__centralWidget.__outfitsCtrl )
                item.setData( QtCore.Qt.UserRole, outfit )
                if  currentOutfit == str (outfit).lower():
                    self.__centralWidget.__outfitsCtrl.setCurrentItem( item )
                    self.RefreshAccessoriesList()  
  
    def RefreshAccessoriesList( self ):                  
        character = self.GetCurrentCharacter()
        
        self._refreshingAccessoryList = True        
        for accessory in character.GetAccessoryNameList() : 
            item = QtGui.QListWidgetItem( accessory, self.__centralWidget.__accessoriesCtrl )
            
            if character.GetAccessoryState( accessory ):
                item.setData( QtCore.Qt.UserRole, accessory ) 
                item.setCheckState( QtCore.Qt.CheckState.Checked ) 
            else:
                item.setData( QtCore.Qt.UserRole, accessory ) 
                item.setCheckState( QtCore.Qt.CheckState.Unchecked )
        self._refreshingAccessoryList = False        
        
    #Helper functions    
    def GetCurrentOutfit( self ):
        item = self.__centralWidget.__outfitsCtrl.currentItem()
                      
        if item:
            return self.__centralWidget.__outfitsCtrl.currentItem().data( QtCore.Qt.UserRole )
        
    
    def GetCurrentOutfitName( self ):
        character = self.GetCurrentCharacter()
                      
        if character:
            return str( character.GetCurrentOutfit( ))
        return None
    
    def GetCurrentCharacter( self ):
        item = self.__centralWidget.__charactersCtrl.currentText()
        
        if (item):
            for character in self.manager.GetReferenceListByType( Character ):
                if str(item) == str(character.Namespace):            
                    return character
        return None
    
    def RemoveCallbacks (self):
        Globals.Application.OnFileOpenCompleted.Remove( self.checkSceneCallback )
        Globals.Application.OnFileNewCompleted.Remove( self.checkSceneCallback )  
        
    #callbacks    
    def checkSceneCallback( self, source, event ):          
        print "Closing Outfit tool"  
        self.RemoveCallbacks()   
        self.close() 
        
    ## Event Handlers ##
    
    def closeEvent(self, event):
        print "Closing Outfit tool" 
        #Need to remove the callbacks on a normal close as well
        self.RemoveCallbacks()     
        self.close()
        
    def OnAccessoryChanged( self, item ):
        if not self._refreshingAccessoryList:
            obj = item.data( QtCore.Qt.UserRole )
            
            character = self.GetCurrentCharacter()
            if character != None:            
                if obj:
                    if item.checkState() == QtCore.Qt.CheckState.Checked:
                        character.SetAccessoryState( item.text(), True)                      
                    else:
                        character.SetAccessoryState( item.text(), False)  
    
    def OnCharacterSelected( self, index ):
        self.RefreshOutfitsList()
        
    def OnOutfitSelected( self, item ):
        character = self.GetCurrentCharacter()
        if character != None:
            character.SetCurrentOutfit( str(item.text()) )
            self.RefreshOutfitsList()    
        
def Run():
    tool = OutfitSelectionTool()
    tool.show()
    