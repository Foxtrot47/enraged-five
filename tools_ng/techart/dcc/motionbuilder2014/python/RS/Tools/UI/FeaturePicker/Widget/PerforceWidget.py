"""
Contains information retrieved from perforce based on a given file
"""
from PySide import QtGui, QtCore

from RS.Tools.UI import Run


class Perforce(QtGui.QFrame):

    def __init__(self, parent=None):
        super(Perforce, self).__init__(parent=parent)
        label = QtGui.QLabel("Path :")
        self.labelText = QtGui.QLineEdit()

        self.changelistDescription = QtGui.QTextEdit()
        self.changelistDescription.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.changelistDescription.setMinimumSize(QtCore.QSize(0, 0))

        changelist = QtGui.QLabel("Changelist : ")
        self.changelistText = QtGui.QLineEdit()
        date = QtGui.QLabel(" Date Submitted : ")
        self.dateText = QtGui.QLineEdit()

        self.contributorsList = QtGui.QListView()
        self.dependenciesList = QtGui.QListView()
        self.contributorsModel = QtGui.QStandardItemModel()
        self.dependenciesModel = QtGui.QStandardItemModel()

        self.contributorsList.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.dependenciesList.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        layout = QtGui.QVBoxLayout()
        gridLayout = QtGui.QGridLayout()

        gridLayout.addWidget(label, 0, 0)
        gridLayout.addWidget(self.labelText, 0, 1)

        anotherGridLayout = QtGui.QGridLayout()
        anotherGridLayout.addWidget(changelist, 0, 0)
        anotherGridLayout.addWidget(self.changelistText, 0, 1)
        anotherGridLayout.addWidget(date, 0, 2)
        anotherGridLayout.addWidget(self.dateText, 0, 3)


        listGridLayout = QtGui.QGridLayout()
        listGridLayout.addWidget(QtGui.QLabel("Contributors"), 0, 0)
        listGridLayout.addWidget(QtGui.QLabel("Dependencies"), 0, 1)
        listGridLayout.addWidget(self.contributorsList, 1, 0)
        listGridLayout.addWidget(self.dependenciesList, 1, 1)

        layout.addLayout(gridLayout)
        layout.addLayout(anotherGridLayout)
        layout.addWidget(self.changelistDescription)
        layout.addLayout(listGridLayout)

        layout.setSpacing(1)
        self.setFrameShape(self.StyledPanel)
        # self.setFrameStyle(QtGui.QFrame.Panel)
        self.setLineWidth(1)
        self.setLayout(layout)
        self.setMinimumSize(QtCore.QSize(75, 150))
        self.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        self.contributorsList.setModel(self.contributorsModel)
        self.dependenciesList.setModel(self.dependenciesModel)
        for _, widget in self.__dict__.iteritems():
            if isinstance(widget, (QtGui.QLineEdit, QtGui.QTextEdit)):
                widget.setReadOnly(True)

    def setPath(self, path):
        self.labelText.setText(path)

    def setChangelist(self, changelist):
        self.changelistText.setText(str(changelist))

    def setChangelistDescription(self, text):
        self.changelistDescription.setText(text)

    def setDate(self, date):
        self.dateText.setText(date)

    def setContributors(self, *contributors):
        self.contributorsModel.clear()
        for contributor in contributors:
            self.contributorsModel.appendRow(QtGui.QStandardItem(contributor))

    def setDependencies(self, *dependencies):
        self.dependenciesModel.clear()
        for dependency in dependencies:
            self.dependenciesModel.appendRow(QtGui.QStandardItem(dependency))
            
@Run("Perforce")
def Run():
    return Perforce()