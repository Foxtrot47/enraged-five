from PySide import QtCore, QtGui
#import RS.Config
import icons_rc

class watchstarListNode(object):

    def __init__(self, 
                 name = None,
                 parent =None):

        self._name = name
        self._children = []
        self._parent = parent

        #self._path = nodePath
        self._version = ""

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "NodeType"

    def addChild(self, child):
        self._children.append(child)

    # getters	
    def name(self):
        return self._name
    
    def verDisplay(self):
        return ''
    
    def depotPath(self):
        return ''

    def child(self, row):
        return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def children(self):
        return self._children

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)
        
    def mediaItem(self):
        return


class wsFbxNode(watchstarListNode):

    def __init__(self, name, parent = None):
        super(wsFbxNode, self).__init__(name, parent)

    def typeInfo(self):
        return "FBX"

class wsFolderNode(watchstarListNode):

    def __init__(self, name, parent = None):
        super(wsFolderNode, self).__init__(name, parent)     
        

    def typeInfo(self):
        return "Folder"

class wsFileNode(watchstarListNode):

    def __init__(self, parent = None, item = None):
        super(wsFileNode, self).__init__(item.Name, parent)
        self._item = item

    def typeInfo(self):
        return "Asset"
    
    def haveRevision(self):
        return self._item.HaveRevision
    
    def headRevision(self):
        return self._item.HeadRevision
    
    def verDisplay(self):
        return '{0} of {1}'.format(self.haveRevision(), self.headRevision())   
    
    def depotPath(self):
        return self._item.DepotFilename
    
    def mediaItem(self):
        return self._item
    

class WatchstarModel(QtCore.QAbstractItemModel):
    """ Inputs: Node, QObject"""
    def __init__(self, root, parent = None):
        super(WatchstarModel, self).__init__(parent)

        ### private attributes ###
        self._rootNode = root

    userRole = QtCore.Qt.UserRole + 4	
    typeRole = QtCore.Qt.UserRole + 1
    childCountRole = QtCore.Qt.UserRole + 2
    childrenRole = QtCore.Qt.UserRole + 3
    nodeRole = QtCore.Qt.UserRole 
    


    ### View needs to know how many items this model contains ###
    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def rowCount(self, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent):	
        return 3

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "FBX Scenes"

                elif section == 1:
                    return "Version"

                else:
                    return "Filename"


    """ Inputs: QModelIndex"""
    """ Outputs: int (flag)"""	
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    """ Inputs: QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        node = self.getNode(index)
        parentNode = node.parent()

        if parentNode == self._rootNode:
            #root node, create an empty QModelIndex
            return QtCore.QModelIndex()

        # wrap in a QModelIndex
        return self.createIndex(parentNode.row(), 0, parentNode)

    """ Inputs: int, int, QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column, and parent node"""	
    def index(self, row, column, parent):
        # is the parent valid (when QmodelIndex is empty)
        parentNode = self.getNode(parent)

        childItem = parentNode.child(row)

        #if there are no children
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()	

    """ Inputs: QModelIndex, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if index.column() == 0:
                #if node.mediaItem() and node.haveRevision() != node.headRevision():
                    
                    #name = node.name().setForeground(QtGui.QBrush("red"))
                return node.name()
            elif index.column() == 1:
                if node.mediaItem() and node.haveRevision() != node.headRevision():
                    pass                
                return node.verDisplay()
            elif index.column() == 2:
                if node.mediaItem() and node.haveRevision() != node.headRevision():
                    pass                
                return node.depotPath()
            #else:
                #return node.path()			

        if role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                typeInfo = node.typeInfo()

                if typeInfo == "FBX":
                    return QtGui.QIcon(QtGui.QPixmap(":/FBX.png"))  ## replace with pretty purple film icon
                
                elif typeInfo == "Folder":
                    return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
                else:
                    return QtGui.QIcon(QtGui.QPixmap(":/media.png"))
                
        if role == QtCore.Qt.ForegroundRole:
            if node.mediaItem() and node.haveRevision() != node.headRevision():            
                return QtGui.QBrush(QtGui.QColor(255, 170, 0))
            return QtGui.QBrush(QtGui.QColor(255, 255, 255))

        if role == self.typeRole:
            return node.typeInfo()
        
        elif role == self.userRole:            
            return node.mediaItem()
        
        elif role == self.childCountRole:            
            return node.childCount()
        
        elif role == self.childrenRole:            
            return node.children()
        
        elif role == self.nodeRole:
            return node


    """ Inputs: QModelIndex"""

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._rootNode






