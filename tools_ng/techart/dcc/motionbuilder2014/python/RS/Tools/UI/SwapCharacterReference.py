from pyfbsdk import *
from pythonidelib import * 
from PySide import QtCore, QtGui
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types.Character import Character

import os
import RS.Tools.UI
import RS.Core.Face.Lib
import RS.Core.Face.Toolbox
import RS.Core.Face.FixFace
import RS.Core.Face.FixJawNull
#import RS.Core.Reference.RefNullEdit 


class MainWidget( QtGui.QWidget ):
    def __init__( self, *args, **kwargs ):
        QtGui.QWidget.__init__( self, *args, **kwargs )

        # Setting main layout to vertical box layout
        mainLayout = QtGui.QVBoxLayout( )
        self.setLayout( mainLayout )
        self.setAutoFillBackground( True )

        # Node to search for in the scene to find cutscene characters
        self.idControl = "FACIAL_*acialRoot"
        self.idControl_Ambient = "Ambient_UI"

        # GUI Element Shared Properties
        lButtonLook = FBButtonLook.kFBLookNormal

        # List creation
        self.ctrlsList = QtGui.QListWidget( )
        self.ctrlsList.MultiSelect = False
        self.ctrlsList.setFixedHeight( 60 )
        mainLayout.addWidget( self.ctrlsList, 100 )

        # Fill the list with data
        manager = Manager( )

        characters = manager.GetReferenceListByType( Character )

        for character in characters:
            self.ctrlsList.addItem( character.Namespace )


        # Button **************************************************************************
        # Button -- Swap Character Reference
        self.but_swapReference = QtGui.QPushButton( "Swap Character Reference", parent=self )

        self.but_swapReference.setMaximumSize( 150,200 )
        self.but_swapReference.setToolTip( "Selects the facial gui for facial tagging ")
        self.but_swapReference.Look = lButtonLook
        self.but_swapReference.Enabled = False


        # Layout **************************************************************************
        # Create Horizontal and Vertical Box layouts
        horizontalBox = QtGui.QHBoxLayout()
        horizontalBox.addWidget(self.but_swapReference)

        verticalBox = QtGui.QVBoxLayout()
        verticalBox.addLayout(horizontalBox)

        mainLayout.addLayout(verticalBox)


        # Bind event handler ***************************************************************
        self.but_swapReference.clicked.connect( self.SwapCharacter_Clicked )


    # Function ******************************************************************************      

    # On Click - Swap Character
    def SwapCharacter_Clicked(self):
        """
        Swaps character reference.
        """
        oldCharRef_index = self.ctrlsList.currentRow()
        oldCharRef_name = str( self.ctrlsList.item(oldCharRef_index).text())

        if oldCharRef_name:

            caption = 'Select character reference file'
            dirPath = "X:\\rdr3\\art\\animation\\resources\\characters\\Models\\cutscene"

            popup = QtGui.QFileDialog()
            popup.setDirectory(dirPath)
            popup.setWindowTitle(caption)
            popup.setNameFilters(["*.fbx", "*.FBX"])

            if popup.exec_():
                filePath = popup.selectedFiles()[0]
                try:
                    str(filePath)
                except UnicodeEncodeError:
                    QtGui.QMessageBox.warning(None,
                                              "File Name Error",
                                              "This filename uses invalid characters.\n\nPlease rename the file and try again.")
                    return
                filePath = str(os.path.normpath(filePath))
                if not os.path.isfile(filePath):
                    QtGui.QMessageBox.warning(None,
                                              "File Name Error",
                                              "This file does not exist on disk.\n\n Please check your file path is correct.")
                    return                
                # initiate manager
                manager = Manager()
                # get reference for old character
                sourceReference = manager.GetReferenceByNamespace(oldCharRef_name)
                # file path
                filePath = popup.selectedFiles()[0]
                # file info (to get file name)
                fileInfo = QtCore.QFileInfo(filePath)
                # file name
                newCharRef_name = fileInfo.baseName()
                # Swaps sourceReference with targetReference file path
                import cProfile
                cProfile.runctx('manager.SwapReference(sourceReference, filePath)', globals(), locals())
                #manager.SwapReference(sourceReference, filePath)
                # Changes namespace 
                manager.ChangeNamespace(sourceReference, newCharRef_name)


class SwapCharacter(RS.Tools.UI.QtMainWindowBase):
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__(self, title = 'Swap Reference', size = [230, 130])

        self.centralWidget = MainWidget()
        self.setCentralWidget(self.centralWidget)

def Run():
    tool = SwapCharacter()
    tool.show()