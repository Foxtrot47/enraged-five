"""
Description:
    Widget for displaying information related to a module or command

"""
import os
import subprocess
import webbrowser

from PySide import QtGui, QtCore

from RS import Config
from RS.Tools.UI import Run
from RS.Tools.UI.FeaturePicker.Widget import ToolIconWidget, PerforceWidget

ICON_SIZE = (54, 54)


class Description(QtGui.QWidget):
    """
    Widget that displays the information on a module/command retrieved from the menu.config
    """
    _RowNumber = 5

    def __init__(self, parent=None, imagePath=ToolIconWidget.DEFAULT_ICON_PATH):
        """
        Constructor

        Arguments:
            parent (QWidget): parent widget
            imagePath (string): path to image to use

        """
        super(Description, self).__init__(parent=parent)
        self._module = ""
        self._Wiki = r"https://devstar.rockstargames.com/wiki/index.php/Main_Page"
        self._MobuToolsRoot = os.path.dirname(Config.Script.Path.Root)
        layout = QtGui.QVBoxLayout()
        self.setLayout(layout)

        # Icon and Tool Title
        topLayout = QtGui.QHBoxLayout()

        self._Icon = ToolIconWidget.ToolIcon(imagePath=imagePath)
        self._Title = QtGui.QLabel("Title")

        self._Icon.setFixedSize(*ICON_SIZE)
        self._Title.setAlignment(QtCore.Qt.AlignCenter)

        # Hot key Associated with the Tool
        hotkeyLayout = QtGui.QHBoxLayout()
        revision = QtGui.QLabel("Revision :")
        self._Revision = QtGui.QLineEdit()
        hotkey = QtGui.QLabel("Hotkey :")
        self._Hotkey = QtGui.QLineEdit()

        self.Frame = QtGui.QFrame()
        self.frameLayout = QtGui.QVBoxLayout()

        # Tool description

        self.ToolDescription = QtGui.QTextEdit()
        self.ToolDescription.setReadOnly(True)

        self.Frame.setFrameStyle(QtGui.QFrame.StyledPanel)
        self.Frame.setLayout(self.frameLayout)

        horizontalLayout = QtGui.QHBoxLayout()

        documentationButton = QtGui.QPushButton("Documentation")
        wikiButton = QtGui.QPushButton("Wiki Page")
        sourceButton = QtGui.QPushButton("Source Code")

        self.PerforceWidget = PerforceWidget.Perforce()

        horizontalLayout.addWidget(documentationButton)
        horizontalLayout.addWidget(wikiButton)
        horizontalLayout.addWidget(sourceButton)
        horizontalLayout.setContentsMargins(0, 0, 0, 0)
        horizontalLayout.setSpacing(0)

        #commandLabel = QtGui.QLabel("Tool Info")
        #commandLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.CommandDescription = QtGui.QTextEdit()
        self.CommandDescription.setReadOnly(True)

        topLayout.addWidget(self._Icon)
        topLayout.addWidget(self._Title)
        layout.addLayout(topLayout)

        hotkeyLayout.addWidget(revision)
        hotkeyLayout.addWidget(self._Revision)
        hotkeyLayout.addWidget(hotkey)
        hotkeyLayout.addWidget(self._Hotkey)

        layout.addLayout(hotkeyLayout)

        layout.addWidget(self.ToolDescription)
        layout.addLayout(horizontalLayout)
        #layout.addWidget(commandLabel)
        #layout.addWidget(self.CommandDescription)
        layout.addWidget(self.PerforceWidget)

        self._Icon.setDisabled(True)
        self.Frame.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

        documentationButton.clicked.connect(self.openDocumentation)
        wikiButton.clicked.connect(self.openWiki)
        sourceButton.clicked.connect(self.openSource)
        self.setLayout(layout)

        self._Title.setStyleSheet(
            "font: 48px;"
            "font-family: Times New Roman;")

    def addWidget(self, widget):
        """
        Adds a widget to the standalone display

        Arguments:
            widget (QtGui.QWidget): widget to add to the standalone display
        """
        count = self.frameLayout.count()
        widget.setFixedSize(*ICON_SIZE)
        self.frameLayout.addWidget(widget, count / self._RowNumber, count % self._RowNumber)

    def setToolTitle(self, toolTitle):
        """
        Sets the title of the tool whose information is being displayed

        Arguments:
            toolTitle (string): title of the current tool

        """
        fontSize = 48
        self._ToolName = toolTitle
        self._Title.setText(toolTitle)
        if len(toolTitle) >= 5:
            fontSize -= (len(toolTitle) / 2)

        self._Title.setStyleSheet(
            "font: {}px;"
            "font-family: Times New Roman;".format(fontSize))

    def setDescription(self, description):
        """
        Sets the description of the tool whose information is being displayed

        Arguments:
            description (string): doc string content of the module/command the tool is ran with

        """
        self.ToolDescription.setText(description)

    def setIcon(self, path):
        """
        Sets the icon of the tool whose information is being displayed

        Arguments:
            path (string): path to the icon that the tool whose information is being displayed uses

        """
        self._Icon.setIcon(path)

    def setIconText(self, text):
        """
        Sets the text that goes under the icon

        Arguments:
            text (string): text to put under the icon
        """
        self._Icon.setText(text)

    def setWiki(self, path):
        """
        Sets the URL to the wiki page

        Arguments:
            path (string): url to the devstar wiki page for the selected tool
        """
        self._Wiki = path

    def setModule(self, modulePath):
        """
        Sets the module for the selected tool

        Arguments:
            modulePath (string): the full module path , ei. RS.Core.Animation.Lib
        """
        self._module = modulePath

    def setRevision(self, revision):
        self._Revision.setText(str(revision))

    def openWiki(self):
        """
        Opens the wiki for the currently selected tool
        """
        webbrowser.open(self._Wiki)

    def openSource(self):
        """
        Opens the source code for the currently selected tool
        """
        if self._module:
            nameParts = self._module.split(".")
            path = os.path.join(self._MobuToolsRoot, "python", *nameParts)

            if os.path.isdir(path):
                path = os.path.join(path, "__init__.py")
            else:
                path = "{}.py".format(path)

            # TODO: Add option to set a text editor to open the files
            if os.path.exists(path):
                try:
                    subprocess.call("start notepad++ {}".format(path), shell=True)
                except WindowsError, e:
                    subprocess.call("notepad {}".format(path))
            else:
                QtGui.QMessageBox.warning(None, "File Not Found", "Could not find file:\n {}".format(path))

    def openDocumentation(self):
        """
        Opens the documentation for the currently selected tool
        """
        if self._module:
            nameParts = self._module.split(".")
            nameParts[-1] = "{}.html".format(nameParts[-1])

            path = os.path.join(self._MobuToolsRoot, "help", *nameParts[1:])
            if os.path.exists(path):
                webbrowser.open(path)
            else:
                QtGui.QMessageBox.warning(None, "File Not Found", "Could not find file:\n {}".format(path))



@Run(title="Feature Picker Description")
def Run():
    """
    Shows the Feature Picker

    Return:
        RS.Tools.UI.FeaturePicker.Widget.DescriptionWidget.Description()
    """
    return Description()