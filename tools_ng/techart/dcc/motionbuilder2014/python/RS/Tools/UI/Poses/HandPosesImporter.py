from pyfbsdk import *
from PySide import QtCore, QtGui

import RS.Config
import RS.Tools.UI
from pyfbsdk_additions import *

import RS.Utils.UserPreferences as userPref
import RS.Perforce as p4

reload(userPref)

###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gProjRoot = RS.Config.Project.Path.Root
gDevelopment = False

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\Poses\\QT\\handPoseImporter_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\Poses\\handPoseImporter_QT.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.UI.Poses.handPoseImporter_QT as handposes

class HandPoses( RS.Tools.UI.QtMainWindowBase, handposes.Ui_handPoseImporter  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Pose Importer')
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
	self.__historyList = []
	self.__defaultFBX = 'x:\\rdr3\\art\\animation\\resources\\characters\\Poses\\Cutscene\\RDR_Hand_Pose_Library_NewSkeleton.fbx'
	
	# Add the Rockstar Banner
	if gProjRoot == 'x:\\rdr3':
	    self.__banner = RS.Tools.UI.BannerWidget(name = 'Pose Importer', helpUrl ="https://rsgediwiki1/bob/index.php/Editing_and_converting_exported_.anim_files_back_into_FBX")
	    #self.fbxSelectionCombo.addItem(self.__defaultFBX)
	else:
	    self.__banner = RS.Tools.UI.BannerWidget(name = 'Pose Importer')
	    
	self.lytBanner.addWidget(self.__banner)	
	self.readFBXHistory()
	#self.fbxSelectionCombo.addItem('Browse...')
	
	self.cbCloseTool.setChecked(userPref.__Readini__("poseImporter","close tool after import"))
	self.cbSyncFBX.setChecked(userPref.__Readini__("poseImporter","sync to latest"))
	
	self.cbCloseTool.clicked.connect(self.OnCloseToolAfterImport_Checked)
	self.cbSyncFBX.clicked.connect(self.onSyncToLatest_Checked)
	self.fbxSelectionCombo.activated[str].connect(self.browseForFBX)
	self.btnImportPoses.clicked.connect(self.importPoses)
	self.btnClearHistory.clicked.connect(self.clearHistory)
	
    def clearHistory(self):
	self.__historyList = []
	userPref.__RemoveSection__("poseImporterFBXHistory")
	count = self.fbxSelectionCombo.count() - 1
	for i in range(count,-1, -1) :
	    self.fbxSelectionCombo.removeItem(i) 
	    
	if gProjRoot == 'x:\\rdr3':
	    self.fbxSelectionCombo.addItem(self.__defaultFBX)
	    self.__historyList.append(self.__defaultFBX)
	    userPref.__Saveini__("poseImporterFBXHistory", "historycount", 1)
	    userPref.__Saveini__("poseImporterFBXHistory", "path_1", self.__defaultFBX)	
	self.fbxSelectionCombo.addItem('Browse...')
	
	
    def readFBXHistory(self):
	historyCount = userPref.__Readini__("poseImporterFBXHistory","historycount", returnValue = True)
	
	if historyCount == False:
	    if gProjRoot == 'x:\\rdr3':
		self.fbxSelectionCombo.addItem(self.__defaultFBX)
		userPref.__Saveini__("poseImporterFBXHistory", "historycount", 1)
		userPref.__Saveini__("poseImporterFBXHistory", "path_1", self.__defaultFBX)
	else:
	    for i in range(int(historyCount)):
		path = userPref.__Readini__("poseImporterFBXHistory","path_"+ str(i+1), returnValue = True)
		self.__historyList.append(path)
		self.fbxSelectionCombo.addItem(path)
	self.fbxSelectionCombo.addItem('Browse...')

	
    def OnCloseToolAfterImport_Checked(self):
	userPref.__Saveini__("poseImporter","close tool after import", self.cbCloseTool.isChecked())
	
    def onSyncToLatest_Checked(self):
	userPref.__Saveini__("poseImporter","sync to latest", self.cbSyncFBX.isChecked())

    def OnAddingFBXToCombo(self, fileName):
	#historyCount = 0
	historyCount = userPref.__Readini__("poseImporterFBXHistory","historycount", returnValue = True)
	fbxCount = int(historyCount) + 1
	userPref.__Saveini__("poseImporterFBXHistory", "historycount", fbxCount)    
	userPref.__Saveini__("poseImporterFBXHistory", "path_" + str(fbxCount), fileName)
	

    def browseForFBX(self, index):
	if unicode(index) == 'Browse...':

	    lFilePopup = FBFilePopup();
	    lFilePopup.Filter = '*.fbx'
	    lFilePopup.Style = FBFilePopupStyle.kFBFilePopupOpen
	    lFilePopup.Path = "{0}\\art\\animation\\resources\\characters\\Poses".format( gProjRoot )
    
	    lFileName = ""
	
	    if lFilePopup.Execute():
		lFileName = lFilePopup.FullFilename
		
		if lFileName not in self.__historyList:
		    self.__historyList.append(lFileName)		
		    self.OnAddingFBXToCombo(lFileName)
		    self.fbxSelectionCombo.removeItem(self.fbxSelectionCombo.count()-1)
		    self.fbxSelectionCombo.addItem(lFileName)
		    self.fbxSelectionCombo.addItem('Browse...')
		    self.fbxSelectionCombo.setCurrentIndex(self.fbxSelectionCombo.count()-2)
		else:
		    FBMessageBox("Warning", "Path exists in your History already...", "OK")
		    self.fbxSelectionCombo.setCurrentIndex(1)
		    
	    else:
		return        
	     
	else:
	    return
	
    def importPoses(self):
	mergePath = str(self.fbxSelectionCombo.currentText())
	
	self.syncFBX(mergePath)
	
	# merge options
	options = FBFbxOptions( True )
	
	options.CamerasAnimation = False
	options.BaseCameras = False
	options.CurrentCameraSettings = False
	options.CameraSwitcherSettings = False
	options.Poses = FBElementAction.kFBElementActionMerge
	options.ViewingMode = FBVideoRenderViewingMode().FBViewingModeXRay  	
	
	
 
	FBApplication().FileMerge( mergePath, False, options ) 
	
	if self.cbCloseTool.isChecked():
	    self.close()
	    #pass #close tool
	    
    def syncFBX(self, fbx):
	if self.cbSyncFBX.isChecked():
	    p4.Sync(fbx)
	pass
	
def Run( show = True ):
    if gDevelopment:
        import RS.Tools.UI.Commands as cmds
        cmds.ConnectToWing()

    tool = HandPoses()

    if show:
        tool.show()

    return tool