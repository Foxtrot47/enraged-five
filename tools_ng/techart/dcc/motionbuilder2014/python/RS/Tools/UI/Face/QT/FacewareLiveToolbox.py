'''

 Script Path: RS/Tools/UI/Face/QT/FacewareLiveToolbox.py

 Written And Maintained By: Kathryn Bodey

 Created: 19.06.14

 Description: Toolbox for Faceware transfer  
              Modules in FacewareLive.py
              url:bugstar:1822499

'''


from pyfbsdk import *

from PySide import QtGui
from PySide import QtCore

import RS.Config
import RS.Core.AssetSetup.Resource.GroupCleanup
import RS.Core.Face.FacewareLive as face

reload( face )

#compile .ui file to populate the main window
RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Face\\QT\\designer_FacewareToolbox.ui'.format( RS.Config.Script.Path.Root ),'{0}\\RS\\Tools\\UI\\Face\\QT\\FacewareToolbox_compiledQTDesigner.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Face.QT.FacewareToolbox_compiledQTDesigner as qtCompiled
reload( qtCompiled )


class MainWindow( RS.Tools.UI.QtMainWindowBase, qtCompiled.Ui_MainWindow ):
	def __init__(self ):

		RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Plot Selected Takes' )

		self.setupUi( self )

		self.CharacterList = []
		self.CharacterCheckBoxDict = {}
		self.spinBox = None
		self.comboBoxTarget = None 
		self.comboBoxSource = None

		# build takes list
		for i in RS.Globals.Characters:
			self.CharacterList.append( i )        


		'''

        Build UI

        '''          

		# namelist
		pushButtonDisable = QtGui.QPushButton( self.centralwidget )
		pushButtonDisable.setGeometry( QtCore.QRect( 10, 20, 61, 16 ) )
		pushButtonDisable.setText( "Disable All" )
		pushButtonEnable = QtGui.QPushButton( self.centralwidget )
		pushButtonEnable.setGeometry(QtCore.QRect( 80, 20, 61, 16 ) )
		pushButtonEnable.setText( "Enable All" )
		pushButtonRefresh = QtGui.QPushButton( self.centralwidget )        
		pushButtonRefresh.setGeometry(QtCore.QRect( 150, 20, 61, 16 ) )
		pushButtonRefresh.setText( "Refresh" )


		# create scroll area
		scrollSize = 165

		listLength = len( self.CharacterList )

		# 8 entries can sit in the default scrollarea size, any more than this need 20* 
		# more space for the scroll bar to appear and have enough scroll to fit
		if listLength > 8:
			excessList = listLength - 8
			scrollSize = scrollSize + ( excessList * 20 ) 

		scrollArea = QtGui.QScrollArea( self.centralwidget )
		scrollArea.setGeometry( QtCore.QRect( 10, 40, 325, 171 ) )
		scrollArea.setWidgetResizable( False )
		scrollAreaWidgetContents = QtGui.QWidget()
		scrollAreaWidgetContents.setGeometry( QtCore.QRect( 0, 0, 305, scrollSize ) )
		scrollArea.setWidget( scrollAreaWidgetContents )

		# key for checkboxes
		font = QtGui.QFont()
		font.setPointSize( 7 )

		redCheckBox = QtGui.QCheckBox( self.centralwidget )
		redCheckBox.setGeometry( QtCore.QRect( 10, 215, 8, 8 ) )  
		redCheckBox.setStyleSheet("color: black; background-color: rgb(255,204,204);")
		redCheckBox.setEnabled( False )
		redLabel = QtGui.QLabel( self.centralwidget )
		redLabel.setGeometry( QtCore.QRect( 26, 212, 500, 15 ) )
		redLabel.setText( 'Faceware does not exist for this character' )    
		redLabel.setFont(font)

		greenCheckBox = QtGui.QCheckBox( self.centralwidget )
		greenCheckBox.setGeometry( QtCore.QRect( 10, 230, 8, 8 ) )  
		greenCheckBox.setStyleSheet("color: black; background-color: rgb(229,255,204);")
		greenCheckBox.setEnabled( False )
		greenLabel = QtGui.QLabel( self.centralwidget )
		greenLabel.setGeometry( QtCore.QRect( 26, 227, 500, 15 ) )
		greenLabel.setText( 'Faceware exists for this character' )  
		greenLabel.setFont(font)

		# create scrollarea take check boxes
		counter = 8
		if self.CharacterList:
			for i in self.CharacterList:
				# checkboxes
				characterNameCheckBox = QtGui.QCheckBox( scrollAreaWidgetContents )
				characterNameCheckBox.setGeometry( QtCore.QRect( 5, counter, 13, 13.5 ) ) 
				# labels
				characterNameLabel = QtGui.QLabel( scrollAreaWidgetContents )
				characterNameLabel.setGeometry( QtCore.QRect( 25, counter, 500, 15 ) )
				characterNameLabel.setText( i.LongName )

				self.CharacterCheckBoxDict[ characterNameLabel ] = characterNameCheckBox

				counter = counter + 20     

				# uncheck checkboxes where a character already has faceware set up
				FacewareConstraints = face.FacewareConstraint()
				facewareDict = FacewareConstraints.CheckFaceWareExists( i )

				for key, value in facewareDict.iteritems():
					if value[0] == False and value[1] == False:
						#
						characterNameCheckBox.setStyleSheet("color: black; background-color: rgb(229,255,204);")
					else:
						characterNameCheckBox.setStyleSheet("color: black; background-color: rgb(255,204,204);")


		# setup/delete
		# buttons
		pushButtonSetup = QtGui.QPushButton( self.centralwidget )
		pushButtonSetup.setGeometry( QtCore.QRect( 60, 260, 231, 21 ) )
		pushButtonSetup.setText( "Setup Faceware Live on Selected" )
		pushButtonDelete = QtGui.QPushButton( self.centralwidget )
		pushButtonDelete.setGeometry( QtCore.QRect( 60, 290, 231, 21 ) )
		pushButtonDelete.setText("Delete Faceware Live on Selected")    
		pushButtonDeviceDelete = QtGui.QPushButton( self.centralwidget )
		pushButtonDeviceDelete.setGeometry( QtCore.QRect( 60, 320, 231, 21 ) )
		pushButtonDeviceDelete.setText("Delete All Faceware Live Devices")                  


		# time shift
		# frame        
		frameTimeShift = QtGui.QFrame( self.centralwidget )
		frameTimeShift.setGeometry( QtCore.QRect( 10, 370, 325, 60 ) )
		frameTimeShift.setFrameShape( QtGui.QFrame.StyledPanel )
		frameTimeShift.setFrameShadow( QtGui.QFrame.Plain )

		labelTimeShift = QtGui.QLabel( self.centralwidget )
		labelTimeShift.setGeometry( QtCore.QRect( 20, 359, 120, 22 ) )
		labelTimeShift.setText( 'Plot and Time Shift' )        

		# spinbox
		self.spinBox = QtGui.QSpinBox( frameTimeShift )
		self.spinBox.setGeometry( QtCore.QRect( 50, 20, 70, 22 ) )
		self.spinBox.setMinimum( -10000.0 )
		self.spinBox.setMaximum( 10000.0 )
		self.spinBox.setSingleStep( 1.0 )
		self.spinBox.setProperty( "value", -5.0 )

		# button
		pushButtonShiftAll = QtGui.QPushButton( frameTimeShift )
		pushButtonShiftAll.setGeometry( QtCore.QRect( 130, 20, 150, 21 ) )
		pushButtonShiftAll.setText("Plot and Time Shift All Heads")   


		# swap rig
		# frame        
		frameRigSwap = QtGui.QFrame( self.centralwidget )
		frameRigSwap.setGeometry( QtCore.QRect( 10, 450, 325, 130 ) )
		frameRigSwap.setFrameShape( QtGui.QFrame.StyledPanel )
		frameRigSwap.setFrameShadow( QtGui.QFrame.Plain )

		labelRigSwap = QtGui.QLabel( self.centralwidget )
		labelRigSwap.setGeometry( QtCore.QRect( 20, 439, 70, 22 ) )
		labelRigSwap.setText( 'Rig Swapping' )        

		# source label & combo box
		labelSource = QtGui.QLabel( frameRigSwap )
		labelSource.setGeometry( QtCore.QRect( 50, 20, 60, 22 ) )
		labelSource.setText( 'Source:' )

		self.comboBoxSource = QtGui.QComboBox( frameRigSwap )
		self.comboBoxSource.setGeometry( QtCore.QRect( 100, 20, 180, 22 ) )
		if self.CharacterList:
			for i in self.CharacterList:        
				self.comboBoxSource.addItem( i.LongName )          

		# target label & combo box
		labelTarget = QtGui.QLabel( frameRigSwap )
		labelTarget.setGeometry( QtCore.QRect( 50, 50, 60, 22 ) )
		labelTarget.setText( 'Target:' )

		self.comboBoxTarget = QtGui.QComboBox( frameRigSwap )
		self.comboBoxTarget.setGeometry( QtCore.QRect( 100, 50, 180, 22 ) )
		if self.CharacterList:
			for i in self.CharacterList:        
				self.comboBoxTarget.addItem( i.LongName )        

		# button    
		pushButtonSwapRig = QtGui.QPushButton( frameRigSwap )
		pushButtonSwapRig.setGeometry( QtCore.QRect( 50, 90, 230, 21 ) )
		pushButtonSwapRig.setText("Swap Selected Rigs")   



		'''

        Event Handler Binding   

        '''

		pushButtonEnable.released.connect( self.EnableCheckboxes ) 
		pushButtonDisable.released.connect( self.DisableCheckboxes )
		pushButtonRefresh.released.connect( self.Refresh )
		pushButtonSetup.released.connect( self.FacewareSetup )
		pushButtonDelete.released.connect( self.FacewareDelete )
		pushButtonDeviceDelete.released.connect( self.FacewareDeviceDelete )
		pushButtonShiftAll.released.connect( self.TimeShift )
		pushButtonSwapRig.released.connect( self.RigSwap )


	''' 

    Callbacks  

    '''


	def Refresh( self ):
		self.close()
		Run()


	def EnableCheckboxes( self ):
		for key, value in self.CharacterCheckBoxDict.iteritems():
			value.setChecked( True )  

	def DisableCheckboxes( self ):
		for key, value in self.CharacterCheckBoxDict.iteritems():
			value.setChecked( False ) 


	def FacewareSetup( self ):
		# get user selected characetrs
		checkedCharacterList = []
		setupCharacterList = []

		for key, value in self.CharacterCheckBoxDict.iteritems():
			if value.isChecked() == True:
				checkedCharacterList.append( key )

		for i in checkedCharacterList:
			for char in RS.Globals.Characters:
				if char.LongName == i.text():
					setupCharacterList.append( char )

		# set up faceware
		if setupCharacterList:
			FacewareConstraints = face.FacewareConstraint()

			for i in setupCharacterList: 

				facewareDict = FacewareConstraints.CheckFaceWareExists( i )

				for key, value in facewareDict.iteritems():

					characterNamespace = key
					headMergeBool = value[0]
					constraintMergeBool = value[1]
					deviceMergeBool = value[2]

					if headMergeBool == True:
						FacewareConstraints.MergeHeadFile()

					if constraintMergeBool == True:
						FacewareConstraints.MergeConstraintFile()

					if deviceMergeBool == True:
						FacewareConstraints.AddDevice()           

		self.Refresh()        


	def FacewareDelete( self ):         
		# get user selected characetrs
		checkedCharacterList = []
		facewarepropertyList = []
		deleteList = []

		for key, value in self.CharacterCheckBoxDict.iteritems():
			if value.isChecked() == True:
				split = key.text().split( ':' )
				checkedCharacterList.append( split[0] )

		for i in RS.Globals.Components:
			facewareProperty = i.PropertyList.Find( 'Faceware Component' )
			if facewareProperty:
				if 'device' in facewareProperty.Data:
					None
				else:
					facewarepropertyList.append( i )

		for i in facewarepropertyList:
			for name in checkedCharacterList:
				if i.LongName.startswith( name + '_' ):
					deleteList.append( i ) 

		# delete faceware
		for i in deleteList:
			try:
				i.FBDelete()
			except:
				pass   

		# delete empty groups        
		RS.Core.AssetSetup.Resource.GroupCleanup.CleanGroups()

		self.Refresh()


	def FacewareDeviceDelete( self ): 


		result = FBMessageBox( 'R* Warning', 'Are you sure you wish to delete devices?\n\nNote: if you have not plotted yet, please hit cancel and plot first,\nor you will lose the animation.', 'Continue', 'Cancel' )		
		if result == 1:

			deviceDeleteList = []        
	
			for i in RS.Globals.Devices:
				facewareProperty = i.PropertyList.Find( 'Faceware Component' )
				if facewareProperty:
					deviceDeleteList.append( i )
	
			if deviceDeleteList:
				for i in deviceDeleteList:
					try:
						i.FBDelete()
					except:
						pass
		else:
			None


	def TimeShift( self ):

		FacewareConstraints = face.FacewareConstraint()        

		buttonName = self.sender().text()
		spinBoxValue = self.spinBox.value()
		FacewareConstraints.TimeShift( spinBoxValue )


	def RigSwap( self ):      

		FacewareConstraints = face.FacewareConstraint()        

		sourceText = self.comboBoxSource.currentText()
		targetText = self.comboBoxTarget.currentText()
		sourceCharacter = sourceText.split( ':' )
		targetCharacter = targetText.split( ':' )

		if sourceCharacter[0] == targetCharacter[0]:
			FBMessageBox( 'R*', 'Your Source and Target selections are the same. Please try again.', 'Ok' )
		else:
			FacewareConstraints.SwapRig( sourceCharacter[0], targetCharacter[0] )      
	
			RS.Core.AssetSetup.Resource.GroupCleanup.CleanGroups()
	
			self.Refresh()


def Run():   
	########################
	# Launch UI
	########################
	form = MainWindow()
	form.show()     