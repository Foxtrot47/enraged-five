from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Globals as glo
import RS.Tools.PreviewHighHeels as core
import RS.Tools.UI


class PreviewHighHeelsTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, 'Preview High Heels', size = [ 317, 180 ] )
        
    def Create( self, pMain ):
        lReferenceListX = FBAddRegionParam(20,FBAttachType.kFBAttachLeft,"")
        lReferenceListY = FBAddRegionParam(self.BannerHeight,FBAttachType.kFBAttachTop,"")
        lReferenceListW = FBAddRegionParam(265,FBAttachType.kFBAttachNone,"")
        lReferenceListH = FBAddRegionParam(30,FBAttachType.kFBAttachNone,"") 
    
        lLoadAnimButton = FBLabel()
        lLoadAnimButton.Caption = "                                  WARNING:\n   Do Not Plot Character When Previewing High Heels"
        lLoadAnimButton.Look = FBButtonLook.kFBLookPush
        lLoadAnimButtonY = FBAddRegionParam(32,FBAttachType.kFBAttachTop,"lBannerLabel")
        pMain.AddRegion("lLoadAnimButton","lLoadAnimButton", lReferenceListX, lLoadAnimButtonY, lReferenceListW, lReferenceListH )
        pMain.SetControl("lLoadAnimButton",lLoadAnimButton) 
        
        lLoadAnimFolderButton = FBButton()
        lLoadAnimFolderButton.Caption = "Preview High Heels"
        lLoadAnimFolderButton.Look = FBButtonLook.kFBLookPush
        lLoadAnimFolderButtonY = FBAddRegionParam(62,FBAttachType.kFBAttachTop,"lBannerLabel")
        pMain.AddRegion("lLoadAnimFolderButton","lLoadAnimFolderButton", lReferenceListX, lLoadAnimFolderButtonY, lReferenceListW, lReferenceListH )
        pMain.SetControl("lLoadAnimFolderButton",lLoadAnimFolderButton) 
        lLoadAnimFolderButton.OnClick.Add(core.rs_PreviewHighHeels)
        
        lLoadAnimFolderButton1 = FBButton()
        lLoadAnimFolderButton1.Caption = "Deactivate High Heels"
        lLoadAnimFolderButton1.Look = FBButtonLook.kFBLookPush
        lLoadAnimFolderButton1Y = FBAddRegionParam(32,FBAttachType.kFBAttachTop,"lLoadAnimFolderButton")
        pMain.AddRegion("lLoadAnimFolderButton1","lLoadAnimFolderButton1", lReferenceListX, lLoadAnimFolderButton1Y, lReferenceListW, lReferenceListH )
        pMain.SetControl("lLoadAnimFolderButton1",lLoadAnimFolderButton1) 
        lLoadAnimFolderButton1.OnClick.Add(core.rs_DeactivateHighHeels)

def Run( show = True ):
    tool = PreviewHighHeelsTool()
    
    if show:
        tool.Show()
        
    return tool
