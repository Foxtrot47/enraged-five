'''

 Script Path: RS\Tools\UI\Animation\PropDampingToolbox.py

 Written And Maintained By: Kathryn Bodey

 Created: 13.08.14

 Description: props set up in such a way to get a delayed time offset between each bone 
              to get snake/tail-like motion which could be used for ropes/straps/chains 
              as well as possible other props

'''

from pyfbsdk import *

from PySide import QtGui
from PySide import QtCore

import RS.Tools.UI
import RS.Core.Animation.PropDamping

reload( RS.Core.Animation.PropDamping )

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\UI\\Animation\\QT\\designer_PropDampingToolbox.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\UI\\Animation\\QT\\PropDampingToolbox_compiled.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.UI.Animation.QT.PropDampingToolbox_compiled as toolboxQT

reload( toolboxQT )

## Create a UI with a dropbown box listing propa ssets with control objects

class DampingToolbox(  RS.Tools.UI.QtMainWindowBase, toolboxQT.Ui_MainWindow ):
	def __init__(self, parent = None):

		RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'Damping Toolbox' )
		
		self.setupUi( self )
		
		# banner	
		banner = RS.Tools.UI.BannerWidget(name = 'Damping Toolbox', helpUrl ="")
		self.lytBannerWidget.addWidget ( banner )
		
		# populate prop combobox
		propList = RS.Core.Animation.PropDamping.ReferencePropList()
		
		if propList:
			for i in propList:
				splitName = i.LongName.split( ':' )
				self.comboBox.addItem( splitName[-1] )
				
		# check if current combobox item has a damping constraint already
		for i in RS.Globals.Constraints:
			dampingProperty = i.PropertyList.Find( 'Damping Relation' )
			dampingValueProperty = i.PropertyList.Find( 'Damping Value' )
			if dampingProperty and dampingValueProperty:
				if dampingProperty.Data == str( self.comboBox.currentText() ):
					self.spinBox_2.setEnabled( True )
					self.spinBox.setEnabled( False )
					self.spinBox.setValue( int( dampingValueProperty.Data ) )
					break
				else:
					self.spinBox_2.setEnabled( False )
					self.spinBox.setEnabled( True )					
			else:
				self.spinBox_2.setEnabled( False )
				self.spinBox.setEnabled( True )		
		
		# events
		self.pushButton.pressed.connect( self.PropSelected )
		self.comboBox.currentIndexChanged.connect( self.OnChangeComboBox )
		self.pushButtonRefresh.pressed.connect( self.Refresh )


	def Refresh( self ):
		self.close()
		Run()	

	def PropSelected( self ):
		
		selectedString = self.comboBox.currentText()	
		
		if self.spinBox.isEnabled() == True:
			newBool = True
		else:
			newBool = False
					
		relation = RS.Core.Animation.PropDamping.CreateConstraint( str( selectedString ), self.spinBox.value(), self.spinBox_2.value(), newBool )
		
		FBSystem().Scene.Evaluate() 		
		
		if relation != None:		
		
			# reset combo boxes
			if newBool == True:
				self.spinBox_2.setEnabled( True )
				self.spinBox.setEnabled( False )
				
			else:
				self.spinBox.setValue( self.spinBox_2.value() )
				self.spinBox_2.setValue( 0 )
				
			relation.Active = True
			
		else:
			self.spinBox.setValue( 0 )
			self.spinBox_2.setValue( 0 )

			

	def OnChangeComboBox( self ):
		# check if current combobox item has a damping constraint already
		for i in RS.Globals.Constraints:
			dampingProperty = i.PropertyList.Find( 'Damping Relation' )
			dampingValueProperty = i.PropertyList.Find( 'Damping Value' )
			if dampingProperty and dampingValueProperty:
				if dampingProperty.Data == str( self.comboBox.currentText() ):
					self.spinBox_2.setEnabled( True )
					self.spinBox.setEnabled( False )
					self.spinBox.setValue( int( dampingValueProperty.Data ) )
					break
				else:
					self.spinBox_2.setEnabled( False )
					self.spinBox.setEnabled( True )	
			else:
				self.spinBox_2.setEnabled( False )
				self.spinBox.setEnabled( True )	
				
			if self.spinBox_2.isEnabled == True:
				self.spinBox_2.setValue( 0 )
			else:
				self.spinBox.setValue( 0 )

def Run():   

	form = DampingToolbox()
	form.show()



