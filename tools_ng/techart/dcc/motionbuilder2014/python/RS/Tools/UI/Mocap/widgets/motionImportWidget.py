from PySide import QtGui, QtCore

import os

import pyfbsdk as mobu

from RS import Globals
from RS.Tools.UI.Mocap import const
from RS.Tools.UI.Mocap.Models import motionImportModelTypes
from RS.Core.Mocap import Clapper, MocapCommands
from RS.Utils import Scene
from RS.Core.Animation import Lib
from RS.Core.ReferenceSystem.Manager import Manager


class MotionImportWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(MotionImportWidget, self).__init__(parent=parent)
        self.setupUi()

    def setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        timeShiftLayout = QtGui.QGridLayout()

        # model and tree widget
        self.mainModel =  motionImportModelTypes.MotionImportModel()
        self.treeView = QtGui.QTreeView()
        self.treeView.setModel(self.mainModel)

        # widgets for timeShift
        self.autoShiftCheckbox = QtGui.QCheckBox()
        self.manualShiftCheckbox = QtGui.QCheckBox()
        self.slateListCombobox = QtGui.QComboBox()
        self.frameCombobox = QtGui.QComboBox()
        self.frameSpinBox = QtGui.QSpinBox()
        timeShiftPushButton = QtGui.QPushButton()

        # horizontal lines
        horizontalLine = QtGui.QFrame()
        horizontalLine2 = QtGui.QFrame()

        # motion import and push button widgets
        selectAllPushButton = QtGui.QPushButton()
        deselectAllPushButton = QtGui.QPushButton()
        motionImportPushButton = QtGui.QPushButton()
        plotPushButton = QtGui.QPushButton()
        plotCharactersPushButton = QtGui.QPushButton()

        # tree properties
        self.treeView.setAlternatingRowColors(True)
        self.treeView.setStyleSheet("QTreeView {\nalternate-background-color: rgb(56, 56, 56); \nbackground-color: rgb(43, 43, 43);\n} \n")
        self.treeView.setSortingEnabled(True)
        self.treeView.header().setSortIndicator(0, QtCore.Qt.AscendingOrder)
        self.treeView.header().resizeSection(0, 400)
        self.treeView.header().resizeSection(1, 400)

        # spacer line properties
        horizontalLine.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine.setFrameShadow(QtGui.QFrame.Sunken)
        horizontalLine2.setFrameShape(QtGui.QFrame.HLine)
        horizontalLine2.setFrameShadow(QtGui.QFrame.Sunken)

        # motion import/plot button properties
        motionImportPushButton.setText("Bring in Clean Data for Selected")
        plotPushButton.setText("Plot all")
        selectAllPushButton.setText("Select all")
        deselectAllPushButton.setText("Deselect all")
        plotPushButton.setToolTip("Plots all non-gs models")
        plotCharactersPushButton.setText("Plot all Characters to Skeleton")
        plotPushButton.setToolTip("Plots all character models (non-gs), props and vehicles to skeleton")

        # timeShift widget properties
        self.autoShiftCheckbox.setText('Auto-generate Timecode(s) to shift to:')
        self.manualShiftCheckbox.setText('Manually set Frame to shift to:')
        timeShiftPushButton.setText("Time Shift Scene")
        self.autoShiftCheckbox.setCheckState(QtCore.Qt.CheckState.Checked)
        checkBoxGroup = QtGui.QButtonGroup(self)
        checkBoxGroup.addButton(self.autoShiftCheckbox)
        checkBoxGroup.addButton(self.manualShiftCheckbox)
        self.frameSpinBox.setEnabled(False)

        # populate slate combo box with slate(s) in the current scene
        self._populateSlateComboBox()

        # add widgets to layout
        timeShiftLayout.addWidget(self.autoShiftCheckbox, 0, 0)
        timeShiftLayout.addWidget(self.manualShiftCheckbox, 1, 0)
        timeShiftLayout.addWidget(self.slateListCombobox, 0, 1)
        timeShiftLayout.addWidget(self.frameCombobox, 0, 2)
        timeShiftLayout.addWidget(self.frameSpinBox, 1, 2)
        timeShiftLayout.addWidget(timeShiftPushButton, 2, 2)

        mainLayout.addLayout(timeShiftLayout, 0, 0, 1, 4)
        mainLayout.addWidget(horizontalLine, 1, 1, 1, 2)
        mainLayout.addWidget(self.treeView, 2, 0, 1, 4)
        mainLayout.addWidget(selectAllPushButton, 4, 0)
        mainLayout.addWidget(deselectAllPushButton, 4, 1)
        mainLayout.addWidget(motionImportPushButton, 4, 3)
        mainLayout.addWidget(horizontalLine2, 5, 1, 1, 2)
        mainLayout.addWidget(plotCharactersPushButton, 6, 0, 1, 4)
        mainLayout.addWidget(plotPushButton, 7, 0, 1, 4)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        self.autoShiftCheckbox.stateChanged.connect(self._setEnableOnCheckBoxes)
        self.manualShiftCheckbox.stateChanged.connect(self._setEnableOnCheckBoxes)
        self.slateListCombobox.currentIndexChanged.connect(self._populateBeepsFromSlate)
        timeShiftPushButton.released.connect(self._handleTimeShift)
        motionImportPushButton.released.connect(self._handleMotionImport)
        plotCharactersPushButton.released.connect(self._handlePlotCharactersToSkeleton)
        plotPushButton.released.connect(self._handlePlotAll)
        selectAllPushButton.released.connect(self._handleSelectAll)
        deselectAllPushButton.released.connect(self._handleDeselectAll)

    def _handleSelectAll(self):
        for child in self.mainModel.rootItem.children():
            idx = self.mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Checked
            self.mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
        self.treeView.repaint()

    def _handleDeselectAll(self):
        for child in self.mainModel.rootItem.children():
            idx = self.mainModel.rootItem.childItems.index(child)
            index0 = child.createIndex(idx, 0, child)
            index1 = child.createIndex(idx, 1, child)
            dataValue = QtCore.Qt.CheckState.Unchecked
            self.mainModel.setData(index0, dataValue, QtCore.Qt.CheckStateRole)
        self.treeView.repaint()

    def _handleTimeShift(self):
        '''
        Logic to time shift the scene
        '''
        if self.autoShiftCheckbox.isChecked():
            frame = self.frameCombobox.currentText()
            if frame != "":
                MocapCommands.ZeroOutScene(int(frame))
        else:
            frame = self.frameSpinBox.value()
            MocapCommands.ZeroOutScene(int(frame))

    def _setEnableOnCheckBoxes(self):
        '''
        Sets the enable properties of the timeshift-related widgets, based off 
        of which checkbox is active.
        '''
        if self.autoShiftCheckbox.isChecked():
            self.slateListCombobox.setEnabled(True)
            self.frameCombobox.setEnabled(True)
            self.frameSpinBox.setEnabled(False)
        else:
            self.slateListCombobox.setEnabled(False)
            self.frameCombobox.setEnabled(False)
            self.frameSpinBox.setEnabled(True)

    def _populateSlateComboBox(self):
        '''
        Populates the slate combo box with any slates found in the scene (string)
        '''
        self.slateListCombobox.clear()
        clapsDict = Clapper.GetClaps()
        if len(clapsDict) == 0:
            return
        for key in clapsDict.iterkeys():
            self.slateListCombobox.addItem(key.LongName)
        self._populateBeepsFromSlate()

    def _populateBeepsFromSlate(self):
        """
        Get all the bleeps in the slate and populate the combobox with their frame numbers
        """
        slateString = self.slateListCombobox.currentText()
        if slateString == None:
            return
        self.frameCombobox.clear()
        clapsDict = Clapper.GetClaps()

        if len(clapsDict) == 0:
            return
        for key, value in clapsDict.iteritems():
            if str(slateString) == key.LongName:
                for clap in value:
                    self.frameCombobox.addItem(str(clap))

    def _getGSAssetInfoDict(self):
        '''
        Generates the GS Asset info List
        
        Return:
            dict (FBModel:string): key=itemNull value=defaultPath
        '''
        assetInfoList = []
        gsSlateInfoDict = {}
        gsCharacterInfoDict = {}
        gsPropInfoDict = {}
        # get items from tree
        for child in self.mainModel.rootItem.children():
            # get checked state
            if child.GetCheckedState():
                # get index
                index = None
                idx = self.mainModel.rootItem.childItems.index(child)
                index1 = child.createIndex(idx, 1, child)
                # get item null
                itemRoot = child.GetGSAssetRoot()
                # get item default path
                defaultPath = child.GetDefaultPath()
                # get asset type
                assetType = child.GetGSType()
                # add info to dict
                if assetType == const.GSModelTypes.Slate:
                    gsSlateInfoDict[itemRoot] = defaultPath, index1, child, assetType
                if assetType == const.GSModelTypes.Character:
                    gsCharacterInfoDict[itemRoot] = defaultPath, index1, child, assetType
                if assetType == const.GSModelTypes.Prop:
                    gsPropInfoDict[itemRoot] = defaultPath, index1, child, assetType
                # reset checked state
                child.SetCheckedState(False)
        # add dicts to a list
        assetInfoList.append(gsSlateInfoDict)
        assetInfoList.append(gsCharacterInfoDict)
        assetInfoList.append(gsPropInfoDict)
        return assetInfoList

    def _getNonGSCharacters(self):
        '''
        Finds all of the non-gs characters in the scene
        
        Return:
            List (FBCharacter): list of character nodes found, which are not gs-related
        '''
        characterModelList = []
        for character in Globals.Characters:
            if 'gs' not in character.Name and not character.Name.startswith('M'):
                characterModelList.append(character)
        return characterModelList

    def _handleMotionImport(self):
        '''
        Motion import used to bring in clean data onto selected assets, from the UI.
        User selects the path to the clean data file for each asset, via a QFileDialog
        '''
        assetInfoList = self._getGSAssetInfoDict()
        try:
            for assetDictionary in assetInfoList:
                for key, value in assetDictionary.iteritems():
                    key.Name
        except:
            QtGui.QMessageBox.warning(None,
                                      "",
                                      "Please refresh the UI for the current scene's data.")
            return

        # deselect all
        Scene.DeSelectAll()

        # motion import for each selected item
        for assetDictionary in assetInfoList:
            for key, value in assetDictionary.iteritems():
                if value[3] == const.GSModelTypes.Slate:
                    # slate found, we merge this instead of using motion import
                    fileName, filePath = self.SlateMerge(defaultPath=value[0],
                                                        windowTitle="{0}".format(key.LongName.upper()),
                                                        nameFilters=["*.fbx", "*.FBX"])
                    # update slate combobox
                    self._populateSlateComboBox()
                else:
                    # select gs item's root/bone
                    key.Selected = True

                    # user selects the file to motion import to selected
                    fileName = None
                    filePath = None
                    filePopup = QtGui.QFileDialog()
                    filePopup.setWindowTitle("{0}".format(key.LongName.upper()))
                    filePopup.setViewMode(QtGui.QFileDialog.List)
                    filePopup.setNameFilters(["*.fbx", "*.FBX"])
                    filePopup.setDirectory(value[0])
                    if filePopup.exec_() :
                        if len(filePopup.selectedFiles()) > 0:
                            filePath = filePopup.selectedFiles()[0]
                            fileInfo = QtCore.QFileInfo(filePath)
                            fileName = str(fileInfo.baseName())
                            # normalise path and turn into a string
                            filePath = str(os.path.normpath(filePath))
                            # import motion
                            mobu.FBApplication().FileImport(filePath)

                    # deselect itemNull
                    key.Selected = False

                # create properties with user selected clean data path and name
                if fileName and filePath:
                    cleanDataPathProperty = key.PropertyCreate('Clean Data Path',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "", False,
                                                               True,
                                                               None)
                    cleanDataPathProperty.Data = filePath
                    cleanDataNameProperty = key.PropertyCreate('Clean Data Name',
                                                               mobu.FBPropertyType.kFBPT_charptr,
                                                               "",
                                                               False,
                                                               True,
                                                               None)
                    cleanDataNameProperty.Data = fileName

                # set data (update view)
                if value[1] != None:
                    # set column 1 editrole data
                    dataValue = fileName
                    self.mainModel.setData(value[1], dataValue, QtCore.Qt.EditRole)
                    # set column 1 editrole data
                    dataValue = filePath
                    self.mainModel.setData(value[1], dataValue, QtCore.Qt.ToolTipRole)
                    ## set column 0 checkstate data
                    #dataValue = False
                    #self.mainModel.setData(value[1], dataValue, QtCore.Qt.CheckStateRole)

                # deselected any row which may be selected
                self.treeView.clearSelection()

    def _handlePlotCharactersToSkeleton(self):
        '''
        Plots all non-gs characters to their skeletons
        '''
        characterModelList = self._getNonGSCharacters()
        for character in characterModelList:
            plotOptions = mobu.FBPlotOptions()
            plotOptions.PlotOnFrame = True
            plotOptions.UseConstantKeyReducer = False
            plotOptions.PlotAllTakes = True
            plotOptions.PlotTranslationOnRootOnly = True
            plotOptions.RotationFilterToApply = mobu.FBRotationFilter.kFBRotationFilterUnroll
            plotOptions.PlotPeriod = mobu.FBTime(0, 0, 0, 1)

            if character.ActiveInput == True:
                character.PlotAnimation(mobu.FBCharacterPlotWhere.kFBCharacterPlotOnSkeleton, plotOptions)
                character.ActiveInput == False

    def _handlePlotAll(self):
        '''
        Plots characters, props and vehicles.
        Will only find and plot the models that have references.
        '''
        # plot character models
        self._handlePlotCharactersToSkeleton()

        # plot everything else
        manager = Manager()
        nonCharacterReferenceList = []
        # deselect all
        Scene.DeSelectAll()
        # get references
        referenceList = manager.GetReferenceListAll()
        for reference in referenceList:
            # only plot for prop and vehicle references
            if reference.FormattedTypeName in ['Prop', 'Vehicle']:
                referenceNamespace = reference.Name
                for component in Globals.Components:
                    if '{0}:'.format(referenceNamespace) in component.LongName and isinstance(component, mobu.FBModel):
                        nonCharacterReferenceList.append(component)
                        break
        # iterate through found references, select all items in their heirarchy
        heirarchyList = []
        for reference in nonCharacterReferenceList:
            parent = Scene.GetParent(reference)
            Scene.GetChildren(parent, heirarchyList, "", False)
        # select items in heirarchy
        for item in heirarchyList:
            item.Selected = True
        # plot selected
        Scene.Plot.PlotCurrentTakeonSelected()
        # deselect all
        Scene.DeSelectAll()

    def SlateMerge(self, defaultPath=None, windowTitle="", nameFilters="*"):
        '''
        Clean slate is merged into the scene
        
        Return:
            FileName (string): the name of the slate file, selected by the user
            FilePath (String): the path to the slate file selected by the user
        '''
        fileName = None
        filePath = None

        # popup settings
        filePopup = QtGui.QFileDialog()
        filePopup.setWindowTitle(windowTitle)
        filePopup.setViewMode(QtGui.QFileDialog.List)
        filePopup.setNameFilters(nameFilters)
        filePopup.setDirectory(defaultPath)

        # launch popup
        if filePopup.exec_() :
            if len(filePopup.selectedFiles()) > 0:
                filePath = filePopup.selectedFiles()[0]
                fileInfo = QtCore.QFileInfo(filePath)
                fileName = str(fileInfo.baseName())
                # normalise path and turn into a string
                filePath = str(os.path.normpath(filePath))
                # set merge options
                options = mobu.FBFbxOptions(True, filePath)
                options.BaseCameras = False
                options.CameraSwitcherSettings = False
                options.CurrentCameraSettings = False
                options.GlobalLightingSettings = False
                options.TransportSettings = False
                # merge
                mobu.FBApplication().FileMerge(filePath, False, options)
        return fileName, filePath