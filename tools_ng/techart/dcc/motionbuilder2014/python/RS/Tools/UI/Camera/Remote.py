"""
Remote Console
"""
import clr
import sys

import os
import time
import RS.Config
import RS.Utils.Rag as Rag
reload(Rag)

import urllib2


clr.AddReference("RSG.TechArt.RemoteConnection")
from pyfbsdk import *
from pyfbsdk_additions import *
from RSG.TechArt.RemoteConnection import RemoteConnection


# Port may vary depending on project/platform
PORT = ":7890"
step = 1
walk = 100
playerHeight = 1.8

lSystem = FBSystem()


class camMatrix():
    def __init__( self ):
        self.Pos = FBVector3d()  
        self.Rot = FBVector3d()  

      


class RemoteConnect(): 
    def __init__( self ):
        self.sys = FBSystem()
        self.app = FBApplication()
        self.lEvaluator = FBEvaluateManager() 
        self.camera = None 

        # Local Trasn and ROtation Values
        self.t = None
        self.r = None

        self.camSetup = False
        self.localCamOffset = FBVector3d()   
        
        self.timer = 0
        self.graceTime = 0.01
        self.LastReceiveTime = time.time()
        
        self.mConnection = RemoteConnection()
        # Player Control
        self.Player = FBPlayerControl()
        
        
        
         
        
        

    def PopulateLayout(self, mainLyt):
    
        mainLyt.AddRegion("main","main",
              FBAddRegionParam(0,FBAttachType.kFBAttachLeft,""), 
              FBAddRegionParam(0,FBAttachType.kFBAttachTop,""),
              FBAddRegionParam(0,FBAttachType.kFBAttachRight,""),
              FBAddRegionParam(240,FBAttachType.kFBAttachNone,""))
              
              
        lyt = FBVBoxLayout(FBAttachType.kFBAttachTop)      
        mainLyt.SetControl("main",lyt)
        
                
        l = FBLabel()
        l.Caption = 'Version 0.6'
        l.Justify = FBTextJustify.kFBTextJustifyRight
        lyt.Add(l,20)
        
        b = FBButton()
        b.Caption = "Connect Cameras"
        lyt.Add(b,20)
        b.OnClick.Add(self.BtnCallback)
        

        b = FBButton()
        b.Caption = "Set World location from Game"
        lyt.Add(b,20)
        b.OnClick.Add(self.BtnCallback)
        
        lb = FBLabel()
        lb.Caption = 'Position'  
        lb.Justify = FBTextJustify.kFBTextJustifyCenter
        lyt.Add(lb,20)   
        
        self.t = FBEditVector()
        self.t.SmallStep = 0.0001
        self.t.Precision = 7.8
        lyt.Add(self.t,20)
        self.t.OnChange.Add(self.OnChange)
        
        lb = FBLabel()
        lb.Caption = 'Rotation'  
        lb.Justify = FBTextJustify.kFBTextJustifyCenter
        lyt.Add(lb,20)
        
        self.r = FBEditVector()
        lyt.Add(self.r,20)
        
        lb = FBLabel()
        lb.Caption = 'Scene -> Game Refesh (ms)'
        lb.Justify = FBTextJustify.kFBTextJustifyCenter
        lyt.Add(lb,20)
        
        hs = FBEditNumber()  
        #hs.Orientation = FBOrientation.kFBHorizontal      
        hs.SmallStep = 0.0001
        hs.LargeStep = 0.0001 
        hs.Min = 0.0
        hs.Max = 0.1
        hs.Precision = 3.4
        hs.Value = self.graceTime        
        hs.OnChange.Add(self.ValueChange)
        lyt.Add(hs, 20, height=5)
        
        b = FBButton()
        b.Caption = "Play/Capture"
        lyt.Add(b,20)
        b.OnClick.Add(self.PlayCapture)        
        
        



        return
        
    def CreateTool(self):
        # Tool creation will serve as the hub for all other controls
        t = FBCreateUniqueTool("Remote Connection")
        t.StartSizeX = 180
        t.StartSizeY = 240
        self.PopulateLayout(t)
        ShowTool(t)
        self.Unregister()
        # Not sure
        self.unregisterCam()
        self.registerCam()
        
    """ EVENTS """ 
    
    
        
 
    def BtnCallback(self, control, event):
        if control.Name == "region1":
            if control.Caption == "Disconnect Cameras":
                control.Caption =  "Connect Cameras"
                self.Unregister()
                print 'Connection Disabled'                              
            else:
                control.Caption =  "Disconnect Cameras"                
                self.setupGameCamera()                             
                self.registarcallback()
                print 'Connection Ensabled'                  

            
        if control.Name == "region2":
            self._getOffset()
            
    def OnChange(self, control, event):
        return
        
    def ValueChange(self, control,event):
        self.graceTime = control.Value
        
        
    def PlayCapture(self, control, event):
        _frameDir = 'X:/framecap'
        files = os.listdir(_frameDir)
        startIndex = len(files)
        
        self.Player.Stop()
        self.Player.GotoStart()
        
        self.Player.Play()
        
        
        
        '''
        
        lEndHardFrame = int(lSystem.CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*',''))
        
        currentIndex = startIndex
        
        for i in range(lEndHardFrame):
            #self.FrameCapture()
            #while currentIndex == startIndex:
            #    currentIndex = len(os.listdir(_frameDir))
            self.Player.StepForward()    
            startIndex = currentIndex
            lSystem.Scene.Evaluate()
        '''
     

   
        
   
        

    """ METHODS """   
    
  
        
        
    '''
    Get our game camera and our local camera offset.
    Set our camera to the game game posiotn/rotation
    '''
    def _getOffset(self):
            
        sceneOffset = self.getGameCameraPosition()    
        
        self.getSelectedCamera()

        # Get our Camera offset in MB Space and invert
        camVec = FBVector3d()  
        camVec[0] = ((self.localCamOffset[0]/walk)*-1)
        camVec[1] = ((self.localCamOffset[1]/walk)*-1)
        camVec[2] = ((self.localCamOffset[2]/walk)*-1)  
        
        # Add our offset to the world postion offset 9Converted to MB already)
        vec = (sceneOffset.Pos + camVec)
        #print 'Local Camera offset',camVec        
        #print 'world offset',sceneOffset.Pos 
        #print 'Final offset',vec
                        
        # Set our Offset Values
        self.t.Value = vec        
        self.r.Value = sceneOffset.Rot

        
        if self.camera:
            self.camera.Rotation = sceneOffset.Rot
     
            
            
        
     
     
    '''
    Get Selected Camera if any
    '''    
    def getSelectedCamera(self):
        lModelList = FBModelList()
        FBGetSelectedModels( lModelList )  
        if len( lModelList ) == 1:
            if lModelList[0].ClassName() == 'FBCamera':
                self.camera = lModelList[0]
                self.localCamOffset = self.camera.Translation
     
                

        
    
    '''
    Set up settings for Camera
    '''
    def setupGameCamera(self):          

        if not self.camSetup:
            self.mConnection.PressButton("Camera/Create camera widgets")                                                                   #open up the Camera widget Bank
            self.mConnection.WriteBoolWidget("Camera/Free camera/Override streaming focus", True)             
            self.mConnection.WriteBoolWidget("Camera/Frame propagator/Override FOV", True)             
            self.mConnection.PressButton("Renderer/Create Renderer widgets")  
            self.mConnection.PressButton("Renderer/Post FX/Create PostFX widgets") 
            
             
            self.camSetup = True
            
        self._getOffset()    
    
    '''
    Get scene Position
    '''    
    def getGameCameraPosition(self):
        
        camMx = camMatrix()
        camMx.Pos[2] = self.mConnection.ReadFloatWidget("Camera/Free camera/Position X") 
        camMx.Pos[0] = self.mConnection.ReadFloatWidget("Camera/Free camera/Position Y") 
        camMx.Pos[1] = self.mConnection.ReadFloatWidget("Camera/Free camera/Position Z") 
        
        camMx.Rot[2] = self.mConnection.ReadFloatWidget("Camera/Free camera/Orientation X") 
        camMx.Rot[0] = self.mConnection.ReadFloatWidget("Camera/Free camera/Orientation Y") 
        camMx.Rot[1] = self.mConnection.ReadFloatWidget("Camera/Free camera/Orientation Z") 

        return camMx
            
            

    
    def setGameCameraPosition(self, pos, rot):
        # Position
        self.mConnection.WriteFloatWidget("Camera/Free camera/Position X", pos[0])  
        self.mConnection.WriteFloatWidget("Camera/Free camera/Position Y", pos[1])  
        self.mConnection.WriteFloatWidget("Camera/Free camera/Position Z", pos[2])  
        # Rotation
        self.mConnection.WriteFloatWidget("Camera/Free camera/Orientation X", rot[0])  
        self.mConnection.WriteFloatWidget("Camera/Free camera/Orientation Y", rot[1])  
        self.mConnection.WriteFloatWidget("Camera/Free camera/Orientation Z", rot[2])  
        
    
    
    def updateFOV(self, fov):           
        self.mConnection.WriteFloatWidget("Camera/Frame propagator/Overridden FOV", fov)  
        
    
    '''
    
    '''    
    def FrameCapture(self):
        result = self.mConnection.WriteBoolWidget("Renderer/Post FX/Continuous Frame Capture/Single Frame Capture", True)  
        print result
        

        
        
    def setCameraRotationOrder(self):
        self.camera.PropertyList.Find("Enable Rotation DOF").Data = True
        self.camera.PropertyList.Find("Rotation Order").Data = 1 # (Set cam to XZY)
        
        
        
        
    def OnSyncUpdateCameraPosition(self, control, event):
        if (time.time() - self.LastReceiveTime) > self.graceTime:
         
            if self.camera:
                
                if self.camera.ClassName() == 'FBCamera' and self.camera.Selected:
                    lCamera = self.camera
                    
                    self.localCamOffset = lCamera.Translation
                    
                    #print 'local Z offset=',lCamera.Translation[1]
                  
                    
                    pos = FBVector3d((self.t.Value[2]*step)+(lCamera.Translation[2]/walk),
                                     (self.t.Value[0]*step)+(lCamera.Translation[0]/walk),
                                     (self.t.Value[1]*step)+(lCamera.Translation[1]/walk))
                           
                    
                    rot = FBVector3d(lCamera.Rotation[2],
                                     lCamera.Rotation[0],
                                     lCamera.Rotation[1])
                    
                    
                    self.setGameCameraPosition(pos, rot)
                    
                    # update FOV
                    FOV = lCamera.PropertyList.Find("Field Of View").Data                    
                    self.updateFOV(FOV)                    
                    
                    self.LastReceiveTime = time.time()
    
          
    def OnConnectionStateNotify(self, control, event):
        if event.Action == FBConnectionAction.kFBSelect:
            
            if event.Plug.ClassName() == 'FBCamera':
                self.camera = event.Plug                 
                self.setCameraRotationOrder()
                self._getOffset()
                
                

            
   
            
    def registerCam(self, control=None, event=None):
        self.sys.OnConnectionStateNotify.Add(self.OnConnectionStateNotify)
        self.app.OnFileExit.Add(self.unregisterCam)
        ""
    
    def unregisterCam(self, control=None, event=None):
        self.sys.OnConnectionStateNotify.Remove(self.OnConnectionStateNotify)
        
        ""
    
    
    def registarcallback(self):
        self.Unregister()
        #self.sys.OnConnectionStateNotify.Add(self.OnConnectionStateNotify)
        self.lEvaluator.OnSynchronizationEvent.Add(self.OnSyncUpdateCameraPosition)
        self.app.OnFileExit.Add(self.Unregister)
    
    
     
        
    def Unregister(self, control=None, event=None):     
        self.lEvaluator.OnSynchronizationEvent.Remove(self.OnSyncUpdateCameraPosition) 
        #self.sys.OnConnectionStateNotify.Remove(self.OnConnectionStateNotify)
        self.app.OnFileExit.Remove(self.Unregister)


    
    
    
def Run():
    remoteConnect = RemoteConnect()
    remoteConnect.CreateTool()
    

    
Run()
    




    
    








