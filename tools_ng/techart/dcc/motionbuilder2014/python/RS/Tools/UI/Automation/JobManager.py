import sys
import getpass
import inspect

from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.UI
import RS.Utils.Creation as cre
import RS.Core.Automation.Server
import RS.Config

toolName = 'Cutscene Update System - Job Manager'
emailTo = "RSG MotionBuilder SDK Support"
toolPath = inspect.getfile( inspect.currentframe() )
cc = "jason.hayes@rockstarsandiego.com"
wikipage = None

global jobsInProgressEditor
global jobsFailedEditor
global jobsInQueueEditor
global jobsCompletedEditor

global superUsers
superUsers = [ 'jhayes' ]


class JobManagerTool( RS.Tools.UI.Base ):
    def __init__( self ):
        RS.Tools.UI.Base.__init__( self, "Cutscene Update System - Job Manager", size = [ 1200, 830 ] )
        

    ## Event Handlers ##
    
    def OnClearAllCompleted_Clicked( self, source, event ):
        global superUsers
        
        if getpass.getuser() in superUsers:
            result = FBMessageBox( 'Rockstar', 'Are you sure you want to clear all completed jobs?', 'Yes', 'No' )
            
            if result == 1:
                jobsCompleted = RS.Core.Automation.Server.getCompletedJobs()
                
                for job in jobsCompleted:
                    RS.Core.Automation.Server.removeJob( job.id )
                    
                self.RefreshJobsEditors()
            
        else:
            FBMessageBox( 'Rockstar', 'You do not have sufficient privelages to perform this operation!', 'OK' )
    
    def OnClearAllInQueue_Clicked( self, source, event ):
        global superUsers
        
        if getpass.getuser() in superUsers:
            result = FBMessageBox( 'Rockstar', 'Are you sure you want to clear all jobs in the queue?', 'Yes', 'No' )
            
            if result == 1:
                jobsInQueue = RS.Core.Automation.Server.getJobsInQueue()
                
                for job in jobsInQueue:
                    RS.Core.Automation.Server.removeJob( job.id )
                    
                self.RefreshJobsEditors()
            
        else:
            FBMessageBox( 'Rockstar', 'You do not have sufficient privelages to perform this operation!', 'OK' )
    
    def OnClearAllFailed_Clicked( self, source, event ):
        global superUsers
        
        if getpass.getuser() in superUsers:
            result = FBMessageBox( 'Rockstar', 'Are you sure you want to clear all failed jobs?', 'Yes', 'No' )
            
            if result == 1:
                failedJobs = RS.Core.Automation.Server.getFailedJobs()
                
                for job in failedJobs:
                    RS.Core.Automation.Server.removeJob( job.id )
                    
                self.RefreshJobsEditors()
            
        else:
            FBMessageBox( 'Rockstar', 'You do not have sufficient privelages to perform this operation!', 'OK' )
    
    def OnRefreshEditors_Clicked( self, source, event ):
        self.RefreshJobsEditors()
        
    def OnSubmitJob_Clicked( self, source, event ):
        import RS.UI.Automation.SubmitJob
        
        '''
        path = '{0}\\UserInterface\\rs_Automation\\'.format( project.mobuPyToolsRoot )
        
        if path not in sys.path:
            sys.path.append( path )
            
        if 'rs_SubmitJob' not in sys.modules:
            __import__( 'rs_SubmitJob' )
            
        else:
            module = sys.modules[ 'rs_SubmitJob' ]
            
            if module:
                reload( module )
                __import__( 'rs_SubmitJob' )
        '''
                
        self.RefreshJobsEditors()
        
    def OnJobsFailedEditor_Clicked( self, source, event ):
        global jobsFailedEditor
         
        val = source.GetCellValue( event.Row, event.Column )
    
        jobIdColumn = 2
    
        # Resubmit
        if event.Column == 0:
            jobId = int( source.GetCellValue( event.Row, jobIdColumn ) )
            RS.Core.Automation.Server.resubmitJob( jobId )
            
            self.RefreshJobsEditors()
            
        # Remove
        elif event.Column == 1:
            jobId = int( source.GetCellValue( event.Row, jobIdColumn ) )
            
            job = RS.Core.Automation.Server.getJobById( jobId )
            
            if job:
                if job.submittedBy == RS.Config.User.Email or getpass.getuser() in superUsers:
                    RS.Core.Automation.Server.removeJob( jobId )
                    self.RefreshJobsEditors()
                    
                else:
                    FBMessageBox( 'Rockstar', 'You can only remove your jobs!', 'OK' )
                
    def OnJobsInQueueEditor_Clicked( self, source, event ):
        global jobsInQueueEditor
         
        val = source.GetCellValue( event.Row, event.Column )
    
        if event.Column == 0:
            jobId = int( source.GetCellValue( event.Row, 1 ) )
            
            job = RS.Core.Automation.Server.getJobById( jobId )
            
            if job:
                if job.submittedBy == RS.Config.User.Email or getpass.getuser() in superUsers:
                    RS.Core.Automation.Server.removeJob( jobId )
                    self.RefreshJobsEditors()
                    
                else:
                    FBMessageBox( 'Rockstar', 'You can only remove your jobs!', 'OK' )
            
    
    ## Functions ##
    
    def RefreshJobsCompletedEditor( self ):
        global jobsCompletedEditor
        
        jobsCompleted = RS.Core.Automation.Server.getCompletedJobs()
        
        jobsCompletedEditor.Clear()
        
        jobsCompletedEditor.ColumnAdd( 'ID' )
        jobsCompletedEditor.GetColumn( 0 ).Width = 40
        jobsCompletedEditor.GetColumn( 0 ).ReadOnly = True
        
        jobsCompletedEditor.ColumnAdd( 'Submitted By' )
        jobsCompletedEditor.GetColumn( 1 ).ReadOnly = True
        jobsCompletedEditor.GetColumn( 1 ).Width = 100
        
        jobsCompletedEditor.ColumnAdd( 'Status' )
        jobsCompletedEditor.GetColumn( 2 ).ReadOnly = True
        jobsCompletedEditor.GetColumn( 2 ).Width = 270
        
        jobsCompletedEditor.ColumnAdd( 'Modes' )
        jobsCompletedEditor.GetColumn( 3 ).ReadOnly = True
        
        jobsCompletedEditor.ColumnAdd( 'Total Time' )
        jobsCompletedEditor.GetColumn( 4 ).Width = 80
        jobsCompletedEditor.GetColumn( 4 ).ReadOnly = True    
        
        jobsCompletedEditor.ColumnAdd( 'Machine' )
        jobsCompletedEditor.GetColumn( 5 ).Width = 80
        jobsCompletedEditor.GetColumn( 5 ).ReadOnly = True
            
        jobsCompletedEditor.ColumnAdd( 'Filename' )
        jobsCompletedEditor.GetColumn( 6 ).ReadOnly = True
        jobsCompletedEditor.GetColumn( 6 ).Width = 500
        
        row = 0
        
        for job in reversed( jobsCompleted ):
            jobsCompletedEditor.RowAdd( job.name, row )
            
            jobsCompletedEditor.SetCellValue( row, 0, str( job.id ) )
            
            submittedBy = str( job.submittedBy ).split( '@' )[ 0 ]
            
            jobsCompletedEditor.SetCellValue( row, 1, str( submittedBy ) )
            jobsCompletedEditor.SetCellValue( row, 2, str( job.status ) )
            
            modes = ''
                    
            for mode in job.modes:
                modes += '{0},'.format( mode )
                
            modes = str( modes ).rstrip( ',' )         
    
            jobsCompletedEditor.SetCellValue( row, 3, str( modes ) )
            jobsCompletedEditor.SetCellValue( row, 4, str( job.totalTime ) )
            jobsCompletedEditor.SetCellValue( row, 5, str( job.machine ) )
            jobsCompletedEditor.SetCellValue( row, 6, str( job.filename ) )
            
            row += 1
    
    def RefreshInProgressJobsEditor( self ):
        global jobsInProgressEditor
        
        jobsInProgress = RS.Core.Automation.Server.getJobsInProgress()
        
        # In progress jobs
        jobsInProgressEditor.Clear()
        
        jobsInProgressEditor.ColumnAdd( 'ID' )
        jobsInProgressEditor.GetColumn( 0 ).Width = 40
        jobsInProgressEditor.GetColumn( 0 ).ReadOnly = True
        
        jobsInProgressEditor.ColumnAdd( 'Submitted By' )
        jobsInProgressEditor.GetColumn( 1 ).ReadOnly = True
        jobsInProgressEditor.GetColumn( 1 ).Width = 100
        
        jobsInProgressEditor.ColumnAdd( 'Status' )
        jobsInProgressEditor.GetColumn( 2 ).ReadOnly = True
        
        jobsInProgressEditor.ColumnAdd( 'Modes' )
        jobsInProgressEditor.GetColumn( 3 ).ReadOnly = True
        
        jobsInProgressEditor.ColumnAdd( 'Elapsed Time' )
        jobsInProgressEditor.GetColumn( 4 ).ReadOnly = True    
        jobsInProgressEditor.GetColumn( 4 ).Width = 90
        
        jobsInProgressEditor.ColumnAdd( 'Machine' )
        jobsInProgressEditor.GetColumn( 5 ).Width = 80
        jobsInProgressEditor.GetColumn( 5 ).ReadOnly = True
            
        jobsInProgressEditor.ColumnAdd( 'Filename' )
        jobsInProgressEditor.GetColumn( 6 ).ReadOnly = True
        jobsInProgressEditor.GetColumn( 6 ).Width = 500
        
        row = 0
        
        for job in jobsInProgress:
            jobsInProgressEditor.RowAdd( job.name, row )
            
            jobsInProgressEditor.SetCellValue( row, 0, str( job.id ) )
            
            submittedBy = str( job.submittedBy ).split( '@' )[ 0 ]
            
            jobsInProgressEditor.SetCellValue( row, 1, str( submittedBy ) )
            jobsInProgressEditor.SetCellValue( row, 2, str( job.status ) )
            
            modes = ''
    
            for mode in job.modes:
                modes += '{0},'.format( mode )
                
            modes = str( modes ).rstrip( ',' )
            
            jobsInProgressEditor.SetCellValue( row, 3, str( modes ) )
            
            jobsInProgressEditor.SetCellValue( row, 4, str( RS.Core.Automation.Server.timestampGetJobElapsedTime( job.id ) ) )
            
            jobsInProgressEditor.SetCellValue( row, 5, str( job.machine ) )
            jobsInProgressEditor.SetCellValue( row, 6, str( job.filename ) )
            
            row += 1
            
    def RefreshJobsInQueueEditor( self ):
        global jobsInQueueEditor
        
        jobsInQueue = RS.Core.Automation.Server.getJobsInQueue()
        
        # In progress jobs
        jobsInQueueEditor.Clear()
        
        # Remove job
        jobsInQueueEditor.ColumnAdd( '' )
        jobsInQueueEditor.GetColumn( 0 ).Style = FBCellStyle.kFBCellStyleButton
        jobsInQueueEditor.GetColumn( 0 ).Width = 56
        
        jobsInQueueEditor.ColumnAdd( 'ID' )
        jobsInQueueEditor.GetColumn( 1 ).Width = 40
        jobsInQueueEditor.GetColumn( 1 ).ReadOnly = True
        
        jobsInQueueEditor.ColumnAdd( 'Submitted By' )
        jobsInQueueEditor.GetColumn( 2 ).ReadOnly = True
        jobsInQueueEditor.GetColumn( 2 ).Width = 100
        
        jobsInQueueEditor.ColumnAdd( 'Status' )
        jobsInQueueEditor.GetColumn( 3 ).ReadOnly = True
        
        jobsInQueueEditor.ColumnAdd( 'Modes' )
        jobsInQueueEditor.GetColumn( 4 ).ReadOnly = True
        
        jobsInQueueEditor.ColumnAdd( 'Machine' )
        jobsInQueueEditor.GetColumn( 5 ).Width = 80
        jobsInQueueEditor.GetColumn( 5 ).ReadOnly = True
            
        jobsInQueueEditor.ColumnAdd( 'Filename' )
        jobsInQueueEditor.GetColumn( 6 ).ReadOnly = True
        jobsInQueueEditor.GetColumn( 6 ).Width = 500
        
        row = 0
        
        for job in jobsInQueue:
            jobsInQueueEditor.RowAdd( job.name, row )
            
            jobsInQueueEditor.SetCellValue( row, 0, 'Remove' )
            jobsInQueueEditor.SetCellValue( row, 1, str( job.id ) )
            
            submittedBy = str( job.submittedBy ).split( '@' )[ 0 ]
            
            jobsInQueueEditor.SetCellValue( row, 2, str( submittedBy ) )
            jobsInQueueEditor.SetCellValue( row, 3, str( job.status ) )
            
            modes = ''
            
            for mode in job.modes:
                modes += '{0},'.format( mode )
                
            modes = str( modes ).rstrip( ',' )        
    
            jobsInQueueEditor.SetCellValue( row, 4, str( modes ) )
            jobsInQueueEditor.SetCellValue( row, 5, str( job.machine ) )
            jobsInQueueEditor.SetCellValue( row, 6, str( job.filename ) )
            
            row += 1
            
    def RefreshFailedJobsEditor( self ):
        global jobsFailedEditor
        
        jobsFailed = RS.Core.Automation.Server.getFailedJobs()
    
        jobsFailedEditor.Clear()
        
        # Resubmit job.
        jobsFailedEditor.ColumnAdd( '' )
        jobsFailedEditor.GetColumn( 0 ).Style = FBCellStyle.kFBCellStyleButton
        jobsFailedEditor.GetColumn( 0 ).Width = 56
        
        # Remove job.
        jobsFailedEditor.ColumnAdd( '' )
        jobsFailedEditor.GetColumn( 1 ).Style = FBCellStyle.kFBCellStyleButton
        jobsFailedEditor.GetColumn( 1 ).Width = 56
        
        jobsFailedEditor.ColumnAdd( 'ID' )
        jobsFailedEditor.GetColumn( 2 ).Width = 40
        jobsFailedEditor.GetColumn( 2 ).ReadOnly = True
        
        jobsFailedEditor.ColumnAdd( 'Submitted By' )
        jobsFailedEditor.GetColumn( 3 ).ReadOnly = True
        jobsFailedEditor.GetColumn( 3 ).Width = 100
        
        jobsFailedEditor.ColumnAdd( 'Status' )
        jobsFailedEditor.GetColumn( 4 ).ReadOnly = True
        
        jobsFailedEditor.ColumnAdd( 'Modes' )
        jobsFailedEditor.GetColumn( 5 ).ReadOnly = True
        
        jobsFailedEditor.ColumnAdd( 'Total Time' )
        jobsFailedEditor.GetColumn( 6 ).ReadOnly = True
        jobsFailedEditor.GetColumn( 6 ).Width = 80
        
        jobsFailedEditor.ColumnAdd( 'Machine' )
        jobsFailedEditor.GetColumn( 7 ).Width = 80
        jobsFailedEditor.GetColumn( 7 ).ReadOnly = True
            
        jobsFailedEditor.ColumnAdd( 'Filename' )
        jobsFailedEditor.GetColumn( 8 ).ReadOnly = True
        jobsFailedEditor.GetColumn( 8 ).Width = 500
        
        row = 0
        
        for job in jobsFailed:
            jobsFailedEditor.RowAdd( job.name, row )
            
            jobsFailedEditor.SetCellValue( row, 0, 'Resubmit' )
            jobsFailedEditor.SetCellValue( row, 1, 'Remove' )
            jobsFailedEditor.SetCellValue( row, 2, str( job.id ) )
            
            submittedBy = str( job.submittedBy ).split( '@' )[ 0 ]
            
            jobsFailedEditor.SetCellValue( row, 3, str( submittedBy ) )
            jobsFailedEditor.SetCellValue( row, 4, str( job.status ) )
            
            modes = ''
            
            for mode in job.modes:
                modes += '{0},'.format( mode )
                
            modes = str( modes ).rstrip( ',' )        
    
            jobsFailedEditor.SetCellValue( row, 5, str( modes ) )
            jobsFailedEditor.SetCellValue( row, 6, str( job.totalTime ) )
            jobsFailedEditor.SetCellValue( row, 7, str( job.machine ) )
            jobsFailedEditor.SetCellValue( row, 8, str( job.filename ) )
            
            row += 1
    
    def RefreshJobsEditors( self ):
        self.RefreshInProgressJobsEditor()
        self.RefreshJobsInQueueEditor()
        self.RefreshFailedJobsEditor()
        self.RefreshJobsCompletedEditor()
        
    def Create( self, mainLyt ):
        global jobsInProgressEditor
        global jobsFailedEditor
        global jobsInQueueEditor
        global jobsCompletedEditor
        
        editorsLyt = FBVBoxLayout()
        
        x = FBAddRegionParam( 0, FBAttachType.kFBAttachLeft, '' )
        y = FBAddRegionParam( self.BannerHeight, FBAttachType.kFBAttachTop, '' )
        w = FBAddRegionParam( 0, FBAttachType.kFBAttachRight, '' )
        h = FBAddRegionParam( 0, FBAttachType.kFBAttachBottom, '' )
        mainLyt.AddRegion( 'main', 'main', x, y, w, h )
        
        buttonsLyt = FBHBoxLayout( FBAttachType.kFBAttachLeft )
        
        # Buttons
        
        btnRefresh = FBButton()
        btnRefresh.Caption = 'Refresh'
        btnRefresh.OnClick.Add( self.OnRefreshEditors_Clicked )
        
        btnSubmitJob = FBButton()
        btnSubmitJob.Caption = 'Submit Job'
        btnSubmitJob.OnClick.Add( self.OnSubmitJob_Clicked )
        
        if getpass.getuser() in superUsers:
            btnClearAllFailed = FBButton()
            btnClearAllFailed.Caption = 'Clear All Failed'
            btnClearAllFailed.OnClick.Add( self.OnClearAllFailed_Clicked )
            
            btnClearAllInQueue = FBButton()
            btnClearAllInQueue.Caption = 'Clear All In Queue'
            btnClearAllInQueue.OnClick.Add( self.OnClearAllInQueue_Clicked )
            
            btnClearAllCompleted = FBButton()
            btnClearAllCompleted.Caption = 'Clear All Completed'
            btnClearAllCompleted.OnClick.Add( self.OnClearAllCompleted_Clicked )
        
        buttonsLyt.Add( btnRefresh, 110 )
        buttonsLyt.Add( btnSubmitJob, 110 )
        
        if getpass.getuser() in superUsers:
            buttonsLyt.Add( btnClearAllFailed, 110 )
            buttonsLyt.Add( btnClearAllInQueue, 110 )
            buttonsLyt.Add( btnClearAllCompleted, 110 )
        
        
        # Spreadsheets
        
        jobsInProgressEditor = FBSpread()
        jobsInProgressEditor.Caption = 'Jobs In Progress'
        
        jobsInQueueEditor = FBSpread()
        jobsInQueueEditor.Caption = 'Jobs In Queue'
        jobsInQueueEditor.OnCellChange.Add( self.OnJobsInQueueEditor_Clicked )
        
        jobsFailedEditor = FBSpread()
        jobsFailedEditor.Caption = 'Jobs Failed'
        jobsFailedEditor.OnCellChange.Add( self.OnJobsFailedEditor_Clicked )
        
        jobsCompletedEditor = FBSpread()
        jobsCompletedEditor.Caption = 'Jobs Completed'
        
        self.RefreshJobsEditors()
    
        editorsLyt.Add( buttonsLyt, 24 )
        editorsLyt.AddRelative( jobsInProgressEditor, 1.0 )   
        editorsLyt.AddRelative( jobsInQueueEditor, 1.0 )  
        editorsLyt.AddRelative( jobsFailedEditor, 1.0 )
        editorsLyt.AddRelative( jobsCompletedEditor, 1.0 )
    
        mainLyt.SetControl( 'main', editorsLyt )
  
def Run( show = True ):
    if RS.Config.User.Studio == None:
        FBMessageBox( 'Rockstar', 'This system is only supported for internal Rockstar studios!', 'OK' )
        
    else:
        tool = JobManagerTool()
            
        if show:
            tool.Show()
            
        return tool
    
    return None