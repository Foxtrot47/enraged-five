'''

 Script Path: RS\Tools\UI\Camera\QT\FPS_Toolbox.py

 Written And Maintained By: Kathryn Bodey

 Created: 25.11.14

 Description: Toolbox for Faceware transfer
              Modules in FacewareLive.py
              url:bugstar:1822499

'''


from pyfbsdk import *

from PySide import QtGui
from PySide import QtCore

import os
import glob
import ctypes
import getpass
import datetime

import RS.Config
import RS.Utils.UserPreferences
import RS.Core.AssetSetup.FPS_Cam_Setup

reload( RS.Utils.UserPreferences )
reload( RS.Core.AssetSetup.FPS_Cam_Setup )

#compile .ui file to populate the main window
RS.Tools.UI.CompileUi( '{0}\\RS\\Tools\\UI\\Animation\\QT\\AnimDailies_Toolbox.ui'.format( RS.Config.Script.Path.Root ),'{0}\\RS\\Tools\\UI\\Animation\\QT\\AnimDailies_Toolbox_compiled.py'.format( RS.Config.Script.Path.Root ) )

import RS.Tools.UI.Animation.QT.AnimDailies_Toolbox_compiled as dailiesCompiled
reload( dailiesCompiled )


class MainWindow( RS.Tools.UI.QtMainWindowBase, dailiesCompiled.Ui_MainWindow ):
    def __init__(self ):

        # main window settings
        RS.Tools.UI.QtMainWindowBase.__init__( self, None, title = 'AnimDailies Toolbox' )

        # class variables
        self.TakeList = []
        self.TakeCheckboxList = []
        self.TodaysDailiesList = []
        self.TodaysDate = None
        self.PictureFormatDict = None
        self.RenderName = None
        self.RenderExtension = None
        self.PictureFormatSelected = None
        self.RenderFolder = False
        self.PictureFormatIndex = 0
        self.RenderExtensionIndex = 0
        self.IncludeLastFrameSelected = False
        self.IncludeAudioSelected = False

        # get today's date
        self.TodaysDate = str( datetime.date.today() )

        # run ui from designer file
        self.setupUi( self )

        # add banner
        banner = RS.Tools.UI.BannerWidget(name = 'FPS Toolbox', helpUrl ="")
        self.vboxlayout_banner.addWidget ( banner )

        # populate comboBox_outputformat
        outputFormatList = [ 'MOV',
                             'AVI',
                             'JPG',
                             'TGA',
                             'TIF',
                             'TIFF',
                             'YUV',
                             'SWF'
                             ]

        for i in outputFormatList:
            self.comboBox_outputformat.addItem( i )

        # populate comboBox_pictureformat
        self.PictureFormatDict = {  'From Camera':FBCameraResolutionMode.kFBResolutionCustom,
                                    '(720 x 486) D1 NTSC':FBCameraResolutionMode.kFBResolutionD1NTSC,
                                    '(640 x 480) NTSC':FBCameraResolutionMode.kFBResolutionNTSC,
                                    '(570 x 486) PAL':FBCameraResolutionMode.kFBResolutionPAL,
                                    '(720 x 576) D1 PAL':FBCameraResolutionMode.kFBResolutionD1PAL,
                                    '(1920 x 1080) HD':FBCameraResolutionMode.kFBResolutionHD,
                                    '640 x 480':FBCameraResolutionMode.kFBResolution640x480,
                                    '320 x 200':FBCameraResolutionMode.kFBResolution320x200,
                                    '320 x 240':FBCameraResolutionMode.kFBResolution320x240,
                                    '128 x 128':FBCameraResolutionMode.kFBResolution128x128,
                                    '(1600 x 1200) Full Screen':FBCameraResolutionMode.kFBResolutionFullScreen
                                    }

        for key, value in self.PictureFormatDict.iteritems():
            self.comboBox_pictureformat.addItem( key )

        # set user default preferences
        self.RenderFolder = RS.Utils.UserPreferences.__Readini__( "RenderControlToolbox","RenderFolder", self.RenderFolder )
        self.RenderExtensionIndex = RS.Utils.UserPreferences.__Readini__( "RenderControlToolbox","RenderExtensionIndex", self.RenderExtensionIndex )
        self.PictureFormatIndex = RS.Utils.UserPreferences.__Readini__( "RenderControlToolbox","PictureFormatIndex", self.PictureFormatIndex )
        self.IncludeLastFrameSelected = RS.Utils.UserPreferences.__Readini__( "RenderControlToolbox","IncludeLastFrameSelected", self.IncludeLastFrameSelected )
        self.IncludeAudioSelected = RS.Utils.UserPreferences.__Readini__( "RenderControlToolbox","IncludeAudioSelected", self.IncludeAudioSelected )

        if self.RenderFolder == False:
            user = getpass.getuser()
            self.RenderFolder = 'C:\Users\{0}\Desktop\\'.format( user )
            self.label_savepath.setText( str( self.RenderFolder ) )
        else:
            self.label_savepath.setText( str( self.RenderFolder ) )

        self.comboBox_outputformat.setCurrentIndex( self.RenderExtensionIndex )

        self.comboBox_pictureformat.setCurrentIndex( self.PictureFormatIndex )

        if self.IncludeLastFrameSelected == True:
            self.checkBox_includelastframe.setCheckState( QtCore.Qt.CheckState.Checked )

        if self.IncludeAudioSelected == True:
            self.checkBox_includeaudio.setCheckState( QtCore.Qt.CheckState.Checked )

        # set default render name
        file_name = RS.Utils.Path.GetBaseNameNoExtension( FBApplication().FBXFileName )
        self.lineEdit_rendername.setText( file_name )

        # populate self.TakeList
        for take in RS.Globals.Takes:
            self.TakeList.append( take )

        # create and populate scrollArea_takelist
        vertical_scroll_area = 99

        listLength = len( self.TakeList )

        # 8 entries can sit in the default scrollarea size, any more than this need 20* 
        # more space for the scroll bar to appear and have enough scroll to fit
        if self.TakeList > 4:
            excessList = listLength - 4
            vertical_scroll_area = vertical_scroll_area + ( excessList * 17 )

        self.scrollArea_takelist = QtGui.QScrollArea(self.verticalLayoutWidget)
        self.verticalLayout.addWidget( self.scrollArea_takelist )
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.scrollArea_takelist.setFont(font)
        self.scrollArea_takelist.setWidgetResizable(False)
        self.scrollAreaWidgetContents = QtGui.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 350, vertical_scroll_area))
        self.scrollArea_takelist.setWidget(self.scrollAreaWidgetContents)

        counter = 5
        if self.TakeList > 0:
            for take in self.TakeList:
                self.checkBox_takename = QtGui.QCheckBox(self.scrollAreaWidgetContents)
                self.checkBox_takename.setGeometry(QtCore.QRect( 10, counter, 300, 16))
                self.checkBox_takename.setText( take.Name )
                counter = counter + 17
                self.TakeCheckboxList.append( self.checkBox_takename )

        # populate comboBox_todaysdailies
        self.TodaysDailies()

        # callbacks
        self.pushbutton_refresh.pressed.connect( self.Refresh )	
        self.pushButton_selectall.pressed.connect( self.EnableCheckboxes )
        self.pushButton_deselectall.pressed.connect( self.DisableCheckboxes )
        self.pushButton_pathdirectory.pressed.connect( self.PathDirectoryPopup )
        self.pushButton_copytoclipboard.pressed.connect( self.CopyToClipboard )
        self.pushButton_render.pressed.connect( self.Render )
        self.comboBox_pictureformat.currentIndexChanged.connect( self.PictureFormat )
        self.comboBox_outputformat.currentIndexChanged.connect( self.OuputFormat )
        self.checkBox_includelastframe.stateChanged.connect( self.LastFrameRenderBool )
        self.checkBox_includeaudio.stateChanged.connect( self.AudioRenderBool )
        self.lineEdit_rendername.editingFinished.connect( self.RenderNameEdit )

    def Refresh( self ):
        self.close()
        Run()

    def EnableCheckboxes( self ):
        for checkBox in self.TakeCheckboxList:
            checkBox.setChecked( True )

    def DisableCheckboxes( self ):
        for checkBox in self.TakeCheckboxList:
            checkBox.setChecked( False )

    def PathDirectoryPopup( self ):
        folder_popup = FBFolderPopup()
        folder_popup.Caption = "Select Render Output Folder"

        if folder_popup.Execute():
            file_path = folder_popup.Path
            self.label_savepath.setText( file_path + '\\' )
            self.RenderFolder = self.label_savepath.text()
            RS.Utils.UserPreferences.__Saveini__( "RenderControlToolbox", "RenderFolder", str( self.RenderFolder ) )

    def OuputFormat( self ):
        self.RenderExtension = '.' + str( self.comboBox_outputformat.currentText() )
        self.RenderExtensionIndex = self.comboBox_outputformat.currentIndex()
        RS.Utils.UserPreferences.__Saveini__( "RenderControlToolbox", "RenderExtensionIndex", int( self.RenderExtensionIndex ) )

    def RenderNameEdit( self ):  
        self.RenderName = str( self.lineEdit_rendername.text() )

    def PictureFormat( self ):
        picture_format_string = str( self.comboBox_pictureformat.currentText() )

        for key, value in self.PictureFormatDict.iteritems():
            if picture_format_string == key:
                self.PictureFormatSelected = value
                self.PictureFormatIndex = self.comboBox_pictureformat.currentIndex()
                RS.Utils.UserPreferences.__Saveini__( "RenderControlToolbox", "PictureFormatIndex", int( self.PictureFormatIndex ) )

    def AudioRenderBool( self ):
        if self.checkBox_includeaudio.checkState() == QtCore.Qt.CheckState.Checked:
            self.IncludeAudioSelected = True
        else:
            self.IncludeAudioSelected = False

        RS.Utils.UserPreferences.__Saveini__( "RenderControlToolbox", "IncludeAudioSelected", self.IncludeAudioSelected )

    def LastFrameRenderBool( self ):
        if self.checkBox_includelastframe.checkState() == QtCore.Qt.CheckState.Checked:
            self.IncludeLastFrameSelected = True
        else:
            self.IncludeLastFrameSelected = False

        RS.Utils.UserPreferences.__Saveini__( "RenderControlToolbox", "IncludeLastFrameSelected", self.IncludeLastFrameSelected )

    def Render( self ):
        self.RenderNameEdit()
        self.OuputFormat()
        self.PictureFormat()

        # check folder exists
        if os.path.exists(self.RenderFolder):

            for take_checkbox in self.TakeCheckboxList:
                if take_checkbox.checkState() == QtCore.Qt.CheckState.Checked:
                    for take in RS.Globals.Takes:
                        if take_checkbox.text() == take.Name:

                            FBSystem().CurrentTake = take
                            current_take = FBSystem().CurrentTake

                            print str( self.RenderFolder ), self.RenderName, take.Name, self.TodaysDate, self.RenderExtension

                            render_path = str( self.RenderFolder ) + self.RenderName + '_' + take.Name + '_' + self.TodaysDate + self.RenderExtension

                            start_frame = current_take.LocalTimeSpan.GetStart().GetFrame()
                            end_frame = current_take.LocalTimeSpan.GetStop().GetFrame()

                            render_audio_bool = self.IncludeAudioSelected
                            
                            allAudio = {}
                            if self.IncludeAudioSelected == True:
                                # If we're including the audio, go through the story track and put 
                                # all the audio onto the current take, set the correct story in 
                                # frame, and keep a record so we can reset at the end
                                manager = FBStory()
                                for track in (manager.RootFolder.Tracks, manager.RootEditFolder.Tracks):
                                    for clip in track:
                                        if clip.Type != FBStoryTrackType.kFBStoryTrackAudio or clip.Mute or clip.Solo:
                                            continue
                                        for part in clip.Clips:
                                            # Store the orginal take and set the start frame
                                            allAudio[part.AudioClip] = part.AudioClip.CurrentTake
                                            # Assign it to the current take so it gets 'rendered'
                                            part.AudioClip.CurrentTake = take
                                            part.AudioClip.InPoint = part.Start

                            render_last_frame_bool = self.LastFrameRenderBool()
                            if render_last_frame_bool == True:
                                end_frame + 1

                            codec_manager = FBVideoCodecManager()
                            codec_manager.VideoCodecMode = FBVideoCodecMode.FBVideoCodecStored

                            render_options = FBVideoGrabber().GetOptions()
                            render_options.BitsPerPixel = FBVideoRenderDepth.FBVideoRender32Bits
                            render_options.ShowTimeCode = True
                            render_options.RenderAudio = render_audio_bool
                            capture_range = FBTimeSpan()
                            capture_range.Set( FBTime( 0,0,0,start_frame ), FBTime( 0,0,0,end_frame ) )
                            render_options.TimeSpan = capture_range
                            render_options.TimeSteps = FBTime(0, 0, 0, 1)
                            render_options.CameraResolution = self.PictureFormatSelected
                            render_options.OutputFileName = render_path

                            FBApplication().FileRender(render_options)
                            
                            # Reset the orginal clip takes, if they're from story
                            for clip, val in allAudio.iteritems():
                                clip.CurrentTake = val
            self.close()
            Run()
        else:
            QtGui.QMessageBox.warning(
                                    self, 
                                    "R* Warning",
                                    "Folder does not exist:\n{0}\n\nPlease re-select an existsing Save Path for output.".format(self.RenderFolder),
                                    QtGui.QMessageBox.Ok
                                    )

        # not sure if this is needed? Only for .avi's?
        '''
        # convert render using ffmpeg
        ffmpeg = r'{0}\bin\video\ffmpeg.exe'.format( RS.Config.Tool.Path.Root )
        source_render = self.RenderPath      
        source_render.replace( self.RenderExtension, '.mp4' )
        
        if os.path.exists( ffmpeg ) == False:
            message = "ffmpeg library does not exist, unable to convert to .mov. Get latest from Perforce to enable convertion ('<project>/tools/bin/video/ffmpeg.exe').\nVideo rendered as .avi."
            FBMessageBox('GTA5_AnimDailies - ffmpeg ERROR', message, 'OK')
            #raise TypeError(message)
        else:
            #use convertion settings;
            #audio codec = MP3 (This was removed as ffmpeg would create playback errors. command was '-acodec libmp3lame')
            #number of audio tracks = 2 (stereo)
            #audio rate 44100 (cd quality)
            #video codec = H.264
            #GOP (group of pictures - helps looping) = 10
            #overwrite existing file = yes
            os.system('%s -i "%s" -ac 2 -ar 44100 -vcodec libx264 -g 10 -y "%s"' % (ffmpeg, sourceMovie, finalMovie))
            os.remove(sourceMovie)
            print ('Successfully converted %s to .mp4 using H.264 codec.' % sourceMovie)
        '''

    def TodaysDailies( self ):
        if self.RenderFolder:
            dailies_files = glob.glob( self.RenderFolder + '*' )
            for i in dailies_files:
                if self.TodaysDate in i:
                    self.TodaysDailiesList.append( i )
                    self.comboBox_todaysdailies.addItem( i )

    def CopyToClipboard( self ):
        # create string to copy
        clipboard_string = ""
        if self.TodaysDailiesList:
            for i in self.TodaysDailiesList:
                clipboard_string = clipboard_string + i + '\n'
        # copy to clipboard
        if clipboard_string != "":
            ctypes.windll.user32.OpenClipboard( None )
            ctypes.windll.user32.EmptyClipboard()
            hcd = ctypes.windll.kernel32.GlobalAlloc (0x2000, len( bytes( clipboard_string ) )+1 )
            pd = ctypes.windll.kernel32.GlobalLock( hcd )
            ctypes.cdll.msvcrt.strcpy( ctypes.c_char_p( pd ), bytes( clipboard_string ) )
            ctypes.windll.kernel32.GlobalUnlock( hcd )
            ctypes.windll.user32.SetClipboardData( 1, hcd )
            ctypes.windll.user32.CloseClipboard()	


def Run():   
    form = MainWindow()
    form.show()     