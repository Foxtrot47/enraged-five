__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded


from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import textModelItem


class LoadingModelItem(textModelItem.TextModelItem):

    triggerLoading = QtCore.Signal()

    def __init__(self, parent=None):
        super(LoadingModelItem, self).__init__(["Loading..."], parent=parent)
        self._triggeredLoading = False

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not self._triggeredLoading:
            self.triggerLoading.emit()
        self._triggeredLoading = True
        if role == QtCore.Qt.DisplayRole:
            return "Loading..."
        return None
