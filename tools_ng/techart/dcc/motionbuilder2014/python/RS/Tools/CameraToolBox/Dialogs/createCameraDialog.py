"""
"The Create Camera Dialog to allow users to specify the lens type and the amount of creates to add
"""
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Widgets import hBoxWidget
from RS.Tools.CameraToolBox import const


class CreateCameraDialog(QtGui.QDialog):
    """
    The Create Camera Dialog to allow users to specify the lens type and the amount of
    creates to add

    One Signal:
        CameraCreated - When the user hits the lens type to create. emits the name of the
                        lens and the amount to create
    """
    CameraCreated = QtCore.Signal(str, int)

    def __init__(self, parent=None):
        """
        Constuctor
        """
        super(CreateCameraDialog, self).__init__(parent=parent)
        self._cams = {}
        self.SetupUi()

    def SetupUi(self):
        """
        Internal method

        Set up the UI and make all the connections
        """
        layout = QtGui.QGridLayout()
        buttonLayout = QtGui.QHBoxLayout()

        buttonWidget = QtGui.QWidget()

        createButton = QtGui.QPushButton("Create Cameras")
        cancelButton = QtGui.QPushButton("Cancel")
        mulitCamFrame = hBoxWidget.HBoxWidget()
        self._multiCameraCount = QtGui.QSpinBox()

        for label in const.lensTypes:
            button = QtGui.QRadioButton(label)
            if label == "35mm":
                button.setChecked(True)
            self._cams[button] = label
            buttonLayout.addWidget(button)

        buttonLayout.setContentsMargins(0, 0, 0, 0)
        self._multiCameraCount.setMinimum(1)

        buttonWidget.setLayout(buttonLayout)

        mulitCamFrame.addWidget(QtGui.QLabel("How Many Cameras to add:"))
        mulitCamFrame.addWidget(self._multiCameraCount)

        layout.addWidget(buttonWidget, 0, 0, 1, 2)
        layout.addWidget(mulitCamFrame, 1, 0, 1, 2)
        layout.addWidget(createButton, 2, 0)
        layout.addWidget(cancelButton, 2, 1)

        cancelButton.pressed.connect(self.hide)
        createButton.pressed.connect(self._handleCreateCameras)

        self.setLayout(layout)
        self.setModal(True)

    def _handleCreateCameras(self):
        """
        Internal method

        When the lens button has been pressed, will emit the CameraCreated signal with the
        lens and amount to create
        """
        self.hide()

        for button, label in self._cams.iteritems():
            if not button.isChecked():
                continue
            self.CameraCreated.emit(label, self._multiCameraCount.value())

            # Reset Values
            self._multiCameraCount.setValue(1)
            return


if __name__ == "__main__":
    win = CreateCameraDialog()
    win.show()
