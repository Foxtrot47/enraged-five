from PySide import QtCore, QtGui

from RS.Tools.UI.Mocap import const


class ComboBoxDelegate(QtGui.QItemDelegate):
    '''
    Creates a combobox delgate, items are set to contain a string only
    
    args:   List - list of strings
    '''
    def __init__(self, items, parent=None):
        super(ComboBoxDelegate, self).__init__(parent=parent)
        self._items = items
        self._items.sort()

    def createEditor(self, parent, option, index):
        '''
        Creates QCombobox and sets the items - just sets a string
        '''
        editor = QtGui.QComboBox(parent)
        editor.addItems(self._items)
        return editor

    def setEditorData(self, comboBox, index):
        '''
        Reads the value from the Model Data and sets the current index of the combobox accordingly
        
        Model Data -> ComboBox
        '''
        value = index.model().data(index, QtCore.Qt.DisplayRole)
        for idx in range(len(self._items)):
            if self._items[idx] == value:
                comboBox.setCurrentIndex(idx)
                break

    def setModelData(self, comboBox, model, index):
        '''
        Reads the current text of the combobox and pushes that information to the Model Data
        
        ComboBox -> Model SetData
        '''
        value = comboBox.currentText()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        Sets the combobox's position
        '''
        editor.setGeometry(option.rect)


class ComboBoxDelegateWithIcons(QtGui.QItemDelegate):
    '''
    Creates a combobox delgate, items are set to contain an icon and a string
    
    args:   dictionary
            key:String
            value:QIcon
    '''
    def __init__(self, iconDict, parent=None):
        super(ComboBoxDelegateWithIcons, self).__init__(parent=parent)
        self._iconStringList = iconDict.keys()
        self._iconList = []
        for value in iconDict.itervalues():
            self._iconList.append(value[0])

    def createEditor(self, parent, option, index):
        '''
        Creates QCombobox and sets the items - sets both string and an icon
        '''
        editor = QtGui.QComboBox(parent)
        for idx in range(len(self._iconStringList)):
            editor.addItem(self._iconList[idx], self._iconStringList[idx])
        return editor

    def setEditorData(self, comboBox, index):
        '''
        Reads the current text of the combobox and pushes that information to the Model Data
        
        Model Data -> ComboBox
        '''
        value = index.model().data(index, QtCore.Qt.DisplayRole)
        for idx in range(len(self._iconStringList)):
            if self._iconStringList[idx] == value:
                comboBox.setCurrentIndex(idx)
                break

    def setModelData(self, comboBox, model, index):
        '''
        Reads the current text of the combobox and pushes that information to the Model Data
        
        ComboBox -> Model SetData
        '''
        value = comboBox.currentText()
        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        Sets the combobox's position
        '''
        editor.setGeometry(option.rect)