"""
Config loader class
"""
import os
from xml.dom import minidom


def GetConfig():
    """
    Get the XML Doc for the lens config

    returns:
        minidom xml object
    """
    xmldoc = minidom.parse(os.path.join(os.path.dirname(__file__), "lens.xml"))
    return xmldoc


def GetLenses():
    """
    Get a list of the all the lenes in the config

    returns:
        list of strings
    """
    xmldoc = GetConfig()
    lensList = xmldoc.getElementsByTagName('lens')
    return [str(lens.attributes['name'].value) for lens in lensList]
