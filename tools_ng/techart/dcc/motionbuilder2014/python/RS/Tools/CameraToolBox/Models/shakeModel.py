# Models to deal with Camera Shake Files
import os
import time

from PySide import QtCore, QtGui
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel, baseModelItem, textModelItem
from RS import Config


class ShakeFileModelItem(baseModelItem.BaseModelItem):
    """
    Base Item Model Type
    
    This will store the shake file path, and provie information such as its name, 
    its modified date and its size
    """
    def __init__(self, filePath, parent=None):
        super(ShakeFileModelItem, self).__init__(parent=parent)
        
        self.itemData = {
            const.ShakeFileModelIndex.Name: os.path.splitext(os.path.basename(filePath))[0],
            const.ShakeFileModelIndex.Path: filePath,
            const.ShakeFileModelIndex.Size: "{0} bytes".format(os.path.getsize(filePath)),
            const.ShakeFileModelIndex.DateModifed: time.ctime(os.path.getmtime(filePath)),
        }

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return self.itemData.get(const.ShakeFileModelIndex.Name)
            if column == 1:
                return self.itemData.get(const.ShakeFileModelIndex.Size)
            if column == 2:
                return self.itemData.get(const.ShakeFileModelIndex.DateModifed)
            self.itemData[column] = value
        elif role == const.ShakeFileModelIndex.Path:
            return self.itemData[const.ShakeFileModelIndex.Path]
        elif role == const.ShakeFileModelIndex.IsShake:
            return True
        elif role == const.ShakeFileModelIndex.IsCat:
            return False
        return None

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class CameraShakeCatagoryModel(textModelItem.TextModelItem):
    """
    Base Model Type
    
    Model for getting the shake files from a catagory (folder on disk)
    """
    def __init__(self, folderPath, parent=None):
        """
        Constructor
        
        Takes a folder path to the catagory
        """
        super(CameraShakeCatagoryModel, self).__init__([os.path.basename(folderPath)], parent=parent)
        self._folderPath = folderPath
        self.setupModelData()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        if role == const.ShakeFileModelIndex.Path:
            return self._folderPath
        elif role == const.ShakeFileModelIndex.IsShake:
            return False
        elif role == const.ShakeFileModelIndex.IsCat:
            return True
        return super(CameraShakeCatagoryModel, self).data(index, role=role)

    def setupModelData(self):
        """
        Set up the model, finding all the .csf files in the folder path given to the constructor
        """
        for fileName in [item for item in os.listdir(self._folderPath) if os.path.splitext(item)[-1].lower() == ".csf"]:
            self.appendChild(
                ShakeFileModelItem(
                    os.path.join(self._folderPath, fileName),
                    parent=self
                    )
                )


class CameraShakeModel(baseModel.BaseModel):
    """
    Base Model Type
    
    Model for getting all the catagories in a folder, which in turn will get 
    the files in thoes catagories
    """
    def __init__(self, parent=None):
        super(CameraShakeModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        The column headings the model has
        """
        return ['Camera Shakes', 'Size', 'Modified date']
    
    def columnCount(self, parent):
        """
        The number of columns the model has
        """
        return len(self.getHeadings())
        
    def setupModelData(self, parent):
        """
        Set up the model, and find all the catagories in the root folder and add them to 
        the parentObject
        
        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which 
        the other items are parented to
        """
        cameraDataPath = os.path.abspath(os.path.join(Config.Tool.Path.TechArt, 'etc', 'CameraData'))
        if not os.path.exists(cameraDataPath):
            parent.appendChild(
                textModelItem.TextModelItem(["No Camera Data Found in {0}".format(cameraDataPath)], parent=parent)
            )
            return
        for folder in [item for item in os.listdir(cameraDataPath) if os.path.isdir(os.path.join(cameraDataPath, item))]:
            parent.appendChild(
                CameraShakeCatagoryModel(
                    os.path.join(cameraDataPath, folder),
                    parent=parent
                    )
                )


if __name__ == "__main__":
    mainModel = CameraShakeModel()
    win = QtGui.QTreeView()
    win.setModel(mainModel)
    win.show()
