"""
Waiting Decorator
"""
from PySide import QtCore, QtGui


class waiting(object):
    """
    Decorator. Will run the given method with the the spinning mouse cursors to show that
    the method is running

    Example:

        @waiting
        def bigLongFunctionThatTakesAgesToRun():
            for i in xrange(10000000):
              continue

    """
    def __init__(self, func):
        """
        Constructor

        args:
            func (Function): The function to decorate
        """
        QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        try:
            func()
        except:
            raise
        finally:
            QtGui.QApplication.restoreOverrideCursor()
