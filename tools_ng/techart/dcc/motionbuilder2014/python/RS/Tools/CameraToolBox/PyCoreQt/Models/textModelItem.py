__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded


from PySide import QtCore

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem


class TextModelItem(baseModelItem.BaseModelItem):
    """
    Text Item to display text in columns for the model
    """
    ItemRole = QtCore.Qt.UserRole + 42

    def __init__(self, text, font=None, parent=None, autoAppend=False):
        super(TextModelItem, self).__init__(parent=parent, autoAppend=autoAppend)
        self.itemData = self._listToDict(text)
        self.itemFont = self._listToDict(font or [])

    def columnCount(self):
        """ returns number of columns """
        return len(self.itemData)

    def data(self, index=None, role=QtCore.Qt.DisplayRole):
        column = index.column() if index is not None else 0
        if role == QtCore.Qt.DisplayRole:
            return self.itemData.get(column)
        elif role == QtCore.Qt.FontRole:
            return self.itemFont.get(column)
        elif role == self.ItemRole:
            return self
        return None

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        column = index.column()
        if role == QtCore.Qt.DisplayRole:
            self.itemData[column] = value
            return True
        elif role == QtCore.Qt.FontRole:
            self.itemFont[column] = value
            return True
        return False

    def flags(self, index=None):
        if index and index.isValid():
            return super(TextModelItem, self).flags(index)
        return QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
