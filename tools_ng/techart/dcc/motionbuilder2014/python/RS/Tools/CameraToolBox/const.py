"""
Const file, to hold indexes and consts for the camera tool box project
"""

from PySide import QtCore
from RS.Tools.CameraToolBox import configLoader


class CameraControlPropertiesIndex(object):
    """
    Easier way to deal with the camera control pinners and which properties they are
    controlled by on the camera's properties
    """
    focus = 'Focus (cm)'
    farInnerOverride = "Far Inner Override"
    farOuterOverride = "Far Outer Override"
    nearInnerOverride = "Near Inner Override"
    nearOuterOverride = "Near Outer Override"

    motionBlur = "Motion_Blur"
    circleOfConfusion = "CoC"
    circleOfConfusionOverride = "CoC Override"
    circleOfConfusionNight = "CoC Night"
    circleOfConfusionNightOverride = "CoC Night Override"

    lens = "Lens"
    shallowDoF = 'Shallow_DOF'
    simpleDof = 'Simple_DOF'

    zoom = "Field Of View"


class MobuCameraModelIndex(object):
    """
    Camera Model index for getting data out of the camera model
    """
    Name = QtCore.Qt.DisplayRole
    IsSystemCamera = QtCore.Qt.UserRole + 1
    IsSelected = QtCore.Qt.UserRole + 2
    IsInSwitcher = QtCore.Qt.UserRole + 3
    GetMobuObject = QtCore.Qt.UserRole + 10


class ShakeFileModelIndex(object):
    """
    ShakeFile Model index for getting data out of the shake model
    """
    Name = 0
    Path = QtCore.Qt.UserRole
    Size = 2
    DateModifed = 3
    IsShake = QtCore.Qt.UserRole + 1
    IsCat = QtCore.Qt.UserRole + 2

# Default Value for the FOV property
fovDefaultValue = 37

# The different lenses expected and to be used through the project
lensTypes = configLoader.GetLenses()

# Help Email CC List
ccEmailList = ["Geoff.Samuel@rockstarnorth.com"]

# Web address for the wiki
helpWikiAddress = "https://devstar.rockstargames.com/wiki/index.php/Cameras_Ingame"
