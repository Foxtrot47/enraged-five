# Widget to show and intreact with Camera Shake Files
import os
import time

from PySide import QtCore, QtGui
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.Models import shakeModel
from RS import Config

class ShakeExplorer(QtGui.QWidget):
    """
    Widget to show and allow interaction with Camera Shake Files
    
    One Signal:
        ShakeSelected - which emits with the file path of the selected shake file
    """
    
    ShakeSelected = QtCore.Signal(str)
    
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ShakeExplorer, self).__init__(parent=parent)
        self.SetupUi()
        
    def SetupUi(self):
        """
        Internal Method
        
        Setup up the Widgets UI and internals such as the model and the icons
        """
        layout = QtGui.QVBoxLayout()
        self._mainModel = shakeModel.CameraShakeModel()
        refreshIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera","icons","refresh.png")))
        refreshButton = QtGui.QPushButton(refreshIcon, "Refresh")
        
        self._treeView = QtGui.QTreeView()
        self._treeView.setModel(self._mainModel)
        self._treeView.setAlternatingRowColors(True)
        
        layout.addWidget(refreshButton)
        layout.addWidget(self._treeView)
        
        self.setLayout(layout)
        # SelectonModel needs to be its own var so GC does not clean up instantly. its a Qt "thing"
        selectionMode = self._treeView.selectionModel()
        selectionMode.selectionChanged.connect(self._handleSelectionChange)
        
    def GetSelectedShake(self):
        """
        Get the selected shake file path or None if a shake file is not selected
        """
        selected = self._treeView.selectedIndexes()
        # Only one can be returned, make sure that only one thing is selected
        if len(selected ) == 0:
            return None
        index = selected[0]
        if not self._mainModel.data(index, const.ShakeFileModelIndex.IsShake):
            return None
        return self._mainModel.data(index, const.ShakeFileModelIndex.Path)

    def GetSelectedCatagory(self):
        """
        Get the selected shake catagory or None if a shake catagory is not selected
        """
        selected = self._treeView.selectedIndexes()
        # Only one can be returned, make sure that only one thing is selected
        if len(selected ) == 0:
            return None
        index = selected[0]
        if not self._mainModel.data(index, const.ShakeFileModelIndex.IsCat):
            return None
        return self._mainModel.data(index, const.ShakeFileModelIndex.Path)

    def _handleSelectionChange(self):
        """
        Internal Method
        
        Handle the selection change of the shake tree view, and emit a new 
        shakeSelected signal if needed
        """
        newShake = self.GetSelectedShake()
        if newShake  is None:
            return
        self.ShakeSelected.emit(newShake)

if __name__ == "__main__":
    win = ShakeExplorer()
    win.show()
