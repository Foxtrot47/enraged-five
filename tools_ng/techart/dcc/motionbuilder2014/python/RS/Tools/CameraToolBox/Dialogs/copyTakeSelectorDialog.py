"""
The Copy Take Selector dialog allows users to select which take to copy data into
"""
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Widgets import hBoxWidget
from RS.Utils.Scene import Take


class CopyTakeSelectorDialog(QtGui.QDialog):
    """
    The Copy Take Selector dialog allows users to select which take to copy data into
    This is a modal dialog and will only update the list of takes on show event
    One Signal:
        TakeSelected (FBTake): When the copy button is clicked, emits the FBTake object
    """
    TakeSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constuctor
        """
        super(CopyTakeSelectorDialog, self).__init__(parent=parent)
        self._takeDict = {}
        self.SetupUi()

    def SetupUi(self):
        """
        Internal method

        Set up the UI and make all the connections
        """
        layout = QtGui.QVBoxLayout()
        buttonLayout = hBoxWidget.HBoxWidget()

        self._takeSelector = QtGui.QComboBox()
        cancelButton = QtGui.QPushButton('Cancel')
        copyButton = QtGui.QPushButton("Copy to Take")

        buttonLayout.layout().setContentsMargins(0, 0, 0, 0)

        buttonLayout.addWidget(copyButton)
        buttonLayout.addWidget(cancelButton)
        layout.addWidget(self._takeSelector)
        layout.addWidget(buttonLayout)
        self.setLayout(layout)

        copyButton.pressed.connect(self._handleCopyTakeClicked)
        cancelButton.pressed.connect(self.hide)

        self.setModal(True)

    def showEvent(self, event):
        """
        ReImplement
        Show event, where we get all the take names and populate the combobox

        args:
            event (QShowEvent) : Event parameters
        """
        self._takeSelector.clear()
        self._takeDict = dict([(tk.Name, tk) for tk in Take.GetTakeList()])
        self._takeSelector.addItems(self._takeDict.keys())

    def _handleCopyTakeClicked(self):
        """
        Internal Method

        Event for when the copy button is clicked, emit signal and close
        """
        selectedTake = self._takeDict.get(self._takeSelector.currentText())
        if selectedTake is None:
            raise IndexError("Unable to find Take '{0}'".format(self._takeSelector.currentText()))
        self.TakeSelected.emit(selectedTake)
        self.hide()


if __name__ == "__main__":
    win = CopyTakeSelectorDialog()
    win.show()
