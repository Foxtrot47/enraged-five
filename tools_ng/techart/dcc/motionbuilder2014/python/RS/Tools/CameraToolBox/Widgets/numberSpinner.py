"""
CameraLens Dial Widget
"""
import math

from PySide import QtCore, QtGui


class IntSpinnerControl(QtGui.QWidget):
    """
    Spinner Widget to allow for a better graphical interface to the dial, like a camera lens

    One Signal:
        ValueChanged - Emmited when the current spinner value changes, emits a string
    """

    ValueChanged = QtCore.Signal(int)

    def __init__(self, defaultValue=None, minimum=None, maximum=None, emitWhileScroll=True, parent=None):
        """
        Constructor

        defaultLens (str) The default item to select
        lensTypes (list of str) A list of items to represent the items in the dial
        """
        super(IntSpinnerControl, self).__init__(parent=parent)

        self._emitWhileScroll = emitWhileScroll
        self._minValue = minimum or 0
        self._maxValue = maximum or 0

        self._scrollAmount = None
        self._scrollAmountCtrl = None
        self._scrollAmountShift = None

        self._spinnerOffset = 0
        self._spacing = 60

        self.__leftPressed = False

        self._lastPosition = None

        if defaultValue is not None:
            self.CurrentValue = defaultValue

        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding))

    def setScrollAmount(self, newValue):
        """
        Set the normal scroll amount
        """
        self._scrollAmount = newValue

    def setCtrlScrollAmount(self, newValue):
        """
        Set the scroll amount when the ctrl button is being pressed
        """
        self._scrollAmountCtrl = newValue

    def setShiftScrollAmount(self, newValue):
        """
        Set the scroll amount when the shift button is being pressed
        """
        self._scrollAmountShift = newValue

    @property
    def CurrentValue(self):
        """
        Returns the the current Lens as a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            print derp.CurrentValue
        """
        return self._internalGetIndex() * -1

    @CurrentValue.setter
    def CurrentValue(self, newValue):
        """
        Sets the current lens in the dial. Takes a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            derp.CurrentLens = 'Test2'

        """
        if newValue < self._minValue or newValue > self._maxValue:
            raise ValueError("{0} is outside the Min/Max Range".format(newValue))
        self._SnapTo(newValue)
        self.repaint()
        self.ValueChanged.emit(self.CurrentValue)

    @property
    def MaximumValue(self):
        """
        Returns the the current Lens as a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            print derp.CurrentValue
        """
        return self._maxValue

    @MaximumValue.setter
    def MaximumValue(self, newValue):
        """
        Sets the current lens in the dial. Takes a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            derp.CurrentLens = 'Test2'

        """
        self._maxValue = newValue
        if self.CurrentValue > newValue:
            self.CurrentValue = newValue
        self.repaint()

    @property
    def MinimumValue(self):
        """
        Returns the the current Lens as a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            print derp.CurrentValue
        """
        return self._minValue

    @MinimumValue.setter
    def MinimumValue(self, newValue):
        """
        Sets the current lens in the dial. Takes a string

        Example:
            derp = IntSpinnerControl(50, 0, 100)
            derp.CurrentLens = 'Test2'

        """
        self._minValue = newValue
        if self.CurrentValue < newValue:
            self.CurrentValue = newValue
        self.repaint

    def _UpdateSpinner(self, position):
        """
        Internal Method

        Update the spinner based of the XY position. Position expects a QPoint
        """
        posY = position.y()
        if self._lastPosition is None:
            self._lastPosition = posY

        delta = self._lastPosition - posY
        self._lastPosition = posY

        newSpinnerOffset = self._spinnerOffset
        maxValue = (self._spacing * self._maxValue) * -1
        minValue = (self._spacing * self._minValue) * -1

        if self._spinnerOffset - delta <= maxValue:
            newSpinnerOffset = maxValue
        elif self._spinnerOffset - delta >= minValue:
            newSpinnerOffset = minValue
        else:
            newSpinnerOffset -= delta
        self._spinnerOffset = newSpinnerOffset

        if self._emitWhileScroll:
            self.ValueChanged.emit(self.CurrentValue)

    def _SnapTo(self, index):
        """
        Internal Method

        Snap the current render offset to the item index, taking into account the spacing
        """
        self._spinnerOffset = index * self._spacing * -1
        self.repaint()
        self.ValueChanged.emit(self.CurrentValue)

    def _handleSnapTo(self):
        """
        Internal Method

        Handle the snap part of the mouse release, to snap to the closest item
        """
        self._SnapTo(self._internalGetIndex() * -1)

    def _internalGetIndex(self):
        """
        Internal Method

        Get the current item index, by using the render offset and the spacing
        Returns a negative number
        """
        return round(self._spinnerOffset / float(self._spacing))

    def wheelEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Wheel Event
        """
        if not self.isEnabled():
            return

        delta = self._maxValue - self._minValue

        # Scroll amount is influenced by keyboard buttons
        keyMods = event.modifiers()
        keyModifierValues = {
                             QtCore.Qt.ControlModifier: 5000,
                             QtCore.Qt.ShiftModifier: 20
                             }

        if self._scrollAmountCtrl is not None:
            scrollAmount = self._scrollAmountCtrl
        else:
            scrollAmount = delta / keyModifierValues.get(keyMods, 1000)

        if event.delta() < 0:
            newValue = self.CurrentValue + scrollAmount
        else:
            newValue = self.CurrentValue - scrollAmount

        # Checks to ensure we're not going over or under max, min
        if newValue > self._maxValue:
            newValue = self._maxValue
        elif newValue < self._minValue:
            newValue = self._minValue

        self.CurrentValue = newValue

    def mousePressEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Press Event
        """
        # getting the position of where you have clicked the left mouse button
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self._UpdateSpinner(event.pos())

            # tracking if the button has been clicked
            # we have to do this ourself as 'QtCore.Qt.MouseButton.LeftButton' doesnt get recognised in the mouse move event
            self.__leftPressed = True

        # getting the built-in functionality of the mouse press events
        QtGui.QWidget.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Move Event
        """
        if self.__leftPressed:
            self._UpdateSpinner(event.pos())

            # force a repaint event to happen ( reruns 'paintEvent' def each time )
            self.update()

        # getting the built-in functionality of the mouse move events
        QtGui.QWidget.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        """
        ReImplemented from Qt
        Mouse Release Event
        """
        # tracking turned off when you release the left mouse button
        self.__leftPressed = False
        self._lastPosition = None
        self._handleSnapTo()
        # getting the built-in functionality of the mouse release events
        QtGui.QWidget.mouseReleaseEvent(self, event)

    def paintEvent(self, event):
        """
        ReImplemented from Qt
        Paint Event

        This is where we do all the painting for the widget, the text, the offsets
        and the dot
        """
        painter = QtGui.QPainter()
        painter.begin(self)

        middle = self.height() * 0.5
        spliiterSize = 30
        splitter = self.width() - spliiterSize

        blackPen = QtGui.QPen(QtCore.Qt.black)
        blackBrush = QtGui.QBrush(QtCore.Qt.black)
        whitePen = QtGui.QPen(QtCore.Qt.white)
        whiteBrush = QtGui.QBrush(QtCore.Qt.white)
        grayPen = QtGui.QPen(QtCore.Qt.darkGray)
        grayBrush = QtGui.QBrush(QtCore.Qt.darkGray)

        painter.setPen(blackPen)
        painter.setBrush(blackBrush)
        painter.drawRect(QtCore.QRectF(0, 0, self.width(), self.height()))

        # Paint Splitter Line
        painter.setPen(grayPen)
        painter.drawLine(splitter, 0, splitter, self.height())

        # Paint Middle Dot
        if self.isEnabled():
            painter.setPen(whitePen)
            painter.setBrush(whiteBrush)
        else:
            painter.setPen(grayPen)
            painter.setBrush(grayBrush)

        painter.drawEllipse(QtCore.QRectF(splitter + 10, middle - 5, 10, 10))

        # Paint the numbers
        lensScrollerWidth = (self.width() - spliiterSize)
        painter.setFont(QtGui.QFont('Decorative', 12))

        iterAmount = int(math.ceil((self.height() / 2.0) / self._spacing))
        midValue = int(self._spinnerOffset / self._spacing) * -1
        if midValue > self._maxValue:
            midValue = self._maxValue

        if midValue < self._minValue:
            midValue = self._minValue

        for idx in xrange(midValue, midValue + iterAmount + 1):
            if idx > self._maxValue:
                continue
            yPos = (idx * self._spacing) + self._spinnerOffset + middle - 10
            textRect = QtCore.QRect(0, yPos, lensScrollerWidth, 20)
            painter.drawText(textRect, QtCore.Qt.AlignCenter, str(idx))

        for idx in xrange(midValue - iterAmount, midValue + 1):
            if idx < self._minValue:
                continue
            yPos = (idx * self._spacing) + self._spinnerOffset + middle - 10
            textRect = QtCore.QRect(0, yPos, lensScrollerWidth, 20)
            painter.drawText(textRect, QtCore.Qt.AlignCenter, str(idx))
        painter.end()


class FloatSpinnerControl(IntSpinnerControl):
    """
    Spinner Widget to allow for a better graphical interface to the dial, like a camera lens

    One Signal:
        ValueChanged - Emmited when the current spinner value changes, emits a string
    """

    ValueChanged = QtCore.Signal(float)

    def __init__(self, defaultValue=None, minimum=None, maximum=None, parent=None):
        """
        Constructor

        defaultLens (str) The default item to select
        lensTypes (list of str) A list of items to represent the items in the dial
        """
        super(FloatSpinnerControl, self).__init__(defaultValue=defaultValue, minimum=minimum, maximum=maximum, parent=parent)

    def _internalGetIndex(self):
        """
        Internal Method

        Get the current item index, by using the render offset and the spacing
        Returns a negative number
        """
        return self._spinnerOffset / float(self._spacing)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)

    def handleValueChanged(newValue):
        print newValue

    spinner = FloatSpinnerControl(0, minumim=-1, maximum=1)
    spinner.ValueChanged.connect(handleValueChanged)
    spinner.show()
    sys.exit(app.exec_())
