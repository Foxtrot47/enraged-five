"""
Widget for all the Camera Controls, such as the spinners
"""
import os
from functools import partial

import unbind
from PySide import QtCore, QtGui

import pyfbsdk as mobu

from RS import Config
from RS.Utils.Scene import Property as RsProperty
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.Widgets import numberSpinner
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive

# unbind is the class that holds the classes for unbound instances in motion builder


class DepthOfFieldControlsWidget(QtGui.QWidget):
    """
    This Widget combines all the Camera Controls, such as the spinners
    One Signal:
        CameraDataChanged - Emmited when the camera data is changed, emits the camera object
                            the index value thats changed and the new value
    """
    CameraDataChanged = QtCore.Signal(object, object, object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(DepthOfFieldControlsWidget, self).__init__(parent=parent)
        self._uiSpinners = {}
        self._activeCamera = None
        self._activeCameraFocus = None
        self._propertyMonitor = RsProperty.PropertyMonitor()
        # Spinners details: name, propertyIndexMapping, minValue, maxValue, scrollAmounts (3xfloat)
        spinners = (
            ("CoC", const.CameraControlPropertiesIndex.circleOfConfusion, int, 0.0, 15.0, (1, 0.1, 10)),
            ("Near Inner Override", const.CameraControlPropertiesIndex.nearInnerOverride, float, -1.0, 1.0, (0.1, 0.01, 1)),
            ("CoC Override", const.CameraControlPropertiesIndex.circleOfConfusionOverride, int, 0.0, 15.0, (1, 0.1, 10)),
            ("Near Outer Override", const.CameraControlPropertiesIndex.nearOuterOverride, float, -1.0, 1.0, (0.1, 0.01, 1)),
            ("CoC Night", const.CameraControlPropertiesIndex.circleOfConfusionNight, int, 0.0, 15.0, (1, 0.1, 10)),
            ("Far Inner Override", const.CameraControlPropertiesIndex.farInnerOverride, float, -1.0, 1.0, (0.1, 0.01, 1)),
            ("CoC Night Override", const.CameraControlPropertiesIndex.circleOfConfusionNightOverride, int, 0.0, 15.0, (1, 0.1, 10)),
            ("Far Outer Override", const.CameraControlPropertiesIndex.farOuterOverride, float, -1.0, 1.0, (0.1, 0.01, 1)),
        )
        self.SetupUi(spinners)

    def SetupUi(self, spinners):
        """
        Intenal Method

        Setups up the UI and all the connections.
        Takes in a list of lists of data for the spinners.
        The data should be comprised of name, propertyIndexMapping, tyoeOfValue, minValue, maxValue, scrollAmount

        Example could be:
        (
            ("CoC", const.CameraControlPropertiesIndex.circleOfConfusion, int, 0.0, 15.0, (1, 0.1, 10)),
            ("CoC Override", const.CameraControlPropertiesIndex.circleOfConfusionOverride, float, 0.0, 15.0, (1, 0.1, 10)),
        )
        """
        # Create Layouts
        mainLayout = QtGui.QGridLayout()
        focusSizeLayout = QtGui.QVBoxLayout()

        # Create Widgets
        spacerItem = QtGui.QSpacerItem(20, 50, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        focusSizeWidget = QtGui.QWidget()
        self._focusSizeSlider = QtGui.QSlider()
        keyIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Camera", "icons", "key.png"))
        # Generate the spin widgets
        spinWidgets = []
        for spinner in spinners:
            # Unpack data
            name, cameraControlIndex, typeToUse, minValue, maxValue, scrollVals = spinner
            normalScroll, slowScroll, fastScroll = scrollVals

            layout = QtGui.QGridLayout()
            spinWidget = QtGui.QWidget()

            if typeToUse == float:
                dial = numberSpinner.FloatSpinnerControl(minValue, minValue, maxValue)
            elif typeToUse == int:
                dial = numberSpinner.IntSpinnerControl(minValue, minValue, maxValue)
            else:
                raise ValueError("Unknown spinner Type {0}".format(typeToUse))
            self._uiSpinners[cameraControlIndex] = dial

            dialButton = QtGui.QPushButton(name)
            keyButton = QtGui.QToolButton()
            keyButton.setIcon(keyIcon)
            dial.setScrollAmount(normalScroll)
            dial.setCtrlScrollAmount(slowScroll)
            dial.setShiftScrollAmount(fastScroll)

            layout.addWidget(dialButton, 0, 0)
            layout.addWidget(keyButton, 0, 1)
            layout.addWidget(dial, 1, 0, 2, 2)

            spinWidget.setLayout(layout)
            fixedHeight = 100
            spinWidget.setMinimumHeight(fixedHeight)
            spinWidget.setMaximumHeight(fixedHeight)

            spinWidgets.append(spinWidget)
            dial.ValueChanged.connect(partial(self._handleSpinner, dial, cameraControlIndex))
            dialButton.pressed.connect(partial(self._handleDialButton, cameraControlIndex))
            keyButton.pressed.connect(partial(self._handleKeyButton, cameraControlIndex))

        # Customize Widgets
        self._focusSizeSlider.setMaximum(3000)
        self._focusSizeSlider.setMinimum(100)
        self._focusSizeSlider.setOrientation(QtCore.Qt.Horizontal)
        self._focusSizeSlider.setStyleSheet("""
        QSlider::groove:horizontal {
            border: 1px solid #999999;
            height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */
            background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
            margin: 2px 0;
        }

        QSlider::handle:horizontal {
            background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);
            border: 1px solid #5c5c5c;
            width: 18px;
            margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
            border-radius: 3px;
        }
        """)
        focusSizeWidget.setMaximumHeight(50)

        # Assign Layouts
        focusSizeLayout.addWidget(QtGui.QLabel("Focus Marker Size"))
        focusSizeLayout.addWidget(self._focusSizeSlider)
        focusSizeWidget.setLayout(focusSizeLayout)

        maxColumnCount = 2
        column = 0
        row = 0
        for spinner in spinWidgets:
            if column >= maxColumnCount:
                row += 1
                column = 0
            mainLayout.addWidget(spinner, row, column)
            column += 1
        row += 1
        mainLayout.addWidget(focusSizeWidget, row, 0, 2, 2)
        row += 1
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.addItem(spacerItem, row, 0)

        # Set Layouts
        self.setLayout(mainLayout)

        self.setEnabled(False)

        # Connect Signals
        self._focusSizeSlider.valueChanged.connect(self._handleFocusSizeSliderChange)

    @noRecursive.noRecursive
    def _handleButton(self, prop, value):
        """
        Internal Method

        Handle a toggle button change, update the property in the MoBu Camera

        Prop (str) The property name to edit
        value (bool) The new Value to set
        """
        self._activeCamera.PropertyList.Find(prop).Data = value
        self.CameraDataChanged.emit(self._activeCamera, prop, value)

    @noRecursive.noRecursive
    def _handleSpinner(self, ui, prop, value):
        """
        Internal Method

        Handle a spinner value change, update the property in the MoBu Camera

        Prop (str) The property name to edit
        value (float) The new value to set
        """
        cameraAttra = self._activeCamera.PropertyList.Find(prop)
        if type(cameraAttra.Data) is int:
            value = int(value)

        cameraAttra.Data = value
        self.CameraDataChanged.emit(self._activeCamera, prop, value)
        ui.CurrentValue = cameraAttra.Data

    @noRecursive.noRecursive
    def _handleKeyButton(self, prop):
        """
        Internal Method

        Key the selected FCurve property for the active camera
        """
        if self._activeCamera is None:
            return

        fProp = self._activeCamera.PropertyList.Find(prop)
        if (fProp):
            fProp.Key()

    def _handleDialButton(self, prop):
        """
        Internal Method

        Select the FCurve property for the active camera
        """
        if self._activeCamera is None:
            return

        # deselect all the other properties
        for propAll in [_prop for _prop in self._activeCamera.PropertyList if _prop.IsAnimatable()]:
            propAll.SetFocus(False)

        fProp = self._activeCamera.PropertyList.Find(prop)
        if (fProp):
            # Need to use this method to actually select the property in MotionBuilder
            fProp.SetFocus(True)

    @noRecursive.noRecursive
    def _handleFocusSizeSliderChange(self, newValue):
        """
        Change the size of the focus marker for the active camera

        Arguments:
            newValue (float): size for the focus marker
        """
        if isinstance(self._activeCameraFocus, tuple(unbind.unbound_wrapper.values())):
            self._getFocusMarker()

        if self._activeCamera.GetObjectStatus(mobu.FBObjectStatus.kFBStatusOwnedByUndo):
            self._activeCamera = None

        if self._activeCameraFocus is not None:
            self._activeCameraFocus.Size = newValue

    def ActiveCamera(self):
        """
        Gets the current active camera on the widget
        
        return:
            FBCamera Object
        """
        return self._activeCamera

    @noRecursive.noRecursive
    def SetActiveCamera(self, camera):
        """
        Set the active camera to the given camera.
        This will populate the UI controls

        Takes a FBCamera Object
        """
        self._propertyMonitor.Clear()
        self._propertyMonitor.Deactivate()

        lens = camera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)
        if lens is None:
            self.setEnabled(False)
            self._activeCamera = None
            return

        self._activeCamera = camera
        self.setEnabled(True)

        for key, ui in self._uiSpinners.iteritems():
            fProp = camera.PropertyList.Find(key)
            ui.CurrentValue = fProp.Data
            self._propertyMonitor.Add(fProp, self._handleMobuSpinnerPropertyUpdate, extraArgs={"uiElement": ui, "prop": key})

        self._activeCameraFocus = self._getFocusMarker()
        if self._activeCameraFocus is not None:
            self._focusSizeSlider.setValue(self._activeCameraFocus.Size)
        # activate callback
        self._propertyMonitor.Activate()

    def _getFocusMarker(self):
        if self._activeCamera is None:
            return None
        
        if isinstance(self._activeCamera, tuple(unbind.unbound_wrapper.values())):
            return None
        
        if self._activeCamera.GetObjectStatus(mobu.FBObjectStatus.kFBStatusOwnedByUndo):
            return True
        
        for child in self._activeCamera.Children:
            if "focus_marker" in child.LongName.lower():
                return child

    @noRecursive.noRecursive
    def _handleMobuSpinnerPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        """
        Internal Method

        Not Recurisve

        Handler event to update the ui from the mobu object getting changed
        """
        if uiElement is None:
            return
        uiElement.CurrentValue = targetProperty.Data
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.Data)


if __name__ == "__main__":
    win = DepthOfFieldControlsWidget()
    win.show()
