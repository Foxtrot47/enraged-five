"""
Main Shake Control Widget, hold and allows interaction with all the different parts of the Shake controls
"""
from functools import partial

from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.PyCoreQt.Widgets import checkboxButton
from RS.Tools.CameraToolBox.Widgets import shakeExplorer
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive
from RS.Core.Camera import ShakerLib


class ShakeControlsWidget(QtGui.QWidget):
    """
    Widget to hold the controls for shake, such as the loading of shake files, the phase,
    and options to plot, delete shake and export shake to the camera in the widget
    """
    def __init__(self, parent=None):
        """
        Constructor
        """
        super(ShakeControlsWidget, self).__init__(parent=parent)
        self._activeCamera = None
        self.SetupUi()

    def SetupUi(self):
        """
        Internal Method

        Setup the Ui, setting up all the layouts, widgets and connections
        """
        # Create Layouts
        layout = QtGui.QGridLayout()
        shakePhaseLayout = QtGui.QVBoxLayout()

        # Create Widgets
        self._shakeExp = shakeExplorer.ShakeExplorer()
        shakePhaseWidget = QtGui.QWidget()
        self._shakePhaseSlider = QtGui.QSlider()
        self._shakePhaseText = QtGui.QLabel("0%")
        self._shakePhaseLiveUpdate = QtGui.QCheckBox("Live Update")

        shakeFrameLayout = QtGui.QGridLayout()
        self._shakeFrame = QtGui.QGroupBox("Apply Shake Properties")

        self._shakeTranslation = checkboxButton.CheckboxButon("Translation")
        self._shakeRotation = checkboxButton.CheckboxButon("Rotation", checked=True)
        self._shakeUseFov = checkboxButton.CheckboxButon("On")

        shakeButtonsWidget = QtGui.QWidget()
        shakeButtonsLayout = QtGui.QVBoxLayout()
        self._applyShakeButton = QtGui.QPushButton("Apply Shake to Selected Camera")
        self._removeShakeButton = QtGui.QPushButton("Remove Shake from Selected Camera")
        self._applyShakeToAllButton = QtGui.QPushButton("Apply Shake to All Cameras")
        self._removeShakeFromAllButton = QtGui.QPushButton("Remove Shake from All Cameras")
        self._exportShakeButton = QtGui.QPushButton("Export Camera as Shake Data")
        self._exportLocalTransformsShakeButton = QtGui.QCheckBox("Export Local Transforms")

        # Set Properties
        self._shakePhaseSlider.setOrientation(QtCore.Qt.Orientation.Horizontal)

        # Assign Layouts
        shakePhaseLayout.addWidget(QtGui.QLabel("Shake Phase:"))
        shakePhaseLayout.addWidget(self._shakePhaseText)
        shakePhaseLayout.addWidget(self._shakePhaseSlider)
        shakePhaseLayout.addWidget(self._shakePhaseLiveUpdate)
        shakeFrameLayout.addWidget(self._shakeTranslation, 0, 0)

        shakeFrameLayout.addWidget(self._shakeRotation, 1, 0)

        shakeFrameLayout.addWidget(QtGui.QLabel("FOV"), 2, 0)
        shakeFrameLayout.addWidget(self._shakeUseFov, 3, 0)

        for item in [self._applyShakeButton, self._removeShakeButton, self._applyShakeToAllButton,
                     self._removeShakeFromAllButton, self._exportShakeButton]:
            shakeButtonsLayout.addWidget(item)

        layout.addWidget(self._shakeExp, 0, 0, 3, 1)
        layout.addWidget(shakePhaseWidget, 3, 0)
        layout.addWidget(self._shakeFrame, 0, 1)
        layout.addWidget(shakeButtonsWidget, 2, 1, 2, 1)

        # Set Layouts
        shakePhaseWidget.setLayout(shakePhaseLayout)
        self._shakeFrame.setLayout(shakeFrameLayout)
        shakeButtonsWidget.setLayout(shakeButtonsLayout)

        self.setLayout(layout)

        # Connect Signals
        for item in [self._shakeTranslation, self._shakeRotation, self._shakeUseFov]:
            item.pressed.connect(self._handleShakerProperties)

        self._applyShakeButton.pressed.connect(partial (self.ApplyShake))
        self._removeShakeButton.pressed.connect(partial (self.RemoveShake))
        self._applyShakeToAllButton.pressed.connect(partial (self.ApplyShake, True))
        self._removeShakeFromAllButton.pressed.connect(partial (self.RemoveShake, False))
        self._exportShakeButton.pressed.connect(self.ExportShake)

        self._shakePhaseSlider.valueChanged.connect(self._handleShakePhaseSliderUpdate)
        self._shakeExp.ShakeSelected.connect(self._handleShakeSelected)

    def _buildShakeMapping(self):
        """
        Internal Method

        Builds a Dict with the trans, rot and fov based off the UI so it can be
        used with the shaker lib
        """
        mapper = ShakerLib.ShakerInterFaceMap()

        mapper.transX = self._shakeTranslation.isDown()
        mapper.transY = self._shakeTranslation.isDown()
        mapper.transZ = self._shakeTranslation.isDown()

        mapper.rotX = self._shakeRotation.isDown()
        mapper.rotY = self._shakeRotation.isDown()
        mapper.rotZ = self._shakeRotation.isDown()

        mapper.fov = self._shakeUseFov.isDown()

        return mapper

    def _handleShakeSelected(self, shakeFile):
        """
        Internal Method

        Handle a shake being selected in the shakeExplorer, and loading it into the Shaker lib

        args:
            shakeFile (stirng): File path to a shake file
        """
        self.LoadShake(shakeFile)

    def _handleShakePhaseSliderUpdate(self, newValue):
        """
        Internal Method

        Handle the phase value being updated, with the changes being pushed to the Shaker lib
        and the UI getting an update as well

        args:
            newValue (int): New value of the shake phase slider
        """
        self._shakePhaseText.setText("{0}%".format(newValue))
        self.ShakeWeight(newValue)

    def _handleShakerProperties(self):
        """
        Internal Method

        Handle the Shaker properties being changed, with the changes being pushed to the Shaker lib
        """
        ShakerLib.HandShake.SetPropertiesMapping(self._buildShakeMapping())

    def ExportShake(self):
        """
        Export the Shake
        """
        ShakerLib.HandShake.DisableLocalExportTransforms(self._exportLocalTransformsShakeButton.isChecked())
        ShakerLib.HandShake.ExportCamShake(self._activeCamera)

    def ApplyShake(self, allCameras=False):
        """
        Apply the Shake to all or just the current camera

        kwargs:
            allCameras (bool): If the shake is to be added to all the cameras or just the active one
        """
        if not allCameras:
            ShakerLib.HandShake.AddShakeSelected(self._activeCamera)
        else:
            ShakerLib.HandShake.AddShake()

    def RemoveShake(self, allCameras=False):
        """
        Remove the Shake from all or just the current camera

        kwargs:
            allCameras (bool): If the shake is to be removed from all the cameras or just the active one
        """
        if not allCameras:
            ShakerLib.HandShake.DeleteShakeSelected(self._activeCamera)
        else:
            ShakerLib.HandShake.DeleteShake()

    def LoadShake(self, filePath):
        """
        Load a given shake file given its file path

        args:
            filePath (string): Path to load in
        """
        ShakerLib.HandShake.LoadShakeFile(filePath)

    def LiveShakeUpdate(self):
        """
        Get the current state of the live update on the shake phase checkbox

        returns:
            bool : If the live update is on or off
        """
        return self._shakePhaseLiveUpdate.checkState()

    def ShakeWeight(self, shakeValue):
        """
        Update the shake weight value

        args:
            shakeValue (float): shake weight value to use
        """
        ShakerLib.HandShake.UpdateShakeWeight(shakeValue)

        if self._activeCamera is not None and self.LiveShakeUpdate():
            ShakerLib.HandShake.AddShakeSelected(self._activeCamera)

    def ActiveCamera(self):
        """
        Gets the current active camera on the widget
        
        return:
            FBCamera Object
        """
        return self._activeCamera

    @noRecursive.noRecursive
    def SetActiveCamera(self, newCamera):
        """
        Set the current Active Camera for the widget.

        args:
            newCamera (FBCamera): The camera to set as the active camera for the widget
        """
        self._activeCamera = newCamera

if __name__ == "__main__":
    win = ShakeControlsWidget()
    win.show()
