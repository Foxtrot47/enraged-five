from PySide import QtGui


class VBoxWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(VBoxWidget, self).__init__(parent=parent)
        self._layout = QtGui.QVBoxLayout()
        self.setLayout(self._layout)
        
    def addWidget(self, widget):
        self._layout.addWidget(widget)
