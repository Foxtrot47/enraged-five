"""
Widget for the Lens and Zoom FOV spinner
"""
import os
from functools import partial

from PySide import QtCore, QtGui

from RS import Config
from RS.Tools import UI
from RS.Utils.Scene import Property as RsProperty
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.Widgets import cameraLensDial, numberSpinner
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive
from RS.Tools.CameraToolBox.PyCoreQt.Widgets import dragableSpinBox


class CameraControlsWidget(QtGui.QWidget):
    """
    This widget contains the lens and zoom FOV spinner
    One Signal:
        CameraDataChanged - Emmited when the camera data is changed, emits the camera object
                            the index value thats changed and the new value
    """
    CameraDataChanged = QtCore.Signal(object, object, object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(CameraControlsWidget, self).__init__(parent=parent)
        self._activeCamera = None
        self._propertyMonitor = RsProperty.PropertyMonitor()
        self._uiSpinnersWidgets = {}
        self._uiSpinners = {}
        self.SetupUi()

    def SetupUi(self):
        """
        Intenal Method

        Setups up the UI and all the connections.
        """
        keyIcon = QtGui.QIcon(os.path.join(Config.Script.Path.ToolImages, "Camera", "icons", "key.png"))

        self._focusSpinnerSliderUpdate = False

        # Create Layouts
        mainLayout = QtGui.QGridLayout()
        lensLayout = QtGui.QGridLayout()
        focusLayout = QtGui.QHBoxLayout()

        # Create Widgets
        lensWidget = QtGui.QWidget()
        lensLabel = QtGui.QPushButton("Lens")
        self._lensFinder = cameraLensDial.CameraLensDial()

        focusButton = QtGui.QPushButton('Focus (cm)')
        self._focusSpinner = dragableSpinBox.DragableSpinBox()
        self._focusSlider = QtGui.QSlider()

        # Generate the spin widgets - Slightly weird as its moddled after the camera controls setup
        spinners = [
            ("Zoom", const.CameraControlPropertiesIndex.zoom, float, 0.0, 180.0, (100, 5, 500)),
        ]
        spinWidgets = []
        for spinner in spinners:
            # Unpack data
            name, cameraControlIndex, typeToUse, minValue, maxValue, scrollVals = spinner
            normalScroll, slowScroll, fastScroll = scrollVals

            layout = QtGui.QGridLayout()
            spinWidget = QtGui.QWidget()

            if typeToUse == float:
                dial = numberSpinner.FloatSpinnerControl(minValue, minValue, maxValue)
            elif typeToUse == int:
                dial = numberSpinner.IntSpinnerControl(minValue, minValue, maxValue)
            else:
                raise ValueError("Unknow spinner Type {0}".format(typeToUse))

            dialButton = QtGui.QPushButton(name)
            keyButton = QtGui.QToolButton()
            keyButton.setIcon(keyIcon)
            dial.setScrollAmount(normalScroll)
            dial.setCtrlScrollAmount(slowScroll)
            dial.setShiftScrollAmount(fastScroll)

            layout.addWidget(dialButton, 0, 0)
            layout.addWidget(keyButton, 0, 1)
            layout.addWidget(dial, 1, 0, 2, 2)

            spinWidget.setLayout(layout)
            spinWidgets.append(spinWidget)
            fixedHeight = 100
            spinWidget.setMinimumHeight(fixedHeight)
            spinWidget.setMaximumHeight(fixedHeight)

            self._uiSpinnersWidgets[cameraControlIndex] = spinWidget
            self._uiSpinners[cameraControlIndex] = dial

            dial.ValueChanged.connect(partial(self._handleSpinner, dial, cameraControlIndex))
            dialButton.pressed.connect(partial(self._handleDialButton, cameraControlIndex))
            keyButton.pressed.connect(partial(self._handleKeyButton, cameraControlIndex))

        # Set Properties
        fixedHeight = 100
        lensWidget.setMinimumHeight(fixedHeight)
        lensWidget.setMaximumHeight(fixedHeight)
        lensWidget.setMinimumWidth(180)
        self._focusSlider.setMaximum(5000.0)
        self._focusSlider.setMinimum(0.0)
        self._focusSlider.setOrientation(QtCore.Qt.Horizontal)
        self._focusSlider.setStyleSheet("""
        QSlider::groove:horizontal {
            border: 1px solid #999999;
            height: 8px; /* the groove expands to the size of the slider by default. by giving it a height, it has a fixed size */
            background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4);
            margin: 2px 0;
        }

        QSlider::handle:horizontal {
            background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #b4b4b4, stop:1 #8f8f8f);
            border: 1px solid #5c5c5c;
            width: 18px;
            margin: -2px 0; /* handle is placed by default on the contents rect of the groove. Expand outside the groove */
            border-radius: 3px;
        }
        """)
        self._focusSpinner.setMaximum(5000.0)
        self._focusSpinner.setMinimum(0.0)
        self._focusSpinner.setSingleStep(2)
        focusButton.setMaximumWidth(100)
        self._focusSpinner.setMaximumWidth(100)

        # Assign Layouts
        lensLayout.addWidget(lensLabel, 0, 0)
        lensLayout.addWidget(self._lensFinder, 1, 0)

        focusLayout.addWidget(self._focusSpinner)
        focusLayout.addWidget(self._focusSlider)
        focusLayout.addWidget(focusButton)

        colIdx = 0
        mainLayout.addWidget(lensWidget, 0, colIdx)
        for spin in spinWidgets:
            colIdx += 1
            mainLayout.addWidget(spin, 0, colIdx)
        mainLayout.addLayout(focusLayout, 1, 0, 1, colIdx + 1)

        # Set Layouts
        lensWidget.setLayout(lensLayout)
        self.setLayout(mainLayout)

        # Connect Signals
        self._lensFinder.ValueChanged.connect(self._handleLensFinderValueChannged)
        lensLabel.pressed.connect(self._handleLensLabelPressed)
        focusButton.pressed.connect(self._handleFocusButtonPressed)
        self._focusSpinner.valueChanged.connect(self._handleFocusSpinnerChange)
        self._focusSlider.valueChanged.connect(self._handleFocusSliderChange)

        self.setEnabled(False)

        self._zoomWidget = self._uiSpinnersWidgets[const.CameraControlPropertiesIndex.zoom]
        self._zoomDial = self._uiSpinners[const.CameraControlPropertiesIndex.zoom]

        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed))


    def _handleLensFinderValueChannged(self, newValue):
        self._handleLensChange(const.CameraControlPropertiesIndex.lens, newValue)

    def _handleFocusButtonPressed(self):
        self._handleDialButton(const.CameraControlPropertiesIndex.focus)

    def _handleLensLabelPressed(self):
        self._handleKeyButton(const.CameraControlPropertiesIndex.lens)

    @noRecursive.noRecursive
    def _handleFocusSpinnerChange(self, newValue):
        """
        Internal Method

        Handles the focus spinner value changing so the slider and Mobu can be updated

        args:
            newValue (float): The focus Value
        """
        if self._focusSpinnerSliderUpdate is True:
            return
        self._focusSpinnerSliderUpdate = True
        self._focusSlider.setValue(newValue)
        self._focusSpinnerSliderUpdate = False

        self._handleFocusChange(newValue)

    @noRecursive.noRecursive
    def _handleFocusChange(self, newValue):
        """
        Internal Method

        Updates the Mobu Camera's focus to the new value

        args:
            newValue (float): The focus Value
        """
        if self._activeCamera is None:
            return
        cameaAttra = self._activeCamera.PropertyList.Find(const.CameraControlPropertiesIndex.focus)
        cameaAttra.Data = newValue
        self.CameraDataChanged.emit(self._activeCamera, const.CameraControlPropertiesIndex.focus, newValue)

    @noRecursive.noRecursive
    def _handleFocusSliderChange(self, newValue):
        """
        Internal Method

        Handles the focus slider value changing so the spinner and Mobu can be updated

        args:
            newValue (float): The focus Value
        """
        if self._focusSpinnerSliderUpdate is True:
            return
        self._focusSpinnerSliderUpdate = True
        self._focusSpinner.setValue(newValue)
        self._focusSpinnerSliderUpdate = False
        self._handleFocusChange(newValue)

    @noRecursive.noRecursive
    def _handleLensChange(self, prop, value):
        """
        Internal Method

        Handle the lens changing, update the property in the MoBu Camera

        Prop (str) The property name to edit
        value (str) The new Value to set
        """
        cameaAttra = self._activeCamera.PropertyList.Find(prop)
        cameaAttra.Data = cameaAttra.GetEnumStringList().IndexOf(str(value))
        self.CameraDataChanged.emit(self._activeCamera, prop, str(value))

        self._zoomWidget.setEnabled("zoom" in value.lower())
        if self._zoomWidget.isEnabled() is False:
            self._zoomDial.CurrentValue = const.fovDefaultValue

    @noRecursive.noRecursive
    def _handleSpinner(self, ui, prop, value):
        """
        Internal Method

        Handle a spinner value change, update the property in the MoBu Camera

        Prop (str) The property name to edit
        value (float) The new value to set
        """
        cameraAttra = self._activeCamera.PropertyList.Find(prop)
        if type(cameraAttra.Data) is int:
            value = int(value)

        cameraAttra.Data = value
        self.CameraDataChanged.emit(self._activeCamera, prop, value)
        ui.CurrentValue = cameraAttra.Data

    def _handleKeyButton(self, prop):
        """
        Internal Method

        Key the selected FCurve property for the active camera
        """
        if self._activeCamera is None:
            return

        fProp = self._activeCamera.PropertyList.Find(prop)
        if (fProp):
            fProp.Key()

    def _handleDialButton(self, prop):
        """
        Internal Method

        Select the FCurve property for the active camera
        """
        if self._activeCamera is None:
            return
        # deselect all the other properties
        for propAll in self._activeCamera.PropertyList:
            if propAll.IsAnimatable():
                propAll.SetFocus(False)

        fProp = self._activeCamera.PropertyList.Find(prop)
        if (fProp):
            # Need to use this method to actually select the property in MotionBuilder
            fProp.SetFocus(True)

    def ActiveCamera(self):
        """
        Gets the current active camera on the widget
        
        return:
            FBCamera Object
        """
        return self._activeCamera

    @noRecursive.noRecursive
    def SetActiveCamera(self, camera):
        """
        Set the active camera to the given camera.
        This will populate the UI controls

        Takes a FBCamera Object
        """
        self._propertyMonitor.Clear()
        self._propertyMonitor.Deactivate()

        lens = camera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)

        if lens is None:
            self.setEnabled(False)
            self._activeCamera = None
            return
        self._activeCamera = camera
        self.setEnabled(True)

        fProp = camera.PropertyList.Find(const.CameraControlPropertiesIndex.zoom)
        foucsProp = camera.PropertyList.Find(const.CameraControlPropertiesIndex.focus)
        self._zoomDial.CurrentValue = fProp.Data
        self._propertyMonitor.Add(fProp, self._handleMobuSpinnerPropertyUpdate, extraArgs={"uiElement": self._zoomDial, "prop": const.CameraControlPropertiesIndex.zoom})

        self._propertyMonitor.Add(lens, self._handleMobuLensPropertyUpdate, extraArgs={"uiElement": self._lensFinder, "prop": const.CameraControlPropertiesIndex.lens})
        self._propertyMonitor.Add(foucsProp, self._handleMobuFocusPropertyUpdate, extraArgs={"uiElement": self._focusSpinner, "prop": const.CameraControlPropertiesIndex.focus})
        self._lensFinder.CurrentLens = lens.AsString()
        self._focusSlider.setValue(foucsProp.Data)

        # activate callback
        self._propertyMonitor.Activate()

    @noRecursive.noRecursive
    def _handleMobuFocusPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        if uiElement is None:
            return
        uiElement.setValue(targetProperty.Data)
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.Data)

    @noRecursive.noRecursive
    def _handleMobuSpinnerPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        if uiElement is None:
            return
        uiElement.CurrentValue = targetProperty.Data
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.Data)

    @noRecursive.noRecursive
    def _handleMobuLensPropertyUpdate(self, targetProperty, previousValue, uiElement=None, prop=None):
        if uiElement is None:
            return

        self._lensFinder.CurrentLens = targetProperty.AsString()
        self.CameraDataChanged.emit(self._activeCamera, prop, targetProperty.AsString())

if __name__ == "__main__":
    win = CameraControlsWidget()
    win.show()
