__DoNotReload__ = True  # Module is NOT safe to be dynamciy reloaded

from PySide import QtCore


class BaseModelItem(QtCore.QAbstractItemModel):
    """
    Text Item to display text in columns for the model
    MOVE THIS TO GENERIC PLACE
    """
    def __init__(self, parent=None, autoAppend=False):
        super(BaseModelItem, self).__init__(parent=parent)
        self.parentItem = parent
        self.childItems = []
        if autoAppend and parent is not None:
            parent.appendChild(self)

    def _listToDict(self, inputList):
        resultDict = {}
        for idx in xrange(len(inputList)):
            resultDict[idx] = inputList[idx]

        return resultDict

    def clearChildren(self):
        self.childItems = []

    def removeChild(self, item):
        self.childItems.remove(item)
        item.parentItem = None

    def appendChild(self, item, index=-1):
        if index < 0:
            self.childItems.append(item)
        else:
            self.childItems.insert(index, item)
        item.parentItem = self

    def moveChild(self, item, index):
        """
        Moves a child to the given position index

        Arguments:
            item (BaseModelItem): child to move
            index (index): position to move child to

        """
        currentIndex = self.childItems.index(item)
        self.childItems.pop(currentIndex)
        self.childItems.insert(index - (index > currentIndex), item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self, index=QtCore.QModelIndex()):
        return len(self.childItems)

    def columnCount(self, parent=None):
        return 0

    def flags(self, index):
        if not index.isValid():
            # These flags allow items to be dropped unto the the top level of a view
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDropEnabled
        childItem = index.internalPointer()
        if childItem is not self:
            if childItem is not None:
                return childItem.flags(index)
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled

    def data(self, index, role=QtCore.Qt.DisplayRole):
        raise NotImplementedError("data should be redefined in inherited classes")

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        raise NotImplementedError("setData should be redefined in inherited classes")

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def rowCount(self, parent=QtCore.QModelIndex()):
        return self.childCount()

    def parent(self, index=None):
        if index is not None:
            return self.createIndex(self.parentItem.row(), 0, self.parentItem)
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0

    def supportedDragActions(self):
        """ Flags for supported drag actions """
        return super(BaseModelItem, self).supportedDragActions()

    def supportedDropActions(self):
        """ Flags for supported drop actions """
        return super(BaseModelItem, self).supportedDropActions()
