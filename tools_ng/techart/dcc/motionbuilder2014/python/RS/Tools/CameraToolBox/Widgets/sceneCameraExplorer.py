# Widget to show and interact with camera's in the current scene
import os
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.Models import cameraModel
from RS.Tools.CameraToolBox import const
from RS import Config
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive


class SceneCameraExplorer(QtGui.QWidget):
    """
    Widget to show and allow interaction with Motion Builder scene camera's

    One Signal:
        CameraSelected - which emits when a camera is selected, emiting the FBCamera object in the model
    """
    CameraSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(SceneCameraExplorer, self).__init__(parent=parent)
        self.SetupUi()
        self._selectionBlocker = False

    def SetupUi(self):
        """
        Internal Method

        Setup up the Widgets UI and internals such as the model and the icons
        """
        layout = QtGui.QVBoxLayout()
        hideNonSwitcher = QtGui.QCheckBox("Hide Non-Switcher Cameras")
        self._mainModel = cameraModel.SceneCameraModel()
        self._proxyModel = cameraModel.SceneCameraProxyFilterModel()
        self._proxyModel.setSourceModel(self._mainModel)
        refreshIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "refresh.png")))
        refreshButton = QtGui.QPushButton(refreshIcon, "Refresh")

        self._treeView = QtGui.QTreeView()
        self._treeView.setModel(self._proxyModel)
        self._treeView.header().resizeSection(1, 50)
        self._treeView.setAlternatingRowColors(True)

        self._checkbox = QtGui.QCheckBox("Auto-Select Current Camera Switcher Camera")

        layout.addWidget(refreshButton)
        layout.addWidget(hideNonSwitcher)
        layout.addWidget(self._treeView)
        layout.addWidget(self._checkbox)

        self.setLayout(layout)
        hideNonSwitcher.stateChanged.connect(self._handleHideNonSwitcher)
        self._treeView.clicked.connect(self._handleSelectionChange)
        refreshButton.pressed.connect(self._handleRefresh)

        hideNonSwitcher.setChecked(True)

    def _handleRefresh(self):
        """
        Internal Method

        Handle the refresh button being clicked, and refresh the model
        """
        self.Refresh()

    def _handleHideNonSwitcher(self, newVal):
        """
        Internal Method

        Handle the hide non switcher checkbox value change
        """
        self._proxyModel.HideNonSwitcherCameras(newVal)

    @noRecursive.noRecursive
    def _handleSelectionChange(self, index):
        """
        Internal Method

        Handle the camera selection change, and emit the new camera
        """
        if self._selectionBlocker is True:
            self._selectionBlocker = False
            return
        selectedCamera = self.GetSelectedCamera()
        if index.column() == 2:
            for plane in selectedCamera.Children:
                plane.Show = not plane.Show
            self.UpdateCameraData()
            return
        self.CameraSelected.emit(selectedCamera)

    def UpdateCameraData(self):
        """
        Updates the camera data, does not refersh the current camera list
        """
        self._mainModel.UpdateCameraData()

    def Refresh(self):
        """
        Refresh the model to pick up any scene changes
        """
        # Needs to be run twice as changes are not picked up first time.
        self._mainModel.reset()

    def GetSelectedCamera(self):
        """
        Get the currently selected camera object (FBCamera)
        """
        selected = self._treeView.selectedIndexes()
        # Only one can be returned, make sure that only one thing is selected
        if len(selected) == 0:
            return None
        index = selected[0]
        return self._proxyModel.data(index, const.MobuCameraModelIndex.GetMobuObject)

    @noRecursive.noRecursive
    def SelectCamera(self, camera):
        """
        Select the given camera in the list of cameras.
        This will emit a CameraSelected signal.

        The input camera must be from an FBCamera class
        """
        items = self._mainModel.match(self._mainModel.index(0, 0), const.MobuCameraModelIndex.Name, camera.Name, -1, QtCore.Qt.MatchRecursive)
        if len(items) > 0:
            self._selectionBlocker = True
            self._treeView.setCurrentIndex(self._proxyModel.mapFromSource(items[0]))

    @property
    def CameraSwitcherCheckBoxChecked(self):
        """ 
        Returns the camera switcher checkbox 
        """
        return self._checkbox.toggled

if __name__ == "__main__":
    import sys
    win = SceneCameraExplorer()
    win.show()
