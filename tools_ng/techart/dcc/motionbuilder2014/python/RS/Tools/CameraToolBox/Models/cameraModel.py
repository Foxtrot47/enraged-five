"""
Models to deal with MotionBuilder Cameras, and the proxy models that are used with them
"""
import os

from pyfbsdk import FBSystem
from PySide import QtCore, QtGui
import unbind

from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModelItem, baseModel
import RS.Globals as glo
from RS.Core.Camera import Lib as camLib
from RS.Tools.CameraToolBox import const
from RS import Config


def CheckForRdrLensData(camFovFloat):
    rdrLensFloats = [73.7397952917, 51.2820116486, 37.8492888321, 26.9914665616, 18.1805538416, 11.421186275]
    for rdrLensFloat in rdrLensFloats:
        if abs(camFovFloat - rdrLensFloat) < 0.01:
            return True
    return False

class MobuCameraModel(baseModelItem.BaseModelItem):
    """
    Base Item Model Type

    This will store the actual camera object, and allow the model to ask questions like
        * Name
        * is currently Selected
        * is a system camera
        * is in the camera switcher
    """

    lockedIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "locked.png")))
    unlockedIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "unlocked.png")))
    activeIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "activeCamera.png")))
    showPlatesIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "plane_showselected.png")))
    hidePlatesIcon = QtGui.QIcon(os.path.realpath(os.path.join(Config.Script.Path.ToolImages, "camera", "icons", "plane_hideselected.png")))

    def __init__(self, camera, parent=None):
        """
        Constructor

        Takes the FBCamera Object to construct the item around it.
        """
        super(MobuCameraModel, self).__init__(parent=parent)
        self._camera = camera
        self._rdrLensUpdateNeeded = CheckForRdrLensData(self._camera.FieldOfView)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        column = index.column()
        try:
            camName = self._camera.LongName

            if role == QtCore.Qt.DisplayRole or role == const.MobuCameraModelIndex.Name:
                if column == 0:
                    return camName
                elif column == 2:
                    return self.getLensInfo()
                elif column == 3:
                    strUpdate = ""
                    val = camLib.camUpdate.camFunctions.GetRequiresUpdate(self._camera)
                    if val:
                        strUpdate = "invalid: %s" % val
                    return strUpdate

            elif role == const.MobuCameraModelIndex.IsSystemCamera:
                return self.isSystemCamera()
            elif role == const.MobuCameraModelIndex.IsSelected:
                return self.isSelected()
            elif role == const.MobuCameraModelIndex.IsInSwitcher:
                return self.isInSwitcher()
            elif role == const.MobuCameraModelIndex.GetMobuObject:
                return self._camera
            elif role == QtCore.Qt.TextAlignmentRole:
                if column == 1:
                    return QtCore.Qt.AlignCenter
            elif role == QtCore.Qt.DecorationRole:
                if column == 0:
                    if self.isCameraLocked():
                        return self.lockedIcon
                    else:
                        return self.unlockedIcon
                elif column == 1:
                    if self.isSelected():
                        return self.activeIcon
                elif column == 3:
                    if self.isPlatesShowing():
                        return self.showPlatesIcon
                    else:
                        return self.hidePlatesIcon
            elif role == QtCore.Qt.BackgroundRole:
                if self._rdrLensUpdateNeeded:
                    return QtGui.QBrush(QtCore.Qt.darkMagenta)
        # Check for unbind Error (Camera might have been deleted)
        except unbind.UnboundWrapperError:
            if role == QtCore.Qt.DisplayRole or role == const.MobuCameraModelIndex.Name:
                return "<DELETED CAMERA NODE>"
            elif role == const.MobuCameraModelIndex.IsSystemCamera:
                return True
            elif role == const.MobuCameraModelIndex.IsSelected:
                return False
            elif role == const.MobuCameraModelIndex.IsInSwitcher:
                return False
            elif role == const.MobuCameraModelIndex.GetMobuObject:
                return None

    def isPlatesShowing(self):
        """
        Is the camera plate showing

        Return bool
        """
        return any([plane.Show for plane in self._camera.Children])

    def isCameraLocked(self):
        """
        is the camera locked

        Returns bool
        """
        lockCameraProp = self._camera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)
        if lockCameraProp is None:
            return False
        return lockCameraProp.IsLocked()

    def getLensInfo(self):
        """
        Get the lens info

        Returns String
        """
        lensInfo = self._camera.PropertyList.Find(const.CameraControlPropertiesIndex.lens)
        if lensInfo is None:
            return "No Lens Info"
        return lensInfo.AsString()

    def isSystemCamera(self):
        """
        is the camera in a system camera

        Returns bool
        """
        return self._camera.SystemCamera

    def isSelected(self):
        """
        is the camera currently selected

        Returns bool
        """
        return self._camera.Selected

    def isInSwitcher(self):
        """
        is the camera in the camera switcher

        Returns bool
        """
        return self._camera in camLib.camUpdate.camFunctions.SwitcherCameras

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        """
        ReImplemented from Qt
        """
        return False


class SceneCameraModel(baseModel.BaseModel):
    """
    Base Model Type

    Main model for the cameras in the scene
    """
    def __init__(self, parent=None):
        super(SceneCameraModel, self).__init__(parent=parent)

    def getHeadings(self):
        """
        Model does not have any headings that mean anything
        """
        return ["Name", "Active", "Lens", "Planes", "Needs Update"]

    def columnCount(self, parent=QtCore.QModelIndex()):
        """
        Only has one column, for the name of the cameras
        """
        return len(self.getHeadings())

    def UpdateCameraData(self, camera=None):
        """
        Update the given camera, all if one is not given, all cameras
        This will not refresh the list of cameras, just the current list's data
        """
        self.dataChanged.emit(self.createIndex(0, 0), self.createIndex(self.rootItem.childCount(), self.columnCount()))

    def setupModelData(self, parent):
        """
        Set up the model, and find all the camera's in the scene and add them to
        the parentObject

        Takes in a (baseModelItem.BaseModelItem) object as the root Item, which
        the other items are parented to
        """
        FBSystem().CurrentTake.SetCurrentLayer(0)
        parent.clearChildren()

        camLib.camUpdate.camFunctions.UpdateActiveSwitcherCameras()
        for cam in glo.gCameras:
            parent.appendChild(MobuCameraModel(cam, parent=parent))


class SceneCameraProxyFilterModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model to hide the system cameras and the non-switcher cameras
    """
    def __init__(self, parent=None):
        super(SceneCameraProxyFilterModel, self).__init__(parent=parent)
        self._hideSystemCameras = True
        self._hideNonSwitcher = False

    def filterAcceptsRow(self, row, parent=None):
        """
        Interal Method

        Main logic if a row is shown or hidden based off the properties in
        the SceneCameraProxyFilterModel.
        """
        # Hide System
        if self.sourceModel is None:
            return False

        index = self.sourceModel().index(row, 0, parent)

        if self._hideSystemCameras:
            if self.sourceModel().data(index, const.MobuCameraModelIndex.IsSystemCamera):
                return False

        if self._hideNonSwitcher:
            if not self.sourceModel().data(index, const.MobuCameraModelIndex.IsInSwitcher):
                return False
        return True

    def HideNonSwitcherCameras(self, val):
        """
        Set if the Non-Switcher cameras are shown or hidden
        """
        self._hideNonSwitcher = val
        self.invalidate()

    def HideSystemCameras(self, val):
        """
        Set if the System cameras are shown or hidden
        """
        self._hideSystemCameras = val
        self.invalidate()


if __name__ == "__main__":
    mainModel = SceneCameraModel()
    proxyModel = SceneCameraProxyFilterModel()
    proxyModel.setSourceModel(mainModel)
    win = QtGui.QTreeView()
    win.setModel(proxyModel)
    win.show()

