# The Main Camera Tool Widget, which is used by the main dialog and puts and hooks the different widgets together
from PySide import QtCore, QtGui

from RS.Tools.CameraToolBox.Widgets import cameraControlsWidget, shakeControlsWidget, sceneCameraExplorer, zoomControlsWidget
from RS.Tools.CameraToolBox import const
from RS.Tools.CameraToolBox.PyCore.Decorators import noRecursive


class CameraToolWidget(QtGui.QWidget):
    """
    The Main Camera Tool Widget, which is used by the main dialog and puts and
    hooks the different widgets together

    One Signal:
        CameraSelected - Which emits if a camera is selected, and the signal will have the FBCamera object
    """

    CameraSelected = QtCore.Signal(object)

    def __init__(self, parent=None):
        """
        Constructor
        """
        super(CameraToolWidget, self).__init__(parent=parent)
        self.SetupUi()

    def _SetupTabUi(self):
        """
        Internal Method

        Set up the Tab UI portion of the UI. Returns a QWidget
        """
        result = QtGui.QWidget()
        layout = QtGui.QHBoxLayout()
        cameraControls = QtGui.QWidget()

        self._zoomControls = zoomControlsWidget.ZoomControlsWidget()

        self._tabWidget = QtGui.QTabWidget()
        self._tabWidget.setTabPosition(QtGui.QTabWidget.TabPosition.West)

        self._dofControls = cameraControlsWidget.CameraControlsWidget()
        self._shakeControls = shakeControlsWidget.ShakeControlsWidget()

        self._tabWidget.addTab(self._dofControls, 'DOF Controls')
        self._tabWidget.addTab(self._shakeControls, 'Shake It!')

        layout.addWidget(self._zoomControls)
        layout.addWidget(self._tabWidget)

        result.setLayout(layout)

        self._zoomControls.CameraDataChanged.connect(self._handleCameraDataChanged)
        self._dofControls.CameraDataChanged.connect(self._handleCameraDataChanged)

        return result

    def _SetupCameraUi(self):
        """
        Internal Method

        Set up the Camera portion of the UI. Returns a QWidget
        """
        result = QtGui.QWidget()
        layout = QtGui.QVBoxLayout()
        self._cameraList = sceneCameraExplorer.SceneCameraExplorer()
        layout.addWidget(self._cameraList)
        result.setLayout(layout)
        self._cameraList.CameraSelected.connect(self._handleCameraSelected)

        return result

    def SetupUi(self):
        """
        Internal Method

        Set up all the UI for the widget
        """
        layout = QtGui.QVBoxLayout()

        self._splitter = QtGui.QSplitter()

        self._splitter.addWidget(self._SetupCameraUi())
        self._splitter.addWidget(self._SetupTabUi())

        layout.addWidget(self._splitter)

        self.setLayout(layout)

    @noRecursive.noRecursive
    def RefreshCameraList(self):
        """
        Force a refresh of the camera's in the camera explorers list of cameras
        """
        self._cameraList.Refresh()

    def GetSelectedCamera(self):
        """
        Get the currently selected camera, returns as None if nothing is selected
        or an FBCamera object
        """
        return self._cameraList.GetSelectedCamera()

    @noRecursive.noRecursive
    def SelectCamera(self, cameraToSelect):
        """
        Select the input camera from the list of cameras, and update the UI accordingly
        Takes a FBCamera Object
        """
        self._dofControls.SetActiveCamera(cameraToSelect)
        self._shakeControls.SetActiveCamera(cameraToSelect)
        self._cameraList.SelectCamera(cameraToSelect)
        self._zoomControls.SetActiveCamera(cameraToSelect)

    @noRecursive.noRecursive
    def _handleCameraDataChanged(self, camera, prop, value):
        self._cameraList.UpdateCameraData()

    @noRecursive.noRecursive
    def _handleCameraSelected(self, camObject):
        """
        Internal Method

        Handle a camera being selected, and updates the various UI elements accordingly
        """
        self._dofControls.SetActiveCamera(camObject)
        self._zoomControls.SetActiveCamera(camObject)
        self.CameraSelected.emit(camObject)


if __name__ == "__main__":
    win = CameraToolWidget()
    win.show()
