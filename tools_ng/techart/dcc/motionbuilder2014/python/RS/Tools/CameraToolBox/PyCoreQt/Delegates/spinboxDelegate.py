from PySide import QtCore, QtGui


class SpinBoxDelegate(QtGui.QItemDelegate):
    def createEditor(self, parent, option, index):
        '''
        Creates Qspinbox and sets the min and max values (int)
        '''        
        editor = QtGui.QSpinBox(parent)
        editor.setMinimum(1)
        editor.setMaximum(200)
        return editor

    def setEditorData(self, spinBox, index):
        '''
        Reads the value from the Model Data and sets the current value (int) of the spinbox
        
        Model Data -> ComboBox
        '''
        value = index.model().data(index, QtCore.Qt.EditRole)
        spinBox.setValue(value)

    def setModelData(self, spinBox, model, index):
        '''
        Reads the current value (int) of the spinbox and pushes that information to the Model Data
        
        ComboBox -> Model SetData
        '''
        spinBox.interpretText()
        value = spinBox.value()

        model.setData(index, value, QtCore.Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)