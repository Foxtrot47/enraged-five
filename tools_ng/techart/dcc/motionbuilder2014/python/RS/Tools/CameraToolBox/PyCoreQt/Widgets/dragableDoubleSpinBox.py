"""
UI Control to allow users to click and drag to get a numerical value
"""
import math
from PySide import QtGui


class DragableDoubleSpinBox(QtGui.QDoubleSpinBox):
    """
    Float spin box control
    """
    def __init__(self, parent=None):
        self._eventHandlerDict  = {
                         QtGui.QMouseEvent.MouseButtonPress: self.mousePressEvent,
                         QtGui.QMouseEvent.MouseButtonRelease: self.mouseReleaseEvent,
                         QtGui.QMouseEvent.MouseMove: self.mouseMoveEvent,
        }
        
        super(DragableDoubleSpinBox, self).__init__(parent=parent)
        lineEdit = self.findChild(QtGui.QLineEdit, "qt_spinbox_lineedit")
        lineEdit.installEventFilter(self)
        
        self.setMouseTracking(True)
        self._isDown = False
        self._lastPos = None
        self._initalPos = None

    def eventFilter(self, target, event):
        for eventType, eventMethod in self._eventHandlerDict.iteritems():
            if event.type() == eventType:
                eventMethod(event)
        return False
    
    def mousePressEvent(self, event):
        self._isDown = True
        self._lastPos = event.pos()
        self._initalPos = event.pos()
        super(DragableDoubleSpinBox, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self._isDown = False
        self._lastPos = None
        self._initalPos = None
        super(DragableDoubleSpinBox, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self._isDown is False:
            return
        currentPoint = event.pos()
        delta = math.sqrt(
                     math.pow((self._lastPos.x() - currentPoint.x()), 2) +
                     math.pow((self._lastPos.y() - currentPoint.y()), 2)
                    )
        if self._initalPos.y() < currentPoint.y():
            delta *= -1
        self.setValue(self.value() + delta)
        self._lastPos = currentPoint


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    
    spinner = DragableDoubleSpinBox()
    spinner.show()
    sys.exit(app.exec_())