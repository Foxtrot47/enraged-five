from PySide import QtGui


class HBoxWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(HBoxWidget, self).__init__(parent=parent)
        self._layout = QtGui.QHBoxLayout()
        self.setLayout(self._layout)
        
    def addWidget(self, widget):
        self._layout.addWidget(widget)
