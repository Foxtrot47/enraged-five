from PySide import QtCore
from RS.Tools.CameraToolBox.PyCore import singleton


class PySideSingleton(type(QtCore.QObject), singleton.Singleton):
    """
    A metaclass for creating singletons which inherit from PySide classes
    (avoids metaclass conflict).
    """
    pass
