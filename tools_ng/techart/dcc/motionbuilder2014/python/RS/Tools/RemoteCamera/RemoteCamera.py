__author__ = 'mharrison-ball'


__DoNotReload__ = True # Module is NOT safe to be dynamciy reloaded

import RS
from PySide import QtGui, QtCore
from RS.Tools.UI import QtBannerWindowBase, QPrint
from RS.Tools.RemoteCamera.Models import  RemoteModel
import os


reload(RemoteModel)
con = RemoteModel.RemoteConnectionModel()



class MainWindow(QtBannerWindowBase):
    def __init__(self):
        '''
        '''
        super(MainWindow, self).__init__()
        self.resize(0, 0)
        self.Layout.addWidget(SetupWidget())


        ''''''

    def closeEvent(self, event):
        '''

        :param event:
        :return:
        '''
        con.Disconnect()



class SetupWidget(QtGui.QWidget):
    def __init__(self, parent=None, f=0):
        '''

        :param parent:
        :param f:
        :return:

        '''
        super(SetupWidget, self).__init__(parent=parent, f=f)
        layout = QtGui.QVBoxLayout()

        # ICONS
        self.iconMouse = QtGui.QIcon(os.path.realpath(os.path.join(RS.Config.Script.Path.ToolImages, "Remote", "camera_Mouse.png")))
        self.iconController = QtGui.QIcon(os.path.realpath(os.path.join(RS.Config.Script.Path.ToolImages, "Remote", "camera_Controller.png")))

        # Toggle Connect Button
        self.connectButton = QtGui.QPushButton('Connect')
        self.connectButton.setStyleSheet("QPushButton {  font-size: 12px;  }")
        self.connectButton.setCheckable(True)
        self.connectButton.setFixedHeight(20)
        self.connectButton.clicked.connect(self._eventConnect)
        self.connectButton.setToolTip(QtGui.QApplication.translate("Form", "Connect to the Game\n"
                                                                           "Once connected you can select cameras in motionbuilder\n"
                                                                           "to update in the game and vice versa unless your Joe then\n"
                                                                           "everything will be broken anyway", None, QtGui.QApplication.UnicodeUTF8))
        layout.addWidget(self.connectButton, 0, 0)

        groupBox = QtGui.QGroupBox("Cutscene Only")
        vbox = QtGui.QVBoxLayout()
        # Play Cutscene
        self.playCutsceneButton = QtGui.QPushButton('Sync to Cutscene')
        self.playCutsceneButton.setToolTip(QtGui.QApplication.translate("Form", "This will play the cutscene and sync the camera\n"
                                                                                "and playabck with moitonbuilder timeline", None, QtGui.QApplication.UnicodeUTF8))
        self.playCutsceneButton.setFixedHeight(20)
        self.playCutsceneButton.clicked.connect(self._eventPlayCutscene)

        # TRack Camera Switcher
        self.trackSwitcher = QtGui.QCheckBox("Track Camera Switcher")
        self.trackSwitcher.setToolTip(QtGui.QApplication.translate("Form", "This will use the current active camera switcher to detmeine\n"
                                                                           "which cameras to show in game", None, QtGui.QApplication.UnicodeUTF8))
        self.trackSwitcher.setFixedHeight(20)
        self.trackSwitcher.setChecked(True)
        # print dir(self.trackSwitcher)
        self.trackSwitcher.stateChanged.connect(self._eventUseCameraSwitcher)

        # ENable FUll Range Tracking
        self.fullRange = QtGui.QCheckBox("Full Range Support")
        self.fullRange.setToolTip(QtGui.QApplication.translate("Form", "This will use the Full Cutscene Range\n"
                                                                           "to playback the scene", None, QtGui.QApplication.UnicodeUTF8))
        self.fullRange.setFixedHeight(20)
        self.fullRange.setChecked(False)
        # print dir(self.trackSwitcher)
        self.fullRange.stateChanged.connect(self._eventfullRange)

        # Load from RPF
        self.rpfLoad = QtGui.QCheckBox("Load RPF instead of from Disk")
        self.rpfLoad.setToolTip(QtGui.QApplication.translate("Form", "This will Load the RPF cutscenes e\n"
                                                                           "and not the disk version", None, QtGui.QApplication.UnicodeUTF8))
        self.rpfLoad.setFixedHeight(20)
        self.rpfLoad.setChecked(False)
        self.rpfLoad.stateChanged.connect(self._eventLoadFromRPF)









        vbox.addWidget(self.playCutsceneButton)
        vbox.addWidget(self.trackSwitcher)
        vbox.addWidget(self.fullRange)
        vbox.addWidget(self.rpfLoad)

        groupBox.setLayout(vbox)
        layout.addWidget(groupBox)


        # Create our offset Spinners
        self.spinOffsetX = QtGui.QDoubleSpinBox()
        self.spinOffsetX.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        # self.spinOffsetX.setStyleSheet("QDoubleSpinBox { color: rgb(50, 50, 50); font-size: 11px; background-color: rgba(255, 188, 20, 50); }")
        self.spinOffsetX.setMaximum(10000)
        self.spinOffsetX.setMinimum(-10000)
        self.spinOffsetX.setFixedWidth(50)
        self.spinOffsetX.setDecimals(5)
        self.spinOffsetX.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetY = QtGui.QDoubleSpinBox()
        self.spinOffsetY.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        self.spinOffsetY.setMaximum(10000)
        self.spinOffsetY.setMinimum(-10000)
        self.spinOffsetY.setFixedWidth(50)
        self.spinOffsetY.setDecimals(5)
        self.spinOffsetY.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetZ = QtGui.QDoubleSpinBox()
        self.spinOffsetZ.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        self.spinOffsetZ.setMaximum(10000)
        self.spinOffsetZ.setMinimum(-10000)
        self.spinOffsetZ.setFixedWidth(50)
        self.spinOffsetZ.setDecimals(5)
        self.spinOffsetZ.valueChanged.connect(self._eventOffsetChanged)



        groupBox = QtGui.QGroupBox("Read Offsets")
        # self.groupBox.setStyleSheet("QLabel { color: rgb(50, 50, 50); font-size: 11px; background-color: rgba(188, 188, 188, 50); border: 1px solid rgba(188, 188, 188, 250); }")

        self.playAnimSceneeButton = QtGui.QPushButton('Sync to Anim Scene Offsets')
        self.playAnimSceneeButton.setFixedHeight(20)
        self.playAnimSceneeButton.clicked.connect(self._eventPlayAnimScene)
        self.playAnimSceneeButton.setToolTip(QtGui.QApplication.translate("Form", "This will read the current Anim scene that is playing offset", None, QtGui.QApplication.UnicodeUTF8))



        self.synsGameButton = QtGui.QPushButton('Sync to Player Offsets')
        self.synsGameButton.setFixedHeight(20)
        self.synsGameButton.clicked.connect(self._eventSyncGameOffsets)
        self.synsGameButton.setToolTip(QtGui.QApplication.translate("Form", "This will read the current players positions offsets", None, QtGui.QApplication.UnicodeUTF8))

        hbox = QtGui.QVBoxLayout()
        hbox.addWidget(self.playAnimSceneeButton)
        hbox.addWidget(self.synsGameButton)
        groupBox.setLayout(hbox)
        layout.addWidget(groupBox)



        groupBox = QtGui.QGroupBox("Global")
        # Create a groupbox to hold our spinners
        horizontalGroupBox = QtGui.QGroupBox('Scene Position Offset (X, Y, Z)')
        horizontalGroupBox.setAlignment(QtCore.Qt.AlignCenter)
        # Add a horizontal layout to contain our spinners
        QHBoxLayout = QtGui.QHBoxLayout()
        QHBoxLayout.addWidget(self.spinOffsetX)
        QHBoxLayout.addWidget(self.spinOffsetY)
        QHBoxLayout.addWidget(self.spinOffsetZ)

        # add the horizontal layout to our groupbox

        horizontalGroupBox.setLayout(QHBoxLayout)
        # horizontalGroupBox.setLayout(QHBoxLayout2)

        # Add to our mian layour
        layout.addWidget(horizontalGroupBox)

        horizontalGroupBox = QtGui.QGroupBox('Scene Rotation Offset (X, Y, Z)')
        horizontalGroupBox.setAlignment(QtCore.Qt.AlignCenter)
        QHBoxLayout2 = QtGui.QHBoxLayout()

        self.spinOffsetRX = QtGui.QDoubleSpinBox()
        self.spinOffsetRX.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        self.spinOffsetRX.setFixedWidth(50)
        self.spinOffsetRX.setMaximum(10000)
        self.spinOffsetRX.setMinimum(-10000)
        self.spinOffsetRX.setDecimals(5)
        self.spinOffsetRX.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetRY = QtGui.QDoubleSpinBox()
        self.spinOffsetRY.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        self.spinOffsetRY.setFixedWidth(50)
        self.spinOffsetRY.setMaximum(10000)
        self.spinOffsetRY.setMinimum(-10000)
        self.spinOffsetRY.setDecimals(5)
        self.spinOffsetRY.valueChanged.connect(self._eventOffsetChanged)

        self.spinOffsetRZ = QtGui.QDoubleSpinBox()
        self.spinOffsetRZ.setStyleSheet("QDoubleSpinBox {  font-size: 10px;  }")
        self.spinOffsetRZ.setFixedWidth(50)
        self.spinOffsetRZ.setMaximum(10000)
        self.spinOffsetRZ.setMinimum(-10000)
        self.spinOffsetRZ.setDecimals(5)
        self.spinOffsetRZ.valueChanged.connect(self._eventOffsetChanged)

        QHBoxLayout2.addWidget(self.spinOffsetRX)
        QHBoxLayout2.addWidget(self.spinOffsetRY)
        QHBoxLayout2.addWidget(self.spinOffsetRZ)
        horizontalGroupBox.setLayout(QHBoxLayout2)
        layout.addWidget(horizontalGroupBox)

        # Label
        label = QtGui.QLabel("Camera Mode In Control")
        label.setAlignment(QtCore.Qt.AlignCenter)
        layout.addWidget(label)

        # Toggle Mode button
        self.modeButton = QtGui.QPushButton('Game')
        self.modeButton.setCheckable(True)
        self.modeButton.clicked.connect(self._eventModeChanged)
        self.modeButton.setIcon(self.iconController)
        self.modeButton.setIconSize(QtCore.QSize(84, 55))
        self.modeButton.setMinimumSize((QtCore.QSize(84, 55)))
        self.modeButton.setToolTip(QtGui.QApplication.translate("Form", "This will swap between motionbuilder contolling the game camera\n"
                                                                        "or the game's free camera controlling Moitonbuilder's Camera", None, QtGui.QApplication.UnicodeUTF8))
        layout.addWidget(self.modeButton, 0, 0)



        # Layout

        self.setLayout(layout)

        # Disable all our buttons
        self.modeButton.setEnabled(False)
        self.spinOffsetX.setEnabled(False)
        self.spinOffsetY.setEnabled(False)
        self.spinOffsetZ.setEnabled(False)
        self.spinOffsetRX.setEnabled(False)
        self.spinOffsetRY.setEnabled(False)
        self.spinOffsetRZ.setEnabled(False)
        self.playCutsceneButton.setEnabled(False)
        self.trackSwitcher.setEnabled(False)
        self.playAnimSceneeButton.setEnabled(False)
        self.synsGameButton.setEnabled(False)

    def setOffsets(self):
        '''

        :return:
        '''
        # read back our values into the offsets, we need to disconnec telse the valuse don;t update :(
        self.spinOffsetX.valueChanged.disconnect()
        self.spinOffsetY.valueChanged.disconnect()
        self.spinOffsetZ.valueChanged.disconnect()
        self.spinOffsetRZ.valueChanged.disconnect()


        self.spinOffsetX.setValue(con.OFFSET_X)
        self.spinOffsetY.setValue(con.OFFSET_Y)
        self.spinOffsetZ.setValue(con.OFFSET_Z)
        self.spinOffsetRZ.setValue(con.OFFSET_RZ)

        self.spinOffsetX.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetY.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetZ.valueChanged.connect(self._eventOffsetChanged)
        self.spinOffsetRZ.valueChanged.connect(self._eventOffsetChanged)

    # EVENTS
    def _eventConnect(self):
        '''

        :return:
        '''
        try:
            if self.connectButton.isChecked():
                self.connectButton.setText("Disconnect")
                self.modeButton.setEnabled(True)
                self.spinOffsetX.setEnabled(True)
                self.spinOffsetY.setEnabled(True)
                self.spinOffsetZ.setEnabled(True)
                self.spinOffsetRX.setEnabled(True)
                self.spinOffsetRY.setEnabled(True)
                self.spinOffsetRZ.setEnabled(True)
                self.playCutsceneButton.setEnabled(True)
                self.trackSwitcher.setEnabled(True)
                self.playAnimSceneeButton.setEnabled(True)
                self.synsGameButton.setEnabled(True)
                con.Connect()
            else:
                self.connectButton.setText("Connect")
                self.modeButton.setEnabled(False)
                self.spinOffsetX.setEnabled(False)
                self.spinOffsetY.setEnabled(False)
                self.spinOffsetZ.setEnabled(False)
                self.spinOffsetRX.setEnabled(False)
                self.spinOffsetRY.setEnabled(False)
                self.spinOffsetRZ.setEnabled(False)
                self.playCutsceneButton.setEnabled(False)
                self.trackSwitcher.setEnabled(False)
                self.playAnimSceneeButton.setEnabled(False)
                self.synsGameButton.setEnabled(False)
                con.Disconnect()
        except RemoteModel.MissingGameError:
            QtGui.QMessageBox.critical(None, "Game Not Found", "The Game was not found.\n"
                                                               "Boot up the game and try again.")

    def _eventUseCameraSwitcher(self):
        '''

        :return:
        '''

        con.enableSwitcher(self.trackSwitcher.isChecked())

    def _eventfullRange(self):
        '''
        Enable Full Range Syncing
        :return:
        '''
        # QPrint(self.fullRange.isChecked()) #,
        con.enableFullRange(self.fullRange.isChecked())

    def _eventLoadFromRPF(self):
        '''
        Enable Full Range Syncing
        :return:
        '''
        # QPrint(self.fullRange.isChecked()) #,
        con.enableRPFLoad(self.rpfLoad.isChecked())


    def _eventPlayAnimScene(self):
        '''

        :return:
        '''
        con.startAnimScene()
        self.setOffsets()


        self.modeButton.setChecked(True)
        self.modeButton.setText("Mobu")
        con.GAME_MODE = False

    def _eventPlayCutscene(self):
        '''

        :return:
        '''
        con.startStopCutscene()
        self.setOffsets()


        # Set to Mobu mode
        self.modeButton.setChecked(True)
        self.modeButton.setText("Mobu")
        con.GAME_MODE = False
        con.setSwitcherCamera()

    def _eventSyncGameOffsets(self):
        '''

        :return:
        '''
        con.SyncGameOffsets()
        self.setOffsets()


    def _eventModeChanged(self):
        '''

        :return:
        '''
        if self.modeButton.isChecked():
            icon = self.iconMouse
            self.modeButton.setText("Mobu")
            con.GAME_MODE = False
        else:
            icon = self.iconController
            self.modeButton.setText("Game")
            con.GAME_MODE = True

        self.modeButton.setIcon(icon)


    def _eventOffsetChanged(self):
        '''

        :return:
        '''
        con.OFFSET_X = self.spinOffsetX.value()
        con.OFFSET_Y = self.spinOffsetY.value()
        con.OFFSET_Z = self.spinOffsetZ.value()
        con.OFFSET_RX = self.spinOffsetRX.value()
        con.OFFSET_RY = self.spinOffsetRY.value()
        con.OFFSET_RZ = self.spinOffsetRZ.value()



'''
    Test code  for right click mouse increase
'''
class Wrap_Spinner(QtGui.QDoubleSpinBox):
    def __init__(self, minVal=0, maxVal=100000, default=0):
        super(Wrap_Spinner, self).__init__()
        self.drag_origin = None
        self.setAccelerated(True)
        self.setRange(minVal, maxVal)
        self.setValue(default)

    def get_is_dragging(self):
        # are we the widget that is also the active mouseGrabber?
        return self.mouseGrabber() == self

    ### Dragging Handling Methods ################################################
    def do_drag_start(self):
        # Record position
        # Grab mouse
        self.drag_origin = QtGui.QCursor().pos()
        self.grabMouse()

    def do_drag_update(self):
        # Transpose the motion into values as a delta off of the recorded click position
        curPos = QtGui.QCursor().pos()
        offsetVal = self.drag_origin.y() - curPos.y()
        self.setValue(offsetVal)


    def do_drag_end(self):
        self.releaseMouse()
        # Restore position
        # Reset drag origin value
        self.drag_origin = None

    ### Mouse Override Methods ################################################
    def mousePressEvent(self, event):
        if QtCore.Qt.LeftButton:

            self.do_drag_start()
        elif self.get_is_dragging() and QtCore.Qt.RightButton:
            # Cancel the drag
            self.do_drag_end()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)


    def mouseMoveEvent(self, event):
        if self.get_is_dragging():
            self.do_drag_update()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)


    def mouseReleaseEvent(self, event):
        if self.get_is_dragging() and QtCore.Qt.LeftButton:

            self.do_drag_end()
        else:
            super(Wrap_Spinner, self).mouseReleaseEvent(event)



def Run(show=True):
    """
    Standard method to show the UI in mobu
    """
    tool = MainWindow()
    if show:
        tool.show()
    return tool
