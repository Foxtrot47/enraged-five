import clr
import time
from xml.etree import ElementTree

from ctypes import *
from pyfbsdk import *

import RS.Config
import RS.Utils.Math as Math
import RS.Utils.MathGameUtils as MathGameUtils

"""
TODO:

Support:
* Disable Cutscene Blending for when scrubbing
* CHeck support for mutipile scenes
* Check if channel becomes falted thenn re-open new channel

"""

clr.AddReference("RSG.TechArt.Remote")
clr.AddReference("RSG.Rag.Clients")
clr.AddReference("RSG.Base")
from RSG.TechArt.Remote import Remote
from RSG.Base import Math as RageMath
from RS.Core.Export import RexExport
from RSG.Rag.Clients import RagClientFactory


class atdict(dict):
    __getattr__ = dict.__getitem__

cfgSettings = 'remote_console_settings.xml'

# Load in our config settings, this is shared between the tools Config
_configPath = "{0}\\config\\anim\\{1}".format(RS.Config.Tool.Path.Config, cfgSettings)

_cmds = dict()
root = ElementTree.parse(_configPath).getroot()
for child in root:
    for item in child.getchildren():
        _cmds[item.get('key')] = item.text

# These are values not in the config file.

_cmds['widgetCameraOverrideCam']        = 'Cut Scene Debug/Current Scene/Camera Editing/Camera Overrides'
_cmds['widgetSceneOffsetRotation']      = 'Cut Scene Debug/Current Scene/Scene Properties/Scene Heading'

_cmds['widgetSetupWorldCutscene']       = 'Cut Scene Debug/Setup world for cutscene'

_cmds['widgetSearchCutscene']           = 'Anim Scenes/Load scene/Scene name selector: /Enter search text:'



_cmds['widgetCamera']                   = 'Camera/Create camera widgets'
_cmds['widgetOverrideFOV']             = 'Camera/Frame propagator/Override FOV'
_cmds['widgetOverridenFOV']             = 'Camera/Frame propagator/Overridden FOV'

_cmds['widgetCameraAX']                 = 'Camera/Renderer/CamMtx/WorldMtx a X'
_cmds['widgetCameraAY']                 = 'Camera/Renderer/CamMtx/WorldMtx a Y'
_cmds['widgetCameraAZ']                 = 'Camera/Renderer/CamMtx/WorldMtx a Z'

_cmds['widgetCameraBX']                 = 'Camera/Renderer/CamMtx/WorldMtx b X'
_cmds['widgetCameraBY']                 = 'Camera/Renderer/CamMtx/WorldMtx b Y'
_cmds['widgetCameraBZ']                 = 'Camera/Renderer/CamMtx/WorldMtx b Z'

_cmds['widgetCameraCX']                 = 'Camera/Renderer/CamMtx/WorldMtx c X'
_cmds['widgetCameraCY']                 = 'Camera/Renderer/CamMtx/WorldMtx c Y'
_cmds['widgetCameraCZ']                 = 'Camera/Renderer/CamMtx/WorldMtx c Z'

_cmds['widgetCameraDX']                 = 'Camera/Renderer/CamMtx/WorldMtx d X'
_cmds['widgetCameraDY']                 = 'Camera/Renderer/CamMtx/WorldMtx d Y'
_cmds['widgetCameraDZ']                 = 'Camera/Renderer/CamMtx/WorldMtx d Z'

_cmds['widgetFreeCameraPX']             = 'Camera/Renderer/CamMtx/WorldMtx d X'
_cmds['widgetFreeCameraPY']             = 'Camera/Renderer/CamMtx/WorldMtx d Y'
_cmds['widgetFreeCameraPZ']             = 'Camera/Renderer/CamMtx/WorldMtx d Z'

_cmds['widgetFreeCameraRX']             = 'Camera/Free camera/Orientation X'
_cmds['widgetFreeCameraRY']             = 'Camera/Free camera/Orientation Y'
_cmds['widgetFreeCameraRZ']             = 'Camera/Free camera/Orientation Z'

_cmds['widgetInputFov']                 = 'Cascade Shadows/Entity Tracker/Input FOV'

_cmds['widgetBankRefCamMtrx']           = 'Camera/Renderer/CamMtx'
_cmds['widgetBankRefEntityTracker']     = 'Cascade Shadows/Entity Tracker'
_cmds['widgetBankRefCutsceneDebug']     = 'Cut Scene Debug'
_cmds['widgetBankAnimScene']            = 'Anim Scenes'

_cmds['widgetBankAnimSceneToggle']      = 'Anim Scenes/Toggle Anim Scenes bank'
_cmds['widgetBankAnimSelectAnimScene']  = 'Anim Scenes/Load scene/Scene name selector: /Select animscene'

_cmds['widgetBankAnimScenePosition']    = 'Anim Scenes/Anim scenes/Scene details/sceneOrigin/position'
_cmds['widgetBankAnimSceneRotation']    = 'Anim Scenes/Anim scenes/Scene details/sceneOrigin/orientation'
_cmds['widgetBankAnimScenePlayback']    = 'Anim Scenes/Anim scenes/Playback'

_cmds['widgetBankAnimSceneOverrideMatrix'] = 'Anim Scenes/Remote Console/Camera Editing/Override Cam Using Matrix'
_cmds['widgetBankWriteCameraMatrix'] = 'Anim Scenes/Remote Console/Camera Editing/Cam Matrix'
_cmds['widgetBankHideTrackEvents'] = 'Anim Scenes/Editor Settings/General/Hide interface'

# We create a widgets container to refernce the variable

widget = atdict(_cmds)


class MissingGameError(Exception):
    pass


class RemoteConnectionModel(object):
    def __init__(self):
        """
        """
        self.client = None
        self.mConnection = Remote()
        self.ENABLED = False
        self.GAME_MODE = False
        self.USESWITCHER = True
        self.USEFULLRANGE = False
        self.OFFSET_X = 0
        self.OFFSET_Y = 0
        self.OFFSET_Z = 0
        self.OFFSET_RX = 0 # Rotation
        self.OFFSET_RY = 0 # Rotation
        self.OFFSET_RZ = 0 # Rotation
        self.FRAME_OFFSET = 0
        self.lastFrame = 0
        self.FRAME_END = 100000
        self.CurrentCamera = None
        self.Lastcamera = None
        self.Fov = 37.0
        self.CamMatrix = FBMatrix()

        self.USECUTSCENE = False
        self.USEANIMSCNE = False

        self.sys = FBSystem()
        self.app = FBApplication()
        self.lEvaluator = FBEvaluateManager()


        # Initialize the cutscene export if its a cutscene

        RexExport.InitializeCutscene()

    """
        Public Methods
    """

    def Connect(self):
        """
         Create our connection
        """

        if not self.mConnection.IsConnected:
            self.mConnection.EnsureConnection( )
            self.initClient()
            self.intiRequiredWidgets()
            self.setWidgetBankReferences()
            self.Unregister()
            self.Register()
            # Read in any offsets from the FBX file
            #self.ReadOffsets()
            # set our offset for the range start for playback
            self.ReadStartRange()
            # Read the end range if any
            self.ReadEndRange()

    def Disconnect(self):
        """
            Disconnect connection
        """
        if self.mConnection.IsConnected:
            #self.client.ChannelFactory.Faulted -= self.connection_Faulted
            self.CloseRequiredWidgets()
            self.Unregister()
            self.mConnection.Disconnect()
            self.mConnection.Dispose()

    """
        Private Methods
    """

    def initClient(self):
        """
            Init Client connection
        """

        if not self.client:
            self.client = self.mConnection.initClient()
            #self.client.ChannelFactory.Faulted += self.connection_Faulted

    def getClient(self):
        """
        get an instance our our client to send commands to
        :return:
        """
        try:
            self.client = RagClientFactory.CreateWidgetClient(self.mConnection.GameConnection)
        except:
            raise MissingGameError

    def connection_Faulted(self, sender, e):
        """

        :return:
        """
        print "Connection is in a faulted state"

    def getSelectedCamera(self):
        """

        """
        self.CurrentCamera = FBCameraSwitcher().CurrentCamera
        self.CurrentCamera.Selected = True

    def setSwitcherCamera(self):
        """

        :return:
        """
        #FBCameraSwitcher().Selected = True

        #for iCamera in FBSystem().Scene.Cameras:
        #    print iCamera

        # deselect any cameras we have currently

        self.ResetSelection()
        self.getSelectedCamera()

    def enableSwitcher(self, value):
        """

        :param value:
        :return:
        """
        self.USESWITCHER = value

    def enableFullRange(self, value):
        """

        :param value:
        :return:
        """
        self.USEFULLRANGE = value

        self.ReadStartRange()
        self.ReadEndRange()

    def setWidgetBankReferences(self):
        """
            This will set references to the widgets banks we need to Read
        """

    def intiRequiredWidgets(self):
        """
        Enable any widgets we need to expand (Mightnot need this)

        :return:
        """
        # Open up our Camera widget

        print '......setting up'

        self.getClient()

        if self.client.WidgetExists( widget.widgetCamera ):
            self.client.PressButton( widget.widgetCamera )

        # Need to check if this is opened or not
        if not self.client.WidgetExists( widget.CutsceneSceneList ):
            self.client.PressButton( widget.CutsceneBankToggle )

        # Used for anim scene

        self.client.SendSyncCommand(5)
        
    def CloseRequiredWidgets(self):
        self.client.PressButton( widget.widgetBankAnimSceneToggle )
        self.client.WriteBoolWidget( widget.widgetBankAnimSceneOverrideMatrix, False )
        self.client.WriteBoolWidget( widget.widgetBankHideTrackEvents, False )

    def VerifyOffsets(self):
        """
        Check the cutscene and fbx offsets
        :return:
        """
        self.getClient()

        _offset = self.client.ReadVector3Widget( widget.CutsceneSceneOrientationWidgetName )
        _rotation = self.client.ReadVector3Widget( widget.widgetSceneOffsetRotation )

    def SynchCutscenePlayback(self):
        """
            Play back our cutscene
        """
        if self.client:

            _cutName = self.ReadCutsceneName()

            self.USECUTSCENE = True
            self.USEANIMSCNE = False

            self.getClient()

            self.client.WriteStringWidget(widget.widgetSearchCutscene, '')
            self.client.SendSyncCommand(0)

            self.client.WriteStringWidget(widget.CutsceneSceneList, _cutName)
            self.client.SendSyncCommand(0)
            
            # Need to check here to see if the cutscene exits: We read teh combo list
            _validCutscene = self.client.ReadStringWidget(widget.CutsceneSceneList).lower()

            if  _cutName == _validCutscene:

                self.client.WriteBoolWidget(widget.widgetBankAnimSceneOverrideMatrix, True)

                if RS.Config.Project.Name == 'RDR3':
                    self.client.WriteBoolWidget( widget.widgetBankHideTrackEvents, True )
                    self.client.PressButton( widget.CutsceneSceneStartButton )
                else: 
                    self.client.PressButton( widget.CutsceneStopSelectedCutScene )
                time.sleep(2)    
                self.ValidateCutsceneOffset()

            else:
                FBMessageBox( "Error!!!", "Cutscene does not Exist in your build, either export or sync to newer build if available", "OK" )

    def SyncAnimScene(self):
        """

        :return:
        """
        if self.client:

            self.ReadAnimOffsets()

            self.lastFrame = 0

            self.USECUTSCENE = False
            self.USEANIMSCNE = True

    def SyncGameOffsets(self):
        """

        :return:
        """
        if self.client:
            self.readGameOffsets()

    def startStopCutscene(self):
        """
        """

        self.SynchCutscenePlayback()

    def startAnimScene(self):
        """

        :return:
        """
        self.SyncAnimScene()

    """
        Read Methods
    """

    def ValidateCutsceneOffset(self):
        """

        :return:
        """
        isValid = True
        self.ReadFBXOffsets()
        _offset = self.client.ReadVector3Widget( widget.CutsceneSceneOrientationWidgetName )
        if abs(_offset.X-self.OFFSET_X) < 5.1:
            isValid = False
        if abs(_offset.Y-self.OFFSET_Y) < 5.1:
            isValid = False
        if abs(_offset.Z-self.OFFSET_Z) < 5.1:
            isValid = False

        """
        if isValid == False:
            FBMessageBox( "Warning!!!", "FBX offsets are different from the build, most likely this cutscene needs to be exported"
                                        "\n\nThis FBX Coords    :\tX:{0}\tY:{1}\tZ:{2}"
                                        "\n\nThis Build  Coords :\tX:{3}\tY:{4}\tZ:{5}".format(_offset.X, _offset.Y, _offset.Z,
                                                                                         _offset.X,  _offset.Y,  _offset.Z ), "OK" )

        """

    def IsCutscenePlaying(self):
        """

        :return:
        """
        if self.client.WidgetExists( widget.CutsceneSceneOrientationWidgetName ):
            _offset = self.client.ReadVector3Widget( widget.CutsceneSceneOrientationWidgetName )
            if _offset.X == 0.0:
                return False
            else:
                return True
        return False

    # Private
    def ReadFBXOffsets(self):
        """
            Read any offsets from teh Mobu File.
            Might be better to read from Game

        """
        self.OFFSET_X = RexExport.GetCutsceneOffsetsX()
        self.OFFSET_Y = RexExport.GetCutsceneOffsetsY()
        self.OFFSET_Z = RexExport.GetCutsceneOffsetsZ()
        self.OFFSET_RZ = RexExport.GetCutsceneOrientation()

    def readCutsceneOffsets(self):
        """
            Read any offset setup from any init cutscenes

        :return:
        """
        self.getClient()
        if self.client.WidgetExists( widget.CutsceneSceneOrientationWidgetName ):
            _offset = self.client.ReadVector3Widget( widget.CutsceneSceneOrientationWidgetName )
            _rotation = self.client.ReadVector3Widget( widget.widgetSceneOffsetRotation )

        self.OFFSET_X = _offset.X
        self.OFFSET_Y = _offset.Y
        self.OFFSET_Z = _offset.Z

        self.OFFSET_RX =_rotation.X
        self.OFFSET_RY =_rotation.Y
        self.OFFSET_RZ =_rotation.Z

    def readGameOffsets(self):
        """

        :return:
        """

        self.getClient()
        self.OFFSET_X = self.client.ReadFloatWidget( widget.widgetFreeCameraPX )
        self.OFFSET_Y = self.client.ReadFloatWidget( widget.widgetFreeCameraPY )
        self.OFFSET_Z = self.client.ReadFloatWidget( widget.widgetFreeCameraPZ )

    def ReadAnimOffsets(self):
        """

        :return:
        """
        self.getClient()
        _offsetPos = self.client.ReadVector3Widget( widget.widgetBankAnimScenePosition )
        _offsetRot = self.client.ReadVector3Widget( widget.widgetBankAnimSceneRotation )


        self.OFFSET_X = _offsetPos.X
        self.OFFSET_Y = _offsetPos.Y
        self.OFFSET_Z = _offsetPos.Z

        self.OFFSET_RX =_offsetRot.X
        self.OFFSET_RY =_offsetRot.Y
        self.OFFSET_RZ =_offsetRot.Z

    def ReadCutsceneName(self):
        """

        :return:
        """
        cdll.rexMBRage.GetCutsceneSceneName_Py.restype = c_char_p
        return cdll.rexMBRage.GetCutsceneSceneName_Py().lower()

    def ReadStartRange(self):
        """

        :return:
        """
        if self.USEFULLRANGE:
            self.FRAME_OFFSET = 0
        else:
            self.FRAME_OFFSET = -cdll.rexMBRage.GetCutsceneShotStartRange_Py( 0 )

    def ReadEndRange(self):
        """

        :return:
        """
        if self.USEFULLRANGE:
            self.FRAME_END = int(self.sys.CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*',''))
        else:
            _shots = cdll.rexMBRage.GetNumberCutsceneShots_Py()
            _cutRange = 0
            for i in range(_shots):
                _cutRange += (cdll.rexMBRage.GetCutsceneShotEndRange_Py( i ) - cdll.rexMBRage.GetCutsceneShotStartRange_Py( i ))

            self.FRAME_END = _cutRange + 2

    def ReadMobuCamera(self):
        """
            Read a Mobu Camera Matrix
        """
        self.CurrentCamera.GetCameraMatrix(self.CamMatrix, FBCameraMatrixType.kFBModelView)

    def ReadGameCameraFov(self):
        """
            Read our Camera FOV
        """
        # Turn Off override
        self.getClient()
        self.client.WriteBoolWidget(widget.widgetOverrideFOV, False)
        self.Fov = self.client.ReadFloatWidget( widget.widgetInputFov )

    def ReadGameCamera(self):
        """
            Read the Game Camera Matrix
        """
        # Get our Camera Matrix
        self.getClient()
        aX = self.client.ReadFloatWidget( widget.widgetCameraAX )
        aY = self.client.ReadFloatWidget( widget.widgetCameraAY )
        aZ = self.client.ReadFloatWidget( widget.widgetCameraAZ )

        bX = self.client.ReadFloatWidget( widget.widgetCameraBX )
        bY = self.client.ReadFloatWidget( widget.widgetCameraBY )
        bZ = self.client.ReadFloatWidget( widget.widgetCameraBZ )

        cX = self.client.ReadFloatWidget( widget.widgetCameraCX )
        cY = self.client.ReadFloatWidget( widget.widgetCameraCY )
        cZ = self.client.ReadFloatWidget( widget.widgetCameraCZ )

        dX = self.client.ReadFloatWidget( widget.widgetCameraDX )
        dY = self.client.ReadFloatWidget( widget.widgetCameraDY )
        dZ = self.client.ReadFloatWidget( widget.widgetCameraDZ )

        # Record the values to our local Game matrix
        self.CamMatrix[0] = aX
        self.CamMatrix[1] = aY
        self.CamMatrix[2] = aZ

        self.CamMatrix[4] = bX
        self.CamMatrix[5] = bY
        self.CamMatrix[6] = bZ

        self.CamMatrix[8] = cX
        self.CamMatrix[9] = cY
        self.CamMatrix[10] = cZ

        self.CamMatrix[12] = 0
        self.CamMatrix[13] = 0
        self.CamMatrix[14] = 0

        # add in any offset Rotations from teh game ie. Cutscene offsets or Anim scene Offsets
        lAdjustmentMatrixA = FBMatrix()
        lVector3d = FBVector3d(self.OFFSET_RY ,self.OFFSET_RX , -self.OFFSET_RZ)
        FBRotationToMatrix(lAdjustmentMatrixA, lVector3d, FBRotationOrder.kFBXZY)

        # Multiply these Matricies togather to get Final Result
        lFinalMatrix = FBMatrix()
        FBMatrixMult( lFinalMatrix, lAdjustmentMatrixA , self.CamMatrix)

        # Add back our Positin offsets
        lFinalMatrix[12] = (dX - self.OFFSET_X) * 100
        lFinalMatrix[13] = (dZ - self.OFFSET_Z) * 100
        lFinalMatrix[14] = ((dY - self.OFFSET_Y) * 100) *-1


        self.CamMatrix = lFinalMatrix

    """
        Write Methods
    """

    def WriteCameraToGame( self  ):
        """

        :param lmatrix:
        :return:
        """

        # rotate camera
        lMatrixResult = FBMatrix()
        lMatrixResult[0] = self.CamMatrix[2]*-1
        lMatrixResult[1] = self.CamMatrix[6]*-1
        lMatrixResult[2] = self.CamMatrix[10]*-1

        lMatrixResult[4] = self.CamMatrix[1]
        lMatrixResult[5] = self.CamMatrix[5]
        lMatrixResult[6] = self.CamMatrix[9]

        lMatrixResult[8] = self.CamMatrix[0]
        lMatrixResult[9] = self.CamMatrix[4]
        lMatrixResult[10] = self.CamMatrix[8]

        # We need to rotate anti clockwise
        lFinalGameMatrix = MathGameUtils._rotMatrix( lMatrixResult, rotX=0, rotY=-90, rotZ=0, AB=True )

        # Get our querterion from the matrix
        lVector3d = FBVector3d()
        FBMatrixToRotation(lVector3d, lFinalGameMatrix, FBRotationOrder.kFBXYZ)
        lVector4d = FBVector4d()
        FBRotationToQuaternion(lVector4d, lVector3d, FBRotationOrder.kFBXYZ)

        # Set the rotation order to be the game. There is a bug in MB's FBRotationTOQuaterion
        # where it ignroes the rotation order it seems
        newVector = Math.FBQuaternionToEuler(lVector4d[0], lVector4d[1], lVector4d[2], lVector4d[3], rotationOrder="XZY")

        PosX = ((self.CurrentCamera.Translation[0] *0.01) + self.OFFSET_X)
        PosY = ((self.CurrentCamera.Translation[2] *0.01)*-1) + self.OFFSET_Y
        PosZ = ((self.CurrentCamera.Translation[1] *0.01) + self.OFFSET_Z)

        # Work out our Rotations, add any offset rotations
        RotZ = newVector[2] + self.OFFSET_RZ
        RotY = newVector[1] + self.OFFSET_RY
        RotX = newVector[0] + self.OFFSET_RX

        # For the cutscene Mode
        vecA = RageMath.Vector3f(lFinalGameMatrix[10],   lFinalGameMatrix[8],    lFinalGameMatrix[9])
        vecB = RageMath.Vector3f(lFinalGameMatrix[2],   lFinalGameMatrix[0],    lFinalGameMatrix[1])
        vecC = RageMath.Vector3f(lFinalGameMatrix[6],   lFinalGameMatrix[4],    lFinalGameMatrix[5])
        vecD = RageMath.Vector3f(PosX,  PosY,   PosZ)

        lGameMatrix = RageMath.Matrix34f(vecA, vecB, vecC, vecD)

        # Write our new positions to Game for Game Camera
        # Position
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraPX, PosX)
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraPY, PosY)
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraPZ, PosZ)
        # Rotation
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraRX, RotY) # Swp X and Y for rotation
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraRY, RotX)
        self.getClient()
        self.client.WriteFloatWidget(widget.widgetFreeCameraRZ, RotZ)

        # If in cutscene Mode write our camera matrix
        self.getClient()
        
        if RS.Config.Project.Name == 'RDR3':
            self.client.WriteMatrix34Widget(widget.widgetBankWriteCameraMatrix, lGameMatrix )
        else:
            self.client.WriteMatrix34Widget(widget.CameraMatrixWidgetName, lGameMatrix )

    def WriteFovToGame(self):
        """

        :return:
        """
        self.getClient()
        self.client.WriteBoolWidget(widget.widgetOverrideFOV, True)
        self.client.WriteFloatWidget(widget.widgetOverridenFOV, self.CurrentCamera.FieldOfView)

    def WriteCameraToMobu(self):
        """

        :return:
        """
        # Convert our Game Matrix to EulerMatrix
        lFinalMatrix = MathGameUtils.convertGameToMobu( self.CamMatrix )

        # Set the matrix and update scene
        self.CurrentCamera.SetMatrix( lFinalMatrix )
        self.CurrentCamera.FieldOfView = self.Fov

    def ResetSelection(self):
        """
        De-Select any models we have selected
        :return:
        """
        selectedModels = FBModelList()
        FBGetSelectedModels( selectedModels, None, True )
        for model in selectedModels:
            model.Selected = False

    def UpdateFrame(self):
        """
            Update the Game frame number if its a cutscene
        """

        CurrentFrame = FBSystem().LocalTime.GetFrame() + self.FRAME_OFFSET

        self.getClient()
        if self.USECUTSCENE:

            if CurrentFrame < self.FRAME_END:
                self.client.WriteIntWidget(widget.CutsceneCurrentFrameWidgetName,CurrentFrame )

        if self.USEANIMSCNE:

            # For updating anim Sync scenes. Pretty crappy
            if CurrentFrame > self.lastFrame:
                self.client.PressVCRButton(widget.widgetBankAnimScenePlayback,3 )
                self.lastFrame = CurrentFrame
            elif CurrentFrame < self.lastFrame:
                self.client.PressVCRButton(widget.widgetBankAnimScenePlayback,4 )
                self.lastFrame = CurrentFrame

    """
        MB Callbacks
    """

    def SendCameraUpdatesToMobu(self):
        """
        Wrapper callback methods to read the Game camera and Update Mobu's Camera
        :return:
        """

        self.ReadGameCameraFov()
        self.ReadGameCamera()
        self.WriteCameraToMobu()

    def SendCameraUpdatesToGame(self):
        """
        Wrapper Callback methods to read Mobu Camera and Update Game CAmera
        :return:
        """

        if self.USESWITCHER:
            self.CurrentCamera = FBCameraSwitcher().CurrentCamera

            if self.Lastcamera != self.CurrentCamera:
                # delesect old camera
                if self.Lastcamera:
                    self.Lastcamera.Selected = False
                # new camera == last camera
                self.Lastcamera = self.CurrentCamera
                # select new camera
                self.CurrentCamera.Selected = True

        self.ReadMobuCamera()
        self.WriteFovToGame()
        self.WriteCameraToGame()
        self.UpdateFrame()

    # Check if Camera is selected
    def OnConnectionStateNotify( self, control, event ):
        """
        """

        if event.Action == FBConnectionAction.kFBSelect:

            if event.Plug.ClassName() == 'FBCamera':
                self.CurrentCamera = event.Plug

            if event.Plug.ClassName() == 'FBCameraSwitcher':
                self.CurrentCamera = event.Plug.CurrentCamera

    def OnSyncUpdateCameraPosition( self, control, event):
        """
        Update our camera in game or in Mobu
        """
        if self.CurrentCamera:
            if self.GAME_MODE:
                self.SendCameraUpdatesToMobu()
            else:
                self.SendCameraUpdatesToGame()

    """
        Register Callbacks
    """

    def Register(self):
        """

        :return:
        """
        self.lEvaluator.OnSynchronizationEvent.Add(self.OnSyncUpdateCameraPosition)
        self.sys.OnConnectionStateNotify.Add(self.OnConnectionStateNotify)
        self.app.OnFileExit.Add(self.Unregister)

    def Unregister(self, control=None, event=None):
        """

        :param control:
        :param event:
        :return:
        """
        self.lEvaluator.OnSynchronizationEvent.Remove(self.OnSyncUpdateCameraPosition)
        self.sys.OnConnectionStateNotify.Remove(self.OnConnectionStateNotify)
        self.app.OnFileExit.Remove(self.Unregister)
