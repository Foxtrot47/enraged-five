from pyfbsdk import *
from pyfbsdk_additions import *

import os
import string
import sys

import RS.Config
import RS.Tools.AnimationTransferUtil
reload(RS.Tools.AnimationTransferUtil)

from xml.dom import minidom

def FileErrorCheck(filepath, fbxCheck=0):
    if os.path.exists(filepath) == 0:
        print "ERROR:", filepath, " Does Not Exist!"
        #log(("ERROR:" + filepath + " Does Not Exist!\n"), 0)
        return "Filepath Does Not Exist"
    
    if os.path.isfile(filepath) == 0:
        print "Error:", filepath, " Is Not A File!"
        #log(("ERROR:" + filepath + " Is Not A File!\n"), 0)
        return "Filepath Is Not a File!"

    if fbxCheck:    
        if os.path.isfile(filepath) and filepath.endswith(".fbx") == 0:
            print "Error:", filepath, " Is Not A FBX File!"
            #log(("ERROR:" + filepath + " Is Not A FBX File!\n"), 0)
            return "Filepath Is Not a FBX!"
    
    return 1

def ProcessJob(jobInst):
    inputFileStat = FileErrorCheck(jobInst.inputFilePath, 1)
    outputFileStat = 1 #FileErrorCheck(jobInst.outputFilePath, 1)
    
    if inputFileStat and outputFileStat and jobInst.stage == '0':
        #Open the template file
        #try:
            
        RS.Tools.AnimationTransferUtil.ProcessFile(jobInst.inputFilePath,  jobInst.outputFilePath)           
                        
        #except:
        #    print "Could not process: {0}".format(jobInst.inputFilePath)
        #    return 0, ["Could not process: {0}".format(jobInst.inputFilePath)]

    return 1, []

class AnimJob():
    def __init__(self):
        self.inputFilePath = ''
        self.outputFilePath = ''
        self.stage = ''
        self.status = 0
        self.erros = []
        
        
    def Process(self):
        
        self.status = 1
        result, self.errors = ProcessJob(self)
        '''
        if result == 0:
            self.status = -1
        else:
            self.status = 1
        '''
        print self.errors
            
    
class JobManager():
    def __init__(self):
        self.currentJob = None
        self.loadedJobFiles = {}
        self.status = 0
        self.paused = 0
        self.queue = []
        
        
    def ProcessCurrentJob(self):
        
        if self.currentJob != None:
            self.currentJob.Process()
            
            if self.currentJob.status == 1:
                return 1        
            
            else:
                return 0
            
        else:
            return 0
        

    def AddAllToQueue(self, jobFileName=""):
        if jobFileName == "":
            #If we are not just adding a single job file, then process all loaded files
            for jobFileKey in self.loadedJobFiles.keys():
                self.queue += self.loadedJobFiles[jobFileKey]
                
        else:
            self.queue = self.loadedJobFiles[jobFileKey]
            

    def AddJobToQueue(self, jobInst):
        if jobInst not in self.queue:
            self.queue.append(jobInst)
            
                    
    def ProcessQueue(self):
        self.status = 1
        result = -1
        
        while(self.status == 1):
            if self.paused != 1:
                if len(self.queue) > 0:
                    self.currentJob = self.queue[0]
                    self.queue.pop(0)
                    
                    result = self.ProcessCurrentJob()
                    print result    
                    
                else:
                    self.status = 0
                    
            
                

    def LoadJobFile(self, jobFilePath):
        if FileErrorCheck(jobFilePath) == 1:
            self.loadedJobFiles[jobFilePath] = []
            xmldoc = minidom.parse( jobFilePath )     
            jobNodes = xmldoc.getElementsByTagName( 'job' ) 
            for jobNode in jobNodes:
                jobInst = AnimJob()
                jobInst.inputFilePath = str( jobNode.attributes[ 'source' ].value )
                jobInst.outputFilePath = str( jobNode.attributes[ 'output' ].value )
                jobInst.stage = str( jobNode.attributes[ 'stage' ].value )
                
                self.loadedJobFiles[jobFilePath].append(jobInst)
                
            return 1
        else:
            return 0


        

jobXMLPath = "{0}\etc\config\general\ConvertAnimSkeletonJob.xml".format(RS.Config.Tool.Path.Root)

batchManager = JobManager()
batchManager.LoadJobFile(jobXMLPath)
batchManager.AddAllToQueue()
batchManager.ProcessQueue()
