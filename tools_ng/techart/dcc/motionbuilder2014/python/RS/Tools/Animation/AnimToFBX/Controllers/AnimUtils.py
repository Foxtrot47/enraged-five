"""
This module contains code specific for the AnimToFbx Motion Builder tool but that is independent from the QT UI used

The main purpose of this module is to separate the functionality of the tool from the UI components.
"""

import os
import clr
from functools import wraps
from collections import OrderedDict

import pyfbsdk as mobu

import RS.Config
import RS.Globals
import RS.Utils.Path
import RS.Utils.AnimFile
from RS.Utils.Scene import Take
from RS.Core.Animation import Lib
from RS.Tools.UI import Commands
from RS.Tools.ModelViews import AnimToFBX as depotFileData
from RS.Core.Animation.DataManager.Skeleton import SkeletonObject

# Catch to make sure the Anim2Fbx file is renamed properly because perforce won't do it for us on windows

try:
    import RS.Core.Animation.Anim2Fbx as Anim2Fbx

except ImportError, error:
    import RS.Core.Animation.anim2Fbx as Anim2Fbx
    FILEPATH, _ = os.path.split(Anim2Fbx.__file__)
    os.rename(Anim2Fbx.__file__, os.path.join(FILEPATH, "Anim2Fbx.py"))

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory

_Config = ConfigFactory.CreateConfig()

TOOLS_ROOT = RS.Config.Tool.Path.Root
PROJECT_ROOT = RS.Config.Project.Path.Root

FILE_FOLDER = ["File", "Folder"]

PROJECT_PATH = {
    "PROPS": "\\assets\\cuts",
    "POSE": "\\art\\anim\\export_mb",
    "FACIAL": "\\audio\\dev_ng\\assets\\LipsyncAnims",
    "CAMERA": "\\art\\anim\\export_mb",
    }

TYPE_DICTIONARY = {mobu.FBCamera: "Camera",
                   mobu.FBModel: "Props",
                   mobu.FBCharacter: "Character"}
ALL_PROJECTS_ROOT = {each_project.Name: each_project.Root
                     for each_project in _Config.AllProjects().Values}


def GetDlcRoot(lbSelItem=None):
    """
    Gets the Root path for the project

    Arguments:
        lbSelItem (string); text of the selected project in the Anim2Fbx tool
    """

    path = "art"

    if lbSelItem == 'rdr3 old location':
        return 'x:\\rdr3\\art\\anim\\export_mb'

    elif lbSelItem == "rdr3":
        path = "assets\\anim\\ingame"

    projectList = _Config.AllProjects()
    # Returns a list with only values whose name matched lbSelItem
    filteredList = filter(lambda eachProject: eachProject.Name == lbSelItem, projectList.Values)

    if filteredList:
        return os.path.join(filteredList[0].Root, path)


def ImportCamera(filepath):
    """
    Creates a new camera in the scene that matches the animation from the .anim file with camera
    keyframe data

    Arguments:
        filepath (string): path to the anim file

    """
    Lib.CopyTake(filepath)
    cameraData = Anim2Fbx.AnimObject(path=filepath)
    cameraData.ImportCamera()
    return True


def Plot(path, namespace, skeletonPath="", start_frame=0, pose_frame=None):
    """
    Plots the animation data found in an anim file to the selected character.

    Arguments:
        animationPath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.

    Returns:
        AnimTrack instance
    """

    bones = []
    create_pose = type(pose_frame) == int or type(pose_frame) == float

    # We check if skeleton path has a value before running os.path.exists
    # os.path.exists errors out if you pass None or anything other than a string
    skeletonData = None
    if skeletonPath and os.path.exists(skeletonPath):
        skeletonData = SkeletonObject(skeletonPath)
        bones = skeletonData.BoneIds

    # animationPath = animationPath.replace("%40", "@")
    AnimData = Anim2Fbx.AnimObject(path=path, SkeletonObject=skeletonData, SkeletonPath=skeletonPath)

    if create_pose:
        result = AnimData.ApplyPose(namespace, bones=bones, frame=pose_frame, start_frame=start_frame)

    else:
        SetTakeRange(start_frame, AnimData.Duration)                   
      
        result = AnimData.ApplyAnimation(namespace, bones=bones, start_frame=start_frame)

    Lib.SetPlayerControl()

    return result


def SelectFile(file_type=None):
    """
    Opens a file dialog to ask the user which file they want to select

    Arguments:
        file_type (string): the type of data that you want the dialog to display.
                               Accepts Props, Pose, Facial or Camera
`
    Return:
        string; path to the selected file
    """
    path = os.path.join(RS.Config.Project.Path.Root,
                        PROJECT_PATH.get(file_type, "art\\anim\\export_mb\\"))

    if not os.path.exists(path):
        lWarning = ("{} project path does not exist".format(path))
        mobu.FBMessageBox("Sorry, ", lWarning, "OK")

    path, valid = Commands.FileDialog(path=path, filter="anim")
    if not valid:
        path = ""
    return path


def SelectFolder(project=None):
    """
    Opens a file dialog to ask the user which file they want to select

    Arguments:
        project (string): the current project the local machine is set to

    Return:
        string; path to the selected file
    """
    path = os.path.join(RS.Config.Project.Path.Art, 'anim\\export_mb')

    if project not in ALL_PROJECTS_ROOT:
        path = os.path.join(RS.Config.Project.Path.Assets, 'anim\\ingame')

    path, valid = Commands.FileDialog("Folder", path=path, filter="anim")
    if not valid:
        path = ""
    return path


def DoesPathExist(paths, sync):
    """
    Checks if a path exists, if they don't then we try to sync with perforce to retrieve the files.

    Arguments:
        paths = list[string, string, etc.] ; paths to check if they exist
        sync (boolean): if we should sync with perforce

    Return:
        Boolean
    """
    # Check to see if a path was passed in
    if isinstance(paths, basestring):
        paths = [paths]

    for path in paths:

        if not path:
            return False

        if type(path) is unicode:
            path = str(path)

        path = os.path.abspath(path.encode("string-escape"))
        path = path.replace("%40", "@")

        # Sync file

        if not os.path.exists(path) or sync:
            try:
                depotFileData.syncSelectedFile(path)
            except Exception, e:
                print "Error encounter when trying to Sync File:\n", e

        # Check if we have the file locally
        if not os.path.exists(path):
            mobu.FBMessageBox("Warning", '{} \n was not found on local drive.\n\
            Try syncing to perforce depot'.format(path), "OK")
            return False
    return True


def CheckPathExist(func):
    """
    Checks if a path exists, if they don't then we try to sync with perforce to retrieve the files.

    This decorator does intercept the incoming parameters of the methods it decorates. It expects the decorated
    methods first argument to be a path to a file/directory and adds support for the sync keyword even if the
    decorated function doesn't support it. The decorated function must be able to support keyword arguments.
    """

    @wraps(func)
    def wrapper(path, *args, **kwargs):
        sync = kwargs.get("sync", True)

        if not DoesPathExist(path, sync):
            return

        isList = isinstance(path, list)
        if not isList:
            path = [path]

        paths = OrderedDict()
        for eachPath in path:
            eachPath = eachPath.replace("%40", "@")

            correctedPath = eachPath
            if "^" in eachPath:
                correctedPath = eachPath.replace("^", "_")
                os.rename(eachPath, correctedPath)

            paths[eachPath] = correctedPath

        results = func([paths.values()[0], paths.values()][isList], *args, **kwargs)

        # rename files that had the "^" removed
        [os.rename(corrected, original) for original, corrected in paths.iteritems() if original != corrected]

        return results
    return wrapper


@CheckPathExist
def LoadFile(filePath, skeletonPath="", start_frame=0, pose_frame=None, useCurrentTake=False,
             componentType=mobu.FBCharacter, namespaces=[], *arguments, **keywordArguments):
    """
    Applies the animation from the provided file to the currently selected character.

    This method differs from the Plot() method as this method creates a new take or completely overrides
    all animation on the current take and replaces it with the data from the provided file. The Plot() method
    will only override animation data that lies on the same frame as the data from the provided file. Any subsequent
    keyframe information is kept on the rig. The LoadFile() method will clear all animation data before applying
    the new keyframe data.

    Arguments:
        filePath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
        componentType (FBComponent): type of component to apply animation on, defaults to mobu.FBCharacter
        namespaces (list[string, etc.]); list of namespaces to look for objects to apply animation to in.
    """

    Lib.CopyTake(filePath, useCurrentTake)

    if componentType is mobu.FBCamera:
        return ImportCamera(filePath)

    return any([Plot(path=filePath, namespace=namespace, skeletonPath=skeletonPath, start_frame=start_frame,
                     pose_frame=pose_frame) for namespace in namespaces])


@CheckPathExist
def LoadFiles(filePaths, *arguments, **keywordArguments):
    """
    Applies the animation from the provided files to the currently selected character.

    Arguments:
        filePaths = list[string]; path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
    """
    for eachFilePath in filePaths:
        if not LoadFile(eachFilePath,  *arguments, **keywordArguments):
            print "{} failed to load".format(eachFilePath)
            return
    return True


@CheckPathExist
def LoadFolder(folderPath, *arguments, **keywordArguments):
    """
    Applies the animation from the files in the provided folder to the currently selected character.

    Arguments:
        folderPath (string): path to the .anim file with the animation data to plot
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
    """
    files = [os.path.join(path, eachFile) for path, folders, files in os.walk(folderPath) for eachFile in files
             if os.path.isfile(os.path.join(path, eachFile))]
    return LoadFiles(files, *arguments, **keywordArguments)


def LoadFileByType(type=mobu.FBCharacter,  *arguments, **keywordArguments):
    """
    Applies the animation from the file selected  by the user to the currently selected character.

    Arguments:
        type (string): The type of data that you want shown on the file/directory dialog
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
    """

    keywordArguments.setdefault("componentType", type)

    filepath = SelectFile(TYPE_DICTIONARY.get(type, "Character"))

    return LoadFile(filepath, *arguments, **keywordArguments)


def LoadFolderByType(type=mobu.FBCharacter, *arguments, **keywordArguments):
    """
    Applies the animations from the folder selected by the user to the currently selected character.

    Arguments:
        type (string): The type of data that you want shown on the file/directory dialog
        skeletonPath (string): path to the .skel file that lists which bones should be plotted
        start_frame (int): frame on which to start the animation
        pose_frame (int): the frame of the pose that you want to plot. If this argument used, then only
                     that pose at the specified frame will be plotted. All other animation data will be
                     ignored.
        useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the file
                    provided when True. The keyframe data is applied a new take when False.
    """

    keywordArguments.setdefault("componentType", type)

    directory_path = SelectFolder(TYPE_DICTIONARY.get(type, "Character"))

    return LoadFolder(directory_path, *arguments, **keywordArguments)


@CheckPathExist
def AddPoseToPoseControl(filepath, frame=0):
    """
    Adds pose to to the pose control

    Arguments:
        filepath (string): path to the pose that you want to add to the pose control
        frame (int): frame number of the pose that you want ot apply
    """
    # Create Temporary Take to avoid overriding current animation

    Lib.CopyTake("TempTake")
    # Apply Pose
    Plot(filepath, pose_frame=frame)
    # Evaluate Scene force the character to go into the new pose
    mobu.FBSystem().Scene.Evaluate()
    # Save to the Pose Control
    poseName = "FRAME_{0:03d}_of_ANIM_{1}".format(frame, RS.Utils.Path.GetBaseNameNoExtension(filepath))
    Lib.AddPoseToPoseControl(poseName)
    # Delete the temporary take

    take = RS.Utils.Scene.Take.GetTakeByName("TempTake")
    if take:
        take.FBDelete()


@CheckPathExist
def AddPosesToPoseControl(path, frame=0):
    """
    Adds pose to to the pose control

    Arguments:
        filepath (string): path to the pose that you want to add to the pose control
        frame (int): frame number of the pose that you want ot apply
    """
    for each_file in RS.Utils.Path.Walk(path, pRecurse=0, pPattern='*.anim', pReturn_folders=0):
        AddPoseToPoseControl(each_file, frame)


def SetTakeRange(start_frame, duration):
    """
    Adjusts the range of the current take 

    Arguments:
        start_frame (int): most animations start at frame 0, but can have an offset
        duration (int): duration (in frames) of the current animation track
    """    
    # apply range here (start_frame, duration + start_frame)
    lStartFrame = mobu.FBTime(0, 0, 0, start_frame)
    lEndFrame = mobu.FBTime(0, 0, 0, duration+start_frame-1)
    
    lPlayerControls = mobu.FBPlayerControl()
    lPlayerControls.LoopStart = lStartFrame
    lPlayerControls.ZoomWindowStart = lStartFrame
    lPlayerControls.LoopStop = lEndFrame
    lPlayerControls.ZoomWindowStop = lEndFrame
    lPlayerControls.Goto(lStartFrame)                        
    # Workaround for getting the last take to commit
    curTake = mobu.FBSystem().CurrentTake
    curTake.LocalTimeSpan = mobu.FBTimeSpan(lStartFrame, lEndFrame)