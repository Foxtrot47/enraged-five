"""
Widget that displays the current anim files available locally and on perforce
"""
import time

from PySide import QtGui, QtCore

from RS.Tools.Animation.Anim2Fbx.Models import ModelManager
from RS.Tools.Animation.Anim2Fbx.Widgets import ModelManagerTree

from RS.Tools.UI import QPrint


class AnimFileTree(QtGui.QWidget):
    """
    Widget that displays the current anim files available locally and on perforce
    """
    doubleClickSignal = QtCore.Signal()

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        super(AnimFileTree, self).__init__(parent=parent)

        self._comboBoxDictionary = {"-- Select Directory --": ""}
        self._previousComboBoxSelection = "-- Select Directory --"

        layout = QtGui.QVBoxLayout()
        topLayout = QtGui.QHBoxLayout()

        self.directoryComboBox = QtGui.QComboBox()
        self.localCheckBox = QtGui.QCheckBox("Only Show Local Files ")
        self.fileTree = ModelManagerTree.ModelManagerTree(title="Anim Files")

        self.localCheckBox.setFixedWidth(122)
        self.directoryComboBox.addItem("-- Select Directory --")

        topLayout.addWidget(self.directoryComboBox)
        topLayout.addWidget(self.localCheckBox)

        layout.addLayout(topLayout)
        layout.addWidget(self.fileTree)

        layout.setSpacing(0)
        topLayout.setSpacing(4)
        layout.setContentsMargins(0, 0, 0, 0)

        self.fileTree.addFileExtension(".anim")
        self.fileTree.tree.setSelectionMode(self.fileTree.tree.ExtendedSelection)
        self.fileTree.tree.setDragDropMode(self.fileTree.tree.DragDrop)
        self.fileTree.tree.model().mimeTypes = self.mimeTypes
        self.fileTree.tree.doubleClicked.connect(self.doubleClick)
        self.directoryComboBox.activated.connect(self.UpdateTreeView)
        self.setLayout(layout)

    def AddComboBoxEnum(self, text, value):
        """
        Adds a new enum value to the combobox

        Arguments:
            text (string): text to display on the combobox
            value (string): value to associate with the new enum value

        """
        self._comboBoxDictionary[text] = value
        self.directoryComboBox.addItem(text)

    def UpdateTreeView(self):
        """ Draws the contents of the Tree View """
        directory = self._comboBoxDictionary.get(self.directoryComboBox.currentText(), "")

        if directory:
            start = time.time()
            self.fileTree.setDirectory(directory)
            self.fileTree.UpdateView()
            QPrint(time.time() - start)

    def setAutoFilter(self, status):
        """
        Sets the auto filter on or off

        Arguments:
            status (boolean): whether the autofilter feature should be on or off
        """
        self.fileTree.setAutoFilter(status)

    def setProgressBar(self, status):
        """
        Sets if the progress bar should be shown when adding new folders updated

        Arguments:
            status (boolean): status to set the progress bar to; Enabled (True) or disabled (False)

        """
        self.fileTree.setProgressBar(status)

    def Directory(self):
        """ Current Directory to look for anim files in """
        return self.fileTree.rootDirectory

    def SelectedFiles(self):
        """ The selected file from the Tree View """
        selectedFiles = []
        for modelIndex in self.fileTree.selectedItems:
            modelIndex = self.fileTree.filter.mapToSource(modelIndex)
            invalid = modelIndex.data(ModelManager.AnimStandardItem.Delete)
            if not invalid:
                selectedFiles.append(self.fileTree.Model().itemFromIndex(modelIndex).toolTip())
        return selectedFiles

    def mimeTypes(self):
        """ Tells the model what mime types to allow when dropping items """
        return []

    def doubleClick(self, modelIndex):
        """
        Overrides the double click event to emit custom signal

        Arguments:
            modelIndex (QtGui.QModelIndex): model index of the item being double clicked upon

        """
        self.doubleClickSignal.emit()
