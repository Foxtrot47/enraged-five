from PySide import QtGui


class FileFilterModel(QtGui.QSortFilterProxyModel):
    """
    Custom QSortFilterProxyModel so that when a match is found it also finds the parents
    """
    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        Took from http://stackoverflow.com/questions/250890/using-qsortfilterproxymodel-with-a-tree-model
        If a child matches the filter string, show the parents

        Arguments:
            sourceRow (int): location of the row
            sourceParent (QtGui.QModelIndex): index of the parent

        Return:
            boolean
        """
        if not self.filterRegExp().isEmpty():
            sourceIndex = self.sourceModel().index(sourceRow, self.filterKeyColumn(), sourceParent)
            if sourceIndex.isValid():
                for index in xrange(self.sourceModel().rowCount(sourceIndex)):
                    if self.filterAcceptsRow(index, sourceIndex):
                        return True
                currentString = str(self.sourceModel().data(sourceIndex, self.filterRole()))

                return self.filterRegExp().pattern().lower() in currentString.lower()

        return super(FileFilterModel, self).filterAcceptsRow(sourceRow, sourceParent)


