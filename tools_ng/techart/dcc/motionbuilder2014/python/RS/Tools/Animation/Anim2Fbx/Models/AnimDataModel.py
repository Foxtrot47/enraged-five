"""
Model for displaying a hierarchy of anim files
"""
import cPickle

from PySide import QtCore, QtGui

from RS.Tools.UI import QPrint
from RS.Tools.Animation.Anim2Fbx.Models.AnimData import AnimData


class AnimMimeData(QtCore.QMimeData):
    """ Mime Data for sharing model indexes """
    def __init__(self):
        """ Constructor """

        super(AnimMimeData, self).__init__()
        self._ModelIndexes = []

    def indicies(self):
        """ The stored QModelIndexes """
        return self._ModelIndexes

    def setIndicies(self, indicies):
        """
        Stores the indicies within the class

        Arguments:
            indicies (list(PySide.QtGui.QModelIndex)): QModelIndexes to store

        """
        self._ModelIndexes = indicies


class AnimDataModel(QtCore.QAbstractItemModel):
    """ A drag and drop enabled, editable, hierarchical item model."""

    def __init__(self, root=None):
        """ Instantiates the model with a root item. """
        super(AnimDataModel, self).__init__()
        self.root = root or AnimData("root")
        self._topLevelEditable = False

    def Root(self):
        """ The root AnimData of the Model"""
        return self.root

    def setRoot(self, root):
        """
        Sets the root AnimData for the model
        Arguments:
            root (AnimData): AnimData to use as the root for the model

        """
        self.root = root

    def clear(self):
        rowCount = self.root.rowCount()
        self.removeRows(0, rowCount, QtCore.QModelIndex())

    def topLevelEditable(self):
        """ Are the direct children of the root editable """
        return self._topLevelEditable

    def setTopLevelEditable(self, value):
        """
        Sets if the direct children of the root editable

        Arguments:
            value (boolean): should the direct children of the root be editable

        """
        self._topLevelEditable = bool(value)

    def itemFromIndex(self, index):
        """ Returns the TreeItem instance from a QModelIndex. """
        return index.internalPointer() if index.isValid() else self.root

    def indexFromItem(self, item):
        parentItem = item.parent()
        if parentItem is None:
            return QtCore.QModelIndex()

        return self.createIndex(item.row(), 0, item)

    def rowCount(self, index):
        """ Returns the number of children for the given QModelIndex. """
        item = self.itemFromIndex(index)
        return len(item.children())

    def columnCount(self, index):
        """ This model will have only one column. """
        return 2

    def flags(self, index):
        """
        flags that tell the view how to handle the items

        Arguments:
            index (QtCore.QModelIndex): model index for the current item

        """
        # Invalid indices (open space in the view) are also drop enabled, so you can drop items onto the top level.
        if not index.isValid() and self._topLevelEditable:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDropEnabled
        elif not index.isValid() and not self._topLevelEditable:
            return QtCore.Qt.ItemIsEnabled

        # For items to be drag and drop enabled they need to return the
        # QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsDragEnabled flags

        return self.itemFromIndex(index).flags(index)

    def headerData(self, section, orientation, role):
        """ Return the header title."""
        if role == QtCore.Qt.DisplayRole:
            return ["Path", "Frame", "Length"][section]

    def data(self, index, role):
        """
        data from the given index

        Arguments:
            index (QtCore.QModelIndex): modelIndex to get data from
            role (QtCore.RoleType): role being requested

        Return:
            object
        """
        if not index.isValid():
            return None

        if role != QtCore.Qt.EditRole:
            item = index.internalPointer()
            if item:
                return item.data(index, role)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        Sets the value of the data for the given index based on the provided
        role

        Arguments:
            index (QtGui.QModelIndex): the model index associated with this object
            value (object): value to set
            role (QtCore.Qt.Role): the role that is being requested by the View that owns this model

        """
        # Make sure that the model index is valid
        if not index.isValid():
            return False
        item = self.itemFromIndex(index)
        column = index.column()

        setFrame = column >= 1 and value.isdigit()
        setName = not column and item.isNameEditable()
        setData = setFrame or setName
        QPrint(setFrame)
        if role == QtCore.Qt.EditRole and setData:
            item.setData(index, value, role)
            self.dataChanged.emit(index, index)
            return True

        return False

    def index(self, row, column, parentIndex):
        """ Creates a QModelIndex for the given row, column, and parent. """
        if not self.hasIndex(row, column, parentIndex):
            return QtCore.QModelIndex()

        parent = self.itemFromIndex(parentIndex)
        return self.createIndex(row, column, parent.childAtRow(row))

    def parent(self, index):
        """ Returns a QModelIndex for the parent of the item at the given index. """
        item = self.itemFromIndex(index)
        parent = item.parent()
        if parent in (self.root, None):
            return QtCore.QModelIndex()

        return self.createIndex(parent.row(), 0, parent)

    # Required to enable Drag and Drop Actions

    def supportedDragActions(self):
        """ Items can be moved and copied (but we only provide an interface for moving items in this example. """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction

    def supportedDropActions(self):
        """ Items can be moved and copied (but we only provide an interface for moving items in this example. """
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction

    # Required for Move Operations
    
    def insertRows(self, row, count, parentIndex):
        """ Add a number of rows to the model at the given row and parent. """
        self.beginInsertRows(parentIndex, row, row+count-1)
        self.endInsertRows()
        return True

    def removeRows(self, row, count, parentIndex):
        """ Remove a number of rows from the model at the given row and parent. """
        self.beginRemoveRows(parentIndex, row, row+count-1)
        parent = self.itemFromIndex(parentIndex)
        for _ in xrange(count):
            parent.removeChildAtRow(row)
        self.endRemoveRows()
        return True
    
    def mimeTypes(self):
        """ The MimeType for the encoded data. """
        return ['text/plain', u'application/x-qabstractitemmodeldatalist', u'application/x-qstandarditemmodeldatalist']

    def mimeData(self, indices):
        """ Encode serialized data from the item at the given index into a QMimeData object. """

        data = ''
        items = []
        [items.append(self.itemFromIndex(index)) for index in indices if self.itemFromIndex(index) not in items]

        try:
            data += cPickle.dumps(items)
        except Exception, e:
            print e
            pass

        mimedata = AnimMimeData()
        mimedata.setData('text/plain', data)
        mimedata.setIndicies(indices)
        return mimedata

    def dropMimeData(self, mimedata, action, row, column, parentIndex):
        """
        Handles the dropping of an item onto the model.

        De-serializes the data into a TreeItem instance and inserts it into the model.
        """

        # Get parent from the parent index
        dropParent = self.itemFromIndex(parentIndex)
        row = row if (row, column) != (-1, -1) else None
        externalData = False

        if dropParent.isFile():
            return False

        elif mimedata.hasFormat(u'application/x-qstandarditemmodeldatalist'):
            model = QtGui.QStandardItemModel()
            model.dropMimeData(mimedata, QtCore.Qt.CopyAction, 0, 0, QtCore.QModelIndex())
            items = [self.StandardItemToAnimData(model.item(index, 0)) for index in xrange(model.rowCount())]
            externalData = True

        elif hasattr(mimedata, "indicies"):
            items = [self.itemFromIndex(index) for index in mimedata.indicies() if index.parent().isValid()]

        elif mimedata.hasFormat('text/plain'):
            try:
                items = cPickle.loads(str(mimedata.data('text/plain')))
            except EOFError:
                items = []

        else:
            return False

        self.moveItems(parentIndex, items, row=row, copy=externalData)

        return True

    def moveItems(self, parentIndex, items, row=None, copy=False, drop=True):
        """
        Moves an items within the tree model

        Arguments:
            parentIndex (QtCore.QModelIndex): the parent model index
            items (AnimData): AnimData items to move to a new parent
            row (int): row to insert items at
            copy (boolean): should the items not be removed from their original
            drop (boolean): should the items be added under the new parent

        """
        parent = self.itemFromIndex(parentIndex)
        rowCount = parent.rowCount()
        for item in items:
            itemRow = item.row()
            itemParent = item.parent()
            if not copy:
                item.parent().removeChildAtRow(item.row())

            if row is None and drop:
                parent.appendChild(item)

            elif drop:
                parent.addChildAtRow(item, row - (itemRow < row and itemParent == parent))

        if rowCount > parent.childCount():
            self.removeRows(parent.childCount()-1, 1, parentIndex)

        else:
            self.insertRows(parent.childCount()-1, 1, parentIndex)

        self.dataChanged.emit(parentIndex, parentIndex)

    def StandardItemToAnimData(self, standardItem):
        """
        Converts StandardItems used in the FileTreeWidget to AnimData models:

        Arguments:
            standardItem (QtGui.QStandardItem): QStandardItem to convert into AnimData

        Return:
            AnimData

        """
        animData = AnimData(standardItem.toolTip())
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers != QtCore.Qt.AltModifier:
            for index in xrange(standardItem.rowCount()):
                child = self.StandardItemToAnimData(standardItem.child(index))
                animData.appendChild(child)
        return animData
