"""
Widget for selecting characters in the scene and skeleton files to use
"""
import os

from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Config, ProjectData, Perforce
from RS.Tools.Animation.Anim2Fbx.Widgets import FileTree

CHARACTER_ICON = os.path.join(Config.Script.Path.ToolImages, "MotionBuilder", "character.png")
MODEL_ICON = os.path.join(Config.Script.Path.ToolImages, "MotionBuilder", "model.png")
SKELETON_DIRECTORY = ProjectData.data.GetSkelFilesDirectory()
IGNORE_NAMESPACES = ("RS_Null", "REFERENCE")

# Make sure
Perforce.Sync(os.path.join(SKELETON_DIRECTORY, "..."), force=True)


class SelectionWidget(QtGui.QWidget):
    """
    Widget for selecting FBCharacters in the scene interactively without having to go to the Navigator and for
    selecting which skeleton files to use when importing animation
    """
    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget

        """
        super(SelectionWidget, self).__init__(parent=parent)

        layout = QtGui.QVBoxLayout()
        self.__block = False
        self._keepInSync = True

        self.splitter = QtGui.QSplitter()
        self.splitter.setHandleWidth(10)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.namespaceModel = QtGui.QStandardItemModel()

        self.namespaceView = QtGui.QTreeView()
        self.namespaceView.setModel(self.namespaceModel)
        self.namespaceView.setSelectionMode(self.namespaceView.ExtendedSelection)
        self.namespaceView.setEditTriggers(self.namespaceView.NoEditTriggers)

        self.skeletonTree = FileTree.FileTreeWidget(title="Skeleton Files", directory=SKELETON_DIRECTORY)
        self.skeletonTree.addFileExtension(".skel")
        self.skeletonTree.setExpandMinimum(0)
        self.skeletonTree.tree.setSelectionMode(self.skeletonTree.tree.SingleSelection)
        self.skeletonTree.PopulateView()

        self.splitter.addWidget(self.namespaceView)
        self.splitter.addWidget(self.skeletonTree)
        layout.addWidget(self.splitter)

        self.UpdateNamespaceList()

        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)
        self.skeletonTree.Select(os.path.join(SKELETON_DIRECTORY, "human", "biped.skel"))
        if not self.SkelFile():
            self.skeletonTree.Select(os.path.join(SKELETON_DIRECTORY, "human", "player.skel"))

    def AddMotionBuilderCallbacks(self, *_):
        """ Adds motion builder callback so whenever a selection is made the widget characters selection is updated """
        mobu.FBSystem().Scene.OnChange.Add(self._selectionCallback)
        mobu.FBApplication().OnFileOpenCompleted.Add(self._selectionCallback)

    def RemoveMotionBuilderCallbacks(self, *_):
        """
        Removes motion builder callback so whenever a selection is made the widget characters selection is updated
        """
        mobu.FBSystem().Scene.OnChange.Remove(self._selectionCallback)
        mobu.FBApplication().OnFileOpenCompleted.Remove(self._selectionCallback)
        mobu.FBApplication().OnFileExit.Remove(self.RemoveMotionBuilderCallbacks)

    def UpdateNamespaceList(self, *_):
        """ Updates the Character Tree View based on the current selection in motion builder """
        self.__block = True
        self.namespaceModel.clear()
        characterNamespaces = [character.LongName.split(":", 1)[0] for character in mobu.FBSystem().Scene.Characters
                               if ":" in character.LongName]

        validNamespaces = {namespace.Name: namespace for namespace in mobu.FBSystem().Scene.Namespaces if
                           not (namespace.Name.endswith("_Ctrl") or namespace.Name in IGNORE_NAMESPACES or
                           isinstance(mobu.FBFindModelByLabelName(namespace.Name), mobu.FBCamera))}
        validNamespaces = [validNamespaces[key] for key in sorted(validNamespaces.keys())]
        selectionModel = self.namespaceView.selectionModel()

        for namespace in validNamespaces:

            icon = (MODEL_ICON, CHARACTER_ICON)[namespace.Name in characterNamespaces]
            item = QtGui.QStandardItem()
            item.setText(namespace.Name)
            item.setIcon(QtGui.QIcon(icon))

            # Tagging that namespace this item represents belongs to character
            item.setData(namespace.Name in characterNamespaces, QtCore.Qt.UserRole)

            self.namespaceModel.appendRow(item)

            components = mobu.FBComponentList()
            namespace.GetContentList(components)

            for component in components:
                if component.Selected: 
                    modelIndex = self.namespaceModel.indexFromItem(item)
                    selectionModel.select(modelIndex, selectionModel.Select)
                    break

        if not selectionModel.selection() and self.namespaceModel.index(0,0).isValid():
            selectionModel.select(self.namespaceModel.index(0, 0), selectionModel.Select)

        self.namespaceModel.setHeaderData(0, QtCore.Qt.Horizontal, "Characters")

        self.__block = False

    def _selectionCallback(self, control, event):
        """
        callback for character selection

        Arguments:
            control (): pydbsdk UI component calling this callback
            event (pyfbsdk.FBEvent): event being called that is activating this callback
        """

        # Don't run when Sync is turned off

        if not self._keepInSync:
            return

        events = (mobu.FBSceneChangeType.kFBSceneChangeSelect,
                  mobu.FBSceneChangeType.kFBSceneChangeUnselect,
                  mobu.FBSceneChangeType.kFBSceneChangeChangeName,
                  mobu.FBSceneChangeType.kFBSceneChangeRenameUnique,
                  mobu.FBEventName.kFBEventFileOpenCompleted)
        # Try to update the selection widget, if it doesn't exist then remove the callback
        try:
            if event.Type in events:
                self.UpdateNamespaceList()
        except:
            self.RemoveMotionBuilderCallbacks()

    def Namespaces(self):
        """ Selected Namespaces in the scene """
        return [index.data() for index in self.namespaceView.selectionModel().selectedIndexes()]

    def Characters(self):
        """ Selected Namespaces in the scene that belong to FBCharacters """
        return [index.data() for index in self.namespaceView.selectionModel().selectedIndexes()
                if index.data(QtCore.Qt.UserRole) is True]

    def SkelFile(self):
        """ Selected Skel File """
        if self.skeletonTree.selectedItems:
            index = self.skeletonTree.filter.mapToSource(self.skeletonTree.selectedItems[0])
            skelFile = self.skeletonTree.model.itemFromIndex(index).toolTip()
            return "x:\\{}".format(skelFile[2:].replace("depot/", "").replace("/", os.path.sep))

    def sizes(self):
        """
        Size for the widgets in the QSplitter
        Return:
            list[int, int]
        """
        return self.splitter.sizes()

    def setSizes(self, sizes):
        """
        Sets the size of the widgest in the QSplitter

        Arguments:
            sizes (list[int, int]): list of sizes to set the widgets to

        """
        self.splitter.setSizes(sizes)
