from PySide import QtCore, QtGui
#import RS.Config
#import RS.Tools.Resources.resources
#import os

class animNode(object):

    def __init__(self, name, parent =None, nodePath = None, animBranch = None):

        self._name = name
        self._children = []
        self._parent = parent

        self._path = nodePath
        self._branch = animBranch

        if parent is not None:
            parent.addChild(self)

    def typeInfo(self):
        return "NodeType"

    def addChild(self, child):
        self._children.append(child)
        
    def insertChild(self, position, child):
        if position  < 0  or position > len(self._children):
            return False
        
        self._children.insert(position, child)
        child._parent = self
        return True
        
    def removeChild(self, position):
        if position <0 or position >len(self._children):
            return False
        
        child = self._children.pop(position)
        child._parent = None
        
        return True

    # getters	
    def name(self):
        return self._name

    def displayName(self):
        lFileName = str(self._name)

        fName = lFileName.replace('%40','@')		
        return fName

    def child(self, row):
        if row != -1 and row < self.childCount():
            return self._children[row]

    def childCount(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def path(self):
        return self._path
    
    def displayPath(self):
        lFileName = str(self._path)

        fName = lFileName.replace('%40','@')		
        return fName        

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)      
        
    def branch(self):
        return self._branch
    
    # setters	
    def setName(self, name):
        self._name = name



class animPathFolderNode(animNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(animPathFolderNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "File Folder"

class animPathFileNode(animNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(animPathFileNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "anim depot File"

class animPathWorkSpaceFileNode(animNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(animPathWorkSpaceFileNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "anim local File"
    
class animSelectionSetNode(animNode):

    def __init__(self, name, parent = None, nodePath = None, animBranch = None):
        super(animSelectionSetNode, self).__init__(name, parent, nodePath, animBranch)

    def typeInfo(self):
        return "Selection Set"    