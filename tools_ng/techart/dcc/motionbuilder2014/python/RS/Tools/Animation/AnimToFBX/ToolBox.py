import os

import pyfbsdk as mobu

from PySide import QtCore

# Import the Controllers/Widgets we will add to the ToolBox Layouts
import RS.Tools.UI
import RS.Tools.Animation.AnimToFBX.Controllers.AnimUtils as utils
import RS.Tools.Animation.AnimToFBX.Controllers.QTUtils as QTUtils
import RS.Tools.Animation.AnimToFBX.Controllers.ToolBox as TBox
import RS.Tools.Animation.AnimToFBX.Controllers.PoseTabWidget as Pose
import RS.Tools.Animation.AnimToFBX.Controllers.AnimTreeWidget as Anim
import RS.Tools.Animation.AnimToFBX.Controllers.SequencingTabWidget as Seq
import RS.Tools.Animation.AnimToFBX.Controllers.SelectionListWidget as List
import RS.Tools.Animation.AnimToFBX.Controllers.SelectionSetsWidget as Sets
from RS.Tools.UI.Commands import QPrint
from RS.Utils import Scene

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)

STRING_FILTER_TYPE = {
   mobu.FBCharacter: "Character",
   mobu.FBCamera: "Camera",
   mobu.FBModel: "Prop",
    }

# Compile and import the Qt Designer Layout
RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\ToolBox_QT.ui'.format(RS.Config.Script.Path.Root),
                '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\ToolBox_QT.py'.format(RS.Config.Script.Path.Root))
import RS.Tools.Animation.AnimToFBX.ViewWidgets.ToolBox_QT as tBox
reload(tBox)


class ToolBox(RS.Tools.UI.QtMainWindowBase, tBox.Ui_Form):
    def __init__(self):
        RS.Tools.UI.QtMainWindowBase.__init__(self, None, title='Anim to FBX')

        # Required to create the ui from Qt Designer
        self.setupUi(self)

        # Add the Rockstar Banner, after the ToolBox ui has been initialized
        self._banner = RS.Tools.UI.BannerWidget(name='Anim to FBX',
                                                helpUrl=("https://rsgediwiki1/bob/index.php/"
                                                         "Editing_and_converting_exported_.anim_files_back_into_FBX"))

        self.lytBanner.addWidget(self._banner)

        # Add the AnimTreeWidget
        self._animTree = Anim.AnimTreeWidget()
        self.lytAnimTree.addWidget(self._animTree)

        # Add the SelectionListWidget
        self._listWidget = List.SelectionListWidget()
        self.lytSelectionList.addWidget(self._listWidget)

        # Add the SelectionSetsWidget
        self._setsWidget = Sets.SelectionSetsWidget()
        self.lytSelectionSets.addWidget(self._setsWidget)

        # Add the SequencingTabWidget
        self._sequencingWidget = Seq.SequencingTabWidget()
        self.lytSequencingTab.addWidget(self._sequencingWidget)

        # Add the PoseTabWidget
        self._poseTab = Pose.PoseTabWidget()
        self.lytPosesTab.addWidget(self._poseTab)

        # add/remove to Selection List
        self.addToSelectionButton.pressed.connect(self.AddToSelList)
        self.removeFromSelectionButton.pressed.connect(self.RemoveFromSelList)

        # connecting mapper widgets
        self.ConnectListMapperWidget()

        self.ConnectSetsMapperWidget()
        # self._setsWidget.setModel(self._setsWidget._selSetModel)
        # QtCore.QObject.connect(self._setsWidget.SetListView.selectionModel(),
        # QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._setsWidget.setSelection)

        self._poseTab.setModel(self._poseTab._poseModel)
        QtCore.QObject.connect(self._poseTab.poseLibTreeView.selectionModel(),
                               QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._poseTab.setSelection)

        self._listWidget.lLoadSelectionList.pressed.connect(self.lLoadSelection)
        self._listWidget.clearList.pressed.connect(self.ClearSelectionList)
        self._listWidget.lSaveSelSetList.pressed.connect(self.SaveSelList)

        self._setsWidget.lLoadSelSetList.pressed.connect(self.LoadSelList)
        self._setsWidget.lDelSelSetList.pressed.connect(self.DeleteSelectionSet)
        self._setsWidget.lDeleteAllSets.pressed.connect(self.DeleteAllSetNodes)

        # Facial/Prop/Camera import buttons
        self.lLoadCharButton.pressed.connect(self.BrowseAnims)
        self.lLoadFacialButton.pressed.connect(lambda *args: self.ApplyAnimation("File", False,mobu.FBCharacter))
        self.lLoadFacialFolderButton.pressed.connect(lambda *args: self.ApplyAnimation("Folder", False,mobu.FBCharacter))
        self.lLoadPropButton.pressed.connect(lambda *args: self.ApplyAnimation("File", False,mobu.FBModel))
        self.lLoadPropFolderButton.pressed.connect(lambda *args: self.ApplyAnimation("Folder", False,mobu.FBModel))
        self.lLoadCameraAnimButton.pressed.connect(lambda *args: self.ApplyAnimation("File", False,mobu.FBCamera))

        # Import Pose tab buttons
        self.lPoseAnimButton.pressed.connect(self.MalePoseFileAnim)
        self.lPoseAnimFolderButton.pressed.connect(self.MalePoseFolderAnims)
        self.lFemalePoseAnimButton.pressed.connect(self.FemalePoseFileAnim)
        self.lFemalePoseAnimFolderButton.pressed.connect(self.FemalePoseFolderAnims)

    def AddToSelList(self):
        TBox.AddToSelList(self)

    def RemoveFromSelList(self):
        TBox.RemoveFromSelList(self)

    def ClearSelectionList(self):
        TBox.ClearSelectionList(self)

    def SaveSelList(self):
        TBox.SaveSelList(self)

    def LoadSelList(self):
        TBox.LoadSelList(self)

    def DeleteSelectionSet(self):
        TBox.DeleteSelectionSet(self)

    def SaveSelSet(self):
        TBox.SaveSelSet(self)

    def DeleteHistory(self, warn=True):
        TBox.DeleteHistory(warn)

    def SaveSetHistory(self):
        TBox.SaveSetHistory()

    def lLoadSelection(self):
        namespaces = Scene.GetNamespacesFromSelection(filterByType=mobu.FBCharacter)

        if not self.CheckSelection(componentType=mobu.FBCharacter) or not self.CheckSelectionIsReferenced(namespaces,
                                                                                                    mobu.FBCharacter):
            return

        TBox.lLoadSelection(self, namespaces=namespaces)

    def DeleteAllSetNodes(self):
        TBox.DeleteAllSetNodes(self)

    def ConnectListMapperWidget(self):
        # connecting mapper widgets
        self._listWidget.setModel(self._listWidget._selModel)
        QtCore.QObject.connect(self._listWidget.selectionListView.selectionModel(),
                               QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._listWidget.setSelection)

    def ConnectSetsMapperWidget(self):
        # connecting mapper widgets
        self._setsWidget.setModel(self._setsWidget._selSetModel)
        QtCore.QObject.connect(self._setsWidget.SetListView.selectionModel(),
                               QtCore.SIGNAL("currentChanged(QModelIndex, QModelIndex)"), self._setsWidget.setSelection)

    def BrowseAnims(self):
        if not self.CheckSelection():
            return
        TBox.browseAnims(self)

    def MalePoseFileAnim(self):
        """
        Adds a pose to the pose control , gender should be irrelevant but we have the gender specific buttons
        """
        # useTake = QTUtils.GetAnimTreeCheckboxValue(self, "cbImportToCurrentTake")
        # sync = QTUtils.GetAnimTreeCheckboxValue(self, "syncCheckbox")
        file = utils.SelectFile()
        utils.AddPoseToPoseControl(file)

    def MalePoseFolderAnims(self):
        """
        Adds poses to the pose control , gender should be irrelevant but we have the gender specific buttons
        """
        # useTake = QTUtils.GetAnimTreeCheckboxValue(self, "cbImportToCurrentTake")
        # sync = QTUtils.GetAnimTreeCheckboxValue(self, "syncCheckbox")

        folder = utils.SelectFolder()
        utils.AddPosesToPoseControl(folder)

    def FemalePoseFileAnim(self):
        """
        Adds a pose to the pose control , gender should be irrelevant but we have the gender specific buttons
        """
        # useTake = QTUtils.GetAnimTreeCheckboxValue(self, "cbImportToCurrentTake")
        # sync = QTUtils.GetAnimTreeCheckboxValue(self, "syncCheckbox")

        self.MalePoseFileAnim()

    def FemalePoseFolderAnims(self):
        """
        Adds poses to the pose control , gender should be irrelevant but we have the gender specific buttons
        """
        # useTake = QTUtils.GetAnimTreeCheckboxValue(self, "cbImportToCurrentTake")
        # sync = QTUtils.GetAnimTreeCheckboxValue(self, "syncCheckbox")

        self.MalePoseFolderAnims()

    def GetSkeletonPath(self):
        """
        Builds the skeleton path based on the current selection from the skeleton combo boxes

        Return:
            string
        """
        skelFile = ""
        if QTUtils.GetAnimTreeCheckboxValue(self, "useSkelCheckbox"):
            skelFile = os.path.join(RS.Config.Project.Path.Assets, "anim", "skeletons", "sp")
            if not os.path.exists(skelFile):
                skelFile = os.path.join(RS.Config.Tool.Path.Root, "etc", "config", "anim", "skeletons", "sp")

            skelFile = os.path.join(skelFile,
                                QTUtils.GetAnimTreeWidget(self, "skelFileTypeComboBox").currentText(),
                                QTUtils.GetAnimTreeWidget(self, "useSkelCombo").currentText())
        return skelFile

    def CheckSelection(self, componentType=mobu.FBCharacter):

        selection = Scene.GetSelection(filterByType=componentType)
        if not selection:
           mobu.FBMessageBox("Warning", ('No {} was selected.\n'
                                     'Note: Characters should be selected through the Navigator'
                                   ).format(STRING_FILTER_TYPE.get(componentType,mobu.FBCharacter)),
                         "OK")
        return selection

    def CheckSelectionIsReferenced(self, namespaces, componentType):
        if not namespaces:
            mobu.FBMessageBox("Warning",
                              'The current selection is not referenced.\nReference your selection and try again'.format(
                              STRING_FILTER_TYPE.get(componentType, mobu.FBCharacter)),
                              "OK")

        elif "" in namespaces:
            mobu.FBMessageBox("Warning", ('There are components in your current selection that are not referenced.\n'
                                          'Only the refernced components will have animation applied to them').format(
                STRING_FILTER_TYPE.get(componentType, mobu.FBCharacter)), "OK")
        return namespaces

    def ApplyAnimation(self, fileType="file", multiple=False, componentType=mobu.FBCharacter):

        namespaces = Scene.GetNamespacesFromSelection(filterByType=componentType)

        if componentType is not mobu.FBCamera and (not self.CheckSelection(componentType=componentType) \
                                              or not self.CheckSelectionIsReferenced(namespaces, componentType)):
            return

        methodName = "Load{}{}ByType".format(fileType.capitalize(), "s"*multiple)
        getattr(utils, methodName)(componentType, skeletonPath=self.GetSkeletonPath(), namespaces=namespaces)


def Run(show=True):
    tool = ToolBox()

    if show:
        tool.show()

    return tool