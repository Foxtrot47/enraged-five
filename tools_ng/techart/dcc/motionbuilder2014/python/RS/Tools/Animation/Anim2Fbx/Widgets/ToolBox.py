"""
The main widget of the Anim2Fbx
"""
import os
import webbrowser

from functools import partial 

import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import ProjectData, Config, Perforce
from RS.Core import DatabaseConnection
from RS.Utils import LocalSettings
from RS.Tools.UI import Run
from RS.Tools.UI.Media.Widgets import Player
from RS.Core.Animation.Anim2Fbx import Load
from RS.Tools.Animation.Anim2Fbx.Models import ModelManager
from RS.Tools.Animation.Anim2Fbx.Widgets import FileTree
from RS.Tools.Animation.Anim2Fbx.Widgets import SetsWidget
from RS.Tools.Animation.Anim2Fbx.Widgets import AnimFileTree
from RS.Tools.Animation.Anim2Fbx.Widgets import SelectionWidget
from RS.Tools.Animation.Anim2Fbx.Widgets import CollapsableWidget

PROGRESSBAR_COLOR, TEXT_COLOR  = FileTree.PROGRESSBAR_COLOR, FileTree.TEXT_COLOR

PROJECT_PATH = {
    mobu.FBModel: os.path.join(Config.Project.Path.Assets, "cuts")
    }

TYPE_DICTIONARY = {mobu.FBCamera: "Camera",
                   mobu.FBModel: "Props",
                   mobu.FBCharacter: "Character"}

ANIMATION_DATABASE = None
if Config.User.IsInternal or Config.Project.Name=="GTA5":
    ANIMATION_DATABASE = DatabaseConnection.DatabaseConnection(driver="{SQL Server}", server="NYCW-MHB-SQL",
                                                               database="AnimationStats_{}".format(Config.Project.Name))


class ToolBox(QtGui.QWidget):
    """
    Main widget for the Anim2Fbx Tool
    Sets up the logic that is shared between the different widgets
    """
    ImportOptions = ("Animation", "First Frame to Pose Control", "Last Frame to Pose Control",
                     "Set Frames to Pose Control", "Sequence", "Nothing")

    DEFAULT_VIDEO_PATH = os.path.join(Config.Script.Path.ToolImages, "VideoPlayer", "error.mp4")

    def __init__(self, parent=None):
        """
        Constructor

        Arguments:
            parent (QtGui.QWidget): parent widget
        """
        super(ToolBox, self).__init__(parent=parent)
        self.settings = LocalSettings.Settings(directory=os.path.dirname(os.path.dirname(__file__)))
        self._databaseConnection = None
        self._previewActionString = self.tr("Preview")

        showLocal = self.settings.Get("ShowLocal", True)
        self.useSetWidget = False

        self.splitter = QtGui.QSplitter()
        self.setsSplitter = QtGui.QSplitter()
        self.setsSplitter.setOrientation(QtCore.Qt.Vertical)

        # Set Main Portion of the UI
        self.tree = AnimFileTree.AnimFileTree()
        self.tree.localCheckBox.setChecked(showLocal)
        self.tree.fileTree.ShowLocal = showLocal

        for project, directory in ProjectData.data.GetAnimationDirectories():
            self.tree.AddComboBoxEnum(project, directory)
        self.tree.localCheckBox.clicked.connect(self.ShowLocal)

        self.tree.fileTree.tree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tree.fileTree.selectionChangedSignal.connect(self.setUseSetWidget)
        self.tree.fileTree.lastItemSelected.connect(self.setPreview)

        self.tree.fileTree.tree.customContextMenuRequested.connect(self.AnimFileTreeContextMenu)
        self.tree.doubleClickSignal.connect(self.LoadByDoubleClick)
        self.tree.setAutoFilter(not self.settings.Get("disableAutoFilter", False))
        self.tree.setProgressBar(self.settings.Get("disableProgressBar", False))

        # Characters and SkelBox
        characterSkelCollapsableWidget = CollapsableWidget.CollapsableWidget(
            direction=CollapsableWidget.CollapsableWidget.Right)
        self.selectionCollapsableWidget = characterSkelCollapsableWidget

        self.selection= SelectionWidget.SelectionWidget()
        characterSkelCollapsableWidget.addWidget(self.selection)

        # Advance Options Collapsable Widget
        optionsCollapsableWidget = CollapsableWidget.CollapsableWidget(
            direction=CollapsableWidget.CollapsableWidget.Left)
        self.setsCollapsableWidget = optionsCollapsableWidget

        self.sets = SetsWidget.SetsWidget()
        self.preview = Player.VideoPlayer(instanceKey="Anim2Fbx")

        # Weird bug is causing Motion Builder to crash if the Video Widget ever happens to reach the size of 0
        # while a video is playing, this particular minimum size prevents the widget from ever reaching a size of 0
        self.preview.setMinimumSize(QtCore.QSize(50, 150))
        self.preview.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.preview.path = self.DEFAULT_VIDEO_PATH
        self.preview.setLoop(self.settings.Get("enableLooping", True))

        previewLayoutWidget = QtGui.QFrame()
        previewLayout = QtGui.QVBoxLayout()
        previewLabel = QtGui.QLabel()
        previewLabel.setText("Preview")
        previewLabel.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        previewLabel.setFixedHeight(23)
        previewLayoutWidget.setFrameStyle(QtGui.QFrame.StyledPanel)

        previewLayout.addWidget(previewLabel)
        previewLayout.addWidget(self.preview)

        previewLayout.setSpacing(0)
        previewLayout.setContentsMargins(0, 0, 0, 0)

        previewLayoutWidget.setLayout(previewLayout)
        previewLayoutWidget.setStyleSheet(
            "QFrame{"
            "background-color: #2a2a2a;"
            "color: #424242;"
            "}"
            "QLabel {"
            "margin: 1px;"
            "border-color: #2a2a2a;"
            "border-style: outset;"
            "border-radius: 1px;"
            "border-width: 1px;"
            "color: #c8c8c8;"
            "background-color: #636363;"
            "}"
            )

        self.setsSplitter.addWidget(previewLayoutWidget)
        self.setsSplitter.addWidget(self.sets)
        self.setsSplitter.setHandleWidth(10)
        self.setsCollapsableWidget.addWidget(self.setsSplitter)

        self.sets.tree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.sets.tree.customContextMenuRequested.connect(self.SetsContextMenu)
        self.sets.doubleClickSignal.connect(lambda *args: self.LoadByDoubleClick(useSetSelection=True))
        self.sets.selectionChangedSignal.connect(self.setUseSetWidget)
        self.sets.lastItemSelected.connect(self.setPreview)
        self.splitter.addWidget(characterSkelCollapsableWidget)
        self.splitter.addWidget(self.tree)
        self.splitter.addWidget(self.setsCollapsableWidget)

        self.splitter.setHandleWidth(10)

        self.animationButton = QtGui.QPushButton()
        self.poseButton = QtGui.QPushButton()
        self.sequenceButton = QtGui.QPushButton()

        self.animationButton.setText("Load Animation")
        self.poseButton.setText("Load Pose To Pose Control")
        self.sequenceButton.setText("Load Sequence")

        self.animationButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.poseButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.sequenceButton.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addWidget(self.sequenceButton)
        buttonLayout.addWidget(self.animationButton)
        buttonLayout.addWidget(self.poseButton)
        buttonLayout.setContentsMargins(0, 0, 0, 0)
        buttonLayout.setSpacing(0)

        # Set up motion builder callbacks
        mobu.FBApplication().OnFileExit.Add(self.selection.RemoveMotionBuilderCallbacks)
        self.selection.AddMotionBuilderCallbacks()

        self.animationButton.clicked.connect(lambda *args: self.Load(useSetSelection=self.useSetWidget))
        self.poseButton.clicked.connect(lambda *args: self.Load(poseFrame=0, storePose=True,
                                                                useSetSelection=self.useSetWidget))
        self.sequenceButton.clicked.connect(self.MissingFeatureWarning)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.splitter)
        layout.addLayout(buttonLayout)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        self.setLayout(layout)

        for widget in (self.sets, self.tree, self.animationButton, self.sequenceButton, self.poseButton,
                       self.selectionCollapsableWidget):
            widget.setStyleSheet("QPushButton{"
                                 "font: bold 11px;"
                                 "background-color: #535353;"
                                 "}")

    def CreateMenu(self):
        """ Creates a menu bar for the widget """
        self.menuBar = QtGui.QMenuBar(self)

        self.importMenu = QtGui.QMenu("Single Anim Import")
        self.settingsMenu = QtGui.QMenu("Settings")

        self.menuBar.addMenu(self.settingsMenu)
        self.menuBar.addMenu(self.importMenu)

        self.useCurrentTake = QtGui.QAction("Import animation to current take", self)
        self.syncLatestAnim = QtGui.QAction("Sync .anim(s) to latest revision", self)
        self.runGimbalKiller = QtGui.QAction("Run 'Gimbal Killer' filter on Mover", self)
        self.disableProgressBar = QtGui.QAction("Disable ProgressBar", self)
        self.disableAutoFilter = QtGui.QAction("Disable automatic filtering", self)
        self.disableVideoPreview = QtGui.QAction("Disable Video Preview", self)
        self.enableLooping = QtGui.QAction("Enable looping of the video preview", self)
        self.importOptionGroup = QtGui.QActionGroup(self)

        self.importActions = []

        checked = False
        checkAction = self.settings.Get("doubleClickOption", 6)
        for index, title in enumerate(self.ImportOptions):
            self.importActions.append(QtGui.QAction("Import {}".format(title), self))
            self.importOptionGroup.addAction(self.importActions[-1])
            self.importActions[-1].setCheckable(True)
            if index == checkAction:
                checked = True
                self.importActions[-1].setChecked(True)
        if not checked:
            self.importActions[0].setChecked(True)

        self.useCurrentTake.setCheckable(True)
        self.syncLatestAnim.setCheckable(True)
        self.runGimbalKiller.setCheckable(True)
        self.disableProgressBar.setCheckable(True)
        self.disableAutoFilter.setCheckable(True)
        self.disableVideoPreview.setCheckable(True)
        self.enableLooping.setCheckable(True)

        self.useCurrentTake.setChecked(self.settings.Get("useCurrentTake", False))
        self.syncLatestAnim.setChecked(self.settings.Get("syncLatestAnim", True))
        self.runGimbalKiller.setChecked(self.settings.Get("runGimbalKiller", False))
        self.disableProgressBar.setChecked(self.settings.Get("disableProgressBar", False))
        self.disableAutoFilter.setChecked(self.settings.Get("disableAutoFilter", False))
        self.disableVideoPreview.setChecked(self.settings.Get("disableVideoPreview", False))
        self.enableLooping.setChecked(self.settings.Get("enableLooping", True))

        self.disableProgressBar.triggered.connect(self.updateProgressBarEnabledStatus)
        self.disableAutoFilter.triggered.connect(self.updateAnimAutoFilterStatus)

        self.importOptionGroup.triggered.connect(lambda *args: setattr(self.settings, "doubleClickOption",
                                                                       self.importActions.index(
                                                                           self.importOptionGroup.checkedAction())))
        self.enableLooping.triggered.connect(lambda *args: self.preview.setLoop(self.enableLooping.isChecked()))

        self.settingsMenu.addSeparator().setText("Anim2Fbx Settings")
        self.settingsMenu.addAction(self.useCurrentTake)
        self.settingsMenu.addAction(self.syncLatestAnim)
        self.settingsMenu.addAction(self.runGimbalKiller)
        self.settingsMenu.addAction(self.disableProgressBar)
        self.settingsMenu.addAction(self.disableAutoFilter)
        self.settingsMenu.addSeparator().setText("Video Settings")
        self.settingsMenu.addAction(self.disableVideoPreview)
        self.settingsMenu.addAction(self.enableLooping)

        self.settingsMenu.addSeparator().setText("Double Click Settings")
        for action in self.importActions:
            self.settingsMenu.addAction(action)

        characterFileImport = QtGui.QAction("Character File", self)
        characterFolderImport = QtGui.QAction("Character Folder", self)
        facialFileImport = QtGui.QAction("Ambient Facial File", self)
        facialFolderImport = QtGui.QAction("Ambient Facial Folder", self)
        propFileImport = QtGui.QAction("Prop/Weapon/Vehicle File", self)
        propFolderImport = QtGui.QAction("Prop/Weapon/Vehicle Folder", self)
        cameraImport = QtGui.QAction("Camera File", self)

        poseFile = QtGui.QAction("Pose from Character File", self)
        poseFolder = QtGui.QAction("Pose from Character Folder", self)

        characterFileImport.triggered.connect(self.LoadByType)
        characterFolderImport.triggered.connect(lambda *args: self.LoadByType(selectFile=False))
        facialFileImport.triggered.connect(self.LoadByType)
        facialFolderImport.triggered.connect(lambda *args: self.LoadByType(selectFile=False))
        propFileImport.triggered.connect(lambda *args: self.LoadByType(componentType=mobu.FBModel))
        propFolderImport.triggered.connect(lambda *args: self.LoadByType(componentType=mobu.FBModel, selectFile=False))
        cameraImport.triggered.connect(lambda *args: self.LoadByType(componentType=mobu.FBCamera))

        poseFile.triggered.connect(lambda *args: self.LoadByType(poseFrame=0, storePose=True))
        poseFolder.triggered.connect(lambda *args: self.LoadByType(poseFrame=0, selectFile=False, storePose=True))

        self.importMenu.addAction(characterFileImport)
        self.importMenu.addAction(characterFolderImport)
        self.importMenu.addAction(facialFileImport)
        self.importMenu.addAction(facialFolderImport)
        self.importMenu.addAction(propFileImport)
        self.importMenu.addAction(propFolderImport)
        self.importMenu.addAction(cameraImport)
        self.importMenu.addSeparator()
        self.importMenu.addAction(poseFile)
        self.importMenu.addAction(poseFolder)

        return self.menuBar

    def AnimFileTreeContextMenu(self, position):
        """
        The context menu for the main .anim file tree

        Arguments:
            position (QtCore.QPosition): location of the mouse within the tree view

        """
        menu = QtGui.QMenu()

        menu.addAction(self._previewActionString,
                       partial(webbrowser.open, "http://nycw-mhb-sql/Pages/Anim/Anim_Poses.aspx"))

        menu.addSeparator().setText("Load Animation/Poses")
        menu.addAction(self.tr("Import Animation"), self.Load)
        menu.addAction(self.tr("Import First Frame To Pose Control"), lambda *args: self.Load(poseFrame=0,
                                                                                              storePose=True))
        menu.addAction(self.tr("Import Last Frame To Pose Control"), lambda *args: self.Load(poseFrame=-1,
                                                                                             storePose=True))

        menu.exec_(self.tree.fileTree.tree.viewport().mapToGlobal(position))

    def SetsContextMenu(self, position):
        """
        The context menu for the .anim file sets tree

        Arguments:
            position (QtCore.QPosition): location of the mouse within the tree view

        """
        menu = QtGui.QMenu()
        menu.addAction(self._previewActionString,
                       partial(webbrowser.open, "http://nycw-mhb-sql/Pages/Anim/Anim_Poses.aspx"))

        menu.addSeparator().setText("Load Animation/Poses")
        menu.addAction(self.tr("Import Animation"), partial(self.Load, useSetSelection=True))
        menu.addAction(self.tr("Import First Frame To Pose Control"), lambda *args: self.Load(poseFrame=0,
                                                                                              storePose=True,
                                                                                              useSetSelection=True))
        menu.addAction(self.tr("Import Last Frame To Pose Control"), lambda *args: self.Load(poseFrame=-1,
                                                                                             storePose=True,
                                                                                             useSetSelection=True))
        menu.addAction(self.tr("Import Set Frames To Pose Control"), self.LoadPosesBySets)
        menu.addAction(self.tr("Import Sequence"), self.MissingFeatureWarning)

        menu.addSeparator().setText("Organization")
        menu.addAction(self.tr("Create Empty Folder"), self.sets.addFolder)
        menu.addAction(self.tr("Delete Selection"),self.sets.deleteSelection)
        menu.exec_(self.sets.tree.viewport().mapToGlobal(position))

    def ShowLocal(self):
        """ Updates the Tree View to show local files on the computer or those from Perforce """

        show = self.tree.localCheckBox.isChecked()
        self.settings.ShowLocal = show
        self.tree.fileTree.ShowLocal = show
        self.tree.UpdateTreeView()

    def FileDialog(self, selectFile=True):
        """
        Opens a file dialog to ask the user which file they want to select

        Arguments:
            selectFolder (bool): if only folders should be selectable

        Return:
            string; path to the selected file
        """

        if selectFile:
            path, _ = QtGui.QFileDialog.getOpenFileName(None, "Select Anim File",
                                                        self.tree.Directory() or Config.Project.Path.Anim, "*.anim")
        else:
            path = QtGui.QFileDialog.getExistingDirectory(None, "Select Anim Folder",
                                                          self.tree.Directory() or Config.Project.Path.Anim)
        return path.replace("/", "\\")

    def GetNamespaceFromSelection(self, selection=[]):
        """
        Gets the namespaces from the given selection of Mobu objects

        Arguments:
            selection (list[pyfbsdk.FBComponent, etc.]): selection from motion builder

        """
        if selection and isinstance(selection[0], basestring):
            namespaces = [namespace.Name for namespace in mobu.FBSystem().Scene.Namespaces
                          if namespace.Name in selection]
        else:
            namespaces = [eachSelection.LongName[:(len(eachSelection.Name) + 1) * -1]
                          for eachSelection in selection if eachSelection.LongName != eachSelection.Name]
        warning = ""
        if not selection:
            warning = ("No component selected\n\n"
                       "Characters can be selected through the character selection list or the Navigator.\n"
                       "For other objects (props, weapons, vehicles, etc.), select a single component from that object"
                       " through the viewport or navigator ")
        elif len(selection) > len(namespaces):
            warning = ("Not all components selected are referenced\n\n"
                       "Components that are not referenced will not have animation applied to them\n"
                       )
        return namespaces, warning

    def CheckContent(self, selection, animPaths, skeletonPath):
        """
        Checks that the provided selection, anim file paths and skeleton paths are all valid.
        A warning dialog is raised if any of the provided values are not valid . Also converts the selection into
        a list of namespaces.

        Arguments:
            selection (list): FBComponents who have namespaces
            animPaths (list): list of paths of anim files or directories containing anim files
            skeletonPath (string): path of .skel file to use to figure out which bones to extract animation from

        Return:
            list(strings), boolean
        """
        namespaces, warning = self.GetNamespaceFromSelection(selection=selection)

        if not skeletonPath or not skeletonPath.endswith(".skel"):
            warning = "".join(("No skeleton file selected\n", warning))
            skeletonPath = None

        if not animPaths:
            warning = "".join(("No anim file selected\n", warning))

        if warning:
            QtGui.QMessageBox.warning(None, "Selection Error", warning)

        return namespaces, all((namespaces, animPaths, skeletonPath))

    def Load(self, startFrame=0, poseFrame=None, storePose=False, useSetSelection=False):
        """ Imports Character Animations from Anim Files """

        selection = self.selection.Namespaces() if not storePose else self.selection.Characters()
        skeletonPath = self.selection.SkelFile()
        selectedFiles = self.tree.SelectedFiles() if not useSetSelection else self.sets.selectedFiles()

        namespaces, valid = self.CheckContent(selection=selection, skeletonPath=skeletonPath, animPaths=selectedFiles)

        if not valid:
            return

        folders, files = Load.Load(selectedFiles, skeletonPath=skeletonPath, namespaces=namespaces,
                  useCurrentTake=self.useCurrentTake.isChecked(), sync=self.syncLatestAnim.isChecked(),
                  startFrame=startFrame, poseFrame=poseFrame, storePose=storePose)

        # Update UI Icons
        if useSetSelection:
            self.sets.updateStateOfSelectedFiles()

        else:

            files.extend([os.path.join(root, file)
                         for folder in folders
                         for root, _, _files in os.walk(folder)
                         for file in _files])

            for file in files:
                _local = file.replace("/", os.sep).lower()

                items = self.tree.fileTree.manager.items
                item = items.get(_local, None)

                if item:
                    state = Perforce.GetFileState(_local)
                    if state:
                        item.setData(state.HeadRevision == state.HaveRevision, ModelManager.AnimStandardItem.Synched)
                        item.setData(True, ModelManager.AnimStandardItem.Perforce)
                    else:
                        item.setData(False, ModelManager.AnimStandardItem.Perforce)

                    item.setData(os.path.exists(file), ModelManager.AnimStandardItem.Local)

    def LoadPosesBySets(self):
        """ Imports Character Poses based on the frame number associated with the file"""

        selection = self.selection.Namespaces()
        skeletonPath = self.selection.SkelFile()
        selectedItems = self.sets.selectedItems()

        namespaces, valid = self.CheckContent(selection=selection, skeletonPath=skeletonPath, animPaths=selectedItems)

        if not valid:
            return

        for selectedItem in selectedItems:
            path = selectedItem.path()
            frame = selectedItem.frames()[0]
            Load.Load([path], skeletonPath=skeletonPath, namespaces=namespaces,
                      useCurrentTake=self.useCurrentTake.isChecked(), sync=self.syncLatestAnim.isChecked(),
                      startFrame=0, poseFrame=int(frame), storePose=True)

    def LoadByType(self, componentType=mobu.FBCharacter, selectFile=True, startFrame=0, poseFrame=None, storePose=False):
        """
        Applies the animation from the file selected  by the user to the currently selected character.

        Arguments:
            type (string): The type of data that you want shown on the file/directory dialog
            skeletonPath (string): path to the .skel file that lists which bones should be plotted
            startFrame (int): frame on which to start the animation
            poseFrame (int): the frame of the pose that you want to plot. If this argument used, then only
                         that pose at the specified frame will be plotted. All other animation data will be
                         ignored.
            useTake (boolean): Clears all animation data from the current take and applies the keyframe data from the
                        file provided when True. The keyframe data is applied a new take when False.
        """
        selection = [each for each in mobu.FBSystem().Scene.Components if isinstance(each, componentType)
                     and each.Selected]
        selectedPath = self.FileDialog(selectFile=selectFile)
        skeletonPath = self.selection.SkelFile()

        if componentType is mobu.FBCamera and selectedPath:
            Load.ImportCamera(selectedPath)
            return

        namespaces, valid = self.CheckContent(selection=selection, skeletonPath=skeletonPath, animPaths=selectedPath)

        if not valid:
            return

        Load.Load([selectedPath], skeletonPath=skeletonPath, namespaces=namespaces,
                  useCurrentTake=self.useCurrentTake.isChecked(), sync=self.syncLatestAnim.isChecked(),
                  componentType=componentType, startFrame=startFrame, poseFrame=poseFrame, storePose=storePose)

    def LoadByDoubleClick(self, useSetSelection=False):
        """
        Loads anim files after double clicking

        Arguments:
            useSetSelection (boolean): should the selection from the Set widget be used instead of the Anim widget

        """
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ShiftModifier:
            return

        checkedAction = self.importOptionGroup.checkedAction()
        if checkedAction is None or checkedAction.text() == "Import Animation":
            self.Load(useSetSelection=useSetSelection)
        elif checkedAction.text() == "Import First Frame to Pose Control":
            self.Load(poseFrame=0, storePose=True, useSetSelection=useSetSelection)
        elif checkedAction.text() == "Import Last Frame to Pose Control":
            self.Load(poseFrame=-1, storePose=True, useSetSelection=useSetSelection)
        elif checkedAction.text() == "Import Set Frames to Pose Control":
            self.LoadPosesBySets() if useSetSelection else self.Load(poseFrame=0, storePose=True)
        elif checkedAction.text() == "Import Sequence":
            self.MissingFeatureWarning()

    @staticmethod
    def MissingFeatureWarning():
        """ Missing Feature Warning """
        QtGui.QMessageBox.warning(None, "Warning", "This feature has yet to be implemented\n"
                                                   "Thank you for your patiance")

    def storeWindowState(self, qsetting):
        """
        Stores settings into the QSettings when the tool closes

        Arguments:
            qsetting (QtGui.QSettings): object to store settings in

        """
        self.settings.SplitterSizes = self.splitter.sizes()
        self.settings.SetsSplitterSizes = self.setsSplitter.sizes()
        self.settings.SelectionSplitterSizes = self.selection.sizes()
        self.settings.SelectionVisible = self.selectionCollapsableWidget.isCenterWidgetVisible()
        self.settings.SetsVisible = self.setsCollapsableWidget.isCenterWidgetVisible()

    def restoreWindowState(self, qsetting):
        """
        Restores settings from the QSettings when the tool opens

        Arguments:
            qsetting (QtGui.QSettings): object to restore settings from

        """

        self.splitter.setSizes(self.settings.Get("SplitterSizes", [26, self.tree.width(), 26]))
        self.setsSplitter.setSizes(self.settings.Get("SetsSplitterSizes", [self.tree.height()/2, self.tree.height()/2]))
        self.selection.setSizes(self.settings.Get("SelectionSplitterSizes", [self.tree.height()/2, self.tree.height()/2]))
        self.selectionCollapsableWidget.click() if self.settings.Get("SelectionVisible", False) else None
        self.setsCollapsableWidget.click() if self.settings.Get("SetsVisible", False) else None

    def closeEvent(self, event):
        """
        Overrides built-in method:
        Unload callbacks here

        Arguments:
            event (QtCore.Event): event associated with this call

        """
        self.selection.RemoveMotionBuilderCallbacks()
        return True

    def updateProgressBarEnabledStatus(self):
        """
        Sets the state of the progress bar for the Anim list based on the settings selected in the UI
        """
        self.tree.setProgressBar(self.disableProgressBar.isChecked())
        self.settings.disableProgressBar = self.disableProgressBar.isChecked()

    def updateAnimAutoFilterStatus(self):
        """
        Updates the state of anim filter to be automatic or not based on the settings selected in the UI
        """
        self.tree.setAutoFilter(not self.disableAutoFilter.isChecked())
        self.settings.disableAutoFilter = self.disableAutoFilter.isChecked()

    def setUseSetWidget(self, widgetClass):
        """
        Sets the flag that determines whether the Load Sequence, Load Animation, and Load Pose To Pose Control
        buttons use the main anim tree or the Anim Sets tree

        Arguments:
            widgetClass (class): class of the widget calling this method

        """
        self.useSetWidget = isinstance(self.sets, widgetClass)
        styleSheet = ("QTreeView{"
                      "border-style: outset;"
                      "border-width: 2px;"
                      "border-color: %s;}"
                      "QTreeView::item:selected {"
                      "background-color:%s;"
                      "color: %s; }") % (PROGRESSBAR_COLOR, PROGRESSBAR_COLOR, TEXT_COLOR)
        # Change highlight color of selected items and TreeView

        self.sets.tree.setStyleSheet(styleSheet * self.useSetWidget)
        self.tree.fileTree.tree.setStyleSheet(styleSheet * (self.useSetWidget is False))

    def setPreview(self, modelIndexes):
        """
        Sets the preview video

        Arguments:
            modelIndexes (list[QtGui.QModelIndex, etc.]): list of the selected

        """
        if not modelIndexes or self.disableVideoPreview.isChecked():
            return

        path = modelIndexes[0].data(QtCore.Qt.ToolTipRole)
        if path.endswith(".anim") and ANIMATION_DATABASE is not None:
            results = ANIMATION_DATABASE.ExecuteQuery("SELECT MoviePath FROM Anims WHERE Path = '{}'".format(path))
            videopath = self.DEFAULT_VIDEO_PATH
            if results:
                videopath = results[0][0]

            self.preview.path = videopath
            self.preview.play()


@Run(title='Anim to FBX', url='https://hub.rockstargames.com/pages/viewpage.action?pageId=48959286')
def Run():
    return ToolBox()
