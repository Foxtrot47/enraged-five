"""
ModelView Item for storing anim file information
"""
import os

from PySide import QtGui, QtCore

from RS import Config, Perforce
from RS.Tools.CameraToolBox.PyCore.Decorators import memorize

class AnimData(object):
    """ ModelView Item for storing anim file information """

    Editable = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable| QtCore.Qt.ItemIsDragEnabled
    DragDropEditable = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled | QtCore.Qt.ItemIsEditable

    Uneditable = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    DragDropUneditable = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled

    def __init__(self, path, parent=None):
        """
        constructor

        Arguments:
            path (string): path to the anim file
            parent (AnimData): parent AnimData

        """
        super(AnimData, self).__init__()
        self.rootItem = None
        self._path = path
        self._name = None
        self._basename = os.path.basename(path)
        self._poseFrames = [0]
        self._length = 0
        self._parent = parent
        self._children = []
        self._nameIsEditable = False
        self._local = False
        self._perforce = False
        self._synched = False

        if not self._path.lower().startswith("x:"):
            self._path = r"x:{}".format(self._path[1:])
        self._path = self._path.replace("/", "\\")

    def path(self):
        """ Path to the .anim file or folder """
        return self._path

    def name(self):
        """ Display name """
        return self._name or self._basename

    def setName(self, name):
        """
        Set the display name

        Arguments:
            name (string): name to display
        """
        self._name = name

    def filename(self):
        """ basename of the full path to the anim file """
        return self._basename

    def frames(self):
        """ The frame numbers for poses """
        return self._poseFrames

    def setFrames(self, frame):
        """
        Sets frame numbers to use for poses

        Arguments:
            frame (int): frame number to add

        """
        if not isinstance(frame, (tuple, list)):
            raise ValueError, "Value is not a list or tuple "

        if any([not isinstance(each, int) for each in frame]):
            raise ValueError, "Not all values are integers"

        self._poseFrames = list(frame)

    def length(self):
        """ frame lenght of the anim """
        return self._length

    def isNameEditable(self):
        """ Is the display name editable """
        return self._nameIsEditable

    def setNameIsEditable(self, isEditable):
        """
        Set the editable state of the display name

        Arguments
            isEditable (boolean): editable state to set for the display name

        """
        self._nameIsEditable = bool(isEditable)

    @memorize.memoized
    def isFile(self):
        """ Is the data stored that of a file """
        return self._path.endswith(".anim")

    def columnCount(self, *args):
        """ Number of Columns """
        return 2

    def headerData( self, section, orientation, role ):
        """
        Return the header title.

        Arguments:
            section (int): column number
            orientation (QtCore.QHorizontal): orienation of the view
            role (QtCore.Qt.Role): role being requested

        Return:
            String
        """
        if section == 0 and orientation == QtCore.Qt.Horizontal:
            if role == QtCore.Qt.DisplayRole:
                return ["Path", "Frame",  "Length"][section]

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        The data owned by this object. The data retured depends on the index and role provided.

        Arguments:
            index (QtGui.QModelIndex): the model index associated with this object
            role (QtCore.Qt.Role): the role that is being requested by the View that owns this model

        """
        # Make sure that the model index is valid
        if not index.isValid():
            return None

        column = index.column()

        # The Display Role is what gets displayed in the TreeView
        # The Edit Role is what gets displayed while the user is editing the TreeView
        if role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):

            if not column:
                return self.name()
            # elif column == 1:
            #     return self._length
            # elif column >= 2:
            #     return self._poseFrames[column-2]
            else:
                return self._poseFrames[column-1]

        # The tool tip role is what gets displayed as the tool tip
        elif role == QtCore.Qt.ToolTipRole:
            return self._path

        # The decoration role is what gets displayed as the image for the data
        elif role == QtCore.Qt.DecorationRole and not column:
            isFile = self.isFile()

            filename = "folder.png"
            if isFile and not self._local and self._perforce:
                filename = "perforce.png"

            elif isFile and not self._local and not self._perforce:
                filename = "missing.png"

            elif isFile and self._local and not self._perforce:
                filename = "local.png"

            elif isFile and self._local and self._perforce and self._synched:
                filename = "sync.png"

            elif isFile and self._local and self._perforce and not self._synched:
                filename = "outofdate.png"
            path = os.path.join(Config.Script.Path.ToolImages, "Perforce", filename)

            return QtGui.QIcon(path)

        elif role == QtCore.Qt.UserRole:
            return self

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        Sets the value of the data for the given index based on the provided
        role

        Arguments:
            index (QtGui.QModelIndex): the model index associated with this object
            value (object): value to set
            role (QtCore.Qt.Role): the role that is being requested by the View that owns this model

        """
        # Make sure that the model index is valid
        if not index.isValid():
            return False

        column = index.column()
        if role == QtCore.Qt.EditRole and column >= 1:
            frameIndex = column-1
            if frameIndex > len(self._poseFrames):
                self._poseFrames.append(value)
            else:
                self._poseFrames[frameIndex] = value

        elif role == QtCore.Qt.EditRole and not column and self._nameIsEditable:
            self.setName(value)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        """
        Gets the index at the given row and column that is a child of the given QModelIndex

        Arguments:
            row (int): row associated with the model index
            column (int): column associated with the model index
            parent (QtCore.QModelIndex): parent of the model index

        Return:
            QtCore.QModelIndex()

        """
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def flags(self, index):
        """
        Qt Flags that are enabled on this object

        Arguments:
            index (QtGui.QModelIndex): model index whose flags are being requested

        """
        if not index.column() and self._nameIsEditable:
            pass
        elif index.column() < 1:
            return (self.DragDropUneditable, self.Uneditable)[self.isFile()]
        elif not index.parent().isValid():
            return self.Uneditable

        return (self.DragDropEditable, self.Editable)[self.isFile()]

    def row(self):
        """ The row this object is located on"""
        if self._parent:
            return self._parent.children().index(self)
        return 0

    def rowCount(self):
        """
        Number of rows this BaseModelItem has

        Arguments:
            parent(BaseModelItem):

        Return:
            int
        """
        return self.childCount()

    def child(self, row):
        """
        Returns the child of the AnimData at the given row

        Arguments:
            row (int): row number

        Return:
            AnimData or None

        """

        try:
            return self._children[row]

        except IndexError:
            return None

    def childCount(self):
        """ Number of children """
        return len(self._children)

    def childAtRow(self, row):
        """
        Gets the child at the given row

        Arguments:
            row (int): row child is at

        Return:
            AnimData

        """
        return self._children[row]

    def appendChild(self, child):
        """
        Adds a new child to the AnimData

        Arguments:
            item (AnimData): child AnimData

        """
        if child not in self._children:
            parent = child.parent()
            if parent:
                parent.removeChild(child)

            self._children.append(child)
            child._parent = self

    def addChildAtRow(self, child, row):
        """
        adds child to the given row

        Arguments:
            child (AnimData): child to add
            row (int): row to add child at

        """
        if child not in self._children:
            parent = child.parent()
            if parent:
                parent.removeChild(child)
            child._parent = self
        else:
            child = self._children.pop(row)
        self._children.insert(row, child)

    def removeChild(self, child):
        """
        Removes a single child

        Arguments:
            child (AnimData): child to remove
        """
        try:
            self._children.remove(child)
            child._parent = None

        except ValueError, e:
            print e

    def removeChildAtRow(self, row):
        """
        Removes child at the given row

        Arguments:
            row (int): row to add child at

        """
        if len(self._children) > row:
            self.removeChild(self._children[row])

    def children(self):
        """ The children of this object """
        return self._children

    def clearChildren(self):
        """ Removes all children """
        for child in self._children:
            child.setParent(None)
        self._children = []

    def parent(self):
        """ parent of this object """
        return self._parent

    def setParent(self, parent):
        """ Sets the parent of this object """
        if self in self._parent.children():
            self._parent.removeChild(self)
        else:
            parent.addChild(self)

    # Support for Drag & Drop

    def supportedDragActions(self):
        """ Flags for supported drag actions """
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    def supportedCopyActions(self):
        """ Flags for supported drop actions """
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    # check state of the file
    def updateStateOfFile(self):
        """
        Checks the state of the file, if it is on the local machine, on perforce and should it be on perforce it
        checks if the local revision matches the head revision.
        """
        if self.isFile():
            self._local = os.path.exists(self._path)
            self._perforce = Perforce.GetFileState(self._path)
            self._synched = False
            if self._perforce:
                self._synched = self._perforce.HeadRevision == self._perforce.HaveRevision
            self._perforce = bool(self._perforce)