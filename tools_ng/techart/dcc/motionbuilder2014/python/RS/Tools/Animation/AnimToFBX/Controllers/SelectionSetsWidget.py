from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

import RS.Tools.UI
import clr

import RS.Utils.UserPreferences as userPref
import RS.Tools.Animation.AnimToFBX.Data.AnimNodes as animNodes
import RS.Tools.Animation.AnimToFBX.Controllers.QTUtils as QTUtils
import RS.Tools.Animation.AnimToFBX.Models.SelectionSet as setsModel
import RS.Tools.Animation.AnimToFBX.Controllers.AnimUtils as animUtils


clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory

reload(animUtils)
reload(animNodes)
reload(setsModel)

RS.Tools.UI.CompileUi(
    '{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\SelectionSetsWidget_QT.ui'.format(RS.Config.Script.Path.Root),
    '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\SelectionSetsWidget_QT.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.Animation.AnimToFBX.ViewWidgets.SelectionSetsWidget_QT as SelSet
reload(SelSet)

_Config = ConfigFactory.CreateConfig()

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)


class SelectionSetsWidget( RS.Tools.UI.QtMainWindowBase, SelSet.Ui_Form  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None)
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
	self.ConnectSelectionSetModel()
	self._nodeDataMapper = QtGui.QDataWidgetMapper()
	
	#Load Selection sets from mbtools.ini
	self.LoadSelSets()		
	
    def setModel(self, model):
	self._model = model
	self._nodeDataMapper.setModel(model)

	self._nodeDataMapper.addMapping(self.lItemName, 0)
	self._nodeDataMapper.addMapping(self.lItemPath, 2)	
	
    def setSelection(self, current, old):
	parent = current.parent()
	self._nodeDataMapper.setRootIndex(parent)
	
	self._nodeDataMapper.setCurrentModelIndex(current)
	
    def ConnectSelectionSetModel(self):
	#Create a Root Node
	self._SetsRootnode = animNodes.animNode("Root")
	
	# Create a Selection Set Model instance
	self._selSetModel = setsModel.SelectionSet(self._SetsRootnode)
	
	#Set the Model on the Tree View
	self.SetListView.setModel(self._selSetModel)
	
	
    def LoadSelSets(self):
	
	#Read in the # of Selection Sets
	setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue = True))
	
	#Loop Through each selection set
	for i in range(setCount):
	
	    # Read in the selection set branch
	    branch = str(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "branch", returnValue = True))
	    
	    # iS THE SELECTION SET FROM A VALID BRANCH IN THE CONFIG
	    found = False
	    dlcList = _Config.AllProjects()
	    for dlc in dlcList.Values:
		if branch == dlc.get_Name() or branch == 'rdr3 old location':	
		    found = True	    
	    
	    if found:
		# Get the Selection Set Name 
		setname = userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "setname", returnValue = True)
		
		# Gte the selection set listcount
		listCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listcount", returnValue = True))
		
		# Create a SelectionSetNode
		selSetRoot = animNodes.animSelectionSetNode(setname, self._SetsRootnode, animBranch = branch)
		
		# Loop through list Items
		for j in range(listCount):
		
		    # Read in the list item
		    item = userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listitem_" + str(j), returnValue = True)
		    itemDict = item.split("|")		    

		    # Create a listitemNode for each item
		    if itemDict[1] == "File Folder":
			node = animNodes.animPathFolderNode(itemDict[0], parent = selSetRoot, nodePath = itemDict[2])
		    elif itemDict[1] == "anim depot File":
			node = animNodes.animPathFileNode(itemDict[0], parent = selSetRoot, nodePath = itemDict[2])
			
	QTUtils.adjustSelectionSetsColumnwidth(self.SetListView)
	                                
    def CreateSelectionSetModel(self):
	# Create a Selection Set Model instance
	self._SetsRootnode = animNodes.animSelectionSetNode("Root")
	
	#Create a Root Node
	self._selSetModel = setsModel.SelectionSet(self._SetsRootnode)
	
	#Set the Model on the Tree View
	self.SetListView.setModel(self._selSetModel)	

	return	

	                                