from PySide import QtCore, QtGui
import RS.Config
import RS.Tools.Animation.AnimToFBX.Resources.resources
import RS.Utils.Path as rsPath

class SelectionList(QtCore.QAbstractItemModel):
    def __init__(self, root, parent = None):
        ### call the superclass constructor and pass the parent class to it ###
        #QtCore.QAbstractListModel.__init__(self, parent)
        super(SelectionList, self).__init__(parent)

        ### private attributes ###
        self.__rootNode = root

    def getRoot(self):
        return self.__rootNode
    
    filterRole = QtCore.Qt.UserRole	
    typeRole = QtCore.Qt.UserRole +1
    pathRole = QtCore.Qt.UserRole + 2    


    ### View needs to know how many items this model contains ###
    def rowCount(self, parent = QtCore.QModelIndex()):
        if not parent.isValid():
            parentNode = self.__rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()


    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent):	
        return 1	

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    return "Folders/Files"
                elif section == 1:
                    return "Type"
                else:
                    return "Path"
                
    ### Making the lists editable, need two more methods, flags and setData ###
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    """ Inputs: QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        node = self.getNode(index)
        #node = index.internalPointer()
        
        parentNode = node.parent()

        if parentNode == self.__rootNode:
            #root node, create an empty QModelIndex
            return QtCore.QModelIndex()

        # wrap in a QModelIndex
        return self.createIndex(parentNode.row(), 0, parentNode)   

    """ Inputs: int, int, QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column, and parent node"""	
    def index(self, row, column, parent):
        # is the parent valid (when QmodelIndex is empty)
        parentNode = self.getNode(parent)
        #if not parent.isValid():
            #parentNode = self.__rootNode
            #childItem = None
        #else:
            #parentNode = parent.internalPointer()
            
        childItem = parentNode.child(row)

        #if there are no children
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()	

    """ Inputs: QModelIndex"""
    ### returns True or False depending on whwther the data was valid or not    ###
    """ Inputs: QModelIndex, QVariant, int (flag)"""
        
    def setData(self, index, value, role = QtCore.Qt.EditRole):
        if index.isValid():			
            if role == QtCore.Qt.EditRole:
                
                node = index.internalPointer()
                
                if index.column() == 0:
                    node.setName(value)
                    self.dataChanged.emit(index, index)
                return True

        return False
        
    ### QModelIndex and role ###
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:        

            if index.column() == 0:
                #return value.name()
                return node.name()
            elif index.column() == 1:
                #return value.typeInfo()
                return node.typeInfo()
            elif index.column() == 2:
                #return value.path()
                return node.displayPath()

        if role == QtCore.Qt.DecorationRole:

            if index.column() == 0:
                typeInfo = node.typeInfo()

                if typeInfo == "File Folder":
                    return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
                elif typeInfo == "anim depot File":
                    return QtGui.QIcon(QtGui.QPixmap(":/depotFile_icon.png"))
                else:
                    return QtGui.QIcon(QtGui.QPixmap(":/workspaceFile_icon.png"))
         


    """ Inputs: QModelIndex"""
    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self.__rootNode
    
    #=====================================================================#
    # INSERTING AND REMOVING
    #=====================================================================#
        
    def insertRows(self, position, rows, node, parent = QtCore.QModelIndex()):
        parentNode = self.getNode(parent)
        
        self.beginInsertRows(parent, position, position + rows - 1)
        
        for row in range(rows):
            childNode = node
            success = parentNode.insertChild(position, childNode)
            
        self.endInsertRows()
        
        return success
        
    def removeRows(self, position, rows, parent = QtCore.QModelIndex()):
        parentNode = self.getNode(parent)
        
        self.beginRemoveRows(parent, position, position + rows - 1)
        
        for row in range(rows):
            success = parentNode.removeChild(position)
            
        self.endRemoveRows()    
        
        return success