"""
Sets up the Animation Tree widget that shows a list of the available anim files that on the Perforce Server.

This widget is designed in Qt Desginer and uses the AnimTree_Qt.ui file

The desginer file can be found at:
<project>\\tools\\techart\\dcc\\motionbuilder(current version)\\python\\RS\\Tools\\Animation\\AnimToFBX\\UI\\AnimTree_QT.ui
"""

import clr
import os

from PySide import QtCore, QtGui

import RS.Tools.UI
import RS.Perforce as p4
import RS.Utils.Path as rsPath

import RS.Tools.Animation.AnimToFBX.Data.AnimNodes as nodes
import RS.Tools.Animation.AnimToFBX.Models.AnimTree as animModel
import RS.Tools.Animation.AnimToFBX.Models.FilterProxy as proxy
import RS.Tools.Animation.AnimToFBX.Controllers.QTUtils as QTUtils
import RS.Tools.Animation.AnimToFBX.Models.DlcProjectList as dlcBranch
import RS.Tools.Animation.AnimToFBX.Controllers.AnimUtils as AnimUtils

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory
_Config = ConfigFactory.CreateConfig()


# ##############################################################################################################
# #* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
# #
# # Description: Globals
# #
# #* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
# ##############################################################################################################

DEFAULT_SKEL_PATH = os.path.join(RS.Config.Project.Path.Assets, "anim", "skeletons", "sp").replace("\\", "/")
if not os.path.exists(DEFAULT_SKEL_PATH):
    DEFAULT_SKEL_PATH = os.path.join(RS.Config.Tool.Path.Root, "etc", "config", "anim", "skeletons", "sp"
    ).replace("\\", "/")

PROJECT_ROOT = RS.Config.Project.Path.Root
PROJECT_NAME = str(RS.Config.Project.Name)

DEVELOPMENT = False

RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\AnimTree_QT.ui'.format(RS.Config.Script.Path.Root),
                      '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\AnimTree_QT.py'.format(RS.Config.Script.Path.Root))

import RS.Tools.Animation.AnimToFBX.ViewWidgets.AnimTree_QT as animTree
reload(animTree)


class AnimTreeWidget(RS.Tools.UI.QtMainWindowBase,  animTree.Ui_Form):

    def __init__(self, parent=None):
        RS.Tools.UI.QtMainWindowBase.__init__(self, parent=parent)
        self.setupUi(self)

        self.skeletonStandardModels = {}

        self.treeLoadingProgress.setVisible(False)

        if not DEVELOPMENT:
            self.animSearch_hidden.setVisible(False)
        else:
            import RS.Tools.UI.Commands as cmds
            cmds.ConnectToWing()

        self._customPath = False

        self._dlcBranch = None 

        # List of DLC Branches for project root (from configfactory assembly)
        self._projectList = self.GetAnimsFolderList()

        # setting the source models to their views
        self.ConnectDlcModelToView()

        # Populate the skeleton combo box
        self.CreateSkelModels()

        # start filter
        self.filterNowButton.pressed.connect(self.ApplyFilterText)

        # expand and collapse tree buttons
        self.btnExpandAll.pressed.connect(self.ExpandTree)
        self.btnCollapseAll.pressed.connect(self.CollapseTree)

        # create the model based on dlc selection and setting the model to the tree view
        self.lDlcCombo.activated[str].connect(self.ConnectAnimListModelToView)

        # setting the treeView QTreeView Selection Mode and Behavior
        self.treeView.setSelectionMode(QtGui.QTreeView.ExtendedSelection)
        self.treeView.setSelectionBehavior(QtGui.QTreeView.SelectRows)

        self.lLoadSelected.pressed.connect(self.LoadAnim)
        self.skelFileTypeComboBox.currentIndexChanged.connect(self.PopulateSkelsCombo)
        self.PopulateSkelType()

    def GetAnimsFolderList(self):
        self.projectList = ["Select a Branch to Load Character Animation From:"]
        if PROJECT_ROOT == "x:\\rdr3":
            self.projectList.append("rdr3 old location")
        # Get a list of the current local project branches

        dlcList = _Config.AllProjects()
        for dlc in dlcList.Values:
            name = "%s" % (str(dlc.get_Name()))
            self.projectList.append(name)

        self.projectList.append("Browse ***WARNING - Tree may take time to populate ***")
        return self.projectList

    def ConnectDlcModelToView(self):

        self._projModel = dlcBranch.DlcProjectList(self._projectList)

        self.lDlcCombo.setModel(self._projModel)
        if len(self._projectList) == 2:
            self.ConnectAnimListModelToView(self._projectList[1])

    def CreateSkelModels(self):
        """ Create StandardItemModels for each directory in the the skeleton directory on perforce """
        self.skeletonStandardModels = {"_directories": QtGui.QStandardItemModel()}

        for directory in self.GetContentFromDepot(cmd="dirs"):
            self.skeletonStandardModels.setdefault(directory, QtGui.QStandardItemModel())
            directoryItem = QtGui.QStandardItem(str(directory))
            directoryItem.setData(str(directory))
            self.skeletonStandardModels["_directories"].appendRow(directoryItem)

            for eachFile in self.GetContentFromDepot(os.path.join(DEFAULT_SKEL_PATH[2:], directory),
                                                     cmd="files", ext="skel"):
                item = QtGui.QStandardItem(str(eachFile))
                item.setData(str(eachFile))
                self.skeletonStandardModels[directory].appendRow(item)

    def PopulateSkelType(self):
        """ Populates the skeleton type combo box with the directories available on perforce """
        
        self.skelFileTypeComboBox.setModel(self.skeletonStandardModels["_directories"])

        defaultTypes = self.skeletonStandardModels["_directories"].findItems("human")
        if defaultTypes:
            self.skelFileTypeComboBox.setCurrentIndex(
                self.skeletonStandardModels["_directories"].indexFromItem(defaultTypes[0]).row())

    def PopulateSkelsCombo(self, index):
        """
        Populates the skeleton combo box with the content of the currently selected skeleton type

        Arguments:
            index (int): index of the current skeleton type selected on the skeleton file type combo box
        """
        
        skeletonItem = self.skeletonStandardModels["_directories"].item(index)

        if not skeletonItem:
            return

        skeletonDirectoryName = skeletonItem.data()
        self.useSkelCombo.setModel(self.skeletonStandardModels.get(skeletonDirectoryName, QtGui.QStandardItemModel()))

        if skeletonDirectoryName in "human" and "human" in self.skeletonStandardModels:

            for defaultName in ("biped.skel", "player.skel"):
                defaultItems = self.skeletonStandardModels["human"].findItems(defaultName)
                if defaultItems:
                    self.useSkelCombo.setCurrentIndex(
                        self.skeletonStandardModels["human"].indexFromItem(defaultItems[0]).row())
                    break

    def GetSkeletonPath(self):
        """
        Builds the skeleton path based on the current selection from the skeleton combo boxes

        Return:
            string
        """

        return os.path.join(DEFAULT_SKEL_PATH, self.skelFileTypeComboBox.currentText(), self.useSkelCombo.currentText())

    def ConnectAnimListModelToView(self, index):
        # Create the Data List
        if index == u'Select a Branch to Load Character Animation From:':
            return

        # setting up loading bar from QtDesigner ui
        self.handleProgress = RS.Tools.progBar(self.treeLoadingProgress, self.lblLoadingBar)
        self.handleProgress.reset()
        self.handleProgress.setVisible(True)
        # self.handleProgress.simProgress()
        if index is not None:
            rootNode = self.SetupModelData(unicode(index), self.handleProgress)
            self._dlcBranch = str(index)
        else:
            rootNode = None

        if rootNode:
            self._treeModel = animModel.AnimTree(rootNode)
            self._treeModel.setbranchRootPath(unicode(index))

            """ VIEW <---------> PROXY MODEL <----------> DATA MODEL"""
            # # proxy model for filtering
            self._proxyModel = proxy.LeafFilterProxyModel()

            self._proxyModel.setSourceModel(self._treeModel)

            self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
            self._proxyModel.setFilterRole(animModel.AnimTree.filterRole)

            self.treeView.setModel(self._proxyModel)

            QtCore.QObject.connect(self.animSearch_hidden, QtCore.SIGNAL("textChanged(QString)"),
                                   self._proxyModel.setFilterRegExp)

            self.treeView.resizeColumnToContents(0)
            self.treeView.setColumnWidth(1, 100)

    def SetupModelData(self, dlcRootFolder=None, progBar=None):

        # create a new tree rootNode
        lPath = self.GetDlcRoot(dlcRootFolder)

        # A catch for when user cancels the folder selection and lPath is not defined
        if lPath is None:
            return None	

        # prefixes only used in gta5_dlc tree creation
        oldGen = '' 
        nextGen = ''

        # Test here if our branch 
        if 'gta5_dlc' in lPath and not self._customPath:

            ngPath = lPath + '/ng/anim/export_mb'
            ogPath = lPath + '/anim/export_mb'        

            animsNotDepot = self.GetAnimsNotInDepot(lPath + '\\')
            if os.path.exists(ngPath) and os.path.exists(ogPath):
                oldGen = 'anim/export_mb/'
                nextGen = 'ng/anim/export_mb/'            
                rootNode = nodes.animNode("Root", None, lPath)
                if self.cbLocalAnims.isChecked():
                    anims = []
                    altNgAnims = []
                else:
                    altNgAnims = self.GetDepotFiles(ngPath, prog=progBar)
                    anims = self.GetDepotFiles(ogPath, prog=progBar)

            elif os.path.exists(ogPath) and not self.cbLocalAnims.isChecked():
                rootNode = nodes.animNode("Root", None, ogPath)
                altNgAnims = []
                anims = self.GetDepotFiles(ogPath, prog = progBar)

            elif os.path.exists(ngPath) and not self.cbLocalAnims.isChecked():
                rootNode = nodes.animNode("Root", None, ngPath)
                altNgAnims = self.GetDepotFiles(ngPath, prog = progBar)
                anims = []

            else:
                # FBMessageBox("Warning", "Could not find the anim folder for this branch. ", "OK")
                return None

        # So I need a better way to catch when a folder was a custom choice rather than a branch choice	
        elif 'gta5' in lPath and not self._customPath:
            lPath = "{}{}".format(lPath, '\\ng\\anim\\export_mb')
            rootNode = nodes.animNode("Root", None, lPath)

            # Get all the anim files under the "ingame" folder of the current project from perforce
            if self.cbLocalAnims.isChecked():
                anims = []
            else:   
                anims = self.GetDepotFiles(lPath, prog=progBar)
                animsNotDepot = self.GetAnimsNotInDepot(lPath + '\\')
                altNgAnims = []

        else:
            rootNode = nodes.animNode("Root", None, lPath)

            # Get all the anim files under the "ingame" folder of the current project from perforce
            if self.cbLocalAnims.isChecked() or self._customPath:
                anims = []
            else:
                anims = self.GetDepotFiles(lPath, prog=progBar)

            progBar.text('Getting Anims...')
            animsNotDepot = self.GetAnimsNotInDepot(lPath+'\\')
            progBar.text('Anims Found {0}...'.format(len(animsNotDepot)))
            altNgAnims = []        

        totalAnims = len(anims) + len(animsNotDepot) + len(altNgAnims)
        progress = 0
        progBar.setValue(progress)
        progBar.text('Creating Tree...')

        # Go through each filename and create the nodes
        for filename in anims:  

            self.CreateNodesFromFilename(oldGen + filename, rootNode)

            progress += 1        
            progPercent = progress * 100 / totalAnims         
            progBar.setValue(progPercent)

        for filename in animsNotDepot:

            self.CreateNodesFromFilename(filename, rootNode, False)

            progress += 1        
            progPercent = progress * 100 /totalAnims        
            progBar.setValue(progPercent)        

        for filename in altNgAnims:

            self.CreateNodesFromFilename(nextGen + filename, rootNode)

            progress += 1        
            progPercent = progress * 100 /totalAnims        
            progBar.setValue(progPercent) 

        progBar.setVisible(False)

        return rootNode

    def GetDlcRoot(self, lbSelItem=None):
        self._customPath = False

        if lbSelItem == 'rdr3 old location':
            # self.cbLocalAnims.setChecked(False)
            return 'x:\\rdr3\\art\\anim\\export_mb' 
        elif lbSelItem == 'Browse ***WARNING - Tree may take time to populate ***':
            self._customPath = True
            self.cbLocalAnims.setChecked(True)

            # Need to add a browse function here like the single folder import
            return AnimUtils.SelectFolder()

        dlcList = _Config.AllProjects()
        for dlc in dlcList.Values:
            if lbSelItem == dlc.get_Name():
                # RDR3
                if lbSelItem == "rdr3":
                    return str(dlc.get_Root()) + '\\assets\\anim\\ingame'
                else:
                    return str(dlc.get_Root()) + "\\art" 

    def GetAnimsNotInDepot(self, path=None):
        if self._customPath:
            lRecurse = 0
        else: 
            lRecurse = 1
        mlist = rsPath.Walk(path, pRecurse=lRecurse, pPattern='*.anim', pReturn_folders=1)
        myList = []

        # Need to look for files that are read-only also
        for anim in mlist:
            ret = os.access(anim, os.W_OK)

            if ret:
                animName = anim.split(path)[1]
                fName = animName.replace('@','%40')
                nName = fName.replace('\\', '/')
                # pName = nName + 'anim'
                myList.append(nName)

        return myList

    def CreateNodesFromFilename(self, filename, parent=None, depot=True):

        paths = filename.split('/')
        nodePath = parent._path
        for n in xrange(len(paths)):
            found = 0
            nodePath = nodePath + "/" + str(paths[n])

            if n == len(paths)-1:
                if depot:
                    nodes.animPathFileNode(str(paths[n]).split('.')[0], parent, nodePath)
                else:
                    nodes.animPathWorkSpaceFileNode(str(paths[n]).split('.')[0], parent, nodePath)
            else:
                # does this exist in the parents children
                for m in xrange(parent.childCount()):
                    if parent.child(m).name().upper() == paths[n].upper():
                        parent = parent.child(m)
                        found = 1
                        break
                if not found:
                    parent = nodes.animPathFolderNode(str(paths[n]), parent, nodePath)

    def ApplyFilterText(self):
        self.CollapseTree()
        # I am sure there is a better and faster way to filter, but for now, this is the best solution
        self.animSearch_hidden.setText(self.animSearch.text())

    def ExpandTree(self):
        self.treeView.expandAll()

    def CollapseTree(self):
        self.treeView.collapseAll()

    def GetDepotFiles(self, dlcRoot="{0}\\assets\\anim\\ingame".format(RS.Config.Project.Path.Root),
                      ext='anim', prog=None):

        fileList = []
        cmd = "files"
        args = ["-e","{0}/...{1}".format(dlcRoot, ext)]  # args = ["-e","{0}\...{1}".format(CUTSCENE_PATH,ext)]

        p4Files = p4.Run(cmd, args)  # p4Files = p4.Run(cmd, args)

        progress = 0

        if isinstance(p4Files, unicode):
            # if str('has been unloaded') in p4Files:
            message = QtGui.QMessageBox()
            message.setWindowTitle("Perforce Warning")
            message.setText(("{0}\n\n Working in AnimToFbx tool will only allow you to work in local files.\n"
                             "Recommended action is to close Motionbuilder and re-install tools").format(str(p4Files))
                            )
            message.exec_()

        else:
            totalAnims = len(p4Files.Records)

        if prog:
            prog.setValue(progress)
            prog.text('Getting Depot files...')

        for record in p4Files.Records:

            try:
                sPath = record['depotFile'].split('ingame/')[1]
                fileList.append(str(sPath))

            except:
                sPath = record['depotFile'].split('export_mb/')[1]
                fileList.append(str(sPath))

            progress += 1
            progPercent = (progress * 100 / totalAnims)
            prog.setValue(progPercent)

        return fileList

    @staticmethod
    def GetContentFromDepot(dlcRootPath=DEFAULT_SKEL_PATH[2:], ext='', cmd="files", warning=False):
        """
        Gets a list of the skeleton files on the server

        Arguments:
            dlcRootPath (string): path to the skel files on perforce
            ext (string): file extension to filter for
            cmd (string): P4 command to use
            warning (boolean): if a warning should pop up when perforce errors out

        Return:
            list(string, etc.)
        """
        fileList = []
        args = ["{}/*".format(dlcRootPath)]

        if cmd == "files" and ext:
            args = ["-e", "{0}/...{1}".format(dlcRootPath, ext)]
        elif cmd == "files":
            args = ["-e", "{0}/...".format(dlcRootPath)]

        p4Files = p4.Run(cmd, args)

        if not isinstance(p4Files, unicode):
            fileList = [os.path.split((record['depotFile'] or record['dir']))[-1] for record in p4Files.Records]

        elif isinstance(p4Files, unicode) and warning:
            warning = QtGui.QMessageBox()
            warning.setText("Could not connect to P4\nRestart the tool to reconnect to P4")
            warning.exec_()

        return fileList

    def LoadAnim(self, singleAnim=None):
        QTUtils.LoadAnim(self, singleAnim)