"""
Description:
    Abstract Tree View used to manipulate the Moving Cutscene Setup in the scene

Author:
    David Vega <david.vega@rockstargames.com>

MOVE TO GENERIC PLACE
"""

from PySide import QtGui, QtCore


class AbstractTreeView(QtGui.QWidget):
    """
    Abstract QWidget with support methods for interacting with the QTreeView
    This widget can be used in any python application that support PySide
    """
    _currentHover = []

    ConstraintOnBackground = QtGui.QBrush(QtGui.QColor(37, 93, 219))
    ConstraintOffBackground = QtGui.QBrush(QtGui.QColor(255, 0, 0))
    ConstraintBackgrounds = (ConstraintOffBackground, ConstraintOnBackground)

    ChangedSelection = QtCore.Signal(QtGui.QItemSelection, QtGui.QItemSelection)
    ItemDrop = QtCore.Signal(object, object)

    def __init__(self, *args,  **kwargs):
        """
        Constructor

        parent = QWidget; parent for this widget
        """
        super(AbstractTreeView, self).__init__(*args, **kwargs)
        mainLayout = QtGui.QVBoxLayout()

        self.Tree = QtGui.QTreeView()
        self.Tree.setSelectionMode(self.Tree.ExtendedSelection)
        self.Tree.setMouseTracking(True)

        self.Model = QtGui.QStandardItemModel()
        self.Tree.setModel(self.Model)
        self.RefreshButton = QtGui.QPushButton("Refresh UI")
        mainLayout.addWidget(self.Tree)
        mainLayout.addWidget(self.RefreshButton)

        self.Tree.entered.connect(self.Hover)
        self.Tree.selectionChanged = self.selectionChanged

        self.Tree.setDragEnabled(True)
        self.Tree.setAcceptDrops(True)
        self.Tree.setDropIndicatorShown(True)
        self.Tree.setDragDropMode(self.Tree.InternalMove)
        self.Model.dropMimeData = self.dropMimeData

        self.setLayout(mainLayout)

    def Hover(self, modelIndex):
        """
        Gets the QModelIndex of the QStandardItem the mouse is currently hovering over
        Arguments:
            modelIndex: QModelIndex; the QModelIndex of the QStandardItem the mouse is currently hovering over
        """
        # atIndex doesn't seem to work so we are adding this hack to register the last hovered item
        # to memory
        self._currentHover = modelIndex

    @property
    def CurrentItem(self):
        """ The QStandardItem of the last item the mouse hovered over"""
        item = self.Model
        if self._currentHover:
            item = self.Model.itemFromIndex(self._currentHover)
        return item

    @property
    def AllItems(self):
        """ List of all the QStandardItems in the TreeView"""
        items = []
        root = [self.Model.index(index, 0) for index in xrange(self.Model.rowCount())]
        if root:
            items = [root[0]] + self.GetChildren(root[0])
        return items

    def GetChildren(self, modelIndex):
        """
        Gets the children of the provided modelIndex

        Arguments:
            modelIndex: QModelIndex; the parent modelIndex

        Return:
            list(QStandardItem, etc.)
        """
        children = []
        model = self.Model.itemFromIndex(modelIndex)
        for index in xrange(model.rowCount()):
            child = modelIndex.child(index, 0)
            if child.isValid():
                children.append(child)
                children.extend(self.GetChildren(child))
        return children

    def GetModelIndexByData(self, data):
        """
        Gets the first ModelIndex whose data matches the data passed in
        Arguments:
            data: string; data to look for
        return list[QModelIndex]
        """
        return filter(lambda each: each.data() == data, self.AllItems)

    def supportedDropActions(self):
        """ Overrides QTreeView's supportedDropAction method """
        return QtCore.Qt.MoveAction

    def dropMimeData(self, mimeData, dropAction, row, column, parentModelIndex):
        """ Overrides QTreeView's dropMimeData method"""
        QtGui.QStandardItemModel.dropMimeData(self.Model, mimeData, dropAction, row, column, parentModelIndex)
        parentItem = self.Model.itemFromIndex(parentModelIndex)

        if not parentModelIndex.isValid():
            parentItem = self.Model.invisibleRootItem()

        self.ItemDrop.emit(parentModelIndex, parentItem)
        # return True needed for the method to work
        return True

    def selectionChanged(self, selectedItems, deselectedItems):
        """
        Overrides QtTreeView selectionChanged event

        Arguments:
            selectedItems: QItemSelection(); a list of the newly selected items
            deselectedItems: QItemSelection(); a list of the deselected items
        """
        self.ChangedSelection.emit(selectedItems, deselectedItems)