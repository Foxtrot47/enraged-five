from PySide import QtCore, QtGui
import RS.Config
import RS.Tools.Animation.AnimToFBX.Resources.resources
import os
import RS.Tools.Animation.AnimToFBX.Controllers.AnimUtils as animUtils

class AnimTree(QtCore.QAbstractItemModel):
    """ Inputs: Node, QObject"""
    def __init__(self, root, parent = None):
        super(AnimTree, self).__init__(parent)

        ### private attributes ###
        self._rootNode = root
        self._branchRootPath = None

    filterRole = QtCore.Qt.UserRole	
    typeRole = QtCore.Qt.UserRole +1
    pathRole = QtCore.Qt.UserRole + 2

    ### View needs to know how many items this model contains ###
    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def rowCount(self, parent):
        if not parent.isValid():
            parentNode = self._rootNode
        else:
            parentNode = parent.internalPointer()

        return parentNode.childCount()

    """ Inputs: QModelIndex"""
    """ Outputs: int"""
    def columnCount(self, parent):	
        return 1

    """ Inputs: int, Qt::Orientation, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                if section == 0:
                    lPath = animUtils.GetDlcRoot(self.branchRootPath())
                    if lPath != None:
                        if 'gta5_dlc' in lPath:   
                            
                            ngPath = lPath + '/ng/anim/export_mb'
                            ogPath = lPath + '/anim/export_mb'        
    
                            if os.path.exists(ngPath) and os.path.exists(ogPath):
                                return lPath
                                           
                            elif os.path.exists(ogPath):
                                return ogPath
                                
                            elif os.path.exists(ngPath):
                                return ngPath
                            
                            else:
                                FBMessageBox("Warning", "Could not find the anim folder for this branch. ", "OK")
                                return ""
                        elif 'gta5' in lPath:
                            lPath = lPath + '/ng/anim/export_mb'
                            return lPath
                    #return animUtils.GetDlcRoot(self.branchRootPath())
                    return "Custom Location"

                elif section == 1:
                    return "Type"

                else:
                    return "Path"				



    """ Inputs: QModelIndex"""
    """ Outputs: int (flag)"""	
    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    """ Inputs: QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return the parent of the node with the given QModelIndex"""
    def parent(self, index):
        node = self.getNode(index)
        parentNode = node.parent()

        if parentNode == self._rootNode:
            #root node, create an empty QModelIndex
            return QtCore.QModelIndex()

        # wrap in a QModelIndex
        return self.createIndex(parentNode.row(), 0, parentNode)

    """ Inputs: int, int, QModelIndex"""
    """ Outputs: QModelIndex"""
    """Should return a QModelIndex that corresponds to the given row, column, and parent node"""	
    def index(self, row, column, parent):
        # is the parent valid (when QmodelIndex is empty)
        parentNode = self.getNode(parent)

        if parentNode.childCount() != 0:
            childItem = parentNode.child(row)
        else:
            childItem = None

        #if there are no children
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()	


    ### returns True or False depending on whwther the data was valid or not    ###
    """ Inputs: QModelIndex, QVariant, int (flag)"""

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        
        if index.isValid():
            
            node = index.internalPointer()
            
            if role == QtCore.Qt.EditRole:
                
                if index.column() == 0:
                    node.setName(value)
                    self.dataChanged.emit(index, index)
                return True

        return False

    """ Inputs: QModelIndex, int"""
    """ Outputs: QVariant, strins are cast to QString which is a QVariant"""
    def data(self, index, role):

        if not index.isValid():
            return None
        node = index.internalPointer()

        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if index.column() == 0:
                return node.displayName()
            elif index.column() == 1:
                return node.typeInfo()
            else:
                return node.displayPath()			

        if role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                typeInfo = node.typeInfo()

                if typeInfo == "File Folder":
                    return QtGui.QIcon(QtGui.QPixmap(":/folder.png"))
                elif typeInfo == "anim depot File":
                    return QtGui.QIcon(QtGui.QPixmap(":/depotFile_icon.png"))
                else:
                    return QtGui.QIcon(QtGui.QPixmap(":/workspaceFile_icon.png"))

        # May be a hack, but this is to extract the data from the selected node for now
        if role == self.filterRole:

            return node.displayName()

        if role == self.pathRole:

            return node.path()

        if role == self.typeRole:

            return node.typeInfo()		


    """ Inputs: QModelIndex"""

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node

        return self._rootNode

    def setbranchRootPath(self, path):
        self._branchRootPath = path

    def branchRootPath(self):
        return self._branchRootPath