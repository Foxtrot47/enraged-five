from pyfbsdk import *

import os
import RS.Config
import RS.Tools.UI
import RS.Utils.UserPreferences as userPref
import RS.Tools.Animation.AnimToFBX.Data.AnimNodes as animNodes
import AnimUtils
import QTUtils

from collections import OrderedDict

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)

# Import the Controllers/Widgets we will add to the ToolBox Layouts
import RS.Tools.Animation.AnimToFBX.Controllers.AnimTreeWidget as Anim
reload(Anim)

import RS.Tools.Animation.AnimToFBX.Controllers.PoseTabWidget as Pose
reload(Pose)

import RS.Tools.Animation.AnimToFBX.Controllers.SelectionListWidget as List
reload(List)

import RS.Tools.Animation.AnimToFBX.Controllers.SelectionSetsWidget as Sets
reload(Sets)


def AddToSelList(mainWidget):
    # Get the tree selections
    listIndexes = mainWidget._animTree.treeView.selectedIndexes()
    removeList = []
    appendList = {}  # needs to be a key dictionary
    # loop through the tree selections
    for selection in listIndexes: # selection is a LeafFilterProxyModel

        appendList.update({selection:True})

    # Get the source model QModelIndex of the selected item (instead of the proxymodel QModelIndex)
    index = mainWidget._animTree._proxyModel.mapToSource(selection)  # selection is a DepotAnimListModel

    rowNum = 0
    currSelectionList = []

    # Get the current selection list
    for i in range(mainWidget._listWidget._selModel.getRoot().childCount()):
        currSelectionList.append(mainWidget._listWidget._selModel.getRoot().child(i))


    for listItem in currSelectionList:

        # is selection is the same as item in list
        if listItem.path() == index.data(34):
            FBMessageBox("Message", "{0} is already in your list".format(index.data(32)), "OK")
            appendList[selection] = False
            rowNum += 1
            #continue

        # selection is a child of the folder? this one needs work
        elif listItem.path() in index.data(34):
            lTempList = index.data(34).replace(listItem.path(), "").split("/")

            # looking for length 2 because the first item will be  u""
            if len(lTempList)==2:
        
                lFileName = str(listItem.path())
                liName = lFileName.replace('%40', '@')
        
                choice = FBMessageBox(
                    "Warning", "{0} is already included in the selection list under the\n{1} folder".format(
                        str(lTempList[1]), liName), "Add anyway", "Skip")
        
                if choice == 2:
                    appendList[selection] = False
                    rowNum += 1
                    # continue

        # selected item is a folder and an item in the list is a child (anim file)
        elif listItem.typeInfo() != "File Folder" and index.data(34) in listItem.path():
            lFileName = str(index.data(32))
            iName = lFileName.replace('%40', '@')
            choice = FBMessageBox("Message", "{1} is in the \n{0} folder. ".format(iName, listItem.name()),
                                  "Remove Anim", "Do Not Remove Anim")

            if choice == 1:
                # remove listItem
                removeList.append(rowNum)
                rowNum += 1
                # continue
            else:
                rowNum += 1
            # continue

    # remove all items flagged for remove
    if len(removeList) > 0:
        for i in range(len(removeList)-1, -1, -1):
            mainWidget._listWidget._selModel.removeRows(removeList[i], 1)

    # add all items flagged for add
    if len(appendList) > 0:
        for k, v in appendList.items():

            if v is True:
                index = mainWidget._animTree._proxyModel.mapToSource(k)
                # rows = mainWidget._selModel.rowCount()
                parentNode = mainWidget._listWidget._selModel.getRoot()

            if index.data(33) == "File Folder":
                # insert to the model
                # node = animNodes.animPathFolderNode(index.data(32), parent = parentNode, nodePath = index.data(34))
                node = animNodes.animPathFolderNode(index.data(32), nodePath = index.data(34))

            elif index.data(33) == "anim depot File":
                # node = animNodes.animPathFileNode(index.data(32), parent = parentNode, nodePath = index.data(34))
                node = animNodes.animPathFileNode(index.data(32), nodePath = index.data(34))

            else:
                # node = animNodes.animPathWorkSpaceFileNode(index.data(32)
                # , parent = parentNode, nodePath = index.data(34))
                node = animNodes.animPathWorkSpaceFileNode(index.data(32), nodePath = index.data(34))

            mainWidget._listWidget._selModel.insertRows(
                mainWidget._listWidget._selModel.getRoot().childCount(), 1, node)

    # mainWidget._listWidget.selectionListView.setModel(mainWidget._listWidget._selModel)


def RemoveFromSelList(mainWidget):
    removalDict = {}
    listIndexes = mainWidget._listWidget.selectionListView.selectedIndexes()

    rootNode = mainWidget._listWidget._selModel.getRoot()

    for selection in listIndexes:
        for i in range(rootNode.childCount()):
            if rootNode.child(i).name() == selection.data():
                removalDict.update({selection:i})
                break
    sorted_removalDict = OrderedDict(sorted(removalDict.items(), key=lambda x: x[1], reverse = True))

    for k, v in sorted_removalDict.items():
        mainWidget._listWidget._selModel.removeRows(v,1)


def SaveSelSet(mainWidget):
    # Delete all sets first
    DeleteHistory(False)

    # Resave all sets
    SaveSetHistory(mainWidget)


def LoadSelList(mainWidget):
    # get current index from the selection sets (same as delete set)
    setNode = mainWidget._setsWidget._selSetModel.getNode(mainWidget._setsWidget.SetListView.currentIndex())
    rootNode = mainWidget._setsWidget._selSetModel.getRoot()
    if setNode != None and setNode != rootNode:
        # check if it is typeInfo "Selection Set"
        # if it is not, get the parent, and check if it is typeInfo "Selection Set"
        while setNode.typeInfo() != "Selection Set":
            setNode = setNode.parent()
    else:
        return

    # create new SelectionListModel
    mainWidget._listWidget.ConnectSelModelToSelView()

    # get the childcount of the set node
    childcount = setNode.childCount()

    # create a nodeList
    nodeList = []
    rows = 0
    # for each childnode, create a selectionlist node
    for i in range(childcount):
        # Get number of rows, should be 0
        # rows = 0

        # Get the child node from the selection set children
        node = setNode.child(i)

        # use the info to create a new node and add it to the list
        if node.typeInfo() == "File Folder":
            selNode = animNodes.animPathFolderNode(node.name(), nodePath=node.path())

        elif node.typeInfo() == "anim depot File":
            # insertRows(mainWidget, position, rows, node, parent = QtCore.QModelIndex()):
            selNode = animNodes.animPathFileNode(node.name(), nodePath=node.path())

        mainWidget._listWidget._selModel.insertRows(rows, 1, selNode)
        rows +=1

    # load the branch
    test = mainWidget._animTree._dlcBranch
    if test != setNode.branch():
        mainWidget._animTree.ConnectAnimListModelToView(setNode.branch())
        mainWidget.ConnectListMapperWidget()


def DeleteSelectionSet(mainWidget):
    # get current index from the selection sets (same as delete set)
    setNode = mainWidget._setsWidget._selSetModel.getNode(mainWidget._setsWidget.SetListView.currentIndex())

    # check if it is typeInfo "Selection Set"
    # if it is not, get the parent, and check if it is typeInfo "Selection Set"
    while setNode.typeInfo() != "Selection Set":
        setNode = setNode.parent()

    # Messagebox confirmation
    result = FBMessageBox("Delete Set Confirmation", "Are you sure?", "Delete Set", "Cancel")

    if result == 2:
        return

    # remove node
    row = setNode.row()
    mainWidget._setsWidget._selSetModel.removeRows(row, 1)

    # saveSelSet()
    mainWidget.SaveSelSet()


def DeleteHistory(warn=True):
    if warn:
        # Prompt a warning before deleting all
        result = FBMessageBox("Confirm Deleting All Selection Sets", "Are you sure?", "Delete All", "Cancel")
    else:
        result = 1

    if result == 1:
        # Read in the # of Selection Sets
        setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue=True))

    # Loop Through each selection set
    for i in range(setCount):

        # Delete section, only if exists in branch

        userPref.__RemoveSection__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i))
        return True
    else:
        return False


def SaveSetHistory(mainWidget):

    # Get number of sets from model root childcount
    numSets = mainWidget._setsWidget._selSetModel.getRoot().childCount()

    # print the section and setcount
    userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName, "setcount", numSets)

    # Loop through children
    for i in xrange(numSets):

        # Get the child node (list Item)
        node = mainWidget._setsWidget._SetsRootnode.child(i)

        # print the set alias (i.e. [AnimToFbx Selection Sets_1])
        userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "listcount", node.childCount())

        # print the setname
        userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "setname", node.name())

        # print the branch
        userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i), "branch", node.branch())

        # Get the Set Node child count
        nodeChildCount = node.childCount()

        # Loop through items (setNode children)
        for j in xrange(nodeChildCount):

            # create the listitem data
            lItemText = node.child(j).name() + "|" + node.child(j).typeInfo() + "|" + node.child(j).path()

            # print the list item alias (listitem_1)
            userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName + "_" + str(i),
                                 "listitem_" + str(j), lItemText)

def SaveSelList(mainWidget):
    # get the number of items in the selection list, and return if 0
    numItems = mainWidget._listWidget._selModel.getRoot().childCount()

    # get the number of sets from ini
    setCount = int(userPref.__Readini__("AnimToFbx Selection Sets_" + gProjName, "setcount", returnValue=True))

    # ask for name of set
    setname = GetSelectionSetNameFromUser()

    # check the name is not a dup

    # create a node for the set Node and parent to the rootnode
    selSetNode = animNodes.animSelectionSetNode(setname, animBranch=mainWidget._animTree._dlcBranch)

    # insert to the model
    mainWidget._setsWidget._selSetModel.insertRows(mainWidget._setsWidget._SetsRootnode.childCount(), 1, selSetNode)

    # get a list of the nodes
    nodeList = mainWidget._listWidget._selModel.getRoot()

    # Loop through list Items
    for i in range(nodeList.childCount()):
        # create a node for each item, and parent to the setNode
        if nodeList.child(i).typeInfo() == "File Folder":
            node = animNodes.animPathFolderNode(nodeList.child(i).name(), selSetNode, nodeList.child(i).path())
        elif nodeList.child(i).typeInfo() == "anim depot File":
            node = animNodes.animPathFileNode(nodeList.child(i).name(), selSetNode, nodeList.child(i).path())

    # saveSelSet()
    mainWidget.SaveSelSet()

    # mainWidget.adjustSelectionSetsColumnwidth()


def lLoadSelection(mainWidget, componentType=FBCharacter, namespaces=[]):
    sync = QTUtils.GetAnimTreeCheckboxValue(mainWidget, "syncCheckbox")
    useSkel = QTUtils.GetAnimTreeCheckboxValue(mainWidget, "useSkelCheckbox")

    # Add a loading bar

    if [character for character in FBSystem().Scene.Characters if character.Selected]:
        mList = mainWidget._listWidget._selModel.getRoot()

        skelDirectory = mainWidget._animTree.skelFileTypeComboBox.currentText()
        skelFileName = "biped.skel"
        if useSkel:
            skelFileName = str(mainWidget._animTree.useSkelCombo.currentText())

        skelFile = os.path.join(RS.Config.Project.Path.Assets, "anim", "skeletons", "sp", skelDirectory, skelFileName)
        if not os.path.exists(skelFile):
            skelFile = os.path.join(RS.Config.Tool.Path.Root, r"etc", "config", "anim", "skeletons", skelFileName)
            
        for index in range(mList.childCount()):

            if mList.child(index).typeInfo() == u"File Folder":
                AnimUtils.LoadFolder(mList.child(index).path(), skeletonPath=skelFile, sync=sync,
                                     componentType=componentType, namespaces=namespaces)
            else:
                AnimUtils.LoadFile(mList.child(index).path(), skeletonPath=skelFile, sync=sync,
                                   componentType=componentType, namespaces=namespaces)
    else:
        warning = ("No {} is selected in the scene. Please select one, and try again!\n"
                   "Note: Characters should be selected through the Navigator")

        FBMessageBox("Error", warning.format(componentType.__name__[2:]), "OK")


def GetSelectionSetNameFromUser(default_text="SetName"):
    btn, value = FBMessageBoxGetUserValue("New Selection Set", "Enter Unique Name for Selection Set: ", default_text,
                                           FBPopupInputType.kFBPopupString, "Ok" )

    return value


def DeleteAllSetNodes(mainWidget, delete=True):
    # Delete all sets first
    result = DeleteHistory(True)

    if result is True:
        # Re-create model and rootnode
        mainWidget._setsWidget.CreateSelectionSetModel()

        userPref.__Saveini__("AnimToFbx Selection Sets_" + gProjName, "setcount", str(0))

        mainWidget.ConnectSetsMapperWidget()


def browseAnims(mainWidget):

    filepath = AnimUtils.SelectFile()
    if filepath:
        QTUtils.LoadAnim(mainWidget, filepath)


def ClearSelectionList(mainWidget):
    QTUtils.ClearSelectionList(mainWidget)