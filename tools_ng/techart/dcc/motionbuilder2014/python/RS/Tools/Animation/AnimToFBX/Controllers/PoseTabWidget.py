from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

import RS.Tools.UI


###############################################################################################################
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
##
## Description: Globals
##
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
###############################################################################################################

gProjRoot = RS.Config.Project.Path.Root
gProjName = str(RS.Config.Project.Name)


RS.Tools.UI.CompileUi('{0}\\RS\\Tools\\Animation\\AnimToFBX\\UI\\PoseTabWidget_QT.ui'.format(RS.Config.Script.Path.Root), '{0}\\RS\\Tools\\Animation\\AnimToFBX\\ViewWidgets\\PoseTabWidget_QT.py'.format(RS.Config.Script.Path.Root))
import RS.Tools.Animation.AnimToFBX.ViewWidgets.PoseTabWidget_QT as poseTab
reload(poseTab)

#Import model and Data Nodes
import RS.Tools.Animation.AnimToFBX.Data.PoseMetadataNodes as poseNodes
import RS.Tools.Animation.AnimToFBX.Models.PoseLibrary as posesModel
reload(poseNodes)
reload(posesModel)

import RS.Core.Animation.PoseLibrary.MetaDataManager as poselib
import RS.Utils.Path as rsPath

class PoseTabWidget( RS.Tools.UI.QtMainWindowBase, poseTab.Ui_Form  ):
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self, None)
        
        # Required to create the ui from Qt Designer
        self.setupUi(self)
	
	# Create a Pose Group List from the meta files
	self.lPoseGroups, self.lPoseGroupPaths = self.GetMetaFilesList()	
	self.ConnectPoseLibraryModelToView()
	
	self.posesDataMapper = QtGui.QDataWidgetMapper()
	
    def setModel(self, model):
	self._model = model
	self.posesDataMapper.setModel(model)

	self.posesDataMapper.addMapping(self.lineEdit_2, 0)
	self.posesDataMapper.addMapping(self.lineEdit_3, 1)
	self.posesDataMapper.addMapping(self.lineEdit_4, 2)
	self.posesDataMapper.addMapping(self.lineEdit_5, 3)
	self.posesDataMapper.addMapping(self.lineEdit_6, 4)
	self.posesDataMapper.addMapping(self.lineEdit_7, 5)
	
    def setSelection(self, current, old):
	parent = current.parent()
	self.posesDataMapper.setRootIndex(parent)
	
	self.posesDataMapper.setCurrentModelIndex(current)
	
    def ConnectPoseLibraryModelToView(self): 
	
	for group in self.lPoseGroups:
	    self.cbPoseGroup.addItem(group)	
	    
	self.LoadMetaContent(self.cbPoseGroup.currentIndex())
	    
    def LoadMetaContent(self, index):
	#parse the poseLibrary xmls
	self._data = poselib.load(self.lPoseGroupPaths[index])
    
	poseLibRoot = poseNodes.LibraryPoseNode("root")
	
	for _cat in self._data.Root.Poses:
	    
	    catRoot = poseNodes.LibraryCategoryNode(_cat.Category, parent = poseLibRoot)
	    for _pose in _cat.PoseList:
		poseNodes.LibraryPoseNode(_pose.Name, parent = catRoot, anim = _pose.Anim, frame = _pose.Frame, date = _pose.Date, user = _pose.User)
	    
	self._poseModel = posesModel.PoseLibrary(poseLibRoot)
	self.poseLibTreeView.setModel(self._poseModel)  
	self.poseLibTreeView.expandAll()

    def GetMetaFilesList(self, path = '{0}\etc\poseLibrary\\'.format(RS.Config.Tool.Path.TechArt)):
	
	fileList = rsPath.Walk(path, pRecurse = 1, pPattern = '*.meta', pReturn_folders = 1)
	metaList = []
	
	for filename in fileList:
	    metaName = filename.split(path)[1]
	    metaList.append(metaName)
	    
	return metaList, fileList