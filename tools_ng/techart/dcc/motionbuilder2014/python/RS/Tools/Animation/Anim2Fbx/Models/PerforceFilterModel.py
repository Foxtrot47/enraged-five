"""
Custom filter model for displaying files from Perforce and those that are on the local machine quickly
"""
from PySide import QtGui, QtCore


class PerforceFilterModel(QtGui.QSortFilterProxyModel):
    """
    A custom filter model for just displaying models that meet a regex expression and that are listed as being on
    the local machine or non perforce
    """
    _local = QtCore.Qt.UserRole + 1
    _perforce = QtCore.Qt.UserRole + 2

    def __init__(self, parent=None):
        """ Constructor """
        super(PerforceFilterModel, self).__init__(parent=parent)
        self._checkPerforce = False

    def filterAcceptsRow(self, sourceRow, sourceParent):
        """
        Overrides built-in method.
        Changes filter logic so that parents of the folders/files that meet the filter requirements are still
        shown.
        """
        # Get the source model index
        sourceIndex = self.sourceModel().index(sourceRow, self.filterKeyColumn(), sourceParent)
        valid = sourceIndex.data((self._local, self._perforce)[self._checkPerforce])
        # Check if source index is valid
        valid = True if valid is None else valid

        if sourceIndex.isValid() and valid:

            for index in xrange(self.sourceModel().rowCount(sourceIndex)):
                if self.filterAcceptsRow(index, sourceIndex) and valid:
                    return True

            currentString = str(self.sourceModel().data(sourceIndex, self.filterRole()))
            return self.filterRegExp().pattern().lower() in currentString.lower() and valid

        return super(PerforceFilterModel, self).filterAcceptsRow(sourceRow, sourceParent) and valid

    def local(self):
        """
        Are we only showing local files

        Return:
            boolean
        """
        return self._checkPerforce is False

    def setLocal(self, value):
        """
        Set whether to display local or perforce files

        Arguments:
            value (boolean): should local files be displayed

        """
        self._checkPerforce = value is False
        self.setFilterRegExp(self.filterRegExp())