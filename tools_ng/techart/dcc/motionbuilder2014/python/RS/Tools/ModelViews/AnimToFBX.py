import clr
import os, stat

from pyfbsdk import *
from PySide import QtCore, QtGui
from pyfbsdk_additions import *

import RS.Tools.Models.DepotAnimListModel as animList
import RS.Tools.Models.SelectionSetModel as selSetModel
import RS.Perforce as p4

import RS.Config
import RS.Utils.Path as rsPath


gProjRoot = RS.Config.Project.Path.Root
gToolsRoot = RS.Config.Tool.Path.Root

clr.AddReference("RSG.Base.Configuration")
from RSG.Base.Configuration import ConfigFactory
_Config = ConfigFactory.CreateConfig()

def setupModelData(dlcRootFolder = None, progBar = None):
    ## Need to somehow setCheckable
    
    #create a new tree rootNode
    lPath = GetDlcRoot(dlcRootFolder)
    
    #prefixes only used in gta5_dlc tree creation
    oldGen = '' 
    nextGen = ''
    
    if 'gta5_dlc' in lPath:

        
        ngPath = lPath + '/ng/anim/export_mb'
        ogPath = lPath + '/anim/export_mb'        
        
        animsNotDepot = GetAnimsNotInDepot(lPath + '\\')
        if os.path.exists(ngPath) and os.path.exists(ogPath):
            oldGen = 'anim/export_mb/'
            nextGen = 'ng/anim/export_mb/'            
            rootNode = animList.animPathNode("Root", None, lPath)
            altNgAnims = getDepotFiles(ngPath, prog = progBar)
            anims = getDepotFiles(ogPath, prog = progBar)
            
        elif os.path.exists(ogPath):
            rootNode = animList.animPathNode("Root", None, ogPath)
            altNgAnims = []
            anims = getDepotFiles(ogPath, prog = progBar)
            
        elif os.path.exists(ngPath):
            rootNode = animList.animPathNode("Root", None, ngPath)
            altNgAnims = getDepotFiles(ngPath, prog = progBar)
            anims = []
            
        else:
            FBMessageBox("Warning", "Could not find the anim folder for this branch. ", "OK")
            return None
            
            
    elif 'gta5' in lPath:
        lPath = lPath + '\\ng\\anim\\export_mb'
        rootNode = animList.animPathNode("Root", None, lPath)
        
        #Get all the anim files under the "ingame" folder of the current project from perforce
        anims = getDepotFiles(lPath, prog = progBar)
        animsNotDepot = GetAnimsNotInDepot(lPath + '\\')
        altNgAnims = []
        
    else:
        rootNode = animList.animPathNode("Root", None, lPath)
        
        #Get all the anim files under the "ingame" folder of the current project from perforce
        anims = getDepotFiles(lPath, prog = progBar)
        animsNotDepot = GetAnimsNotInDepot(lPath+ '\\')
        altNgAnims = []        
    
    totalAnims = len(anims) + len(animsNotDepot) + len(altNgAnims)
    progress = 0
    progBar.setValue(progress)
    progBar.text('Creating Tree...')
    
    # Go through each filename and create the nodes
    for filename in anims:  
        
        createNodesFromFilename(oldGen + filename, rootNode)
        
        progress += 1        
        progPercent = progress * 100 / totalAnims         
        progBar.setValue(progPercent)

    for filename in animsNotDepot:

        createNodesFromFilename(filename, rootNode, False)
        
        progress += 1        
        progPercent = progress * 100 /totalAnims        
        progBar.setValue(progPercent)        

    for filename in altNgAnims:

        createNodesFromFilename(nextGen + filename, rootNode)
        
        progress += 1        
        progPercent = progress * 100 /totalAnims        
        progBar.setValue(progPercent) 
        
    progBar.setVisible(False)
    
    return rootNode

def createNodesFromFilename(filename, parent = None, depot = True):
    
    paths = filename.split('/')
    nodePath = parent._path
    for n in range(len(paths)):
        found = 0
        nodePath = nodePath + "/" + str(paths[n])

        if n == len(paths)-1:
            if depot:
                animList.animPathFileNode(str(paths[n]).split('.')[0], parent, nodePath)
            else:
                animList.animPathWorkSpaceFileNode(str(paths[n]).split('.')[0], parent, nodePath)
        else:
            #does this exist in the parents children	    
            for m in range(parent.childCount()):
                if parent.child(m).name().upper() == paths[n].upper():
                    parent = parent.child(m)
                    found = 1
                    break
            if not found:
                parent = animList.animPathFolderNode(str(paths[n]), parent, nodePath)


##############################################################################################################
#Borrowed from HB's Cutscene.py for testing			
"""
    Get valid P4 files from depot 
    Valid p4 Fields
	time
	depotFile
	action
	type
	change
	rev 
"""
def getDepotFiles(dlcRoot = "{0}\\assets\\anim\\ingame".format(RS.Config.Project.Path.Root), ext = 'anim', prog = None):
    fileList = []
    cmd = "files"
    args = ["-e","{0}/...{1}".format(dlcRoot, ext)]
    
    #records = p4.Run( p4Cmd, False, args )
    #if records.HasErrors():
        #return records.ErrorMessage
    #else:
        #return records    
    
    p4Files = p4.Run(cmd, args)
    if str('has been unloaded') in p4Files:
        workspace = p4Files.split(" ")
        FBMessageBox("Perforce Warning", "{0} \nUnable to load depot anim files.\n\nTry running <p4 reload -c {1}> from command prompt.".format(str(p4Files), workspace[1]), "OK")

    else:
        totalAnims = len(p4Files.Records)
        progress = 0 
        if prog:
            prog.setValue(progress)
            prog.text('Getting Depot files...')
        
        for record in  p4Files.Records:
            rPath = record['depotFile']
            
            try:
                sPath = record['depotFile'].split('ingame/')[1]
                fileList.append(str(sPath))
                
            except:
                sPath = record['depotFile'].split('export_mb/')[1]
                fileList.append(str(sPath))
                
            progress += 1        
            progPercent = (progress * 100 / totalAnims)        
            prog.setValue(progPercent)
    #elif len(p4Files.Records) == 0:
        #FBMessageBox("Perforce Error", 'Check that following path is mapped to your workspace in Perforce:\n{0}'.format(dlcRoot), "OK") 
    #else:
        #FBMessageBox("Perforce Error", str(p4Files), "OK")
        
    return fileList

def getDepotSkelFiles(dlcRoot = "{0}\\etc\\config\\anim\\skeletons".format( RS.Config.Tool.Path.Root ), ext = 'skel'):

    fileList = []
    cmd = "files"
    args = ["-e","{0}/...{1}".format(dlcRoot, ext)]
    p4Files = p4.Run(cmd, args)  
    if str('has been unloaded') in p4Files:
        workspace = p4Files.split(" ")
        FBMessageBox("Perforce Warning", "{0} \nUnable to load skeleton files.\n\nTry running <p4 reload -c {1}> from command prompt.".format(str(p4Files), workspace[1]), "OK")
     
    else:
        
        for record in  p4Files.Records:
            rPath = record['depotFile']
            rRev = record['rev']
            
            sPath = record['depotFile'].split('skeletons/')[1]
            fileList.append(str(sPath))
            
    return fileList      


def GetDlcRoot(lbSelItem = None):
    if lbSelItem == 'rdr3 old location':
        return 'x:\\rdr3\\art\\anim\\export_mb'    
    dlcList = _Config.AllProjects()
    for dlc in dlcList.Values:
        if lbSelItem == dlc.get_Name():
            # RDR3
            if (lbSelItem == "rdr3"):
                return str(dlc.get_Root()) + '\\assets\\anim\\ingame'
            else:
                return str(dlc.get_Root())  + "\\art"  

            

def GetAnimsNotInDepot(path = None):
    mlist = rsPath.Walk(path, pRecurse = 1, pPattern = '*.anim', pReturn_folders = 1)
    myList = []
    
    for anim in mlist:
        ret = os.access(anim, os.W_OK)

        if ret:
            animName = anim.split(path)[1]
            fName = animName.replace('@','%40')	
            nName = fName.replace('\\', '/')
            #pName = nName + 'anim'
            myList.append(nName)

    return myList

def syncSelectedFile(filename = None):
    # its a folder if it doesn't end with ".anim"  
    if not filename.endswith("anim"):
        fileList = []
        cmd = "files"
        fName = filename.replace('/', '\\')
        mName = fName.replace('@','%40')
        args = ["-e","{0}/...anim".format(mName)]
        p4Files = p4.Run(cmd, args)
        for record in  p4Files.Records:
            rPath = record['depotFile']
            p4.Sync(rPath)
    else:
        #is file read-only
        if not (os.access(filename, os.W_OK)):
            #sync
            fName = filename.replace('@','%40')
            results = p4.Sync(fName)
        else:
            #is the file in the depot
            if (True):
                #warn that file is writable?
                pass
            else:
                #mark for add
                pass
     
    
def rs_SaveSelList(selectionModel, setsModel, branch):
    # Get the set name
    setname = GetSelectionSetNameFromUser()

    # get the list of nodes in the selection List View
    mList = selectionModel.getList()
  
    
def GetSelectionSetNameFromUser(default_text = "SetName"):
    btn, value = FBMessageBoxGetUserValue( "New Selection Set", "Enter Unique Name for Selection Set: ", default_text, FBPopupInputType.kFBPopupString, "Ok" )

    return value