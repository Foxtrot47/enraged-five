"""
The Network Exporter is an automated service from the Rockstar Tools Team that fulfills a variety of 'roles'. The
role this network exporter serves is to export and build multiple scenes in a sequence.

Author:
    <jason.hayes@rockstarsandiego.com>

Wiki:
    <https://devstar.rockstargames.com/wiki/index.php/Cutscene_Batch_Exporter>
"""
import os
import xml.etree.cElementTree

import pyfbsdk as mobu

import RS.Config
import RS.Perforce


CUTS_DIR = '{0}\\assets\\cuts'.format(RS.Config.Project.Path.Root)
BATCHXML_FILE_EXT = 'batchxml'


def syncCutsDir():
    """ Syncs the cutxml directory from perforce """
    global CUTS_DIR
    RS.Perforce.Sync('{0}...data.cutpart'.format(CUTS_DIR))
        

class RsCutsceneBatchNetworkEntry(object):
    """
    Class for managing data to be added to the batchxml
    """
    def __init__(self, fbxFilename, useSourceControlFiles=True):
        """
        Constructor

        Arguments:
            fbxFilename (string): path to an fbx file
            useSourceControlFiles (boolean): should source control files be used

        """
        self.name = os.path.basename(fbxFilename).lower().split('.')[0]
        self.fbxFilename = fbxFilename.lower()
        self.cutxmlFilename = None
        self.exportCutFile = None
        self.useSourceControlFiles = useSourceControlFiles
        
    def writeToBatchXml(self, batchXmlStream):
        """
        Write information to the provided batchXml

        Arguments:
            batchXmlStream (file): batchxml file to write data to
        """
        if batchXmlStream:
            batchXmlStream.write('\t<Entry>\n')
            batchXmlStream.write('\t\t<FBXFile>{0}</FBXFile>\n'.format(self.fbxFilename))
            batchXmlStream.write('\t\t<Concat>False</Concat>\n')
            batchXmlStream.write('\t\t<ImportCutFile>True</ImportCutFile>\n')
            batchXmlStream.write('\t\t<UseSourceControlFiles>{0}</UseSourceControlFiles>\n'.format(
                self.useSourceControlFiles))
            batchXmlStream.write('\t</Entry>\n')
        

class RsCutsceneBatchNetworkFile(object):
    """
    Class for managing data from a batchxml file
    """
    def __init__(self, filename):
        """
        Constructor

        Arguments:
            filename (string): path of the batchxml file
        """
        self.filename = filename
        self.__entries = {}
        self.__dirty = False
        
    @property
    def dirty(self):
        """
        Has the file has been edited

        Return:
            boolean
        """
        return self.__dirty
    
    @property
    def entries(self):
        """
        files that have been added to the batchxml

        Return:
            dictionary
        """
        return self.__entries
        
    def validate(self):
        """
        Checks that the files in the batchxml exist and syncs/removes missing files

        Return:
            boolean
        """
        # Get a list of files that don't exist
        missingFiles = [filename.lower()
                        for cutsceneName, entry in self.__entries.iteritems()
                        for filename in (entry.fbxFilename, entry.cutxmlFilename)
                        if not os.path.isfile(filename)]

        if not missingFiles:
            mobu.MessageBox('Rockstar', 'Validation Successful!', 'OK')

        else:
            missingFilesStr = ''.join(['{}\n'.format(missingFile) for missingFile in missingFiles])
            
            result = mobu.MessageBox('Rockstar', 'The following files could not be found!'
                                                 '  This will cause network exporting issues.'
                                                 ' Would you like to sync them now?\n\n{0}'.format(
                missingFilesStr), 'Yes', 'No')
            
            if result == 1:
                # Sync files
                RS.Perforce.Sync(missingFiles)
                
            # Double-check that the missing files now exist after the sync.
            stillMissingFiles = [missingFile for missingFile in missingFiles if not os.path.isfile(missingFile)]

            if stillMissingFiles:

                stillMissingStr = ''.join(['{}\n'.format(missingFile) for missingFile in stillMissingFiles])

                result = mobu.MessageBox('Rockstar',
                                         'The following files are still missing!  '
                                         'This will cause network exporting issues if these are not resolved!  '
                                         'Would you like to remove them from the list?\n\n{0}!'.format(stillMissingStr),
                                         'Yes', 'No')

                if result == 1:
                    # Get files that still don't exist after syncing
                    entriesToDelete = [cutsceneName
                                       for stillMissing in stillMissingFiles
                                       for cutsceneName, entry in self.__entries.iteritems()
                                       if stillMissing in (entry.fbxFilename, entry.cutxmlFilename)]

                    # Delete the entries
                    [self.__entries.pop(entry) for entry in entriesToDelete if entry in self.__entries]

                else:
                    return False

        return True
        
    def addEntry(self, fbxFilename, cutxmlFilename=None, validate=False, dirty=True):
        """
        Adds an entry to the batchxml file

        Arguments:
            fbxFilename (string or RsCutsceneBatchNetworkEntry): name of the fbx filename
            cutxmlFilename (string): name of the cutxml
            validate (boolean): validate that file exists
            dirty (boolean): batchxml has been edited

        """
        if isinstance(fbxFilename, RsCutsceneBatchNetworkEntry):
            if fbxFilename.name not in self.__entries:
                self.__entries[fbxFilename.name] = fbxFilename
                self.__dirty = dirty
                return True
            
            else:
                return False
            
        else:
            fbxFilename = fbxFilename.lower()
            basename = os.path.basename(fbxFilename).split('.')[0]
            
            if basename not in self.__entries:
                
                if not validate:
                    if not cutxmlFilename:
                        basename = os.path.basename(fbxFilename).split('.')[0]
                        cutxmlFilename = os.path.join(CUTS_DIR, basename, 'data.cutpart')
                        
                    self.__entries[basename] = RsCutsceneBatchNetworkEntry(fbxFilename, True)
                    self.__dirty = dirty
                    return True
                    
                else:
                    
                    # Make sure we have the latest version.
                    fbxFileInfo = RS.Perforce.GetFileState(fbxFilename)
    
                    if fbxFileInfo:
                        if not RS.Perforce.IsLatest(fbxFileInfo):
                            RS.Perforce.Sync(fbxFilename)
                        
                    if os.path.isfile(fbxFilename):
                        
                        # Attempt to automatically find the cutxml file if one wasn't supplied.
                        if not cutxmlFilename:
                            basename = os.path.basename(fbxFilename).split('.')[0]
                            cutxmlFilename = os.path.join(CUTS_DIR, basename, 'data.cutpart')
                            
                        # Make sure we have the file and it is up to date.
                        RS.Perforce.Sync(cutxmlFilename)
                            
                        if os.path.isfile(cutxmlFilename):
                            self.__entries[basename] = RsCutsceneBatchNetworkEntry(fbxFilename, True)
                            self.__dirty = dirty
                            return True
                        
                        else:
                            print 'The data.cutxml filename is not valid! {0}'.format(cutxmlFilename)
                            return False
                    
                    else:
                        print 'The fbx filename is not valid! {0}'.format(fbxFilename)
                        return False
            
            mobu.MessageBox('Rockstar', 'The following mobu.X file already exists in the list and will not be added.'
                                        '\n\n{0}'.format(fbxFilename), 'OK')
            return False
        
    def deleteEntry(self, fbxFilename):
        """
        Deletes the fbx from the batchxml
        Arguments:
            fbxFilename (string): name of the fbx file

        """
        basename = os.path.basename(fbxFilename).lower()
                
        if basename in self.__entries:        
            self.__entries.pop(basename)
            self.__dirty = True
            
    def deleteEntryByCutsceneName(self, cutsceneName):
        """
        Deletes an fbx from the batchxml based on the cutscene name associated with that fbx

        Arguments:
            cutsceneName (string):

        """
        if cutsceneName in self.__entries:
            self.__entries.pop(cutsceneName)
            self.__dirty = True
            
    def setExportCutState(self, cutsceneName, state):
        """
        sets the state for the cutscene

        Arguments:
            cutsceneName (string): name of the cutscene
            state (boolean): state to set for the cutscene

        """
        if cutsceneName in self.__entries:
            self.__entries[cutsceneName].exportCutFile = state
            self.__dirty = True
            
    def setUseSourceControlFiles(self, cutsceneName, state):
        """
        sets the UseSourceControlFiles attribute for the cutscene

        Arguments:
            cutsceneName (string): name of the cutscene
            state (boolean): value to set for the useSourceControlFiles attribute

        """
        if cutsceneName in self.__entries:
            self.__entries[cutsceneName].useSourceControlFiles = state
            self.__dirty = True        
            
    def setCutXmlFilename(self, cutsceneName, cutxmlFilename):
        """
        Sets the path to the cutxml file for the cutscene

        Arguments:
            cutsceneName (string): name of the cutscene
            cutxmlFilename (string): path to the cutxml

        """
        if cutsceneName in self.__entries:
            self.__entries[cutsceneName].cutxmlFilename = cutxmlFilename
            self.__dirty = True
            
    def createBatchNetworkXml(self, saveFilename):
        """
        create/save batchxml with the data from this object

        Arguments:
            saveFilename (string): path to to save batchxml at

        """
        with open(saveFilename, 'w') as batchXml:
        
            batchXml.write('<?xml version="1.0"?>\n')
            batchXml.write('<Entries>\n')

            for entryName, entry in self.__entries.iteritems():
                entry.writeToBatchXml(batchXml)

            batchXml.write('</Entries>\n')
        
        self.__dirty = False
        
        return True
        
       
class RsCutsceneBatchNetworkFileManager(object):
    """
    Class for loading and merging batchxml files and text files that contain similar data
    """
    def __init__(self):
        """ Constructor """
        self.currentFile = None
    
    def mergeFromFile(self, filename):
        """
        Merges data from the given file into the current RsCutsceneBatchNetworkFile()

        Arguments:
            filename (string): name of the batchxml or text file with the data to merge

        """
        filename = filename.lower()
        
        if filename.endswith('.txt'):
            self.loadFromTxtFile(filename, merge=True, dirty=True)
            
        elif filename.endswith('.batchxml'):
            self.loadFromBatchXmlFile(filename, merge=True, dirty=True)
            
    def loadFromBatchXmlFile(self, batchXmlFilename, merge=False, dirty=False):
        """
        Loads data from a Batch XML File

        Arguments:
            batchXmlFilename (string): path to the batchxml file
            merge (boolean): merge the data from the batchxml file into the current RsCutsceneBatchNetworkFile()
            dirty (boolean): set the dirty status of the RsCutsceneBatchNetworkFile

        """
        if batchXmlFilename.endswith('.batchxml'):
            doc = xml.etree.cElementTree.parse(batchXmlFilename)
            root = doc.getroot()
            
            if merge and self.currentFile:
                batchXmlFile = self.currentFile
                
            else:
                batchXmlFile = RsCutsceneBatchNetworkFile(batchXmlFilename)
                    
            if root:
                if batchXmlFile:
                    entryNodes = root.findall('Entry')
                    
                    for entryNode in entryNodes:
                        fbxFilename = entryNode.find('FBXFile').text
                        
                        if fbxFilename:
                            useSourceControlFiles = True
                            
                            if entryNode.find('UseSourceControlFiles') is not None:
                                useSourceControlFiles = entryNode.find('UseSourceControlFiles').text
                                
                                if useSourceControlFiles == 'True':
                                    useSourceControlFiles = True
                                    
                                else:
                                    useSourceControlFiles = False

                            entry = RsCutsceneBatchNetworkEntry(fbxFilename, useSourceControlFiles)
                            batchXmlFile.addEntry(entry, dirty=dirty)
                    
            self.currentFile = batchXmlFile
        
    def loadFromTxtFile(self, txtFilename, merge=False, dirty=False):
        """
        Loads data from a text file

        Arguments:
            txtFilename (string): path to the batchxml file
            merge (boolean): merge the data from the batchxml file into the current RsCutsceneBatchNetworkFile()
            dirty (boolean): set the dirty status of the RsCutsceneBatchNetworkFile

        """
        global CUTS_DIR
        
        txtFilename = txtFilename.lower()
        
        if os.path.isfile(txtFilename) and txtFilename.endswith('.txt'):

            try:
                txtFileStream = open(txtFilename, 'r')
                lines = txtFileStream.readlines()
                
            except:
                print 'could not read txt file!'
                
            finally:
                txtFileStream.close()
             
            if lines:       
                if merge and self.currentFile:   
                    batchXmlFile = self.currentFile
                    
                else:
                    batchXmlFile = RsCutsceneBatchNetworkFile(txtFilename)
                
                fbxFilenames = []
                
                for line in lines:
                    if not line.startswith('\\'):
                        fbxFilenames.append(line.lower().rstrip('\n'))
                        
                for fbxFilename in fbxFilenames:
                    batchXmlFile.addEntry(fbxFilename, dirty=dirty)
                        
                self.currentFile = batchXmlFile
            
            return batchXmlFile

        return None