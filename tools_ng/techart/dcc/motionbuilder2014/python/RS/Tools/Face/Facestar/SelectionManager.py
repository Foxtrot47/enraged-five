'''
SelectionManager.py

Static module for managing the selection of objects in Facestar.

Author: Jason Hayes <Jason.hayes@rockstarsandiego.com

'''
from pyfbsdk import *
from pyfbsdk_additions import *

import RS.Tools.Face.Facestar.Sliders
import RS.Tools.Face.Facestar.Markers

# Dictionaries to manage the selected items for all defintions.
#
# Key: Character namespace.
# Value: A list of QWidget items that are selected.
#
__OffFaceItems = {}
__OnFaceItems = {}
__AttributeItems = {}


## Private Functions ##

def __SelectItem( definition, itemDict, item ):
    if definition.Namespace in itemDict:
        items = itemDict[ definition.Namespace ]
        
        if item not in items:
            items.append( item )
            itemDict[ definition.Namespace ] = items
        
    else:
        itemDict[ definition.Namespace ] = [ item ]
        
def __DeselectItem( definition, itemDict, item ):
    if definition.Namespace in itemDict:
        items = itemDict[ definition.Namespace ]
        
        if item in items:
            items.remove( item )
            itemDict[ definition.Namespace ] = items


## Public Functions ##

def Select( definition, item ):
    global __OffFaceItems
    global __OnFaceItems
    global __AttributeItems
    
    if isinstance( item, RS.Tools.Face.Facestar.Sliders.AttributeSlider ):
        __SelectItem( definition, __AttributeItems, item )
    
    elif isinstance( item, RS.Tools.Face.Facestar.Sliders.OffFaceSlider ):
        __SelectItem( definition, __OffFaceItems, item.Marker )
        item.Marker.setSelected( True )
        item.Model.Selected = True
    
    elif isinstance( item, RS.Tools.Face.Facestar.Markers.OnFace ):
        __SelectItem( definition, __OnFaceItems, item )
        item.setSelected( True )
        item.Model.Selected = True

def ClearStoredItems( definition ):
    global __OffFaceItems
    global __OnFaceItems
    global __AttributeItems
    __OffFaceItems[ definition.Namespace ] = []
    __OnFaceItems[ definition.Namespace ] = []
    __AttributeItems[ definition.Namespace ] = []

def Deselect( definition, item ):
    global __OffFaceItems
    global __OnFaceItems
    global __AttributeItems
    
    if isinstance( item, RS.Tools.Face.Facestar.Sliders.AttributeSlider ):        
        __DeselectItem( definition, __AttributeItems, item )
    
    elif isinstance( item, RS.Tools.Face.Facestar.Sliders.OffFaceSlider ):
        __DeselectItem( definition, __OffFaceItems, item )
        item.setSelected( False )
    
    elif isinstance( item, RS.Tools.Face.Facestar.Markers.OnFace ):
        __DeselectItem( definition, __OnFaceItems, item )
        item.setSelected( False )
   
def DeselectAttributes( definition ):
    global __AttributeItems
    
    if definition.Namespace in __AttributeItems:
        for item in __AttributeItems[ definition.Namespace ]:
            item.Deselect()
            item.repaint()
            
        __AttributeItems[ definition.Namespace ] = []
            
def DeselectOffFaceMarkers( definition ):
    global __OffFaceItems
    if definition.Namespace in __OffFaceItems:
        for item in __OffFaceItems[ definition.Namespace ]:
            item.setSelected( False )
            
        __OffFaceItems[ definition.Namespace ] = []
             
def DeselectOnFaceMarkers( definition ):
    global __OnFaceItems
    if definition.Namespace in __OnFaceItems:
        for item in __OnFaceItems[ definition.Namespace ]:
            item.setSelected( False )
            
        __OnFaceItems[ definition.Namespace ] = []
   
def DeselectAllMarkers( definition ):
    DeselectAttributes( definition )
    DeselectOffFaceMarkers( definition )
    DeselectOnFaceMarkers( definition )
    
def DeselectAllSceneObj():
    selectedModels = FBModelList()
    FBGetSelectedModels ( selectedModels, None, True )
    for obj in selectedModels:
        obj.Selected = False
    
def GetSelected( definition ):
    global __OffFaceItems
    global __OnFaceItems
    global __AttributeItems
    
    items = []
    
    if definition.Namespace in __OffFaceItems:
        items.extend( __OffFaceItems[ definition.Namespace ] )
        
    if definition.Namespace in __OnFaceItems:
        items.extend( __OnFaceItems[ definition.Namespace ] )
        
    if definition.Namespace in __AttributeItems:
        items.extend( __AttributeItems[ definition.Namespace ] )
        
    return items

def GetSelectedOffFaceMarkers( definition ):
    items = []
    selectedItems = GetSelected( definition )
    
    if selectedItems:
        items = [ item for item in selectedItems if isinstance( item, RS.Tools.Face.Facestar.Sliders.OffFaceSlider ) ]
        
    return items

def PrintStoredItems():
    global __OffFaceItems
    global __OnFaceItems
    global __AttributeItems
    print "ITEMS ARE:"
    if len( __OffFaceItems ) > 0:
        for each in __OffFaceItems.values()[0]:
            print each.ModelName
    print "Groups are:"
    print __OffFaceItems
    print __OnFaceItems
    print __AttributeItems


def SetModelUpdateFromWidget( TF, definition ):
    for each in RS.Tools.Face.Facestar.SelectionManager.GetSelected( definition ):
        each.TimerStop() if TF else each.TimerStart()
        
        each.AllowModelUpdateFromWidget( TF )

def ReselectAllMarkers( definition ):
    items = []
    for item in RS.Tools.Face.Facestar.SelectionManager.GetSelected( definition ):
        items.append( item )
    
    DeselectAllSceneObj()
    DeselectAttributes( definition )
    DeselectOnFaceMarkers( definition )
    DeselectOffFaceMarkers( definition )
    ClearStoredItems( definition )
    
    for item in items:
        Select( definition, item )
    
def UpdateSelection( definition, markers ):
    
    items = []
    for marker in markers:
        '''
        if isinstance( item, RS.Tools.Face.Facestar.Sliders.AttributeSlider ):
            Select( definition, item )
        '''
        if isinstance( marker, RS.Tools.Face.Facestar.Sliders.OffFaceSlider ):
            if marker.Marker.isSelected() == True:
                items.append( marker )
        
        elif isinstance( marker, RS.Tools.Face.Facestar.Markers.OnFace ):
            if marker.isSelected() == True:
                items.append( marker )
                
    DeselectAllSceneObj()
    DeselectAttributes( definition )
    #DeselectOnFaceMarkers( definition )
    DeselectOffFaceMarkers( definition )
    ClearStoredItems( definition )

    for item in items:
        Select( definition, item )
        
