## Interface Modes ##

MODE_EDIT_MARKERS = False
MODE_BOX_SELECTION = False

LocalConfig = None

## Pressed keys ##

ControlPressed = False
AltPressed = False
ShiftPressed = False


## Functions ##
        
def Run():
    import RS.Tools.Face.Facestar.Main
    reload( RS.Tools.Face.Facestar.Main )
    
    tool = RS.Tools.Face.Facestar.Main.MainWindow()
    tool.show()