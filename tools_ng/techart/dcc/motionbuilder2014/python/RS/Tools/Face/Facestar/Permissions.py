'''
RS.Tools.Face.Facestar.Permissions

Module to control permissions of who can do certain operations within the Facestar environment.

Author: Jason Hayes <jason.hayes@rockstarsandiego.com>
'''

import os

## Constants ##

__GROUP_FULL_EDIT = [ 'jhayes', 'tphan' ]


## Functions ##

def CanEdit():
    global __GROUP_FULL_EDIT
    
    username = os.environ.get( 'USERNAME' ).lower()
    return username in __GROUP_FULL_EDIT