import sys
import math
import webbrowser

from pyfbsdk import *
from pyfbsdk_additions import *
from pythonidelib import *

from PySide import QtCore, QtGui, shiboken

from PIL import Image

import RS.Utils.Scene
import RS.Core.Face.Toolbox
import RS.Core.Face.Facestar
import RS.Tools.UI
import RS.Tools.Face.Facestar
import RS.Tools.Face.Facestar.SelectionSet
import RS.Tools.Face.Facestar.SelectionManager
import RS.Tools.Face.Facestar.Tabs
import RS.Tools.Face.Facestar.ViewManager
import RS.Tools.Face.Facestar.Sliders
import RS.Tools.Face.Facestar.Permissions

# Compile QtDesigner .ui file for the main window.
RS.Tools.UI.CompileUi( '{0}\\Tools\\UI\\Facestar.ui'.format( RS.Config.Script.Path.RockstarRoot ) )

import RS.Tools.UI.Facestar

#reload( RS.Tools.UI.Facestar )
#reload( RS.Tools.Face.Facestar.Tabs )
#reload( RS.Tools.Face.Facestar.SelectionSet )
#reload( RS.Tools.Face.Facestar.SelectionManager )
#reload( RS.Core.Face.Facestar )
#reload( RS.Tools.Face.Facestar )
#reload( RS.Tools.UI )
#reload( RS.Tools.Face.Facestar.ViewManager )
#reload( RS.Tools.Face.Facestar.Sliders )
#reload( RS.Tools.Face.Facestar.Permissions )

class MainWindow( RS.Tools.UI.QtMainWindowBase, RS.Tools.UI.Facestar.Ui_MainWindow ):
    '''
    Facestar main window.
    '''
    def __init__( self ):
        RS.Tools.UI.QtMainWindowBase.__init__( self )
        
        # Load local configuration settings for Facestar.
        RS.Tools.Face.Facestar.LocalConfig = RS.Core.Face.Facestar.LocalConfiguration()
        RS.Tools.Face.Facestar.LocalConfig.Load()
        
        # Setup the basic UI using the compiled .ui file.
        self.setupUi( self )
        
        #self.timeSlider = RS.Tools.Face.Facestar.Sliders.TimeSlider()
        #self.mainLayout.addWidget( self.timeSlider )
        #self.mainLayout.addSpacing( 10 )
        
        self.tabWidget = QtGui.QTabWidget()
        self.mainLayout.addWidget( self.tabWidget )
        
        # Collect the face definitions found in the current scene.
        self.__definitions = RS.Core.Face.Facestar.GetSceneDefinitions()
        
        # Setup tab pages.  Each  face definition found in the scene will get its own tab page.
        # The key is the tab index, the value is theface controls widget.
        self.__tabPages = {}
        
        # Set our starting tab index.
        tabIdx = self.tabWidget.count()
        
        for namespace, definition in self.__definitions.iteritems():
            self.SetupTemplateDropdown( definition )
            
            faceGui = RS.Tools.Face.Facestar.Tabs.Character( self, definition )
            self.tabWidget.addTab( faceGui, definition.DisplayName )
            self.__tabPages[ tabIdx ] = faceGui
            
            tabIdx += 1
            
        # Setup Icons.  We have to do this here instead of the .ui file because of pathing issues.
        playIcon = QtGui.QIcon()
        playIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/play.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionPlay.setIcon( playIcon )
        
        stopIcon = QtGui.QIcon()
        stopIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/stop.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionStop.setIcon( stopIcon )
        
        rewindIcon = QtGui.QIcon()
        rewindIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/goToStart.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionGoToStart.setIcon( rewindIcon )
        
        stepBackwardIcon = QtGui.QIcon()
        stepBackwardIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/stepBackward.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionStepBackward.setIcon( stepBackwardIcon )
        
        stepForwardIcon = QtGui.QIcon()
        stepForwardIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/stepForward.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionStepForward.setIcon( stepForwardIcon )
        
        goToEndIcon = QtGui.QIcon()
        goToEndIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/goToEnd.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionGoToEnd.setIcon( goToEndIcon )
        
        editMarkersIcon = QtGui.QIcon()
        editMarkersIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/editMarkers.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionEditMarkers.setIcon( editMarkersIcon )
        
        switchToHeadCamIcon = QtGui.QIcon()
        switchToHeadCamIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/switchToHeadCam.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionSwitchToHeadCam.setIcon( switchToHeadCamIcon )
        
        newSelectionSetIcon = QtGui.QIcon()
        newSelectionSetIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/newSelectionSet.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionNewSelectionSet.setIcon( newSelectionSetIcon )
        
        selectionSetManagerIcon = QtGui.QIcon()
        selectionSetManagerIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/selectionSetManager.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionSelectionSetManager.setIcon( selectionSetManagerIcon )
        
        zoomExtentsIcon = QtGui.QIcon()
        zoomExtentsIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/zoomExtents.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionZoomExtents.setIcon( zoomExtentsIcon )
        
        zoomSelectedIcon = QtGui.QIcon()
        zoomSelectedIcon.addPixmap( QtGui.QPixmap( "{0}/Facestar/zoomSelected.png".format( RS.Config.Script.Path.ToolImages ) ), QtGui.QIcon.Normal, QtGui.QIcon.Off )
        self.actionZoomSelected.setIcon( zoomSelectedIcon )          
        
        # Event handler binding.
        self.actionPlay.triggered.connect( self.Play )
        self.actionStop.triggered.connect( self.Stop )
        self.actionGoToStart.triggered.connect( self.Rewind )
        self.actionStepForward.triggered.connect( self.StepForward )
        self.actionStepBackward.triggered.connect( self.StepBackward )
        self.actionNextKey.triggered.connect( self.GoToNextKey )
        self.actionPreviousKey.triggered.connect( self.GoToPreviousKey )
        self.actionSaveDefinition.triggered.connect( self.SaveDefinition )
        self.actionEditMarkers.toggled.connect( self.ToggleEditMarkers )
        self.actionGoToEnd.triggered.connect( self.GoToEnd )
        self.actionFacestarHelp.triggered.connect( self.OpenFacestarHelp )
        self.actionSwitchToHeadCam.triggered.connect( self.SwitchToHeadCam )
        self.actionSelectionSetManager.triggered.connect( self.OpenSelectionSetManager )
        self.actionNewSelectionSet.triggered.connect( self.NewSelectionSet )
        self.actionZoomSelected.triggered.connect( self.ZoomSelected )
        self.actionZoomExtents.triggered.connect( self.ZoomExtents )
        
        self.__playerControl = FBPlayerControl()
        
        # meta Variable
        self.actionEditMarkers.Cancelled = False
        
        self.LoadState()
        
    
    ## Overrides ##
    
    def closeEvent( self, event ):
        self.SaveState() 
        
        QtGui.QMainWindow.closeEvent( self, event )
        
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = True
            
        elif event.key() == QtCore.Qt.Key.Key_Space:
            RS.Tools.Face.Facestar.MODE_BOX_SELECTION = True
            
        elif event.key() == QtCore.Qt.Key.Key_Z:
            if RS.Tools.Face.Facestar.ControlPressed == True:
                '''
                print "UNDO PRessed"
                print RS.Tools.Face.Facestar.UndoArray
                FlushOutput()
                i = 0
                for each in RS.Tools.Face.Facestar.UndoArray:
                    print ("Round {0}").format(i)
                    j = 0
                    for item in each:
                        print ("item {0}").format(j)
                        print item.ModelName
                        FlushOutput()
                        #item.UpdateToUndo()
                        j+=1
                    i+=1'''
                # DO THE ACTUAL UNDO
                
                if len( RS.Tools.Face.Facestar.UndoArray ) > 0:
                    for y, z in RS.Tools.Face.Facestar.UndoArray[0].iteritems():
                        y.UpdateToUndo( z[0], z[1], z[2] )
                        
                    RS.Tools.Face.Facestar.UndoArray.pop(0)
                    
            else:
                selectedItems = RS.Tools.Face.Facestar.SelectionManager.GetSelectedOffFaceMarkers( self.GetCurrentDefinition() )
            
                if selectedItems:
                    self.ZoomSelected()
                    
                else:
                    self.ZoomExtents()
        
        QtGui.QMainWindow.keyPressEvent( self, event )
        
    def keyReleaseEvent( self, event ):
        
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = False
            
        if event.key() == QtCore.Qt.Key.Key_Alt:
            RS.Tools.Face.Facestar.AltPressed = False
            
        elif event.key() == QtCore.Qt.Key.Key_Shift:
            RS.Tools.Face.Facestar.ShiftPressed = False
            
        RS.Tools.Face.Facestar.MODE_BOX_SELECTION = False
        
        QtGui.QMainWindow.keyReleaseEvent( self, event )
        
    
    ## Methods ##
    
    def SaveState( self ):
        settings = QtCore.QSettings( 'Rockstar', 'Facestar' )
        settings.setValue( 'mainwindow/size', self.size() )
        settings.setValue( 'mainwindow/pos', self.pos() )
        
    def LoadState( self ):
        settings = QtCore.QSettings( 'Rockstar', 'Facestar' )
        self.resize( settings.value( 'mainwindow/size', QtCore.QSize( 600, 400 ) ) )
        self.move( settings.value( 'mainwindow/pos', QtCore.QPoint( 200, 200 ) ) )
    
    def GetCurrentTabIndex( self ):
        return self.tabWidget.currentIndex()
    
    def GetAllTabs( self ):
        tabs = []
        for each in self.tabWidget:
            tabs.append( each )
        return tabs
    
    def GetCurrentDefinition( self ):
        return self.__tabPages[ self.GetCurrentTabIndex() ].Definition
        
    def RefreshTabs( self ):
        
        currentTab = self.tabWidget.currentIndex()
        self.tabWidget.clear()
        #for key in self.__tabPages.keys():
        #    del self.__tabPages[key]
        self.__tabPages = {}
        
        tabIdx = self.tabWidget.count()
        
        for namespace, definition in self.__definitions.iteritems():
            self.SetupTemplateDropdown( definition )
            
            faceGui = RS.Tools.Face.Facestar.Tabs.Character( self, definition )
            self.tabWidget.addTab( faceGui, definition.DisplayName )
            self.__tabPages[ tabIdx ] = faceGui
            
            tabIdx += 1
            
        self.tabWidget.setCurrentIndex(currentTab)
        
        RS.Tools.Face.Facestar.MODE_EDIT_MARKERS = self.actionEditMarkers.isChecked()
        self.__tabPages[ self.GetCurrentTabIndex() ].SetMarkerEditMode( self.actionEditMarkers.isChecked() )        
            
    def onTemplateChanged ( self, text ):
        comboDefinition = self.GetCurrentDefinition()
        sceneDefinitions = self.__definitions
        for nameItem, defItem in sceneDefinitions.iteritems():
            if defItem.Namespace == comboDefinition.Namespace:
                defItem.TemplateName = text
        self.RefreshTabs()
        
    def PopulateComboBox ( self, defItem ):
        templateFileOptions = ["A_Rig", "B_Rig", "C_Rig", "Default", "Other_Rig"]
        
        defItem.TemplateDropdown.addItem( defItem.TemplateName )
        
        for each in templateFileOptions:
            if each != defItem.TemplateName:
                defItem.TemplateDropdown.addItem( each )
    
    def SetupTemplateDropdown ( self, definition ):
        font = QtGui.QFont()
        font.setPointSize( 11 )
        font.setBold( True )

        definition.DefinitionLabelName = QtGui.QLabel( self )
        definition.DefinitionLabelName.setText( definition.DefinitionFilename )
        definition.DefinitionLabelName.setIndent( 5 )
        definition.DefinitionLabelName.setFixedHeight( 26 )
        definition.DefinitionLabelName.setFont( font )
        
        
        if RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            definition.TemplateDropdown = QtGui.QComboBox( self )
            self.PopulateComboBox( definition )
            definition.TemplateDropdown.setFont( font )
            definition.TemplateDropdown.activated[ str ].connect( self.onTemplateChanged )
            definition.LoadDefinition()
            
        else:
            definition.TemplateDropdown = QtGui.QLabel( self )
            definition.TemplateDropdown.setFont( font )
            definition.TemplateDropdown.setIndent( 5 )
            definition.TemplateDropdown.setFixedHeight( 26 )
            definition.TemplateDropdown.setText( definition.TemplateName.replace( "_", " " ) )
            definition.LoadDefinition()
            
        
    ## Event Handlers ##
    
    def ZoomExtents( self ):
        view = RS.Tools.Face.Facestar.ViewManager.GetView( self.GetCurrentDefinition() )
        view.ZoomExtents()
    
    def ZoomSelected( self ):
        view = RS.Tools.Face.Facestar.ViewManager.GetView( self.GetCurrentDefinition() )
        view.ZoomSelected()
    
    def NewSelectionSet( self ):
        text, okResult = QtGui.QInputDialog.getText( self, 'New Selection Set', 'Name:' )
        
        if okResult:
            RS.Tools.Face.Facestar.LocalConfig.CreateNewSelectionSet( text )
    
    def OpenSelectionSetManager( self ):
        dialog = RS.Tools.Face.Facestar.SelectionSet.ManagerDialog( self, self.GetCurrentDefinition() )
        dialog.show()
    
    def SwitchToHeadCam( self ):
        definition = self.GetCurrentDefinition()
        headCam = FBFindModelByLabelName( '{0}:headCam'.format( definition.Namespace ) )
        
        if not headCam:
            parent = RS.Core.Face.Toolbox.findHeadByNamespace( definition.Namespace )
            
            if parent:
                RS.Core.Face.Toolbox.createConstrainedHeadCam( definition.Namespace, parent )
                headCam = FBFindModelByLabelName( '{0}:headCam'.format( definition.Namespace ) )
                
        if headCam:
            FBApplication().SwitchViewerCamera( headCam )
    
    def OpenFacestarHelp( self ):
        webbrowser.open( 'https://devstar.rockstargames.com/wiki/index.php/Facestar' )
    
    def SaveDefinition( self ):
        self.__tabPages[ self.GetCurrentTabIndex() ].SaveDefinition()
        
    def ToggleEditMarkers( self ):
        if RS.Tools.Face.Facestar.Permissions.CanEdit():
            RS.Tools.Face.Facestar.MODE_EDIT_MARKERS = self.actionEditMarkers.isChecked()
            if not RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
                
                #Check to see if user wants to save
                flags = QtGui.QMessageBox.StandardButton.Yes 
                flags |= QtGui.QMessageBox.StandardButton.No
                flags |= QtGui.QMessageBox.StandardButton.Cancel
                question = "Would you like to save this layout?"
                response = QtGui.QMessageBox.question( self, "Facestar", question, flags )
                if response == QtGui.QMessageBox.Yes:
                    self.SaveDefinition()
                elif response == QtGui.QMessageBox.Cancel:
                    self.__tabPages[ self.GetCurrentTabIndex() ].SetMarkerEditMode( True )
                    self.actionEditMarkers.Cancelled = True
                    self.actionEditMarkers.toggle()
                    return
                else:
                    #if True == True:
                    # Compare value of current toggle with saved toggle.  If different, change it back.
                    currentDef = self.GetCurrentDefinition()
                    if currentDef.TemplateName != currentDef.TemplateFileName:
                        currentDef.TemplateName = None
                        self.__definitions = RS.Core.Face.Facestar.GetSceneDefinitions()
                        pass
                    else:
                        pass
            
            if not self.actionEditMarkers.Cancelled:
                #self.RefreshTabs()
                self.__tabPages[ self.GetCurrentTabIndex() ].SetMarkerEditMode( self.actionEditMarkers.isChecked() )
            else:
                self.actionEditMarkers.Cancelled = False
            
        else:
            if self.actionEditMarkers.isChecked():
                self.actionEditMarkers.setChecked( False )
                msg = QtGui.QMessageBox.warning( self, 'Facestar', 'You do not have permissions to edit the marker placement!  Please contact your Lead if you feel this is in error.' )
        
    def Play( self ):
        self.__playerControl.Play()
        
    def Stop( self ):
        self.__playerControl.Stop()
        
    def Rewind( self ):
        self.__playerControl.GotoStart()
        
    def GoToEnd( self ):
        self.__playerControl.GotoEnd()
        
    def StepForward( self ):
        self.__playerControl.StepForward()
        
    def StepBackward( self ):
        self.__playerControl.StepBackward()
        
    def Key( self ):
        self.__playerControl.Key()
        
    def GoToNextKey( self ):
        self.__playerControl.GotoNextKey()
        
    def GoToPreviousKey( self ):
        self.__playerControl.GotoPreviousKey()