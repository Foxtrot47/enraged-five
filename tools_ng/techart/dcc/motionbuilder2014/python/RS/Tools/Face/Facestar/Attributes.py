from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtCore, QtGui

import RS.Tools.Face.Facestar.Sliders
reload( RS.Tools.Face.Facestar.Sliders )
    
        
class AttributeGroup( QtGui.QWidget ):
    '''
    Represents a group of off-face slider attributes.  Each group show up under a different, expandable tab.
    
    Arguments:
    
        definition: A RS.Core.Face.Facestar.Definition object.
        attributeGroupObj: A RS.Core.Face.Facestar.AttributeGroup object.
    '''
    def __init__( self, definition, attributeGroupObj, parent, *args, **kwargs ):
        QtGui.QWidget.__init__( self, *args, **kwargs )
        
        self.__parent = parent

        self.__definition = definition
        self.__attributeGroup = attributeGroupObj        
        
        self.__mainLayout = QtGui.QVBoxLayout( self )
        self.__mainLayout.setAlignment( QtCore.Qt.AlignTop )
        
        self.setLayout( self.__mainLayout )
        
        self.__sliders = {}
        
        for attribute in self.__attributeGroup.Attributes:
            sliderLabel = QtGui.QLabel( self )
            sliderLabel.setText( attribute.DisplayName )
            
            slider = RS.Tools.Face.Facestar.Sliders.AttributeSlider( definition, attribute, self )
            
            self.__mainLayout.addWidget( sliderLabel )
            self.__mainLayout.addWidget( slider )
            
            self.__sliders[ attribute.Name ] = slider
            

    def SelectAttribute( self, attributeName ):
        if attributeName in self.__sliders:
            self.__sliders[ attributeName ].Select()
    
    def DeselectAll( self ):
        for attributeName, slider in self.__sliders.iteritems():
            slider.Deselect()
            
    def keyPressEvent( self, event ):
        if event.key() == QtCore.Qt.Key.Key_Control:
            RS.Tools.Face.Facestar.ControlPressed = True
    
    def keyReleaseEvent( self, event ):
        RS.Tools.Face.Facestar.ControlPressed = False
        
    def mousePressEvent( self, event ):
        self.setFocus()
        
        for attrName, slider in self.__sliders.iteritems():
            slider.repaint()
            slider.update()
            
        QtGui.QWidget.mousePressEvent( self, event )    
            
class Sidebar( QtGui.QWidget ):
    '''
    Widget that holds off-face attribute sliders.  This will show up on the side of the viewport in a splitter panel.
    
    Arguments:
    
        parent: The parent QWidget.
        defintion: A RS.Core.Face.Facestar.Definition object.
    '''
    def __init__( self, parent, definition ):
        QtGui.QWidget.__init__( self, parent )
        
        layout = QtGui.QVBoxLayout( self )
        self.setLayout( layout )
        
        # Attribute sliders.
        toolbox = QtGui.QToolBox()
        toolbox.setMinimumWidth( 234 )

        TemplateDropdownLabel = QtGui.QLabel( self )
        TemplateDropdownLabel.setText( "Rig Template: " )
        DefinitionLabel = QtGui.QLabel( self )
        DefinitionLabel.setText( "Definition: " )
        
        layout.addWidget( TemplateDropdownLabel )
        layout.addWidget( definition.TemplateDropdown )
        layout.addWidget( DefinitionLabel )
        layout.addWidget( definition.DefinitionLabelName )
        layout.addWidget( toolbox )
        
        
        for group in definition.AttributeGroups:
            groupWidget = AttributeGroup( definition, group, self )
            toolbox.addItem( groupWidget, group.Name )
            
        self.__selectedAttributes = []
        
        self.__definition = definition
    
    @property
    def Definition( self ):
        return self.__definition
    
    def SelectAttribute( self, attributeName ):
        pass