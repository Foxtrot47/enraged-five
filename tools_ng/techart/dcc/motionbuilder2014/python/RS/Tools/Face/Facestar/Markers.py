from pyfbsdk import *
from pyfbsdk_additions import *

from PySide import QtCore, QtGui

import RS.Tools.Face.Facestar
#import RS.Tools.Face.Facestar.SelectionManager

reload( RS.Tools.Face.Facestar )
#reload( RS.Tools.Face.Facestar.SelectionManager )


class OffFace( QtGui.QGraphicsItem ):
    '''
    Represents the movable marker on an off-face slider object.
    '''
    def __init__( self, definition, parent, size, modelName, *args, **kwargs ):
        QtGui.QGraphicsItem.__init__( self, *args, **kwargs )
        
        self.__definition = definition
        self.__parent = parent
        self.__modelAnimationNode = parent.Model.PropertyList.Find( 'Lcl Translation' ).GetAnimationNode()
        
        # The x, y value of the marker in controller space.  Typically it'll be in the -1.0 to 1.0 range.
        self.__value = QtCore.QPointF( 0.0, 0.0 )
        
        # Pixel size of the marker.
        self.__size = size
        
        # Setup flags.
        self.setParentItem( parent )
        self.setFlag( QtGui.QGraphicsItem.ItemIsMovable, True )
        self.setFlag( QtGui.QGraphicsItem.ItemIsSelectable, True )
        self.setFlag( QtGui.QGraphicsItem.ItemSendsGeometryChanges, True )
        self.setAcceptHoverEvents( True )
        
        # Setup timer.  When the timer is running, it queries the model position and updates our marker position
        # within the gui.
        self.__timerSpeed = 30
        self.__timer = QtCore.QTimer()
        self.__timer.timeout.connect( self.Update )
        self.__timer.start( self.__timerSpeed )
        
        # Flag for whether the model is allowed to be updated from this marker.
        self.__allowModelUpdateFromWidget = False
        
        # Bounding rect for hittesting and drawing.        
        self.__boundingRect = QtCore.QRectF( -self.__size - 1.0, -self.__size - 1.0, self.__size * 2.0 + 2.0, self.__size * 2.0 + 2.0 )
        
        # Rendering objects.
        self.__greenBrush = QtGui.QBrush( QtGui.QColor( 0, 255, 0 ) )
        self.__whiteBrush = QtGui.QBrush( QtGui.QColor( 255, 255, 255 ) )
        self.__blackBrush = QtGui.QBrush( QtGui.QColor( 0, 0, 0 ) )
        self.__redBrush = QtGui.QBrush( QtGui.QColor( 255, 0, 0 ) )
        self.__redPen = QtGui.QPen( QtGui.QColor( 255, 0, 0 ), 2 )
        self.__blackPen = QtGui.QPen( QtGui.QColor( 0, 0, 0 ), 2 )
        self.__thinBlackPen = QtGui.QPen( QtGui.QColor( 0, 0, 0 ), 1 )
        self.__whitePen = QtGui.QPen( QtGui.QColor( 255, 255, 255 ), 2 )
        self.__thinWhitePen = QtGui.QPen( QtGui.QColor( 255, 255, 255 ), 1 )
        
        self.__lastX = self.pos().x()
        self.__lastY = self.pos().y()
    
    ## Properties ##
    
    @property
    def ValueX( self ):
        return self.__value.x()
        
    @ValueX.setter
    def ValueX( self, value ):
        self.__value.setX( value )
 
        # Adjust marker position to match our value.
        pos = self.GetPixelPosFromControllerPos( QtCore.QPointF( value, self.ValueY ) )
        self.setX( pos.x() )
        
    @property
    def ValueY( self ):
        return self.__value.y()
        
    @ValueY.setter
    def ValueY( self, value ):
        self.__value.setY( value )
 
        # Adjust marker position to match our value.
        pos = self.GetPixelPosFromControllerPos( QtCore.QPointF( self.ValueX, value ) )
        self.setY( pos.y() )
        
    @property
    def ModelName( self ):
        return self.__parent.ModelName
        
    
    ## Methods ##    
    
    def Update( self ):
        '''
        Updates this marker position based on its associated controller position.
        '''
        try:
            modelPos = FBVector3d( self.__parent.Model.Translation.Data )
            self.ValueX = modelPos[ 0 ]
            self.ValueY = modelPos[ 1 ]
            
        except:
            pass
        
        self.update()
        
    def GetPixelPosFromControllerPos( self, controllerPos ):
        '''
        Returns the pixel-space coordinates of the supplied controller-space position.
        
        Arguments:
            
            controllerPos: The controller-space coordinates to query.  Requires a QtCore.QPointF object.
        '''
        minX = self.__parent.Min.x()
        maxX = self.__parent.Max.x()
        
        minY = self.__parent.Min.y()
        maxY = self.__parent.Max.y()
        
        pMinX = self.__parent.ConstraintRect.left()
        pMaxX = self.__parent.ConstraintRect.right()
        
        pMinY = self.__parent.ConstraintRect.top()
        pMaxY = self.__parent.ConstraintRect.bottom()
        
        x = 0.0
        y = 0.0
        
        try:
            cX = controllerPos.x() - minX
            nX = cX / ( maxX - minX )
            x = nX * ( pMaxX - pMinX ) + pMinX
            
        except ZeroDivisionError:
            pass
            
        try:
            cY = controllerPos.y() - minY
            nY = cY / ( maxY - minY )
            y = nY * ( pMaxY - pMinY ) + pMinY
            
        except ZeroDivisionError:
            pass
            
        return QtCore.QPointF( -x, y )
        
    def GetControllerPosFromPixelPos( self, pixelPos ):
        '''
        Returns the controller-space coordinates of the supplied pixel-space position.
        
        Arguments:
            
            pixelPos: The pixel-space coordinates to query.  Requires a QtCore.QPointF object.
        '''
        x = 0.0
        y = 0.0
        
        minX = self.__parent.Min.x()
        maxX = self.__parent.Max.x()
        
        minY = self.__parent.Min.y()
        maxY = self.__parent.Max.y()
        
        pMinX = self.__parent.ConstraintRect.left()
        pMaxX = self.__parent.ConstraintRect.right()
        
        pMinY = self.__parent.ConstraintRect.top()
        pMaxY = self.__parent.ConstraintRect.bottom()
        
        try:
            pX = -pixelPos.x() - pMinX
            nX = pX / ( pMaxX - pMinX )
            x = nX * ( maxX - minX ) - abs( minX )
            
        except ZeroDivisionError:
            pass
        
        try:
            pY = pixelPos.y() - pMinY
            nY = pY / ( pMaxY - pMinY )
            y = nY * ( maxY - minY ) - abs( minY )
            
        except ZeroDivisionError:
            pass
            
        return QtCore.QPointF( x, y )
    
    def HasKeysAtCurrentTime( self ):
        '''
        Return whether or not the model object for this marker has a keyframe at the current time.
        '''
        return self.__modelAnimationNode.IsKey()
    
    def AllowModelUpdateFromWidget( self, TF ):
        self.__allowModelUpdateFromWidget = TF
        
    def CapturePosition( self ):
        self.__lastX = self.pos().x()
        self.__lastY = self.pos().y()
        
    def CaptureCurrentPosition( self ):
        self.__currentX = self.pos().x()
        self.__currentY = self.pos().y()

    
    ## Overrides ##

    def mousePressEvent( self, event ):
        '''
        if ( self not in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ) ):
        if RS.Tools.Face.Facestar.ControlPressed:
        '''
        self.__parent.Viewport.CapturePosition()
        QtGui.QGraphicsItem.mousePressEvent( self, event )
        #if RS.Tools.Face.Facestar.ControlPressed:
        #    RS.Tools.Face.Facestar.SelectionManager.ReselectAllMarkers( self.__definition )
            
        
    def mouseMoveEvent( self, event ):
        RS.Tools.Face.Facestar.SelectionManager.SetModelUpdateFromWidget( True, self.__definition )
        self.__parent.Viewport.CaptureCurrentPosition()
        
        if RS.Tools.Face.Facestar.AltPressed:
            self.__lastX = self.__currentX
        
        if RS.Tools.Face.Facestar.ShiftPressed:
            self.__lastY = self.__currentY
        
        QtGui.QGraphicsItem.mouseMoveEvent( self, event )
    
    def mouseReleaseEvent( self, event ):
        
        QtGui.QGraphicsItem.mouseReleaseEvent( self, event )
        RS.Tools.Face.Facestar.SelectionManager.SetModelUpdateFromWidget( False, self.__definition )
        self.__parent.Viewport.CapturePosition()
    
    def itemChange( self, change, value ):
        '''
        Constrain the marker within a rect.
        '''
        if change == QtGui.QGraphicsItem.GraphicsItemChange.ItemPositionChange:
            x = value.x()
            y = value.y()
            
            if RS.Tools.Face.Facestar.AltPressed:
                x = self.__lastX
                
            if RS.Tools.Face.Facestar.ShiftPressed:
                y = self.__lastY
            
            # First constraint the marker within the parent constraint rect.
            if y > self.__parent.ConstraintRect.bottom():
                y = self.__parent.ConstraintRect.bottom()
                
            elif y < self.__parent.ConstraintRect.top():
                y = self.__parent.ConstraintRect.top()

            if x > self.__parent.ConstraintRect.right():
                x = self.__parent.ConstraintRect.right()
                
            elif x < self.__parent.ConstraintRect.left():
                x = self.__parent.ConstraintRect.left()
              
            # Update this marker's controller space value.
            controllerPos = self.GetControllerPosFromPixelPos( QtCore.QPointF( x, y ) )
            self.__value.setX( controllerPos.x() )
            self.__value.setY( controllerPos.y() )
                             
            # Update controller model in the scene.
            if self.__allowModelUpdateFromWidget:
                modelPos = FBVector3d( self.__parent.Model.Translation.Data )
                newModelPos = FBVector3d( self.ValueX, self.ValueY, modelPos[ 2 ] )
                self.__parent.Model.Translation = newModelPos
                
            value.setX( x )
            value.setY( y )

            return value

        return QtGui.QGraphicsItem.itemChange( self, change, value )
        
    def boundingRect( self ):
        return self.__boundingRect
        
    def paint( self, painter, option, widget ):
        if not RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            if self.isSelected():
                
                # Outer circle.
                painter.setPen( self.__thinBlackPen )
                painter.setBrush( self.__whiteBrush )
                painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size + 3, self.__size + 3 )
        
                painter.setPen( self.__blackPen )
                
                if self.__modelAnimationNode != None:
                    if self.HasKeysAtCurrentTime():
                        painter.setBrush( self.__redBrush )
                        
                    else:
                        painter.setBrush( self.__greenBrush )
                else:
                    painter.setBrush( self.__greenBrush )
                        
                painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size, self.__size )   
                
            else:
                painter.setBrush( self.__blackBrush )
                painter.setPen( self.__thinWhitePen )  
                painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size + 3, self.__size + 3 )
                painter.setPen( self.__whitePen )
                painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size, self.__size )
                
    def TimerStop ( self ):
        self.__timer.stop()
        
    def TimerStart ( self ):
        self.__timer.start( self.__timerSpeed )
        
        
class OnFace( QtGui.QGraphicsItem ):
    '''
    Represents an on-face marker object.  Mostly for selection purposes only.
    
    Arguments:
    
        marker: A RS.Core.Face.Facestar.Marker object.
        definition: A RS.Core.Face.Facestar.Definition object.
        pos: The (x, y) position in pixel-space for the marker.
    '''
    def __init__( self, marker, parent, definition, pos, *args, **kwargs ):
        QtGui.QGraphicsItem.__init__( self, *args, **kwargs )
        
        
        self.__marker = marker
        self.__definition = definition
        self.__parent = parent
        
        self.__modelName = '{0}:{1}'.format( definition.Namespace, marker.Name )
        self.__model = FBFindModelByLabelName( self.__modelName )
        
        if not self.__model:
            #raise AttributeError( 'Model using the name "{0}" could not be found!'.format( self.__modelName ) )
            print 'Model using the name "{0}" could not be found!'.format( self.__modelName )
        
        self.setFlag( QtGui.QGraphicsItem.ItemIsMovable, False )
        self.setFlag( QtGui.QGraphicsItem.ItemIsSelectable, True )
        self.setAcceptHoverEvents( True )
        self.setToolTip( marker.DisplayName )
        
        self.__size = 8        
        
        self.__boundingRect = QtCore.QRectF( -self.__size - 1.0, -self.__size - 1.0, self.__size * 2.0 + 3.0, self.__size * 2.0 + 3.0 )
        
        # Timer
        self.__timerSpeed = 30
        self.__timer = QtCore.QTimer()
        self.__timer.timeout.connect( self.Update )
        self.__timer.start( self.__timerSpeed )
        
        # Rendering objects.
        self.__greenBrush = QtGui.QBrush( QtGui.QColor( 0, 255, 0, 100 ) )
        self.__whiteBrush = QtGui.QBrush( QtGui.QColor( 255, 255, 255 ) )
        self.__blackBrush = QtGui.QBrush( QtGui.QColor( 0, 0, 0 ) )
        self.__redBrush = QtGui.QBrush( QtGui.QColor( 255, 0, 0 ) )
        self.__redPen = QtGui.QPen( QtGui.QColor( 255, 0, 0 ), 2 )
        self.__blackPen = QtGui.QPen( QtGui.QColor( 0, 0, 0 ), 2 )
        self.__thinBlackPen = QtGui.QPen( QtGui.QColor( 0, 0, 0 ), 1 )
        self.__whitePen = QtGui.QPen( QtGui.QColor( 255, 255, 255 ), 2 )
        self.__thinWhitePen = QtGui.QPen( QtGui.QColor( 255, 255, 255 ), 1 )
        
        self.setPos( pos[ 0 ], pos[ 1 ] )
        
    
    ## Properties ##
        
    @property
    def ModelName( self ):
        return self.__modelName
    
    @property
    def Model( self ):
        return self.__model
    
    ## Methods ##
    
    def Update( self ):
        # Force redraw.
        self.update()
        
    def CapturePosition( self ):
        pass
        
    def CaptureCurrentPosition( self ):
        pass
    
    ## Overrides ##
    
    def mousePressEvent( self, event ):
        '''
        if ( self not in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition.Namespace ) ):
            if RS.Tools.Face.Facestar.ControlPressed:
                pass
            else:
                RS.Tools.Face.Facestar.SelectionManager.DeselectAllMarkers( self.__definition )
                self.__parent.OnFaceView.Viewport.UpdateSelection()
                
        self.setSelected( True )
        '''
        #RS.Tools.Face.Facestar.SelectionManager.Select( self.__definition, self )
        
        QtGui.QGraphicsItem.mousePressEvent( self, event )
        self.setSelected( True )
        '''
        if ( self in RS.Tools.Face.Facestar.SelectionManager.GetSelected( self.__definition ) ):
            if RS.Tools.Face.Facestar.ControlPressed:
                self.setSelected( False )
        
        self.__parent.OnFaceView.Viewport.UpdateSelection()
        '''
        
    def mouseReleaseEvent( self, event ):
        
        QtGui.QGraphicsItem.mouseReleaseEvent( self, event )
        '''
        self.__parent.OnFaceView.Viewport.ReselectAllMarkers()
        self.__parent.OnFaceView.Viewport.UpdateSelection()
        '''
        
    '''    
    def mousePressEvent( self, event ):
        self.setSelected( True )
        
        self.__lastX = self.pos().x()
        self.__lastY = self.pos().y()
        self.__allowModelUpdateFromWidget = True
        self.__timer.stop()
        
        QtGui.QGraphicsItem.mousePressEvent( self, event )
        
    def mouseReleaseEvent( self, event ):
        self.setSelected( False )
        
        self.__lastX = self.pos().x()
        self.__lastY = self.pos().y()
        
        self.__parent.Model.Selected = False
        self.__allowModelUpdateFromWidget = False
        self.__timer.start( self.__timerSpeed )
        
        QtGui.QGraphicsItem.mouseReleaseEvent( self, event )
    '''
        
    def boundingRect( self ):
        return self.__boundingRect
        
    def paint( self, painter, option, widget ):
        if not RS.Tools.Face.Facestar.MODE_EDIT_MARKERS:
            if self.__model.Selected:
                painter.setBrush( self.__greenBrush )      
                
            else:
                painter.setBrush( self.__blackBrush )
                
            painter.setPen( self.__thinWhitePen )  
            painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size + 3, self.__size + 3 )
            painter.setPen( self.__whitePen )
            painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size, self.__size ) 
        
        else:
            if self.isSelected():
                painter.setBrush( QtGui.QColor( 0, 255, 0, 25 ) )
                painter.setPen( QtGui.QColor( 0, 255, 0 ) )
                
            else:
                painter.setBrush( QtGui.QColor( 0, 0, 255, 25 ) )
                painter.setPen( QtGui.QColor( 0, 0, 255 ) )                

            painter.drawEllipse( QtCore.QPointF( 0, 0 ), self.__size + 3, self.__size + 3 )
            
            if self.isSelected():
                painter.setBrush( QtGui.QColor( 0, 255, 0 ) )
                painter.setPen( QtGui.QColor( 0, 255, 0 )  )
                
            else:
                painter.setBrush( QtGui.QColor( 0, 0, 255 ) )
                painter.setPen( QtGui.QColor( 0, 0, 255 )  )                

            painter.drawEllipse( QtCore.QPointF( 0, 0 ), 2, 2 )