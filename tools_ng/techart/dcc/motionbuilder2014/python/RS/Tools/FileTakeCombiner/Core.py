import os
import time
import pyfbsdk as mobu
import fbx

from RS import Globals
from RS.Utils.Scene import Take


class TakeData(object):
    '''
        Package take informations.
    '''
    def __init__(self):
        self.name = ''
        self.zoomRange = [0,1]
        self.globalRange = [0,1]


class FileAnimationStack(object):
    '''
        FileTakes data structure.
    '''
    def __init__(self):
        self.filePath = ''
        self.fileName = ''
        self.currentTake = ''
        self.takes = []
        self.takeNames = []
        self.importWasSuccessful = False
        self.errorResult = ''

    def get(self, inputFile):
        '''
            Package takes Data from an input file.

            Args:
                inputFile (str): motionbuilder filepath used to merge TakeData from.
        '''
        if not os.path.exists(inputFile):
            return

        ioFileManager = fbx.FbxManager.Create()

        #Prepare scene parser
        externalTakesParser = fbx.FbxImporter.Create(ioFileManager, 'FileTakeParser')

        ioSettings = fbx.FbxIOSettings.Create(ioFileManager, fbx.IOSROOT)

        self.importWasSuccessful = externalTakesParser.Initialize(inputFile, -1, ioSettings)

        if not self.importWasSuccessful:
            self.errorResult = externalTakesParser.GetLastErrorString()
            return

        inputTakeCount = externalTakesParser.GetAnimStackCount()
        takeInfo = fbx.FbxTakeInfo()

        takeLocalTimeSpan = fbx.FbxTimeSpan()
        takeReferenceTimeSpan = fbx.FbxTimeSpan()

        self.filePath = inputFile
        self.fileName = os.path.basename(inputFile)
        self.fileName = os.path.splitext(self.fileName)[0]

        for index in xrange(inputTakeCount):
            takeInfo = externalTakesParser.GetTakeInfo(index);

            #Store relevant Data
            animTake = TakeData()
            animTake.name = '{0}_{1}'.format(self.fileName, takeInfo.mName)
            takeLocalTimeSpan = takeInfo.mLocalTimeSpan
            takeReferenceTimeSpan = takeInfo.mReferenceTimeSpan

            self.takeNames.append(animTake.name)

            animTake.zoomRange = [takeLocalTimeSpan.GetStart().GetSecondDouble(),
                                  takeLocalTimeSpan.GetStop().GetSecondDouble()]

            animTake.globalRange = [takeReferenceTimeSpan.GetStart().GetSecondDouble(),
                                    takeReferenceTimeSpan.GetStop().GetSecondDouble()]

            self.takes.append(animTake)

        self.currentTake = -1

        externalTakesParser.Destroy()
        ioFileManager.Destroy()


def SetMergeTakeParameters(filePath,
                           animationStack):
    '''
        Set the correct import options in order to be able to merge takes from a list of files.

        Args:
            filePath(str): motionbuilder file path.
            animationStack (FileAnimationStack): data structure holding Take informations.

        Returns (FBFbxOptions).
    '''
    mergeOptions = mobu.FBFbxOptions(True, filePath)

    mergeOptions.SetAll(mobu.FBElementAction.kFBElementActionDiscard , False)

    mergeOptions.Story   = mobu.FBElementAction.kFBElementActionDiscard
    mergeOptions.Lights  = mobu.FBElementAction.kFBElementActionDiscard
    mergeOptions.Devices  = mobu.FBElementAction.kFBElementActionDiscard

    mergeOptions.IgnoreConflicts = True
    mergeOptions.UpdateRecentFiles = False

    mergeOptions.ConstraintsAnimation = True
    mergeOptions.BonesAnimation = True
    mergeOptions.CharactersAnimation = True
    mergeOptions.ModelsAnimation = True

    #Support File references from motion builder?
    #For this tool we only want to transfer animation from external takes
    #So the answer  is supposed to be no
    processReference = False
    if processReference:
        mergeOptions.FileReference = True
        mergeOptions.FileReferenceEdit = True

    #CharacterAnimation Options
    mergeOptions.TransferMethod = mobu.FBCharacterLoadAnimationMethod.kFBCharacterLoadCopy
    mergeOptions.ProcessAnimationOnExtension = True

    #We need to limit the cache size use for import (>1Go doesnt improve reading from disk)
    mergeOptions.CacheSize = 1024
    mergeOptions.TakeSpan =  mobu.FBTakeSpanOnLoad.kFBFrameAnimation
    mergeOptions.UpdateRecentFiles = False

    for index in xrange(len(animationStack.takeNames)):
        mergeOptions.SetTakeDestinationName(index,animationStack.takeNames[index])
        mergeOptions.SetTakeSelect(index, True)

    return mergeOptions


def loadTakeParameters(filePath):
    '''
        Set the correct Loading options in order to open the master shti file[the file we want to merge take into].

        Args:
            filePath(str): motionbuilder file path.

        Returns (FBFbxOptions).
    '''
    mergeOptions = mobu.FBFbxOptions(True, filePath)

    for index in xrange(mergeOptions.GetTakeCount()):
        mergeOptions.SetTakeSelect(index, False)

    return mergeOptions


def CollectTakeMetaData(fbxfiles):
    '''
        Package takes Data from an input file.

        Args:
            fbxfiles(list): list of motionbuilder scene files.

        Returns (list of FileAnimationStack).
    '''
    animationStackArray = []

    for inputFile in fbxfiles:
        animTakes = FileAnimationStack()
        animTakes.get(inputFile)

        if animTakes.importWasSuccessful :
            animationStackArray.append(animTakes)

    return animationStackArray


def MergeTakesFromFiles(fbxfiles,appendInFirstFile=True):
    '''
        Internal Method

        Merges all Takes from the provided file list .
        Args:
            fbxfiles(list): list of motionbuilder scene files.
    '''
    #We need to check inputPath here
    mobu.FBApplication().FileNew()
    currentScene = mobu.FBSystem().Scene
    currentScene.Evaluate()

    filepathList = list(fbxfiles)
    animTakesArray = CollectTakeMetaData(filepathList)

    openFileWithoutTakeOptions = loadTakeParameters(filepathList[0])

    mobu.FBApplication().FileOpen(fbxfiles[0],
                                  False,
                                  openFileWithoutTakeOptions)

    mobu.FBSystem().Scene.Evaluate()

    #Store the CurrentTake
    openFileCurrentTake = mobu.FBSystem().CurrentTake

    #Set the new takes name for the master file
    if len(animTakesArray)>0:
        initialTakes = Take.GetTakeDictionary()
        for take in initialTakes.values():
            take.Name = '{0}_{1}'.format(animTakesArray[0].fileName, take.Name)

    #Import and apply prefix to takes
    for index,inputFile in enumerate(filepathList):
        for camera in mobu.FBSystem().Scene.Cameras:
            camera.HUDs.removeAll()

        mergeTakeOptions = SetMergeTakeParameters(inputFile,
                                                  animTakesArray[index])

        Globals.Application.FileMerge(inputFile, mobu.FBElementAction.kFBElementActionMerge, mergeTakeOptions)
        #It is critical to refresh the scene tree between imports
        mobu.FBSystem().Scene.Evaluate()

    for camera in mobu.FBSystem().Scene.Cameras:
        camera.HUDs.removeAll ()

        #Set layer frame range for everyTake
        finalTakesList = Take.GetTakeDictionary()
        startTime = mobu.FBTime()
        endTime = mobu.FBTime()

        for animationStack in animTakesArray:
            for take in animationStack.takes:
                startTime.SetSecondDouble(take.zoomRange[0])
                endTime.SetSecondDouble(take.zoomRange[1])
                finalTakesList[take.name].LocalTimeSpan = mobu.FBTimeSpan(startTime, endTime)

                startTime.SetSecondDouble(take.globalRange[0])
                endTime.SetSecondDouble(take.globalRange[1])
                finalTakesList[take.name].ReferenceTimeSpan = mobu.FBTimeSpan(startTime, endTime)

        mobu.FBSystem().CurrentTake = openFileCurrentTake