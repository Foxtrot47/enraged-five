from PySide import QtGui

from RS.Tools import UI
from RS.Core.ReferenceSystem.Manager import Manager


class EditNamespaceDialog(UI.QtMainWindowBase):

    def __init__(self, referenceNamespace, mainModel, treeView, parent=None):
        """
        Constructor
        """
        self._toolName = "Namespace Editor"
        self._size = [400, 100]
        super(EditNamespaceDialog, self).__init__(title=self._toolName,
                                                  size=self._size,
                                                  dialog=True,
                                                  dockable=False)

        self.manager = Manager()
        self._origreferenceNamespace = referenceNamespace
        self._mainModel = mainModel
        self._treeView = treeView
        self._characterList = []
        self._nameLineEdit = None
        self._origNamespaceLabel = None

        self.setupUi()

    def _handleSetLineEditDefaultText(self):
        # set default namespace
        self._nameLineEdit.setText(selectedNamespace)
        return

    def _handleUpdateNamespace(self):
        # get old and new names
        oldName = str(self._origNamespaceLabel.text())
        newName = str(self._nameLineEdit.text())

        #Call the manager's Change namespace function
        self.manager.ChangeNamespace(self.manager.GetReferenceByNamespace(oldName), newName)

        # reload ui data
        self._mainModel.reset()
        self._treeView.expandAll()

        # dialog will automatically close the main widget, but we still have to close the window
        self._parent.accept()

    def setupUi(self):
        # create layout
        layout = QtGui.QGridLayout()

        # create widgets
        mainWidget = QtGui.QWidget()
        namespaceLabel = QtGui.QLabel('Original Namespace:')
        self._origNamespaceLabel = QtGui.QLabel()
        self._origNamespaceLabel.setText(str(self._origreferenceNamespace))
        updateLabel = QtGui.QLabel('New Namespace:')
        self._nameLineEdit = QtGui.QLineEdit()
        self._nameLineEditText = self._nameLineEdit.setText(self._origNamespaceLabel.text())
        updatePushButton = QtGui.QPushButton('Update Namespace')

        # add widgets to layout
        layout.addWidget(namespaceLabel, 0, 0)
        layout.addWidget(self._origNamespaceLabel, 0, 1)
        layout.addWidget(updateLabel, 1, 0)
        layout.addWidget(self._nameLineEdit, 1, 1)
        layout.addWidget(updatePushButton, 2, 1)

        # set layout
        mainWidget.setLayout(layout)
        self.setCentralWidget(mainWidget)

        self.setContentsMargins(0, 0, 0, 0)

        # set signals
        updatePushButton.clicked.connect(self._handleUpdateNamespace)