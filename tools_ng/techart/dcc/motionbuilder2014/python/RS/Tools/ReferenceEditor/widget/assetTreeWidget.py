from PySide import QtCore, QtGui

from RS.Tools.ReferenceEditor import const, menuEvents
from RS.Tools.ReferenceEditor.model import assetModelTypes

from functools import partial


class AssetTreeWidget(QtGui.QTreeView):
    '''
    We create our TreeView here and generate actions
    '''
    # right click signal
    referenceMenuEvent = QtCore.Signal(object, object, list)
    # double click signal
    iconCommandEvent = QtCore.Signal(object, object)
    def __init__(self, model=None, parent=None):
        '''
        adding items to right click menu - unique sets of menu of categories and individual assets
        '''
        super(AssetTreeWidget, self).__init__(parent=parent)
        model = model or assetModelTypes.CategoriesModel()
        self.setModel(model)
        self.setSortingEnabled(True)
        self.sortByColumn(0, QtCore.Qt.AscendingOrder)
        self.header().setToolTip("Right Click assets for menus")
        self._categoriesWithSubCatsMenu = QtGui.QMenu()
        self._categoriesMenu = QtGui.QMenu()
        self._assetMenu = QtGui.QMenu()
        self._character = QtGui.QMenu()
        self._vehicle = QtGui.QMenu()
        self._threeLateral = QtGui.QMenu()
        self._ambientFace = QtGui.QMenu()
        self._audio = QtGui.QMenu()
        self._props = QtGui.QMenu()
        self._sets = QtGui.QMenu()
        self._rayfire = QtGui.QMenu()
        self._vehicles = QtGui.QMenu()
        self._mocapBasePreviz = QtGui.QMenu()
        self._mocapBlockingModel = QtGui.QMenu()
        self._mocapGsSkeleton = QtGui.QMenu()
        self._mocapPropToy = QtGui.QMenu()
        self._mocapSlate = QtGui.QMenu()
        self._mocapStage = QtGui.QMenu()

        self.menuDict = {
                    self._categoriesWithSubCatsMenu:(menuEvents.ContextMenuEventDict.categoriesWithSubCats, None),
                    self._categoriesMenu:(menuEvents.ContextMenuEventDict.categories, None),
                    self._assetMenu:(menuEvents.ContextMenuEventDict.individual, None),
                    self._character:(menuEvents.ContextMenuEventDict.character, 'Character'),
                    self._threeLateral:(menuEvents.ContextMenuEventDict.face, 'Face'),
                    self._ambientFace:(menuEvents.ContextMenuEventDict.ambientFace, 'AmbientFace'),
                    self._audio:(menuEvents.ContextMenuEventDict.audio, 'Audio'),
                    self._props:(menuEvents.ContextMenuEventDict.props, 'Prop'),
                    self._sets:(menuEvents.ContextMenuEventDict.sets, 'Set'),
                    self._rayfire:(menuEvents.ContextMenuEventDict.rayfire, 'Rayfire'),
                    self._vehicles:(menuEvents.ContextMenuEventDict.vehicles, 'Vehicle'),
                    self._mocapBasePreviz:(menuEvents.ContextMenuEventDict.mocapBasePreviz, 'Mocap BasePreviz'),
                    self._mocapBlockingModel:(menuEvents.ContextMenuEventDict.mocapBlockingModel, 'Mocap BlockingModel'),
                    self._mocapGsSkeleton:(menuEvents.ContextMenuEventDict.mocapGsSkeleton, 'Mocap gs Skeleton'),
                    self._mocapPropToy:(menuEvents.ContextMenuEventDict.mocapPropToy, 'Mocap PropToy'),
                    self._mocapSlate:(menuEvents.ContextMenuEventDict.mocapSlate, 'Mocap Slate'),
                    self._mocapStage:(menuEvents.ContextMenuEventDict.mocapStage, 'Mocap Stage'),

                    }

        for menu, actionList in self.menuDict.iteritems():
            if actionList[0]:
                for action in actionList[0]:
                    if action is None:
                        menu.addSeparator()
                        continue
                    actionKey = menu.addAction(action.KEY)
                    # toolTip
                    for menuType, toolTip in const.ContextMenuToolTipDict.tooltipDict.iteritems():
                        if menuType == action.KEY:
                            actionKey.setToolTip(toolTip)

    def _handleCategoriesWithSubCatsMenu(self, event, indexList, categoryList):
        '''
        set position for the category menu and emit a signal if a menu option is selected
        '''
        result = self._categoriesWithSubCatsMenu.exec_(event.globalPos())
        if result is None:
            return
        else:
            self.referenceMenuEvent.emit(indexList, menuEvents.ContextMenuBase.GetBinding(result.text()), categoryList)

    def _handleCategoriesMenu(self, event, indexList, categoryList):
        '''
        set position for the category menu and emit a signal if a menu option is selected
        '''
        result = self._categoriesMenu.exec_(event.globalPos())
        if result is None:
            return
        else:
            self.referenceMenuEvent.emit(indexList, menuEvents.ContextMenuBase.GetBinding(result.text()), categoryList)

    def _handleAssetsMenu(self, event, indexList, categoryList):
        '''
        set position for the individual asset menu and emit a signal if a menu option is selected
        '''
        # check that only one row has been selected and check what category it has been
        # selected from
        result = None
        categoryList = list(set(categoryList))
        if not len(categoryList) == 1:
            result = self._assetMenu.exec_(event.globalPos())

        for key, value in self.menuDict.iteritems():
            if len(value) > 1 and value[1] == categoryList[0]:
                result = key.exec_(event.globalPos())
                break

        if not result:
            return

        self.referenceMenuEvent.emit(indexList, menuEvents.ContextMenuBase.GetBinding(result.text()), [])

    def contextMenuEvent(self, event):
        '''
        user has used a right click - check if this is on a category item or individual asset
        populate menu with appropriate options.
        '''
        # selectedIndexes returns 1 item per column for each row,
        # we need to create a list which just has one item per row.
        selectedIndexes = []
        selectedDict = {}
        for idx in self.selectedIndexes():
            if not idx.data(0) == None:
                selectedDict[idx.row()] = idx
        for value in selectedDict.itervalues():
            selectedIndexes.append(value)

        # nothing is selected, do not display a right click menu
        if len(selectedIndexes) == 0:
            return

        # get list of category names (to be used in the path filter for 'create reference' menu option)
        catsNameList = [str(idx.data(0)) for idx in selectedIndexes]
        indvParentList = [idx.data(const.AssetModelDataIndex.DataDict)[3] for idx in selectedIndexes if idx.data(const.AssetModelDataIndex.DataDict)]

        # get list of datadicts (categories dont have a datadict, so will always return None)
        categoryList = [idx for idx in selectedIndexes if self.model().data(idx, const.AssetModelDataIndex.DataDict) is None]
        assetList = [idx for idx in selectedIndexes if self.model().data(idx, const.AssetModelDataIndex.DataDict) != None]

        if len(assetList) == 0:
            # category selected
            if idx.data(const.AssetModelDataIndex.ItemRole).SubCats is not None:
                self._handleCategoriesWithSubCatsMenu(event, categoryList, catsNameList)
            else:
                self._handleCategoriesMenu(event, categoryList, catsNameList)
        else:
            # asset selected
            self._handleAssetsMenu(event, assetList, indvParentList)

    def mouseDoubleClickEvent(self, event):
        '''
        user has double clicked, emit signal
        '''
        if event.button() != QtCore.Qt.MouseButton.LeftButton:
            return

        idx = self.currentIndex()
        data = self.model().data(idx, const.AssetModelDataIndex.DataDict)
        self.iconCommandEvent.emit(idx, data)