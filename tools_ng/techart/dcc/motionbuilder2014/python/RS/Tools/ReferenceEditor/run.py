from RS.Tools import UI
from RS.Tools import ReferenceEditor
from RS.Tools.ReferenceEditor.widget import referenceWidget


@UI.Run(title="Reference Editor", size=(450, 1000),
        dockable=True, url="https://hub.rockstargames.com/display/ANIM/Motionbuilder+Reference+Editor+UI")
def Run():
    return referenceWidget.ReferenceWidget()
