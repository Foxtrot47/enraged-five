from PySide import QtCore, QtGui

from functools import partial

from RS.Tools.ReferenceEditor import const
from RS.Core.Reference import GSPairing, HealthCheck


class AssetInfoWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(AssetInfoWidget, self).__init__(parent=parent)
        self._data = {}
        self._uiBits = {}
        self.orderedAssetInfo = (
                                    {'Asset Path:':(const.AssetModelIndex.Path, "Local Path to the Asset in P4.\n\nYou can select this path to copy paste.")},
                                    {'Have/Head Perforce Revision:' :(const.AssetModelIndex.HaveandHeadRevision, "Your current Perforce/Latest Perforce Revision.")},
                                    {'Have Perforce Revision Last Updated By:' :(const.AssetModelIndex.PerforceUserHaveRevision, "Resourcer who last updated your current revision of the selected asset.")},
                                )

        self.setupTab(self.orderedAssetInfo)

    def setupTab(self, orderedAssetInfo):
        layout = QtGui.QVBoxLayout()

        for assetInfo in self.orderedAssetInfo:
            for key, value in assetInfo.iteritems():
                label = QtGui.QLabel(key)
                label.setToolTip(value[1])
                editBox = QtGui.QLineEdit()
                editBox.setReadOnly(True)
                self._uiBits[key] = editBox
                self._data[key] = value[0]

                layout.addWidget(label)
                layout.addWidget(editBox)

        self.setLayout(layout)

    def populate(self, inputDict):
        for key, box in self._uiBits.iteritems():
            box.setText(str(inputDict.get(self._data[key], '')))

    def clear(self):
        for box in self._uiBits.itervalues():
            box.setText('')

class ComboBoxInformation():
    def __init__( self, Label, ComboBox, PairedCharacter, CharacterList ):
        self.Label = Label
        self.ComboBox = ComboBox
        self.PairedCharacter = PairedCharacter
        self.CharacterList = CharacterList


class GsPairingWidget(QtGui.QWidget):
    # signal for pairing warning icon
    displayWarningEvent = QtCore.Signal()
    removeWarningEvent = QtCore.Signal()

    def __init__(self, parent=None):
        super(GsPairingWidget, self).__init__(parent=parent)
        self._data = {}
        self.ComboBoxDataList = []
        self.checkbox = None
        self.ActorAnimManager = GSPairing.ActorAnimManager()

        self.setupTab()

    def setupTab(self):
        # layouts for tab
        vBoxLayout = QtGui.QVBoxLayout()
        gridLayout = QtGui.QGridLayout()

        # scroll area and content widget
        scrollArea = QtGui.QScrollArea(self)
        widget = QtGui.QWidget()

        # fonts for header labels
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)

        # header labels
        actorAnimLabel = QtGui.QLabel("Actor Anim")
        sceneCharactersLabel = QtGui.QLabel("Scene Characters")
        spacerLabel = QtGui.QLabel("")

        # checkbox to enable editing
        self.checkbox = QtGui.QCheckBox( "Enable Editing" )

        # set labels
        if self.ActorAnimManager.ActorAnimList:
            row = 3
            column = 0
            for anm in self.ActorAnimManager.ActorAnimList:
                # generate character list for combobox, based on characters not assigned to an anm already
                comboBoxCharacterList = ["None"]

                if anm.CharacterName != "None":
                    comboBoxCharacterList.append( anm.CharacterName )

                for unpairedChar in self.ActorAnimManager.GetUnpairedCharacterNames():
                    comboBoxCharacterList.append( unpairedChar )

                # create label and combobox
                label = QtGui.QLabel(str( anm.AnimName ))
                comboBox = QtGui.QComboBox()

                # disable combobox on initial startup of ref system
                comboBox.setEnabled(False)

                # add widgets to layout
                gridLayout.addWidget(label, row, column)
                gridLayout.addWidget(comboBox, row, column+1)

                # step up a row for the next entry
                row += 1

                # build a library of info relating to each combobox
                self.ComboBoxDataList.append( ComboBoxInformation( label, comboBox, anm.CharacterName, comboBoxCharacterList ) )
        else:
            label = QtGui.QLabel('No GS Skels present')
            gridLayout.addWidget(label, 2, 1)
            actorAnimLabel.setEnabled(False)
            sceneCharactersLabel.setEnabled(False)
            self.checkbox.setEnabled(False)
            label.setEnabled(False)

        if self.ComboBoxDataList:
            for obj in self.ComboBoxDataList:
                # assign character list to combo box
                obj.ComboBox.addItems( obj.CharacterList )
                # set index to match the character name match
                for idx in range(obj.ComboBox.count()):
                    if obj.PairedCharacter == obj.ComboBox.itemText(idx):
                        obj.ComboBox.setCurrentIndex(idx)
                        continue

        # set fonts
        actorAnimLabel.setFont(font)
        sceneCharactersLabel.setFont(font)

        # set/add layouts and widgets
        gridLayout.addWidget(self.checkbox, 1, 1)
        gridLayout.addWidget(actorAnimLabel, 0, 0)
        gridLayout.addWidget(sceneCharactersLabel, 0, 1)
        gridLayout.addWidget(spacerLabel, 2, 0)
        vBoxLayout.addWidget(scrollArea)
        widget.setLayout(gridLayout)
        scrollArea.setWidget(widget)
        self.setLayout(vBoxLayout)

        # checkbox callback connect
        self.checkbox.stateChanged.connect( self.checkBoxCallback )

        # combobox callback connect
        if self.ComboBoxDataList:
            for obj in self.ComboBoxDataList:
                obj.ComboBox.currentIndexChanged.connect(partial(self.comboBoxCallback, obj.ComboBox))

    def checkBoxCallback(self):
        for obj in self.ComboBoxDataList:
            obj.ComboBox.setEnabled(self.checkbox.isChecked())

    def comboBoxCallback(self, comboBox, idx):
        try:
            for obj in self.ComboBoxDataList:
                obj.ComboBox.blockSignals(True)
            # get label (anim name) for combobox edited
            anmText = None
            currentText = comboBox.currentText()
            if self.ComboBoxDataList:
                for obj in self.ComboBoxDataList:
                    if obj.ComboBox == comboBox:
                        # get label (anim name) for combobox edited
                        anmText = obj.Label.text()
                        # set new character match
                        obj.PairedCharacter = currentText
                        continue

            # update ActorAnimManager pairing newly selected character name with anim name
            if self.ActorAnimManager.ActorAnimList:
                for anm in self.ActorAnimManager.ActorAnimList:	
                    if str(anm.AnimName) == anmText:
                        anm.CharacterName = comboBox.currentText()
                        continue

            # updated reference property and comboboxes
            self.UpdatePropertyDataAndComboBox( comboBox )

        finally:
            for obj in self.ComboBoxDataList:
                obj.ComboBox.blockSignals(False)

    def UpdatePropertyDataAndComboBox(self, comboBox):
        if self.ActorAnimManager.ActorAnimList:
            for anm in self.ActorAnimManager.ActorAnimList:	
                # find reference property and update the character name data
                propertyFound = False
                for prop in self.ActorAnimManager.PropertyReferenceList:
                    if anm.AnimName == prop.AnimReferenceProperty.Name:
                        prop.AnimReferenceProperty.SetLocked( False )
                        prop.AnimReferenceProperty.Data = str(anm.CharacterName)
                        prop.AnimReferenceProperty.SetLocked( True )
                        propertyFound = True
                ## if property not found, create a new one
                if propertyFound == False:
                    anmProperty = self.ActorAnimManager.ReferenceScene.PropertyCreate( anm.AnimName, FBPropertyType.kFBPT_charptr, "", False, True, None )
                    anmProperty.Data = str(anm.CharacterName)
                    anmProperty.SetLocked( True )

                # create lists for comboboxes
                comboBoxCharacterList = ["None"]

                if anm.CharacterName != "None":
                    comboBoxCharacterList.append( anm.CharacterName )

                for unpairedChar in self.ActorAnimManager.GetUnpairedCharacterNames():
                    comboBoxCharacterList.append( unpairedChar )

                for obj in self.ComboBoxDataList:
                    if str(anm.AnimName) == obj.Label.text():
                        obj.CharacterList = comboBoxCharacterList
                        break

        for obj in self.ComboBoxDataList:
            # clear current combobox items out
            obj.ComboBox.clear()

            # set index to match the character name match
            obj.ComboBox.addItems(obj.CharacterList)

            for idx in range(obj.ComboBox.count()):
                if obj.PairedCharacter == obj.ComboBox.itemText(idx):
                    obj.ComboBox.setCurrentIndex(idx)

        # re-populate reference list
        self.ActorAnimManager.PopulateReferenceProperty()

        # display warning icon if any of the dropdowns are set to 'None' 
        # - emit signals, which are picked up in referenceWidget '_setupTabs'
        noneFound = False
        for obj in self.ComboBoxDataList:
            if obj.PairedCharacter == 'None':
                noneFound = True 
                continue

        if noneFound == True:
            self.displayWarningEvent.emit() 
        else:
            self.removeWarningEvent.emit() 

class HealthCheckWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(HealthCheckWidget, self).__init__(parent=parent)
        self.__checkboxDict = {}
        self.setupTab()

    def _handleHealthCheck(self):
        checkboxnameString = ''
        for checkbox, module in self._checkboxDict.iteritems():
            if checkbox.checkState() == QtCore.Qt.CheckState.Checked:
                module()
                checkboxnameString = checkboxnameString + str(checkbox.text()) + '\n'
                checkbox.setCheckState(QtCore.Qt.CheckState.Unchecked)
            else:
                None
        QtGui.QMessageBox.information(
                                        self,
                                        "R* Health Check",
                                        "Health Check completed.\n\n{0}".format(checkboxnameString),
                                        QtGui.QMessageBox.Ok
                                     )
        return None

    def setupTab(self):
        # layouts for tab
        vBoxLayout = QtGui.QVBoxLayout()
        gridLayout = QtGui.QGridLayout()

        # scroll area and content widget
        scrollArea = QtGui.QScrollArea(self)
        widget = QtGui.QWidget()

        # add button & checkbox widgets
        pushButton = QtGui.QPushButton('Run Health Check')
        resetEnvCheckbox = QtGui.QCheckBox( "Reset Environment Transforms" )
        resetFPScheckbox = QtGui.QCheckBox( "Reset FPS" )
        fixHeelsCheckbox = QtGui.QCheckBox( "Fix High Heels" )
        disableRotationDofCheckbox = QtGui.QCheckBox( "Disable Rotation DOF on Props" )
        deleteFoldersCheckbox = QtGui.QCheckBox( "Delete Empty Folders" )
        deleteCtrlRigsCheckbox = QtGui.QCheckBox( "Delete Unused Control Rigs" )
        deleteheadCamCheckbox = QtGui.QCheckBox( "Delete headCam & headLight" )
        deleteNamespacesCheckbox = QtGui.QCheckBox( "Delete Unused Namespaces" )
        deleteHIKSolversCheckbox = QtGui.QCheckBox( "Delete HIK Solvers" )
        deleteFaceCheckbox = QtGui.QCheckBox( "Delete Bad Facial Bones" )
        deleteExtCheckbox = QtGui.QCheckBox( "Delete Unused Character Extensions" )
        deleteLayersCheckbox = QtGui.QCheckBox( "Delete Empty Anim Layers" )
        deleteTexturesCheckbox = QtGui.QCheckBox( "Delete Floating Textures" )

        self._checkboxDict = {
                                resetEnvCheckbox:HealthCheck.rs_RotationScale_Check,
                                resetFPScheckbox:HealthCheck.rs_TimeMode_Check,
                                fixHeelsCheckbox:HealthCheck.rs_FixHighHeel_Check,
                                disableRotationDofCheckbox:HealthCheck.rs_DisableRotDOF_Check,
                                deleteFoldersCheckbox:HealthCheck.rs_DeleteFolders_Check,
                                deleteCtrlRigsCheckbox:HealthCheck.rs_DeleteUnusedControlRigs_Check,
                                deleteheadCamCheckbox:HealthCheck.rs_DeleteHeadCamLightObjects,
                                deleteNamespacesCheckbox:HealthCheck.rs_DeleteUnusedNamespace_Check,
                                deleteHIKSolversCheckbox:HealthCheck.rs_DeleteHIKSolver_Check,
                                deleteFaceCheckbox:HealthCheck.rs_DeleteFacialBone_Check,
                                deleteExtCheckbox:HealthCheck.rs_FixBadCharExt,
                                deleteLayersCheckbox:HealthCheck.rs_DeleteAnimLayer_Check,
                                deleteTexturesCheckbox:HealthCheck.rs_DeleteTextureVideoClip,
                            }

        # set widgets/layouts
        gridLayout.addWidget(pushButton)
        for checkbox in self._checkboxDict.iterkeys():
            gridLayout.addWidget(checkbox)
        vBoxLayout.addWidget(scrollArea)
        widget.setLayout(gridLayout)
        scrollArea.setWidget(widget)
        self.setLayout(vBoxLayout)

        # signals
        pushButton.clicked.connect(self._handleHealthCheck)