from PySide import QtCore

from RS import Globals
from RS.Tools.ReferenceEditor.model import assetModelItemTypes
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel

from RS.Tools.CameraToolBox.PyCore import pySideSingleton
from RS.Core.ReferenceSystem.Manager import Manager


class CategoriesModel(baseModel.BaseModel):
    manager = Manager()
    modelUpdatedSignal = QtCore.Signal()

    MERGE_LOAD = False
    NEW_SCENE = True

    __metaclass__ = pySideSingleton.PySideSingleton

    def __init__(self, internalModel=None, parent=None):
        self._categoriesDict = {}
        self._assetsDict = {}
        super(CategoriesModel, self).__init__(parent=parent)
        # These callbacks should persist even after the UI is closed to enable the UI to auto-refresh when changing
        # between scenes
        Globals.Callbacks.OnFileOpenCompleted.Add(self.onFileOpenCompleted)
        Globals.Callbacks.OnFileNewCompleted.Add(self.onFileOpenCompleted)
        Globals.Callbacks.OnFileMerge.Add(self.onFileMergeCallback)

    @classmethod
    def onFileMergeCallback(cls, source, event):
        """
        Sets the Merge Load property to False and New Scene property to True when a new file is opened
        """
        cls.MERGE_LOAD = True

    @classmethod
    def onFileOpenCompleted(cls, source, event):
        """ Sets the Merge Load property to True when file merge starts """
        if cls.MERGE_LOAD:
            cls.MERGE_LOAD = False
        else:
            cls.NEW_SCENE = True
            cls().updateAll(forceReload=True)

    def getHeadings(self):
        """ headings for the asset tree view """
        return ["Name", "P4 Revision", "", "", "", "Local Asset Path"]

    def columnCount(self, parent):
        """ The number of columns the model has """
        return len(self.getHeadings())

    def _getCurrentSceneItems(self, forceReloadBool=False, categoryFilter=None):
        """
        returns a dictionary of references and their properties

        Arguments:
            forceReloadBool (bool): clears out the cached references
        """
        self.layoutAboutToBeChanged.emit()
        for reference in self.manager.GetReferenceListAll(forceReload=forceReloadBool):

            categoryType = reference.FormattedTypeName
            if not reference.IsDestroyed() and reference.ModelNull and reference.Path and reference.Namespace and categoryType:

                if categoryFilter is not None and categoryType.lower() not in categoryFilter:
                    continue

                asset = self._assetsDict.get(reference.ModelNull, None)
                if asset is None:
                    asset = assetModelItemTypes.AssetModelItem(reference)
                    self._assetsDict[reference.ModelNull] = asset
                yield asset

    def setupModelData(self, parent):
        self._categoriesDict = {}
        self.categoryList = []

        # go through the hierarchy list (list of all top categories, excluding sub categories)
        for referenceType in self.manager.GetReferenceHierarchyList():
            categoryNode = assetModelItemTypes.AssetTextModelItem((referenceType,), parent=parent)
            self._categoriesDict[referenceType.FormattedTypeName.lower()] = categoryNode
            parent.appendChild(categoryNode)

            # check if a top category have any sub categories
            for cat in referenceType.SubCats or []:
                subCategoryNode = assetModelItemTypes.AssetTextModelItem((cat,), parent=categoryNode)
                self._categoriesDict[cat.FormattedTypeName.lower()] = subCategoryNode
                categoryNode.appendChild(subCategoryNode)

            # go through the scene assets and add them to the appropriate categories

        for item in self._getCurrentSceneItems():
            reference = item.GetReference()
            item.Update(checkPerforce=True)
            self._categoriesDict[reference.FormattedTypeName.lower()].appendChild(item)

    def _getChildNodeFromReferenceNull(self, category, referenceNull):
        for child in category:
            if child.GetReferenceNull() == referenceNull:
                return child
        raise ValueError("Unable to find reference null!!")

    def UpdateGroup(self, category):
        self.manager.UpdateReferences(self._getReferencesFromCategory(category))

    def StripGroup(self, category):
        self.manager.StripReferences(self._getReferencesFromCategory(category))

    def DeleteGroup(self, category):
        self.manager.DeleteReferences(self._getReferencesFromCategory(category))

    def _getCategoryNode(self, category):
        # check that category exists in dict
        categoryNode = self._categoriesDict.get(category.lower(), None)
        if categoryNode is None:
            raise ValueError("Category '{0}' doesn't exist".format(category))

        return categoryNode

    def _getReferencesFromCategory(self, category):
        """
        Gets all the reference nulls from a category

        Arguments:
            category (string): category name

        Return:
            list
        """
        categoryNode = self._getCategoryNode(category.lower())
        if categoryNode.hasSubCategories:
            return []
        return [item.GetReference() for item in categoryNode]

    def Update(self, modelIndex, item, referenceToPush=None):
        """
        Update the contents of the item

        Arguments:
            modelIndex (QtCore.QModelIndex): model index associated with the item to update
            item  (RS.Tools.ReferenceSystem.model.AssetModelItemTypes.AssetModelItem): item to update
            referenceToPush (ReferenceEditor.model): specific reference to push for the update (used for character swaps where the reference changes)
        """
        if item.HasChanged():
            item.Update(reference=referenceToPush)

    def UpdateCategory(self, category, checkPerforce=False):
        """
        Checking for any removed and newly added scene references.
        Compares UI Nodes with Scene References, and vice versa, to check for any changes.
        UI is updated accordingly.

        Arguments:
            category (string): name of the category to use ("FormattedTypeName" of a reference)
            checkPerforce (bool): run P4 commands to get the latest information on the referenced assets
        """
        # check that category exists in dict
        # get category node (dict value)

        categoryNode = self._getCategoryNode(category.lower())
        # check if the child is an asset, rather than a category. We don't need to edit category text items
        if categoryNode.hasSubCategories:
            return

        delete = []
        for child in categoryNode:
            reference = child.GetReference()
            # check if reference has been removed already, or the formatted name does not match the
            # category, we want to delete the child in these 2 cases
            if reference.IsDestroyed() or reference.FormattedTypeName.lower() != category.lower():
                delete.append(child)
                continue

            # check for any node property updates
            filestate = None
            if checkPerforce:
                filestate = reference.GetFileState()
                reference.UpdateReferenceP4Version(filestate)

            if child.HasChanged(checkPerforce=checkPerforce):
                child.Update(checkPerforce=checkPerforce, filestate=filestate)
                idx = categoryNode.findIndexByChild(child)
                self.dataChanged.emit(categoryNode.createIndex(idx, 0),
                                      categoryNode.createIndex(idx, len(self.getHeadings())-1))

        # Check for newly added items to the scene of the current category
        add = [item for item in self._getCurrentSceneItems(categoryFilter=(category.lower(),))
               if item not in categoryNode.childItems]

        # check if there is any change for adding and/or removing nodes
        if not add and not delete:
            return

        # we need to find the category node by matching the name
        catNodeIndexes = self.match(self.createIndex(0, 0),
                                    0, categoryNode.data(self.createIndex(0, 0), 0),
                                    1, QtCore.Qt.MatchExactly | QtCore.Qt.MatchRecursive)

        if len(catNodeIndexes) == 0:
            return

        # category node has been found
        catNodeIndex = catNodeIndexes[0]

        # remove before you add
        # remove node
        if delete:
            self.beginRemoveRows(catNodeIndex, 0, len(delete))
            for item in delete:
                categoryNode.removeChild(item)
            self.endRemoveRows()

        # add node
        if add:
            self.beginInsertRows(catNodeIndex, 0, len(add))
            for item in add:
                item.Update(checkPerforce=True)
                categoryNode.appendChild(item)
            self.endInsertRows()

        self.modelUpdatedSignal.emit()

    def updateAll(self, forceReload=False, checkPerforce=False):
        '''
        Updates all items in categories with latest information
        '''
        if forceReload:
            # getCurrentSceneItems is a generator, so it needs to be converted to list for it go through all the
            # new items.
            list(self._getCurrentSceneItems(True))
        for catName in self._categoriesDict.keys():
            self.UpdateCategory(catName, checkPerforce=forceReload or checkPerforce)
