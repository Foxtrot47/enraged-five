import functools
import re

import pyfbsdk as mobu

from PySide import QtGui, QtCore

from RS import Globals
from RS.Tools import UI
from RS.Utils import Namespace
from RS.Utils.Scene import Component
from RS.Core.ReferenceSystem.Manager import Manager


class NamespaceCorrectionDialog(QtGui.QDialog):
    '''
    Dialogue to run NamespaceCorrectionWidget and retain size settings for opening and closing of the widget
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(NamespaceCorrectionDialog, self).__init__(parent=parent)
        self._setupUi()

    def _correctNamespace(self):
        """
        correct the namespace based on the user's input
        """
        # go through dict and see which namespaces need to be changed, and to what value
        for key, value in  self.namespaceCorrectionWidget.namespaceDict.iteritems():
            lineedit = key
            combobox = value[0]
            objectList = value[1]
            deleteButton = value[2]

            # get namespaces
            refNamespace = str(combobox.currentText())
            brokenNamespace = str(lineedit.text())

            # check if combobox option is not the 'no change' option, skip this iteration if it is
            if refNamespace == self.namespaceCorrectionWidget.defaultNoChangeString:
                continue

            # change namespaces
            for object in objectList:
                object.LongName = '{0}:{1}'.format(refNamespace, object.Name)

            # remove widgets
            lineedit.close()
            combobox.close()
            deleteButton.close()

        # refresh GetBrokenNamespaceDict
        # close ui if there are no remaining broken namespaces
        if not self.namespaceCorrectionWidget.GetBrokenNamespaceDict():
            self.close()
            return self.result()

    def cancelButton(self):
        """
        close the ui
        """
        self.close()

    def _setupUi(self):
        # create layout and add widget
        self.setWindowTitle("Namespace Correction Tool")

        myLayout = QtGui.QGridLayout()
        self.namespaceCorrectionWidget = NamespaceCorrectionWidget()

        banner = UI.BannerWidget(name='Namespace Correction')
        cancelButton = QtGui.QPushButton("Cancel")
        correctNamespaceButton = QtGui.QPushButton("Correct Namespaces")


        myLayout.addWidget(banner, 0, 0, 1, 2)
        myLayout.addWidget(self.namespaceCorrectionWidget, 1, 0, 1, 2)
        myLayout.addWidget(correctNamespaceButton, 2, 0)
        myLayout.addWidget(cancelButton, 2, 1)

        self.setLayout(myLayout)

        myLayout.setSpacing(0)
        myLayout.setContentsMargins(0, 0, 0, 0)

        correctNamespaceButton.pressed.connect(self._correctNamespace)
        cancelButton.pressed.connect(self.cancelButton)


class NamespaceCorrectionWidget(QtGui.QWidget):
    '''
    Class to generate the widgets, and their functions, for the Toolbox UI
    '''
    def __init__(self, parent=None):
        '''
        Constructor
        '''
        super(NamespaceCorrectionWidget, self).__init__(parent=parent)
        self._manager = Manager()
        self.namespaceDict = {}
        self.defaultNoChangeString = '[No Change]'
        self._setupUi()

    @classmethod
    def GetBrokenNamespaceDict(cls):
        """
        find namespaces with a space and number e.g. "Player_Zero 1"

        Returns:
        Dict: [string] = list(FBComponent)
        """
        brokenNamespaceDict = {}
        approvedTypesList = (mobu.FBModel, mobu.FBConstraint, mobu.FBFolder, mobu.FBMaterial,
                             mobu.FBTexture, mobu.FBHandle, mobu.FBVideoClipImage, mobu.FBShader,
                             mobu.FBCharacter, mobu.FBCharacterExtension, mobu.FBGroup, mobu.FBSet,
                             mobu.FBCharacterPose)

        for namespace in Globals.Namespaces:
            name = namespace.LongName
            if re.findall(r"([^'Camera']\s\d+$)", name, re.IGNORECASE) or name == '__temp__':
                for content in Namespace.GetContents(namespace):
                    if isinstance(content, approvedTypesList):
                        brokenNamespaceDict.setdefault(name, []).append([content])
        return brokenNamespaceDict

    def GetAvailableReferenceNamespaceList(self):
        """
        gather namespaces from the scene

        Returns:
        list (string)
        """
        referenceNamespaceList = [self.defaultNoChangeString]
        for reference in self._manager.GetReferenceListAll():
            referenceNamespaceList.append(reference.Namespace)
        return referenceNamespaceList

    def populateNamespaceCombobox(self, combobox, refNameList):
        """
        populate the qcomboboxes

        Args:
        combobox: QComboBox
        refNameList: List (String) of ref editor names
        """
        combobox.clear()
        combobox.addItems(refNameList)

    def _deleteObjects(self, namespace):
        """
        deletes objects which carry the broken namespace
        """
        result = QtGui.QMessageBox.question(self,
                                            'Delete Broken Namespace Objects',
                                            'Are you sure you want to proceed with deleting?',
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

        if result == QtGui.QMessageBox.Yes:
            # get widgets, so we can remove them later
            combobox = None
            lineedit = None
            objectList = None
            deleteButton = None
            for key, value in self.namespaceDict.iteritems():
                if namespace == str(key.text()):
                    lineedit = key
                    combobox = value[0]
                    objectList = value[1]
                    deleteButton = value[2]
                    break

            # delete items with the broken namespace
            if objectList is not None:
                Component.Delete(objectList)

                # remove widgets
                if all([combobox, lineedit, deleteButton]):
                    lineedit.close()
                    combobox.close()
                    deleteButton.close()

    def _setupUi(self):
        '''
        Sets up the widgets, their properties and launches call events
        '''
        # create layouts
        mainLayout = QtGui.QGridLayout()
        scrollLayout = QtGui.QGridLayout()
        scrollGridLayout = QtGui.QGridLayout()

        # create main widgets
        brokenNameLabel = QtGui.QLabel("Broken Namespace(s):")
        refNameLabel = QtGui.QLabel("Available Namespace(s):")
        deleteLabel = QtGui.QLabel("Delete Objects")
        self.scrollArea = QtGui.QScrollArea()

        # properties for main widgets
        brokenNameLabel.setToolTip("namespace as it appears in the scene")
        refNameLabel.setToolTip("namespace available to correct to")
        self.scrollArea.setGeometry(QtCore.QRect(0, 90, 600, 161))
        self.scrollArea.setWidgetResizable(False)
        self.scrollAreaWidget = QtGui.QWidget()
        self.scrollArea.setWidget(self.scrollAreaWidget)

        # widgets based off broken namespaces found
        brokenNamespaceDict = self.GetBrokenNamespaceDict()
        refNameList = self.GetAvailableReferenceNamespaceList()

        counter = 1
        for key, value in brokenNamespaceDict.iteritems():
            # change the list of lists into one list
            objectList = []
            for item in value:
                objectList.extend(item)

            tooltipString = ""
            for item in objectList:
                tooltipString = tooltipString + "[{1}] {0}\n".format(item.LongName, item.ClassName())

            # create qlineedit
            brokenNameLineEdit = QtGui.QLineEdit()

            # set properties for qlineedit
            brokenNameLineEdit.setText(key)
            brokenNameLineEdit.setToolTip(tooltipString)
            brokenNameLineEdit.setAlignment(QtCore.Qt.AlignCenter)
            brokenNameLineEdit.setReadOnly(True)
            font = QtGui.QFont('', 0)
            fontMetrics = QtGui.QFontMetrics(font)
            brokenNameLineEdit.setFixedSize(fontMetrics.width(key), fontMetrics.height())

            # create qcombobox
            refNameCombobox = QtGui.QComboBox()

            # populate qcomboBox
            self.populateNamespaceCombobox(refNameCombobox, refNameList)

            # create delete button
            deleteButton = QtGui.QPushButton("Delete")

            # delete button properties
            deleteButton.setToolTip(tooltipString)
            deleteButton.pressed.connect(functools.partial(self._deleteObjects, key))

            # add qWidgets to dict
            self.namespaceDict[brokenNameLineEdit] = refNameCombobox, objectList, deleteButton

            scrollGridLayout.addWidget(brokenNameLineEdit, counter, 0, QtCore.Qt.AlignHCenter)
            scrollGridLayout.addWidget(refNameCombobox, counter, 1, QtCore.Qt.AlignHCenter)
            scrollGridLayout.addWidget(deleteButton, counter, 2, QtCore.Qt.AlignHCenter)

            counter += 1

        # set widgets to layout
        scrollGridLayout
        mainLayout.addWidget(brokenNameLabel, 1, 0, QtCore.Qt.AlignHCenter)
        mainLayout.addWidget(refNameLabel, 1, 1, QtCore.Qt.AlignHCenter)
        mainLayout.addWidget(deleteLabel, 1, 2, QtCore.Qt.AlignHCenter)
        scrollLayout.addLayout(scrollGridLayout, 0, 0)
        mainLayout.addLayout(scrollLayout, 2, 0, 1, 3)

        # set layout
        self.setLayout(mainLayout)