from PySide import QtCore, QtGui

import os

from RS import Config, Perforce
from RS.Tools.ReferenceEditor import const
from RS.Tools.ReferenceEditor.model import directoryTextModelItem
from RS.Tools.CameraToolBox.PyCoreQt.Models import baseModel


class FilesModel(baseModel.BaseModel):
    def __init__(self, parent=None):
        super(FilesModel, self).__init__(parent=parent)

    def _getDirectory(self, folderString):
        '''
        arg: String - a string with the name of a toplevel folder in the resources folder
        Get list of resource files from P4 depot view
        '''
        fileList = []
        cmd = "files"
        resourcePath = os.path.join(Config.Project.Path.Root,
                                    "art",
                                    "animation",
                                    "resources",
                                    folderString,
                                    "...fbx")
        args = ["-e", resourcePath]
        p4Files = Perforce.Run(cmd, args)
        if str('has been unloaded') in p4Files:
            None
        else:
            for record in p4Files.Records:
                pathPartsList = []
                path = record['depotFile']
                try:
                    fileList.append(str(path))
                except UnicodeEncodeError:
                    continue
        return fileList

    def findIndexByChild(self, item):
        return self.childItems.index(item)

    def getHeadings(self):
        return ['Asset List']

    def _createNodesFromFilePath(self, filePath, category, parentNode):
        filePathSplit = filePath.lower().split('/{0}'.format(category))
        categoryPath = filePathSplit[-1]
        categoryPathSplit = categoryPath.split('/')
        categoryPathList = categoryPathSplit[1:]
        assetNameSplit = categoryPathList[-1].split('.')
        assetName = assetNameSplit[0]

        currentNode = parentNode
        for part in categoryPathList:
            if '.' in part.lower():
                partSplit = part.split('.')
                part = partSplit[0]

            childNode = currentNode.getChildByName(part)
            
            if childNode is None:
                if part == assetName:
                    childNode = directoryTextModelItem.DirectoryTextModelItem(part,
                                                                              filePath,
                                                                              parent=currentNode)
                    currentNode.appendChild(childNode)
                else:
                    childNode = directoryTextModelItem.DirectoryTextModelItem(part,
                                                                              folderBool=True,
                                                                              parent=currentNode)
                    currentNode.appendChild(childNode)
            currentNode = childNode

    def getNodesByName(self, name):
        return self.rootItem.children()[0].getAncestryByPartialName(name)


class AssetModel(FilesModel):
    def __init__(self, internalModel=None, parent=None):
        super(AssetModel, self).__init__(parent=parent)

    def setupModelData(self, parent):
        parents = [parent]

        # characters
        parentNode = directoryTextModelItem.DirectoryTextModelItem(const.DirectoryFolderString.Characters,
                                                                   folderBool=True,
                                                                   parent=parents[-1])
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Characters)
        from RS.Tools import UI
        if parentNode:
            for path in pathList:
                pathSplit = path.split('/')
                if 'Models' in pathSplit:
                    self._createNodesFromFilePath(path,
                                                  const.DirectoryFolderString.Characters,
                                                  parentNode)

        # props
        parentNode = directoryTextModelItem.DirectoryTextModelItem(const.DirectoryFolderString.Props,
                                                                   folderBool=True,
                                                                   parent=parents[-1])
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Props)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Props,
                                              parentNode)

        # sets
        parentNode = directoryTextModelItem.DirectoryTextModelItem(const.DirectoryFolderString.Sets,
                                                                   folderBool=True,
                                                                   parent=parents[-1])
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Sets)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Sets,
                                              parentNode)

        # vehicles
        parentNode = directoryTextModelItem.DirectoryTextModelItem(const.DirectoryFolderString.Vehicles,
                                                                   folderBool=True,
                                                                   parent=parents[-1])
        parents[-1].appendChild(parentNode)
        pathList = self._getDirectory(const.DirectoryFolderString.Vehicles)
        if parentNode:
            for path in pathList:
                self._createNodesFromFilePath(path, const.DirectoryFolderString.Vehicles,
                                              parentNode)


class AssetFilterProxyModel(QtGui.QSortFilterProxyModel):
    """
    Proxy Filter Model to hide the system cameras and the non-switcher cameras
    """
    def __init__(self, parent=None):
        super(AssetFilterProxyModel, self).__init__(parent=parent)
        self._filterText = ''

    def setFilterText(self, text):
        self._filterText = text
        self._test = self.sourceModel().getNodesByName(self._filterText)
        self.invalidate()

    def getFilterText(self):
        return self._filterText

    def filterAcceptsRow(self, row, parent=None):
        """
        Interal Method

        Main logic if a row is shown or hidden based off the properties in 
        the AssetFilterProxyModel.
        """
        if self.sourceModel is None:
            return False

        if self._filterText == '':
            return True

        index = self.sourceModel().index(row, 0, parent)
        return self.sourceModel().data(index, QtCore.Qt.UserRole) in self._test
