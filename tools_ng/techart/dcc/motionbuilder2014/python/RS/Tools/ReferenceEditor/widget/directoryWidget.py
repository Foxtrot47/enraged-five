from PySide import QtGui, QtCore

import pyfbsdk as mobu

from RS import Globals, Perforce
from RS.Utils import Scene
from RS.Tools import UI
from RS.Core.ReferenceSystem.Manager import Manager
from RS.Core.ReferenceSystem.Types import Base
from RS.Tools.ReferenceEditor import const
from RS.Tools.ReferenceEditor.model import directoryModelTypes, selectedDirectoryModelTypes
from RS.Tools.CameraToolBox.PyCoreQt.Delegates import comboboxDelegate, spinboxDelegate


class DirectoryDialog(QtGui.QDialog):
    # signal to notify when the createReferences button has ran
    createMultiRefSignal = QtCore.Signal()
    cancelMultiRefSignal = QtCore.Signal()
    def __init__(self, parent=None):
        super(DirectoryDialog, self).__init__(parent=parent)
        self._settings = QtCore.QSettings( "RockstarSettings", "DirectoryDialog" )
        directoryWidget = DirectoryWidget()
        # create layout and add widget
        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(directoryWidget)
        self.setLayout(mainLayout)
        mainLayout.setContentsMargins(0, 0, 0, 0)
        # set modal - so the only active window is this one, until it is closed
        self.setModal(True)
        # connect signal so the window closes after the update button has run
        directoryWidget.createMultiRefSignal.connect(self._createMultiReference)
        directoryWidget.cancelMultiRefSignal.connect(self._handleHideDialog)

    def _createMultiReference(self):
        # emit signal to referenceWidget
        self.createMultiRefSignal.emit()
        self._handleHideDialog()

    def _handleHideDialog(self):
        # hide dialogue
        self.hide()

    def showEvent(self, event):
        try:
            super(DirectoryDialog, self).showEvent( event )
            self.resize(self._settings.value("size", QtCore.QSize(400, 150)))
            self.move(self._settings.value("pos", QtCore.QPoint(200, 200)))
        except:
            pass

    def closeEvent(self, event):
        try:
            super(DirectoryDialog, self).closeEvent( event )
            self._settings.setValue("size", self.size())
            self._settings.setValue("pos", self.pos())
        except:
            pass

class DirectoryTreeWidget(QtGui.QTreeView):
    '''
    We create our TreeView here and generate actions
    '''
    def __init__(self, model=None, parent=None):
        super(DirectoryTreeWidget, self).__init__(parent=parent)
        model = model or directoryModelTypes.AssetModel()
        self.setModel(model)


class selectedPathTreeView(QtGui.QTreeView):
    def __init__(self, model=None, parent=None):
        super(selectedPathTreeView, self).__init__(parent=parent)
        self.setModel(model)
        # spinbox delegate
        self._deletegate = spinboxDelegate.SpinBoxDelegate()
        self.setItemDelegateForColumn(1, self._deletegate)
        # proxy combobox delegate
        self._comboDeletegate = comboboxDelegate.ComboBoxDelegate(['True', 'False'])
        self.setItemDelegateForColumn(2, self._comboDeletegate)


class DirectoryWidget(QtGui.QWidget):
    # signal to notify when the createReferences button has ran
    createMultiRefSignal = QtCore.Signal()
    cancelMultiRefSignal = QtCore.Signal()
    def __init__(self, parent=None):
        super(DirectoryWidget, self).__init__(parent=parent)
        self._settings = QtCore.QSettings("RockstarSettings", "DirectoryDialog")
        self.setupUi()
        self.manager = Manager()

    def setupUi(self):
        # set models
        self._directoryModel = directoryModelTypes.AssetModel()
        self._selectedModel = selectedDirectoryModelTypes.SelectedModelTypes()
        self._proxyModel = directoryModelTypes.AssetFilterProxyModel()

        # add layouts
        mainLayout = QtGui.QVBoxLayout()
        widget = QtGui.QWidget()
        gridLayout = QtGui.QGridLayout()
        filterLayout = QtGui.QGridLayout()
        addButtonLayout = QtGui.QVBoxLayout()

        # add widgets
        filterButton = QtGui.QPushButton('Filter')
        spacerLabel = QtGui.QLabel('')
        self._lineEditWidget = QtGui.QLineEdit()
        self.directoryTreeView = DirectoryTreeWidget(model=self._proxyModel)
        self.selectedPathTreeView = selectedPathTreeView(model=self._selectedModel)
        addButton = QtGui.QPushButton('Add Asset(s)')
        removeButton = QtGui.QPushButton('Remove Asset(s)')
        createButton = QtGui.QPushButton('Create References')
        cancelButton = QtGui.QPushButton('Cancel')

        # widget properties
        spacerLabel.setFixedHeight(10)
        addButton.setFixedWidth(100)
        removeButton.setFixedWidth(100)

        # directoryTreeView properties
        self.directoryTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.directoryTreeView.setAlternatingRowColors(True)
        self.directoryTreeView.resizeColumnToContents(0)

        # selectedPathTreeView properties
        self.selectedPathTreeView.header().swapSections(0, 2)
        self.selectedPathTreeView.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.selectedPathTreeView.setAlternatingRowColors(True)
        self.selectedPathTreeView.header().resizeSection(2, 50)
        self.selectedPathTreeView.header().resizeSection(1, 40)

        # set widgets to be flush to the edges of the main window
        mainLayout.setContentsMargins(0, 0, 0, 0)

        # add widgets to filter layout
        filterLayout.addWidget(self._lineEditWidget, 0, 0)
        filterLayout.addWidget(filterButton, 0, 1)
        # add widget to vboxlayout
        addButtonLayout.addWidget(addButton)
        addButtonLayout.setAlignment(addButton, QtCore.Qt.AlignRight)
        # add widgets to mainlayout
        gridLayout.addLayout(filterLayout, 0, 0, 1, 2)
        gridLayout.addLayout(addButtonLayout, 2, 0)
        gridLayout.addWidget(self.directoryTreeView, 1, 0)
        gridLayout.addWidget(removeButton, 2, 1)
        gridLayout.addWidget(self.selectedPathTreeView, 1, 1)
        gridLayout.addWidget(spacerLabel, 3, 1)
        gridLayout.addWidget(createButton, 4, 1)
        gridLayout.addWidget(cancelButton, 5, 1)

        # set layout
        mainLayout.addWidget(widget)
        widget.setLayout(gridLayout)
        self.setLayout(mainLayout)

        # set proxy source model
        self._proxyModel.setSourceModel(self._directoryModel)

        # signals
        addButton.pressed.connect(self._handleAddButtonPress)
        removeButton.pressed.connect(self._handleRemoveButtonPress)
        createButton.clicked.connect(self._handleCreateReferences)
        self._lineEditWidget.editingFinished.connect(self._handleFilterSearch)
        filterButton.pressed.connect(self._handleFilterSearch)
        self._lineEditWidget.editingFinished.connect(self._handleFilterSearch)
        cancelButton.pressed.connect(self._handleCancel)

    def _getNodesDict(self, indexes):
        selectedNodesDict = {}
        if len(indexes) > 0:
            for idx in indexes:
                # 0 = string: name of asset, that is in the tree row
                name = idx.data(0)
                # 3 = string: path to the asset, that is in the tree row
                path = idx.data(3)
                isFile = idx.data(const.DirectoryModelDataIndex.folderFileRole)
                selectedNodesDict[name] = path, isFile, idx
            return selectedNodesDict
        return

    def ProxySetup(self, reference):
        reference.IsProxy = True

    def _handleAddButtonPress(self):
        # get list of strings to add
        selectedIdxs = {idx.row():idx for idx in self.directoryTreeView.selectedIndexes() if idx.column() == 0}.values()
        directorySelectedNodesDict = self._getNodesDict(selectedIdxs)
        # get list of string already existing in _selectedModel
        selectedModelItemList = []
        for child in self._selectedModel.rootItem.childItems:
            selectedModelItemList.append(str(child.getName()))
        # check if names already exist in both lists
        if not directorySelectedNodesDict:
            return
        keysToRemoveList = []
        for key in directorySelectedNodesDict.iterkeys():
            if str(key) in selectedModelItemList:
                keysToRemoveList.append(key)

        for key in keysToRemoveList:
            del directorySelectedNodesDict[key]

        # pass string list to add function
        self._selectedModel.AddSelectedPathTree(directorySelectedNodesDict)
        # clear selection
        self.directoryTreeView.clearSelection()

    def _handleRemoveButtonPress(self):
        # get list of strings to add
        selectedNodeList = self.selectedPathTreeView.selectedIndexes()
        # pass string list to add function
        self._selectedModel.RemoveSelectedPathTree(selectedNodeList)
        self.selectedPathTreeView.clearSelection()

    def _handleCreateReferences(self):
        # get the paths and number of each asset required and add them to a list
        batchCreateList = []
        proxyDict = {}
        for child in self._selectedModel.rootItem.childItems:
            path = child.getPath()
            number = int(child.getReferenceNumber())
            proxyBool = child.getProxyBool()
            name = str(child.getName())
            proxyDict[name] = proxyBool
            # paths are added to a list, they are added as many times as the user defined they needed
            for idx in range(number):
                Perforce.Sync(str(path))
                fileState = Perforce.GetFileState(str(path))
                if isinstance(fileState, list):
                    continue
                localPath = fileState.ClientFilename
                batchCreateList.append(str(localPath))

        # create References
        referenceList = self.manager.CreateReferences(batchCreateList)

        # apply proxy materials to any proxy objects
        if referenceList:
            for reference in referenceList:
                reference.Selected = True
                for key, value in proxyDict.iteritems():
                    if key.lower() in reference.Namespace.lower() and value == 'True':
                        self.ProxySetup(reference)
                        break

        # clear widget
        self._selectedModel.reset()

        # send a signal so the dialog closes
        self.createMultiRefSignal.emit()

    def _handleCancel(self):
        self.cancelMultiRefSignal.emit()

    def _handleFilterSearch(self):
        filterText = self._lineEditWidget.text()
        self._proxyModel.setFilterText(filterText)
        if filterText != "":
            self.directoryTreeView.expandAll()
        else:
            self.directoryTreeView.collapseAll()

    def show(self):
        main = UI.QtMainWindowBase()
        main.setCentralWidget(self)
        main.show()