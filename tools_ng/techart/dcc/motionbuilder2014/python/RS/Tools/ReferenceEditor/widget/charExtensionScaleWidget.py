from PySide import QtGui, QtCore

from RS.Tools import UI
from RS.Utils import Scene


class CharExtensionWidgetDialog(UI.QtMainWindowBase):
    """
    Dialogue to run CharExtensionWidget and retain size settings for opening and closing of the widget
    """
    closeToolSignal = QtCore.Signal()
    def __init__(self, reference, parent=None):
        """
        Constructor
        """
        self._settings = QtCore.QSettings( "RockstarSettings", "CharExtensionWidgetDialog" )
        super(CharExtensionWidgetDialog, self).__init__(parent=parent, title="R* Expression Scale Fix", dockable=True)
        self._dock.closeEvent = self.closeEvent
        self._dock.resize(self._settings.value("size", QtCore.QSize(400, 500)))
        charExtensionWidget = CharExtensionWidget(reference)
        charExtensionWidget.closeToolSignal.connect(self._handleCloseDialog)

        # create layout and add widget
        self.setCentralWidget(charExtensionWidget)

    def showEvent(self, event):
        self._dock.restoreGeometry(self._settings.value("geometry"))

    def closeEvent(self, event):
        self._settings.setValue("geometry", self._dock.saveGeometry())
        self._settings.setValue("size", self._dock.size())
        return super(CharExtensionWidgetDialog, self).closeEvent(event)

    def _handleCloseDialog(self):
        # hide dialogue
        self.close()


class CharExtensionWidget(QtGui.QWidget):
    """
    Class to generate the widgets, and their functions, for the Toolbox UI
    """
    closeToolSignal = QtCore.Signal()
    def __init__(self, reference, parent=None):
        """
        Constructor
        """
        super(CharExtensionWidget, self).__init__(parent=parent)
        self._reference = reference
        self.setupUi()

    def getBoneList(self):
        boneList = []
        boneList = self._reference.GetProblemCharacterExtensions()
        return boneList

    def fixExtensionScales(self):
        boneList = self.getBoneList()
        self._reference.FixScaleOnCharacterExtensions(boneList)
        self.closeToolSignal.emit()

    def populateTreeWidget(self):
        self.treeWidget.clear()
        boneList = []
        boneList = self.getBoneList()
        for  bone in boneList:
            treeWidgetitem = QtGui.QTreeWidgetItem(self.treeWidget)
            treeWidgetitem.setText(0, bone.LongName)

    def cancelScaleFix(self):
        self.closeToolSignal.emit()

    def setupUi(self):
        """
        Sets up the widgets, their properties and launches call events
        """
        # deselect all
        Scene.DeSelectAll()

        # create layouts
        mainLayout = QtGui.QGridLayout()

        # widgets
        self.label = QtGui.QLabel("The below list of bones, that are a part of the Character Extensions,\ndo not have a scale value of (1,1,1).\n\nPlease decide whether you'd like to proceed with fixing the scale\nvalues for these bones, or if you would like to Cancel.")
        self.spacerLabel = QtGui.QLabel('')
        self.treeWidget = QtGui.QTreeWidget()
        resetScaleButton = QtGui.QPushButton('Reset Scale Values')
        cancelButton = QtGui.QPushButton('Cancel')

        # widget properties
        resetScaleButton.setToolTip("removes keys on the bone's scale property\nand sets the scale value back to (1,1,1)")
        self.treeWidget.header().setVisible(False)
        self.treeWidget.setColumnCount(1)
        self.treeWidget.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.treeWidget.setAlternatingRowColors(True)
        self.treeWidget.setSortingEnabled(True)
        self.populateTreeWidget()

        # add widgets to layout
        mainLayout.addWidget(self.label, 0, 0, 1, 2)
        mainLayout.addWidget(self.spacerLabel, 1, 0, 1, 2)
        mainLayout.addWidget(self.treeWidget, 2, 0, 1, 2)
        mainLayout.addWidget(resetScaleButton, 3, 0)
        mainLayout.addWidget(cancelButton, 3, 1)

        # set layout
        self.setLayout(mainLayout)

        # set connections
        resetScaleButton.pressed.connect(self.fixExtensionScales)
        cancelButton.pressed.connect(self.cancelScaleFix)