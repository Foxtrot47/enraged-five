from PySide import QtCore, QtGui

import os

from RS import Config
from RS.Tools.VirtualProduction import const as vpConst


class AssetModelIndex(object):
    ReferenceNull = 0
    Path = 1
    Namespace = 2
    FormattedTypeName = 3
    HaveRevision = 4
    HeadRevision = 5
    HaveandHeadRevision = 6
    PerforceUserHaveRevision = 7
    ProxyAsset = 8


class AssetModelDataIndex(object):
    DataDict = QtCore.Qt.UserRole
    IconCommand = QtCore.Qt.UserRole + 1
    CategoryString = QtCore.Qt.UserRole + 2
    ItemRole = QtCore.Qt.UserRole + 3
    NamespaceRole = QtCore.Qt.UserRole + 4


class DirectoryModelDataIndex(object):
    selfReturn = QtCore.Qt.UserRole
    folderFileRole = QtCore.Qt.UserRole + 1
    spinBoxRole = QtCore.Qt.UserRole + 2


class IconCommandType(object):
    Refresh = 0
    Edit = 1
    Delete = 2


class Icons(object):
    CreateReference = QtGui.QIcon()
    CreateMultipleReferences = QtGui.QIcon()
    UpdateReference = QtGui.QIcon()
    RefreshTextures = QtGui.QIcon()
    DeleteTextures = QtGui.QIcon()
    DeleteReference = QtGui.QIcon()
    InSync = QtGui.QIcon()
    OutSync = QtGui.QIcon()
    NoSync = QtGui.QIcon()
    SaveReference = QtGui.QIcon()
    PairingWarning = QtGui.QIcon()
    IsProxy = QtGui.QIcon()
    Folder = QtGui.QIcon()
    File = QtGui.QIcon()
    Refresh = QtGui.QIcon()
    CleanScene = QtGui.QIcon()
    UnCleanScene = QtGui.QIcon()

    iconFolderString = "{0}/ReferenceEditor/".format(Config.Script.Path.ToolImages)

    CreateReference.addPixmap(QtGui.QPixmap("{0}CreateReference.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    CreateMultipleReferences.addPixmap(QtGui.QPixmap("{0}CreateMultipleReferences.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    UpdateReference.addPixmap(QtGui.QPixmap("{0}UpdateReference.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    RefreshTextures.addPixmap(QtGui.QPixmap("{0}UpdateSceneTexture.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DeleteTextures.addPixmap(QtGui.QPixmap("{0}NoSync.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    DeleteReference.addPixmap(QtGui.QPixmap("{0}DeleteReference.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    InSync.addPixmap(QtGui.QPixmap("{0}InSync.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    OutSync.addPixmap(QtGui.QPixmap("{0}OutSync.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    NoSync.addPixmap(QtGui.QPixmap("{0}NoSync.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    SaveReference.addPixmap(QtGui.QPixmap("{0}SaveReference.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    PairingWarning.addPixmap(QtGui.QPixmap("{0}PairingWarning.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    IsProxy.addPixmap(QtGui.QPixmap("{0}IsProxy.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    CreateReference.addPixmap(QtGui.QPixmap("{0}CreateReference.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    File.addPixmap(QtGui.QPixmap("{0}File.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Folder.addPixmap(QtGui.QPixmap("{0}Folder.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    Refresh.addPixmap(QtGui.QPixmap("{0}Refresh.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    CleanScene.addPixmap(QtGui.QPixmap("{0}SceneClean.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    UnCleanScene.addPixmap(QtGui.QPixmap("{0}SceneUnClean.png".format(iconFolderString)), QtGui.QIcon.Normal, QtGui.QIcon.Off)


class ContextMenuType(object):
    CreateReference = 'Create Reference'
    CreateMultipleReferences = 'Create Multiple References'
    StripGroup = 'Strip Group(s)'
    UpdateGroup = 'Update Group(s)'
    DeleteGroup = 'Delete Group(s)'
    DeleteCategory = 'Delete Category (incl. sub-categories)'
    ForceUpdateGroup = 'Force Update Group(s)'
    UpdateScene = 'Update Scene References'
    StripAsset = 'Strip Asset(s)'
    UpdateAsset = 'Update Asset(s)'
    ForceUpdateAsset = 'Force Update Asset(s)'
    SwapCharacter = 'Swap Character / Outfit'
    ChangeCharacter = 'Swap Character / Outfit - Maintain Namespace'
    RefreshTextures = 'Sync Texture(s)'
    DeleteTextures = 'Delete Texture(s)'
    SceneTextures = 'Sync Scene Textures'
    DeleteAsset = 'Delete Asset(s)'
    SaveReference = 'Save Scene References'
    LoadReference = 'Load Scene References'
    AddLights = 'Add Cutscene Lights'
    SceneCleanup = 'Scene Cleanup'
    ToggleOptimzation = 'Enable Reference Optimization'
    ValidateCharacters = 'Validate Character Setup'
    NonReferencedItems = 'Non-Referenced Item Viewer'
    DuplicateReference = 'Duplicate Reference(s)'
    VehicleControlToolbox = 'Vehicle Controls'
    LoadReferenceVideo = 'Load Reference Video'
    PerforceSubmitNotes = 'Display P4 Submit Notes'
    EditNamespace = 'Edit Namespace'
    ShowLog = 'Show Log'
    ToggleIgnoreProject = "Ignore Project"
    DisableDOF = 'Disable DOFs'
    FixHeel = 'Fix High Heels'
    DeleteHeadObjects = 'Delete headCam && headLight'
    ResetEnvironment = 'Reset Environment'
    DeleteProblematicFacialBones = 'Delete Problematic Facial Bones'
    ResetRollBones = 'Reset Rollbones'
    FixOHScale = 'Fix OH Scale'
    FixCharacterExtensionScale = 'Fix Character Extension Scale'
    FixHierarchyName = 'Fix Hierarchy Name'
    CleanCharacterExtensions = 'Clean Extensions'
    AdjustColor = 'Change Color'
    MakeActiveStage = 'Make Active Stage'
    SelectModel = 'Select Model'
    HideModel = 'Hide Model'
    ShowModel = 'Show Model'
    ToggleP4Sync = 'Enable P4 Sync'
    DuplicateTakesWithAudio = 'Duplicate Takes Toolbox'
    FixHandleNamespace = 'Fix Handle Namespace'
    SwitchOffExpressions = 'Switch Off Expressions'
    SwitchOnExpressions = 'Switch On Expressions'
    OnlyShowGeometry = 'Only Show Geometry'
    ConvertExprDeviceToConst = 'Convert Expression Device to Constraint'
    ClearNanKeys = 'Clear Nan Keys'
    FixZeroScaleNulls = 'Fix Zero Scale'
    PlotToSkeletonCurrentTake = 'Plot To Skeleton: Current Take'
    PlotToControlRigCurrentTake = 'Plot To Control Rig: Current Take'
    PlotToSkeletonAllTakes = 'Plot To Skeleton: All Takes'
    PlotToControlRigAllTakes = 'Plot To Control Rig: All Takes'
    PlotReferenceCurrentTake = 'Plot: Current Take'
    PlotReferenceAllTakes = 'Plot: All Takes'
    CopyLocalAssetPath = 'Copy Local Asset Path'
    CopyDepotAssetPath = 'Copy Depot Asset Path'
    AddToFavourites = 'Add To Favourites'
    OpenFavourites = 'Favourites...'
    SetupPlotNull = "Resource: Set up 'Plotted Null'"
    BindPosePropertyControl = "BindPose Export Property Control"
    RefreshFpsCamConstraints = "FPS Camera: Refresh Constraints"
    EditPath = "Edit Path"

class ContextMenuToolTipDict(object):
    tooltipDict = {ContextMenuType.CreateReference:'popup appears for you to select and add a new reference',
                   ContextMenuType.CreateMultipleReferences:'popup appears for you to select and add multiple new references',
                   ContextMenuType.StripGroup:"strips geo and several 'safe' components from all assets in the group"}


class DirectoryFolderString(object):
    Audio = "audio"
    Characters = "characters"
    Props = "props"
    Sets = "sets"
    Vehicles = "vehicles"


class DirectoryModelIndex(object):
    Name = 0
    Path = 1
    Category = 2


class CategoryFormattedTypeString(object):
    AmbientFace = 'AmbientFace'
    Audio = 'Audio'
    Character = 'Character'
    CharacterHelper = 'Character Helper'
    CharacterCutscene = 'Cutscene Character'
    CharacterIngame = 'Ingame Character'
    CharacterAnimal = 'Animal'
    Face = 'Face'
    Prop = 'Prop'
    RayFire = 'RayFire'
    Set = 'Set'
    Vehicle = 'Vehicle'
    MocapBasePreviz = 'Mocap BasePreviz'
    MocapBlockingModel = 'Mocap BlockingModel'
    MocapCamera = 'MocapCamera'
    MocapPropToy = 'Mocap PropToy'
    MocapSlate = 'Mocap Slate'
    MocapStage = 'Mocap Stage'
    MocapReferencePose = 'Mocap Reference Pose'
    MocapEssential = 'Mocap Essential'
    MocapNonEssential = 'Mocap Non Essential'
    MocapBuildAsset = 'Mocap Build Asset'


class CategoryTypePathsDict(object):
    '''
    Using the RefNull's FormattedName (from types) as the key and the path to that type as the value

    This is the folder the user will  taken to as default, when selected 'Create a New Reference'
    menu option.

    Returns:
        Dict[String] = String (path)
    '''
    categoryPathDict = {CategoryFormattedTypeString.AmbientFace:os.path.join(Config.Project.Path.Root,
                                                                            'art',
                                                                            'animation',
                                                                            'resources'
                                                                            ),
                        CategoryFormattedTypeString.Audio:os.path.join(Config.Project.Path.Root,'art',
                                                                      'animation',
                                                                      'resources',
                                                                      'audio'
                                                                      ),
                        CategoryFormattedTypeString.Character:os.path.join(Config.Project.Path.Root,
                                                                          'art',
                                                                          'animation',
                                                                          'resources',
                                                                          'characters',
                                                                          'Models'
                                                                          ),
                        CategoryFormattedTypeString.CharacterCutscene:os.path.join(Config.Project.Path.Root,
                                                                          'art',
                                                                          'animation',
                                                                          'resources',
                                                                          'characters',
                                                                          'Models',
                                                                          'cutscene',
                                                                          ),
                        CategoryFormattedTypeString.CharacterIngame:os.path.join(Config.Project.Path.Root,
                                                                          'art',
                                                                          'animation',
                                                                          'resources',
                                                                          'characters',
                                                                          'Models',
                                                                          'ingame',
                                                                          ),
                        CategoryFormattedTypeString.CharacterAnimal:os.path.join(Config.Project.Path.Root,
                                                                          'art',
                                                                          'animation',
                                                                          'resources',
                                                                          'characters',
                                                                          'Models',
                                                                          'Animals',
                                                                          ),
                        CategoryFormattedTypeString.CharacterHelper:os.path.join(Config.Project.Path.Root,
                                                                          'art',
                                                                          'animation',
                                                                          'resources',
                                                                          'characters',
                                                                          'characterHelpers'
                                                                          ),
                        CategoryFormattedTypeString.Face:os.path.join(Config.Project.Path.Root,
                                                                     'art',
                                                                     'animation',
                                                                     'resources'
                                                                     ),
                        CategoryFormattedTypeString.Prop:os.path.join(Config.Project.Path.Root,
                                                                     'art',
                                                                     'animation',
                                                                     'resources',
                                                                     'props'
                                                                     ),
                        CategoryFormattedTypeString.RayFire:os.path.join(Config.Project.Path.Root,
                                                                        'art',
                                                                        'animation',
                                                                        'resources',
                                                                        'destruction'
                                                                        ),
                        CategoryFormattedTypeString.Set:os.path.join(Config.Project.Path.Root,
                                                                    'art',
                                                                    'animation',
                                                                    'resources',
                                                                    'Sets'
                                                                    ),
                        CategoryFormattedTypeString.Vehicle:os.path.join(Config.Project.Path.Root,
                                                                        'art',
                                                                        'animation',
                                                                        'resources',
                                                                        'vehicles'
                                                                        ),
                        CategoryFormattedTypeString.MocapEssential:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                              vpConst.PrevizFolder().GetStudioFolderName(),
                                                                              ),
                        CategoryFormattedTypeString.MocapNonEssential:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                              vpConst.PrevizFolder().GetStudioFolderName(),
                                                                              ),
                        CategoryFormattedTypeString.MocapPropToy:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                              vpConst.PrevizFolder().GetStudioFolderName(),
                                                                              'propToys'
                                                                              ),
                        CategoryFormattedTypeString.MocapStage:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                            vpConst.PrevizFolder().GetStudioFolderName(),
                                                                            'stages'
                                                                            ),
                        CategoryFormattedTypeString.MocapBasePreviz:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                 vpConst.PrevizFolder().GetStudioFolderName(),
                                                                                 'basePreviz',
                                                                                 'baseAssets'
                                                                                 ),
                        CategoryFormattedTypeString.MocapBuildAsset:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                    'globalAssets',
                                                                                    'preBuildAssets',
                                                                                    ),
                        CategoryFormattedTypeString.MocapBlockingModel:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                    'globalAssets',
                                                                                    'blockingModels',
                                                                                    ),
                        CategoryFormattedTypeString.MocapSlate:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                    vpConst.PrevizFolder().GetStudioFolderName(),
                                                                                    'slate',
                                                                                    ),
                        CategoryFormattedTypeString.MocapCamera:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                    vpConst.PrevizFolder().GetStudioFolderName(),
                                                                                    'cameras',
                                                                                    ),
                        CategoryFormattedTypeString.MocapReferencePose:os.path.join(Config.VirtualProduction.Path.Previz,
                                                                                    'globalAssets',
                                                                                    'referencePoses'),
                        }