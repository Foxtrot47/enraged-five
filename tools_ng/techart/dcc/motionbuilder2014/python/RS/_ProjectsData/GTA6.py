import os

from RS._ProjectsData import default
from RS import Config


class Gta6Data(default.DefaultData):
    """
    GTA6 Config
    """

    KEY = "americas"

    def GetConfigDict(self):
        orgDict = super(Gta6Data, self).GetConfigDict()
        orgDict.update({
                # americas is purple background with white text
                "MenuColour": ("#800080", "white"),
                })
        return orgDict

    @classmethod
    def GetAnimDataProjectName(cls):
        return ""

    @classmethod
    def GetAnimDataProjectCodeNames(cls):
        return []

    def GetGsSkeletonPath(self):
        # TODO: CHECK!
        return os.path.join(Config.Project.Path.Assets, "fbx", "gs_characters_NewSkel")

    def GetGsAssetPrepSkeletonPath(self):
        # TODO: CHECK!
        return os.path.join("x:\\", "projects", "americas", "assetPrep", "fbxPrep", "Characters_NewSkel")

    def GetSkeltonArray(self):
        return ['SKEL_L_Finger02_NUB',
              'SKEL_L_Finger13_NUB',
              'SKEL_L_Finger23_NUB',
              'SKEL_L_Finger33_NUB',
              'SKEL_L_Finger43_NUB',
              'SKEL_R_Finger02_NUB',
              'SKEL_R_Finger13_NUB',
              'SKEL_R_Finger23_NUB',
              'SKEL_R_Finger33_NUB',
              'SKEL_R_Finger43_NUB',
              'SKEL_L_Clavicle',
              'SKEL_L_UpperArm',
              'SKEL_L_Forearm',
              'SKEL_L_Finger00',
              'SKEL_L_Finger01',
              'SKEL_L_Finger02',
              'SKEL_L_Finger11',
              'SKEL_L_Finger12',
              'SKEL_L_Finger13',
              'SKEL_L_Finger21',
              'SKEL_L_Finger22',
              'SKEL_L_Finger23',
              'SKEL_L_Finger31',
              'SKEL_L_Finger32',
              'SKEL_L_Finger33',
              'SKEL_L_Finger41',
              'SKEL_L_Finger42',
              'SKEL_L_Finger43',
              'SKEL_R_Clavicle',
              'SKEL_R_UpperArm',
              'SKEL_R_Forearm',
              'SKEL_R_Finger00',
              'SKEL_R_Finger01',
              'SKEL_R_Finger02',
              'SKEL_R_Finger11',
              'SKEL_R_Finger12',
              'SKEL_R_Finger13',
              'SKEL_R_Finger21',
              'SKEL_R_Finger22',
              'SKEL_R_Finger23',
              'SKEL_R_Finger31',
              'SKEL_R_Finger32',
              'SKEL_R_Finger33',
              'SKEL_R_Finger41',
              'SKEL_R_Finger42',
              'SKEL_R_Finger43',
              'SKEL_L_Thigh',
              'SKEL_R_Thigh',
              'SKEL_ROOT',
              'SKEL_L_Calf',
              'SKEL_L_Foot',
              'SKEL_L_Toe0_NUB',
              'SKEL_L_Toe0',
              'SKEL_R_Calf',
              'SKEL_R_Foot',
              'SKEL_R_Toe0_NUB',
              'SKEL_R_Toe0',
              'SKEL_R_Hand',
              'SKEL_L_Hand',
              'SKEL_Spine0',
              'SKEL_Spine1',
              'SKEL_Spine2',
              'SKEL_Spine3',
              'SKEL_Spine4',
              'SKEL_Spine5',
              'SKEL_Spine6',
              'SKEL_Neck0',
              'SKEL_Neck1',
              'SKEL_Neck2',
              'SKEL_Head',
              'SKEL_Pelvis',
              'SKEL_Spine_Root',
              'RB_R_BumRoll',
              'RB_L_BumRoll',
              'RB_Neck_1',
              'SKEL_L_Clavicle',
              'SKEL_R_Clavicle',
              'RB_L_ArmRoll',
              'RB_R_ArmRoll',
              'RB_L_ForeArmRoll',
              'RB_R_ForeArmRoll',
              'RB_R_ForeArmRoll_1',
              'RB_R_ForeArmRoll_1',
              'RB_L_ThighRoll',
              'RB_R_ThighRoll',
              'RB_L_CalfRoll',
              'RB_R_CalfRoll',
              'PH_L_Hand',
              'PH_R_Hand',
              'IK_L_Hand',
              'IK_R_Hand',
              'PH_L_Foot',
              'PH_R_Foot',
              'IK_L_Foot',
              'IK_R_Foot',
              'OH_LookDir',
              'OH_FacingDir',
              'RB_Neck',
              'SKEL_L_Finger10',
              'SKEL_L_Finger20',
              'SKEL_L_Finger30',
              'SKEL_L_Finger40',
              'SKEL_R_Finger10',
              'SKEL_R_Finger20',
              'SKEL_R_Finger30',
              'SKEL_R_Finger40',
              'PH_Belt_Front',
              'PH_Belt_Melee',
              'PH_Belt_Rear',
              'PH_Belt_Thrower',
              'PH_L_Hip',
              'PH_R_Boot',
              'PH_Bow',
              'PH_L_Breast',
              'PH_R_Breast',
              'PH_Rifle',
              'PH_R_Shoulder',
              'PH_R_Hip']

    def GetFacingDirectionArrowDict(self):
        """
        Used for character setup - Facing Arrows for Ingame Setup.

        Returns:
            Dict:   key:String
                    value[0]:String
                    value[1]:Int
        """
        return {'OH_FacingDir':("OH_FacingDirection", 140),
                'OH_UpperFixup':("OH_UpperFixupDirection", 100),
                'OH_IndependentMover':("OH_IndependentMoverDirection", 0)}

    def UFCAssetSetupButtonState(self):
        """
        Used for character face setup - UFC Setup.  Will determine if the buttons, for ufc setup
        are in an active state or not.

        Returns:
            Bool
        """
        return True

    def GetGSSkelFolder(self):
        """
        Get folder path for where the gs skeletons live

        Returns:
            String (file path)
        """
        filePath = os.path.join(Config.Project.Path.MocapRoot,
                                'assets',
                                'fbx',
                                'gs_characters_NewSkel')
        return filePath

    def GetDefaultGSSkelName(self):
        return "M01"

    def LaunchVehicleControls(self):
        from RS.Tools.VehicleControl.widgets import vehicleControlWidget
        form = vehicleControlWidget.VehicleControlWidgetDialog()
        form.setFixedSize(425, 920)
        form.show()
        return form

    def fixupExpressions(self):
        """
        launches fixupExpressions script if applies to project.
        """
        from RS.Core.AssetSetup import Characters
        Characters.fixupExpressions(None, None)

    def GetAnimationDirectories(self):
        """
        Used for animation - Anim2Fbx. Returns the animation directories to search for animations

        Returns:
            list
        """
        directories = [("GTA6", os.path.join(Config.Project.Path.Art, "animation", "resources"))]
        directories += [(dlc.Name, os.path.join(dlc.Path.Art, "anim", "export_mb"))
                        for dlc in Config.DLCProjects.itervalues()]
        return tuple(directories)

    def GetSkelFilesDirectory(self):
        """
        Returns the directory where the skel files used by the Anim2Fbx tool lives

        Return:
            string
        """
        return os.path.join(Config.Project.Path.Assets, "anim", "skeletons", "sp")

    def GetFbxDirectories(self):
        """ Returns the directories where the cutscene and ingame fbx live for the project """
        ingamePath = os.path.join(Config.Project.Path.Art, "animation", "ingame", "source")
        dlcIngamePaths = [os.path.join(dlc.Path.Art, "animation", "ingame", "!!scenes")
                          for dlc in Config.DLCProjects.itervalues()]

        cutscenePath = os.path.join(Config.Project.Path.Art, "animation", "cutscene", "!!scenes")
        dlcCutscenePaths = [os.path.join(dlc.Path.Art, "animation", "cutscene", "!!scenes")
                            for dlc in Config.DLCProjects.itervalues()]

        return cutscenePath, dlcCutscenePaths, ingamePath, dlcIngamePaths

    def PrepJointsCharacterSetup(self, model, femaleSetup, fileName, csSetup):
        """
        For Character setup script.
        Args:
            model - FBModel
            femaleSetup - boolean (whether the character is female or not)
            filename - string (name of the file)
            csSetup - boolean (whether this is run for cutscene setup or not)
        Returns:
            vector - for translation
            vector - for color
            boolean - for global or local
            float - for size
            integer - key for prep type
        """
        # bone prep type
        jointPrep = 0
        spinePrep = 1

        # color key
        red = (1, 0, 0)
        orange = (0.9, 0.4, 0)
        pink = (1, 0, 1)
        green = (0.4, 0.7, 0)
        yellow = (1, 1, 0)
        darkBlue = (0, 0.2, 0.65)
        cyan = (0, 1, 1)

        # skel bone list
        skelArray = self.GetSkeltonArray()

        # prep the bones
        if model.Name == skelArray[48]:
            return (0, 0, -180, red, False, None, jointPrep)
        elif model.Name == skelArray[70]:
            return (0, 90, 0, darkBlue, False, None, jointPrep)
        elif model.Name == skelArray[71]:
            return (0, -90, 0, darkBlue, False, None, jointPrep)
        if "CS_" in fileName:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (None, None, None, darkBlue, False, None, spinePrep)
            elif model.Name == skelArray[66]:
                if femaleSetup:
                    return (0, 0, -25.07, darkBlue, False, None, jointPrep)
                else:
                    return (0, 0, -32.66, darkBlue, False, None, jointPrep)
            elif model.Name in [skelArray[67], skelArray[68], skelArray[74], skelArray[69]]:
                return (0, 0, 0, darkBlue, False, None, jointPrep)

        else:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (None, None, None, darkBlue, False, None, spinePrep)
            elif model.Name == skelArray[66]:
                if femaleSetup:
                    return (0, 0, -25.07, darkBlue, False, None, jointPrep)
                else:
                    return (0, 0, -32.66, darkBlue, False, None, jointPrep)
            elif model.Name in [skelArray[67], skelArray[68], skelArray[74], skelArray[69]]:
                return (0, 0, 0, darkBlue, False, None, jointPrep)

        if model.Name == skelArray[10]:
            return (90, -78.48, -122.66, green, False, None, jointPrep)
        elif model.Name == skelArray[11]:
            if femaleSetup:
                return (7.59, 0, 11.517, green, False, None, jointPrep)
            else:
                return (0, 0, 11.517, green, False, None, jointPrep)
        elif model.Name in [skelArray[77], skelArray[79], skelArray[81]]:
            return (0, 0, 0, yellow, False, None, jointPrep)
        elif model.Name == skelArray[12]:
            return (0, 0, 0, green, False, None, jointPrep)
        elif model.Name in [skelArray[58], skelArray[13], skelArray[14], skelArray[15]]:
            return (0, 0, 0, green, False, 0.5, jointPrep)
        # meta carps left
        elif model.Name in [skelArray[98], skelArray[99], skelArray[100], skelArray[101]]:
            return (-90, 0, 0, green, False, 0.5, jointPrep)
        elif model.Name in [skelArray[16], skelArray[17], skelArray[18], skelArray[19], skelArray[20],
                           skelArray[21], skelArray[22], skelArray[23], skelArray[24], skelArray[25],
                           skelArray[26], skelArray[27]]:
            return (0, 0, 0, green, False, 0.5, jointPrep)
        elif model.Name == skelArray[28]:
            return (-90, 78.48, -122.66, orange, False, None, jointPrep)
        elif model.Name == skelArray[29]:
            if femaleSetup:
                return (-7.59, 0, 11.517, orange, False, None, jointPrep)
            else:
                return (0, 0, 11.517, orange, False, None, jointPrep)
        elif model.Name in [skelArray[78], skelArray[80], skelArray[82]]:
            return(0, 0, 0, pink, False, None, jointPrep)
        elif model.Name in [skelArray[30], skelArray[31], skelArray[32], skelArray[33], skelArray[34],
                           skelArray[35], skelArray[36], skelArray[37], skelArray[38], skelArray[39],
                           skelArray[40], skelArray[41], skelArray[42], skelArray[43], skelArray[44],
                           skelArray[45]]:
            return (0, 0, 0, orange, False, None, jointPrep)
        elif model.Name == skelArray[57]:
            return (0, 0, 0, orange, False, 0.5, jointPrep)
        # meta carps right
        elif model.Name in [skelArray[102], skelArray[103], skelArray[104], skelArray[105]]:
            return (90, 0, 0, orange, False, 0.5, jointPrep)
        elif model.Name in [skelArray[46], skelArray[49]]:
            return (0, 0, 0, green, False, None, jointPrep)
        elif model.Name in [skelArray[83], skelArray[85]]:
            return (0, 0, 0, yellow, False, None, jointPrep)
        elif model.Name in [skelArray[47], skelArray[53]]:
            return (0, 0, 0, orange, False, None, jointPrep)
        elif model.Name in [skelArray[84], skelArray[86]]:
            return (0, 0, 0, pink, False, None, jointPrep)

        return (None, None, None, None, None, None, None)

    def SafeBoneList(self):
        """
        list of bone names for adding to a safe bone group
        Returns:
            list of strings
        """
        # skel bone list
        skelArray = self.GetSkeltonArray()

        # safe bone list
        safeBoneList = [skelArray[10],
                        skelArray[11],
                        skelArray[12],
                        skelArray[13],
                        skelArray[14],
                        skelArray[15],
                        skelArray[16],
                        skelArray[17],
                        skelArray[18],
                        skelArray[19],
                        skelArray[20],
                        skelArray[21],
                        skelArray[22],
                        skelArray[23],
                        skelArray[24],
                        skelArray[25],
                        skelArray[26],
                        skelArray[27],
                        skelArray[28],
                        skelArray[29],
                        skelArray[30],
                        skelArray[31],
                        skelArray[32],
                        skelArray[33],
                        skelArray[34],
                        skelArray[35],
                        skelArray[36],
                        skelArray[37],
                        skelArray[38],
                        skelArray[39],
                        skelArray[40],
                        skelArray[41],
                        skelArray[42],
                        skelArray[43],
                        skelArray[44],
                        skelArray[45],
                        skelArray[46],
                        skelArray[47],
                        skelArray[48],
                        skelArray[49],
                        skelArray[50],
                        skelArray[52],
                        skelArray[53],
                        skelArray[54],
                        skelArray[56],
                        skelArray[57],
                        skelArray[58],
                        skelArray[59],
                        skelArray[60],
                        skelArray[61],
                        skelArray[62],
                        skelArray[63],
                        skelArray[64],
                        skelArray[65],
                        skelArray[66],
                        skelArray[67],
                        skelArray[68],
                        skelArray[69],
                        skelArray[70],
                        skelArray[71],
                        skelArray[87],
                        skelArray[88],
                        skelArray[89],
                        skelArray[90],
                        skelArray[91],
                        skelArray[92],
                        skelArray[93],
                        skelArray[94], ]
        return safeBoneList

    def CharacterExtensionBoneStringList(self):
        """
        list of bone names for populating character extensions
        Returns:
            list of strings
        """
        boneList = (# base helpers
                    "mover",
                    "PH_L_Hand",
                    "PH_R_Hand",
                    "PH_L_Foot",
                    "PH_R_Foot",
                    "IK_L_Hand",
                    "IK_R_Hand",
                    "IK_L_Foot",
                    "IK_R_Foot",
                    "HeelHeight",
                    "IK_Head",
                    "IK_Root",
                    'OH_Mover',
                    'OH_Pelvis',
                    # facing direction
                    "LookAt_Target",
                    "CTRL_C_lookAt",
                    "IK_FacingDirection",
                    "OH_FacingDirection",
                    "OH_UpperFixupDirection",
                    "OH_IndependentMoverDirection",
                    # bag
                    "BagBody",
                    "BagBone_R",
                    "BagBone_L",
                    # satchel
                    "ES_SatchelRoot",
                    "ES_Flap",
                    "ES_FrontFlap0",
                    "ES_FrontFlap01",
                    "ES_FrontFlap02",
                    "ES_BackFlap0",
                    "ES_BackFlap01",
                    "ES_BackFlap02",
                    "ES_MidFlap0",
                    "ES_MidFlap01",
                    "ES_OpenBag0",
                    "ES_OpenBag01",
                    "ES_OpenBag02",
                    "ES_WeightBag0",
                    "ES_WeightBag01",
                    # weapon attachments
                    "PH_Belt_Front",
                    "PH_Belt_Melee",
                    "PH_Belt_Rear",
                    "PH_Belt_Thrower",
                    "PH_L_Hip",
                    "PH_R_Boot",
                    "PH_Bow",
                    "PH_L_Breast",
                    "PH_R_Breast",
                    "PH_Rifle",
                    "PH_R_Shoulder",
                    "PH_R_Hip",
                    "SM_rifle",
                    # mask
                    "MH_MaskRoot",
                    "MH_Mask_LowRoot",
                    "MH_Mask_R_Low",
                    "MH_Mask_L_Low",
                    "MH_Mask_C_Low",
                    "MH_Mask_MidRoot",
                    "MH_Mask_R_Mid",
                    "MH_Mask_L_Mid",
                    "MH_Mask_C_Mid",
                    "MH_Mask_UpRoot",
                    "MH_Mask_R_Upp",
                    "MH_Mask_L_Upp",
                    "MH_Mask_C_Upp",
                    # contact point helpers
                    "XH_L_Hand00",
                    "XH_R_Hand00",
                    "OH_Spine00",
                    "OH_Spine01",
                    "OH_Incline",
                    # other
                    "FPSCamera_Extension",
                    "CH_L_Hand",
                    "CH_R_Hand",
                    "OH_TorsoDirection",
                    )
        return boneList

    def CharacterIKPHBoneList(self):
        """
        list of IK/PH bone names
        Returns:
            list of strings
        """
        # skel bone list
        skelArray = self.GetSkeltonArray()

        boneList = ['mover',
                    skelArray[87],
                    skelArray[88],
                    skelArray[91],
                    skelArray[92],
                    skelArray[89],
                    skelArray[90],
                    skelArray[93],
                    skelArray[94],
                    skelArray[95],
                    skelArray[96], ]
        return boneList

    def OldExpressionBoneNameRegex(self):
        """
        Returns a list of all the names of old expression Constraints
        """
        nameRegex = ("abs(x)|acos(x)|atan(x)|ceil(x)|comp(v,i)|cos(x)|"
                     "degToRad(x)|e|exp(x)|floor(x)|length(v)|ln(x)|log(x)|max(x,y)|"
                     "min(x,y)|pi|mod(x,y)|radToDeg(x)|sin(x)|sqrt(x)|tan(x)|unit(v)|"
                     "vif(c,v1,v2)|RB_L_ThighRoll-Y_rotation|RB_L_ThighRoll-Z_rotation|RB_R_ThighRoll-Y_rotation|"
                     "RB_R_ThighRoll-Z_rotation|RB_L_ForeArmRoll-Y_rotation|RB_L_ForeArmRoll-Z_rotation|"
                     "RB_L_ArmRoll-Y_rotation|RB_L_ArmRoll-Z_rotation|RB_R_ForeArmRoll-Y_rotation|"
                     "RB_R_ForeArmRoll-Z_rotation|RB_R_ArmRoll-Y_rotation|RB_R_ArmRoll-Z_rotation|"
                     "EO_L_Foot-Z_rotation|EO_L_Toe-Z_rotation|PH_L_Heel-X_Position|PH_L_Heel-Y_Position|"
                     "RB_L_KneeFront-X_rotation|RB_L_KneeFront-Y_rotation|RB_L_KneeFront-Z_rotation|"
                     "MH_L_KneeBack-X_Position|EO_R_Foot-Z_rotation|EO_R_Toe-Z_rotation|PH_R_Heel-X_Position|"
                     "PH_R_Heel-Y_Position|RB_R_KneeFront-X_rotation|RB_R_KneeFront-Y_rotation|"
                     "RB_R_KneeFront-Z_rotation|MH_R_KneeBack-X_Position|RB_L_ThighRoll-X_rotation|"
                     "RB_R_ThighRoll-X_rotation|RB_L_BumRoll-X_Position|RB_L_BumRoll-Y_Position|"
                     "RB_L_BumRoll-Z_Position|RB_L_BumRoll-Y_rotation|"
                     "RB_L_BumRoll-Z_rotation|RB_L_BumRoll-X_rotation|RB_R_BumRoll-X_Position|"
                     "RB_R_BumRoll-Y_Position|RB_R_BumRoll-Z_Position|RB_R_BumRoll-Y_rotation|"
                     "RB_R_BumRoll-Z_rotation|RB_R_BumRoll-X_rotation|RB_Satchel-X_rotation|"
                     "RB_Satchel-Y_rotation|MH_Hair_Scale-X_scale|MH_Hair_Scale-Y_scale|"
                     "MH_Hair_Scale-Z_scale|MH_Hair_Crown-X_Position|MH_Hair_Crown-X_scale|"
                     "MH_Hair_Crown-Y_scale|MH_Hair_Crown-Z_scale|MH_L_Finger41-X_Position|"
                     "MH_L_HandSide-X_Position|MH_L_HandSide-Z_Position|MH_L_Finger31-X_Position|"
                     "MH_L_Finger21-X_Position|MH_L_Finger11-X_Position|MH_L_Finger01-X_Position|"
                     "MH_L_Finger01Bulge-X_Position|MH_L_Finger01Top-X_Position|MH_L_Finger01Top-Y_Position|"
                     "MH_L_Finger01Top-Z_Position|RB_L_ForeArmRoll-X_rotation|MH_L_Wrist-X_rotation|"
                     "RB_L_ArmRoll-X_rotation|MH_L_Elbow-X_Position|MH_L_Elbow-Y_Position|"
                     "MH_R_Finger01-X_Position|MH_R_Finger01Bulge-X_Position|MH_R_Finger11-X_Position|"
                     "MH_R_Finger21-X_Position|MH_R_Finger31-X_Position|MH_R_Finger41-X_Position|"
                     "MH_R_HandSide-X_Position|MH_R_HandSide-Z_Position|MH_R_Finger01Top-X_Position|"
                     "MH_R_Finger01Top-Y_Position|MH_R_Finger01Top-Z_Position|RB_R_ForeArmRoll-X_rotation|"
                     "MH_R_Wrist-X_rotation|RB_R_ArmRoll-X_rotation|MH_R_Elbow-X_Position|MH_R_Elbow-Y_Position|"
                     "MH_L_Jacket2-Y_Position|MH_L_Jacket2-Z_Position|MH_R_Jacket2-Y_Position|"
                     "MH_R_Jacket2-Z_Position|MH_L_Jacket1-Y_Position|MH_L_Jacket1-Z_Position|"
                     "MH_R_Jacket1-Y_Position|MH_R_Jacket1-Z_Position|RB_L_OpenJacket-Y_rotation|"
                     "RB_L_OpenJacket-Z_rotation|RB_R_OpenJacket-Y_rotation|RB_R_OpenJacket-Z_rotation|"
                     "LOC_R_eyeDriver_orientConstraint1|LOC_L_eyeDriver_orientConstraint1|"
                     "GRP_convergenceGUI_parentConstraint1|GRP_C_lookAt_parentConstraint1|"
                     "GRP_L_lookAtOffset_parentConstraint1|GRP_R_lookAtOffset_parentConstraint1|"
                     "LOC_L_LookAtDriver_aimConstraint1|LOC_R_LookAtDriver_aimConstraint1|"
                     "LOC_L_LookAtDriverOffset_aimConstraint1|LOC_R_LookAtDriverOffset_aimConstraint1|"
                     "setup_relationConstraint|3Lateral UFC|")
        return nameRegex

    def GetFpsChBones(self):
        """
        Returns a list of Bones used in fpsController Tool (bug:2635558)
        """
        return ('CH_R_Hand', 'CH_L_Hand')

    def OldLookAtRigDict(self):
        """
        Dict of 'old' LookAt rig components and their FB Type
        url:bugstar:2575815
        To be used in the ref editor to remove components

        Returns:
                Dict[string] = list(FBType)
        """
        import pyfbsdk as mobu

        return {'OH_LookDir_Lookat': [mobu.FBModel, mobu.FBMaterial, mobu.FBGroup],
                'IK_Control_Rig': [mobu.FBModel],
                'TargetParent': [mobu.FBModel],
                'MoverSpaceTarget': [mobu.FBModel],
                'HeadSpaceTarget': [mobu.FBModel],
                'EyeOrigin': [mobu.FBModel],
                'Look_At_Direction': [mobu.FBModel],
                'MoverTracer': [mobu.FBConstraint],
                'HeadTracer': [mobu.FBConstraint],
                'EyeOriginRotation': [mobu.FBConstraint],
                'Head_Delta': [mobu.FBConstraint],
                'Lookat_Aim': [mobu.FBConstraint],
                'MoverSpaceConstraint': [mobu.FBConstraint],
                'HeadSpaceConstraint': [mobu.FBConstraint],
                'FollowHead_Bool': [mobu.FBConstraint],
                'IK_Head_Eye_Control': [mobu.FBConstraint],
                'Eye_Track_Bool': [mobu.FBConstraint],
                'OH_LookDir_LookAtNull': [mobu.FBGroup]}

    def UseUFC(self):
        """
        Setups the background processes for running the facial animation in Motion Builder
        """
        return True
