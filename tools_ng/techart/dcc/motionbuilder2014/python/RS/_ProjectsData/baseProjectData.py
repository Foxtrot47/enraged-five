"""
===================================================================================================

                        DO NOT TOUCH, EDIT OR ADD METHODS TO THIS CLASS
                        WHICH ARE TO DO IN ANY REGUARD TO THE PROJECT OR
                        CONFIG.

                        THIS IS THE BASE PLUGIN CLASS, AND IS ONLY A META
                        CLASS FACTORY
===================================================================================================
"""


class BaseProjectData(object):
    # Key is the identifier for the plugin (from the called classes)
    KEY = None

    # KEY_BINDINGS SHOULD NEVER BE TOUCHED ON SUBCLASS
    KEY_BINDINGS = {}

    class __metaclass__(type):
        def __init__(cls, name, bases, dict):
            if not hasattr(cls, 'KEY_BINDINGS'):
                cls.KEY_BINDINGS = {}
            else:
                if cls.KEY is None:
                    return
                cls.KEY_BINDINGS[cls.KEY] = cls

    def __init__(self):
        pass
