from RS._ProjectsData import baseProjectData


class DefaultData(baseProjectData.BaseProjectData):
    """
    Default Config, this is the base class all configs should be derived from, allowing for a
    common base class.
    """
    KEY = "other"

    @classmethod
    def GetProject(cls, key):
        return cls.KEY_BINDINGS.get(key)

    @classmethod
    def GetAllProjectNames(cls):
        return cls.KEY_BINDINGS.keys()

    def GetConfigDict(self):
        return {
                "MenuColour": ("#ffa500", "black"),
                "FPSTool": False,
                }

    def GetConfig(self, name, defaultValue=None):
        return self.GetConfigDict().get(name)

    def GetGsAssetPrepSkeletonPath(self):
        raise NotImplementedError("Not Set In Config")

    def GetGsSkeletonPath(self):
        raise NotImplementedError("Not Set In Config")

    def GetNullName(self, null):
        return null.Parent.Name.lower()

    def GetSkeltonArray(self):
        return ['SKEL_L_Finger0_NUB',
              'SKEL_L_Finger1_NUB',
              'SKEL_L_Finger2_NUB',
              'SKEL_L_Finger3_NUB',
              'SKEL_L_Finger4_NUB',
              'SKEL_R_Finger0_NUB',
              'SKEL_R_Finger1_NUB',
              'SKEL_R_Finger2_NUB',
              'SKEL_R_Finger3_NUB',
              'SKEL_R_Finger4_NUB',
              'SKEL_L_Clavicle',
              'SKEL_L_UpperArm',
              'SKEL_L_Forearm',
              'SKEL_L_Finger00',
              'SKEL_L_Finger01',
              'SKEL_L_Finger02',
              'SKEL_L_Finger10',
              'SKEL_L_Finger11',
              'SKEL_L_Finger12',
              'SKEL_L_Finger20',
              'SKEL_L_Finger21',
              'SKEL_L_Finger22',
              'SKEL_L_Finger30',
              'SKEL_L_Finger31',
              'SKEL_L_Finger32',
              'SKEL_L_Finger40',
              'SKEL_L_Finger41',
              'SKEL_L_Finger42',
              'SKEL_R_Clavicle',
              'SKEL_R_UpperArm',
              'SKEL_R_Forearm',
              'SKEL_R_Finger00',
              'SKEL_R_Finger01',
              'SKEL_R_Finger02',
              'SKEL_R_Finger10',
              'SKEL_R_Finger11',
              'SKEL_R_Finger12',
              'SKEL_R_Finger20',
              'SKEL_R_Finger21',
              'SKEL_R_Finger22',
              'SKEL_R_Finger30',
              'SKEL_R_Finger31',
              'SKEL_R_Finger32',
              'SKEL_R_Finger40',
              'SKEL_R_Finger41',
              'SKEL_R_Finger42',
              'SKEL_L_Thigh',
              'SKEL_R_Thigh',
              'SKEL_ROOT',
              'SKEL_L_Calf',
              'SKEL_L_Foot',
              'SKEL_L_Toe0Nub',
              'SKEL_L_Toe0',
              'SKEL_R_Calf',
              'SKEL_R_Foot',
              'SKEL_R_Toe0Nub',
              'SKEL_R_Toe0',
              'SKEL_R_Hand',
              'SKEL_L_Hand',
              'SKEL_Spine0',
              'SKEL_Spine1',
              'SKEL_Spine2',
              'SKEL_Spine3',
              'SKEL_Neck_1',
              'SKEL_Head',
              'SKEL_Pelvis',
              'SKEL_Spine_Root',
              'RB_R_BumRoll',
              'RB_L_BumRoll',
              'RB_Neck_1',
              'SKEL_L_Clavicle',
              'SKEL_R_Clavicle',
              'RB_L_ArmRoll',
              'RB_R_ArmRoll',
              'RB_L_ForeArmRoll',
              'RB_R_ForeArmRoll',
              'RB_R_ForeArmRoll_1',
              'RB_R_ForeArmRoll_1',
              'RB_L_ThighRoll',
              'RB_R_ThighRoll',
              'RB_L_CalfRoll',
              'RB_R_CalfRoll',
              'PH_L_Hand',
              'PH_R_Hand',
              'IK_L_Hand',
              'IK_R_Hand',
              'PH_L_Foot',
              'PH_R_Foot',
              'IK_L_Foot',
              'IK_R_Foot',
              'IK_Head',
              'IK_Root',
              'RB_Neck']

    def GetFacingDirectionArrowDict(self):
        '''
        Used for character setup - Facing Arrows for Ingame Setup.

        Returns:
            Dict:   key:String
                    value[0]:String
                    value[1]:Int
        '''
        return {'IK_Root':("IK_FacingDirection", 0)}

    def GetHelperPrefix(self):
        '''
        Used for character setup - Facing Arrows for Ingame Setup.

        Returns:
            String, String
        '''
        return "IK", "IK_Head"

    def UFCAssetSetupButtonState(self):
        '''
        Used for character face setup - UFC Setup.  Will determine if the buttons, for ufc setup
        are in an active state or not.

        Returns:
            Bool
        '''
        return False

    def GetGSSkelFolder(self):
        raise NotImplementedError("Not Set In Config")

    def GetDefaultGSSkelName(self):
        return "CS_Male_Proxy_gs"

    def LaunchVehicleControls(self):
        from RS.Tools.UI import VehicleControls
        tool = VehicleControls.VehicleControlsToolbox()
        tool.Show()
        return tool


    def fixupExpressions(self):
        '''
        launches fixupExpressions script if applies to project.
        '''
        return

    def UseUFC(self):
        '''
        Sets up the background processes for running the facial animation in Motion Builder
        '''
        pass