import os
from RS._ProjectsData import default

from RS import Config


class Gta5Data(default.DefaultData):
    """
    GTA5 Config
    """

    KEY = "gta5"

    @classmethod
    def GetAnimDataProjectName(cls):
        return "GTA 5"

    def GetGsSkeletonPath(self):
        return os.path.join("x:\\", "projects", "paradise_dlc", "assets", "fbx", "gs_characters", "*.fbx")

    def GetNullName(self, null):
        return null.Name.lower()

    def GetConfigDict(self):
        orgDict = super(Gta5Data, self).GetConfigDict()

        orgDict.update({
                # gta is the Rockstar North Logo Blue background color with white text
                "MenuColour": ("#004684", "white"),
                "FPSTool": True,
                "CharSetupGestureSafeArea":True,
                "CharSetupFacingDirection":False,
                "CharSetupPrepSpine":False,
                "CharSetupNeckZeroTrns":False,
                "CharSetupFaceGUI":True,
                "CharSetupFaceTempParent":True,
                "CharSetupPelvisSetup":False,
                "CharSetupGTAFaceFix":True,
                "CharSetupMasterDummy":False,
                "CharSetupUFCSetup":False,
                "CharSetupArmRotation":False,
                "CharSetupPlot":False,
                "CharSetupUFCPreSetup":False,
                "UseUFC":True,
                })
        return orgDict

    def GetGSSkelFolder(self):
        """
        Get folder path for where the gs skeletons live

        Returns:
            String (file path)
        """
        filePath = os.path.join(Config.Project.Path.MocapRoot,
                                'fbx',
                                'gs')
        return filePath

    def GetDefaultGSSkelName(self):
        return "CS_Male_Proxy_gs"

    def fixupExpressions(self):
        """ launches fixupExpressions script if applies to project. """
        return

    def GetAnimationDirectories(self):
        """
        Used for animation - Anim2Fbx. Returns the animation directories to search for animations

        Returns:
            list
        """
        directories = [("GTA5", "x:\\gta5\\art\\ng\\anim\\export_mb")]
        directories += [(dlc.Name, os.path.join(dlc.Path.Art, "anim", "export_mb"))
                        for dlc in Config.DLCProjects.itervalues()]
        return tuple(directories)

    def GetSkelFilesDirectory(self):
        """
        Returns the directory where the skel files used by the Anim2Fbx tool lives

        Return:
            string
        """
        return os.path.join(Config.Tool.Path.Root, "etc", "config", "anim", "skeletons", "sp")

    def GetFbxDirectories(self):
        """ Returns the directories where the cutscene and ingame fbx live for the project """
        ingamePath = os.path.join(Config.Project.Path.Art, "anim", "source_fbx")
        dlcIngamePaths = [os.path.join(dlc.Path.Art, "anim", "source_fbx")
                          for dlc in Config.DLCProjects.itervalues()]

        cutscenePath = os.path.join(Config.Project.Path.Art, "animation", "cutscene", "!!scenes")
        dlcCutscenePaths = [os.path.join(dlc.Path.Art, "animation", "cutscene", "!!scenes")
                            for dlc in Config.DLCProjects.itervalues()]

        return cutscenePath, dlcCutscenePaths, ingamePath, dlcIngamePaths

    def PrepJointsCharacterSetup(self, model, femaleSetup, fileName, csSetup):
        '''
        For Character setup script.
        Args:
            model - FBModel
            femaleSetup - boolean (whether the character is female or not)
            filename - string (name of the file)
            csSetup - boolean (whether this is run for cutscene setup or not)
        Returns:
            vector - for translation
            vector - for color
            boolean - for global or local
            float - for size
            integer - key for prep type
        '''
        # bone prep type
        jointPrep = 0
        spinePrep = 1
        colorPrep = 2

        # color key
        red = (1, 0, 0)
        orange = (0.9, 0.4, 0)
        pink = (1, 0, 1)
        green = (0.4, 0.7, 0)
        yellow = (1, 1, 0)
        darkBlue = (0, 0.2, 0.65)
        cyan = (0, 1, 1)

        # skel bone list
        skelArray = self.GetSkeltonArray()

        # prep the bones
        if model.Name == skelArray[48]:
            return (0, 0,-180, red, False, None, jointPrep)
        elif model.Name == skelArray[65]:
            return (0,90,0, darkBlue, False, None, jointPrep)
        elif model.Name == skelArray[66]:
            return (0,-90,0, darkBlue, False, None, jointPrep)
        if "Male" in fileName or "Female" in fileName:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (0,0,0, darkBlue, False, None, jointPrep)
            elif model.Name == skelArray[69]:
                return (0,0,0, cyan, False, None, jointPrep)
        else:
            if model.Name in [skelArray[59], skelArray[60], skelArray[61], skelArray[62], skelArray[63],
                              skelArray[64]]:
                return (None, None, None, darkBlue, None, None, colorPrep)
            elif model.Name == skelArray[69]:
                return (None, None, None, cyan, None, None, colorPrep)
        if model.Name == skelArray[10]:
            return (90,-71.880,-90, green, False, None, jointPrep)
        elif model.Name == skelArray[11]:
            return (0,0,18.12, green, False, None, jointPrep)
        elif model.Name == skelArray[12]:
            return (0,0,0, green, False, None, jointPrep)
        elif model.Name in [skelArray[72], skelArray[74], skelArray[76]]:
            return (0,0,0, yellow, False, None, jointPrep)
        elif model.Name == skelArray[58]:
            return (0,0,0, green, False, 0.5, jointPrep)
        if csSetup == 1:
            if model.Name in [skelArray[13], skelArray[14], skelArray[15]]:
                return (-90,0,0, green, False, 0.5, jointPrep)
        else:
            if model.Name in [skelArray[13], skelArray[14], skelArray[15]]:
                return (None, None, None, green, None, 0.5, colorPrep)

        if model.Name in [skelArray[16],skelArray[19], skelArray[22], skelArray[25]]:
            return (-90,0,0, green, False, 0.5, jointPrep)
        elif model.Name in [skelArray[17], skelArray[18], skelArray[20], skelArray[21], skelArray[23],
                          skelArray[24], skelArray[26], skelArray[27]]:
            return (0,0,0, green, False, 0.5, jointPrep)
        elif model.Name == skelArray[18]:
            return (0,0,0, green, False, 0.5, jointPrep)

        elif model.Name == skelArray[28]:
            return (-90,71.880,-90, orange, False, None, jointPrep)
        elif model.Name == skelArray[29]:
            return (0,0,18.12, orange, False, None, jointPrep)

        elif model.Name in [skelArray[73], skelArray[75], skelArray[77]]:
            return (0,0,0, pink, False, None, jointPrep)
        elif model.Name == skelArray[30]:
            return (0,0,0, orange, False, None, jointPrep)
        elif model.Name == skelArray[57]:
            return (0,0,0, orange, False, 0.5, jointPrep)

        if csSetup == 1:
            if model.Name in [skelArray[31], skelArray[32], skelArray[33]]:
                return (90,0,0, orange, False, 0.5, jointPrep)
        else:
            if model.Name in [skelArray[31], skelArray[32], skelArray[33]]:
                return (None, None, None, orange, None, 0.5, colorPrep)
        if model.Name in [skelArray[34], skelArray[37], skelArray[40],skelArray[43]]:
            return (-90,0,0, orange, False, 0.5, jointPrep)
        elif model.Name in [skelArray[35],skelArray[36], skelArray[38], skelArray[39], skelArray[41],
                            skelArray[42], skelArray[44], skelArray[45]]:
            return (0,0,0, orange, False, 0.5, jointPrep)
        elif model.Name in [skelArray[46], skelArray[49]]:
            return (0,0,0, green, False, None, jointPrep)
        elif model.Name in [skelArray[78], skelArray[80]]:
            return (0,0,0, yellow, False, None, jointPrep)
        elif model.Name in [skelArray[47], skelArray[53]]:
            return (0,0,0, orange, False, None, jointPrep)
        elif model.Name in [skelArray[79], skelArray[81]]:
            return (0,0,0, pink, False, None, jointPrep)

        return (None, None, None, None, None, None, None)


    def SafeBoneList(self):
        '''
        list of bones
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        # safe bone list
        safeBoneList = [skelArray[10],
                        skelArray[11],
                        skelArray[12],
                        skelArray[13],
                        skelArray[14],
                        skelArray[15],
                        skelArray[16],
                        skelArray[17],
                        skelArray[18],
                        skelArray[19],
                        skelArray[20],
                        skelArray[21],
                        skelArray[22],
                        skelArray[23],
                        skelArray[24],
                        skelArray[25],
                        skelArray[26],
                        skelArray[27],
                        skelArray[28],
                        skelArray[29],
                        skelArray[30],
                        skelArray[31],
                        skelArray[32],
                        skelArray[33],
                        skelArray[34],
                        skelArray[35],
                        skelArray[36],
                        skelArray[37],
                        skelArray[38],
                        skelArray[39],
                        skelArray[40],
                        skelArray[41],
                        skelArray[42],
                        skelArray[43],
                        skelArray[44],
                        skelArray[45],
                        skelArray[46],
                        skelArray[47],
                        skelArray[48],
                        skelArray[49],
                        skelArray[50],
                        skelArray[52],
                        skelArray[53],
                        skelArray[54],
                        skelArray[56],
                        skelArray[57],
                        skelArray[58],
                        skelArray[59],
                        skelArray[60],
                        skelArray[61],
                        skelArray[62],
                        skelArray[63],
                        skelArray[64],
                        skelArray[65],
                        skelArray[66],
                        skelArray[82],
                        skelArray[83],
                        skelArray[84],
                        skelArray[85],
                        skelArray[86],
                        skelArray[87],
                        skelArray[88],
                        skelArray[89],]
        return safeBoneList

    def CharacterExtensionBoneStringList(self):
        '''
        list of bone names for populating character extensions
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        boneList = ["mover",
                    skelArray[82],
                    skelArray[83],
                    skelArray[86],
                    skelArray[87],
                    skelArray[84],
                    skelArray[85],
                    skelArray[88],
                    skelArray[89],
                    skelArray[90],
                    skelArray[91],
                    "IK_FacingDirection",
                    "HeelHeight",
                    "BagBody",
                    "BagBone_R",
                    "BagBone_L",]
        return boneList


    def CharacterIKPHBoneList(self):
        '''
        list of IK/PH bone names
        Returns:
            list of strings
        '''
        # skel bone list
        skelArray = self.GetSkeltonArray()

        boneList = ['mover',
                    skelArray[82],
                    skelArray[83],
                    skelArray[86],
                    skelArray[87],
                    skelArray[84],
                    skelArray[85],
                    skelArray[88],
                    skelArray[89],
                    skelArray[90],
                    skelArray[91],]
        return boneList

    def UseUFC(self):
        '''
        Sets up the background processes for running the facial animation in Motion Builder
        '''
        pass