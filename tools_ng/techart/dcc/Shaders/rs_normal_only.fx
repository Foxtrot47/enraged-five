bool g_showR <
	string UIName = "Show Red";
> = true;

bool g_showG <
	string UIName = "Show Green";
> = true;

bool g_showB <
	string UIName = "Show Blue";
> = true;

bool g_flipG <
	string UIName = "Flip Green";
> = true;


// transformations
float4x4 WorldViewProj : WORLDVIEWPROJ;
float4x4 WorldViewIt : WORLDVIEWIT;

/********** CG SHADER FUNCTIONS *********************/

// input from application
struct VS_INPUT 
{
	float4 position : POSITION;
	float3 normal   : NORMAL;
};

// output to fragment program
struct VS_OUTPUT 
{
    float4 position : POSITION;
    float3 normal : TEXCOORD0;
};


// VERTEX SHADER --------------------------

VS_OUTPUT vertShader(VS_INPUT In, uniform float4x4 worldViewProj, uniform float4x4 WorldViewIt)
{
  VS_OUTPUT Out;
  float4 N = mul(In.normal, -WorldViewIt);  //normal to camera space
  Out.position = mul(In.position, worldViewProj); //vertex to clip space
  Out.normal = N;
  return Out;
}

// FRAGMENT PROGRAM --------------------------

float4 fragShad(VS_OUTPUT In) : COLOR
{     
	float4 color = float4(In.normal,1);
	color.xy = -color.xy * 0.5 + 0.5;
	if (!g_showR) color.r = 0;
	if (!g_showG) color.g = 0;
	if (!g_showB) color.b = 0;
	// FLIP GREEN?
	if (g_flipG) color.y = 1 - color.y;
	return color;
}

// TECHNIQUES --------------------------

technique NormalMap
{ 
    pass p0 
    {		
		CullMode = CW;
        VertexShader = compile vs_2_0 vertShader(WorldViewProj,WorldViewIt);
        PixelShader  = compile ps_2_0 fragShad();
    }
}