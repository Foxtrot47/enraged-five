//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // build-config
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	processFunction: collapsePerMission,
	
	restEndpoint: config.memoryPoolsStats,
	restEndpointAsync: config.memoryPoolsStatsAsync,

	enableCSVExport: true,

	// No nested array results
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Pool Name", getValue: function(d) {return d.PoolName; }},
	     {title: "Samples", getValue: function(d) {return d.NumSamples; }},
	     //{title: "Mission(s) With Max Peak", getValue: function(d) {return d.MissionName; }},
	     {title: "Mission(s) With Max Peak", getValue: function(d) {return d.MissionNames.join(",<br />"); }},
	     {title: "Min Peak", getValue: function(d) {return fixed(d.MinPeak); }},
	     {title: "Avg Peak", getValue: function(d) {return fixed(d.AvgPeak); }},
	     {title: "Max Peak", getValue: function(d) {return fixed(d.MaxPeak); }},     
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
	
	// One level only
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	   {title: "Memory Pools List", regexp: "", filterItem: 0},
	],

};

function collapsePerMission(data) {
	var processedData = {};
	
	$.each(data, function(i, d) {
		if (processedData.hasOwnProperty(d.PoolName)) {
			// Save the min peak
			processedData[d.PoolName].MinPeak = 
					(processedData[d.PoolName].MinPeak < d.MinPeak) ? processedData[d.PoolName].MinPeak : d.MinPeak;
			
			// Save the max peak and the mission where that was reached
			if (processedData[d.PoolName].MaxPeak <= d.MaxPeak) {
				processedData[d.PoolName].MaxPeak = d.MaxPeak;
				processedData[d.PoolName].MissionNames.push(d.MissionName);
			}
			
			// Recalculate the Avg Peak
			processedData[d.PoolName].AvgPeak = (processedData[d.PoolName].AvgPeak + d.AvgPeak) / 2; 
				
			// Sum up the sample size
			processedData[d.PoolName].NumSamples += d.NumSamples;
		}
		else {
			processedData[d.PoolName] = d;
			processedData[d.PoolName].MissionNames = [d.MissionName];
		}
	});
	
	return processedData;
}