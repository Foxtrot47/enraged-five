//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // build-config
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.memorySkeletonsStats,
	restEndpointAsync: config.memorySkeletonsStatsAsync,
	
	enableCSVExport: true,

	// No nested array results
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Mission", getValue: function(d) {return d.MissionName; }},
	     {title: "Samples", getValue: function(d) {return d.NumSamples; }},

	     //{title: "Platform", getValue: function(d) {return d.PlatformName; }},
	     //{title: "Build Config", getValue: function(d) {return d.BuildConfigName; }},
	
	     {title: "Min Buildings Peak", getValue: function(d) {return fixed(d.MinBuildingsPeak); }},
	     {title: "Avg Buildings Peak", getValue: function(d) {return fixed(d.AvgBuildingsPeak); }},
	     {title: "Max Buildings Peak", getValue: function(d) {return fixed(d.MaxBuildingsPeak); }},
	     
	     {title: "Min Objects Peak", getValue: function(d) {return fixed(d.MinObjectsPeak); }},
	     {title: "Avg Objects Peak", getValue: function(d) {return fixed(d.AvgObjectsPeak); }},
	     {title: "Max Objects Peak", getValue: function(d) {return fixed(d.MaxObjectsPeak); }},
	     
	     {title: "Min Peds Peak", getValue: function(d) {return fixed(d.MinPedsPeak); }},
	     {title: "Avg Peds Peak", getValue: function(d) {return fixed(d.AvgPedsPeak); }},
	     {title: "Max Peds Peak", getValue: function(d) {return fixed(d.MaxPedsPeak); }},
	     
	     {title: "Min Total Peak", getValue: function(d) {return fixed(d.MinTotalPeak); }},
	     {title: "Avg Total Peak", getValue: function(d) {return fixed(d.AvgTotalPeak); }},
	     {title: "Max Total Peak", getValue: function(d) {return fixed(d.MaxTotalPeak); }},
	     
	     {title: "Min Vehicles Peak", getValue: function(d) {return fixed(d.MinVehiclesPeak); }},
	     {title: "Avg Vehicles Peak", getValue: function(d) {return fixed(d.AvgVehiclesPeak); }},
	     {title: "Max Vehicles Peak", getValue: function(d) {return fixed(d.MaxVehiclesPeak); }},
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
	
	// One level only
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	   {title: "Memory Skeletons List", regexp: "", filterItem: 0},
	],

};