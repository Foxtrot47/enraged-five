//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-map-exports", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 false, // section
	                 false, // user
	                 false, // dates
    ],
};

var platformNames = getPlatforms();

var restEndpoint = config.mapExportsByUser,
	restEndpointAsync = config.mapExportsByUserAsync,
	defaultFilterText = "",
	exportsStats,
	dateTimeTip = "(dd:hh:mm:ss)";

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	if (!$("#section").val())
		$("#section").val(defaultFilterText);
	$("#section").focus(function() {
		if($(this).val() == defaultFilterText)
			$(this).val("");
	});
	$("#section").blur(function() {
		if($(this).val() == "")
			$(this).val(defaultFilterText);
	});
	/*
	$("#section").change(function() {
		if (mapExportsAllStats)
			generateOverlays();
	});
	*/
	$("#section").keyup(function() {
		if (exportsStats)
			populateExportsTable(exportsStats);
	});
	
	$("#filter").click(function() {
		generatePage();
	});
	
	generatePage();
}

function generatePage() {
	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var req = new ReportRequest(config.restHost + restEndpoint,
			"json",
			config.restHost + restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);

	req.sendSingleAsyncRequest(pValues, populateExportsTable);	
	
} // end of generateCharts()

function populateExportsTable(resultData) {
	$("#content-body").empty();
	
	if (resultData)
		exportsStats = resultData;
	
	var title = "Total exports between "
		+ new Date(config.dateInputFormat.parse($("input#date-from").val())).toLocaleDateString() 
		+ " - " 
		+ new Date(config.dateInputFormat.parse($("input#date-to").val())).toLocaleDateString();
	var user = $("input#user").val();
	var sectionRegExp = new RegExp($("#section").val(), "i");
	
	$("#content-body").append(
		$("<table>")
			.addClass("title-only")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.attr("colspan", 11)
							.text(function() {
								return (user) ? title + " for user " + user : title;
							})
					)
			)
	);
			
	var table = $("<table>").addClass("tablesorter"); 
	
	$("<thead>").append(
		$("<tr>")
			.append(
				$("<th>").text("")
			)
			.append(
				$("<th>").text("User")
			)
			.append(
				$("<th>").html("Total Time<br/>" + dateTimeTip)
			)
			.append(
				$("<th>").text("Started")
			)
			.append(
				$("<th>").text("Export Type")
			)
			.append(
				$("<th>").text("Success")
			)
			.append(
				$("<th>").text("Tools Version")
			)
			.append(
				$("<th>").text("Sections Exported")
			)
			.append(
				$("<th>").text("Export Time")
			)
	)
	.appendTo(table);
	
	var tbody = $("<tbody>");
	
	$.each(exportsStats, function(i, exportStat) {				
		var row = 
			$("<tr>")
				.append(
					$("<td>")
						.text("+")
						.addClass("hand")
						.attr("title", "Click to expand")
						.click(function() {showMoreInfo($(this), exportStat);})
				)
				.append(
					$("<td>").text(exportStat.Username)
				)
				.append(
					$("<td>").text(formatSecsWithDays(exportStat.TotalTime))
				)
				.append(
					$("<td>")
						.html(function() {
							var d = parseJsonDate(exportStat.Start);
							return (d.toLocaleDateString() + "<br /> " + d.toLocaleTimeString())
							//return d.toLocaleString();
						})
				)
				.append(
					$("<td>").text(exportStat.ExportType)
				)
				.append(
					$("<td>").text(exportStat.Success)
				)
				.append(
					$("<td>").text(exportStat.ToolsVersion)
				)
				.append(
					$("<td>").text(exportStat.SectionsExported.join(", "))
				)
				.append(
					$("<td>")
						.text(function () {
							var exportTime = 0;

							var regexp = new RegExp("section export$");
							$.each(exportStat.SubSteps, function(i, subStep) {
								if (!regexp.test(subStep.Name))
									return;
							
								//exportTime += datesDiffInSecs(parseJsonDate(subStep.End), parseJsonDate(subStep.Start))
								exportTime += subStep.Time;
							});
						
							return formatSecsWithDays(exportTime);						
						})
				);
			
		// This is for filtering out the non matched sections
		var sectionExpMatched = false;
		exportStat.SectionsExported.map(function(section) {if (sectionRegExp.test(section)) sectionExpMatched = true});
		// If it has matched at least one section add the row
		if (sectionExpMatched)
			row.appendTo(tbody);
		
	});	
	
	tbody.appendTo(table);
	
	if (tbody.find("tr").length > 0) { // show the table if rows have been added	
		// Add the tablesorter plugin
		table.tablesorter({
			// Sort on the second column ASC
			sortList: [[3, 0]]
		});
		
		// Remove the more info rows on sorting click events - they destroy the sorting 
		table.find("th").bind("click", function() {
			removeAllInfoRows($(this).parent().parent().parent());
		});
	
		table.appendTo("#content-body");
	}
}

function removeAllInfoRows(table) {
	table.find("tr.details-row").remove();
	table.find("td.hand").text("+").attr("title", "Click to Expand");
}

function showMoreInfo(element, exportStat) {
	var rowElement = element.parent();
	
	// If the next row has a table it need to be removed
	if (rowElement.next().hasClass("details-row")) {
		rowElement.next().remove();
		element.text("+")
		element.attr("title", "Click to Expand")
	}
	else {
		var table = $("<table>")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.text("Export Details: ")
							.attr("colspan", 2)
							.addClass("title")
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("User Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.Username)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Export Type: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ExportType)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Export Number: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ExportNumber)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Success: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.Success)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Tools Version: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ToolsVersion)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Platforms Enabled: ").addClass("label")
					)
					.append(
						$("<td>").text(
							exportStat.PlatformsEnabled.map(
								function(d) {return platformNames[d].Name; }
							).join(", ")
						)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Machine Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.MachineName)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Processor Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ProcessorName)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Processor Cores: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ProcessorCores)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Installed Memory: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.InstalledMemory)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Private Bytes Start: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.PrivateBytesStart)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Private Bytes End: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.PrivateBytesEnd)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Enabled: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEEnabled)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Force CPU Count: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEForceCPUCount)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Standalone: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEStandalone)
					)
			);
		
		populateSubsteps(exportStat.SubSteps, table);
		
		var newRow = $("<tr>")
			.addClass("details-row")
			.append(
				$("<td>")
					.attr("colspan", 9)
					.append(
						//$("<fieldset>").append(table)
						table
					)
			);
		
		rowElement.after(newRow);
		element.text("-")
		element.attr("title", "Click to Collapse")
	}
	
	function populateSubsteps(subSteps, element) {
		if (subSteps.length < 1)
			return
		
		var table = $("<table>")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.text("Sub Steps: ")
							.attr("colspan", 2)
					)
			);
			
		$.each(subSteps, function(i, subStep) {
			table
				.append(
					$("<tr>")
						.append(
							$("<td>")
								.addClass("label")
								.text(subStep.Name + " " + dateTimeTip + ":")
						)
						.append(
							$("<td>").text(formatSecsWithDays(subStep.Time))
						)
				);
			
			populateSubsteps(subStep.SubSteps, table);
		});
			
		element.append(
			$("<tr>")
				.append(
					$("<td>")
						.attr("colspan", 2)
						.append(
							//$("<fieldset>").append(table)
							table
						)
				)
		);
	} // End of populateSubsteps
	
} // End of showMoreInfo

/*
function convertExports(xml) {
	var exports = [];
	
	
	$(xml).find("MapExportStatBundle").each(function() {
		var exportStatElement = $(this).find("ExportStat");
		var exportStat = {
			Id: exportStatElement.find("Id").text(),
			CreatedOn: exportStatElement.find("CreatedOn").text(),
			Start: exportStatElement.find("Start").text(),
			End: exportStatElement.find("End").text(),
			Success: exportStatElement.find("Success").text(),
			Username: exportStatElement.find("Username").text(),
			MachineName: exportStatElement.find("MachineName").text(),
			ExportType: exportStatElement.find("ExportType").text(),
			ToolsVersion: exportStatElement.find("ToolsVersion").text(),
			UsingAP3: exportStatElement.find("UsingAP3").text(),
			ExportNumber: exportStatElement.find("ExportNumber").text(),
		};
		
		var mapExportStats = [];
		// MapExportStats has SectionExportStatBundle elements
		$(this).find("MapExportStats").children("SectionExportStatBundle").each(function() {
			var sectionExportStatBundle = {
				Id: $(this).children("Id").text(),
				CreatedOn: $(this).children("CreatedOn").text(),
				Start: $(this).children("Start").text(),
				End: $(this).children("End").text(),
				Success: $(this).children("Success").text(),
				MapSectionIdentifier: $(this).children("MapSectionIdentifier").text(),
				//SubStats: $(this).find("SubStats").text(),
			};
			mapExportStats.push(sectionExportStatBundle);
		});
		
		var imageBuildStatElement = $(this).find("ImageBuildStat");
		
		var mapSectionIdentifiers = [];
		imageBuildStatElement.find("MapSectionIdentifiers").find("MapSectionIdentifier").each(function() {
			mapSectionIdentifiers.push($(this).text());
		});
		var imageBuildStat = {
			Id: imageBuildStatElement.find("Id").text(),
			CreatedOn: imageBuildStatElement.find("CreatedOn").text(),
			Start: imageBuildStatElement.find("Start").text(),
			End: imageBuildStatElement.find("End").text(),
			Success: imageBuildStatElement.find("Success").text(),
			MapSectionIdentifiers: mapSectionIdentifiers,
		};
		//console.log(mapExportStats);
		exports.push({
			ExportStat: exportStat,
			MapExportStats: mapExportStats,
			ImageBuildStat: imageBuildStat,
		});
	});
	
	return exports;
}

*/
