var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	false, // build
		false, // platform
		false, // user
	],
};

var reportOptions = {
	processFunction: groupVehicleCommentsByUser,
	restEndpoint: config.playthroughsComments,
	restEndpointAsync: config.playthroughsCommentsAsync,
	
	legendText: ""
		+ "Comment Report"
		+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
		+ "Vehicle Comments",

	// No report summary info
	reportSummaryTitle: false,
	
	// No nested array data
	getReportArray: function(d) { return d; },
	reportArrayItems: [
	    {title: "Username", getValue: function(d) {return d.Username;} },
	    {title: "Total Comments", getValue: function(d) {return d.Comments.length} },
	],
	
	/* Groups */
	groups: [
	    {title: "Vehicle Comments", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	hasReportArrayItemMoreInfo: true,
	
	// No nested array data
	getReportMoreInfoArray: function(d) {return d},
	// Generate one table for all the results
	hasMoreInfoMultipleTables: false,
	getMoreInfoKey: function(d) {
		return d.Username;
	},
	// get the extra info data from the array
	getMoreInfoValues: function(d) {return d.Comments},
	reportMoreInfoArrayItems: [
	    //{title: "Username", getValue: function(d) {return d.Username;} },
	    {title: "TimeStamp", getValue: function(d) {return parseJsonDate(d.Timestamp).toLocaleString();} },
	    {title: "Vehicle Name", getValue: function(d) {return d.VehicleFriendlyName;} },
	    {title: "In Game Name", getValue: function(d) {return d.VehicleGameName;} },
	    {title: "Comment", getValue: function(d) {return d.Comment;} },
   	],	
	
};


function groupVehicleCommentsByUser(data) {
	var grouped = {};
		
	$.each(data, function(i, d) {
		if ((typeof d.VehicleFriendlyName !== "undefined")) {
		
			if (grouped.hasOwnProperty(d.Username)) {
				grouped[d.Username].Comments.push(d);
			}
			else
				grouped[d.Username] = {
					Username: d.Username,
					Comments: [d],
				};
		}
	});
	return grouped;
}
