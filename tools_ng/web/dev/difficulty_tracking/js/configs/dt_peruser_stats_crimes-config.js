var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
	  	true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "KILLS_COP",
    	title: "Cops killed",
    	getValue: getCharValue,
    },
    {
    	name: "KILLS_SWAT",
    	title: "NOOSE killed",
    	getValue: getCharValue,
    },
    {
    	name: "NO_TIMES_WANTED_LEVEL",
    	title: "Times Wanted",
    	getValue: getCharValue,
    },
    {
    	name: "BUSTED",
    	title: "Times Busted",
    	getValue: getCharValue,
    },
    {
    	name: "STARS_ATTAINED",
    	title: "Wanted stars attained",
    	getValue: getCharValue,
    },
    {
    	name: "STARS_EVADED",
    	title: "Wanted stars evaded",
    	getValue: getCharValue,
    },
    {
    	name: "LAST_CHASE_TIME",
    	title: "Last chase duration",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "LONGEST_CHASE_TIME",
    	title: "Longest chase duration",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "TOTAL_TIME_MAX_STARS",
    	title: "Time spent with a 5 star Wanted Level",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "DB_KILLS",
    	title: "Drive-by kills as driver",
    	getValue: getCharValue,
    },
    {
    	name: "PASS_DB_KILLS",
    	title: "Drive-by kills as passenger",
    	getValue: getCharValue,
    },
    {
    	name: "TIRES_POPPED_BY_GUNSHOT",
    	title: "Tires shot out",
    	getValue: getCharValue,
    },
    {
    	name: "HIGHEST_SKITTLES",
    	title: "Peds run over",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_CARS",
    	title: "Cars stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_BIKES",
    	title: "Motorcycles stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_HELIS",
    	title: "Helicopters stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_PLANES",
    	title: "Planes stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_BOATS",
    	title: "Boats stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_QUADBIKES",
    	title: "ATVs stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_BICYCLES",
    	title: "Bicycles stolen",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_STOLEN_COP_VEHICLE",
    	title: "Cop vehicles stolen",
    	getValue: getCharValue,
    },
    {
    	name: "KILLS_SINCE_LAST_CHECKPOINT",
    	title: "Number of kills since last checkpoint",
    	getValue: getCharValue,
    },
    {
    	name: "KILLS_SINCE_SAFEHOUSE_VISIT",
    	title: "Number of kills since safehouse visit",
    	getValue: getCharValue,
    },   
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Crimes",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	
	hasExtraRestParams: [
	{
		key: "StatNames",
		value: getStatNames,
	},
	],
		
	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],	
				
	reportArrayItems: profileStatList.map(
				function(d) {
					d["requestSubString"] = "_";
					return d;
				}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														//d.name = d.title;
														return d;});
