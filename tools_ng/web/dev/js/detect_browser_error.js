// Had to create and load this file before any javascript library loading (except jquery)
// Some libraries cause very old browsers to crash and never display any error message

// Requires jquery to be loaded first, jquery is safe and supports the majority of old browsers

// Objects are not supported by old browsers ??
/*
var error = {
	msMinVersion : 9,
	mozMinVersion : 4,
	page: "/error.html",
};
*/

var msMinVersion = 9;
var	mozMinVersion = 4;
var	page = "/error.html";


if (($.browser.msie && (parseInt($.browser.version) < msMinVersion) || document.documentMode < msMinVersion) // IE
	|| ($.browser.mozilla && (parseInt($.browser.version) < mozMinVersion)) // FF
	|| (!$.browser.msie && !$.browser.mozilla && !$.browser.webkit)) {
	
	window.location = page;
}

	