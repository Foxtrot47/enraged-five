function initPage() {
	initHeaderAndFilters(null);
	
	populateUsers();
	
	$("#new-top-group").click(function() {
		addGroup(); 
	});
	$("#new-sub-group").click(function() {
		addSubGroup(); 
	});
	$("#delete-sub-group").click(function() {
		deleteSubGroup(); 
	});
	$("#delete-sub-group").attr("disabled", true);
	
	populateGroups();

}

function populateUsers() {
	// Get All the Users
	var object = {
		url: config.restHost + config.SCUsers,
	  	data: {},
	  	method: "GET",
	  	dataType: "json",
	  	async: false,
	}
	var df = {
		getValue: function(d) {return d.Id},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	}
	
	block();
	var users = getAjaxData(object);
	populateSelectElements(["all-users"], users, df);
	unBlock();
	
	$("#users-top").append(
		$("<button>").text("Add ->")
			.attr("title", "Add Selected Users to Group")
			.addClass("right")
			.click(function(e) {
				addUsersToGroup($("#all-users").val()); 
			})
		)
}

function populateGroups() {
	$("#groups").empty();
	
	var object = {
		url: config.restHost + config.SCHierarchies,
	  	data: {},
	  	method: "GET",
	  	dataType: "json",
	  	async: false,
	};
	var df = {
		getValue: function(d) {return d.Id},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
		getChildren: function(d) {return d.ChildGroups},
		onClick: function(id, text) {
			populateGroupUsers(id, text)
		}
	};
	
	block();
	var groups = getAjaxData(object);
	populateHierarchicalElements($("#groups"), groups, df);
	unBlock();
}

function addGroup() {
	var newGroupName = $("#new-group-text").val();
	if (!newGroupName)
		return;
	
	block();
	$.ajax({
		url: config.restHost + config.SCGroups,
		type: "PUT",
		data: JSON.stringify({"Name": newGroupName}),
		dataType: "json",
		contentType: "application/json",
		async: "true",
			
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
}

function addSubGroup() {
	var newGroupName = $("#new-group-text").val();
	if (!newGroupName)
		return;
	
	var selectedGroupId = $("#group-selected-id").text();
	if (!selectedGroupId)
		return;
	
	block();
	$.ajax({
		url: config.restHost + config.SCGroups + "/" + selectedGroupId + "/Groups",
		type: "PUT",
		data: JSON.stringify({"Name": newGroupName}),
		dataType: "json",
		contentType: "application/json",
		async: "true",
			
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
}

function deleteSubGroup() {

	var selectedGroupId = $("#group-selected-id").text();
	if (!selectedGroupId)
		return;
	
	block();
	$.ajax({
		url: config.restHost + config.SCGroups + "/" + selectedGroupId + "/Groups",
		type: "DELETE",
		data: {},
		dataType: "json",
		contentType: "application/json",
		async: "true",
			
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			//console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
}

function addUsersToGroup(users) {
	//if (users.length == 0)
	if (!users)
		return;
	
	var selectedGroupId = $("#group-selected-id").text();
	if (!selectedGroupId)
		return;
	
	block();
	$.each(users, function(i, user) {
		$.ajax({
			url: config.restHost + config.SCGroups + "/" + selectedGroupId + "/Users/" + user,
			type: "PUT",
			data: {},
			dataType: "json",
			contentType: "application/json",
			async: "true",
				
			success: function(data, textStatus, jqXHR) {
				//console.log("added");			
			},
			error: function (xhr, ajaxOptions, thrownError) {
				//console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
			},
		});
	});
	unBlock();
	
	populateGroupUsers($("#group-selected-id").text(), $("#group-selected-name").text());
}

function populateGroupUsers(groupID, groupName) {
	$("#group-selected-id").text(groupID);
	$("#group-selected-name").text(groupName);
	$("#group-head button").remove();
	$("#group-users").empty();
	
	var restobj = {
		  url: config.restHost + config.SCGroups + "/" + groupID + "/Users",
		  data: {recursive: true},
		  method: "GET",
		  dataType: "json",
		  async: false,
	};
	var df = {
		getValue: function(d) {return d.Id},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	block();
	var groupUsers = getAjaxData(restobj);
	populateSelectElements(["group-users"], groupUsers, df);
	unBlock();
	
	$("#group-head").append(
			$("<button>").text("< - Remove")
				.attr("title", "Remove Selected Users From Group")
				//.addClass("right")
				.click(function(e) {
					deleteUsersFromGroup($("#group-users").val()); 
				})
		)
}

function deleteUsersFromGroup(users) {
	//if (users.length == 0)
	if (!users)
		return;
	
	var selectedGroupId = $("#group-selected-id").text();
	if (!selectedGroupId)
		return;
	
	block();
	$.each(users, function(i, user) {
		$.ajax({
			url: config.restHost + config.SCGroups + "/" + selectedGroupId + "/Users/" + user,
			type: "DELETE",
			data: {},
			dataType: "json",
			contentType: "application/json",
			async: "true",
				
			success: function(data, textStatus, jqXHR) {
				//console.log("added");
			},
			error: function (xhr, ajaxOptions, thrownError) {
				//console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
			},
		});
	});
	unBlock();
	
	populateGroupUsers($("#group-selected-id").text(), $("#group-selected-name").text());
}
