var usersPath = "/cache/users/",
	usersXml = "_users.xml",
	usersInfo = {};

function processUsersInfo() {
	$.ajax({
		url: usersPath + usersXml,
		type: "GET",
		dataType: "xml",
		
		success: function(xml, textStatus, jqXHR) {
			
			$(xml).find("User").each(function() {
				
				usersInfo[$(this).find("UserName").text().toLowerCase()] = {
					Name: $(this).find("Name").text(),
					Email: $(this).find("Email").text(),
					JobTitle: $(this).find("JobTitle").text(),
					//ImageFilename: stripImagePath($(this).find("ImageFilename").text()),
					ImageFilename: $(this).find("ImageFilename").text(),
				}
				
			});
			
    	},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error("Failed loading content");
		},
		complete: function() {
			//console.log(usersInfo);
		}
	});
}

// Deprecated
function stripImagePath(imageFullPath) {
	return imageFullPath.substr(imageFullPath.lastIndexOf("\\") + 1);
}

function createUserInfoDiv(username, element) {
	var user = usersInfo[username.toLowerCase()];
	
	if (typeof user !== "undefined") {
		$("<div />")
			.addClass("user")
			.append(
					$("<img />").attr("src", usersPath + user.ImageFilename)
			)
			.append(
					$("<div />")
					.addClass("user-info")
					.append(
						$("<div />")
							.addClass("user-info-name")
							.html(user.Name.replace(" (", "<br />("))
					)
					.append(
						$("<div />")
							.addClass("user-info-rest")
							.html(user.Email + "<br />" + user.JobTitle)
					)	
			)
			.appendTo(element);
	}
	else
		$(element).attr("title", username.toLowerCase());
	
}
