var styleSheets, parameters, reportType;

var headerAndFilters  = {
		headerType: "header-reports", 	// reports filtering header
		disabledFields: [ 			// disabled header fields
		                 false, // parameters
		                 false, // stylesheet
	    ],
	};

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	repopulateNavigationWithReports();
	
	$("#navigation li a").click(function() {
		generateFilters();
	});
	
	$("#filter").click(function() {
		generateReport();
	});

}

function generateFilters() {
	$("#report").empty();
	reportType = $("#navigation a.active").attr("href").substr(1); // remove the #
	
	block();
	$.ajax({
		url: config.restHost + config.reportsPrefix + reportType + config.reportsStyleSuffix,
		type: "GET",
		dataType: "json",
		data: {},
		async: false,
		success: function(json, textStatus, jqXHR) {
			styleSheets = json;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			//unBlock();
			$("#stylesheet").empty();
			$.each(styleSheets, function(i, styleSheet) {
				$("#stylesheet").append(
						$("<option />")
							.text(styleSheet)
							.val(styleSheet)
					);
			});
		}
	});
	
	$.ajax({
		url: config.restHost + config.reportsPrefix + reportType + config.reportsSuffix,
		type: "GET",
		dataType: "json",
		data: {},
		success: function(json, textStatus, jqXHR) {
			parameters = json;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			unBlock();
			
			$("#parameters-field fieldset").remove();
			$.each(parameters, function(i, parameter) {
				//console.log(parameter);
				appendFilter(parameter);
			});
		}
	});
}

function appendFilter(parameter) {
	if ((parameter.Type == "list<string>") || (parameter.Type == "string")) {
		$("#parameters-field").append(
			$("<fieldset />").attr("id", parameter.Name)
				.css("width", "17%")
				.css("height", "75%")
				.append(
					$("<legend />").text(function() {
						return (parameter.Required) ? parameter.Name + " *" : parameter.Name
					})
				)
				.append(
					$("<input />").addClass("no-label")
				)
			);
	}
	else if (parameter.Type == "bool") {
		$("#parameters-field").append(
				$("<fieldset />").attr("id", parameter.Name)
					.css("width", "17%")
					.css("height", "75%")
					.append(
						$("<legend />").text(function() {
							return (parameter.Required) ? parameter.Name + " *" : parameter.Name
						})
					)
					.append(
						$("<select />").addClass("single")
						.append(
							$("<option />")
								.text("True")
								.val("True")
						)
						.append(
							$("<option />")
								.text("False")
								.val("False")
						)
					)
				);
	}
	else if (parameter.Type == "datetime") {
		$("#parameters-field").append(
				$("<fieldset />").attr("id", parameter.Name)
					.css("width", "17%")
					.css("height", "75%")
					.append(
						$("<legend />").text(function() {
							return (parameter.Required) ? parameter.Name + " *" : parameter.Name
						})
					)
					.append(
						$("<input />").addClass("noLabel")
							.datepicker({
								dateFormat: config.datePickerFormat,
							})
					)
				);
		
	}
	
}

function generateReport() {
	var styleSheet = $("#stylesheet").val();
	
	if (!styleSheets) {
		Sexy.alert("Please select a report type first");
		return;
	}
	else if (!styleSheet) {
		Sexy.alert("No stylesheets available, cannot render this report");
		return;
	}
	$("#report").empty();
	updateParameters();
	
	var xmlDoc, xslDoc;
	block();
	$.ajax({
		url: config.restHost + config.reportsPrefix + reportType + config.reportsSuffix,
		type: "POST",
		dataType: "xml",
		contentType: "application/json",
		data: JSON.stringify(parameters),
		success: function(xml, textStatus, jqXHR) {
			
			$.ajax({
				url: config.restHost + config.reportsPrefix + reportType + config.reportsStyleSuffix + "/" + styleSheet,
				type: "GET",
				dataType: "xml",
				data: {},
				cache: false,
				success: function(xsl, textStatus, jqXHR) {
					//console.log($(xml);
					// Do the xml transformation
					if (window.ActiveXObject) { // IE
						ex = xml.transformNode(xsl);
						document.getElementById("report").innerHTML = ex;
					}
					else if (document.implementation && document.implementation.createDocument) { // Mozilla/Chrome etc
						var xsltProcessor = new XSLTProcessor();
						xsltProcessor.importStylesheet(xsl);
						var resultDocument = xsltProcessor.transformToFragment(xml, document);
						document.getElementById("report").appendChild(resultDocument);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
				}
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			unBlock();
		}
	});
	
	
	
	
	
}

function updateParameters() {
	$.each(parameters, function(i, parameter) {
		var val = $("#" + parameter.Name + " input").val() || $("#" + parameter.Name + " select").val();
		if (val) {
			if (parameter.Type == "datetime") {
				parameter.Value = new Date(config.dateInputFormat.parse(val)).toISOString();
			}
			else
				parameter.Value = val;
		}
	});
}

function repopulateNavigationWithReports() {
	block();
	$.ajax({
		url: config.restHost + config.reportsAll,
		type: "GET",
		dataType: "json",
		data: {},
		success: function(jsonReports, textStatus, jqXHR) {
			
			$.each(jsonReports, function(i, report) {
				navigationItems.push(
					{
						title: (i+1) + ". " + report,
						href: "#" + report,
						display: "inherit",
						hide: false,
						config: null,
						subItems: [],
					}
				);
			});
			
			removeNavigation();
			populateNavigation(navigationItems);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			$("#navigation li a").each(function() {		
				$(this).click(function(e) {
					$("#navigation li").removeClass("expanded");
					$("#navigation a").removeClass("active");
					$(this).addClass("active");

					e.preventDefault();
					generateFilters();
				});
			});
			
			unBlock();
		}
	});
			
}