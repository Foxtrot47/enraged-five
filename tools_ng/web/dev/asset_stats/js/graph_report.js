var builds,
	graphData;

var graphId = "graph";
var filterInputId = "filter-input";

var defaultFilterText = "type text to filter the chart";

var typewatch = function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    }
}();

function initPage() {
	initHeaderAndFilters(headerAndFilters);
	
	if (reportOptions.hasFilterInput) {
		$("#content-description").append(
			$("<input />")
				.attr("id", filterInputId)
				.attr("type", "text")
				.val(defaultFilterText)
				.focus(function() {
					if($(this).val() == defaultFilterText)
						$(this).val("");
				})
				.blur(function() {
					if ($(this).val() == "")
						$(this).val(defaultFilterText);
				})
				.keyup(function() {
					if (graphData)
						typewatch(drawGraph, 500);
				})
		);
	}
	
	$("#content-body")
		.append(
			$("<div>")
				.attr("id", graphId)
				.addClass("single-graph")
		);
	
	initSvg(graphId);
	
	// function from generic.js, variable from config file
	
	
	builds = getAllBuilds().splice(-reportOptions.graphHistory);
	//console.log(builds.length);
	
	/*
	$("#chart-filter label").text(reportOptions.inputTextLabel);
	
	$("#chart-filter-input").val(reportOptions.defaultInputText);
	$("#chart-filter-input").focus(function() {
		if($(this).val() == reportOptions.defaultInputText)
			$(this).val("");
	});
	$("#chart-filter-input").blur(function() {
		if($(this).val() == "")
			$(this).val(reportOptions.defaultInputText);
	});
	// IE fix for not firing the change event when pressing enter
	if ($.browser.msie) {
		$("#chart-filter-input").live("keypress", function(e) {
			if (e.keyCode == 13)
				if (graphData)
					drawGraph();
			});
	}
	else {
		$("#chart-filter-input").change(function() {
			//alert("changed");
			if (graphData)
				drawGraph();
		});
	}
	*/
	
	$("#filter").click(function() {
		generateGraph();
	});
	
	generateGraph();	
}

function generateGraph() {
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
	req.sendSingleAsyncRequest(pValues, drawGraph);
}

// OVERENGINEERED ???
function drawGraph(response) {
	
	if (response)
		graphData = response;
	//console.log(graphData.length);
	
	// We will plot only the first group, the rest will be filtered out
	var filterGroup = reportOptions.groups[reportOptions.graphGroupId];
	var groupRegExp = new RegExp(filterGroup.regexp, "i");
	// The metric from the returned object to use for filtering, usually this is the name
	var filterMetric = reportOptions.reportArrayItems[filterGroup.filterItem];
	
	var filterInputText = $("#" + filterInputId).val();
	if (filterInputText == defaultFilterText)
		filterInputText = "";
		
	if (filterInputText.length < reportOptions.graphMinChars)
		return;
	
	// regular expression for matching the inout text
	var textRegExp = new RegExp(filterInputText, "i");
	
	var graphArray = [];
	$.each(graphData, function(i, element) {
		
		if (!groupRegExp.test(filterMetric.getValue(element))
			|| !textRegExp.test(filterMetric.getValue(element)))
				return;
		// deep copy the element cause splice will alter it
		var seriesValues = reportOptions.graphValuesArray($.extend(true, {}, element), builds);
		if (seriesValues.length < 1)
			return;
		
		// This is for converting our data to a format that NVD3 understands
		graphArray.push({
			key: filterMetric.getValue(element),
			values: seriesValues,
			color: config.colourRange(Math.random()*20), 
		});
		
	});

	drawLinegraph(graphArray, builds, graphId, reportOptions.graphObject);
}

// Copied from Capture Stats Report (slightly modified)
function drawLinegraph(datum, tickValues, htmlId, graphObject) {
	if (datum.length < 1) {
		d3.select("#" + htmlId + " svg g.nvd3").remove();
		Sexy.alert(config.noDataText);
		return;
	}
	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
			.x(function(d) { return graphObject.getKey(d); })
			.y(function(d) { return graphObject.getValue(d); })
			.margin({top: 50, right: 40, bottom: 100, left: graphObject.leftMargin})
			.tooltips(true);
		
		chart.xAxis
			.axisLabel(graphObject.xLabel)
			.tickValues(d3.range(tickValues.length))
			.tickFormat(function(d, i) {
				return (tickValues[parseInt(d)] ? tickValues[parseInt(d)] : d);
			})
			.rotateLabels(-45);
			
		chart.yAxis
			.axisLabel(graphObject.yLabel)
			.tickFormat(d3.format(".02f"));
	
		d3.select("#" + htmlId + " svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
	 
		nv.utils.windowResize(chart.update);
	 
		return chart;
	});
}
