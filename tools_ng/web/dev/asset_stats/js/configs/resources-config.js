//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-rs", 	// resource stats filtering header
	disabledFields:				// disabled header fields 
		[
		 false, // platform
		 false, // file-type
		 false, // resource names
		 true, // detailed - not used
		 false, // dates 
		 ],
};

var fixed = function(d) {return d.toFixed(2); }
var bucketNames = ["Physical", "Virtual"];
var fileTypes = getFileTypes().map(function(d) {return d.Name}); 
//console.log(fileTypes);

var reportOptions = {
	restEndpoint: config.newResourceStats,
	restEndpointAsync: config.newResourceStatsAsync,
		
	enableCSVExport: true,

	/*
	reportSummaryTitle: "Resource Stats Summary in KB",
	reportSummaryItems: [
	      {title: "Total Physical Capacity", getValue: function(d) {return fixed(d.TotalPhysicalCapacity/1024);} },
	      {title: "Total Physical Size", getValue: function(d) {return fixed(d.TotalPhysicalSize/1024);} },
	      {title: "Total Virtual Capacity", getValue: function(d) {return fixed(d.TotalVirtualCapacity/1024);} },
	      {title: "Total Virtual Size", getValue: function(d) {return fixed(d.TotalVirtualSize/1024);} },
	 ],
	*/
	
	// no nested array in new reports
	getReportArray: function(d) {return d},
	reportArrayItems: [
	    {title: "ID", getValue: function(d) {return d.Id;} },
	    {title: "Resource Name", getValue: function(d) {return d.ResourceName;} },
	    {title: "File Type", getValue: function(d) {return fileTypes[d.FileType];} },
	    {title: "Physical Capacity (KB)", getValue: function(d) {return fixed(d.PhysicalCapacity/1024);} },
	    {title: "Physical Size (KB)", getValue: function(d) {return fixed(d.PhysicalUsed/1024);} },
	    {title: "Physical Loss (KB)", getValue: function(d) {return fixed((d.PhysicalCapacity-d.PhysicalUsed)/1024);} },
	    {
	    	title: "Physical Loss Fragmentation (%)", 
	    	getValue: function(d) {
	    		var loss = d.PhysicalCapacity-d.PhysicalUsed;
	    		return fixed((d.PhysicalCapacity) ? (loss/d.PhysicalCapacity)*100 : 0);
	    	}
	    },
	    {title: "Virtual Capacity (KB)", getValue: function(d) {return fixed(d.VirtualCapacity/1024);} },
	    {title: "Virtual Size (KB)", getValue: function(d) {return fixed(d.VirtualUsed/1024);} },
	    {title: "Virtual Loss (KB)", getValue: function(d) {return fixed((d.VirtualCapacity-d.VirtualUsed)/1024);} },
	    {
	    	title: "Virtual Loss Fragmentation (%)", 
	    	getValue: function(d) {
	    		var loss = d.VirtualCapacity-d.VirtualUsed; 
	    	   	return fixed((d.VirtualCapacity) ? (loss/d.VirtualCapacity)*100 : 0);
	    	}
	    },
	],
	//reportArraySort: [[1, 0]], // [second item, ASC]
	
	hasReportArrayItemMoreInfo: true,
	// if the more info need to come via an extra rest call
	reportArrayItemMoreInfoRest: {
		restEndpoint: config.newResourceDetailsStats,
		restEndpointAsync: config.newResourceDetailsStatsAsync,
		extraParamName: "ResourceStatId",
	},
	
	// No nested results 
	getReportMoreInfoArray: function(d) {return d},
	// Generate one table for all the results
	hasMoreInfoMultipleTables: false,
	getMoreInfoKey: function(d) {return "Details"},
	getMoreInfoValues: function(d) {return d},
	reportMoreInfoArrayItems: [
	    {title: "Bucket Type", getValue: function(d) {return bucketNames[d.BucketType];} },
	    {title: "Bucket ID", getValue: function(d) {return d.ID;} },
	    {title: "Capacity (KB)", getValue: function(d) {return fixed(d.Capacity/1024);} },
	    {title: "Size (KB)", getValue: function(d) {return fixed(d.Size/1024);} },
	    {title: "Loss (KB)", getValue: function(d) {return fixed((d.Capacity-d.Size)/1024);} },
	    {
	    	title: "Loss Fragmentation (%)", 
	    	getValue: function(d) {
	    		var loss = d.Capacity-d.Size; 
	    	   	return fixed((d.Capacity) ? (loss/d.Capacity)*100 : 0);
	    	}
	    },
   	],
   	reportMoreInfoArraySort: [[0, 0]], // [first item, ASC]
	
	groups: [
	    {title: "Resource Stats List", regexp: "", filterItem: 0},
	],
	
	/*
	// Graph only related vars
	*/
};

