// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
{
	types: [
	"_PROFILE_SETTING_300", // AUDIO_SFX_LEVEL = 300,
	],
	
	requestSubString: "_",
	name: "SFX Volume",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: 10,
	showOnlyLowerRange: true,
	validRange: [0, 10],
	orderDescBarchart: true,
},
{
	types: [
	"_PROFILE_SETTING_301", // AUDIO_MUSIC_LEVEL = 301,
	],

	requestSubString: "_",
	name: "Music Volume",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: 10,
	showOnlyLowerRange: true,
	validRange: [0, 10],
	orderDescBarchart: true,
},
{
	types: [
	"_PROFILE_SETTING_308", // AUDIO_DIALOGUE_BOOST = 308,
	],

	requestSubString: "_",
	name: "Dialog Boost",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: 10,
	showOnlyLowerRange: true,
	validRange: [0, 10],
	orderDescBarchart: true,
},
{
	types: [
	"_PROFILE_SETTING_307", // AUDIO_INTERACTIVE_MUSIC = 307,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Score",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_305", // AUDIO_SPEAKER_OUTPUT = 305,
	],
	altNames:[
	   "TV", //MO_TVOUTP
	   "Speakers", //MO_SPEAKERS
	   "Headphones", //MO_SHEAD
	],
	requestSubString: "_",
	name: "Output",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_302", // AUDIO_VOICE_OUTPUT = 302,
	],
	altNames:[
	   "Fade Radio Volume", // MO_VFRV
	   "Retain Radio Volume", // MO_VRRV
	],
	requestSubString: "_",
	name: "Voice Chat",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_309", // AUDIO_VOICE_SPEAKERS = 309,
	],
	altNames:[
   	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
   	],
	requestSubString: "_",
	name: "Voice Through Speakers",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},

];

var reportOptions = {	
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
			
	availableCharts: profileStatList.map(function(d) {
		d["hideStartDate"] = true;
		return d;
	}),
}