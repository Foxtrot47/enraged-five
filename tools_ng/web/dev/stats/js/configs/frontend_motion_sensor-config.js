// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
/*
// Autosave
{
	types: [
	"_PROFILE_SETTING_208", // DISPLAY_AUTOSAVE_MODE = 208,
	],

	requestSubString: "_",
	name: "Autosave",
	description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
*/

// SIXAXIS TM
{
	types: [
	"_PROFILE_SETTING_5", // CONTROLLER_SIXAXIS_HELI = 5,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Helicopter",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_6", // CONTROLLER_SIXAXIS_BIKE = 6,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Bike",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_7", // CONTROLLER_SIXAXIS_BOAT = 7,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Boat",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_8", // CONTROLLER_SIXAXIS_RELOAD = 8,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Reload",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},

/*
// Facebook

{
	types: [
	"_PROFILE_SETTING_900", // FACEBOOK_UPDATES = 900
	],

	requestSubString: "_",
	name: "Facebook",
	description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
*/
];

var reportOptions = {	
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
			
	availableCharts: profileStatList.map(function(d) {
		d["hideStartDate"] = true;
		return d;
	}),
}