// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var currentStat = "Purchases";

var reportOptions = {
	restEndpoint: config.shoppingStats,
	restEndpointAsync: config.shoppingStatsAsync,
	
	processFunction: groupShoppingItems,
	
	hasExtraRestParams: [
	{
		key: "GroupByType",
	    value: false,
	},
	],
	
	hasFilterInput: true,
	
	//enablePNGExport: "content-description-controls",
	enableCSVExport: "content-description-controls",
	
	chartGroups: {
		getGroups: function(d) {return [d]}, // One group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
		title: currentStat,
		colour: config.chartColour1,
	    getName: function(d) {return d.ItemName + " (" + d.ShopName + " )"},
	    getValue: function(d) {return d.TimesPurchased;},
	    getObject: function(d) {return d;},
	},
	],
	
	tooltipContent: function(key, x, y, e, graph) {
		//console.log(e);
		
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
				+ "<tr><td>Times Purchased: </td><td class='right'>" + commas(e.point.Object.TimesPurchased) + "</td></tr>"
				+ "<tr><td>Total Spent: </td><td class='right'>$ " + commas(e.point.Object.TotalSpent) + "</td></tr>"
				+ "<tr><td>Unique Gamers: </td><td class='right'>" + commas(e.point.Object.UniqueGamers) + "</td></tr>";
		
		html += "</table>"; 
		
		return html;
	},

	units: " Times Purchased",
	
	chartLeftMargin: 350,
};

function groupShoppingItems(shops) {
	var items = [];
	
	$.each(shops, function(i, shop) {
		// add the store name to each item
		shop.Items.map(function(d) {d["ShopName"] = shop.ShopName});
		//console.log(shop.Items);
		items = items.concat(shop.Items);
	});
	
	return items;
}