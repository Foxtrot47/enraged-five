// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-tableau", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	tableauEndpoint: config.tableauEndpoint,
	tableauReportDev: "RPHourly/RPHourly",
	tableauReportProd: "RPHourly_0/RPHourly",
	oldTelemetryReport: config.webHost + "/stats/cash_rp_hourly.html",
};