//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // character
	     false, // date-grouping
	     [false, false, false], // dates+builds
	],
};

var items = [
	{
		metric: "$ Bank Out" ,
		val: function(d) {return d.BankOut;},
	},
	{
		metric: "$ Bank In" ,
		val: function(d) {return d.BankIn;},
	},
	{
		metric: "$ Freemode Out" ,
		val: function(d) {return d.FreemodeOut;},
	},
	{
		metric: "$ Freemode In" ,
		val: function(d) {return d.FreemodeIn;},
	},
];

var reportOptions = {
		
	restEndpoint: config.transactionHistory,
	restEndpointAsync: config.transactionHistoryAsync,
	
	processFunction: convertToDict,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	//graphTitle: "Transaction History",
	
	elementId: "line-area-chart",
	//backgroundColour: "#ffffff",
	backgroundColour: "transparent",
	lineColour: "#000000",
	textColour: "#000000",
	gridColour: "#333333",
	
	name: function(d) { return d.name; },
	truncatedNameChars: 35,

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 130, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: "#333333"}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
			+ "<table>";
	
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'> $ " + commasFixed2(item.val(d.values)) + "</td></tr>");
			});
					
			content += "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	
	var dict = {};
	
	array.map(function(d) {
		var date = toUTCStringWOSecs(parseJsonDate(d.Start)) + " - " + toUTCStringWOSecs(parseJsonDate(d.End));
		
		$.each(items, function(i, item) {
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: date, value: item.val(d), values : d}];
			else
				dict[item.metric].push({ name: date, value: item.val(d), values : d});
		});
		
	});
	
	return dict;
}
