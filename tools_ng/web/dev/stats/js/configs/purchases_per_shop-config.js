var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.shoppingStats,
	restEndpointAsync: config.shoppingStatsAsync,
	
	multipleRequests: generateEndpoints,
	processFunction: calcTotals,
	
	/*
	hasExtraRestParams: [
	{
		key: "GroupByType",
	    value: false,
	},
	],
	*/
				
	isClickable: true,
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	
	// This is a piechart
	main: {
		legend: true,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.ShopName; }, // this is added to the results
		
		// unit: "players" - not always
		lrMargin: 20,
	},
	// This is the breakdown barchart
	breakdown: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getLabel: function(d) { return d.label},
		getColor: function(d) { return d.color},
		getValuesArray: function(d) {return d.Items; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.ItemName.trim(); }, 
		
		getYLabel: function(d) {return d.key; },
		
		matchColoursFromIndex: true,
		
		addUnitsToBarchart: true,
		
		leftMargin: 250,
		
		getObject: function(d) { return d; },
	},
};

var radioButtonsPrefix = "purchases-per-shop-radio";

function setReportOptions() {
	var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var metric = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[0];
	var unit = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[1];
	
	reportOptions.main.title = label;
	//reportOptions.breakdown.title = "sdasd"; 
	
	reportOptions.main.unit = " " + unit;
	reportOptions.breakdown.unit = reportOptions.main.unit;
	
	reportOptions.main.getValue = function(d) {		
		return d[metric]; 
	};
	reportOptions.breakdown.getValue = reportOptions.main.getValue; 
	
	reportOptions.main.tooltipContent = function(key, y, e, graph) {
		var sum = d3.sum(graph.container.__data__[0].values, function(d) {
			return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
		});
		// Use e.value instead of y, y is a formated string and parsing to number fails
		var percentage = ((e.value/sum)*100).toFixed(2);
		var metadata = graph.container.__data__[0].metadata;
		
		var html = "<h3>" + key + "</h3><br/>";
		
		var commasFunc = (unit == "$") ? cashCommasFixed : commasFixed2;
		var ttUnit = (unit == "$") ? "" : unit;
		
		html += "<table>"
			+ "<tr><td>" + label + ":</td>"
			+ "<td>" 
				+ commasFunc(e.value) + " " + ttUnit 
				+ ((metric != "UniqueGamers") ? (" (" + percentage + "%)") : "")
			+ "</td></tr>"
			
		if (metric != "UniqueGamers")
			html += "<tr><td>Total:</td><td>" +  commasFunc(sum) + " " + ttUnit + "</td></tr>";
		else
			html += "<tr><td>Online Players:</td><td>" 
						+  commasFunc(metadata.onlinePlayers)
						+ ((metadata.onlinePlayers) ? (" (" + commasFixed2((e.value/metadata.onlinePlayers)*100) + "%)") : "")
					+ "</td></tr>";
				
		html += "</table>";
		
		return html;
	};
	reportOptions.breakdown.tooltipContent = reportOptions.main.tooltipContent;
}

function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiometrics")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("TimesPurchased|times")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Times Purchased   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-2")
					.val("TotalSpent|$")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-2")
					.text("Total Spent   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-3")
					.val("UniqueGamers|players")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-3")
					.text("Unique Players   ")
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	pValues.Pairs["GroupByType"] = false;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function calcTotals(data) {
	// calling these functions here to make sure it's after the page generation
	addMetrics();
	setReportOptions();
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data[0][0].response,
	   	"metadata": {"onlinePlayers" : data[1][0].response},
	};
	
}
