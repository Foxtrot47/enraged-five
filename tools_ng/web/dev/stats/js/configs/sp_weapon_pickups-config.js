// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var currentStat = "Weapons Collected";
var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "NUM_WEAPON_PICKUPS",
     ],
     name: currentStat,
     requestSubString: "_",
     bucketSize: 10,
     //singleMetric: true,
     singleOnly: true,
     fillWithEmptyBuckets: true,
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	
	availableCharts: profileStatList,
	
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	{
	    key: "BucketSize",
	    value: profileStatList[0].bucketSize,
	},
	],
	
	processFunction: formatData,
		
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.Bucket; },
	value: function(d) { 
		return ((d.TotalUsers) ? (Number(d.YValue)/d.TotalUsers)*100 : 0); 
	},
	
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Percentage of Players (%)",
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	hideLegend: true,
	legend: {height: 30, width: 180, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + "" + "</div><br />"
			+ "<table>"
					+ "<tr><td>Percentage of Players:</td><td class='right'>" + commasFixed2(this.value(d)) + " %</td></tr>"
					+ "<tr><td>Number of Players:</td><td class='right'>" + commasFixed2(Number(d.YValue)) + "</td></tr>"
					+ "<tr><td>Total Players:</td><td class='right'>" + commasFixed2(d.TotalUsers) + "</td></tr>"
				+ "</table>";
			return content;
	},
			              
};

function formatData(data) {	
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	var populatedResult = profStat.processBucketResults(data, profileStatList[0]);
	// Calc the total users
	var totalUsers = 0;
	populatedResult.map(function(r) {totalUsers += Number(r.YValue); });
	
	// Add the total users
	populatedResult.map(function(r) {r["TotalUsers"] = totalUsers; return r; })
	
	// convert to dict for the line graph
	return profStat.convertResultsToDict(
			[populatedResult],
			[reportOptions.yLabel]); 
}

function getStatNames() {
	
	var statNames = [];
	$.each(reportOptions.availableCharts, function (i, profileStat) {
		statNames = statNames.concat(
			profStat.constructStatNames(profileStat, profileStat.types[0], config.gameTypes)
		);
	});
		
	return statNames.join(",");
}
