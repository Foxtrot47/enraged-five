//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// capture stats filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // date-grouping
	     false, // character
	     [false, false, false], // dates+builds
	],
};

var pBet = "$ Amount Players Bet";
var pWon = "$ Amount Players Won";
var rMade = "$ Amount R* Made";

var reportOptions = {
		
	restEndpoint: config.cashPurchasesReport,
	restEndpointAsync: config.cashPurchasesReportAsync,
	
	processFunction: convertToDict,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	//graphTitle: "Money Purchases",
	
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },
	truncatedNameChars: 35,

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 150, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: config.chartColour1}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
				+ "<table>"
					+ "<tr><td>Times Purchased: </td><td class='right'>" + commas(d.values.TimesPurchased) + "</td></tr>"
					+ "<tr><td>Unique Gamers: </td><td class='right'>" + commas(d.values.UniqueGamers) + "</td></tr>"
				+ "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	var postFix = " Cash Packs";
	var dict = {};
	
	var dates = {}; // keep track of all the available dates
	
	// Sort by  amount DESC
	array.CashPackData.sort(function(a, b) { return (a.InGameAmount < b.InGameAmount) ? 1 : -1});
		
	array.CashPackData.map(function(d) {
		if (!dict.hasOwnProperty(d.CashPackName))
			dict[d.CashPackName + postFix] = [];
			
		// Convert the stat data to a dict for efficient lookup when filling the empty valued dates
		d.StatDict = {}; 
		
		d.Stats.map(function(s) {
			var startDate = parseJsonDate(s.Start).getTime();
	
			// Store the entry to the dict
			d.StatDict[startDate] = s;
			
			// Store the date
			if (!dates.hasOwnProperty(startDate))
				dates[startDate] = s.End;
		});
	
	});
	
	//dates.sort(function(a, b) {console.log(a);});
	var datesKeys = Object.keys(dates).sort(function(a, b) {return (Number(a) > Number(b)) ? 1: -1 });
	
	//Loop again over the data and fill the gaps with empty values for non existing dates
	array.CashPackData.map(function(d) {
		
		$.each(datesKeys, function(index, startDateKey) {
			// startDateKey needs to be a number; dates[startDateKey] will return the End date in json
			var name = toUTCStringWOSecs(new Date(Number(startDateKey))) 
						+ " - " + toUTCStringWOSecs(new Date(parseJsonDate(dates[startDateKey])));
			if (d.StatDict.hasOwnProperty(startDateKey)) {
				dict[d.CashPackName + postFix].push(
					{
						name: name, 
						value: d.StatDict[startDateKey].TimesPurchased, 
						values: d.StatDict[startDateKey],
					}
				);
			}
			else {
				dict[d.CashPackName + postFix].push(
					{ name: name, value: 0, values : {"TimesPurchased": 0, "UniqueGamers": 0}}
				);
			}
		});
	});
	
	$("#content-description")
		.empty()
		.html("Total Percentage : " + formatPercentage(array.TotalPercentage, 1) + "%"
			+ " - "
			+ "Unique Gamers : " + array.UniqueGamers
		);

	return dict;
}
