// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var altNames = [
	"Bar Hen House",
	"Bar Hookies",
	"Bar Pitchers",
	"Bar Tequilala",
	"Downtown Cinema",
	"Morningwood Cinema",
	"Vinewood Cinema",
	"Car Mod Shop",
	"Car Scrap Yard",
	"Golf Club",
	"Sonar Collections",
	"Strip Club",
	"Taxi Lot",
	"Towing Impound",
	"Arms Trafficking",
	"Weed Shop",
];

var profileStatList = [
{
	types: [
	    "PROP_BOUGHT_BARHE",
	    "PROP_BOUGHT_BARHO",
	    "PROP_BOUGHT_BARPI",
	    "PROP_BOUGHT_BARTE",
	    "PROP_BOUGHT_CIND",
	    "PROP_BOUGHT_CINM",
	    "PROP_BOUGHT_CINV",
	    "PROP_BOUGHT_CMSH",
	    "PROP_BOUGHT_CSCR",
	    "PROP_BOUGHT_GOLF",
	    "PROP_BOUGHT_SOCO",
	    "PROP_BOUGHT_STRIP",
	    "PROP_BOUGHT_TAXI",
	    "PROP_BOUGHT_TOWI",
	    "PROP_BOUGHT_TRAF",
	    "PROP_BOUGHT_WEED",
     ],
     altNames: altNames,
     name: "Properties Bought",
     requestSubString: "_",
     bucketSize: 1, // this will return separate the users who bought and the users who didn't
     singleMetric: true,
     unit: "times",
},
{
	types: [
	    "PROP_EARNED_BARHE",
	    "PROP_EARNED_BARHO",
	    "PROP_EARNED_BARPI",
	    "PROP_EARNED_BARTE",
	    "PROP_EARNED_CIND",
	    "PROP_EARNED_CINM",
	    "PROP_EARNED_CINV",
	    "PROP_EARNED_CMSH",
	    "PROP_EARNED_CSCR",
	    "PROP_EARNED_GOLF",
	    "PROP_EARNED_SOCO",
	    "PROP_EARNED_STRIP",
	    "PROP_EARNED_TAXI",
	    "PROP_EARNED_TOWI",
	    "PROP_EARNED_TRAF",
	    "PROP_EARNED_WEED",
     ],
     name: "Money Earned",
     altNames: altNames,
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "$",
},
{
	types: [
	    "PROP_MISSIONS_BARHE",
	    "PROP_MISSIONS_BARHO",
	    "PROP_MISSIONS_BARPI",
	    "PROP_MISSIONS_BARTE",
	    "PROP_MISSIONS_CIND",
	    "PROP_MISSIONS_CINM",
	    "PROP_MISSIONS_CINV",
	    null, //"PROP_MISSIONS_CMSH", // NE
	    "PROP_MISSIONS_CSCR",
	    null, //"PROP_MISSIONS_GOLF", // NE
	    null, // "PROP_MISSIONS_SOCO", // NE
	    null, //"PROP_MISSIONS_STRIP", // NE
	    "PROP_MISSIONS_TAXI",
	    null, //"PROP_MISSIONS_TOWI", // NE
	    null, //"PROP_MISSIONS_TRAF", // NE
	    "PROP_MISSIONS_WEED",
     ],
     name: "Missions Triggered",
     altNames: altNames,
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "missions",
},
];

var lineGraphItems = [
    {
		metric: "Average Times Completed" ,
        val: function(d) {return (d.NumGamersAttempted) ? (d.TotalPassed/d.NumGamersAttempted) : 0;},
    },
    {
		metric: "Average Times Triggered" ,
		val: function(d) {return (d.NumGamersAttempted) ? (d.TotalAttempts/d.NumGamersAttempted) : 0;},
	}
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	isClickable: false,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: false,
		
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,
		
		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name; }, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value; },
		
		getObject: function(d) { return (d); },
		
		tooltipContent: function(key, y, e, graph) {
			var sum = d3.sum(graph.container.__data__[0].values, function(d) {
				return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
			});
			
			var metadata = graph.container.__data__[0].metadata;
			
			// Use e.value instead of y, y is a formated string and parsing to number fails
			var percentage = ((e.value/sum)*100).toFixed(2);
			
			var html = "<h3>" + key + "</h3><br/>"
				+ "<table>"
					+ "<tr><td>" + "Times Purchased" + ":</td>"
					+ "<td>" + commasFixed(e.value, 4) + " " + metadata.unit + " (" + percentage + "%)" + "</td></tr>"
					+ "<tr><td>Total Property Purchases:</td><td>" 
						+  commasFixed(sum, 4) 
					+ "</td></tr>"
					+ "<tr><td>Players Purchased:</td><td>" 
						+  commasFixed(Number(e.point.users), 4) 
					+  "</td></tr>"
					+ "<tr><td>Average Money Earned:</td><td>" 
						+  cashCommasFixed((e.point.users) ? e.point.money/e.point.users : 0, 4) 
					+ "</td></tr>"
					+ "<tr><td>Average Missions Triggered:</td><td>" 
						+  commasFixed((e.point.users) ? e.point.missions/e.point.users : 0, 6) 
					+  "</td></tr>";
						
			return html += "</table>";
		},
		
		lrMargin: 30,
	},
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			if (!statName)
				return;
			
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	var array = [];	
	
	var pieValues = [];
	var missionID = 0; // index of the mission results as they are less than the rest metrics
	$.each(data[0], function(j, typeResult) {
		pieValues.push({
			"name": profStat.getName(typeResult.response[0], profileStatList[0], j), 
			"value": (typeResult.response.length > 1) ?  Number(typeResult.response[1].Total) : 0,
			"users":  (typeResult.response.length > 1) ?  Number(typeResult.response[1].YValue) : 0,
			"money": Number(data[1][j].response[0].Total),
			"missions": (profileStatList[2].types[j]) ? Number(data[2][missionID++].response[0].Total) : 0,
		});
	});
	
	array.push(
		{
		   	"label": profileStatList[0].name,
		   	"values": pieValues,
		   	"metadata": {"unit": profileStatList[0].unit},
		}
	);
				
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
