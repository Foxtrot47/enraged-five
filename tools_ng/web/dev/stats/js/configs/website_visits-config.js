// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

//var platformNames = getPlatforms();

var reportOptions = {
	//processFunction: groupByUser,
	restEndpoint: config.websiteVisits,
	restEndpointAsync: config.websiteVisitsAsync,

	enableCSVExport: true,

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Website Name", getValue: function(d) {return d.Name;} },
	    {title: "Times Visited", getValue: function(d) {return d.Count;} },   
	    {title: "Number Of Gamers", getValue: function(d) {return d.UniqueGamers;} },
	    {title: "Average Time Spent", getValue: function(d) {
	    	return formatSecsWithDays((d.UniqueGamers) ? d.Duration/d.UniqueGamers : 0);} 
	    },
	],
	
	/* Groups */
	groups: [
	    {title: "Website List", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: false,
	
};

function groupByUser(data) {
	
	var users = {};
	$.each(data, function(i, d) {
		if (users.hasOwnProperty(d.UserName))
			users[d.UserName].Sessions.push(d);
		else {
			users[d.UserName] = {
				"UserName": d.UserName,
				"Sessions": [d],
			}
		}
	});
	
	//console.log(users);
	return users;
}
