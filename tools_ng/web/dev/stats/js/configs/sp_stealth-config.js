// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "STEALTH_TIME",
     ],
     name: "Time in Stealth Mode",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "hours",
},
{
	types: [
	   "KILLS_STEALTH",
     ],
     name: "Stealth Takedowns",
     requestSubString: "_",
     bucketSize: 5,
     fillWithEmptyBuckets: true,
     //singleMetric: true,
     singleOnly: true,
     unit: "takedowns",
},
];

var currentStat = profileStatList[1].name;

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	enableCSVExport: "content-description",
	graphTitle: currentStat,
	
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.Bucket; },
	value: function(d) { 
		return ((d.TotalUsers) ? (Number(d.YValue)/d.TotalUsers)*100 : 0); 
	},
	
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Percentage of Players (%)",
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	hideLegend: true,
	legend: {height: 30, width: 180, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + "" + "</div><br />"
			+ "<table>"
					+ "<tr><td>Percentage of Players:</td><td class='right'>" + commasFixed2(this.value(d)) + " %</td></tr>"
					+ "<tr><td>Number of Players:</td><td class='right'>" + commasFixed2(Number(d.YValue)) + "</td></tr>"
					+ "<tr><td>Total Players:</td><td class='right'>" + commasFixed2(d.TotalUsers) + "</td></tr>"
				+ "</table>";
			return content;
	},
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	// This is the stealth time
	$("#content-description")
		.empty()
		.html(profileStatList[0].name + ": " + formatSecsWithDays(Number(data[0][0].response[0].Total)/1000))
	
	// This is the stealth kills
	var populatedResult = profStat.processBucketResults(data[1][0].response, profileStatList[1]);
	//var populatedResult = data[1][0].response;
	// Calc the total users
	var totalUsers = 0;
	populatedResult.map(function(r) {totalUsers += Number(r.YValue); });
	
	// Add the total users
	populatedResult.map(function(r) {r["TotalUsers"] = totalUsers; return r; })
	
	// convert to dict for the line graph
	return profStat.convertResultsToDict(
			[populatedResult],
			[reportOptions.yLabel]); 
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
