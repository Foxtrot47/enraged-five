//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// capture stats filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // date-grouping
	     false, // character
	     [false, false, false], // dates+builds
	],
};

var items = [
	{
		metric: "Concurrent Players" ,
		val: function(d) {return d.Value;},
	}
	];

var reportOptions = {
		
	restEndpoint: config.concurrentUsers,
	restEndpointAsync: config.concurrentUsersAsync,
	
	multipleRequests: generateEndpoints,
	processFunction: convertToDict,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	//graphTitle: "Total Players",
	
	elementId: "line-area-chart",
	//backgroundColour: "#ffffff",
	backgroundColour: "transparent",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },
	truncatedNameChars: 35,

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 200, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: config.chartColour1, hasBrackets: true}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
			+ "<table>";
	
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'>" + commasFixed2(item.val(d.values)) + "</td></tr>");
				content += ("<tr><td>Unique Players:</td><td class='right'>" + commasFixed2(d.values.UniqueGamers) + "</td></tr>");
			});
								
			content += "</table>";

		return content;
	},
		              
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	
	var pValuesArray = [];
	if ($("#platforms-split").prop("checked") && $("#platforms").val()) {
		$.each($("#platforms").val(), function(i, platform) {
			var pValuesClone = $.extend(true, {}, pValues);
			pValuesClone.Pairs["PlatformNames"] = platform;
			pValuesArray.push(pValuesClone);
		});
	}
	else
		pValuesArray.push(pValues);
	
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: pValuesArray,
	   	}
	];
			
	return endpointObjects;
}

function convertToDict(data) {
	$("#platforms-split-div").removeClass("hidden");
	$("#platforms-field select").height(125);
	
	var splitPlatforms = ($("#platforms-split").prop("checked") && $("#platforms").val());
	var selectedPlatforms = $("#platforms").val();
	
	var dict = {};
	
	$.each(data[0], function(index, result) {
		if (!result.response.Stats)
			return;
		// result.response.Values = [];
		
		result.response.Stats.map(function(d) {
			var date = toUTCStringWOSecs(parseJsonDate(d.Start)) + " - " + toUTCStringWOSecs(parseJsonDate(d.End));
					
			$.each(items, function(i, item) {
				d["UniqueGamers"] = result.response.UniqueGamers;
				var dictName = (splitPlatforms) ? (selectedPlatforms[index]) : item.metric;
				
				if (!dict.hasOwnProperty(dictName))
					dict[dictName] = [{ name: date, value: item.val(d), values : d }];
				else
					dict[dictName].push({ name: date, value: item.val(d), values : d });
			});
			
		}); // End of each result values
		
	}); // End of each endpoint results
	
	
	return dict;
}
