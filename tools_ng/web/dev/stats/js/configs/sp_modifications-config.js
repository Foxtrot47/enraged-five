//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.weaponModPurchases,
	restEndpointAsync: config.weaponModPurchasesAsync,
	
	processFunction: formatData,
	multipleRequests: generateEndpoints,
		
	hasFriendlierNames: true,
	
	description: "",
	
	chartGroups: {
		getGroups: function(d) {return d}, // Data are returned grouped
		getGroupName: function(d) {
			return (($("#friendlier-names").is(":checked") && d.WeaponFriendlyName) ? d.WeaponFriendlyName : d.WeaponName);
		},
	    getGroupValues: function(d) {return d.WeaponMods},
   	},
	
	bars: [
	{
	    title: "Percentage of Users Purchased",
	    colour: config.chartColour1,
	    getName: function(d) {
	    	return ($("#friendlier-names").is(":checked") && d.WeaponModFriendlyName) ? d.WeaponModFriendlyName : d.WeaponModName;
	    },
	    getValue: function(d) {
	    	return (d.OnlineGamers) ? (commasFixed2((d.UniqueGamers/d.OnlineGamers)*100)) : commasFixed2(0);
	    	},
	    getObject: function(d) {return d},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
					+ "<tr><td>" + key + ": </td><td class='right'>" 
						+ commasFixed2(Number(e.value))
					+ " %</td></tr>"
					+ "<tr><td>Times Purchased: </td><td class='right'>" 
						+ commasFixed2(e.point.Object.TimesPurchased)
					+ "</td></tr>"
					+ "<tr><td>Total Spent: </td><td class='right'>$ " 
						+ commasFixed2(e.point.Object.TotalSpent)
					+ "</td></tr>"
					+ "<tr><td>Unique Gamers: </td><td class='right'>" 
						+ commasFixed2(e.point.Object.UniqueGamers)
					+ "</td></tr>"
					+ "<tr><td>Online Gamers: </td><td class='right'>" 
						+ commasFixed2(e.point.Object.OnlineGamers)
					+ "</td></tr>";
		html += "</table>"; 
			
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(Number(d));
	},
	units: "Users (%)",
	
	chartLeftMargin: 185,
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function formatData(data) {
	return data[0][0].response.map(function(d) {
		d.WeaponMods = d.WeaponMods.map(function(w) {
			w["OnlineGamers"] = data[1][0].response; return w; 
		});
		return d;
	});
}

//Friendly names by default
//$("#friendlier-names").prop("checked", true);