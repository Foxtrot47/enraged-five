// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-topusers",
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // upper xp limit
	     false, // dates+builds
	     ],
};

var reportOptions = {
	getSelectableTable: true,
	
	restEndpoint: config.topUsersCashReport,
	restEndpointAsync: config.topUsersCashReportAsync,
	
	selectedEndpoints: generateUserCashEndpoints,
	//multipleRequests: generateEndpoints,
	
	processFunction: groupResults,
	
	enableCSVExport: "content-description",
	
	hasExtraRestParams: [
	{
		key: "Count",
		value: 50,
	},
	{
		key: "Offset",
		value: 0,
	},
	/*
	{
		key: "UpperXPLimit",
		value: 500000000,
	},
	*/
    ],
};


function generateUserCashEndpoints(pValues, gamers) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	
	var pValuesArray = [];
	
	// pValues belong to sc header, need to add the extra details 
	$.each(gamers, function(i , gamer) {
		var copiedPvalues = $.extend(true, {}, pValues);
		//copiedPvalues.Pairs["GamerHandle"] = gamer.GamerHandle;
		copiedPvalues.Pairs["AccountId"] = gamer.AccountId;
		copiedPvalues.Pairs["PlatformIdentifier"] = rosPlatforms[gamer.Platform].Name;
		copiedPvalues.Pairs["CharacterSlot"] = gamer.CharacterSlot;
		copiedPvalues.Pairs["CharacterId"] = gamer.CharacterId;
		
		pValuesArray.push(copiedPvalues);
	});
	
	var endpointObjects = [
		{
			restUrl: config.restHost + config.userCashReport,
			restAsyncUrl: config.restHost + config.userCashReportAsync + pValues.ForceUrlSuffix,
			pValues: pValuesArray,
	   	}
	];
			
	return endpointObjects;
}

function groupResults(data) {
	var array = [];
	
	$.each(data[0], function(i, d) {
		array.push(d.response);
	});
	
	return array;
}