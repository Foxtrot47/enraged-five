// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var items = [
	{
		metric: " Average Cash Earned" ,
		val: function(d) {return d.TotalAvgCashEarned;},
	},
	{
		metric: " Average Time" ,
		val: function(d) {return (d.TotalAvgPlayingTime/config.anHourInMillisecs);},
	},
	
];

var reportOptions = {
		
	restEndpoint: config.rankCashReport,
	restEndpointAsync: config.rankCashReportAsync,
	
	processFunction: calcTotals,
	
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	//graphTitle: "Total Cash & RP",
	
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: "#000000",
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	xLabel: "Rank",
	yLabel: "Total Cash ($)",
	y1Label: "Hour",
	
	secondAxisKeys: [items[1].metric],
		
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 150, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: "#333333"}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
			+ "<table>";
		
			/*
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'> " + commasFixed2(item.val(d.values)) + "</td></tr>");
			});
			*/
			content += ("<tr><td>" + items[0].metric + ":</td><td class='right'> " 
					+ cashCommasFixed(items[0].val(d.values), 2) + "</td></tr>");
			content += ("<tr><td>" + items[1].metric + ":</td><td class='right'> " 
					+ commasFixed2(items[1].val(d.values)) + " hours</td></tr>");
			

			content += "</table>";

		return content;
	},
		              
};

function calcTotals(array) {
	
	var dict = {}; 
	
	// sort by rank
	array.sort(function(a, b) { return (a.Rank < b.Rank) ? -1 : 1 });
	
	array = fillEmptyResults(array, "Rank", 1);
	
	array.map(function(d, i) {
		d["TotalAvgPlayingTime"] = (i>0) ? (array[i-1].TotalAvgPlayingTime + d.AvgPlayingTime) : (d.AvgPlayingTime);
 		d["TotalAvgCashEarned"] = (i>0) ? (array[i-1].TotalAvgCashEarned + d.AvgCashEarned) : (d.AvgCashEarned);
		
		//var name = reportOptions.xLabel + " " + d.Rank;
 		var name = d.Rank;
		
		$.each(items, function(i, item) {
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: name, value: item.val(d), values : d}];
			else
				dict[item.metric].push({ name: name, value: item.val(d), values : d});
		});
		
	});
	
	return dict;
}
