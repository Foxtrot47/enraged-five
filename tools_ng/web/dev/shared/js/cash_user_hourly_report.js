var reportData = [];

var rosPlatformsDict = getRosPlatformsDict();
var matchTypesDict = getMatchTypesDict();
var jobResultsDict = getJobResultsDict();

var columnGroups = {
   "Cash" : {
	   "classname": "cash-column",
	   "columns": ["Cash Balance", "Cash Spent", "Cash Earned"],
   },
   "RP" : {
	   "classname": "rp-column", 
	   "columns": ["Rank", "RP Total", "RP Earned"],
   },
   "Jobs" : {
	   "classname": "jobs-column", 
	   "columns": ["Job Name", "RP Earned", "Cash Earned", "Amount Bet", "Bet Winnings", "Rewards"],
   },
   "Unlocks" : {
	   "classname": "unlocks-column", 
	   "columns": ["Weapons / Addons / Kit", "Phone Unlocks", "Car Mods",
	                "Male Hair", "Female Hair", "Male Clothes", "Female Clothes"],
   },
};
var columnGroupsKeys = Object.keys(columnGroups);

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
		
	$("#filter").click(function() {
		generateReport();
	});
		
	generateReport();
}

function generateReport() {
	$("#content-body").empty();

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if(!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else {
					value = paramObj.value;
				}
				pValues.Pairs[paramObj.key] = value;
			});
		}
		
		if (reportOptions.storePValues) {
			reportOptions.storePValues(pValues);
		}
	
		var callbackFunc = ((reportOptions.getSelectableTable) 
				? populateUserTable 
				: populateSelectedReport);
		
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
				"json",
				config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, callbackFunc);
	}
}

function populateUserTable(data) {
	
	$("#content-body").empty();
	
	if (data.length == 0) {
		Sexy.alert(config.noDataText);
		return;
	}
	
	// Title Table
	$("<table />")
		.addClass("title-only")
		.append(
			$("<tr>")
				.append(
					$("<th>")
					.addClass("title")
					.text("Top Users")
			)
		)
		.appendTo("#content-body");
	
	// Data Table
	var table = $("<table />").addClass("tablesorter top-users"); 
	table.append(
		$("<thead>")
			.append(
				$("<tr>")
			)
	);
	
	var tr = table.find("tr");

	tr.append(
		$("<th>").text("Rank")
    )
	.append(
		$("<th>").text("Gamer Handle")
	)
	.append(
		$("<th>").text("Platform")
	)
	.append(
		$("<th>").text("RP")
	);
	
	var tbody = $("<tbody>");

	$.each(data, function(i, d) {
		var tr = $("<tr>");
		tr.append(
			$("<td>")
				.text(i+1)
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		tr.append(
			$("<td>")
				.text(d.GamerHandle)
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.append(
			$("<td>")
				.text(rosPlatformsDict[d.Platform])
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.append(
			$("<td>")
				.text(commasFixed2(d.XP))
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.appendTo(tbody);
	});
						
	tbody.appendTo(table);
	table.tablesorter();
	table.appendTo("#content-body");
	
	//populateSelectedReport(data);
}

function populateSelectedReport(selectedUsers) {	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	var endpointsArray = reportOptions.selectedEndpoints(pValues, selectedUsers);
	
	var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
	
	setTimeout(function() {
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}, 500);
	
}

function populateReport(data) {
	//$("#content-body").css("overflow", "hidden");
	$("#content-body").empty();
	
	generateCheckboxes();
	
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
	
	if (reportData.length == 0) {
		Sexy.alert(config.noDataText);
		return;
	}
	
	var gamerAccountDict = getGamerAccountDict();
	
	// Store the max row spans for each Hour row
	var rowMaxSpan = [];
	reportData.map(function(r) {
		// Sort the data first by hour
		r.Data.sort(function(a, b) { return (a.GameplayHour < b.GameplayHour) ? -1 : 1});
		
		// b*#1640644 - fill the gap hours with default values
		var DataCopy = [];
		var counter = 0; // using this for counting the current GameplayHour in the new default including array
		$.each(r.Data, function(i, d) {
			//console.log(d.GameplayHour, counter);
			
			if (d.GameplayHour > counter) {
				for (j=0; j<(d.GameplayHour-counter); j++) {
					counter++;
					DataCopy.push({
						//CashBalance: d.CashBalance,
						CashSpent: 0,
						CashSpentDetails: [],
						CashEarned: 0,
						CashEarnedDetails: [],
						XPEarned: 0,
						Rank: (r.Data[i-1]) ? r.Data[i-1].Rank : 0,
						//XPTotal: d.XPTotal,
						XPEarned: 0,
						XPEarnedDetails: 0, 
						Matches: [],
						Unlocks: [],
					});
				}
			}
			
			DataCopy.push(d);
			
			/*
			if (!rowMaxSpan[d.GameplayHour])
				rowMaxSpan[d.GameplayHour] = d.Matches.length;
			else
				rowMaxSpan[d.GameplayHour] = (rowMaxSpan[d.GameplayHour] > d.Matches.length)
			*/
			counter++;
		});
		//r.Data = $.extend(true, {}, DataCopy);
		r.Data = DataCopy;
		
		r.Data.map(function(d, i) {
			// Find the maximum matches count of the hour
			if (!rowMaxSpan[i])
				rowMaxSpan[i] = d.Matches.length;
			else
				rowMaxSpan[i] = (rowMaxSpan[i] > d.Matches.length) ? rowMaxSpan[i] : d.Matches.length;
		})
		
	});
	
	var table = $("<table />")
			.attr("id", "cash-table")
			.addClass("cash-report");
	
	var thead = $("<thead>"); //.addClass("fixed");
	var trhead1 = $("<tr>").addClass("black-border-top");
	
	var csvColumns = [];
	var csvRows = [];
	
	/*
	trhead.append(
		$("<th>")
			//.addClass("invisible")
			.addClass("title3")
			.addClass("black-border-right")
			.addClass("no-bottom-border")
	);
	*/
	
	function constructUserText(d) {
		return (
			((gamerAccountDict.hasOwnProperty(d.AccountId)) ? (gamerAccountDict[d.AccountId]) : d.AccountId)
			//((gamerAccountDict.hasOwnProperty(d.GamerHandle)) ? (gamerAccountDict[d.GamerHandle]) : d.GamerHandle)
			+ " (" 
			+ rosPlatformsDict[d.Platform]
			+ " - ID " 
			+ d.CharacterId
			+ " - SLOT " 
			+ d.CharacterSlot
			+ ")"
		);
	}
	
	$.each(reportData, function(userIndex, userData) {
		trhead1.append(
			$("<th>")
				//.addClass("invisible")
				.addClass("title3")
				.addClass("black-border-left black-border-right")
				.addClass("no-bottom-border")
		);
		
		trhead1.append(
			$("<th>")
				.addClass("title")
				//.addClass("black-border-right")
				.attr("colspan", 19)
				.text(constructUserText(userData))
		)
	});
	trhead1.appendTo(thead);
	
	var trhead2 = $("<tr>");
	/*
	trhead2.append(
		$("<th>")
			//.addClass("invisible")
			.addClass("title3")
			.addClass("black-border-right")
			.addClass("no-bottom-border")
	);
	*/
	$.each(reportData, function(userIndex, userData) {
		trhead2.append(
			$("<th>")
				//.addClass("invisible")
				.addClass("title3")
				.addClass("black-border-left black-border-right")
				.addClass("no-bottom-border")
		);
		/*
		trhead2.append(
				$("<th>")
					.addClass("title2")
					.attr("colspan", 3)
					.text("Cash")
					.addClass("cash-column")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.attr("colspan", 3)
						.text("RP")
						.addClass("rp-column")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.attr("colspan", 6)
						.text("Jobs")
						.addClass("jobs-column")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.addClass("black-border-right") 
						.attr("colspan", 7)
						.text("Unlocks")
						.addClass("unlocks-column")
				);
		*/
		
		$.each(columnGroups, function(key, value) {
			trhead2.append(
				$("<th>")
					.addClass("title2")
					//.addClass((columnGroupsKeys[columnGroupsKeys.length-1] == key) ? "black-border-right" : "")
					.attr("colspan", value["columns"].length)
					.text(key)
					.addClass(value["classname"])
			);
		});
				
	});
	trhead2.appendTo(thead);
	
	var trhead3 = $("<tr>");
	/*
	trhead3.append(
		$("<th>")
			.addClass("title3")
			.addClass("black-border-right")
			.text("Hour")
		);
	*/
	var hourText = "Hour";
	$.each(reportData, function(userIndex, userData) {
		trhead3.append(
			$("<th>")
			.addClass("title3")
			.addClass("black-border-left black-border-right")
			.append(
				$("<span>").html(hourText)
			)
			.append(
				$("<span>")
					.addClass("hidden")
					.html(" - " + constructUserText(userData))
			)
		);
		
		/*
		trhead3
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("cash-column")
					.text("Cash Balance")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("cash-column")
					.text("Cash Spent")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("cash-column")
					.text("Cash Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("rp-column")
					.text("Rank")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("rp-column")
					.text("RP Total")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("rp-column")
					.text("RP Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("Job Name")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("RP Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("Cash Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("Amount Bet")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("Bet Winnings")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("jobs-column")
					.text("Rewards")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Weapons / Addons / Kit")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Phone Unlocks")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Car Mods")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Male Hair")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Female Hair")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.text("Male Clothes")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("unlocks-column")
					.addClass("black-border-right")
					.text("Female Clothes")
			)
			*/
		
		$.each(columnGroups, function(key, value) {
			$.each(value["columns"], function(c, column) {
				
				trhead3
					.append(
						$("<th>")
							.addClass("title3")
							.addClass(value["classname"])
							//.addClass(((c == value["columns"].length-1) && (columnGroupsKeys[columnGroupsKeys.length-1] == key)) ? "black-border-right" : "")
							.text(column)
					)
			});
		});
		
	});
	trhead3.appendTo(thead);
	
	// Store the row data to csv
	$(trhead3).find("th").each(function(thId, th) {
		csvColumns.push($(th).text());
	});
	
	//thead.appendTo(table);
	
	// Loop for each row
	$.each(rowMaxSpan, function(rowIndex, rowSpan) {
		
		if ((rowIndex % 2) == 0) { // even rows repeat the header
			var trheadClone = trhead1.clone();
			var trheadClone2 = trhead2.clone();
			var trheadClone3 = trhead3.clone();
			
			trheadClone.appendTo(table);
			trheadClone2.appendTo(table);
			trheadClone3.appendTo(table);
		}
		
		rowSpan += 1; // Increase by one to follow the Matches array where the total will be added
		
		var row = $("<tr>").addClass("black-border-top");
		
		//rowSpan = 1;	
		var hourName = ("Hour " + (rowIndex + 1));
		/*
		row.append(
			$("<td>")
				.attr("rowspan", rowSpan)
				.addClass("black-border-right")
				.addClass("td-row-label")
				//.addClass("fixed")
				.html(hourName)
			);
		*/
		csvRows[rowIndex] = [];
		
		// Loop for each user
		$.each(reportData, function(userIndex, userData) {
			//console.log(userData);
			row.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass("black-border-left black-border-right")
					.addClass("td-row-label")
					//.addClass("fixed")
					.html(hourName)
			);
			
			if (!userData.Data[rowIndex]) {
				row.append(
					$("<td>")
						.attr("rowspan", rowSpan)
						.attr("colspan", 19)
						.addClass("empty-hour")
						//.addClass("black-border-right")
						.html("")
				);
				// move to next user;
				return;
			}
		
			var totalMatchXP = 0,
				totalMatchCash = 0,
				totalMatchAmountBet = 0,
				totalMatchBetWinnings = 0,
				totalMatchReward = 0;
		
			userData.Data[rowIndex].Matches.map(function(m) {
				totalMatchXP += m.XPEarned;
				totalMatchCash += m.CashEarned;
				
				m.AmountBet = (m.AmountBet) ? m.AmountBet : 0; // Initialise if null
				totalMatchAmountBet += m.AmountBet;
				
				m.BetWinnings = (m.BetWinnings) ? m.BetWinnings : 0; // Initialise if null
				totalMatchBetWinnings += m.BetWinnings;
				
				m.JobRewardCash = (m.JobRewardCash) ? m.JobRewardCash : 0; // Initialise if null
				totalMatchReward += m.JobRewardCash;
			});
		
			userData.Data[rowIndex].Matches.push({
				"Name" : "TOTAL: ",
				"XPEarned" : totalMatchXP,
				"CashEarned" : totalMatchCash,
				"AmountBet" : totalMatchAmountBet,
				"BetWinnings" : totalMatchBetWinnings,
				"JobRewardCash" : totalMatchReward,
			});
				
			/*
			var XPTotal = (((rowIndex > 0) && userData.Data[rowIndex-1]) ? userData.Data[rowIndex-1].XPTotal : 0) 
							+ userData.Data[rowIndex].XPEarned;
			var CashBalance = (((rowIndex > 0) && userData.Data[rowIndex-1]) ? userData.Data[rowIndex-1].CashBalance : 0) 
									+ (userData.Data[rowIndex].CashEarned - userData.Data[rowIndex].CashSpent);
			*/
			var XPTotal = ((rowIndex > 0) ? userData.Data[rowIndex-1].XPTotal : 0) 
							+ userData.Data[rowIndex].XPEarned;
			var CashBalance = ((rowIndex > 0) ? userData.Data[rowIndex-1].CashBalance : 0) 
							+ (userData.Data[rowIndex].CashEarned - userData.Data[rowIndex].CashSpent);
			
			userData.Data[rowIndex]["XPTotal"] = XPTotal;
			userData.Data[rowIndex]["CashBalance"] = CashBalance;
		
			row.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Cash"]["classname"])
					.html(cashCommasFixed(userData.Data[rowIndex].CashBalance))
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Cash"]["classname"])
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].CashSpentDetails.length > 0) {
									return userData.Data[rowIndex].CashSpentDetails.reduce(
											function(a, b) {
												return (a + cashCommasFixed(b.Amount) 
														+ " (" + b.Action + ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(cashCommasFixed(userData.Data[rowIndex].CashSpent))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Cash"]["classname"])
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].CashEarnedDetails.length > 0) {
									return userData.Data[rowIndex].CashEarnedDetails.reduce(
											function(a, b) {
												return (a + cashCommasFixed(b.Amount) 
														+ " (" + b.Action + ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(cashCommasFixed(userData.Data[rowIndex].CashEarned))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["RP"]["classname"])
					.html(userData.Data[rowIndex].Rank)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["RP"]["classname"])
					.html(commasFixed2(userData.Data[rowIndex].XPTotal))
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["RP"]["classname"])
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].XPEarnedDetails.length > 0) {
									return userData.Data[rowIndex].XPEarnedDetails.reduce(
											function(a, b) {
												return (a + commasFixed2(b.Amount) 
														+ " (" + b.Action + " - " + b.Category +  ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(commasFixed2(userData.Data[rowIndex].XPEarned))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass((userData.Data[rowIndex].Matches.length == 1) ? "td-sum" : "")
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(
								(userData.Data[rowIndex].Matches[0]) 
									? constructMatchname(userData.Data[rowIndex].Matches[0])
									: ""
								+ "<br>"
							)
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return constructMatchname(d);
									})
									.join("<br>")
							)
					)
					
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(commasFixed(userData.Data[rowIndex].Matches[0].XPEarned) + "<br>")
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return commasFixed(d.XPEarned);
									})
									.join("<br>")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(cashCommasFixed(userData.Data[rowIndex].Matches[0].CashEarned) + "<br>")
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return cashCommasFixed(d.CashEarned);
									})
									.join("<br>")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(cashCommasFixed(userData.Data[rowIndex].Matches[0].AmountBet) + "<br>")
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return cashCommasFixed(d.AmountBet);
									})
									.join("<br>")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(cashCommasFixed(userData.Data[rowIndex].Matches[0].BetWinnings) + "<br>")
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return cashCommasFixed(d.BetWinnings);
									})
									.join("<br>")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass(columnGroups["Jobs"]["classname"])
					.append(
						$("<div>")
							.html(cashCommasFixed(userData.Data[rowIndex].Matches[0].JobRewardCash) + "<br>")
					)
					.append(
						$("<div>")
							.addClass("hidden")
							.html(
								userData.Data[rowIndex].Matches
									.filter(function(d, i) {
										return (i != 0);
									})
									.map(function(d) {
										return cashCommasFixed(d.JobRewardCash);
									})
									.join("<br>")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 0 && b.UnlockType<= 2) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 3) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>")
					//.addClass("black-border-right")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 4) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Male Hair - 6
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 6) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Female Hair - 7
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 7) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Male Clothes - 10-17
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 10 && b.UnlockType <= 17) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") //Female Clothes - 18-25
					//.addClass("black-border-right")
					.attr("rowspan", rowSpan)
					.addClass(columnGroups["Unlocks"]["classname"])
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 18 && b.UnlockType <= 25) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			);
			
		
		}); // End of each reportData
		
		// Add the basic row
		row.appendTo(table);
		
		// Store the row data to csv
		$(row).find("td").each(function(tdId, td) {
			// Replace first the breaks with new liens and then remove the html part
			var tdContent = $(td).html().replace(/<br>/g, "\n").replace(/<[^>]*>/g, "");
			//console.log(tdContent);
			csvRows[rowIndex].push(tdContent);
		});
		
		
		// This is tricky, the jobs columns have different number of rows across users
		// Continue for the remaining extra span rows
		
		for (var j=1; j<rowSpan; j++) { // skip the first element, already displayed in the basic row
		
			var extraRow = $("<tr>");
			// Loop again through users ...
			$.each(reportData, function(userIndex, userData) {
				if (!userData.Data[rowIndex])
					return;
				
				match = userData.Data[rowIndex].Matches[j];
				if (match) {
							
				// calc the difference for the total row cells rowspan
				rowSpanDiff = rowSpan - userData.Data[rowIndex].Matches.length + 1;
		
				extraRow
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass((j == userData.Data[rowIndex].Matches.length-1) ? "td-sum" : "")
						.addClass(columnGroups["Jobs"]["classname"])
						.html(function () {return constructMatchname(match)})
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass(columnGroups["Jobs"]["classname"])
						.html(commasFixed(match.XPEarned))
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass(columnGroups["Jobs"]["classname"])
						.html(cashCommasFixed(match.CashEarned))
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass(columnGroups["Jobs"]["classname"])
						.html(cashCommasFixed(match.AmountBet))
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass(columnGroups["Jobs"]["classname"])
						.html(cashCommasFixed(match.BetWinnings))
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass(columnGroups["Jobs"]["classname"])
						.html(cashCommasFixed(match.JobRewardCash))
					);
				}
				
			}); //End users loop
			
			extraRow.appendTo(table);
			
		} // End of FOR LOOP (j<rowSpan)
	
	}); // End of each rowMaxSpan (rowIndex, rowSpan)
	
	table.appendTo("#content-body");
	//table.dataTable();
	
	if (reportOptions.enableCSVExport) {
		var csvFrameID = "hourly-report-frame";
		
		$("#" + csvFrameID).remove();
			
		addCSVButton(reportOptions.enableCSVExport, 
				csvFrameID,
				function() {
					exportToCSV(csvColumns, csvRows, csvFrameID, getCSVFilename()); 
				});
		
		$("#" + csvFrameID).addClass("csv-right");
	}
}

function constructMatchname(match) {
	return (match.Name 
			+ ((matchTypesDict[match.Type]) 
				? (" (" + matchTypesDict[match.Type] 
					+ ((match.CheckpointCount) ? (", " + match.CheckpointCount + " Checkpoints") : "") 
				  + ")") 
				: ""
				)
			
			+ ((match.Duration) ? (" [" + formatSecs(match.Duration/1000) + "]") : "")
			+ ((jobResultsDict[match.Result]) ? (" - " + jobResultsDict[match.Result]) : "")
			+ ((match.MaxSurvivalWave && (match.Type == 3)) ? (", " + "Wave " + match.MaxSurvivalWave) : "")
			+ ((match.Rank && (match.Type == 2 || match.Type == 1)) ? (", Position " + match.Rank) : "")
			+ ((match.Players) ? (", " + match.Players + " Players") : "")
	);
}


var divId = "content-description-checkboxes";
function generateCheckboxes() {
	var checkBoxPrefix = "columnCheck";
	
	$("#content-description")
		.empty()
		.append(
			$("<div>")
				.attr("id", divId)
				.css("float", "left")
		)
		
	var k=0;
	$.each(columnGroups, function(key, value) {
		$("#" + divId)
			.append(
				$("<input>")
					.attr("type", "checkbox")
					.attr("id", checkBoxPrefix + "-" + k)
					//.val(value["classname"])
					.val(key)
					.attr("checked", true)
					.change(function(){
						// Hide/show the classed columns
						$("." + value["classname"]).toggleClass("hidden");
						
						var colspan = calcColspan();
						
						// Set it to the first table header - user info
						$(".title").attr("colspan", colspan);
						// And to the empty hour entries
						$(".empty-hour").attr("colspan", colspan);
						
					})
			)
			.append(
				$("<label>")
					.attr("for", checkBoxPrefix + "-" + k++)
					.attr("title", key)
					.text("  " + key + "    ")
			);
	});
	
}

function calcColspan() {
	// Calc the new visible colspan
	var colspan = 0;
	$("#" + divId + " :checkbox").each(function() {
		if ($(this).is(":checked"))
			colspan += columnGroups[$(this).val()]["columns"].length; 
	});
	colspan = (colspan == 0) ? 1 : colspan;
	
	return colspan;
}