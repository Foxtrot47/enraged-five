﻿var Deployment = function () {

    function addConfirmDialog(confirmClassname, dialogMessage) {
        $("." + confirmClassname).click(function (e) {
            if (!confirm(dialogMessage)) {
                e.preventDefault();
            }
        });
    }

    return {
        addConfirmDialog : addConfirmDialog,
    }
}
