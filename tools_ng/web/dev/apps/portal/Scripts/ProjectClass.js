﻿var Project = function () {

    function addSyncButtonEvent(syncButtonConfig) {
        var timeoutId = null;

        $("#" + syncButtonConfig.syncButtonId).click(function () {
            clearTimeout(timeoutId);
            $("#" + syncButtonConfig.syncMessageId).empty();

            var syncGuid = $("input:radio[name='" + syncButtonConfig.shortcutMenuItemRadiogroupName + "']:checked").val();
            if (syncGuid) {
                $.getJSON(
                    syncButtonConfig.syncControllerUri + "/" + syncGuid,
                    function (data) {
                        $("#" + syncButtonConfig.syncMessageId).text("Sync Command Was Sent Successfully!");
                        $("#" + syncButtonConfig.syncMessageId).fadeIn('fast', function () {
                            timeoutId = setTimeout('$("#" + syncButtonConfig.syncMessageId).fadeOut("slow");', 3000);
                        });
                    }
                )
                .fail(function (xhr, ajaxOptions, thrownError) {
                    $("#" + syncButtonConfig.syncMessageId)
                        .addClass("red")
                        .text("Sync Command Failed!");
                    $("#" + syncButtonConfig.syncMessageId).fadeIn('fast', function () {
                        timeoutId = setTimeout('$("#" + syncButtonConfig.syncMessageId).fadeOut("fast").removeClass("red");', 3000);
                    });
                });

            }
        });
    }

    return {
        addSyncButtonEvent: addSyncButtonEvent,
    }
}