﻿var defaultFilterText = "" //"Find Text ...",
var tableScrollHeight = function () { return ($("#content-body").height() - 145) }
var tooltipDelay = 500; // half sec

var _redrawTimeoutID = null;
var _redrawTimeoutDelay = 1200;

var _tableId;
var _buttonsStateDict = {};

function setupUlog(tableCloneId, tableId) {
    // populate the _buttonsStateDict
    $("#filters span").each(function (i, span) {
        _buttonsStateDict[$(span).attr("key")] = $(span).hasClass("selected");
    });

    // Custom Extension for Log Level filtering
    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {

            var tr = oSettings.aoData[iDataIndex].nTr;
            var rowKey = $(tr).attr("key");

            if (rowKey) {
                var rowKeys = [];
                if (rowKey.indexOf('_') != -1) {
                    var rowKeys = rowKey.split('_');
                }
                else {
                    rowKeys.push(rowKey);
                }

                var include = true;
                $.each(rowKeys, function (k, key) {
                    if (_buttonsStateDict.hasOwnProperty(key) && (_buttonsStateDict[key] == false)) {
                        include = false;
                    }
                });

            }
            if (include) {
                formatDate(tr, false);
            }

            return include;
        }
    );

    _tableId = tableId;

    var oTable = $("#" + _tableId).DataTable({
        "scrollX": "100%",
        "scrollXInner": "100%",
        "scrollY": tableScrollHeight(),
        "scrollCollapse": true,
        "deferRender": true,
        "ordering": true,           // Enable Sorting
        "pageLength": 25,
        "paging": true,            // Enable Pagination
        "searching": true,          // Enable Filter
        "info": true,              // Enable Info
        "order": [[1, "asc"]],
        "columnDefs": [
            { "targets": [0], "sortable": false }
        ],
        "stateSave": false, // save the state of the table
        "autoWidth": true,
        "dom": 'CrtiS',
        //"processing": true,
        "oColVis": {
			"buttonText": "Visible Columns",
        },
        "initComplete": function (oSettings, json) {
            $("div.ColVis button")
                .addClass("table-title-button")
                .appendTo("#colvis-button");
        },
    });

    // Configure the filter input text
    $("#filter")
        .val(defaultFilterText)
		.focus(function() {
		    if ($(this).val() == defaultFilterText)
		        $(this).val("");
		})
		.blur(function() {
		    if ($(this).val() == "")
		        $(this).val(defaultFilterText);
		})
		.keyup(function() {
		    oTable.search($(this).val());
		    refreshTable(reset = false);
		});
   
    // Setup the log level filters
    $("#filters .filter-wrap").click(function () {
        $(this).toggleClass("selected");
        _buttonsStateDict[$(this).attr("key")] = $(this).hasClass("selected");
        refreshTable(reset = false);
    });
    
    // Setup the job info tooltip
    $("#job-type").tooltip({
        items: "span",
        tooltipClass: "job-info-tooltip",
        position: { my: "left top", at: "left bottom" },
        content: function() {
            return $(".job-info-wrapper").html();
        },
        show: {
            effect: "fadeIn",
            delay: tooltipDelay,
        },
        open: function() {
            //debugger;
        },
        close: function (event, ui) {
            var me = this;
            ui.tooltip.hover(
                function () {
                    $(this).stop(true).show();
                },
                function () {
                    $(this).fadeOut("slow", function () {
                        $(this).remove();
                    });
                }
            );
        },
    });

    //Update the table's format
    updateTable(tableCloneId, oTable);

    // Redraw on window resize
    $(window).resize(function () {
        updateScrollHeight(oTable);
    });

    // Show the table - chrome fix
    $("#" + tableCloneId).remove();
}

function formatDate(tr, utc) {
    var td = $(tr).find("td.timestamp-cell");
    // treat the given date as utc so subtract the Timezone offset
    var date = new Date(new Date(td.attr("title")).getTime() - new Date().getTimezoneOffset()*60*1000);

    //td.text((isValidDate(date)) ? getDatetimeString(date, utc) : "");
    td.text((isValidDate(date)) ? toEuropeanFormat(date, utc) : "");
}

function updateTable(tableId, oTable) {
    var rows = $("#" + tableId + " tbody tr");

    $.each(rows, function (i, row) {
        oTable.row.add(row);
    });
    oTable.draw(false);
}

function updateScrollHeight(oTable) {
    oTable.settings()[0].oScroll.sY = tableScrollHeight();
    refreshTable(reset = false);
}

function refreshTable(reset) {
    var oTable = $("#" + _tableId).DataTable();

    clearTimeout(_redrawTimeoutID);
    _redrawTimeoutID = setTimeout(function () {
            oTable.draw(reset);
        },
        _redrawTimeoutDelay
    );
}
