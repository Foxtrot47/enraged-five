﻿////////////////////////////////// From users.js /////////////////////////////
var usersInfo = {};

function processUsersInfo() {
    $.ajax({
        url: automationConfig.usersPath + automationConfig.usersXml,
        type: "GET",
        dataType: "xml",

        success: function (xml, textStatus, jqXHR) {

            $(xml).find("User").each(function () {

                usersInfo[$(this).find("UserName").text().toLowerCase()] = {
                    Name: $(this).find("Name").text(),
                    Email: $(this).find("Email").text(),
                    JobTitle: $(this).find("JobTitle").text(),
                    ImageFilename: $(this).find("ImageFilename").text(),
                }

            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.error("Failed loading content");
        },
        complete: function () {
        }
    });
}

function appendUserInfoDiv(username, element) {
    var user = usersInfo[username.toLowerCase()];

    if (typeof user !== "undefined") {
        var userDiv = $("<div />");
        
        userDiv.addClass("user")
			.append(
                $("<div />")
                    .addClass("img-wrap")
                    .append(
			            $("<img />").attr("src", automationConfig.usersPath + user.ImageFilename)
                    )
			)
			.append(
                $("<div />")
					.addClass("user-info")
					.append(
						$("<div />")
							.addClass("user-info-name")
							.html(user.Name.replace(" (", "<br />("))
					)
					.append(
						$("<div />")
							.addClass("user-info-rest")
							.html(user.Email + "<br />" + user.JobTitle)
					)
			);

        var offset = $(element).offset();
        var x = offset.left;
        var y = ((offset.top + userDiv.height()) <= $(window).height())
            ? (offset.top + userDiv.height())
            : (offset.top - userDiv.height());

        userDiv.position({ "left": x, "top": y });

        userDiv.appendTo(element);
    }
    else {
        $(element).attr("title", username.toLowerCase());
    }

}

//////////////////////////////////// From clients.js ///////////////////////////////////////

/*
var coordinatorLogMessageCount = "/automation.svc/admin/CoordinatorLogMessageCount",
	coordinatorLogUrl = "/automation.svc/admin/CoordinatorLog",
	ulogUrl = "/ulogs/index.html";
*/

var firstload;

/*
var clientStateEnum,
    taskStateEnum,
    capabilityTypeEnum,
    jobstateenum,
	jobpriorityenum;
*/

var jobJson,
	jobResultsJson;

var utc;
var timezoneOffset = new Date().getTimezoneOffset() / 60; // hours offset from UTC

// The timezoneOffset is the hours difference 
// form our current timezone to UTC
var timezoneString = "(Local Time " + ((timezoneOffset >= 0) ? "+" : "") + timezoneOffset + ")";
//var elId, serverHost, webHost, ulogHost;
var automationServiceObj;

var _intervalId = null;

function initAutomation(automationServiceObject) {
    automationServiceObj = automationServiceObject;
    firstload = true;

    drawLoadingBox();
    processUsersInfo();
    buildPage();

    setUpdateInterval();
}

function setUpdateInterval() {
    _intervalId = setInterval(buildPage, automationConfig.reloadInterval);
    //console.log(_intervalId);
}
function cancelUpdateInterval() {
    clearInterval(_intervalId);
    //console.log(_intervalId + " canceled");
}

function buildPage() {
    /*
    if (firstload) {
        buildHtml();
    }
    */
    getData();
    //getTasks();
    //getClients();
}

/**
** Draws the loading rectangle animated box in the middle of the page
**/
function drawLoadingBox() {
    $("#" + automationServiceObj.elementId).empty();
    $("#" + automationServiceObj.elementId)
        .append(
            $("<div>").addClass("content-loading")
        );
}

/**
** Display the error message at the centre of the screen when the server cannot be accessed
**/
function drawLoadingError() {
    $("#" + automationServiceObj.elementId).empty();
    $("#" + automationServiceObj.elementId)
        .append(
            $("<div>")
                .addClass("content-loading-error")
                .html("Error connecting to '" + automationServiceObj.serverHost + "'"
                    + "<br/>"
                    + "Will retry in " + (automationConfig.reloadInterval / 1000) + " seconds"
                )
        );
}


function getClients() {
    /* DEPRECATED
    $.ajax({
        type: "GET",
        url: automationServiceObj.webHost + automationConfig.clientsUrlPath,
        dataType: "json",
        success: function (json) {

            var table1 = $("#clientlist1").DataTable();
            table1.off("draw").on("draw", updateClientsTable);
            // delete any events and clear the table
            table1.clear();

            $.each(json, function (i, clientstatus) {
                table1.row.add([i,
                          clientstatus.ID,
                          clientstatus.ServiceConnection,
                          clientStateEnum[clientstatus.State],
                          clientstatus.AssignedJob
                ],
                          false);
            });

            table1.draw(false);
        }
    });
    */ 
    //setTimeout(onTimerClients, automationConfig.reloadInterval);
}

//Called when the tasks table is being redrawn - adds additional styling and hover user/files info
function updateClientsTable() {
    var clientRows = $("#clientlist1 tr");
    $.each(clientRows, function (i, clientRow) {
        // add the user div to the job-id's column 
        var jobIdTd = $(clientRow).find("td.job-id");

        jobIdTd.unbind().bind("mouseover", function (e) {
            if (jobJson[$(this).text()])
                appendUserInfoDiv(jobJson[$(this).text()].Trigger.Username, $(this));
            $(this).children("div.user").fadeIn("fast");
        });
        jobIdTd.bind("mouseleave", function (e) {
            $(this).children("div.user")
				.fadeOut("fast")
				.remove();
        });
    });
}

function initClients() {
    //get the client state enum list
    /* DEPRECATED
    $.ajax({
        type: "GET",
        url: automationServiceObj.webHost + automationConfig.enumsPath,
        data: { type: automationConfig.clientStateEnumDataType },
        dataType: "json",
        async: false,
        success: function (json) {
            clientStateEnum = {};
            $.each(json, function (i, item) {
                clientStateEnum[item.Key] = item.Value;
            });
        }
    });
    */

    //create the tables with the correct headers
    var clientTableData = {
        "columns": [],
        "stateSave": true,
        "autoWidth": false,
        "searching": false,
        "info": false,
        "paging": false,
    };

    clientTableData.columns.push({ "title": "Index", "width": "2%" });
    clientTableData.columns.push({ "title": "ID", "width": "29%" });
    clientTableData.columns.push({ "title": "Connection", "width": "35%" });
    clientTableData.columns.push({ "title": "State", "width": "5%" });
    clientTableData.columns.push({ "title": "Job", "width": "29%", "sClass": "job-id" });
    
    $("#clientlist1").DataTable(clientTableData);

    //addCoordinatorLog();
}

/*
function addCoordinatorLog() {
    $("#coordinator-log").append(
		$("<a>").attr("href", automationServiceObj.webHost + ulogUrl + "?ulog=" + coordinatorLogUrl)
				.text("Open Coordinator Log")
				.attr("title", "Open Coordinator Log")
				.attr("target", "_blank")
	)
}
*/

//////////////////////////////////// From jobdetail.js /////////////////////////////////////

function getDatetimeString(date, utc) {
    if (date.getFullYear() >= automationConfig.notProcessedYear)
        return "";

    return ((utc) ? date.toUTCString() : (date.toDateString() + " " + date.toLocaleTimeString()));
}

// Return the difference of two date objects in secs
/*
function datesDiffInSecs(date1, date2) {
    return (((date1.getFullYear() == automationConfig.notProcessedYear) || (date1.getTime() < date2.getTime())))
                                ? 0
                                : ((date1.getTime() - date2.getTime()) / 1000);
}
*/

function formatSecs(secs) {
    var zeros = d3.format("02d");

    var h = zeros(Math.floor(secs / 3600));
    var m = zeros(Math.floor((secs % 3600) / 60));
    var s = zeros(Math.floor((secs % 3600) % 60));

    return (h + ":" + m + ":" + s);
}

function getChangelistLink(changelist) {
    var link;
    var urlPrefix = "http://rsgedip4s1:8080/@md=d&cd=//&c=7FM@/";
    var urlSuffix = "?ac=10";

    if (changelist) {
        if (automationConfig.submissionStates.hasOwnProperty(changelist)) {
            link = automationConfig.submissionStates[changelist];
        }
        else {
            link = $("<a />", {
                text: changelist,
                title: "View Changelist on Perforce Web",
                href: urlPrefix + changelist + urlSuffix,
                target: "_blank",
            }).prop("outerHTML");
        }
    }
    else
        link = "-";

    return link;
}

function createBugstarLinks(text) {
    // Search for only one bugstar url as there is one bug per changelist
    if (!text)
        return "";

    var bugstarUrlPattern = /url:bugstar:\d+/i;
    var matches = text.match(bugstarUrlPattern);

    if (matches) {
        var link = $("<a />", {
            text: matches[0],
            title: "Open in Bugstar client",
            href: matches[0].replace("url:", ""), // Remove the "url:" prefix
            //target: "_blank",
        })
		.prop("outerHTML");

        text = text.replace(matches[0], link)
    }

    return text;
}

function addJob(guid) {
    //var tabs = $("#tabs").tabs();

    if ($("#jobpage" + guid).length === 0) {
        //create the html for the tab
        var newjobpage = $("#jobpage").clone();
        newjobpage[0].id = "jobpage" + guid;

        var li = "<li><a href='#" + newjobpage[0].id + "'>"
                    + "Job " + guid
                    + "<span id='#" + newjobpage[0].id + "' class='ui-icon ui-icon-close'>Remove Tab</span>"
                    + "</a></li>";
        $("#tabs").find(".ui-tabs-nav").append(li);
        $("#tabs").append(newjobpage);
        $("#tabs").tabs("refresh");
        $("#tabs").tabs("option", "active", -1);

        //$("#tabs").tabs("add", jobid, "Job " + guid + "<span id='" + jobid 
        //				+ "' class='ui-icon ui-icon-close'>Remove Tab</span>");

        $("#tabs span.ui-icon-close").unbind().click(function (e) {
            e.preventDefault(); // Don't add the job id to the url

            // Remove the panel's page which will become hidden
            $("#tabs " + $(this).attr("id")).remove();
            // Remove the tab (html li)
            $(this).closest("li").remove();

            // Refresh the tabs and select the last one (doesn't always work (why?))
            $("#tabs").tabs("refresh");
            $("#tabs").tabs("option", "active", -1);
        });

        updateJob(guid);
        //$("#tabs").tabs("select",  - 1);
    }
    else {
        var index = $("#tabs a[href='#jobpage" + guid + "']").parent().index();
        $("#tabs").tabs("option", "active", index);
    }
};

function updateJob(guid) {
    var node = $("#tabs #jobpage" + guid);
    //node.id = node.id + index;

    var job = jobJson[guid];

    var hout = "Type: " + job.__type;
    hout += "<br/>ID: " + job.ID;
    hout += "<br/><br/>Trigger: ";

    if (job.Trigger.__type == "RSG.Pipeline.Automation.Common.Triggers.UserRequestTrigger, RSG.Pipeline.Automation.Common") {

        job.Trigger.TriggeredAt = (job.Trigger.TriggeredAt) ? job.Trigger.TriggeredAt
                                                            : job.Trigger.SubmittedAt;

        hout += "User request by <span class='username'>" + job.Trigger.Username + "</span>";
        hout += " at " + getDatetimeString(parseJsonDate(job.Trigger.TriggeredAt), utc);

    }
    else {
        hout += "Changelist " + getChangelistLink(job.Trigger.Changelist);
        hout += " checked in by <span class='username'>" + job.Trigger.Username + "</span> (" + job.Trigger.Client + ")";
        hout += " at <i>" + getDatetimeString(parseJsonDate(job.Trigger.TriggeredAt), utc) + "</i>";
        hout += "<br/><br/>Description: " + createBugstarLinks(job.Trigger.Description);
    }

    //hout += "<br><br>Processing State: " + jobstateenum[job.State];
    hout += "<br><br>Processing State: " + job.State;
    hout += "<br>Processing Host: " + ((job.ProcessingHost) ? job.ProcessingHost : "");

    if (job.State == "Completed") {

        hout += "<br/>Processing Finished At: " + getDatetimeString(parseJsonDate(job.ProcessedAt), utc);
        hout += "<br/>Processing Time: "
            + formatSecs(datesDiffInSecs(parseJsonDate(job.CompletedAt), parseJsonDate(job.ProcessedAt)));
    }
    else {
        //hout += "<br>Priority: " + jobpriorityenum[job.Priority];
        hout += "<br>Priority: " + job.Priority;
    }

    hout += "<br/><br/>";
    hout += "&nbsp;<span id='action-buttons'></span>";
    hout += "&nbsp;<span id='action-status'></span>";
    hout += "<br/><br/>";

    node.children("#jobdetail").html(hout);

    var buttonsId = "#jobpage" + job.ID + " #action-buttons";
    var statusId = "#jobpage" + job.ID + " #action-status";
    $.each(automationConfig.jobActions, function (action, actionProps) {
        $(buttonsId)
            .append(
                $("<button>")
                    .attr("title", action)
                    .attr("disabled", actionProps.disabled)
                    .text(action)
                    .on("click", function () {
                        performJobAction(job.ID, action, actionProps, statusId)
                    })
            )
    });

    var userElement = node.children("#jobdetail").find(".username");

    appendUserInfoDiv(job.Trigger.Username, userElement);
    $(userElement).unbind().hover(function () {
        $(this).children("div.user").fadeIn("fast");
    },
	function () {
	    $(this).children("div.user").fadeOut("fast");
	});

    //$("button").button();

    /*
    $("#jobdetail button").button();
    var currentJobDiv = "#jobpage" + job.ID + " #action-status";

    node.find("#clpri").click(function() {

        $.ajax({
            type: "GET",
            url: webHost + automationConfig.prioritiseAdminPath,
            data: { job: job.ID },
            dataType: "json",
            success: function (json) {
                $(currentJobDiv)
					.removeClass("failed").addClass("success")
					.html("'Prioritise' action for <i>" + job.ID + "</i> was sent successfully.");
            },
            error: function () {
                $(currentJobDiv)
					.html("'Prioritise' action  for <i>" + job.ID + "</i> has failed!")
					.removeClass("success").addClass("failed");
            },
            complete: function() { }
        });

    });
    // Disabling the prioritise button
    node.find("#clpri").attr("disabled", true);

    node.find("#clskip").click(function () {

        $.ajax({
            type: "GET",
            url: webHost + automationConfig.skipAdminPath,
            data: { job: job.ID },
            dataType: "json",
            success: function (json) {
                $(currentJobDiv)
                    .removeClass("failed").addClass("success")
                    .html("'Skip' action for <i>" + job.ID + "</i> was sent successfully.");
            },
            error: function () {
                $(currentJobDiv)
                    .html("'Skip' action for <i>" + job.ID + "</i> has failed!")
                    .removeClass("success").addClass("failed");
            },
            complete: function () {
            }
        });
    });

    node.find("#clforce").click(function() {

        $.ajax({
            type: "GET",
            url: webHost + automationConfig.reprocessAdminPath,
            data: { job: job.ID },
            dataType: "json",
            success: function (json) {
                $(currentJobDiv)
                    .removeClass("failed").addClass("success")
                    .html("'Force' action for <i>" + job.ID + "</i> was sent successfully.");
            },
            error: function () {
                $(currentJobDiv)
                    .html("'Force' action  for <i>" + job.ID + "</i> has failed!")
                    .removeClass("success").addClass("failed");
            },
            complete: function () {}
        });
    });
    */

    addFilesListTable(job, node.children("#list3"));
}

function performJobAction(jobId, actionName, actionProps, statusId) {
    var succeed = false;
    
    $.ajax({
        type: "GET",
        url: automationServiceObj.automationAdminRest,
        data: { 
            actionName: actionProps.actionName, 
            jobID: jobId 
        },
        dataType: "json",
        async: false,
        success: function (actionResult) {
            succeed = true;
            applyJobActionResultClass(jobId, actionName, statusId, actionResult.Status);
        },
        error: function () {
            applyJobActionResultClass(jobId, actionName, statusId, false);
        },
    });

    return succeed;
}

function applyJobActionResultClass(jobId, actionName, statusId, succeed) {
    if (succeed) {
        if (typeof statusId != "undefined") {
            $(statusId)
                .removeClass()
                .html("'" + actionName + "' action for <i>" + jobId + "</i> was sent successfully.")
                .addClass("success");
        }
    }
    else {
        if (typeof statusId != "undefined") {
            $(statusId)
                .removeClass()
                .html("'" + actionName + "' action  for <i>" + jobId + "</i> has failed!")
                .addClass("failed");
        }
    }
}

// Adds a dataTable with the files of the current job to a given jQuery element
function addFilesListTable(job, element) {
    var tabledata = {
        "columns": [],
        "stateSave": true,
        "searching": false,
        "info": false,
        "paging": false,
    };
    tabledata.columns.push({ "title": "Files" });
    var table3 = element.DataTable(tabledata);

    table3.clear();
    $.each(job.Trigger.Files, function (i, file) {
        table3.row.add([file], false);
    });
    table3.draw(false);
}

//////////////////////////////////// From tasks.js /////////////////////////////////////////

//function onTimerTasks(runOnce) {
function getData() {
    utc = $("#utc-force").is(":checked");

    $.ajax({
        type: "GET",
        //url: automationServiceObj.webHost + automationConfig.monitorUrlPath,
        url: automationServiceObj.automationMonitorRest,
        dataType: "json",
        success: function (restResult) {

            if (firstload) {
                buildHtml();
            }

            // Populate the Tasks
            jobJson = {};
            jobResultsJson = {};

            var table1 = $("#tasklist1").DataTable();
            $("#tasklist1").children("tbody").find("*").unbind();
            table1.off("draw").on("draw", updateTasksTable);
            table1.clear();

            $.each(restResult.Monitor, function (i, item) {
                table1.row.add(
                    [
                    i + 1,
                    item.Name,
                    //(item.Role && capabilityTypeEnum.hasOwnProperty(item.Role)) ? capabilityTypeEnum[item.Role] : "",
                    (item.Role) ? item.Role : "",
                    item.Jobs.length,
                    item.AvailableJobCount,
                    /*
                    (item.TaskState && taskStateEnum.hasOwnProperty(item.TaskState))
                            ? "<button title='Click to change State'>" + taskStateEnum[item.TaskState] + "</button>"
                            : "",
                    */
                    (item.TaskState) ? "<button title='Click to change State'>" + item.TaskState + "</button>" : "",
                    ],
                    false);
            });
            table1.draw(false);

            var table2 = $("#tasklist2").DataTable();
            $("#tasklist2").children("tbody").find("*").unbind();
            table2.off("draw").on("draw", updateJobsTable);
            table2.clear();

            var taskDetails = restResult.Monitor;

            // process jobs results
            $.each(taskDetails, function (i, taskDetail) {
                $.each(taskDetail.JobResults, function (j, jobResult) {
                    if (jobResultsJson.hasOwnProperty(jobResult.JobId))
                        jobResultsJson[jobResult.JobId].push(jobResult.SubmittedChangelistNumber);
                    else
                        jobResultsJson[jobResult.JobId] = [jobResult.SubmittedChangelistNumber];
                });
            });

            $.each(taskDetails, function (taskIndex, taskDetail) {
                $.each(taskDetail.Jobs, function (j, taskJob) {
                    // Filter the jobs if specified
                    if (automationServiceObj.jobFilterKey
                        && taskJob.hasOwnProperty(automationServiceObj.jobFilterKey)
                        && taskJob[automationServiceObj.jobFilterKey] != automationServiceObj.jobFilterValue) {
                        return; // This is equivalent to continue;
                    }

                    //if (!$("input#" + jobstateenum[taskJob.State]).is(":checked")) {
                    //    return;
                    //}

                    jobJson[taskJob.ID] = taskJob;
                    //var state = (jobstateenum.hasOwnProperty(taskJob.State)) ? jobstateenum[taskJob.State] : "";
                    var state = (taskJob.State) ? taskJob.State : "";

                    taskJob.Trigger.TriggeredAt = (taskJob.Trigger.TriggeredAt) ? taskJob.Trigger.TriggeredAt
                                                                                : taskJob.Trigger.SubmittedAt;

                    var completedAtDate = parseJsonDate(taskJob.CompletedAt);

                    table2.row.add([
                        "<input type='checkbox' value='" + taskJob.ID + "' title='Select / Deselect Row' class='select-row-cb'>",
                        getChangelistLink(taskJob.Trigger.Changelist),
				        //truncate(taskJob.ID, 15),
                        taskIndex+1,
                        taskJob.ID,

				        (typeof taskJob.Trigger.Username != "undefined") ? taskJob.Trigger.Username : "",
				        //(typeof taskJob.Priority != "undefined") ? jobpriorityenum[taskJob.Priority] : "",
                        (typeof taskJob.Priority != "undefined") ? taskJob.Priority : "",

                        getDatetimeString(parseJsonDate(taskJob.Trigger.TriggeredAt), utc),
				        getDatetimeString(completedAtDate, utc),

				        formatSecs(datesDiffInSecs(completedAtDate, parseJsonDate(taskJob.Trigger.TriggeredAt))),
				        formatSecs(datesDiffInSecs(completedAtDate, parseJsonDate(taskJob.ProcessedAt))),
                        ((taskJob.ProcessingHost) ? taskJob.ProcessingHost : ""),

                        $("<div>").append(
                            (automationConfig.nonUlogStates.hasOwnProperty(state))
                                    ? $("<div>").addClass("state-text").text(state)
                                    : $("<div>").addClass("relative")
                                        .append(
                                            $("<a>")
                                                .addClass("state-text")
                                                .attr("href", automationServiceObj.ulogHost + "?"
                                                        + $.param({
                                                            "ServerHost": automationServiceObj.serverHost, "JobId": taskJob.ID,
                                                            "ErrorsOnly": ((state == automationConfig.errorStateName) ? true : false),
                                                        })
                                                )
                                                .attr("target", "_blank")
                                                .attr("title", "View Log")
                                                .text(state)
                                        )
                                        .append(
                                            $("<a>")
                                                .attr("href", automationServiceObj.ulogGetHost + "?"
                                                        + $.param({
                                                            "ServerHost": automationServiceObj.serverHost, "JobId": taskJob.ID,
                                                        })
                                                )
                                                .append(
                                                    $("<img>")
                                                        .addClass("download-ulog")
                                                        .attr("src", automationServiceObj.imagesRootURI 
                                                                    + "/" 
                                                                    + automationConfig.iconMappings["ulogGet"]
                                                                )
                                                        .attr("title", "Download Log")
                                                )
                                        )
                        ).html(),

				        ((jobResultsJson.hasOwnProperty(taskJob.ID)) ? getJobResultsList(jobResultsJson[taskJob.ID]) : ""),
				         "<button title='View Job Details'>View</button>"
                    ], false);

                });
            });

            table2.draw(false);

            // Populate The Clients
            //getClients();
            var table1 = $("#clientlist1").DataTable();
            table1.off("draw").on("draw", updateClientsTable);
            // delete any events and clear the table
            table1.clear();

            $.each(restResult.Clients, function (i, clientstatus) {
                table1.row.add([
                          i+1,
                          clientstatus.ID,
                          clientstatus.ServiceConnection,
                          //clientStateEnum[clientstatus.State],
                          clientstatus.State,
                          (clientstatus.AssignedJob) ? (clientstatus.AssignedJob) : "",
                ],
                          false);
            });

            table1.draw(false);
            // End of populating the clients

            firstload = false;
        }, // end of ajax success:
        error: function () {
            firstload = true;
            drawLoadingError();
        },
    });
}

function buildHtml() {
    $("#" + automationServiceObj.elementId).empty();

    $("#" + automationServiceObj.elementId).append(
        $("<div>")
            .attr("id", "tabs")
            .append(
                $("<ul>")
                    .append(
                        $("<li>")
                            .append(
                                $("<a>")
                                    .attr("href", "#tabs-2")
                                    .text("Tasks")
                            )
                    )
                    .append(
                        $("<li>")
                            .append(
                                $("<a>")
                                    .attr("href", "#tabs-1")
                                    .text("Clients")
                            )
                    )
            ) // ul

            .append(
                $("<div>")
                    .attr("id", "tabs-1")
                    .append(
                        $("<div>")
                            .attr("id", "clients")
                            .append(
                                $("<div>")
                                    .attr("id", "clientlist")
                                    .append(
                                        $("<table>")
                                            .attr("id", "clientlist1")
                                    )
                            )
                    )
                    .append(
                        $("<br>")
                    )
                    .append(
                        $("<br>")
                    )
                    .append(
                        $("<div>")
                            .attr("id", "coordinator-log")
                    )
            ) // tabs-1

            .append(
                $("<div>")
                    .attr("id", "tabs-2")
                    .append(
                        $("<div>")
                            .attr("id", "tasks")
                            .append(
                                $("<div>")
                                    .attr("id", "tasklist")
                                    .append(
                                        $("<table>")
                                            .attr("id", "tasklist1")
                                    )
                            ) // tasklist
                            .append(
                                $("<br>")
                            )
                            .append(
                                $("<div>")
                                    .attr("id", "joblist")
                                    .append(
                                        $("<div>")
                                            .attr("id", "jobfilters")
                                    )
                                    .append(
                                        $("<br>")
                                    )
                                    .append(
                                        $("<div>")
                                            .attr("id", "jobactions")
                                    )
                                    .append(
                                        $("<table>")
                                            .attr("id", "tasklist2")
                                            .mouseover(cancelUpdateInterval)
                                            .mouseout(setUpdateInterval)
                                    )
                            ) // joblist
                    ) // tasks                     
            ) // tabs-2

        ); // tabs
    //<!-- end of #content-body -->

    $("#jobfilters").text("Filter by State: ");
    $.each(automationConfig.jobFilters, function (i, filter) {
        $("#jobfilters").append(
            $("<span>")
                .addClass(filter.toLowerCase())
                .append(
                    $("<input>")
                        .attr("type", "checkbox")
                        .attr("id", filter)
                        .attr("checked", "checked")
                )
                .append(
                    $("<label>")
                        .attr("for", filter)
                        .attr("title", filter)
                        .text(filter)
                )
        )
    });
    $("#jobfilters").append(
        $("<span>")
            .addClass("float-right")
            .append(
                $("<input>")
                    .attr("type", "checkbox")
                    .attr("id", "utc-force")
            )
            .append(
                $("<label>")
                    .attr("for", "utc-force")
                    .attr("title", " Display Dates In UTC")
                    .html("Display Dates In UTC<span id='utc-tz'></span>")
            )
    );

    $("#jobactions")
        .append(
            $("<label>")
                .html("For selected jobs send action: ")
        )
        .append(
            $("<select>")
        );

    $.each(automationConfig.jobActions, function (action, actionProps) {
        $("#jobactions select")
            .append(
                $("<option>")
                    .attr("value", action)
                    .text(action)
                    .attr("disabled", actionProps.disabled)
            )
    });
    $("#jobactions")
        .append(
            $("<span>")
        )
        .append(
            $("<button>")
                .attr("title", "Go")
                .text("Go")
                .click(performActionOnSelectedJob)
        );

    $("#" + automationServiceObj.elementId).append(
        $("<div>")
            .css("display", "none")
            .append(
                $("<div>")
                    .attr("id", "jobpage")
                    .append(
                        $("<div>")
                            .attr("id", "jobdetail")
                    )
                    .append(
                        $("<table>")
                            .attr("id", "list3")
                    )
            )
    );

    $("#tabs").tabs();

    initTasks();
    initClients();
}

function initTasks() {

    $("#utc-tz").text(timezoneString); // set the timezone text
    //get the jobstate enum list

    /* DEPRECATED
    $.ajax({
        type: "GET",

        url: automationServiceObj.webHost + automationConfig.enumsPath,
        data: { type: automationConfig.jobStateEnumDataType },
        dataType: "json",
        async: false,
        success: function (json) {
            jobstateenum = {};

            $.each(json, function (i, item) {
                jobstateenum[item.Key] = item.Value;
            });
        }
    });

    $.ajax({
        type: "GET",
        url: automationServiceObj.webHost + automationConfig.enumsPath,
        data: { type: automationConfig.jobPriorityEnumDataType },
        dataType: "json",
        async: false,
        success: function (json) {
            jobpriorityenum = {};

            $.each(json, function (i, item) {
                jobpriorityenum[item.Key] = item.Value;
            });
        }
    });

    $.ajax({
        type: "GET",
        url: automationServiceObj.webHost + automationConfig.enumsPath,
        data: { type: automationConfig.taskStateEnumDataType },
        dataType: "json",
        async: false,
        success: function (json) {
            taskStateEnum = {};

            $.each(json, function (i, item) {
                taskStateEnum[item.Key] = item.Value;
            });
        }
    });

    $.ajax({
        type: "GET",
        url: automationServiceObj.webHost + automationConfig.enumsPath,
        data: { type: automationConfig.capabilityTypeEnumDataType },
        dataType: "json",
        async: false,
        success: function (json) {
            capabilityTypeEnum = {};

            $.each(json, function (i, item) {
                capabilityTypeEnum[item.Key] = item.Value;
            });
        }
    });
    */

    //create the tables with the correct headers

    var tabledataList1 = {
        "columns": [],
        "autoWidth": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "paging": false
    };

    tabledataList1.columns.push({ "title": "Index", "width": "10%" });
    tabledataList1.columns.push({ "title": "Task", "width": "30%" });
    tabledataList1.columns.push({ "title": "Role", "className": "role", "width": "30%" });
    tabledataList1.columns.push({ "title": "Total Jobs", "className": "middle", "width": "10%" });
    tabledataList1.columns.push({ "title": "Open Jobs", "className": "middle", "width": "10%" });
    tabledataList1.columns.push({ "title": "State", "className": "middle", "width": "10%" });

    $("#tasklist1").DataTable(tabledataList1);

    var tabledataList2 = {
        "columns": [],
        "stateSave": true,
        "stateLoadParams": function (settings, data) {
            data.search.search = automationServiceObj.jobId;
        },
        "autoWidth": false,
        "processing": true,
        "pageLength": 25,
        "order": [[6, "desc"]], // 2 Columns are hidden
        "columnDefs": [
            { "targets": [0], "sortable": false,  }
        ],
        "pagingType": "simple_numbers",
        "dom": '<"top"plf>rt<"bottom"ip<"clear">>'
    };
    tabledataList2.columns.push({ "title": "<input title='Select / Deselect All Rows' type='checkbox' id='select-all-cb' />", "className": "" });
    tabledataList2.columns.push({ "title": "Change list", "className": "data-chlist" });
    tabledataList2.columns.push({ "title": "Task", "visible": false, "className": "task-index" });
    tabledataList2.columns.push({ "title": "ID", "visible": false, "className": "data-id" });

    tabledataList2.columns.push({ "title": "User", "className": "data-username" });
    tabledataList2.columns.push({ "title": "Priority", "className": "" });

    tabledataList2.columns.push({ "title": "Triggered At", "width": "140", "type": "date" });
    tabledataList2.columns.push({ "title": "Completed At", "width": "140", "type": "date" });

    tabledataList2.columns.push({ "title": "Turnaround Time", "className": "" });
    tabledataList2.columns.push({ "title": "Processing Time", "className": "" });
    tabledataList2.columns.push({ "title": "Processing Host", "className": "" });

    tabledataList2.columns.push({ "title": "State", "className": "data-state", "width": "120" });
    tabledataList2.columns.push({ "title": "Submitted Changelists", "className": "" });
    tabledataList2.columns.push({ "title": "Action", "className": "data-view" });

    // Unbind all events before each draw - not sure if that works
    //tabledataList2["fnPreDrawCallback"] = function () { $("#tasklist2 tbody").find("*").unbind(); };

    /**
    ** Extension to hide the unchecked rows
    **/
    $.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            
            var tr = oSettings.aoData[iDataIndex].nTr;

            // Filter by task
            var rowTask = oSettings.aoData[iDataIndex]._aData[2];
            // When Row's task index is found but the task row is not selected
            if (rowTask && !$("#tasklist1").find("tr:nth-child(" + rowTask + ")").hasClass("selected")) {
                return false;
            }

            // Filter by state
            var rowState = $(tr).find(".data-state .state-text").text();

            if (rowState && !$("input#" + rowState).is(":checked")) {
                return false;
            }

            return true;
        }
    );

    var taskJobsTable = $("#tasklist2").DataTable(tabledataList2);
    
    $("#jobfilters input[type=checkbox]").change(function () {
        // Run only once to avoid breaking the periodical timeout
        //onTimerTasks(true);

        //getData();
        if ($(this).attr("id") == "utc-force") {
            getData();
        }
        else {
            taskJobsTable.draw(true);
        }
    });

    $("#select-all-cb").change(function () {
        if ($(this).is(":checked")) {
            $(".select-row-cb").prop("checked", true);
        }
        else {
            $(".select-row-cb").prop("checked", false);
        }
    });
}

function performActionOnSelectedJob() {
    var selectedAction = $("#jobactions select option:selected").val();
    var selectedActionProps = automationConfig.jobActions[selectedAction];

    $(".select-row-cb").each(function (i, cb) {
        if ($(cb).is(":checked")) {
            performJobAction($(cb).val(), selectedAction, selectedActionProps);
        }
    });

    // Update the list
    //onTimerTasks(true);
    getData();
    // Uncheck the select all cb - stays checked
    $("#select-all-cb").prop("checked", false);
}

// Called when the tasks table is being redrawn - adds additional events
function updateTasksTable() {
    var table1 = $("#tasklist1").DataTable();
    var rows = $("#tasklist1 tbody tr");
    //Select all rows by default
    $(rows).addClass("selected");
    var taskJobsTable = $("#tasklist2").DataTable();

    $.each(rows, function (i, row) {
        $(row).click(function () {
            $(this).toggleClass("selected");
            taskJobsTable.draw(true);
        });

        // State Button
        var role = $(row).find("td.role").text().trim();
        var button = $(row).find("td button");

        button.unbind().bind("click", function () {
            // When Active - try to inactivate
            if ($(this).text() == "Active") {
                $.ajax({
                    type: "GET",
                    url: automationServiceObj.webHost + automationConfig.stopTaskPath,
                    data: { taskname: role },
                    success: function (json) {
                        getData();
                    },
                    error: function () {
                        button.addClass("outline-red");
                    }
                });
            }
            // When Inactive or Unknown - try to activate
            else if (($(this).text() == "Inactive") || ($(this).text() == "Unknown")) {
                $.ajax({
                    type: "GET",
                    url: automationServiceObj.webHost + automationConfig.startTaskPath,
                    data: { taskname: role },
                    //dataType: "json",
                    success: function (json) {
                        getData();
                    },
                    error: function () {
                        button.addClass("outline-red");
                    }
                });
            }
        });

        button.button().attr("disabled", true);
    });

}

// Called when the jobs table is being redrawn - adds additional styling and hover user/files info
function updateJobsTable() {

    var table2 = $("#tasklist2").DataTable();

    var rows = $("#tasklist2 tbody tr");
    $.each(rows, function (i, row) {
        // Add coloured classes to each state
        var state = $(row).find("td.data-state .state-text").text().toLowerCase();
        $(row).removeClass().addClass(state);

        /* DEPRECATED
        var rowData = table2.fnGetData(this);
        var currentJobId = "";
        if (rowData) {
            currentJobId = rowData[2];
        }
        */
        var rowData = table2.row(row).data();
        var currentJobId = "";
        if (rowData) {
            currentJobId = rowData[3];
        }

        var chListTd = $(row).find("td.data-chlist");

        chListTd.unbind().bind("mouseenter", function (e) {
            if (jobJson[currentJobId]) {
                showJobFilesList(jobJson[currentJobId], $(this), e);
                var tableDiv = $(this).children("div.job-files");
                updateTooltipPosition(tableDiv, e);
                tableDiv.fadeIn("fast");
            }
        });
        chListTd.bind("mouseleave", function (e) {
            $(this).children("div.job-files")
				.fadeOut("fast")
				.remove();
        });

        // View Button
        var button = $(row).find("td.data-view button");

        button.unbind().bind("click", function () {
            addJob(currentJobId);
        });

        button.button();

        // User Info pop-up : add the user div to the respective table's td 
        var userTd = $(row).find("td.data-username");

        userTd.unbind().bind("mouseover", function (e) {
            appendUserInfoDiv($(this).text(), $(this));
            var userDiv = $(this).children("div.user");
            updateTooltipPosition(userDiv, e);
            userDiv.fadeIn("fast");
        });
        userTd.bind("mouseleave", function () {
            $(this).children("div.user")
				.fadeOut("fast")
				.remove();
        });

    });

}

function showJobFilesList(job, element, event) {
    var tableDiv = $("<div />")
			.addClass("job-files");

    var idTable = $("<table />");
    idTable.append(
		$("<tr />").append(
			$("<th />")
				.addClass("title")
                .attr("colspan", 2)
				.html("Job ID: " + job.ID)
		)
	);
    if (job.hasOwnProperty("ProjectName")) {
        idTable.append(
		    $("<tr />")
                .append(
			        $("<td />")
                        .addClass("label")
				        .html("Project Name: ")
		        )
                .append(
			        $("<td />")
				        .html(job.ProjectName)
		        )
        )
    }
    if (job.hasOwnProperty("BranchName")) {
        idTable.append(
		    $("<tr />")
                .append(
			        $("<td />")
                        .addClass("label")
                        .html("Branch Name: ")
		        )
                .append(
			        $("<td />")
                        .html(job.BranchName)
		        )
        )
    }
    idTable.appendTo(tableDiv);

    
    if (job.Trigger.Description) {
        var descTable = $("<table />");
        descTable.append(
            $("<tr />").append(
                $("<th />").html("Description")
            )
        );

        descTable.append(
            $("<tr />").append(
                $("<td />").html(createBugstarLinks(job.Trigger.Description)) // Added CL Description - b*#1187359
            )
        );

        descTable.appendTo(tableDiv);
    }
    
    if (job.Trigger.Files) {
        var filesTable = $("<table />");
        filesTable.append(
            $("<tr />").append(
                $("<th />").html("Files Processed")
            )
        );

        $.each(job.Trigger.Files, function (i, file) {
            $("<tr />")
                //.addClass(function() { return ((i%2) ? "odd" : "even"); })
                .append(
                    $("<td />").text(file)
                )
                .appendTo(filesTable)
        });

        filesTable.appendTo(tableDiv);
    }

    tableDiv.appendTo(element);
}

function getJobResultsList(jobResult) {
    return jobResult.map(function (d) { return getChangelistLink(d); }).join(",<br />")
}

function updateTooltipPosition(element, event) {
    var ttw = element.width(),
        tth = element.height(),

        offset = {
            x: 10,
            y: 0,
        },

        xScroll = $(window).scrollLeft(),
        yScroll = $(window).scrollTop(),

        curX = (document.all) ? event.clientX + xScroll : event.pageX,
        curY = (document.all) ? event.clientY + yScroll : event.pageY,

        left = ((curX - xScroll + ttw) > $(window).width()) ? curX - ttw : curX,
        top = ((curY - yScroll + tth) > $(window).height()) ? curY - tth : curY;

    element.css({ "top": top + offset.y, "left": left + offset.x });
}
