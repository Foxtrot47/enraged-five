﻿/*
* Class AutomationService
*
* Requires jQuery 
*
*/

var AutomationService = function (automationServiceObj) {
    var _automationLib = new AutomationLib();
    var _jobMatrixTitleElementId = "jobMatrixTitle";
    var _jobMatrixTableElementId = "jobMatrixTable";
    var _jobMatrixSummaryTableElementId = "jobMatrixSummaryTable";
    var _tableFilterBoxId = "table-filter-box";
    var _tableEntriesId = "table-entries";
    var _tableEntriesListId = "table-entries-list";
    var _firstload = true;
    var _jobMatrixLight = false;
    var _jobMatrixStatic = false;

    // css & class names
    var _tooltipClass = automationConfig.tooltipClass;
    var _stateTooltipClass = "state-tooltip";
    var _headerTooltipClass = "header-tooltip";
    var _emptyCellClass = "empty-cell";
    var _stateHeaderClass = "state-header-class";
    var _variationSuffix = "_variation";
    var _gradientSuffix = "_gradient";
    var _fullLabelAttr = "full-label";
    var _originalLabelKey = "originalLabel";
    var _highlightedTdClass = "highlighted-td";

    var _loadDataIntervalID = null;
    var _redrawTimeoutID;
    var _redrawTimeoutDelay = 1000;

    var _serviceType = automationServiceObj.serviceType.toLowerCase();

    var _usersInfo = {};

    var _consumerJobsDict = {};
    var _jobResults = {};
    
    var _groupedJobs = [];

    // Will store all the different groups found in _groupedJobs hierarchically
    var _columnGroups = {};
    var _columnGroupKeys = [];
    var _groupHierarchy = [];

    // The Description and triggered at columns
    var _nonStateColumnsNum = 2; 

    var _tooltipSetFlag = "tooltipset";

    var _toUnderscoreSplit = (automationConfig.toUnderBarSplitServiceTypes.hasOwnProperty(_serviceType) ? true : false);
    var _titleSplit = ((automationConfig.titleSplitServiceTypes.hasOwnProperty(_serviceType) && !_toUnderscoreSplit) ? true : false);

    var _initialHeaderHeight;
    var _getTableScrollHeight = function () {
        if (typeof _initialHeaderHeight === "undefined") {
            _initialHeaderHeight = $("#" + _jobMatrixTableElementId + " thead").height();
        }
        return (
                $("#content-body").height()
                - $("#" + automationServiceObj.tableId + " .title-only").height()
                - _initialHeaderHeight
                - 45
                );
    };

    var _fixedColumns = {
        0: {
            width: 55,
            class: "fixed-cl-width",
            visible: true,
        },
        1: {
            width: 120,
            class: "fixed-user-width",
            visible: true,
        },
        2: {
            width: 120,
            class: "fixed-buddy-width",
            visible: true,
        },
    };

    var _columnStates = {};

    var _boxEventsOverriden = false;
    var _generatedAtId = "generatedAt";
    var _generatedAt = new Date();
    var _toggleVisibilityButtonId = "toggle-visibility-button";
    var _columnVisibility = {};
    
    var _utcCbID = "utc-force";
    // The timezoneOffset is the hours difference 
    // form our current timezone to UTC
    var _timezoneOffset = new Date().getTimezoneOffset() / 60; // hours offset from UTC
    var _timezoneString = "(Local Time " + ((timezoneOffset >= 0) ? "+" : "") + timezoneOffset + ")";
    var _inUTC = true;

    // Enable icons by default
    var _iconsCbID = "icons-text-state";
    var _iconsState = true;

    var _pauseUpdateId = "pause-update";

    function initJobMatrixSummary() {
        //loadJobMatrix();
        drawLoadingBox();
        buildMatrixSummaryTable();
        _loadDataIntervalID = setInterval(buildMatrixSummaryTable, automationConfig.reloadInterval);
    }

    function initJobMatrix(jobMatrixType) {
        _jobMatrixStatic = (jobMatrixType == "JobMatrixStatic") ? true : false;
        // Light also included Static behaviour
        _jobMatrixLight = ((jobMatrixType == "JobMatrixLight") || _jobMatrixStatic) ? true : false;
        

        if (!_jobMatrixLight) {
            _usersInfo = _automationLib.processUsersInfo();
        }
        
        drawLoadingBox();
        buildMatrixTable();
    }

    function setBuildMatrixTableInterval() {
        if (!_loadDataIntervalID) {
            _loadDataIntervalID = setInterval(buildMatrixTable, automationConfig.reloadInterval);
            //console.log("set : " + _loadDataIntervalID);
        }
    }

    function clearBuildMatrixTableInterval() {
        if (_loadDataIntervalID) {
            //console.log("clear : " + _loadDataIntervalID);
            clearInterval(_loadDataIntervalID);
            _loadDataIntervalID = null;
        }
    }

    /**
    ** Draws the loading rectangle animated box in the middle of the page
    **/
    function drawLoadingBox() {
        $("#" + automationServiceObj.tableId).empty();
        $("#" + automationServiceObj.tableId)
            .append(
                $("<div>").addClass("content-loading")
            );
    }

    /**
    ** Display the error message at the centre of the screen when the server cannot be accessed
    **/
    function drawLoadingError() {
        $("#" + automationServiceObj.tableId).empty();
        $("#" + automationServiceObj.tableId)
            .append(
                $("<div>")
                    .addClass("content-loading-error")
                    .html(
                        //"Error connecting to '" + automationServiceObj.serverHost + "'"
                        ((!_jobMatrixStatic) ? "Error communicating with the Database" : "Error loading the local jobs file")
                        + "<br/>"
                        + "Will retry in " + (automationConfig.reloadInterval / 1000) + " seconds"
                    )
            );
    }
   
    /**
    ** Fetches the json data from the server and (re)populates the table
    **/
    function buildMatrixTable() {
        // Only if the pause updates checkbox is not checked
        if (!$("#" + _pauseUpdateId).prop("checked")) {

            if (!_firstload) {
                showUpdatingMessage();
            }

            $.ajax({
                type: "GET",
                url: automationServiceObj.automationGroupedRest,
                dataType: "json",
                success: function (data) {
                    buildDataStructures(data.AutomationGroupedJobs);
                    // Create the table only the first time
                    if (_firstload) {
                        generateMatrixTable();
                        if (!_jobMatrixLight) {
                            setupMatrixDatatables();
                        }
                        setupCheckboxEvents();
                    }
                    _generatedAt = parseJsonDate(data.AutomationGroupedJobs.GeneratedAt);
                    populateMatrixTable();
                    // Memory leak?
                    delete (data.AutomationGroupedJobs);
                },
                error: function (jqxhr, textStatus, error) {
                    _firstload = true;
                    hideUpdatingMessage();
                    drawLoadingError();
                }
            });
        } // End of if (!$("#" + _pauseUpdateId).prop("checked"))
    }

    function buildMatrixSummaryTable() {
        // Only if the pause updates checkbox is not checked
        if (!$("#" + _pauseUpdateId).prop("checked")) {

            $.ajax({
                type: "GET",
                url: automationServiceObj.automationGroupedRest,
                dataType: "json",
                success: function (data) {
                    buildDataStructures(data.AutomationGroupedJobs);
                    _generatedAt = parseJsonDate(data.AutomationGroupedJobs.GeneratedAt);
                    generateSummaryMatrixTable();
                    // Memory leak?
                    delete (data.AutomationGroupedJobs);
                },
                error: function (jqxhr, textStatus, error) {
                    drawLoadingError();
                }
            });
        } // End of if (!$("#" + _pauseUpdateId).prop("checked"))
    }

    /**
    ** Builds and populates the variable data structures used to generate the report
    **/
    function buildDataStructures(data) {
        // Save the consumer jobs dictionary and remove it from the list
        _consumerJobsDict = data.ConsumerJobs;
        _jobResults = data.JobResults;
        _columnGroups = data.ColumnGroups;
        if (_firstload) {
            _columnGroupKeys = Object.keys(_columnGroups)
                .sort(function (a, b) {
                    return (extractFileName(a.split(automationConfig.groupKeySeparator)[0]).toLowerCase()
                            > extractFileName(b.split(automationConfig.groupKeySeparator)[0]).toLowerCase()
                        ? 1 : -1);
                })
                .sort(); // final sort based on the whole string

            if (_columnGroupKeys.length > 0) {
                _groupHierarchy = _columnGroupKeys[0].split(automationConfig.groupKeySeparator);
            }
        }       
        _groupedJobs = data.TriggerGroups;
    }

    /**
    ** Shows all or only the selected columns based on whether the "show all" button is pressed
    **/
    function toggleVisibility() {
        var button = $("#" + _toggleVisibilityButtonId);
        if (button.hasClass("show")) {
            showAllColumns();
            button.removeClass("show").addClass("hide");
        }
        else if (button.hasClass("hide")) {
            showStoredColumns();
            button.removeClass("hide").addClass("show");
        }
    }
    /**
    ** Hides the unselected columns
    **/
    function hideDisabled() {
        var button = $("#" + _toggleVisibilityButtonId);
        if (button.hasClass("hide")) {
            showStoredColumns();
            button.removeClass("hide").addClass("show");
        }
    }

    /**
    ** Generates the matrix table
    **/
    function generateMatrixTable() {
        $("#" + automationServiceObj.tableId).empty();

        $("<table>")
            .attr("id", _jobMatrixTitleElementId)
            .addClass("title-only")
            .append(
                $("<tr>")
                    .append(
                        $("<th>")
                            .addClass("title")
                            .addClass("custom-left")
                            .append(
                                $("<span>")
                                    .attr("id", _tableEntriesId)
                                    .addClass((_jobMatrixLight) ? "hidden" : "") //hide if in light mode
                                    .append(
                                        $("<select>")
                                            .attr("id", _tableEntriesListId)
                                            .append(
                                                $("<option>")
                                                    .attr("title", 10)
                                                    .val(10)
                                                    .text("10")
                                            )
                                            .append(
                                                $("<option>")
                                                    .attr("title", 25)
                                                    .attr("selected", true)
                                                    .val(25)
                                                    .text("25")
                                            )
                                            .append(
                                                $("<option>")
                                                    .attr("title", 50)
                                                    .val(50)
                                                    .text("50")
                                            )
                                            .append(
                                                $("<option>")
                                                    .attr("title", 100)
                                                    .val(100)
                                                    .text("100")
                                            )
                                    )
                            )
                            .append(
                                $("<span>")
                                    .attr("id", "colvis-button")
                            )
                            .append(
                                $("<span>")
                                    .attr("id", "toggle-button")
                                    .append(
                                        $("<button>")
                                            .addClass((_jobMatrixLight) ? "hidden" : "") //hide if in light mode
                                            .attr("id", _toggleVisibilityButtonId)
                                            .addClass("table-title-button")
                                            .addClass("show") // show by default
                                            .click(toggleVisibility)
                                            .attr("title", "Show All Columns")
                                            .text("Show All")
                                    )
                            )
                            .append(
                                $("<span>")
                                     .html(
                                        "&nbsp;Generated At: <span id=" + _generatedAtId + "></span>"
                                        + "<br />"
                                        + "&nbsp;Reload INV: " + (automationConfig.reloadInterval / 1000) + " secs"
                                    )
                            )
                        )
                        .append(
                            $("<th>")
                                .addClass("title")
                                .append(
                                    $("<span>")
                                        .addClass("custom-right")
                                        .append(
                                            $("<span>")
                                                .append(
                                                    $("<input>")
                                                        .attr("type", "checkbox")
                                                        .attr("id", _pauseUpdateId)
                                                        .attr("checked", false)
                                                )
                                                .append(
                                                    $("<label>")
                                                        .attr("for", _pauseUpdateId)
                                                        .attr("title", "Check to Pause Updates Interval")
                                                        .text("Pause Updates // ")
                                                )
                                        )
                                        .append(
                                            $("<span>")
                                                .append(
                                                    $("<input>")
                                                        .attr("type", "checkbox")
                                                        .attr("id", _iconsCbID)
                                                        .attr("checked", _iconsState)
                                                )
                                                .append(
                                                    $("<label>")
                                                        .attr("for", _iconsCbID)
                                                        .attr("title", "Toggle State Icons / Full Text")
                                                        .text("State Icons //")
                                                )
                                        )
                                        .append(
                                            $("<span>")
                                                .append(
                                                    $("<input>")
                                                        .attr("type", "checkbox")
                                                        .attr("id", _utcCbID)
                                                        .attr("checked", _inUTC)
                                                )
                                                .append(
                                                    $("<label>")
                                                        .attr("for", _utcCbID)
                                                        .attr("title", "Display Dates In UTC " + _timezoneString)
                                                        .text("UTC " + _timezoneString + " ")
                                                )
                                        )
                                        .append(
                                            $("<span>")
                                                .addClass((_jobMatrixLight) ? "hidden" : "") //hide if in light mode
                                                .append(
                                                    $("<label>")
                                                        .text("//")
                                                )
                                                .append(
                                                    $("<input>")
                                                        .attr("type", "text")
                                                        .attr("id", _tableFilterBoxId)
                                                )
                                        )
                                )
                            )
                    )
            .appendTo($("#" + automationServiceObj.tableId));

        var thead = $("<thead>");
        var trhead = $("<tr>");

        var subTrObjectsArray = [];
        _groupHierarchy.map(function (d, i) { subTrObjectsArray[i] = [] }); // Initialise an array for each hierarchical header

        $.each(_columnGroupKeys, function (i, groupKey) {
            var subGroupArray = groupKey.split(automationConfig.groupKeySeparator);
            var titleArray = [];

            $.each(subGroupArray, function (j, subGroupKey) {
                var formattedText = (_titleSplit) ? splitWord(extractFileName(subGroupKey)) : mapText(subGroupKey);
                if (_toUnderscoreSplit) {
                    // Add the unsplit text first to the title and then split it
                    titleArray.push(formattedText);
                    formattedText = splitToUnderscore(formattedText);
                }
                else {
                    titleArray.push(formattedText);
                }

                var title = "";
                var fullLabel = "";
                if (j == 0) {
                    fullLabel = subGroupKey;
                    // The Current key + extra description
                    title = ((!_jobMatrixLight) ? wrapTitleInHtmlTable(subGroupKey) + _columnGroups[groupKey] : subGroupKey);
                }
                else {
                    fullLabel = titleArray.join(automationConfig.groupKeySeparator);
                    //The column hierarchy path so far
                    title = (!_jobMatrixLight) ? wrapTitleInHtmlTable(fullLabel) : fullLabel;
                }

                if ((subTrObjectsArray[j].length > 0) && (subTrObjectsArray[j][subTrObjectsArray[j].length - 1].title == title)) {
                    subTrObjectsArray[j][subTrObjectsArray[j].length - 1].colspan++;
                }
                else {
                    subTrObjectsArray[j].push({
                        colspan: 1,
                        title: title,
                        fullLabel: fullLabel,
                        originalLabel: groupKey,
                        text: formattedText,
                    });
                }
            });
        });

        var colspan = _columnGroupKeys.length;
        var rowSpan = 1 + _groupHierarchy.length;
        trhead
            .append(
                $("<th>")
                    .attr("title", "Changelist")
                    .addClass(_fixedColumns[0].class)
                    .attr("rowspan", rowSpan)
                    .text("Change list")
            )
            .append(
                $("<th>")
                    .attr("title", "User")
                    .addClass(_fixedColumns[1].class)
                    .attr("rowspan", rowSpan)
                    .text("User")
            )
            .append(
                $("<th>")
                    .attr("title", "Buddy")
                    .addClass(_fixedColumns[2].class)
                    .attr("rowspan", rowSpan)
                    .text("Buddy")
            )
            .append(
                $("<th>")
                    .attr("title", "Status")
                    .attr("rowspan", 1)
                    .attr("colspan", colspan)
                    .html("Status"
                        + "<div class='lowercase'>"
                            + "[<i>The jobs that have been processed together are with the same colour shade on consecutive changelists</i>]"
                        + "</div>"
                    )
            )
            .append(
                $("<th>")
                    .attr("rowspan", rowSpan)
                    .attr("title", "Description")
                    .addClass("fixed-dp-width")
                    .text("Description")
            )
            .append(
                $("<th>")
                    .attr("rowspan", rowSpan)
                    .attr("title", "Triggered At")
                    .addClass("fixed-time-width")
                    .text("Triggered  At")
            );


        thead.append(trhead);
        $.each(subTrObjectsArray, function (i, subTrObjects) {
            var tr = $("<tr>");
            $.each(subTrObjects, function (j, subTrObject) {
                var th = $("<th>");
                th.attr("colspan", subTrObject.colspan)
                    .addClass("lowercase")
                    .addClass((_toUnderscoreSplit) ? "small-font" : "")
                    .addClass((i == subTrObjectsArray.length - 1) ? _stateHeaderClass : "")
                    .attr("title", subTrObject.title)
                    .attr(_fullLabelAttr, subTrObject.fullLabel)
                    .html(subTrObject.text)
                    .addClass(_tooltipClass)
                    .attr(_originalLabelKey, subTrObject.originalLabel);

                if (!_jobMatrixLight) {
                    th.tooltip({
                        tooltipClass: "summary-tooltip",
                        position: { my: "middle bottom", at: "middle top" },
                        content: function () {
                            return $(this).attr("title");
                        },
                        show: "slow",
                        close: function (event, ui) {
                            var me = this;
                            ui.tooltip.hover(
                                function () {
                                    $(this).stop(true).show();
                                },
                                function () {
                                    $(this).fadeOut("fast", function () {
                                        $(this).remove();
                                        $(".ui-tooltip").remove();
                                    });
                                    $(".ui-tooltip").hide();
                                }
                            );
                            $(".ui-tooltip").hide();
                        }
                    });
                }

                tr.append(th);
            });
            thead.append(tr);
        });

        $("#" + automationServiceObj.tableId).append(
            $("<table>")
                .attr("id", _jobMatrixTableElementId)
                .append(thead)
                .append(
                    $("<tbody>")
                )
        );    // </table>

        //$("#" + _jobMatrixTitleElementId).width($("#" + _jobMatrixTableElementId).width());

        // Mouse Over and Scroll enable/disable reloading Events
        $("#" + _jobMatrixTableElementId)
            .mouseover(clearBuildMatrixTableInterval)
            .mouseleave(setBuildMatrixTableInterval);
    }

    function generateSummaryMatrixTable() {
        $("#" + automationServiceObj.tableId).empty();

        $("<table>")
            .addClass("title-only")
            .append(
                $("<tr>")
                    .append(
                        $("<th>")
                            .addClass("title")
                            .append(
                                $("<span>")
                                    .html(
                                        "Generated At: <span id=" + _generatedAtId + "></span>"
                                        + " - "
                                        + "Reload Interval: " + (automationConfig.reloadInterval / 1000) + " secs"
                                    )
                            )
                            .append(
                                $("<span>")
                                    .addClass("right")
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<input>")
                                                    .attr("type", "checkbox")
                                                    .attr("id", _pauseUpdateId)
                                                    .attr("checked", false)
                                            )
                                            .append(
                                                $("<label>")
                                                    .attr("for", _pauseUpdateId)
                                                    .attr("title", "Check to Pause Updates Interval")
                                                    .html("Pause Updates &nbsp;&nbsp;")
                                            )
                                    )
                            )
                    )
            )
        .appendTo($("#" + automationServiceObj.tableId));

        var summaryTable = $("<table>").attr("id", _jobMatrixSummaryTableElementId);
        summaryTable
            .append(
                $("<tr>")
                    .append(
                        $("<th>").text("Group")
                    )
                    .append(
                        $("<th>").text("Last Processed Changelist")
                    )
                    .append(
                        $("<th>").text("pending Jobs")
                    )
                    .append(
                        $("<th>").text("Status")
                    )
            );

        // Reset the latest column states dict
        _columnStates = {};

        // Build the column/group summaries
        $.each(_groupedJobs, function (groupIndex, groupObj) {
            $.each(_columnGroupKeys, function (i, groupKey) {
                var job = groupObj.GroupJobsDict[groupKey];
                var trigger = groupObj.GroupJobTrigger;
                updateColumnStates(groupKey, job, trigger);
            });
        });

        $.each(_columnGroupKeys, function (i, groupKey) {
            var statusTd = $("<td>").addClass("centre");
            updateMatrixSummaryCellStatus(statusTd, _columnStates[groupKey]);

            $("<tr>")
                .append(
                    $("<td>").html(groupKey)
                )
                .append(
                    $("<td>")
                        .addClass("centre")
                        .html(_automationLib.getChangelistLink(_columnStates[groupKey].lastChangelist))
                )
                .append(
                    $("<td>")
                        .addClass("centre")
                        .html(_columnStates[groupKey].pendingJobs)
                )
                .append(statusTd)
                .appendTo(summaryTable);
        });
        summaryTable.appendTo($("#" + automationServiceObj.tableId));
        
        updateGenereatedAtDateTime();
    }

    function setupMatrixDatatables() {
        /**
        ** Extension to hide the empy lines / replace Hide
        **/
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                var visibleColumns = oSettings.aoColumns.filter(
                    function (d, i) {
                        return (d.bVisible && !_fixedColumns.hasOwnProperty(i))
                    }).length
                    - _nonStateColumnsNum;

                var tr = oSettings.aoData[iDataIndex].nTr;
                // test for property on nTr
                var emptyCells = $(tr).find("." + _emptyCellClass).size();

                if ((emptyCells > 0) && (emptyCells == visibleColumns)) {
                    return false;
                }

                return true;
            }
        );

        retrieveColumnVisibility();
        var oTable = $("#" + _jobMatrixTableElementId).DataTable({
            "scrollX": "100%",
            //"sScrollXInner": "100%",
            "scrollY": _getTableScrollHeight(),
            "scrollCollapse": true,
            "ordering": false,          // Disable Sorting
            "pageLength": 25,
            "paging": true,             // Enable Pagination
            "searching": true,          // Enable Filter
            //"search": {
            //    "search": automationServiceObj.jobId
            //},
            "info": true,
            //"dom": 'C<"clear">lrtip',
            "dom": 'Crtip',
            "autoWidth": true,
            "colVis": {
                "buttonText": "Visible Columns", // Change the text for the ColVis button
                "label": function (index, label, th) {
                    var fullLabel = $(th).attr(_fullLabelAttr);
                    return (fullLabel) ? fullLabel : label;
                },
            },
            "initComplete": function (settings, json) {
                $("div.ColVis button")
                    .attr("title", "Visible Columns")
                    .addClass("table-title-button")
                    .click(hideDisabled) // Show only the visible ones in case the 'show all' button is clicked
                    .click(overrideVisibilityBoxEvents)
                    .appendTo("#colvis-button");
            },
        });

        // Fix the first three columns
        var fc = new $.fn.DataTable.FixedColumns(oTable, {
            "leftColumns": Object.keys(_fixedColumns).length,
            "heightMatch": "none",
            "paging": true,
        });

        if (automationServiceObj.jobId) {
            $("#" + _tableFilterBoxId).val(automationServiceObj.jobId);
            oTable.search(automationServiceObj.jobId);
        }

        // Add search functionality to the 
        $("#" + _tableFilterBoxId).keyup(function () {
            oTable.search($(this).val()).draw(false);
        });

        $("#" + _tableEntriesListId).change(function () {
            oTable.page.len($(this).val()).draw(false);
        });
        
        $(window).resize(function () {
            setTimeout(function () { customTableRedraw(oTable); }, _redrawTimeoutDelay);
        });

        oTable.on("draw", function (e) { updateMatrixTableEvents(e); });
        showStoredColumns();
    }

    /**
    **
    **/
    function setupCheckboxEvents() {
        $("#" + _utcCbID).change(function () {
            _inUTC = $(this).prop("checked");
            populateMatrixTable();
        });

        $("#" + _iconsCbID).change(function () {
            _iconsState = $(this).prop("checked");
            populateMatrixTable();
        });
    }
      
    /**
    ** Populates the previously generated table, called periodically
    **/
    function populateMatrixTable() {
        var oTable;

        if (!_jobMatrixLight) {
            oTable = $("#" + _jobMatrixTableElementId).DataTable();
            clearDataTable(oTable);
        }
        else {
            oTable = $("#" + _jobMatrixTableElementId + " tbody");
            oTable.find("td").off();
            oTable.empty();
        }
        // Reset the latest column states dict
        _columnStates = {};

        $.each(_groupedJobs, function (groupIndex, groupObj) {
            // Get a random job from the changelist dict for populating the other columns 
            // (they should all have the same Trigger object)
            //var randomJob = getRandomDictValue(groupedJobDict);
            var groupJobTrigger = groupObj.GroupJobTrigger;
            var groupJobDict = groupObj.GroupJobsDict;
            var previousGroupJobDict = (groupIndex > 0) ? _groupedJobs[groupIndex - 1].GroupJobsDict : undefined;

            var rowClass = (groupJobTrigger.Trigger.TriggerId)
                            ? groupJobTrigger.Trigger.TriggerId
                            : unixTimeToMsTicks(parseJsonDate(groupJobTrigger.Trigger.TriggeredAt).getTime()).toString();
            var tr = $("<tr>");
            tr.addClass(rowClass);
            //tr.attr(rowId, rowClass);

            var row = [];

            groupJobTrigger.Trigger.Files = (groupJobTrigger.Trigger.Files)
                                            ? groupJobTrigger.Trigger.Files
                                            : [];

            // Changelist Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[0].class)
                    .addClass("centre")
                    .attr(
                        "title",
                        (
                            (groupJobTrigger.Trigger.Description)
                                ? "Description: \n" + groupJobTrigger.Trigger.Description + "\n\n"
                                : ""
                        )
                        + (
                            (groupJobTrigger.Trigger.Files)
                                ? "Processed Files: \n" + groupJobTrigger.Trigger.Files.join("\n")
                                : ""
                        )
                    )
                    .html(_automationLib.getChangelistLink(groupJobTrigger.Trigger.Changelist))
            )

            // User Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[1].class)
                    .attr("title", groupJobTrigger.Trigger.Username)
                    .text(truncate(groupJobTrigger.Trigger.Username, automationConfig.truncateChars))
                    .addClass(_tooltipClass)
            );

            // Buddy Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[2].class)
                    .attr("title", extractBuddyName(groupJobTrigger.Trigger.Description))
                    .text(truncate(extractBuddyName(groupJobTrigger.Trigger.Description), automationConfig.truncateChars))
            );

            $.each(_columnGroupKeys, function (i, groupKey) {
                var job = groupJobDict[groupKey];
                var state = (job) ? job.State : "";
                var jobId = (job) ? job.ID : "";

                var consumedById;
                if (state === automationConfig.consumeState) {
                    consumedById = job.ConsumedByJobID;

                    if (_consumerJobsDict.hasOwnProperty(consumedById)) {
                        var consumeJob = _consumerJobsDict[consumedById];
                        state = consumeJob.State;

                        // For colouring the next line
                        groupJobDict[groupKey].State = consumeJob.State;
                    }
                }

                var colourClass = (state) ? state.toLowerCase() : _emptyCellClass;
                updateColumnStates(groupKey, job, groupJobTrigger.Trigger);

                if (previousGroupJobDict && previousGroupJobDict.hasOwnProperty(groupKey)) {
                    var higherState = previousGroupJobDict[groupKey].State;

                    // If the state is the same but the consumed by job is different or undefined, swap the colour shades
                    if (
                        (state == higherState) &&
                        (
                            (consumedById != previousGroupJobDict[groupKey].ConsumedByJobID)
                            || (typeof consumedById === "undefined")
                        )
                    ) {
                        if (!previousGroupJobDict[groupKey].hasOwnProperty("Variation")) {
                            colourClass = state.toLowerCase() + _variationSuffix;
                            groupJobDict[groupKey]["Variation"] = true; // Set the flag for next row's class
                        }
                    }
                        // If the state is the same and the consumed by job is the same, keep the same shade as above
                    else if (
                        (state == higherState) &&
                        (consumedById == previousGroupJobDict[groupKey].ConsumedByJobID)
                        ) {
                        if (previousGroupJobDict[groupKey].hasOwnProperty("Variation")) {
                            colourClass = state.toLowerCase() + _variationSuffix;
                            groupJobDict[groupKey]["Variation"] = true; // Set the flag for next row's class
                        }
                    }
                }

                // State Column
                var td = $("<td>").addClass(colourClass);

                if (state) {
                    var tdTitle = "State: " + state
                        + "\nJob ID: " + jobId
                        + (consumedById ? "\nConsumed By ID: " + consumedById : "");
                    td.attr("title", tdTitle);

                    td.addClass(_tooltipClass) // will add a tooltip
                        .addClass(_stateTooltipClass) // the tooltip will be based on the cell job state - colourclass
                        .attr("state", state)
                        .attr("jobId", jobId)
                        .attr("consumedById", consumedById)
                        .addClass("centre")
                        .addClass((consumeJob) ? consumeJob.ID : "")
                        .append(
                            (!(state) || automationConfig.nonUlogStates.hasOwnProperty(state))
                            ? $("<span>")
                                .append(
                                    $("<span>")
                                        .addClass("hidden")
                                        .text(jobId + " " + consumedById)
                                )
                                .append(
                                    $("<span>")
                                        .html((_iconsState) ? mapTextToIcon(state) : mapText(state))
                                )
                            : $("<span>").append(
                                    $("<span>")
                                        .addClass("hidden")
                                        .text(jobId + " " + consumedById)
                                )
                                .append(
                                    $("<a>")
                                        .attr("href", automationServiceObj.ulogHost + "?"
                                            + $.param({
                                                "ServerHost": automationServiceObj.serverHost,
                                                "JobId": (((consumedById) ? consumedById : jobId)),
                                                "ErrorsOnly": ((state == automationConfig.errorStateName) ? true : false),
                                            })
                                        )
                                        .attr("target", "_blank")
                                        .attr("title", "Click to view the Universal Log")
                                        .html((_iconsState) ? mapTextToIcon(state) : mapText(state))
                                )
                        );                         

                }

                tr.append(td);
            });

            if (groupJobTrigger.Trigger.TriggeredAt === undefined) {
                groupJobTrigger.Trigger.TriggeredAt = groupJobTrigger.Trigger.SubmittedAt;
            }
            // Description Column
            tr.append(
                $("<td>")
                    .addClass("fixed-dp-width")
                    .attr("title", groupJobTrigger.Trigger.Description)
                    .text(truncate(groupJobTrigger.Trigger.Description, automationConfig.truncateDescChars))
            );

            // Triggered at Column
            tr.append(
                $("<td>")
                    .addClass("fixed-time-width")
                    .addClass("monospace")
                    .attr("title", toCustomISO(parseJsonDate(groupJobTrigger.Trigger.TriggeredAt), _inUTC)
                                    + "\nTime Ticks: " + unixTimeToMsTicks(parseJsonDate(groupJobTrigger.Trigger.TriggeredAt).getTime())
                                    + "\nTrigger Id: " + groupJobTrigger.Trigger.TriggerId
                    )
                    .text(toCustomISO(parseJsonDate(groupJobTrigger.Trigger.TriggeredAt), _inUTC))
            );

            if (!_jobMatrixLight) {
                oTable.row.add(tr[0]);
                tr.removeData().remove();
            }
            else {
                oTable.append(tr);
            }

            delete (tr);
        });

        if (_firstload) {
            _firstload = false;
        }

        //_consumerJobsDict = {};
        //_groupedJobs = [];
        //_groupHierarchy = [];

        clearBuildMatrixTableInterval();
        setBuildMatrixTableInterval();

        updateStateHeaders();
        customTableRedraw(oTable);

        // Update the generated at info
        updateGenereatedAtDateTime();

        hideUpdatingMessage();
    }

    function updateColumnStates(groupKey, job, trigger) {
        var state = (job) ? job.State : "";
        var changelist = (trigger.Changelist) ? trigger.Changelist : "";

        // This is only for the Matrix Summary View
        if (state === automationConfig.consumeState) {
            if (_consumerJobsDict.hasOwnProperty(job.ConsumedByJobID)) {
                state = _consumerJobsDict[job.ConsumedByJobID].State;
            }
        }

        if (state != undefined) {
            if (!_columnStates.hasOwnProperty(groupKey)) {
                _columnStates[groupKey] = {};
                _columnStates[groupKey]["pendingJobs"] = 0;
                _columnStates[groupKey]["lastChangelist"] = "";
                _columnStates[groupKey]["integrations"] = {};
            }

            // Skip the Skipped states
            if (state == automationConfig.skippedStateName) {
            }
            else if (state == automationConfig.pendingStateName) {
                // count the pending jobs
                _columnStates[groupKey]["pendingJobs"]++;
            }
            else {
                if (!_columnStates[groupKey]["currentState"]) {
                    _columnStates[groupKey]["currentState"] = state;
                }
                else if (!_columnStates[groupKey]["previousState"]) {
                    if ((_columnStates[groupKey]["currentState"] == automationConfig.assignedStateName)
                            && (state == automationConfig.assignedStateName)) {
                        // Ignore the adjacent assigned states
                    }
                    else {
                        _columnStates[groupKey]["previousState"] = state;
                    }
                }

                if (!_columnStates[groupKey]["lastChangelist"]) {
                    if (state != automationConfig.assignedStateName) {
                        _columnStates[groupKey]["lastChangelist"] = changelist;
                    }
                }

            }

            // Check if there are integration regex
            var jobIntegrationP4Regex = getIntegrationP4Regex(job);
            $.each(jobIntegrationP4Regex, function (key, value) {
                // Using key+value for dict key as there might be entries with same key - diff value
                // Will take the substring afterward by removing the value from the key
                if (!_columnStates[groupKey]["integrations"].hasOwnProperty(key + value)) {
                    _columnStates[groupKey]["integrations"][key + value] = value;
                }
            });
        }
    }

    /**
    **
    **/
    function updateGenereatedAtDateTime() {
        $("#" + _generatedAtId)
            .switchClass("", "flash-red", automationConfig.animationDuration)
            .html(toCustomISO(_generatedAt, _inUTC));
        $("#" + _generatedAtId).switchClass("flash-red", "", automationConfig.animationDuration);
    }

    /**
    **
    **/
    function updateMatrixTableEvents(e) {
        removeTableBodyEvents();
        addTableBodyEvents();
    }

    /**
    ** Adds a jquery tooltip if there isn't one and opens it
    **/
    function addTooltip(td) {
        if (!$(td).attr(_tooltipSetFlag)) {
            if ($(td).hasClass(_fixedColumns[1].class)) {
                $(td).tooltip({
                    tooltipClass: "user-tooltip",
                    position: { my: "left middle", at: "right middle" },
                    content: function () {
                        return createUserInfoDiv($(this).attr("title"));
                    },
                    show: "fast",
                    hide: "fast",
                    //open: function (event, ui) {
                    //   debugger;
                    //}
                    close: function () {
                        destroyTooltip(td);
                    }
                });
            }
            else if ($(td).hasClass(_stateTooltipClass) && !($(td).hasClass(_emptyCellClass))) {
                var state = $(td).attr("state");
                var jobId = $(td).attr("jobId");
                var consumedById = $(td).attr("consumedById");

                var hasErrors = automationConfig.errorUlogStates.hasOwnProperty(state);

                $(td).tooltip({
                    items: "td",
                    tooltipClass: "job-tooltip",
                    position: { my: "left middle", at: "right middle" },
                    content: function () {
                        return createSpinnerDiv(
                                createBasicJobInfoDiv({ ID: jobId, ConsumedByJobID: consumedById, State: state })
                            );
                    },
                    show: {
                        effect: "fadeIn",
                        delay: automationConfig.tooltipDelay,
                    },
                    open: function (event, ui) {
                        $el = $(this),
                        pos = $el.tooltip("option", "position");
                        pos.of = $el;

                        $(td).attr("loading", true);
                        setTimeout(function () {
                            // console.log(td.attr("loading"));
                            // Don't do the ajax request if the mouse has been moved over in the meantime
                            if (!$(td).attr("loading")) {
                                ui.tooltip.html(ui.tooltip.find("div.job-info").html());
                                return;
                            }

                            //$.get(automationServiceObj.ulogErrorsRest,
                            $.get(automationServiceObj.automationJobFullInfoRest,
                                {
                                    "JobId": ((consumedById) ? consumedById : jobId)
                                },
                                function (data) {
                                    // get the current tooltip html info
                                    var jobInfoTable = ui.tooltip.find("table.job-info-table");
                                    ui.tooltip.html(
                                        createJobFullInfoDiv(data.Job, jobInfoTable)
                                        +
                                        createJobExtraInfoDiv(data)
                                    );
                                    ui.tooltip.position(pos);
                                }
                            )
                        }, automationConfig.tooltipDelay + automationConfig.tooltipAjaxDelay);

                        clearBuildMatrixTableInterval();
                    },
                    close: function (event, ui) {
                        // Cancel the ajax query if it hasn't happened yet
                        $(td).attr("loading", null);

                        var me = this;
                        ui.tooltip.hover(
                            function () {
                                $(td).attr("hovered", true);
                                $(this).stop(true).show();
                                clearBuildMatrixTableInterval();
                            },
                            function () {
                                $(this).fadeOut("fast", function () {
                                    $(td).attr("loading", null);
                                    $(this).remove();
                                    destroyTooltip(td);
                                });
                                $(".ui-tooltip").hide();
                                setBuildMatrixTableInterval();
                            }
                        );
                        $(".ui-tooltip").hide();
                        setBuildMatrixTableInterval();
                    },
                });

            }  // End of ! _stateTooltipClass
            $(td).attr(_tooltipSetFlag, true);

        } // End of !_tooltipSetFlag

        //console.log($(td).attr("jobId") + " : open");
        // Open the tooltip after it's added
        $(td).tooltip("open");
    }

    /**
    ** Destroys a jquery tooltip
    **/
    function destroyTooltip(td) {
        //console.log($(td).attr("jobId") + " : destroy");
        $(".ui-tooltip").remove();
        $(td).tooltip("destroy");
        $(td).attr(_tooltipSetFlag, null);
    }

    /**
    **
    **/
    function addTableBodyEvents() {
        $("#" + automationServiceObj.tableId).find("td." + _tooltipClass).each(function (i, td) {
            var consumedById = $(td).attr("consumedById");

            $(td)
                .off("click")
                .on("click", function () {
                    $("." + _highlightedTdClass).removeClass(_highlightedTdClass);
                    if (consumedById) {
                        $("." + consumedById).toggleClass(_highlightedTdClass);
                    }
                })
                .off("mouseenter")
                .on("mouseenter", function () {
                    addTooltip(td);
                })
        });
    }

    /**
    **
    **/
    function removeTableBodyEvents() {
        $("#" + automationServiceObj.tableId).find("td." + _tooltipClass).each(function (i, td) {
            // This will destroy the tooltips that were opened but not hovered
            if ($(td).attr(_tooltipSetFlag)) {
                destroyTooltip(td);
            }
        });
    }

    /**
    **
    **/
    function removeTableBodyEventsAndEmptyCells() {
        $("#" + automationServiceObj.tableId).find("td." + _tooltipClass).each(function (i, td) {
            // This will destroy the tooltips that were opened but not hovered
            if ($(td).attr(_tooltipSetFlag)) {
                destroyTooltip(td);
            }
            $(td).removeData().removeAttr().removeClass().off();
        });
    }

    /**
    **
    **/
    function clearDataTable(oTable) {
        removeTableBodyEventsAndEmptyCells();

        $(".ui-tooltip").remove();
        $(".ui-helper-hidden-accessible").remove();

        if ((oTable == null) && ($("#" + _jobMatrixTableElementId).size() > 0)) {
            oTable = $("#" + _jobMatrixTableElementId).DataTable();
        }

        if (oTable) {
            oTable.clear();
        }
    }
    
    /**
    **
    **/
    function updateStateHeaders() {
        var classSuffixRE = new RegExp(_gradientSuffix + "$", "ig");

        $("." + _stateHeaderClass).each(function (i, th) {
            // Remove the current gradient class if exists
            $(th).removeClass(function (index, classNames) {
                $.each(classNames.split(' '), function (index2, className) {
                    className = className.trim();
                    if (className.match(classSuffixRE)) {
                        $(th).removeClass(className);
                    }
                });
            });

            if (_columnStates.hasOwnProperty($(th).attr(_originalLabelKey))) {
                var currentState = _columnStates[$(th).attr(_originalLabelKey)].currentState;
                var previousState = _columnStates[$(th).attr(_originalLabelKey)].previousState;
                var pendingJobs = _columnStates[$(th).attr(_originalLabelKey)].pendingJobs;
                var lastChangelist = _columnStates[$(th).attr(_originalLabelKey)].lastChangelist;
                var integrations = _columnStates[$(th).attr(_originalLabelKey)].integrations;

                var colourClass;
                if ((currentState == automationConfig.assignedStateName) &&
                        (previousState == automationConfig.errorStateName)) {
                    colourClass = automationConfig.reassignedStateName.toLowerCase() + _gradientSuffix;
                } 
                else {
                    if (currentState) {
                        colourClass = currentState.toLowerCase() + _gradientSuffix;
                    }
                }
                $(th).addClass(colourClass);
                var previousColourClass = (previousState) ? (previousState.toLowerCase() + _gradientSuffix) : "";

                if (!_jobMatrixLight) {
                    var originalTitle = $(th).attr("title").split("<table class='summary-table monospace'>")[0]; // Single quotes are important
                    var originalText = $(th).html().split('<span class="integrationP4Regex">')[0];

                    var integationsText = (Object.keys(integrations).length > 0)
                                    ? "<span class='integrationP4Regex'> (* x " + Object.keys(integrations).length + ")</span>"
                                    : "";

                    var summaryTitle = originalTitle
                                    + "<table class='summary-table monospace'>"
                                        + "<tr><th colspan='2' class='title'>Summary</th></tr>"
                                        + "<tr>"
                                            + "<td>Current State: </td>"
                                            + "<td class='" + colourClass + "'>" + mapText(currentState) + "</td>"
                                        + "</tr>"
                                    + ((previousState && (previousState != currentState))
                                        ? ("<tr><td>Previous State: </td>"
                                            + "<td class='" + previousColourClass + "'>" + mapText(previousState) + "</td></tr>")
                                        : "")
                                    + ((pendingJobs)
                                        ? ("<tr><td>Pending Jobs: </td><td>" + pendingJobs + "</td></tr>")
                                        : "")
                                    + ((lastChangelist)
                                        ? ("<tr><td>Last Processed Changelist: </td><td>" + _automationLib.getChangelistLink(lastChangelist) + "</td></tr>")
                                        : "")
                                    + ((Object.keys(integrations).length > 0)
                                        ? ("<tr><td>Integration P4Regex: </td><td>"
                                            + createIntegrationP4RegexText(integrations) + "</td></tr>")
                                        : "")
                                    + "</table>";
                    var summaryText = originalText + integationsText;

                    $(th).attr("title", summaryTitle);
                }
                $(th).html(summaryText);
            }
        });
    }

    /**
    **
    **/
    function updateMatrixSummaryCellStatus(td, groupSummary) {
        var currentState = groupSummary.currentState;
        var previousState = groupSummary.previousState;
        var pendingJobs = groupSummary.pendingJobs;
        var lastChangelist = groupSummary.lastChangelist;
        //var integrations = groupSummary.integrations;

        var colourClass;
        var cellText;
        if (
            (currentState == automationConfig.errorStateName)
            || ((currentState == automationConfig.assignedStateName) && (previousState == automationConfig.errorStateName)) 
            ) {
            //colourClass = automationConfig.reassignedStateName.toLowerCase() + _gradientSuffix;
            colourClass = automationConfig.errorStateName.toLowerCase();
            cellText = "Failed";
        }
        else if (((currentState == automationConfig.assignedStateName) && (previousState == automationConfig.completedStateName))
            || (currentState == automationConfig.completedStateName) && (pendingJobs > 0)
            ) {
            colourClass = automationConfig.completedStateName.toLowerCase();
            cellText = "Processing";
        }
        else if ((currentState == automationConfig.completedStateName) && (pendingJobs == 0)) {
            colourClass = currentState.toLowerCase();
            cellText = "Processed To Head Revision";
        }

        $(td).addClass(colourClass);
        $(td).text(cellText);
    }
    
    /**
    ** Overrides the events of ColVis with the custom ones
    **/
    function overrideVisibilityBoxEvents() {
        // Return it hasn't been initialised yet
        if ($("ul.ColVis_collection > li").size() == 0) {
            return;
        }

        // Loop through all the li elements
        $("ul.ColVis_collection > li").each(function (i, li) {
            var cb = $(li).find("input[type='checkbox']");

            /** DEPRECATED
            // If it's a fixed one restore the state, fnDraw resets it from the original table's visibility
            if (isFixedColumn(i)) {
                fCol = _fixedColumns[i];
                cb.prop("checked", (fCol.visible) ? true : false);
            }
            **/

            // If it's the first time here, bind the event
            if (!_boxEventsOverriden) {
                //Disable all events
                $(li).off();
                $(cb).off();
                $(cb).on("change", function (e) {
                    clearTimeout(_redrawTimeoutID);
                    updateColumnVisibility(i, cb.is(":checked"));
                });
            }
        });

        _boxEventsOverriden = true;
    }

    /**
    ** Shows all the available columns
    **/
    function showAllColumns() {
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        oTable.columns().visible(true);

        //customTableRedraw(oTable);
        updateStateHeaders();
    }
    /**
    ** Shows only the currently selected columns
    **/
    function showStoredColumns() {
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        $.each(oTable.columns()[0], function (index, columnIndex) {
            // Use the full label key if exists, otherwise use the title
            var header = oTable.column(columnIndex).header();
            var fullLabelKey = $(header).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : $(header).attr("title"));

            if (_columnVisibility.hasOwnProperty(columnStoreKey)) {
                var storedVisibility = _columnVisibility[columnStoreKey];

                // set the visibility and redraw the table at the end
                oTable.column(columnIndex).visible(_columnVisibility[columnStoreKey]);
            }
        });

        //customTableRedraw(oTable);
        updateStateHeaders();
    }

    // Loads the locally saved settings about column visibility
    function retrieveColumnVisibility() {
        // From legacy generic.js
        _columnVisibility = retrieveLocalObject(config.fullWebPath) || _columnVisibility;
    }
    // Updates the local list of visibility of the given column
    function updateColumnVisibility(index, visibility) {
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        
        if (oTable.column(index)) {
            // Use the full label key if exists, otherwise use the title
            var header = oTable.column(index).header();
            var fullLabelKey = $(header).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : $(header).attr("title"));

            _columnVisibility[columnStoreKey] = visibility;
        }
        
        storeColumnVisibility();
        _redrawTimeoutID = setTimeout(showStoredColumns, _redrawTimeoutDelay);
    }
    // Stores the local visibility list in html5's browser storage
    function storeColumnVisibility() {
        // From legacy generic.js
        storeLocalObject(config.fullWebPath, _columnVisibility);
    }

    /**
    ** Custom redraw function
    **/
    function customTableRedraw(oTable) {
        if (!_jobMatrixLight) {
            $(".ui-tooltip").hide();
            oTable.settings()[0].oScroll.sY = _getTableScrollHeight();
            oTable.draw(false);
        }
    }

    /**
    **
    **/
    function extractBuddyName(text) {
        var noBuddy = " ";
        if (!text) {
            return noBuddy;
        }

        var regexp = /Buddy\:(.+?)(?:\n|$)/i;
        var matches = text.match(regexp);
        return (matches && matches.length > 1) ? matches[1] : noBuddy;
    }

    /**
    **
    **/
    function extractFileName(text) {
        if (!text) {
            return "-";
        }

        var regexp = /([^\\]+)(.sln|.scproj)$/i;
        var matches = text.match(regexp);
        return (matches && matches.length > 1) ? splitWord(matches[1]) : text;
    }
   
    /**
    ** Split words from text in capital characters
    **/
    function splitWord(text) {
        return text.split(/[_\-.]/g).map(function (substr) { return (mapText(substr).match(/[A-Z]*[^A-Z]+/g) || [substr]).join(" "); }).join(" ");
    }

    /**
    **
    **/
    function splitToUnderscore(text) {
        return text.split(/_to_/gi).join("<br/>_to_<br/>");
    }

    /**
    **
    **/
    function mapText(state) {
        return (state && automationConfig.nameMappings.hasOwnProperty(state.toLowerCase())
            ? automationConfig.nameMappings[state.toLowerCase()]
            : state);
    }

    /**
    **
    **/
    function mapTextToIcon(state) {
        return (state && automationConfig.iconMappings.hasOwnProperty(state.toLowerCase())
            ? (
                $("<div>").append(
                    $("<img />").attr("src", automationServiceObj.imagesRootURI + automationConfig.iconMappings[state.toLowerCase()])
                )
                .html()
            )
            : state);
    }

    /**
    **
    **/
    function getRandomDictValue(dict) {
        for (var key in dict) {
            return dict[key];
        }
    }
    
    /**
    **
    **/
    function createUserInfoDiv(username) {
        var user = _usersInfo[username.toLowerCase()];
        var userInfoDiv;

        if (typeof user !== "undefined") {
            userInfoDiv = $("<div />")
                //.addClass("userdiv")
                .append(
                    $("<div />")
                        .addClass("img-wrap")
                        .append(
                            $("<img />").attr("src", automationConfig.usersPath + user.ImageFilename)
                        )
                )
                .append(
                        $("<div />")
                        .addClass("user-info")
                        .append(
                            $("<div />")
                                .addClass("user-info-name")
                                .html(user.Name.replace(" (", "<br />("))
                        )
                        .append(
                            $("<div />")
                                .addClass("user-info-rest")
                                .html(user.Email + "<br />" + user.JobTitle)
                        )
                )
        }
        else {
            userInfoDiv = $("<div />")
                .text(username.toLowerCase())
        }

        return userInfoDiv.html();
    }

    function createBasicJobInfoDiv(job) {
        var spanWrapper = $("<span>");
        var jobInfoHtmlTableSpan = $("<span />").addClass("job-info");

        var jobInfoHtmlTable =
            $("<table>")
                .addClass("monospace")
                .addClass("job-info-table");

        if (job) {
            jobInfoHtmlTable.append(
            $("<tr>").append(
                $("<th>")
                    .attr("colspan", "2")
                    .addClass("title")
                    .text("Job Details")
                )
            );

            if (job.State) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("State:")
                    )
                    .append(
                        $("<td>")
                            .addClass(job.State.toLowerCase() + _gradientSuffix)
                            .text(mapText(job.State))
                    )
                )
            }

            jobInfoHtmlTable.append(
                $("<tr>").append(
                    $("<td>").text("ID:")
                )
                .append(
                    $("<td>").text(job.ID)
                )
            );

            if (job.ConsumedByJobID) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Consumed By:")
                    )
                    .append(
                        $("<td>").text(job.ConsumedByJobID)
                    )
                );
            }

            jobInfoHtmlTableSpan.append(jobInfoHtmlTable);
        } // End of if job

        spanWrapper.append(jobInfoHtmlTableSpan);

        return spanWrapper.html(); // This will remove the spanWrapper tag
    }

    /**
    **
    **/
    function createJobFullInfoDiv(job, jobInfoHtmlTable) {
        var outerSpan = $("<span>");

        if (job) {
            if (job.Priority) { // Only High Priority
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Priority:")
                    )
                    .append(
                        //$("<td>").text(_jobPriorityEnum[job.Priority])
                        $("<td>").text(job.Priority)
                    )
                )
            }

            var processedAtDate = parseJsonDate(job.ProcessedAt);
            var completedAtDate = parseJsonDate(job.CompletedAt);
            var createdAtDate = parseJsonDate(job.CreatedAt);

            jobInfoHtmlTable
                .append(
                    $("<tr>").append(
                        $("<td>").text("Created At :")
                    )
                    .append(
                        $("<td>").text(toCustomISO(createdAtDate, _inUTC))
                    )
                )
                .append(
                    $("<tr>").append(
                        $("<td>").text("Processed At :")
                    )
                    .append(
                        $("<td>").text(
                            (
                                (!isFutureDate(processedAtDate) && (typeof job.ProcessingTime != "undefined")) ?
                                    (toCustomISO(processedAtDate, _inUTC)
                                        + " (Processing Time: "
                                        + formatSecs(job.ProcessingTime)
                                        + ")"
                                    )
                                    : "Not processed"
                            )
                        )
                    )
                )
                .append(
                    $("<tr>").append(
                        $("<td>").text("Completed At :")
                    )
                    .append(
                        $("<td>").text(
                            ((!isFutureDate(completedAtDate)) ? toCustomISO(completedAtDate, _inUTC) : "Not completed")
                        )
                    )
                );

            if (job.ProcessingHost) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Processing Host:")
                    )
                    .append(
                        $("<td>").text(job.ProcessingHost)
                    )
                )
            }

            var jobIntegrationP4Regex = getIntegrationP4Regex(job);
            if (Object.keys(jobIntegrationP4Regex).length > 0) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Integration P4Regex:")
                    )
                    .append(
                        $("<td>").html(createIntegrationP4RegexText(jobIntegrationP4Regex))
                    )
                )
            }

            /* Reporter Properties */
            if (job.ReportName) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Report Name:")
                    )
                    .append(
                        $("<td>").html(job.ReportName)
                    )
                )
            }
            if (job.ReportOwners) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Report Owners:")
                    )
                    .append(
                        $("<td>").html(job.ReportOwners.join(", "))
                    )
                )
            }
            /*
            if (job.Changes) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Changes:")
                    )
                    .append(
                        $("<td>").html(job.Changes.map(function (c) {
                            return (c.ID + ": " + c.Property + " = " + c.NewValue);
                        }).join("<br />"))
                    )
                )
            }
            */
            if (job.Trigger.ChangeIds) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Change IDs:")
                    )
                    .append(
                        $("<td>").html(job.Trigger.ChangeIds.join("<br />"))
                    )
                )
            }

            jobInfoHtmlTable.append(
                $("<tr>").append(
                    $("<td>").text("Components:")
                )
                .append(
                    $("<td>").html(_automationLib.constructJobComponentsHtml(job))
                )
            )
        }

        outerSpan.append(jobInfoHtmlTable);
        return outerSpan.html();
    }

    /**
    **
    **/
    function createJobExtraInfoDiv(jsonResult) {
        var tooltipSpan = $("<span />")

        // Job Results Table
        var jobResults = _jobResults[jsonResult.Job.ID];

        if (jobResults != undefined) {
            var jobResultsTable = $("<table />").addClass("monospace");
            jobResultsTable.append(
                $("<tr />").append(
                    $("<th />")
                        .addClass("title")
                        .attr("colspan", 2)
                        .text("Job Results")
                )
            );

            if (jobResults.SubmittedChangelistNumber) {
                jobResultsTable.append(
                    $("<tr>").append(
                        $("<td>").text("Submitted Changelist:")
                    )
                    .append(
                        $("<td>").html(_automationLib.getChangelistLink(jobResults.SubmittedChangelistNumber))
                    )
                )
            }

            if (jobResults.Notifications) {
                jobResultsTable.append(
                    $("<tr>").append(
                        $("<td>").text("Notifications:")
                    )
                    .append(
                        $("<td>").html(jobResults.Notifications.join("<br />"))
                    )
                )
            }
            if (jobResults.BugsCreated) {
                jobResultsTable.append(
                    $("<tr>").append(
                        $("<td>").text("Bugs Created:")
                    )
                    .append(
                        $("<td>").html(jobResults.BugsCreated.join("<br />"))
                    )
                )
            }
            if (jobResults.OutputFiles) {
                jobResultsTable.append(
                    $("<tr>").append(
                        $("<td>").text("Output Files:")
                    )
                    .append(
                        $("<td>").html(jobResults.OutputFiles.join("<br />"))
                    )
                )
            }

            // Add table to returned html
            tooltipSpan.append(jobResultsTable);
        }

        // Errors Table
        var errors = jsonResult.Errors;
        if ((errors) && (errors.length != 0)) {
            var errorsTable = $("<table />").addClass("monospace");
            errorsTable.append(
                $("<tr />").append(
                    $("<th />")
                        .addClass("title")
                        .html("Errors (<b>" + errors.length + "</b>)")
                )
            );

            $.each(errors, function (i, error) {
                $("<tr />")
                    .append(
                        $("<td />")
                            .addClass("red")
                            //.text(error.Message)
                            .text(error)
                    )
                    .appendTo(errorsTable)
            });
            // Add table to returned html
            tooltipSpan.append(errorsTable);
        }

        return tooltipSpan.html();
    }

    /**
    **
    **/
    function createSpinnerDiv(text) {
        var div = $("<div />");

        div.append(
            $("<div />")
            .append(
                $("<div />").html((text) ? text : "")
            )
            .append(
                $("<div />").addClass("spinner")
                    //.html("Loading ...")
            )
        );

        return div.html();
    }

    /**
    **
    **/
    function showUpdatingMessage() {

        $("body").append(
            $("<div>")
                .attr("id", "MessageOverlay")
                .addClass("updating-message-overlay")
                .append(
                    $("<div>")
                        .addClass("updating-message-text")
                        .html("Please Wait.<br/>The report is being updated.")
                )
        );
    }

    /**
    **
    **/
    function hideUpdatingMessage() {
       $("#" + "MessageOverlay").hide().remove();
    }

    /**
    **
    **/
    function getIntegrationP4Regex(job) {
        var integrationP4Regex = {};

        if (job && job.hasOwnProperty("Integration")) {
            if (job["Integration"].hasOwnProperty("P4RegexResolveOptions")) {
                $.each(job["Integration"]["P4RegexResolveOptions"], function (key, value) {
                    if (!integrationP4Regex.hasOwnProperty(key)) {
                        integrationP4Regex[key] = value;
                    }
                });
            }
        }

        return integrationP4Regex;
    }

    /**
    **
    **/
    function createIntegrationP4RegexText(integrationP4Regex) {
        var integrationP4RegexText = Object.keys(integrationP4Regex).map(function (key) {
                                        // remove the value from the key if it's there
                                        var filteredKey = key.replace(integrationP4Regex[key], "");
                                        return ("regex='" + filteredKey + "' resolve options='" + integrationP4Regex[key] + "'");
                                     })
                                    .join("\n");
        return integrationP4RegexText;
    }

    /**
    **
    **/
    function wrapTitleInHtmlTable(title) {
        var titleHtml =
                $("<div>")
                    .append(
                        $("<table>")
                            .addClass("monospace")
                            .append(
                                $("<tr>")
                                    .append(
                                        $("<th>")
                                            .html(title)
                                    )
                            )
                    );
        return titleHtml.html();
    }

    /**
    **
    **/
    function minimiseJob(job) {
        var mJob = {
            "ID": job.ID,
            "ConsumedByJobID": job.ConsumedByJobID,
            "State": job.State,
            "Priority": job.Priority,
            "CreatedAt": job.CreatedAt,
            "ProcessedAt": job.ProcessedAt,
            "CompletedAt": job.CompletedAt,
            "ProcessingHost": job.ProcessingHost,
            "ProcessingTime": job.ProcessingTime,
        }

        return mJob;
    }

    /**
    **
    **/
    function getLoadDataIntervalID() {
        return _loadDataIntervalID;
    }

    /**
    **
    **/
    return {
        clearDataTable: clearDataTable,
        getLoadDataIntervalID: getLoadDataIntervalID,
        initJobMatrix: initJobMatrix,
        initJobMatrixSummary: initJobMatrixSummary,
    };

};

