/*
var pieChartLegend = "Pie Chart :: ",
	barChartLegend = "Bar Chart :: ";
*/

var pieChartId = "piechart",
	barChartId = "barchart";

// Object used for local storage of the user selections
//var pageData = {
//	availableChart: 0,
//}
var restData = [];

var commasFixed2 = function(d) {return d3.format(",")(d.toFixed(2)); };

var currentStat = (getUrlVars()["stat"]) ? getUrlVars()["stat"] : 0;
//var currentFilename = config.currentFilename + "?stat=" + currentStat;
var currentFilename = config.currentFilename;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
		
	$("#content")
	.append(
		$("<div>")
			.attr("id", "friendlier-names-field")
			.append(
				$("<input>")
					.attr("id", "friendlier-names")
					.attr("type", "checkbox")
					.attr("disabled", true)
			)
			.append(
				$("<label>")
					.attr("for", "friendlier-names")
					.attr("title", "Check to Switch to Friedlier Names")
					.text("Friendlier Names")
			)
	);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", pieChartId)
		)
		.append(
			$("<div>").attr("id", barChartId)
		);
	
	initSvg(pieChartId);
	initSvg(barChartId);
	
	//var localData = retrieveLocalObject(config.currentFilename);
	//pageData = (localData) ? localData : pageData;

	/*
	var currentA = $("#navigation a[href$='" + currentFilename + "']");
	console.log(currentA);
	currentA.addClass("active");
	
	var currentUl = currentA.parent().parent();
	currentUl.prev().addClass("active");
	currentUl.prev().parent().addClass("expanded");
	
	var topUl = currentUl.parent().parent();
	if (topUl.css("display") == "none") {
		topUl.prev().addClass("active");
		topUl.prev().parent().addClass("expanded");
		topUl.slideToggle();
	}
	*/
	
	//updateTitle();
	
	/*
	var currentUl = $("#navigation a.active[href$='" + config.currentFilename + "']:first").next();
	
	// Populate the chart drop down menu from the config file variables
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		currentUl.append(
			$("<li />").append(
				$("<a />")
					//.addClass(function() {if (pageData && pageData.availableChart == i) return "active"; })
					.addClass(function() {return (i==0) ? "active" : ""; })
					.text(availableChart.title)
					.attr("href", i)
					.attr("title", availableChart.title)
					.click(function(e) {
						e.preventDefault();
						
						$(this).parent().parent().find("a").removeClass("active");
						$(this).addClass("active");
						
						//pageData.availableChart = parseInt($(this).attr("href"));
						//storeLocalObject(config.currentFilename, pageData);
						
						// generateCharts();
						if (restData)
							drawCharts();
					})
			)
		);
	});
	*/
	
	//$("#nav-ul a.active").next().slideToggle();
	//if (reportOptions.availableCharts.length > 1)
	//	currentUl.slideToggle();

	if (reportOptions.hasFriendlierNames) {
		$("#friendlier-names").attr("disabled", false);
		$("#friendlier-names").prop("checked", true);
		$("#friendlier-names").click(function() {
			if (restData)
				drawCharts();
		});
	}
	
	$("#filter").click(function() {
		generateCharts();
	});
	
	//updateChartHeight();
	generateCharts();
	
	/*
	$("select#available-charts-options").change(function() {
		pageData.availableChart = parseInt($(this).val());
		storeLocalObject(config.currentFilename, pageData);
		if (restData)
			drawCharts();
	});
	*/
	
}

function generateCharts() {
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();

	var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);

	req.sendSingleAsyncRequest(pValues, drawCharts);
} // end of generateCharts()

/*
function updateChartHeight() {
	// Adjust the height of the charts automatically
	//var windowHeight = $("#content").height() - 40;
	var windowHeight = $("#content").height() - 3.0 * $("#sub-header").height();
	$("#piechart").css("height", windowHeight);
	$("#barchart").css("height", windowHeight);
}
*/

function drawCharts(data) {
	if (data) {
		if (reportOptions.processFunction)
			restData = reportOptions.processFunction(data);
		else
			restData = data;
	}	
	
	//var selectedChart = reportOptions.availableCharts[parseInt($("select#available-charts-options").val())];
	//var selectedChart = reportOptions.availableCharts[parseInt($("#nav-ul a.active").next().find("a.active").attr("href"))];
	
	//var currentUl = $("#navigation a.active[href$='" + config.currentFilename + "']:first").next();
	//var selectedChart = reportOptions.availableCharts[parseInt(currentUl.find("a.active").attr("href"))];
	selectedChart = reportOptions.availableCharts[currentStat];
	
	//$("#content-description").empty();
	//$("#content-description").html(selectedChart.description);
	
	var chartData = [], 
		chartValuesSum = 0;
	$.each(restData, function(i, element) {
		var chartObject = {
			Name: selectedChart.getName(element),
			Value: selectedChart.getValue(element)
		};
		
		chartData.push(chartObject);
		chartValuesSum += chartObject.Value;
	});

	// If the summary of the values is 0 the pie chart will cause errors, so clean the graphs and return
	if (chartValuesSum == 0) {
		Sexy.alert(config.noDataText);
		cleanGraph(pieChartId);
		cleanGraph(barChartId);
		return;
	}	

	//var datum = [{values: chartData, color: config.chartColour1}];
	chartData.sort(function (a, b) {return (a.Value > b.Value) ? -1 : 1} )
	var datum = [{values: chartData}];
	
	drawPiechart(datum, selectedChart);
	
} // end of drawCharts()

function drawPiechart(datum, selectedChart) {
	nv.addGraph({
		generate: function() {
			
			var chart = nv.models.pieChart()
				.x(function(d) {return d.Name })
				.y(function(d) {return d.Value })
				.margin(selectedChart.pieMargin)
				.showLabels(true)
				.showLegend(false)
				.donut(true)
				.donutLabelsOutside(true)
				.tooltipContent(function(key, y, e, graph) { 
					var sum = d3.sum(graph.container.__data__[0].values, function(d) {return d.Value;});
				
					var thisChart = reportOptions.availableCharts[currentStat];
					
					// 	Use e.value instead of y, y is a formated string and parsing to number fails
					var percentage = ((e.value/sum)*100).toFixed(2);
										
					if (thisChart.units == "hours") {
						tooltipValue = formatSecsWithDays(e.value*config.anHourInSecs);
						tooltipSum = formatSecsWithDays(sum*config.anHourInSecs);
					}
					else {
						tooltipValue = (commasFixed(e.value, 4) + " " + thisChart.units);
						tooltipSum = (commasFixed2(sum, 4) + " " + thisChart.units);
					}
					
					return "<h3>" + key + "</h3>"
							+ "<p>" +  tooltipValue 
							+ ((thisChart.title != "Unique Players") 
									? (" - " + percentage + "%" + "<br /> (of " + tooltipSum + ")") 
									: " ") 
							+ "</p>";
				});
			
			var svg = d3.select("#" + pieChartId + " svg"); 
			
			svg.datum(datum)
				.transition()
					.duration(config.transitionDuration)
					.call(chart);
			 		
			nv.utils.windowResize(chart.update);
			//nv.utils.windowResize(updateChartHeight);
			nv.utils.windowResize(function() {
				drawDonutLabel(pieChartId, selectedChart.title);
				decorateKeys(pieChartId); 
			});
	 	
			return chart;
		},
		callback: function() {
			drawDonutLabel(pieChartId, selectedChart.title);
			decorateKeys(pieChartId);
			drawBarchart(datum, selectedChart);
		},
	});
	//$("#piechart legend").text(pieChartLegend + selectedChart.title);

}

function drawBarchart(datum, selectedChart) {
	
	nv.addGraph({
		generate: function() {
			var chart = nv.models.multiBarHorizontalChart()
				.x(function(d) {return d.Name })
				.y(function(d) {return d.Value })
				.margin({top: 20, right: 20, bottom: 50, left: selectedChart.leftMargin})
				.showValues(true)
				.tooltips(false)
				.showControls(false)
				.showLegend(false);
			
			chart.yAxis
				.axisLabel(selectedChart.label)
				.tickFormat(d3.format(".02f"));
	
			d3.select("#" + barChartId + " svg")
				.datum(datum)
				.transition()
					.duration(config.transitionDuration)
					.call(chart);
	 
			nv.utils.windowResize(chart.update);
			//nv.utils.windowResize(updateChartHeight);
	 
			return chart;
		},
		callback: function() {
			matchColours(pieChartId, barChartId)
		}
	});
	//$("#barchart legend").text(barChartLegend + selectedChart.title);
}

function matchColours(piechartID, barchartID) {
	
	var colours = [];
	d3.select("#" + piechartID + " svg").selectAll(".nv-slice").each(function(d, i) {
		colours.push(d3.select(this).attr("fill"));
	});
	
	d3.select("#" + barchartID + " svg").selectAll(".nv-bar").each(function(d, i) {
		d3.select(this).attr("fill", colours[i]);
	});
	
}

function drawDonutLabel(elementID, labelText) {
	var svg = d3.select("#" + elementID + " svg");
	
	svg.selectAll(".donut-label").remove();

	svg.append("svg:text")
		.attr("class", "donut-label")
    	.attr("x", $("#" + elementID).width() / 2)
    	.attr("y", $("#" + elementID).height() / 2)
    	.attr("text-anchor", "middle")
    	.attr("stroke", 5)
    	.text(labelText);
}

function decorateKeys(elementID) {
	$(".nv-legend rect").remove();
	
	var legend = d3.select("#" + elementID + " svg")
				.select("g.nv-legend g");
	if (!legend[0][0])
		return;
	
	var bbox = legend[0][0].getBBox();
	var margin = 5;
	
	legend.append("svg:rect")
		.attr("x", bbox.x - margin)
		.attr("y", bbox.y - margin)
		.attr("width", bbox.width + 2*margin)
		.attr("height", bbox.height + 2*margin)
		.attr("rx", margin)
		.attr("ry", margin)
		.attr("pointer-events", "none");
}

