
var barHeight = 20; //pixels
// reportOptions variable comes from respective config file

var defaultFilterText = "type comma separated keys to filter the results";
// Array that stores objects for each element returned via rest
var chartsData = [];

var filterInputId = "filter-input";
var barchartDOMIdPrefix = "single-barchart_"; 

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content")
		.append(
			$("<div>")
				.attr("id", "friendlier-names-field")
				.append(
					$("<input>")
						.attr("id", "friendlier-names")
						.attr("type", "checkbox")
						.attr("disabled", true)
				)
				.append(
					$("<label>")
						.attr("for", "friendlier-names")
						.attr("title", "Check to Switch to Friedlier Names")
						.text("Friendlier Names")
				)	
		);
	
	if (reportOptions.hasFilterInput) {
		$("#content-description").append(
			$("<input />")
				.attr("id", filterInputId)
				.attr("type", "text")
				.val(defaultFilterText)
				.focus(function() {
					if($(this).val() == defaultFilterText)
						$(this).val("");
				})
				.blur(function() {
					if ($(this).val() == "")
						$(this).val(defaultFilterText);
				})
				.keyup(function() {
					if (chartsData)
						typewatch(drawCharts, 500);
				})
		);
	}
	
	if (reportOptions.hasFriendlierNames) {
		$("#friendlier-names").attr("disabled", false);
		$("#friendlier-names").prop("checked", true);
		$("#friendlier-names").click(function() {
			if (chartsData)
				typewatch(drawCharts, 500);
		});
	}
	
	if (reportOptions.description) {
		$("#content-description")
			.append(
				$("<div>")
					.addClass("description")
					.html(reportOptions.description)
			)
	}
	
	if (reportOptions.enableCSVExport) {
		$("#content-description")
			.append(
				$("<div>")
					.attr("id", "content-description-controls")
					.html(" ")
			)
	}
	
	$("#filter").click(function() {
		generateCharts();
	});
	
	generateCharts();
}

function generateCharts() {
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
		
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
	
} // end of generateCharts()

function drawCharts(data) {
	if (data)  {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
	
	if (chartsData.length == 0) {
		//cleanGraph("single-barchart");
		// clean all the previous charts
		$("#content-body").find(".barchart").remove()
		Sexy.alert(config.noDataText);
	}
	
	var filterText = $("#" + filterInputId).val();
	if (filterText == defaultFilterText)
		filterText = "";
	
	var groups = reportOptions.chartGroups.getGroups(chartsData);

	if (groups.length >= 2) {
		// clean all the previous charts for many groups 
		$("#content-body").find("fieldset.barchart").remove();
	}
		
	$.each(groups, function(gindex, group) {
		var groupName = reportOptions.chartGroups.getGroupName(group);
		var groupValues = reportOptions.chartGroups.getGroupValues(group);
		
		//Add the graph elements if they don't exist
		if (!$("#" + barchartDOMIdPrefix + gindex).length) {
			$("#content-body")
				.append(
					$("<div>")
						.attr("id", barchartDOMIdPrefix + gindex)
						.addClass("barchart")
				)
			if (groupName)
				$("#" + barchartDOMIdPrefix + gindex)
					.append(
						$("<legend>")
							.addClass("barchart-title")
							.html(groupName)
				)
			initSvg(barchartDOMIdPrefix + gindex);
		}
		else {
			if (groupName)
				$("#" + barchartDOMIdPrefix + gindex + " legend").html(groupName);
		}
				
		var filterKeys = (filterText) ? filterText.split(",").map(function(d) {return (d.trim()); }) : "";
		var filterReg = (filterKeys) ? (filterKeys.join(".*") + "|" + filterKeys.reverse().join(".*")) : "";
		
		//console.log(filterReg);
		var re = new RegExp(filterReg, "i");
		
		// create an array that will store the filtered chart data
		var datum = [];
		
		var csvColumns = [" "];
		var csvRows = [];
		
		// Loop for each defined bar
		$.each(reportOptions.bars, function(i, bar) {
			var filteredArray = [];
			
			// loop to each element's data
			$.each(groupValues, function(j, groupValue) {
				// match the name with the input filter text
				//console.log(filterText + " : " +  bar.getName(groupValue) + " : " + re.test(bar.getName(groupValue)))
				
				if (re.test(bar.getName(groupValue))) {
					// for each chart type, populate its array with the predefined functions over the data 
					filteredArray.push({Name: bar.getName(groupValue, j),
									Value: bar.getValue(groupValue),
									Object: bar.getObject(groupValue),
					});
					
				}
				
			});
			
			datum.push(
				{
					key: bar.title,
					values: filteredArray,
					color: bar.colour,
				}
			);
			
			$.each(filteredArray, function(i, arrayItem) {
				if (!csvRows[i]) {
					csvRows[i] = [];
					csvRows[i][0] = arrayItem.Name;
				}
				csvRows[i][csvColumns.length] = arrayItem.Value;
			});
			
			csvColumns.push(bar.title);
		
		}); // End of each reportOptions.bars
		
		// all the elements will have the same length so selecting the first one 
		if (datum[0].values.length < 1) { 
			//console.log("Nothing matched for '" + filterText + "'");
			return;
		}
		
		var cssHeight = (barHeight*reportOptions.bars.length) * (datum[0].values.length + 2); // pixels
		var contentBody = $("#content-body").height() - 30;

		if ((cssHeight < contentBody) && (groups.length == 1)) {
			$("#" + barchartDOMIdPrefix + gindex).css("height", contentBody);
			$("#" + barchartDOMIdPrefix + gindex + "svg").css("height", contentBody);
		}
		else {
			$("#" + barchartDOMIdPrefix + gindex).css("height", cssHeight);
			$("#" + barchartDOMIdPrefix + gindex + "svg").css("height", cssHeight);
		}
		
		if (reportOptions.enableCSVExport) {
			var csvFrameID = "single-barchart-frame";
			
			$("#" + csvFrameID).remove();
				
			addCSVButton(reportOptions.enableCSVExport, 
					csvFrameID, 
					function() {
						exportToCSV(csvColumns, csvRows, csvFrameID, encodeURIComponent(reportOptions.graphTitle + ".csv")); 
					});
			
			$("#" + csvFrameID).addClass("csv-right");
		}
			
		nv.addGraph(function() {
			var chart = nv.models.multiBarHorizontalChart()
				.x(function(d) { return d.Name })
				.y(function(d) { return d.Value })
				.margin({top: 20, right: 30, bottom: 50, left: reportOptions.chartLeftMargin})
				.showValues(true)
				.tooltips(true)
				.showControls(false)
				.showLegend((reportOptions.bars.length>1) ? true : false);
				
			chart.tooltipContent(reportOptions.tooltipContent);
				
			chart.yAxis
				.axisLabel(reportOptions.units)			
			if (reportOptions.yTickFormat)
				chart.yAxis.tickFormat(reportOptions.yTickFormat);
			else
				chart.yAxis.tickFormat(d3.format(".2f"));
			
			d3.select("#" + barchartDOMIdPrefix + gindex + " svg")
				.datum(datum)
				.transition()
					.duration(config.transitionDuration)
				.call(chart);
			
			nv.utils.windowResize(chart.update);
			
			return chart;
		}); // End of addGraph
		
		
	})
			
}
