// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var commas = function(d) { return d3.format(",")(d); };
var currentStat = "Percent Complete";
var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "TOTAL_PROGRESS_MADE",
     ],
     name: currentStat,
     requestSubString:  "_",
     bucketSize: 5,
     bucketCount: null,
     singleMetric: true,
},
];

var reportOptions = {
		
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
	
	availableCharts: profileStatList,

	hasExtraRestParams: [
	    {
	    	key: "StatNames",
	    	value: getStatNames,
	    },
	    {
	    	key: "BucketSize",
	    	value: profileStatList[0].bucketSize,
	    },
	],
	
	processFunction: convertResultToDict,
	
	enableCSVExport: "content-description",
	graphTitle: currentStat,
		
	/* Line graph related */
	elementId: "line-area-chart",
	//backgroundColour: "#ffffff",
	backgroundColour: "transparent",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.name; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) { return d.value; },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Number of Players",
		
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
		
	hideLegend: true,
	legend: {height: 30, width: 180, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + "</div><br /><br />"
			+ "<table>"
					+ "<tr><td>Number of Players:</td><td class='right'>" + commas(d.value) + "</td></tr>"
				+ "</table>";
			return content;
	},
			              
};

function convertResultToDict(array) {
	$("#date-from").addClass("hidden");
	$("#date-from").parent().addClass("hidden");
	
	var dict = {}; 
	dict[reportOptions.yLabel] = [];
	
	array.map(function(d) {
		if (d.Bucket.split("-")[0] == "100")
			d.Bucket = "100";
		dict[reportOptions.yLabel].push({ name: d.Bucket + " %", value: Number(d.YValue) });
	});
	
	return dict;
}

function getStatNames() {
	
	gameTypes = ($("#game-types").val()) ? $("#game-types").val() : config.gameTypes;
	
	var statNames = [];
	$.each(reportOptions.availableCharts, function (i, profileStat) {
		statNames = statNames.concat(
			profStat.constructStatNames(profileStat, profileStat.types[0], gameTypes)
		);
	});
		
	return statNames.join(",");
}
