// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {

	restEndpoint: config.freemodeActivitiesStats,
	restEndpointAsync: config.freemodeActivitiesStatsAsync,
	
	enableCSVExport: "content-description",
	graphTitle: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Activities",
	
	chartGroups: {
		getGroups: function(d) {return [d]}, // One group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
		title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Activities (times performed)",
		colour: config.chartColour1,
	    getName: function(d) {return d.Name;},
	    getValue: function(d) {return d.TimesPerformed;},
	    getObject: function(d) {return d;},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
				+ "<tr><td>Times Performed: </td><td class='right'>" + e.point.Object.TimesPerformed + "</td></tr>"
				+ "<tr><td>Unique Gamers: </td><td class='right'>" + e.point.Object.NumUniqueGamersPerformed + "</td></tr>";
		html += "</table>"; 		
		return html;
	},

	units: "Times Performed",
	
	chartLeftMargin: 100,
};

