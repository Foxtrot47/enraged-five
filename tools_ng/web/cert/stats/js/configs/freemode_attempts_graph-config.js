// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var rosPlatformsDict = getRosPlatformsDict();
var freemodeCategoriesDict = getFreemodeCategoriesDict();

var currentStat = "Average Attempts per User";

var reportOptions = {
		
	restEndpoint: config.freemodeMissionsStats,
	restEndpointAsync: config.freemodeMissionsStatsAsync,
	
	processFunction: convertToDict,
	
	hasExtraRestParams: [
    {
	     key: "MaxVariants",
	     value: 1000,
	},
    ],
	
    enableCSVExport: "content-description",
	graphTitle: currentStat,
    
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.Name; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) {return (round2(d.TotalGamers/d.UniqueGamers)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 180, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, 
	
	valueTooltipContent: function(d, b) {
		var html = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
				+ "<table>"
					+ "<tr><td>Average Attempts per User: </td><td class='right'>" + this.value(d) + "</td></tr>"
					+ "<tr><td>Total User Attempts: </td><td class='right'>" + d.TotalGamers + "</td></tr>"
					+ "<tr><td>Unique Users: </td><td class='right'>" + d.UniqueGamers + "</td></tr>";
					/*
					+ "<tr><td>Average Time: </td><td class='right'> " + formatSecs(d.TimeSpentPlaying/d.NumUniqueGamersPlayed) + "</td></tr>"
					+ "<tr><td>Total Time: </td><td class='right'> " + formatSecsWithDays(d.TimeSpentPlaying) + "</td></tr>"
					*/
			/*
			if (typeof d.AverageRating !== "undefined")
				html += "<tr><td>Average Rating:</td><td class='right'>" + commasFixed2(d.AverageRating) + "</td></tr>";
			if (typeof d.UGCIdentifier !== "undefined")
				html += "<tr><td>UGC ID:</td><td class='right'>" + d.UGCIdentifier + "</td></tr>";
			if (typeof d.CreatedDate !== "undefined")
				html += "<tr><td>Created At:</td><td class='right'>" + parseJsonDate(d.CreatedDate).toUTCString() + "</td></tr>";
		
			var creatorPlatform = (d.CreatorPlatform) ? (" (" + rosPlatformsDict[d.CreatorPlatform] + ")") : "";
			if (typeof d.Creator !== "undefined")
				html += "<tr><td>Creator:</td><td class='right'>" + d.Creator + creatorPlatform + "</td></tr>";
		
			if (typeof d.Category !== "undefined")
				html += "<tr><td>Category:</td><td class='right'>" + freemodeCategoriesDict[d.Category] + "</td></tr>";	
		
			if (typeof d.IsPublished !== "undefined")
				html += ("<tr><td>UGC Status:</td><td class='right'>" 
					+ ((d.IsPublished) ? "Published" : "Saved")
					+ "</td></tr>");
			*/
			
			html += "</table>";

		return html;
	},
		              
};

function convertToDict(array) {
	var dict = {};
	var missions = array.filter(function(d) {
			return (d.MatchType == 0); 
	 	});
	
	dict[currentStat] = (missions.length > 0)
						? missions[0].Variants
							.sort(function(a, b) {
								return ((a.TotalGamers/a.UniqueGamers) > (b.TotalGamers/b.UniqueGamers)) ? -1 : 1 
							})
						: [];
	
	return dict;
}
