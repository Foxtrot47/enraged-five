//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var flowStages = [
    "ARM3",
    "JH2a",
    "JH2b",
    "TRV3",
    "FIB4",
    "MAR1",
    "FIB5",
    "AH3a",
    "AH3b",
    "FINA",
    "FINB",
    "FINC2",
];

var reportOptions = {
	restEndpoint: config.characterSkills,
	restEndpointAsync: config.characterSkillsAsync,
	
	processFunction: formatData,
	//multipleRequests: generateEndpoints,
		
	hasFriendlierNames: false,
	
	description: "",
	
	chartGroups: {
		getGroups: function(d) { return d; }, // Data are returned grouped
		getGroupName: function(d) { return d.MissionName; },
	    getGroupValues: function(d) { return d.Skills; },
   	},
	
	bars: [
	{
	    title: "Skill Percentage",
	    colour: config.chartColour1,
	    getName: function(d) {
	    	return d.SkillName;
	    },
	    getValue: function(d) { return d.SkillValue },
	    getObject: function(d) { return d },
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
					+ "<tr><td>" + key + ": </td><td class='right'>" 
						+ commasFixed2(Number(e.value))
					+ "</td></tr>";
		html += "</table>"; 
			
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(Number(d));
	},
	units: "%",
	
	chartLeftMargin: 100,
};

/*
function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + config.weaponModPurchases,
			restAsyncUrl: config.restHost + config.weaponModPurchasesAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}
*/

function formatData(data) {
	var resultObject = {};
		
	if (data.length == 0)
		return [];
	
	$.each(data, function(i, r) {
		
		var flowIndex = flowStages.indexOf(r.MissionId);
		
		if (flowIndex !== -1) {
			var skillsObject = r.Michael;
			var skills = [
            {
            	"SkillName" : "Special",
            	"SkillValue" : skillsObject.SpecialAbility,
			},
			{
            	"SkillName" : "Stamina",
            	"SkillValue" : skillsObject.Stamina,
			},
			{
            	"SkillName" : "Shooting",
            	"SkillValue" : skillsObject.Shooting,
			},
			{
            	"SkillName" : "Strength",
            	"SkillValue" : skillsObject.Strength,
			},
			{
            	"SkillName" : "Stealth",
            	"SkillValue" : skillsObject.Stealth,
			},
			{
            	"SkillName" : "Flying",
            	"SkillValue" : skillsObject.Flying,
			},
			{
            	"SkillName" : "Driving",
            	"SkillValue" : skillsObject.Driving,
			},
			{
            	"SkillName" : "Lung Capacity",
            	"SkillValue" : skillsObject.LungCapacity,
			},
			];
			
			r["Skills"] = skills;
			
			resultObject[flowIndex] = r;
		}
		
	});
	
	// create an ordered by key-index array
	var result = Object.keys(resultObject).map(function(d) { return resultObject[d]; });
	
	return result;
}
