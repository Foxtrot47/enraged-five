// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
{
	types: [
	"DROWNING",
	//"DROWNINGINVEHICLE",
	"EXPLOSION",
	"FALL",
	"FIRE",
	"ROAD"
	],
	requestSubString: "_DIED_IN_",
	name: "Death Types",
	description: "No of deaths for each death type",
	label: "No of Deaths",
	units: "deaths",

	bucketSize: null,
},
{
	types: [
	"DEATHS",
	"TOTAL_PLAYING_TIME"
	],
	requestSubString: "_",
	name: "Deaths per Hour",
	description: "No of players per bucket of 0.5 deaths per hour",
	label: "No of players",
	units: "players",
	
	bucketSize: 0.5 / config.anHourInMillisecs, // Bucket of 0.5 deaths per hour in millisecs,
	
	divided: true,
	
	perHour: true,
},                       
{
	types: [
	"DEATHS",
	],
	requestSubString: "_",
	name: "Player Deaths",
	description: "No of players per bucket of 10 deaths",
	label: "No of players",
	units: "players",
	
	bucketSize: 10,
},
{
	types: [
	"BUSTED",
	"TOTAL_PLAYING_TIME"
	],
	requestSubString: "_",
	name: "Arrests per Hour",
	description: "No of players per bucket of 0.1 arrests per hour",
	label: "No of players",
	units: "players",

	bucketSize: 0.1 / config.anHourInMillisecs, // Bucket of 0.1 arrests per hour in millisecs,

	divided: true,
	perHour: true,
	
	//singleOnly: true,
},
{
	types: [
	"BUSTED",
	],
	requestSubString: "_",
	name: "Player Arrests",
	description: "No of players per bucket of 10 arrests",
	label: "No of players",
	units: "players",
	
	bucketSize: 5,
	
	//singleOnly: true,
}];

var reportOptions = {
		
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
				
	availableCharts: profileStatList,
}