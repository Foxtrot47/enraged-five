//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-missions", 	// filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // date-grouping
	     false, // dates+builds
	     false, // mission categories
	     false, // mission ugc gamertags
	     false, // created dates
	],
};

var items = [
	{
		metric: "$ R* Takings" ,
		//val: function(d) {return -1*d.AmountWon + d.RockstarCommission;},
		val: function(d) {return -1*d.AmountWon;}, // b* #1591546
	},
	/*
	{
   		metric: "$ R* Commision" ,
   		val: function(d) {return d.RockstarCommission;},
   	},
   	*/
	{
   		metric: "$ R* Payout" ,
   		val: function(d) {return ( d.AmountBet - (-1*d.AmountWon) );},
   	},
   	{
   		metric: "$ Players Takings" ,
   		val: function(d) {return d.AmountWon;},
   	},
   	{
   		metric: "$ Amount Players Bet" ,
   		val: function(d) {return d.AmountBet;},
   	},
];

var reportOptions = {
		
	restEndpoint: config.bettingEarned,
	restEndpointAsync: config.bettingEarnedAsync,
	
	processFunction: convertToDict,
	
	
	enableCSVExport: "content-description",
	graphTitle: "Amount Bet",
	
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: "#000000",
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return round2(d.value); },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 180, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: "#333333"}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
			+ "<table>";
		
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'> $ " + commasFixed2(item.val(d.values)) + "</td></tr>");
			});
			
			content += "<tr><td>Total Bets:</td><td class='right'>" + (d.values.BetsMade) + "</td></tr>";
			content += "<tr><td>Bets Won:</td><td class='right'>" + (d.values.BetsThatWon) + "</td></tr>";
			content += "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	
	var dict = {};
	
	if (!array.Values)
		return dict;
	
	array.Values.map(function(d) {
		var date = toUTCStringWOSecs(parseJsonDate(d.StartInterval));
		
		$.each(items, function(i, item) {
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: date, value: item.val(d), values : d}];
			else
				dict[item.metric].push({ name: date, value: item.val(d), values : d});
		});
		
	});
	
	var rakes = (array.Rakes.length) 
				? array.Rakes.map(function(r) {
					return (" " + round2(Number(r.Key)*100) + "% (" + r.Value + " times)")
				}).join(",")
				: "";
	
	$("#content-description")
		.empty()
		.html("Rakes : " + rakes);
	
	return dict;
}
