var headerAndFilters  = {
	headerType: "header-sc-fps", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     false, // level
	     false, // match type
	     false, // resolution
	     false, // freeroam flag
	     ],
};

var reportOptions = {
	restEndpoint: config.mapFpsStats,
	restEndpointAsync: config.mapFpsStatsAsync,
	
	//processFunction: calcTotals,
	
	mapDataFunctions: {
		getValue: function(d) { return frameTimeToFPS(d.AvgFrameTime); },
		getX: function(d) { return Number(d.Location.X); },
		getY: function(d) { return Number(d.Location.Y); },
		getResolution: function(d) { return Number($("#resolution").val()); },
		getTitle: function(d) {
		  		var title = ""
				+ "Average FPS: " + commasFixed2(frameTimeToFPS(d.AvgFrameTime))
				+ ", \nLower Left Corner: (x=" + d.Location.X
				+ ", y=" 
				+ d.Location.Y
				+ ")"
				+ "\nSample Size: "
				+ d.SampleSize;
											
			return title;
		},
	},
	
};

function calcTotals(data) {
	
	return	data;
}
