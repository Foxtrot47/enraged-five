//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// capture stats filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // date-grouping
	     [false, false, false], // dates+builds
	],
};

var items = [
	{
		metric: "Concurrent Players" ,
		val: function(d) {return d.Value;},
	}
	];

var reportOptions = {
		
	restEndpoint: config.concurrentUsers,
	restEndpointAsync: config.concurrentUsersAsync,
	
	processFunction: convertToDict,
	
	enableCSVExport: "content-description",
	graphTitle: "Total Players",
	
	elementId: "line-area-chart",
	//backgroundColour: "#ffffff",
	backgroundColour: "transparent",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 130, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "Concurrent Players", colour: config.chartColour1}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br /><br />"
			+ "<table>";
	
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'>" + commasFixed2(item.val(d.values)) + "</td></tr>");
				content += ("<tr><td>Unique Players:</td><td class='right'>" + commasFixed2(item.UniqueGamers) + "</td></tr>");
			});
								
			content += "</table>";

		return content;
	},
		              
};

function convertToDict(data) {
	
	var dict = {};
	
	data.Values.map(function(d) {
		var date = toUTCStringWOSecs(parseJsonDate(d.Key));
				
		$.each(items, function(i, item) {
			item["UniqueGamers"] = data.UniqueGamers;
			
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: date, value: item.val(d), values : d }];
			else
				dict[item.metric].push({ name: date, value: item.val(d), values : d });
		});
		
	});
	
	return dict;
}
