// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
    /*
    {
    	types: ["MISS_SWITCH_agency_heist1"],
    	name: "The Agency Heist 1 - Setup - Follow Janitor",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist2A"],
    	name: "The Agency Heist 2 - Mug the Architect",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist2B"],
    	name: "The Agency Heist 2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist2C"],
    	name: "The Agency Heist 2C",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist3A"],
    	name: "The Agency Heist 3A Finale - Plant Bombs",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist3B"],
    	name: "The Agency Heist 3B Finale - Steal FIB Docs (Helicopter)",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist4A"],
    	name: "The Agency Heist 4A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_armenian1"],
    	name: "Armenian 1 - Franklin and Lamar",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_armenian2"],
    	name: "Armenian 2 - Repossession",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_armenian3"],
    	name: "Armenian 3 - Complications",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal_intro_cut"],
    	name: "Car Steal Intro Cutscene",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal1"],
    	name: "Car Steal 1 - I fought the law...",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal2"],
    	name: "Car Steal 2 -  Eye in the Sky",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal3"],
    	name: "Car Steal 3 - Agent",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal4"],
    	name: "Car Steal 4 - Finale - Pack Man",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_carsteal5"],
    	name: "Car Steal 5",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_chinese1"],
    	name: "Chinese 1 - Trevor Philips Industries",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_chinese2"],
    	name: "Chinese 2 - Crystal Maze",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_heistA"],
    	name: "Docks Heist A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_heistB"],
    	name: "Docks Heist B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_prep1"],
    	name: "Docks Heist Prep1",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_prep2B"],
    	name: "Docks Heist Prep2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_setup"],
    	name: "Docks Setup",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_exile1"],
    	name: "Exile 1 - Minor Turbulence",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_exile2"],
    	name: "Exile 2 - Predator",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_exile3"],
    	name: "Exile 3 - Derailed",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family1"],
    	name: "Family 1 - Father and Son",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family2"],
    	name: "Family 2 - Daddy's Little Girl",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family3"],
    	name: "Family 3 - Marriage Counselling",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family4"],
    	name: "Family 4 - Fame or Shame",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family5"],
    	name: "Family 5 - Did somebody say yoga",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_family6"],
    	name: "Family 6 - Reunite Family ",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi1"],
    	name: "FIB 1 - Dead Man Walking",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi2"],
    	name: "FIB 2 - Three's Company",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi3"],
    	name: "FIB 3 - By The Book",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi4"],
    	name: "FIB 4",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi5A"],
    	name: "FIB 5A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi5B"],
    	name: "FIB 5B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale_heist1"],
    	name: "Finale Heist 1",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale_heist2A"],
    	name: "Finale Heist 2A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale_heist2B"],
    	name: "Finale Heist 2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale1"],
    	name: "Finale 1 - Kill Trevor",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale2"],
    	name: "Finale 2 - Kill Michael",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale3"],
    	name: "Finale 3 - Save Michael and Trevor",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_franklin0"],
    	name: "Franklin 0 - Chop",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_franklin1"],
    	name: "Franklin 1 - Ghetto Safari",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_franklin2"],
    	name: "Franklin 2 - Lamar Down",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_jewelry_heist"],
    	name: "Lamar1 - The Long Stretch",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_jewelry_setup1"],
    	name: "Jewelry Setup1",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_lamar1"],
    	name: "Lamar 1",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_lester1a"],
    	name: "Lester1 - Friend Request A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_lester1b"],
    	name: "Lester1 - Friend Request B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_michael1"],
    	name: "Michael 1 - Bury the Hatchet",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_michael2"],
    	name: "Michael 2 - Fresh Meat",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_michael3"],
    	name: "Michael 3 - The Wrap Up",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_nice_house_heist2A"],
    	name: "Nice House Heist 2A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_nice_house_heist2B"],
    	name: "Nice House Heist 2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_nice_house_heist2C"],
    	name: "Nice House Heist 2C",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_nice_house_setup1A"],
    	name: "Nice House Heist Setup 1A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_nice_house_setup1B"],
    	name: "Nice House Heist Setup 1B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_prologue1"],
    	name: "Prologue",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_rural_bank_heist"],
    	name: "Rural Bank Heist",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_rural_bank_setup"],
    	name: "Rural Bank Setup",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_solomon1"],
    	name: "Solomon 1 - Mr. Richards",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_solomon2"],
    	name: "Solomon 2 - The Ballad of Rocco",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_solomon3"],
    	name: "Solomon 3 - Blow Back",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_solomon4"],
    	name: "Solomon4",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_solomon5"],
    	name: "Solomon5",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor1"],
    	name: "Trevor 1 - Mr. Philips",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor2"],
    	name: "Trevor 2 - Nervous Ron",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor3"],
    	name: "Trevor 3 - Friends Reunited",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor4"],
    	name: "Trevor 4",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor5"],
    	name: "Trevor5",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_trevor6"],
    	name: "Trevor6",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    */ 
    // New List b*#1598548
    {
    	types: ["MISS_SWITCH_family3"],
    	name: "Family 3 - Marriage Counselling",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi2"],
    	name: "FIB 2 - Three's Company",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi3"],
    	name: "FIB 3 - By The Book",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_fbi4"],
    	name: "FIB 4",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_franklin1"],
    	name: "Franklin 1 - Ghetto Safari",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_franklin2"],
    	name: "Franklin 2 - Lamar Down",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_exile3"],
    	name: "Exile 3 - Derailed",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_michael2"],
    	name: "Michael 2 - Fresh Meat",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_michael3"],
    	name: "Michael 3 - The Wrap Up",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale3"],
    	name: "Finale 3 - Save Michael and Trevor",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    
    // 
    {
    	types: ["MISS_SWITCH_docks_heistA"],
    	name: "The Port of LS Heist 2A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_docks_heistB"],
    	name: "The Port of LS Heist 2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_rural_bank_heist"],
    	name: "The Paleto Score 2A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist3A"],
    	name: "The Agency Heist 3A Finale - Plant Bombs",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_agency_heist3B"],
    	name: "The Agency Heist 3B Finale - Steal FIB Docs (Helicopter)",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale_heist2A"],
    	name: "The Big Score 2A",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    {
    	types: ["MISS_SWITCH_finale_heist2B"],
    	name: "The Big Score 2B",
    	bucketSize: null,
        singleMetric: true,
        isAverage: true,
    },
    
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,

	/* Tabular Report conf */
	
	enableCSVExport: true,
	
	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Mission Name", getValue: function(d) {return d.name;} },
	    {title: "Average Switches", getValue: function(d) {return commasFixed(d.value, 4);} },
	    //{title: "Total Switches", getValue: function(d) {return d.Total;} },
	    //{title: "Total Players", getValue: function(d) {return d.YValue;} },
	],
	//reportArraySort: [[0, 0]], // [first item ASC]
	
	/* Groups */
	groups: [
	    {title: "Switches per mission (non-objective switches only)", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: false,
	
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		
		var values = [];
		
		data[i][0].response.map(function(d) {
		   	d["name"] = availableChart.name; 
		   	d["value"] = profStat.getValue(d, availableChart);
		   	return d;
		})
				
		array.push(data[i][0].response[0]);
	});
	
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
