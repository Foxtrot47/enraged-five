var headerAndFilters  = {
	headerType: "header-sc-weapons", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.weaponKillsDeaths,
	restEndpointAsync: config.weaponKillsDeathsAsync,
	
	processFunction: formatData,
	
	hasExtraRestParams: [
	{
		key: "KillMode",
	    value: true,
	},
	],
				
	isClickable: false,
	hasFriendlierNames: true,
	
	description: "Total no of kills per weapon",
	
	// This is a piechart
	main: {
		title: "Total Kills",
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: true,
		donut: true,
		donutLabelsOutside: false,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d[0].values; },
		getMetadata: function(d) {return d[0].metadata; },
		
		// function to get the  name from the rest data
		getName: getWeaponName,
		
		getValue: function(d) {
			return d.Count; 
		},
		
		unit: "kills",
		
		lrMargin: 55,
	},
	// This is the breakdown barchart
	breakdown: {
		title: "No of Kills",
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d[1].color; },
		getValuesArray: function(d) {return d[1].values; },
		getMetadata: function(d) {return d[1].metadata; },
		
		// function to get the  name from the rest data
		getName: getWeaponName,
		getValue: function(d) {
			return d.Count; 
		},
		
		getYLabel: function(d) {return d.key; },
		
		matchColoursFromPieElement: "piechart",
		
		unit: "kills",
		
		leftMargin: 220,
		
		getObject: function(d) { return d; },
	},
};

function formatData(data) {
	return	[
	{
	   	"label": "", 
	   	"values": data,
	 	"metadata": {},
	},
	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data,
	 	"metadata": {},
	},
	];

}

function getWeaponName(d) {
	return ($("#friendlier-names").is(":checked") && d.WeaponFriendlyName && (d.WeaponFriendlyName != "Invalid")) 
			? d.WeaponFriendlyName : d.WeaponName; 
};
