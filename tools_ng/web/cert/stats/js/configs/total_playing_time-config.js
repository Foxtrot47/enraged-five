// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var commas = function(d) { return d3.format(",")(d); };
var currentStat = "Total Playing Time (hours)";
var profStat = new ProfileStats();

var profileStatList = [
	{
		types: [
		    "TOTAL_PLAYING_TIME",
		],
		name: currentStat,
		requestSubString:  "_",
		bucketSize: 5* config.anHourInMillisecs,
	},
];

var reportOptions = {
		
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	
	availableCharts: profileStatList,

	hasExtraRestParams: [
	    {
	    	key: "StatNames",
	    	value: getStatNames,
	    },
	    {
	    	key: "BucketSize",
	    	value: profileStatList[0].bucketSize,
	    },
	],
	
	processFunction: convertResultToDict,
		
	enableCSVExport: "content-description",
	graphTitle: currentStat,
	
	/* Line graph related */
	elementId: "line-area-chart",
	
	//backgroundColour: "#ffffff",
	backgroundColour: "transparent",
	
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.name; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) { return d.value; },
	label: function(d) { return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Number of Players",
		
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
		
	hideLegend: true,
	legend: {height: 30, width: 200, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + "</div><br />"
			+ "<table>"
				+ "<tr><td>Number of Players:</td><td class='right'>" + commas(d.value) + "</td></tr>"
				+ "<tr><td>Total Players:</td><td class='right'>" + commas(d.totalUsers) + "</td></tr>"
			+ "</table>";
		return content;
	},
			              
};

function convertResultToDict(array) {
	var dict = {}; 
	dict[reportOptions.yLabel] = [];
	
	var totalUsers = 0;
	array.map(function(r) {totalUsers += Number(r.YValue); });
	
	// Add the total users
	//populatedResult.map(function(r) {r["TotalUsers"] = totalUsers; return r; })
	
	array.map(function(d) {		
		d.Bucket = d.Bucket.split("-").map(function(d) {return Number(d)/config.anHourInMillisecs}).join("-"); 
		dict[reportOptions.yLabel].push({ name: d.Bucket, value: Number(d.YValue), totalUsers: totalUsers});
	});
	
	return dict;
}

function getStatNames() {
	
	//MP_PLAYING_TIME
	
	gameTypes = ($("#game-types").val()) ? $("#game-types").val() : config.gameTypes;
	
	var statNames = [];
		
	if (gameTypes.indexOf(config.gameTypes[0]) !== -1)
		statNames = statNames.concat(profStat.constructStatNames(profileStatList[0], profileStatList[0].types[0], [config.gameTypes[0]]));
	
	// Manually add a different stat for multiplayer : url:bugstar:1560540
	if (gameTypes.indexOf(config.gameTypes[1]) !== -1)
		statNames.push("MP_PLAYING_TIME");
	//console.log(statNames);
	
	return statNames.join(",");
}
