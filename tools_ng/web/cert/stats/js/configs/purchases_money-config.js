//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// capture stats filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // date-grouping
	     [true, false, true], // dates+builds
	],
};

var pBet = "$ Amount Players Bet";
var pWon = "$ Amount Players Won";
var rMade = "$ Amount R* Made";

var reportOptions = {
		
	restEndpoint: config.cashPurchasesReport,
	restEndpointAsync: config.cashPurchasesReportAsync,
	
	processFunction: convertToDict,
	
	enableCSVExport: "content-description",
	graphTitle: "Money Purchases",
	
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },

	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	
	value: function(d) { return d.value; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 120},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 150, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "Cash Pack", colour: config.chartColour1}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
				+ "<table>"
					+ "<tr><td>Times Purchased: </td><td class='right'>" + commas(d.values.TimesPurchased) + "</td></tr>"
					+ "<tr><td>Unique Gamers: </td><td class='right'>" + commas(d.values.UniqueGamers) + "</td></tr>"
				+ "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	var postFix = " Cash Packs";
	var dict = {};
	
	var dates = {}; // keep track of all the available dates
	
	// Sort by  amount DESC
	array.CashPackData.sort(function(a, b) { return (a.InGameAmount < b.InGameAmount) ? 1 : -1});
		
	array.CashPackData.map(function(d) {
		if (!dict.hasOwnProperty(d.CashPackName))
			dict[d.CashPackName + postFix] = [];
			
		// Convert the stat data to a dict for efficient lookup when filling the empty valued dates
		d.StatDict = {}; 
		
		d.Stats.map(function(s) {
			var key = parseJsonDate(s.Key).getTime();
	
			// Store the entry to the dict
			d.StatDict[key] = s;
			
			// Store the date
			if (!dates.hasOwnProperty(key))
				dates[key] = key;
		});
	
	});
	
	//dates.sort(function(a, b) {console.log(a);});
	var datesKeys = Object.keys(dates).sort(function(a, b) {return (Number(a) > Number(b)) ? 1: -1 });
	//console.log(datesKeys);
	
	//Loop again over the data and fill the gaps with empty values for non existing dates
	array.CashPackData.map(function(d) {
		$.each(datesKeys, function(index, dateKey) {
				var name = toUTCStringWOSecs(new Date(dates[dateKey]));
				if (d.StatDict.hasOwnProperty(dates[dateKey])) {
					dict[d.CashPackName + postFix].push(
						{ name: name, value: d.StatDict[dates[dateKey]].Value.TimesPurchased, values: d.StatDict[dates[dateKey]].Value }
					);
				}
				else {
					dict[d.CashPackName + postFix].push(
						{ name: name, value: 0, values : {"TimesPurchased": 0, "UniqueGamers": 0}}
					);
				}
		});
	});

	return dict;
}
