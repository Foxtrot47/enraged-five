//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true] // dates+builds
	     ],
};

var characters = [
	"Michael",
	"Franklin",
	"Trevor",
];

var profStat = new ProfileStats();
var profileStatList = [
{
	types: [
	   "SP0_SPECIAL_ABILITY_UNLOCKED",
	   "SP1_SPECIAL_ABILITY_UNLOCKED",
	   "SP2_SPECIAL_ABILITY_UNLOCKED",
     ],
     name: "Special",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	    "SP0_SPECIAL_ABILITY_MAXED",
		"SP1_SPECIAL_ABILITY_MAXED",
		"SP2_SPECIAL_ABILITY_MAXED",
     ],
     name: "Special maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_STAMINA",
	   "SP1_STAMINA",
	   "SP2_STAMINA",
     ],
     name: "Stamina",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_STAMINA_MAXED",
		"SP1_STAMINA_MAXED",
		"SP2_STAMINA_MAXED",
     ],
     name: "Stamina maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_SHOOTING_ABILITY",
	   "SP1_SHOOTING_ABILITY",
	   "SP2_SHOOTING_ABILITY",
     ],
     name: "Shooting",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_SHOOTING_ABILITY_MAXED",
		"SP1_SHOOTING_ABILITY_MAXED",
		"SP2_SHOOTING_ABILITY_MAXED",
     ],
     name: "Shooting maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_STRENGTH",
	   "SP1_STRENGTH",
	   "SP2_STRENGTH",
     ],
     name: "Strength",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_STRENGTH_MAXED",
		"SP1_STRENGTH_MAXED",
		"SP2_STRENGTH_MAXED",
     ],
     name: "Strength maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_STEALTH_ABILITY",
	   "SP1_STEALTH_ABILITY",
	   "SP2_STEALTH_ABILITY",
     ],
     name: "Stealth",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_STEALTH_ABILITY_MAXED",
		"SP1_STEALTH_ABILITY_MAXED",
		"SP2_STEALTH_ABILITY_MAXED",
     ],
     name: "Stealth max",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_FLYING_ABILITY",
	   "SP1_FLYING_ABILITY",
	   "SP2_FLYING_ABILITY",
     ],
     name: "Flying",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_FLYING_ABILITY_MAXED",
		"SP1_FLYING_ABILITY_MAXED",
		"SP2_FLYING_ABILITY_MAXED",
     ],
     name: "Flyin maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_WHEELIE_ABILITY",
	   "SP1_WHEELIE_ABILITY",
	   "SP2_WHEELIE_ABILITY",
     ],
     name: "Driving",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_WHEELIE_ABILITY_MAXED",
		"SP1_WHEELIE_ABILITY_MAXEDD",
		"SP2_WHEELIE_ABILITY_MAXED",
     ],
     name: "Driving maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	   "SP0_LUNG_CAPACITY",
	   "SP1_LUNG_CAPACITY",
	   "SP2_LUNG_CAPACITY",
     ],
     name: "Lung Capacity",
     requestSubString: "_",
     bucketSize: 100,
     singleMetric: true,
     unit: "time",
},
{
	types: [
	    "SP0_LUNG_CAPACITY_MAXED",
		"SP1_LUNG_CAPACITY_MAXED",
		"SP2_LUNG_CAPACITY_MAXED",
     ],
     name: "Lung Capacity maxed",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "time",
},

];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,
	
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	// Horizontal Graph  
	hasFriendlierNames: false,
	
	description: "",
	
	chartGroups: {
		getGroups: function(d) { return d; }, // Data are returned grouped
		getGroupName: function(d) { return d.GroupName; },
	    getGroupValues: function(d) { return d.GroupValues; },
   	},
	
	bars: [
	{
	    title: "Users reached Max",
	    colour: config.chartColour1,
	    getName: function(d) {
	    	return d.name;
	    },
	    getValue: function(d) { return d.value },
	    getObject: function(d) { return d },
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
					+ "<tr><td>" + key + ": </td><td class='right'>" 
						+ commasFixed2(Number(e.point.Value))
					+ " %</td></tr>"
					+ "<tr><td>Average Time Maxed</td><td class='right'>" 
						+ formatSecsWithDays(e.point.Object.time)
					+ "</td></tr>";
		html += "</table>"; 
			
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(Number(d));
	},
	units: "%",
	
	chartLeftMargin: 100,
};


function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
				
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	// For each skill result
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		// skip the time maxed results from the loop
		if (!availableChart.bucketSize)
			return;
			
		var values = [];
		// For each character
		$.each(data[i], function(j, typeResult) {
			var totalUsers = 0;
			typeResult.response.map(function(r) {totalUsers += Number(r.YValue); });
			
			// Get the time maxed from the next profile stat which will be skiped from the main loop
			var timeMaxed = ((data[i+1][j].response && Number(data[i+1][j].response[0].YValue)) 
					? Number(data[i+1][j].response[0].Total)/Number(data[i+1][j].response[0].YValue) 
					: 0);
			
			values.push({
				"name" : characters[j],
				"value" : ((typeResult.response.length>1 && totalUsers) ? (Number(typeResult.response[1].YValue)/totalUsers)*100 : 0),
				"time" : timeMaxed,
			});
		});
		
		array.push(
			{
			   	"GroupName": availableChart.name,
			   	"GroupValues": values,
			}
		);
	});
	
	return array;
}

function getStatNames(profileStat) {
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
