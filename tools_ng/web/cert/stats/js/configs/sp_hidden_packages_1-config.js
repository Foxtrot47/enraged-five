// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var currentStat = "Spaceship Parts";
var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "NUM_HIDDEN_PACKAGES_1",
     ],
     name: currentStat,
     requestSubString: "_",
     bucketSize: 5,
     bucketCount: null,
     singleMetric: true,
     fillWithEmptyBuckets: true,
},
];

var reportOptions = {
		
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	
	availableCharts: profileStatList,
	
	hasExtraRestParams: 
	[
	 	{
	 		key: "StatNames",
	        value: getStatNames,
	 	},
	 	{
	 		key: "BucketSize",
	 		value: profileStatList[0].bucketSize,
	 	},
	],	
	
	processFunction: function(d) { 
		return profStat.convertResultsToDict(
				[profStat.processBucketResults(d, profileStatList[0])],
				[reportOptions.yLabel]); 
	},
		
	enableCSVExport: "content-description",
	graphTitle: currentStat,
	
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.name; },
	name: function(d) { return d.Bucket; },
	value: function(d) { return Number(d.YValue); },
	
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Number of Players",
		
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
		
	hideLegend: true,
	legend: {height: 30, width: 180, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + " found" + "</div><br /><br />"
			+ "<table>"
					+ "<tr><td>Number of Players:</td><td class='right'>" + commas(this.value(d)) + "</td></tr>"
				+ "</table>";
			return content;
	},
			              
};

function getStatNames() {
		
	var statNames = [];
	$.each(reportOptions.availableCharts, function (i, profileStat) {
		statNames = statNames.concat(
			profStat.constructStatNames(profileStat, profileStat.types[0], config.gameTypes)
		);
	});
		
	return statNames.join(",");
}