// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	name: "Missions :: ", // Not used in clickable pie chart version - subheader removed
	restEndpoint: config.missionsStats,
	restEndpointAsync: config.missionsStatsAsync,
	
	processFunction: groupMissions,
	
	isClickable: true,
	
	/*
	legend: true,
	detailedLegend: false,
	
	pieLabelsOutside: true,
	detailedPieLabelsOutside: true,
	
	labelSunbeamLayout: false,
	detailedLabelSunbeamLayout: true,
	
	donut: true,
	detailedDonut: true,
	
	donutLabelsOutside: true,
	detailedDonutLabelsOutside: false,
	
	// function to get the name from the rest data
	getName: function(d) {return d.MissionType;},
	// function to calculate the value from the rest data
	getValue: function(d) {
		return d.TotalTime/config.anHourInSecs;
	},
	// function to check if an entry has available detailed data  
	hasDetailedData: function(d) {return ((d.Variants) ? true : false);},
	
	getDetailedName: function(d) {return (d.MissionName); },
	// function to create the value from the rest data
	getDetailedValue: function(d) {return d3.round(d.TotalTime/config.anHourInSecs, 2);},
	// function to get the value from the rest data
	getDetailedObjects: function(d) {return [{Items: d.Variants}];},
		
	unit: " hours",
	lrMargin: 40,
	*/
	
	// This is a piechart
	main: {
		legend: true,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		title: "Total Time",
		getName: function(d) {
			return (d.FriendlyName) ? d.FriendlyName : d.MissionType;
		},
		getValue: function(d) {
			return d.TotalTime/config.anHourInSecs;
		},
		
		unit: " hours",
		lrMargin: 40,
	},
	// This is the breakdown piechart
	breakdown: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: true,
		donut: true,
		donutLabelsOutside: false,
		
		sortByValueDesc: true,

		getPieLabel: function(d) {return d.label},
		getValuesArray: function(d) {return d.Variants; }, // this is the values array inside the returned object
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName 
						: d.MissionName;
		},
		getValue: function(d) {
			return d.TotalTime/config.anHourInSecs;
		},
		
		unit: " hours",
		lrMargin: 40,
		
		// Pass the breakdown object (need to include label/values/metadata)
		getObjects: function(d) { return [d]; },
	},
	
};

function groupMissions(ajaxList) {
	var groupDefs = {
	     "Main Missions" : "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|VOID|NET))",
	     "Ambient" : "^AMB",
	     "Cops n Crooks" : "^CnC",
	     "Mini Games" : "^MG",
	     "Odd Jobs" : "^Odd",
	     "Random Events": "^RE",
	     "Random Characters" : "^RC",
	     "Special Peds" : "^Special Ped",
	};
	
	var groupedMissions = {};
	$.each(ajaxList, function(i, ajaxMission) {
		
		$.each(groupDefs, function(name, regexp) {
						
			var re = new RegExp(regexp);
			if (re.test(ajaxMission.MissionName)) {
				
				if (!groupedMissions.hasOwnProperty(name)) {
					groupedMissions[name] = {
						MissionType: name,
						Variants: [],
						TotalTime: 0,
					};
				}
				
				groupedMissions[name].Variants.push(ajaxMission);
				groupedMissions[name].TotalTime += ajaxMission.TotalTime;
			
				return;
			}
			
		});
			
	});
		
	// Convert to Indexed array:
	groupedMissions = Object.keys(groupedMissions).map(function (key) {
	    return groupedMissions[key];
	});
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": groupedMissions,
	   	"metadata": {},
	};
}
