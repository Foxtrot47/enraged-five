var navigationTitle = "Statistics";
var navigationItems = [{
		title: "Report Presets",
		href: "index.html",
		hide: false,
		config: null,
		subItems: [],
	},

	// General Game
	{
		title: "General / Game",
		href: "#",
		hide: false,
		config: null,
		profile: true,
		subItems: [
		    {
		    	title: "Total Playing Time",
		    	href: "total_playing_time.html",
				config: "js/configs/total_playing_time-config.js",
				profile: true,
				subItems: [],
		    },
		    {
				title: "Total Players",
				href: "total_players.html",
				hide: false,
				config: "js/configs/total_players-config.js",
				profile: false,
				subItems: [],
			},
		    {
		    	title: "SP: Favourite Character",
		    	href: "favourite_character.html",
				config: "js/configs/favourite_character-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Percent Complete",
		    	href: "percent_complete.html",
				config: "js/configs/percent_complete-config.js",
				profile: true,
				subItems: [],
		    },
			{
				title: "SP: Choices",
				href: "sp_choices.html",
				hide: false,
				config: "js/configs/sp_choices-config.js",
				profile: true,
				subItems: [],
			},
			{
				title: "SP: Missions Skipped",
				href: "sp_skipped_missions.html",
				hide: false,
				config: "js/configs/sp_skipped_missions-config.js",
				profile: true,
				subItems: [],
			},
			{
				title: "SP: Sleep Mode",
				href: "sp_sleep_mode.html",
				hide: false,
				config: "js/configs/sp_sleep_mode-config.js",
				profile: true,
				subItems: [],
			},
			{
				title: "SP: Chop",
				href: "sp_chop.html",
				hide: false,
				config: "js/configs/sp_chop-config.js",
				profile: true,
				subItems: [],
			},
			{
				title: "SP Vs MP Playing Time",
				href: "single_vs_multi.html",
				hide: false,
				config: "js/configs/single_vs_multi-config.js",
				profile: true,
				subItems: [],
			},
		],
	},
	
	// Cash and XP
	{
		title: "Cash and RP",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		     {
		       	   title: "Per User Hourly Report",
		       	   href: "cash_user_hourly_report.html",
		       	   hide: false,
		       	   //config: "js/configs/cash__user_hourly_report-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     {
		       	   title: "Top User Hourly Report",
		       	   href: "cash_top_user_hourly_report.html",
		       	   hide: false,
		       	   //config: "js/configs/cash_top_user_hourly_report-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     // Page renaming - b*#1641208
		     {
		       	   title: "Average Cash Earned Per Hour", 
		       	   href: "cash_cash_hourly_report.html",
		       	   hide: false,
		       	   config: "js/configs/cash_cash_hourly_report-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     {
		       	   title: "Average RP Earned per Hour",
		       	   href: "cash_rp_hourly_report.html",
		       	   hide: false,
		       	   config: "js/configs/cash_rp_hourly_report-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     {
		       	   title: "Average Cash Earnings, Balance, RP & Rank",
		       	   href: "cash_total_cash_rp.html",
		       	   hide: false,
		       	   config: "js/configs/cash_total_cash_rp-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     {
		       	   title: "Average Cash Earned and Time Passed per Rank",
		       	   href: "cash_total_cash_hour.html",
		       	   hide: false,
		       	   config: "js/configs/cash_total_cash_hour-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		     {
		       	   title: "Transaction History",
		       	   href: "transaction_history.html",
		       	   hide: false,
		       	   config: "js/configs/transaction_history-config.js",
		       	   profile: false,
		       	   subItems: [],
		     },
		],
	},
	
	// Expenditure
	{
		title: "Player Expenditure",
		href: "#",
		hide: false,
		config: "",
		profile: false,
		subItems: [
		   {
				title: "SP: Cash at Flow Stage",
				href: "sp_cash_at_flow_stage.html",
				hide: false,
				config: "js/configs/sp_cash_at_flow_stage-config.js",
				profile: false,
				subItems: [],
			},
			{
				title: "SP: Properties",
				href: "sp_properties.html",
				hide: false,
				config: "js/configs/sp_properties-config.js",
				profile: true,
				subItems: [],
			},
			{
				title: "SP: Taxis",
				href: "#",
				hide: false,
				config: null,
				profile: false,
				subItems: [
				    {
				    	title: "Times Used",
						href: "sp_taxis_times_used.html",
						hide: false,
						config: "js/configs/sp_taxis_times_used-config.js",
						profile: true,
						subItems: [],
				    },
				    {
				    	title: "Amount Spent",
						href: "sp_taxis_amount_spent.html",
						hide: false,
						config: "js/configs/sp_taxis_amount_spent-config.js",
						profile: true,
						subItems: [],
				    },
				],
			},
			{
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " All Expenditure",
		    	href: "purchases_all_expenditure.html",
		    	hide: false,
		    	config: "js/configs/purchases_all_expenditure-config.js",
		    	profile: false,
		    	subItems: [],
		    },
			{
		    	title: "All Shop Purchases",
		    	href: "purchases_all_shop.html",
		    	hide: false,
		    	config: "js/configs/purchases_all_shop-config.js",
		    	profile: false,
		    	subItems: [],
		    },
		    {
		    	title: "Purchases Per Shop",
		    	href: "purchases_per_shop.html",
		    	hide: false,
		    	config: "js/configs/purchases_per_shop-config.js",
		    	profile: false,
		    	subItems: [],
		    },
		    {
		       	title: "Money Purchases",
		       	href: "purchases_money.html",
		       	hide: false,
		       	config: "js/configs/purchases_money-config.js",
		       	profile: false,
		       	subItems: [],
		     },
		],
	},
	
	// Expenditure
	{
		title: "Player Earnings",
		href: "#",
		hide: false,
		config: "",
		profile: false,
		subItems: [
		   {
				title: "Earning Stats",
				href: "earnings.html",
				hide: false,
				config: "js/configs/earnings-config.js",
				profile: false,
				subItems: [],
			},
		]
	},
	
	// SP:Character
	{
		title: "SP: Character",
		href: "#",
		hide: false,
		config: "",
		profile: false,
		subItems: [
		    {
		    	title: "Appearance",
		    	href: "sp_appearance.html",
		    	hide: false,
		    	config: "js/configs/sp_appearance-config.js",
		    	profile: true,
		    	subItems: [],
		    },
		    {
		    	title: "Switches: On-mission",
		    	href: "sp_switches_on_mission.html",
		    	hide: false,
		    	config: "js/configs/sp_switches_on_mission-config.js",
		    	profile: true,
		    	subItems: [],
		    },
		    {
		    	title: "Switches: Off-mission",
		    	href: "sp_switches_off_mission.html",
		    	hide: false,
		    	config: "js/configs/sp_switches_off_mission-config.js",
		    	profile: true,
		    	subItems: [],
		    },
		    {
		       	title: "Special Ability",
		       	href: "sp_special_ability.html",
		       	hide: false,
		       	config: "js/configs/sp_special_ability-config.js",
		       	profile: false,
		       	subItems: [],
		     },
		],
	},
	
	{
		title: "SP: Character Stats",
		href: "#",
		hide: false,
		config: "",
		subItems: [
		   {
			   title: "Michael",
			   href: "sp_char_stats_michael.html",
			   hide: false,
			   config: "js/configs/sp_char_stats_michael-config.js",
			   profile: false,
			   subItems: [],
		   },
		   {
				title: "Trevor",
				href: "sp_char_stats_trevor.html",
				hide: false,
				config: "js/configs/sp_char_stats_trevor-config.js",
				profile: false,
				subItems: [],
			},
			{
				title: "Franklin",
				href: "sp_char_stats_franklin.html",
				hide: false,
				config: "js/configs/sp_char_stats_franklin-config.js",
				profile: false,
				subItems: [],
			},
			{
				title: "Overall",
				href: "sp_char_stats_overall.html",
				hide: false,
				config: "js/configs/sp_char_stats_overall-config.js",
				profile: false,
				subItems: [],
			},
        ],
	},
	
	// Betting
	{
		title: "Betting",
		href: "#",
		hide: false,
		config: "",
		profile: false,
		subItems: [
		    {
		    	title: "Amount Bet",
		       	href: "bets_amount.html",
		       	hide: false,
		       	config: "js/configs/bets_amount-config.js",
		       	profile: false,
		       	subItems: [],
		    },
		    /*
		    {
		    	title: "Amount Bet (New)",
		       	href: "bets_amount_new.html",
		       	hide: false,
		       	config: "js/configs/bets_amount_new-config.js",
		       	profile: false,
		       	subItems: [],
		    },
		    */
		    {
		    	title: "Average Bets",
		       	href: "bets_average.html",
		       	hide: false,
		       	config: "js/configs/bets_average-config.js",
		       	profile: false,
		       	subItems: [],
		     },
		],
	},
	
	// Missions / Activities
	{
		title: "Missions / Activities",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		    // Freemode
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Tutorial",
		    	href: "freemode_tutorial.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_tutorial-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Game Modes",
		    	href: "freemode_missions.html",
		    	config: "js/configs/freemode_missions-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Invites",
		    	href: "freemode_invites.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_invites-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Races Breakdown",
		    	href: "freemode_races.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_races-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Mission Attempts",
		    	href: "freemode_attempts_graph.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_attempts_graph-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") 
		    			+ " Mission Attempts To First Success",
		    	href: "freemode_attempts_to_first_success.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_attempts_to_first_success-config.js",
		    	tableau: true,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Activities",
		    	href: "freemode_activities.html",
		    	display: "inherit",
		    	config: "js/configs/freemode_activities-config.js",
		    	profile: false,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Content",
		    	href: "#",
		    	display: "inherit",
		    	config: null,
		    	profile: false,
		    	subItems: [
		    	    {
		    	    	title: "Graphs",
		    	    	href: "freemode_content_graphs.html",
				    	config: "js/configs/freemode_content_graphs-config.js",
				    	profile: false,
		    	    },
		    	    {
		    	    	title: "Table",
		    	    	href: "freemode_content_table.html",
				    	config: "js/configs/freemode_content_table-config.js",
				    	profile: false,
		    	    },
		    	    {
		    	    	title: "Rating",
		    	    	href: "freemode_content_rating.html",
				    	config: "js/configs/freemode_content_rating-config.js",
				    	tableau: true,
		    	    },
		    	],
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Job Points Earned", 
				href: "freemode_job_points.html",
				hide: false,
				config: "js/configs/freemode_job_points-config.js",
				tableau: true,
				subItems: [],
			},
			{
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Jobs By Type", 
				href: "freemode_jobs_by_type.html",
				hide: false,
				config: "js/configs/freemode_jobs_by_type-config.js",
				tableau: true,
				subItems: [],
			},
			/*
			{
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Jobs Detail", 
				href: "freemode_jobs_detail.html",
				hide: false,
				config: "js/configs/freemode_jobs_detail-config.js",
				tableau: true,
				subItems: [],
			},
			*/
		    
		    // SP
		    {
		    	title: "SP: Missions",
		    	href: "missions.html",
		    	config: "js/configs/missions-config.js",
		    	profile: false,
		    },
		    // HEISTS
			{
				title: "SP: Heist Approaches",
				href: "sp_heist_approaches.html",
				config: "js/configs/sp_heist_approaches-config.js",
				profile: true,
			},
			{
		    	title: "SP: Oddjobs",
		    	href: "sp_oddjobs.html",
		    	display: "inherit",
		    	config: "js/configs/sp_oddjobs-config.js",
		    	profile: false,
		    },
		    {
		    	title: "SP: Epsilon",
		    	href: "sp_epsilon.html",
		    	display: "inherit",
		    	config: "js/configs/sp_epsilon-config.js",
		    	profile: false,
		    },
		    {
		    	title: "SP: Mission Attempts Graphs",
		    	href: "#",
		    	display: "inherit",
		    	profile: false,
		    	subItems: [
		    	{
		    		title: "Main Missions",
		    		href: "missions_attempts_graphs_main.html",
		    		display: "inherit",
		    		config: "js/configs/missions_attempts_graphs_main-config.js",
		    	},
		    	{
		    		title: "Oddjobs",
		    		href: "missions_attempts_graphs_oj.html",
		    		display: "inherit",
		    		config: "js/configs/missions_attempts_graphs_oj-config.js",
		    	},
		    	{
		    		title: "Minigames",
		    		href: "missions_attempts_graphs_mg.html",
		    		display: "inherit",
		    		config: "js/configs/missions_attempts_graphs_mg-config.js",
		    	},
		    	{
		    		title: "Random Events",
		    		href: "missions_attempts_graphs_re.html",
		    		display: "inherit",
		    		config: "js/configs/missions_attempts_graphs_re-config.js",
		    	},
		    	{
		    		title: "Random Characters",
		    		href: "missions_attempts_graphs_rc.html",
		    		display: "inherit",
		    		config: "js/configs/missions_attempts_graphs_rc-config.js",
		    	},
		    	],
		    },
			{
		    	title: "SP: Mission Attempts Table",
		    	href: "missions_attempts_table.html",
		    	display: "inherit",
		    	config: "js/configs/missions_attempts_table-config.js",
		    	profile: false,
		    },
		    {
		    	title: "SP: Mission Replays",
		    	href: "sp_mission_replays.html",
		    	display: "inherit",
		    	config: "js/configs/sp_mission_replays-config.js",
		    	profile: false,
		    },
		    /*
		    {
		    	title: "SP: Performance Table",
		    	href: "missions_cs.html",
		    	display: "inherit",
		    	config: "js/configs/missions_cs-config.js",
		    	profile: false,
		    	capture: true,
		    },
		    {
		    	title: "SP: Performance Graph",
		    	href: "missions_cs_graph.html",
		    	display: "inherit",
		    	config: "js/configs/missions_cs-config.js",
		    	profile: false,
		    	capture: true,
		    },
		    */
		],
	},
		
	// Vehicles
	{
		title: "Vehicles",
		//href: "vehicles.html",
		href: "#",
		hide: false,
		//config: "js/configs/vehicles-config.js",
		profile: false,
		subItems: [
		    {
		    	title: "Stats Per Category",
		    	href: "vehicles.html",
		    	hide: false,
		    	config: "js/configs/vehicles-config.js",
		    	profile: false,
		    	subItems: [],
		    },
		],
	},
	{
		title: "Combat / Weapons",
		href: "#",
		hide: false,
		config: "js/configs/weapons_heldtime-config.js",
		profile: true,
		subItems: [
		    {
		    	title: "Total Held Time",
		    	href: "weapons_heldtime.html?stat=0",
		    	subItems: [],
			},
			{
		    	title: "Average Held Time",
		    	href: "weapons_heldtime.html?stat=1",
		    	subItems: [],
			},
			{
		    	title: "SP: Favourite Weapons",
		    	href: "sp_favourite_weapons.html",
				config: "js/configs/sp_favourite_weapons-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Modifications",
		    	href: "sp_modifications.html",
				config: "js/configs/sp_modifications-config.js",
				profile: false,
				subItems: [],
		    },
			{
		    	title: "SP: Stealth",
		    	href: "sp_stealth.html",
				config: "js/configs/sp_stealth-config.js",
				profile: true,
				subItems: [],
		    },
			{
		    	title: "Total Kills",
		    	href: "weapons_total_kills.html",
				config: "js/configs/weapons_total_kills-config.js",
				profile: false,
				subItems: [],
		    },
		    {
		    	title: "Average Kills",
		    	href: "weapons_avg_kills.html",
				config: "js/configs/weapons_avg_kills-config.js",
				profile: false,
				subItems: [],
		    },
		    {
		    	title: "Total Deaths",
		    	href: "weapons_total_deaths.html",
				config: "js/configs/weapons_total_deaths-config.js",
				profile: false,
				subItems: [],
		    },
		    {
		    	title: "Average Deaths",
		    	href: "weapons_avg_deaths.html",
				config: "js/configs/weapons_avg_deaths-config.js",
				profile: false,
				subItems: [],
		    },
		],
	},
	
	// Arrests & Deaths
	{
		title: "Arrests & Deaths",
		//href: "arrests_n_deaths.html",
		href: "#",
		hide: false,
		config: "js/configs/arrests_n_deaths-config.js",
		profile: true,
		subItems: [
		    {
			   	title: "Death Types",
			   	href: "arrests_n_deaths.html?stat=0",
			   	subItems: [],
			},
			{
			   	title: "Deaths per Hour",
			   	href: "arrests_n_deaths.html?stat=1",
			   	subItems: [],
			},
			{
		    	title: "SP: Mission Deaths",
		    	href: "sp_mission_deaths.html",
		    	display: "inherit",
		    	config: "js/configs/sp_mission_deaths-config.js",
		    	profile: false,
		    },
			{
			   	title: "Player Deaths",
			   	href: "arrests_n_deaths.html?stat=2",
			   	subItems: [],
			},
			{
			   	title: "Arrests per Hour",
			   	href: "arrests_n_deaths.html?stat=3",
			   	subItems: [],
			},
			{
			   	title: "Player Arrests",
			   	href: "arrests_n_deaths.html?stat=4",
			   	subItems: [],
			},
			{
		    	title: "SP: Wanted",
		    	href: "#",
		    	display: "inherit",
		    	config: null,
		    	profile: false,
		    	subItems: [
		    	    {
		    		   	title: "Times Wanted",
		    		   	href: "sp_wanted_times.html",
		    		   	hide: false,
		    		   	config: "js/configs/sp_wanted_times-config.js",
		    		   	profile: true,
		    		  	subItems: [],
		    	    },
		    	    {
		    		   	title: "Time Spent Wanted",
		    		   	href: "sp_wanted_time_spent.html",
		    		   	hide: false,
		    		   	config: "js/configs/sp_wanted_time_spent-config.js",
		    		   	profile: true,
		    		  	subItems: [],
		    	    },
		    	    {
		    		   	title: "5 Stars Level",
		    		   	href: "sp_wanted_five_stars.html",
		    		   	hide: false,
		    		   	config: "js/configs/sp_wanted_five_stars-config.js",
		    		   	profile: true,
		    		  	subItems: [],
		    	    },
		    	    {
		    		   	title: "Wanted Level Per Hour",
		    		   	href: "sp_wanted_level_per_hour.html",
		    		   	hide: false,
		    		   	config: "js/configs/sp_wanted_level_per_hour-config.js",
		    		   	tableau: true,
		    		  	subItems: [],
		    	    },
		    	],
		    },
		],
	},
	
	// Media
	{
		title: "Media",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		{
			title: "Radio Stations",
			href: "radio_stations.html",
			//href: "#",
			hide: false,
			config: "js/configs/radio_stations-config.js",
			profile: false,
			subItems: [],
			/*
			subItems: [
			    {
			    	title: "Total Users",
			    	href: "radio_stations.html?stat=0",
			    	subItems: [],
			    },
			    {
			    	title: "Total Times Tuned",
			    	href: "radio_stations.html?stat=1",
			    	subItems: [],
			    },
			    {
			    	title: "Total Listening Time",
			    	href: "radio_stations.html?stat=2",
			    	subItems: [],
			    },
			],
			*/
		},
		{
			title: "TV Shows",
			href: "tv_shows.html",
			//href: "#",
			hide: false,
			config: "js/configs/tv_shows-config.js",
			profile: false,
			subItems: [],
			/*
			subItems: [
			    {
				  	title: "Total Users",
				   	href: "tv_shows.html?stat=0",
				   	subItems: [],
				},
				{
				   	title: "Total Times Tuned",
				   	href: "tv_shows.html?stat=1",
				   	subItems: [],
				},
				{
				  	title: "Total Watching Time",
				   	href: "tv_shows.html?stat=2",
				   	subItems: [],
				},
			],
			*/
		},
		{
			title: "Cinema",
			href: "cinema.html",
			//href: "#",
			hide: false,
			config: "js/configs/cinema-config.js",
			profile: false,
			subItems: [],
			/*
			subItems: [
			    {
				   	title: "Total Users",
				   	href: "cinema.html?stat=0",
				   	subItems: [],
				},
				{
				  	title: "Total Times Tuned",
				  	href: "cinema.html?stat=1",
				   	subItems: [],
				},
				{
				  	title: "Total Watching Time",
				  	href: "cinema.html?stat=2",
				   	subItems: [],
				},
			],
			*/
		},
		{
			title: "Website Visits",
			href: "website_visits.html",
			hide: false,
			config: "",
			profile: false,
			subItems: [],
		},
		],
	},
	{
		title: "SP: Pickups / Packages",
		//href: "hidden_packages.html",
		href: "#",
		hide: false,
		//config: "js/configs/hidden_packages-config.js",
		config: null,
		profile: true,
		subItems: [
		    {
		    	title: "SP: Letter Scraps",
		    	href: "sp_hidden_packages_0.html",
				config: "js/configs/sp_hidden_packages_0-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Spaceship Parts",
		    	href: "sp_hidden_packages_1.html",
				config: "js/configs/sp_hidden_packages_1-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Epsilon Tracts",
		    	href: "sp_hidden_packages_2.html",
				config: "js/configs/sp_hidden_packages_2-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Nuclear Waste",
		    	href: "sp_hidden_packages_3.html",
				config: "js/configs/sp_hidden_packages_3-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Submarine Pieces",
		    	href: "sp_hidden_packages_4.html",
				config: "js/configs/sp_hidden_packages_4-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Total Packages",
		    	href: "sp_hidden_packages_5.html",
				config: "js/configs/sp_hidden_packages_5-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Weapon Pickups",
		    	href: "sp_weapon_pickups.html",
				config: "js/configs/sp_weapon_pickups-config.js",
				profile: true,
				subItems: [],
		    },
		    {
		    	title: "SP: Health Pickups",
		    	href: "sp_health_pickups.html",
				config: "js/configs/sp_health_pickups-config.js",
				profile: true,
				subItems: [],
		    },
		],
	},
	
	// Apps
	{
		title: "Apps",
		href: "#",
		hide: false,
		config: null,
		profile: true,
		subItems: [
		    {
		    	title: "Car App",
		    	href: "app_car.html",
		    	config: "js/configs/app_car-config.js",
		    	profile: true,
		    },
		    {
		    	title: "Chop App",
		    	href: "app_chop.html",
		    	config: "js/configs/app_chop-config.js",
		    	profile: true,
		    },
		],
	},
	
	// Cheats
	{
		title: "Cheats",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		    {
		    	title: "Cheats Used",
		    	href: "cheats_used.html",
		    	config: "js/configs/cheats_used-config.js",
		    	profile: false,
		    },
		],
	},
	
	// Sessions
	{
		title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Sessions",
		href: "#",
		hide: false,
		subItems: [
		    {
		    	title: "Sessions With Username",
		    	href: "sessions_with_username.html",
		    	hide: false,
		    	config: "js/configs/sessions_with_username-config.js",
		    	tableau: true,
		    	subItems: [],
		    },
		],
	},
	
	// Map Telemetry
	{
		title: "Map Telemetry",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		    {
		    	title: "Deathmatch",
		    	href: "map_deathmatch.html",
		    	config: null,
		    },
		    {
		    	title: "FPS",
		    	href: "map_fps.html",
		    	config: null,
		    },
		],
	},
	
	// Weather
	{
		title: "Weather",
		href: "#",
		hide: false,
		subItems: [
		    {
				title: "SP: Weather Pie Charts",
				href: "weather_pie_charts_sp.html",
				hide: false,
				config: "js/configs/weather_pie_charts_sp-config.js",
				tableau: true,
				subItems: [],
			},
		    {
		    	title: "SP: Weather Over Time",
		    	href: "weather_over_time_sp.html",
		    	hide: false,
		    	config: "js/configs/weather_over_time_sp-config.js",
		    	tableau: true,
		    	subItems: [],
		    },
		    {
				title: "MP: Weather Pie Charts",
				href: "weather_pie_charts_mp.html",
				hide: false,
				config: "js/configs/weather_pie_charts_mp-config.js",
				tableau: true,
				subItems: [],
			},
		    {
		    	title: "MP: Weather Over Time",
		    	href: "weather_over_time_mp.html",
		    	hide: false,
		    	config: "js/configs/weather_over_time_mp-config.js",
		    	tableau: true,
		    	subItems: [],
		    },
		],
	},
	
	// Cutscenes
	{
		title: "Cutscenes",
		href: "#",
		hide: false,
		config: null,
		profile: false,
		subItems: [
		    {
		    	title: "Stats",
				href: "cutscenes.html",
				config: "js/configs/cutscenes-config.js",
				profile: false,
			},
			{
		    	title: "Stats Per Mission",
				href: "cutscenes_per_mission.html",
				config: "js/configs/cutscenes_per_mission-config.js",
				profile: false,
			},
			{
		    	title: "Watched Stats",
				href: "cutscenes_watched.html",
				config: "js/configs/cutscenes_watched-config.js",
				profile: false,
			},
			/*
			{
		    	title: "Sizes",
				href: "cutscenes_sizes.html",
				config: "js/configs/cutscenes_sizes-config.js",
				profile: false,
			},
			{
				title: "Performance Table",
				href: "cutscenes_cs.html",
				config: "js/configs/cutscenes_cs-config.js",
				profile: false,
				capture: true,
			},
			{
		    	title: "Lights Table",
				href: "cutscenes_lights.html",
				config: "js/configs/cutscenes_lights-config.js",
				profile: false,
			},
			*/
		],
	},
	{
		title: "Front End Settings",
		href: "#",
		hide: false,
		config: null,
		profile: true,
		subItems: [
		   {
			   title: "Controls",
			   //href: "frontend_controls.html",
			   href: "#",
			   config: "js/configs/frontend_controls-config.js",
			   profile: true,
			   subItems: [
			       {
			    	   title: "Control Type",
			    	   href: "frontend_controls.html?stat=0",
			    	   subItems: [],
			  		},
			  		{
			  			title: "Targeting Mode",
			  			href: "frontend_controls.html?stat=1",
			  			subItems: [],
			  		},
			  		{
			  			title: "Vibration",
			  			href: "frontend_controls.html?stat=2",
			  			subItems: [],
			  		},
			  		{
	  			   		title: "Invert Look",
	  			   		href: "frontend_controls.html?stat=3",
			  			subItems: [],
			  		},
			  		{
			  		   	title: "Aim Sensitivity",
			  		   	href: "frontend_controls.html?stat=4",
			  		   	subItems: [],
			  		},
			  	],
		   },
		   {
			   title: "Display",
			   //href: "frontend_display.html",
			   href: "#",
			   config: "js/configs/frontend_display-config.js",
			   profile: true,
			   subItems: [
				    {
					   title: "Radar",
					   href: "frontend_display.html?stat=0",
					   subItems: [],
					},
					{
						title: "HUD",
						href: "frontend_display.html?stat=1",
						subItems: [],
					},
					{
						title: "Weapon Target",
						href: "frontend_display.html?stat=2",
						subItems: [],
					},
					{
			  			title: "GPS ROUTE",
			  			href: "frontend_display.html?stat=3",
						subItems: [],
					},
					{
					   	title: "Vehicle Camera Height",
					   	href: "frontend_display.html?stat=4",
					   	subItems: [],
					},
					{
					   	title: "Brightness",
					   	href: "frontend_display.html?stat=5",
					   	subItems: [],
					},
					{
					   	title: "Safezone Size",
					   	href: "frontend_display.html?stat=6",
					   	subItems: [],
					},
					{
					   	title: "Subtitles",
					   	href: "frontend_display.html?stat=7",
					   	subItems: [],
					},
					{
					   	title: "Language",
					   	href: "frontend_display.html?stat=8",
					   	subItems: [],
					},
				],
		   },
		   {
			   title: "Audio",
			   //href: "frontend_audio.html",
			   href: "#",
			   config: "js/configs/frontend_audio-config.js",
			   profile: true,
			   subItems: [
				    {
					   title: "SFX Volume",
					   href: "frontend_audio.html?stat=0",
					   subItems: [],
					},
					{
						title: "Music Volume",
						href: "frontend_audio.html?stat=1",
						subItems: [],
					},
					{
						title: "Dialog Boost",
						href: "frontend_audio.html?stat=2",
						subItems: [],
					},
					{
						title: "Score",
						href: "frontend_audio.html?stat=3",
						subItems: [],
					},
					{
					   	title: "Output",
					   	href: "frontend_audio.html?stat=4",
					   	subItems: [],
					},
					{
					   	title: "Voice Chat",
					   	href: "frontend_audio.html?stat=5",
					   	subItems: [],
					},
					{
					   	title: "Voice Through Speakers",
					   	href: "frontend_audio.html?stat=6",
					   	subItems: [],
					},
				],
		   },
		   {
			   title: "Notifications",
			   //href: "frontend_notifications.html",
			   href: "#",
			   config: "js/configs/frontend_notifications-config.js",
			   profile: true,
			   subItems: [
			        {
					   title: "Phone Alerts",
					   href: "frontend_notifications.html?stat=0",
					   subItems: [],
					},
					{
						title: "Stats Alerts",
						href: "frontend_notifications.html?stat=1",
						subItems: [],
					},
					{
						title: "Crew Updates",
						href: "frontend_notifications.html?stat=2",
						subItems: [],
					},
					{
						title: "Friend Updates",
						href: "frontend_notifications.html?stat=3",
						subItems: [],
					},
					{
					   	title: "Social Club",
					   	href: "frontend_notifications.html?stat=4",
					   	subItems: [],
					},
					{
					   	title: "Store",
					   	href: "frontend_notifications.html?stat=5",
					   	subItems: [],
					},
				],
		   },
		   {
			   title: "Motion Sensor",
			   //href: "frontend_motion_sensor.html",
			   href: "#",
			   config: "js/configs/frontend_motion_sensor-config.js",
			   profile: true,
			   subItems: [
					{
					   title: "Helicopter",
					   href: "frontend_motion_sensor.html?stat=0",
					   subItems: [],
					},
					{
						title: "Bike",
						href: "frontend_motion_sensor.html?stat=1",
						subItems: [],
					},
					{
						title: "Boat",
						href: "frontend_motion_sensor.html?stat=2",
						subItems: [],
					},
					{
						title: "Reload",
						href: "frontend_motion_sensor.html?stat=3",
						subItems: [],
					},
				],
		   },
	   ],
	},
];
