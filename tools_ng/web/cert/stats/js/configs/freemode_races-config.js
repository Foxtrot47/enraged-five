// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var freemodeCategoriesDict = getFreemodeCategoriesDict();
var rosPlatformsDic = getRosPlatformsDict();

var reportOptions = {
	restEndpoint: config.freemodeRacesStats,
	restEndpointAsync: config.freemodeRacesStatsAsync,
	
	hasFilterInput: true,
	
	enableCSVExport: "content-description",
	graphTitle: "Races",
		
	chartGroups: {
		getGroups: function(d) {return [d]}, // One group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},

	bars: [
	{
		title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Races breakdown (times played)",
		colour: config.chartColour1,
	    getName: function(d) {return d.Name + " (" + d.UGCIdentifier + ")"; },
	    getValue: function(d) {return d.TimesPlayed; },
	    getObject: function(d) {return d; },
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		// Sort the winning vehicles desc and get the top value
		var winningVehicle = e.point.Object.WinnerInfo.sort(
					function(a, b) { return (a.TimesWon > b.TimesWon) ? -1 : 1; }
					)[0];
					
		// Sort the selected vehicles desc and get the top value
		var usedVehicle = e.point.Object.SelectionInfo.sort(
				function(a, b) { return (a.TimesUsed > b.TimesUsed) ? -1 : 1; }
				)[0];
		
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
				+ "<tr><td>Times Played: </td><td class='right'>" + e.point.Object.TimesPlayed + "</td></tr>";
		
		if (winningVehicle)
			html += "<tr><td>Most Winning Vehicle: </td><td class='right'> " 
				+ capitaliseString(winningVehicle.VehicleName) + " (" + winningVehicle.TimesWon + " times)</td></tr>";
		if (usedVehicle) 
			html += "<tr><td>Most Selected Vehicle: </td><td class='right'>" 
				+ capitaliseString(usedVehicle.VehicleName) + " (" + usedVehicle.TimesUsed + " times)</td></tr>";
				
		if (typeof e.point.Object.AverageRating !== "undefined")
			html += "<tr><td>Average Rating:</td><td>" + commasFixed2(e.point.Object.AverageRating) + "</td></tr>";
		if (typeof e.point.Object.UGCIdentifier !== "undefined")
			html += "<tr><td>UGC ID:</td><td>" + e.point.Object.UGCIdentifier + "</td></tr>";
		if (typeof e.point.Object.CreatedDate !== "undefined")
			html += "<tr><td>Created At:</td><td>" + parseJsonDate(e.point.Object.CreatedDate).toUTCString() + "</td></tr>";
		
		var creatorPlatform = (e.point.Object.CreatorPlatform) ? (" (" + rosPlatformsDic[e.point.Object.CreatorPlatform] + ")") : "";
		if (typeof e.point.Object.Creator !== "undefined")
			html += "<tr><td>Creator:</td><td>" + e.point.Object.Creator + creatorPlatform + "</td></tr>";
		
		if (typeof e.point.Object.Category !== "undefined")
			html += "<tr><td>Category:</td><td>" + freemodeCategoriesDict[e.point.Object.Category] + "</td></tr>";	
		
		if (typeof e.point.Object.IsPublished !== "undefined")
			html += "<tr><td>UGC Status:</td><td>" 
				+ ((e.point.Object.IsPublished) ? "Published" : "Saved") 
				+ "</td></tr>";
		
		html += "</table>"; 
		
		return html;
	},

	units: " Times Played",
	
	chartLeftMargin: 350,
};
