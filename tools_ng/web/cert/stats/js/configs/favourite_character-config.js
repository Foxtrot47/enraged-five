var profileStatList = [
/*
{
	types: [
	"TOTAL_PLAYING_TIME",
	],
	requestSubString: "_",
	name: "Total Playing Time",
	description: "No of users per bucket of 10 hours",
	label: "No of Users",
	units: "users",
	bucketSize: 10 * config.anHourInMillisecs, // Bucket of 10 hours in milliseconds
	convertToHours: true,	
},
*/
{
	types: [
	"SP0_TOTAL_PLAYING_TIME",
	"SP1_TOTAL_PLAYING_TIME",
	"SP2_TOTAL_PLAYING_TIME",
	],
	altNames: [
	"Michael",
	"Franklin",
	"Trevor",
	],
	//name: "Time Per Character",
	name: "Favourite Character",
	description: "Total playing time in hours for each singleplayer game character",
	label: "Hours",
	units: "hours",
	bucketSize: null,
	
	singleMetric: true, // use the metric as it is, do not add sp0-1-2 or mp...
	convertToHours: true
},
/*
{
	types: [
	"TOTAL_PROGRESS_MADE",
	],
	requestSubString: "_",
	name: "Percent Complete",
	label: "No of Users",
	units: "users",
	description: "No of users per bucket of 10% progress",
	bucketSize: 10,
	
	singleMetric: true, // one metric only
	addPercentageLabel: true,
	singleOnly: true,
},
*/
];

var reportOptions = {
		
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
		
	availableCharts: profileStatList,
}

//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};