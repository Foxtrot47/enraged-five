// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
{
	types: [
	"_PROFILE_SETTING_204", // DISPLAY_RADAR_MODE = 204,
	],
	altNames:[
	"Off", // "MO_OFF",
	"On", // "MO_ON",
	"Blips", // "MO_BLI",
	],
	requestSubString: "_",
	name: "Radar",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_205", // DISPLAY_HUD_MODE = 205,
	],
	altNames:[
	"Off", // "MO_OFF",
	"On", // "MO_ON",
	],
	requestSubString: "_",
	name: "HUD",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_412", // DISPLAY_RETICULE = 412,
	],
	altNames:[
	   "Basic", // "MO_RETICULE_OF",
	   "Complex", // "MO_RETICULE_ON",
	],
	requestSubString: "_",
	name: "Weapon Target",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_207", // DISPLAY_GPS = 207,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "GPS ROUTE",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_220", // DISPLAY_CAMERA_HEIGHT = 220,
	],
	altNames:[
	   "Low", // "MO_LOW",
	   "High", // "MO_HIGH",
	],
	requestSubString: "_",
	name: "Vehicle Camera Height",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_213", // DISPLAY_GAMMA = 213,
	],

	requestSubString: "_",
	name: "Brightness",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: parseInt(100/31),
	showOnlyLowerRange: true,
	validRange: [0, 30],
	orderDescBarchart: true,
},
{
	types: [
	"_PROFILE_SETTING_212", // DISPLAY_SAFEZONE_SIZE = 212,
	],

	requestSubString: "_",
	name: "Safezone Size",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: 10,
	showOnlyLowerRange: true,
	validRange: [0, 10], // 14
	//validRange: [-2, 11], // 14
	orderDescBarchart: true,
},
{
	types: [
	"_PROFILE_SETTING_203", // DISPLAY_SUBTITLES = 203,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Subtitles",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_206", // DISPLAY_LANGUAGE = 206,
	],
	altNames:[
	   "English", //"LANG_E",
	   "French", //"LANG_F",
	   "German", //"LANG_G",
	   "Italian", //"LANG_I",
	   "Spanish", //"LANG_S",
	   "Portuguese", //"LANG_PT",
	   "Polish", //"LANG_PL",
	   "Russian", //"LANG_R",
	   "Korean", //"LANG_K",
	   "Chinese", //"LANG_CH",
	   "Japanese", //"LANG_J",
	   "Mexican Spanish", //"LANG_MEX",
	],
	requestSubString: "_",
	name: "Language",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
];


var reportOptions = {	
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
			
	availableCharts: profileStatList.map(function(d) {
		d["hideStartDate"] = true;
		return d;
	}),
}