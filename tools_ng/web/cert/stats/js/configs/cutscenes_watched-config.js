// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};


var currentStat = "Watched Stats";
var items = [
   {
   		metric: "Times Watched To Completion" ,
  		val: function(d) {return (d.Started) ? (round2(d.Ended/d.Started) * 100) : 0;},
   },
   {
  		metric: "Players Watched To Completion" ,
 		val: function(d) {return (d.NumGamers) ? (round2(d.PlayersWatchedToCompletion/d.NumGamers) * 100) : 0;},
  },
];

var reportOptions = {
		
	restEndpoint: config.cutscenesStats,
	restEndpointAsync: config.cutscenesStatsAsync,
	
	processFunction: convertToDict,
	
    enableCSVExport: "content-description",
	graphTitle: currentStat,
    
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.name; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) {return (d.value); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	//xLabel: currentStat,
	yLabel: "Percentage (%)",
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 250, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "", colour: config.chartColour1}, 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br /><br />"
				+ "<table>";
		
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'> $ " + commasFixed2(item.val(d.values)) + "%</td></tr>");
			});
			
			content += "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	var dict = {};
	
	array.map(function(m) {
		m.CutsceneViews.map(function(c) {
			
			$.each(items, function(i, item) {
				var obj = {
					name : (c.CutsceneFriendlyName) ? c.CutsceneFriendlyName : c.CutsceneName,
					value: item.val(c),
					values: c,
				}
				
				if (!dict.hasOwnProperty(item.metric))
					dict[item.metric] = [obj];
				else
					dict[item.metric].push(obj);
			});
			
		});
	});
		
	return dict;
}
