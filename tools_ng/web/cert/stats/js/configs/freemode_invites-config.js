//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-dategrouping", 	// capture stats filtering header
	disabledFields:					// disables header fields 
	[
	 	false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // date-grouping
	     [false, false, false], // dates+builds
	],
};

var minutesInterval = 5;

var items = [
	{
		metric: "Invites from Players" ,
		val: function(d) {return d.InvitesReceived;},
	},
   	{
   		metric: "Invites from NPC" ,
   		val: function(d) {return d.NPCInvitesReceived;},
   	},
];

var reportOptions = {
	restEndpoint: config.freemodeInvites,
	restEndpointAsync: config.freemodeInvitesAsync,
	
	processFunction: convertToDict,
	
	enableCSVExport: "content-description",
	graphTitle: "Invites",
		
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
					
	name: function(d) { return d.name; },
	value: function(d) { return Number(d.value); },
		
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	//xLabel: minutesInterval + " minutes intervals",
	yLabel: "No of Invites",
			
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
			
	hideLegend: false,
	legend: {height: 30, width: 150, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : "", colour: config.chartColour1}, // keep this colour in sync with lineColour 
			
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
			+ "<table>";
		
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'>" 
							+ commasFixed2((item.val(d.values)) ? item.val(d.values) : 0) + "</td></tr>");
			});
			
			content += "</table>";

		return content;
	},

};

function convertToDict(array) {
	/*
	var filledArray = [];
	
	array.map(function(d, i) {
		var currentEpochTime = parseJsonDate(d.key).getTime();
		var previousEpochTime = (i!= 0) ? parseJsonDate(array[i-1].key).getTime() : currentEpochTime;
		
		if ((currentEpochTime - previousEpochTime) > minutesInterval*config.aMinInMillisecs) {
			var step = ((currentEpochTime - previousEpochTime)/(minutesInterval*config.aMinInMillisecs));
			for (var j=1; j<step; j++) {
				
				//console.log(currentEpochTime + j*(minutesInterval*config.aMinInMillisecs));
				
				filledArray.push({
					"key": "/Date(" + (previousEpochTime + (j*minutesInterval*config.aMinInMillisecs)) + ")/",
					"value": {
						"GamerCount": 1,
						"InvitesReceived": 0,
						"NPCInvitesReceived": 0,
					},
				});
			}
		}
		
		filledArray.push(d);
	});
	*/
	
	var dict = {};
		
	array.map(function(d) {
		//var date = (new Date(parseJsonDate(d.key).getTime() + minutesInterval*config.aMinInMillisecs)).toUTCString();
		var date = toUTCStringWOSecs(parseJsonDate(d.key));
		
		$.each(items, function(i, item) {
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: date, value: item.val(d.value), values : d.value}];
			else
				dict[item.metric].push({ name: date, value: item.val(d.value), values : d.value});
		});
		
	});
	
	return dict;
}
