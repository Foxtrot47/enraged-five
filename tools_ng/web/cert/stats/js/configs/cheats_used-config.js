// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

//var platformNames = getPlatforms();

var reportOptions = {
	restEndpoint: config.cheatsUsed,
	restEndpointAsync: config.cheatsUsedAsync,
		
	processFunction: formatData,
	multipleRequests: generateEndpoints,

	enableCSVExport: true,
	
	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Cheat Name", getValue: function(d) {return d.CheatName;} },
	    {title: "Times Used", getValue: function(d) {return d.TimesUsed;} },   
	    {
	    	title: "Percentage of Players (%)", 
	    	getValue: function(d) { return commasFixed2(((d.OnlineGamers) ? d.UniqueGamers/d.OnlineGamers : 0)*100); } 
	    },
	    //{title: "Online Gamers", getValue: function(d) {return d.OnlineGamers;} },
	],
	//reportArraySort: [[0, 0]], // [first item ASC]
	
	/* Groups */
	groups: [
	    {title: "Cheats List", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: false,
	
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function formatData(data) {
	return data[0][0].response.map(function(d) { d["OnlineGamers"] = data[1][0].response; return d; });
}