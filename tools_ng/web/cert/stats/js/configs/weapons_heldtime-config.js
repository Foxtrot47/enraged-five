var weapons = [
    "ADVRIFLE",
    "APPISTOL",
    "ASLTMG",
    "ASLTRIFLE",
    "ASLTSHTGN",
    //"ASLTSMG_DB",
    "ASLTSMG",
    "ASLTSNIPER",
    "BAT",
    "BULLPUP",
    "BUZZARD_BULLET",
    "BUZZARD_ROCKET",
    "CMBTMG",
    //"CMBTPISTOL_DB",
    "CMBTPISTOL",
    "CRBNRIFLE",
    "CROWBAR",
    //"DB",
    "EXTINGUISHER",
    "GCLUB",
    "GRENADE",
    "GRNLAUNCH",
    "HAMMER",
    "HUNTER_BULLET",
    "HUNTER_ROCKET",
    "HVYRIFLE",
    "HVYSNIPER",
    "KNIFE",
    "LAZER_BULLET",
    "LAZER_ROCKET",
    "LOUDHAILER",
    "MG",
    //"MICROSMG_DB",
    "MICROSMG",
    "MINIGUNS",
    "MOLOTOV",
    "NIGHTSTICK",
    //"PASS_DB",
    //"PISTOL_DB",
    "PISTOL",
    //"PISTOL50_DB",
    "PISTOL50",
    "PRGRMMBLAR",
    "PUMP",
    "RHINO_ROCKET",
    "RPG",
    "SAWNOFF",
    "SHOVEL",
    //"SMG_DB",
    "SMG",
    "SMKGRENADE",
    "SNIPERRFL",
    "SPPLAYER_LASER",
    "STKYBMB",
    "STUNGUN",
    "UNARMED",
    "WATER_CANNON",
    "WRENCH",
];	

/*
var killsTypes = [
	"ADVRIFLE",
	"APPISTOL",
	"ASLTMG",
	"ASLTRIFLE",
	"ASLTSHTGN",
	"ASLTSMG_DB",
	"ASLTSMG",
	"ASLTSNIPER",
	"BAT",
	"BULLPUP",
	"BUZZARD_BULLET",
	"BUZZARD_ROCKET",
	"CMBTMG",
	"CMBTPISTOL_DB",
	"CMBTPISTOL",
	"CRBNRIFLE",
	"CROWBAR",
	"EXTINGUISHER",
	"GCLUB",
	"GRENADE",
	"GRNLAUNCH",
	"HAMMER",
	"HUNTER_BULLET",
	"HUNTER_ROCKET",
	"HVYRIFLE",
	"HVYSNIPER",
	"KNIFE",
	"LAZER_BULLET",
    "LAZER_ROCKET",
    "LOUDHAILER",
    "MG",
    "MICROSMG_DB",
    "MICROSMG",
    "MINIGUN",
    "MOLOTOV",
    "NIGHTSTICK",
    "PASS_DB",
    "PISTOL_DB",
    "PISTOL",
    "PISTOL50_DB",
    "PISTOL50",
    "PRGRMMBLAR",
    "PUMP",
    "RHINO_ROCKET",
    "RPG",
    "SAWNOFF",
    "SHOVEL",
    "SMG_DB",
    "SMG",
    "SMKGRENADE",
    "SNIPERRFL",
    "SPPLAYER_LASER",
    "STKYBMB",
    "STUNGUN",
    "UNARMED",
    "WATER_CANNON",
    "WRENCH",
];
    
var deathsTypes = [
	"ADVRIFLE",
	"APPISTOL",
	"ASLTMG",
	"ASLTRIFLE",
	"ASLTSHTGN",
	"ASLTSMG",
	"ASLTSNIPER",
	"BAT",
	"BULLPUP",
	"BUZZARD_BULLET",
	"BUZZARD_ROCKET",
	"CMBTMG",
	"CMBTPISTOL",
	"CRBNRIFLE",
	"CROWBAR",
	"GRENADE",
	"GRNLAUNCH",
	"HAMMER",
	"HUNTER_BULLET",
	"HUNTER_ROCKET",
	"HVYRIFLE",
	"HVYSNIPER",
	"KNIFE",
	"LAZER_BULLET",
	"LAZER_ROCKET",
	"MG",
	"MICROSMG",
	"MINIGUN",
	"MOLOTOV",
	"NIGHTSTICK",
	"PISTOL50",
	"PISTOL",
	"PRGRMMBLAR",
	"PUMP",
	"RHINO_ROCKET",
	"RPG",
//	"RUBBERGUN",
	"SAWNOFF",
	"SHOVEL",
	"SMG",
	"SMKGRENADE",
	"SNIPERRFL",
	"SPPLAYER_LASER",
    "STKYBMB",
    "STUNGUN",
    "UNARMED",
//    "VULKAN_ROCKET",
    "WRENCH",
];
*/

//The types now will be added dynamically from the weapons endpoint

var profileStatList = [
{
	types: weapons,
	requestSubString: "_HELDTIME",
	name: "Total Held Time",
	description: "Total held time per weapon",
	label: "Hours",
	units: "hours",

	bucketSize: null,

	isWeaponStat: true,
	getName: getWeaponName,
	
	convertToHours: true,
},
{
	types: weapons,
	requestSubString: "_HELDTIME",
	name: "Average Held Time",
	description: "Average held time per weapon by each user",
	label: "Average held time per User",
	units: "hours",
	
	bucketSize: null,
	
	isWeaponStat: true,
	getName: getWeaponName,
	
	convertToHours: true,
	isAverage: true,
},
/* These have been moved to telemetry
{
	types: weapons,
	requestSubString: "_KILLS",
	name: "Total Kills",
	description: "Total no of kills per weapon",
	label: "No of Kills",
	units: "kills",
	
	bucketSize: null,
	
	isWeaponStat: true,
	getName: getWeaponName,
},

{
	types: weapons,
	requestSubString: "_KILLS",
	name: "Average Kills",
	description: "Average no of kills per weapon by each user",
	label: "Average No of Kills per User",
	units: "kills",
	
	bucketSize: null,
	
	isWeaponStat: true,
	getName: getWeaponName,
	isAverage: true
},
{
	types: weapons,
	requestSubString: "_DEATHS",
    name: "Total Deaths",
    description: "Total No of deaths per weapon",
    label: "No of Deaths",
    units: "deaths",
    
    bucketSize: null,
	
    isWeaponStat: true,
    getName: getWeaponName,
},
{
	types: weapons,
	requestSubString: "_DEATHS",
    name: "Average Deaths",
    description: "Average number of deaths per weapon for each user",
    label: "Average No of Deaths per User",
    units: "deaths",
    
    bucketSize: null,
	
    isWeaponStat: true,
    getName: getWeaponName,
    isAverage: true,
}
*/
];

function getWeaponName(d) {
	//console.log(d.FriendlyName);
	return (($("#friendlier-names").is(":checked") && d.FriendlyName) ? d.FriendlyName : d.Name);
}

var reportOptions = {
		
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
			
	availableCharts: profileStatList,
}

// Friendly names by default
$("#friendlier-names").prop("checked", true);

//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     [true, false, true], // dates+builds
	     ],
};