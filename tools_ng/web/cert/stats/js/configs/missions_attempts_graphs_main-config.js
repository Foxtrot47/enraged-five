// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var currentStat = "Average Attempts";

var reportOptions = {
		
	restEndpoint: config.missionsStats,
	restEndpointAsync: config.missionsStatsAsync,
	
	processFunction: convertToDict,
		
    enableCSVExport: "content-description",
	graphTitle: currentStat,
    
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.MissionName; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) {return (round2(d.TotalAttempts/d.NumGamersAttempted)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 180, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, 
	
	valueTooltipContent: function(d, b) {
		var html = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
				+ "<table>"
					+ "<tr><td>Average Attempts: </td><td class='right'>" + this.value(d) + "</td></tr>"
					+ "<tr><td>Total Attempts: </td><td class='right'>" + d.TotalAttempts + "</td></tr>"
					+ "<tr><td>Total Players: </td><td class='right'>" + d.NumGamersAttempted + "</td></tr>";
								
			html += "</table>";

		return html;
	},
		              
};

var regExp = project.spMissionGroups[0].regexp; // For getting only the main missions from the results
var missionsOrder = [
	"PRO1",
	"ARM1",
	"ARM2",
	"ARM3",
	"FAM1",
	"FAM2",
	"FAM3",
	"FRA0",
	"LS1",
	"LM1",
	"JH1",
	"JHP1A",
	"JHP2A",
	"JHP1B",
	"JH2a",
	"JH2b",
	"TRV1",
	"TRV2",
	"CHN1",
	"CHN2",
	"TRV3",
	"FAM4",
	"FIB1",
	"FIB2",
	"DH1",
	"FAM5",
	"FRA1",
	"FIB3",
	"DHP1",
	"DHP2b",
	"DH2a",
	"DH2b",
	"FB4P1",
	"FB4P2",
	"FB4P3",
	"FB4P4",
	"FB4P5",
	"FIB4",
	"CAR1",
	"CAR2",
	"SOL1",
	"MAR1",
	"CAR3",
	"EXL1",
	"RH1",
	"RHP1",
	"EXL2",
	"RH2",
	"EXL3",
	"FIB5",
	"TRV4",
	"FH1",
	"MIC1",
	"CAR4",
	"MIC2",
	"AH1",
	"AH2",
	"AHP1",
	"FAM6",
	"SOL2",
	"AH3a",
	"AH3b",
	"MIC3",
	"SOL3",
	"FRA2",
	"MIC4",
	"FH2i",
	"FHPRA",
	"FHPRB",
	"FHPC1",
	"FHPC2",
	"FHPC3",
	"FHPRD",
	"FH2a",
	"FH2b",
	"FINI",
	"FINA",
	"FINB",
	"FINC1",
	"FINC2",
];

function convertToDict(array) {
	var dict = {};
	
	dict[currentStat] = array.filter(function(d) {
			return (d.MissionName.match(regExp) && (missionsOrder.indexOf(d.MissionId) != -1)); 
	 	})
	 	.sort(function(a, b) {
	 		return (missionsOrder.indexOf(a.MissionId) < missionsOrder.indexOf(b.MissionId)) ? -1 : 1;
	 	});
		
	return dict;
}
