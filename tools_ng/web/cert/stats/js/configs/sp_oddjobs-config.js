// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

var fixed2 = function(d) {return d.toFixed(2);}

var reportOptions = {
	restEndpoint: config.missionsStats,
	restEndpointAsync: config.missionsStatsAsync,
	
	processFunction: filterOddJobs,
	
	//hasFilterInput: true,
	
	hasFriendlierNames: false,
	
	description: "",
	   	   	
	chartGroups: {
		getGroups: function(d) {return [d]}, // one group
		getGroupName: function(d) {return "Oddjobs"},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
	    title: "Average Times Triggered",
	    colour: config.chartColour1,
	    getName: function(d) {return d.MissionName; },
	    getValue: function(d) {return (d.NumGamersAttempted) ? d.TotalAttempts/d.NumGamersAttempted : 0; },
	    getObject: function(d) {return d},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
					+ "<tr><td>" + key + ": </td><td class='right'>" 
						+ commasFixed(e.point.Value, 2)
					+ "</td></tr>"
					+ "<tr><td>Average Time Spent: </td><td class='right'>"
						+ formatSecs((e.point.Object.NumGamersAttempted) ? 
								e.point.Object.TotalTime/e.point.Object.NumGamersAttempted : 0
						)
					+ "</td></tr>";
		html += "</table>"; 
			
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(Number(d));
	},
	units: "Times Triggered",
	
	chartLeftMargin: 280,
};


function filterOddJobs(data) {
	return data.filter(function(d) { return d.MissionName.match("^Odd"); });
}
