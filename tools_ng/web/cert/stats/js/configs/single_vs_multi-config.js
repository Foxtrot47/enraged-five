var profileStatList = [
{
	types: [
	"SP0_TOTAL_PLAYING_TIME,SP1_TOTAL_PLAYING_TIME,SP2_TOTAL_PLAYING_TIME",
	//"MP0_TOTAL_PLAYING_TIME,MP1_TOTAL_PLAYING_TIME,MP2_TOTAL_PLAYING_TIME,MP3_TOTAL_PLAYING_TIME,MP4_TOTAL_PLAYING_TIME"
	//"PLAYING_TIME",
	"MP_PLAYING_TIME",
	],
	altNames: [
    "Singleplayer",
    "Multiplayer",
	],
	requestSubString: "",
	name: "SP Vs MP Playing Time",
	description: "Single versus multiplayer total playing time in hours",
	label: "Hours",
	units: "hours",
	
	bucketSize: null,
	
	singleMetric: true,
	
	singleVsMulti: true,
	
	convertToHours: true,
}
];

var reportOptions = {	
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
				
	availableCharts: profileStatList,
}

//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};