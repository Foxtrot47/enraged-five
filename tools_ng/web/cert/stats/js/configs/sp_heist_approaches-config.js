// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "SP_HEIST_CHOSE_JEWEL_STEALTH",
     ],
     name: "Jewel Store Job",
     altNames: [
         "Aggressive",
         "Stealth",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_HEIST_CHOSE_DOCKS_SINK_SHIP",
     ],
     name: "Port of LS Heist",
     altNames: [
         "Deep Sea Diving",
         "Sink Tanker",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_HEIST_CHOSE_BUREAU_FIRECREW",
     ],
     name: "Bureau Raid",
     altNames: [
         "Aerial Assault",
         "Plant Bombs",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_HEIST_CHOSE_BIGS_TRAFFIC",
     ],
     name: "Big Score",
     altNames: [
         "Helicopter",
         "Traffic Control",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name}, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return Number(d.YValue); },
		getObject: function(d) { return (d); },
		
		unit: "players",
		lrMargin: 15,
	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	var pValuesArray = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var copiedPvalues = $.extend(true, {}, pValues);
		
		copiedPvalues.Pairs["StatNames"] = getStatNames(availableChart);
		copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
		
		pValuesArray.push(copiedPvalues);
	});
	
	endpointObjects.push(
	   	{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: pValuesArray,
	   	}
	);
	
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		array.push(
			{
			   	"label" : availableChart.name,
			   	"values": data[0][i].response.map(function(d) {d["name"] = profStat.getName(d, availableChart); return d; }),
			}
		);
	});
	
	return array;
}

function getStatNames(profileStat) {
	return profStat.constructStatNames(profileStat, profileStat.types[0], config.gameTypes[0]).join(",");
}