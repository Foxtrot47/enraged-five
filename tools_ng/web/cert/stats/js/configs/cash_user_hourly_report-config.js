//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cash-report", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // build
		 false, // gamers
	     ],
};

var reportOptions = {
	restEndpoint: config.topUsersCashReport,
	restEndpointAsync: config.topUsersCashReportAsync,
		
	//multipleRequests: generateEndpoints,
	selectedEndpoints: generateUserCashEndpoints,
	processFunction: groupResults,
	
	hasExtraRestParams: [
	{
		key: "Count",
	    value: 50,
	},
	{
		key: "Offset",
	    value: 0,
	},
	{
		key: "UpperXPLimit",
		value: 90000000,
	},
	],
};

/*
function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	
	var pValuesArray = [];
	
	var gamerHandles = pValues.Pairs["GamerHandlePlatformPairs"].split(",");
			
	$.each(gamerHandles, function(i , gamerHandle) {
		var copiedPvalues = $.extend(true, {}, pValues);
		copiedPvalues.Pairs["GamerHandle"] = gamerHandle.split("|")[0].replace("<","");
		copiedPvalues.Pairs["PlatformIdentifier"] = gamerHandle.split("|")[1].replace(">","");
		copiedPvalues.Pairs["CharacterSlot"] = 0;
		copiedPvalues.Pairs["CharacterId"] = 1;
		
		pValuesArray.push(copiedPvalues);
	});
	
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: pValuesArray,
	   	}
	];
			
	return endpointObjects;
}
*/

function generateUserCashEndpoints(pValues, gamers) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	
	var pValuesArray = [];
	
	// pValues belong to sc header, need to add the extra details 
	$.each(gamers, function(i , gamer) {
		var copiedPvalues = $.extend(true, {}, pValues);
		copiedPvalues.Pairs["GamerHandle"] = gamer.GamerHandle;
		copiedPvalues.Pairs["PlatformIdentifier"] = rosPlatforms[gamer.Platform].Name;
		copiedPvalues.Pairs["CharacterSlot"] = gamer.CharacterSlot;
		copiedPvalues.Pairs["CharacterId"] = gamer.CharacterId;
		
		pValuesArray.push(copiedPvalues);
	});
	
	var endpointObjects = [
		{
			restUrl: config.restHost + config.userCashReport,
			restAsyncUrl: config.restHost + config.userCashReportAsync + pValues.ForceUrlSuffix,
			pValues: pValuesArray,
	   	}
	];
			
	return endpointObjects;
}

function groupResults(data) {
	var array = [];
	
	$.each(data[0], function(i, d) {
		array.push(d.response);
	});
	
	return array;
}