// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.freemodeTutorialStats,
	restEndpointAsync: config.freemodeTutorialStatsAsync,
	
	processFunction: formatData,
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values},
		getMetadata: function(d) {return d.metadata},
		
		// function to get the  name from the rest data
		getName: function(d) {return (d.Key)},
		// function to calculate the value from the rest data
		getValue: function(d) { return (d.Value.Count); },
		getObject: function(d) { return (d); },
		
		tooltipContent: function(key, y, e, graph) {
			var sum = d3.sum(graph.container.__data__[0].values, function(d) {
				return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
			});
			
			var metadata = graph.container.__data__[0].metadata;
			
			// Use e.value instead of y, y is a formated string and parsing to number fails
			var percentage = ((e.value/sum)*100).toFixed(2);
			
			var html = "<h3>" + key + "</h3><br/>"
				+ "<table>"
					+ "<tr><td>" + metadata.Metric + ":</td>"
					+ "<td>" + commasFixed2(e.value) + " " + metadata.Unit + " (" + percentage + "%)" + "</td></tr>"
					+ "<tr><td>Total:</td><td>" +  commasFixed2(sum) + " " + metadata.Unit + "</td></tr>";
			
			if (typeof e.point.Value.AverageDeaths !== "undefined")
				html += "<tr><td>Average Deaths:</td><td>" 
						+ commasFixed(e.point.Value.AverageDeaths, 4)
					+ "</td></tr>";
			
			if (typeof metadata.TimeSpentPlaying !== "undefined")
				html += "<tr><td>Time Playing:</td><td>" 
						+ formatSecsWithDays(metadata.TimeSpentPlaying)
					+ "</td></tr>";
			
			if (typeof metadata.TimesPlayed !== "undefined")
				html += "<tr><td>Times Played:</td><td>" 
						+ commasFixed2(metadata.TimesPlayed)
					+ "</td></tr>";
			
			if (typeof metadata.UniqueGamers !== "undefined")
				html += "<tr><td>Unique Players:</td><td>" 
						+ commasFixed2(metadata.UniqueGamers)
					+ "</td></tr>";
			
			//That's for jobs
			if (typeof e.point.TimeSpentPlaying !== "undefined")
				html += "<tr><td>Time Playing:</td><td>" 
						+ formatSecsWithDays(e.point.TimeSpentPlaying)
					+ "</td></tr>";
			
			if (typeof e.point.TimesPlayed !== "undefined")
				html += "<tr><td>Times Played:</td><td>" 
						+ commasFixed2(e.point.TimesPlayed)
					+ "</td></tr>";
			
			if (typeof e.point.UniqueGamers !== "undefined")
				html += "<tr><td>Unique Players:</td><td>" 
						+ commasFixed2(e.point.UniqueGamers)
					+ "</td></tr>";
						
			return html += "</table>";
		},
		
		lrMargin: 40,
	},
};

function formatData(data) {
	var array = [
	    {
	    	"label" : "Race - Players",
	    	"values": data.RacePlayerDetails.ParticipantInfo.map(function(d) {d.Key = "Total Players on Race: " + d.Key; return d; }),
	    	"metadata": $.extend(data.RacePlayerDetails, {"Unit" : "players", "Metric" : "Number of Affected Players"}),
	    },
	    {
	    	"label" : "Mission - Players",
	    	"values": data.MissionPlayerDetails.ParticipantInfo.map(function(d) {d.Key = "Total Players on Mission: " + d.Key; return d; }),
	    	"metadata": $.extend(data.MissionPlayerDetails, {"Unit" : "players", "Metric" : "Number of Affected Players"}),
	    },
	    {
	    	"label" : "Tutorial Jobs",
	    	"values": data.TutorialJobs.map(function(d) {
	    		d["Key"] = d.Name;
	    		d["Value"] = {};
	    		d["Value"]["Count"] = d.TotalGamers;
	    		return d;
	    	}),
	    	"metadata": {"Unit" : "players", "Metric" : "Total Players"},
	    },
	];
	
	return array;
}
