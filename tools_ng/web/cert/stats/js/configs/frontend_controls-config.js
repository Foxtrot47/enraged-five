// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
{
	types: [
	"_PROFILE_SETTING_12", // CONTROLLER_CONTROL_CONFIG = 12,
	],
	altNames: [
	    "Standard", //"MO_CCONF_1",
	    "Alternate", //"MO_CCONF_2",
	    "Southpaw", //"MO_CCONF_3",
	    "Alternate + Southpaw", //"MO_CCONF_4",
	],
	requestSubString: "_",
	name: "Control Type",
	//description: "No of users for front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1, 
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_0", // TARGETING_MODE = 0,
	],
	altNames: [
	    "Traditional GTA", //"MO_TAR1",
	    "Assisted Aiming", //"MO_TAR2",
	    "Free Aim", //"MO_TAR3",
	],
	requestSubString: "_",
	name: "Targeting Mode",
	//description: "No of users for front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1, 
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_2", // CONTROLLER_VIBRATION = 2,
	],
	altNames: [
	    "Off", //"MO_OFF",
	    "On", //"MO_ON",
	],
	requestSubString: "_",
	name: "Vibration",
	//description: "No of users for front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1, 
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_1", // AXIS_INVERSION = 1,
	],
	altNames: [
	    "Off", //"MO_OFF",
	    "On", //"MO_ON",
	],
	requestSubString: "_",
	name: "Invert Look",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_13", //CONTROLLER_AIM_SENSITIVITY = 13,
	],
	requestSubString: "_",
	name: "Aim Sensitivity",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
	
	//addPercentageLabel: true,
	//multiplyToPercentage: 10,
	showOnlyLowerRange: true,
	validRange: [0, 10],
	
	orderDescBarchart: true,
},

];

var reportOptions = {	
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
			
	availableCharts: profileStatList.map(function(d) {
		d["hideStartDate"] = true;
		return d;
	}),
}
