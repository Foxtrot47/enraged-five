var navigationTitle = "Asset Stats";
var navigationItems = [{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Asset Sizes",
		href: "asset_sizes.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Asset Sizes Graph",
		href: "asset_sizes_graph.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Resources",
		href: "resources.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
];

