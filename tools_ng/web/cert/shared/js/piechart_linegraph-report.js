// reportOptions variable comes from respective config file

// Array that stores objects for each element returned via rest
var chartsData;

var NVGraphs = new NVD3CustomGraphs();
var LineGraphs = new LinesGraph();

var mainDOMId = "piechart";
var breakdownDOMId = "breakdown";

var lastSliceClicked = null;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", mainDOMId)
				//.addClass((reportOptions.isClickable) ? "clickable" : "")
		)
		.append(
			$("<div>").attr("id", breakdownDOMId)
		);
	
	initSvg(mainDOMId);
			
	$("#filter").click(function() {
		generateCharts();
	});

	generateCharts();
}

function generateCharts() {
	chartsData = [];
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
			
} //end of generateCharts()

function cleanCharts() {
	cleanGraph(mainDOMId);
	cleanDetailedChart();
}

function cleanDetailedChart() {
	cleanGraph(breakdownDOMId);
	lastSliceClicked = null;
}

function drawCharts(data) {
	//cleanDetailedChart();
	//$("#detailed-piechart").hide("fade", {}, "normal");
	
	// If a processFunction function is specified
	if (data) {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
		
	var graphOptions = reportOptions.main;
	
	var datum = [{
		key: graphOptions.getPieLabel(chartsData),
		values: graphOptions.getValuesArray(chartsData),
		metadata: graphOptions.getMetadata(chartsData),
	}];
	
	var clickFunction = (reportOptions.isClickable) ? 
			(function(event) { 
				lastSliceClicked = event.label;
				event.point["label"] = event.label;
				drawDetailedChart(event.point);
			})
			: null;
	
	NVGraphs.drawPiechart(datum, mainDOMId, graphOptions, clickFunction);
	
	if (!reportOptions.isClickable)
		drawDetailedChart(chartsData);
	
	if (lastSliceClicked) {
		var availableLabels = datum[0].values.map(function(d) { return graphOptions.getName(d); });
		var searchIndex = availableLabels.indexOf(lastSliceClicked);
		
		if (searchIndex != -1) {
			drawDetailedChart(datum[0].values[searchIndex]);
		}
		else {
			// clean it cause the previous selected item is not available
			cleanDetailedChart();
		}
	}
				
}

function drawDetailedChart(selectedObject) {
	// Construct the chart data in a format that nvd3 understands
	
	var graphOptions = reportOptions.breakdown;	
	var detailedObject = graphOptions.getObject(selectedObject);

	// This will add the svg on the first time
	if ($("#" + breakdownDOMId).find("#" + graphOptions.elementId).size() == 0) {
		$("#" + breakdownDOMId)
			.append(
				$("<div>")
					.attr("id", graphOptions.elementId)
			);
		initSvg(graphOptions.elementId);
	}
		
	//NVGraphs.drawPiechart(datum, breakdownItemDOMId, graphOptions);
	LineGraphs.draw(detailedObject, graphOptions, graphOptions.elementId, breakdownDOMId);
}
