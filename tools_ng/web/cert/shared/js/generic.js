var rosPlatforms,
	rosPlatformsDict,
	matchTypes,
	matchTypesDict,
	matchSubTypesDict,
	freemodeCategories,
	freemodeCategoriesDict,
	jobResults,
	jobResultsDict;

/* KEEP THIS IN SYNC WITH THE CSS VALUES */
var headerNavProperties = {
	localStoragekey: "header-nav-settings",
		
	//headerHideTop: "-14%",
	//contentHeaderHideTop: "1%",
	//contentHeaderHideHeight: "99%",
		
	headerShowTop: 0,
	headerBottomMargin: 5,
	//headerBottomMargin: 15,
		
	//contentHeaderShowTop: "15%",
	//contentHeaderShowHeight: "85%",
	//contentHeaderShowTop: "1%",
	//contentHeaderShowHeight: "99%",
		
	/*
	navHideLeft: "-15%",
	contentNavHideLeft: "1%",
	contentNavHideWidth: "98%",
	headerNavHideLeft: "0%",
	headerNavHideWidth: "100%",
	
	navShowLeft: "0%",
	contentNavShowLeft: "15.5%",
	contentNavShowWidth: "84%",
	headerNavShowLeft: "15%",
	headerNavShowWidth: "85%",
	*/
}

var config = {
	restHost: window.location.protocol + "//" + window.location.host + "/StatisticsService/",
	webHost: window.location.protocol + "//" + window.location.host,
	currentFilename: window.location.pathname.substr(
						window.location.pathname.lastIndexOf("/") + 1, 
						window.location.pathname.indexOf(".html") + 5),
						
    csvBackend: "/AspUtils/SaveCSV.aspx",
    tableauEndpoint: "http://data.rockstargames.com/views/",
		
	//currentFilename: window.location.pathname.substr(window.location.pathname.lastIndexOf("/") + 1),
	serverTypeScript: "/shared/server_type.js",
	serverType: null,
	serverTypeProd: "prod",
	
	profileStatDefPath: "ProfileStatDefinitions/All",
	
	profileStatsCombined: "Reports/VerticaCombinedProfileStats/Parameters",
	profileStatsCombinedAsync: "Reports/VerticaCombinedProfileStats/ParametersAsync",
	profileStatsCombinedDiff: "Reports/VerticaCombinedDiffProfileStats/Parameters",
	profileStatsCombinedDiffAsync: "Reports/VerticaCombinedDiffProfileStats/ParametersAsync",
	profileStatsDivided: "Reports/VerticaDividedProfileStats/Parameters",
	profileStatsDividedAsync: "Reports/VerticaDividedProfileStats/ParametersAsync",
	profileStatsDividedDiff: "Reports/VerticaDividedDiffProfileStats/Parameters",
	profileStatsDividedDiffAsync: "Reports/VerticaDividedDiffProfileStats/ParametersAsync",
	profileStatsPerUser: "Reports/VerticaPerUserProfileStats/Parameters",
	profileStatsPerUserAsync: "Reports/VerticaPerUserProfileStats/ParametersAsync",
	profileStatsHistorical: "Reports/VerticaHistoricalProfileStats/Parameters",
	profileStatsHistoricalAsync: "Reports/VerticaHistoricalProfileStats/ParametersAsync",
	
	asyncRequestFailTxt: "Request Failed! Please check the console for more info.",
	
	//weaponsAll: "Weapons/All",
	weaponsAll: "GameAssets/Weapons",
	
	// Map Telemetry Specific
	buildsAll: "Builds/All",
	levelsAll: "Levels/All",
	vehiclesAll: "GameAssets/Vehicles",
	//platformsAll: "Platforms/All",
	cutscenesAll: "Cutscenes/All",
	
	overlayGroupsAll: ["None", "Telemetry"],
	overlaysAll: [
				{Name: "Fps/Draw Lists", Value: "Fps"},
				{Name: "Physics Shape Test", Value: "ShapeTest"},
				{Name: "Memory Shortfall", Value: "MemShortfall"},
				{Name: "Capture Performance", Value: "Performance"},
				{Name: "Deaths", Value: "Deaths"},
	            //{Name: "Spawn Points", Value: "Spawn"},
				],
				//{Name: "Mag Demo", Value: "Mag Demo"}],
	//gametypesAll: "Gametypes/All",
	//resolutionsAll: "ProcessedStatResolutions/All",
	resolutionsAll: [20, 100],
	//buildConfigsAll: "BuildConfigs/All",
	metricAll: ["FrameTime", "UpdateTime", "DrawTime", "GpuTime",
	            "IndexCount", "DrawcallCount", "ObjectCount", "GBufferIndexCount",
	            "DrawLists"],
	fpsDrawListMetricName: "DrawLists",
	//drawListAll: "DrawLists/All",
	drawListAll: "GameAssets/DrawLists", 

	statAll: [{Name:"Minimum", Value:"min"},
	          {Name:"Maximum", Value:"max"},
	          {Name:"Average", Value:"avg"}],
	automatedTestsAll: [{Name: "Other", Value: 0, Description: "Automated tests that don't fall in a specific category."},
						{Name: "Map Only", Value: 1, Description: "Automated test where only map rendering/update is enabled."},
						{Name: "Everything", Value: 2, Description: "Automated Test where everything is enabled."},
						//{Name: "Mag Demo", Value: 3, Description: "Shows stats that were captured on MagDemo play through runs."},
						{Name: "Night Time Map Only", Value: 6, Description: "Automated test where only map rendering/update is enabled and the stats are captured at night."},
						],
	memoryTypesAll: [
	                 {Name: "Physical Shortfall", Value: "PhysicalShortfall"},
	                 {Name: "Virtual Shortfall", Value: "VirtualShortfall"},
	                 ],
	
	buildsPath: "Builds/",
	fpsPath: "Fps",
	drawListPath: "DrawLists",
	
	capturePerformanceMetrics: "CaptureStats/AutomatedTests/5/PerformanceMetrics",
	capturePerformanceStats: "CaptureStats/AutomatedTests/5/PerformanceStats",
	capturePerformanceZones: "CaptureStats/AutomatedTests/5/Zones",
	capturePerformanceSummary: "CaptureStats/AutomatedTests/5/Summary",
	
	cpsDrawListMetricName: "Draw List",
	
	fpsPerformanceStats: "Reports/VerticaFpsPerformance/Parameters",
	fpsPerformanceStatsAsync: "Reports/VerticaFpsPerformance/ParametersAsync",
	
	fpsDrawListPerformanceStats: "Reports/VerticaFpsDrawListPerformance/Parameters",
	fpsDrawListPerformanceStatsAsync: "Reports/VerticaFpsDrawListPerformance/ParametersAsync",
	
	memoryShortfallStats: "Reports/VerticaMemoryShortfall/Parameters",
	memoryShortfallStatsAsync: "Reports/VerticaMemoryShortfall/ParametersAsync",
	
	physicsShapeTestStats: "Reports/VerticaPhysicsShapeCost/Parameters",
	physicsShapeTestStatsAsync: "Reports/VerticaPhysicsShapeCost/ParametersAsync",
	
	/*
	spawnStats: "Reports/FreemodeInjuryReport/Parameters",
	spawnStatsAsync: "Reports/FreemodeInjuryReport/ParametersAsync",
	*/
	mapDeathmatchStats: "Reports/VerticaDMInjuryHeatmap/Parameters",
	mapDeathmatchStatsAsync: "Reports/VerticaDMInjuryHeatmap/ParametersAsync",
	
	mapFpsStats: "Reports/BasicFpsPerformance/Parameters",
	mapFpsStatsAsync: "Reports/BasicFpsPerformance/ParametersAsync",
	// End of Map Telemetry Specific
	
	// Social Club Lists
	//SCFreemodeNames: "SocialClub/FreemodeMissions",
	SCCountries: "SocialClub/Countries",
	
	//Legacy
	//SCGroupsAndUsers: "SocialClub/Actors",
	
	SCVerticaUsersSearch: "VerticaData/Users/Search",
	SCVerticaUsersGroups: "VerticaData/Users/Groups",
	
	//SCUsers: "SocialClub/Actors/Users",
	//SCGroups: "SocialClub/Actors/Groups",
	//SCHierarchies: "SocialClub/Actors/Hierarchy",
	
	VerticaGamersSearch: "VerticaData/Gamers/Search",
	VerticaGamersGroups: "VerticaData/Gamers/Groups",
	VerticaGamersValidate: "VerticaData/Gamers/Validate",	
	
	VerticaMissionsSearch: "VerticaData/Missions/Search",
	
	//gamersPath: "Gamers/",
	//sessionsSubPath: "/Sessions",
	//sessionsSummaryPath: "RawTelemetryStats/SessionSummary",
	
	//missionsStats: "Missions/Stats",
	missionsStats: "Reports/VerticaMissionAttempts/Parameters",
	missionsStatsAsync: "Reports/VerticaMissionAttempts/ParametersAsync",
	
	missionsDeaths: "Reports/VerticaMissionDeaths/Parameters",
	missionsDeathsAsync: "Reports/VerticaMissionDeaths/ParametersAsync",
	
	wantedLevels: "Reports/VerticaWantedLevels/Parameters",
	wantedLevelsAsync: "Reports/VerticaWantedLevels/ParametersAsync",
	
	freemodeMissionsStats: "Reports/VerticaFreemodeMatches/Parameters",
	freemodeMissionsStatsAsync: "Reports/VerticaFreemodeMatches/ParametersAsync",
	freemodeRacesStats: "Reports/VerticaFreemodeRaces/Parameters",
	freemodeRacesStatsAsync: "Reports/VerticaFreemodeRaces/ParametersAsync",
	freemodeActivitiesStats: "Reports/VerticaFreemodeAmbientMissions/Parameters",
	freemodeActivitiesStatsAsync: "Reports/VerticaFreemodeAmbientMissions/ParametersAsync",
	freemodeTutorialStats: "Reports/VerticaMPTutorial/Parameters",
	freemodeTutorialStatsAsync: "Reports/VerticaMPTutorial/ParametersAsync",
	
	freemodeContentStats: "Reports/MissionContent/Parameters",
	freemodeContentStatsAsync: "Reports/MissionContent/ParametersAsync",
	
	freemodeContentDetailsStats: "Reports/MissionContentDetails/Parameters",
	freemodeContentDetailsStatsAsync: "Reports/MissionContentDetails/ParametersAsync",
	
	cutscenesStats: "Reports/VerticaCutscenesWatched/Parameters",
	cutscenesStatsAsync: "Reports/VerticaCutscenesWatched/ParametersAsync",
	radioStationsStats: "Reports/VerticaRadioStationListeners/Parameters",
	radioStationsStatsAsync: "Reports/VerticaRadioStationListeners/ParametersAsync",
	tvShowsStats: "Reports/VerticaTVShowViewers/Parameters",
	tvShowsStatsAsync: "Reports/VerticaTVShowViewers/ParametersAsync",
	cinemaStats: "Reports/VerticaMovieViewers/Parameters",
	cinemaStatsAsync: "Reports/VerticaMovieViewers/ParametersAsync",
	vehiclesStats: "Reports/VerticaVehicleDistanceDriven/Parameters",
	vehiclesStatsAsync: "Reports/VerticaVehicleDistanceDriven/ParametersAsync",
	
	shoppingStats: "Reports/VerticaShopPurchases/Parameters",
	shoppingStatsAsync: "Reports/VerticaShopPurchases/ParametersAsync",
	
	expenditureStats: "Reports/VerticaMPExpenditure/Parameters",
	expenditureStatsAsync: "Reports/VerticaMPExpenditure/ParametersAsync",
	
	earningsStats: "Reports/VerticaMPEarnings/Parameters",
	earningsStatsAsync: "Reports/VerticaMPEarnings/ParametersAsync",
	
	characterCash: "Reports/VerticaCharacterCash/Parameters",
	characterCashAsync: "Reports/VerticaCharacterCash/ParametersAsync",
	
	characterSkills: "Reports/VerticaCharacterSkills/Parameters",
	characterSkillsAsync: "Reports/VerticaCharacterSkills/ParametersAsync",
	
	weaponKillsDeaths: "Reports/VerticaKillDeath/Parameters",
	weaponKillsDeathsAsync: "Reports/VerticaKillDeath/ParametersAsync",
	
	weaponModPurchases: "Reports/VerticaWeaponModPurchases/Parameters",
	weaponModPurchasesAsync: "Reports/VerticaWeaponModPurchases/ParametersAsync",
	
	concurrentUsers: "Reports/VerticaConcurrentUsers/Parameters",
	concurrentUsersAsync: "Reports/VerticaConcurrentUsers/ParametersAsync",
		
	onlineGamerCount: "Reports/VerticaOnlineGamerCount/Parameters",
	onlineGamerCountAsync: "Reports/VerticaOnlineGamerCount/ParametersAsync",
	
	onlineGamers: "Reports/VerticaOnlineGamers/Parameters",
	onlineGamersAsync: "Reports/VerticaOnlineGamers/ParametersAsync",
		
	topUsersCashReport: "Reports/VerticaMPTopRankedGamers/Parameters",
	topUsersCashReportAsync: "Reports/VerticaMPTopRankedGamers/ParametersAsync",
	
	userCashReport: "Reports/VerticaPerUserCashReport/Parameters",
	userCashReportAsync: "Reports/VerticaPerUserCashReport/ParametersAsync",
	
	hourlyCashReport: "Reports/VerticaMPHourlyCash/Parameters",
	hourlyCashReportAsync: "Reports/VerticaMPHourlyCash/ParametersAsync",
	
	rankCashReport: "Reports/PerRankInfo/Parameters",
	rankCashReportAsync: "Reports/PerRankInfo/ParametersAsync",
	
	cashPurchasesReport: "Reports/VerticaCashPurchases/Parameters",
	cashPurchasesReportAsync: "Reports/VerticaCashPurchases/ParametersAsync",
		
	bettingEarned: "Reports/VerticaMPBetting/Parameters",
	bettingEarnedAsync: "Reports/VerticaMPBetting/ParametersAsync",
	
	bettingEarnedNew: "Reports/NewMPBetting/Parameters",
	bettingEarnedNewAsync: "Reports/NewMPBetting/ParametersAsync",
	
	averageBets: "Reports/VerticaMPAverageBets/Parameters",
	averageBetsAsync: "Reports/VerticaMPAverageBets/ParametersAsync",
	
	transactionHistory: "Reports/VerticaMPTransactionHistory/Parameters",
	transactionHistoryAsync: "Reports/VerticaMPTransactionHistory/ParametersAsync",
	
	freemodeInvites: "Reports/VerticaMPInvites/Parameters",
	freemodeInvitesAsync: "Reports/VerticaMPInvites/ParametersAsync",
	
	websiteVisits: "Reports/VerticaWebsiteVisits/Parameters",
	websiteVisitsAsync: "Reports/VerticaWebsiteVisits/ParametersAsync",
	
	cheatsUsed: "Reports/VerticaSPCheaters/Parameters",
	cheatsUsedAsync: "Reports/VerticaSPCheaters/ParametersAsync",
	
	missionCaptureStats: "Reports/VerticaMissionPerformance/Parameters",
	missionCaptureStatsAsync: "Reports/VerticaMissionPerformance/ParametersAsync",
	
	cutsceneCaptureStats: "Reports/VerticaCutscenePerformance/Parameters",
	cutsceneCaptureStatsAsync: "Reports/VerticaCutscenePerformance/ParametersAsync",
	cutsceneCaptureDetailsStats: "Reports/VerticaCutscenePerformanceDetails/Parameters",
	cutsceneCaptureDetailsStatsAsync: "Reports/VerticaCutscenePerformanceDetails/ParametersAsync",
	
	cutsceneLights: "Reports/VerticaCutsceneLights/Parameters",
	cutsceneLightsAsync: "Reports/VerticaCutsceneLights/ParametersAsync",
	
	perBuildCutscenes: "Reports/PerBuildCutscenes/Parameters",
	perBuildCutscenesAsync: "Reports/PerBuildCutscenes/ParametersAsync",
	
	mapExportSummary: "Reports/MapExportsDailySummary/Parameters",
	mapExportSummaryAsync: "Reports/MapExportsDailySummary/ParametersAsync",
	
	mapExportsByUser: "Reports/MapExportsByUser/Parameters",
	mapExportsByUserAsync: "Reports/MapExportsByUser/ParametersAsync",
	
	mapExportsBySection: "Reports/MapExportsBySection/Parameters",
	mapExportsBySectionAsync: "Reports/MapExportsBySection/ParametersAsync", 
	
	mapExportsSectionAge: "Reports/MapExportsSectionAge/Parameters",
	mapExportsSectionAgeAsync: "Reports/MapExportsSectionAge/ParametersAsync",
	
	mapExportHistorical: "Reports/MapExportsHistorical/Parameters",
	mapExportHistoricalAsync: "Reports/MapExportsHistorical/ParametersAsync",

	// Deprecated
	mapExportStats: "MapExportStats/",
	mapExportStatsAll: "MapExportStats/All",
	mapExportStatsUsers: "MapExportStats/Users",
	mapSectionsAll: "MapSections/All",
	mapExportStatsRaw: "MapExportStats/AllExportStats",
	//
	
	captureStats: "CaptureStats/",
	captureStatsBuildsPath: "PerfStats/",
	captureStatsChangelistPath: "Changelist/",
	captureStatsZonesPath: "Zones",
	captureStatsHistoricalPath: "Historical",
	captureStatsDecimals: 4,
	captureStatsChangelists: "CaptureStats/Changelists",
	captureStatsBuilds: "CaptureStats/PerfStats/Builds",
	
	cutsceneCaptures: "Reports/CutsceneCaptures/Parameters",
	cutsceneCapturesAsync: "Reports/CutsceneCaptures/ParametersAsync",
	
	playthroughsUsers: "Playthroughs/Users",
	playthroughsBuilds: "Playthroughs/Builds",
	playthroughsPlatforms: "Playthroughs/Platforms",
	// Reports
	playthroughsSummary: "Reports/PlaythroughSummary/Parameters",
	playthroughsSummaryAsync: "Reports/PlaythroughSummary/ParametersAsync",
	playthroughsBreakdown: "Reports/PlaythroughBreakdown/Parameters",
	playthroughsBreakdownAsync: "Reports/PlaythroughBreakdown/ParametersAsync",
	playthroughsComments: "Reports/PlaythroughComments/Parameters",
	playthroughsCommentsAsync: "Reports/PlaythroughComments/ParametersAsync",
	playthroughsMissions: "Reports/PlaythroughMissionAttempts/Parameters",
	playthroughsMissionsAsync: "Reports/PlaythroughMissionAttempts/ParametersAsync",
		
	// For admin
	playthroughsSessions: "Playthroughs/Sessions",
		
	/* Difficutly tracking endpoints*/
	
	userMissionCash: "Reports/VerticaPerUserMissionCash/Parameters",
	userMissionCashAsync: "Reports/VerticaPerUserMissionCash/ParametersAsync",
	
	userMissionAttempts: "Reports/VerticaPerUserMissionAttempts/Parameters",
	userMissionAttemptsAsync: "Reports/VerticaPerUserMissionAttempts/ParametersAsync",

	userMissionRatings: "Reports/PlaythroughMissionRatings/Parameters",
	userMissionRatingsAsync: "Reports/PlaythroughMissionRatings/ParametersAsync",
	
	userFriendActivity: "Reports/VerticaPerUserFriendActivity/Parameters",
	userFriendActivityAsync: "Reports/VerticaPerUserFriendActivity/ParametersAsync",
	
	userWantedlevels: "Reports/VerticaPerUserWantedLevels/Parameters",
	userWantedlevelsAsync: "Reports/VerticaPerUserWantedLevels/ParametersAsync",
		
	userClothChanges: "Reports/VerticaPerUserClothChanges/Parameters",
	userClothChangesAsync: "Reports/VerticaPerUserClothChanges/ParametersAsync",
		
	userPropChanges: "Reports/VerticaPerUserPropChanges/Parameters",
	userPropChangesAsync: "Reports/VerticaPerUserPropChanges/ParametersAsync",
	
	userSPCheats: "Reports/VerticaPerUserSPCheats/Parameters",
	userSPCheatsAsync: "Reports/VerticaPerUserSPCheats/ParametersAsync",
	
	userMGMedals: "Reports/VerticaPerUserMiniGameMedals/Parameters",
	userMGMedalsAsync: "Reports/VerticaPerUserMiniGameMedals/ParametersAsync",
	
	memoryPoolsStats: "Reports/VerticaMemoryPools/Parameters",
	memoryPoolsStatsAsync: "Reports/VerticaMemoryPools/ParametersAsync",
	
	memoryStoresStats: "Reports/VerticaMemoryStores/Parameters",
	memoryStoresStatsAsync: "Reports/VerticaMemoryStores/ParametersAsync",
	
	memorySkeletonsStats: "Reports/VerticaMemorySkeletons/Parameters",
	memorySkeletonsStatsAsync: "Reports/VerticaMemorySkeletons/ParametersAsync",
	
	memoryUsageStats: "Reports/VerticaMemoryUsage/Parameters",
	memoryUsageStatsAsync: "Reports/VerticaMemoryUsage/ParametersAsync",
	
	assetSizes: "Reports/GameAssetsHistoricalMapSectionAssetBreakdown/Parameters",
	assetSizesAsync: "Reports/GameAssetsHistoricalMapSectionAssetBreakdown/ParametersAsync",
	
	resourceStats: "Reports/ResourceStatReport/Parameters",
	resourceStatsAsync: "Reports/ResourceStatReport/ParametersAsync",
	
	newResourceStats: "Reports/NewResourceStats/Parameters",
	newResourceStatsAsync: "Reports/NewResourceStats/ParametersAsync",
	
	newResourceDetailsStats: "Reports/NewResourceStatDetails/Parameters",
	newResourceDetailsStatsAsync: "Reports/NewResourceStatDetails/ParametersAsync",
	
	clipsPerformanceStats: "Reports/VerticaClips/Parameters",
	clipsPerformanceStatsAsync: "Reports/VerticaClips/ParametersAsync",

	unusedClipsStats: "Reports/UnusedClips/Parameters",
	unusedClipsStatsAsync: "Reports/UnusedClips/ParametersAsync",
		
	// Enums
	platformsEnums: "Enums/Platforms",
	rosPlatformsEnums: "Enums/ROSPlatforms",
	buildConfigsEnums: "Enums/BuildConfigs",
	gameTypesEnums: "Enums/GameTypes",
	fileTypesEnums: "Enums/FileTypes",
	platformsEnums: "Enums/Platforms",
	clipCategoriesEnums: "Enums/ClipDictionaryCategories",
	resourceBucketEnums: "Enums/ResourceBucketTypes",
	matchTypesEnums: "Enums/MatchTypes",
	matchSubTypesEnumsDict: {
		"Deathmatch": "Enums/DeathmatchSubTypes",
		"Mission": "Enums/MissionSubTypes",
		"Race": "Enums/RaceSubTypes",
	},
	freemodeCategoriesEnums: "Enums/FreemodeMissionCategories",
	dateGroupingEnums: "Enums/DateTimeGroupModes",
	weaponCategoriesEnums: "Enums/weaponcategories",
	expenditureCategoriesEnums: "Enums/SpendCategories",
	getJobResultsEnums: "Enums/JobResults",
	
	gameTimes: [
	    {
	    	"Name": "Day",
    		"Value": "7-19",
	    },
	    {
	    	"Name": "Night",
    		"Value": "20-6",
	    }
	],
	
	ugcStatus: [
	    {
	    	"Name": "Saved & Published",
    		"Value": null,
	    },
	    {
	    	"Name": "Published Only",
    		"Value": true,
	    },
	    {
	    	"Name": "Saved Only",
    		"Value": false,
	    },
    ],
	
	/*
	dateGrouping: [
	    "HOURLY",
	    "DAILY",
	    "WEEKLY",
	    "MONTHLY",
	    "YEARLY",
	],
	*/
	
	datesBuildsOptions: [
        {Name: "Dates + Builds", Value: 0},
        {Name: "Dates Only", Value: 1},
        {Name: "Builds Only", Value: 2},
        {Name: "CWP Only", Value: 3},
	],
	
	reportsAll: "Reports/All",
	reportsPrefix: "Reports/",
	reportsSuffix: "/Parameters",
	reportsStyleSuffix: "/Stylesheets",
	
	reportsQueryAsync: "Reports/QueryAsync",
	reportsPresets: "Reports/Presets",
	reportForceSuffix: "?forceRegen=true",

	transitionDuration: 600,
	transitionDelay: 600,
	
	anHourInMillisecs: (60 * 60 * 1000),
	anHourInSecs: (60 * 60),
	aMinInMillisecs: (60 * 1000),
	aKmInMetres: 1000,
	
	storageExpirePeriod: 86400, // (60 * 60 *24) one day in secs
	
	// Treat input as local time
	dateInputFormat: d3.time.format("%d/%m/%Y %H:%M"),
	// Treat input as UTC
	dateInputUTCFormat: d3.time.format.utc("%d/%m/%Y %H:%M"),
	// Format of the date picker
	datePickerFormat: "dd/mm/yy",
	timePickerFormat: "hh:mm",
	
	gameTypes: ["Singleplayer", "Multiplayer"],
	
	chartColour1: "#ff6600",
	chartColour2: "#0054E6",
	chartColour3: "#697D91",
	chartColour4: "#b7bc31",
	colourRange: d3.scale.category20(),
	
	headerOptions: {
			"header-sc" : {
				filename: "/shared/html-snippets/header_sc.html",
				fields: [
				     {fieldId: "platforms-field", elementsId: ["platforms"]},
				     {fieldId: "locations-field", elementsId: ["locations"]},
				     {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				     {fieldId: "gamers-field", elementsId: ["gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				],
				storeName: "header-sc",
				getParamValues: getSCParamValues,
				height: 175, //px
			},
			"header-sc-tableau" : {
				filename: "/shared/html-snippets/header_sc_tableau.html",
				fields: [
				     {fieldId: "rosplatforms-field", elementsId: ["rosplatforms"]},
				     {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				],
				storeName: "header-sc",
				getParamValues: getSCTableauParamValues,
				height: 175, //px
			},
			"header-sc-freemode" : {
				filename: "/shared/html-snippets/header_sc_freemode.html",
				fields: [
				     {fieldId: "platforms-field", elementsId: ["platforms"]},
				     {fieldId: "locations-field", elementsId: ["locations"]},
				     {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				     {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				     {fieldId: "freemode-categories-field", elementsId: ["freemode-categories"]},
				     {fieldId: "freemode-ugcstatus-field", elementsId: ["freemode-ugcstatus"]},
				     {fieldId: "creators-field", elementsId: ["creators"]},
				     {fieldId: "created-dates-field", elementsId: ["created-date-from", "created-date-to", "created-utc-force"]},
				],
				storeName: "header-sc",
				getParamValues: getSCFreemodeParamValues,
				height: 270, //px
			},
			"header-sc-missions" : {
				filename: "/shared/html-snippets/header_sc_missions.html",
				fields: [
				      {fieldId: "platforms-field", elementsId: ["platforms"]},
				      {fieldId: "locations-field", elementsId: ["locations"]},
				      {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				      {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				      {fieldId: "game-types-field", elementsId: ["game-types"]},
				      {fieldId: "date-grouping-field", elementsId: ["date-grouping"]},
				      {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				      {fieldId: "freemode-category-field", elementsId: ["freemode-category"]},
				      {fieldId: "freemode-ugcstatus-field", elementsId: ["freemode-ugcstatus"]},
				      {fieldId: "missions-field", elementsId: ["missions"]},
				      {fieldId: "created-dates-field", elementsId: ["created-date-from", "created-date-to", "created-utc-force"]},
				],
				getParamValues: getSCMissionsParamValues,
				storeName: "header-sc",
				height: 270, // px
			},
			"header-sc-map" : {
				filename: "/shared/html-snippets/header_sc_map.html",
				fields: [
				    {fieldId: "platforms-field", elementsId: ["platforms"]},
				    {fieldId: "locations-field", elementsId: ["locations"]},
				    {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				    {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				    {fieldId: "game-types-field", elementsId: ["game-types"]},
				    {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				    {fieldId: "deathmatch-field", elementsId: ["deathmatch"]},
				    {fieldId: "spawn-time-field", elementsId: ["time-since-spawn"]},
				],
				storeName: "header-sc",
				getParamValues: getSCMapParamValues,
				height: 210, // px
			},
			"header-sc-fps" : {
				filename: "/shared/html-snippets/header_sc_fps.html",
				fields: [
				    {fieldId: "platforms-field", elementsId: ["platforms"]},
				    {fieldId: "locations-field", elementsId: ["locations"]},
				    {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				    {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				    {fieldId: "game-types-field", elementsId: ["game-types"]},
				    {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				    {fieldId: "level-field", elementsId: ["level"]},
				    {fieldId: "match-type-field", elementsId: ["match-type"]},
				    {fieldId: "resolution-field", elementsId: ["resolution"]},
				    {fieldId: "freeroam-field", elementsId: ["freeroam"]},
				],
				storeName: "header-sc",
				getParamValues: getSCFpsParamValues,
				height: 210, // px
			},
			"header-sc-weapons" : {
				filename: "/shared/html-snippets/header_sc_weapons.html",
				fields: [
				    {fieldId: "platforms-field", elementsId: ["platforms"]},
				    {fieldId: "locations-field", elementsId: ["locations"]},
				    {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				    {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				    {fieldId: "game-types-field", elementsId: ["game-types"]},
				    {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				    {fieldId: "match-type-field", elementsId: ["match-type"]},
				    {fieldId: "freeroam-field", elementsId: ["freeroam"]},
				],
				storeName: "header-sc",
				getParamValues: getSCWeaponsParamValues,
				height: 190, // px
			},			
			"header-sc-dategrouping" : {
				filename: "/shared/html-snippets/header_sc_dategrouping.html",
				fields: [
				     {fieldId: "platforms-field", elementsId: ["platforms"]},
				     {fieldId: "locations-field", elementsId: ["locations"]},
				     {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				     {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "date-grouping-field", elementsId: ["date-grouping"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				],
				storeName: "header-sc",
				getParamValues: getSCDGParamValues,
				height: 175, //px
			},
			"header-sc-debug" : {
				filename: "/shared/html-snippets/header_sc_debug.html",
				fields: [
				     {fieldId: "platforms-field", elementsId: ["platforms"]},
				     {fieldId: "locations-field", elementsId: ["locations"]},
				     {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				     {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "debug-field", elementsId: ["include-debug"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				],
				storeName: "header-sc",
				getParamValues: getSCDebugParamValues,
				height: 175, //px
			},
			"header-sc-topusers" : {
				filename: "/shared/html-snippets/header_sc_topusers.html",
				fields: [
				     {fieldId: "platforms-field", elementsId: ["platforms"]},
				     {fieldId: "locations-field", elementsId: ["locations"]},
				     {fieldId: "age-field", elementsId: ["age-min", "age-max"]},
				     {fieldId: "gamers-field", elementsId: ["sc-users", "sc-user-groups", "gamertags", "gamertag-groups"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "upper-xp-field", elementsId: ["upper-xp"]},
				     {fieldId: "dates-builds-field", elementsId: ["dates-builds", "date-from", "date-to", "utc-force", "builds", "cwp"]},
				],
				storeName: "header-sc",
				getParamValues: getSCTopUsersParamValues,
				height: 175, //px
			},
			"header-freemode" : {
				filename: "/shared/html-snippets/header_freemode.html",
				fields: [
				     {fieldId: "freemode-categories-field", elementsId: ["freemode-categories"]},
				     {fieldId: "freemode-ugcstatus-field", elementsId: ["freemode-ugcstatus"]},
				     {fieldId: "creators-field", elementsId: ["creators"]},
				     {fieldId: "flags-field", elementsId: ["latest-versions-only", "include-deleted"]},
				     {fieldId: "created-dates-field", elementsId: ["created-date-from", "created-date-to", "created-utc-force"]},
				],
				storeName: "header-sc",
				getParamValues: getFreemodeParamValues,
				height: 175, //px
			},
			"header-cs" : {
				filename: "/shared/html-snippets/header_cs.html",
				fields: [{fieldId: "platform-field", elementsId: ["platform"]},
				         {fieldId: "build-field", elementsId: ["build"]},
				         {fieldId: "build-config-field", elementsId: ["build-config"]},
				],
				getParamValues: getCsParamValues,
				height: 175, // px
			},
			"header-cs-build-only" : {
				filename: "/shared/html-snippets/header_cs_build_only.html",
				fields: [
				         {fieldId: "build-field", elementsId: ["build"]},
				],
				getParamValues: getCsParamValues,
				height: 175, // px
			},
			"header-cs-report" : {
				filename: "/shared/html-snippets/header_cs_report.html",
				fields: [{fieldId: "platform-field", elementsId: ["platform"]},
				         {fieldId: "cutscene-field", elementsId: ["cutscene"]},
				],
				getParamValues: getCsReportParamValues,
				height: 175, // px
			},
			"header-cs-per" : {
				filename: "/shared/html-snippets/header_cs_per.html",
				fields: [{fieldId: "platform-field", elementsId: ["platform"]},
				         {fieldId: "build-field", elementsId: ["build"]},
				         {fieldId: "build-config-field", elementsId: ["build-config"]},
				         {fieldId: "percentiles-field", elementsId: ["percentile-lower", "percentile-upper"]},
				         {fieldId: "game-times-field", elementsId: ["game-times"]},
				],
				getParamValues: getCsPerParamValues,
				height: 175, // px
			},
			"header-cl" : {
				filename: "/shared/html-snippets/header_cl.html",
				fields: [{fieldId: "platform-field", elementsId: ["platform"]},
				         {fieldId: "build-field", elementsId: ["build"]},
				         {fieldId: "flags-field", elementsId: ["report-detailed", "report-detailed", "report-detailed"]},
				],
				getParamValues: getClParamValues,
				height: 175, // px
			},
			"header-map-exports" : {
				filename: "/shared/html-snippets/header_map_exports.html",
				fields: [{fieldId: "section-field", elementsId: ["section"]},
				         {fieldId: "user-field", elementsId: ["user"]},
				         {fieldId: "date-field-30pc", elementsId: ["date-from", "date-to", "utc-force"]}
				],
				height: 175, // px
				getParamValues: getMapExpParamValues,
			},
			"header-dt" : {
				filename: "/shared/html-snippets/header_dt.html",
				fields: [{fieldId: "dt-build-field", elementsId: ["dt-build"]},
				         {fieldId: "dt-platform-field", elementsId: ["dt-platform"]},
				         {fieldId: "dt-users-field", elementsId: ["dt-users"]},
				],
				getParamValues: getDTParamValues,
				height: 170, // px
			},
			"header-reports" : {
				filename: "/shared/html-snippets/header_reports.html",
				fields: [{fieldId: "parameters-field", elementsId: []},
				         {fieldId: "stylesheet-field", elementsId: []},
				],
				height: 350, // px
			},
			"header-as" : {
				filename: "/shared/html-snippets/header_as.html",
				fields: [
				     {fieldId: "platform-field", elementsId: ["platform"]},
				     {fieldId: "level-field", elementsId: ["level"]},
				     {fieldId: "file-types-field", elementsId: ["file-types"]},
				],
				getParamValues: getAsParamValues,
				height: 175, // px
			},
			"header-clips" : {
				filename: "/shared/html-snippets/header_clips.html",
				fields: [
				     {fieldId: "build-field", elementsId: ["build"]},
				     {fieldId: "builds-field", elementsId: ["builds"]},
				     {fieldId: "clip-category-field", elementsId: ["clip-category"]},
				],
				getParamValues: getClipsParamValues,
				height: 175, // px
			},
			"header-rs" : {
				filename: "/shared/html-snippets/header_rs.html",
				fields: [
				     {fieldId: "platform-field", elementsId: ["platform"]},
				     {fieldId: "file-types-field", elementsId: ["file-types"]},
				     {fieldId: "resource-names-field", elementsId: ["resource-names"]},
				     {fieldId: "report-detailed-field", elementsId: ["report-detailed"]},
				     {fieldId: "dates-field", elementsId: ["date-from", "date-to", "utc-force"]},
				],
				getParamValues: getRsParamValues,
				height: 175, // px
			},
			"header-status" : {
				filename: "/shared/html-snippets/header_status.html",
				fields: [
				     {fieldId: "rosplatforms-field", elementsId: ["rosplatforms"]},
				     {fieldId: "builds-field", elementsId: ["builds"]},
				     {fieldId: "game-types-field", elementsId: ["game-types"]},
				     {fieldId: "dates-field", elementsId: ["date-from", "date-to", "utc-force"]},
				     {fieldId: "gamer-group-field", elementsId: ["gamer-group"]},
				     {fieldId: "gamer-csv-field", elementsId: []},				     
				],
				getParamValues: getStatusParamValues,
				height: 175, // px
			},
			"header-cash-report" : {
				filename: "/shared/html-snippets/header_cash_report.html",
				fields: [
				     {fieldId: "build-field", elementsId: ["build"]},
				     {fieldId: "gamertags-field", elementsId: ["gamertags"]},
				],
				getParamValues: getCashRepParamValues,
				storeName: "header-sc",
				height: 175, // px
			},
	},
	
	styleOptions: {
		"staging" : "/shared/css/generic_rockstar_north.css",
		"test" : "/shared/css/generic_vice_city.css",
		"print" : "/shared/css/print.css",
	},
	
	noDataText: "No data returned for the specified filter options;<br/>Please refine your search criteria.",
	noDataMapText: "No data returned for the selected options!",
	noUserText: "You need to select at least one gamer!",
	
	buildAliasText: "Latest",
	buildAllText: "All",
	
	redirect: {
		from: "rsgedista2:8080",
		//from: "127.0.0.1",
		to: "rsgedista4",
		//to: "rsgedista2:8080",
	},
	
	missionHashesCSV: "/shared/data/mission_names.csv",
	vehicleHashesCSV: "/shared/data/vehicle_names.csv", // not used
	radioHashesCSV: "/shared/data/radio_names.csv",
	weaponHashesCSV: "/shared/data/weapon_names.csv",
	weaponStatnamesCSV: "/shared/data/weapon_statnames.csv",
}

function getSCParamValues() {
	// Get Dates
	var start = getDateString("date-from", $("#utc-force").is(":checked"));
	var end = getDateString("date-to", $("#utc-force").is(":checked"));
	
	// Get Builds
	var builds = getHeaderBuilds();
	
	// Get CWP
	var cwp = ($("#cwp").val()) ? Number($("#cwp").val()) : null;
	
	// If the dropdown menu is present
	if (!$("#dates-builds").parent().hasClass("hidden")) {
		if ($("#dates-builds").val() == 3) {  // CWP only
			start = null;
			end = null;
			builds = null;
		}
		if ($("#dates-builds").val() == 2) {  // Builds only
			start = null;
			end = null;
			cwp = null;
		}
		if ($("#dates-builds").val() == 1) {  // Dates only
			builds = null;
			cwp = null;
		}
	}
	
	var platforms = ($("#platforms").val()) ? $("#platforms").val().join() : "";
	
	var locations = ($("#locations").val()) ? $("#locations").val().join() : "";
	
	if (locations && !$("#locations").val()[0]) // Option All selected
		locations = null;
	
	var ageMin = ($("#age-min").val()) ? Number($("#age-min").val()) : null;
	var ageMax = ($("#age-max").val()) ? Number($("#age-max").val()) : null;

	var scUsers, gamertags = null;
	
	if ($("#sc-users-tab").css("display") != "none") {
		var scUsers = $("#sc-users").tokenInput("get");
		scUsers = scUsers.map(function(d) {return d.name}).join();
	}
	
	if ($("#sc-user-groups-tab").css("display") != "none") {
		var scUserGroups = $("#sc-user-groups").tokenInput("get");
		scUserGroups = scUserGroups.map(function(d) {return d.name}).join();
	}

	if ($("#gamertags-tab").css("display") != "none") {
		var gamertags = getGamertags();
	}
	
	if ($("#gamertag-groups-tab").css("display") != "none") {
		var gamerGroups = $("#gamertag-groups").tokenInput("get");
		gamerGroups = gamerGroups.map(function(d) {return d.name}).join();
	}
	
	var gameTypes = ($("#game-types").val()) ? $("#game-types").val().join() : "";
	//if ($("#game-types").attr("disabled") == "disabled")
	if ($("#game-types-field").hasClass("hidden"))
		gameTypes = null;
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformNames": platforms,
			"CountryCodes" : locations,
			"AgeMin" : ageMin,
			"AgeMax" : ageMax,
			"SocialClubUserNames" : scUsers,
			"SocialClubUserGroups" : scUserGroups,
			"GamerHandlePlatformPairs" : gamertags,
			"GamerGroups": gamerGroups,
			"GameTypeNames": gameTypes,
			"StartDate": start,
			"EndDate": end,
			"BuildIdentifiers": builds,
			"CompanywidePlaytestId": cwp,
			"TutorialRaceRootContentIds": ["CA69F971-6A4B-4758-A16C-E919CC7A5F73","1380A3E4-CAC1-4E2E-97E6-41462629141D","D107D492-63A3-448F-A85B-1F84D7ED8CDB","25546EDD-AE2F-4011-99DF-9C298300398D","076C2F01-7A69-492E-B849-629D1E0614E4"].join(","),
			"TutorialMissionRootContentIds": ["A155D363-7766-4CEB-A0C0-B4237128B26D","87335C78-20B7-4A1C-9EC0-AA29979E8955","0FFB3AAC-629C-434C-8388-38C8DE57E696","CA31EECE-F32E-4912-AD06-0222A86D0883","62A592DE-EAAA-4070-A697-FB991C7EFF8E"].join(","),
			"TutorialLTSMissionRootContentIds": ["8FEFDFCA-F994-41F6-9CD3-15F5F26FCF4A","8DC093E7-13F6-47DB-87C7-0DF201D0B9D3"].join(","),
		}
	}
	
	return pValues;
}

function getSCFreemodeParamValues() {
	
	var categories = ($("#freemode-categories").val()) ? $("#freemode-categories").val().join(",") : "";
	var ugcStatus = ($("#freemode-ugcstatus").val()) ? $("#freemode-ugcstatus").val() : null;
	
	var creators = getCreators();
	
	var createdStart = getDateString("created-date-from", $("#created-utc-force").is(":checked"));
	var createdEnd = getDateString("created-date-to", $("#created-utc-force").is(":checked"));
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	pValues.Pairs["MissionCategoryNames"] = categories;
	pValues.Pairs["MissionPublishedOnly"] = ugcStatus;
	pValues.Pairs["MissionCreatorGamerHandlePlatformPairs"] = creators;
	pValues.Pairs["MissionCreatedStart"] = createdStart;
	pValues.Pairs["MissionCreatedEnd"] = createdEnd;
	
	return pValues;
}

function getSCMissionsParamValues() {
	
	var dateGrouping = ($("#date-grouping").val());
	var category = ($("#freemode-category").val()) ? $("#freemode-category").val() : null;
	
	var matchIdentifiers = $("#missions").tokenInput("get");
	matchIdentifiers = matchIdentifiers.map(function(d) {return d.id}).join();
	
	var pValues = getSCFreemodeParamValues(); //same as freemode except the creators
	
	pValues.Pairs["Grouping"] = dateGrouping;
	pValues.Pairs["MissionCategoryNames"] = category;
	pValues.Pairs["UGCMatchIdentifiers"] = matchIdentifiers;
	
	return pValues;
}

function getFreemodeParamValues() {
	var categories = ($("#freemode-categories").val()) ? $("#freemode-categories").val().join(",") : "";
	var ugcStatus = ($("#freemode-ugcstatus").val()) ? $("#freemode-ugcstatus").val() : null;
	
	var creators = getCreators();
	
	var createdStart = getDateString("created-date-from", $("#created-utc-force").is(":checked"));
	var createdEnd = getDateString("created-date-to", $("#created-utc-force").is(":checked"));
	
	var latestVersionsOnly = ($("#latest-versions-only").is(":checked"));
	var includeDeleted = ($("#include-deleted").is(":checked"));
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"MissionCategoryNames": categories,
			"MissionPublishedOnly" : ugcStatus,
			"MissionCreatorGamerHandlePlatformPairs" : creators,
			"MissionCreatedStart" : createdStart,
			"MissionCreatedEnd" : createdEnd,
			"LatestVersionsOnly" : latestVersionsOnly,
			"IncludeDeleted" : includeDeleted,
		}
	}
	
	return pValues;
}

function getSCMapParamValues() {
	var radius = 2;
	
	var timeSinceSpawn = ($("input#time-since-spawn").val()) ? $("input#time-since-spawn").val() : 10;
	
	var deathmatchUGC = $("#deathmatch").tokenInput("get");
	deathmatchUGC = deathmatchUGC.map(function(d) {return d.id}).join();

	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	// Add the extra Spaw Points param values
	pValues.Pairs["DeathmatchUGCIdentifier"] = deathmatchUGC;
	pValues.Pairs["BlockSize"] = radius;
	pValues.Pairs["TimeSinceSpawn"] = Number(timeSinceSpawn);
	
	return pValues;
}

function getSCWeaponsParamValues() {
	
	var matchType = ($("#match-type").val()) ? $("#match-type").val() : null;
	var freeroam = ($("#freeroam").is(":checked"));

	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	// Add the extra param values
	pValues.Pairs["MatchTypeNames"] = matchType;
	pValues.Pairs["FreeroamOnly"] = freeroam;
	
	return pValues;
}

function getSCDGParamValues() {
	var dateGrouping = ($("#date-grouping").val());
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	pValues.Pairs["Grouping"] = dateGrouping;
	
	return pValues;
}

function getSCDebugParamValues() {
	var includeDebug = ($("#include-debug").is(":checked"));
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	pValues.Pairs["IncludeDebug"] = includeDebug;
	
	return pValues;
}

function getSCFpsParamValues() {
	
	var level = ($("#level").val()) ? $("#level").val() : null;
	var matchType = ($("#match-type").val()) ? $("#match-type").val() : null;
	var resolution = ($("#reoslution").val()) ? $("#resolution").val() : 100;
	var freeroam = ($("#freeroam").is(":checked"));

	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	// Add the extra Spaw Points param values
	pValues.Pairs["LevelIdentifier"] = level;
	pValues.Pairs["MatchTypeNames"] = matchType;
	pValues.Pairs["Resolution"] = resolution;
	pValues.Pairs["FreeroamOnly"] = freeroam;
	
	return pValues;
}

function getSCTopUsersParamValues() {
	var upperXP = ($("#upper-xp").val() ? $("#upper-xp").val() : 0);
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	pValues.Pairs["UpperXPLimit"] = upperXP;
	
	return pValues;
}

function getSCTableauParamValues() {
	var platforms = ($("#rosplatforms").val()) ? $("#rosplatforms").val().join() : "";
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = getSCParamValues();
	
	pValues.Pairs["PlatformNames"] = platforms;
	
	return pValues;
}

function getCsParamValues() {
	var platform = ($("#platform").val()) ? $("#platform").val() : "";
	var build = getHeaderBuild();
	var buildConfig = ($("#build-config").val()) ? $("#build-config").val() : "";
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformName": platform,
			"BuildIdentifier": build,
			"BuildConfigName": buildConfig,
		},
	}
	
	return pValues;
}

function getCsReportParamValues() {
	var platform = ($("#platform").val()) ? $("#platform").val() : "";
	var cutscene = ($("#cutscene").val()) ? $("#cutscene").val() : "";
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformName": platform,
			"CutsceneName": cutscene,
		},
	}
	
	return pValues;
}

function getCsPerParamValues() { // With Percentiles
	var lowerPer = ($("#percentile-lower").val()) ? $("#percentile-lower").val() : "0";
	var upperPer = ($("#percentile-upper").val()) ? $("#percentile-upper").val() : "100";
	
	var gameTimes = ($("#game-times").val()) ? $("#game-times").val() : config.gameTimes.map(function(d) {return d.Value});	
	
	var pValues = getCsParamValues();
	pValues.Pairs["LowerPercentileClamp"] = Number(lowerPer);
	pValues.Pairs["UpperPercentileClamp"] = Number(upperPer);
	// Get the first range in the array, split by - and get the first time number
	pValues.Pairs["StartHour"] = Number(gameTimes[0].split("-")[0]);
	// Get the last range in the array, split by - and get the last time number
	pValues.Pairs["EndHour"] = Number(gameTimes.slice(-1)[0].split("-").slice(-1)[0]);
		
	return pValues;
}

function getClParamValues() {
	var platform = ($("#platform").val()) ? $("#platform").val() : "";
	//var build = ($("#build").val()) ? $("#build").val() : "";
	var build = getHeaderBuild();
	var depth = ($("#depth-active").is(":checked"));
	var camera = ($("#camera-approved").is(":checked"));
	var lighting = ($("#lighting-approved").is(":checked"));
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformName": platform,
			"BuildIdentifier": build,
			"DepthOfFieldActive": depth,
			"CameraApproved": camera,
			"LightingApproved": lighting,
		},
	}
	
	return pValues;
}

function getAsParamValues() {
	var platform = ($("#platform").val()) ? $("#platform").val() : "";
	var level = ($("#level").val()) ? $("#level").val() : "";
	var fileTypes = ($("#file-types").val()) ? $("#file-types").val().join(",") : "";
		
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformName": platform,
			"LevelIdentifier": level,
			"FileTypeNames": fileTypes,
		},
	}
	
	return pValues;
}

function getRsParamValues() {
	var platform = ($("#platform").val()) ? $("#platform").val() : "";
	var fileTypes = ($("#file-types").val()) ? $("#file-types").val().join(",") : "";
	var resources = $("#resource-names").val();
	var detailed = ($("#report-detailed").is(":checked"));
	var start, end;
	
	if ($("#utc-force").is(":checked")) { // UTC input forced
		start = new Date(config.dateInputUTCFormat.parse($("input#date-from").val())).toISOString();
		end = new Date(config.dateInputUTCFormat.parse($("input#date-to").val())).toISOString();
	}
	else { // Treat input as local time
		start = new Date(config.dateInputFormat.parse($("input#date-from").val())).toISOString();
		end = new Date(config.dateInputFormat.parse($("input#date-to").val())).toISOString();
	}
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformName": platform,
			"FileTypeNames": fileTypes,
			"ResourceNames": resources,
			"ReportDetail": detailed,
			"StartDate": start,
			"EndDate": end,
		},
	};
	
	return pValues;
}

function getStatusParamValues() {
	var platforms = ($("#rosplatforms").val()) ? $("#rosplatforms").val().join() : "";
	var builds = getHeaderBuilds();
	var gameTypes = ($("#game-types").val()) ? $("#game-types").val().join() : "";
	
	var start = getDateString("date-from", $("#utc-force").is(":checked"));
	var end = getDateString("date-to", $("#utc-force").is(":checked"));
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformNames": platforms,
			"BuildIdentifiers": builds,
			"GameTypeNames": gameTypes,
			"StartDate": start,
			"EndDate": end,
		},
	};
	
	return pValues;
}


function getClipsParamValues() {
	//var build = ($("#build").val()) ? $("#build").val() : "";
	var build = getHeaderBuild();
	//var builds = ($("#builds").val()) ? $("#builds").val().join(",") : "";
	var builds = getHeaderBuilds();
	var clipCategory = ($("#use-clip-category").is(":checked")) ? $("#clip-category").val() : null;
		
	if (!builds)
		builds = build;
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"BuildIdentifier": build,
			"SourceBuildIdentifier": build,
			"DataBuildIdentifiers": builds,
			"ClipDictionaryCategoryName": clipCategory,
		},
	}
	
	return pValues;
}

/*
function getBettingParamValues() {
	// Get Dates
	var start = getDateString("date-from", $("#utc-force").is(":checked"));
	var end = getDateString("date-to", $("#utc-force").is(":checked"));
	
	var platforms = ($("#platforms").val()) ? $("#platforms").val().join() : "";
	var dateGrouping = ($("#date-grouping").val());


	if ($("#gamertags-tab").css("display") != "none") {
		var gamertags = getGamertags();
	}
	
	if ($("#gamertag-groups-tab").css("display") != "none") {
		var gamerGroups = $("#gamertag-groups").tokenInput("get");
		gamerGroups = gamerGroups.map(function(d) {return d.name}).join();
	}
	
	var matchIdentifiers = $("#missions").tokenInput("get");
	matchIdentifiers = matchIdentifiers.map(function(d) {return d.id}).join();
			
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"PlatformNames": platforms,
			"GamerHandlePlatformPairs" : gamertags,
			"GamerGroups" : gamerGroups,
			"StartDate": start,
			"EndDate": end,
			"Grouping": dateGrouping,
			"UGCMatchIdentifiers": matchIdentifiers,
		}
	}
	
	return pValues;
}
*/

function getCashRepParamValues() {
	//var build = ($("#build").val()) ? $("#build").val() : null;
	var build = getHeaderBuild();
	
	var gamertags = getGamertags();
	
	if (gamertags.length == 0) {
		Sexy.alert(config.noUserText);
		return;
	}
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"BuildIdentifier" : build,
			"BuildIdentifiers" : build,
			"GamerHandlePlatformPairs" : gamertags,
		}
	}
	
	return pValues;
}

function getMapExpParamValues() {
	var start, end;
	
	if ($("#utc-force").is(":checked")) { // UTC input forced
		start = new Date(config.dateInputUTCFormat.parse($("input#date-from").val())).toISOString();
		end = new Date(config.dateInputUTCFormat.parse($("input#date-to").val())).toISOString();
	}
	else { // Treat input as local time
		start = new Date(config.dateInputFormat.parse($("input#date-from").val())).toISOString();
		end = new Date(config.dateInputFormat.parse($("input#date-to").val())).toISOString();
	}
	
	//var user = $("input#user").val();
	var user = ($("input#user").val()) ? $("input#user").val() : null;
	var hourOffset = 6;
	var grouping = "DAY"; // DAY, WEEK, MONTH, YEAR
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = {
		ForceUrlSuffix: forceUrlSuffix,
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {
			"Date": start,
			"StartDate": start,
			"EndDate": end,
			"Username": user,
			"Grouping": grouping,
			"HourOffset": hourOffset,
		},
	};
	
	return pValues;
}

function getDTParamValues() {
	var MAX_BUILD_SPLIT = 5;
	//var MAX_GAMERTAGS = 10;
	
	var dt_builds = ($("#dt-build").val()) ? $("#dt-build").val().join() : "";
	var dt_platforms = ($("#dt-platform").val()) ? $("#dt-platform").val().join() : "";
	
	var dt_users = $("#dt-users").tokenInput("get");
	
	var dt_usernames = dt_users.map(function(d) {return d.name.split("(")[0]}).join();
	
	// The #dt_users select value is the gamer tag and text is the username  
	
	var dt_gamerTags = dt_users.map(function(d) {
				if (d.id)
					return new String("<" + d.id + ">");
			})
			.join(",");

	//if (!dt_gamerTags && $("#dt-platform-field").attr("disabled")) {
	if (!dt_gamerTags && $("#dt-platform-field").hasClass("hidden")) {
		// Sexy.alert("No Users selected!");
		return;
	}
	
	//if ($("#dt-build-field").attr("disabled"))
	if ($("#dt-build-field").hasClass("hidden"))
		dt_builds = null;
	//if ($("#dt-platform-field").attr("disabled"))
	if ($("#dt-platform-field").hasClass("hidden"))
		dt_platforms = null;
	
	// Not used anymore
	var exclude = ($("#exclude-user").is(":checked"));
	
	var build_split = ($("#build-split").is(":checked"));
	
	//var forceUrlSuffix = "";
	//if ($("#filter-force").is(":checked"))
	//	forceUrlSuffix = config.reportForceSuffix;
	var forceUrlSuffix = config.reportForceSuffix;
	
	if (build_split && dt_builds) {
		var pValues = [];
		
		var builds = dt_builds.split(",").slice(0, MAX_BUILD_SPLIT);
		//console.log(builds);
		// Reversing so the newest is being drawn last at the graph 
		$.each(builds.reverse(), function(i, dt_build) {
			pValues.push(
				{
					ForceUrlSuffix: forceUrlSuffix,
					ElementKeyName: "Name",
					ElementValueName: "Value",
					Pairs: {
						"BuildIdentifiers" : dt_build,
						"PlatformIdentifiers": dt_platforms,
						// 
						"PlatformNames": dt_platforms,
						"Users" : dt_usernames,
						//"ExcludeUsers" : exclude,
						"GamerHandlePlatformPairs": dt_gamerTags,
					},
				}
			);
		});
		
		return pValues;
	}
	else {
		var pValues = {
			ForceUrlSuffix: forceUrlSuffix,
			ElementKeyName: "Name",
			ElementValueName: "Value",
			Pairs: {
				"BuildIdentifiers" : dt_builds,
				"PlatformIdentifiers": dt_platforms,
				// 
				"PlatformNames": dt_platforms,
				"Users" : dt_usernames,
				//"ExcludeUsers" : exclude,
				"GamerHandlePlatformPairs": dt_gamerTags,
			},
		}
		
		return pValues;
	}
	
}

function initSvg(htmlId, height) {
	//console.log(htmlId);
	
	var svg = d3.select("#" + htmlId)
		.append("svg:svg");
		//.attr("pointer-events", "none");
		
	if (typeof height !== "undefined")
		svg.attr("height", height);		
    		
   	var dropShadowFilter = svg.append("svg:filter")
   		.attr("id", "dropShadow");
   	dropShadowFilter.append("svg:feOffset")
    	.attr("result", "offOut")
    	.attr("in", "SourceAlpha")
    	.attr('dx', 3)
    	.attr('dy', 3);
    dropShadowFilter.append("svg:feGaussianBlur")
	   	.attr("result", "blurOut")
    	.attr("in", "offOut")
    	.attr("stdDeviation", 3);
    dropShadowFilter.append("svg:feBlend")
	   	.attr("in", "SourceGraphic")
    	.attr("in2", "blurOut")
    	.attr("mode", "normal");	
}

function cleanSvg(htmlId) {
	var svg = d3.select("#" + htmlId)
		.selectAll("g")
		.transition()
			.style("opacity", 0.01)
		.remove();
}

function fadeOutGraph(htmlId) {
	d3.select("#" + htmlId + " svg")
		.selectAll("g")
		.transition()
			.duration(config.transitionDuration)
			.style("opacity", 0.01)
			.remove();
}

function sortByIdentifierAsc(s1, s2){
	return s1.Identifier > s2.Identifier ? 1 : -1;
};

function sortByNameAsc(s1, s2){
	return s1.Name > s2.Name ? 1 : -1;
};

function sortByIdentifierDesc(s1, s2){
	return s1.Identifier > s2.Identifier ? -1 : 1;
};

function block() {
	$.blockUI({ 
		message: '<h1><img src="../images/ajax_load_black-orange.gif" />&nbsp;&nbsp;Please Wait ...</h1>',
		centerY: 0,
		css: { 
			border: 'none', 
			padding: '0.5em', 
			backgroundColor: '#000',
			color: '#fcb131',
			'border-radius': '5px',
			opacity: 0.5,
			top: '1em', 
			left: '',
			right: '1em',
			border: 'none',
			baseZ: 2000,
		}
	});	
}

function blockDt() {
	block();
}

/*
function blockDt() {
	$.blockUI({ 
		message: '<div><img src="../images/ajax_load_gray-orange.gif" />&nbsp;&nbsp;Please Wait ...</div>',
		centerY: 0,
		css: {
			'position': 'fixed',
			'opacity': '0.7',
			'padding': '0.5em',
			'background-color': '#555555',
			'color': '#fcb131',
	
			'border-radius': '5px',

			'top': '40%',
			'left': '50%',
			'margin-top': '-1.25em',
			'margin-left': '-10em',

			'width': '20em',
			'height': '2.5em',
			'z-index': '1500',
			
			
			'font-family': '"Calibri", sans-serif',
			'font-size': '20px',
			'font-weight': 'bold',
			
			'border': '1px solid rgba(255, 255, 255, 0.7)',
			
			//baseZ: 2000,
		}
	});
}
*/

function unBlock() {
	$.unblockUI();
}

function blockUIOnly() {
	$.blockUI({ message: null, css:{backgroundColor: "#000", opacity: 0.5} });
}

function showProgressbar() {
	$("#progress-bar").progressbar({value: 0});
	$("#loading").fadeIn("normal");
	//$.blockUI({ message: null, css:{backgroundColor: "#000", opacity: 0.5} });
	blockUIOnly();
}

function hideProgressbar() {
	$("#loading").fadeOut("fast");
	$.unblockUI();
}

function updateProgressbar(value) {
	$("#progress-bar").progressbar({value: value});
	$("#progress-percent").text((value).toFixed(2) + "%");
}

function init() {
	try {
		checkRedirection();
	
		// Add the project name and logo
		//$("head title").text(project.name + $("head title").text());
		//$("#frontpage-title").text(project.fullName + $("#frontpage-title").text());
		//$("#frontpage-title-alt").text(project.fullName + $("#frontpage-title-alt").text());
		/*
		$("#frontpage-logo").append(
			$("<img />")
				.attr("src", project.logo.src)
				.attr("title", project.logo.title)
		);
		*/
	
		/*
		$("body").append("<div id='loading'>Please Wait ... " +
						"<div id='progress-bar'></div>" +
						"<div id='progress-info'>Progress : <span id='progress-percent'></span><div>" +
						"<div class='cancel'>" +
							//"<label for='cancel-request' title='Check to Cancel'>Cancel </label>" +
							"<input type='checkbox' id='cancel-request' class='hidden' />" +
						"</div>" + 
					"</div>");
		*/
		$("head")
			.append(
				$("<meta>")
					.attr("http-equiv", "cache-control")
					.attr("content", "no-cache")
			)
		
		$("body")
			.append(
				$("<div />")
					.attr("id", "loading")
					.text("Please Wait ... ")
					.append(
						$("<div />")
							.attr("id", "progress-bar")
					)
					.append(
						$("<div />")
							.attr("id", "progress-info")
							.html("Progress : <span id='progress-percent'></span>")
					)
					.append(
						$("<div />")
							.addClass("cancel")
							.html("<input type='checkbox' id='cancel-request' class='hidden' />")
							.attr("title", "Cancel")
							.click(function() { $("#cancel-request").prop("checked", true); })
					)
			);		
		
	
		$("#progress-bar").progressbar({value: 0});
	
		// 	Populate the navigation menu from the config javascript object
		populateNavigation(navigationItems);
		
		initContent();
		
		// Load antiscroll
		getScriptSync(config.webHost + "/libs/antiscroll/antiscroll.js")
		$(function () {
	        $(window).on("load resize", function() {
	        	$("#nav-ul-wrap").css({
	        		height: $(window).height() - $("#nav-logo").height(),
	        	});
	        }).resize();

	        var scroller = $("#nav-ul-wrap").antiscroll().data("antiscroll");

	        $(window).resize(function() {        	
	        	scroller.refresh();
	        });
	    });
	
		if (isPrint()) {
			loadAlternativeCss("print");
		}
		else
			loadAlternativeCss();
	}
	catch(e) {
		console.error(e);
	}
	finally {
		//checkSupportedBrowser();
		// For betting header
	}
}

function isPrint() {
	return getUrlVars()["print"];
}

function checkRedirection() {
	if (config.redirect.from == window.location.host) {
		alert("You will be redirected to the new stats server. Please update your bookmarks!");
		window.location = window.location.protocol + "//" + config.redirect.to + window.location.pathname;
	}
}

function populateNavigation(navigationItems) {
	var serverType = getServerType();

	$("#navigation")
		//.addClass("antiscroll-wrap")
		.append(
			$("<div>")
				.attr("id", "nav-logo")
					.append(
						$("<img />")
							.attr("src", project.logo.src)
							.attr("title", project.logo.title)
					)
		)
		.append(
			$("<div />")
				.addClass("antiscroll-wrap")
				.append(
					$("<div />")
						.attr("id", "nav-ul-wrap")
						//.addClass("antiscroll-inner")
						.append(
							$("<ul />")
								.addClass("antiscroll-inner")
								.attr("id", "nav-ul")
					)
					
				)
		);

	$.each(navigationItems, function(i, navItem) {
	
		$("<li />")
			.css("display", function() {return ((navItem.hide) ? "none" : "inherit") })
			.append(
				$("<a />")
					.addClass(function() {return (navItem.href == config.currentFilename) ? "active" : ""; })
					.addClass(function() {return (!navItem.subItems.length) ? "noChildren" : ""; })
					.text(navItem.title)
					.attr("href", navItem.href)
					.attr("title", navItem.title)
			)
			.append($("<ul />")
					.attr("id", i)
					.css("display", "none"))
			.appendTo("#nav-ul");
	
		$.each(navItem.subItems, function(j, subItem) {
			currentUL = $("#"+i);
					
			$("<li />")
				.css("display", subItem.display)
				.append(
					$("<a />")
						.addClass(function() {return (subItem.href == config.currentFilename) ? "active" : ""; })
						.addClass(function() {return (!navItem.subItems.length) ? "noChildren" : ""; })
						.text(subItem.title)
						.attr("href", subItem.href)
						.attr("title", subItem.title)
				)
				.append($("<ul />")
					.attr("id", i + "_" +j)
					.css("display", "none"))
				.appendTo(currentUL);
	
			if (subItem.href == config.currentFilename) {

				currentUL.parent().children("a").addClass("active");
				currentUL.parent().addClass("expanded");
				currentUL.css("display", "inherit");
			}
			
			
			if (typeof subItem.subItems === "undefined") return;
			$.each(subItem.subItems, function(k, subSubItem) {
				var currentSubUL = $("#"+ i + "_" + j);
						
				$("<li />")
					.css("display", subSubItem.display)
					.append(
						$("<a />")
							.addClass(function() {if (subSubItem.href == config.currentFilename) return "active"; })
							.text(subSubItem.title)
							.attr("href", subSubItem.href)
							.attr("title", subSubItem.title)
					)
					.appendTo(currentSubUL);

				if (subSubItem.href == config.currentFilename) {
					currentSubUL.parent().children("a").addClass("active");
					currentSubUL.parent().addClass("expanded");
					currentSubUL.parent().parent().parent().children("a").addClass("active");
					currentSubUL.parent().parent().parent().addClass("expanded");
					currentSubUL.css("display", "inherit");
					currentSubUL.parent().parent().css("display", "inherit");
				}
				
			});
		});
	});

	$("#navigation").find("a").click(function(e) {
		if ($(this).attr("href") == "#") {
			e.preventDefault();
	 
			$(this).next().slideToggle();
		}
	
		if ($(this).parent().hasClass("expanded")) // find the parent ul
			$(this).parent().removeClass("expanded");
		else
			$(this).parent().addClass("expanded");
	}); // End of click
	
	
}

function initContent() {
	// Temp hack until all the pages adopt the new style
	if (($("#content").find("#map-wrapper").length > 0) ||
			($("#content").find("#report").length > 0))
		return;
	
	if ($("#content").find("#content-title").length == 0) {
		$("#content")
			.append(
				$("<div>").attr("id", "content-title")
			)
			.append(
				$("<div>").attr("id", "content-description")
			)
			.append(
				$("<div>").attr("id", "content-body")
			);
	}
	
	// Adjust the height of the content automatically
	$("#content").height($(window).height() - $("#header").height() - 50); // 20 is the padding top/bottom
	
	$(window).resize(function() {
		$("#content").height($(window).height() - $("#header").height() - 50); // 20 is the padding top/bottom
	});
	
	updateTitle();
}

function updateTitle() {
	$("head title").text("");
	$("#content-title").text("");
	
	var titleTokens = [project.name, navigationTitle]; 
	$("#navigation").find(".active").each(function(i, d){
		titleTokens.push($(d).attr("title")); 
	});
	//console.log(titleTokens);
	$("head title").text(titleTokens.join(" : "));
	
	var html = titleTokens.slice(0, -1).join(" : ")
	html += (" : <span class='last'>" +  titleTokens.slice(-1)[0] + "</span>");
	$("#content-title").html(html);
	
	//$("#frontpage-title").text([$("#frontpage-title").text(), navigationTitle].join(" : "));
	//$("#frontpage-title-alt").text([$("#frontpage-title-alt").text(), navigationTitle].join(" : "));
}

function emptyNavigation() {
	$("#navigation").empty();
}

function removeNavigation() {
	$("#nav-ul").remove();
}

function initAndCall(callback) {
	init();
	// call the callback function when everything is finished
	callback();
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, 
    	function(m, key, value) {
       		vars[key] = value;
    });
    return vars;
}

function loadAlternativeCss(cssType) {
	if (!cssType)
		cssType = getServerType();
	
	if (config.styleOptions.hasOwnProperty(cssType)) {
		$("head")
			.append(
				$("<link />")
					.attr({
						rel: "stylesheet",
						type: "text/css",
						href: config.styleOptions[cssType]
					})
			);
	}	
}

function getServerType() {
	if (!config.serverType) {
		// Load the script code synchronously
		$.ajaxSetup({async: false});
	
		$.getScript(config.serverTypeScript)
			.done(function(script, textStatus) {
				config.serverType = serverType;
			});
	
		$.ajaxSetup({async: true});
	}
	return config.serverType;
}

function getScriptSync(url) {
	// Load the script code synchronously
	$.ajaxSetup({async: false});
	
	$.getScript(url)
		.done(function(script, textStatus) {});
	$.ajaxSetup({async: true});
};


function storeLocalObject(key, obj) {
	//Stores the JavaScript object as a string
	//console.log(JSON.stringify(obj));
	localStorage.setItem(key, JSON.stringify(obj));
	//console.log("stored : " + key + " - " + JSON.stringify(obj));
}

function retrieveLocalObject(key) {
	// Parses the saved string into a JavaScript object again
	//console.log("retrieved : " + key + " - " + localStorage.getItem(key));
	return JSON.parse(localStorage.getItem(key));
}

//Available filters is an array of boolean values provided by each page's config file 
function initHeaderAndFilters(headerAndFilters) {
	if (!headerAndFilters) {
		$("#header").css("display", "none");
	}
	else {
		// Load the header html code synchronously
		jQuery.ajaxSetup({async: false});
		$("#header").load(config.headerOptions[headerAndFilters.headerType].filename);
		jQuery.ajaxSetup({async: true});
		
		$("#filter").addClass("red-button hand"); // apply class to the button
		
		// Set the header height dynamically
		$("#header").height(config.headerOptions[headerAndFilters.headerType].height);
		
		// Sync nav logo's height with the header
		$("#nav-logo").height(config.headerOptions[headerAndFilters.headerType].height);
	
		// Disable/Enable the header fields base on each page's given options
		$.each(headerAndFilters.disabledFields, function(i, filterBoolean) {
			if (typeof filterBoolean === "boolean") {
				$("#" + config.headerOptions[headerAndFilters.headerType].fields[i].fieldId)
					//.attr("disabled", filterBoolean)
					//.addClass((filterBoolean) ? "disabled" : "");
					.addClass((filterBoolean) ? "hidden" : "");
			}
			else { // filterboolean is array - must match the length of the sub fields
				$("#" + config.headerOptions[headerAndFilters.headerType].fields[i].fieldId).children().each(function(j, e) {
					$(e).attr("disabled", filterBoolean[j])
						//.addClass((filterBoolean[j]) ? "disabled" : "");
						.addClass((filterBoolean[j]) ? "hidden" : "");
				});
				/*
				$.each(config.headerOptions[headerAndFilters.headerType].fields[i].elementsId, function(j, elementId) {
					$("#" + elementId)
						.attr("disabled", filterBoolean[j])
						.addClass((filterBoolean[j]) ? "disabled" : "");
				});
				*/
			}
			
		});
	
		populateHeader(headerAndFilters);
	}
	
	/*
	$("#navigation").append(
		"<div id='nav-toggle'><a href='#' title='Toggle Navigation Menu'></a></div>"
	);
	$("#header").append(
		"<div id='header-toggle'><a href='#' title='Toggle Options Header'></a></div>"
	);
	*/
		
	// Show/Hide the header/nav according to local settings and keep the object in a temp var
	var localHeaderNavSettings = retrieveAndSetHeaderAndNavSettings(headerAndFilters);

	var windowEventTimeout = 500;
	$("#header-toggle").click(function(e) {
		e.preventDefault();
		
		// If the collapse image clicked (arrow up), increase content and nav
		if ($("#header-toggle").hasClass("toggle-arrow-up")) {
			hideHeader(animate = true);
			localHeaderNavSettings.header = false;
		}
		// Else revert to normal heights/positions 
		else {
			showHeader(animate = true);
			localHeaderNavSettings.header = true;
		}
		
		storeLocalHeaderAndNavSettings(localHeaderNavSettings);
		
		setTimeout("$(window).resize()", windowEventTimeout);
	});
		
	$("#nav-toggle").click(function(e) {
		e.preventDefault();
		//$("#navigation").toggle("slide", { direction: "left" }, "slow");
		
		if ($("#nav-toggle").hasClass("toggle-arrow-left")) {
			hideNav(animate = true);
			localHeaderNavSettings.nav = false;
		}
		else {
			showNav(animate = true);
			localHeaderNavSettings.nav = true;
		}
		
		storeLocalHeaderAndNavSettings(localHeaderNavSettings);
		
		setTimeout("$(window).resize()", windowEventTimeout);
	});
	
	if (isPrint()) {
		headerAndFilters = null;
		hideHeader();
		hideNav();
	}
	
}

function hideHeader(animate) {
	if (typeof headerAndFilters === "undefined" || headerAndFilters == null)
		return;
	
	$("#header-toggle").removeClass("toggle-arrow-up").addClass("toggle-arrow-down");
	if (animate) {
		$("#header").animate({
			top: (-1*config.headerOptions[headerAndFilters.headerType].height)
			}, "slow");
		$("#content").animate({top: (headerNavProperties.headerBottomMargin)}, "slow");
	}
	else {
		$("#header").css({
			top: (-1*config.headerOptions[headerAndFilters.headerType].height)
			});
		$("#content").css({top: (headerNavProperties.headerBottomMargin)});
	}
	$("#content").css({
		height: $(window).height() - (2*headerNavProperties.headerBottomMargin)
	});
}
function showHeader(animate) {
	$("#header-toggle").removeClass("toggle-arrow-down").addClass("toggle-arrow-up");
	if (animate) {
		$("#header").animate({top: headerNavProperties.headerShowTop}, "slow");
		$("#content").animate({
				top: (config.headerOptions[headerAndFilters.headerType].height + headerNavProperties.headerBottomMargin) 
			}, "slow");
	}
	else {
		$("#header").css({top: headerNavProperties.headerShowTop});
		$("#content").animate({
				top: (config.headerOptions[headerAndFilters.headerType].height + headerNavProperties.headerBottomMargin)
			});
	}
	$("#content").css({height: $(window).height() 
		- (config.headerOptions[headerAndFilters.headerType].height + 2*headerNavProperties.headerBottomMargin)
	});
}
function hideNav(animate) {
	$("#nav-toggle").removeClass("toggle-arrow-left").addClass("toggle-arrow-right");
	if (animate) {
		$("#navigation").animate({left: headerNavProperties.navHideLeft}, "slow");
		$("#header").animate({
							left: headerNavProperties.headerNavHideLeft,
							width: headerNavProperties.headerNavHideWidth
							}, "slow");
		$("#content").animate({
							left: headerNavProperties.contentNavHideLeft,
							width: headerNavProperties.contentNavHideWidth
							}, "slow");
	}
	else {
		$("#navigation").css({left: headerNavProperties.navHideLeft});
		$("#header").css({
							left: headerNavProperties.headerNavHideLeft,
							width: headerNavProperties.headerNavHideWidth
							});
		$("#content").css({
							left: headerNavProperties.contentNavHideLeft,
							width: headerNavProperties.contentNavHideWidth
							});
	}
}
function showNav(animate) {
	$("#nav-toggle").removeClass("toggle-arrow-right").addClass("toggle-arrow-left");
	if (animate) {
		$("#navigation").animate({left: headerNavProperties.navShowLeft}, "slow");
		$("#header").animate({
							left: headerNavProperties.headerNavShowLeft,
							width: headerNavProperties.headerNavShowWidth
							}, "slow");
		$("#content").animate({
							left: headerNavProperties.contentNavShowLeft,
							width: headerNavProperties.contentNavShowWidth
							}, "slow");
	}
	else {
		$("#navigation").css({left: headerNavProperties.navShowLeft});
		$("#header").css({
							left: headerNavProperties.headerNavShowLeft,
							width: headerNavProperties.headerNavShowWidth
							});
		$("#content").css({
							left: headerNavProperties.contentNavShowLeft,
							width: headerNavProperties.contentNavShowWidth
							});
	}
		
}

function populateHeader(headerAndFilters) {
	//block();
	$.each(config.headerOptions[headerAndFilters.headerType].fields, function(i, field) {
		if (field.fieldId == "builds-field")
			populateAllBuilds({elementId: "builds", async: false});
		if (field.fieldId == "build-field")
			populateAllBuilds({elementId: "build", async: false});
		else if (field.fieldId == "dates-builds-field") {
			populateAllBuilds({elementId: "builds", async: false});
			populateDatesBuildsOptions("dates-builds");
		}
		else if (field.fieldId == "build-config-field")
			populateBuildConfigs();
		else if ((field.fieldId == "platforms-field") || (field.fieldId == "platform-field"))
			populatePlatforms();
		else if ((field.fieldId == "rosplatforms-field") || (field.fieldId == "rosplatform-field"))
			populateROSPlatforms();
		else if (field.fieldId == "level-field")
			populateLevels();
		else if (field.fieldId == "resolution-field")
			populateResolutions();
		else if (field.fieldId == "locations-field")
			populateLocations();
		else if (field.fieldId == "gamers-field") {
			var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
						config.headerOptions[headerAndFilters.headerType].storeName 
						: headerAndFilters.headerType;
			var localData = retrieveLocalObject(config.restHost + headerStoreName);

			tabularise("gamers-field", (localData) ? localData["gamers-field"] : null);
			populateSCUsers();
			populateSCUserGroups();
			populateGamertags();
			populateGamertagGroups();
		}
		else if (field.fieldId == "gamers-betting-field") {
			var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
						config.headerOptions[headerAndFilters.headerType].storeName 
						: headerAndFilters.headerType;
			var localData = retrieveLocalObject(config.restHost + headerStoreName);

			tabularise("gamers-betting-field", (localData) ? localData["gamers-betting-field"] : null);
			populateGamertags();
			populateGamertagGroups();
		}
		else if (field.fieldId == "gamertags-field") {
			populateGamertags();
		}
		else if (field.fieldId == "gamer-group-field") {
			populateGamerGroups();
		}
		else if (field.fieldId == "creators-field") {
			populateCreators();
		}
		else if (field.fieldId == "missions-field") {
			$("#freemode-category").change(populateMissions);
			$("#freemode-ugcstatus").change(populateMissions);
			populateMissions();
		}
		else if (field.fieldId == "deathmatch-field") {
			populateDeathmatch();
		}
		else if (field.fieldId == "match-type-field") {
			populateMatchTypes();
		}
		else if (field.fieldId == "game-types-field")
			populateGameTypes();
		else if ((field.fieldId == "freemode-categories-field") || (field.fieldId == "freemode-category-field"))
			populateFreemodeCategories();
		else if (field.fieldId == "freemode-ugcstatus-field")
			populateUGCStatus();
		else if (field.fieldId == "user-field")
			populateExportUsers();
		else if (field.fieldId == "dt-build-field")
			populatePlaythroughsBuilds();
		else if (field.fieldId == "dt-platform-field")
			populatePlaythroughsPlatforms();
		else if (field.fieldId == "dt-users-field")
			populatePlaythroughsUsers();
		else if (field.fieldId == "file-types-field")
			populateFileTypes();
		else if (field.fieldId == "game-times-field")
			populateGameTimes();
		else if (field.fieldId == "date-grouping-field")
			populateDateGrouping();
		else if (field.fieldId == "cutscene-field")
			populateCutscenes();
		else if (field.fieldId == "clip-category-field")
			populateClipCategories();
	});
	//unBlock();
	
	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);

	$("input#date-from").datetimepicker({
		dateFormat: config.datePickerFormat,
		timeFormat: config.timePickerFormat,
		maxDate: new Date(),
	});
	
	$("input#date-to").datetimepicker({
		dateFormat: config.datePickerFormat,
		timeFormat: config.timePickerFormat,
		maxDate: tomorrow,
	});
	
	//
	$("input#created-date-from").datetimepicker({
		dateFormat: config.datePickerFormat,
		timeFormat: config.timePickerFormat,
		maxDate: new Date(),
	});
	$("input#created-date-to").datetimepicker({
		dateFormat: config.datePickerFormat,
		timeFormat: config.timePickerFormat,
		maxDate: tomorrow,
	});
	
	var timezoneOffset = new Date().getTimezoneOffset()/60; // hours offset from utc
	// reverse the offset and the sign, this happens cause timezoneOffset is the hours difference 
	// form our current timezone to Utc and we need the opposite
	var timezoneString = "UTC" + ((timezoneOffset>0) ? "-" : "+") + -1*timezoneOffset; 
	$("#dates-field legend span").text("Dates (" + timezoneString + "):");
	
	retrieveAndSetLocalHeaderOptions(headerAndFilters);
	
	$("body input").focus(function() {
		$(this).select();
	});
	$("#header input").keypress(function(e) {
		if ($(this).attr("id") == "section" || 
			$(this).attr("id") == "token-input-sc-users" ||
			$(this).attr("id") == "token-input-sc-user-groups" ||
			$(this).attr("id") == "token-input-gamertags" ||
			$(this).attr("id") == "token-input-gamertag-groups" ||
			$(this).attr("id") == "token-input-dt-users" ||
			$(this).attr("id") == "token-input-missions" ||
			$(this).attr("id") == "token-input-creators")
			return;
		
		if (e.which == 13) { 
			 $("#filter").trigger("click");
		}
	});
	$("#filter")
		.click(function() {
		storeLocalHeaderOptions(headerAndFilters);
		//hideHeader(animate=true);
	});
}

function retrieveAndSetLocalHeaderOptions(headerAndFilters) {
	var yesterday = new Date();
	yesterday.setDate(yesterday.getDate() - 1);
	
	var defaultHeaderData = {
		"platform" : ["PS3"],
		"platforms" : ["PS3", "Xbox360"],
		"locations" : [],
		"age-min" : "",
		"age-max" : "",
		"sc-users" : [],
		"gamertags" : [],
		"game-type": [],
		"date-from" : $.datepicker.formatDate(config.datePickerFormat, yesterday) + " 00:00",
		"date-to" : $.datepicker.formatDate(config.datePickerFormat, new Date()) + " 00:00",
		"utc-force" : true, 
		"build" : ["-1"],
		"builds" : ["-1"],
		"cwp" : [0],
		"build-config" : ["Beta"],
		"percentile-lower" : "0",
		"percentile-upper" : "100",
		"radio-checked": "dates-field", // for default radio button selection
		"upper-xp" : 900000,
		"time-since-spawn" : 10,
		"section": "",
		"user": "",
		"time-since-spawn": "10",
		// Difficulty tracking
		"dt-build" : [],
		"dt-platform" : [],
		"dt-user" : [],
		"last_modified" : new Date(),
	};
	
	var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
			config.headerOptions[headerAndFilters.headerType].storeName 
			: headerAndFilters.headerType;
	
	var headerData = retrieveLocalObject(config.restHost + headerStoreName);
	if (headerData) {
		var lastModified = new Date(headerData.last_modified);
		// Check for expiration
		if (datesDiffInSecs(new Date(), lastModified) > config.storageExpirePeriod) {
			//console.log("storage expired");
			headerData = defaultHeaderData;
		}
	}
	else 
		headerData = defaultHeaderData;
	
	$.each(config.headerOptions[headerAndFilters.headerType].fields, function(i, field) {
		//if ($("#" + field.fieldId).is(":disabled"))
		//	return // continue the fields loop
		
		/*
		// If the field has a radio button and it's prefered as default
		if (field.hasRadio && (headerData["radio-checked"] == field.fieldId)) {
			// If the field is not disabled
			if (!$("#" + field.fieldId).attr("disabled")) {
				// check the radio button
				$("#" + field.fieldId + " legend input:radio").prop("checked", true);
			}
			// If the default radio is disabled identify the alternative option 
			else {
				var alternativeField;
				if (field.fieldId == "dates-field") {
					alternativeField = "builds-field";
				}
				else if (field.fieldId == "builds-field") {
					alternativeField = "dates-field";
				}
			
				// And if it's not disabled as well, select it
				if (!$("#" + alternativeField).attr("disabled")) {
					$("#" + alternativeField + " legend input:radio").prop("checked", true);
				}
			}
		}
		*/
			
		$.each(field.elementsId, function(i, elementId) {
			 if ($("#" + elementId).is("input")) {
				 // token input
				 if ($("#" + elementId).hasClass("token-input")) {
					 if (!headerData[elementId])
						return;
					 
					 $.each(headerData[elementId], function(i, elementObj) {
						 $("#" + elementId).tokenInput("add", elementObj);
					 });
					 
				 }
				 else if ($("#" + elementId).is(":checkbox")) {
					 $("#" + elementId).prop("checked", headerData[elementId]);
				 }
				 else {
					$("#" + elementId).val(headerData[elementId]);
				 }
			 }
			 else if ($("#" + elementId).is("select")){
				 $("#" + elementId + " > option").each(function(i) {
					 if ($.inArray($(this).val(), headerData[elementId]) != -1)
						 $(this).attr("selected", true);
					 else
						 $(this).attr("selected", false);
				 });
			 }
		 });
	 });
	
	// For showing/hiding the dates/builds/cwt
	if (!$("#dates-builds").parent().hasClass("hidden"))
		updateDateBuildFields("dates-builds");
}

function storeLocalHeaderOptions(headerAndFilters) {
	var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
			config.headerOptions[headerAndFilters.headerType].storeName 
			: headerAndFilters.headerType;
	
	var localData = retrieveLocalObject(config.restHost +  headerStoreName);
	var headerData = (localData) ? localData : {};
	
	$.each(config.headerOptions[headerAndFilters.headerType].fields, function(i, field) {
		//console.log($("#" + field.fieldId + " legend input:radio"));
		
		if (field.hasRadio && $("#" + field.fieldId + " legend input:radio").is(":checked"))
			headerData["radio-checked"] = field.fieldId;
		
		$.each(field.elementsId, function(i, elementId) {
			// Stores the value for inputs and the selected options array for selects
			//console.log(elementId);
			//console.log(typeof $("#" + elementId).val());
					
			if ((typeof $("#" + elementId).val() == "string") && $("#" + elementId).is("select"))
				headerData[elementId] = [$("#" + elementId).val()];
			else if ($("#" + elementId).hasClass("token-input")) {
				if ($("#" + elementId).parent().css("display") != "none") // save the selected tab 
					headerData[field.fieldId] = i;
				
				headerData[elementId] = $("#" + elementId).tokenInput("get"); 
			}
			else if ($("#" + elementId).is(":checkbox"))
				 headerData[elementId] = $("#" + elementId).is(":checked");
			else
				headerData[elementId] = $("#" + elementId).val();
		});
	 });
	//console.log(headerData);
	
	headerData.last_modified = new Date();
	
	storeLocalObject(config.restHost +  headerStoreName, headerData);
}

function checkForEmptyHeaderFields() {
	
}

function retrieveHeaderAndNavSettings() {
	var defaultHeaderNavSettings = {
			header: true,
			nav: true,
		};
		
	var headerNavSettings = retrieveLocalObject(headerNavProperties.localStoragekey);
	headerNavSettings = (headerNavSettings) ? headerNavSettings : defaultHeaderNavSettings;
	
	return headerNavSettings;
}

function retrieveAndSetHeaderAndNavSettings(headerAndFilters) {
	/*
	var defaultHeaderNavSettings = {
		header: false,
		nav: true,
	};
	
	var headerNavSettings = retrieveLocalObject(headerNavProperties.localStoragekey);
	headerNavSettings = (headerNavSettings) ? headerNavSettings : defaultHeaderNavSettings;
	*/
	
	
	headerNavSettings = retrieveHeaderAndNavSettings();
	/*
	
	// css hides the header by default so check if it needs to be expanded
	if (headerNavSettings.header && ($("#header").css("display") != "none")) {
		showHeader();
	}
	else {
		hideHeader();
	}
	
	// css show the nav by default so check if it needs to be hidden
	if (!headerNavSettings.nav) {
		hideNav();
	}
	else
		showNav();
	
	*/
	windowEventTimeout = 500;
	if (headerAndFilters) {
		showHeader();
		/*
		$(window).resize(function() {
			setTimeout(showHeader, windowEventTimeout);
		});
		*/
	}
	showNav();
	
	return headerNavSettings;
}

function storeLocalHeaderAndNavSettings(headerNavSettings) {
	storeLocalObject(headerNavProperties.localStoragekey, headerNavSettings);
}

function tabularise(elementId, selected) {
	
	if (!selected)
		selected = 0; // select first element by default
	
	deselectAll(elementId);
	$("#" + elementId).find("li").each(function(i, item) {
		var link = $(item).find("a");
		link.click(function(e) {
				e.preventDefault();
				deselectAll(elementId);

				$(this).addClass("active");
				$($(this).attr("href")).removeClass("hidden");
			
				// hide/reset the token input - b*#1429040
				$(".token-input-dropdown-facebook").hide();
				$(".token-input-input-token-facebook input").val("");
			});
		
		if (i == selected) {
			link.addClass("active");
			$(link.attr("href")).removeClass("hidden");
		}
	});
	
	function deselectAll(elementId) {
		$("#" + elementId).find("li").find("a").each(function(j, a) {
			$(this).removeClass("active");
			$($(this).attr("href")).addClass("hidden");
		})
	}
}

function populateLevels() {
	
	$.ajax({
		url: config.restHost + config.levelsAll,
		type: "GET",
		data: {},
		dataType: "xml",
		async: false,
		
		success: function(xml, textStatus, jqXHR) {
			var levels = convertLevelsXml(xml);
		
			$.each(levels, function(i, level) {
				$("select#level").append(
					$("<option />")
						.text(level.Name)
						.val(level.Identifier)
						.attr("title", level.Name)
				);
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
	
}

function convertLevelsXml(xml) {
	var elementsArray = [];
	
	$(xml).find("Level").each(function(){ 
		var element = {
			Id: $(this).find("Id").text(),
			CreatedOn: $(this).find("CreatedOn").text(),
			ModifiedOn: $(this).find("ModifiedOn").text(),
			Name: $(this).find("Name").text(),
			Identifier: $(this).find("Identifier").text(),
			LevelIdx: $(this).find("LevelIdx").text(),
		}
		
		elementsArray.push(element);
	});
	
	return elementsArray;
}

function populateLocations() {
	$("select#locations").append(
			$("<option />")
			.text("All")
			.val(null)
			.attr("title", "All")
	);
	
	$.ajax({
		url: config.restHost + config.SCCountries,
		type: "GET",
		data: {},
		dataType: "json",
		async: false,
		
		success: function(result, textStatus, jqXHR) {
	
			$.each(result, function(i, location) {
				$("select#locations").append(
						$("<option />")
						.text(location.Name)
						.val(location.Code)
						.attr("title", location.Name)
				);
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
}

function populateSCUsers() {
	$("#sc-users").tokenInput(
			config.restHost + config.SCVerticaUsersSearch,
			{
				theme: "facebook",
				hintText: "Type at least 3 characters to start searching",
                searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 3,
                //preventDuplicates: true,
                onResult: function(result) {
                	return result.map(
                			function(d) {return {"name": d}; }
                			)
                			.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                },
                //propertyToSearch: null,
                //resultsFormatter: function(item){return item},
                //tokenFormatter: function(item) { return "<li><p>" + item.first_name + " <b style='color: red'>" + item.last_name + "</b></p></li>" }
            }
		);
}

function populateSCUserGroups() {
	$("#sc-user-groups").tokenInput(
			config.restHost + config.SCVerticaUsersGroups,
			{
				theme: "facebook",
				hintText: "Type text to get matched group names",
                searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 0,
                //preventDuplicates: true,
                
                onResult: function(result) {
                	var input = $("#token-input-sc-user-groups").val();
                	var re = new RegExp(input, "i");
                	var filteredResult = [];
                	
                	result.map(
                		function(d) {
                			if (re.test(d))
                				filteredResult.push({ "name": d }); 
                	});
                	filteredResult.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                	
                	return filteredResult;
                },
			}
		);
}

function populateGamertags() {
	rosPlatformsDict = getRosPlatformsDict();
	
	$("#gamertags").tokenInput(
			config.restHost + config.VerticaGamersSearch,
			{
				theme: "facebook",
				hintText: "Type at least 3 characters to start searching",
                //searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 3,
                //preventDuplicates: true,
                onResult: function(result) {
                	return result.map(
                			function(d) {
                				return {
                					name: d.GamerHandle + "(" + rosPlatformsDict[d.Platform] + ")",
                					id: rosPlatformsDict[d.Platform],
                				};
                			})
                			.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                },
            }
		);
}


function getGamertags() {
	var gamertags = $("#gamertags").tokenInput("get");
	
	gamertags = gamertags.map(function(d) {
			return "<" + d.name.split("(")[0] + "|" + d.id + ">";
			})
			.join();
	
	return gamertags;
}

function populateGamertagGroups() {
	$("#gamertag-groups").tokenInput(
			config.restHost + config.VerticaGamersGroups,
			{
				theme: "facebook",
				hintText: "Type text to get matched group names",
                searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 0,
                //preventDuplicates: true,
                
                onResult: function(result) {
                	var input = $("#token-input-gamertag-groups").val();
                	var re = new RegExp(input, "i");
                	var filteredResult = [];
                	
                	result.map(
                		function(d) {
                			if (re.test(d.Name))
                				filteredResult.push({ "name": d.Name }); 
                	});
                	filteredResult.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                	
                	return filteredResult;
                },

            }
		);
}

function populateMissions() {
	var rosPlatformsDict = getRosPlatformsDict();
	
	var customUrl = "";
	var category = ($("#freemode-category").val()) ? $("#freemode-category").val() : null;
	var ugcStatus = ($("#freemode-ugcstatus").val()) ? $("#freemode-ugcstatus").val() : null;
	
	if (category)
		customUrl += ("&category=" + category);
	if (ugcStatus)
		customUrl += ("&isPublished=" + ugcStatus);
	if (customUrl)
		customUrl = "?" + customUrl;
	//console.log(customUrl);
	
	$("#missions").empty();
	$("#missions-field").find("ul").remove();
	$(".token-input-dropdown-facebook").hide();
	$(".token-input-input-token-facebook input").val("");
	$("#missions").tokenInput(
			config.restHost + config.VerticaMissionsSearch + customUrl,
			{
				theme: "facebook",
				hintText: "Type at least 3 characters to start searching",
                //searchDelay: 1000,
                method: "GET",
                //queryParam: "name",
                queryParam: "name",
                minChars: 3,
                //preventDuplicates: true,
                onResult: function(result) {
                	return result.map(
                		function(d) {
                			return {
                				name: d.Name + " (" + d.Creator 
                				+ ((rosPlatformsDict[d.CreatorPlatform]) ? ("[" + rosPlatformsDict[d.CreatorPlatform] + "]") : "") 
    								+ " - v." + d.Version + ")",
                				id: d.Identifier,
                			};
                		})
                		.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? -1 : 1});
                },
            }
		);

}

function populateDeathmatch() {
	var rosPlatformsDict = getRosPlatformsDict();
	
	$("#deathmatch").tokenInput(
			config.restHost + config.VerticaMissionsSearch,
			{
				theme: "facebook",
				hintText: "Type at least 3 characters to start searching",
                //searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 3,
                //preventDuplicates: true,
                tokenLimit: 1,
                onResult: function(result) {
                	return result.map(
                		function(d) {
                			return {
                				name: d.Name + " (" + d.Creator 
                							+ ((rosPlatformsDict[d.CreatorPlatform]) ? ("[" + rosPlatformsDict[d.CreatorPlatform] + "]") : "") 
                							+ " - v." + d.Version + ")",
                				id: d.Identifier,
                			};
                		})
                		.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? -1 : 1});
                },
            }
		);
}

function populateCreators() {
	var rosPlatformsDict = getRosPlatformsDict();
	
	$("#creators").tokenInput(
			config.restHost + config.VerticaGamersSearch,
			{
				theme: "facebook",
				hintText: "Type at least 3 characters to start searching",
                //searchDelay: 1000,
                method: "GET",
                queryParam: "name",
                minChars: 3,
                //preventDuplicates: true,
                onResult: function(result) {
                	return result.map(
                			function(d) {
                				return {
                					name: d.GamerHandle + "(" + rosPlatformsDict[d.Platform] + ")",
                					id: rosPlatformsDict[d.Platform],
                				};
                			})
                			.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                },
            }
		);
}


function getCreators() {
	if ($("#creators").size() == 0)
		return [];
	
	var gamertags = $("#creators").tokenInput("get");
	
	gamertags = gamertags.map(function(d) {
			return "<" + d.name.split("(")[0] + "|" + d.id + ">";
			})
			.join();
	
	return gamertags;
}

// Same as above but for select element
function getGamerGroups() {
	var obj = {
		url: config.restHost + config.VerticaGamersGroups,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateGamerGroups() {
	var gamerGroups = getGamerGroups();
	
	var dfunc = {
		getValue: function(d) { return JSON.stringify(d.Gamers); },
		getTitle: function(d) { return d.Name; },
		getText: function(d) { return d.Name; },
	};
	
	populateSelectElements(["gamer-group"], gamerGroups, dfunc);
}

function getGamertagsFromGroups(groups) {
	var gamertags = [];
	var rosPlatformsDict = getRosPlatformsDict();
	
	var obj = {
		url: config.restHost + config.VerticaGamersGroups,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	
	var allGroups = getAjaxData(obj);
	
	$.each(allGroups, function(i, group) {
		
		if ($.inArray(group.Name, groups) != -1) {
			gamertags = gamertags.concat(
							group.Gamers.map(function(d) {
								return ("<" + d.GamerHandle + "|" + rosPlatformsDict[d.Platform] + ">");
							})
						);
		}
	});
	
	return gamertags;
}

/*
function populateGamers() {
	$.ajax({
		url: config.restHost + config.SCGroupsAndUsers,
		type: "GET",
		data: {},
		dataType: "json",
		async: false,
			
		success: function(result, textStatus, jqXHR) {
			//console.log(result);
			var gamerNames = result.map(function(d) {return d.Name});
			$("#gamer").autocomplete({
				source: gamerNames,
	            minlength: 2,
	            change: function(e, ui) {
	            	// when the typed item is not in the list, then ui.item will be null
		            if (!ui.item)
		            	$("#gamer").val("");
	            },
	        })
	        .keypress(function(e) { 
	        	// when pressing enter check if the value is valid, otherwise set to null 
	        	if (e.which == 13) {
	        		if ($.inArray($(this).val(), gamerNames) == -1)
	        			$(this).val("");
	        	}
	        });
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
}
*/

function populateGameTypes() {
	
	$.ajax({
		url: config.restHost + config.gametypesAll,
		type: "GET",
		data: {},
		dataType: "xml",
		async: false,
			
		success: function(xml, textStatus, jqXHR) {
			var gametypes = convertGametypesXml(xml);
			
			$.each(gametypes, function(i, gametype) {
				$("select#game-types").append(
					$("<option />")
						.text(gametype.Value)
						.val(gametype.Value)
						.attr("title", gametype.Value)
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
	
}

function populateExportUsers() {
	
	$.ajax({
		url: config.restHost + config.mapExportStatsUsers,
		type: "GET",
		data: {},
		dataType: "xml",
		async: false,
			
		success: function(xml, textStatus, jqXHR) {
			var exportUsers = getExportUsersList(xml);
			var defaultVal;
			//console.log(exportUsers);
			$("#user").autocomplete({
				source: exportUsers,
	            minlength: 2,
	            change: function(event, ui) {
	            	// when the typed item is not in the list, then ui.item will be null
		            if (!ui.item)
		            	$("#user").val("");
	            },

	        })
	        .keypress(function(e) { 
	        	// when pressing enter check if the value is valid, otherwise set to null 
	        	if (e.which == 13) {
	        		if ($.inArray($(this).val(), exportUsers) == -1)
	        			$(this).val("");
	        	}
	        });
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
	
}

function populatePlaythroughsBuilds() {
	
	$.ajax({
		url: config.restHost + config.playthroughsBuilds,
		type: "GET",
		dataType: "json",
		data: {},
		async: false,
			
		success: function(json, textStatus, jqXHR) {
			// Order the build DESC
			json.sort(function(a, b) {return (a>b) ? -1 : 1})
			$.each(json, function(i, build) {
				$("select#dt-build").append(
					$("<option />")
						.text(build)
						.val(build)
						.attr("title", build)
						
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
	
}

function populatePlaythroughsPlatforms() {
	
	$.ajax({
		url: config.restHost + config.playthroughsPlatforms,
		type: "GET",
		dataType: "xml",
		data: {},
		async: false,
			
		success: function(xml, textStatus, jqXHR) {
			
			$(xml).find("Platform").each(function() {
				$("select#dt-platform").append(
					$("<option />")
						.text($(this).text())
						.val($(this).text())
						.attr("title", $(this).text())
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
}

function populatePlaythroughsUsers() {
	var rosPlatformsDict = getRosPlatformsDict();
	
	$("#dt-users").tokenInput(
			config.restHost + config.playthroughsUsers,
			{
				theme: "facebook",
				hintText: "Type text to select from the list",
                searchDelay: 1000,
                minChars: 0,
                //propertyToSearch: "Username",
                tokenLimit: 8,
               // preventDuplicates: true,
                
                onResult: function(result) {
                	var input = $("#token-input-dt-users").val();
                	var re = new RegExp(input, "i");
                	var filteredResult = [];
                	
                	result.map(
                		function(d) {
                			if (re.test(d.Username))
                				filteredResult.push(
                					{
                						"name": d.Username + ((rosPlatformsDict[d.Platform]) ? ("(" + rosPlatformsDict[d.Platform] + ")") : ""),
                						"id": d.GamerHandle + ((rosPlatformsDict[d.Platform]) ? ("|" + rosPlatformsDict[d.Platform]) : ""),  
                					}
                				); 
                			});
                	filteredResult.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
                	
                	return filteredResult;
                },
                
                //resultsFormatter: function(item){return item},
                //tokenFormatter: function(item) { return "<li><p>" + item.first_name + " <b style='color: red'>" + item.last_name + "</b></p></li>" }
            }
		);
	
	/*
	$.ajax({
		url: config.restHost + config.playthroughsUsers,
		type: "GET",
		data: {},
		dataType: "json",
		async: false,
			
		success: function(json, textStatus, jqXHR) {
			/*
			$("#dt-user").autocomplete({
				source: json,
	            minlength: 2,
	            change: function(event, ui) {
	            	// when the typed item is not in the list, then ui.item will be null
		            if (!ui.item)
		            	$("#dt-user").val("");
	            },

	        })
	        .keypress(function(e) { 
	        	// when pressing enter check if the value is valid, otherwise set to null 
	        	if (e.which == 13) {
	        		if ($.inArray($(this).val(), json) == -1)
	        			$(this).val("");
	        	}
	        });
	        */	
	
			/*
			$.each(json, function(i, user) {
				$("select#dt-users").append(
					$("<option />")
						.text(user.Username)
						.val(user.GamerHandle)
						.attr("title", user.Username + " :: " + user.GamerHandle)
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		}
	});
	*/
	
}
/*
function populateDeathmatches() {
	// Social Club Freemode Matches
	$.ajax({
		url: config.restHost + config.SCFreemodeNames,
		type: "GET",
		data: {type: "DEATHMATCH"},
		dataType: "json",
		async: false,
			
		success: function(deathmatches, textStatus, jqXHR) {
			$.each(deathmatches, function(i, deathmatch) {
				$("select#deathmatch-list").append(
					$('<option />')
						.text(deathmatch.Name + " (" + deathmatch.Creator + ")")
						.val(deathmatch.UGCIdentifier)
						.attr("title", deathmatch.Name + " (" + deathmatch.Creator + ") : " + deathmatch.Description)
						//.attr("selected", function() {
						//	return (pageData && pageData.deathmatch_list == deathmatch.UGCIdentifier) ? "selected" : false;
						//})
				)
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			
		}
	});
}
*/

/* Retreives the builds and populates given select element
 * 
 * Available options {
 * 	elementId - The DOM element ID
 *  async - the ajax request sync mode
 * } 
 *  
 */
function populateAllBuilds(options) {

	$.ajax({
		url: config.restHost + config.buildsAll,
		type: "GET",
		data: {},
		dataType: "json",
		async: options.async,
		success: function(json, textStatus, jqXHR) {
			var builds = json.Items.sort(sortByIdentifierDesc);
			
			if (options.elementId == "build") {
				$("select#" + options.elementId).append(
						$("<option />")
							.text(config.buildAllText)
							.val("")
							.attr("title", config.buildAllText)
					);
			}
			
			if (builds.length > 0) {
				$("select#" + options.elementId).append(
						$("<option />")
							.text(config.buildAliasText)
							.val(-1)
							.attr("title", config.buildAliasText)
					);
			}
			
			$.each(builds, function(i, build) {
				if (!build.GameVersion)
					return;
								
				$("select#" + options.elementId).append(
					$("<option />")
						.text(build.Identifier)
						.val(build.Identifier)
						.attr("title", build.Identifier)
				);
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {}
	});
}

function getAllBuilds() {
	var builds = [];
	
	$.ajax({
		url: config.restHost + config.buildsAll,
		type: "GET",
		data: {},
		dataType: "json",
		async: false,
		success: function(json, textStatus, jqXHR) {
			builds = json.Items.map(function(b) {return b.Identifier});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {}
	});
	
	return builds;
}

function getHeaderBuilds() {
	var builds;
	var availableBuilds = $.map($("#builds option"), function(e) {
		return e.value; 
	});
	//console.log(availableBuilds);
	
	if ($("#builds").val()) {
		if (($("#builds").val().length == 1) && $("#builds").val()[0] == -1) // if latest only is select
			builds = (availableBuilds.length > 1) ? availableBuilds[1] : null; // 0 is -1, 1 is actual latest
		else
			builds = $("#builds").val().filter(function(d) {return (d != -1);}).join(",");
	}
	else
		builds = null;
	
	//console.log(builds);
	return builds;
}

function getHeaderBuild() {
	var build;
	var availableBuilds = $.map($("#build option"), function(e) {
		return e.value; 
	});
	//console.log(availableBuilds);
	
	if ($("#build").val()) {
		if ($("#build").val() == -1) // if latest only is select
			build = (availableBuilds.length > 1) ? availableBuilds[1] : null; // 0 is -1, 1 is actual latest
		else
			build = $("#build").val();
	}
	else
		build = null;
	
	return build;
}

function populateDatesBuildsOptions(elementId) {
	var dfunc = {
		getValue: function(d) {return d.Value},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
		
	populateSelectElements([elementId], config.datesBuildsOptions, dfunc);

	$("#" + elementId).change(function() {updateDateBuildFields(elementId); });
	
}

function updateDateBuildFields(elementId) {
	$("#" + elementId).parent().parent().children("fieldset").each(function(i, f) {			
		if ($("#" + elementId).val() == 0) {
			$(f).removeClass("hidden"); // Show all
			if (i == 2) // hide cwp
				$(f).addClass("hidden");
		}
		else {
			$(f).addClass("hidden"); // Hide all
			if (i == ($("#" + elementId).val() - 1)) // show selected 
				$(f).removeClass("hidden");
		}
	});
};

/*
function convertBuildsXml(xml) {
	var elementsArray = [];
	
	$(xml).find("Build").each(function() {
		var build = {
			Id: Number($(this).find("Id").text()),
			CreatedOn: $(this).find("CreatedOn").text(),
			ModifiedOn: $(this).find("ModifiedOn").text(),
			Identifier: ($(this).find("Identifier").text()),
			HasAssetStats: ($(this).find("HasAssetStats").text() == "true"),
			HasProcessedStats: ($(this).find("HasProcessedStats").text() == "true"),
			HasAutomatedEverythingStats: ($(this).find("HasAutomatedEverythingStats").text() == "true"),
			HasAutomatedMapOnlyStats: ($(this).find("HasAutomatedMapOnlyStats").text() == "true"),
			HasMagDemoStats: ($(this).find("HasMagDemoStats").text() == "true"),
			HasMemShortfallStats: ($(this).find("HasMemShortfallStats").text() == "true"),
			HasShapeTestStats: ($(this).find("HasShapeTestStats").text() == "true"),
		};
		elementsArray.push(build);
	});
	
	//console.log(elementsArray);
	
	return elementsArray;
}
*/

/*
function populateBuildConfigs() {
	$.ajax({
		url: config.restHost + config.buildConfigsAll,
		type: "GET",
		data: {},
		dataType: "xml",
		async: false,
			
		success: function(xml, textStatus, jqXHR) {
			var buildConfigs = convertBuildConfigsXml(xml);
			
			$.each(buildConfigs, function(i, buildConfig) {
				$("select#build-config").append(
					$("<option />")
						.text(buildConfig.Value)
						.val(buildConfig.Value)
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {}
	});
}

function convertBuildConfigsXml(xml) {
	var elementsArray = [];
		
	$(xml).find("BuildConfig").each(function(){
		var buildConfig = {
			Value: $(this).find("Value").text()
		};
		elementsArray.push(buildConfig);
	});
	
	return elementsArray;
}
*/

function populateCutscenes() {
	$.ajax({
		url: config.restHost + config.cutscenesAll,
		type: "GET",
		data: {},
		dataType: "xml",
		async: false,
			
		success: function(xml, textStatus, jqXHR) {
			var cutscenes = convertCutscenesXml(xml)
						.sort(function(a, b) {return ((a.Name > b.Name) ? 1 : -1)});
			
			$.each(cutscenes, function(i, cutscene) {
				$("select#cutscene").append(
					$("<option />")
						.text(cutscene.Name)
						.val(cutscene.Name)
						.attr("title", cutscene.Name)
				);
			});
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {}
	});
}

function convertCutscenesXml(xml) {
	var elementsArray = [];
		
	$(xml).find("Cutscene").each(function(){
		var element = {
			Id: $(this).find("Id").text(),
			CreatedOn: $(this).find("CreatedOn").text(),
			ModifiedOn: $(this).find("ModifiedOn").text(),
			Name: $(this).find("Name").text(),
			Identifier: $(this).find("Identifier").text(),
			Duration: $(this).find("Duration").text(),
		};
		elementsArray.push(element);
	});
	
	return elementsArray;
}

/* End of xml? */

function getPlatforms() {
	var obj = {
		url: config.restHost +  config.platformsEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populatePlatforms() {
	allPlatforms = getPlatforms().slice(1);
	//console.log(allPlatforms);
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["platform"], allPlatforms, dfunc);
	populateSelectElements(["platforms"], allPlatforms, dfunc);
}

function getRosPlatforms() {
	if (!rosPlatforms) {
		var obj = {
			url: config.restHost +  config.rosPlatformsEnums,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
		
		rosPlatforms = getAjaxData(obj);
	}
	return rosPlatforms;
}

function getRosPlatformsDict() {
	if (!rosPlatformsDict) {
		rosPlatformsDict = {};
		getRosPlatforms().map(function(d) {rosPlatformsDict[d.Value] = d.Name});
	}
	
	return rosPlatformsDict;
}

function populateROSPlatforms() {
	var	rosPlatforms = getRosPlatforms();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["rosplatform"], rosPlatforms, dfunc);
	populateSelectElements(["rosplatforms"], rosPlatforms, dfunc);
}

function getGameTypes() {
	var obj = {
		url: config.restHost +  config.gameTypesEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateGameTypes() {
	var gameTypes = getGameTypes();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["game-types"], gameTypes, dfunc);
}

function getBuildConfigs() {
	var obj = {
		url: config.restHost +  config.buildConfigsEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateBuildConfigs() {
	var buildConfigs = getBuildConfigs();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["build-config"], buildConfigs, dfunc);
}

function getFileTypes() {
	var obj = {
		url: config.restHost +  config.fileTypesEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateFileTypes() {
	var fileTypes = getFileTypes();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["file-types"], fileTypes, dfunc);
}

function populateGameTimes() {
	var dfunc = {
		getValue: function(d) {return d.Value},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
		
	populateSelectElements(["game-times"], config.gameTimes, dfunc);
}

function getDateGrouping() {
	var obj = {
		url: config.restHost +  config.dateGroupingEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateDateGrouping() {
	var dateGrouping = getDateGrouping();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
		getText: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
	};
		
	populateSelectElements(["date-grouping"], dateGrouping, dfunc);
}

function getMatchTypes() {
	if (!matchTypes) {
		
		var obj = {
			url: config.restHost +  config.matchTypesEnums,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
		
		matchTypes = getAjaxData(obj);
	}
	
	return matchTypes;
}

function getMatchTypesDict() {
	if (!matchTypesDict) {
		matchTypesDict = {};
		getMatchTypes().map(function(d) {matchTypesDict[d.Value] = (d.FriendlyName) ? d.FriendlyName : d.Name});
	}
	
	return matchTypesDict;
}

function populateMatchTypes() {
	var matchTypes = getMatchTypes();
	
	matchTypes.unshift({
		"FriendlyName": "None",
		"Name" : "",
		"Value": null
	});
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
		getText: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
	};
		
	populateSelectElements(["match-type", "match-types"], matchTypes, dfunc);
}

function getMatchSubTypesDict() {
	var matchTypesDict = getMatchTypesDict();
	
	if (!matchSubTypesDict) {
		matchSubTypesDict = {};
		
		$.each(matchTypes, function(matchKey, matchType) {
			if (config.matchSubTypesEnumsDict.hasOwnProperty(matchType.Name)) {
				
				var obj = {
					url: config.restHost + config.matchSubTypesEnumsDict[matchType.Name],
					data: {},
					method: "GET",
					dataType: "json",
					async: false,
				};
				
				var subTypeDict = {};
				getAjaxData(obj).map(function(d) {subTypeDict[d.Value] = (d.FriendlyName) ? d.FriendlyName : d.Name});
				matchSubTypesDict[matchKey] = subTypeDict;
			}
		});
		
		//console.log(matchSubTypesDict);
	}
	
	return matchSubTypesDict;
}

function getFreemodeCategories() {
	if (!freemodeCategories) {
	
		var obj = {
			url: config.restHost +  config.freemodeCategoriesEnums,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
		freemodeCategories = getAjaxData(obj);
	}
	
	return freemodeCategories;
}

function getFreemodeCategoriesDict() {
	if (!freemodeCategoriesDict) {
		freemodeCategoriesDict = {};
		getFreemodeCategories().map(function(d) {freemodeCategoriesDict[d.Value] = (d.FriendlyName) ? d.FriendlyName : d.Name});
	}
	
	return freemodeCategoriesDict;
}

function populateFreemodeCategories() {
	var freemodeCategories = getFreemodeCategories();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
		getText: function(d) {return (d.FriendlyName) ? d.FriendlyName : d.Name},
	};
	
	populateSelectElements(["freemode-categories"], freemodeCategories, dfunc);
	
	freemodeCategories.unshift({FriendlyName: "All", Name: ""});
	populateSelectElements(["freemode-category"], freemodeCategories, dfunc);
}

function getJobResults() {
	if (!jobResults) {
	
		var obj = {
			url: config.restHost +  config.getJobResultsEnums,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
		jobResults = getAjaxData(obj);
	}
	
	return jobResults;
}

function getJobResultsDict() {
	if (!jobResultsDict) {
		jobResultsDict = {};
		getJobResults().map(function(d) {jobResultsDict[d.Value] = (d.FriendlyName) ? d.FriendlyName : d.Name});
	}
	
	return jobResultsDict;
}

function populateUGCStatus() {
	var freemodeCategories = getFreemodeCategories();
	
	var dfunc = {
		getValue: function(d) {return d.Value},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["freemode-ugcstatus"], config.ugcStatus, dfunc);
}

function populateResolutions() {
	var clipCategories = getClipCategories();
	
	var dfunc = {
		getValue: function(d) {return d},
		getTitle: function(d) {return d},
		getText: function(d) {return d},
	};
	
	populateSelectElements(["resolution"], config.resolutionsAll, dfunc);
}

function getClipCategories() {
	var obj = {
		url: config.restHost +  config.clipCategoriesEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function populateClipCategories() {
	var clipCategories = getClipCategories();
	
	var dfunc = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
	};
	
	populateSelectElements(["clip-categories"], clipCategories, dfunc);
	populateSelectElements(["clip-category"], clipCategories, dfunc);
}

function getPlaythroughsUsers() {
	var obj = {
			url: config.restHost + config.playthroughsUsers,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
	return getAjaxData(obj);
}

function getPlaythroughUsersAsDict() {
	var dict = {};
	
	getPlaythroughsUsers().map(function(d) {
		//console.log(d.GamerHandle + " " + d.Username);
		
		if (!d.GamerHandle)
			return;
		
		if (!dict.hasOwnProperty(d.GamerHandle))
			dict[d.GamerHandle] = d.Username;
	});
	
	return dict;
}

function getMissionHashes() {
	var obj = {
			url: config.webHost + config.missionHashesCSV,
			data: {},
			method: "GET",
			dataType: "text",
			async: false,
		};
	
	var dict = {};
	
	getAjaxData(obj).split("\n").map(function(d) {
		var pair = d.split(",");
		dict[pair[0]] = pair[1]; 
	});
	
	//console.log(dict);
	return dict;
}

function getRadioHashes() {
	var obj = {
			url: config.webHost + config.radioHashesCSV,
			data: {},
			method: "GET",
			dataType: "text",
			async: false,
		};
	
	var dict = {};
	
	getAjaxData(obj).split("\n").map(function(d) {
		var pair = d.split(",");
		dict[pair[0]] = pair[1]; 
	});
	
	//console.log(dict);
	return dict;
}

function getWeaponHashes() {
	var obj = {
			url: config.webHost + config.weaponHashesCSV,
			data: {},
			method: "GET",
			dataType: "text",
			async: false,
		};
	
	var dict = {};
	
	getAjaxData(obj).split("\n").map(function(d) {
		var pair = d.split(",");
		dict[pair[0]] = pair[1]; 
	});
	
	//console.log(dict);
	return dict;
}

function getWeaponStatnames() {
	var obj = {
			url: config.webHost + config.weaponStatnamesCSV,
			data: {},
			method: "GET",
			dataType: "text",
			async: false,
		};
	
	var dict = {};
	
	getAjaxData(obj).split("\n").map(function(d) {
		var pair = d.split(",");
		dict[pair[0]] = {
			"Name" : pair[0],
			"FriendlyName" : pair[1], 
		};
	});
	
	//console.log(dict);
	return dict;
}

function getWeaponsAll() {
	var obj = {
			url: config.restHost + config.weaponsAll,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
	
	return getAjaxData(obj);
}

function getWeaponCategoriesEnums() {
	var obj = {
		url: config.restHost +  config.weaponCategoriesEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function getExpenditureCategoriesEnums() {
	var obj = {
		url: config.restHost +  config.expenditureCategoriesEnums,
		data: {},
		method: "GET",
		dataType: "json",
		async: false,
	};
	return getAjaxData(obj);
}

function getVehicleHashes() {
	var obj = {
			url: config.restHost + config.vehiclesAll,
			data: {},
			method: "GET",
			dataType: "json",
			async: false,
		};
	
	var dict = {};
	
	getAjaxData(obj).map(function(d) {
		dict[d.Identifier] = d.FriendlyName; 
	});
	
	//console.log(dict);
	return dict;
}

/*
 * 
 * Generic Function to replace the ones above
 * 
 * */
/*
  	datafunctions = {
		getValue: function(d) {return d....},
		getTitle: function(d) {return d....},
		getText: function(d) {return d....},
	}
	
	data can come from getAjaxData
*/
function populateSelectElements(elementIds, data, dataFunctions) {
	
	$.each(elementIds, function(i, elementId) {
		//console.log(elementId);
		$.each(data, function(j, d) {
			//console.log(dataFunctions.getText(d));
			var option = $("<option />");
			
			option.text(dataFunctions.getText(d))
				.val(dataFunctions.getValue(d))
				.attr("title", dataFunctions.getTitle(d));
			
			/* This will not work on IE
			if (dataFunctions.onClick) {
				option.on("click", function() {dataFunctions.onClick(d);})
			}
			*/
			$("select#" + elementId).append(option);
		});	
		
		
		if (dataFunctions.onClick) {
			$("select#" + elementId).on("click", function() {
				dataFunctions.onClick(data[$("select#" + elementId)[0].selectedIndex]);
			});
		}
		if (dataFunctions.onChange) {
			$("select#" + elementId).on("change", function() {
				dataFunctions.onChange(data[$("select#" + elementId)[0].selectedIndex]);
			});
		}
		
	});
	
}

/*
	datafunctions = {
		getValue: function(d) {return d....},
		getText: function(d) {return d....},
		getTitle: function(d) {return d....},
		getChildren: function(d) {return d....},
		onClick: function() {return d...}
	};

	Data can come from getAjaxData
*/
function populateHierarchicalElements(elements, data, dataFunctions) {

	$.each(elements, function(i, element) {

		var ul = $("<ul />");
		$.each(data, function(j, d) {
			var li = $("<li />");
			
			li.append(
				$("<span>")
					.text(dataFunctions.getText(d))
					.val(dataFunctions.getValue(d))
					.attr("title", dataFunctions.getTitle(d))
					.click(function() {
						dataFunctions.onClick(dataFunctions.getValue(d), dataFunctions.getText(d));
					})
					.addClass("hand")
				);
				
			populateHierarchicalElements(li, dataFunctions.getChildren(d), dataFunctions);
			
			li.appendTo(ul);
		});
		if (ul.children("li").length > 0)
			ul.appendTo(element);
	});

}

/**
 * object has the format of  
 * object = {
 * 		url: ...,
 * 		data: {...},
 * 		method: GET/POST,
 * 		dataType: ...,
 * 		contentType: ...,
 * 		async: true/false,
 * }
 */
function getAjaxData(object) {
	var result = [];
	
	$.ajax({
		url: object.url,
		type: object.method,
		data: object.data,
		dataType: object.dataType,
		contentType: object.contentType,
		async: object.async,
		cache: object.cache,
			
		success: function(data, textStatus, jqXHR) {
			result = data;			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {}
	});
	
	return result;
}
	
/* END of retrieve and populate functions */

function cleanGraph(id) {
	var svg = d3.select("#" + id + " svg");
	
	svg.selectAll("g")
		.transition()
			.style("opacity", 0.01)
		.remove();
	
	svg.selectAll("text")
		.transition()
			.style("opacity", 0.01)
		.remove();
	
	if (typeof window.onresize === "function")
			window.onresize = null;
}

// Function for key delay searches 
var typewatch = function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    }
}();

var commas = function(d) { return d3.format(",")(d); };
var commasFixed2 = function(d) { return d3.format(",")(d.toFixed(2)); };
var commasFixed = function(d, n) { return d3.format(",")(d.toFixed((n) ? n : 0)); };
var cashCommasFixed = function(d, n) { return "$ " + commasFixed2(d, n); };
var round2 = function(d) {return d3.round(d, 2); };

function msTicks2UnixTime(ticks) {
	/*
	 * MS ticks counting start at 12:00:00 midnight, Jan 1, 0001
	 * Unix Time starts at midnight Jan 1, 1970 (in milliseconds)
	 * 1 tick is 10000 milliseconds 
	 * There are 621355968000000000 ticks between 01/01/0001 and 01/01/1970
	 */
	return ((ticks - 621355968000000000) / 10000);
}

function msTicks2Secs(ticks) {
	/* 1 tick is 10000 milliseconds */
	return (ticks / (10000 * 1000));
}

function getDateString(dateElementId, isUTC) {
	var dateString = $("#" + dateElementId).val();
	
	if (!dateString)
		return null;
	
	if (isUTC) {
		dateString = new Date(config.dateInputUTCFormat.parse(dateString)).toISOString();
	}
	else
		dateString = new Date(config.dateInputFormat.parse(dateString)).toISOString();
		
	return dateString;
}

// Returns the date object from a json date string
function parseJsonDate(jsonDate) {
	//var dateRe = new RegExp("^/Date\\((\\d+)[\\+\\d]*\\)/$");
	
    //var constructor = date.replace(dateRe, "new Date($1)");

    //if (constructor == date) {
    //    throw 'Invalid serialized DateTime value: "' + date + '"';
    //}
    //var date = new Date(parseInt(jsonDate.slice(6, -1), 10));
	if (!jsonDate)
		return "";
		
	var epochTime;
	var jsonDateString = jsonDate.slice(6, -2);
			
	if (jsonDateString.indexOf("+") != -1) {
		var dates = jsonDateString.split("+");
	
		epochTime = parseInt(dates[0]) 
			+ ((dates[1]) ? Number(dates[1].slice(0, 2))*config.anHourInMillisecs : 0);
	}
	else if (jsonDateString.indexOf("-") != -1) {
		var dates = jsonDateString.split("-");
		
		epochTime = parseInt(dates[0]) 
			- ((dates[1]) ? Number(dates[1].slice(0, 2))*config.anHourInMillisecs : 0);
	}
	else
		epochTime = parseInt(jsonDateString);
	
	return new Date(epochTime);
};

// Return the diff of two date strings in secs
function dateStringsDiffInSecs(dateString1, dateString2) {
	var date1 = new Date(dateString1),
		date2 = new Date(dateString2);
		
	if (isValidDate(date1) && (isValidDate(date2)))
		return datesDiffInSecs(date1, date2);
	else
		return 0;
}

// Return the difference of two day objects in secs
function datesDiffInSecs(date1, date2) {
	// Convert to unix time millisecs and subtract; then convert to secs
	return ((date1.getTime() - date2.getTime()) / 1000);
}

// Return only the date in utc from a datetime string
function extractDate(dateString) {
	var date = new Date(dateString);
	if (isValidDate(date))
		return date.getFullYear() +"-"+ zeros(date.getMonth()+1) +"-"+ zeros(date.getDate());
	else
		return null;
}

function isValidDate(d) {
	if (Object.prototype.toString.call(d) !== "[object Date]")
		return false;
	
	return !isNaN(d.getTime());
}

var zeros = d3.format("02d");

function truncate(string, chars) {
	if (chars < 3) chars += 3;
	return string.slice(0, chars-3) + "...";
}

function formatSecs(secs) {
	var h = zeros(Math.floor(secs / 3600));
	var m = zeros(Math.floor((secs % 3600) / 60));
	var s = zeros(Math.floor((secs % 3600) % 60));
	
	return (h + ":" + m + ":" + s);
}

function formatMins(mins) {
	return formatSecs(mins*60);
}

function formatSecsWithDays(secs) {
	var d = zeros(Math.floor(secs / 86400));
	var h = zeros(Math.floor((secs % 86400) / 3600));
	var m = zeros(Math.floor(((secs % 86400) % 3600) / 60));
	var s = zeros(Math.floor(((secs % 86400) % 3600) % 60));
	
	return (d + ":" + h + ":" + m + ":" + s);
}

function frameTimeToFPS(frameTime) {
	return (frameTime) ? (1000 / frameTime) : frameTime;
}

function capitaliseString(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function toUTCStringWOSecs(d) {
	return d.toUTCString().replace(/:\d+ /g, " ");
}

function formatPercentage(value, total) {
	return (commasFixed2((value/total)*100));
}

/* CSV operations*/
function exportToCSV(columnsArray, rowsArray, frameId, filename) {
	
	var Columns = [{ ColumnNames : columnsArray.map(function(d) {return ("\"" + d +"\""); }).join(",") }];
	
	var Rows = [];
	rowsArray.map(function(r) {
		Rows.push({row : r.map(function(d) {return ("\"" + d +"\""); }).join(",")});
	});
	
	var csvData = { "Columns": Columns, "Rows": Rows };
    $("#" + frameId).contents().find("#CsvData")[0].value = JSON.stringify(csvData);
    
    $("#" + frameId).contents().find("#CsvFilename")[0].value = filename;
    
	return;
}
function addCSVButton(elementId, frameId, onClickFunction) {
	
	$("#" + elementId)
		.append(
			$("<iframe />")
				.attr("id", frameId)	
				.addClass("csv-frame")			
				.attr("src", config.webHost + config.csvBackend)
				.load(function() {
					var iFrameForm = $(this).contents().find("form");
					
					if (iFrameForm.size() == 0)
						return;
								
					$(iFrameForm)
						.find("#ExportCSVButton")
						.addClass("red-button hand")
						.attr("onCLick", onClickFunction)
				})
		)
};

function getCSVFilename(extra) {
	return (encodeURI(($("#content-title").text() + ((extra) ? (" : " + extra) : "")).replace(/\s/g, "_")) + ".csv");
}

function fillEmptyResults(resultArray, metric, size) {
	var extraItems = [];
	
	$.each(resultArray, function(i, resultIem) {	
		
		if ((i > 0) && ((resultIem[metric]) - resultArray[i-1][metric] > size)) {
				
			var repeat = (resultIem[metric] - resultArray[i-1][metric] -size )/size;
			
			for(var j=1; j<=repeat; j++) {
				var newItem = {};
				
				$.each(resultArray[i-1], function(key, value) {
					newItem[key] = 0;
				});
				newItem[metric] = resultArray[i-1][metric] + size*j;
										
				extraItems.push(newItem);
			} 
		}
		
	});
	
	if (extraItems) {
		resultArray = (resultArray.concat(extraItems)).sort(
				function(a, b) {
					return (a[metric] >= b[metric]) ? 1 : -1; 
				});
	}
	
	return resultArray;
}


/*
function addCSVButton(elementId, onClickFunction) {
	$("#" + elementId).append(
		$("<input />")
			.attr("type", "button")
			.attr("id", "export-csv")
			.attr("name", "export-csv")
			.attr("title", "Export to CSV")
			.val("Export Data to CSV")
			.css("float", "right")
			.addClass("red-button hand")
			.click(onClickFunction)
		);
}
*/

// convert xml functions
function convertGametypesXml(xml) {
	var elementsArray = [];
	
	$(xml).find("GameType").each(function(){
		var gametype = {
			Value: $(this).find("Value").text()
		};
		elementsArray.push(gametype);
	});
	return elementsArray;
}

function getExportUsersList(xml) {
	var elementsArray = [];
	
	$(xml).find("ExportUser").each(function(){
		elementsArray.push($(this).text());
	});
	return elementsArray;
}
	
