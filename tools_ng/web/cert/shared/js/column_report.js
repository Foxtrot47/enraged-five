var reportData;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();	
}

function generateReport() {
	//$("#report").empty();

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if(!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
		
		// Pass back the request object to the original config page for result manipulation
		if (reportOptions.storePValues) {
			reportOptions.storePValues(pValues);
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
				"json",
				config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, populateReport);
	}
}

function populateReport(data, tab) {
	$("#content-body").empty();
	
	//if (reportData.length == 0) {
	//	Sexy.alert(config.noDataText);
	//	return;
	//}
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
	
	if (reportOptions.reportTabs && !tab)
		tab = reportOptions.reportTabs[0].value;
	
	if (reportOptions.reportTabs) {
		var ul = $("<ul />").addClass("tab-nav");
		
		$.each(reportOptions.reportTabs, function(i, rTab) {
			ul.append(
				$("<li />")
					.text(rTab.key)
					.attr("title", rTab.key)
					.addClass("hand")
					.addClass(function() {return (tab == rTab.value) ? "active" : ""})
					.click(function() {
						populateReport(null, rTab.value)}
					)
			)
		});
		
		ul.appendTo($("#content-body"));
	}
	
	// run the function to regenerate the ReportArrayItems based on the results
	if (reportOptions.reGenerateReportArrayItems)
		reportOptions.reportArrayItems = reportOptions.reGenerateReportArrayItems(reportData);
	
	if (reportOptions.reportArrayItems) {
		var table = $("<table />");
		
		table.append(
			$("<thead>")
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title")
								.text(reportOptions.columnText)
						)
				)
		);
		$.each(reportData, function(key, value) {
			table.find("thead tr")
				.append(
					$("<th>")
						.addClass("title")
						.text(key)
				);
		});
		
		$.each(reportOptions.reportArrayItems, function(i, item) {
			var tr = $("<tr>");
			
			tr.append(
				$("<td>")
					.addClass("label")
					.text(item.title)
				);
			
			$.each(reportData, function(key, prop) {
				tr.append(
					$("<td>")
						.html(item.getValue(prop, tab))
				);
				
			});
			
			tr.appendTo(table);
			
		});
		
		table.appendTo("#content-body");
		
	} // End of if (reportOptions.reportSummaryItems)
	
} // End of populateReport

