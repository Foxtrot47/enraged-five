//this code was found online and used only for testing, need to create a library with svg map controls

function svg_interact(svg,p) {

    return new svg_interactObj(svg,p);
}

function svg_interactObj(svg,p) {  
    var p = p ? p : {},
        zoom_speed = p.zoom_speed ? p.zoom_speed : 1.15,
        viewBox = svg[0][0].viewBox.baseVal;
        panning = null, 
        current_mouse = null;
        
    /* Panning moves the viewbox */

    function mousemove(){
    	//console.log(current_mouse);
        current_mouse = d3.svg.mouse(this);
        if (panning)  {
            viewBox.x += (panning[0] - current_mouse[0]);
            viewBox.y += (panning[1] - current_mouse[1]);
            
            updateViewBoxAttr(viewBox);
        }
    };
    
    function mousedown() {
    	panning = d3.svg.mouse(this);
    }
    
    /* Zoom with mousewheel - keeping mouse position in same location*/

    function wheel(event) {
    	var delta = 0;
        //if (!event) event = window.event;
    	if (event.wheelDelta) {
    		delta = event.wheelDelta/120; 
    	} 
        else if (event.detail) {
        	delta = -event.detail/3;
        }
        move = (delta<0) ? -delta * zoom_speed : 1/(delta*zoom_speed);
        //console.log(move);
        	
        viewBox.x = (current_mouse[0] - (current_mouse[0]-viewBox.x) * move);
        viewBox.y = (current_mouse[1] - (current_mouse[1]-viewBox.y) * move); 
        viewBox.height = viewBox.height * move;
        viewBox.width = viewBox.width * move;
        
        updateViewBoxAttr(viewBox);
    };
    
    // Webkit fix - otherwise the viewbox is't always picking up the changes and zoom/pan fails
    function updateViewBoxAttr(viewBox) {
    	svg[0][0].setAttribute("viewBox", viewBox.x + " " + viewBox.y + " " + viewBox.width + " " + viewBox.height);
    }
    
    if (navigator.userAgent.toLowerCase().indexOf("firefox") > -1)
    	svg[0][0].addEventListener("DOMMouseScroll", wheel, false);
    else
    	svg[0][0].addEventListener("mousewheel", wheel, false);
    svg[0][0].onmousewheel = wheel;
    svg.onmousewheel = wheel;
    
    svg.on("mousemove", mousemove);
    svg.on("mousedown", mousedown);
    
    d3.select(window).on("mouseup", function () { panning = null;})
    svg[0][0].ondragstart = function() { return false }  // Firefox fix

    return svg;
}