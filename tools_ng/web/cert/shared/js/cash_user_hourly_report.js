var reportData = [];

var rosPlatformsDict = getRosPlatformsDict();
var matchTypesDict = getMatchTypesDict();
var jobResultsDict = getJobResultsDict();

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();
}

function generateReport() {
	$("#content-body").empty();

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if(!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else {
					value = paramObj.value;
				}
				pValues.Pairs[paramObj.key] = value;
			});
		}
		
		if (reportOptions.storePValues) {
			reportOptions.storePValues(pValues);
		}
	
		var callbackFunc = ((reportOptions.getSelectableTable) 
				? populateUserTable 
				: populateSelectedReport);
		
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
				"json",
				config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, callbackFunc);
	}
}

function populateUserTable(data) {
	$("#content-body").empty();
	
	if (data.length == 0) {
		Sexy.alert(config.noDataText);
		return;
	}
	
	// Title Table
	$("<table />")
		.addClass("title-only")
		.append(
			$("<tr>")
				.append(
					$("<th>")
					.addClass("title")
					.text("Top Users")
			)	
		)
		.appendTo("#content-body");
	
	// Data Table
	var table = $("<table />").addClass("tablesorter top-users"); 
	table.append(
		$("<thead>")
			.append(
				$("<tr>")
			)
	);
	
	var tr = table.find("tr");

	tr.append(
		$("<th>").text("Rank")
    )
	.append(
		$("<th>").text("Gamer Handle")
	)
	.append(
		$("<th>").text("Platform")
	)
	.append(
		$("<th>").text("RP")
	);
	
	var tbody = $("<tbody>");

	$.each(data, function(i, d) {
		var tr = $("<tr>");
		tr.append(
			$("<td>")
				.text(i+1)
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		tr.append(
			$("<td>")
				.text(d.GamerHandle)
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.append(
			$("<td>")
				.text(rosPlatformsDict[d.Platform])
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.append(
			$("<td>")
				.text(commasFixed2(d.XP))
				.attr("title", "Click to get user's cash report")
				.addClass("hand")
				.click(function() {
					populateSelectedReport([d]); 
				})
		)
		.appendTo(tbody);
	});
						
	tbody.appendTo(table);
	table.tablesorter();
	table.appendTo("#content-body");
	
	//populateSelectedReport(data);
}

function populateSelectedReport(selectedUsers) {	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	var endpointsArray = reportOptions.selectedEndpoints(pValues, selectedUsers);
	
	var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
	
	setTimeout(function() {
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}, 500);
	
}

function populateReport(data) {
	$("#content-body").empty();
	
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
	
	if (reportData.length == 0) {
		Sexy.alert(config.noDataText);
		return;
	}
		
	// Store the max row spans for each row
	var rowMaxSpan = [];
	reportData.map(function(r) {
		// Sort the data first by hour
		r.Data.sort(function(a, b) { return (a.GameplayHour < b.GameplayHour) ? -1 : 1});
		
		// b*#1640644 - fill the gap hours with default values
		var DataCopy = [];
		var counter = 0; // using this for counting the current GameplayHour in the new default including array
		$.each(r.Data, function(i, d) {
			//console.log(d.GameplayHour, counter);
			
			if (d.GameplayHour > counter) {
				for (j=0; j<(d.GameplayHour-counter); j++) {
					counter++;
					DataCopy.push({
						//CashBalance: d.CashBalance,
						CashSpent: 0,
						CashSpentDetails: [],
						CashEarned: 0,
						CashEarnedDetails: [],
						XPEarned: 0,
						Rank: (r.Data[i-1]) ? r.Data[i-1].Rank : 0,
						//XPTotal: d.XPTotal,
						XPEarned: 0,
						XPEarnedDetails: 0, 
						Matches: [],
						Unlocks: [],
					});
				}
			}
			
			DataCopy.push(d);
			
			/*
			if (!rowMaxSpan[d.GameplayHour])
				rowMaxSpan[d.GameplayHour] = d.Matches.length;
			else
				rowMaxSpan[d.GameplayHour] = (rowMaxSpan[d.GameplayHour] > d.Matches.length)
			*/
			counter++;
		});
		//r.Data = $.extend(true, {}, DataCopy);
		r.Data = DataCopy;
		
		r.Data.map(function(d, i) {
			// Find the maximum matches number of the hour
			if (!rowMaxSpan[i])
				rowMaxSpan[i] = d.Matches.length;
			else
				rowMaxSpan[i] = (rowMaxSpan[i] > d.Matches.length) ? rowMaxSpan[i] : d.Matches.length;
		})
		
	});
	//console.log(rowMaxSpan);
	
	var table = $("<table />").addClass("cash-report");
	var thead = $("<thead>"); //.addClass("fixed");
	var trhead = $("<tr>");
	
	trhead.append(
		$("<th>")
			.addClass("invisible")
			.addClass("black-border-right")
	);
	
	$.each(reportData, function(userIndex, userData) {
		trhead.append(
			$("<th>")
				.addClass("title")
				.addClass("black-border-right")
				.attr("colspan", 16)
				.text(userData.GamerHandle + " ( " + rosPlatformsDict[userData.Platform]
					+ " - ID " + userData.CharacterId
					+ " - SLOT " + userData.CharacterSlot
					+ " )"
				)
		)
	});
	trhead.appendTo(thead);
	
	var trhead2 = $("<tr>");
	trhead2.append(
		$("<th>")
			.addClass("invisible")
			.addClass("black-border-right")
	);
	$.each(reportData, function(userIndex, userData) {
		trhead2.append(
				$("<th>")
					.addClass("title2")
					.attr("colspan", 3)
					.text("Cash")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.attr("colspan", 3)
						.text("RP")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.attr("colspan", 3)
						.text("Jobs")
				)
				.append(
					$("<th>")
						.addClass("title2")
						.addClass("black-border-right") 
						.attr("colspan", 7)
						.text("Unlocks")
				)
	});
	trhead2.appendTo(thead);
	
	var trhead3 = $("<tr>");
	trhead3.append(
		$("<th>")
			.addClass("title3")
			.addClass("black-border-right")
			.text("Hour")
		);
	$.each(reportData, function(userIndex, userData) {
		trhead3
			.append(
				$("<th>")
					.addClass("title3")
					.text("Cash Balance")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Cash Spent")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Cash Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Rank")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("RP Total")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("RP Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Job Name")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("RP Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Cash Earned")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Weapons / Addons / Kit")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Phone Unlocks")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Car Mods")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Male Hair")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Female Hair")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.text("Male Clothes")
			)
			.append(
				$("<th>")
					.addClass("title3")
					.addClass("black-border-right")
					.text("Female Clothes")
			)
	});
	trhead3.appendTo(thead);
	
	thead.appendTo(table);
	
	// Loop for each row
	$.each(rowMaxSpan, function(rowIndex, rowSpan) {
		rowSpan += 1; // Increase by one to follow the Matches array where the total will be added
		
		var row = $("<tr>").addClass("black-border-top");
		
		//rowSpan = 1;	
		row.append(
			$("<td>")
				.attr("rowspan", rowSpan)
				.addClass("black-border-right")
				.addClass("td-row-label")
				//.addClass("fixed")
				.html("Hour " + (rowIndex + 1))
			);
		
		// Loop for each user
		$.each(reportData, function(userIndex, userData) {
			
			if (!userData.Data[rowIndex]) {
				row.append(
					$("<td>")
						.attr("rowspan", rowSpan)
						.attr("colspan", 16)
						.addClass("black-border-right")
						.html("")
				);
				// move to next user;
				return;
			}
		
			var totalMatchXP = 0,
				totalMatchCash = 0;
		
			userData.Data[rowIndex].Matches.map(function(m) { 
				totalMatchXP += m.XPEarned;
				totalMatchCash += m.CashEarned; 
			});
		
			userData.Data[rowIndex].Matches.push({
				"Name" : "TOTAL: ",
				"XPEarned" : totalMatchXP,
				"CashEarned" : totalMatchCash,
			});
			
			/*
			var XPTotal = (((rowIndex > 0) && userData.Data[rowIndex-1]) ? userData.Data[rowIndex-1].XPTotal : 0) 
							+ userData.Data[rowIndex].XPEarned;
			var CashBalance = (((rowIndex > 0) && userData.Data[rowIndex-1]) ? userData.Data[rowIndex-1].CashBalance : 0) 
									+ (userData.Data[rowIndex].CashEarned - userData.Data[rowIndex].CashSpent);
			*/
			var XPTotal = ((rowIndex > 0) ? userData.Data[rowIndex-1].XPTotal : 0) 
							+ userData.Data[rowIndex].XPEarned;
			var CashBalance = ((rowIndex > 0) ? userData.Data[rowIndex-1].CashBalance : 0) 
							+ (userData.Data[rowIndex].CashEarned - userData.Data[rowIndex].CashSpent);
			
			userData.Data[rowIndex]["XPTotal"] = XPTotal;
			userData.Data[rowIndex]["CashBalance"] = CashBalance;
		
			row.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.html(cashCommasFixed(userData.Data[rowIndex].CashBalance))
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].CashSpentDetails.length > 0) {
									return userData.Data[rowIndex].CashSpentDetails.reduce(
											function(a, b) {
												return (a + cashCommasFixed(b.Amount) 
														+ " (" + b.Action + ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(cashCommasFixed(userData.Data[rowIndex].CashSpent))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].CashEarnedDetails.length > 0) {
									return userData.Data[rowIndex].CashEarnedDetails.reduce(
											function(a, b) {
												return (a + cashCommasFixed(b.Amount) 
														+ " (" + b.Action + ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(cashCommasFixed(userData.Data[rowIndex].CashEarned))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.html(userData.Data[rowIndex].Rank)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.html(commasFixed2(userData.Data[rowIndex].XPTotal))
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks td-left")
							.html(function() {
								if (userData.Data[rowIndex].XPEarnedDetails.length > 0) {
									return userData.Data[rowIndex].XPEarnedDetails.reduce(
											function(a, b) {
												return (a + commasFixed2(b.Amount) 
														+ " (" + b.Action + " - " + b.Category +  ") " + "<br />");
											}, "")
								}
							})
					)
					.append(
						$("<div>")
							.html("<br/>")
							.append(
								$("<span>").addClass("td-sum")
									.html("Summary: ")
							)
							.append(
								$("<span>").addClass("td-right")
									.html(commasFixed2(userData.Data[rowIndex].XPEarned))
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.addClass((userData.Data[rowIndex].Matches.length == 1) ? "td-sum" : "")
					.html(
							(userData.Data[rowIndex].Matches[0]) 
								? constructMatchname(userData.Data[rowIndex].Matches[0])
								: ""
							)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.html(userData.Data[rowIndex].Matches[0].XPEarned)
			)
			.append(
				$("<td>")
					.attr("rowspan", (userData.Data[rowIndex].Matches.length == 1) ? rowSpan : 1)
					.html(cashCommasFixed(userData.Data[rowIndex].Matches[0].CashEarned))
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 0 && b.UnlockType<= 2) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 3) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>")
					//.addClass("black-border-right")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 4) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Male Hair - 6
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 6) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Female Hair - 7
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType == 7) ?
										(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") // Male Clothes - 10-17
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 10 && b.UnlockType <= 17) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			)
			.append(
				$("<td>") //Female Clothes - 18-25
					.addClass("black-border-right")
					.attr("rowspan", rowSpan)
					.append(
						$("<div>").addClass("unlocks")
							.html(
								userData.Data[rowIndex].Unlocks.reduce(function(a, b) {
									return (b.UnlockType >= 18 && b.UnlockType <= 25) ?
											(a + b.Name + "<br />") : a;
								}, "")
							)
					)
			);
			
		
		}); // End of each reportData
		
		// Add the basic row
		row.appendTo(table);
		
		// This is tricky, the jobs columns have different number of rows across users
		// Continue for the remaining extra span rows
		
		for (var j=1; j<rowSpan; j++) { // skip the first element, already displayed in the basic row
		
			var extraRow = $("<tr>");
			// Loop again through users ...
			$.each(reportData, function(userIndex, userData) {
				if (!userData.Data[rowIndex])
					return;
				
				match = userData.Data[rowIndex].Matches[j];
				if (match) {
							
				// calc the difference for the total row cells rowspan
				rowSpanDiff = rowSpan - userData.Data[rowIndex].Matches.length + 1;
		
				extraRow
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.addClass((j == userData.Data[rowIndex].Matches.length-1) ? "td-sum" : "")
						.html(function () {return constructMatchname(match)})
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.html(commasFixed(match.XPEarned))
					)
					.append(
						$("<td>")
						.attr("rowspan", (j == userData.Data[rowIndex].Matches.length-1) ? rowSpanDiff : 1)
						.html(cashCommasFixed(match.CashEarned))
					);
				}
				
			}); //End users loop
			
			extraRow.appendTo(table);
			
		} // End of FOR LOOP (j<rowSpan)
	
	}); // End of each rowMaxSpan (rowIndex, rowSpan)
	
	
	table.appendTo("#content-body");
}

function constructMatchname(match) {
	return (match.Name 
			+ ((matchTypesDict[match.Type]) ? (" (" + matchTypesDict[match.Type] + ")") : "")
			+ ((jobResultsDict[match.Result]) ? (" - " + jobResultsDict[match.Result]) : "")
			+ ((match.MaxSurvivalWave && (match.Type == 3)) ? (", " + "Wave " + match.MaxSurvivalWave) : "")
			+ ((match.Rank && (match.Type == 2 || match.Type == 1)) ? (", Position " + match.Rank) : "")
			+ ((match.Players) ? (", " + match.Players + " Players") : "")
	);
}
