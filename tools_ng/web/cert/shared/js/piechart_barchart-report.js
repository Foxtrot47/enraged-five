// reportOptions variable comes from respective config file

// Array that stores objects for each element returned via rest
var chartsData;

var NVGraphs = new NVD3CustomGraphs();

var mainDOMId = "piechart";
var breakdownDOMId = "breakdown";

var barHeight = 15;

var lastSliceClicked = null;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content")
	.append(
		$("<div>")
			.attr("id", "friendlier-names-field")
			.append(
				$("<input>")
					.attr("id", "friendlier-names")
					.attr("type", "checkbox")
					.attr("disabled", true)
			)
			.append(
				$("<label>")
					.attr("for", "friendlier-names")
					.attr("title", "Check to Switch to Friedlier Names")
					.text("Friendlier Names")
			)	
	);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", mainDOMId)
				//.addClass((reportOptions.isClickable) ? "clickable" : "")
		)
		.append(
			$("<div>").attr("id", breakdownDOMId)
		);
	
	initSvg(mainDOMId);
	
	if (reportOptions.hasFriendlierNames) {
		$("#friendlier-names").attr("disabled", false);
		$("#friendlier-names").prop("checked", true);
		$("#friendlier-names").click(function() {
			if (chartsData)
				typewatch(drawCharts, 500);
		});
	}
	
	if (reportOptions.description)
		$("#content-description").html(reportOptions.description);
	
	$("#filter").click(function() {
		generateCharts();
	});
	

	generateCharts();
}

function generateCharts() {
	chartsData = [];
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
			
} //end of generateCharts()
function cleanCharts() {
	cleanGraph(mainDOMId);
	cleanDetailedChart();
}

function cleanDetailedChart() {
	//$("#" + breakdownItemDOMId).hide("fade", {}, "normal");
	cleanGraph(breakdownDOMId);
	lastSliceClicked = null;
}

function drawCharts(data) {
	//cleanDetailedChart();
	//$("#detailed-piechart").hide("fade", {}, "normal");
	
	// If a processFunction function is specified
	if (data) {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
		
	var graphOptions = reportOptions.main;
	
	var datum = [{
		key: graphOptions.getPieLabel(chartsData),
		values: graphOptions.getValuesArray(chartsData),
		metadata: graphOptions.getMetadata(chartsData),
	}];
	
	var clickFunction = (reportOptions.isClickable) ? 
			(function(event) { 
				lastSliceClicked = event.label;
				event.point["label"] = event.label;
				event.point["color"] = event.color;
				drawDetailedChart(event.point);
			})
			: null;
	
	NVGraphs.drawPiechart(datum, mainDOMId, graphOptions, clickFunction);
	/*
	NVGraphs.drawPiechart(datum, mainDOMId, graphOptions, 
			function(event) { 
				if (reportOptions.isClickable) {
					lastSliceClicked = event.label;
					event.point["label"] = event.label;
					event.point["color"] = event.color;
					drawDetailedChart(event.point);
					}
				}
	);
	*/
	
	if (!reportOptions.isClickable)
		drawDetailedChart(chartsData);
	
	if (lastSliceClicked) {
		var availableLabels = datum[0].values.map(function(d) { return graphOptions.getName(d); });
		var searchIndex = availableLabels.indexOf(lastSliceClicked);
		
		if (searchIndex != -1) {
			drawDetailedChart(datum[0].values[searchIndex]);
		}
		else {
			// clean it cause the previous selected item is not available
			cleanDetailedChart();
		}
	}
				
}

function drawDetailedChart(selectedObject) {
	// Construct the chart data in a format that nvd3 understands
	
	var graphOptions = reportOptions.breakdown;	
	var detailedObject = graphOptions.getObject(selectedObject);

	var elementId = breakdownDOMId + "_0"; // will that support multiple?
	
	// This will add the svg on the first time
	if ($("#" + breakdownDOMId).find("svg").size() == 0) {
		$("#" + breakdownDOMId)
			.addClass("barchart-breakdown")
			.append(
				$("<div>")
					.attr("id", elementId)
			);
		
		initSvg(elementId);
	}
	
	var datum = [{
		key: graphOptions.getLabel(detailedObject),
		values: graphOptions.getValuesArray(detailedObject),
		color: graphOptions.getColor(detailedObject),
		metadata: graphOptions.getMetadata(detailedObject),
	}];
	
	var graphHeight = (barHeight * datum[0].values.length); // pixels
	var containerHeight = $("#" + breakdownDOMId).height();

	if (graphHeight < containerHeight) {
		$("#" + elementId).css("height", containerHeight);
		$("#" + elementId + " svg").css("height", containerHeight);
	}
	else {
		$("#" + elementId).css("height", graphHeight);
		$("#" + elementId + " svg").css("height", graphHeight);
	}

	NVGraphs.drawHorizBarchart(datum, elementId, graphOptions);
}
