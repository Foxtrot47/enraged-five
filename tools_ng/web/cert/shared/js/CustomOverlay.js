/*
 * Class CustomOverlay
 * 
 * Creates a custom overlay with given html content;
 * Requires JQuery, blockUI, generic.js and CustomOverlay.css.
 * 
 * @elementId - the html DOM element id for the overlay
 * @width - optional  width of the overlay
 * @height - optional  height of the overlay
 */

var CustomOverlay = function(elementId, width, height) {
	var _defaultWidth = "60%",
		_defaultHeight = "70%";

    var _elementId = elementId,
    	_width = width,
    	_height = height;
    
    //$("body").append("<div class='overlay' id='" + elementId + "'></div>");
    $("body").append(
    		$("<div>")
    			.addClass("overlay")
    			.attr("id", elementId)
    		)
    
    var _element = $("#" + _elementId);
    var close = (0)
    var width = (_width) ? _width : _defaultWidth;
    var height = (_height) ? _height : _defaultHeight;
    
    _element.css("width", width);
    _element.css("height", height);  
    
    function showOverlay(content) {
    	blockUIOnly();
    	// For disabling the scrolling underneath
    	$("body").addClass("no-scroll");
    	// Reposition before showing
    	updatePosition();
    	_element
    		.empty()
    		.append(
    			$("<div>")
					.addClass("overlay-close")
					.attr("title", "Close")
					.on("click", hideOverlay)
			)
            .append(content);
    	
    	$(document).keyup(function(e) {
    		//console.log("pressed");
			if (e.keyCode == 27) {hideOverlay();}
		})
		
        _element.fadeIn("fast");
    }

    function updatePosition() {
    	_element.css("left", $(window).width()/2 - width/2);
        _element.css("top", $(window).height()/2 - height/2);
    }
    
    function hideOverlay(){
    	_element.fadeOut();
    	
    	// Re-enable scrolling
    	$("body").removeClass("no-scroll");
    	
    	$(document).unbind("keyup");
    	
    	unBlock();
    }
    
    return {
    	showOverlay: showOverlay,
    	hideOverlay: hideOverlay,
    };

};
