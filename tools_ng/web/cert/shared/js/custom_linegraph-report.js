var graphClass = new LinesGraph();
// Array that stores objects for each element returned via rest
var reportData = [];

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body").append(
		$("<div>").attr("id", reportOptions.elementId)
	);
	
	initSvg(reportOptions.elementId);
	
	$("#content-description")
		.append(
			$("<input>")
				.attr("type", "checkbox")
				.attr("id", "locked-scales")
				.val("")
		)
		.append(
			$("<label>")
				.attr("for", "locked-scales")
				.text("Lock Scales  ")
		);
	
	$("#filter").click(function() {
		generateReport();
	});
	generateReport();
}

function generateReport() {

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if(!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}
	else {
	
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, populateReport);
	}
	
} //end of generateReport()

function populateReport(data) {
	
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
		
	drawGraph();
}

function drawGraph() {
	if (reportOptions.secondAxisKeys)
		var secondAxisKeys = reportOptions.secondAxisKeys;
	
	var lockedScales = ($("#locked-scales").is(":checked"));
		
	graphClass.draw(reportData, reportOptions, reportOptions.elementId, "content-body", lockedScales, secondAxisKeys);
}
