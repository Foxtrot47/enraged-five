// reportOptions variable comes from respective config file

var tableauDOMId = "tableauViz";
var tableauProdSuffix = "_0/";
var workbook, activeSheet;
var filterReplace = tableauSoftware.FilterUpdateType.REPLACE;
var filterAll = tableauSoftware.FilterUpdateType.ALL;
var platforms, builds, gamertags, startDate, endDate;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body")
		//.css("overflow", "hidden")
		.append(
			$("<div>").attr("id", tableauDOMId)
		);
	
	$("#filter").click(function() {
		//generateCharts();
		filterCharts();
	});
	
	generateCharts();
}

function generateCharts() {
	var url = reportOptions.tableauEndpoint 
			+ reportOptions.tableauReport.replace("/", (config.serverType == config.serverTypeProd) ? tableauProdSuffix : "/");

	// Add a link to the original tableau page
	$("#content-description")
		.append(
			$("<a>")
				.attr("href", url)
				.attr("target", "_blank")
				.attr("title", "View on Tableau Server")
				.text(url)
		);
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	if (headerAndFilters)
		var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var placeholderDiv = document.getElementById(tableauDOMId);

	var options = {
		width: placeholderDiv.offsetWidth,
		height: placeholderDiv.offsetHeight,
		hideTabs: true,
		hideToolbar: true,
		onFirstInteractive: function () {
			workbook = viz.getWorkbook();
  			activeSheet = workbook.getActiveSheet();
  			filterCharts();
		},
	};
	
	//viz = new tableauSoftware.Viz(placeholderDiv, url + "/" + ticket + "/views/ToolsTest/VehicleDash", options);
	viz = new tableauSoftware.Viz(placeholderDiv, url, options);
}

function filterCharts() {
	if (!headerAndFilters)
		return;
		
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	builds = (pValues.Pairs["BuildIdentifiers"]) ? pValues.Pairs["BuildIdentifiers"].split(",") : null;
	platforms = (pValues.Pairs["PlatformNames"]) ? pValues.Pairs["PlatformNames"].split(",") : null;
	gamertags = (pValues.Pairs["GamerHandlePlatformPairs"]) 
		? pValues.Pairs["GamerHandlePlatformPairs"].split(",").map(function(d) {return d.split("|")[0].substring(1)}) 
		: null;
	gamertags = (pValues.Pairs["GamerGroups"]) 
		? getGamertagsFromGroups(pValues.Pairs["GamerGroups"].split(","))
										.map(function(d) {return d.split("|")[0].substring(1)})
		: gamertags;
		
	startDate = pValues.Pairs["StartDate"];
	endDate = pValues.Pairs["EndDate"];
		
	if (activeSheet.getWorksheets) { // If it's a dashboard it implements getWorksheets()
		
		//activeSheet.changeSizeAsync({
	    //    behavior: tableauSoftware.SheetSizeBehavior.AUTOMATIC
	   // });
		
		$.each(activeSheet.getWorksheets(), function(i, sheet) {
			filterTableauSheet(sheet);
		});
	}
	else if (activeSheet.applyFilterAsync) { // If it's a sheet it implements applyFilterAsync()
		filterTableauSheet(activeSheet);
	}
}

function filterTableauSheet(sheet) {
	//sheet.getFiltersAsync().then(function(d) { console.log(d); });
	
	//workbook.activateSheetAsync(sheet.getName())
	sheet.selectMarksAsync()
		.then(function () {
			return (builds) 
					? sheet.applyFilterAsync("Builds", builds, filterReplace)
					: sheet.applyFilterAsync("Builds", "", filterAll);
		})
		.then(function () {
			return (platforms) 
					? sheet.applyFilterAsync("Platforms", platforms, filterReplace)
					: sheet.applyFilterAsync("Platforms", "", filterAll);
		})
		.then(function () {
			return (gamertags) 
					? sheet.applyFilterAsync("Gamertags", gamertags, filterReplace)
					: sheet.applyFilterAsync("Gamertags", "", filterAll);
		})
		.then(function () {
			
			return sheet.applyRangeFilterAsync("Dates", {
					min: new Date(startDate),
					max: (endDate) ? (new Date(endDate)) : new Date(new Date().getFullYear()+1, 0, 1),
				})
		});
}

