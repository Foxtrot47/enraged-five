// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-status", 	// offline filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // builds
	     false, // game-types
	     false, // dates
	     true, // gamer-groups
	     false, // gamers-csv
	     ],
};

var rosPlatformsDict = getRosPlatformsDict();

var onText = "Online",
	offText = "Offline";

var reportOptions = {
	restEndpoint: config.onlineGamers,
	restEndpointAsync: config.onlineGamersAsync,
	
	processFunction: filterGamers,

	legendText: "Status List",

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Gamer", getValue: function(d) {return d.Gamer;} },   
	    {
	    	title: "Status", 
	    	getValue: function(d) {return d.Status;},
	    	getClass: function(d) {return (d.Status == offText) ? "red" : "green";}
	    },
	],
	
	/* Groups */
	groups: [
	    {title: "Group's Gamers", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	hasReportArrayItemMoreInfo: false,
};

function filterGamers(data) {
	if (!(window.File && window.FileReader && window.FileList)) {
	 	$("#gamer-csv").attr("disabled", true);
		Sexy.alert("Your browser doesn't suppurt The File APIs, you will not be able to upload csv files!");
		return;
	}
	
	
	if ((data.length > 0) && (data[0]).hasOwnProperty("Status")) {
		// This is when the callback is called after the process of the data
		return data;
	}
	
	// Exit if no file is selected
	if ($("#gamer-csv").prop("files").length == 0) {
		Sexy.alert("You need to select a file first");
		return;
	}
		
	// Build a dictionary with the online users for efficient look up
	var onlineUsers = {};
	$.each(data, function(i, onlineUser) {
		onlineUsers[onlineUser.GamerHandle + "|" + rosPlatformsDict[onlineUser.Platform]] = onlineUser;
	});
	
	var validFileTypes = ["application/vnd.ms-excel", "text/plain"];
	var finalUsers = [];
	
	try {
		var file = $("#gamer-csv").prop("files")[0];
		
		if (validFileTypes.indexOf(file.type) == -1) {
			Sexy.error("Invalid File type, try csv or txt files");
			return;
		}
		
		fr = new FileReader();
	
		var loadComplete = false;
		fr.onload = function () {
			var csvObjects = $.csv.toObjects(this.result);
			
			$.each(csvObjects, function(i, csvGamer) {
				
				$.each(csvGamer.PS3.split(","), function(j, p) {
					if (onlineUsers.hasOwnProperty(p.trim() + "|PS3")) {
						csvGamer["Status"] = onText;
						return;
					}
				});
							
				$.each(csvGamer.Xbox360.split(","), function(j, p) {
					if (onlineUsers.hasOwnProperty(p.trim() + "|Xbox360")) {
						csvGamer["Status"] = onText;
						return;
					}
				});
				
				$.each(csvGamer.Win.split(","), function(j, p) {
					if (onlineUsers.hasOwnProperty(p.trim() + "|Win")) {
						csvGamer["Status"] = onText;
						return;
					}
				});
				
				// Set offline if no gamertags were online
				if (!csvGamer.hasOwnProperty("Status"))
					csvGamer["Status"] = offText;
					
				finalUsers.push(csvGamer);
			});
		
			populateReport(finalUsers);

		};
		
		fr.readAsText(file);
	}
	catch(e) {
		console.log(e);
	}
	
	return finalUsers;
}
