var reloadInterval = 30000; // in millisecs
//var reloadInterval = 10000; // in millisecs
var prioritiseAdminUrl = "/automation.svc/admin/prioritise",
	skipAdminUrl = "/automation.svc/admin/skip",
	reprocessAdminUrl = "/automation.svc/admin/reprocess",
	notProcessedYear = 9999;
	
function toDate(s) {

	if (!s) {
        return null;
    }

    var toDateRe = new RegExp("^/Date\\((\\-?\\d+)[\\+\\d]*\\)/$");
    var unixtime = Number(s.replace(toDateRe, "$1"));
    unixtime = (unixtime > 0) ? unixtime : 0;
    
    return (new Date(unixtime));
};

function getLocaleDatetimeString(date, separator) {
	return (date.toLocaleDateString() + separator + date.toLocaleTimeString());
}

function getSortableDatetimeString(date, separator) {
	return ((date.getFullYear() != notProcessedYear) ? (date.toDateString() + separator + date.toLocaleTimeString()) : "");
	//return (date.toDateString() + separator + date.toLocaleTimeString());
}

// Return the difference of two date objects in secs
function datesDiffInSecs(date1, date2) {
	return (((date1.getFullYear() == notProcessedYear) || (date1.getTime() < date2.getTime()))) ? 0 : ((date1.getTime() - date2.getTime()) / 1000);
}

function formatSecs(secs) {
	var zeros = d3.format("02d");
	
	var h = zeros(Math.floor(secs / 3600));
	var m = zeros(Math.floor((secs % 3600) / 60));
	var s = zeros(Math.floor((secs % 3600) % 60));
	
	return (h + ":" + m + ":" + s);
}

function getChangelistLink(changelist) {
	var link;
	var urlPrefix = "http://rsgedip4s1:8080/@md=d&cd=//&c=7FM@/";
	var urlSuffix = "?ac=10";
	
	if (changelist) {
		link = $("<a />" , {
			text: changelist,
			title: "View Changelist on Perforce Web",
			href: urlPrefix + changelist + urlSuffix,
			target: "_blank",
		}).prop("outerHTML");
	}
	else
		link = " ";	
	
	//console.log(link);
	return link;
}

function addJob(guid) { 

	if( $("#jobpage" + guid).length === 0) {

		//create the html for the tab
		var newjobpage = $("#jobpage").clone();
		newjobpage[0].id = "jobpage" + guid;
		$("#tabs").append(newjobpage);

		var jobid = "#" + newjobpage[0].id;

		$("#tabs").tabs("add", jobid, "Job " + guid + "<span id='" + jobid 
						+ "' class='ui-icon ui-icon-close'>Remove Tab</span>");
		
		$("#tabs span.ui-icon-close").click(function(e) {
			e.preventDefault(); // Don't add the job id to the url
			
			/* Old buggy code
			var index = $( "li", $("#tabs") ).index( $( this ).parent() );
			$("#tabs").tabs( "remove", index );
			*/
			
			// Remove the panel's page which will become hidden
			$(jobid).remove($(this).attr("id"));
			// Remove the tab (html li)
			$(this).closest("li").remove();
			
			// Refresh the tabs and select the last one (doesn't always work (why?))
			$("#tabs").tabs("refresh");
			$("#tabs").tabs("select", getNumberOfTabs() - 1);			
		});		

		updateJob(guid);
		$("#tabs").tabs("select", $("#tabs").tabs("length") - 1);
	}
};

function getNumberOfTabs() {
	// Because $("#tabs").tabs("length") is not updated when a tab is removed
	return ($("#tabs ul li:visible").length);
}

function updateJob( guid ) {

	var node = $("#tabs #jobpage" + guid );
	//node.id = node.id + index;

	//currentcl = cl;	
	var job = jobJson[guid];

	var hout = "Type: " + job.__type;
	hout += "<br/>ID: " + job.ID;
	hout += "<br/><br/>Trigger: "; 
	
	if(job.Trigger.__type == "UserRequestTrigger") {
	
		hout += "User request by <span class='username'>" + job.Trigger.Username + "</span>";
		hout += " at " + getLocaleDatetimeString(toDate(job.Trigger.SubmittedAt), " ");
	
	} else {
	
		hout += "Changelist " + getChangelistLink(job.Trigger.Changelist);
		hout += " checked in by <span class='username'>" + job.Trigger.Username + "</span> (" + job.Trigger.Client + ")";
		hout += " at <i>" + getLocaleDatetimeString(toDate(job.Trigger.SubmittedAt), " ") + "</i>";
		hout += "<br/><br/>Description: " + job.Trigger.Description;
	}

	hout += "<br><br>Processing State: " + jobstateenum[job.State];

	if(job.State == 2) {

		hout += "<br/>Processing Finished At: " + getLocaleDatetimeString(toDate(job.ProcessedAt), " ");
		hout += "<br/>Processing Time: " + job.ProcessingTime;
	} else {

		hout += "<br>Priority: " + job.Priority;
	}

	hout += "<br/><br/><button id='clpri' title='Prioritise'>Prioritise</button>";
	hout += "&nbsp;<button id='clskip' title='Skip'>Skip</button>";
	hout += "&nbsp;<button id='clforce' title='Force'>Force</button>";
	hout += "&nbsp;<span id='action-status'></span>";
	hout += "<br/><br/>";

	node.children("#jobdetail").html(hout);
	
	var userElement = node.children("#jobdetail").find(".username");
	
	createUserInfoDiv(job.Trigger.Username, userElement);
	$(userElement).unbind().hover(function() {
		$(this).children("div.user").fadeIn("fast");
	},
	function() {
		$(this).children("div.user").fadeOut("fast");
	});
	
	$("#jobdetail button").button();
	var currentJobDiv = "#jobpage" + job.ID + " #action-status";
	
	node.find("#clpri").click(function() {

		$.ajax({
			type: "GET",
			url: prioritiseAdminUrl,
			data: {job: job.ID},
			dataType: "json",
			success: function(json) {
				$(currentJobDiv)
					.removeClass("failed").addClass("success")
					.html("'Prioritise' action for <i>" + job.ID + "</i> was sent successfully.");
			},
			error: function() {
				$(currentJobDiv)
					.html("'Prioritise' action  for <i>" + job.ID + "</i> has failed!")
					.removeClass("success").addClass("failed");
			},
			complete: function() {}
		});
	});
	// Disabling the prioritise button
	node.find("#clpri").attr("disabled", true);
	
	node.find("#clskip").click(function() {

		$.ajax({
				type: "GET",
				url: skipAdminUrl,
				data: {job: job.ID},
				dataType: "json",
				success: function(json) {
					$(currentJobDiv)
						.removeClass("failed").addClass("success")
						.html("'Skip' action for <i>" + job.ID + "</i> was sent successfully.");
				},
				error: function() {
					$(currentJobDiv)
						.html("'Skip' action for <i>" + job.ID + "</i> has failed!")
						.removeClass("success").addClass("failed");
				},
				complete: function() {
					
				}
		});		
	});
	// Disabling the skip button
	//node.find("#clskip").attr("disabled", true);
	
	//console.log(node)
	node.find("#clforce").click(function() {
		
		$.ajax({
				type: "GET",
				url: reprocessAdminUrl,
				data: {job: job.ID},
				dataType: "json",
				success: function(json) {
					$(currentJobDiv)
						.removeClass("failed").addClass("success")
						.html("'Force' action for <i>" + job.ID + "</i> was sent successfully.");
				},
				error: function() {
					$(currentJobDiv)
						.html("'Force' action  for <i>" + job.ID + "</i> has failed!")
						.removeClass("success").addClass("failed");
				},
				complete: function() {
					
				}
		});		
	});
	// Disabling the force button
	//node.find("#clforce").attr("disabled", true);
	

	/*
	var tabledata = {"aaData" : [], "aoColumns" : [], "bStateSave": true};
	tabledata.aoColumns.push({ "sTitle": "Files" });
	var table3 = node.children('#list3').dataTable(tabledata);

	table3.fnClearTable();

	$.each(job.Trigger.Files, function(i, file) {
		table3.fnAddData([file]);
	});
	*/
	
	addFilesListTable(job, node.children("#list3"));
}

// Adds a dataTable with the files of the current job to a given jQuery element
function addFilesListTable(job, element) {
	//console.log(element);
	
	var tabledata = {"aoColumns" : [], "bStateSave": true};
	tabledata.aoColumns.push({ "sTitle": "Files" });
	var table3 = element.dataTable(tabledata);

	table3.fnClearTable();
	$.each(job.Trigger.Files, function(i, file) {
		table3.fnAddData([file], false);
	});
	table3.fnDraw();
}
