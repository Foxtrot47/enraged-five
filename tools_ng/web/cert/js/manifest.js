function generateManifest() {
	initHeaderAndFilters(null); // no header
	
	var profStats = new ProfileStats();
	
	$("<tr>")
		.append(
			$("<th>")
				.addClass("title")
				.text("Page Name")
		)
		.append(
			$("<th>")
				.addClass("title")
				.text("Stats Used")
		)
		.appendTo("#manifest-table");
	
		
	$.each(navigationItems, function(i, navItem) {
		var statNames;
		if (navItem.config == null)
			return;
		else if (navItem.profile) {
			//statsText = "<i>Profile Stats: </i><br />";
			
			$.getScript(/stats/ + navItem.config, function() {
				var statsText = "";
				$.each(reportOptions.availableCharts, function(i, chart) {
					var fullStatnames = [];
					
					//console.log(chart.types);
					
					$.each(chart.types, function(i, type) {
						fullStatnames = 
							fullStatnames.concat(profStats.constructStatNames(chart, type, config.gameTypes));
					});
					//console.log(fullStatnames);
					
					statsText += ("<i>" + chart.name + "</i> (" + chart.description + ") : <br />[" 
								+ fullStatnames.join(", ") + "]<br /><br />");
				});
				populateTableRow("#manifest-table", navItem.title + "<br />(Profile_Stat)", statsText);
			});
		}
		else {
			//statNames = navItem.config;
			//populateTableRow("#manifest-table", navItem.title + "<br />(Telemetry_Stat)", statNames);
		}
		
	});
	
}

function populateTableRow(tableId, title, text) {
	$(tableId).append(
		$("<tr>")
			.append(
				$("<td>").html(title)
			)
			.append(
				$("<td>").html(text)
			)
	)
}

