var navigationTitle = "Web Tools";
var navigationItems = [
	{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	/*		
	{
		title: "Automation Client",
		href: "/automation/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	{
		title: "Statistics",
		href: "stats/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "All Profile Stats",
		href: "all-stats.html",
		hide: true,
		config: null,
		subItems: [],
	},
	{
		title: "Session Timeline",
		href: "session_timeline.html",
		hide: true,
		config: "",
		profile: false,
		subItems: [],
	},
	{
		title: "Map Telemetry",
		href: "map_telemetry.html",
		hide: false,
		config: null,
		subItems: [],
	},
	{
		title: "Map Exports",
		href: "map_exports/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Capture Stats",
		href: "capture_stats/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Asset Stats",
		href: "asset_stats/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Difficulty Tracking",
		href: "difficulty_tracking/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	/*
	{
		title: "Reusable Reports",
		href: "/reusable_reports/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	{
		title: "Social Club Admin",
		href: "social_club_admin.html",
		hide: true,
		config: "",
		profile: false,
		subItems: [],
	},
	{
		title: "Manifest",
		href: "manifest.html",
		hide: true,
		config: null,
		subItems: [],
	}
	/*
	{
		title: "Universal Log Viewer",
		href: "/ulogs/index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
];
