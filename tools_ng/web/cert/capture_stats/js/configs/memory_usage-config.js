//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // build-config
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.memoryUsageStats,
	restEndpointAsync: config.memoryUsageStatsAsync,

	enableCSVExport: true,

	// No nested array results
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Mission Name", getValue: function(d) {return d.MissionName; }},
	     {title: "Resource Name", getValue: function(d) {return d.ResourceName; }},
	     {title: "Samples", getValue: function(d) {return d.NumSamples; }},
	
	     {title: "Min Free", getValue: function(d) {return fixed(d.MinFree); }},
	     {title: "Avg Free", getValue: function(d) {return fixed(d.AvgFree); }},
	     {title: "Max Free", getValue: function(d) {return fixed(d.MaxFree); }},
	     
	     {title: "Min Peak", getValue: function(d) {return fixed(d.MinPeak); }},
	     {title: "Avg Peak", getValue: function(d) {return fixed(d.AvgPeak); }},
	     {title: "Max Peak", getValue: function(d) {return fixed(d.MaxPeak); }},
	     
	     {title: "Min Used", getValue: function(d) {return fixed(d.MinUsed); }},
	     {title: "Avg Used", getValue: function(d) {return fixed(d.AvgUsed); }},
	     {title: "Max Used", getValue: function(d) {return fixed(d.MaxUsed); }},
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
	
	// One level only
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	   {title: "Memory Usage List", regexp: "", filterItem: 0},
	],

};