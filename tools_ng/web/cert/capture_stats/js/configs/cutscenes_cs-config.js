//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs-per", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // build-config
		 false, // percentiles
		 false, // game-times
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.cutsceneCaptureStats,
	restEndpointAsync: config.cutsceneCaptureStatsAsync,
	
	enableCSVExport: true,
	
	reportSummaryTitle: null,
	
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Cutscene Name", getValue: function(d) {return d.CutsceneName; }},
	     {title: "Sample Size", getValue: function(d) {return d.SampleSize; }},
	
	     {
	    	 title: "Min FPS", 
	    	 getValue: function(d) {return (d.MaxFrameTime) ? fixed(1000 / d.MaxFrameTime) : fixed(d.MaxFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MaxFrameTime) >= 30) ? "green" : "red"); }
	     },
	     {
	    	 title: "Avg FPS", 
	    	 getValue: function(d) {return (d.AvgFrameTime) ? fixed(1000 / d.AvgFrameTime) : fixed(d.AvgFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.AvgFrameTime) >= 30) ? "green" : "red"); },
	     },
	     {
	    	 title: "Max FPS", 
	    	 getValue: function(d) {return (d.MinFrameTime) ? fixed(1000 / d.MinFrameTime) : fixed(d.MinFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MinFrameTime) >= 30) ? "green" : "red"); }
	     },
	     
	     {title: "Min Frame Time (ms)", getValue: function(d) {return fixed(d.MinFrameTime); }},
	     {title: "Avg Frame Time (ms)", getValue: function(d) {return fixed(d.AvgFrameTime); }},
	     {title: "Max Frame Time (ms)", getValue: function(d) {return fixed(d.MaxFrameTime); }},
		 
	     {title: "Min Draw Time (ms)", getValue: function(d) {return fixed(d.MinDrawTime); }},
	     {title: "Avg Draw Time (ms)", getValue: function(d) {return fixed(d.AvgDrawTime); }},
	     {title: "Max Draw Time (ms)", getValue: function(d) {return fixed(d.MaxDrawTime); }},
	     
	     {title: "Min Gpu Time (ms)", getValue: function(d) {return fixed(d.MinGpuTime); }},
	     {title: "Avg Gpu Time (ms)", getValue: function(d) {return fixed(d.AvgGpuTime); }},
	     {title: "Max Gpu Time (ms)", getValue: function(d) {return fixed(d.MaxGpuTime); }},
	
	     {title: "Min Update Time (ms)", getValue: function(d) {return fixed(d.MinUpdateTime); }},
	     {title: "Avg Update Time (ms)", getValue: function(d) {return fixed(d.AvgUpdateTime); }},
	     {title: "Max Update Time (ms)", getValue: function(d) {return fixed(d.MaxUpdateTime); }},
	     
	     {title: "Min Drawcall Count", getValue: function(d) {return fixed(d.MinDrawcallCount);} },
	     {title: "Avg Drawcall Count", getValue: function(d) {return fixed(d.AvgDrawcallCount);} },
	     {title: "Max Drawcall Count", getValue: function(d) {return fixed(d.MaxDrawcallCount);} },
	     
	     {title: "Min Object Count", getValue: function(d) {return fixed(d.MinObjectCount);} },
	     {title: "Avg Object Count", getValue: function(d) {return fixed(d.AvgObjectCount);} },
	     {title: "Max Object Count", getValue: function(d) {return fixed(d.MaxObjectCount);} },
	],
	reportArraySort: [[1, 0]], // [second item (first is +) ASC]
	
	hasReportArrayItemMoreInfo: true,
	// if the more info need to come via an extra rest call
	reportArrayItemMoreInfoRest: {
		restEndpoint: config.cutsceneCaptureDetailsStats,
		restEndpointAsync: config.cutsceneCaptureDetailsStatsAsync,
		extraParamName: "CutsceneName",
	},
	
	// Return the result array as it is
	getReportMoreInfoArray: function(d) {return d;},
	// Generate one table for the results
	hasMoreInfoMultipleTables: false,
	// No key available at the results (no multiple tables), using a hardcoded table title
	getMoreInfoKey: function(d) {return "Details"},
	// use the whole array result as well (no multiple tables)
	getMoreInfoValues: function(d) {return d},
	reportMoreInfoArrayItems: [
	    {title: "Current Frame", getValue: function(d) {return (d.CutsceneFrame);} },
	    {title: "Total Frames", getValue: function(d) {return (d.CutsceneTotalFrames);} },
	    {title: "Current Section", getValue: function(d) {return (d.CutsceneCurrentConcatSection);} },
	    {
	    	title: "FPS", 
	    	getValue: function(d) {return (d.FrameTime) ? fixed(1000 / d.FrameTime) : fixed(d.FrameTime) ;},
	    	getClass: function(d) {return (((1000 / d.FrameTime) >= 30) ? "green" : "red"); },
	    },
	    {title: "Frame Time (MS)", getValue: function(d) {return fixed(d.FrameTime);} },
	    {title: "Draw Time (MS)", getValue: function(d) {return fixed(d.DrawTime);} },
	    {title: "Gpu Time (MS)", getValue: function(d) {return fixed(d.GpuTime);} },
	    {title: "Update Time (MS)", getValue: function(d) {return fixed(d.UpdateTime);} },
	    {title: "Drawcall Count", getValue: function(d) {return (d.DrawcallCount);} },
	    {title: "Object Count", getValue: function(d) {return (d.ObjectCount);} },
   	],
   	reportMoreInfoArraySort: [[0, 0]], // [first item, ASC]
	
   	// Group the results 
	groups: [
	     //filterItem is the array index of the item for the regexp match, name is at id=0 of reportArrayItems
	     {title: "All Cutscenes", regexp: "", filterItem: 0}, 
	],

	/*
	// Graph only related vars
	*/
};


