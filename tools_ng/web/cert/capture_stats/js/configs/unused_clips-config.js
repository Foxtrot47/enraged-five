var headerAndFilters  = {
	headerType: "header-clips", // clips filtering header
	disabledFields:			 // disables header fields 
		[
		 false, // source build
		 false, // data builds
		 false, // clip categories
	     ],
};

var fixed = function(d) { return d.toFixed(2); }

var clipCategories = getClipCategories(); 

var reportOptions = {
	restEndpoint: config.unusedClipsStats,
	restEndpointAsync: config.unusedClipsStatsAsync,
		
	enableCSVExport: true,

	// No nested array results
	getReportArray: function(d) {return d},
	reportArrayItems: [
	    {title: "Dictionary Name", getValue: function(d) {return d.DictionaryName; }},
	    {title: "Category", getValue: function(d) {return clipCategories[d.Category].Name; }},
		{title: "Clip Name", getValue: function(d) {return d.ClipName; }},
		{title: "Times Played", getValue: function(d) {return d.TimesPlayed; }},
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
	
	// One level only
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	   {title: "Clip Dictionaries", regexp: "", filterItem: 0},
	],

};