var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var profileStatList = [
	{ // Extra
		name: "TOTAL_PROGRESS_MADE",
		title: "Total Progress",
		getValue: getValuePerCent,
		singleMetric: true,
	},
    {
    	name: "NUM_MISSIONS_COMPLETED",
    	title: "Missions completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_MISSIONS_AVAILABLE",
    	title: "Missions available",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_MINIGAMES_COMPLETED",
    	title: "Minigames completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    { 
    	name: "NUM_MINIGAMES_AVAILABLE",
    	title: "Minigames available",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_ODDJOBS_COMPLETED",
    	title: "Oddjobs completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    { 
    	name: "NUM_ODDJOBS_AVAILABLE",
    	title: "Oddjobs available",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_RNDPEOPLE_COMPLETED",
    	title: "Random Characters completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_RNDPEOPLE_AVAILABLE",
    	title: "Random Characters available",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_RNDEVENTS_COMPLETED",
    	title: "Random Events completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_RNDEVENTS_AVAILABLE",
    	title: "Random Events available",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_MISC_COMPLETED",
    	title: "Miscellaneous completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_MISC_AVAILABLE",
    	title: "Miscellaneous available",
    	getValue: getValue,
    	singleMetric: true,
    },
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "100% Checklist",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
			
	reportArrayItems: profileStatList.map(
		function(d) {
			d["requestSubString"] = "_";
			
			return d;
		}),
		
};
