var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
	  	true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "SHOTS",
    	title: "Total Shots",
    },
    {
    	name: "HITS_PEDS_VEHICLES",
    	title: "Total hits",
    },
    {
    	name: "KILLS",
    	title: "Total kills",
    },
    {
    	name: "HEADSHOTS",
    	title: "Headshot kills",
    },
    {
    	name: "KILLS_ARMED",
    	title: "Armed kills",
    },
    {
    	name: "KILLS_IN_FREE_AIM",
    	title: "Free aim kills",
    },
    {
    	name: "KILLS_STEALTH",
    	title: "Stealth kills",
    },
    {
    	name: "SUCCESSFUL_COUNTERS",
    	title: "Counter attacks",
    },
    {
    	name: "EXPLOSIVES_USED",
    	title: "Explosives used",
    },
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Combat",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
	

	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],		
		
	reportArrayItems: profileStatList.map(
				function(d) {
					d["requestSubString"] = "_";
					d["getValue"] = getCharValue;
					return d;
				}),
};
