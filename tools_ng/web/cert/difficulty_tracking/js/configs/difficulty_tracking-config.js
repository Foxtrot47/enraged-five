var navigationTitle = "Difficulty Tracking";
var navigationItems = [{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "User Report",
		href: "#",
		display: "inherit",
		config: "",
		subItems: [
		   {
			   title: "User Sessions",
			   href: "dt_user_report.html",
			   display: "inherit",
		   },
		   {
			   title: "User Sessions Admin",
			   href: "dt_user_report_admin.html",
			   display: "inherit",
		   },
		],
	},
	{
		title: "Comment Report",
		href: "#",
		display: "inherit",
		config: "",
		subItems: [
		   {
			   title: "General Comments",
			   href: "dt_comments_general.html",
			   display: "inherit",
		   },
		   {
			   title: "Mission Comments",
			   href: "dt_comments_missions.html",
			   display: "inherit",
		   },
		   {
			   title: "Weapon Comments",
			   href: "dt_comments_weapons.html",
			   display: "inherit",
		   },
		   {
			   title: "Vehicle Comments",
			   href: "dt_comments_vehicles.html",
			   display: "inherit",
		   },
		],
	},
	{
		title: "Mission Report",
		href: "#",
		display: "inherit",
		config: null,
		subItems: [
		    {
		    	title: "Main Story Attempts",
		    	href: "dt_mission_ms_attempts.html",
		    	display: "inherit",
		    },
		    {
		    	title: "Main Story Timings",
		    	href: "dt_mission_ms_timings.html",
		    	display: "inherit",
		    },
		    {
		    	title: "RC Attempts",
		    	href: "dt_mission_rc_attempts.html",
		    	display: "inherit",
		    },
		    {
		    	title: "RC Timings",
		    	href: "dt_mission_rc_timings.html",
		    	display: "inherit",
		    },
		    {
		    	title: "RE Attempts",
		    	href: "dt_mission_re_attempts.html",
		    	display: "inherit",
		    },
		    {
		    	title: "RE Timings",
		    	href: "dt_mission_re_timings.html",
		    	display: "inherit",
		    },
		],
	},
	{
		title: "Per User Stats",
		href: "#",
		display: "inherit",
		config: null,
		subItems: [
		    {
		    	title: "Skills",
		    	href: "dt_peruser_stats_skills.html",
				display: "inherit",
			},
			{
			  	title: "General",
			  	href: "dt_peruser_stats_general.html",
			   	display: "inherit",
			},
			{
			   	title: "Crimes",
			   	href: "dt_peruser_stats_crimes.html",
			   	display: "inherit",
			},
			{
			   	title: "Vehicles",
			   	href: "dt_peruser_stats_vehicles.html",
			   	display: "inherit",
			},
			{
			   	title: "Favourite Vehicles",
			   	href: "dt_peruser_stats_fav_vehicles.html",
			   	display: "inherit",
			},
			{
			   	title: "Favourite Missions",
			   	href: "dt_peruser_stats_fav_missions.html",
			   	display: "inherit",
			},
			{
			   	title: "Mission Ratings",
			   	href: "dt_peruser_stats_mission_ratings.html",
			   	display: "inherit",
			},
			{
			   	title: "Switches On Mission",
			   	href: "dt_peruser_switched_per_mission.html",
			   	display: "inherit",
			},
			{
			   	title: "Cash",
			   	href: "dt_peruser_stats_cash.html",
			   	display: "inherit",
			},
			 {
		    	title: "Cash at Stages",
		    	href: "dt_peruser_cash_at_stages.html",
		    	display: "inherit",
		    },
			{
			   	title: "Combat",
			   	href: "dt_peruser_stats_combat.html",
			   	display: "inherit",
			},
			{
			   	title: "Weapons",
			   	href: "dt_peruser_stats_weapons.html",
			   	display: "inherit",
			},
			{
			   	title: "Favourite Mods",
			   	href: "dt_peruser_stats_fav_mods.html",
			   	display: "inherit",
			},
			{
			   	title: "100% Checklist",
			   	href: "dt_peruser_stats_100pc.html",
			   	display: "inherit",
			},	
			{
			   	title: "Properties",
			   	href: "dt_peruser_stats_properties.html",
			   	display: "inherit",
			},
			{
			   	title: "Settings",
			   	href: "dt_peruser_stats_settings.html",
			   	display: "inherit",
			},
		],
	},
];
