var headerAndFilters = null;
/*
var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		true, // user
	],
};
*/

var platformNames = getPlatforms();

var resultNames = {
	"0": "Passed",
	"1": "Failed",
	"2": "Cancelled",
	"3": "Over",
	"4": "Unknown",
};

var reportOptions = {
	processFunction: groupByUser,
	notReportFramework: true,
	restEndpoint: config.playthroughsSessions,

	legendText: ""
				+ "User Report"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "User Sessions Admin",

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Username", getValue: function(d) {return d.UserName;} },
	    {title: "Total Sessions", getValue: function(d) {return d.Sessions.length} },
	    /* TimeSpentPlaying  is in millisecs */
	    // {title: "Total Time", getValue: function(d) {return formatSecs(d.TimeSpentPlaying/1000);} }, 
	    // {title: "Total Missions", getValue: function(d) {return (d.TotalMissionAttempts);} },
	    // {title: "Unique Missions", getValue: function(d) {return (d.UniqueMissionAttempts);} },
	    // {title: "Total Checkpoints", getValue: function(d) {return (d.TotalCheckpointAttempts);} },
	    // {title: "Unique Checkpoints", getValue: function(d) {return (d.UniqueCheckpointAttempts);} },
	],
	
	/* Groups */
	groups: [
	    {title: "Summary", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: true,
	// if the more info need to come via an extra rest call
	/*
	reportArrayItemMoreInfoRest: {
		restEndpoint: config.playthroughsBreakdown,
		restEndpointAsync: config.playthroughsBreakdownAsync,
		extraParamName: "User",
	},
	*/
	
	// Nested array data 
	getReportMoreInfoArray: function(d) {
		//blockDt();
		$.each(d.Sessions, function(i, session) {
			$.ajax({
				url: config.restHost + reportOptions.restEndpoint + "/" + session.Id + "/MissionAttempts",			
				dataType: "json",
				async: false,
				cache: false,
				success: function(result) {
					session.MissionAttempts = result;
					session.MissionAttempts.map(function(d) {
						d["SessionId"] = session.Id;
						return d;
					});
				},
			});
		});
		//unblock();
		
		return d.Sessions;
	},
	// Generate one table for each result
	hasMoreInfoMultipleTables: true,
	getMoreInfoKey: function(d) {		
		return "Session :: " + d.FriendlyName 
			+ " - " + parseJsonDate(d.Start).toLocaleString()
			+ " (" 
			+ formatSecs(d.TimeSpentPlaying/1000) // milliseconds
			+ ") :: " 
			+ d.BuildIdentifier + " :: " 
			+ platformNames[d.Platform].Name;
	},
	// get the extra info data from the array
	getMoreInfoValues: function(d) {return d.MissionAttempts},
	reportMoreInfoArrayItems: [
	    {title: "Mission Attempt", getValue: function(d) {return d.MissionIdentifier;} },
	    // {title: "Total Checkpoints", getValue: function(d) {return d.CheckpointCount;} },
	    // {title: "Checkpoint Attempts", getValue: function(d) {return d.CheckpointAttempts.length;} },
	    {title: "Result", getValue: function(d) {return resultNames[d.Result];} },
	    {title: "Started at", getValue: function(d) {return parseJsonDate(d.Start).toLocaleString(); } },
	    // TimeToComplete is in millisecs
	    {title: "Duration", getValue: function(d) {return formatSecs(d.TimeToComplete/1000);} },
	    {
	    	title: "Ignored",
	    	titleAttr: "click to toggle",
	    	getValue: function(d) { return d.Ignore; },
	    	getClass: function(d) { return ((d.Ignore) ? "red hand" : "green hand"); },
	    	onClick: function(d, td) {
	    		d.Ignore = !d.Ignore;
	    		
	    		blockDt();
	    		$.ajax({
	    			url: config.restHost + reportOptions.restEndpoint + "/" 
	    								+ d.SessionId + "/MissionAttempts/" + d.Id,
	    			type: "POST",
	    			data: JSON.stringify(d),
	    			dataType: "json",
	    			contentType: "application/json",
	    			async: true,
	    			success: function() {
	    				td.text(d.Ignore);
	    				td.removeClass();
	    				td.addClass(function() {return (d.Ignore) ? "red hand" : "green hand"; });
	    				
	    				// collapse the row
	    				var rowElement = td.parent()
	    				if (rowElement.next().hasClass("details-row")) {
	    					rowElement.next().remove();
	    					rowElement.find("td.first").text("+").attr("title", "Click to Expand");
	    				}
	    				
	    			},
	    			complete: unBlock,
	    		});
	    		
	    	},
	    }
   	],	
   	
   	/* Second level breakdown */
   	
   	hasReportArraySubItemMoreInfo: true,
	
	// No nested array data
	getReportSubItemMoreInfoArray: function(d) {
		return d;
	},
	// Generate one table for all the results
	hasSubItemMoreInfoMultipleTables: false,
	getSubItemMoreInfoKey: function() {
		return "Checkpoint Attempts";
	},
	// get the extra info data from the array
	getSubItemMoreInfoValues: function(mission) {
		
		var checkpointAttempts = [];
		blockDt();
		$.ajax({
			url: config.restHost + reportOptions.restEndpoint + "/" 
								+ mission.SessionId + "/MissionAttempts/"
								+ mission.Id + "/CheckpointAttempts",
			dataType: "json",
			cache: false,
			async: false,
			success: function(result) {
				checkpointAttempts = result;
				checkpointAttempts.map(function(d) {
					d["SessionId"] = mission.SessionId;
					d["MissionId"] = mission.Id;
					//d["MissionIgnore"] = mission.Ignore;
					return d;
				});
			},
			complete: unBlock,
		});
		
		return checkpointAttempts;
	},
	reportSubItemMoreInfoArrayItems: [
	    //{title: "Index", getValue: function(d) {return d.CheckpointIndex;} },
	    {title: "Checkpoint", getValue: function(d) {return d.CheckpointName;} },
	    {title: "Started at", getValue: function(d) {return parseJsonDate(d.Start).toLocaleString();} },
	    //{title: "End", getValue: function(d) {return (d.End) ? parseJsonDate(d.End).toLocaleString() : "";} },
	    // TimeToComplete is in milliseconds
	    {title: "Duration", getValue: function(d) {return formatSecs(d.TimeToComplete/1000);} },
	    //{title: "Comment", getValue: function(d) {return (d.Comment) ? (d.Comment) : "";} },
	    {
	    	title: "Ignored",
	    	titleAttr: "click to toggle",
	    	getValue: function(d) { return d.Ignore},
	    	getClass: function(d) { return ((d.Ignore) ? "red hand" : "green hand"); },
	    	onClick: function(d, td) {
	    		
	    		d.Ignore = !d.Ignore;
	    		
	    		blockDt();
	    		$.ajax({
	    			url: config.restHost + reportOptions.restEndpoint + "/" 
	    								+ d.SessionId + "/MissionAttempts/"
	    								+ d.MissionId + "/CheckpointAttempts/" + d.Id,
	    			type: "POST",
	    			data: JSON.stringify(d),
	    			dataType: "json",
	    			contentType: "application/json",
	    			async: true,
	    			success: function() {
	    				td.text(d.Ignore);
	    				td.removeClass();
	    				td.addClass(function() {return (d.Ignore) ? "red hand" : "green hand"; });
	    			},
	    			complete: unBlock,
	    		});
	    		
	    	},
	    }
   	],
	
};

function groupByUser(data) {
	
	var users = {};
	$.each(data, function(i, d) {
		if (users.hasOwnProperty(d.UserName))
			users[d.UserName].Sessions.push(d);
		else {
			users[d.UserName] = {
				"UserName": d.UserName,
				"Sessions": [d],
			}
		}
	});
	
	//console.log(users);
	return users;
}

// Recursive function for querying multiple async requests in a row
function queryMissionsAsync(sessions, currentIndex, ended) {
	// End condition
	if (currentIndex >= sessions.length) {
		ended = true;
		return;
	}
	
	console.log(currentIndex);
	console.log(sessions[currentIndex]);
	console.log(sessions.length);

	var session = sessions[currentIndex];
	$.ajax({
		url: config.restHost + reportOptions.restEndpoint + "/" + session.Id + "/MissionAttempts",			
		dataType: "json",
		async: false,
		cache: false,
		success: function(result) {
			session.MissionAttempts = result;
			session.MissionAttempts.map(function(d) {
				d["SessionId"] = session.Id;
				return d;
			});
			
			queryMissionsAsync(sessions, ++currentIndex, ended);
		},
	});
	
	/*
	$.each(d.Sessions, function(i, session) {
		$.ajax({
			url: config.restHost + reportOptions.restEndpoint + "/" + session.Id + "/MissionAttempts",			
			dataType: "json",
			async: false,
			cache: false,
			success: function(result) {
				session.MissionAttempts = result;
				session.MissionAttempts.map(function(d) {
					d["SessionId"] = session.Id;
					return d;
				});
			},
		});
	});
	*/
}
