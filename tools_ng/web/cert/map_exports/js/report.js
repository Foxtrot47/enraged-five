//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-map-exports", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 true, // section
	                 true, // user
	                 false, // dates
    ],
};

var platformNames = getPlatforms();

var restEndpoint = config.mapExportSummary,
	restEndpointAsync = config.mapExportSummaryAsync,
	usersThreshold = 10,
	sectionsThreshold = 20,
	exportsThreshold = 20;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();
	
	// Disable end date
	$("input#date-to").attr("disabled", true);
}

function generateReport() {	
	//$("#content table").remove().trigger("update");
	$("#content-body").empty();
	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
		
	var req = new ReportRequest(config.restHost + restEndpoint,
			"json",
			config.restHost + restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);

	req.sendSingleAsyncRequest(pValues, populateReportTables);
	
	/*
	block();
	
	$.ajax({
		url: config.restHost + restEndpoint 
			+ date.getFullYear() + "/"
			+ (parseInt(date.getMonth()) + 1) + "/"// getMonth() returns value from 0-11
			+ date.getDate() + "/" 
			+ restExportsSuffix,
		type: "GET",
		dataType: "xml",
		data: {
			hourOffset: hourOffset
		},
		success: function(xml, textStatus, jqXHR) {
			var exportsStats = calculateExportStats(xml);
			populateExportsSummaryTable(exportsStats, date);
    	},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error("Failed loading content");
		},
		complete: function() {
			$.ajax({
				url: config.restHost + restEndpoint 
					+ date.getFullYear() + "/"
					+ (parseInt(date.getMonth()) + 1) + "/"// getMonth() returns value from 0-11
					+ date.getDate() + "/" 
					+ restReportSuffix,
				type: "GET",
				dataType: "xml",
				data: {
					hourOffset: hourOffset,
					users: users,
					sections: sections,
					exports: exports
				},
				success: function(xml, textStatus, jqXHR) {
					var summaryData = convertMapExportsSummaryXml(xml);
					populateReportTables(summaryData);
		    	},
				error: function (xhr, ajaxOptions, thrownError) {
					console.error("Failed loading content");
				},
				complete: function() {
					unBlock();
				}
			});
		}
	});
	
	 */
	
} // end of generateCharts()

function populateReportTables(summaryData) {
	var date = new Date(config.dateInputFormat.parse($("input#date-from").val()));
	
	var exportsSummary = calcExpSummary(summaryData.ExportStats);
	
	// EXPORT STATS
	$("#content-body")
		.append(
			$("<table>")
			.attr("id", "export-stats")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.attr("colspan", 2)
							.text("Map Export Stats for " + date.toLocaleDateString())
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Project :")
					)
					.append(
						$("<td>").text(project.name)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Total Number of Exports :")
					)
					.append(
						$("<td>").text(summaryData.ExportStats.length)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Overall Success Ratio :")
					)
					.append(
						$("<td>").text(exportsSummary.successRatio)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Total Time :")
					)
					.append(
						$("<td>").text(exportsSummary.totalTime)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Average Total Time :")
					)
					.append(
						$("<td>").text(exportsSummary.averageTime)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>")
							.addClass("label")
							.text("Unique Users Count :")
					)
					.append(
						$("<td>").text(exportsSummary.uniqueUsers)
					)
			)
		);
	
	// TOP USERS	
	
	$("#content-body")
		.append($("<table>")
			.attr("id", "top-users-title")
			.addClass("title-only")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.attr("colspan", 6)
							.text("Top " + usersThreshold + " Users Based on Total Time")
					)
			)
		);
		
	
	$("#content-body")
		.append($("<table>")
			.attr("id", "top-users")
			.addClass("tablesorter")
			.append(
			$("<thead>").append(
				$("<tr>")
					.append(
						$("<th>").text("Username")
					)
					.append(
						$("<th>").text("Total Time")
					)
					.append(
						$("<th>").text("Export Count")
					)
					.append(
						$("<th>").text("Success Ratio")
					)
					.append(
						$("<th>").text("Average Total Time")
					)
					.append(
						$("<th>").text("Max Total Time")
					)
				)
			)
		);
	
	var tbody = $("<tbody>");
	// Need a sort function to crop the top ten users (TotalTime DESC)
	var sortFunction = function (a, b) { return a.TotalTime > b.TotalTime ? -1 : 1};
	$.each(summaryData.UserStats.sort(sortFunction), function(i, user) {
		// Limit the results
		if (i >= usersThreshold)
			return;
		
		$("<tr>")
			.append(
				$("<td>").text(user.Username)
			)
			.append(
				$("<td>").text(formatSecsWithDays(user.TotalTime))
			)
			.append(
				$("<td>").text(user.TotalCount)
			)
			.append(
				$("<td>")
					.text((user.TotalCount) ? (user.SuccessfulCount / user.TotalCount).toFixed(2) : "0.00")
			)
			.append(
				$("<td>")
					.text(
						formatSecsWithDays((user.TotalCount) ? user.TotalTime / user.TotalCount : 0)
					)
			)
			.append(
				$("<td>").text(formatSecsWithDays(user.MaxTime))
			)
			.appendTo(tbody)
	});
	
	if (tbody.find("tr").length > 0) {
		tbody.appendTo("#top-users");
		
		$("#top-users").tablesorter({
			// Sort on the second column DESC
			sortList: [[1,1]]
		});
	}
	
	// WORST SECTIONS 
	
	$("#content-body")
		.append($("<table>")
			.attr("id", "worst-sections-title")
			.addClass("title-only")
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title")
								.attr("colspan", 11)
								.text("Worst " + sectionsThreshold + " Sections Based on Average Total Time")
						)
				)
		);
	
	$("#content-body")
		.append($("<table>")
			.attr("id", "worst-sections")
			.addClass("tablesorter")
			.append(
			$("<thead>").append(
				$("<tr>")
					.append(
						$("<th>").text("Section")
					)
					.append(
						$("<th>").text("Average Total Time")
					)
					.append(
						$("<th>").text("Average Export Time")
					)
					.append(
						$("<th>").text("Max Export Time")
					)
					.append(
						$("<th>").text("Times Exported")
					)
					.append(
						$("<th>").text("Export Success Ratio")
					)
					.append(
						$("<th>").text("Average Image Build Time")
					)
					.append(
						$("<th>").text("Max Image Build Time")
					)
					.append(
						$("<th>").text("Times Built")
					)
					.append(
						$("<th>").text("Build Success Ratio")
					)
					/*
					.append(
						$("<th>").text("Average Build Count")
					)
					*/
				)
			)
		);
	
	var tbody = $("<tbody>");
	// Need a sort function to crop the top ten users (TotalTime DESC)
	var sortFunction = function (a, b) { return a.AvgTotalTime > b.AvgTotalTime ? -1 : 1};
	$.each(summaryData.SectionStats.sort(sortFunction), function(i, section) {
		// Limit the results
		if (i >= sectionsThreshold)
			return;
		
		$("<tr>")
			.append(
				$("<td>").text(section.SectionName)
			)
			.append(
				$("<td>").text(formatSecsWithDays(section.AvgTotalTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(section.AvgSectionExportTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(section.MaxSectionExportTime))
			)
			.append(
				$("<td>").text(section.SectionExportCount)
			)
			.append(
				$("<td>").text((section.SectionExportCount) ? (section.SectionExportSuccessCount / section.SectionExportCount).toFixed(2) : "0.00")
			)
			.append(
				$("<td>").text(formatSecsWithDays(section.AvgBuildTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(section.MaxBuildTime))
			)
			.append(
				$("<td>").text(section.BuildCount)
			)
			.append(
				$("<td>").text((section.BuildCount) ? (section.BuildSuccessCount / section.BuildCount).toFixed(2) : "0.00")
			)
			/*
			.append(
				$("<td>").text(
					(totalImageBuilds) ? ((parseInt(section.TotalSectionsBuilt) / totalImageBuilds).toFixed(2)) : "0.00"
				)
			)
			*/
			.appendTo(tbody);
	});
	
	if (tbody.find("tr").length > 0) {
		tbody.appendTo("#worst-sections");
		
		$("#worst-sections").tablesorter({
			// Sort on the second column DESC
			sortList: [[1,1]]
		});
	}
	
	// WORST EXPORTS
	$("#content-body")
		.append($("<table>")
			.attr("id", "worst-exports-title")
			.addClass("title-only")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.attr("colspan", 11)
							.text("Worst " + exportsThreshold + " Exports of the Day")
					)
			)
		);
	
	$("#content-body")
		.append($("<table>")
			.attr("id", "worst-exports")
			.addClass("tablesorter")
			.append(
			$("<thead>").append(
				$("<tr>")
					.append(
						$("<th>").text("")
					)
					.append(
						$("<th>").text("User")
					)
					.append(
						$("<th>").text("Total Time")
					)
					.append(
						$("<th>").text("Started")
					)
					.append(
						$("<th>").text("Export Type")
					)
					.append(
						$("<th>").text("Success")
					)
					.append(
						$("<th>").text("Tools Version")
					)
					.append(
						$("<th>").text("Sections Exported")
					)
					.append(
						$("<th>").text("Export Time")
					)
					/*
					.append(
						$("<th>").text("Sections Built")
					)
					.append(
						$("<th>").text("Build Time")
					)
					*/
				)
			)
		);
	
	var tbody = $("<tbody>");
	
	var sortFunction = function (a, b) { return a.TotalTime > b.TotalTime ? -1 : 1};
	$.each(summaryData.ExportStats.sort(sortFunction), function(i, exportStat) {
		// Limit the results
		if (i >= exportsThreshold)
			return;
		
		$("<tr>")
			.append(
				$("<td>")
					.text("+")
					.addClass("hand")
					.attr("title", "Click to expand")
					.click(function() {showMoreInfo($(this), exportStat);})
			)
			.append(
				$("<td>").text(exportStat.Username)
			)
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.TotalTime))
			)
			.append(
				$("<td>").text(parseJsonDate(exportStat.Start).toLocaleTimeString())
			)
			.append(
				$("<td>").text(exportStat.ExportType)
			)
			.append(
				$("<td>").text(exportStat.Success)
			)
			.append(
				$("<td>").text(exportStat.ToolsVersion)
			)
			.append(
				$("<td>")
					.text(exportStat.SectionsExported.join(", "))
			)
			.append(
				$("<td>")
					.text(function () {
						var exportTime = 0;

						var regexp = new RegExp("section export$");
						$.each(exportStat.SubSteps, function(i, subStep) {
							if (!regexp.test(subStep.Name))
								return;
							
							//exportTime += datesDiffInSecs(parseJsonDate(subStep.End), parseJsonDate(subStep.Start))
							exportTime += subStep.Time;
						});
						
						return formatSecsWithDays(exportTime);						
					})
			)
			/*
			.append(
				$("<td>").text(exportStat.ImageBuildStat.MapSectionIdentifiers.join(", "))
			)
			.append(
				$("<td>").text(
					formatSecsWithDays(
						dateStringsDiffInSecs(exportStat.ImageBuildStat.End, exportStat.ImageBuildStat.Start)
					)
				)
			)
			*/
			.appendTo(tbody);
	});
	
	if (tbody.find("tr").length > 0) {
		tbody.appendTo("#worst-exports");
		
		$("#worst-exports").tablesorter({
			// Sort on the fourth column DESC
			sortList: [[1,1]]
		});
		
		$("#worst-exports th").bind("click", function() {
			removeAllInfoRows($(this).parent().parent().parent());
		});
	
	}
	
}

function calcExpSummary(expData) {
	var totalSecs = 0,
		users = {},
		succeed = 0;
	
	$.each(expData, function(i, item) {
		
		//console.log(item);
		//totalSecs += dateStringsDiffInSecs(parseJsonDate(item.End), parseJsonDate(item.Start));
		totalSecs += item.TotalTime;
		
				
		if (users.hasOwnProperty(item.Username))
			users[item.Username]++;
		else
			users[item.Username] = 1;
		
		if (item.Success)
			succeed++;
	});
		
	return {
		successRatio: ((succeed / expData.length) ? (succeed / expData.length) : 0).toFixed(2),
		totalTime: formatSecsWithDays(totalSecs),
		averageTime: formatSecsWithDays((totalSecs / expData.length) ? (totalSecs / expData.length) : 0),
		uniqueUsers: Object.keys(users).length,
	}
	
}


// Copied from export log
function removeAllInfoRows(table) {
	table.find("tr.details-row").remove();
	table.find("td.hand").text("+").attr("title", "Click to Expand");
}

function showMoreInfo(element, exportStat) {
	var rowElement = element.parent();
	
	// If the next row has a table it need to be removed
	if (rowElement.next().hasClass("details-row")) {
		rowElement.next().remove();
		element.text("+")
		element.attr("title", "Click to Expand")
	}
	else {
		var table = $("<table>")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.text("Export Details: ")
							.attr("colspan", 2)
							.addClass("title")
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("User Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.Username)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Export Type: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ExportType)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Export Number: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ExportNumber)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Success: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.Success)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Tools Version: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ToolsVersion)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Platforms Enabled: ").addClass("label")
					)
					.append(
						$("<td>").text(
							exportStat.PlatformsEnabled.map(
								function(d) {return platformNames[d].Name; }
							).join(", ")
						)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Machine Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.MachineName)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Processor Name: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ProcessorName)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Processor Cores: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.ProcessorCores)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Installed Memory: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.InstalledMemory)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Private Bytes Start: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.PrivateBytesStart)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("Private Bytes End: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.PrivateBytesEnd)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Enabled: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEEnabled)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Force CPU Count: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEForceCPUCount)
					)
			)
			.append(
				$("<tr>")
					.append(
						$("<td>").text("XGE Standalone: ").addClass("label")
					)
					.append(
						$("<td>").text(exportStat.XGEStandalone)
					)
			);
		
		populateSubsteps(exportStat.SubSteps, table);
		
		var newRow = $("<tr>")
			.addClass("details-row")
			.append(
				$("<td>")
					.attr("colspan", 9)
					.append(
						//$("<fieldset>").append(table)
						table
					)
			);
		
		rowElement.after(newRow);
		element.text("-")
		element.attr("title", "Click to Collapse")
	}
	
	function populateSubsteps(subSteps, element) {
		if (subSteps.length < 1)
			return
		
		var table = $("<table>")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.text("Sub Steps: ")
							.attr("colspan", 2)
					)
			);
			
		$.each(subSteps, function(i, subStep) {
			table
				.append(
					$("<tr>")
						.append(
							$("<td>")
								.addClass("label")
								.text(subStep.Name)
						)
						.append(
							$("<td>").text(formatSecsWithDays(subStep.Time))
						)
				);
			
			populateSubsteps(subStep.SubSteps, table);
		});
			
		element.append(
			$("<tr>")
				.append(
					$("<td>")
						.attr("colspan", 2)
						.append(
							//$("<fieldset>").append(table)
							table
						)
				)
		);
	} // End of populateSubsteps
	
} // End of showMoreInfo

/*
function calculateExportStats(xml) {
	var counter = 0,
		totalSecs = 0,
		users = {},
		succeed = 0;

	$(xml).find("MapExportStatBundle").each(function() {
		var exportStat = $(this).find("ExportStat");
		var start = exportStat.find("End").text();
		var end = exportStat.find("Start").text();
		var user = exportStat.find("Username").text();
		var success = exportStat.find("Success").text();
		
		counter++;
		totalSecs += dateStringsDiffInSecs(start, end);
		
		if (users[user] === undefined)
			users[user] = 1;
		else
			users[user]++;
		
		if (success == "true")
			succeed++;
	});
	
	var finalExportstats = {
		totalExports: counter,
		successRatio: ((succeed / counter) ? (succeed / counter) : 0).toFixed(2),
		totalTime: formatSecsWithDays(totalSecs),
		averageTime: formatSecsWithDays((totalSecs / counter) ? (totalSecs / counter) : 0),
		uniqueUsers: Object.keys(users).length,
	}
	
	return finalExportstats;
}

function convertMapExportsSummaryXml(xml) {

	var users = [];
	$(xml).find("UserStat").each(function() {
		var exportStat = $(this).find("ExportStat");
		var user = {
			Username: $(this).find("Username").text(),
			ExportStat: {
				SuccessfulCount: exportStat.find("SuccessfulCount").text(),
				FailedCount: exportStat.find("FailedCount").text(),
				TotalTimeTicks: exportStat.find("TotalTimeTicks").text(),
				MinTimeTicks: exportStat.find("MinTimeTicks").text(),
				MaxTimeTicks: exportStat.find("MaxTimeTicks").text()
			}
		};
		//console.log(user);
		users.push(user);
	});
	
	var sections = [];
	$(xml).find("SectionStat").each(function() {
		var exportStat = $(this).find("ExportStat");
		var imageBuildStat = $(this).find("ImageBuildStat");
		var section = {
			SectionName: $(this).find("SectionName").text(),
			ExportStat: {
				SuccessfulCount: exportStat.find("SuccessfulCount").text(),
				FailedCount: exportStat.find("FailedCount").text(),
				TotalTimeTicks: exportStat.find("TotalTimeTicks").text(),
				MinTimeTicks: exportStat.find("MinTimeTicks").text(),
				MaxTimeTicks: exportStat.find("MaxTimeTicks").text()
			},
			ImageBuildStat: {
				SuccessfulCount: imageBuildStat.find("SuccessfulCount").text(),
				FailedCount: imageBuildStat.find("FailedCount").text(),
				TotalTimeTicks: imageBuildStat.find("TotalTimeTicks").text(),
				MinTimeTicks: imageBuildStat.find("MinTimeTicks").text(),
				MaxTimeTicks: imageBuildStat.find("MaxTimeTicks").text()
			},
			TotalSectionsBuilt: $(this).find("TotalSectionsBuilt").text(),
		};
		//console.log(section);
		sections.push(section);
	});
	
	var exports = [];
	// Getting the ExportStat children of general ExportStats
	// Find() would fail as there are elements deeper in the subtree with the same name (ExportStat)
	$(xml).find("ExportStats").children().each(function() {
		var exportStatElement = $(this).find("ExportStat");
		var exportStat = {
			Id: exportStatElement.find("Id").text(),
			CreatedOn: exportStatElement.find("CreatedOn").text(),
			Start: exportStatElement.find("Start").text(),
			End: exportStatElement.find("End").text(),
			Success: exportStatElement.find("Success").text(),
			Username: exportStatElement.find("Username").text(),
			MachineName: exportStatElement.find("MachineName").text(),
			ExportType: exportStatElement.find("ExportType").text(),
			ToolsVersion: exportStatElement.find("ToolsVersion").text(),
			UsingAP3: exportStatElement.find("UsingAP3").text(),
			ExportNumber: exportStatElement.find("ExportNumber").text(),
		};
		
		var mapExportStats = [];
		// MapExportStats has SectionExportStatBundle elements
		$(this).find("MapExportStats").children("SectionExportStatBundle").each(function() {
			var sectionExportStatBundle = {
				Id: $(this).children("Id").text(),
				CreatedOn: $(this).children("CreatedOn").text(),
				Start: $(this).children("Start").text(),
				End: $(this).children("End").text(),
				Success: $(this).children("Success").text(),
				MapSectionIdentifier: $(this).children("MapSectionIdentifier").text(),
				//SubStats: $(this).find("SubStats").text(),
			};
			mapExportStats.push(sectionExportStatBundle);
		});
		
		var imageBuildStatElement = $(this).find("ImageBuildStat");
		
		var mapSectionIdentifiers = [];
		imageBuildStatElement.find("MapSectionIdentifiers").find("MapSectionIdentifier").each(function() {
			mapSectionIdentifiers.push($(this).text());
		});
		var imageBuildStat = {
			Id: imageBuildStatElement.find("Id").text(),
			CreatedOn: imageBuildStatElement.find("CreatedOn").text(),
			Start: imageBuildStatElement.find("Start").text(),
			End: imageBuildStatElement.find("End").text(),
			Success: imageBuildStatElement.find("Success").text(),
			MapSectionIdentifiers: mapSectionIdentifiers,
		};
		//console.log(mapExportStats);
		exports.push({
			ExportStat: exportStat,
			MapExportStats: mapExportStats,
			ImageBuildStat: imageBuildStat,
		});
	});
	
	var summary = {
		UserStats: users,
		SectionStats: sections,
		ExportStats: exports,
	};
	
	return summary;
}

*/