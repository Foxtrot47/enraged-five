//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-map-exports", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 false, // section
	                 false, // user
	                 false, // dates
    ],
};

var restEndpoint = config.mapExportsBySection,
	restEndpointAsync = config.mapExportsBySectionAsync,
	defaultFilterText = "",
	exportsStats;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	if (!$("#section").val())
		$("#section").val(defaultFilterText);
	$("#section").focus(function() {
		if($(this).val() == defaultFilterText)
			$(this).val("");
	});
	$("#section").blur(function() {
		if($(this).val() == "")
			$(this).val(defaultFilterText);
	});
	/*
	$("#section").change(function() {
		if (mapExportsAllStats)
			generateOverlays();
	});
	*/
	$("#section").keyup(function() {
		//console.log(exportsStats);
		if (exportsStats)
			populateExportsTable(exportsStats);
	});
	
	$("#filter").click(function() {
		generatePage();
	});
	
	generatePage();
}

function generatePage() {
	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var req = new ReportRequest(config.restHost + restEndpoint,
			"json",
			config.restHost + restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);

	req.sendSingleAsyncRequest(pValues, populateExportsTable);
	
} // end of generatePage()

function populateExportsTable(resultData) {

	if (resultData)
		exportsStats = resultData;
	
	$("#content-body").empty();
	
	var title = "Total exports between " 
			+ new Date(config.dateInputFormat.parse($("input#date-from").val())).toLocaleDateString() 
			+ " - " 
			+ new Date(config.dateInputFormat.parse($("input#date-to").val())).toLocaleDateString();
	var user = $("input#user").val();
	var filterText = $("#section").val();
	var re = new RegExp(filterText, "i");
	
	//if (filterText == defaultFilterText)
	//	filterText = "";

	$("#content-body").append(
		$("<table>")
			.addClass("title-only")
			.append(
				$("<tr>")
					.append(
						$("<th>")
							.addClass("title")
							.attr("colspan", 14)
							.text(function() {
								return (user) ? title + " for user " + user : title;
							})
					)
			)
	);
	
	var table = $("<table>").addClass("tablesorter"); 
	
	$("<thead>").append(
			$("<tr>")
				.append(
					$("<th>").text("Section")
				)
				/*
				.append(
					$("<th>").text("Total Time Min")
				)
				*/
				.append(
					$("<th>").text("Total Time Avg")
				)
				.append(
					$("<th>").text("Total Time Max")
				)
				/*
				.append(
					$("<th>").text("Export Time Min")
				)
				*/
				.append(
					$("<th>").text("Export Time Avg")
				)
				.append(
					$("<th>").text("Export Time Max")
				)
				.append(
					$("<th>").text("Export Count")
				)
				.append(
					$("<th>").text("Export Success")
				)
				/*
				.append(
					$("<th>").text("Build Time Min")
				)
				*/
				.append(
					$("<th>").text("Build Time Avg")
				)
				.append(
					$("<th>").text("Build Time Max")
				)
				.append(
					$("<th>").text("Build Count")
				)
				.append(
					$("<th>").text("Build Success")
				)
	)
	.appendTo(table);
	
	var tbody = $("<tbody>");
	
	$.each(exportsStats, function(i, exportStat) {
		//console.log(exportStat);
		if (!re.test(exportStat.SectionName))
			return;
		
		$("<tr>")
			.append(
				$("<td>").text(exportStat.SectionName)
			)
			/*
			.append(
				$("<td>").text("-")
			)
			*/
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.AvgTotalTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.MaxTotalTime))
			)
			/*
			.append(
				$("<td>").text("-")
			)
			*/
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.AvgSectionExportTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.MaxSectionExportTime))
			)
			.append(
				$("<td>").text(exportStat.SectionExportCount)
			)
			.append(
				$("<td>").text(((exportStat.SectionExportCount) ? (exportStat.SectionExportSuccessCount / exportStat.SectionExportCount) : 0).toFixed(2))
			)
			/*
			.append(
				$("<td>").text("-")
			)
			*/
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.AvgBuildTime))
			)
			.append(
				$("<td>").text(formatSecsWithDays(exportStat.MaxBuildTime))
			)
			.append(
				$("<td>").text(exportStat.BuildCount)
			)
			.append(
				$("<td>").text(((exportStat.BuildCount) ? (exportStat.BuildSuccessCount / exportStat.BuildCount) : 0).toFixed(2))
			)
		.appendTo(tbody);
		
	});	
	
	tbody.appendTo(table);
	
	if (tbody.find("tr").length > 0) {// show the table if rows have been added
		// Add the tablesorter plugin
		table.tablesorter({
			// Sort on the first column ASC
			sortList: [[0, 0]]
		});
	
		table.appendTo("#content-body");
	}
	//else
	//	Sexy.alert(config.noDataText);

}
