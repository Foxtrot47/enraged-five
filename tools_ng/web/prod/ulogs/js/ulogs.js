var tableId = "#ulogs-table";
var dateOutputFormat = d3.time.format("%m/%d/%Y %I:%M:%S %p");
var filterText = "filter the message";
var xmldata = null;

var types = {
		error: {name: "error", id: "errors", css: "error",},
		warning: {name: "warning", id: "warnings", css: "warning",},
		message: {name: "message", id: "messages", css: "message",},
		profile: {name: "profile", id: "debug", css: "message",},
		profile_end: {name: "profile", id: "debug", css: "message",},
		debug: {name: "profile", id: "debug", css: "message",},
};

function init() {
	
	var logEndpoint = getUrlVars()["ulog"];
	var requestData = {};
	
	if (getUrlVars()["start"])
		requestData["start"] = getUrlVars()["start"];
	if (getUrlVars()["count"])
		requestData["count"] = getUrlVars()["count"];
	
	//logEndpoint
	if (!logEndpoint)
		return;
	
	block();
	
	$.ajax({
		url: logEndpoint,
		type: "GET",
		data: requestData,
		dataType: "xml",
				
		success: function(xml, textStatus, jqXHR) {
			xmldata = xml;
			populateTable(tableId, xml);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			$("#filter").val(filterText);
			$("#filter").focus(function() {
				if($(this).val() == filterText)
					$(this).val("");
			});
			$("#filter").blur(function() {
				if($(this).val() == "")
					$(this).val(filterText);
			});
			
			$("#filter").change(function() {
				filterTable(tableId, xmldata);
			});
			$("#header input:checkbox").change(function() {
				filterTable(tableId, xmldata);
			});
		
		unBlock();
		}
	});

}

function populateTable(id, xml) {
	
	$("<thead>").append(
	
		$("<tr>")
			.append(
					$("<th class='type-cell'>").text("Type")
			)
			.append(
					$("<th class='timestamp-cell'>").text("Timestamp")
			)
			.append(
					$("<th class='context-cell'>").text("Context")
			)
			.append(
					$("<th>").text("Message")
			)
	).appendTo(id);
	
	populateLines(id, xml);
	
	$(id).tablesorter({
		// Sort on the second column asc
		sortList: [[1,0]] 
	});
}

function populateLines(id, xml) {
	var filterValue = $("#filter").val();
	if (filterValue == filterText)
		filterValue = "";
	var re = new RegExp(filterValue, "i");
	
	var context = $(xml).find("context");
	var contextName = context.attr("name");
	var tbody = $("<tbody>").appendTo(id);
	
	context.children().each(function() {
				
		var ticks = parseInt($(this).attr("timestamp_ticks"));
		// Message is the element's content
		var message = $(this).text();
		// If filter text matches the message
		if (re.test(message)) {
			var tag = $(this).prop("tagName");
			
			if (!$("input#" + types[tag].id).is(":checked"))
				return;
			
			$("<tr class='" + types[tag].css + "'>")
				.append(
						$("<td>").text(tag)
				)
				.append(
						$("<td>").text(dateOutputFormat(new Date(msTicks2UnixTime(ticks))))
				)
				.append(
						$("<td>").text(contextName)
				)
				.append(
						$("<td>").html(replaceURLsWithLinks(message))
				)
				.appendTo(tbody);
		}
		
	});
}

function emptyLines(id) {
	$(id + " tbody").remove();
}

function filterTable(id, xml) {
	emptyLines(id);
	populateLines(id, xml);
	$(id).trigger("update");
}

function msTicks2UnixTime(ticks) {
	/*
	 * MS ticks counting start at 12:00:00 midnight, Jan 1, 0001
	 * Unix Time starts at midnight Jan 1, 1970 (in milliseconds)
	 * 1 tick is 10000 milliseconds 
	 * There are 621355968000000000 ticks between 01/01/0001 and 01/01/1970
	 */
	return ((ticks - 621355968000000000) / 10000);
}

function replaceURLsWithLinks(text) {
	var replacePattern = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    return text.replace(replacePattern, '<a href="$1" title="$1" target="_blank">$1</a>');
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, 
    	function(m, key, value) {
       		vars[key] = value;
    });
    return vars;
}
