var navigationTitle = "Map Exports";
var navigationItems = [{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Report",
		href: "report.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Exports",
		href: "#",
		display: "inherit",
		config: "",
		subItems: [
		    {
				title: "Exports Log",
				href: "exports_log.html",
				display: "inherit",
			},
			/*
			{
		    	title: "Exports By User",
		    	href: "exports_by_user.html",
		    	display: "inherit",
		    },
		    */
		    {
		    	title: "Exports By Section",
		    	href: "exports_by_section.html",
		    	display: "inherit",
		    },
		  ],
	},
	{
		title: "Overlay",
		href: "overlay.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Time Graphs",
		href: "time_graphs.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	/*
	{
		title: "Map Exports",
		href: "#",
		display: "inherit",
		config: "",
		profile: false,
		subItems: [{
			title: "Map Exports Report",
			href: "map_exports_report.html",
			display: "inherit",
		},
		{
			title: "Map Exports Overlay",
			href: "map_exports_overlay.html",
			display: "inherit",
		},
		{
			title: "Map Exports Time Graphs",
			href: "map_exports_time_graphs.html",
			display: "inherit",
		},
		],
	},
	*/
];

