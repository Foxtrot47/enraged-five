//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-map-exports", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 false,  // section
	                 true,  // user
	                 false, // dates
    ],
};

//var defaultFilterText = "type at least two characters to filter by section";
var defaultFilterText = "",
	lineChartId = "graph",
	sectionMinLength = 2,
	chartData,
	restEndpoint = config.mapExportHistorical,
	restEndpointAsync = config.mapExportHistoricalAsync;

availableMetrics = [{
		name: "Average Total Time",
		description: "Average total time of all the overall exports (export & image build)",
		getKey: function(d) { return parseJsonDate(d.Key); },
		getValue: function(d) { return d.Value.AvgTotalTime; },
		formatValue: function(d) { return formatSecsWithDays(d); },
	},
	{
		name: "Average Export Time",
		description: "Average time of the all the exports",
		getKey: function(d) { return parseJsonDate(d.Key); },
		getValue: function(d) { return d.Value.AvgSectionExportTime; },
		formatValue: function(d) { return formatSecsWithDays(d); },
	},
	{
		name: "Average Image Build Time",
		description: "Average time of all the image builds",
		getKey: function(d) { return parseJsonDate(d.Key); },
		getValue: function(d) { return d.Value.AvgBuildTime; },
		formatValue: function(d) { return formatSecsWithDays(d); },
	},
	{
		name: "Total Count",
		description: "Total count of all the overall exports (export & image build)",
		getKey: function(d) { return parseJsonDate(d.Key); },
		getValue: function(d) { return d.Value.TotalCount; },
		formatValue: function(d) { return d; },
	},
	{
		name: "Success Ratio",
		description: "Succesful ratio of all the overall exports (export & image build)",
		getKey: function(d) { return parseJsonDate(d.Key); },
		getValue: function(d) { return d.Value.SuccessfulCount / d.Value.TotalCount; },
		formatValue: function(d) { return (d*100).toFixed(2) + " %"; },
	},
	];

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body")
	.append(
		$("<div>")
			.attr("id", lineChartId)
			.addClass("single-graph")
	);
	
	initSvg(lineChartId);
	
	
	if (!$("#section").val())
		$("#section").val(defaultFilterText);
	$("#section").focus(function() {
		if($(this).val() == defaultFilterText)
			$(this).val("");
	});
	$("#section").blur(function() {
		if($(this).val() == "")
			$(this).val(defaultFilterText);
	});
	/*
	$("#section").change(function() {
		if (mapExportsAllStats)
			generateOverlays();
	});
	*/
	$("#section").keyup(function() {
		//if ($(this).val().length >= sectionMinLength)
		generateCharts();
	});
	
	$.each(availableMetrics, function(i, availableMetric) {
		$("select#available-charts-options").append(
			$("<option />")
				.text(availableMetric.name)
				.val(i)
				//.attr("selected", function() {return (pageData && pageData.availableChart == i) ? "selected" : false;})
		);
	});
	
	$("select#available-charts-options").change(function() {
		//pageData.availableChart = parseInt($(this).val());
		//storeLocalObject(config.currentFilename, pageData);
		generateCharts();
	});
	
	$("#filter").click(function() {
		getAndProcessData();
	});
	
	updateChartHeight();
	getAndProcessData();
}

function updateChartHeight() {
	// Adjust the height of the charts automatically
	//var windowHeight = $("#content").height() - 2 * $("#sub-header").height();
	//$("#timechart").css("height", windowHeight);
}

function getAndProcessData() {
	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var req = new ReportRequest(config.restHost + restEndpoint,
			"json",
			config.restHost + restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);

	req.sendSingleAsyncRequest(pValues, generateCharts);
	
	/*
	block();
	$.ajax({
		url: config.restHost + config.mapExportStatsRaw,
		type: "GET",
		dataType: "xml",
		data: {
			start: start,
			end: end,
		},
		async: true,
		success: function(xml, textStatus, jqXHR) {
			processedGeneralData = processGeneralData(xml);
			processedSectionData = processSectionsData(xml);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error("Failed loading content");
		},
		complete: function() {
			unBlock();
			generateCharts();
		}
	});
	*/
	
} // end of generateCharts()

function generateCharts(resultData) {
	if (resultData)
		chartData = resultData;
	
	var selectedMetric = availableMetrics[parseInt($("select#available-charts-options").val())];
	
	drawCharts(selectedMetric);
} // end of generateCharts()

function drawCharts(selectedMetric) {
	
	var datum = [];
	var colour = d3.scale.category20();
	var filterText = $("#section").val();
	
	$("#content-description").text(selectedMetric.description);
	//cleanSvg("timechart", "nvd3");
	
	if (filterText.length < sectionMinLength) {
		var nvd3Array = [];

		$.each(chartData.GeneralStats, function(i, item) {
			//console.log({date: selectedMetric.getKey(item) , value: selectedMetric.getValue(item)});
			nvd3Array.push({date: selectedMetric.getKey(item) , value: selectedMetric.getValue(item)});
		});
	
		datum = [{
			values: nvd3Array, 
			color: colour(Math.random()*20),
			key: selectedMetric.name,
		}];
	}
	else {
		var regexp = new RegExp(filterText, "i");
		$.each(chartData.SectionStats, function(i, section) {
			
			if (!regexp.test(section.Key))
				return;
			
			var sectionArray = [];

			$.each(section.Value, function(i, item) {
				sectionArray.push({date: selectedMetric.getKey(item) , value: selectedMetric.getValue(item)});
			});
			
			//console.log(sectionArray);
			datum.push({
				values: sectionArray,
				color: colour(Math.random()*20),
				key: section.Key,
			});
		});
	}
	
	nv.addGraph(function() {
		//var chart =  nv.models.lineWithFocusChart()
		var chart =  nv.models.lineChart()
			.x(function(d) {
				//console.log(d.date);
				return new Date(d.date);
			})
			.y(function(d) { return Number(d.value); })
			.margin({top: 50, right: 40, bottom: 80, left: 80})
			.tooltips(true);

		chart.xAxis
			//.axisLabel("date")
			.tickFormat(function(d) {
				return d3.time.format("%Y-%m-%d")(new Date(d));
			})
			.rotateLabels(-45);
		//chart.x2Axis
		//	.tickFormat(function(d) {
		//		return d3.time.format("%Y-%m-%d")(new Date(d));
		//	});
			
		chart.yAxis
			//.axisLabel("time")
			.tickFormat(function(d) {
				return selectedMetric.formatValue(Number(d));
			});
		//chart.y2Axis
		//	.tickFormat(function(d) {
		//		return formatSecsWithDays(Number(d));
		//	});
		
		chart.tooltipContent(function(key, x, y, e, graph) {
	        return "<h3>" + key + "</h3>" +
            		"<p>" +  y + " at " + (new Date(x)).toDateString() + "</p>";
		});
	
		d3.select("#" + lineChartId + " svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
	 
		nv.utils.windowResize(chart.update);
		//nv.utils.windowResize(updateChartHeight);
	 
		return chart;
	});
}

/* DEPRECATED

function processGeneralData(xml) {
	var exports = {};
	
	$(xml).find("MapExportStatBundle").each(function() {
		var exportStat = $(this).find("ExportStat");
		
		if (exportStat.find("Success").text() == "false") {
			//console.log(exportStat.find("Success").text());
			return;
		}
		var overallStart = exportStat.find("Start").text();
		var overallTime = dateStringsDiffInSecs(exportStat.find("End").text(), overallStart);
		
		var exportDate = extractDate(overallStart);
		
		var exportTime = 0;
		// MapExportStats has SectionExportStatBundle elements
		$(this).find("MapExportStats").children("SectionExportStatBundle").each(function() {
			exportTime += dateStringsDiffInSecs($(this).children("End").text(), $(this).children("Start").text());
		});
			
		var imageBuildStat = $(this).find("ImageBuildStat");
		var buildTime = dateStringsDiffInSecs(imageBuildStat.find("End").text(), imageBuildStat.find("Start").text());

		if (exports.hasOwnProperty(exportDate)) {
			exports[exportDate].totalOverallTime += overallTime;
			exports[exportDate].totalExportTime += exportTime;
			exports[exportDate].totalBuildTime += buildTime;
			exports[exportDate].count++;
		}
		else {
			exports[exportDate] = {
				totalOverallTime: overallTime,
				totalExportTime: exportTime,
				totalBuildTime: buildTime,
				count: 1,
			}
		}
	});
	
	console.log(exports);
	return exports;
}

function processSectionsData(xml) {
	var sections = {};
	
	$(xml).find("MapExportStatBundle").each(function() {
		var exportStat = $(this).find("ExportStat");
		
		if (exportStat.find("Success").text() == "false") {
			//console.log(exportStat.find("Success").text());
			return;
		}
		
		var imageBuildStat = $(this).find("ImageBuildStat");
		var sectionsBuildTime = dateStringsDiffInSecs(imageBuildStat.find("End").text(), imageBuildStat.find("Start").text());
		
		var mapExportStats = $(this).find("MapExportStats");
		// MapExportStats has SectionExportStatBundle elements
		mapExportStats.find("SectionExportStatBundle").each(function() {
			var sectionExportStart = $(this).children("Start").text();
			
			var sectionExportTime = dateStringsDiffInSecs($(this).children("End").text(), sectionExportStart);
			var sectionBuildTime = 0;
			
			if (mapExportStats.find("SectionExportStatBundle").length == 1)
				sectionBuildTime = sectionsBuildTime;
			
			var sectionOverallTime = sectionExportTime + sectionBuildTime;
			
			var sectionName = $(this).children("MapSectionIdentifier").text();
			var sectionExportDate = extractDate(sectionExportStart);
			
			if (!sections.hasOwnProperty(sectionName)) {
				sections[sectionName] = {};
			}
			
			if (sections[sectionName].hasOwnProperty(sectionExportDate)) {
				sections[sectionName][sectionExportDate].totalOverallTime += sectionOverallTime;
				sections[sectionName][sectionExportDate].totalExportTime += sectionExportTime;
				sections[sectionName][sectionExportDate].totalBuildTime += sectionBuildTime;
				sections[sectionName][sectionExportDate].count++;
			}
			else {
				sections[sectionName][sectionExportDate] = {
					totalOverallTime: sectionOverallTime,
					totalExportTime: sectionExportTime,
					totalBuildTime: sectionBuildTime,
					count: 1,
				};
			}
			
		});
			
	});
	
	return sections;
}

*/