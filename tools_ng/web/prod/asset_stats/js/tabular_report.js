var reportData;

getScriptSync("/libs/jquery-tablesorter/jquery.tablesorter.min.js");
getScriptSync("/libs/jquery-tablesorter/jquery.tablesorter.widgets.min.js");
getScriptSync("/libs/jquery-tablesorter/widgets/widget-scroller.js");
//getScriptSync("/libs/jquery-scrollableFixedHeaderTable/jquery.scrollableFixedHeaderTable.js");

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();	
}

function generateReport() {
	$("#content-body").empty();

	if (reportOptions.notReportFramework) {
		block();
		$.ajax({
			url: config.restHost + reportOptions.restEndpoint,			
			dataType: "json",
			async: false,
			success: populateReport,
		});
		unBlock();
	}
	else { // use the reporting framework by default
		var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
		if (!pValues)
			return;
		
		if (reportOptions.multipleRequests) {
			var endpointsArray = reportOptions.multipleRequests(pValues);
			
			var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
			req.sendMultipleAsyncRequest(endpointsArray, populateReport);
		}
		else {
			var returnedDataType = (reportOptions.returnedDataType) ? reportOptions.returnedDataType : "json";
		
			var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
				"json",
				config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				config.restHost + config.reportsQueryAsync);
			req.sendSingleAsyncRequest(pValues, populateReport, returnedDataType);
		}
	}
}

function populateReport(data, tab) {
	$("#content-body").empty();
	/*
	if (reportData.length == 0) {
		Sexy.alert(config.noDataText);
		return;
	}
	*/
	
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
	
	if (reportOptions.reportTabs && !tab)
		tab = reportOptions.reportTabs[0].value;
	
	if (reportOptions.reportTabs) {
		var ul = $("<ul />").addClass("tab-nav");
		
		$.each(reportOptions.reportTabs, function(i, rTab) {
			ul.append(
				$("<li />")
					.text(rTab.key)
					.attr("title", rTab.key)
					.val(rTab.value)
					.addClass("hand")
					.addClass(function() {return (tab == rTab.value) ? "active" : ""})
					.click(function() {
						populateReport(null, rTab.value)}
					)
			)
		});
		
		ul.appendTo($("#content-body"));
	}
		
	if (reportOptions.reportSummaryItems) {
		var summaryTable = $("<table />");
		
		summaryTable.append(
			$("<thead>")
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title")
								.attr("colspan", 2)
								.text(reportOptions.reportSummaryTitle)
						)
				)
		);
		
		$.each(reportOptions.reportSummaryItems, function(i, summaryItem) {
			summaryTable.append(
				$("<tr>")
					.append(
						$("<td>")
						.addClass("label")
						.text(summaryItem.title)
					)
					.append(
						$("<td>")
						.text(summaryItem.getValue(reportData))
					)
			);
		});
		
		summaryTable.appendTo("#content-body");
		
	} // End of if (reportOptions.reportSummaryItems)
	
	if (!reportOptions.getReportArray(reportData))
		return;
	
	$.each(reportOptions.groups, function(i, group) {
		
		var table = $("<table />")
			.addClass("tablesorter");

		table.append(
			$("<thead>")
				.append(
					$("<tr>")
					//$("<tr>").addClass("persist-header")
				)
		);
		
		var tr = table.find("tr");
		if (reportOptions.hasReportArrayItemMoreInfo) {
			tr.append(
				$("<th>")
					.text("")
			);
		}
	
		$.each(reportOptions.reportArrayItems, function(i, item) {
						
			tr.append(
				$("<th>")
					.text(item.title)
			);
		}); // End of each reportOptions.reportItems - table header generation
		
		var tbody = $("<tbody>");

		var csvColumns = reportOptions.reportArrayItems.map(function(i) {return i.title; });
		var csvRows = [];
		$.each(reportOptions.getReportArray(reportData), function(i, d) {
			var re = new RegExp(group.regexp);
			if (!re.test(reportOptions.reportArrayItems[group.filterItem].getValue(d)))
				return;
			else {		
				var tr = $("<tr>");
				if (reportOptions.hasReportArrayItemMoreInfo) {
					tr.append(
						$("<td>")
							.text("+")
							.addClass("hand first")
							.attr("title", "Click to expand")
							.click(function() {
								showMoreInfo($(this), d, group, tab); 
							})
					)
				}
					
				var csvRow = [];
				$.each(reportOptions.reportArrayItems, function(i, item) {
					csvRow.push((item.getNonHtmlValue) ? item.getNonHtmlValue(d, tab) : item.getValue(d, tab));
					tr.append(
						$("<td>")
							.html(item.getValue(d, tab))
							.attr("title", function() {return ((item.titleAttr) ? item.titleAttr : ""); })
							.addClass(function() {return ((item.getClass) ? item.getClass(d) : ""); })
							.click(function() {
								if (item.onClick) item.onClick(d, $(this)); 
							})
					)
				});
				tr.appendTo(tbody);
				csvRows.push(csvRow);
			}
		
		}); // End of each reportData - table content generation
				
		tbody.appendTo(table);
		
		if (table.find("tr").length > 1) {// show the table if rows have been added
			var titleID = "titleTable_" + i;
			var csvID = "csv_" + i;
			var csvFrameID = "csv_frame_" + i;
						
			$("<table />")
				.attr("id", titleID)
				.addClass("title-only")
				.append(
					$("<thead>")
						.append(
							$("<tr>")
								.append(
									$("<th>")
										.addClass("title")
										.append(
											$("<span>")
												.text(group.title)
										)
										.append(
											$("<span>")
												.attr("id", csvID)
												.addClass("title-csv")
										)
								)
						)
				)
				.appendTo("#content-body");

			/*
			var topOffset = getCurrentHeaderHeight() 
							+ $("#content-description").offset().top 
							+ $("#content-description").height();
			*/
			
			if (reportOptions.enableCSVExport) {
				addCSVButton(csvID, 
					csvFrameID, 
					function() { exportToCSV(csvColumns, csvRows, csvFrameID, getCSVFilename(group.title)); });
			}
									
			table.appendTo("#content-body");
		
			//	Add the tablesorter plugin
			var obj = ((reportOptions.reportArraySort) ? {"sortList": reportOptions.reportArraySort} : {});
			//sortObj["widgets"] = ["stickyHeaders"];
			obj["showProcessing"] = true;
			//obj["widgets"] = ["filter", "scroller"];
			obj["widgets"] = ["filter"];
			table.tablesorter(obj);
						
			// Remove the more info rows on sorting click events - they destroy the sorting 
			table.find("th").bind("click", function() {
				removeAllInfoRows($(this).parent().parent().parent());
			});
			
		}
		
	}); //End of each reportOptions.groups

	hideProgressbar();
}

function showMoreInfo(element, rowData, group, tab) {
	var rowElement = element.parent();
	
	// If the next row has a table it need to be removed
	if (rowElement.next().hasClass("details-row")) {
		rowElement.next().remove();
		element.text("+")
		element.attr("title", "Click to Expand")
	}
	else {
		if (reportOptions.reportArrayItemMoreInfoRest) {
			var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
			pValues.Pairs[reportOptions.reportArrayItemMoreInfoRest.extraParamName] = 
									reportOptions.reportArrayItems[group.filterItem].getValue(rowData);
			
			var req = new ReportRequest(config.restHost 
							+ reportOptions.reportArrayItemMoreInfoRest.restEndpoint,
						"json",
						config.restHost 
							+ reportOptions.reportArrayItemMoreInfoRest.restEndpointAsync 
							+ pValues.ForceUrlSuffix,
						config.restHost + config.reportsQueryAsync);
			 
			req.sendSingleAsyncRequest(pValues, function(d) {expandRow(element, d)});
		}
		else
			expandRow(element, rowData);
		
	}
	
	function expandRow(element, rowData) {
		var div = $("<div>");

		if (reportOptions.hasMoreInfoMultipleTables) {
			$.each(reportOptions.getReportMoreInfoArray(rowData), function(i, tData) {
				generateSubTable(tData, div);
			});
		}
		else
			generateSubTable(rowData, div);
	
		var newRow = $("<tr>")
			.addClass("details-row")
			.append(
				$("<td>")
					.attr("colspan", reportOptions.reportArrayItems.length + 1)
					//.append(
					//	$("<fieldset>").append(div)
					//)
					.append(div)
			);
	
		rowElement.after(newRow);
		element.text("-");
		element.attr("title", "Click to Collapse");
	}
	
	function generateSubTable(data, element) {
	
		var table = $("<table />").addClass("tablesorter"); 
				
		var tr = $("<tr>");
		
		if (reportOptions.hasReportArraySubItemMoreInfo) {
			tr.append(
				$("<th>").text("")
			);
		}
		
		$.each(reportOptions.reportMoreInfoArrayItems, function(i, item) {
			tr.append(
				$("<th>").text(item.title)
			);
		}); // End of each reportOptions.reportItems - table header generation
		
		table.append($("<thead>").append(tr));
		
		$.each(reportOptions.getMoreInfoValues(data), function(i, d) {
			
			var row = $("<tr>");
			if (reportOptions.hasReportArraySubItemMoreInfo) {
				row.append(
					$("<td>")
						.text("+")
						.addClass("hand first")
						.attr("title", "Click to expand")
						.click(function() {
							showSubItemMoreInfo($(this), d, group, tab); 
						})
				)
			}
			
			
			$.each(reportOptions.reportMoreInfoArrayItems, function(i, item) {
				row.append(
					$("<td>")
						.html(item.getValue(d, tab))
						.attr("title", function() {return ((item.titleAttr) ? item.titleAttr : ""); })
						.addClass(function() {return ((item.getClass) ? item.getClass(d) : ""); })
						.click(function() {
							if (item.onClick) item.onClick(d, $(this)); 
						})
				)
			});
			row.appendTo(table);
		
		}); // End of each reportOptions.getMoreInfoValues(data) - subtable content generation
		
		var div = $("<div>"); // table's div
		if (table.find("tr").length > 1) {// show the table and title if rows have been added
			// title
			$("<table />")
				.addClass("title-only")
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title2")
								.text(reportOptions.getMoreInfoKey(data))
						)
				)
				.appendTo(div);
		
			var sortObj = 
				((reportOptions.reportMoreInfoArraySort) ? {"sortList": reportOptions.reportMoreInfoArraySort} : {});
			table.tablesorter(sortObj);
			
			// Remove the more info rows on sorting click events - they destroy the sorting 
			table.find("th").bind("click", function() {
				removeAllInfoRows($(this).parent().parent().parent());
			});
			
			table.appendTo(div);
		}
			
		element.append(div);
				
	} // End of generateSubTable
	
} // End of showMoreInfo

function removeAllInfoRows(table) {
	table.find("tr.details-row").remove();
	table.find("td.hand").text("+").attr("title", "Click to Expand");
}

function showSubItemMoreInfo(element, rowData, group, tab) {
	var rowElement = element.parent();
	
	// If the next row has a table it need to be removed
	if (rowElement.next().hasClass("details-row")) {
		rowElement.next().remove();
		element.text("+")
		element.attr("title", "Click to Expand")
	}
	else {
		expandRow(element, rowData);
	}
	
	function expandRow(element, rowData) {
		var div = $("<div>");

		if (reportOptions.hasSubItemMoreInfoMultipleTables) {
			$.each(reportOptions.getReportSubItemMoreInfoArray(rowData), function(i, tData) {
				generateSubTable(tData, div);
			});
		}
		else
			generateSubTable(rowData, div);
	
		var newRow = $("<tr>")
			.addClass("details-row")
			.append(
				$("<td>")
					.attr("colspan", reportOptions.reportMoreInfoArrayItems.length + 1)
					//.append(
					//	$("<fieldset>").append(div)
					//)
					.append(div)
			);
	
		rowElement.after(newRow);
		element.text("-");
		element.attr("title", "Click to Collapse");
	}
	
	function generateSubTable(data, element) {
	
		var table = $("<table />").addClass("tablesorter"); 
				
		var tr = $("<tr>");
		$.each(reportOptions.reportSubItemMoreInfoArrayItems, function(i, item) {
			tr.append(
				$("<th>").text(item.title)
			);
		}); // End of each reportOptions.reportItems - table header generation
		
		table.append($("<thead>").append(tr));
		
		$.each(reportOptions.getSubItemMoreInfoValues(data), function(i, d) {
			
			var row = $("<tr>");
			$.each(reportOptions.reportSubItemMoreInfoArrayItems, function(i, item) {
				row.append(
					$("<td>")
						.html(item.getValue(d, tab))
						.attr("title", function() {return ((item.titleAttr) ? item.titleAttr : ""); })
						.addClass(function() { return ((item.getClass) ? item.getClass(d) : ""); })
						.click(function() {
							if (item.onClick) item.onClick(d, $(this)); 
						})
				)
			});
			row.appendTo(table);
		
		}); // End of each reportOptions.getMoreInfoValues(data) - subtable content generation
		
		var div = $("<div>"); // table's div
		if (table.find("tr").length > 1) {// show the table and title if rows have been added
			// title
			$("<table />")
				.addClass("title-only")
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title3")
								.text(reportOptions.getSubItemMoreInfoKey(data))
						)
				)
				.appendTo(div);
		
			//	Add the tablesorter plugin
			var sortObj = 
				((reportOptions.reportSubItemMoreInfoArraySort) ? 
						{"sortList": reportOptions.reportSubItemMoreInfoArraySort} : {});
			table.tablesorter(sortObj);
			
			table.appendTo(div);
		}
			
		element.append(div);
				
	} // End of generateSubTable
	
} // End of showSubItemMoreInfo

