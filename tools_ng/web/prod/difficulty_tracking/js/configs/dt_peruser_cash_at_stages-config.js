var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 true, // build
	                 true, // platform
	                 false, // user
	],
};

var commas = d3.format(",");
var usersDict = getPlaythroughUsersAsDict();

var orderedResults = [
    {
    	ids: ["ARM1"],
    	name: "Armenian 1 - Franklin and Lamar",
    },
    {
    	ids: ["LS1"],
    	name: "Lester1 - Friend Request",
    },
    {
    	ids: ["TRV1"],
    	name: "Trevor 1 - Mr. Philips",
    },
    {
    	ids: ["FIB1"],
    	name: "FIB 1 - Dead Man Walking",
    },
    {
    	ids: ["FIB4"],
    	name: "FIB 4 - Blitz Play",
    },
    {
    	ids: ["EXL1"],
    	name: "Exile 1 - Minor Turbulence",
    },
    {
    	ids: ["RH2"],
    	name: "The Paleto Score 2A - Finale",
    },
    {
    	ids: ["TRV4"],
    	name: "Trevor 4",
    },
    {
    	ids: ["MIC3"],
    	name: "Michael 3 - The Wrap Up",
    },
    {
    	ids: ["FINA", "FINB", "FINC2"],
    	name: "Finale",
    },
    
];

var reportOptions = {
		
	restEndpoint: config.userMissionCash,
	restEndpointAsync: config.userMissionCashAsync,
	
	processFunction: convertToDict,
	
	hasExtraRestParams: [
	{
		key: "MissionIdentifiers",
	    value: orderedResults.map(function(d) {return d.ids.join()}).join(),
    },
	],
	
	elementId: "line-area-chart",
	backgroundColour: "#333333",
	lineColour: "#ffffff",
	textColour: "#ffffff",
	gridColour: "#666666",
			
	name: function(d) { return d.MissionName; },

	fullName: function(d, user) { return ((user) ? d.MissionName + "<br />(" + user + ")" : d.MissionName); },
	
	value: function(d) { return d.Cash; },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 80},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 160, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "$ ", colour: "#ffffff"}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br /><br />"
				+ "<table>"
					+ "<tr><td>Cash :</td><td class='right'>" + commas(this.value(d)) + " $</td></tr>"
				+ "</table>";

		return content;
	},
		              
};

function convertToDict(array) {
	var dict = {};
	array.map(function(d) {

		var username = (usersDict[d.GamerHandle]) ? (usersDict[d.GamerHandle]) : d.GamerHandle;

		var newData = [];
		if (orderedResults) { // If the results need to be ordered
			$.each(orderedResults, function(i, orderedItem) { // loop over the order list
				var resultExists; // flag for empty ordered positions
				$.each(d.Data, function(j, item) { // loop over the results and arrange them
					if (orderedItem.ids.indexOf(item.MissionId) != -1) {
						item["label"] = orderedItem.name;
						newData.push(item);
						resultExists = true;
						return; // exit the inner j loop
					}
				});
				if (!resultExists) {
					newData.push({"Cash": 0, "MissionName": orderedItem.name, "label" : orderedItem.name})
				}
			});
		}
		else
			newData = d.Data;
		
		//console.log(newData);
		dict[username] = newData;
	});
	
	return dict;
}
