var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var profileStatList = [
    /*
	{ // Extra
		name: "PROP_MISSIONS_BARAI",
		title: "Missions triggered for bar sightings",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BARBA",
		title: "Missions triggered for bayview lodge",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BARBF",
		title: "Missions triggered for bar fes",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BARCO",
		title: "Missions triggered for bar cockotoos",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BAREC",
		title: "Missions triggered for bar eclipse",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BAREL",
		title: "Missions triggered for cafe rojo",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_BARHE",
		title: "Missions triggered for bar hen house",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_MISSIONS_BARHI",
		title: "Missions triggered for bar hi men",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_BARHO",
		title: "Missions triggered for bar hookies",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_MISSIONS_BARIR",
		title: "Missions triggered for bar shenanigans",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BARLE",
		title: "Missions triggered for bar les bianco",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_BARMI",
		title: "Missions triggered for bar mirror park",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_BARPI",
		title: "Missions triggered for bar pitchers",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_MISSIONS_BARSI",
		title: "Missions triggered for bar singletons",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_BARTE",
		title: "Missions triggered for bar tequilala",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_MISSIONS_BARUN",
		title: "Missions triggered for bar enoteca",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_CIND",
		title: "Missions triggered for downtown cinema",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_CINM",
		title: "Missions triggered for morningwood cinema",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_CINV",
		title: "Missions triggered for vinewood cinema",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_CSCR",
		title: "Missions triggered for car scrap yard",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_MISSIONS_PSCR",
		title: "Missions triggered for aircraft scrap yard",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_MISSIONS_TAXI",
		title: "Missions triggered for taxi lot",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_MISSIONS_WEED",
		title: "Missions triggered for weed shop",
		singleMetric: true,
	},
	
	// Bought
	/*
	{ // Extra
		name: "PROP_BOUGHT_BARAI",
		title: "Purchased bar sightings",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BARBA",
		title: "Purchased bayview lodge",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BARBF",
		title: "Purchased bar fes",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BARCO",
		title: "Purchased bar cockotoos",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BAREC",
		title: "Purchased bar eclipse",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BAREL",
		title: "Purchased cafe rojo",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_BARHE",
		title: "Purchased bar hen house",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_BOUGHT_BARHI",
		title: "Purchased bar hi men",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_BARHO",
		title: "Purchased bar hookies",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_BOUGHT_BARIR",
		title: "Purchased bar shenanigans",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BARLE",
		title: "Purchased bar les bianco",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_BARMI",
		title: "Purchased bar mirror park",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_BARPI",
		title: "Purchased bar pitchers",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_BOUGHT_BARSI",
		title: "Purchased bar singletons",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_BARTE",
		title: "Purchased bar tequilala",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_BOUGHT_BARUN",
		title: "Purchased bar enoteca",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_CIND",
		title: "Purchased downtown cinema", 
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_CINM",
		title: "Purchased morningwood cinema",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_CINV",
		title: "Purchased vinewood cinema",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_CMSH",
		title: "Purchased car mod shop",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_CSCR",
		title: "Purchased car scrap yard",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_GOLF",
		title: "Purchased golf club",
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_BOUGHT_PSCR",
		title: "Purchased aircraft scrap yard",
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_BOUGHT_SOCO",
		title: "Purchased sonar collections",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_STRIP",
		title: "Purchased strip club",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_TAXI",
		title: "Purchased taxi lot",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_TOWI",
		title: "Purchased towing impound",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_TRAF",
		title: "Purchased arms trafficking",
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_BOUGHT_WEED",
		title: "Purchased weed shop",
		singleMetric: true,
	},
	
	// Cash Earned
	
	/*
	{ // Extra
		name: "PROP_EARNED_BARAI",
		title: "Cash earned from bar sightings",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BARBA",
		title: "Cash earned from bayview lodge",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BARBF",
		title: "Cash earned from bar fes",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BARCO",
		title: "Cash earned from bar cockotoos",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BAREC",
		title: "Cash earned from bar eclipse",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BAREL",
		title: "Cash earned from cafe rojo",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_BARHE",
		title: "Cash earned from bar hen house",
		getValue: getCashValue,
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_EARNED_BARHI",
		title: "Cash earned from bar hi men",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_BARHO",
		title: "Cash earned from bar hookies",
		getValue: getCashValue,
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_EARNED_BARIR",
		title: "Cash earned from bar shenanigans",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BARLE",
		title: "Cash earned from bar les bianco",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_BARMI",
		title: "Cash earned from bar mirror park",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_BARPI",
		title: "Cash earned from bar pitchers",
		getValue: getCashValue,
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_EARNED_BARSI",
		title: "Cash earned from bar singletons",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_BARTE",
		title: "Cash earned from bar tequilala",
		getValue: getCashValue,
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_EARNED_BARUN",
		title: "Cash earned from bar enoteca",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_CIND",
		title: "Cash earned from downtown cinema",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_CINM",
		title: "Cash earned from morningwood cinema",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_CINV",
		title: "Cash earned from vinewood cinema",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_CMSH",
		title: "Cash earned from car mod shop",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_CSCR",
		title: "Cash earned from car scrap yard",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_GOLF",
		title: "Cash earned from golf club",
		getValue: getCashValue,
		singleMetric: true,
	},
	/*
	{ // Extra
		name: "PROP_EARNED_PSCR",
		title: "Cash earned from aircraft scrap yard",
		getValue: getCashValue,
		singleMetric: true,
	},
	*/
	{ // Extra
		name: "PROP_EARNED_SOCO",
		title: "Cash earned from sonar collections",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_STRIP",
		title: "Cash earned from strip club",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_TAXI",
		title: "Cash earned from taxi lot",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_TOWI",
		title: "Cash earned from towing impound",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_TRAF",
		title: "Cash earned from arms trafficking",
		getValue: getCashValue,
		singleMetric: true,
	},
	{ // Extra
		name: "PROP_EARNED_WEED",
		title: "Cash earned from weed shop",
		getValue: getCashValue,
		singleMetric: true,
	},
  
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Properties",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
			
	reportArrayItems: profileStatList.map(
		function(d) {
			d["requestSubString"] = "_";
			if (!d.hasOwnProperty("getValue"))
				d["getValue"] = getValue;
			return d;
		}),
		
};


//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														//d.name = d.title;
														return d;});
