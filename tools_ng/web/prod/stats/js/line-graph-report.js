
function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	initSvg(reportOptions.graphId);
	
	/*
	$("#chart-filter label").text(reportOptions.inputTextLabel);
	
	$("#chart-filter-input").val(reportOptions.defaultInputText);
	$("#chart-filter-input").focus(function() {
		if($(this).val() == reportOptions.defaultInputText)
			$(this).val("");
	});
	$("#chart-filter-input").blur(function() {
		if($(this).val() == "")
			$(this).val(reportOptions.defaultInputText);
	});
	// IE fix for not firing the change event when pressing enter
	if ($.browser.msie) {
		$("#chart-filter-input").live("keypress", function(e) {
			if (e.keyCode == 13)
				if (requestProcesses)
					prepareGraphArray(requestProcesses);
			});
	}
	else {
		$("#chart-filter-input").change(function() {
			//alert("changed");
			if (requestProcesses)
				prepareGraphArray(requestProcesses);
		});
	}
	
	//$("#chart-filter-input").keyup(function() {
	//	if (requestProcesses)
	//		prepareGraphArray(requestProcesses);
	//});
 
	*/
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();	
}

function generateReport() {

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();

	// 
	if (reportOptions.hasExtraRestParams) {
		$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
			var value;
			if (typeof paramObj.value === "function") {
				value = paramObj.value(pValues.Pairs["GameTypeNames"].split(","));
			}
			else
				value = paramObj.value;
			pValues.Pairs[paramObj.key] = value;
		});
	}
	
	var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
	
	req.sendSingleAsyncRequest(pValues, populateReport);
}

function populateReport(reportData) {
	if (reportOptions.processFunction)
		reportData = reportOptions.processFunction(reportData);

		var graphArray = [];
		$.each(reportData, function(i, element) {
			// This is for converting our data to a format that NVD3 understands
			graphArray.push({
				key: reportOptions.getKey(element),
				values: reportOptions.getValues(element),
				color: config.colourRange(Math.random()*20), 
			});
			
		});

		drawLinegraph(graphArray, reportOptions.graphObject);
}

function drawLinegraph(datum, graphObj) {

	if (datum[0].values.length < 1) {
		d3.select("#" + reportOptions.graphId + " svg g.nvd3").remove();
		Sexy.alert(config.noDataText);
		return;
	}
		
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
			.x(function(d, i) { return graphObj.getX(d, i); })
			.y(function(d) { return graphObj.getY(d); })
			.margin({top: 50, right: 40, bottom: 100, left: graphObj.leftMargin})
			.tooltips(true);
		
		chart.xAxis
			.axisLabel(graphObj.xLabel)
			.tickValues(function() {
				if (datum.length < 2)
					return graphObj.tickValues(datum[0].values);
			})
			.tickFormat(function(d, i) {
				if (datum.length < 2)
					return graphObj.tickFormat(datum[0].values, d, i);
			})
			.rotateLabels(graphObj.rotateLabels);
				
		chart.yAxis
			.axisLabel(graphObj.yLabel)
		
		d3.select("#" + reportOptions.graphId + " svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
		
		nv.utils.windowResize(chart.update);
		
		return chart;
	});
}
