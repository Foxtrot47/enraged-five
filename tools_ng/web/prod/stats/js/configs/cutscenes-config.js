// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var reportOptions = {
	//name: "Cutscenes :: ",
	restEndpoint: config.cutscenesStats,
	restEndpointAsync: config.cutscenesStatsAsync,
	
	processFunction: ungroupCutscenes,
	
	//enablePNGExport: "content-description-controls",
	enableCSVExport: "content-description-controls",
	
	hasFriendlierNames: true,
	
	description: "For each cutscene there is an orange bar - displaying the entire length of the cutscene - and a blue bar" 
		+ " - displaying the average length of the cutscene that was watched before it was skipped. " 
		+ "Hovering the cursor over either bar will display both these piece of information, as well " 
		+ " as the percentage of times the cutscene was watched in full. "
		+ "<br/><br/>"
		+ "Please be aware: there is a small amount of data coming through from SP, regarding SP cutscenes." 
		+ " This data should be ignored.  Only cutscenes whose names begin with 'NET' should be considered.",
	
	/*
	chartGroups: {
		getGroups: function(d) {return d}, // Data are returned grouped
		getGroupName: function(d) {return "Mission : " + d.MissionName},
	    getGroupValues: function(d) {return d.CutsceneViews},
   	},
   	
   	*/
	chartGroups: {
		getGroups: function(d) {return [d]}, // one group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
	    title: "Average Skip Time",
	    colour: config.chartColour2,
	    getName: function(d) {
	    	return ($("#friendlier-names").is(":checked") && d.CutsceneFriendlyName) ? d.CutsceneFriendlyName : d.CutsceneName;
	    },
	    getValue: function(d) {return d.AverageTimeWatched },
	    getObject: function(d) {return d},
	},
	{
	    title: "Overall Duration",
	    colour: config.chartColour1,
	    getName: function(d) {
	    	return ($("#friendlier-names").is(":checked") && d.CutsceneFriendlyName) ? d.CutsceneFriendlyName : d.CutsceneName;
	    },
	    getValue: function(d) {return d.CutsceneDuration },
	    getObject: function(d) {return d},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
					+ "<tr><td>Overall Duration: </td><td class='right'>" 
						+ formatSecs(e.point.Object.CutsceneDuration)
						+ " (" + commasFixed2(e.point.Object.CutsceneDuration) + " secs)"
					+ "</td></tr>"
					+ "<tr><td>Average Skip Time: </td><td class='right'>" 
						+ formatSecs(e.point.Object.AverageTimeWatched)
						+ " (" + commasFixed2(e.point.Object.AverageTimeWatched) + " secs)"
					+ "</td></tr>"
					+ "<tr><td>Percentage of times entire cutscene was watched: </td><td class='right'>"
						+ commasFixed2((e.point.Object.Ended/e.point.Object.Started) * 100) + "%"
					+ "</td></tr>"
					+ "<tr><td>Has Branches: </td><td class='right'>"
						+ e.point.Object.CutsceneHasBranches
					+ "</td></tr>";
		html += "</table>"; 
			
		return html;
	},

	yTickFormat: function(d) {
		return formatSecs(Number(d));
	},
	units: " hh:mm:ss",
	
	chartLeftMargin: 430,
};

//Friendly names by default
$("#friendlier-names").prop("checked", true);

function ungroupCutscenes(data) {
	var allCutscenes = [];
	
	$.each(data, function(i, d) {
		$.each(d.CutsceneViews, function(j, c) {
			c.CutsceneName = d.MissionName + " : " + c.CutsceneName;
			c.CutsceneFriendlyName = 
				(c.CutsceneFriendlyName) ? (d.MissionName + " : " + c.CutsceneFriendlyName) : c.CutsceneFriendlyName;
			allCutscenes.push(c);
		});
	});
	
	return allCutscenes;
}
