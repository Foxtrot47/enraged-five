// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-freemode", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // categories
	     false, // ugc status
	     false, // creators
	     false, // flags 
	     false, // creation dates 
	     ],
};

var matchTypesDict = getMatchTypesDict();
var matchSubTypesDict = getMatchSubTypesDict();
var rosPlatformsDict = getRosPlatformsDict();
var freemodeCategoriesDict = getFreemodeCategoriesDict();

var reportOptions = {
	restEndpoint: config.freemodeContentStats,
	restEndpointAsync: config.freemodeContentStatsAsync,
	
	processFunction: formatData,
	//multipleRequests: generateEndpoints,
			
	isClickable: true,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: true,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		//getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		//getName: function(d) { return matchTypesDict[d.Type]; },
		
		lrMargin: 15,
		
		//show pie chart values on map when having that property
		showOnMap: function(d) {return (d) ? true : false;},
		getMapObjects: function(types) {
			var mapItems = [];

			if (!types)
				return mapItems;

			$.each(types, function(i, type) {
				if (!type.Breakdowns)
					return mapItems;

				$.each(type.Breakdowns, function(i, breakdown) {
					
					$.each(breakdown.Value, function(i, item) {						
						$.each(item.Instances, function(i, instance) {
							
							if (!instance.hasOwnProperty("StartCoordinate")) {
								instance["StartCoordinate"] = {};
							}
														
							if (filterMap(instance.Rank)) {
								instance.StartCoordinate.Rank = instance.Rank
								instance.StartCoordinate.Colour = type.Colour;
								instance.StartCoordinate.Type = matchTypesDict[type.Type];
								
								mapItems.push(instance.StartCoordinate);
							}
							
						})
					});
				})
				
			});
			
			return mapItems;
		},
		clickFiltersMap: true,
		drawMapByDefault: true, // draw the map on piechart load
	},
	// This is the breakdown piechart
	breakdown: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: true,
		donut: false,
		donutLabelsOutside: false,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label + " - " + d.Key },
		getValuesArray: function(d) { return d.Value; }, // this is the values array inside the returned object
		getMetadata: function(d) { return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.Name; }, // this is added to the results
		
		lrMargin: 15,
		
		// Pass the breakdown object (need to include label/values/metadata)
		getObjects: function(d) {
			return (d.Breakdowns) 
				? d.Breakdowns.map(function(b, i) {
						b["metadata"] = (i==0) ? ({BreakdownMap : true}) : d.metadata;
						b["label"] = d.label;
						return b; 
					})
				: [];
		},
		//show pie chart values on map when having that property
		showOnMap: function(d) {return (d && d.BreakdownMap) ? true : false;}, 
		
		getMapObjects: function(types) {
			var mapItems = [];
						
			if (!types)
				return mapItems;
			
			$.each(types, function(i, type) {
				
				$.each(type.Instances, function(i, instance) {
					
					if (!instance.hasOwnProperty("StartCoordinate")) {
						instance["StartCoordinate"] = {};
					}
				
					if (filterMap(instance.Rank)) {
						instance.StartCoordinate.Rank = instance.Rank
						instance.StartCoordinate.Colour = type.Colour;
						instance.StartCoordinate.SubType = type.Name;
						
						mapItems.push(instance.StartCoordinate);
					}

				})
			});
			return mapItems;
		},
		clickFiltersMap: true,
		//mapKeepColours: true, // don't renew the colours
		
	},
	
	hasBreakdownMap: "breakdown-map",
	mapVisible: true, //never hide the map
	mapGetX: function(d) { return d.X; },
	mapGetY: function(d) { return d.Y; },
	mapTooltipContent: function(d) {
		var content = "<div class='title'>  </div><br />"
			+ "<table>";
		
		if (d.Type) 
			content += ("<tr><td>Mode: </td><td>" + d.Type + "</td></tr>");
		
		if (d.SubType) 
			content += ("<tr><td>Sub-Type: </td><td>" + d.SubType + "</td></tr>");
		
		if (d.Rank) 
			content += ("<tr><td>Rank: </td><td>" + d.Rank + "</td></tr>");
		
		content += ("<tr><td>Location: </td><td>" 
						+ "(" + d.X + ", " + d.Y + ")"  + "</td></tr>");
				
		content += "</table>";

		return content;
	}
	
};

var radioButtonsPrefix = "freemode-content";
var rangeFilterLabel = "&nbsp; :: &nbsp; Filter Map (Rank) :";
var rangeFilterMin = {
	id: "RankMin",
	value: 0,
}
var rangeFilterMax = {
	id: "RankMax",
	value: 75,
}

function setReportOptions() {
	var ttlabel = "Content";
	var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var array = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[0];
	var name = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[1];
	var metric = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[2];
	var unit = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[3];
	
	reportOptions.main.title = label;
	
	reportOptions.main.unit = " " + unit;
	reportOptions.breakdown.unit = reportOptions.main.title;
	
	reportOptions.main.getValuesArray = function(d) {return d.values[array]; },
	
	reportOptions.main.getName = function(d) {
		if (name == "Type")
			return matchTypesDict[d[name]];
		else {
			if (d[name] == -1)
				return  "Max Players";
			else
				return d[name] + " Players";
		}
	},
	
	reportOptions.main.getValue = function(d) {
		return d[metric];
	};
	reportOptions.breakdown.getValue = function(d) {
		return d.Count;
	}; 
	
	reportOptions.main.tooltipContent = function(key, y, e, graph) {
		var sum = d3.sum(graph.container.__data__[0].values, function(d) {
			return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
		});
		// Use e.value instead of y, y is a formated string and parsing to number fails
		var percentage = ((e.value/sum)*100).toFixed(2);
		
		var html = "<h3>" + key + "</h3><br/>";
		
		var commasFunc = (unit == "$") ? cashCommasFixed : commasFixed2;
		var ttUnit = (unit == "$") ? "" : unit;
		
		html += "<table>"
			+ "<tr><td>" + ttlabel + ":</td>"
			+ "<td>" + commasFunc(e.value) + " " + ttUnit + " (" + percentage + "%)" + "</td></tr>"
			+ "<tr><td>Total:</td><td>" +  commasFunc(sum) + " " + ttUnit + "</td></tr>";
				
		html += "</table>";
		
		return html;
	};
	
	reportOptions.breakdown.tooltipContent = function(key, y, e, graph) {
		var sum = d3.sum(graph.container.__data__[0].values, function(d) {
			return reportOptions.breakdown.getValue(d);
		});
		// Use e.value instead of y, y is a formated string and parsing to number fails
		var percentage = ((e.value/sum)*100).toFixed(2);
		
		var html = "<h3>" + key + "</h3><br/>";
		
		var commasFunc = (unit == "$") ? cashCommasFixed : commasFixed2;
		var ttUnit = (unit == "$") ? "" : unit;
		
		html += "<table>"
			+ "<tr><td>" + ttlabel + ":</td>"
			+ "<td>" + commasFunc(e.value) + " " + ttUnit + " (" + percentage + "%)" + "</td></tr>"
			+ "<tr><td>Total:</td><td>" +  commasFunc(sum) + " " + ttUnit + "</td></tr>";
				
		html += "</table>";
		
		return html;
	};

}

function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<span>")
			.attr("id", "content-description-radiometrics")
			//.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("MissionTypes|Type|PiecesOfContent|items")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Content By Mode  ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-3")
					.val("ContentPerPlayerCounts|Key|Value|items")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-3")
					.text("Content By Players   ")
			)
	)
	.append(
		$("<span>")
			.attr("id", "content-description-rangefilter")
			//.css("float", "left")
			.append(
				$("<label>")
					.html(rangeFilterLabel)
			)
			.append(
				$("<input>")
					.attr("type", "text")
					.attr("name", rangeFilterMin.id)
					.attr("id", rangeFilterMin.id)
					.val(rangeFilterMin.value)
			)
			.append(
				$("<input>")
					.attr("type", "text")
					.attr("name", rangeFilterMax.id)
					.attr("id", rangeFilterMax.id)
					.val(rangeFilterMax.value)
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
	$("#content-description-rangefilter :input[type=text]").change(function() {
		//setReportOptions();
		
		drawCharts();
	});
};

/*
function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;

	// Need to get the coordinates from a different endpoint
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.freemodeContentDetailsStats,
			restAsyncUrl: config.restHost + config.freemodeContentDetailsStatsAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}
*/

/*
function addCoordinates(data) {
	//create a dict of types - subtypes - coords
	var coords = {};
	$.each(data[1][0].response, function(i, contentItem) {
		var subTypeName = (matchSubTypesDict.hasOwnProperty(contentItem.Type)) 
				? matchSubTypesDict[contentItem.Type][contentItem.SubType] : "";
		if (contentItem.StartCoordinates && subTypeName) {
			if (!coords.hasOwnProperty(contentItem.Type)) {
				coords[contentItem.Type] = {};
				coords[contentItem.Type][subTypeName] = [contentItem];
			}
			else {
				if (!coords[contentItem.Type].hasOwnProperty(subTypeName))
					coords[contentItem.Type][subTypeName] = [contentItem];
				else
					coords[contentItem.Type][subTypeName].push(contentItem);
			}
			
		}
		else if (contentItem.StartCoordinates && !subTypeName) {
			if (!coords.hasOwnProperty(contentItem.Type)) {
				coords[contentItem.Type] = [contentItem];
			}
			else {
				coords[contentItem.Type].push(contentItem);
			}
		}
			
 	});
	
	// Add the coords to the respective items
	$.each(data[0][0].response.MissionTypes, function(i, missionType) {
		if (coords.hasOwnProperty(missionType.Type)) {
			// Add to breakdowns where available
			$.each(missionType.Breakdowns, function(j, breakdown) {
				
				$.each(breakdown.Value, function(k, item) {
					if (coords[missionType.Type].hasOwnProperty(item.Key)) {
						item["Coords"] = coords[missionType.Type][item.Key];
					}
				});
			});
			
			// Add to top level
			if ($.isArray(coords[missionType.Type]))
				missionType["Coords"] = coords[missionType.Type];
			else {
				missionType["Coords"] = [];
				$.each(coords[missionType.Type], function(k, v) {
					missionType["Coords"] = missionType["Coords"].concat(v);
				})
			}
				
		}
	});	
	
	//console.log(data[0][0].response);
		
	// calling these functions here to make sure it's after the page generation
	addMetrics();
	setReportOptions();
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data[0][0].response,
	   	"metadata": {},
	};
}
*/

function formatData(data) {
	// calling these functions here to make sure it's after the page generation
	addMetrics();
	setReportOptions();
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data,
	   	"metadata": {},
	};
}

function filterMap(rank) {
	var rankMin = Number($("#" + rangeFilterMin.id).val())
	var rankMax = Number($("#" + rangeFilterMax.id).val())
	
	if ((rank >= rankMin) && (rank <= rankMax)) {
		return true;
	}
	else {
		return false;
	}
}