// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "SP_FINAL_DECISION",
     ],
     name: "Finale",
     altNames: [
       	"0. None",
       	"1. Killed Michael",
      	"2. Killed Trevor",
       	"3. Saved Michael and Trevor",
     ],
     ignoredIndices: [0],
     ignoreOutOfRange: true,
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_KILLED_DR_FRIEDLANDER",
     ],
     name: "Shrink 5",
     altNames: [
         "Not Killed Dr Friendlander",
         "Killed Dr Friendlander",
     ],
     ignoredIndices: [],
     ignoreOutOfRange: true,
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_KILLED_ORTEGA",
     ],
     name: "Trevor1",
     altNames: [
         "Not Killed Ortega",
         "Killed Ortega",
     ],
     ignoredIndices: [],
     ignoreOutOfRange: true,
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "SP_KILLED_AL",
     ],
     name: "Nigel3",
     altNames: [
        "Not Killed Al",
		"Killed Al",
     ],
     ignoredIndices: [],
     ignoreOutOfRange: true,
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     unit: "players",
},
{
	types: [
	   "RC_WALLETS_RECOVERED",
	   "RC_WALLETS_RETURNED"
     ],
     name: "Money Returned",
     altNames: [
        "No",
		"Yes",
     ],
     ignoredIndices: [],
     ignoreOutOfRange: true,
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     unit: "times",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name; }, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value; },
		getObject: function(d) { return (d); },
		
		// unit: "players" - not always
		lrMargin: 25,
	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			//copiedPvalues.Pairs["IgnoreDefaultValues"] = true;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		
		var values = [];
		
		$.each(data[i], function(j, typeResult) {
			values = values.concat(
					typeResult.response
					.filter(function(d, i) { // filter out values outside the expected range b*#1670460
						return (
							!((availableChart.ignoreOutOfRange) && (i >= availableChart.altNames.length))
						)
					})
					.filter(function(d, i) { // or filter out if index is unwanted b*#1646916
						return (availableChart.ignoredIndices.indexOf(i) == -1)
					})
					.map(function(d) {
						d["name"] = profStat.getName(d, availableChart, j); 
						d["value"] = profStat.getValue(d, availableChart, j);
						return d;
					}
				)
			);
		});
		
		array.push(
			{
			   	"label": availableChart.name,
			   	"values": values,
			   	"metadata": {"unit": availableChart.unit},
			}
		);
	});
	
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
