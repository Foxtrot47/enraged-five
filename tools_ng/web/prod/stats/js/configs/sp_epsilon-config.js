// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "MS_EP8_MONEY_STOLEN",
     ],
     name: "Epsilon 8",
     altNames: [
       	"Followed Instructions",
       	"Stole Money",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     //groupNonZero: true, // for 0 / non 0 values piechart
     unit: "players",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	isClickable: false,
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d[0].label; },
		getValuesArray: function(d) {return d[0].values; },
		getMetadata: function(d) {return d[0].metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name; }, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value; },
		getObject: function(d) { return (d); },
		
		// unit: "players" - not always
		lrMargin: 15,
	},
	// This is a lingraph
	breakdown: {
		elementId: "line-area-chart",
		backgroundColour: "transparent",
		lineColour: config.chartColour1,
		textColour: "#000000",
		gridColour: "#333333",
					
		name: function(d) { return d.MissionName; },
		value: function(d) { return (d.TotalAttempts) ? (d.TotalPassed/d.TotalAttempts)*100 : 0; },
		
		fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
		label: function(d) {return ((d.label) ? d.label : this.name(d)); },	
		xLabel: "Epsilon Missions",
		yLabel: "Percentage of Times Completed (%)",
			
		orientation: "horizontal",
		margin: {top: 10, right: 10, bottom: 10, left: 10},
			
		hideLegend: true,
		legend: {height: 30, width: 180, rectWidth: 18},
		legendDataConst : [],
		legendDataVar: {label : "", colour: config.chartColour1}, // keep this colour in sync with lineColour 
			
		valueTooltipContent: function(d, b) {
			var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + "</div><br />"
				+ "<table>"
						+ "<tr><td>Percentage of Times Completed :</td><td class='right'>" 
							+ commasFixed2(this.value(d)) 
						+ " %</td></tr>"
						+ "<tr><td>Percentage of Times Failed :</td><td class='right'>" 
							+ commasFixed2((d.TotalAttempts) ? (d.TotalFailed/d.TotalAttempts)*100 : 0)
						+ " %</td></tr>"
						+ "<tr><td>Percentage of Times Skipped :</td><td class='right'>" 
							+ commasFixed2((d.TotalAttempts) ? (d.TotalSkipped/d.TotalAttempts)*100 : 0)
						+ " %</td></tr>"
						+ "<tr><td>Percentage of Times Canceled :</td><td class='right'>" 
							+ commasFixed2((d.TotalAttempts) ? (d.TotalCancelled/d.TotalAttempts)*100 : 0)
						+ " %</td></tr>"
					+ "</table>";
				return content;
		},
		
		getObject: function(d) {return d[1];},

	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
	var statName = getStatNames(profileStatList[0]).join(",");
	
	var copiedPvalues = $.extend(true, {}, pValues);
	
	copiedPvalues.Pairs["StatNames"] = statName;
	copiedPvalues.Pairs["BucketSize"] = profileStatList[0].bucketSize;
				
	var endpointObjects = [
	{
		restUrl: config.restHost + reportOptions.restEndpoint,
		restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
		pValues: [copiedPvalues],
	},
	{
		restUrl: config.restHost + config.missionsStats,
		restAsyncUrl: config.restHost + config.missionsStatsAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	];
				
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	     	
	// Prepare for piechart
	//var pieData = profStat.groupNonZero(data[0][0].response);
	var pieData = data[0][0].response;
			
	var pieValues = pieData.map(function(d) {
		d["name"] = profStat.getName(d, profileStatList[0]); 
		d["value"] = profStat.getValue(d, profileStatList[0]);
		return d;
	});
		
	array.push(
		{
		   	"label": profileStatList[0].name,
		   	"values": pieValues,
		   	"metadata": {"unit": profileStatList[0].unit},
		}
	);
	
	// Calc the total users
	var epsilonMission = data[1][0].response.filter(
			// return only the epsilon missions
			function(d) { return d.MissionName.match("^RC - Epsilonism"); }
		)
		.sort(function(a, b) {return (a.MissionName < b.MissionName) ? -1 : 1});
		
	var lineDataKey = reportOptions.breakdown.xLabel;
	array.push({lineDataKey: epsilonMission});
	
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
