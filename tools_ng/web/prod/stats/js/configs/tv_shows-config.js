//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.tvShowsStats,
	restEndpointAsync: config.tvShowsStatsAsync,
	
	multipleRequests: generateEndpoints,
	processFunction: dummy,
	
	isClickable: false,
	hasFriendlierNames: true,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName : d.Name;
		},
		
		lrMargin: 100,
	},
	// This is the breakdown barchart
	breakdown: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d.color; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName : d.Name;
		},
		
		getYLabel: function(d) {return this.unit; },
		
		matchColoursFromPieElement: "piechart",
				
		leftMargin: 250,
		
		getObject: function(d) { return d; },
	},
	
	/*
	availableCharts: [
	{
		title: "Total Watching Time",
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName : d.Name;
		},
		getValue: function(d) {return (Number(d.Duration)/config.anHourInSecs)},
		label: "Hours",
		units: "hours",
		leftMargin: leftBarMargin,
		pieMargin : pieMargin,
		description: "Total listening time spent to each tv show in hours",
	},
	{
		title: "Total Times Tuned",
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName : d.Name;
		},
		getValue: function(d) {return Number(d.Count)},
		label: "No of Times",
		units: "times",
		leftMargin: leftBarMargin,
		pieMargin : pieMargin,
		description: "Total times tuned into each tv show",
	},
	{
		title: "Unique Players",
		getName: function(d) {
			return ($("#friendlier-names").is(":checked") && d.FriendlyName) 
						? d.FriendlyName : d.Name;
		},
		getValue: function(d) {return Number(d.UniqueGamers)},
		label: "No of Players",
		units: "players",
		leftMargin: leftBarMargin,
		pieMargin : pieMargin,
		description: "Number of unique players watched each tv show",
	},
	],
	*/
};

function setReportOptions() {
	var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var metric = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[0];
	var unit = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[1];
	
	reportOptions.main.title = label;
	
	reportOptions.main.unit = " " + unit;
	reportOptions.breakdown.unit = reportOptions.main.unit;
		
	reportOptions.main.getValue = function(d) {
		var value;
		if (unit == "hours")
			value = (d[metric]/config.anHourInSecs);
		else if (unit == "km")
			value = (d[metric]/config.aKmInMetres);
		else
			value = d[metric];
		
		return value; 
	};
	reportOptions.breakdown.getValue = reportOptions.main.getValue; 
	
	reportOptions.main.tooltipContent = function(key, y, e, graph) {
		var sum = d3.sum(graph.container.__data__[0].values, function(d) {
			return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
		});
		// Use e.value instead of y, y is a formated string and parsing to number fails
		var percentage = ((e.value/sum)*100).toFixed(2);
		var metadata = graph.container.__data__[0].metadata;
		
		var html = "<h3>" + key + "</h3><br/>";
		
		var commasFunc = (unit == "$") ? cashCommasFixed : commasFixed2;
		var ttUnit = (unit == "$") ? "" : unit;
		
		html += "<table>"
			+ "<tr><td>" + label + ":</td>"
			+ "<td>" 
				+ commasFunc(e.value) + " " + ttUnit 
				+ ((metric != "UniqueGamers") ? (" (" + percentage + "%)") : "")
			+ "</td></tr>"
			
		if (metric != "UniqueGamers")
			html += "<tr><td>Total:</td><td>" +  commasFunc(sum) + " " + ttUnit + "</td></tr>";
		else
			html += "<tr><td>Online Players:</td><td>" 
					+  commasFunc(metadata.onlinePlayers)
					+ ((metadata.onlinePlayers) ? (" (" + commasFixed2((e.value/metadata.onlinePlayers)*100) + "%)") : "")
				+ "</td></tr>";
				
		html += "</table>";
		
		return html;
	};

}

var radioButtonsPrefix = "telemetry-radio";
function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiometrics")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("Duration|hours")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Total Watching Time   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-2")
					.val("Count|times")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-2")
					.text("Total Times Tuned   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-3")
					.val("UniqueGamers|players")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-3")
					.text("Unique Players   ")
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function dummy(data) {
	// calling these functions here to make sure it's after the page generation
	addMetrics();
	setReportOptions();
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data[0][0].response,
	   	"metadata": {"onlinePlayers" : data[1][0].response},
	};
}

