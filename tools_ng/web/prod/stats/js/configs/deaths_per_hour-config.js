//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	"DEATHS",
	"TOTAL_PLAYING_TIME"
	],
	requestSubString: "_",
	name: "Deaths per Hour",
	description: "No of players per bucket of 0.5 deaths per hour",

	label: "No of players",
	unit: "players",
	
	bucketSize: (0.5 / config.anHourInMillisecs), // Bucket of 0.5 deaths per hour in millisecs,
	
	perHour: true,
}
];

var reportOptions = {
	restEndpoint: config.profileStatsDividedDiff,
	restEndpointAsync: config.profileStatsDividedDiffAsync,
			
	processFunction: formatData,
		
	availableCharts: profileStatList,
	multipleRequests: generateEndpoints,
		
	isClickable: false,
	
	//description: profileStatList[0].description,
	
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	
	// This is a piechart
	main: {
		title: profileStatList[0].name,
			
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		//sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) { return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) { return d.name;},
		getValue: function(d) { return d.value; },
		getObject: function(d) { return d; },
		
		lrMargin: 20,
	},
	// This is the breakdown barchart
	breakdown: {
		title: profileStatList[0].name,
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		//sortByValueDesc: true,

		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d.color; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) { return d.name; },
		getValue: function(d) { return d.value; },
		
		getYLabel: function(d) { return profileStatList[0].label; },
		
		matchColoursFromPieElement: "piechart",
				
		leftMargin: 120,
		
		getObject: function(d) { return d; },
	},
	
}

function generateEndpoints(pValues) {
	var endpointObjects = [];
	
var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		var copiedPvalues = $.extend(true, {}, pValues);
			
		copiedPvalues.Pairs["NumeratorStatNames"] = statNames[0];
		copiedPvalues.Pairs["DenominatorStatNames"] = statNames[1];
		copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
		pValuesArray.push(copiedPvalues);
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		);
		
	});
		
	return endpointObjects;
}

var radioButtonsPrefix = "deaths-radio";
function setReportOptions() {
	//var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var value = $(":radio[name=" + radioButtonsPrefix + "]:checked").val();
	
	if (value == "Value") {
		reportOptions.main.sortByValueDesc = true;
		reportOptions.main.getSortingValueAsc = null;
	} 
	else if (value == "Bucket") {
		reportOptions.main.sortByValueDesc = false;
		reportOptions.main.getSortingValueAsc = function(d) {
			return d.bucketMin;
		};
	}
	
	reportOptions.breakdown.sortByValueDesc = reportOptions.main.sortByValueDesc;
	reportOptions.breakdown.getSortingValueAsc = reportOptions.main.getSortingValueAsc;
}

function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiooptions")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("Value")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Order by Value   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-2")
					.val("Bucket")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-2")
					.text("Order By Bucket   ")
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function formatData(data) {
	addMetrics();
	setReportOptions();
	
	var pieValues = [];
	
	//console.log(data[0][0].response);
	$.each(data[0][0].response, function(i, bucketResult) {
		pieValues.push({
			"name": profStat.getName(bucketResult, profileStatList[0]), 
			"value": profStat.getValue(bucketResult, profileStatList[0]),
			"bucketMin": bucketResult.BucketMin,
		});
	});
	
	// Return an object
	return {
		"label": profileStatList[0].name,
		"values": pieValues,
		"metadata": {"unit": profileStatList[0].unit},
	};

}

function getStatNames(profileStat) {
	
	var gameTypes = ($("#game-types").val()) ? $("#game-types").val() : config.gameTypes;
	var character = ($("#character").val()) ? $("#character").val() : null;
	var statNames = [];
	
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, gameTypes, character).join(",")
		);
	});
		
	return statNames;
}

