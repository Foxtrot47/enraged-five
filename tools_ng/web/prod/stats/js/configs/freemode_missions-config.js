// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-freemode", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // character
	     false, // dates+builds
	     false, // mission categories
	     false, // creators gamertags
	     false, // created dates
	     ],
};

var matchTypesDict = getMatchTypesDict();
var freemodeCategoriesDict = getFreemodeCategoriesDict();
var rosPlatformsDict = getRosPlatformsDict();

var reportOptions = {
	restEndpoint: config.freemodeMissionsStats,
	restEndpointAsync: config.freemodeMissionsStatsAsync,
	
	multipleRequests: generateEndpoints,
	processFunction: calcTotals,
	
	isClickable: true,
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	
	availableMetrics: [
    {
    	name: "Time Playing   ",
    	metric: "TimeSpentPlaying",
    	unit: "hours"
    },
    {
    	name: "Times Played   ",
    	metric: "TimesPlayed",
    	unit: "times"
    },
    {
    	name: "Unique Players   ",
    	metric: "UniqueGamers",
    	unit: "players"
    },
    {
    	name: "Total Players   ",
    	metric: "TotalGamers",
    	unit: "players"
    },
	],
	
	/*
	hasExtraRestParams: [
    {
		key: "MaxVariants",
	    value: 1000,
	},
	],
	*/
	
	// This is a piechart
	main: {
		legend: true,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		getName: function(d) {return (matchTypesDict.hasOwnProperty(d.MatchType) ? matchTypesDict[d.MatchType] : new String(d.MatchType)); },
		
		lrMargin: 25,
	},
	// This is the breakdown piechart
	breakdown: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: true,
		donut: false,
		donutLabelsOutside: false,
		
		sortByValueDesc: true,

		getPieLabel: function(d) {return d.BreakdownName},
		getValuesArray: function(d) {return d.Items; }, // this is the values array inside the returned object
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return (d.Name);},
		
		lrMargin: 15,
		
		// Pass the breakdown object (need to include label/values/metadata)
		getObjects: function(d) {
			return ([{Items: d.Variants, BreakdownName: d.label}].concat(d.Breakdowns))
				.map(function(b, i) {
					b["metadata"] = (i==0) ? ($.extend({}, d.metadata, {BreakdownMap : true})) : d.metadata;
					return b;
				}); 
			},
		
		showOnMap: function(d) {return (d && d.BreakdownMap) ? true : false;}, //show pie chart values on map when having that property
		
		getMapObjects: function(d) {return d},
		drawMapByDefault: true, // draw the map on piechart load
	},
	
	hasBreakdownMap: "breakdown-map",
	mapGetX: function(d) {return d.StartCoordinates.X; },
	mapGetY: function(d) {return d.StartCoordinates.Y; },
	mapTooltipContent: function(d) {
		var content = "<div class='title'> " + d.Name + "</div><br /><br />"
			+ "<table>";
		
		content += ("<tr><td>Location: </td><td>" 
						+ "(" + d.StartCoordinates.X + ", " + d.StartCoordinates.Y + ")"  + "</td></tr>");
				
		content += ("<tr><td>UGC ID: </td><td>" + d.UGCIdentifier  + "</td></tr>");
			
		content += "</table>";

		return content;
	}
};

function setReportOptions() {
	var label = $(":radio[name=missions-freemode-radio]:checked + label").text();
	var metric = $(":radio[name=missions-freemode-radio]:checked").val().split("|")[0];
	var unit = $(":radio[name=missions-freemode-radio]:checked").val().split("|")[1];
	
	reportOptions.main.title = label;
	
	reportOptions.main.unit = " " + unit;
	reportOptions.breakdown.unit = reportOptions.main.title;
		
	reportOptions.main.getValue = function(d) {
		if (!d.hasOwnProperty(metric))
			return d["UniqueGamers"];
		
		var value;
		if (unit == "hours")
			value = (d[metric]/config.anHourInSecs);
		else if (unit == "km")
			value = (d[metric]/config.aKmInMetres);
		else
			value = d[metric];
		
		return value; 
	};
	reportOptions.breakdown.getValue = reportOptions.main.getValue; 
	
	reportOptions.main.tooltipContent = function(key, y, e, graph) {
		var sum = d3.sum(graph.container.__data__[0].values, function(d) {
			return (!d.disabled) ? reportOptions.main.getValue(d) : 0;
		});
		// Use e.value instead of y, y is a formated string and parsing to number fails
		var percentage = ((e.value/sum)*100).toFixed(2);
		var metadata = graph.container.__data__[0].metadata;
		
		var html = "<h3>" + key + "</h3><br/>";
		
		var commasFunc = (unit == "$") ? cashCommasFixed : commasFixed2;
		var ttUnit = (unit == "$") ? "" : unit;

		// Special case for waves
		if (e.point.hasOwnProperty("TimesReached")) {
			html += "<table>"
				+ "<tr><td>" + "Unique Players" + ":</td>"
				+ "<td>" + commasFunc(e.value) + " " + "players" + " (" + percentage + "%)" + "</td></tr>";
		}
		else {
			html += "<table>"
				+ "<tr><td>" + label + ":</td>"
					+ "<td>"
						+ commasFunc(e.value) + " " + ttUnit 
						+ ((metric != "UniqueGamers") ? (" (" + percentage + "%)") : "")
					+ "</td></tr>";
			
			if (metric != "UniqueGamers")
				html += "<tr><td>Total:</td><td>" +  commasFunc(sum) + " " + ttUnit + "</td></tr>";
			else
				html += "<tr><td>Online Players:</td><td>" 
							+  commasFunc(metadata.onlinePlayers)
							+ ((metadata.onlinePlayers) ? (" (" + commasFixed2((e.value/metadata.onlinePlayers)*100) + "%)") : "")
						+ "</td></tr>";
			
		}
		
		if ((typeof e.point.TotalGamers !== "undefined") && (typeof e.point.TimesPlayed !== "undefined")) {
			html += "<tr><td>Average Players:</td><td>" 
				+ commasFixed2(e.point.TotalGamers/e.point.TimesPlayed)
				+ "</td></tr>";
			/*
			html += "<tr><td>Total Players:</td><td>" 
				+ commasFixed2(e.point.TotalGamers)
				+ "</td></tr>";
			*/
		}
				
		if ((typeof e.point.TimeSpentPlayingToCompletion !== "undefined") && e.point.TimesPlayedToCompletion) {
			html += "<tr><td>Users Quit:</td><td>" 
				+ commasFixed2(e.point.TotalGamers - e.point.TimesPlayedToCompletion)
				+ "</td></tr>";
			
			html += "<tr><td>Average Completion Time:</td><td>" 
					+ formatSecs(e.point.TimeSpentPlayingToCompletion/e.point.TimesPlayedToCompletion)
					+ "</td></tr>";
		}
		
		if (typeof e.point.AverageRating !== "undefined")
			html += "<tr><td>Average Rating:</td><td>" + commasFixed2(e.point.AverageRating) + "</td></tr>";
		
		if (typeof e.point.UGCIdentifier !== "undefined")
			html += "<tr><td>UGC ID:</td><td>" + e.point.UGCIdentifier + "</td></tr>";
		if (typeof e.point.CreatedDate !== "undefined")
			html += "<tr><td>Created At:</td><td>" + parseJsonDate(e.point.CreatedDate).toUTCString() + "</td></tr>";
		
		var creatorPlatform = (e.point.CreatorPlatform) ? (" (" + rosPlatformsDict[e.point.CreatorPlatform] + ")") : "";
		if (typeof e.point.Creator !== "undefined")
			html += "<tr><td>Creator:</td><td>" + e.point.Creator + creatorPlatform + "</td></tr>";
		
		if (typeof e.point.Category !== "undefined")
			html += "<tr><td>Category:</td><td>" + freemodeCategoriesDict[e.point.Category] + "</td></tr>";
		
		if (typeof e.point.IsPublished !== "undefined")
			html += "<tr><td>UGC Status:</td><td>" 
				+ ((e.point.IsPublished) ? "Published" : "Saved") 
				+ "</td></tr>";
				
		if (typeof e.point.TotalXP !== "undefined")
			html += "<tr><td>Total RP:</td><td>"
				+ commasFixed2(e.point.TotalXP) + "</td></tr>";
				
		if ((typeof e.point.TotalXP !== "undefined") && (typeof e.point.TotalGamers !== "undefined"))
			html += "<tr><td>Average RP:</td><td>"
				+ ((e.point.TotalGamers) ? commasFixed2(e.point.TotalXP / e.point.TotalGamers) : 0) + "</td></tr>";
		
		//if (typeof e.point.Description !== "undefined")
		//	html += "<tr><td>Description:</td><td>" + e.point.Description + "</td></tr>";
		
		html += "</table>";
		
		return html;
	};
	reportOptions.breakdown.tooltipContent = reportOptions.main.tooltipContent;
}

function addMetrics() {
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiometrics")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", "missions-freemode-radio")
					.attr("id", "missions-freemode-radio-1")
					.val("TimeSpentPlaying|hours")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", "missions-freemode-radio-1")
					.text("Time Playing   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", "missions-freemode-radio")
					.attr("id", "missions-freemode-radio-2")
					.val("TimesPlayed|times")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", "missions-freemode-radio-2")
					.text("Times Played   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", "missions-freemode-radio")
					.attr("id", "missions-freemode-radio-3")
					.val("UniqueGamers|players")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", "missions-freemode-radio-3")
					.text("Unique Players   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", "missions-freemode-radio")
					.attr("id", "missions-freemode-radio-4")
					.val("TotalGamers|players")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", "missions-freemode-radio-4")
					.text("Total Players   ")
			)
	);
	
	$(":radio[name=missions-freemode-radio]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
	pValues.Pairs["MaxVariants"] = 1000;
	pValues.Pairs["GameTypeNames"] = config.gameTypes[1]; // force multiplayer game types b* #1612511
	
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.onlineGamerCount,
			restAsyncUrl: config.restHost + config.onlineGamerCountAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function calcTotals(data) {	
	// calling these functions here to make sure it's after the page generation
	addMetrics();
	setReportOptions();
	
	return	{
	   	"label": "", // will be updated by setReportOptions()
	   	"values": data[0][0].response,
	   	"metadata": {"onlinePlayers" : data[1][0].response},
	};
}
