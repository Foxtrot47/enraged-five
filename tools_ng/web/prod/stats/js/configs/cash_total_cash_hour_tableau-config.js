// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-tableau", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	tableauEndpoint: config.tableauEndpoint,
	tableauReportDev: "EarnSpendGivePurchandXPbyPlayedHours-DEV_0/MoneyandRPbyPlayedHours",
	tableauReportProd: "EarnSpendGivePurchandXPbyPlayedHours-PROD/MoneyandRPbyPlayedHours",
	oldTelemetryReport: config.webHost + "/stats/cash_total_cash_hour.html",
};