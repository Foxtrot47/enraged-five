var navigationTitle = "Web Tools";
var navigationItems = [
	{
		title: "Home",
		href: "index.html",
		subItems: [],
	},
	{
		title: "Statistics",
		href: "stats/index.html",
		subItems: [],
	},
	{
		title: "All Profile Stats",
		href: "all-stats.html",
		hide: true,
		subItems: [],
	},
	{
		title: "Session Timeline",
		href: "session_timeline.html",
		hide: true,
		subItems: [],
	},
	/* --
	{
		title: "Map Telemetry",
		href: "map_telemetry.html",
		subItems: [],
	},
	*/
	{
		title: "Map Exports",
		href: "map_exports/index.html",
		subItems: [],
	},
	{
		title: "Capture Stats",
		href: "capture_stats/index.html",
		subItems: [],
	},
	{
		title: "Asset Stats",
		href: "asset_stats/index.html",
		subItems: [],
	},
	{
		title: "Difficulty Tracking",
		href: "difficulty_tracking/index.html",
		subItems: [],
	},
	/*
	{
		title: "Reusable Reports",
		href: "/reusable_reports/index.html",
		subItems: [],
	},
	*/
	{
		title: "Social Club Admin",
		href: "social_club_admin.html",
		hide: true,
		subItems: [],
	},
	{
		title: "Manifest",
		href: "manifest.html",
		//hide: true,
		subItems: [],
	}
	/*
	{
		title: "Universal Log Viewer",
		href: "/ulogs/index.html",
		subItems: [],
	},
	*/
];
