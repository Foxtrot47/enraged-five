var profStats = new ProfileStats();
var enableCSVExport = "content-description";
var csvColumns = [],
	csvRows = [];

var statSections = [
	{
		name: "Statistics",
		subPath: "stats/",
		path: "stats/js/configs/stats-config.js",
		tableId: "manifest-stats-table",
	},
	{
		name: "Difficulty Tracking",
		subPath: "difficulty_tracking/",
		path: "difficulty_tracking/js/configs/difficulty_tracking-config.js",
		tableId: "manifest-df-table",
	},
];

function generateManifest() {
	initHeaderAndFilters(null); // no header
	
	csvColumns = ["Profile Stats"];
	csvRows.push([]);
	
	$.each(statSections, function(i, statSection) {
		//csvRows.push([]);
		//csvRows.push([statSection.name]);
		
		$("<table>")
			.attr("id", statSection.tableId)
			.append(
				$("<tr>").append(
					$("<th>")
						.attr("colspan", 2)
						.html(statSection.name)
				)
			)
			.append(
				$("<tr>").append(
					$("<th>")
						.addClass("title")
						.text("Page Name")
				)
				.append(
					$("<th>")
						.addClass("title")
						.text("Stats Used")
				)
			)
		.appendTo("#content-body");
		
		
		getScriptSync(statSection.path); // Get the navigation object for the stats
		var items = navigationItems;
		generateProfileStatTable(items, statSection);
	});
	
	addCSVExport();
}

function generateProfileStatTable(navigationItems, statSection) {
	$.each(navigationItems, function(i, navItem) {
		var statNames;
		
		if (navItem.profile) {
			appendProfileStats(navItem, statSection);
		}
		
		// continue recursively
		if (navItem.subItems && navItem.subItems.length) {
			generateProfileStatTable(navItem.subItems, statSection);
		}
		
	});
}

function appendProfileStats(navItem, statSection) {
	if (navItem.preConfig)
		getScriptSync(statSection.subPath + navItem.preConfig);
	
	$.getScript(statSection.subPath + navItem.config, function() {
		var statsText = "";
		
		$.each(reportOptions.availableCharts, function(i, chart) {
			//console.log(chart);
			
			var fullStatnames = [];
			
			$.each(chart.types, function(i, type) {
				var constructedNames = profStats.constructStatNames(chart, type, config.gameTypes);
				fullStatnames = fullStatnames.concat(constructedNames);
				
				$.each(constructedNames, function(c, constructedName) {
					if (!constructedName)
						return;
					
					csvRows.push([constructedName]);
				});
			});
			
			statsText += ("<i>" + chart.name + "</i> (" + chart.description + ") : <br />[" 
						+ fullStatnames.join(", ") + "]<br /><br />");
		});
		populateTableRow("#" + statSection.tableId, navItem.title + "<br />(Profile_Stat)", statsText);
	});
}

function populateTableRow(tableId, title, text) {
	$(tableId).append(
		$("<tr>")
			.append(
				$("<td>").html(title)
			)
			.append(
				$("<td>").html(text)
			)
	)
}

function addCSVExport() {
	if (enableCSVExport) {
		var csvFrameID = "manifest-frame";
	
		$("#" + csvFrameID).remove();
		
		addCSVButton(enableCSVExport, 
			csvFrameID, 
			function() {
				exportToCSV(csvColumns, csvRows, csvFrameID, "manifest.csv"); 
			});
	
		$("#" + csvFrameID).addClass("csv-right");
	}
}

