var navigationTitle = "Admin";
var navigationItems = [{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Gamertag Groups Admin",
		href: "gamertag_groups_admin.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Gamertag Group Status",
		href: "gamertag_group_status.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Gamers Status",
		href: "gamers_status.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
];