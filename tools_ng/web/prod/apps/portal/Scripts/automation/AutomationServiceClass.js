﻿/*
* Class AutomationService
*
* Requires jQuery 
*
*/

var AutomationService = function(automationServiceObj) {
    var _jobMatrixTableElementId = "jobMatrixTable";
    var _jobMatrixSummaryTableElementId = "jobMatrixSummaryTable";
    var _tableFilterBoxId = "table-filter-box";
    var _firstload = true;

    // css & class names
    //var _tooltipClass = "is-tooltip";
    var _tooltipClass = automationConfig.tooltipClass;
    var _stateTooltipClass = "state-tooltip";
    var _headerTooltipClass = "header-tooltip";
    var _emptyCellClass = "empty-cell";
    var _stateHeaderClass = "state-header-class";
    var _variationSuffix = "_variation";
    var _gradientSuffix = "_gradient";
    var _fullLabelAttr = "full-label";
    var _originalLabelKey = "originalLabel";

    var _loadDataIntervalID = null;
    var _redrawTimeoutID;
    var _redrawTimeoutDelay = 1000;

    var _serviceType = automationServiceObj.serviceType.toLowerCase();

    var _consumerJobsDict = {};
    var _jobResults = {};
    
    var _groupedJobs = [];
    var _usersInfo = {};

    // Will store all the different groups found in _groupedJobs hierarchically
    var _columnGroups = {};
    var _columnGroupKeys = [];
    var _groupHierarchy = [];

    // The Description and triggered at columns
    var _nonStateColumnsNum = 2; 

    var _tooltipSetFlag = "tooltipset";

    var _toUnderscoreSplit = (automationConfig.toUnderBarSplitServiceTypes.hasOwnProperty(_serviceType) ? true : false);
    var _titleSplit = ((automationConfig.titleSplitServiceTypes.hasOwnProperty(_serviceType) && !_toUnderscoreSplit) ? true : false);

    var _initialHeaderHeight;
    var _getTableScrollHeight = function () {
        if (typeof _initialHeaderHeight === "undefined") {
            _initialHeaderHeight = $("#" + _jobMatrixTableElementId + " thead").height();
        }
        return (
                $("#content-body").height()
                - $("#" + automationServiceObj.tableId + " .title-only").height()
                - _initialHeaderHeight
                - 30);
    };

    var _fixedColumns = {
        0: {
            width: 55,
            class: "fixed-cl-width",
            visible: true,
        },
        1: {
            width: 120,
            class: "fixed-user-width",
            visible: true,
        },
        2: {
            width: 120,
            class: "fixed-buddy-width",
            visible: true,
        },
    };

    var _columnStates = {};

    var _boxEventsOverriden = false;
    var _generatedAtId = "generatedAt";
    var _toggleVisibilityButtonId = "toggle-visibility-button";
    var _columnVisibility = {};
    
    var _utcCbID = "utc-force";
    // The timezoneOffset is the hours difference 
    // form our current timezone to UTC
    var _timezoneOffset = new Date().getTimezoneOffset() / 60; // hours offset from UTC
    var _timezoneString = "(Local Time " + ((timezoneOffset >= 0) ? "+" : "") + timezoneOffset + ")";
    var _inUTC = true;

    // Enable icons by default
    var _iconsCbID = "icons-text-state";
    var _iconsState = true;

    var _pauseUpdateId = "pause-update";

    function initJobMatrixSummary() {
        //loadJobMatrix();
        drawLoadingBox();
        buildMatrixSummaryTable();
        _loadDataIntervalID = setInterval(buildMatrixSummaryTable, automationConfig.reloadInterval);
    }

    function initJobMatrix() {
        processUsersInfo();
        drawLoadingBox();
        buildMatrixTable();
    }

    function setBuildMatrixTableInterval() {
        if (!_loadDataIntervalID) {
            _loadDataIntervalID = setInterval(buildMatrixTable, automationConfig.reloadInterval);
            //console.log("set : " + _loadDataIntervalID);
        }
    }

    function clearBuildMatrixTableInterval() {
        if (_loadDataIntervalID) {
            //console.log("clear : " + _loadDataIntervalID);
            clearInterval(_loadDataIntervalID);
            _loadDataIntervalID = null;
        }
    }

    /**
    ** Draws the loading rectangle animated box in the middle of the page
    **/
    function drawLoadingBox() {
        $("#" + automationServiceObj.tableId).empty();
        $("#" + automationServiceObj.tableId)
            .append(
                $("<div>").addClass("content-loading")
            );
    }

    /**
    ** Display the error message at the centre of the screen when the server cannot be accessed
    **/
    function drawLoadingError() {
        $("#" + automationServiceObj.tableId).empty();
        $("#" + automationServiceObj.tableId)
            .append(
                $("<div>")
                    .addClass("content-loading-error")
                    .html("Error connecting to '" + automationServiceObj.serverHost + "'"
                        + "<br/>"
                        + "Will retry in " + (automationConfig.reloadInterval / 1000) + " seconds"
                    )
            );
    }
   
    /**
    ** Fetches the json data from the server and (re)populates the table
    **/
    function buildMatrixTable() {
        // Only if the pause updates checkbox is not checked
        if (!$("#" + _pauseUpdateId).prop("checked")) {

            if (!_firstload) {
                showUpdatingMessage();
            }

            $.ajax({
                type: "GET",
                url: automationServiceObj.automationGroupedRest,
                dataType: "json",
                success: function (data) {
                    buildDataStructures(data);
                    // Create the table only the first time
                    if (_firstload) {
                        generateMatrixTable();
                        setupMatrixDatatables();
                    }
                    populateMatrixTable();
                    // Memory leak?
                    delete data;
                },
                error: function (jqxhr, textStatus, error) {
                    _firstload = true;
                    hideUpdatingMessage();
                    drawLoadingError();
                }
            });
        } // End of if (!$("#" + _pauseUpdateId).prop("checked"))
    }

    function buildMatrixSummaryTable() {
        // Only if the pause updates checkbox is not checked
        if (!$("#" + _pauseUpdateId).prop("checked")) {

            $.ajax({
                type: "GET",
                url: automationServiceObj.automationGroupedRest,
                dataType: "json",
                success: function (data) {
                    buildDataStructures(data);
                    generateSummaryMatrixTable();
                    // Memory leak?
                    delete data;
                },
                error: function (jqxhr, textStatus, error) {
                    drawLoadingError();
                }
            });
        } // End of if (!$("#" + _pauseUpdateId).prop("checked"))
    }

    /**
    ** Builds and populates the variable data structures used to generate the report
    **/
    function buildDataStructures(data) {
        // Save the consumer jobs dictionary and remove it from the list
        _consumerJobsDict = data.ConsumerJobs;
        _jobResults = data.JobResults;
        _columnGroups = data.ColumnGroups;
        if (_firstload) {
            _columnGroupKeys = Object.keys(_columnGroups)
                .sort(function (a, b) {
                    return (extractFileName(a.split(automationConfig.groupKeySeparator)[0]).toLowerCase()
                            > extractFileName(b.split(automationConfig.groupKeySeparator)[0]).toLowerCase()
                        ? 1 : -1);
                })
                .sort(); // final sort based on the whole string

            if (_columnGroupKeys.length > 0) {
                _groupHierarchy = _columnGroupKeys[0].split(automationConfig.groupKeySeparator);
            }
        }       
        _groupedJobs = data.TriggerGroupedJobs;
    }

    /**
    ** Shows all or only the selected columns based on whether the "show all" button is pressed
    **/
    function toggleVisibility() {
        var button = $("#" + _toggleVisibilityButtonId);
        if (button.hasClass("show")) {
            showAllColumns();
            button.removeClass("show").addClass("hide");
        }
        else if (button.hasClass("hide")) {
            showStoredColumns();
            button.removeClass("hide").addClass("show");
        }
    }
    /**
    ** Hides the unselected columns
    **/
    function hideDisabled() {
        var button = $("#" + _toggleVisibilityButtonId);
        if (button.hasClass("hide")) {
            showStoredColumns();
            button.removeClass("hide").addClass("show");
        }
    }

    /**
    ** Generates the matrix table
    **/
    function generateMatrixTable() {
        $("#" + automationServiceObj.tableId).empty();

        $("<table>")
            .addClass("title-only")
            .append(
                $("<tr>")
                    .append(
                        $("<th>")
                            .addClass("title")
                            .append(
                                $("<span>")
                                    .attr("id", "colvis-button")
                            )
                            .append(
                                $("<span>")
                                    .attr("id", "toggle-button")
                                    .append(
                                        $("<button>")
                                            .attr("id", _toggleVisibilityButtonId)
                                            .addClass("table-title-button")
                                            .addClass("show") // show by default
                                            .click(toggleVisibility)
                                            .attr("title", "Show All Columns")
                                            .text("Show All")
                                    )
                            )
                            .append(
                                $("<span>")
                                    .html(
                                        "Generated At: <span id=" + _generatedAtId + "></span>"
                                        + " - "
                                        + "Reload INV: " + (automationConfig.reloadInterval / 1000) + " secs"
                                    )
                            )
                            .append(
                                $("<span>")
                                    .addClass("right")
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<input>")
                                                    .attr("type", "checkbox")
                                                    .attr("id", _pauseUpdateId)
                                                    .attr("checked", false)
                                            )
                                            .append(
                                                $("<label>")
                                                    .attr("for", _pauseUpdateId)
                                                    .attr("title", "Check to Pause Updates Interval")
                                                    .text("Pause Updates // ")
                                            )
                                    )
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<input>")
                                                    .attr("type", "checkbox")
                                                    .attr("id", _iconsCbID)
                                                    .attr("checked", _iconsState)
                                            )
                                            .append(
                                                $("<label>")
                                                    .attr("for", _iconsCbID)
                                                    .attr("title", "Toggle State Icons / Full Text")
                                                    .text("State Icons //")
                                            )
                                    )
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<input>")
                                                    .attr("type", "checkbox")
                                                    .attr("id", _utcCbID)
                                                    .attr("checked", _inUTC)
                                            )
                                            .append(
                                                $("<label>")
                                                    .attr("for", _utcCbID)
                                                    .attr("title", "Display Dates In UTC " + _timezoneString)
                                                    .text("UTC " + _timezoneString)
                                            )
                                    )
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<label>")
                                                    .text(" // Filter Table:")
                                            )
                                            .append(
                                                $("<input>")
                                                    .attr("type", "text")
                                                    .attr("id", _tableFilterBoxId)
                                            )
                                    )
                            )
                    )
            )
        .appendTo($("#" + automationServiceObj.tableId));

        var thead = $("<thead>");
        var trhead = $("<tr>");

        var subTrObjectsArray = [];
        _groupHierarchy.map(function (d, i) { subTrObjectsArray[i] = [] }); // Initialise an array for each hierarchical header

        $.each(_columnGroupKeys, function (i, groupKey) {
            var subGroupArray = groupKey.split(automationConfig.groupKeySeparator);
            var titleArray = [];

            $.each(subGroupArray, function (j, subGroupKey) {
                var formattedText = (_titleSplit) ? splitWord(extractFileName(subGroupKey)) : mapText(subGroupKey);
                if (_toUnderscoreSplit) {
                    // Add the unsplit text first to the title and then split it
                    titleArray.push(formattedText);
                    formattedText = splitToUnderscore(formattedText);
                }
                else {
                    titleArray.push(formattedText);
                }
                
                var title = "";
                var fullLabel = "";
                if (j == 0) {
                    fullLabel = subGroupKey;
                    title = wrapTitleInHtmlTable(subGroupKey) + _columnGroups[groupKey]; // The Current key + extra description
                }
                else {
                    fullLabel = titleArray.join(automationConfig.groupKeySeparator); //The column hierarchy path so far
                    title = wrapTitleInHtmlTable(fullLabel);
                }

                if ((subTrObjectsArray[j].length > 0) && (subTrObjectsArray[j][subTrObjectsArray[j].length - 1].title == title)) {
                    subTrObjectsArray[j][subTrObjectsArray[j].length - 1].colspan++;
                }
                else {
                    subTrObjectsArray[j].push({
                        colspan: 1,
                        title: title,
                        fullLabel: fullLabel,
                        originalLabel: groupKey,
                        text: formattedText,
                    });
                }
            });
        });

        var colspan = _columnGroupKeys.length;
        var rowSpan = 1 + _groupHierarchy.length;
        trhead
            .append(
                $("<th>")
                    .attr("title", "Changelist")
                    //.addClass("fixed-cl-width")
                    .addClass(_fixedColumns[0].class)
                    .attr("rowspan", rowSpan)
                    .text("Change list")
            )
            .append(
                $("<th>")
                    .attr("title", "User")
                    //.addClass("fixed-user-width")
                    .addClass(_fixedColumns[1].class)
                    .attr("rowspan", rowSpan)
                    .text("User")
            )
            .append(
                $("<th>")
                    .attr("title", "Buddy")
                    //.addClass("fixed-buddy-width")
                    .addClass(_fixedColumns[2].class)
                    .attr("rowspan", rowSpan)
                    .text("Buddy")
            )
            .append(
                $("<th>")
                    .attr("title", "Status")
                    .attr("rowspan", 1)
                    .attr("colspan", colspan)
                    .text("Status")
            )
            .append(
                $("<th>")
                    .attr("rowspan", rowSpan)
                    .attr("title", "Description")
                    .addClass("fixed-dp-width")
                    .text("Description")
            )
            .append(
                $("<th>")
                    .attr("rowspan", rowSpan)
                    .attr("title", "Triggered At")
                    .addClass("fixed-time-width")
                    .text("Triggered  At")
            );


        thead.append(trhead);
        $.each(subTrObjectsArray, function (i, subTrObjects) {
            var tr = $("<tr>");
            $.each(subTrObjects, function (j, subTrObject) {
                tr.append(
                    $("<th>")
                        .attr("colspan", subTrObject.colspan)
                        .addClass("lowercase")
                        .addClass((_toUnderscoreSplit) ? "small-font" : "")
                        .addClass((i == subTrObjectsArray.length - 1) ? _stateHeaderClass : "")
                        .attr("title", subTrObject.title)
                        .attr(_fullLabelAttr, subTrObject.fullLabel)
                        .data(_originalLabelKey, subTrObject.originalLabel)
                        .html(subTrObject.text)
                        .addClass(_tooltipClass)
                        .tooltip({
                            tooltipClass: "summary-tooltip",
                            position: { my: "middle bottom", at: "middle top" },
                            content: function () {
                                return $(this).attr("title");
                            },
                            show: "slow",
                            /*
                            show: {
                                //  effect: "fadeIn",
                                delay: automationConfig.tooltipDelay/3,
                            },
                            */
                            close: function (event, ui) {
                                var me = this;
                                ui.tooltip.hover(
                                    function () {
                                        $(this).stop(true).show();
                                    },
                                    function () {
                                        $(this).fadeOut("fast", function () {
                                            $(this).remove();
                                            $(".ui-tooltip").remove();
                                        });
                                        $(".ui-tooltip").hide();
                                    }
                                );
                                $(".ui-tooltip").hide();
                            }
                        })
                        .data({
                            _tooltipSetFlag: true,
                            _originalLabelKey: subTrObject.originalLabel
                        })
                    );
            });
            thead.append(tr);
        });

        $("#" + automationServiceObj.tableId).append(
            $("<table>")
                .attr("id", _jobMatrixTableElementId)
                .append(thead)
                .append($("<tbody>"))
        );    // </table>

        // Mouse Over and Scroll enable/disable reloading Events
        $("#" + _jobMatrixTableElementId)
            .mouseover(clearBuildMatrixTableInterval)
            .mouseleave(setBuildMatrixTableInterval);
    }

    function generateSummaryMatrixTable() {
        $("#" + automationServiceObj.tableId).empty();

        $("<table>")
            .addClass("title-only")
            .append(
                $("<tr>")
                    .append(
                        $("<th>")
                            .addClass("title")
                            .append(
                                $("<span>")
                                    .html(
                                        "Generated At: <span id=" + _generatedAtId + "></span>"
                                        + " - "
                                        + "Reload Interval: " + (automationConfig.reloadInterval / 1000) + " secs"
                                    )
                            )
                            .append(
                                $("<span>")
                                    .addClass("right")
                                    .append(
                                        $("<span>")
                                            .append(
                                                $("<input>")
                                                    .attr("type", "checkbox")
                                                    .attr("id", _pauseUpdateId)
                                                    .attr("checked", false)
                                            )
                                            .append(
                                                $("<label>")
                                                    .attr("for", _pauseUpdateId)
                                                    .attr("title", "Check to Pause Updates Interval")
                                                    .html("Pause Updates &nbsp;&nbsp;")
                                            )
                                    )
                            )
                    )
            )
        .appendTo($("#" + automationServiceObj.tableId));

        var summaryTable = $("<table>").attr("id", _jobMatrixSummaryTableElementId);
        summaryTable
            .append(
                $("<tr>")
                    .append(
                        $("<th>").text("Group")
                    )
                    .append(
                        $("<th>").text("Last Processed Changelist")
                    )
                    .append(
                        $("<th>").text("pending Jobs")
                    )
                    .append(
                        $("<th>").text("Status")
                    )
            );

        // Reset the latest column states dict
        _columnStates = {};

        // Build the column/group summaries
        $.each(_groupedJobs, function (groupIndex, groupedJobDict) {
            $.each(_columnGroupKeys, function (i, groupKey) {
                var job = _groupedJobs[groupIndex][groupKey];
                updateColumnStates(groupKey, job);
            });

        });

        $.each(_columnGroupKeys, function (i, groupKey) {
            var statusTd = $("<td>").addClass("centre");
            updateMatrixSummaryCellStatus(statusTd, _columnStates[groupKey]);

            $("<tr>")
                .append(
                    $("<td>").html(groupKey)
                )
                .append(
                    $("<td>")
                        .addClass("centre")
                        .html(getChangelistLink(_columnStates[groupKey].lastChangelist))
                )
                .append(
                    $("<td>")
                        .addClass("centre")
                        .html(_columnStates[groupKey].pendingJobs)
                )
                .append(statusTd)
                .appendTo(summaryTable);
        });
        summaryTable.appendTo($("#" + automationServiceObj.tableId));
        
        updateGenereatedAtDateTime();
    }

    function setupMatrixDatatables() {
        /**
        ** Extension to hide the empy lines / replace Hide
        **/
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
                var visibleColumns = oSettings.aoColumns.filter(
                    function (d, i) {
                        return (d.bVisible && !_fixedColumns.hasOwnProperty(i))
                    }).length
                    - _nonStateColumnsNum;

                var tr = oSettings.aoData[iDataIndex].nTr;
                // test for property on nTr
                var emptyCells = $(tr).find("." + _emptyCellClass).size();

                if ((emptyCells > 0) && (emptyCells == visibleColumns)) {
                    return false;
                }

                return true;
            }
        );

        retrieveColumnVisibility();
        var oTable = $("#" + _jobMatrixTableElementId).DataTable({
            "scrollX": "100%",
            //"sScrollXInner": "100%",
            "scrollY": _getTableScrollHeight(),
            "scrollCollapse": true,
            "ordering": false,             // Disable Sorting
            //"pageLength": 100,
            "paging": false,         // Disable Pagination
            "searching": true,           // Disable Filter
            "info": true,
            //"dom": 'C<"clear">lrtip',
            "dom": 'Crtip',
            "autoWidth": true,
            "colVis": {
                "buttonText": "Visible Columns", // Change the text for the ColVis button
                "label": function (index, label, th) {
                    var fullLabel = $(th).attr(_fullLabelAttr);
                    return (fullLabel) ? fullLabel : label;
                },
            },
            "initComplete": function (settings, json) {
                $("div.ColVis button")
                    .attr("title", "Visible Columns")
                    .addClass("table-title-button")
                    .click(hideDisabled) // Show only the visible ones in case the 'show all' button is clicked
                    .click(overrideVisibilityBoxEvents)
                    .appendTo("#colvis-button");
            },
        });

        // Add search functionality to the 
        $("#" + _tableFilterBoxId).keyup(function () {
            oTable.search($(this).val());
            customTableRedraw();
        });

        // Fix the first three columns
        var fc = new $.fn.DataTable.FixedColumns(oTable, {
            "leftColumns": Object.keys(_fixedColumns).length,
            "heightMatch": "none",
        });
        /* DEPRECATED
        var fc = new FixedColumns(oTable, {
            "iLeftColumns": Object.keys(_fixedColumns).length,
            "iLeftWidth": getFixedColumnsPanelWidth(),
            //"sHeightMatch": "semi-auto",
            "sHeightMatch": "none",
        });
        */

        showStoredColumns();

        $("#" + _utcCbID).change(function () {
            _inUTC = $(this).prop("checked");
            populateMatrixTable();
        });

        $("#" + _iconsCbID).change(function () {
            _iconsState = $(this).prop("checked");
            populateMatrixTable();
        });

        $(window).resize(function () {
            customTableRedraw();
        });

    }
      
    /**
    ** Populates the previously generated table, called periodically
    **/
    function populateMatrixTable() {
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();

        clearDataTable();
        // Reset the latest column states dict
        _columnStates = {};

        $.each(_groupedJobs, function (groupIndex, groupedJobDict) {
            // Get a random job from the changelist dict for populating the other columns 
            // (they should all have the same Trigger object)
            var randomJob = getRandomDictValue(groupedJobDict);
            var rowClass = (randomJob.Trigger.TriggerId)
                            ? randomJob.Trigger.TriggerId
                            : unixTimeToMsTicks(parseJsonDate(randomJob.Trigger.TriggeredAt).getTime()).toString();
            var tr = $("<tr>");
            tr.addClass(rowClass);
            tr.data({ rowId: rowClass });

            var row = [];

            randomJob.Trigger.Description = (randomJob.Trigger.Changelist)
                                            ? randomJob.Trigger.Description
                                            : randomJob.Trigger.FormattedCommand;

            randomJob.Trigger.Files = (randomJob.Trigger.Files)
                                            ? randomJob.Trigger.Files
                                            : [];

            // Changelist Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[0].class)
                    .addClass("centre")
                    .attr(
                        "title",
                        (
                            (randomJob.Trigger.Description) 
                                ? "Description: \n" + randomJob.Trigger.Description + "\n\n"
                                : ""
                        )
                        + (
                            (randomJob.Trigger.Files)
                                ? "Processed Files: \n" + randomJob.Trigger.Files.join("\n")
                                : ""
                        )
                    )
                    .html(getChangelistLink(randomJob.Trigger.Changelist))
            )

            // User Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[1].class)
                    .attr("title", randomJob.Trigger.Username)
                    .text(truncate(randomJob.Trigger.Username, automationConfig.truncateChars))
                    .addClass(_tooltipClass)
            );

            // Buddy Column
            tr.append(
                $("<td>")
                    .addClass(_fixedColumns[2].class)
                    .attr("title", extractBuddyName(randomJob.Trigger.Description))
                    .text(truncate(extractBuddyName(randomJob.Trigger.Description), automationConfig.truncateChars))
            );

            $.each(_columnGroupKeys, function (i, groupKey) {

                var subGroupArray = groupKey.split(automationConfig.groupKeySeparator).sort();
                $.each(subGroupArray, function (j, subGroupKey) {
                    if (j == (subGroupArray.length - 1)) {

                        var job = _groupedJobs[groupIndex][groupKey];
                        var state = (job) ? job.State : "";
                        var jobId = (job) ? job.ID : "";

                        var consumedById;
                        if (state === automationConfig.consumeState) {
                            consumedById = job.ConsumedByJobID;

                            if (_consumerJobsDict.hasOwnProperty(consumedById)) {
                                var consumeJob = _consumerJobsDict[consumedById];
                                //state = _jobStateEnum[consumeJob.State];
                                state = consumeJob.State;

                                // For colouring the next line
                                _groupedJobs[groupIndex][groupKey].State = consumeJob.State;
                            }
                        }

                        var colourClass = (state) ? state.toLowerCase() : _emptyCellClass;
                        updateColumnStates(groupKey, job);

                        if ((groupIndex > 0) && _groupedJobs[groupIndex - 1].hasOwnProperty(groupKey)) {
                            //var higherState = _jobStateEnum[_groupedJobs[groupKeysOrderedDesc[groupIndex - 1]][groupKey].State];
                            var higherState = _groupedJobs[groupIndex - 1][groupKey].State;

                            // If the state is the same but the consumed by job is different or undefined, swap the colour shades
                            if (
                                (state == higherState) &&
                                (
                                    (consumedById != _groupedJobs[groupIndex - 1][groupKey].ConsumedByJobID)
                                    || (typeof consumedById === "undefined")
                                )
                            ) {
                                if (!_groupedJobs[groupIndex - 1][groupKey].hasOwnProperty("Variation")) {
                                    colourClass = state.toLowerCase() + _variationSuffix;
                                    _groupedJobs[groupIndex][groupKey]["Variation"] = true; // Set the flag for next row's class
                                }
                            }
                                // If the state is the same and the consumed by job is the same, keep the same shade as above
                            else if (
                                (state == higherState) &&
                                (consumedById == _groupedJobs[groupIndex - 1][groupKey].ConsumedByJobID)
                                ) {
                                if (_groupedJobs[groupIndex - 1][groupKey].hasOwnProperty("Variation")) {
                                    colourClass = state.toLowerCase() + _variationSuffix;
                                    _groupedJobs[groupIndex][groupKey]["Variation"] = true; // Set the flag for next row's class
                                }
                            }
                        }

                        // State Column
                        var td = $("<td>").addClass(colourClass);

                        if (state) {
                            td.addClass(_tooltipClass) // will add a tooltip
                               .addClass(_stateTooltipClass) // the tooltip will be based on the cell job state - colourclass
                               .data({ state: state, job: minimiseJob(job)})
                               .addClass("centre")
                               .addClass((consumeJob) ? consumeJob.ID : "")
                               .append(
                                   (!(state) || automationConfig.nonUlogStates.hasOwnProperty(state))
                                    ? $("<span>")
                                        .append(
                                            $("<span>")
                                                .addClass("hidden")
                                                .text(jobId + " " + consumedById)
                                        )
                                        .append(
                                            $("<span>")
                                                .html((_iconsState) ? mapTextToIcon(state) : mapText(state))
                                        )
                                    : $("<span>").append(
                                            $("<span>")
                                                .addClass("hidden")
                                                .text(jobId + " " + consumedById)
                                        )
                                        .append(
                                            $("<a>")
                                                .attr("href", automationServiceObj.ulogHost + "?"
                                                    + $.param({
                                                        "ServerHost": automationServiceObj.serverHost,
                                                        "JobId": (((consumedById) ? consumedById : jobId)),
                                                        "ErrorsOnly": ((state == automationConfig.errorStateName) ? true : false),
                                                    })
                                                )
                                                .attr("target", "_blank")
                                                .attr("title", "Click to view the Universal Log")
                                                .html((_iconsState) ? mapTextToIcon(state) : mapText(state))
                                        )
                                )
                                .click(function (event) {
                                    $(".highlighted-td").removeClass("highlighted-td");
                                    if (consumeJob) {
                                        $("." + consumeJob.ID).toggleClass("highlighted-td")
                                    }
                                });
                        }



                        tr.append(td);
                    }

                });
            });

            if (randomJob.Trigger.TriggeredAt === undefined) {
                randomJob.Trigger.TriggeredAt = randomJob.Trigger.SubmittedAt;
            }
            // Description Column
            tr.append(
                $("<td>")
                    .addClass("fixed-dp-width")
                    .attr("title", randomJob.Trigger.Description)
                    .text(truncate(randomJob.Trigger.Description, automationConfig.truncateDescChars))     
            );

            // Triggered at Column
            tr.append(
                $("<td>")
                    .addClass("fixed-time-width")
                    .addClass("monospace")
                    .attr("title", toCustomISO(parseJsonDate(randomJob.Trigger.TriggeredAt), _inUTC)
                                    + "\nTime Ticks: " + unixTimeToMsTicks(parseJsonDate(randomJob.Trigger.TriggeredAt).getTime())
                                    + "\nTrigger Id: " + randomJob.Trigger.TriggerId
                    )
                    .text(toCustomISO(parseJsonDate(randomJob.Trigger.TriggeredAt), _inUTC))
            );

            oTable.row.add(tr[0]);
            delete tr;
        });

        if (_firstload) {
            _firstload = false;
        }

        $(".ui-tooltip").hide();

        clearBuildMatrixTableInterval();
        setBuildMatrixTableInterval();

        customTableRedraw();
        updateStateHeaders();

        // Update the generated at info
        updateGenereatedAtDateTime();

        hideUpdatingMessage();
    }

    function updateColumnStates(groupKey, job) {
        var state = (job) ? job.State : "";
        var changelist = (job) ? job.Trigger.Changelist : "";

        // This is only for the Matrix Summary View
        if (state === automationConfig.consumeState) {
            if (_consumerJobsDict.hasOwnProperty(job.ConsumedByJobID)) {
                state = _consumerJobsDict[job.ConsumedByJobID].State;
            }
        }

        if (state != undefined) {
            if (!_columnStates.hasOwnProperty(groupKey)) {
                _columnStates[groupKey] = {};
                _columnStates[groupKey]["pendingJobs"] = 0;
                _columnStates[groupKey]["lastChangelist"] = "";
                _columnStates[groupKey]["integrations"] = {};
            }

            // Skip the Skipped states
            if (state == automationConfig.skippedStateName) {
            }
            else if (state == automationConfig.pendingStateName) {
                // count the pending jobs
                _columnStates[groupKey]["pendingJobs"]++;
            }
            else {
                if (!_columnStates[groupKey]["currentState"]) {
                    _columnStates[groupKey]["currentState"] = state;
                }
                else if (!_columnStates[groupKey]["previousState"]) {
                    if ((_columnStates[groupKey]["currentState"] == automationConfig.assignedStateName)
                            && (state == automationConfig.assignedStateName)) {
                        // Ignore the adjacent assigned states
                    }
                    else {
                        _columnStates[groupKey]["previousState"] = state;
                    }
                }

                if (!_columnStates[groupKey]["lastChangelist"]) {
                    if (state != automationConfig.assignedStateName) {
                        _columnStates[groupKey]["lastChangelist"] = changelist;
                    }
                }

            }

            // Check if there are integration regex
            var jobIntegrationP4Regex = getIntegrationP4Regex(job);
            $.each(jobIntegrationP4Regex, function (key, value) {
                // Using key+value for dict key as there might be entries with same key - diff value
                // Will take the substring afterward by removing the value from the key
                if (!_columnStates[groupKey]["integrations"].hasOwnProperty(key + value)) {
                    _columnStates[groupKey]["integrations"][key + value] = value;
                }
            });
        }
    }

    function updateGenereatedAtDateTime() {
        $("#" + _generatedAtId)
            .switchClass("", "flash-red", automationConfig.animationDuration)
            .html(toCustomISO(new Date(), _inUTC));
        $("#" + _generatedAtId).switchClass("flash-red", "", automationConfig.animationDuration);
    }

    /**
    ** Adds jquery tooltips if they haven't been added yet
    **/
    function addTooltips() {
        
        $("#" + automationServiceObj.tableId).find("." + _tooltipClass).each(function (i, td) {

            if (!$(td).data(_tooltipSetFlag)) {

                if ($(td).hasClass(_fixedColumns[1].class)) {
                    $(td).tooltip({
                        tooltipClass: "user-tooltip",
                        position: { my: "left middle", at: "right middle" },
                        content: function () {
                            return createUserInfoDiv($(this).attr("title"));
                        },
                        show: "fast",
                        hide: "fast",
                        //open: function (event, ui) {
                        //   debugger;
                        //}
                    })
                    .data(_tooltipSetFlag, true);
                }
                else if ($(td).hasClass(_stateTooltipClass) && !($(td).hasClass(_emptyCellClass))) {
                    var data = $(td).data();
                    var state = data.state;
                    var job = data.job;

                    var hasErrors = automationConfig.errorUlogStates.hasOwnProperty(state);

                    $(td).tooltip({
                        items: "td",
                        tooltipClass: "job-tooltip",
                        position: { my: "left middle", at: "right middle" },
                        content: function () {
                            if (hasErrors) {
                                return createSpinnerDiv(
                                    createCompactJobInfoDiv(job)
                                );
                            }
                            else {
                                return createCompactJobInfoDiv(job);
                            }
                        },
                        show: {
                            effect: "fadeIn",
                            delay: automationConfig.tooltipDelay,
                        },
                        open: function (event, ui) {
                            if (hasErrors) {
                                $el = $(this),
                                pos = $el.tooltip("option", "position");
                                pos.of = $el;

                                $(td).data("loading", true);
                                setTimeout(function () {
                                    // console.log(td.data("loading"));
                                    // Don't do the ajax request if the mouse has been moved over in the meantime
                                    if (!$(td).data("loading")) {
                                        ui.tooltip.html(ui.tooltip.find("div.job-info").html());
                                        return;
                                    }

                                    $.get(automationServiceObj.ulogErrorsRest,
                                        {
                                            "ServerHost": automationServiceObj.serverHost,
                                            "JobId": (((job.ConsumedByJobID) ? job.ConsumedByJobID : job.ID)),
                                        },
                                        function (data) {
                                            // get the current tooltip html info
                                            var jobDiv = ui.tooltip.find("div.job-info");
                                            ui.tooltip.html(
                                                jobDiv.html() +
                                                createErrorsListDiv(data)
                                            );
                                            ui.tooltip.position(pos);
                                        }
                                    )
                                }, automationConfig.tooltipDelay + automationConfig.tooltipAjaxDelay);
                            }

                            clearBuildMatrixTableInterval();
                        },
                        close: function (event, ui) {
                            // Cancel the ajax query if it hasn't happened yet
                            $(td).data("loading", false);

                            var me = this;
                            ui.tooltip.hover(
                                function () {
                                    $(this).stop(true).show();
                                    clearBuildMatrixTableInterval();
                                },
                                function () {
                                    $(this).fadeOut("fast", function () {
                                        $(this).remove();
                                        $(".ui-tooltip").remove();
                                    });
                                    $(".ui-tooltip").hide();
                                    setBuildMatrixTableInterval();
                                }
                            );
                            $(".ui-tooltip").hide();
                            setBuildMatrixTableInterval();
                        },
                    })
                    .data(_tooltipSetFlag, true);
                } // End of else _stateTooltipClass

            }  // End of ! _tooltipSetFlag

        }); // End of each _tooltipClass

    }

    function removeAllTooltips() {
        $("#" + automationServiceObj.tableId).find("." + _tooltipClass).each(function (i, td) {
            if ($(td).data(_tooltipSetFlag)) {
                $(td).tooltip("destroy").off().removeData().remove();
            }
        });
    }

    function clearDataTable() {
        removeAllTooltips();
        $(".ui-tooltip").remove();
        $(".ui-helper-hidden-accessible").remove();

        if ($("#" + _jobMatrixTableElementId).size() > 0) {
            var oTable = $("#" + _jobMatrixTableElementId).DataTable();
            if (oTable) {
                // Clean Table events and data
                $("#" + _jobMatrixTableElementId).find("td").each(function (j, td) {
                    $(td).off().removeData().remove();
                });
                oTable.clear();
            }
        }
    }

    /**
    ** DEPRECATED
    **/
    function hideEmptyRows() {
        var oTable = $("#" + _jobMatrixTableElementId).dataTable();
        // Calc the number of hidden columns of the table -2 for the non statecolumns / the fixed are always hidden anyway
        var visibleColumns = oTable.fnSettings().aoColumns.filter(function (d) { return d.bVisible }).length
                            - Object.keys(_fixedColumns).length
                            - _nonStateColumnsNum;

        $("#" + _jobMatrixTableElementId).find("tbody tr").each(function (i, row) {   
            var rowClass = $(row).data("rowId");
            $("." + rowClass).removeClass("hidden");

            if ($(row).find("." + _emptyCellClass).size() == visibleColumns) {
                $("." + rowClass).addClass("hidden");
            }
        });
        
        oTable.fnSettings().oScroll.sY = _getTableScrollHeight();
    }

    function updateStateHeaders() {
        var classSuffixRE = new RegExp(_gradientSuffix + "$", "ig");

        $("." + _stateHeaderClass).each(function (i, th) {
            // Remove the current gradient class if exists
            $(th).removeClass(function (index, classNames) {
                $.each(classNames.split(' '), function (index2, className) {
                    className = className.trim();
                    if (className.match(classSuffixRE)) {
                        $(th).removeClass(className);
                    }
                });
            });

            if (_columnStates.hasOwnProperty($(th).data(_originalLabelKey))) {
                var currentState = _columnStates[$(th).data(_originalLabelKey)].currentState;
                var previousState = _columnStates[$(th).data(_originalLabelKey)].previousState;
                var pendingJobs = _columnStates[$(th).data(_originalLabelKey)].pendingJobs;
                var lastChangelist = _columnStates[$(th).data(_originalLabelKey)].lastChangelist;
                var integrations = _columnStates[$(th).data(_originalLabelKey)].integrations;

                var colourClass;
                if ((currentState == automationConfig.assignedStateName) &&
                        (previousState == automationConfig.errorStateName)) {
                    colourClass = automationConfig.reassignedStateName.toLowerCase() + _gradientSuffix;
                } 
                else {
                    if (currentState) {
                        colourClass = currentState.toLowerCase() + _gradientSuffix;
                    }
                }
                $(th).addClass(colourClass);
                var previousColourClass = (previousState) ? (previousState.toLowerCase() + _gradientSuffix) : "";

                var originalTitle = $(th).attr("title").split("<table class='summary-table monospace'>")[0]; // Single quotes are important
                var originalText = $(th).html().split('<span class="integrationP4Regex">')[0];

                var integationsText = (Object.keys(integrations).length > 0)
                                    ? "<span class='integrationP4Regex'> (* x " + Object.keys(integrations).length + ")</span>"
                                    : "";

                var summaryTitle = originalTitle
                                + "<table class='summary-table monospace'>"
                                    + "<tr><th colspan='2' class='title'>Summary</th></tr>"
                                    + "<tr>"
                                        + "<td>Current State: </td>"
                                        + "<td class='" + colourClass + "'>" + mapText(currentState) + "</td>"
                                    + "</tr>"
                                + ((previousState && (previousState != currentState))
                                    ? ("<tr><td>Previous State: </td>"
                                        + "<td class='" + previousColourClass + "'>" + mapText(previousState) + "</td></tr>")
                                    : "")
                                + ((pendingJobs)
                                    ? ("<tr><td>Pending Jobs: </td><td>" + pendingJobs + "</td></tr>")
                                    : "")
                                + ((lastChangelist)
                                    ? ("<tr><td>Last Processed Changelist: </td><td>" + getChangelistLink(lastChangelist) + "</td></tr>")
                                    : "")
                                + ((Object.keys(integrations).length > 0)
                                    ? ("<tr><td>Integration P4Regex: </td><td>"
                                        + createIntegrationP4RegexText(integrations) + "</td></tr>")
                                    : "")
                                + "</table>";
                var summaryText = originalText + integationsText;

                $(th).attr("title", summaryTitle);
                $(th).html(summaryText);
            }
        });
    }

    function updateMatrixSummaryCellStatus(td, groupSummary) {
        var currentState = groupSummary.currentState;
        var previousState = groupSummary.previousState;
        var pendingJobs = groupSummary.pendingJobs;
        var lastChangelist = groupSummary.lastChangelist;
        //var integrations = groupSummary.integrations;

        var colourClass;
        var cellText;
        if (
            (currentState == automationConfig.errorStateName)
            || ((currentState == automationConfig.assignedStateName) && (previousState == automationConfig.errorStateName)) 
            ) {
            //colourClass = automationConfig.reassignedStateName.toLowerCase() + _gradientSuffix;
            colourClass = automationConfig.errorStateName.toLowerCase();
            cellText = "Blocked";
        }
        else if (((currentState == automationConfig.assignedStateName) && (previousState == automationConfig.completedStateName))
            || (currentState == automationConfig.completedStateName) && (pendingJobs > 0)
            ) {
            colourClass = automationConfig.completedStateName.toLowerCase();
            cellText = "Processing";
        }
        else if ((currentState == automationConfig.completedStateName) && (pendingJobs == 0)) {
            colourClass = currentState.toLowerCase();
            cellText = "Processed To Head Revision";
        }

        $(td).addClass(colourClass);
        $(td).text(cellText);
    }


    /**
    ** DEPRECATED - Checks whether a column index belongs to the fixed ones or not
    
    function isFixedColumn(index) {
        return _fixedColumns.hasOwnProperty(index) ? true : false;
    }
    **/

    /**
    ** DEPRECATED - Calculates the widht of the fixed columns panel based on the visible/selected ones
    
    function getFixedColumnsPanelWidth(showAll) {
        var w = 0;
        $.each(_fixedColumns, function (i, fCol) {
            if (showAll) { // Count all the widths if the showAll flag is set
                w += fCol.width;
            }
            else if (fCol.visible) { // Count only the visible columns when the showAll flag is not set
                w += fCol.width;
            }
        });
        return w;
    }
    **/
    
    /**
    ** DEPRECATED - Hides all the fixed columns that are not selected
    
    function hideDisabledFixedColumns() {
        hideFixedColumns(Object.keys(_fixedColumns).filter(function (key) { return !(_fixedColumns[key].visible); }));
    }
    **/

    /**
    ** DEPRECATED - Hides the specified fixed columns
    
    function hideFixedColumns(indices) {
        $.each(indices, function (i, index) {
            if (_fixedColumns.hasOwnProperty(index)) {
                //var fCol = _fixedColumns[index];
                _fixedColumns[index].visible = false;
                $("." + _fixedColumns[index].class).addClass("hidden");
            }
        });

        updateFixedColumnsPanel();
    }
    **/

    /**
    ** DEPRECATED - Shows the specified fixed columns
    
    function showFixedColumns(indices) {
        $.each(indices, function (i, index) {
            if (_fixedColumns.hasOwnProperty(index)) {
                _fixedColumns[index].visible = true;
                $("." + _fixedColumns[index].class).removeClass("hidden");
            }
        });

        updateFixedColumnsPanel();
    }
    **/

    /**
    ** DEPRECATED - Updates the fixed columns panel 
    
    function updateFixedColumnsPanel() {
        var leftPanelWidth = getFixedColumnsPanelWidth();
        var contentWidth = $("#content").width();

        $(".DTFC_LeftWrapper").width(leftPanelWidth);
        $(".dataTables_scroll").css({
            "left": leftPanelWidth,
            "width": contentWidth - leftPanelWidth,
        });
    }
    **/

    /**
    ** Overrides the events of ColVis with the custom ones
    **/
    function overrideVisibilityBoxEvents() {
        // Return it hasn't been initialised yet
        if ($("ul.ColVis_collection > li").size() == 0) {
            return;
        }

        // Loop through all the li elements
        $("ul.ColVis_collection > li").each(function (i, li) {
            var cb = $(li).find("input[type='checkbox']");

            /** DEPRECATED
            // If it's a fixed one restore the state, fnDraw resets it from the original table's visibility
            if (isFixedColumn(i)) {
                fCol = _fixedColumns[i];
                cb.prop("checked", (fCol.visible) ? true : false);
            }
            **/

            // If it's the first time here, bind the event
            if (!_boxEventsOverriden) {
                //Disable all events
                $(li).off();
                $(cb).off();
                $(cb).on("change", function (e) {
                    clearTimeout(_redrawTimeoutID);
                    updateColumnVisibility(i, cb.is(":checked"));
                });
            }
        });

        _boxEventsOverriden = true;
    }

    /**
    ** Shows all the available columns
    **/
    function showAllColumns() {
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();

        /* DEPRECATED
        var columns = oTable.fnSettings().aoColumns;

        $.each(columns, function (i, column) {
            if (isFixedColumn(i)) {
                showFixedColumns([i]);
            }
            else {
                oTable.fnSetColumnVis(i, true, false);
            }
            oTable.fnSetColumnVis(i, true, false);
        });
        */
        oTable.columns().visible(true);

        customTableRedraw();
        updateStateHeaders();
    }
    /**
    ** Shows only the currently selected columns
    **/
    function showStoredColumns() {
        
        /* DEPRECATED
        var oTable = $("#" + _jobMatrixTableElementId).dataTable();

        var columns = oTable.fnSettings().aoColumns;
        $.each(columns, function (index, column) {

            // Use the full label key if exists, otherwise use the title
            var fullLabelKey = $(column.nTh).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : column.nTh.title);

            if (_columnVisibility.hasOwnProperty(columnStoreKey)) {
                var storedVisibility = _columnVisibility[columnStoreKey];

                
                if (isFixedColumn(index)) {
                    if (storedVisibility) {
                        showFixedColumns([index]);
                    }
                    else {
                        hideFixedColumns([index]);
                    }
                }
                else {
                    // set the visibility and redraw the table at the end
                    oTable.fnSetColumnVis(index, _columnVisibility[columnStoreKey], false);
                }
            }
        });
        */

        /** TEMP
        **
        var oTable = $("#" + _jobMatrixTableElementId).dataTable();
        var columns = oTable.fnSettings().aoColumns;
        $.each(columns, function (index, column) {

            // Use the full label key if exists, otherwise use the title
            var fullLabelKey = $(column.nTh).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : column.nTh.title);

            if (_columnVisibility.hasOwnProperty(columnStoreKey)) {
                var storedVisibility = _columnVisibility[columnStoreKey];
                
                // set the visibility and redraw the table at the end
                oTable.fnSetColumnVis(index, _columnVisibility[columnStoreKey], false);
            }
        });
        **/

        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        $.each(oTable.columns()[0], function (index, columnIndex) {
            // Use the full label key if exists, otherwise use the title
            var header = oTable.column(columnIndex).header();
            var fullLabelKey = $(header).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : $(header).attr("title"));

            if (_columnVisibility.hasOwnProperty(columnStoreKey)) {
                var storedVisibility = _columnVisibility[columnStoreKey];

                // set the visibility and redraw the table at the end
                oTable.column(columnIndex).visible(_columnVisibility[columnStoreKey]);
            }
        });

        customTableRedraw();
        updateStateHeaders();
    }

    // Loads the locally saved settings about column visibility
    function retrieveColumnVisibility() {
        // From legacy generic.js
        _columnVisibility = retrieveLocalObject(config.fullWebPath) || _columnVisibility;
    }
    // Updates the local list of visibility of the given column
    function updateColumnVisibility(index, visibility) {
        /** DEPRECATED
        var oTable = $("#" + _jobMatrixTableElementId).dataTable();
        var columns = oTable.fnSettings().aoColumns;

        if (columns[index]) {
            // Use the full label key if exists, otherwise use the title
            var fullLabelKey = $(columns[index].nTh).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : columns[index].nTh.title);

            if (isFixedColumn(index)) {
                _fixedColumns[index].visible = visibility;
            }
            _columnVisibility[columnStoreKey] = visibility;
        }
        */
        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        
        if (oTable.column(index)) {
            // Use the full label key if exists, otherwise use the title
            var header = oTable.column(index).header();
            var fullLabelKey = $(header).attr(_fullLabelAttr);
            var columnStoreKey = (fullLabelKey ? fullLabelKey : $(header).attr("title"));

            _columnVisibility[columnStoreKey] = visibility;
        }
        
        storeColumnVisibility();
        _redrawTimeoutID = setTimeout(showStoredColumns, _redrawTimeoutDelay);
    }
    // Stores the local visibility list in html5's browser storage
    function storeColumnVisibility() {
        // From legacy generic.js
        storeLocalObject(config.fullWebPath, _columnVisibility);
    }

    /**
    ** Custom redraw function
    **/
    function customTableRedraw() {
        //console.log("Redraw called");
        //removeAllTooltips();

        var oTable = $("#" + _jobMatrixTableElementId).DataTable();
        //oTable.api().columns.adjust().draw(false);
        //oTable.fnAdjustColumnSizing();
        // hideEmptyRows();
        oTable.settings()[0].oScroll.sY = _getTableScrollHeight();
        oTable.columns.adjust();
        oTable.draw(false);

        // DEPRECATED
        //overrideVisibilityBoxEvents();
        //hideDisabledFixedColumns();
        //updateFixedColumnsPanel();

        // Will add only the ones that don't have one
        addTooltips();
    }

    /**
    **
    **/
    function constructJobResultsDict(data) {
        // Create a dictionary of the JobResults with job ids as keys
        var jobResults = {};

        $.each(data[0].JobResults, function (i, jobResult) {
            if (jobResults.hasOwnProperty(jobResult.JobId))
                jobResults[jobResult.JobId].push(jobResult.SubmittedChangelistNumber);
            else
                jobResults[jobResult.JobId] = [jobResult.SubmittedChangelistNumber];
        });

        return jobResults;
    }

    /*
    function constructJobsDict(data) {
        // Create a dictionary of the JobResults with job ids as keys
        var jobsDict = {};

        $.each(data, function (i, task) {
            $.each(task.Jobs, function (j, job) {
                if (!jobsDict.hasOwnProperty(job.ID)) {
                    jobsDict[job.ID] = job;
                }
            });
        });

        return jobsDict;
    }
    */

    function constructConsumerJobsDict(data) {
        // Create a dictionary of the JobResults with job ids as keys
        var jobsDict = {};

        $.each(data, function (i, task) {
            $.each(task.Jobs, function (j, job) {
                if (job.Trigger.hasOwnProperty("Triggers") && !jobsDict.hasOwnProperty(job.ID)) {
                    jobsDict[job.ID] = job;
                }
            });
        });

        return jobsDict;
    }

    function getChangelistLink(changelist) {
        var link;

        if ((changelist) && (changelist != "undefined")) {
            if (automationConfig.submissionStates.hasOwnProperty(changelist)) {
                link = automationConfig.submissionStates[changelist];
            }
            else {
                link = $("<a />", {
                    text: changelist,
                    title: "View Changelist on Perforce Web",
                    href: automationConfig.perforceUrlPrefix + changelist + automationConfig.perforceUrlSuffix,
                    target: "_blank",
                }).prop("outerHTML");
            }
        }
        else {
            link = "-";
        }

        return link;
    }

    /**
    **
    **/
    function extractBuddyName(text) {
        var noBuddy = " ";
        if (!text) {
            return noBuddy;
        }

        var regexp = /Buddy\:(.+?)(?:\n|$)/i;
        var matches = text.match(regexp);
        return (matches && matches.length > 1) ? matches[1] : noBuddy;
    }

    /**
    **
    **/
    function extractFileName(text) {
        if (!text) {
            return "-";
        }

        var regexp = /([^\\]+)(.sln|.scproj)$/i;
        var matches = text.match(regexp);
        return (matches && matches.length > 1) ? splitWord(matches[1]) : text;
    }
   
    /**
    ** Split words from text in capital characters
    **/
    function splitWord(text) {
        return text.split(/[_\-.]/g).map(function (substr) { return (mapText(substr).match(/[A-Z]*[^A-Z]+/g) || [substr]).join(" "); }).join(" ");
    }

    /**
    **
    **/
    function splitToUnderscore(text) {
        return text.split(/_to_/gi).join("<br/>_to_<br/>");
    }

    /**
    **
    **/
    function mapText(state) {
        return (state && automationConfig.nameMappings.hasOwnProperty(state.toLowerCase())
            ? automationConfig.nameMappings[state.toLowerCase()]
            : state);
    }

    /**
    **
    **/
    function mapTextToIcon(state) {
        return (state && automationConfig.iconMappings.hasOwnProperty(state.toLowerCase())
            ? (
                $("<div>").append(
                    $("<img />").attr("src", automationServiceObj.imagesRootURI + automationConfig.iconMappings[state.toLowerCase()])
                )
                .html()
            )
            : state);
    }

    /**
    **
    **/
    function getRandomDictValue(dict) {
        for (var key in dict) {
            return dict[key];
        }
    }

    /**
    ** User Info Related
    **/
    function processUsersInfo() {
        $.ajax({
            url: automationConfig.usersPath + automationConfig.usersXml,
            type: "GET",
            dataType: "xml",

            success: function (xml, textStatus, jqXHR) {
                $(xml).find("User").each(function () {
                    _usersInfo[$(this).find("UserName").text().toLowerCase()] = {
                        Name: $(this).find("Name").text(),
                        Email: $(this).find("Email").text(),
                        JobTitle: $(this).find("JobTitle").text(),
                        ImageFilename: $(this).find("ImageFilename").text(),
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error("Failed loading content");
            },
            complete: function () {
                //console.log(usersInfo);
            }
        });
    }

    /**
    **
    **/
    function createUserInfoDiv(username) {
        var user = _usersInfo[username.toLowerCase()];
        var userInfoDiv;

        if (typeof user !== "undefined") {
            userInfoDiv = $("<div />")
                //.addClass("userdiv")
                .append(
                    $("<div />")
                        .addClass("img-wrap")
                        .append(
                            $("<img />").attr("src", automationConfig.usersPath + user.ImageFilename)
                        )
                )
                .append(
                        $("<div />")
                        .addClass("user-info")
                        .append(
                            $("<div />")
                                .addClass("user-info-name")
                                .html(user.Name.replace(" (", "<br />("))
                        )
                        .append(
                            $("<div />")
                                .addClass("user-info-rest")
                                .html(user.Email + "<br />" + user.JobTitle)
                        )
                )
        }
        else {
            userInfoDiv = $("<div />")
                .text(username.toLowerCase())
        }

        return userInfoDiv.html();
    }
    
    /**
    **
    **/
    function createErrorsListDiv(errors) {
        var tableDiv = $("<div />")
                .addClass("errors-list");

        if ((errors) && (errors.length != 0)) {
            var errorsTable = $("<table />").addClass("monospace");
            errorsTable.append(
                $("<tr />").append(
                    $("<th />")
                        .addClass("title")
                        .text("Errors")
                )
            );

            $.each(errors, function (i, error) {
                $("<tr />")
                    .append(
                        $("<td />")
                            .addClass("red")
                            .addClass("monospace")
                            .text(error)
                    )
                    .appendTo(errorsTable)
            });

            errorsTable.appendTo(tableDiv);
        }
        
        return tableDiv.html();
    }

    /**
    **
    **/
    function createCompactJobInfoDiv(job) {
        var tooltipHtmlWrap = $("<div>");
        var tooltipHtml = $("<div>").addClass("job-info");
        var jobInfoHtmlTable = $("<table>").addClass("monospace");

        if (job) {
            jobInfoHtmlTable.append(
            $("<tr>").append(
                $("<th>")
                    .attr("colspan", "2")
                    .addClass("title")
                    .text("Job Details")
                )
            );

            if (job.State) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("State:")
                    )
                    .append(
                        $("<td>")
                            .addClass(job.State.toLowerCase() + _gradientSuffix)
                            .text(mapText(job.State))
                    )
                )
            }
                
            jobInfoHtmlTable.append(
                $("<tr>").append(
                    $("<td>").text("ID:")
                )
                .append(
                    $("<td>").text(job.ID)
                )
            );

            if (job.ConsumedByJobID) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Consumed By:")
                    )
                    .append(
                        $("<td>").text(job.ConsumedByJobID)
                    )
                );
            }

            if (job.Priority) { // Only High Priority
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Priority:")
                    )
                    .append(
                        //$("<td>").text(_jobPriorityEnum[job.Priority])
                        $("<td>").text(job.Priority)
                    )
                )
            }

            var processedAtDate = parseJsonDate(job.ProcessedAt);
            var completedAtDate = parseJsonDate(job.CompletedAt);
            var createdAtDate = parseJsonDate(job.CreatedAt);

            jobInfoHtmlTable
            .append(
                $("<tr>").append(
                    $("<td>").text("Created At :")
                )
                .append(
                    $("<td>").text(toCustomISO(createdAtDate, _inUTC))
                )
            )
            .append(
                $("<tr>").append(
                    $("<td>").text("Processed At :")
                )
                .append(
                    $("<td>").text(
                                (
                                    (!isFutureDate(processedAtDate) && (typeof job.ProcessingTime != "undefined")) ?
                                        (toCustomISO(processedAtDate, _inUTC)
                                            + " (Processing Time: "
                                            + formatSecs(job.ProcessingTime)
                                            + ")"
                                        )
                                        : "Not processed"
                                )
                    )
                )
            )
            .append(
                $("<tr>").append(
                    $("<td>").text("Completed At :")
                )
                .append(
                    $("<td>").text(
                        ((!isFutureDate(completedAtDate)) ? toCustomISO(completedAtDate, _inUTC) : "Not completed")
                    )
                )
            );

            if (job.ProcessingHost) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Processing Host:")
                    )
                    .append(
                        $("<td>").text(job.ProcessingHost)
                    )
                )
            }

            if (_jobResults.hasOwnProperty(job.ID) && _jobResults[job.ID].hasOwnProperty("SubmittedChangelistNumber")) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Submitted Changelist:")
                    )
                    .append(
                        $("<td>").html(getChangelistLink(_jobResults[job.ID]["SubmittedChangelistNumber"]))
                    )
                )
            }

            var jobIntegrationP4Regex = getIntegrationP4Regex(job);
            if (Object.keys(jobIntegrationP4Regex ).length > 0) {
                jobInfoHtmlTable.append(
                    $("<tr>").append(
                        $("<td>").text("Integration P4Regex:")
                    )
                    .append(
                        $("<td>").html(createIntegrationP4RegexText(jobIntegrationP4Regex))
                    )
                )
            }
            
            jobInfoHtmlTable.appendTo(tooltipHtml);
        }

        tooltipHtml.appendTo(tooltipHtmlWrap);
        return tooltipHtmlWrap.html();
    }

    /**
    **
    **/
    function createSpinnerDiv(text) {
        var div = $("<div />");

        div.append(
            $("<div />")
            .append(
                $("<div />").html((text) ? text : "")
            )
            .append(
                $("<div />").addClass("spinner")
                    //.html("Loading ...")
            )
        );

        return div.html();
    }

    /**
    **
    **/
    function showUpdatingMessage() {

        $("body").append(
            $("<div>")
                .attr("id", "MessageOverlay")
                .addClass("updating-message-overlay")
                .append(
                    $("<div>")
                        .addClass("updating-message-text")
                        .html("Please Wait.<br/>The report is being updated.")
                )
        );
    }

    /**
    **
    **/
    function hideUpdatingMessage() {
       $("#" + "MessageOverlay").hide().remove();
    }

    /**
    **
    **/
    function getIntegrationP4Regex(job) {
        var integrationP4Regex = {};

        if (job && job.hasOwnProperty("Integration")) {
            if (job["Integration"].hasOwnProperty("P4RegexResolveOptions")) {
                $.each(job["Integration"]["P4RegexResolveOptions"], function (key, value) {
                    if (!integrationP4Regex.hasOwnProperty(key)) {
                        integrationP4Regex[key] = value;
                    }
                });
            }
        }

        return integrationP4Regex;
    }

    /**
    **
    **/
    function createIntegrationP4RegexText(integrationP4Regex) {
        var integrationP4RegexText = Object.keys(integrationP4Regex).map(function (key) {
                                        // remove the value from the key if it's there
                                        var filteredKey = key.replace(integrationP4Regex[key], "");
                                        return ("regex='" + filteredKey + "' resolve options='" + integrationP4Regex[key] + "'");
                                     })
                                    .join("\n");
        return integrationP4RegexText;
    }

    /**
    **
    **/
    function wrapTitleInHtmlTable(title) {
        var titleHtml =
                $("<div>")
                    .append(
                        $("<table>")
                            .addClass("monospace")
                            .append(
                                $("<tr>")
                                    .append(
                                        $("<th>")
                                            .html(title)
                                    )
                            )
                    );
        return titleHtml.html();
    }

    /**
    **
    **/
    function minimiseJob(job) {
        var mJob = {
            "ID": job.ID,
            "ConsumedByJobID": job.ConsumedByJobID,
            "State": job.State,
            "Priority": job.Priority,
            "CreatedAt": job.CreatedAt,
            "ProcessedAt": job.ProcessedAt,
            "CompletedAt": job.CompletedAt,
            "ProcessingHost": job.ProcessingHost,
            "ProcessingTime": job.ProcessingTime,
        }

        return mJob;
    }

    /**
    **
    **/
    function getLoadDataIntervalID() {
        return _loadDataIntervalID;
    }

    /**
    **
    **/
    return {
        clearDataTable: clearDataTable,
        getLoadDataIntervalID: getLoadDataIntervalID,
        initJobMatrix: initJobMatrix,
        initJobMatrixSummary: initJobMatrixSummary,
    };

};

