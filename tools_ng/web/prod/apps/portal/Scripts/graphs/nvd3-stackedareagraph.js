﻿
function drawStackedAreaGraph(elementId, graphOptions, datum) {
    nv.addGraph({
        generate: function () {
            var chart = nv.models.stackedAreaChart()
						.margin(graphOptions.margin)
						//.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
						.transitionDuration(config.transitionDuration)  //how fast do you want the lines to transition?
						.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
						.showYAxis(true)        //Show the y-axis
						.showXAxis(true)        //Show the x-axis
                        .showControls(false)
						.x(function (d) { return graphOptions.getX(d); })
						.y(function (d) { return graphOptions.getY(d); })
            ;

            chart.xAxis     //Chart x-axis settings
				.axisLabel(graphOptions.xLabel)
				.tickFormat(graphOptions.getXTick)
			    .rotateLabels(graphOptions.rotateXLabel)
            ;
            if (graphOptions.xTickValues) {
                chart.xAxis
                    .tickValues(graphOptions.xTickValues)
            }

            chart.yAxis     //Chart y-axis settings
				.axisLabel(graphOptions.yLabel)
				.tickFormat(graphOptions.getYTick)
            ;

            d3.select("#" + elementId + " svg")
				.datum(datum)
    			.call(chart)
            ;

            if (graphOptions.scaleYAxis) {
                //rescaleYAxis(chart, graphOptions.scaleYAxis);
                //chart.legend.dispatch.on('stateChange', function () { rescaleYAxis(chart, graphOptions.scaleYAxis); });
            }

            nv.utils.windowResize(chart.update);
            return chart;
        },
        callback: function () {
            //fillCircles(elementId);
        },
    });
}

function rescaleYAxis(chart, scaleFactor) {
    var yDomain = chart.yAxis.scale().domain();

    yDomain[0] = (yDomain[0] < 0) ? (yDomain[0] - (yDomain[0] * scaleFactor)) : 0;
    yDomain[1] = yDomain[1] + (yDomain[1] * scaleFactor);

    chart.yDomain(yDomain);
    chart.update();
}

function fillCircles(elementId) {
    //d3.selectAll("#" + elementId + " svg circle.nv-point").classed("hover", true);
}