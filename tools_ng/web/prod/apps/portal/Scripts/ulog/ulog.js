﻿var defaultFilterText = "", //"Find Text ...",
    tableScrollHeight = function () { return ($("#content-body").height() - 145) },
    tooltipDelay = 500; // half sec

// Custom Extention for Log Level filtering
$.fn.dataTableExt.afnFiltering.push(
    function (oSettings, aData, iDataIndex) {

        var tr = oSettings.aoData[iDataIndex].nTr;
        var rowClass = $(tr).find(".cell-icon").attr("title");

        var cb = $("#filters input:checkbox." + rowClass);
        if (rowClass && cb && !$(cb).prop("checked")) {
            return false;
        }

        formatDate(tr, false);
        
        return true;
    }
);

function setupUlog(tableCloneId, tableId) {

    var oTable = $("#" + tableId).DataTable({
        "scrollX": "100%",
        "scrollXInner": "100%",
        "scrollY": tableScrollHeight(),
        "scrollCollapse": true,
        "deferRender": true,
        "ordering": true,           // Enable Sorting
        "pageLength": 25,
        "paging": true,            // Enable Pagination
        "searching": true,          // Enable Filter
        "info": true,              // Enable Info
        "order": [[1, "asc"]],
        "columnDefs": [
            { "targets": [0], "sortable": false }
        ],
        "stateSave": false, // save the state of the table
        "autoWidth": true,
        "dom": 'CrtiS',
        //"processing": true,
        "oColVis": {
			"buttonText": "Visible Columns",
        },
        "initComplete": function (oSettings, json) {
            $("div.ColVis button")
                .addClass("table-title-button")
                .appendTo("#colvis-button");
        },
    });

    // Configure the filter input text
    $("#filter")
        .val(defaultFilterText)
		.focus(function() {
		    if ($(this).val() == defaultFilterText)
		        $(this).val("");
		})
		.blur(function() {
		    if ($(this).val() == "")
		        $(this).val(defaultFilterText);
		})
		.keyup(function() {
		    oTable.search($(this).val()).draw(false);
		});
   
    // Setup the log level filters
    $("#filters input:checkbox").change(function () {
        //typewatch(function () { filterTable(tableId, oTable); }, 500);
        typewatch(function () { oTable.draw(false); });
    });
    
    // Setup the job info tooltip
    $("#job-type").tooltip({
        items: "span",
        tooltipClass: "job-info-tooltip",
        position: { my: "left top", at: "left bottom" },
        content: function() {
            return $(".job-info-wrapper").html();
        },
        show: {
            effect: "fadeIn",
            delay: tooltipDelay,
        },
        open: function() {
            //debugger;
        },
        close: function (event, ui) {
            var me = this;
            ui.tooltip.hover(
                function () {
                    $(this).stop(true).show();
                },
                function () {
                    $(this).fadeOut("slow", function () {
                        $(this).remove();
                    });
                }
            );
        },
    });

    //Update the table's format
    updateTable(tableCloneId, oTable);

    // Redraw on window resize
    $(window).resize(function () {
        updateScrollHeight(oTable);
        //oTable.draw(true);
    });

    // Show the table - chrome fix
    $("#" + tableCloneId).remove();

}

function formatDate(tr, utc) {
    var td = $(tr).find("td.timestamp-cell");
    // treat the given date as utc so subtract the Timezone offset
    var date = new Date(new Date(td.attr("title")).getTime() - new Date().getTimezoneOffset()*60*1000);

    //td.text((isValidDate(date)) ? getDatetimeString(date, utc) : "");
    td.text((isValidDate(date)) ? toEuropeanFormat(date, utc) : "");
}

function updateTable(tableId, oTable) {
    var rows = $("#" + tableId + " tbody tr");

    $.each(rows, function (i, row) {
        oTable.row.add(row);
    });
    oTable.draw();
}

function updateScrollHeight(oTable) {
    oTable.settings()[0].oScroll.sY = tableScrollHeight();
    oTable.draw(false);
}
