// reportOptions variable comes from respective config file

// Array that stores objects for each element returned via rest
var chartsData;

var NVGraphs = new NVD3CustomGraphs();

var mainDOMId = "piechart";
var breakdownDOMId = "breakdown";

var barHeight = 15;

var lastSliceClicked = null;
var csvExportButton;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content")
	.append(
		$("<div>")
			.attr("id", "friendlier-names-field")
			.append(
				$("<input>")
					.attr("id", "friendlier-names")
					.attr("type", "checkbox")
					.attr("disabled", true)
			)
			.append(
				$("<label>")
					.attr("for", "friendlier-names")
					.attr("title", "Check to Switch to Friedlier Names")
					.text("Friendlier Names")
			)	
	);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", mainDOMId)
				//.addClass((reportOptions.isClickable) ? "clickable" : "")
		)
		.append(
			$("<div>").attr("id", breakdownDOMId)
		);
	
	initSvg(mainDOMId);
	
	if (reportOptions.hasFriendlierNames) {
		$("#friendlier-names").attr("disabled", false);
		$("#friendlier-names").prop("checked", true);
		$("#friendlier-names").click(function() {
			if (chartsData)
				typewatch(drawCharts, 500);
		});
	}
	
	if (reportOptions.description)
		$("#content-description").html(reportOptions.description);
	
	$("#filter").click(function() {
		csvExportButton = false; // Regenerate csv export data
		generateCharts();
	});
	

	generateCharts();
}

function generateCharts() {
	chartsData = [];
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if (!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
			
} //end of generateCharts()
function cleanCharts() {
	cleanGraph(mainDOMId);
	cleanDetailedChart();
}

function cleanDetailedChart() {
	//$("#" + breakdownItemDOMId).hide("fade", {}, "normal");
	cleanGraph(breakdownDOMId);
	lastSliceClicked = null;
}

function drawCharts(data) {
	//cleanDetailedChart();
	//$("#detailed-piechart").hide("fade", {}, "normal");
	
	// If a processFunction function is specified
	if (data) {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
		
	if (reportOptions.enableCSVExport && !csvExportButton)
		enableCsvExport();
	
	if (reportOptions.enablePNGExport)
		addExportPngButton(reportOptions.enablePNGExport);
	
	var graphOptions = reportOptions.main;
	
	var datum = [{
		key: graphOptions.getPieLabel(chartsData),
		values: graphOptions.getValuesArray(chartsData),
		metadata: graphOptions.getMetadata(chartsData),
	}];
	
	var clickFunction = (reportOptions.isClickable) ? 
			(function(event) { 
				lastSliceClicked = event.label;
				event.point["label"] = event.label;
				event.point["color"] = event.color;
				drawDetailedChart(event.point);
			})
			: null;
	
	NVGraphs.drawPiechart(datum, mainDOMId, graphOptions, clickFunction);
	
	if (!reportOptions.isClickable)
		drawDetailedChart(chartsData);
	
	if (lastSliceClicked) {
		var availableLabels = datum[0].values.map(function(d) { return graphOptions.getName(d); });
		var searchIndex = availableLabels.indexOf(lastSliceClicked);
		
		if (searchIndex != -1) {
			drawDetailedChart(datum[0].values[searchIndex]);
		}
		else {
			// clean it cause the previous selected item is not available
			cleanDetailedChart();
		}
	}
				
}

function drawDetailedChart(selectedObject) {
	// Construct the chart data in a format that nvd3 understands
	
	var graphOptions = reportOptions.breakdown;	
	var detailedObject = graphOptions.getObject(selectedObject);

	var elementId = breakdownDOMId + "_0"; // will that support multiple?
	
	// This will add the svg on the first time
	if ($("#" + breakdownDOMId).find("svg").size() == 0) {
		$("#" + breakdownDOMId)
			.addClass("barchart-breakdown")
			.append(
				$("<div>")
					.attr("id", elementId)
			);
		
		initSvg(elementId);
	}
	
	var datum = [{
		key: graphOptions.getLabel(detailedObject),
		values: graphOptions.getValuesArray(detailedObject),
		color: graphOptions.getColor(detailedObject),
		metadata: graphOptions.getMetadata(detailedObject),
	}];
	
	NVGraphs.drawHorizBarchart(datum, elementId, graphOptions);
	
	updateBarHeight(datum, elementId);
	$(window).resize(function() {
		updateBarHeight(datum, elementId);
	})
	
}

function updateBarHeight(datum, elementId) {
	var graphHeight = (barHeight * datum[0].values.length); // pixels
	var containerHeight = $("#" + mainDOMId).height();

	if (graphHeight < containerHeight) {
		$("#" + elementId).css("height", containerHeight);
		$("#" + elementId + " svg").css("height", containerHeight);
	}
	else {
		$("#" + elementId).css("height", graphHeight);
		$("#" + elementId + " svg").css("height", graphHeight);
	}
}

/* Copied from piechart_breakdown-report.js */
function enableCsvExport() {
	
	var csvColumns = [" "];
	var csvRows = [];
	
	var selectedMetricObj;
	$("#content-description-radiometrics").find("input").each(function(metricIndex, metricObj) {
		selectedMetricObj = ($(metricObj).prop("checked")) ? metricObj : selectedMetricObj;
		$(metricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	
		populateCSVArrays(csvColumns, csvRows);
	});
	
	if ($("#content-description-radiometrics").find("input").size() == 0)
		populateCSVArrays(csvColumns, csvRows); // Run for the default setting
		
	// Reset checkbox selection
	if (selectedMetricObj) {
		$(selectedMetricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	}
	
	
	// Add the button in the DOM
	var csvFrameID = "piechart-breakdown-frame";
	$("#" + csvFrameID).remove();
			
	addCSVButton(reportOptions.enableCSVExport, 
			csvFrameID, 
			function() {
				exportToCSV(csvColumns, 
							csvRows, 
							csvFrameID, 
							getCSVFilename()); 
			});
		
	$("#" + csvFrameID).addClass("csv-right");
	csvExportButton = true;
}

function populateCSVArrays(csvColumns, csvRows) {
	// Gather the main piechart data
	var mainGraphOptions = reportOptions.main;
	var mainTotal = d3.sum(mainGraphOptions.getValuesArray(chartsData), 
						function(mainItem) {return mainGraphOptions.getValue(mainItem);}
					);
	$.each(mainGraphOptions.getValuesArray(chartsData), function(i, mainItem) {
		if (!csvRows[i]) {
			csvRows[i] = [];
			csvRows[i][0] = mainGraphOptions.getName(mainItem);
		}
		csvRows[i][csvColumns.length] = mainGraphOptions.getValue(mainItem);
		csvRows[i][csvColumns.length+1] = formatPercentage(mainGraphOptions.getValue(mainItem), mainTotal);
	});
	
	csvColumns.push(mainGraphOptions.getPieLabel(chartsData));
	csvColumns.push("%");
	
	var currentRowId = mainGraphOptions.getValuesArray(chartsData).length;
	
	var breakdownGraphOptions = reportOptions.breakdown;
	$.each(mainGraphOptions.getValuesArray(chartsData), function(j, mainItem) {
		
		var breakdownItems = breakdownGraphOptions.getObject(mainItem);
		
		if (typeof breakdownGraphOptions.getValuesArray(breakdownItems) === "undefined")
			return; // No breakdown
		
		csvRows[++currentRowId] = [" ", " ", " "]; // Increase and add Empty Line
		
		currentRowId++; // Increase the row
		if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
			csvRows[currentRowId] = [];
			csvRows[currentRowId][0] = 
				(breakdownGraphOptions.getLabel(breakdownItems) || mainGraphOptions.getName(mainItem)).toUpperCase();
		}
		
		csvRows[currentRowId] = csvRows[currentRowId].concat(
		     // use the breakdown pie label or the main piechart name if absent
		     mainGraphOptions.getPieLabel(chartsData),
		     "%"
		);
		
		var breakdownTotal = d3.sum(breakdownGraphOptions.getValuesArray(breakdownItems), 
				function(breakdownItem) {return breakdownGraphOptions.getValue(breakdownItem);}
		);
		$.each(breakdownGraphOptions.getValuesArray(breakdownItems), function(l, breakdownItem) {
			
			currentRowId++; // Increase the row
			
			if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
				csvRows[currentRowId] = [];
				csvRows[currentRowId][0] = breakdownGraphOptions.getName(breakdownItem);
			}
			
			csvRows[currentRowId] = csvRows[currentRowId].concat([
			    breakdownGraphOptions.getValue(breakdownItem),
			    formatPercentage(breakdownGraphOptions.getValue(breakdownItem), breakdownTotal),
			]);
		});
		
	});
}