// Array that stores objects for each element returned via rest
var reportData = [];
var map;

var mapId = "map";

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body").append(
		$("<div>").attr("id", "map-wrapper")
			.append(
				$("<fieldset>").attr("id", mapId)
			)
			.append(
				$("<div>").attr("id", "map-side-options")
			)
	);
	
	map = new SVGMap(mapId, project.map);
	map.drawLayers("map-side-options");
	map.addHeatmapOptions("map-side-options", drawGraph);
	map.hide();
	
	$("#filter").click(function() {
		generateReport();
	});
	generateReport();
}

function generateReport() {

	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if(!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, populateReport);
	}
	else {
	
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		req.sendSingleAsyncRequest(pValues, populateReport);
	}
	
} //end of generateReport()

function populateReport(data) {
	
	if (data) {
		if (reportOptions.processFunction)
			reportData = reportOptions.processFunction(data);
		else
			reportData = data;
	}
		
	if (reportData.length < 1) {
		Sexy.alert(config.noDataMapText);
		map.hide();
		return;
	}
	
	drawGraph();
}

function drawGraph() {
	map.show();
	map.removeOverlay();
    	
    map.drawHeatmap(reportData, reportOptions.mapDataFunctions);
    	
    $("#export-map").remove();
    	
    $("#content-description").append(
    	$("<input />")
    		.attr("type", "button")
    		.attr("id", "export-map")
    		.attr("name", "export-map")
    		.attr("title", "Export current viewport to png")
    		.val("Export Map to PNG")
    		.css("float", "right")
    		.addClass("red-button hand")
    		.click(map.exportToImage)
    );
    	
    if ($.browser.msie)
    	$("#export-map")
    		.addClass("hidden");
}


