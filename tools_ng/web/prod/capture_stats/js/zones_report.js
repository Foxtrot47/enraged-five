/*
var maxResults = 10,
	valueLimit = 0.5,
	diffLimit = 0.025,
	svgHeight = 600,
	navPosition = 1,
	xAxisLabel,
	reportTypeValue;
*/

//Array to store the fps graph data
var fpsGraphArrays;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#filter").click(function() {
		generateReport();
	});
	
	generateReport();
}

function generateReport() {
	$("#report").empty();
	fpsGraphArrays = [];
	
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
	req.sendSingleAsyncRequest(pValues, populateReport);
}


function populateReport(data) {
		
	$("#report")
		.append(
			$("<legend />")
				.append(
					$("<span />")
						.text(reportOptions.legendText)
				)
		);
	
	var fpsGraphId = "fps_graph";
	$("#report")
		.append(
			$("<fieldset />")
				.attr("id", fpsGraphId)
				.addClass("report-graph")
				.append(
					$("<legend />")
						.html("Average FPS (Click to render)")
				)
				.click(function() {
					if ($(this).find("svg").size() == 0) {					
						initSvg(fpsGraphId, reportOptions.svgHeight);
						drawLinegraph(fpsGraphArrays, fpsGraphId, reportOptions.xAxisLabel, "Average FPS");
						generateGraphTable(fpsGraphArrays, fpsGraphId);
						$(this).css("height", "auto");
						$(this).find("svg").css("height", reportOptions.svgHeight);
						//console.log(fpsGraphArrays);
					}
				})
		);
	
	$("#report")
		.append(
			$("<table>")
				//.attr("id", index)
				.append(
					$("<tr>")
						.append(
							$("<th>")
								.addClass("title")
								.text(data.ZoneName)
							)
					)
			)
		.append(
			$("<fieldset />")
				.attr("id", data.ZoneName)
				.append(
					$("<ul />")
						.addClass("tab-nav")
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_fps")
										.attr("title", "FPS")
										.text("FPS")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_fps", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_threads")
										.attr("title", "Threads")
										.text("Threads")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_threads", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_Gpu")
										.attr("title", "GPU")
										.text("GPU")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_Gpu", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_Main", {duration: "slow"})
										.attr("title", "Main")
										.text("Main")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_Main", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_Render", {duration: "slow"})
										.attr("title", "Render")
										.text("Render")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_Render", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_gpu_graph")
										.attr("title", "GPU Graph")
										.text("GPU Graph")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_gpu_graph", {duration: "slow"})
										})
								)
						)
						.append(
							$("<li />")
								.append(
									$("<a />")
										.attr("href", "#" + data.ZoneName + "_cpu_graph", {duration: "slow"})
										.attr("title", "Main Graph")
										.text("Main Graph")
										.click(function(e) {
											e.preventDefault();
											$.scrollTo("#" + data.ZoneName + "_cpu_graph", {duration: "slow"})
										})
								)
						)
				)
		);
	
	// FPS
	if (data.FpsResults.Values.length == 0)
		return;
	
	fpsGraphArrays.push({
		values: data.FpsResults.Values, 
		color: config.colourRange(Math.random()*20), 
		key: data.ZoneName,
	});
	
	var fpsCurrent = getLastValue(data.FpsResults.Values),
		fpsPrevious = getPenultimateValue(data.FpsResults.Values),
		fpsDiff = (fpsCurrent - fpsPrevious).toFixed(config.captureStatsDecimals);
	
	
	$("#" + data.ZoneName)
		.append(
			$("<div />")
				.attr("id", data.ZoneName + "_fps")
				.append(
				$("<table />")
					.append(
						$("<tr>")
							.append(
								$("<th>")
									.addClass("title2")
									.attr("colspan", 3)
									.text("FPS")
							)
					)
					.append(
						$("<tr>")
							.append(
								$("<th>").text("Current")
							)
							.append(
								$("<th>").text("Previous (std)")
							)
							.append(
								$("<th>").text("Diff")
							)
					)
					.append(
						$("<tr>")
							.append(
								$("<td>").text(fpsCurrent)
							)
							.append(
								$("<td>").text(
									fpsPrevious
									+ " ("
									+ data.FpsResults.StandardDeviation.toFixed(config.captureStatsDecimals)
									+ ")"
								)
							)
							.append(
								$("<td>")
									.css("color", getColour(fpsDiff))
									.text(fpsDiff)
							)
					)
				)
			);
	
	// End of FPS

	// Threads Report
	$("#" + data.ZoneName)
		.append(
			$("<div />")
				.attr("id", data.ZoneName + "_threads")
				.append(
					$("<table />")
						.addClass("title-only")
						.append(
							$("<tr>")
								.append(
									$("<th>")
										.addClass("title2")
										.attr("colspan", 4)
										.text("Threads")
								)
						)
				)
				.append(
					$("<table />")
						//.attr("id", fixedZoneName + "_threads")
						.addClass("tablesorter")
						.append(
							$("<thead>")
								.append(
									$("<tr>")
										.append(
											$("<th>").text("Item")
										)
										.append(
											$("<th>").text("Current")
										)
										.append(
											$("<th>").text("Previous (std)")
										)
										.append(
											$("<th>").text("Diff")
										)
								)
						)
				)
		);
			
	$.each(data.ThreadResults.Results, function(i, item) {
		var threadCurrent = getLastValue(item.Value.Values),
			threadPrevious = getPenultimateValue(item.Value.Values),
			threadDiff = (threadCurrent - threadPrevious).toFixed(config.captureStatsDecimals);
		
		//if ((threadCurrent <= valueLimit) || (threadPrevious <= valueLimit) || (Math.abs(threadDiff) <= diffLimit))
		//	return;
		
		$("#" + data.ZoneName + "_threads " + " table.tablesorter")
			.append(
					$("<tr>")
					.append(
							$("<td>").text(item.Key)
					)
					.append(
							$("<td>").text(threadCurrent)
					)
					.append(
							$("<td>").text(
									threadPrevious
									+ " ("
									+ item.Value.StandardDeviation.toFixed(config.captureStatsDecimals)
									+ ")"
							)
					)
					.append(
							$("<td>")
								.css("color", getColour(threadDiff))
								.text(threadDiff)
					)
		);
				
	});
	
	if ($("#" + data.ZoneName + "_threads " + " table.tablesorter").find("tr").length > 1) {
		$("#" + data.ZoneName + "_threads " + " table.tablesorter").tablesorter({
			//Sort on the first column ASC
			sortList: [[0, 0]]
		});
	}

	// End of Threads 
	
	// CPU 
	var cpuGraphArrays = [];
	var gpuGraphArrays = [];
	
	$.each(data.CpuResults.PerSetResults, function(i, timebarItem) {
		$("#" + data.ZoneName)
			.append(
				$("<div />")
					.attr("id", data.ZoneName + "_" + timebarItem.Key)
					.append(
						$("<table />")
							.addClass("title-only")
							.append(
								$("<tr>")
									.append(
										$("<th>")
											.addClass("title2")
											.attr("colspan", 4)
											.text(timebarItem.Key)
									)
							)
					)
					.append(
						$("<table />")
							.addClass("tablesorter")
							.append(
								$("<thead>")
									.append(
										$("<tr>")
											.append(
												$("<th>").text("Item")
											)
											.append(
												$("<th>").text("Current")
											)
											.append(
												$("<th>").text("Previous (std)")
											)
											.append(
												$("<th>").text("Diff")
											)
									)
							)
					)
			);
					
		$.each(timebarItem.Value.PerMetricResults, function(i, metricItem) {
			var metricCurrent = getLastValue(metricItem.Value.Values),
				metricPrevious = getPenultimateValue(metricItem.Value.Values),
				metricDiff = (metricCurrent - metricPrevious).toFixed(config.captureStatsDecimals);

			if ((metricCurrent <= reportOptions.valueLimit) || (metricPrevious <= reportOptions.valueLimit) 
					|| (Math.abs(metricDiff) <= reportOptions.diffLimit))
				return;
			
			$("#" + data.ZoneName + "_" + timebarItem.Key + " table.tablesorter")
				.append(
					$("<tr>")
					.append(
							$("<td>").text(metricItem.Key)
					)
					.append(
							$("<td>").text(metricCurrent)
					)
					.append(
							$("<td>").text(
									metricPrevious
									+ " ("
									+ metricItem.Value.StandardDeviation.toFixed(config.captureStatsDecimals)
									+ ")"
							)
					)
					.append(
							$("<td>")
								.css("color", getColour(metricDiff))
								.text(metricDiff)
					)
				);
			
			
		
			if (timebarItem.Key == "Gpu") {
				var metricAvg = d3.mean(metricItem.Value.Values, function(d) {return d.Value});
				gpuGraphArrays.push({
					values: metricItem.Value.Values, 
					color: config.colourRange(Math.random()*20), 
					key: metricItem.Key + " (" + metricAvg.toFixed(config.captureStatsDecimals) + ")",
				});
			}
			else if (timebarItem.Key == "Main") {
				var metricAvg = d3.mean(metricItem.Value.Values, function(d) {return d.Value});
				cpuGraphArrays.push({
					values: metricItem.Value.Values, 
					color: config.colourRange(Math.random()*20), 
					key: metricItem.Key + " (" + metricAvg.toFixed(config.captureStatsDecimals) + ")",
				});
			}
		
			
		}); // end of each perMetricResult
		
		
		
		if ($("#" + data.ZoneName + "_" + timebarItem.Key + " table.tablesorter").find("tr").length > 1) {
			$("#" + data.ZoneName + "_" + timebarItem.Key + " table.tablesorter").tablesorter({
			//	//Sort on the first column ASC
				sortList: [[0, 0]]
			});
		}
		
		//console.log(timebarItem.Key);
		//console.log($("#" + fixedZoneName + "_" + timebarItem.Key + " table.tablesorter").find("tr").length);
			
	}); // end of each cpu results
	
	var gpuGraphId = data.ZoneName + "_gpu_graph";
	$("#" + data.ZoneName)
		.append(
			$("<fieldset />")
				.attr("id", gpuGraphId)
				.addClass("report-graph")
				.append(
					$("<legend />")
						.html("GPU Graph (Click to render)")
				)
				.click(function() {
					if ($(this).find("svg").length == 0) {
						$(this).css("height", reportOptions.svgHeight);
						initSvg(gpuGraphId, reportOptions.svgHeight);
						drawLinegraph(gpuGraphArrays, gpuGraphId, reportOptions.xAxisLabel, "Average Time (ms)");
					}
				})
		);
	
	var cpuGraphId = data.ZoneName + "_cpu_graph";
	$("#" + data.ZoneName)
		.append(
			$("<fieldset />")
				.attr("id", cpuGraphId)
				.addClass("report-graph")
				.append(
					$("<legend />")
						.html("Main Graph (Click to render)")
				)
				.click(function() {
					if ($(this).find("svg").length == 0) {
						$(this).css("height", reportOptions.svgHeight);
						initSvg(cpuGraphId, reportOptions.svgHeight);
						drawLinegraph(cpuGraphArrays, cpuGraphId, reportOptions.xAxisLabel, "Average Time (ms)");
					}
				})
		);
	
}

function drawLinegraph(datum, htmlId, xLabel, yLabel) {
	if (datum.length < 1)
		return;
	
	nv.addGraph(function() {
		var chart =  nv.models.lineChart()
			//.x(function(d) { return d.Key; })
			.x(function(d, i) { return i; })
			.y(function(d) { return Number(d.Value); })
			.margin({top: 50, right: 40, bottom: 100, left: 80})
			.tooltips(true);
		
		chart.xAxis
			.axisLabel(xLabel)
			//.tickValues(datum[0].values.map(function(d) {return d.Key; }))
			//.tickFormat(function(d, i) {return d;})
			.tickValues(d3.range(datum[0].values.length))
			.tickFormat(function(d, i) {
				return (datum[0].values[parseInt(d)] ? datum[0].values[parseInt(d)].Key : d);
			})
			.rotateLabels(-45);
			
		chart.yAxis
			.axisLabel(yLabel)
			.tickFormat(d3.format(".02f"));
		
		//chart.tooltipContent(function(key, x, y, e, graph) {
		//     return "<h3>" + key + "</h3>" +
		//     		"<p>" +  y + " at " + (new Date(x)).toDateString() + "</p>";
		//});
	
		d3.select("#" + htmlId + " svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
	 
		nv.utils.windowResize(chart.update);
	 
		return chart;
	});
}

function generateGraphTable(datum, elementId) {
	if (datum.length < 1)
		return;
	
	var keys = {}; // save all the builds or the changelists found in the data
	$.each(datum, function(i, zone) {
		$.each(zone.values, function(i, zoneValue) {
			if (!keys.hasOwnProperty(zoneValue.Key))
				keys[zoneValue.Key] = null;
		})
	});
	
	var table = $("<table />");
	var trh = $("<tr />").append(
		$("<th />")
			.css("font-style", "italic")
			.html(reportOptions.xAxisLabel)
		);
	
	$.each(keys, function(key) {
		trh.append(
			$("<th />")
				.html(key)
		)
	});
	table.append(trh);
		
	$.each(datum, function(i, zone) {
		var tr = $("<tr />");
		tr.append(
			$("<td />").html(zone.key)
		)
		
		$.each(keys, function(key) {
			var zoneValue = "-";
			$.each(zone.values, function(i, value) {
				if (value.Key == key) {
					zoneValue = value.Value;
				}
			});
			
			tr.append(
				$("<td />").html(zoneValue)
			)
		});
		
		table.append(tr);
	});
	
	$("#" + elementId).append(table);
	
}

function getLastValue(array) {
	return array[array.length - 1].Value.toFixed(config.captureStatsDecimals);
}

function getPenultimateValue(array) {
	return ((array.length > 1) ? array[array.length - 2].Value : 0).toFixed(config.captureStatsDecimals);
}

function getColour(value) {
	if (value < 0)
		return "green";
	else
		return "red";
}
