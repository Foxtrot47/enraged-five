var headerAndFilters  = {
	headerType: "header-cs-report", // capture stats report header
	disabledFields:				// disables header fields 
		[
		 false, // first metric - zones
		 false, // second metric - cutscenes
	     ],
};

var fixed = function(d) { return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.cutsceneCaptures,
	restEndpointAsync: config.cutsceneCapturesAsync,
	
	legendText: "Cutsene Captures ",
	
	valueLimit: 0.5,
	diffLimit: 0.025,
	svgHeight: 600,
	xAxisLabel: "Build",
	
	/*
	
	tableTitle: "Clip Dictionaries",

	reportItems: [
	     {title: "Build", getValue: function(d) {return d.BuildName; }},
	     {title: "Clip Name", getValue: function(d) {return d.ClipName; }},
	     {title: "Dictionary Name", getValue: function(d) {return d.DictionaryName; }},
	     {title: "Times Played", getValue: function(d) {return d.TimesPlayed; }},
	],
	*/

};