// Object for storing any project specific variables
var project = {
	name: "GTA V",
	fullName: "Grand Theft Auto V",
	
	logo : {
		src: "/project/gtav-logo-margin.png",
		title: "GTA V logo",
	},
	
	map: {
		svgFile: "/project/gtav-map.svg",
		vectorsFile: "/project/gtav-map_vectors.xml",
		//origin = {x: 4450, y: 7450} , extra = {x: -15, y: 65};
		coords: {
			x: 4450 - 15,
			y: 7450 + 65,
		},
		scale: 10.24,
		
		layers: [
		   "grid",
		   "sea",
		   "landcity",
		   "landbeach",
		   "landgreen",
		   "tunnels",
		   "metro",
		   "roads",
		   "rail",
		],
	},
	
	freemodeAltName: "GTAO:",
	
	scAdminUrl: "https://dev.scadmin.rockstargames.com/GTAV/",
	scAdminJobsPath: "Mission/",
	
	spMissionGroups: [
	     {title: "Main Missions", regexp: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|VOID|NET))", filterItem: 0},
	     {title: "Ambient", regexp: "^AMB", filterItem: 0},
	     {title: "Mini Games", regexp: "^MG", filterItem: 0},
	     {title: "Odd Jobs", regexp: "^Odd", filterItem: 0},
	     {title: "Random Events", regexp: "^RE", filterItem: 0},
	     {title: "Random Characters", regexp: "^RC", filterItem: 0},
	     {title: "Special Peds", regexp: "^Special Ped", filterItem: 0},
	     //{title: "Cops n Crooks", regexp: "^CnC", filterItem: 0},
	],
	
	portalUrl: "http://rsgeditp1/Project/GTAV/",
	portalAutomation: "Automation",
};
