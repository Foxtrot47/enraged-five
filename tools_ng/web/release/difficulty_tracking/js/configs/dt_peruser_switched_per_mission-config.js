var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	     true, // build
	     true, // platform
	     false, // user
	],
};

var usersDict = getPlaythroughUsersAsDict();

var orderedResults = [
    /*
    {
    	ids: ["MISS_SWITCH_agency_heist1"],
    	name: "The Agency Heist 1 - Setup - Follow Janitor",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist2A"],
    	name: "The Agency Heist 2 - Mug the Architect",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist2B"],
    	name: "The Agency Heist 2B",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist2C"],
    	name: "The Agency Heist 2C",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist3A"],
    	name: "The Agency Heist 3A Finale - Plant Bombs",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist3B"],
    	name: "The Agency Heist 3B Finale - Steal FIB Docs (Helicopter)",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist4A"],
    	name: "The Agency Heist 4A",
    },
    {
    	ids: ["MISS_SWITCH_armenian1"],
    	name: "Armenian 1 - Franklin and Lamar",
    },
    {
    	ids: ["MISS_SWITCH_armenian2"],
    	name: "Armenian 2 - Repossession",
    },
    {
    	ids: ["MISS_SWITCH_armenian3"],
    	name: "Armenian 3 - Complications",
    },
    {
    	ids: ["MISS_SWITCH_carsteal_intro_cut"],
    	name: "Car Steal Intro Cutscene",
    },
    {
    	ids: ["MISS_SWITCH_carsteal1"],
    	name: "Car Steal 1 - I fought the law...",
    },
    {
    	ids: ["MISS_SWITCH_carsteal2"],
    	name: "Car Steal 2 -  Eye in the Sky",
    },
    {
    	ids: ["MISS_SWITCH_carsteal3"],
    	name: "Car Steal 3 - Agent",
    },
    {
    	ids: ["MISS_SWITCH_carsteal4"],
    	name: "Car Steal 4 - Finale - Pack Man",
    },
    {
    	ids: ["MISS_SWITCH_carsteal5"],
    	name: "Car Steal 5",
    },
    {
    	ids: ["MISS_SWITCH_chinese1"],
    	name: "Chinese 1 - Trevor Philips Industries",
    },
    {
    	ids: ["MISS_SWITCH_chinese2"],
    	name: "Chinese 2 - Crystal Maze",
    },
    {
    	ids: ["MISS_SWITCH_docks_heistA"],
    	name: "Docks Heist A",
    },
    {
    	ids: ["MISS_SWITCH_docks_heistB"],
    	name: "Docks Heist B",
    },
    {
    	ids: ["MISS_SWITCH_docks_prep1"],
    	name: "Docks Heist Prep1",
    },
    {
    	ids: ["MISS_SWITCH_docks_prep2B"],
    	name: "Docks Heist Prep2B",
    },
    {
    	ids: ["MISS_SWITCH_docks_setup"],
    	name: "Docks Setup",
    },
    {
    	ids: ["MISS_SWITCH_exile1"],
    	name: "Exile 1 - Minor Turbulence",
    },
    {
    	ids: ["MISS_SWITCH_exile2"],
    	name: "Exile 2 - Predator",
    },
    {
    	ids: ["MISS_SWITCH_exile3"],
    	name: "Exile 3 - Derailed",
    },
    {
    	ids: ["MISS_SWITCH_family1"],
    	name: "Family 1 - Father and Son",
    },
    {
    	ids: ["MISS_SWITCH_family2"],
    	name: "Family 2 - Daddy's Little Girl",
    },
    {
    	ids: ["MISS_SWITCH_family3"],
    	name: "Family 3 - Marriage Counselling",
    },
    {
    	ids: ["MISS_SWITCH_family4"],
    	name: "Family 4 - Fame or Shame",
    },
    {
    	ids: ["MISS_SWITCH_family5"],
    	name: "Family 5 - Did somebody say yoga",
    },
    {
    	ids: ["MISS_SWITCH_family6"],
    	name: "Family 6 - Reunite Family ",
    },
    {
    	ids: ["MISS_SWITCH_fbi1"],
    	name: "FIB 1 - Dead Man Walking",
    },
    {
    	ids: ["MISS_SWITCH_fbi2"],
    	name: "FIB 2 - Three's Company",
    },
    {
    	ids: ["MISS_SWITCH_fbi3"],
    	name: "FIB 3 - By The Book",
    },
    {
    	ids: ["MISS_SWITCH_fbi4"],
    	name: "FIB 4",
    },
    {
    	ids: ["MISS_SWITCH_fbi5A"],
    	name: "FIB 5A",
    },
    {
    	ids: ["MISS_SWITCH_fbi5B"],
    	name: "FIB 5B",
    },
    {
    	ids: ["MISS_SWITCH_finale_heist1"],
    	name: "Finale Heist 1",
    },
    {
    	ids: ["MISS_SWITCH_finale_heist2A"],
    	name: "Finale Heist 2A",
    },
    {
    	ids: ["MISS_SWITCH_finale_heist2B"],
    	name: "Finale Heist 2B",
    },
    {
    	ids: ["MISS_SWITCH_finale1"],
    	name: "Finale 1 - Kill Trevor",
    },
    {
    	ids: ["MISS_SWITCH_finale2"],
    	name: "Finale 2 - Kill Michael",
    },
    {
    	ids: ["MISS_SWITCH_finale3"],
    	name: "Finale 3 - Save Michael and Trevor",
    },
    {
    	ids: ["MISS_SWITCH_franklin0"],
    	name: "Franklin 0 - Chop",
    },
    {
    	ids: ["MISS_SWITCH_franklin1"],
    	name: "Franklin 1 - Ghetto Safari",
    },
    {
    	ids: ["MISS_SWITCH_franklin2"],
    	name: "Franklin 2 - Lamar Down",
    },
    {
    	ids: ["MISS_SWITCH_jewelry_heist"],
    	name: "Lamar1 - The Long Stretch",
    },
    {
    	ids: ["MISS_SWITCH_jewelry_setup1"],
    	name: "Jewelry Setup1",
    },
    {
    	ids: ["MISS_SWITCH_lamar1"],
    	name: "Lamar 1",
    },
    {
    	ids: ["MISS_SWITCH_lester1a"],
    	name: "Lester1 - Friend Request A ",
    },
    {
    	ids: ["MISS_SWITCH_lester1b"],
    	name: "Lester1 - Friend Request B",
    },
    {
    	ids: ["MISS_SWITCH_michael1"],
    	name: "Michael 1 - Bury the Hatchet",
    },
    {
    	ids: ["MISS_SWITCH_michael2"],
    	name: "Michael 2 - Fresh Meat",
    },
    {
    	ids: ["MISS_SWITCH_michael3"],
    	name: "Michael 3 - The Wrap Up",
    },
    {
    	ids: ["MISS_SWITCH_nice_house_heist2A"],
    	name: "Nice House Heist 2A",
    },
    {
    	ids: ["MISS_SWITCH_nice_house_heist2B"],
    	name: "Nice House Heist 2B",
    },
    {
    	ids: ["MISS_SWITCH_nice_house_heist2C"],
    	name: "Nice House Heist 2C",
    },
    {
    	ids: ["MISS_SWITCH_nice_house_setup1A"],
    	name: "Nice House Heist Setup 1A",
    },
    {
    	ids: ["MISS_SWITCH_nice_house_setup1B"],
    	name: "Nice House Heist Setup 1B",
    },
    {
    	ids: ["MISS_SWITCH_prologue1"],
    	name: "Prologue",
    },
    {
    	ids: ["MISS_SWITCH_rural_bank_heist"],
    	name: "Rural Bank Heist",
    },
    {
    	ids: ["MISS_SWITCH_rural_bank_setup"],
    	name: "Rural Bank Setup",
    },
    {
    	ids: ["MISS_SWITCH_solomon1"],
    	name: "Solomon 1 - Mr. Richards",
    },
    {
    	ids: ["MISS_SWITCH_solomon2"],
    	name: "Solomon 2 - The Ballad of Rocco",
    },
    {
    	ids: ["MISS_SWITCH_solomon3"],
    	name: "Solomon 3 - Blow Back",
    },
    {
    	ids: ["MISS_SWITCH_solomon4"],
    	name: "Solomon4",
    },
    {
    	ids: ["MISS_SWITCH_solomon5"],
    	name: "Solomon5",
    },
    {
    	ids: ["MISS_SWITCH_trevor1"],
    	name: "Trevor 1 - Mr. Philips",
    },
    {
    	ids: ["MISS_SWITCH_trevor2"],
    	name: "Trevor 2 - Nervous Ron",
    },
    {
    	ids: ["MISS_SWITCH_trevor3"],
    	name: "Trevor 3 - Friends Reunited",
    },
    {
    	ids: ["MISS_SWITCH_trevor4"],
    	name: "Trevor 4",
    },
    {
    	ids: ["MISS_SWITCH_trevor5"],
    	name: "Trevor5",
    },
    {
    	ids: ["MISS_SWITCH_trevor6"],
    	name: "Trevor6",
    },
    */
 // New List b*#1598548
    {
    	ids: ["MISS_SWITCH_family3"],
    	name: "Family 3 - Marriage Counselling",
    },
    {
    	ids: ["MISS_SWITCH_fbi2"],
    	name: "FIB 2 - Three's Company",
    },
    {
    	ids: ["MISS_SWITCH_fbi3"],
    	name: "FIB 3 - By The Book",
    },
    {
    	ids: ["MISS_SWITCH_fbi4"],
    	name: "FIB 4",
    },
    {
    	ids: ["MISS_SWITCH_franklin1"],
    	name: "Franklin 1 - Ghetto Safari",
    },
    {
    	ids: ["MISS_SWITCH_franklin2"],
    	name: "Franklin 2 - Lamar Down",
    },
    {
    	ids: ["MISS_SWITCH_exile3"],
    	name: "Exile 3 - Derailed",
    },
    {
    	ids: ["MISS_SWITCH_michael2"],
    	name: "Michael 2 - Fresh Meat",
    },
    {
    	ids: ["MISS_SWITCH_michael3"],
    	name: "Michael 3 - The Wrap Up",
    },
    {
    	ids: ["MISS_SWITCH_finale3"],
    	name: "Finale 3 - Save Michael and Trevor",
    },
    
    // 
    {
    	ids: ["MISS_SWITCH_docks_heistA"],
    	name: "The Port of LS Heist 2A",
    },
    {
    	ids: ["MISS_SWITCH_docks_heistB"],
    	name: "The Port of LS Heist 2B",
    },
    {
    	ids: ["MISS_SWITCH_rural_bank_heist"],
    	name: "The Paleto Score 2A",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist3A"],
    	name: "The Agency Heist 3A Finale - Plant Bombs",
    },
    {
    	ids: ["MISS_SWITCH_agency_heist3B"],
    	name: "The Agency Heist 3B Finale - Steal FIB Docs (Helicopter)",
    },
    {
    	ids: ["MISS_SWITCH_finale_heist2A"],
    	name: "The Big Score 2A",
    },
    {
    	ids: ["MISS_SWITCH_finale_heist2B"],
    	name: "The Big Score 2B",
    },
];

//console.log(orderedResults.length);

var reportOptions = {
		
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	processFunction: convertToDict,
	
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: orderedResults.map(function(d) {return d.ids.join()}).join(),
    },
	],
	
	
	elementId: "line-area-chart",
	backgroundColour: "#333333",
	lineColour: "#ffffff",
	textColour: "#ffffff",
	gridColour: "#666666",
			
	name: function(d) { return d.label; },

	fullName: function(d, user) { return ((user) ? this.name(d) + "<br />(" + user + ")" : this.name(d)); },
	
	value: function(d) { return Number(d.Value); },
	
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 80},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 170, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : "times", colour: "#ffffff", hasBrackets: true},
	// keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div><br /><br />"
				+ "<table>"
					+ "<tr><td>Times Switched :</td><td class='right'>" + this.value(d) + "</td></tr>"
				+ "</table>";

		return content;
	},
		              
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = orderedResults.map(function(d) {
														d["types"] = d["ids"];
														d["singleMetric"] = true;
														return d;});

function convertToDict(array) {
	var dict = {};
	array.map(function(d) {

		var username = (usersDict[d.GamerHandle]) ? (usersDict[d.GamerHandle]) : d.GamerHandle;

		var newData = [];
		if (orderedResults) { // If the results need to ne ordered
			$.each(orderedResults, function(i, orderedItem) { // loop over the order list
				var resultExists; // flag for empty ordered positions
				$.each(d.Data, function(j, item) { // loop over the results and arrange them
					if (orderedItem.ids.indexOf(item.StatName) != -1) {
						item["label"] = orderedItem.name;
						newData.push(item);
						resultExists = true;
						return; // exit the inner j loop
					}
				});
				if (!resultExists) {
					newData.push({"Cash": 0, "StatName": orderedItem.name, "label" : orderedItem.name});
				}
			});
		}
		else
			newData = d.Data;
		
		//console.log(newData);
		dict[username] = newData;
	});
	
	return dict;
}
