var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "TIME_DRIVING_CAR",
    	title: "Time driving cars",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "DIST_CAR",
    	title: "Distance traveled in cars",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_BIKE",
    	title: "Time riding motorcycles",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_BIKE",
    	title: "Distance traveled on motorcycles",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_HELI",
    	title: "Time flying helicopters",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_HELI",
    	title: "Distance traveled in helicopters",
    	getValue: getCharDistance, 
    },
    {
    	name: "TIME_DRIVING_PLANE",
    	title: "Time piloting planes",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_PLANE",
    	title: "Distance traveled in planes",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_BOAT",
    	title: "Time piloting boats",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_BOAT",
    	title: "Distance traveled in boats",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_QUADBIKE",
    	title: "Time riding ATVs",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_QUADBIKE",
    	title: "Distance traveled on ATVs",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_BICYCLE",
    	title: "Time riding bicycles",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_BICYCLE",
    	title: "Distance traveled on bicycles",
    	getValue: getCharDistance,
    },
    {
    	name: "TIME_DRIVING_SUBMARINE",
    	title: "Time piloting submarines",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "DIST_SUBMARINE",
    	title: "Distance traveled in submarines",
    	getValue: getCharDistance,
    },
    {
    	name: "DIST_AS_PASSENGER_TRAIN",
    	title: "Distance traveled as train passenger",
    	getValue: getCharDistance,
    },
    {
    	name: "DIST_AS_PASSENGER_TAXI",
    	title: "Distance traveled as taxi passenger",
    	getValue: getCharDistance,
    },
    {
    	name: "FASTEST_SPEED",
    	title: "Fastest speed reached in a road vehicle",
    	getValue: getCharSpeed,
    },
    {
    	name: "TOP_SPEED_CAR",
    	title: "Fastest car driven",
    	getValue: vehicleHashCharValue,
    },
    {
    	name: "LONGEST_STOPPIE_DIST",
    	title: "Furthest stoppie distance",
    	getValue: getCharDistance,
    },
    {
    	name: "LONGEST_WHEELIE_DIST",
    	title: "Furthest wheelie distance",
    	getValue: getCharDistance,
    },
    {
    	name: "LONGEST_DRIVE_NOCRASH",
    	title: "Longest time driving without crashing",
    	getValue: getCharTimeValue, // this is millisecs
    },
    {
    	name: "NUMBER_CRASHES_CARS",
    	title: "Number of car crashes",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_CRASHES_BIKES",
    	title: "Number of motorcycle crashes",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_CRASHES_QUADBIKES",
    	title: "Number of ATV crashes",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_CRASHES_BICYCLES",
    	title: "Number of bicycle crashes",
    	getValue: getCharValue,
    },
    {
    	name: "BAILED_FROM_VEHICLE",
    	title: "Bailed from a moving vehicle",
    	getValue: getCharValue,
    },
    {
    	name: "AIR_LAUNCHES_OVER_5M",
    	title: "Air launches over 5 meters",
    	getValue: getCharValue,
    },
    {
    	name: "AIR_LAUNCHES_OVER_40M",
    	title: "Air launches over 100 meters",
    	getValue: getCharValue,
    },
    {
    	name: "FARTHEST_JUMP_DIST",
    	title: "Furthest vehicle jump",
    	getValue: getCharDistance,
    },
    {
    	name: "HIGHEST_JUMP_REACHED",
    	title: "Highest vehicle jump",
    	getValue: getCharDistance,
    },
    {
    	name: "MOST_FLIPS_IN_ONE_JUMP",
    	title: "Most flips in one vehicle jump",
    	getValue: getCharValue,
    },
    {
    	name: "MOST_SPINS_IN_ONE_JUMP",
    	title: "Most spins in one vehicle jump",
    	getValue: getCharValue,
    },
    {
    	name: "USJS_FOUND",
    	title: "Unique Stunt Jumps found",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "USJS_COMPLETED",
    	title: "Unique Stunt Jumps completed",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "THROWNTHROUGH_WINDSCREEN",
    	title: "Launches through a windshield",
    	getValue: getCharValue,
    },
    {
    	name: "NUMBER_NEAR_MISS",
    	title: "Near misses",
    	getValue: getCharValue,
    },
    {
    	name: "CARS_COPS_EXPLODED",
    	title: "Cop cars blown up",
    	getValue: getCharValue,
    },
    {
    	name: "CARS_EXPLODED",
    	title: "Cars blown up",
    	getValue: getCharValue,
    },  
    {
    	name: "BIKES_EXPLODED",
    	title: "Motorcycles blown up",
    	getValue: getCharValue,
    },
    {
    	name: "HELIS_EXPLODED",
    	title: "Helicopters blown up",
    	getValue: getCharValue,
    },
    {
    	name: "PLANES_EXPLODED",
    	title: "Planes blown up",
    	getValue: getCharValue,
    },
    {
    	name: "BOATS_EXPLODED",
    	title: "Boats blown up",
    	getValue: getCharValue,
    },
    {
    	name: "QUADBIKE_EXPLODED",
    	title: "ATVs blown up",
    	getValue: getCharValue,
    },
    {
    	name:  "NO_CARS_REPAIR",
    	title: "Vehicles repaired",
    	getValue: getCharValue,
    },
    {
    	name: "VEHICLES_SPRAYED",
    	title: "Vehicles resprayed",
    	getValue: getCharValue,
    },
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Vehicles",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
		value: getStatNames,
	},
	],

	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],
	
	reportArrayItems: profileStatList.map(
			function(d) {
				d["requestSubString"] = "_";
				
				return d;
			}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														d.name = d.title;
														return d;});

