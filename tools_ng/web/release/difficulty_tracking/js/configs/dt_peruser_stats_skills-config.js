/* requires dt_ingame_stats_generic.js*/

var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "SPECIAL_ABILITY_UNLOCKED",
    	title: "Special ",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "SPECIAL_ABILITY_ACTIVE_TIME",
    	title: "Special Duration",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    { // Extra
    	name: "SPECIAL_ABILITY_ACTIVE_NUM",
    	title: "Special times used",
    	getValue: getCharValue,
    },
    {
    	name: "STAMINA",
    	title: "Stamina",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "STAMINA_MAXED",
    	title: "Stamina maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "SHOOTING_ABILITY",
    	title: "Shooting",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "SHOOTING_ABILITY_MAXED",
    	title: "Shooting maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "STRENGTH",
    	title: "Strength",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "STRENGTH_MAXED",
    	title: "Strength maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "STEALTH_ABILITY",
    	title: "Stealth",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "STEALTH_ABILITY_MAXED",
    	title: "Stealth maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "FLYING_ABILITY",
    	title: "Flying",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "FLYING_ABILITY_MAXED",
    	title: "Flying maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "WHEELIE_ABILITY",
    	title: "Driving",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "WHEELIE_ABILITY_MAXED",
    	title: "Driving maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "LUNG_CAPACITY",
    	title: "Lung Capacity",
    	getValue: getCharValuePerCent,
    },
    { // Extra
    	name: "LUNG_CAPACITY_MAXED",
    	title: "Lung Capacity maxed time",
    	getValue: getCharTimeValue, // this is millisecs 
    },

];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Skills",
	columnText: "User: ",
	
	processFunction: groupLatestProfileStats,
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
		
	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],	
	
	reportArrayItems: profileStatList.map(
				function(d) {
					d["requestSubString"] = "_";
					d["types"] = [d.name];
					return d;
				}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														d.name = d.title;
														return d;});

