var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	false, // build
		false, // platform
		false, // user
	],
};

var platformNames = getPlatforms();

var resultNames = {
	"0" : "Passed",
	"1" : "Failed",
	"2" : "Cancelled",
	"3" : "Over",
	"4" : "Unknown",
};

var reportOptions = {
	restEndpoint: config.playthroughsSummary,
	restEndpointAsync: config.playthroughsSummaryAsync,
	
	legendText: ""
				+ "User Report"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "User Sessions",

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {return d},
	reportArrayItems: [
	    {title: "Username", getValue: function(d) {return d.UserName;} },
	    {title: "Total Sessions", getValue: function(d) {return d.SessionCount} },
	    // TimeSpentPlaying  is in millisecs
	    {title: "Total Time", getValue: function(d) {return formatSecs(d.TimeSpentPlaying/1000);} }, 
	    {title: "Total Missions", getValue: function(d) {return (d.TotalMissionAttempts);} },
	    {title: "Unique Missions", getValue: function(d) {return (d.UniqueMissionAttempts);} },
	    {title: "Total Checkpoints", getValue: function(d) {return (d.TotalCheckpointAttempts);} },
	    {title: "Unique Checkpoints", getValue: function(d) {return (d.UniqueCheckpointAttempts);} },
	],
	
	/* Groups */
	groups: [
	    {title: "Summary", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: true,
	// if the more info need to come via an extra rest call
	reportArrayItemMoreInfoRest: {
		restEndpoint: config.playthroughsBreakdown,
		restEndpointAsync: config.playthroughsBreakdownAsync,
		extraParamName: "User",
	},
	
	// No nested array data
	getReportMoreInfoArray: function(d) {return d},
	// Generate one table for each result
	hasMoreInfoMultipleTables: true,
	getMoreInfoKey: function(d) {
		return "Session :: " + d.FriendlyName 
			+ " - " + parseJsonDate(d.Start).toLocaleString()
			+ " (" 
			+ formatSecs(d.TimeSpentPlaying/1000) // milliseconds
			+ ") :: " 
			+ d.BuildIdentifier + " :: " 
			+ platformNames[d.Platform].Name;
	},
	// get the extra info data from the array
	getMoreInfoValues: function(d) {return d.MissionsAttempted},
	reportMoreInfoArrayItems: [
	    {title: "Mission Name", getValue: function(d) {return d.MissionName;} },
	    {title: "Total Checkpoints", getValue: function(d) {return d.CheckpointCount;} },
	    {title: "Checkpoint Attempts", getValue: function(d) {return d.CheckpointAttempts.length;} },
	    //{title: "Last Checkpoint", getValue: function(d) {
	    //	return (d.CheckpointAttempts.length) ? d.CheckpointAttempts.slice(-1)[0].CheckpointIndex : "";} 
	    //},
	    {title: "Result", getValue: function(d) {return resultNames[d.Result];} },
	    //{title: "Start", getValue: function(d) {return parseJsonDate(d.Start).toLocaleString();} },
	   // {title: "End", getValue: function(d) {return (d.End) ? parseJsonDate(d.End).toLocaleString() : "";} },
	    // TimeToComplete is in millisecs
	    {title: "Duration", getValue: function(d) {return formatSecs(d.TimeToComplete/1000);} },
	    //{title: "Comment", getValue: function(d) {return (d.Comment) ? (d.Comment) : "";} },
   	],	
   	
   	/* Second level breakdown */
   	
   	hasReportArraySubItemMoreInfo: true,
	
	// No nested array data
	getReportSubItemMoreInfoArray: function(d) {return d},
	// Generate one table for all the results
	hasSubItemMoreInfoMultipleTables: false,
	getSubItemMoreInfoKey: function(d) {
		return "Checkpoint Attempts";
	},
	// get the extra info data from the array
	getSubItemMoreInfoValues: function(d) {return d.CheckpointAttempts},
	reportSubItemMoreInfoArrayItems: [
	    //{title: "Index", getValue: function(d) {return d.CheckpointIndex;} },
	    {title: "Name", getValue: function(d) {return d.CheckpointName;} },
	    {title: "Start", getValue: function(d) {return parseJsonDate(d.Start).toLocaleString();} },
	    {title: "End", getValue: function(d) {return (d.End) ? parseJsonDate(d.End).toLocaleString() : "";} },
	    // TimeToComplete is in millisecs
	    {title: "Duration", getValue: function(d) {return formatSecs(d.TimeToComplete/1000);} },
	    {title: "Comment", getValue: function(d) {return (d.Comment) ? (d.Comment) : "";} },
   	],
	
};

