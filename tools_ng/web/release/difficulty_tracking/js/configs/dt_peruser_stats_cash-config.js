var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
	  	true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "TOTAL_CASH_EARNED",
    	title: "Total cash earned",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_TOTAL_SPENT",
    	title: "Total cash spent",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_MADE_FROM_MISSIONS",
    	title: "Cash earned from Missions",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_MADE_FROM_RANDOM_PEDS",
    	title: "Cash earned from random peds",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_PICKED_UP_ON_STREET",
    	title: "Cash picked up",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_IN_COP_BRIBES",
    	title: "Cash spent on bail",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_ON_HEALTHCARE",
    	title: "Cash spent on healthcare",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_CAR_MODS",
    	title: "Cash spent on car modes",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_IN_CLOTHES",
    	title: "Cash spent on cloths",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_ON_TATTOOS",
    	title: "Cash spent on tattoos",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_ON_HAIRDOS",
    	title: "Cash spent on hairstyles",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_IN_STRIP_CLUBS",
    	title: "Cash spent in Strip Clubs",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_ON_TAXIS",
    	title: "Cash spent on cabs",
    	getValue: getCharCashValue,
    },
    {
    	name: "MONEY_SPENT_PROPERTY",
    	title: "Cash spent on property",
    	getValue: getCharCashValue,
    },
    { // Extra
    	name: "MONEY_SPENT_IN_BUYING_GUNS",
    	title: "Cash spent on guns",
    	getValue: getCharCashValue,
    },
    { // Extra
    	name: "RC_WALLETS_RECOVERED",
    	title: "Wallets recovered",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "RC_WALLETS_RETURNED",
    	title: "Wallet returned",
    	getValue: getValue,
    	singleMetric: true,
    },
    
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Cash",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],

	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],	
	
	reportArrayItems: profileStatList.map(
				function(d) {
					d["requestSubString"] = "_";
					
					return d;
				}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														d.name = d.title;
														return d;});
