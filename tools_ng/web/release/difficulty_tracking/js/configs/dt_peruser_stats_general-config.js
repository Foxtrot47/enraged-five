var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
	  	true, // platform
		false, // user
	],
};

var profileStatList = [
    {
    	name: "TOTAL_PLAYING_TIME",
    	title: "Time played as character",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "MISSIONS_PASSED",
    	title: "Missions passed",
    	getValue: getCharValue,
    },
    {
    	name: "TIMES_MISSION_SKIPPED",
    	title: "Times Missions skipped",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "SP_LAST_MISSION_NAME",
    	title: "Last Mission completed",
    	getValue: missionHashValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_HIDDEN_PACKAGES_0",
    	title: "Letter scraps",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_HIDDEN_PACKAGES_1",
    	title: "Spaceship parts",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_HIDDEN_PACKAGES_2",
    	title: "Epsilon tracts",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_HIDDEN_PACKAGES_3",
    	title: "Nuclear waste",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "NUM_HIDDEN_PACKAGES_4",
    	title: "Submarine pieces",
    	getValue: getValue,
    	singleMetric: true,
    },
    {
    	name: "MANUAL_SAVED",
    	title: "Times saved",
    	getValue: getCharValue,
    },
    {
    	name: "TIMES_CHEATED",
    	title: "Times cheated",
    	getValue: getCharValue,
    },
    {
    	name: "DEATHS",
    	title: "Total deaths",
    	getValue: getCharValue,
    },
    {  // Extra
    	name: "DIED_IN_MISSION",
    	title: "Deaths in mission",
    	getValue: getCharValue,
    },
    {
    	name: "DIED_IN_EXPLOSION",
    	title: "Deaths by explosion",
    	getValue: getCharValue,
    },
    {
    	name: "DIED_IN_FALL",
    	title: "Deaths by falling",
    	getValue: getCharValue,
    },
    {
    	name: "DIED_IN_FIRE",
    	title: "Deaths by fire",
    	getValue: getCharValue,
    },
    {
    	name: "DIED_IN_ROAD",
    	title: "Deaths by traffic",
    	getValue: getCharValue,
    },
    {
    	name: "DIED_IN_DROWNING",
    	title: "Deaths by drowning",
    	getValue: getCharValue,
    },
    {
    	name: "TIME_SWIMMING",
    	title: "Time swimming",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "DIST_SWIMMING",
    	title: "Distance traveled swimming",
    	getValue: getCharDistance, // this is meters 
    },
    {
    	name: "TIME_UNDERWATER",
    	title: "Time underwater",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "TIME_WALKING",
    	title: "Time walking",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "DIST_WALKING",
    	title: "Distance traveled walking",
    	getValue: getCharDistance, // this is meters
    },
    {
    	name: "DIST_WALK_ST",
    	title: "Distance walking in Stealth",
    	getValue: getCharDistance, // this is meters
    },
    {
    	name: "DIST_RUNNING",
    	title: "Distance traveled running",
    	getValue: getCharDistance, // this is meters
    },
    {
    	name: "LONGEST_SURVIVED_FREEFALL",
    	title: "Furthest free-fall distance survived",
    	getValue: getCharDistance, // this is meters
    },
    {
    	name: "TIME_IN_COVER",
    	title: "Time in cover",
    	getValue: getCharTimeValue, // this is millisecs 
    },
    {
    	name: "SP_MOST_FAVORITE_STATION",
    	title: "Favourite Station",
    	getValue: radioHashValue,
    	singleMetric: true,
    },
    {
    	name: "NO_PHOTOS_TAKEN",
    	title: "Photos taken",
    	getValue: getCharValue,
    },
    {
    	name: "LAP_DANCED_BOUGHT",
    	title: "Private dances",
    	getValue: getCharValue,
    },

    { // Extra
    	name: "TOTAL_SWITCHES",
    	title: "Total Switches",
    	getValue: getCharValue,
    },
    { // Extra
    	name: "TIME_SPENT_ON_STOCKMARKET",
    	title: "Time in Stockmarket",
    	getValue: getTimeValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_BEER_DRUNK",
    	title: "Beers drunk in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_BONG_SMOKED",
    	title: "Bong hits taken in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_SOFA_USED",
    	title: "Times sofa used in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_TV_WATCHED",
    	title: "Times TV watched in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_SOFA_SMOKED",
    	title: "Times smoked at sofa in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_WINE_DRANK",
    	title: "Glasses of wine drunk in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_WHEATGRASS",
    	title: "Wheatgrass drunk in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_WHISKEY",
    	title: "Shots of whiskey drunk in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_GAS_HUFFED",
    	title: "Times gas huffed in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_GAS_HUFFED",
    	title: "Times gas huffed in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "NUM_SH_MR_JAM_USED",
    	title: "Times used Mr Raspberry Jam in safehouse",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_CHOP_WALK_DONE",
    	title: "Times Chop Walked",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "CHOP_APP_USED",
    	title: "Times Chop app used",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "CAR_MOD_APP_USED",
    	title: "Times Car Mod app used",
    	getValue: getValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_HEIST_CHOSE_BIGS_TRAFFIC",
    	title: "Big Score approach",
    	getValue: bigScoreValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_HEIST_CHOSE_BUREAU_FIRECREW",
    	title: "Bureau Raid approach",
    	getValue: bureauRaidValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_HEIST_CHOSE_DOCKS_SINK_SHIP",
    	title: "Port of LS heist approach",
    	getValue: lsHeistValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_HEIST_CHOSE_JEWEL_STEALTH",
    	title: "Jewel Store Job approach",
    	getValue: jewelStore,
    	singleMetric: true,
    },
    { // Extra
    	name: "SP_FINAL_DECISION",
    	title: "Final Decision",
    	getValue: finalDesicionValue,
    	singleMetric: true,
    },

];

var cheatsStatList = [
	{
    	name: "CHEATS",
    	title: "Cheats Used",
    	getValue: getValue,
	}
];

var medalsMGStatList = [
	{
		name: "MG_GOLD_MEDALS",
		title: "Gold Medals in Mini Games",
		getValue: getNumValue,
	}
];

var clothsChangedStatList = [
	{
		name: "NUM_CLOTHS_CHANGED",
		title: "Times changed cloths",
		getValue: getNumValue,
	},
	{
		name: "NUM_PROPS_CHANGED",
		title: "Times changed accessories",
		getValue: getNumValue,
	}
];

var wantedFiveStarsStatList = [
	{
		name: "NUM_WANTED_FIVE_STAR",
		title: "Times gained 5 stars wanted level",
		getValue: getNumValue,
	},
];

var friendActivitiesStatList = [
	{
		name: "NUM_FRIENDS_ACTIVITIES",
		title: "Times went on friend activities",
		getValue: getNumValue,
	},
];

var reportOptions = {
	/*
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	*/
	multipleRequests: generateGeneralUserEndpoints,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "General",
	columnText: "User: ",

	processFunction: function(d) {
		return groupMultiplePerUserStats(d, endpointProcessFunctions);
	},
	
	/*
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
	*/

	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],
		
	reportArrayItems: profileStatList.map(
		function(d) {
			d["requestSubString"] = "_";
				
			return d;
		})
		.concat(cheatsStatList)
		.concat(medalsMGStatList)
		.concat(clothsChangedStatList)
		.concat(wantedFiveStarsStatList)
		.concat(friendActivitiesStatList),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														d.name = d.title;
														return d;});

function generateGeneralUserEndpoints(pValues) {
	// store the obj to local for processing the results
	requestPValues = pValues;
	
	var pValuesArray = [];
	
	pValues.Pairs["StatNames"] = getStatNames();
	
	var endpointObjects = [
	{
		restUrl: config.restHost + config.profileStatsPerUser,
		restAsyncUrl: config.restHost + config.profileStatsPerUserAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userSPCheats,
		restAsyncUrl: config.restHost + config.userSPCheatsAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userMGMedals,
		restAsyncUrl: config.restHost + config.userMGMedalsAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userClothChanges,
		restAsyncUrl: config.restHost + config.userClothChangesAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userPropChanges,
		restAsyncUrl: config.restHost + config.userPropChangesAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userWantedlevels,
		restAsyncUrl: config.restHost + config.userWantedlevelsAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	{
		restUrl: config.restHost + config.userFriendActivity,
		restAsyncUrl: config.restHost + config.userFriendActivityAsync + pValues.ForceUrlSuffix,
		pValues: [pValues],
	},
	];
	
	return endpointObjects;
}

// A process function for each endpoint, the length must be identical to endpointObjects
var endpointProcessFunctions = 
[
 	groupLatestProfileStats,
 	groupCheatsPerUser,
 	groupMGMedalsPerUser,
 	groupClothsPerUser,
 	groupPropsPerUser,
 	groupWantedlevelsPerUser,
 	groupfriendActivitiesPerUser,
];

function groupCheatsPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][cheatsStatList[0].name] = d.Data.map(
			function(s) { return s.CheatName + " (" + s.TimesUsed + " times)"; }
		)
		.join(",<br />");
	});

	return gamers;
}

function groupMGMedalsPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][medalsMGStatList[0].name] = d.Data.map(
			function(s) { return s.GoldMedals + " (" + s.MiniGameName + ")"; }
		)
		.join(",<br />");
	});

	return gamers;
}

function groupClothsPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][clothsChangedStatList[0].name] = d.Data;
	});

	return gamers;
}

function groupPropsPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][clothsChangedStatList[1].name] = d.Data;
	});

	return gamers;
}

function groupWantedlevelsPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][wantedFiveStarsStatList[0].name] = d.Data.map(
				function(s) { if (s.Level == 5) return s.TimesAchieved; }
			).join("");
	});

	return gamers;
}

function groupfriendActivitiesPerUser(data) {
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
			
		gamers[gamerTag][friendActivitiesStatList[0].name] = d.Data.reduce(
				function(a, b) {
					return a + (b.AbortedCount + b.FailedCount + b.SuccessCount);
				}, 
				0
			);
	});

	return gamers;
}