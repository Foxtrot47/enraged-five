var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var fixed2 = function(d) {return d.toFixed(2)};

var statsList = []; // will be populated by process function

var reportOptions = {
	restEndpoint: config.userMissionRatings,
	restEndpointAsync: config.userMissionRatingsAsync,
		
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Mission Ratings",
	columnText: "User: ",
	
	storePValues: function(pValues) {
		// Store the pValues locally for ordering the results
		requestPValues = pValues;
	},

	processFunction: groupUserResults,
	
	reportArrayItems: statsList,
	
	reGenerateReportArrayItems: generateReportArrayItems,
};

function generateReportArrayItems(reportData) {
	var stats = {};
	
	$.each(reportData, function(key, value) {
		$.each(value, function(j, d) {
			if (!stats.hasOwnProperty(d.MissionId))
				stats[d.MissionId] = {
					name: d.MissionId,
					title: d.MissionName,
				};
		});
	});
		
	statsList = Object.keys(stats).sort(function(a, b) 
										{return ((stats[a].title > stats[b].title) ? 1 : -1); })
								  .map(function(d) {
			var stat = stats[d];
			stat["getValue"] = function(d) {return getMissionRating(d, this.name)}; 
			return stat;
		});
	
	//console.log(statsList);
	return statsList;
}

function groupUserResults(data) {	
	var gamers = populateOrderedGamers();

	$.each(data, function(i, d) {
		gamers[d.GamerHandle] = d.Data;
	});
	
	return gamers;
}

function getMissionRating(data, missionId) { 
	var rating = "-";
	
	var re = new RegExp(missionId);
	
	$.each(data, function(i, d) {
		
		if (re.test(d.MissionId)) {
			rating = fixed2(Number(d.Rating));
		}
		else
			return;
	});
	
	return rating;
}


