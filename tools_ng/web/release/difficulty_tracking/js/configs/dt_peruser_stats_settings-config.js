var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
	  	true, // platform
		false, // user
	],
};

var profileStatList = [
    { // Extra
    	name: "_PROFILE_SETTING_1",
    	title: "Axis Invertion",
    	getValue: getOnOffValue,
    	singleMetric: true,
    },
    { // Extra
    	name: "_PROFILE_SETTING_800",
    	title: "Phone Alerts",
    	getValue: getOnOffValue,
    	singleMetric: true,
    },
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Settings",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],

	reportArrayItems: profileStatList.map(
			function(d) {
				d["requestSubString"] = "_";
					
				return d;
			}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														d.name = d.title;
														return d;});
