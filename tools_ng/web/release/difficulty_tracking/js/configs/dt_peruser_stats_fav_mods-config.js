var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var fixed = function(d) {return d.toFixed(3);};
var usersDict = getPlaythroughUsersAsDict();

function generateUserEndpoints(pValues) {
	var pValuesArray = [];
	
	$.each(pValues.Pairs["GamerHandlePlatformPairs"].split(","), function(i, gamer) {
		pValues.Pairs["BuildIdentifiers"] = null;
		pValues.Pairs["PlatformNames"] = null;
		var copiedPvalues = $.extend(true, {}, pValues);
		copiedPvalues.Pairs["GamerHandlePlatformPairs"] = gamer;
		pValuesArray.push(copiedPvalues);
	});
	
	var endpointObject = {
		restUrl: config.restHost + config.weaponModPurchases,
		restAsyncUrl: config.restHost + config.weaponModPurchasesAsync + pValues.ForceUrlSuffix,
		pValues: pValuesArray,
	};
	
	return [endpointObject];
}

var statList = [
    {
    	name: "WEAPON_PISTOL",
    	title: "Pistol",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_COMBATPISTOL",
    	title: "Combat Pistol",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_APPISTOL",
    	title: "AP Pistol",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_MICROSMG",
    	title: "Micro SMG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_SMG",
    	title: "SMG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ASSAULTRIFLE",
    	title: "Assault Rifle",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_CARBINERIFLE",
    	title: "Carbine Rifle",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ADVANCEDRIFLE",
    	title: "Advanced Rifle",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_MG",
    	title: "MG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_COMBATMG",
    	title: "Combat  MG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_PUMPSHOTGUN",
    	title: "Pump Shotgun",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ASSAULTSHOTGUN",
    	title: "Assault Shotgun",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_SNIPERRIFLE",
    	title: "Sniper Rifle",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_HEAVYSNIPER",
    	title: "Heavy Sniper",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_GRENADELAUNCHER",
    	title: "Grenade Launcher",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_MINIGUN",
    	title: "Minigun",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ASSAULTSMG",
    	title: "Assault SMG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_BULLPUPSHOTGUN",
    	title: "Bullpup Shotgun",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_PISTOL50",
    	title: "Pistol .50",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ASSAULTSNIPER",
    	title: "ASSAULTSNIPER",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_HEAVYRIFLE",
    	title: "HEAVYRIFLE",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_ASSAULTMG",
    	title: "ASSAULTMG",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
    {
    	name: "WEAPON_PROGRAMMABLEAR",
    	title: "PROGRAMMABLEAR",
    	getValue: function(d) {return getMaxPurchasedModForWeapon(d, this.name)}, 
    },
];

var reportOptions = {
	//restEndpoint: config.vehiclesStats,
	//restEndpointAsync: config.vehiclesStatsAsync,
	multipleRequests: generateUserEndpoints,
		
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Favourite Vehicles",
	columnText: "User: ",

	processFunction: groupUserResults,
	
	reportArrayItems: statList,
};

function groupUserResults(data) {	
	var gamers = {};
	//console.log(data[0]);
	
	$.each(data[0], function(i, d) {
		var gamerTag = JSON.parse(d.reqData)[1].Value.split("|")[0].split("<")[1];
		gamerTag = (usersDict[gamerTag]) ? usersDict[gamerTag] : gamerTag;
		
		gamers[(gamerTag) ? gamerTag : i] = d.response;
	});
	
	return gamers;
}

function getMaxPurchasedModForWeapon(data, category) {
	var favMod = "-";
	
	$.each(data, function(i, d) {
		if (d.WeaponName == category) {
			var sortedArray = d.WeaponMods.sort(
					function(a, b) {
						return (a.TimesPurchased > b.TimesPurchased) ? -1 : 1 
					}); 
			favMod = (sortedArray.length > 0 && sortedArray[0].TimesPurchased) ? 
					((sortedArray[0].WeaponModFriendlyName) ? sortedArray[0].WeaponModFriendlyName : sortedArray[0].WeaponModName)  
						+ "<br/>(" +  sortedArray[0].TimesPurchased + " times)" 
					: "-";
		}
		else 
			return;
	});
	
	return favMod;
}
