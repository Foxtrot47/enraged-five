// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profileStatList = [
{
	types: [
	"_PROFILE_SETTING_800", // FEED_PHONE = 800,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Phone Alerts",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_801", // FEED_STATS= 801,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Stats Alerts",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_802", // FEED_CREW = 802,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Crew Updates",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_803", // FEED_FRIENDS = 803,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Friend Updates",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_804", // FEED_SOCIAL = 804,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Social Club",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
{
	types: [
	"_PROFILE_SETTING_805", // FEED_STORE = 805,
	],
	altNames:[
	   "Off", // "MO_OFF",
	   "On", // "MO_ON",
	],
	requestSubString: "_",
	name: "Store",
	//description: "No of users for each front end option",
	label: "No of Users",
	units: "users",
	bucketSize: 1,
	
	singleMetric: true, // One metric for all
},
];

var reportOptions = {	
	restEndpoint: config.profileStatsCombined,
	restEndpointAsync: config.profileStatsCombinedAsync,
				
	availableCharts: profileStatList.map(function(d) {
		d["hideStartDate"] = true;
		return d;
	}),
}
