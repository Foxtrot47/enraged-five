var navigationTitle = "Statistics";
var navigationItems = [{
		title: "Report Presets",
		href: "index.html",
		subItems: [],
	},

	// General Game
	{
		title: "General / Game",
		href: "#",
		subItems: [
		    {
		    	title: "Total Playing Time",
		    	href: "total_playing_time.html",
				config: "js/configs/total_playing_time-config.js",
				profile: true,
		    },
		    {
				title: "Total Players",
				href: "total_players.html",
				config: "js/configs/total_players-config.js",
				profile: false,
			},
		    {
		    	title: "SP: Favourite Character",
		    	href: "favourite_character.html",
				config: "js/configs/favourite_character-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Percent Complete",
		    	href: "percent_complete.html",
				config: "js/configs/percent_complete-config.js",
				profile: true,
		    },
			{
				title: "SP: Choices",
				href: "sp_choices.html",
				config: "js/configs/sp_choices-config.js",
				profile: true,
			},
			{
				title: "SP: Missions Skipped",
				href: "sp_skipped_missions.html",
				config: "js/configs/sp_skipped_missions-config.js",
				profile: true,
			},
			{
				title: "SP: Sleep Mode",
				href: "sp_sleep_mode.html",
				config: "js/configs/sp_sleep_mode-config.js",
				profile: true,
			},
			{
				title: "SP: Chop",
				href: "sp_chop.html",
				config: "js/configs/sp_chop-config.js",
				profile: true,
			},
			{
				title: "SP Vs MP Playing Time",
				href: "single_vs_multi.html",
				config: "js/configs/single_vs_multi-config.js",
				profile: true,
			},
		],
	},
	
	// Cash and XP
	{
		title: "Cash and RP",
		href: "#",
		subItems: [
		     {
		       	   title: "Per User Hourly Report",
		       	   href: "cash_user_hourly_report.html",
		       	   //config: "js/configs/cash__user_hourly_report-config.js",
		     },
		     {
		       	   title: "Top User Hourly Report",
		       	   href: "cash_top_user_hourly_report.html",
		       	   //config: "js/configs/cash_top_user_hourly_report-config.js",
		     },
		     // Page renaming - b*#1641208
		     {
		       	   title: "Average Cash Earned Per Hour", 
		       	   //href: "cash_cash_hourly.html",
		       	   //config: "js/configs/cash_cash_hourly-config.js",
		       	   href: "cash_cash_hourly_tableau.html",
		       	   config: "js/configs/cash_cash_hourly_tableau-config.js",
		       	   tableau: true,
		     },
		     {
		       	   title: "Average RP Earned per Hour",
		       	   //href: "cash_rp_hourly.html",
		       	   //config: "js/configs/cash_rp_hourly-config.js",
		       	   href: "cash_rp_hourly_tableau.html",
		       	   config: "js/configs/cash_rp_hourly_tableau-config.js",
		       	   tableau: true,
		     },
		     {
		       	   title: "Average Cash Earnings, Balance, RP & Rank",
		       	   //href: "cash_total_cash_rp.html",
		       	   //config: "js/configs/cash_total_cash_rp-config.js",
		       	   href: "cash_total_cash_rp_tableau.html",
		       	   config: "js/configs/cash_total_cash_rp_tableau-config.js",
		       	   tableau: true,
		     },
		     {
		       	   title: "Average Cash Earned and Time Passed per Rank",
		       	   href: "cash_total_cash_hour.html",
		       	   config: "js/configs/cash_total_cash_hour-config.js",
		       	   //href: "cash_total_cash_hour_tableau.html",
		       	   //config: "js/configs/cash_total_cash_hour_tableau-config.js",
		       	   //tableau: true,
		     },
		     {
		       	   title: "Transaction History",
		       	   href: "transaction_history.html",
		       	   config: "js/configs/transaction_history-config.js",
		     },
		],
	},
	
	// Expenditure
	{
		title: "Player Expenditure",
		href: "#",
		config: "",
		subItems: [
		    {
				title: "SP: Cash at Flow Stage",
				href: "sp_cash_at_flow_stage.html",
				config: "js/configs/sp_cash_at_flow_stage-config.js",
			},
			{
				title: "SP: Properties",
				href: "sp_properties.html",
				config: "js/configs/sp_properties-config.js",
				profile: true,
			},
			{
				title: "SP: Taxis",
				href: "#",
				subItems: [
				    {
				    	title: "Times Used",
						href: "sp_taxis_times_used.html",
						config: "js/configs/sp_taxis_times_used-config.js",
						profile: true,
				    },
				    {
				    	title: "Amount Spent",
						href: "sp_taxis_amount_spent.html",
						config: "js/configs/sp_taxis_amount_spent-config.js",
						profile: true,
				    },
				],
			},
			{
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " All Expenditure",
		    	href: "purchases_all_expenditure.html",
		    	config: "js/configs/purchases_all_expenditure-config.js",
		    },
			{
		    	title: "All Shop Purchases",
		    	href: "purchases_all_shop.html",
		    	config: "js/configs/purchases_all_shop-config.js",
		    },
		    {
		    	title: "Purchases Per Shop",
		    	href: "purchases_per_shop.html",
		    	config: "js/configs/purchases_per_shop-config.js",
		    },
		    {
		       	title: "Money Purchases",
		       	href: "purchases_money.html",
		       	config: "js/configs/purchases_money-config.js",
		     },
		],
	},
	
	// Expenditure
	{
		title: "Player Earnings",
		href: "#",
		subItems: [
		   {
				title: "Earning Stats",
				href: "earnings.html",
				config: "js/configs/earnings-config.js",
			},
		]
	},
	
	// SP:Character
	{
		title: "SP: Character",
		href: "#",
		subItems: [
		    {
		    	title: "Appearance",
		    	href: "sp_appearance.html",
		    	config: "js/configs/sp_appearance-config.js",
		    	profile: true,
		    },
		    {
		    	title: "Switches: On-mission",
		    	href: "sp_switches_on_mission.html",
		    	config: "js/configs/sp_switches_on_mission-config.js",
		    	profile: true,
		    },
		    {
		    	title: "Switches: Off-mission",
		    	href: "sp_switches_off_mission.html",
		    	config: "js/configs/sp_switches_off_mission-config.js",
		    	profile: true,
		    },
		    {
		       	title: "Special Ability",
		       	href: "sp_special_ability.html",
		       	config: "js/configs/sp_special_ability-config.js",
		       	profile: true,
		     },
		],
	},
	
	{
		title: "SP: Character Stats",
		href: "#",
		subItems: [
		   {
			   title: "Michael",
			   href: "sp_char_stats_michael.html",
			   config: "js/configs/sp_char_stats_michael-config.js",
		   },
		   {
				title: "Trevor",
				href: "sp_char_stats_trevor.html",
				config: "js/configs/sp_char_stats_trevor-config.js",
			},
			{
				title: "Franklin",
				href: "sp_char_stats_franklin.html",
				config: "js/configs/sp_char_stats_franklin-config.js",
			},
			{
				title: "Overall",
				href: "sp_char_stats_overall.html",
				config: "js/configs/sp_char_stats_overall-config.js",
				profile: true,
			},
        ],
	},
	
	// Betting
	{
		title: "Betting",
		href: "#",
		subItems: [
		    {
		    	title: "Amount Bet",
		       	href: "bets_amount.html",
		       	config: "js/configs/bets_amount-config.js",
		    },
		    {
		    	title: "Average Bets",
		       	href: "bets_average.html",
		       	config: "js/configs/bets_average-config.js",
		     },
		],
	},
	
	// Missions / Activities
	{
		title: "Missions / Activities",
		href: "#",
		subItems: [
		    // Freemode
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Tutorial",
		    	href: "freemode_tutorial.html",
		    	config: "js/configs/freemode_tutorial-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Game Modes",
		    	href: "freemode_missions.html",
		    	config: "js/configs/freemode_missions-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Invites",
		    	href: "freemode_invites.html",
		    	config: "js/configs/freemode_invites-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Races Breakdown",
		    	href: "freemode_races.html",
		    	config: "js/configs/freemode_races-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Mission Attempts",
		    	href: "freemode_attempts_graph.html",
		    	config: "js/configs/freemode_attempts_graph-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") 
		    			+ " Mission Attempts To First Success",
		    	href: "freemode_attempts_to_first_success.html",
		    	config: "js/configs/freemode_attempts_to_first_success-config.js",
		    	tableau: true,
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Activities",
		    	href: "freemode_activities.html",
		    	config: "js/configs/freemode_activities-config.js",
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Content",
		    	href: "#",
		    	subItems: [
		    	    {
		    	    	title: "Graphs",
		    	    	href: "freemode_content_graphs.html",
				    	config: "js/configs/freemode_content_graphs-config.js",
		    	    },
		    	    {
		    	    	title: "Table",
		    	    	href: "freemode_content_table.html",
				    	config: "js/configs/freemode_content_table-config.js",
		    	    },
		    	    {
		    	    	title: "Rating",
		    	    	href: "freemode_content_rating.html",
				    	config: "js/configs/freemode_content_rating-config.js",
				    	tableau: true,
		    	    },
		    	],
		    },
		    {
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Job Points Earned", 
				href: "freemode_job_points.html",
				config: "js/configs/freemode_job_points-config.js",
				tableau: true,
			},
			{
		    	title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Jobs By Type", 
				href: "freemode_jobs_by_type.html",
				config: "js/configs/freemode_jobs_by_type-config.js",
				tableau: true,
			},
	    
		    // SP
		    {
		    	title: "SP: Missions",
		    	href: "missions.html",
		    	config: "js/configs/missions-config.js",
		    },
		    // HEISTS
			{
				title: "SP: Heist Approaches",
				href: "sp_heist_approaches.html",
				config: "js/configs/sp_heist_approaches-config.js",
				profile: true,
			},
			{
		    	title: "SP: Oddjobs",
		    	href: "sp_oddjobs.html",
		    	config: "js/configs/sp_oddjobs-config.js",
		    },
		    {
		    	title: "SP: Epsilon",
		    	href: "sp_epsilon.html",
		    	config: "js/configs/sp_epsilon-config.js",
		    	profile: true,
		    },
		    {
		    	title: "SP: Mission Attempts Graphs",
		    	href: "#",
		    	subItems: [
		    	{
		    		title: "Main Missions",
		    		href: "missions_attempts_graphs_main.html",
		    		config: "js/configs/missions_attempts_graphs_main-config.js",
		    	},
		    	{
		    		title: "Oddjobs",
		    		href: "missions_attempts_graphs_oj.html",
		    		config: "js/configs/missions_attempts_graphs_oj-config.js",
		    	},
		    	{
		    		title: "Minigames",
		    		href: "missions_attempts_graphs_mg.html",
		    		config: "js/configs/missions_attempts_graphs_mg-config.js",
		    	},
		    	{
		    		title: "Random Events",
		    		href: "missions_attempts_graphs_re.html",
		    		config: "js/configs/missions_attempts_graphs_re-config.js",
		    	},
		    	{
		    		title: "Random Characters",
		    		href: "missions_attempts_graphs_rc.html",
		    		config: "js/configs/missions_attempts_graphs_rc-config.js",
		    	},
		    	],
		    },
			{
		    	title: "SP: Mission Attempts Table",
		    	href: "missions_attempts_table.html",
		    	config: "js/configs/missions_attempts_table-config.js",
		    },
		    {
		    	title: "SP: Mission Replays",
		    	href: "sp_mission_replays.html",
		    	config: "js/configs/sp_mission_replays-config.js",
		    },
		],
	},
		
	// Vehicles
	{
		title: "Vehicles",
		href: "#",
		subItems: [
		    {
		    	title: "Stats Per Category",
		    	href: "vehicles.html",
		    	config: "js/configs/vehicles-config.js",
		    },
		],
	},
	{
		title: "Combat / Weapons",
		href: "#",
		subItems: [
		    {
		    	title: "Held Time",
		    	href: "weapons_heldtime.html",
		    	config: "js/configs/weapons_heldtime-config.js",
		    	profile: true,
			},
			{
		    	title: "SP: Favourite Weapons",
		    	href: "sp_favourite_weapons.html",
				config: "js/configs/sp_favourite_weapons-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Modifications",
		    	href: "sp_modifications.html",
				config: "js/configs/sp_modifications-config.js",
		    },
			{
		    	title: "SP: Stealth",
		    	href: "sp_stealth.html",
				config: "js/configs/sp_stealth-config.js",
				profile: true,
		    },
			{
		    	title: "Player-on-Player Kills",
		    	href: "weapons_kills.html",
				config: "js/configs/weapons_kills-config.js",
		    },
		    {
		    	title: "Deaths",
		    	href: "weapons_deaths.html",
				config: "js/configs/weapons_deaths-config.js",
		    },
		],
	},
	
	// Arrests & Deaths
	{
		title: "Arrests & Deaths",
		href: "#",
		config: "js/configs/arrests_n_deaths-config.js",
		profile: true,
		subItems: [
		    {
			   	title: "Death Types",
			   	href: "death_types.html",
			   	config: "js/configs/death_types-config.js",
			   	profile: true,
			},
			{
			   	title: "Deaths per Hour",
			   	href: "deaths_per_hour.html",
			   	config: "js/configs/deaths_per_hour-config.js",
			   	profile: true,
			},
			{
		    	title: "SP: Mission Deaths",
		    	href: "sp_mission_deaths.html",
		    	config: "js/configs/sp_mission_deaths-config.js",
		    },
			{
			   	title: "Player Deaths",
			   	href: "arrests_n_deaths.html?stat=0",
			},
			{
			   	title: "Arrests per Hour",
			   	href: "arrests_n_deaths.html?stat=1",
			},
			{
			   	title: "Player Arrests",
			   	href: "arrests_n_deaths.html?stat=2",
			},
			{
		    	title: "SP: Wanted",
		    	href: "#",
		    	subItems: [
		    	    {
		    		   	title: "Times Wanted",
		    		   	href: "sp_wanted_times.html",
		    		   	config: "js/configs/sp_wanted_times-config.js",
		    		   	profile: true,
		    	    },
		    	    {
		    		   	title: "Time Spent Wanted",
		    		   	href: "sp_wanted_time_spent.html",
		    		   	config: "js/configs/sp_wanted_time_spent-config.js",
		    		   	profile: true,
		    	    },
		    	    {
		    		   	title: "5 Stars Level",
		    		   	href: "sp_wanted_five_stars.html",
		    		   	config: "js/configs/sp_wanted_five_stars-config.js",
		    		   	profile: true,
		    	    },
		    	    {
		    		   	title: "Wanted Level Per Hour",
		    		   	href: "sp_wanted_level_per_hour.html",
		    		   	config: "js/configs/sp_wanted_level_per_hour-config.js",
		    		   	tableau: true,
		    	    },
		    	],
		    },
		],
	},
	
	// Media
	{
		title: "Media",
		href: "#",
		subItems: [
		{
			title: "Radio Stations",
			href: "radio_stations.html",
			config: "js/configs/radio_stations-config.js",
		},
		{
			title: "TV Shows",
			href: "tv_shows.html",
			config: "js/configs/tv_shows-config.js",
		},
		{
			title: "Cinema",
			href: "cinema.html",
			config: "js/configs/cinema-config.js",
		},
		{
			title: "Website Visits",
			href: "website_visits.html",
			config: "js/configs/website_visits-config.js",
		},
		],
	},
	{
		title: "SP: Pickups / Packages",
		href: "#",
		subItems: [
		    {
		    	title: "SP: Letter Scraps",
		    	href: "sp_hidden_packages_0.html",
				config: "js/configs/sp_hidden_packages_0-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Spaceship Parts",
		    	href: "sp_hidden_packages_1.html",
				config: "js/configs/sp_hidden_packages_1-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Epsilon Tracts",
		    	href: "sp_hidden_packages_2.html",
				config: "js/configs/sp_hidden_packages_2-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Nuclear Waste",
		    	href: "sp_hidden_packages_3.html",
				config: "js/configs/sp_hidden_packages_3-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Submarine Pieces",
		    	href: "sp_hidden_packages_4.html",
				config: "js/configs/sp_hidden_packages_4-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Total Packages",
		    	href: "sp_hidden_packages_5.html",
				config: "js/configs/sp_hidden_packages_5-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Weapon Pickups",
		    	href: "sp_weapon_pickups.html",
				config: "js/configs/sp_weapon_pickups-config.js",
				profile: true,
		    },
		    {
		    	title: "SP: Health Pickups",
		    	href: "sp_health_pickups.html",
				config: "js/configs/sp_health_pickups-config.js",
				profile: true,
		    },
		],
	},
	
	// Apps
	{
		title: "Apps",
		href: "#",
		subItems: [
		    {
		    	title: "Car App",
		    	href: "app_car.html",
		    	config: "js/configs/app_car-config.js",
		    	profile: true,
		    },
		    {
		    	title: "Chop App",
		    	href: "app_chop.html",
		    	config: "js/configs/app_chop-config.js",
		    	profile: true,
		    },
		],
	},
	
	// Cheats
	{
		title: "Cheats",
		href: "#",
		subItems: [
		    {
		    	title: "Cheats Used",
		    	href: "cheats_used.html",
		    	config: "js/configs/cheats_used-config.js",
		    },
		],
	},
	
	// Sessions
	{
		title: ((project.freemodeAltName) ? project.freemodeAltName : "Freemode") + " Sessions",
		href: "#",
		subItems: [
		    {
		    	title: "Sessions With Username",
		    	href: "sessions_with_username.html",
		    	config: "js/configs/sessions_with_username-config.js",
		    	tableau: true,
		    },
		],
	},
	
	// Map Telemetry
	{
		title: "Map Telemetry",
		href: "#",
		subItems: [
		    {
		    	title: "Deathmatch",
		    	href: "map_deathmatch.html",
		    	//config: "js/map_deathmatch.js",
		    },
		    {
		    	title: "FPS",
		    	href: "map_fps.html",
		    	config: "js/configs/map_fps-config.js",
		    	notInProd: true,
		    },
		],
	},
	
	// Weather
	{
		title: "Weather",
		href: "#",
		subItems: [
		    {
				title: "SP: Weather Pie Charts",
				href: "weather_pie_charts_sp.html",
				config: "js/configs/weather_pie_charts_sp-config.js",
				tableau: true,
				notInProd: true,
			},
		    {
		    	title: "SP: Weather Over Time",
		    	href: "weather_over_time_sp.html",
		    	config: "js/configs/weather_over_time_sp-config.js",
		    	tableau: true,
		    	notInProd: true,
		    },
		    {
				title: "MP: Weather Pie Charts",
				href: "weather_pie_charts_mp.html",
				config: "js/configs/weather_pie_charts_mp-config.js",
				tableau: true,
				notInProd: true,
			},
		    {
		    	title: "MP: Weather Over Time",
		    	href: "weather_over_time_mp.html",
		    	config: "js/configs/weather_over_time_mp-config.js",
		    	tableau: true,
		    	notInProd: true,
		    },
		],
	},
	
	// Cutscenes
	{
		title: "Cutscenes",
		href: "#",
		subItems: [
		    {
		    	title: "Stats",
				href: "cutscenes.html",
				config: "js/configs/cutscenes-config.js",
			},
			{
		    	title: "Stats Per Mission",
				href: "cutscenes_per_mission.html",
				config: "js/configs/cutscenes_per_mission-config.js",
			},
			{
		    	title: "Watched Stats",
				href: "cutscenes_watched.html",
				config: "js/configs/cutscenes_watched-config.js",
			},
		],
	},
	{
		title: "Front End Settings",
		href: "#",
		subItems: [
		   {
			   title: "Controls",
			   href: "#",
			   config: "js/configs/frontend_controls-config.js",
			   profile: true,
			   subItems: [
			       {
			    	   title: "Control Type",
			    	   href: "frontend_controls.html?stat=0",
			  		},
			  		{
			  			title: "Targeting Mode",
			  			href: "frontend_controls.html?stat=1",
			  		},
			  		{
			  			title: "Vibration",
			  			href: "frontend_controls.html?stat=2",
			  		},
			  		{
	  			   		title: "Invert Look",
	  			   		href: "frontend_controls.html?stat=3",
			  		},
			  		{
			  		   	title: "Aim Sensitivity",
			  		   	href: "frontend_controls.html?stat=4",
			  		},
			  	],
		   },
		   {
			   title: "Display",
			   href: "#",
			   config: "js/configs/frontend_display-config.js",
			   profile: true,
			   subItems: [
				    {
					   title: "Radar",
					   href: "frontend_display.html?stat=0",
					},
					{
						title: "HUD",
						href: "frontend_display.html?stat=1",
					},
					{
						title: "Weapon Target",
						href: "frontend_display.html?stat=2",

					},
					{
			  			title: "GPS ROUTE",
			  			href: "frontend_display.html?stat=3",
					},
					{
					   	title: "Vehicle Camera Height",
					   	href: "frontend_display.html?stat=4",
					},
					{
					   	title: "Brightness",
					   	href: "frontend_display.html?stat=5",
					},
					{
					   	title: "Safezone Size",
					   	href: "frontend_display.html?stat=6",
					},
					{
					   	title: "Subtitles",
					   	href: "frontend_display.html?stat=7",
					},
					{
					   	title: "Language",
					   	href: "frontend_display.html?stat=8",
					},
				],
		   },
		   {
			   title: "Audio",
			   href: "#",
			   config: "js/configs/frontend_audio-config.js",
			   profile: true,
			   subItems: [
				    {
					   title: "SFX Volume",
					   href: "frontend_audio.html?stat=0",
					},
					{
						title: "Music Volume",
						href: "frontend_audio.html?stat=1",
					},
					{
						title: "Dialog Boost",
						href: "frontend_audio.html?stat=2",
					},
					{
						title: "Score",
						href: "frontend_audio.html?stat=3",
					},
					{
					   	title: "Output",
					   	href: "frontend_audio.html?stat=4",
					},
					{
					   	title: "Voice Chat",
					   	href: "frontend_audio.html?stat=5",
					},
					{
					   	title: "Voice Through Speakers",
					   	href: "frontend_audio.html?stat=6",
					},
				],
		   },
		   {
			   title: "Notifications",
			   href: "#",
			   config: "js/configs/frontend_notifications-config.js",
			   profile: true,
			   subItems: [
			        {
					   title: "Phone Alerts",
					   href: "frontend_notifications.html?stat=0",
					},
					{
						title: "Stats Alerts",
						href: "frontend_notifications.html?stat=1",
					},
					{
						title: "Crew Updates",
						href: "frontend_notifications.html?stat=2",
					},
					{
						title: "Friend Updates",
						href: "frontend_notifications.html?stat=3",
					},
					{
					   	title: "Social Club",
					   	href: "frontend_notifications.html?stat=4",
					},
					{
					   	title: "Store",
					   	href: "frontend_notifications.html?stat=5",
					},
				],
		   },
		   {
			   title: "Motion Sensor",
			   href: "#",
			   config: "js/configs/frontend_motion_sensor-config.js",
			   profile: true,
			   subItems: [
					{
					   title: "Helicopter",
					   href: "frontend_motion_sensor.html?stat=0",
					},
					{
						title: "Bike",
						href: "frontend_motion_sensor.html?stat=1",
					},
					{
						title: "Boat",
						href: "frontend_motion_sensor.html?stat=2",
					},
					{
						title: "Reload",
						href: "frontend_motion_sensor.html?stat=3",
					},
				],
		   },
	   ],
	},
];
