//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     false, // dates+builds
	     ],
};

var flowStages = [
   	"ARM3",
   	"JH2A",
   	"JH2B",
   	"TRV3",
   	"FIB4",
   	"MAR1",
   	"FIB5",
   	"AH3a",
   	"AH3b",
   	"FINA",
   	"FINB",
   	"FINC2",
  ];

var reportOptions = {
	restEndpoint: config.characterCash,
	restEndpointAsync: config.characterCashAsync,
	
	processFunction: formatData,
		
	hasFriendlierNames: false,
	
	description: "",
	
	//enableCSVExport: "content-description-controls",
	//enablePNGExport: "content-description-controls",	
	
	chartGroups: {
		getGroups: function(d) {return d}, // Data are returned grouped
		getGroupName: function(d) {
			return d.MissionName;
		},
	    getGroupValues: function(d) {
	    	return d.PerCharacter;
	    },
   	},
	
	bars: [
	{
		title: "Average Cash",
		colour: config.chartColour1,
    	getName: function(d) {
    		return d.Name;
    	},
    	getValue: function(d) {
    		return d.Value; 
    	},
    
    	getObject: function(d) { return d; },
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		var html = "<h3>" + e.point.Object.MissionName + "</h3><br />"
				+ "<table>"
					+ "<tr><td>Michael: </td><td class='right'>" 
						+ cashCommasFixed(e.point.Object.Michael, 2)
					+ " </td></tr>"
					+ "<tr><td>Franklin: </td><td class='right'>" 
						+ cashCommasFixed(e.point.Object.Franklin, 2)
					+ " </td></tr>"
					+ "<tr><td>Trevor: </td><td class='right'>" 
						+ cashCommasFixed(e.point.Object.Trevor, 2)
					+ " </td></tr>";
		html += "</table>";
			
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(Number(d));
	},
	units: "$",
	
	chartLeftMargin: 80,
};

function formatData(data) {
	//var result = new Array(flowStages.length);
	var resultObject = {};		
	
	if (data.length == 0)
		return [];
	
	$.each(data, function(i, r) {
		
		var flowIndex = flowStages.indexOf(r.MissionId);
		
		if (flowIndex !== -1) {
			r["PerCharacter"] = [
			    {"Name": "Michael", "Value": r.Michael}, 
			 	{"Name": "Franklin", "Value" : r.Franklin},
			 	{"Name": "Trevor", "Value": r.Trevor},
			 ];
			        		
			 r.PerCharacter.map(function(c) {
				 c["Michael"] = r.Michael;
			     c["Franklin"] = r.Franklin;
			     c["Trevor"] = r.Trevor;
			     c["MissionName"] = r.MissionName;
			     return c;
			  });
			 
			 resultObject[flowIndex] = r;
		}
		
	});
	
	// create an ordered by key-index array
	var result = Object.keys(resultObject).sort().map(function(d) { return resultObject[d]; });
	
	return result;
}
