var headerAndFilters  = {
	headerType: "header-sc-weapons", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var reportOptions = {
	restEndpoint: config.weaponKillsDeaths,
	restEndpointAsync: config.weaponKillsDeathsAsync,
	
	processFunction: formatData,
	
	hasExtraRestParams: [
	{
		key: "KillMode",
	    value: false,
	},
	],
				
	isClickable: false,
	hasFriendlierNames: true,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	description: "Total no of deaths per weapon",
	
	// This is a piechart
	main: {
		//title: "Total Deaths",
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: true,
		donut: true,
		donutLabelsOutside: false,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: getWeaponName,
		//getValue: function(d) {
		//	return d.Count; 
		//},
		getObject: function(d) { return d; },
		
		unit: "deaths",
		
		lrMargin: 55,
	},
	// This is the breakdown barchart
	breakdown: {
		//title: "No of Deaths",
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d.color; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: getWeaponName,
		//getValue: function(d) {
		//	return d.Count; 
		//},
		
		getYLabel: function(d) {return d.key; },
		
		matchColoursFromPieElement: "piechart",
		
		//unit: "deaths",
		
		leftMargin: 220,
		
		getObject: function(d) { return d; },
	},
};

var radioButtonsPrefix = "weapons-radio";
function setReportOptions() {
	var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var metric = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[0];
	var unit = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[1];
	
	reportOptions.main.title = label;
	//reportOptions.breakdown.title = ""; 
	
	reportOptions.main.unit = " " + unit;
	//reportOptions.breakdown.unit = reportOptions.main.title;
	
	reportOptions.main.getValue = function(d) {		
		return d[metric]; 
	};
	reportOptions.breakdown.getValue = reportOptions.main.getValue; 
	
}

function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiometrics")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("Count|deaths")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Total Deaths   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-2")
					.val("AverageCount|deaths")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-2")
					.text("Average Deaths   ")
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function formatData(data) {
	addMetrics();
	setReportOptions();
	
	//Calc and save averages
	data.map(function(d) {d["AverageCount"] = ((d.NumberOfGamers) ? (d.Count/d.NumberOfGamers) :0) });
	
	return	{
	   	"label": "", 
	   	"values": data,
	 	"metadata": {},
	};

}

function getWeaponName(d) {
	return ($("#friendlier-names").is(":checked") && d.WeaponFriendlyName && (d.WeaponFriendlyName != "Invalid")) 
			? d.WeaponFriendlyName : d.WeaponName; 
};
