// Grab the list of valid weapon heldtime profile stats - the variable name is weapons
getScriptSync("/stats/js/configs/weapons_heldtime_list.js");

//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     [true, false, true], // dates+builds
	     ],
};

var weaponNamesMap = getWeaponStatnames();
var profStat = new ProfileStats();

//The types now will be added dynamically from the weapons endpoint

var profileStatList = [
{
	types: weaponsHeldtime,
	requestSubString: "_HELDTIME",
	name: "Held Time",
	description: "Total held time per weapon",
	label: "Hours",
	unit: "hours",

	bucketSize: null,

	isWeaponStat: true,
	getName: getWeaponName,
	
	convertToHours: true,
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
				
	processFunction: formatData,
			
	availableCharts: profileStatList,
	multipleRequests: generateEndpoints,
			
	isClickable: false,
	hasFriendlierNames: true,
		
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
		
		// This is a piechart
	main: {
		title: profileStatList[0].name,
				
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,
		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) { return d.values; },
		getMetadata: function(d) {return d.metadata; },
			
		// function to get the  name from the rest data
		getName: function(d) { return getWeaponName(d.name);},
		//getValue: function(d) { return d.total; },
		getObject: function(d) { return d; },
			
		lrMargin: 20,
	},
	// This is the breakdown barchart
	breakdown: {
		title: profileStatList[0].name,
			
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,
		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d.color; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
			
		// function to get the  name from the rest data
		getName: function(d) { return getWeaponName(d.name); },
		//getValue: function(d) { return d.total; },
			
		getYLabel: function(d) { return profileStatList[0].label; },
			
		matchColoursFromPieElement: "piechart",
					
		leftMargin: 120,
			
		getObject: function(d) { return d; },
	},
		
}

var radioButtonsPrefix = "weapons-radio";
function setReportOptions() {
	var label = $(":radio[name=" + radioButtonsPrefix + "]:checked + label").text();
	var metric = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[0];
	var unit = $(":radio[name=" + radioButtonsPrefix + "]:checked").val().split("|")[1];
	
	reportOptions.main.title = label;
	//reportOptions.breakdown.title = "sdasd"; 
	
	reportOptions.main.unit = " " + unit;
	//reportOptions.breakdown.unit = reportOptions.main.title;
	
	reportOptions.main.getValue = function(d) {		
		return d[metric]; 
	};
	reportOptions.breakdown.getValue = reportOptions.main.getValue; 
	
}

function addMetrics() {
	
	$("#content-description")
	.empty()
	.append(
		$("<div>")
			.attr("id", "content-description-radiometrics")
			.css("float", "left")
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-1")
					.val("total|times")
					.attr("checked", true)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-1")
					.text("Total Held Time   ")
			)
			
			.append(
				$("<input>")
					.attr("type", "radio")
					.attr("name", radioButtonsPrefix)
					.attr("id", radioButtonsPrefix + "-2")
					.val("average|hours")
					.attr("checked", false)
			)
			.append(
				$("<label>")
					.attr("for", radioButtonsPrefix + "-2")
					.text("Average Held Time   ")
			)
	);
	
	$(":radio[name=" + radioButtonsPrefix + "]").change(function() {
		setReportOptions();
		
		drawCharts();
	});
};

function generateEndpoints(pValues) {
	var endpointObjects = [];
		
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
		
	var pieValues = [];
	$.each(data[0], function(j, typeResult) {
		pieValues.push({
			"name": profileStatList[0].types[j],
			"total": profStat.getValue(typeResult.response[0], profileStatList[0], j),
			"average": (Number(typeResult.response[0].YValue)) 
				? (profStat.getValue(typeResult.response[0], profileStatList[0], j) / Number(typeResult.response[0].YValue))
				: 0,
		});
	});
	
	
	addMetrics();
	setReportOptions();
	// Return an object
	return {
		"label": profileStatList[0].name,
		"values": pieValues,
		"metadata": {"unit": profileStatList[0].unit},
	};
}

function getStatNames(profileStat) {
	var gameTypes = ($("#game-types").val()) ? $("#game-types").val() : config.gameTypes;
	var character = ($("#character").val()) ? $("#character").val() : null;
	var statNames = [];
	
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, gameTypes, character).join(",")
		);
	});
			
	return statNames;
}

function getWeaponName(name) {
	if (weaponNamesMap.hasOwnProperty(name))
		return (($("#friendlier-names").is(":checked") && weaponNamesMap[name].FriendlyName) 
				? weaponNamesMap[name].FriendlyName : weaponNamesMap[name].Name);
	else
		return name;
}

