// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-freemode", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // categories
	     false, // ugc status
	     false, // creators
	     false, // flags 
	     false, // creation dates 
	     ],
};

var rosPlatformsDict = getRosPlatformsDict();
var matchTypesDict = getMatchTypesDict();
var matchSubTypesDict = getMatchSubTypesDict();
var freemodeCategoriesDict = getFreemodeCategoriesDict();

var reportOptions = {
	restEndpoint: config.freemodeContentDetailsStats,
	restEndpointAsync: config.freemodeContentDetailsStatsAsync,

	enableCSVExport: true,

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {
	    	title: "Type", 
	    	getValue: function(d) {
	    		return (matchTypesDict[d.Type]) ? matchTypesDict[d.Type] : "";  
	    	},
	    },
	    {
	    	title: "Sub Type", 
	    	getValue: function(d) {
	    		var subTypeName = "";
	    		
	    		if (matchSubTypesDict[d.Type])
	    			if (matchSubTypesDict[d.Type][d.SubType])
	    				subTypeName = matchSubTypesDict[d.Type][d.SubType]; 
	    				
	    		return subTypeName;
	    	}
	    },
	    {title: "Name", getValue: function(d) {return (d.Name) ? d.Name : "";} },   
	    {title: "Rank", getValue: function(d) {return d.Rank;} },
	    {title: "Min Players", getValue: function(d) {return d.MinPlayers;} },
	    {title: "Max Players", getValue: function(d) {return d.MaxPlayers;} },
	    {title: "Min Teams", getValue: function(d) {return d.MinTeams;} },
	    {title: "Max Teams", getValue: function(d) {return d.MaxTeams;} },
	    {title: "Contact", getValue: function(d) {return (d.Contact) ? d.Contact : "";} },
	    {title: "RP", getValue: function(d) {return commasFixed(d.XP);} },
	    //{title: "Cash", getValue: function(d) {return cashCommasFixed(d.Cash, 2);} },
	    {title: "Cash", getValue: function(d) { return mapCashToBand(d.Cash); } },
	    {
	    	title: "UGC ID", 
	    	getValue: function(d) {
	    		return (d.PublicContentId)
	    			? "<a href='" + project.scAdminUrl + project.scAdminJobsPath + d.PublicContentId + "/' " 
    					+ " target='_blank' title='View content on Social Club Admin' "	
    					+ ">" 
    						+ d.PublicContentId
    					+ "</a>"
    				: "";
	    	},
	    	getNonHtmlValue: function(d) {
	    		return (d.PublicContentId) ? d.PublicContentId : "";
	    	},
	    },
	    {
	    	title: "Creator ", 
	    	getValue: function(d) {
	    		return d.CreatorName  
	    			+ ((rosPlatforms[d.CreatorPlatform]) ? (" (" + rosPlatforms[d.CreatorPlatform].Name + ")") : ""); 
	    	} 
	    },
	    {
	    	title: "Creation Date", 
	    	getValue: function(d) {
	    		return (d.CreationDate) ? parseJsonDate(d.CreationDate).toUTCString() : "";
	    	} 
	    },
	    {
	    	title: "Published Date", 
	    	getValue: function(d) {
    			return (d.PublishedDate) ? parseJsonDate(d.PublishedDate).toUTCString() : "";
    		} 
	    },
	    {
	    	title: "Deleted Date", 
	    	getValue: function(d) {
    			return (d.DeletedDate) ? parseJsonDate(d.DeletedDate).toUTCString() : "";
    		} 
	    },
	    {title: "Category", getValue: function(d) {return freemodeCategoriesDict[d.Category]; } },
		{title: "Radio Station", getValue: function(d) {return d.RadioStation;} },
		{title: "Availability", getValue: function(d) {return d.Availability;} },
		{title: "Time Of Day", getValue: function(d) {return d.TimeOfDay;} },
		{title: "Start Coordinates", getValue: function(d) {
			return (d.StartCoordinates) ? 
					("x:" + d.StartCoordinates.X + ", y:" + d.StartCoordinates.Y + ", z:" + d.StartCoordinates.Z) 
					: "";} 
		},
		
		
	    {title: "Description", getValue: function(d) {return (d.Description) ? d.Description.trim() : "";} },
	],
	
	/* Groups */
	groups: [
	    {title: "Content List", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: false,
	
};

function groupByUser(data) {
	
	var users = {};
	$.each(data, function(i, d) {
		if (users.hasOwnProperty(d.UserName))
			users[d.UserName].Sessions.push(d);
		else {
			users[d.UserName] = {
				"UserName": d.UserName,
				"Sessions": [d],
			}
		}
	});
	
	//console.log(users);
	return users;
}

function mapCashToBand(d) {
	var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	//if (d == 0)
	//	return "N/A";
	if (str.charAt(d))
		return ("Band " + str.charAt(d));
	else 
		return d;
}
