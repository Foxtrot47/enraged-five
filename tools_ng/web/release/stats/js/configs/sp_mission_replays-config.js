// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     false, // dates+builds
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.missionsStats,
	restEndpointAsync: config.missionsStatsAsync,
	
	processFunction: filterNoRepeats,
	//legendText: "Mission Repeats",
	
	enableCSVExport: true,

	reportSummaryTitle: null,
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Name", getValue: function(d) {return d.MissionName;} },
	     {title: "Replays", getValue: function(d) {return d.ReplayAttempts;} },
	],
	reportArraySort: [[0, 0]], // [first item ASC]
	
	groups: [
	     //{title: "Main Missions", regexp: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|Unknown Mission|VOID|NET))", filterItem: 0},
	     {title: "Main Missions", regexp: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|VOID|NET))", filterItem: 0},
	     //{title: "Ambient", regexp: "^AMB", filterItem: 0},
	     //{title: "Cops n Crooks", regexp: "^CnC", filterItem: 0},
	     //{title: "Mini Games", regexp: "^MG", filterItem: 0},
	     //{title: "Odd Jobs", regexp: "^Odd", filterItem: 0},
	    // {title: "Random Events", regexp: "^RE", filterItem: 0},
	     {title: "Random Characters", regexp: "^RC", filterItem: 0},
	     //{title: "Special Peds", regexp: "^Special Ped", filterItem: 0},
	],

};

function filterNoRepeats(data) {
	return data.filter(function(d) { return (d.ReplayAttempts); });
}