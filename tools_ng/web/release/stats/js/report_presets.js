/*
 POST EXAMPLE
 
 {
"Name":"Build 303 PS3 Multi Local",
"Parameters": 
[
{"Key":"platforms", "Value":"PS3"},
{"Key":"locations", "Value": ""},
{"Key":"age-min", "Value": "0"},
{"Key":"age-max", "Value": "100"},
{"Key":"gamers", "Value": ""},
{"Key":"game-types", "Value":"Multiplayer"},
{"Key":"date-from", "Value":"03/12/2012"},
{"Key":"date-to", "Value":"08/12/2012"},
{"Key":"builds", "Value":"303"},
{"Key":"dates-builds", "Value":"build-field"},
]}

*/

// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

function initPage(admin) {
	// Hide the header is enabled
	var localHeaderNavSettings = retrieveHeaderAndNavSettings();
	if (localHeaderNavSettings.header) {
		localHeaderNavSettings.header = false;
		storeLocalHeaderAndNavSettings(localHeaderNavSettings);
	}
	
	$("#content-body")
		.append(
			$("<div>")
				.attr("id", "presets")
				.append(
					$("<div>")
						.attr("id", "presets-title")
						.append(
							$("<div>")
								.addClass("left")
								//.text("Report Presets")
						)
						.append(
							$("<div>")
								.addClass("right")
								.text("")
						)
				)
				.append(
					$("<div>").attr("id", "presets-list")
				)
			);
	
	if (admin) {
		initHeaderAndFilters(headerAndFilters);
		enablePresetSaving();
	}
	else 
		initHeaderAndFilters(null);
	
	generatePresetsList(admin);
}

function generatePresetsList(admin) {
	block();
	$.ajax({
		url: config.restHost + config.reportsPresets,
		type: "GET",
		dataType: "json",
		cache: false,
		success: function(presets, textStatus, jqXHR) {
			if (admin)
				populatePresetsAdminTable(presets);
			else
				populatePresetsTable(presets);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			unBlock();
		}
	});	
}

function populatePresetsTable(presets) {
	$("#presets-list").empty();
	
	$("#presets-list").append(
		$("<table>").append(
			$("<tr>")
				.append(
					$("<th>").text("Name")
				)
				//.append(
				//	$("<th>").text("Platforms")
				//)
				.append(
					$("<th>").text("Dates")
				)
				.append(
					$("<th>").text("Builds")
				)
		)
	);
	
	$.each(presets.sort(function(a, b) {return (a.Id > b.Id) ? -1 : 1}), function(i, preset) {
		$("#presets-list table").append(
			$("<tr>")
				.append(
					$("<td>")
						.text(preset.Name)
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)
						})
				)
				//.append(
				//	$("<td>")
				//		.text(function() {
				//			var platforms = preset.Parameters.filter(function(d) {return (d.Key == "platforms")});
				//			return ((platforms.length > 0) ? platforms[0].Value : "");
				//		})
				//		.attr("title", "Click to Select a Report Preset")
				//		.addClass("hand")
				//		.click(function() {
				//			showReport(preset)
				//		})
				//)
				.append(
					$("<td>")
						.text(function() {
							var date_from = preset.Parameters.filter(function(d) {return (d.Key == "date-from")});
							date_from = ((date_from.length > 0) ? date_from[0].Value : "");
							
							var date_to = preset.Parameters.filter(function(d) {return (d.Key == "date-to")});
							date_to = ((date_to.length > 0) ? date_to[0].Value : "");
							
							return (date_from + " - " + date_to); 
						})
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)
						})
				)
				.append(
					$("<td>")
						.text(function() {
							var builds = preset.Parameters.filter(function(d) {return (d.Key == "builds")});
							return ((builds.length > 0) ? builds[0].Value : "");
						})
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)}
						)
				)
			)
	});
}

function populatePresetsAdminTable(presets) {
	$("#presets-list").empty();
	
	$("#presets-list").append(
		$("<table>").append(
			$("<tr>")
				.append(
					$("<th>").text("Name")
				)
				.append(
					$("<th>").text("Dates")
				)
				.append(
					$("<th>").text("Builds")
				)
				.append(
					$("<th>")
						.addClass("delete")
						.text("Cache")
				)
				.append(
					$("<th>")
						.addClass("delete")
						.text("Force")
				)
				.append(
					$("<th>")
						.addClass("delete")
						.text("Delete")
				)
		)
	);
	
	$.each(presets.sort(function(a, b) {return (a.Id > b.Id) ? -1 : 1}), function(i, preset) {
		$("#presets-list table").append(
			$("<tr>")
				.append(
					$("<td>")
						.html(preset.Name)
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)
						})				
				)
				.append(
					$("<td>")
						.text(function() {
							var date_from = preset.Parameters.filter(function(d) {return (d.Key == "date-from")});
							date_from = ((date_from.length > 0) ? date_from[0].Value : "");
							
							var date_to = preset.Parameters.filter(function(d) {return (d.Key == "date-to")});
							date_to = ((date_to.length > 0) ? date_to[0].Value : "");
							
							return (date_from + " - " + date_to); 
						})
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)
						})
				)
				.append(
					$("<td>")
						.text(function() {
							var builds = preset.Parameters.filter(function(d) {return (d.Key == "builds")});
							return ((builds.length > 0) ? builds[0].Value : "");
						})
						.attr("title", "Click to Select a Report Preset")
						.addClass("hand")
						.click(function() {
							showReport(preset)}
						)
				)
				.append(
					$("<td>")
						.html("C")
						.attr("title", "Click to Cache this Preset")
						.addClass("hand")
						//.addClass("delete")
						.click(function() {
							performAllPresetRequests($(this), preset);
						})
				)
				.append(
					$("<td>")
						.html("F")
						.attr("title", "Click to Force Recache this Preset")
						.addClass("hand")
						//.addClass("delete")
						.click(function() {
							performAllPresetRequests($(this), preset, true);
						})
				)
				.append(
					$("<td>")
						.html("")
						.attr("title", "Click to Delete this Preset")
						.addClass("hand")
						.addClass("delete")
						.click(function() {
							deletePreset(preset.Id)}
						)
				)
		)
	});
}

// Saves the selected report and moves to the first page
function showReport(preset) {
	// Save the report's settings as local selected values
	var localPrefs = {};
	
	$.each(preset.Parameters, function(i, parameter) {
		// If the element is select, split the string to an array
		/*
		if (parameter.Key == "platforms" || parameter.Key == "locations"
			|| parameter.Key == "sc-users" || parameter.Key == "gamertags"
			|| parameter.Key == "game-types" || parameter.Key == "builds") {
			localPrefs[parameter.Key] = parameter.Value.split(",");
		}
		else { 
			localPrefs[parameter.Key] = parameter.Value;
		}
		*/
		if (parameter.Key == "radio-checked") {
			if (parameter.Value == "dates-field")
				localPrefs["dates-builds"] = "1";
			else if (parameter.Value == "builds-field")
				localPrefs["dates-builds"] = "2";
			
			return;
		}
		
		localPrefs[parameter.Key] = parameter.Value.split(",");
		
		//console.log(parameter.Key);
		//console.log(localPrefs[parameter.Key]);
	});
	//console.log(localPrefs);

	// Temporary hack for presets with no time
	//console.log(localPrefs["date-from"]);
	if (localPrefs["date-from"] && localPrefs["date-from"][0].length == 10)
		localPrefs["date-from"][0] += " 00:00";
	if (localPrefs["date-to"] && localPrefs["date-to"][0].length == 10)
		localPrefs["date-to"][0] += " 00:00";
	
	// Date grouping to Daily and xp-upper to 900000
	localPrefs["date-grouping"] = ["Daily"];
	localPrefs["upper-xp"] = [900000];
	
	// force UTC
	localPrefs["utc-force"] = true;
	
	var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
			config.headerOptions[headerAndFilters.headerType].storeName 
			: headerAndFilters.headerType;
		
	storeLocalObject(config.restHost + headerStoreName, localPrefs);
	//console.log(retrieveLocalObject(config.restHost + "_" + headerAndFilters.headerType));
	
	// Find the menu's next item and navigate there
	//var nextItemUrl = $("#nav-ul li a.active").parent().next().find("a").attr("href");
	var nextItemUrl = $("#nav-ul li").first().next().children("a").attr("href");
	if (nextItemUrl == "#")
		nextItemUrl = "total_playing_time.html";
	
	$(location).attr("href", nextItemUrl);
}

function enablePresetSaving() {
	$("#presets-title div.right")
		.append(
			$("<span>")
				.append(
					$("<input>")
						.attr("id", "preset-name")
						.attr("type", "text")
						.val("")
						.change(savePreset)
					)
		)	
		.append(
			$("<span>")
				.html("Save Current Settings as a Preset")
				.attr("title", "Save Current Settings as a Preset")
				.addClass("hand")
				.addClass("add")
				.click(savePreset)
		)
		
}

function savePreset() {
	var name = $("#preset-name").val();
	if (!name)
		return;
	
	var headerStoreName = (config.headerOptions[headerAndFilters.headerType].storeName) ?
			config.headerOptions[headerAndFilters.headerType].storeName 
			: headerAndFilters.headerType;
	
	var localPrefs = retrieveLocalObject(config.restHost + headerStoreName);
	
	var presetData = {
		"Name": name,
		"Parameters" : 
			[
			{"Key": "platforms", "Value": (localPrefs["platforms"]) ? localPrefs["platforms"].join(",") : ""},
			{"Key": "locations", "Value": (localPrefs["locations"]) ? localPrefs["locations"].join(",") : ""},
			{"Key": "age-min", "Value": (localPrefs["age-min"]) ? localPrefs["age-min"].join(",") : ""},
			{"Key": "age-max", "Value": (localPrefs["age-max"]) ? localPrefs["age-max"].join(",") : ""},
			//{"Key": "sc-users", "Value": (localPrefs["sc-users"]) ? localPrefs["sc-users"] : []},
			//{"Key": "gamertags", "Value": (localPrefs["gamertags"]) ? localPrefs["gamertags"] : []},
			{"Key": "game-types", "Value": (localPrefs["game-types"]) ? localPrefs["game-types"].join(",") : ""},
			{
				"Key": "date-from", 
				"Value": ((localPrefs["date-from"]) 
						? ( localPrefs["date-from"].join ? localPrefs["date-from"].join(",") : localPrefs["date-from"] ) 
						: "")
			},
			{
				"Key": "date-to", 
				"Value": ((localPrefs["date-to"]) 
						? ( localPrefs["date-to"].join ? localPrefs["date-to"].join(",") : localPrefs["date-to"] ) 
						: "")
			},
			//{"Key": "date-to", "Value": (localPrefs["date-to"]) ? localPrefs["date-to"].join(",") : ""},
			{"Key": "builds", "Value": (localPrefs["builds"]) ? localPrefs["builds"].join(",") : ""},
			{"Key": "dates-builds", "Value": (localPrefs["dates-builds"]) ? localPrefs["dates-builds"].join(",") : ""},
			]
	}
	//console.log(presetData);
	
	block();
	$.ajax({
		url: config.restHost + config.reportsPresets,
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(presetData),
		success: function(response, textStatus, jqXHR) {
			Sexy.alert("Preset Saved Successfully!");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert("Save Failed! Check the Console for more Info");
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			generatePresetsList(true);
			$("#preset-name").val("");
			unBlock();
		}
	});	
	
}

function deletePreset(reportId) {
	block();
	$.ajax({
		url: config.restHost + config.reportsPresets + "/" + reportId,
		type: "DELETE",
		success: function(response, textStatus, jqXHR) {
			Sexy.alert("Preset Deleted Successfully!");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert("Delete Failed! Check the Console for more Info");
			console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: function() {
			generatePresetsList(true);
			unBlock();
		}
	});	
	
}

function performAllPresetRequests(element, preset, force) {
	var forceUrlSuffix = "";
	if (force)
		forceUrlSuffix = config.reportForceSuffix;
	
	element.addClass("caching");
	
	var endpointObjects = [];
	
	var globalParamValues = {
		ElementKeyName: "Name",
		ElementValueName: "Value",
		Pairs: {},
	}
	
	$.each(preset.Parameters, function(i, parameter) {
		if (parameter.Key == "platforms")
			globalParamValues.Pairs["PlatformNames"] = parameter.Value;
		else if (parameter.Key == "locations")
			globalParamValues.Pairs["CountryCodes"] = parameter.Value;
		else if (parameter.Key == "age-min")
			globalParamValues.Pairs["AgeMin"] = (parameter.Value) ? parameter.Value : null;
		else if (parameter.Key == "age-max")
			globalParamValues.Pairs["AgeMax"] = (parameter.Value) ? parameter.Value : null;
		else if (parameter.Key == "sc-users")
			globalParamValues.Pairs["SocialClubUserNames"] = parameter.Value;
		else if (parameter.Key == "gamertags")
			globalParamValues.Pairs["GamerHandlePlatformPairs"] = parameter.Value;
		else if (parameter.Key == "game-types")
			globalParamValues.Pairs["GameTypeNames"] = parameter.Value;
		else if (parameter.Key == "date-from") {
			if (parameter.Value.length == 10)
				parameter.Value += " 00:00";
			globalParamValues.Pairs["StartDate"] = new Date(config.dateInputUTCFormat.parse(parameter.Value)).toISOString();
		}
		else if (parameter.Key == "date-to") {
			if (parameter.Value.length == 10)
				parameter.Value += " 00:00";
			globalParamValues.Pairs["EndDate"] = new Date(config.dateInputUTCFormat.parse(parameter.Value)).toISOString();
		}
		else if (parameter.Key == "builds")
			globalParamValues.Pairs["BuildIdentifiers"] = parameter.Value;
		//else if (parameter.Key == "radio-checked")
		//	globalParamValues.Pairs["radio-checked"] = parameter.Value;
		else if (parameter.Key == "dates-builds") {
			if (parameter.Value == 1)
				globalParamValues.Pairs["BuildIdentifiers"] = null; // dates only - disable builds
			else if (parameter.Value == 2) {
				globalParamValues.Pairs["StartDate"] = null; // builds only - disable dates
				globalParamValues.Pairs["EndDate"] = null;
			}
		}
		
		globalParamValues.Pairs["IncludeDebug"] = false;
		globalParamValues.Pairs["Grouping"] = "Daily";
		globalParamValues.Pairs["GroupByType"] = false;
	});
	//console.log(globalParamValues);

	// navigationItems is read from stats-config.js imported in the html file
	$.each(navigationItems, function(i, navItem) {
		if (navItem.subItems.length > 0) {
			$.each(navItem.subItems, function(i, subItem) {
				populateEndpoints(subItem);
			});
		}
		else
			populateEndpoints(navItem);		
	});
	
	var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
	req.sendMultipleAsyncRequest(endpointObjects, function() {
													Sexy.alert("Caching of '" + preset.Name + "' has finished");
													element.removeClass("caching");
												});
	
	function populateEndpoints(item) {
		if (item.config == null)
			return;
		else {
			// Load the script code synchronously
			$.ajaxSetup({async: false});
			$.getScript(item.config, function() {
				// If it's a profile stat
				if (item.profile) {
					endpointObjects = endpointObjects.concat(constructProfileStatEndpoints(reportOptions));
				}
				else if (item.tableau) {
					// Do nothing for Tableau reports
				}
				// If it's a capture stat report
				else if (item.capture) {
					/* DISABLED
					var pValues = $.extend(true, {}, globalParamValues);
					var latestBuild = pValues.Pairs["BuildIdentifiers"].split(",")[0];
					var availableBuilds = $.map($("#build option"), function(e) { return ((e.value <= latestBuild) ? e.value : null) });
					var reportBuilds = availableBuilds.splice(0, reportOptions.graphHistory);
					
					var pValuesArray = [];
					$.each(reportBuilds, function(i, build) {
						pValuesArray.push({
							ElementKeyName: "Name",
							ElementValueName: "Value",
							Pairs: {
								"BuildIdentifier": build,
							}
						});
					});
					
					//console.log(pValuesArray);
					endpointObjects.push({
						restUrl: config.restHost + reportOptions.restEndpoint,
						restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + forceUrlSuffix,
						pValues: pValuesArray,
					});
					*/
				}
				
				// Else it's a telemetry stat, apply the global param values
				else {
					// Deep copy the globalParams object
					var pValues = $.extend(true, {}, globalParamValues);
					
					if (pValues.Pairs["radio-checked"] == "builds-field") {
						pValues.Pairs["StartDate"] = null;
						pValues.Pairs["EndDate"] = null;
					}
					else if (pValues.Pairs["radio-checked"] == "dates-field") {
						pValues.Pairs["BuildIdentifiers"] = null;
					}
										
					endpointObjects.push({
						restUrl: config.restHost + reportOptions.restEndpoint,
						restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + forceUrlSuffix,
						pValues: [pValues],
					});
				}
				
			});			
			// End of load the script code synchronously
			$.ajaxSetup({async: true});
		}
	}
	
	function constructProfileStatEndpoints(reportOptions) {
		var profileEndpointObjects = [];
		var gameTypeArray = (globalParamValues.Pairs["GameTypeNames"]) ? 
									globalParamValues.Pairs["GameTypeNames"].split(",") : 
									config.gameTypes;
		var profStats = new ProfileStats();
		
		$.each(reportOptions.availableCharts, function(i, chart) {
			
			if (!chart.divided) {
				var pValuesArray = [];
				$.each(chart.types, function(i, type) {
					// Deep copy the globalParams object
					var pValues = $.extend(true, {}, globalParamValues);
					
					var statNames = profStats.constructStatNames(chart, type, gameTypeArray);
					// Add the extra combined profile stat params
					pValues.Pairs["StatNames"] = statNames.join(",");
					pValues.Pairs["BucketSize"] = chart.bucketSize;
					
					pValuesArray.push(pValues);
				});
				//console.log(reportOptions.restEndpoint);
				var endpointObject = {
					restUrl: config.restHost + reportOptions.restEndpoint,
					restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + forceUrlSuffix,
					pValues: pValuesArray,
				}
				
				profileEndpointObjects.push(endpointObject);
			}
			else {// if chart.divided	
				var numeratorStatNames = profStats.constructStatNames(chart, chart.types[0], gameTypeArray);
				var denominatorStatNames = profStats.constructStatNames(chart, chart.types[1], gameTypeArray);
			
				// Deep copy the globalParams object
				var pValues = $.extend(true, {}, globalParamValues);
				// Add the extra divided profile stat params
				pValues.Pairs["NumeratorStatNames"] = numeratorStatNames.join(",");
				pValues.Pairs["DenominatorStatNames"] = denominatorStatNames.join(",");
				pValues.Pairs["BucketSize"] = chart.bucketSize;
				
				var endpointObject = {
					restUrl: config.restHost + config.profileStatsDividedDiff,
					restAsyncUrl: config.restHost + config.profileStatsDividedDiffAsync + forceUrlSuffix,
					pValues: [pValues],
				}
				profileEndpointObjects.push(endpointObject);
			}
		});
		
		return profileEndpointObjects;
	}
	
	//console.log(endpoindObjects);
}
