
function onTimerTasks(runOnce) {
	utc = $("#utc-force").is(":checked");
	
	$.ajax({
		type: "GET",
		url: monitorUrlPath,
		dataType: "json",
		success: function(json) {
			jobJson = {};
			jobResultsJson = {};

			var table1 = $("#tasklist1").dataTable();
			table1.fnClearTable();
			
			$.each(json, function(i, item) {
				table1.fnAddData([i, item.Name, item.AvailableJobCount], false);
			});
			//table1.fnDraw();
			table1.fnStandingRedraw();

			var table2 = $("#tasklist2").dataTable();
			table2.children("tbody").find("*").unbind();
			table2.off("draw").on("draw", updateTasksTable);
			table2.fnClearTable();

			var taskDetail = json[taskIndex];
			//var datetimeSeparator = "<br />"; - breaking sorting
		
			// process jobs results
			$.each(taskDetail.JobResults, function(i, jobResult) {
				if (jobResultsJson.hasOwnProperty(jobResult.JobId))
					jobResultsJson[jobResult.JobId].push(jobResult.SubmittedChangelistNumber);
				else
					jobResultsJson[jobResult.JobId] = [jobResult.SubmittedChangelistNumber];
			});
			
			$.each(taskDetail.Jobs, function(i, taskJob) {
				if (!$("input#" + jobstateenum[taskJob.State]).is(":checked"))
					return;
				
				jobJson[taskJob.ID] = taskJob;
				
				//if(taskDetail.__type == monitorTaskName) {
					table2.fnAddData([
					     getChangelistLink(taskJob.Trigger.Changelist),
					     //truncate(taskJob.ID, 15),
					     taskJob.ID,
					                  
					     taskJob.Trigger.Username,
					     jobpriorityenum[taskJob.Priority],
					                  
					     getDatetimeString(parseJsonDate(taskJob.Trigger.SubmittedAt), utc),
					     getDatetimeString(parseJsonDate(taskJob.CompletedAt), utc),
					                  
					     formatSecs(datesDiffInSecs(parseJsonDate(taskJob.CompletedAt), parseJsonDate(taskJob.Trigger.SubmittedAt))),
					     formatSecs(datesDiffInSecs(parseJsonDate(taskJob.CompletedAt), parseJsonDate(taskJob.ProcessedAt))),
					                  
					     jobstateenum[taskJob.State],
					     ((jobResultsJson.hasOwnProperty(taskJob.ID)) ? getJobResultsList(jobResultsJson[taskJob.ID]) : ""),
					     "<button title='View Job Details'>View</button>"
					],
					false);
				//}
			    /*
				else {
					if(typeof taskJob !== "undefined") {
					//if(job != undefined) {
						table2.fnAddData(["-", //job.Trigger.Changelist,
						        createBugstarLinks(taskJob.Trigger.Description), // Added CL Description - b*#1187359
						        //truncate(taskJob.ID, 15),
						        taskJob.ID,
						                  
						        taskJob.Trigger.Username,
						        jobpriorityenum[taskJob.Priority],
						                  
						        getDatetimeString(parseJsonDate(taskJob.Trigger.SubmittedAt), utc),
						        getDatetimeString(parseJsonDate(taskJob.CompletedAt), utc),
						                  
						        formatSecs(datesDiffInSecs(parseJsonDate(taskJob.CompletedAt), parseJsonDate(taskJob.Trigger.SubmittedAt))),
						        formatSecs(datesDiffInSecs(parseJsonDate(taskJob.CompletedAt), parseJsonDate(taskJob.ProcessedAt))),
						                  
						        jobstateenum[taskJob.State],
						        jobResultsJson.hasOwnProperty(taskJob.ID) ? getJobResultsList(jobResultsJson[taskJob.ID]) : "",
						        "<button title='View Job Details'>View</button>"
						   ],
						   false);
					}
				}
				*/
				
			});
			
			if (runOnce)
				table2.fnDraw();
			else
				table2.fnStandingRedraw();
										
		} // end of ajax success:
	});

	
	if (!runOnce)
		setTimeout(onTimerTasks, reloadInterval);
}

function initTasks() {
	
	$("#utc-tz").text(timezoneString); // set the timezone text

	//get the jobstate enum list

	$.ajax({
		type: "GET",
		url: enumsUrl,
		data: {type: jobStateEnumDataType},
		dataType: "json",
		async: false,
		success: function(json) {
			jobstateenum = {};

			$.each(json, function(i, item) {
				jobstateenum[item.Key] = item.Value;
			});
		}
	});

	$.ajax({
		type: "GET",
		url: enumsUrl,
		data: {type: jobPriorityEnumDataType},
		dataType: "json",
		async: false,
		success: function(json) {
			jobpriorityenum = {};

			$.each(json, function(i, item) {
				jobpriorityenum[item.Key] = item.Value;
			});
		}
	});

	//create the tables with the correct headers

	var tabledataList1 = {"aoColumns" : [], "bAutoWidth": false, 
						"bFilter": false, "bSort": false, "bInfo": false, "bPaginate": false};

	tabledataList1.aoColumns.push({ "sTitle": "Index", "sWidth": "10%"});
	tabledataList1.aoColumns.push({ "sTitle": "Task", "sWidth": "80%" });
	tabledataList1.aoColumns.push({ "sTitle": "Open Jobs", "sWidth": "10%" });

	$("#tasklist1").dataTable(tabledataList1);

	var tabledataList2 = {
			"aoColumns" : [], "bStateSave": true, "bAutoWidth": false, "bProcessing": true,
			"iDisplayLength": 25, "aaSorting": [[ 4, "asc" ]],
			"sDom": '<"top"plf<"clear">>rt<"bottom"ip<"clear">>'
	};
	tabledataList2.aoColumns.push({"sTitle": "Changelist", "sWidth": "10%", "sClass": "data-chlist"});
	//tabledataList2.aoColumns.push({"sTitle": "Description", "sWidth": "10%", "sClass": ""});
	//tabledataList2.aoColumns.push({"sTitle": "ID", "sWidth": "10%", "sClass": "data-id"});
	tabledataList2.aoColumns.push({"sTitle": "ID", "bVisible": false, "sClass": "data-id"});
	
	tabledataList2.aoColumns.push({"sTitle": "User", "sWidth": "10%", "sClass": "data-username"});
	tabledataList2.aoColumns.push({"sTitle": "Priority", "sWidth": "10%", "sClass": ""});
	
	tabledataList2.aoColumns.push({"sTitle": "Submitted At", "sWidth": "10%", "sType": "date"});
	tabledataList2.aoColumns.push({"sTitle": "Completed At", "sWidth": "10%", "sType": "date"});
	
	tabledataList2.aoColumns.push({"sTitle": "Turnaround Time", "sWidth": "10%", "sClass": ""});
	tabledataList2.aoColumns.push({"sTitle": "Processing Time", "sWidth": "10%", "sClass": ""});
	
	tabledataList2.aoColumns.push({"sTitle": "State", "sWidth": "10%", "sClass": "data-state"});
	tabledataList2.aoColumns.push({"sTitle": "Submitted Changelists", "sWidth": "10%", "sClass": ""});
	tabledataList2.aoColumns.push({"sTitle": "Action", "sWidth": "5%", "sClass": "data-view"});
	
	// Unbind all events before each draw - not sure if that works
	tabledataList2["fnPreDrawCallback"] = function() {$("#tasklist2 tbody").find("*").unbind();};
	
	$("#tasklist2").dataTable(tabledataList2);
    
    $("#joblist input[type=checkbox]").change(function() {
    	// Run only once to avoid breaking the periodical timeout
    	onTimerTasks(true);
    });
    
    onTimerTasks(false);
}

// Called when the tasks table is being redrawn - adds additional styling and hover user/files info
function updateTasksTable() {
	//console.log("updateTasksTable() called");
	
	var table2 = $("#tasklist2").dataTable();
	
	var rows = $("#tasklist2 tbody tr");
	$.each(rows, function(i, row) {
		
		// Add coloured classes to each state
		var state = $(row).find("td.data-state").text().toLowerCase();
		$(row).removeClass().addClass(state);
		
		var rowData = table2.fnGetData(this);
		//console.log(rowData);
		var currentJobId = "";
		if (rowData)
			currentJobId = rowData[1];
		
		/*
		var idTd = $(row).find("td.data-id");
		idTd.attr("title", "Click for a list of affected Files");
		idTd.unbind().bind("click", function() {
			if (jobJson[currentJobId]) {
				showJobFilesList(jobJson[currentJobId], $(this));
				$(this).children("div.job-files").fadeIn("fast");
			}
		});
		idTd.bind("mouseleave", function(e) {
			$(this).children("div.job-files")
				.fadeOut("fast")
				.remove();
		});
		*/
		
		/*
		var chListTd = $(row).find("td.data-chlist");
		
		chListTd.unbind().bind("mouseenter", function() {
			if (jobResultsJson.hasOwnProperty(currentJobId)) {
				showJobResultsList(jobResultsJson[currentJobId], $(this));
				$(this).children("div.job-results").fadeIn("fast");
			}
		});
		chListTd.bind("mouseleave", function(e) {
			$(this).children("div.job-results")
				.fadeOut("fast")
				.remove();
		});
		*/ 
		
		var chListTd = $(row).find("td.data-chlist");
		
		chListTd.unbind().bind("mouseenter", function() {
			if (jobJson[currentJobId]) {
				showJobFilesList(jobJson[currentJobId], $(this));
				$(this).children("div.job-files").fadeIn("fast");
			}
		});
		chListTd.bind("mouseleave", function(e) {
			$(this).children("div.job-files")
				.fadeOut("fast")
				.remove();
		});
		
		// View Button
		var button = $(row).find("td.data-view button");

		button.unbind().bind("click", function() {
			addJob(currentJobId);
		});
	
		button.button();
		
		// User Info pop-up : add the user div to the respective table's td 
		var userTd = $(row).find("td.data-username");
		userTd.unbind().bind("mouseover", function() {
			createUserInfoDiv($(this).text(), $(this));
			$(this).children("div.user").fadeIn("fast");
		});
		userTd.bind("mouseleave", function() {
			$(this).children("div.user")
				.fadeOut("fast")
				.remove();
		});

	});
		
}

function showJobFilesList(job, element) {
	var tableDiv = $("<div />")
			.addClass("job-files");
	
	var idTable = $("<table />");
	idTable.append(
		$("<tr />").append(
			$("<th />")
				.addClass("title")
				.html("Job ID: " + job.ID)
		)
	);
	idTable.appendTo(tableDiv);
	
	var descTable = $("<table />");
	descTable.append(
		$("<tr />").append(
			$("<th />").html("Description")
		)
	);
	descTable.append(
		$("<tr />").append(
			$("<td />").html(createBugstarLinks(job.Trigger.Description)) // Added CL Description - b*#1187359
		)
	);
	descTable.appendTo(tableDiv);
	
	var filesTable = $("<table />");
	filesTable.append(
		$("<tr />").append(
			$("<th />").html("Files Processed")
		)
	);
		
	$.each(job.Trigger.Files, function(i, file) {
		$("<tr />")
			//.addClass(function() { return ((i%2) ? "odd" : "even"); })
			.append(
				$("<td />").text(file)
			)
			.appendTo(filesTable)
	});
	filesTable.appendTo(tableDiv);
	
	tableDiv.appendTo(element);
}

/*
function showJobResultsList(jobResult, element) {
	
	var tableDiv = $("<div />")
		.addClass("job-results");
	
	var table = $("<table />");
	table.append(
		$("<tr />").append(
			$("<th />").html("Submitted Changelists")
		)
	);
	
	$.each(jobResult, function(i, changelist) {
		$("<tr />")
			.addClass(function() { return ((i%2) ? "odd" : "even"); })
			.append(
				$("<td />").html(getChangelistLink(changelist))
			)
			.appendTo(table)
	});

	table.appendTo(tableDiv);
	tableDiv.appendTo(element);
}
*/

function getJobResultsList(jobResult) {
	return jobResult.map(function(d) {return getChangelistLink(d); }).join(",<br />")
}

