﻿$.support.cors = true;

var automationMonitorSubpath = "automation.svc/admin/monitor";
var jobsStatSubpath = "automation.svc/admin/EnumValues?type=RSG.Pipeline.Automation.Common.Jobs.JobState";

function updateAutomationStatus(tableId, serverClassname, serverUrlClassname, statusClassname, jobsClassname) {
    $("#" + tableId + " tbody").find("tr").each(function () {
        var serverTd = $(this).find("." + serverClassname);
        var serverUrl = serverTd.find("." + serverUrlClassname).text().trim();
        var statusTd = $(this).find("." + statusClassname);
        var jobsTd = $(this).find("." + jobsClassname);

        $.ajax({
            url: serverUrl + automationMonitorSubpath,
            dataType: "json",
            success: function (result) {

                // Group the job number per status
                var jobsPerState = {};
                $.each(result[0].Jobs, function (i, job) {
                    if (jobsPerState.hasOwnProperty(job.State))
                        jobsPerState[job.State]++;
                    else
                        jobsPerState[job.State] = 1;
                });

                // Get the available job states
                $.ajax({
                    url: serverUrl + jobsStatSubpath,
                    dataType: "json",

                    success: function (jobStates) {
                        statusTd.html("<span class='service-ok'></span> Normal");
                        jobsTd.text("Total: " + result[0].Jobs.length);

                        $.each(jobStates, function (j, jobState) {
                            if (jobsPerState.hasOwnProperty(jobState.Key)) {
                                jobsTd.append("<br/>" + jobState.Value + ": " + jobsPerState[jobState.Key]);
                            }
                            //jobsTd.append("<br/>" + jobState.Value + ": " 
                            //        + ((jobsPerState.hasOwnProperty(jobState.Key)) ? jobsPerState[jobState.Key] : 0));
                        });
                    }
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
                statusTd.html("<span class='service-error'></span> Error &nbsp;&nbsp;&nbsp;");
            }
            
        });
    });
}