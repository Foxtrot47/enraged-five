﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaveCSV.aspx.cs" Inherits="RSG.Statistics.AspUtils.SaveCSV" %>

<!DOCTYPE html>
<html>

<head>
    <title></title>

    <link rel="stylesheet" type="text/css" href="/shared/css/iframe.css" />

</head>

<body>
    <form id="CsvForm" runat="server">
        <asp:Button id="ExportCSVButton" runat="server" text="Export to CSV" tooltip="Export to CSV" />

        <input type="hidden" id="CsvData" name="CsvData"  runat="server" value="" />
        <input type="hidden" id="CsvFilename" name="CsvFilename"  runat="server" value="" />

    </form>
</body>

</html>
