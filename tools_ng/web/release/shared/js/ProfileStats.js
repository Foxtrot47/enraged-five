
var ProfileStats = function() {
	var singlePrefix = "SP",
		multiPrefix = "MP",
		singleLength = 3,
		multiLength = 5;
		rangeWarningFlag = false;

	function constructStatNames(currentStatObj, currentStat, gameTypes, character) { 
		var singleStatNames = [],
			multiStatNames = [],
			finalStatNames = [];
		
		if (character) {
			var charStat;
			if (character < singleLength) {
				if (currentStatObj.isWeaponStat)
					charStat = singlePrefix + character + "_" + currentStat + currentStatObj.requestSubString;
				else
					charStat = singlePrefix + character + currentStatObj.requestSubString + currentStat;
			}
			else {
				if (currentStatObj.isWeaponStat)
					charStat = multiPrefix + (character-singleLength) + "_" + currentStat + currentStatObj.requestSubString;
				else
					charStat = multiPrefix + (character-singleLength) + currentStatObj.requestSubString + currentStat;
			}
			//console.log(charStat);
			finalStatNames.push(charStat);
		}
		else if (!currentStatObj.singleMetric) {
			for (var i=0; i<singleLength; i++) {
				if (currentStatObj.isWeaponStat) // Weapon profile stats have different name patterns
					singleStatNames.push(singlePrefix + i + "_" + currentStat + currentStatObj.requestSubString);
				else
					singleStatNames.push(singlePrefix + i + currentStatObj.requestSubString + currentStat);
			}
			for (var i=0; i<multiLength; i++) {
				if (currentStatObj.isWeaponStat) // Weapon profile stats have different name patterns
					multiStatNames.push(multiPrefix + i + "_" + currentStat + currentStatObj.requestSubString);
				else
					multiStatNames.push(multiPrefix + i + currentStatObj.requestSubString + currentStat);
			}
		
			$.each(gameTypes, function(i, gameType) {
				if (gameType == config.gameTypes[0] && !currentStatObj.multiOnly)
					finalStatNames = finalStatNames.concat(singleStatNames);
				if (gameType == config.gameTypes[1] && !currentStatObj.singleOnly)
					finalStatNames = finalStatNames.concat(multiStatNames);
			});
		}
		else { 
			finalStatNames.push(currentStat);
		}
		
		return finalStatNames;
	}
	
	function getName(dataObject, confObject, currentTypeId) {
		var statName;
		
		if (confObject.convertToHours && !confObject.altNames) { // Total Playing Time chart
			statName = [Number(dataObject.BucketMin) / config.anHourInMillisecs,
			            Number(dataObject.BucketMax) / config.anHourInMillisecs]
				.join(" - ") 
				+ (" (hours)");
		}
		else if (confObject.convertToMinutes && !confObject.altNames) { // Total Playing Time chart in mins
			statName = [Number(dataObject.BucketMin) / config.aMinInMillisecs,
			            Number(dataObject.BucketMax) / config.aMinInMillisecs]
				.join(" - ") 
				+ (" (mins)");
		}
		else if (confObject.perHour) { // Metric Per Hour Chart
			statName = [(Number(dataObject.BucketMin) * config.anHourInMillisecs).toFixed(1),
			        (Number(dataObject.BucketMax) * config.anHourInMillisecs).toFixed(1)]
				.join(" - ");
		}
		else if (confObject.addPercentageLabel) { // Percentage Chart
			if (confObject.multiplyToPercentage) {
				statName = [Number(dataObject.BucketMin) * confObject.multiplyToPercentage,
				        Number(dataObject.BucketMax) * confObject.multiplyToPercentage]
					.join("-");
			}
			
			statName = ((Number(dataObject.BucketMin) * statName.multiplyToPercentage == 100)
						? Number(dataObject.BucketMin) * statName.multiplyToPercentage 
						: statName)
					+ " %";
		}
		else if (confObject.altNames) {
			// replace the bucket names with the given alt ones
			// The altNames id should come from the buckets lower range
			
			if (confObject.types.length > 1) {
				statName = confObject.altNames[currentTypeId];
			}
			else // bucket result
			{		
				var altId = Number(dataObject.BucketMin);
			
				if ((altId !== "undefined") && (confObject.altNames.length > altId))
					statName = confObject.altNames[altId];
				else {
					rangeWarningFlag = true;
					statName = dataObject.Bucket;
				}
			}
		}
		else if (confObject.showOnlyLowerRange) {
			
			var lowerValue = Number(dataObject.BucketMin);
			
			// If the results have to be in specific value ranges
			if (confObject.validRange 
					&& (lowerValue < confObject.validRange[0] || lowerValue > confObject.validRange[1])) {
				rangeWarningFlag = true;
				return;
			}
			else
				statName = lowerValue;
		}
		else if (confObject.bucketSize == null)
			statName = confObject.types[currentTypeId];
		else
			statName = dataObject.Bucket;
		
		return statName;
	}
	
	function getValue(dataObject, confObject, currentTypeId) {
		var value;
		
		if (confObject.isAverage) {
			value = (Number(dataObject.YValue)) ? Number(dataObject.Total)/Number(dataObject.YValue) : 0;
		}
		else if (confObject.bucketSize === null && !confObject.isAverage) {
			value = Number(dataObject.Total);			
		}
		else
			value = Number(dataObject.YValue);
		
		if (confObject.convertToHours) {
			value = value/config.anHourInMillisecs;
		}
		
		
		return value;
	}
	
	function convertResultsToDict(results, keys) {	
		var dict = {}; 

		$.each(results, function (i, resultItem) {
			if (!dict.hasOwnProperty(keys[i]))
				dict[keys[i]] = resultItem;
		});
		
		return dict;
	}
	
	function groupNonZero(resultArray) {
		var newResult = [
        {
        	"Bucket": "0-1",
        	"BucketMax": 1,
        	"BucketMin": 0,
        	"Total": 0,
        	"YValue": 0,
        },
        {
        	"Bucket": "1-2",
        	"BucketMax": 2,
        	"BucketMin": 1,
        	"Total": 0,
        	"YValue": 0,
        }
		];
		
		$.each(resultArray, function(i, resultIem) {
			if (resultIem.BucketMin == 0) {
				newResult[0].YValue += Number(resultIem.YValue);
				newResult[0].Total += Number(resultIem.Total);
			}
			else {
				newResult[1].YValue += Number(resultIem.YValue);
				newResult[0].Total += Number(resultIem.Total);
			}
				
		});
		
		return newResult;
	}
	
	function processBucketResults(resultArray, confObject) {
		var extraBuckets = [];
		
		$.each(resultArray, function(i, resultIem) {
			
			// Limit results b* #1731866
			// Exit the loop of adding extra buckets
			if ((confObject.upperLimit) && (resultIem.BucketMax > confObject.upperLimit)) {
				return;
			}
			
			if (confObject.fillWithEmptyBuckets) {
				if ((i > 0) && (resultArray[i-1].BucketMax != resultIem.BucketMin)) {
					if (confObject.roundPrecision) {
						var round = function(d) { return d3.round(d, confObject.roundPrecision); };
						resultIem.BucketMin = round(resultIem.BucketMin);
						resultArray[i-1].BucketMax = round(resultArray[i-1].BucketMax);
					}
					
					var repeat = (resultIem.BucketMin - resultArray[i-1].BucketMax)/confObject.bucketSize;
					for(var j=0; j<repeat; j++) {
						
						var bucketMin = resultArray[i-1].BucketMax + confObject.bucketSize*j;
						var bucketMax = bucketMin + confObject.bucketSize;
						
						extraBuckets.push({
							"Bucket" : bucketMin + "-" + bucketMax,
							"BucketMin" : bucketMin,
							"BucketMax" : bucketMax,
							"Total" : 0,
							"YValue" : 0,
						});
					} 
				}
			} // End of if fillWithEmptyBuckets
			
		});
		
		if (extraBuckets) {
			resultArray = (resultArray.concat(extraBuckets)).sort(
					function(a, b) {
						return (a.BucketMin >= b.BucketMin) ? 1 : -1; 
					});
		}
		
		// // Limit results b* #1731866 - part 2
		// Filter out the values that are higher than confObject.upperLimit
		if (confObject.upperLimit)
			resultArray = resultArray.filter(function(o) {return (o.BucketMax <= confObject.upperLimit); });
		
		return resultArray;
	}
	
	/* Expose the public functions */
	return {
		constructStatNames: constructStatNames,
		getName: getName,
		getValue: getValue,
		convertResultsToDict: convertResultsToDict,
		groupNonZero: groupNonZero,
		processBucketResults: processBucketResults,
    };

}