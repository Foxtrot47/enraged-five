// reportOptions variable comes from respective config file

// Array that stores objects for each element returned via rest
var chartsData;

var NVGraphs = new NVD3CustomGraphs();
var mainDOMId = "piecharts";

var csvExportButton;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", mainDOMId)
		);
				
	$("#filter").click(function() {
		csvExportButton = false; // Regenerate csv export data
		generateCharts();
	});

	//updateChartHeight();
	generateCharts();
}

function generateCharts() {
	chartsData = [];
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
	
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
			
} //end of generateCharts()

function cleanCharts() {
	$("#" + mainDOMId).empty();
}

/*
function updateChartHeight() {
	// Adjust the height of the charts automatically
	var subHeader = ($("#sub-header").height()) ? $("#sub-header").height() : 30;
	var windowHeight = $("#content").height() - 2*subHeader;
	$("#" + mainDOMId).css("height", windowHeight);
}
*/

function drawCharts(data) {
	// If a processFunction function is specified
	if (data) {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
	
	if (chartsData.length < 1) {
		cleanCharts();
		Sexy.alert(config.noDataText);
		return;
	}
	
	var placeholderWidth = $("#" + mainDOMId).width();
	var placeholderHeight = $("#" + mainDOMId).height();
	//var dataColumns = 2;
	var dataRows = (chartsData.length == 2) ? 1 : 2;
	
	if (reportOptions.enableCSVExport && !csvExportButton)
		enableCsvExport();
	
	if (reportOptions.enablePNGExport)
		addExportPngButton(reportOptions.enablePNGExport);
		
	var graphOptions = reportOptions.main;
	
	var existingItems = $("#" + mainDOMId).find("div");
	if (existingItems.length > chartsData.length)
		existingItems.slice(chartsData.length).remove();
	
	$.each(chartsData, function(i, pieData) {
		
		var pieItemDOMId = mainDOMId + "_" + i;
		
		// This will add the svg on the first time
		if ($("#" + mainDOMId).find("#" + pieItemDOMId).size() == 0) {
			$("#" + mainDOMId)
				.append(
					$("<div>").attr("id", pieItemDOMId)
				);
			initSvg(pieItemDOMId);
		}
		
		var totalColumns = Math.round(chartsData.length / dataRows);
		var currentCol = Math.floor(i/dataRows);
		currentCol = (chartsData.length%2 && i!=0) ? Math.floor((i+1)/dataRows) : currentCol;
		
		var currentRow = Math.round(i%dataRows);
		currentRow = (chartsData.length%2 && i!=0) ? Math.round((i+1)%dataRows) : currentRow;
		
		var pieWidth = Math.floor(placeholderWidth / totalColumns);
		var pieHeight = Math.floor(100 / dataRows);
		
		$("#" + pieItemDOMId)
			.css("position", "absolute")
			//.removeClass().addClass(function() {return (i%2) ? "float-right" : "float-left"})
			/*
			.css("width", function() {
				// give max width to the first element in odd number of detailed piecharts
				return ((i==0) && (chartsData.length%2)) ? "100%" : Math.floor(100/dataColumns) + "%";
			})
			.css("height", Math.floor(placeholderHeight/ Math.round(chartsData.length / dataColumns)));
			*/
			.css("width", pieWidth)
			.css("height", ((i==0) && (chartsData.length%2)) ? "100%" : pieHeight + "%")
			.css("top", currentRow*pieHeight + "%")
			.css("left", currentCol*pieWidth)
			
			//.css("left", function() {
			//	return ((i==0) && (chartsData.length%2)) ? "0%" : Math.floor(100/dataRows) + "%";
			//});
		
		var datum = [{
			key: graphOptions.getPieLabel(pieData),
			values: graphOptions.getValuesArray(pieData),
			metadata: graphOptions.getMetadata(pieData),
		}];
		
		NVGraphs.drawPiechart(datum, pieItemDOMId, graphOptions);	
	}); //end of each detailedObjects
	
}

/* Copied from piechart_breakdown-report.js */
function enableCsvExport() {
	
	var csvColumns = [];
	var csvRows = [];
	
	var selectedMetricObj;
	$("#content-description-radiometrics").find("input").each(function(metricIndex, metricObj) {
		selectedMetricObj = ($(metricObj).prop("checked")) ? metricObj : selectedMetricObj;
		$(metricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	
		populateCSVArrays(csvColumns, csvRows);
	});
	
	if ($("#content-description-radiometrics").find("input").size() == 0)
		populateCSVArrays(csvColumns, csvRows); // Run for the default setting
		
	// Reset checkbox selection
	if (selectedMetricObj) {
		$(selectedMetricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	}
	
	
	// Add the button in the DOM
	var csvFrameID = "piechart-breakdown-frame";
	$("#" + csvFrameID).remove();
			
	addCSVButton(reportOptions.enableCSVExport, 
			csvFrameID, 
			function() {
				exportToCSV(csvColumns, 
							csvRows, 
							csvFrameID, 
							getCSVFilename()); 
			});
		
	$("#" + csvFrameID).addClass("csv-right");
	csvExportButton = true;
}

function populateCSVArrays(csvColumns, csvRows) {
	
	var mainGraphOptions = reportOptions.main;
	var currentRowId = -2; // There are no columns
	
	$.each(chartsData, function(i, chartData) {
		
		var mainTotal = d3.sum(mainGraphOptions.getValuesArray(chartData), 
						function(mainItem) {return mainGraphOptions.getValue(mainItem);}
					);
		csvRows[++currentRowId] = [" ", " ", " "]; // Increase and add Empty Line
		
		currentRowId++; // Increase the row
		if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
			csvRows[currentRowId] = [];
			csvRows[currentRowId][0] = mainGraphOptions.getPieLabel(chartData).toUpperCase();
		}
		var metadata = mainGraphOptions.getMetadata(chartData);
		csvRows[currentRowId] = csvRows[currentRowId].concat(
		    (metadata.Metric || metadata.Unit || metadata.unit),
		     "%"
		);
		$.each(mainGraphOptions.getValuesArray(chartData), function(i, mainItem) {
					
			currentRowId++; // Increase the row
			if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
				csvRows[currentRowId] = [];
				csvRows[currentRowId][0] = mainGraphOptions.getName(mainItem);
			}
						
			csvRows[currentRowId] = csvRows[currentRowId].concat([
			    mainGraphOptions.getValue(mainItem),
			    formatPercentage(mainGraphOptions.getValue(mainItem), mainTotal),
			]);
		});		
		
	});
		
}
