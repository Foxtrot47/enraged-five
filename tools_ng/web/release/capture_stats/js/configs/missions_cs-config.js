//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs-per", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // build-config
		 false, // percentiles
		 false, // game-times
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	inputTextLabel: "Main Missions :: ",
	
	hasFilterInput: true,
	
	restEndpoint: config.missionCaptureStats,
	restEndpointAsync: config.missionCaptureStatsAsync,
	
	enableCSVExport: true,
	
	reportSummaryTitle: null,
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Name", getValue: function(d) {return d.MissionName;} },
	     {title: "Sample Size", getValue: function(d) {return d.SampleSize;} },
	
	     { 
	    	 title: "Min FPS", 
	    	 getValue: function(d) {return (d.MaxFrameTime) ? fixed(1000 / d.MaxFrameTime) : fixed(d.MaxFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MaxFrameTime) >= 30) ? "green" : "red"); }
	     },
	     {
	    	 title: "Avg FPS", 
	    	 getValue: function(d) {return (d.AvgFrameTime) ? fixed(1000 / d.AvgFrameTime) : fixed(d.AvgFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.AvgFrameTime) >= 30) ? "green" : "red"); },
	     },
	     {
	    	 title: "Max FPS",  	 
	    	 getValue: function(d) {return (d.MinFrameTime) ? fixed(1000 / d.MinFrameTime) : fixed(d.MinFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MinFrameTime) >= 30) ? "green" : "red"); }
	     },
	     
	     {title: "Min Frame Time (ms)", getValue: function(d) {return fixed(d.MinFrameTime);} },
	     {title: "Avg Frame Time (ms)", getValue: function(d) {return fixed(d.AvgFrameTime);} },
	     {title: "Max Frame Time (ms)", getValue: function(d) {return fixed(d.MaxFrameTime);} },
	
	     {title: "Min Draw Time (ms)", getValue: function(d) {return fixed(d.MinDrawTime);} },
	     {title: "Avg Draw Time (ms)", getValue: function(d) {return fixed(d.AvgDrawTime);} },
	     {title: "Max Draw Time (ms)", getValue: function(d) {return fixed(d.MaxDrawTime);} },
	     
	     {title: "Min Gpu Time (ms)", getValue: function(d) {return fixed(d.MinGpuTime);} },
	     {title: "Avg Gpu Time (ms)", getValue: function(d) {return fixed(d.AvgGpuTime);} },
	     {title: "Max Gpu Time (ms)", getValue: function(d) {return fixed(d.MaxGpuTime);} },
	
	     {title: "Min Update Time (ms)", getValue: function(d) {return fixed(d.MinUpdateTime);} },
	     {title: "Avg Update Time (ms)", getValue: function(d) {return fixed(d.AvgUpdateTime);} },
	     {title: "Max Update Time (ms)", getValue: function(d) {return fixed(d.MaxUpdateTime);} },
	     
	     {title: "Min Drawcall Count", getValue: function(d) {return fixed(d.MinDrawcallCount);} },
	     {title: "Avg Drawcall Count", getValue: function(d) {return fixed(d.AvgDrawcallCount);} },
	     {title: "Max Drawcall Count", getValue: function(d) {return fixed(d.MaxDrawcallCount);} },
	     
	     {title: "Min Object Count", getValue: function(d) {return fixed(d.MinObjectCount);} },
	     {title: "Avg Object Count", getValue: function(d) {return fixed(d.AvgObjectCount);} },
	     {title: "Max Object Count", getValue: function(d) {return fixed(d.MaxObjectCount);} },
	],
	reportArraySort: [[1, 0]], // [second item (first is +) ASC]
	
	hasReportArrayItemMoreInfo: true,
	
	getReportMoreInfoArray: function(d) {return d;},
	// Generate one table for the results
	hasMoreInfoMultipleTables: false,
	// No key available at the results (no multiple tables), using a hardcoded table title
	getMoreInfoKey: function(d) {return "Checkpoints"},
	// use the whole array result as well (no multiple tables)
	getMoreInfoValues: function(d) {return d.CheckpointPerformances},
	reportMoreInfoArrayItems: [
	     {title: "Name", getValue: function(d) {return d.CheckpointName;} },
	     {title: "Sample Size", getValue: function(d) {return d.SampleSize;} },

	     { 
	    	 title: "Min FPS", 
	    	 getValue: function(d) {return (d.MaxFrameTime) ? fixed(1000 / d.MaxFrameTime) : fixed(d.MaxFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MaxFrameTime) >= 30) ? "green" : "red"); }
	     },
	     {
	    	 title: "Avg FPS", 
	    	 getValue: function(d) {return (d.AvgFrameTime) ? fixed(1000 / d.AvgFrameTime) : fixed(d.AvgFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.AvgFrameTime) >= 30) ? "green" : "red"); },
	     },
	     {
	    	 title: "Max FPS",  	 
	    	 getValue: function(d) {return (d.MinFrameTime) ? fixed(1000 / d.MinFrameTime) : fixed(d.MinFrameTime) ;},
	    	 getClass: function(d) {return (((1000 / d.MinFrameTime) >= 30) ? "green" : "red"); }
	     },

	     {title: "Min Frame Time (ms)", getValue: function(d) {return fixed(d.MinFrameTime);} },
	     {title: "Avg Frame Time (ms)", getValue: function(d) {return fixed(d.AvgFrameTime);} },
	     {title: "Max Frame Time (ms)", getValue: function(d) {return fixed(d.MaxFrameTime);} },
	     
	     {title: "Min Draw Time (ms)", getValue: function(d) {return fixed(d.MinDrawTime);} },
	     {title: "Avg Draw Time (ms)", getValue: function(d) {return fixed(d.AvgDrawTime);} },
	     {title: "Max Draw Time (ms)", getValue: function(d) {return fixed(d.MaxDrawTime);} },
	     
	     {title: "Min Gpu Time (ms)", getValue: function(d) {return fixed(d.MinGpuTime);} },
	     {title: "Avg Gpu Time (ms)", getValue: function(d) {return fixed(d.AvgGpuTime);} },
	     {title: "Max Gpu Time (ms)", getValue: function(d) {return fixed(d.MaxGpuTime);} },
	     
	     {title: "Min Update Time (ms)", getValue: function(d) {return fixed(d.MinUpdateTime);} },
	     {title: "Avg Update Time (ms)", getValue: function(d) {return fixed(d.AvgUpdateTime);} },
	     {title: "Max Update Time (ms)", getValue: function(d) {return fixed(d.MaxUpdateTime);} },
	     
	     {title: "Min Drawcall Count", getValue: function(d) {return fixed(d.MinDrawcallCount);} },
	     {title: "Avg Drawcall Count", getValue: function(d) {return fixed(d.AvgDrawcallCount);} },
	     {title: "Max Drawcall Count", getValue: function(d) {return fixed(d.MaxDrawcallCount);} },
	     
	     {title: "Min Object Count", getValue: function(d) {return fixed(d.MinObjectCount);} },
	     {title: "Avg Object Count", getValue: function(d) {return fixed(d.AvgObjectCount);} },
	     {title: "Max Object Count", getValue: function(d) {return fixed(d.MaxObjectCount);} },
	],
	reportMoreInfoArraySort: [[0, 0]], // [first item, ASC]
	
	     
	groups: [
	     //{title: "Main Missions", regexp: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|Unknown Mission|VOID|NET))", filterItem: 0},
	     {title: "Main Missions", regexp: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|VOID|NET))", filterItem: 0},
	     {title: "Ambient", regexp: "^AMB", filterItem: 0},
	     {title: "Cops n Crooks", regexp: "^CnC", filterItem: 0},
	     {title: "Mini Games", regexp: "^MG", filterItem: 0},
	     {title: "Odd Jobs", regexp: "^Odd", filterItem: 0},
	     {title: "Random Events", regexp: "^RE", filterItem: 0},
	     {title: "Random Characters", regexp: "^RC", filterItem: 0},
	     {title: "Special Peds", regexp: "^Special Ped", filterItem: 0},
	],
	
	graphMetricReportId: 3,
	graphMetricGroupId: 0,
	graphHistory: 10,
};

