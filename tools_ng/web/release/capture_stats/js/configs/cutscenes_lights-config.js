//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cl", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platform
		 false, // build
		 false, // flags
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.cutsceneLights,
	restEndpointAsync: config.cutsceneLightsAsync,
	
	enableCSVExport: true,
	
	reportSummaryTitle: null,
	
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Cutscene Name", getValue: function(d) {return d.CutsceneName; }},
	     {title: "Sample Size", getValue: function(d) {return d.SampleSize; }},
	     
	     {title: "Total Min", getValue: function(d) {return fixed(d.Total.Min); }},
	     {title: "Total Avg", getValue: function(d) {return fixed(d.Total.Average); }},
	     {title: "Total Max", getValue: function(d) {return fixed(d.Total.Max); }},
	     {title: "Total SD", getValue: function(d) {return fixed(d.Total.StandardDeviation); }},
	     
	     {title: "Scene Min", getValue: function(d) {return fixed(d.Scene.Min); }},
	     {title: "Scene Avg", getValue: function(d) {return fixed(d.Scene.Average); }},
	     {title: "Scene Max", getValue: function(d) {return fixed(d.Scene.Max); }},
	     {title: "Scene SD", getValue: function(d) {return fixed(d.Scene.StandardDeviation); }},
	     
	     {title: "LOD Min", getValue: function(d) {return fixed(d.LOD.Min); }},
	     {title: "LOD Avg", getValue: function(d) {return fixed(d.LOD.Average); }},
	     {title: "LOD Max", getValue: function(d) {return fixed(d.LOD.Max); }},
	     {title: "LOD SD", getValue: function(d) {return fixed(d.LOD.StandardDeviation); }},
	     
	     {title: "Directional Min", getValue: function(d) {return fixed(d.Directional.Min); }},
	     {title: "Directional Avg", getValue: function(d) {return fixed(d.Directional.Average); }},
	     {title: "Directional Max", getValue: function(d) {return fixed(d.Directional.Max); }},
	     {title: "Directional SD", getValue: function(d) {return fixed(d.Directional.StandardDeviation); }},
	],
	
	hasReportArrayItemMoreInfo: false,
	
   	// Group the results 
	groups: [
	     //filterItem is the array index of the item for the regexp match, name is at id=0 of reportArrayItems
	     {title: "All Cutscenes", regexp: "", filterItem: 0}, 
	],

	/*
	// Graph only related vars
	*/
};


