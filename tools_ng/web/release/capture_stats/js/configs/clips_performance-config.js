var headerAndFilters  = {
	headerType: "header-clips", // clips filtering header
	disabledFields:			 // disables header fields 
		[
		 false, // source build
		 true, // data builds
		 false, // clip categories
	     ],
};

var reportOptions = {
	restEndpoint: config.clipsPerformanceStats,
	restEndpointAsync: config.clipsPerformanceStatsAsync,
	
	legendText: "Clips Performance",
	tableTitle: "Clip Dictionaries",

	// No nested array results
	getReportArray: function(d) {return d},
	reportArrayItems: [
		{title: "Dictionary Name", getValue: function(d) {return d.DictionaryName; }},	
		{title: "Clip Name", getValue: function(d) {return d.ClipName; }},
		{title: "Times Played", getValue: function(d) {return d.TimesPlayed; }},
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
		
	// One level only
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	   {title: "Clip Dictionaries", regexp: "", filterItem: 0},
	],

};
