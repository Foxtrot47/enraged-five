var graphBuilds,
	requestProcesses;

var graphId = "graph";
var filterInputId = "filter-input";

var defaultFilterText = "type text to filter the chart";

var typewatch = function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    }
}();

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content")
	.append(
		$("<div>")
			.attr("id", "friendlier-names-field")
			.append(
				$("<input>")
					.attr("id", "friendlier-names")
					.attr("type", "checkbox")
					//.attr("disabled", true)
					.click(function() {
						if (requestProcesses) {
							typewatch(function() {
								prepareGraphArray(requestProcesses)
							}, 500);
						}
					})
			)
			.append(
				$("<label>")
					.attr("for", "friendlier-names")
					.attr("title", "Check to Hide the Legends")
					.text("Hide Legends")
			)	
	);
	
	if (reportOptions.hasFilterInput) {
		$("#content-description").append(
			$("<input />")
				.attr("id", filterInputId)
				.attr("type", "text")
				.val(defaultFilterText)
				.focus(function() {
					if($(this).val() == defaultFilterText)
						$(this).val("");
				})
				.blur(function() {
					if ($(this).val() == "")
						$(this).val(defaultFilterText);
				})
				.keyup(function() {
					if (requestProcesses)
						typewatch(function() {
							prepareGraphArray(requestProcesses)
						}, 500);
				})
		);
	}
	
	$("#content-body")
		.append(
			$("<div>")
				.attr("id", graphId)
				.addClass("single-graph")
		);
	
	initSvg(graphId);
	
	/*
	$("#chart-filter label").text(reportOptions.inputTextLabel);
	
	$("#chart-filter-input").val(reportOptions.defaultInputText);
	$("#chart-filter-input").focus(function() {
		if($(this).val() == reportOptions.defaultInputText)
			$(this).val("");
	});
	$("#chart-filter-input").blur(function() {
		if($(this).val() == "")
			$(this).val(reportOptions.defaultInputText);
	});
	// IE fix for not firing the change event when pressing enter
	if ($.browser.msie) {
		$("#chart-filter-input").live("keypress", function(e) {
			if (e.keyCode == 13)
				if (requestProcesses)
					prepareGraphArray(requestProcesses);
			});
	}
	else {
		$("#chart-filter-input").change(function() {
			//alert("changed");
			if (requestProcesses)
				prepareGraphArray(requestProcesses);
		});
	}
	
	/*
	$("#chart-filter-input").keyup(function() {
		if (requestProcesses)
			prepareGraphArray(requestProcesses);
	});
	*/
	
	$("#filter").click(function() {
		generateGraph();
	});
	
	generateGraph();	
}

function generateGraph() {
	var headerValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	var selectedBuild = headerValues.Pairs["BuildIdentifier"];
		
	var latestBuild;
	if (!selectedBuild)
		return;

	var availableBuilds = $.map($("#build option"), function(e) {
		return (((e.value <= selectedBuild) && (e.value != -1)) ? e.value : null) 
	});
	//console.log(availableBuilds);
	
	// get the specified number of historical data and reverse them so the older values are first
	graphBuilds = availableBuilds.splice(0, reportOptions.graphHistory).reverse();
	//console.log(graphBuilds);
		
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var pValues = [];
	$.each(graphBuilds, function(i, gBuild) {
		// Deep copy the object first
		var buildPValues = $.extend(true, {}, headerValues);
		buildPValues.Pairs["BuildIdentifier"] = gBuild;
		
		pValues.push(buildPValues);
	});
	//console.log(pValues);
	
	var forceUrlSuffix = "";
	if ($("#filter-force").is(":checked"))
		forceUrlSuffix = config.reportForceSuffix;
	
	var endpoinds = [{
		restUrl: config.restHost + reportOptions.restEndpoint,
	 	restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + forceUrlSuffix,
	 	pValues: pValues,
	}];
	
	var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
	req.sendMultipleAsyncRequest(endpoinds, storeMonitoredProcesses);
}

function storeMonitoredProcesses(monitoredProcesses) {
	requestProcesses = monitoredProcesses[0];
	prepareGraphArray(requestProcesses);
}

function prepareGraphArray(requestProcesses) {
	graphSeries = {}; // this is for storing the values of each series on the graph (i.e. missions)
	
	// We need only the main missions, the rest will be filtered out
	var filterGroup = reportOptions.groups[reportOptions.graphMetricGroupId];
	var groupRegExp = new RegExp(filterGroup.regexp, "i");
	// The metric from the returned object to use for filtering, usually this is the name
	var filterMetric = reportOptions.reportArrayItems[filterGroup.filterItem];
	// The metric from the returned object to use for the graph (in missions case this is the avg fps)
	var graphMetric = reportOptions.reportArrayItems[reportOptions.graphMetricReportId];
	
	var filterInputText = $("#" + filterInputId).val();
	if (filterInputText == defaultFilterText)
		filterInputText = "";
		
	var textRegExp = new RegExp(filterInputText, "i");
	
	$.each(requestProcesses, function(i, process) {
		// This means that the process has failed and an error text has been set as the response
		if ((typeof process.response) === "string") {
			return;
		}
				
		$.each(process.response, function(j, element) {

			if (!groupRegExp.test(filterMetric.getValue(element))
				|| !textRegExp.test(filterMetric.getValue(element)))
				return;
			
			var entry = {
				Key: i, // The key is the number on the map
				Value: graphMetric.getValue(element),
			};
			if (!graphSeries.hasOwnProperty(filterMetric.getValue(element))) {
				graphSeries[filterMetric.getValue(element)] = [entry];
			}
			else {
				graphSeries[filterMetric.getValue(element)].push(entry);
			}
			
		});
		
	});
	//console.log(graphSeries);
	
	// This is for converting our data to a format that NVD3 understands
	var graphArray = [];
	
	$.each(graphSeries, function(entryKey, entryValue) {
		graphArray.push({
			key: entryKey,
			values: entryValue, 
			color: config.colourRange(Math.random()*20), 
		});
	});
	
	drawLinegraph(graphArray, graphBuilds, graphId, "Build", graphMetric.title);
	//console.log(graphArray);
}

// Copied from Capture Stats Report (slightly modified)
function drawLinegraph(datum, tickValues, htmlId, xLabel, yLabel) {
	d3.select("#" + htmlId + " svg g.nvd3").remove();
	
	if (datum.length < 1) {
		//d3.select("#" + htmlId + " svg g.nvd3").remove();
		Sexy.alert(config.noDataText);
		return;
	}
	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
			//.x(function(d) { return d.Key; })
			.x(function(d) { return d.Key; })
			.y(function(d) { return Number(d.Value); })
			.margin({top: 50, right: 40, bottom: 100, left: 80})
			.tooltips(true)
			.showLegend(($("#friendlier-names").is(":checked")) ? false : true);
		
		chart.xAxis
			.axisLabel(xLabel)
			.tickValues(d3.range(tickValues.length))
			.tickFormat(function(d, i) {
				//console.log(d, i);
				//console.log((tickValues[d] ? tickValues[d] : d));
				return (tickValues[parseInt(d)] ? tickValues[parseInt(d)] : d);
			})
			//.tickValues(tickValues)
			//.tickFormat(function(d, i) {console.log(d); return d;})
			.rotateLabels(-45);
			
		chart.yAxis
			.axisLabel(yLabel)
			.tickFormat(d3.format(".02f"));
	
		d3.select("#" + htmlId + " svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
	 
		nv.utils.windowResize(chart.update);
	 
		return chart;
	});
}
