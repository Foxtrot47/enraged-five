var graphType,
	statDefListJson;
  	  
//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	// Post header filter addition 
	$("#filter").click(function() {
		updateHtmlFields();
	});
	
	generateProfileStatList();
}

function updateHtmlFields() {
	// make sure that at least one option is selected before updating the page html/graph
 	if ($("select#stat-options").val() == null)
 		return;
 	
 	var bucketInput = $("#bucket-options input[type=number]").val();
 	if (bucketInput == null || bucketInput == "" || isNaN(bucketInput) || parseInt(bucketInput) < 0)
 		return;
 	bucketInput = parseFloat(bucketInput);
 	
 	profileAjaxUri = config.restHost + config.profileStatPath + config.combinedPath;
    
   	graphType = $("#graph-type input[name=graph-type-radio]:radio:checked").val();
   	
   	var bucketSize, bucketCount;
   	if ($("#bucket-options input[name=bucket-options-radio]:radio:checked").val() == "bucketSize") {
   		bucketSize = bucketInput;
   		bucketCount = 0;
	}
   	else {
   		bucketSize = 0
   		bucketCount = bucketInput;
   	}
   	
   	var builds = ($("#build").val()) ? $("#build").val().join() : "";
	var platforms = ($("#platform").val()) ? $("#platform").val().join() : "";
	//var gameTypes = ($("#game-type").val()) ? $("#game-type").val().join() : "";
	//var start = "\/Date(" + config.dateInputFormat.parse($("input#date-from").val()).getTime() + ")\/"; 
	//var end = "\/Date(" + config.dateInputFormat.parse($("input#date-to").val()).getTime() + ")\/";
	var start = new Date(config.dateInputFormat.parse($("input#date-from").val())).toISOString();
	var end = new Date(config.dateInputFormat.parse($("input#date-to").val())).toISOString();
   	
	var pValue = {
			ElementKeyName: "Name",
			ElementValueName: "Value",
			Pairs: {
				"StatNames": $("select#stat-options").val().join(","),
				"BucketSize": bucketSize,
				"BucketCount": bucketCount,
				"PlatformNames": platforms,
				//"GameTypeNames": gameTypes,
				"StartDate": start,
				"EndDate": end,
				"BuildIdentifiers": builds,
			}
		};

	var req = new ReportRequest(config.restHost + config.profileStatsCombined,
			"json",
			config.restHost + config.profileStatsCombinedAsync,
			config.restHost + config.reportsQueryAsync);
	
	req.sendSingleAsyncRequest(pValue, generateGraph);
	
	//generateGraph();
}
    
function generateProfileStatList() {
	updateChartHeight();

	block();
	$.ajax({
		url: config.restHost + config.profileStatDefPath,
		type: "GET",
		dataType: "json",
			
		success: function(json, textStatus, jqXHR) {
			statDefListJson = $(json.Items).sort(sortByIdentifierAsc);
			populateHtmlSelectOptions(statDefListJson, "");
    	},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError );
		},
		complete: function() {
			unBlock();
		}
	});
}

function updateChartHeight() {
	var windowHeight = $("#content").height() - 20;
	$("#chart").css("height", windowHeight);
}
    
function populateHtmlSelectOptions(jsonItems, filterText) {
	var re = new RegExp(filterText, "i");
    	
    $.each(jsonItems, function(i, Item) {
    	if (re.test(Item.Identifier)) {
    		$("select#stat-options").append(
    				$('<option />')
    					.text(Item.Identifier)
    					.val(Item.Identifier)
    		);
    	}
	});
}
    
function emptyHtmlSelectOptions(jsonItems, filterText) {
   	$("select#stat-options").empty();
}
	
function generateGraph(response) {
	//console.log(response);
	
	if (graphType == "line")
		drawLineGraph(response);
	else if (graphType == "pie")
		drawPieChart(response);
	else if (graphType == "bar")
		drawBarChart(response, false);
	else if (graphType == "hor-bar")
		drawBarChart(response, true);
	else
		alert("No graph type selected");

}

function drawLineGraph(jsonData) {
	nv.addGraph(function() {  
		var datum = [{key: "Users per Value Range", values: jsonData, color: config.chartColour1}];
		var chart = nv.models.lineChart()
			//.x(function(d, i) { return i })
			.x(function(d, i) {
				return calcMeanValue(d.Bucket.split("-").map(function(d) {return parseInt(d);}));
			})
			.y(function(d) { return parseInt(d.YValue) })
			.tooltips(true);
		 
		chart.xAxis
			.axisLabel('Value Range (Average)')
			.tickFormat(d3.format(",r"));		 
		chart.yAxis
			.axisLabel("Users")
			.tickFormat(d3.format(",r"));
		
		var svg = d3.select("#chart svg"); 
		svg.datum(datum)
			.transition()
				.duration(config.transitionDuration)
			.call(chart);
		
		nv.utils.windowResize(chart.update);
		nv.utils.windowResize(updateChartHeight);
		
		return chart;
	});
}

function drawPieChart(jsonData) {
	var datum = [{key: "Pie Chart", values: jsonData, color: config.chartColour1}];
	
	nv.addGraph(function() {
		var chart = nv.models.pieChart()
			.x(function(d) { return d.Bucket })
			.y(function(d) { return parseInt(d.YValue) })
			.showLabels(true);
		
		var svg = d3.select("#chart svg");
		svg.datum(datum)
			.transition()
				.duration(config.transitionDuration)
		 	.call(chart);
		
		svg.select(".nv-pie")
		 	.attr("filter", "url(#dropShadow)");
		
		nv.utils.windowResize(chart.update);
		nv.utils.windowResize(updateChartHeight);
		 
		return chart;
	});
}
	
function calcArcPercentage(d) {
	return d3.round(((d.endAngle - d.startAngle)/(2 * Math.PI) * 100), 2);
}

function calcMeanValue(array) {
	if (array.leght < 1)
		return 0;
	else
		return array.reduce(function(accumulator, element) { return accumulator + element}) / array.length;
}


function drawBarChart(jsonData, isHorizontal) {
	var datum = [{key: "Users per Value Range", values: jsonData, color: config.chartColour1}];
	
	nv.addGraph(function() {
		
		var chart;
		if (isHorizontal)
			chart = nv.models.multiBarHorizontalChart()
				.showValues(true);
		else
			chart = nv.models.multiBarChart();
		
		chart.x(function(d) { return d.Bucket })
			.y(function(d) { return parseInt(d.YValue) })
			.tooltips(true)
			.showControls(true);
	
		chart.xAxis
			.axisLabel("Value Range");
		
		chart.yAxis
			.axisLabel("Users")
			.tickFormat(d3.format(",r"));
		 
		d3.select("#chart svg")
			.datum(datum)
			.transition()
				.duration(config.transitionDuration)
		 	.call(chart);
		
		nv.utils.windowResize(chart.update);
		nv.utils.windowResize(updateChartHeight);
		 
		return chart;
	});
}
	
$("select#stat-options").change(function() {
	updateHtmlFields();
});
 	
//$("#stat-options-filter").keyup(function() {  // TOO SLOW
$("#stat-options-filter").change(function() {
	emptyHtmlSelectOptions();
	populateHtmlSelectOptions(statDefListJson, $(this).val());
});
   	
$("#graph-type input[name=graph-type-radio]:radio").change(function() {
	fadeOutGraph("chart");
	updateHtmlFields();
});
   	
$("#bucket-options input[name=bucket-options-radio]:radio").change(function() {
	updateHtmlFields();
});
   	
$("#bucket-options input[type=number]").change(function() {
	updateHtmlFields();
});
   	
   	