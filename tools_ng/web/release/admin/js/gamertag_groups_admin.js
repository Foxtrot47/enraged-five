var rosPlatformsDict = getRosPlatformsDict();

function initPage() {
	initHeaderAndFilters(null);
	
	$("#new-top-group").click(function() {
		addGroup(); 
	});
	$("#delete-top-group").click(function() {
		deleteGroup(); 
	});
	
	$("#group-head")
		.append(
			$("<button>").text("Update Group")
				.attr("id", "update-group")
				.attr("title", "Update Group")
				.click(function() {
					updateGroup(); 
				})
		)
		.append(
			$("<span>")
				.css("right", 0)
				.css("position", "absolute")
				.html("CSV File: ")
				.append(
					$("<input>")
						.val("Upload CSV ...")
						.attr("type", "file")
						.attr("id", "upload-csv")
						.attr("name", "upload-csv")
						.attr("title", "Upload CSV ...")
						.change(function() {
							uploadCSV(); 
						})
					)
		);
	
	if (!(window.File && window.FileReader && window.FileList)) {
	 	$("#upload-csv").attr("disabled", true);
		Sexy.alert("Your browser doesn't suppurt The File APIs, you will not be able to upload csv files!");
	}
	
	initialiseGamertags();
	populateGroups();
}

function populateGroups() {
	$("#all-groups").empty();
	$("#new-group-text").val("");
	
	var object = {
		url: config.restHost + config.VerticaGamersGroups,
	  	data: {},
	  	method: "GET",
	  	dataType: "json",
	  	async: false,
	  	cache: false,
	};
	var dataFunctions = {
		getValue: function(d) {return d.Name},
		getTitle: function(d) {return d.Name},
		getText: function(d) {return d.Name},
		onChange: function(d) {
			populateGroup(d);
		}
	};
	
	block();
	var groups = getAjaxData(object);
	populateSelectElements(["all-groups"], groups, dataFunctions);
	unBlock();
}

function addGroup() {
	var newGroupName = $("#new-group-text").val();
	if (!newGroupName || (/[\<\>\*\%\&\:\?\\]/).test(newGroupName)) {
		Sexy.alert("Invalid Name!");
		$("#new-group-text").focus();
		return;
	}
		
	block();
	$.ajax({
		url: config.restHost + config.VerticaGamersGroups,
		type: "PUT",
		data: JSON.stringify({"Name": newGroupName, "Gamers":[]}),
		dataType: "json",
		contentType: "application/json",
		async: "true",
			
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
}

function deleteGroup() {

	var selectedGroupName = $("#all-groups").val();
	if (!selectedGroupName)
		return;
	
	block();
	$.ajax({
		url: config.restHost + config.VerticaGamersGroups + "/" + encodeURIComponent(selectedGroupName),
		type: "DELETE",
		data: {},
		dataType: "json",
		contentType: "application/json",
		async: "true",
			
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
}

function populateGroup(groupObj) {
	
	$("#group-selected-name").text(groupObj.Name);
	//$("#group-head button").remove();
	$("#group-gamertags").tokenInput("clear");
	
	 $.each(groupObj.Gamers, function(i, gamer) {
		 gamer["id"] = gamer.Platform;
		 gamer["name"] = gamer.GamerHandle + "(" + rosPlatformsDict[gamer.Platform] + ")",
		 
		 $("#group-gamertags").tokenInput("add", gamer);
	 });
	
}

function updateGroup() {
	var gamertags = $("#group-gamertags").tokenInput("get");
	
	gamertags = gamertags.map(function(d) {
		return {"GamerHandle": d.name.split("(")[0], "Platform": d.id};
	});
	
	//if (gamertags.length == 0)
	//	return;
	
	var selectedGroupName = $("#group-selected-name").text();
	if (!selectedGroupName)
		return;
	
	block();
	$.ajax({
		url: config.restHost + config.VerticaGamersGroups + "/" + encodeURIComponent(selectedGroupName),
		type: "POST",
		data: JSON.stringify({"Name": selectedGroupName, "Gamers":gamertags}),
		dataType: "json",
		contentType: "application/json",
		async: "true",
				
		success: function(data, textStatus, jqXHR) {
			//console.log("added");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: populateGroups,
	});
	unBlock();
	
}

function uploadCSV() {
	var filePairsDict = {};
	
	var validFileTypes = ["application/vnd.ms-excel", "text/plain"];
	
	try {
		var file = $("#upload-csv").prop("files")[0];
		
		if (validFileTypes.indexOf(file.type) == -1) {
			Sexy.error("Invalid File type, try csv or txt files");
			return;
		}
		
		fr = new FileReader();
	
		fr.onload = function () {
			$.each(this.result.split("\r\n"), function(i, d) {
				var pair = d.split(",").map(function(p) {return p.replace(/^\"/g, "").replace(/\"$/g, "").trim()});
				
				if (pair.length < 2)
					return;
				
				var gamerHandle = pair[0];
				pair[0] = pair[0].toLowerCase();
				filePairsDict[pair] = {
					"GamerHandle": gamerHandle,
					"Platform": getRosPlatformEnum(pair[1]) 
				};
				//console.log(pair);
			});
			
			populateGamertags(filePairsDict);
		};

		block();
		fr.readAsText(file);
	}
	catch(e) {
		console.log(e);
	}
	//console.log(file);
}

function populateGamertags(gamertagsDict) {
	
	// first validate the gamertags 
	$.ajax({
		url: config.restHost + config.VerticaGamersValidate,
		type: "POST",
		data: JSON.stringify(Object.keys(gamertagsDict).map(function(key) {return gamertagsDict[key]})),
		dataType: "json",
		contentType: "application/json",
		async: "false",
				
		success: function(data, textStatus, jqXHR) {
			var valid = [];
			var inValid = [];
			
			$.each(data, function(i, d) {
				valid.push({
					name: d.GamerHandle + "(" + rosPlatformsDict[d.Platform] + ")",
					id: d.Platform,
				});
				
				$("#group-gamertags").tokenInput("add",
					{
						name: d.GamerHandle + "(" + rosPlatformsDict[d.Platform] + ")",
						id: d.Platform,
					}
				);
				
				delete gamertagsDict[d.GamerHandle.toLowerCase() + "," + rosPlatformsDict[d.Platform]];
			});
			
			if (Object.keys(gamertagsDict).length > 0) {
				Sexy.alert("The following gamertags were not valid: <br>" 
						+ Object.keys(gamertagsDict).join("<br>"));
			}
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			Sexy.alert(this.url + "\n" + ajaxOptions + " " + xhr.status + " " + thrownError);
		},
		complete: unBlock,
	});
}

function initialiseGamertags() {
	
	$("#group-gamertags").tokenInput(
		config.restHost + config.VerticaGamersSearch,
		{
			theme: "facebook",
			hintText: "Type at least 3 characters to start searching",
			searchDelay: 1000,
            method: "GET",
            queryParam: "name",
            minChars: 3,
            //preventDuplicates: true,
              
            onResult: function(result) {
            	return result.map(
            		function(d) {
            			return {
            				name: d.GamerHandle + "(" + rosPlatformsDict[d.Platform] + ")",
            				id: d.Platform,
            			};
            		})
            		.sort(function(a, b) {return (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1});
            }

         }
	);
}
function getRosPlatformEnum(platformName) {
	var val = 0; // default 0 -PS3
	
	$.each(rosPlatformsDict, function(key, name) {
		if (name === platformName) {
			val = key;
			return;
		}
	});
	
	return val;
}
