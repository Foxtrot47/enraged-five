// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-status", 	// offline filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // builds
	     false, // game-types
	     false, // dates
	     false, // gamer-groups
	     true, // gamers-csv
	     ],
};

var rosPlatformsDict = getRosPlatformsDict();

var onText = "Online",
	offText = "Offline";

var reportOptions = {
	restEndpoint: config.onlineGamers,
	restEndpointAsync: config.onlineGamersAsync,
	
	processFunction: filterGamers,

	legendText: "Status List",

	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Gamer Tag", getValue: function(d) {return d.GamerHandle;} },
	    {title: "Platform", getValue: function(d) {return rosPlatformsDict[d.Platform];} },    
	    {
	    	title: "Status", 
	    	getValue: function(d) {return d.Status;},
	    	getClass: function(d) {return (d.Status == offText) ? "red" : "green";}
	    },
	],
	
	/* Groups */
	groups: [
	    {title: "Group's Gamers", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	hasReportArrayItemMoreInfo: false,
};

function filterGamers(data) {
	// Exit if no group is selected
	if (!$("#gamer-group").val())
		return;
	
	// Build a dictionary with the online users for efficient look up
	var onlineUsers = {};
	$.each(data, function(i, onlineUser) {
		onlineUsers[onlineUser.GamerHandle + "|" + onlineUser.Platform] = onlineUser;
	});
	
	// Get the selected group's gamers
	var groupGamers = JSON.parse($("#gamer-group").val());
	
	// For each group gamer check who was online/offline ;)
	var finalUsers = [];
	$.each(groupGamers, function(i, groupGamer) {
		// Opt out the gamers with different platform if one is selected
		if ($("#rosplatforms").val() && ($("#rosplatforms").val().indexOf(rosPlatforms[groupGamer.Platform].Name) == -1))
			return;
		
		if (!onlineUsers.hasOwnProperty(groupGamer.GamerHandle + "|" + groupGamer.Platform))
			groupGamer["Status"] = offText;
		else
			groupGamer["Status"] = onText;
		
		finalUsers.push(groupGamer)
	});
	
	return finalUsers;
}
