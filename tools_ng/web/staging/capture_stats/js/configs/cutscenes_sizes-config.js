//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-cs-build-only", 	// capture stats filtering header
	disabledFields:				// disables header fields 
		[
		 false, // build
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.perBuildCutscenes,
	restEndpointAsync: config.perBuildCutscenesAsync,
	returnedDataType: "xml",
	
	processFunction: convertFromXml,
	enableCSVExport: true,
	
	reportTabs: [
	{
		key: "Win32",
		value: 0,
	},
	{
		key: "Win64",
		value: 1,
	},
	{
		key: "Xbox360",
	    value: 2,
	},
	{
		key: "PS3",
	    value: 3,
	},
    ],
    
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Cutscene Name", getValue: function(d) {return d.Name; }},
	     
	     {
	    	 title: "Sections", 
	    	 getValue: function(d, tab) {
	    		 return (d.PlatformStats[tab]) ? d.PlatformStats[tab].Sections.length : 0; 
	    	},
	     },
	     {
	    	 title: "Physical Size (Bytes)", 
	    	 getValue: function(d, tab) {
	    		 return commasFixed2((d.PlatformStats[tab]) 
	    				 ? d.PlatformStats[tab].Sections.reduce(function(a, b) {return a + b.PhysicalSize}, 0) 
	    						 : 0); 
	    	},
	     },
	     {
	    	 title: "Virtual Size (Bytes)", 
	    	 getValue: function(d, tab) {
	    		 return commasFixed2((d.PlatformStats[tab]) 
	    				 ? d.PlatformStats[tab].Sections.reduce(function(a, b) {return a + b.VirtualSize}, 0) 
	    						: 0);
	    	},
	     },
	    
	],
	//reportArraySort: [[0, 0]],
	
   	// Group the results 
	groups: [
	     {title: "All Cutscenes", regexp: "", filterItem: 0}, 
	],

};


function convertFromXml(xml) {
	var cutscenes = [];
		
	$(xml).find("a\\:Cutscenes, Cutscenes").children("b\\:anyType, anyType").each(function(i, c) {
		var platformStats = [];
		$(c).children("a\\:m_platformStats, m_platformStats")
						.children("b\\:KeyValueOfPlatformanyTypeC4JpiMYj, KeyValueOfPlatformanyTypeC4JpiMYj").each(function(j, p) {
			
			var sections = [];
			$(p).children("b\\:Value, Value").children("a\\:Sections, Sections").children("b\\:anyType, anyType").each(function(k, s) {
				sections.push({
					"Index" : Number($(s).children("a\\:Index, Index").text()),
					"PhysicalSize" : Number($(s).children("a\\:PhysicalSize, PhysicalSize").text()),
					"VirtualSize" : Number($(s).children("a\\:VirtualSize, VirtualSize").text()),
				});
			});
			
			platformStats.push({
				"Key" : $(p).children("b\\:Key, Key").text(),
				"Sections" : sections,
			});
		});
		
		cutscenes.push({
			"Hash" : $(c).children("Hash").text(),
			"Name" : $(c).children("Name").text(),
			"PlatformStats" : platformStats,
			"BuildIdentifier" : $(c).children("a\\:BuildIdentifier, BuildIdentifier").text(),
			"m_exportZipFilepath" : $(c).children("a\\:m_exportZipFilepath, m_exportZipFilepath").text(),
			"m_isConcat" : $(c).children("a\\:m_isConcat, m_isConcat").text(),
		});
	});
	
	//console.log(cutscenes);
	return cutscenes;
}
