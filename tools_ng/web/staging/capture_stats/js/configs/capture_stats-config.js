var navigationTitle = "Capture Stats";
var navigationItems = [{
		title: "Home",
		href: "index.html",
		display: "inherit",
		config: "",
		subItems: [],
	},
	{
		title: "Report",
		href: "report.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	/* --
	{
		title: "Cutscenes Report",
		href: "cutscene_captures.html",
		//href: "#",
		display: "inherit",
		config: "js/configs/cutscenes_captures-config.js",
		subItems: [],
	},
	*/
	{
    	title: "Cutscenes Sizes",
		href: "cutscenes_sizes.html",
		config: "js/configs/cutscenes_sizes-config.js",
		subItems: [],
	},
	/* --
	{
		title: "Cutscenes Performance Table",
		href: "cutscenes_cs.html",
		config: "js/configs/cutscenes_cs-config.js",
		subItems: [],
	},
	*/
	/* --
	{
    	title: "Cutscenes Lights Table",
		href: "cutscenes_lights.html",
		config: "js/configs/cutscenes_lights-config.js",
		subItems: [],
	},
	*/
	/* --
	{
    	title: "Missions Performance Table",
    	href: "missions_cs.html",
    	display: "inherit",
    	config: "js/configs/missions_cs-config.js",
    	subItems: [],
    },
    {
    	title: "Missions Performance Graph",
    	href: "missions_cs_graph.html",
    	display: "inherit",
    	config: "js/configs/missions_cs-config.js",
    	subItems: [],
    },
    */
    /* --
	{
		title: "Memory Pools",
		href: "memory_pools.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
    /* --
	{
		title: "Memory Stores",
		href: "memory_stores.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	/* --
	{
		title: "Memory Skeletons",
		href: "memory_skeletons.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
    /* --
	{
		title: "Memory Usage",
		href: "memory_usage.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	/* --
	{
		title: "Clips Performance",
		href: "clips_performance.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	/* --
	{
		title: "Unused Clips",
		href: "unused_clips.html",
		//href: "#",
		display: "inherit",
		config: "",
		subItems: [],
	},
	*/
	
];

