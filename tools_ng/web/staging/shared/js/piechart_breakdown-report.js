// reportOptions variable comes from respective config file

// Array that stores objects for each element returned via rest

var chartsData;
var map;

var NVGraphs = new NVD3CustomGraphs();

var mainDOMId = "piechart";
var breakdownDOMId = "breakdown";

var lastSliceClicked = null;
var csvExportButton;

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body")
		.append(
			$("<div>").attr("id", mainDOMId)
				//.addClass((reportOptions.isClickable) ? "clickable" : "")
		)
		.append(
			$("<div>").attr("id", breakdownDOMId)
		);
	
	initSvg(mainDOMId);
	
	if (reportOptions.hasFriendlierNames) {
		$("#content")
			.append(
				$("<div>")
					.attr("id", "friendlier-names-field")
					.append(
						$("<input>")
							.attr("id", "friendlier-names")
							.attr("type", "checkbox")
							.attr("disabled", false)
							.prop("checked", true)
					)
					.append(
						$("<label>")
							.attr("for", "friendlier-names")
							.attr("title", "Check to Switch to Friedlier Names")
							.text("Friendlier Names")
							.click(function() {
								if (chartsData)
									drawCharts();
							})
					)
			);
	}
	
	if (reportOptions.hasBreakdownMap) {	
		$("#" + breakdownDOMId).parent().append(
			$("<div>")
				.attr("id", reportOptions.hasBreakdownMap)
				.addClass("hidden")
		);
		map = new SVGMap(reportOptions.hasBreakdownMap, project.map);
		if (reportOptions.mapVisible)
			map.show();
		else
			map.hide();
	}
	
	updateWidth();
	
	$("#filter").click(function() {
		csvExportButton = false; // Regenerate csv export data
		generateCharts();
	});
	
	generateCharts();
}

function generateCharts() {

	chartsData = [];
	
	// get the social club header filtering parameter values, headerAndFilters.headerType comes from local conf
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	if (!pValues)
		return;
	
	if (reportOptions.multipleRequests) {
		var endpointsArray = reportOptions.multipleRequests(pValues);
		
		var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
		req.sendMultipleAsyncRequest(endpointsArray, drawCharts);
	}
	else {
		if (reportOptions.hasExtraRestParams) {
			$.each(reportOptions.hasExtraRestParams, function(i, paramObj) {
				var value;
				if (typeof paramObj.value === "function") {
					value = paramObj.value();
				}
				else
					value = paramObj.value;
				pValues.Pairs[paramObj.key] = value;
			});
		}
	
		var req = new ReportRequest(config.restHost + reportOptions.restEndpoint,
			"json",
			config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			config.restHost + config.reportsQueryAsync);
		
		req.sendSingleAsyncRequest(pValues, drawCharts);
	}
			
} //end of generateCharts()

function cleanCharts() {
	cleanGraph(mainDOMId);
	cleanDetailedChart();
}

function cleanDetailedChart() {
	//$("#" + breakdownDOMId).hide("fade", {}, "normal");
	$("#" + breakdownDOMId).find("div").each(function() {
		cleanGraph($(this).attr("id"));
	});
	lastSliceClicked = null;
	
	if (map && !reportOptions.mapVisible)
		map.hide();
}

function updateWidth() {
	if (reportOptions.hasBreakdownMap) {
		$("#" + mainDOMId).css("width", "33%");
		$("#" + breakdownDOMId)
			.css("width", "33%");
		$("#" + reportOptions.hasBreakdownMap)
			.css("width", "33%");
	}
}

function drawCharts(data) {
	// If a processFunction function is specified
	if (data) {
		if (reportOptions.processFunction)
			chartsData = reportOptions.processFunction(data);
		else
			chartsData = data;
	}
	
	if (chartsData.length < 1) {
		cleanCharts();
		Sexy.alert(config.noDataText);
		return;
	}
	
	if (reportOptions.enableCSVExport && !csvExportButton)
		enableCsvExport();
	
	if (reportOptions.enablePNGExport)
		addExportPngButton(reportOptions.enablePNGExport);
	
	var graphOptions = reportOptions.main;
	// Construct the chart data in a format that nvd3 understands
	var datum = [{
		key: graphOptions.getPieLabel(chartsData),
		values: graphOptions.getValuesArray(chartsData),
		metadata: graphOptions.getMetadata(chartsData),
	}];
		
	NVGraphs.drawPiechart(datum, mainDOMId, graphOptions, 
			function(event) { if (reportOptions.isClickable) {
					lastSliceClicked = event.label;
					event.point["label"] = event.label; // Forward the label
					event.point["metadata"] = graphOptions.getMetadata(chartsData); // Forward the metadata

					if (
						(!reportOptions.breakdown.getValuesArray(event.point)
							|| !reportOptions.breakdown.getValuesArray(event.point).length)
						&& (
							reportOptions.breakdown.altLinks
							&& reportOptions.breakdown.altLinks.hasOwnProperty(graphOptions.getName(event.point))
						)
					)
						Sexy.info(reportOptions.breakdown.altLinks[graphOptions.getName(event.point)]);
					
					drawDetailedChart(event.point);
				}
			}
	);
	
	if (!reportOptions.isClickable)
		drawDetailedChart(chartsData);
	
	if (lastSliceClicked) {
		var availableLabels = datum[0].values.map(function(d) { return graphOptions.getName(d); });
		var searchIndex = availableLabels.indexOf(lastSliceClicked);
		
		if (searchIndex != -1) {
			drawDetailedChart(datum[0].values[searchIndex]);
		}
		else {
			// clean it cause the previous selected item is not available
			cleanDetailedChart();
		}
		
	}
		
}

function drawDetailedChart(selectedObject) {
	// Construct the chart data in a format that nvd3 understands
		
	var placeholderHeight = $("#" + breakdownDOMId).height();
	var detailedColumns = 2;

	var graphOptions = reportOptions.breakdown;
	var detailedObjects = (graphOptions.getAdditionalObjects) 
							? graphOptions.getAdditionalObjects(selectedObject)
							: graphOptions.getObjects(selectedObject);
		
	graphOptions.getAdditionalObjects = null // reset dynamic function
	
	if (!detailedObjects)
		return;
		
	// Clean not needed previous svg's
	var existingItems = $("#" + breakdownDOMId).find("div");
	if (existingItems.length > detailedObjects.length)
		existingItems.slice(detailedObjects.length).remove();
	
	$.each(detailedObjects, function(i, detailedObject) {
				
		var breakdownItemDOMId = breakdownDOMId + "_" + i;
		
		// This will add the svg on the first time
		if ($("#" + breakdownDOMId).find("#" + breakdownItemDOMId).size() == 0) {
			$("#" + breakdownDOMId)
				.append(
					$("<div>").attr("id", breakdownItemDOMId)
				);
			initSvg(breakdownItemDOMId);
		}
		
		$("#" + breakdownItemDOMId)
			.removeClass().addClass(function() {return (i%2) ? "float-right" : "float-left"})
			.css("width", function() {
				// give max width to the first element in odd number of detailed piecharts
				return (((i==0) && (detailedObjects.length%2)) || (detailedObjects.length == detailedColumns)) 
								? "100%" : Math.floor(100/detailedColumns) + "%";
			})
			.css("height",function() {
				return (detailedObjects.length == detailedColumns) 
					? Math.floor(placeholderHeight/ Math.round(detailedObjects.length))
					: Math.floor(placeholderHeight/ Math.round(detailedObjects.length / detailedColumns)) 
			});
				
		var datum = [{
			key: graphOptions.getPieLabel(detailedObject),
			values: graphOptions.getValuesArray(detailedObject),
			metadata: graphOptions.getMetadata(detailedObject),
		}];
	
		NVGraphs.drawPiechart(datum, breakdownItemDOMId, graphOptions, graphOptions.breakdownFunc);
		
	}); //end of each detailedObjects
	

}

function enableCsvExport() {
	
	var csvColumns = [" "];
	var csvRows = [];
	
	var selectedMetricObj;
	$("#content-description-radiometrics").find("input").each(function(metricIndex, metricObj) {
		selectedMetricObj = ($(metricObj).prop("checked")) ? metricObj : selectedMetricObj;
		$(metricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	
		populateCSVArrays(csvColumns, csvRows);
	});
	
	if ($("#content-description-radiometrics").find("input").size() == 0)
		populateCSVArrays(csvColumns, csvRows); // Run for the default setting
		
	// Reset checkbox selection
	if (selectedMetricObj) {
		$(selectedMetricObj).prop("checked", true);
		if (typeof setReportOptions !== "undefined")
			setReportOptions();
	}
	
	
	// Add the button in the DOM
	var csvFrameID = "piechart-breakdown-frame";
	$("#" + csvFrameID).remove();
			
	addCSVButton(reportOptions.enableCSVExport, 
			csvFrameID, 
			function() {
				exportToCSV(csvColumns, 
							csvRows, 
							csvFrameID, 
							getCSVFilename()); 
			});
		
	$("#" + csvFrameID).addClass("csv-right");
	csvExportButton = true;
}

function populateCSVArrays(csvColumns, csvRows) {
	// Gather the main piechart data
	var mainGraphOptions = reportOptions.main;
	var mainTotal = d3.sum(mainGraphOptions.getValuesArray(chartsData), 
						function(mainItem) {return mainGraphOptions.getValue(mainItem);}
					);
	$.each(mainGraphOptions.getValuesArray(chartsData), function(i, mainItem) {
		if (!csvRows[i]) {
			csvRows[i] = [];
			csvRows[i][0] = mainGraphOptions.getName(mainItem);
		}
		csvRows[i][csvColumns.length] = mainGraphOptions.getValue(mainItem);
		csvRows[i][csvColumns.length+1] = formatPercentage(mainGraphOptions.getValue(mainItem), mainTotal);
	});
	
	csvColumns.push(mainGraphOptions.getPieLabel(chartsData));
	csvColumns.push("%");
	
	var currentRowId = mainGraphOptions.getValuesArray(chartsData).length;
	
	var breakdownGraphOptions = reportOptions.breakdown;
	$.each(mainGraphOptions.getValuesArray(chartsData), function(j, mainItem) {
					
		$.each(breakdownGraphOptions.getObjects(mainItem), function(k, breakdownItems) {
			csvRows[++currentRowId] = [" ", " ", " "]; // Increase and add Empty Line
			
			currentRowId++; // Increase the row
			if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
				csvRows[currentRowId] = [];
				csvRows[currentRowId][0] = 
					(breakdownGraphOptions.getPieLabel(breakdownItems) || mainGraphOptions.getName(mainItem)).toUpperCase();
			}
			
			//console.log(csvRows[currentRowId][0]);
			csvRows[currentRowId] = csvRows[currentRowId].concat(
			     // use the breakdown pie label or the main piechart name if absent
			     mainGraphOptions.getPieLabel(chartsData),
			     "%"
			);
			
			var breakdownTotal = d3.sum(breakdownGraphOptions.getValuesArray(breakdownItems), 
					function(breakdownItem) {return breakdownGraphOptions.getValue(breakdownItem);}
			);
			$.each(breakdownGraphOptions.getValuesArray(breakdownItems), function(l, breakdownItem) {
				
				currentRowId++; // Increase the row
				
				if (!csvRows[currentRowId] && (csvRows[currentRowId] !== "")) {
					csvRows[currentRowId] = [];
					csvRows[currentRowId][0] = breakdownGraphOptions.getName(breakdownItem);
				}
				
				csvRows[currentRowId] = csvRows[currentRowId].concat([
				    breakdownGraphOptions.getValue(breakdownItem),
				    formatPercentage(breakdownGraphOptions.getValue(breakdownItem), breakdownTotal),
				]);
			});
			
		});
		
	});
}