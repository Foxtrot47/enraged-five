/*
 * Class CustomTooltip
 * 
 * Creates a custom tooltip with html content;
 * Requires JQuery and CustomTooltip.css.
 * 
 * @tooltipId - the html DOM tooltip id
 * @width - optional  width of the tooltip
 */

var CustomTooltip = function(tooltipId, width, height) {

    var _tooltipId = tooltipId,
    	_width = width,
    	_height = height;
    
    $("body").append("<div class='tooltip' id='" + tooltipId + "'></div>");

    var _tooltip = $("#" + _tooltipId);

    if (_width) {
    	_tooltip.css("width", _width);
    }
    else
    	_width = _tooltip.width();
    
    
    if (_height) {
    	_tooltip.height(_height);
    }
    else
    	_height = _tooltip.height(); 

   // tooltip.on("mouseleave", function(e) { hideTooltip(); });
   //hideTooltip();

    function showTooltip(content, event) {
    	_tooltip
            .html(content)
            .fadeIn("fast");

        updatePosition(event);
    }

    function hideTooltip(){
    	_tooltip.fadeOut("fast");
    }

    function updatePosition(event) {
    	
        var xOffset = 0,
            yOffset = 0,
            leftShadowMargin = 10,
            ttw = _width,
            tth = _height,
            wscrY = $(window).scrollTop(),
            wscrX = $(window).scrollLeft(),
            curX = (document.all) ? event.clientX + wscrX : event.pageX,
            curY = (document.all) ? event.clientY + wscrY : event.pageY,
            ttleft = Math.max(((curX - wscrX + xOffset*2 + ttw) > $(window).width()) ? curX - ttw - xOffset*2 - leftShadowMargin : curX + xOffset, wscrX + xOffset),
            tttop = Math.abs(((curY - wscrY + yOffset*2 + tth) > $(window).height()) ? curY - tth - yOffset*2 : curY + yOffset, curY + yOffset);

        _tooltip.css("top", tttop).css("left", ttleft);
    }

    return {
        showTooltip: showTooltip,
        hideTooltip: hideTooltip,
        updatePosition: updatePosition
    };

};
