//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-as", 	// asset stats filtering header
	disabledFields:				// disabled header fields 
		[
		 false, // platform
		 false, // level
		 false, // file-type
	     ],
};

var fixed = function(d) {return d.toFixed(2); }

var reportOptions = {
	restEndpoint: config.assetSizes,
	restEndpointAsync: config.assetSizesAsync,
	
	hasFilterInput: true,
	
	enableCSVExport: true,
	
	getReportArray: function(d) {return d},
	reportArrayItems: [
	     {title: "Name", getValue: function(d) {return d.SectionName;} },
	     {title: "Build", getValue: function(d) {return d.PerBuildStats.slice(-1)[0].Key; } },
	     {title: "Physical Size (KB)", getValue: function(d) {
	    	 	return fixed(d.PerBuildStats.slice(-1)[0].Value.PhysicalSize / 1024);
	    	 }
	     },
	     {title: "Virtual Size (KB)", getValue: function(d) {
	    	 	return fixed(d.PerBuildStats.slice(-1)[0].Value.VirtualSize / 1024);
	    	 }
	     },
	],
	reportArraySort: [[0, 0]], // [first item, ASC]
	
	hasReportArrayItemMoreInfo: false,
	
	groups: [
	     {title: "Asset Sizes List", regexp: "", filterItem: 0},
	],
	
	// Graph only related vars
	inputTextLabel: "Asset Sizes :: ",
	defaultInputText: "type a prefix to generate the chart and press enter!",
	
	graphGroupId: 0, // the id of the groups array for generating the graph only for that group
	// For extracting the arrays of the values for each series and for specific keys
	graphValuesArray: function(d, array) {
		//console.log(d.PerBuildStats.splice(-this.graphHistory));
		var shortArray = [];  
		$.each(d.PerBuildStats.splice(-this.graphHistory), function(i, b) {
			var inArray = $.inArray(Number(b.Key), array);
			if (inArray != -1) {
				// rememeber the graph need ids of the builds array 
				b.ID = inArray;
				shortArray.push(b);
			}
		})
		//console.log(shortArray);
		return shortArray;
	},
	// For passing a way to get the data we are interested in ploting
	graphObject: {
		 getKey: function(d) {return Number(d.ID) },
		 getValue: function(d) {
			 var selectedMetric = $("input[name=asset-sizes-radio]:radio:checked").val();
			 console.log(selectedMetric);
			 
			 return Number(d.Value[selectedMetric]) / 1024
		 },
		 xLabel: "Build",
		 yLabel: "KB",
		 leftMargin: 100,
	},
	
	graphHistory: 20, // the depth of historical values
	graphMinChars: 2, // min text filtering characters
};

// Asset Sizes Only
$("#sub-header input[name=asset-sizes-radio]:radio").change(function () {
	drawGraph();
});
