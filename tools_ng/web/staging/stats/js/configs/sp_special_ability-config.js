// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "SP0_SPECIAL_ABILITY_ACTIVE_TIME",
	   "SP1_SPECIAL_ABILITY_ACTIVE_TIME",
	   "SP2_SPECIAL_ABILITY_ACTIVE_TIME",
     ],
     name: "Average Time",
     altNames: [
       	"Michael",
     	"Franklin",
     	"Trevor",
     ],
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     isAverage: true,
     //isTime: true,
     unit: "secs"
},
{
	types: [
	   "SP0_SPECIAL_ABILITY_ACTIVE_NUM",
	   "SP1_SPECIAL_ABILITY_ACTIVE_NUM",
	   "SP2_SPECIAL_ABILITY_ACTIVE_NUM",
     ],
     name: "Times Used",
     requestSubString: "_",
     bucketSize: 5,
     singleMetric: true,
     unit: "times",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	isClickable: true,
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name; }, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value/1000; },
		getObject: function(d) { return (d); },
		
		// unit: "players" - not always
		lrMargin: 15,
	},
	// This is a lingraph
	breakdown: {
		elementId: "line-area-chart",
		backgroundColour: "transparent",
		lineColour: config.chartColour1,
		textColour: "#000000",
		gridColour: "#333333",
					
		name: function(d) { return d.name; },
		//value: function(d) { return d.value; },
		value: function(d) { 
			return ((d.TotalUsers) ? (Number(d.YValue)/d.TotalUsers)*100 : 0); 
		},
		
		fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
		label: function(d) {return ((d.label) ? d.label : this.name(d)); },	
		//xLabel: profileStatList[1].name,
		xLabel: profileStatList[1].name,
		updateXLabel: function(d) {return Object.keys(d)[0]; },
		yLabel: "Percentage of Players (%)",
		//yLabel: "Number of Players",
			
		orientation: "horizontal",
		margin: {top: 10, right: 10, bottom: 10, left: 10},
			
		hideLegend: true,
		legend: {height: 30, width: 180, rectWidth: 18},
		legendDataConst : [],
		legendDataVar: {label : "", colour: config.chartColour1}, // keep this colour in sync with lineColour 
			
		valueTooltipContent: function(d, b) {
			var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + profileStatList[1].name + "" + "</div><br /><br />"
				+ "<table>"
						+ "<tr><td>Percentage of Players:</td><td class='right'>" + commasFixed2(this.value(d)) + " %</td></tr>"
						+ "<tr><td>Number of Players:</td><td class='right'>" + commasFixed2(Number(d.YValue)) + "</td></tr>"
						+ "<tr><td>Total Players:</td><td class='right'>" + commasFixed2(d.TotalUsers) + "</td></tr>"
					+ "</table>";
				return content;
		},
		
		getObject: function(d) {return d.breakdown;},

	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	     	
	// Prepare for piechart
	
	pieValues = [];
	$.each(data[0], function(j, typeResult) {
		pieValues = pieValues.concat(typeResult.response.map(function(d) {
	   			d["name"] = profStat.getName(d, profileStatList[0], j); 
	   			d["value"] = profStat.getValue(d, profileStatList[0], j);
	   			return d;
	   		})
		);
	});
	$.each(data[1], function(j, typeResult) {
		var linegraphKey = profileStatList[1].name + " (" + pieValues[j].name + ")";
		
		var totalUsers = 0;
		typeResult.response.map(function(r) {totalUsers += Number(r.YValue); });
		
		pieValues[j]["breakdown"] = {};
		pieValues[j]["breakdown"][linegraphKey] =
					typeResult.response.map(function(d) {
						d["name"] = profStat.getName(d, profileStatList[1], j); 
						d["value"] = profStat.getValue(d, profileStatList[1], j);
						d["TotalUsers"] = totalUsers;
						return d;
					});
	});
	
	return	{
		   	"label": profileStatList[0].name,
		   	"values": pieValues,
		   	"metadata": {"unit": profileStatList[0].unit},
		};
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
