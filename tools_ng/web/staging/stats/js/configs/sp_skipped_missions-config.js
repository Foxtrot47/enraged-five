// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "TIMES_MISSION_SKIPPED",
     ],
     name: "Used Shit-Skip",
     altNames: [
        "No",
        "Yes",
     ],
     requestSubString: "_",
     bucketSize: 1,
     singleMetric: true,
     groupNonZero: true,
     
     unit: "players",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: true,
		donut: true,
		donutLabelsOutside: false,
		sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name}, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value; },
		getObject: function(d) { return d; },
		
		lrMargin: 30,
	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
	
	var statName = getStatNames(profileStatList[0]).join(",");
	
	pValues.Pairs["StatNames"] = statName;
	pValues.Pairs["BucketSize"] = profileStatList[0].bucketSize;
	//pValues.Pairs["StartDate"] = null;
		
	var endpointObjects = [
		{
			restUrl: config.restHost + reportOptions.restEndpoint,
			restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	   	{
			restUrl: config.restHost + config.missionsStats,
			restAsyncUrl: config.restHost + config.missionsStatsAsync + pValues.ForceUrlSuffix,
			pValues: [pValues],
	   	},
	];
			
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	profileStatData = profStat.groupNonZero(data[0][0].response);
	profileStatData.map(function(d) {
			d["name"] = profStat.getName(d, profileStatList[0]); 
			d["value"] = profStat.getValue(d, profileStatList[0]);
			return d;
		});
	
	array.push(
		{
		   	"label": profileStatList[0].name,
		   	"values": profileStatData,
		   	"metadata": {"unit": profileStatList[0].unit}
		}
	);
	
	telemetryData = data[1][0].response.map(function(d) {
		d["name"] = d.MissionName; 
		d["value"] = d.TotalSkipped;
		return d;
	});
		
	array.push(
		{
		   	"label": "Per Mission",
		   	"values": telemetryData,
		   	"metadata": {"unit": "skips"}
		}
	);
	
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
