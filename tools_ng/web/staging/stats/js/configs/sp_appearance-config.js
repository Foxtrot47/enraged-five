// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "SP0_MONEY_SPENT_IN_CLOTHES",
	   "SP1_MONEY_SPENT_IN_CLOTHES",
	   "SP2_MONEY_SPENT_IN_CLOTHES",
     ],
     altNames: [
     	"Michael",
 		"Franklin",
 		"Trevor",
 	 ],
     name: "Clothes (Avg)",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     isAverage: true,
     unit: "$ Spent",
},
{
	types: [
	   "SP0_MONEY_SPENT_ON_TATTOOS",
	   "SP1_MONEY_SPENT_ON_TATTOOS",
	   "SP2_MONEY_SPENT_ON_TATTOOS",
     ],
     altNames: [
     	"Michael",
 		"Franklin",
 		"Trevor",
 	 ],
     name: "Tattoos (Avg)",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     isAverage: true,
     unit: "$ Spent",
},
{
	types: [
	   "SP0_MONEY_SPENT_ON_HAIRDOS",
	   "SP1_MONEY_SPENT_ON_HAIRDOS",
	   "SP2_MONEY_SPENT_ON_HAIRDOS",
     ],
     altNames: [
     	"Michael",
 		"Franklin",
 		"Trevor",
 	 ],
     name: "Hair Styles (Avg)",
     requestSubString: "_",
     bucketSize: null,
     singleMetric: true,
     isAverage: true,
     unit: "$ Spent",
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	
	// This is a piechart
	main: {
		legend: false,
		pieLabelsOutside: false,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		//sortByValueDesc: true,

		getPieLabel: function(d) { return d.label; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) {return d.name; }, // this is added to the results
		// function to calculate the value from the rest data
		getValue: function(d) { return d.value; },
		getObject: function(d) { return (d); },
		
		// unit: "players" - not always
		lrMargin: 15,
	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var array = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		
		var values = [];
		
		$.each(data[i], function(j, typeResult) {
			values = values.concat(typeResult.response.map(function(d) {
		   			d["name"] = profStat.getName(d, availableChart, j); 
		   			d["value"] = profStat.getValue(d, availableChart, j);
		   			return d;
		   		})
			);
		});
		
		array.push(
			{
			   	"label": availableChart.name,
			   	"values": values,
			   	"metadata": {"unit": availableChart.unit},
			}
		);
	});
	
	return array;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
