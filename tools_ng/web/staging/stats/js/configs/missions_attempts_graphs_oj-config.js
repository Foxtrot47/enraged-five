// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var currentStat = "Average Attempts";

var reportOptions = {
		
	restEndpoint: config.missionsStats,
	restEndpointAsync: config.missionsStatsAsync,
	
	processFunction: convertToDict,
		
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
    
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
			
	name: function(d) { return d.MissionName; },
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	value: function(d) {return (round2(d.TotalAttempts/d.NumGamersAttempted)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 180, rectWidth: 18},
	
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, 
	
	valueTooltipContent: function(d, b) {
		var html = "<div class='title'>" + this.fullName(d, b) + "</div><br />"
				+ "<table>"
					+ "<tr><td>Average Attempts: </td><td class='right'>" + this.value(d) + "</td></tr>"
					+ "<tr><td>Total Attempts: </td><td class='right'>" + d.TotalAttempts + "</td></tr>"
					+ "<tr><td>Total Players: </td><td class='right'>" + d.NumGamersAttempted + "</td></tr>";
								
			html += "</table>";

		return html;
	},
		              
};

var regExp = project.spMissionGroups[3].regexp;

function convertToDict(array) {
	var dict = {};
	
	dict[currentStat] = array.filter(function(d) {
			return (d.MissionName.match(regExp)); 
	 	})
	 	.sort(function(a, b) {
	 		return (a.MissionName < b.MissionName) ? -1 : 1;
	 	});
	return dict;
}
