// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-tableau", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // gamers
	     true, // game-types
	     false, // dates+builds
	     ],
};

var reportOptions = {
	tableauEndpoint: config.tableauEndpoint,
	tableauReportDev: "SessionReports_2/SessionsWithGamertagFilter",
	tableauReportProd: "SessionReports_1/SessionsWithGamertagFilter",
};