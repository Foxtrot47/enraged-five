// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-debug", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // include-debug
	     false, // dates+builds
	     ],
};

var currentStat = "Average Cash Made";

var reportOptions = {
	restEndpoint: config.hourlyCashReport,
	restEndpointAsync: config.hourlyCashReportAsync,
	
	description: "Average Cash earned per hour across all selected players, where Hour 1 represents the first " 
			+ "hour of play for each player, regardless of when that was.<br />"
			+ "Please note: For clearest results, search by Build.  If using date ranges, set the date to 01.10.13",
	
	enableCSVExport: "content-description-controls",
	enablePNGExport: "content-description-controls",
	//graphTitle: currentStat,
	
	chartGroups: {
		getGroups: function(d) {return [d]}, // one group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
	    title: currentStat,
	    colour: config.chartColour1,
	    getName: function(d, i) {return ("HOUR " + (d.GameplayHour + 1));},
	    getValue: function(d) {return d.CashEarned },
	    getObject: function(d) {return d},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
				+ "<tr><td>" + currentStat + ": </td><td class='right'>" + cashCommasFixed(e.point.Object.CashEarned) + "</td></tr>";
		
		html += "</table>"; 
		
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(d);
	},
	units: "",
	
	chartLeftMargin: 80,
};
