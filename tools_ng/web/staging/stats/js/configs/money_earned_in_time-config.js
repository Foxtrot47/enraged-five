// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // dates+builds
	     ],
};

var timeInterval = 60; // one hour in minutes

var reportOptions = {
	restEndpoint: config.profileStatsHistorical,
	restEndpointAsync: config.profileStatsHistoricalAsync,
	
	processFunction: function(d) {return [d]}, // no multiple keys
	
	hasExtraRestParams: [
		{
			key: "StatNames",
			value: getStatNames,
		},
		{
			key: "SampleIntervalMinutes",
			value: timeInterval,
		},
	],
	
	graphId: "graph",
	getKey: function() {return "Money Earned in Time"}, // No key
	getValues: function(d) {return d}, // No nested array for values
	
	graphObject: {
		
		getX: function(d, i) {return i},
		xLabel: "Time Intervals of " + timeInterval + " mins",
		
		getY: function(d) {return Number(d.Value)},
		yLabel: "Money Earned",
		
		leftMargin: 80,
		
		tickValues: function(a) {
			return d3.range(a.length); 
		},
		tickFormat: function(a, d, i) {
			return (a[parseInt(d)] ? a[parseInt(d)].Interval : d);
		},
		
		rotateLabels: -45,
	},
};

function getStatNames(gameTypes) {
	if (!gameTypes.join())
		gameTypes = config.gameTypes;
	
	var profStat = new ProfileStats();
	var statNames = (profStat.constructStatNames({requestSubString: "_",}, "TOTAL_CASH_EARNED", gameTypes));
	return statNames.join(",");
}

