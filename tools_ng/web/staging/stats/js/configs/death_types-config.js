//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     false, // game-types
	     false, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
/*
{
	types: [
	"SP0_TOTAL_PLAYING_TIME,SP1_TOTAL_PLAYING_TIME,SP2_TOTAL_PLAYING_TIME",
	//"MP0_TOTAL_PLAYING_TIME,MP1_TOTAL_PLAYING_TIME,MP2_TOTAL_PLAYING_TIME,MP3_TOTAL_PLAYING_TIME,MP4_TOTAL_PLAYING_TIME"
	//"PLAYING_TIME",
	"MP_PLAYING_TIME",
	],
	altNames: [
    "Singleplayer",
    "Multiplayer",
	],
	name: "SP Vs MP Playing Time",
	requestSubString: "",
	description: "Single versus multiplayer total playing time in hours",
	
	bucketSize: null,
	singleMetric: true,
	
	label: "Hours",
	unit: "hours",
	
	convertToHours: true,
},
*/
{
	types: [
	"DROWNING",
	//"DROWNINGINVEHICLE",
	"EXPLOSION",
	"FALL",
	"FIRE",
	"ROAD"
	],
	requestSubString: "_DIED_IN_",
	name: "Death Types",
	description: "No of deaths for each death type",
	
	label: "No of Deaths",
	unit: "deaths",

	bucketSize: null,
},
];

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
			
	processFunction: formatData,
		
	availableCharts: profileStatList,
	multipleRequests: generateEndpoints,
		
	isClickable: false,
	
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	
	description: profileStatList[0].description,
	
	// This is a piechart
	main: {
		title: profileStatList[0].name,
			
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getPieLabel: function(d) { return this.title; },
		getValuesArray: function(d) { return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) { return d.name;},
		getValue: function(d) { return d.value; },
		getObject: function(d) { return d; },
		
		lrMargin: 20,
	},
	// This is the breakdown barchart
	breakdown: {
		title: profileStatList[0].name,
		
		legend: false,
		pieLabelsOutside: true,
		labelSunbeamLayout: false,
		donut: true,
		donutLabelsOutside: true,
		
		sortByValueDesc: true,

		getLabel: function(d) { return this.title; },
		getColor: function(d) { return d.color; },
		getValuesArray: function(d) {return d.values; },
		getMetadata: function(d) {return d.metadata; },
		
		// function to get the  name from the rest data
		getName: function(d) { return d.name; },
		getValue: function(d) { return d.value; },
		
		getYLabel: function(d) { return profileStatList[0].label; },
		
		matchColoursFromPieElement: "piechart",
				
		leftMargin: 120,
		
		getObject: function(d) { return d; },
	},
	
}

function generateEndpoints(pValues) {
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var pieValues = [];
	$.each(data[0], function(j, typeResult) {
		pieValues.push({
			"name": profStat.getName(typeResult.response[0], profileStatList[0], j), 
			"value": profStat.getValue(typeResult.response[0], profileStatList[0], j),
		});
	});
	
	// Return an object
	return {
		"label": profileStatList[0].name,
		"values": pieValues,
		"metadata": {"unit": profileStatList[0].unit},
	};

}

function getStatNames(profileStat) {
	
	var gameTypes = ($("#game-types").val()) ? $("#game-types").val() : config.gameTypes;
	var character = ($("#character").val()) ? $("#character").val() : null;
	
	var statNames = [];
	
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, gameTypes, character).join(",")
		);
	});
		
	return statNames;
}

