// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc-debug", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // include-debug
	     false, // dates+builds
	     ],
};

var currentStat = "Average RP Made";

var reportOptions = {
	restEndpoint: config.hourlyCashReport,
	restEndpointAsync: config.hourlyCashReportAsync,
	
	//processFunction: calcRPEarned,
	
	enableCSVExport: "content-description-controls",
	enablePNGExport: "content-description-controls",
	//graphTitle: currentStat,
	
	chartGroups: {
		getGroups: function(d) {return [d]}, // one group
		getGroupName: function(d) {return ""},
	    getGroupValues: function(d) {return d},
   	},
	
	bars: [
	{
	    title: currentStat,
	    colour: config.chartColour2,
	    getName: function(d, i) {return ("HOUR " + (d.GameplayHour + 1));},
	    getValue: function(d) {return d.XPEarned },
	    getObject: function(d) {return d},
	},
	],
		
	tooltipContent: function(key, x, y, e, graph) {
		//console.log(e);
		
		var html = "<h3>" + x + "</h3><br />"
				+ "<table>"
				+ "<tr><td>" + currentStat + ": </td><td class='right'>" + commasFixed2(e.point.Object.XPEarned) + "</td></tr>";
		
		html += "</table>"; 
		
		return html;
	},

	yTickFormat: function(d) {
		return commasFixed2(d);
	},
	units: "",
	
	chartLeftMargin: 80,
};

/*
function calcRPEarned(data) {
	$.each(data, function(i, d) {
		d["XPEarned"] = d.XPTotal - ((i>0) ? data[i-1].XPTotal : 0); 
	});
	return data;
}
*/