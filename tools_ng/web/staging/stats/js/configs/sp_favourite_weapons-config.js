// Grab the list of valid weapon heldtime profile stats - the variable name is weapons
getScriptSync("/stats/js/configs/weapons_heldtime_list.js");
//make a lookup dictionary
var heldtimeWeaponsDict = {};
weaponsHeldtime.map(function(d) {
	return heldtimeWeaponsDict[d] = d;
});

// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true, false, true], // dates+builds
	     ],
};

var profStat = new ProfileStats();
var profileStatList = [];
var weaponCategories = getWeaponCategoriesEnums();
var weaponsAll = getWeaponsAll();

$.each(weaponsAll, function(i, weapon) {
	if ((weaponCategories[weapon.Category].Name == "Unarmed") 
		|| !weapon.ProfileStatName
		|| !heldtimeWeaponsDict.hasOwnProperty(weapon.ProfileStatName))
	//if (weaponCategories[weapon.Category].Name == "Unarmed")
		return;
	
	profileStatList.push(
		{
			types: [weapon.ProfileStatName + "_HELDTIME"],
			name: (weapon.FriendlyName) ? weapon.FriendlyName : weapon.RuntimeName,
			category: weaponCategories[weapon.Category].Name,
			requestSubString: "_",
		    bucketSize: null,
		    //singleMetric: true,
		    singleOnly: true,
		    unit: "hours",
		}
	)
});

var reportOptions = {
	restEndpoint: config.profileStatsCombinedDiff,
	restEndpointAsync: config.profileStatsCombinedDiffAsync,
	//restEndpoint: config.profileStatsCombined,
	//restEndpointAsync: config.profileStatsCombinedAsync,	
		
	processFunction: formatData,
	
	availableCharts: profileStatList,	
	multipleRequests: generateEndpoints,
	
	/* Tabular Report conf */
	
	enableCSVExport: true,
	
	// No report summary info
	reportSummaryTitle: false,
	
	getReportArray: function(d) {
		return d;
	},
	reportArrayItems: [
	    {title: "Category", getValue: function(d) {return d.category;} },
	    {title: "Favourite Weapon", getValue: function(d) {return d.name;} },
	    {title: "Time Held", getValue: function(d) {return formatSecsWithDays(d.value/1000) ;} }, 
	],
	//reportArraySort: [[0, 0]], // [first item ASC]
	
	/* Groups */
	groups: [
	    {title: "Favourite Weapons per Class", regexp: "", filterItem: 0},
	],
	
	/* First level breakdown */
	
	hasReportArrayItemMoreInfo: false,
	
};

function generateEndpoints(pValues) {
	// store the obj to local for processing the results
	//requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var statNames = getStatNames(availableChart);
		$.each(statNames, function(j, statName) {
			var copiedPvalues = $.extend(true, {}, pValues);
			
			copiedPvalues.Pairs["StatNames"] = statName;
			copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
			pValuesArray.push(copiedPvalues);
		});
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		)
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	
	var dict = {};
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
			
		var currentResult = {
			"category": availableChart.category,
			"name": availableChart.name,
			"value": Number(data[i][0].response[0].Total),
		}
		
		if (!dict.hasOwnProperty(availableChart.category)) {
			dict[availableChart.category] = currentResult;
		}
		else {
			dict[availableChart.category] = 
				(dict[availableChart.category].value > currentResult.value) ? dict[availableChart.category] : currentResult;
		} 
	});
	
	return dict;
}

function getStatNames(profileStat) {
	
	var statNames = [];
	$.each(profileStat.types, function (i, profileStatType) {
		statNames.push(
			profStat.constructStatNames(profileStat, profileStatType, config.gameTypes).join(",")
		);
	});
		
	return statNames;
}
