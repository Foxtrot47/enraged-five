// set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     true, // character
	     [true,false,true], // dates+builds
	     ],
};

var profStat = new ProfileStats();

var profileStatList = [
{
	types: [
	   "SP_AMBIENT_SWITCH_COUNT",
	   "SP0_TOTAL_PLAYING_TIME,SP1_TOTAL_PLAYING_TIME,SP2_TOTAL_PLAYING_TIME",
     ],
     name: "Switches per Hour (During Total Playing Time)",
     requestSubString: "_",
     bucketSize: 1 / config.anHourInMillisecs, // Bucket of 1 switch per hour in millisecs,
     perHour: true,
     singleMetric: true,
     //isDivided: true,
     //showOnlyLowerRange: true,
     //unit: "players",
},
];

var currentStat = profileStatList[0].name;

var reportOptions = {
	restEndpoint: config.profileStatsDividedDiff,
	restEndpointAsync: config.profileStatsDividedDiffAsync,
	
	processFunction: formatData,
	availableCharts: profileStatList,
	
	multipleRequests: generateEndpoints,
	
	enableCSVExport: "content-description",
	enablePNGExport: "content-description",
	//graphTitle: currentStat,
	
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
				
	name: function(d) { return d.name; },
	value: function(d) { 
		return ((d.TotalUsers) ? (d.value/d.TotalUsers)*100 : 0); 
	},
	
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: currentStat,
	yLabel: "Percentage of Players (%)",
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	hideLegend: true,
	legend: {height: 30, width: 180, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : currentStat, colour: config.chartColour1}, // keep this colour in sync with lineColour 
		
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "<br />" + currentStat + "" + "</div><br />"
			+ "<table>"
					+ "<tr><td>Percentage of Players:</td><td class='right'>" + commasFixed2(this.value(d)) + " %</td></tr>"
					+ "<tr><td>Number of Players:</td><td class='right'>" + commasFixed2(Number(d.YValue)) + "</td></tr>"
					+ "<tr><td>Total Players:</td><td class='right'>" + commasFixed2(d.TotalUsers) + "</td></tr>"
				+ "</table>";
			return content;
	},
};

function generateEndpoints(pValues) {
	//$("#date-from").addClass("hidden");
	//$("#date-from").parent().addClass("hidden");
	
	// store the obj to local for processing the results
	// requestPValues = pValues;
		
	var endpointObjects = [];
	
	$.each(reportOptions.availableCharts, function(i, availableChart) {
		var pValuesArray = [];
		
		var copiedPvalues = $.extend(true, {}, pValues);
			
		copiedPvalues.Pairs["NumeratorStatNames"] = availableChart.types[0];
		copiedPvalues.Pairs["DenominatorStatNames"] = availableChart.types[1];
		copiedPvalues.Pairs["BucketSize"] = availableChart.bucketSize;
			
		pValuesArray.push(copiedPvalues);
		
		endpointObjects.push(
		   	{
				restUrl: config.restHost + reportOptions.restEndpoint,
				restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues.ForceUrlSuffix,
				pValues: pValuesArray,
		   	}
		);
		
	});
		
	return endpointObjects;
}

function formatData(data) {
	var dict = {};
	
	var array = data[0][0].response;
	
	// Calc the total users
	var totalUsers = 0;
	array.map(function(r) {totalUsers += Number(r.YValue); });
	
	array.map(function(d) {
	   	d["name"] = profStat.getName(d, profileStatList[0]); 
	   	d["value"] = profStat.getValue(d, profileStatList[0]);
	   	d["TotalUsers"] = totalUsers;
	   	
	   	return d;
	 });
	
	dict[reportOptions.yLabel] = array;
		
	return dict;
}
