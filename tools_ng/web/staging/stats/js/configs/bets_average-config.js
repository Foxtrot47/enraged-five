//set true for disabled filter
var headerAndFilters  = {
	headerType: "header-sc", 	// social club filtering header
	disabledFields:				// disables header fields 
		[
		 false, // platforms
	     false, // locations
	     false, // age
	     false, // gamers
	     true, // game-types
	     false, // character
	     false, // dates+builds
	     ],
};

var commasFixed2 = function(d) { return d3.format(",")(d.toFixed(2)); };
// Will make use of some profile stat functions since the returned results have the same format
var profStat = new ProfileStats();

var items = [
	{
		metric: "Average Winning Bets" ,
		val: function(d) {return (d.GamerCount) ? d.WinningBetsMade/d.GamerCount : 0 ;},
	},
   	{
   		metric: "Average Bets" ,
   		val: function(d) {return (d.GamerCount) ? d.TotalBetsMade/d.GamerCount : 0 ;},
   	},
];

var statConf = {
     //bucketSize: 0.1,
	 bucketSize: 5,
     //fillWithEmptyBuckets: true,
     roundPrecision: 1,
};

var reportOptions = {
	restEndpoint: config.averageBets,
	restEndpointAsync: config.averageBetsAsync,
	
	processFunction: convertToDict,
	
	hasExtraRestParams: 
	[
	 	{
	 		key: "BucketSize",
	 		value: statConf.bucketSize,
	 	},
	],
	
	enablePNGExport: "content-description",
	enableCSVExport: "content-description",
	//graphTitle: "Average Bets",
		
	/* Line graph related */
	elementId: "line-area-chart",
	backgroundColour: "#ffffff",
	lineColour: config.chartColour1,
	textColour: "#000000",
	gridColour: "#333333",
					
	name: function(d) { return d.name; },
	value: function(d) { return Number(d.value); },
		
	fullName: function(d, extra) { return ((extra) ? this.name(d) + "<br />(" + extra + ")" : this.name(d)); },
	label: function(d) {return ((d.label) ? d.label : this.name(d)); },
	xLabel: "Size of Bets ($)",
	yLabel: "Average No of Bets",
			
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 10},
			
	hideLegend: false,
	legend: {height: 30, width: 150, rectWidth: 18},
	legendDataConst : [],
	legendDataVar: {label : "", colour: config.chartColour1}, // keep this colour in sync with lineColour 
			
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>$ " + this.fullName(d, b) + "</div><br />"
			+ "<table>";
		
			$.each(items, function(i, item) {
				content += ("<tr><td>" + item.metric + ":</td><td class='right'>" 
							+ commasFixed2((item.val(d.values)) ? item.val(d.values) : 0) + "</td></tr>");
			});
			
			content += "</table>";

		return content;
	},

};

function convertToDict(array) {
	
	var dict = {};
	
	// populate empty ones
	//array = profStat.processBucketResults(array, statConf);
	
	array.map(function(d) {
		var name = d.Bucket;
		
		$.each(items, function(i, item) {
			if (!dict.hasOwnProperty(item.metric))
				dict[item.metric] = [{ name: name, value: (item.val(d)) ? item.val(d) : 0, values : d}];
			else
				dict[item.metric].push({ name: name, value: (item.val(d)) ? item.val(d) : 0, values : d});
		});
		
	});
	
	return dict;
}

