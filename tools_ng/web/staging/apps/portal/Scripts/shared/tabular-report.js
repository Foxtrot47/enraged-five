﻿function generateTabularReport(elementId, reportOptions, dataArray) {

    var _downLoadCsvSpanId = "download-csv-span";

    var titleTable = $("<table>");
    var titleTableHeader = $("<th>");

    titleTableHeader = $("<th>").addClass("title");
    titleTableHeader
        .append(
            $("<span>")
                .text(reportOptions.reportTitle)
        );
    if (reportOptions.csvDownloadUrl) {
        titleTableHeader
            .append(
                $("<span>")
                    .addClass("float-right")
                    .attr("id", _downLoadCsvSpanId)
                    .append(
                        $("<input>")
                            .addClass("red-button hand")
                            .attr("type", "button")
                            .attr("title", "Download as CSV")
                            .val("Download CSV")
                            .click(function (e) {
                                e.preventDefault();
                                window.location.href = reportOptions.csvDownloadUrl;
                            })
                    )
            );
    }

    titleTable
        .append(
            $("<tr>").append(titleTableHeader)
        );
    titleTable.appendTo($("#" + elementId));

    $.each(dataArray, function (i, dataModule) {
        // One title table for each module
        var moduleTitleTable = $("<table>");
        moduleTitleTable.addClass("title-only")
            .append(
                $("<tr>").append(
                    $("<th>")
                        .addClass("title")
                        .text(reportOptions.getKey(dataModule))
                )
            );
        moduleTitleTable.appendTo($("#" + elementId));

        // One table for each data module
        var moduleTable = $("<table>");

        // Module's header row based on the available metrics
        var moduleTableHeader = $("<tr>");
        moduleTableHeader.append(
                $("<th>").text(reportOptions.xLabel)
            )
        $.each(reportOptions.availableMetrics, function (am, metric) {
            moduleTableHeader.append(
                $("<th>").text(metric.title)
            )
        });
        moduleTableHeader.appendTo(moduleTable);

        // Create a row for each value object and get the respective exact value for each metric column
        $.each(dataModule.values, function (dmv, value) {
            var moduleTableRow = $("<tr>");
            $.each(reportOptions.availableMetrics, function (am, metric) {
                if (am == 0) {
                    var xLabel = reportOptions.getX(value);
                    if (reportOptions.formatX) {
                        xLabel = reportOptions.formatX(xLabel);
                    }
                    moduleTableRow.append(
                        $("<td>").text(xLabel)
                    )
                }
                moduleTableRow.append(
                    $("<td>").text((metric.formatY) ? metric.formatY(metric.getY(value)) : metric.getY(value))
                )
            });
            moduleTableRow.appendTo(moduleTable);
        });

        moduleTable.appendTo("#" + elementId);
    });
}