﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <appSettings>
    <add key="webpages:Version" value="2.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="AppTitle" value="R* Tools Portal" />
    <add key="AppTitleSeparator" value=" :: " />
    <add key="AppDarkIconPath" value="~/Content/images/icon-rockstar-circle-dark.png" />
    <add key="AppRedIconPath" value="~/Content/images/icon-rockstar-circle-red.png" />
    <add key="AppLogIconPath" value="~/Content/images/icon-log.png" />
    <add key="AppLogoPath" value="~/Content/images/logo-rockstar-games.png" />
    <add key="AppDataFolder" value="App_Data" />
    <add key="ProjectsXML" value="X:\PortalProjects.xml" />
    <add key="ServerHostTcpFormat" value="net.tcp://{0}:7010" />
    <add key="FileTransferController" value="FileTransfer.svc" />
    <add key="AutomationController" value="automation.svc" />
    <add key="UniversalLogFilename" value="job.ulog" />
    <add key="UniversalLogFileMimeType" value="text/xml" />
    <add key="UniversalLogProfilingFullFilePath" value="App_Data\UlogProfiler.csv" />
    <add key="BuildMonitorFilename" value="convert.ib_mon" />
    <add key="BuildMonitorMimeType" value="application/octet-stream" />
    <add key="CurrentDeploymentServer" value="dev" />
    <add key="DeploymentVersionURISeparator" value="-" />
    <add key="DeploymentVersionViewSeparator" value="." />
    <add key="JobMatrixGroupFromFriendlyNameSeparator" value="." />
    <add key="ShortcutMenuServiceFormat" value="net.tcp://{0}:7020/ShortcutMenuService.svc" />
    <add key="EmptyProcessingHostString" value=" ? " />
    <add key="DefaultAutomationHistoricalDays" value="3" />
    <add key="DefaultAutomationStatusHistoricalDays" value="7" />
    <add key="MaxAutomationMatrixLines" value="300" />
    <add key="JobMatrixStaticFilePath" value="App_Data\Static\" />
    <add key="JobMatrixStaticExtension" value=".json" />
    <!-- -->
    <!-- <add key="DevMode" value="True" /> -->
    <!-- -->
  </appSettings>
  <!-- Enabling this in order to serve the bundle files in not authorised pages (i.e. login) -->
  <location path="Content">
    <system.web>
      <authorization>
        <allow users="?" />
      </authorization>
    </system.web>
  </location>
  <location path="bundles">
    <system.web>
      <authorization>
        <allow users="?" />
      </authorization>
    </system.web>
  </location>
  <system.web>
    <compilation targetFramework="4.5" />
    <httpRuntime targetFramework="4.5" />
    <authentication mode="Forms">
      <forms loginUrl="~/Account/Login" timeout="10080" />
      <!-- Note that timeout is in minutes  - 10080 is 7 days -->
    </authentication>
    <roleManager enabled="true" />
    <pages>
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Optimization" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
    <caching>
      <outputCacheSettings>
        <outputCacheProfiles>
          <add name="Cache1Min" duration="60" varyByParam="*" />
          <add name="Cache90Secs" duration="90" varyByParam="*" />
          <add name="Cache5Mins" duration="300" varyByParam="*" />
          <add name="Cache1Hour" duration="3600" varyByParam="*" />
        </outputCacheProfiles>
      </outputCacheSettings>
    </caching>
  </system.web>
  <!-- WCF Config -->
  <system.serviceModel>
    <bindings configSource="App_Data\Config\Bindings.xml" />
    <behaviors configSource="App_Data\Config\Behaviours.xml" />
    <client configSource="App_Data\Config\Clients_Tcp.xml" />
  </system.serviceModel>
  <!-- End of Wcf Config -->
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <handlers>
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" />
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" />
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
    <httpCompression directory="%SystemDrive%\inetpub\temp\IIS Temporary Compressed Files">
      <scheme name="gzip" dll="%Windir%\system32\inetsrv\gzip.dll" />
      <dynamicTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/javascript" enabled="true" />
        <add mimeType="*/*" enabled="false" />
      </dynamicTypes>
      <staticTypes>
        <add mimeType="text/*" enabled="true" />
        <add mimeType="message/*" enabled="true" />
        <add mimeType="application/javascript" enabled="true" />
        <add mimeType="*/*" enabled="false" />
      </staticTypes>
    </httpCompression>
    <urlCompression doStaticCompression="true" doDynamicCompression="true" />
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.3.0.0" newVersion="1.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="EntityFramework" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.0.0.0" newVersion="5.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>
<!--ProjectGuid: D34BBC69-76EE-4632-A88C-025D31845E56-->