var clientStateEnum,
	clientsUrlPath = "/automation.svc/admin/clients",
	clientStateEnumDataType = "RSG.Pipeline.Automation.Common.Client.WorkerState",
	coordinatorLogMessageCount = "/automation.svc/admin/CoordinatorLogMessageCount",
	coordinatorLogUrl = "/automation.svc/admin/CoordinatorLog",
	ulogUrl = "/ulogs/index.html";

function onTimerClients() {

	$.ajax({type: "GET",
			url: clientsUrlPath,
			dataType: "json",
			success: function(json) {

				var table1 = $("#clientlist1").dataTable();
				table1.off("draw").on("draw", updateClientsTable);
				// delete any events and clear the table
				table1.fnClearTable();

				$.each(json, function(i, clientstatus) {
					table1.fnAddData([i,
			                  clientstatus.ID,
			                  clientstatus.ServiceConnection,
			                  clientStateEnum[clientstatus.State],
			                  clientstatus.AssignedJob
			                  ],
			                  false);
				});
		
				//table1.fnDraw();
				table1.fnStandingRedraw();
			}
	});

	setTimeout(onTimerClients, reloadInterval);
}

//Called when the tasks table is being redrawn - adds additional styling and hover user/files info
function updateClientsTable() {
	var clientRows = $("#clientlist1 tr");
	$.each(clientRows, function(i, clientRow) {
		// add the user div to the job-id's column 
		var jobIdTd = $(clientRow).find("td.job-id");
			
		jobIdTd.unbind().bind("mouseover", function(e) {
			if (jobJson[$(this).text()])
				createUserInfoDiv(jobJson[$(this).text()].Trigger.Username, $(this));
			$(this).children("div.user").fadeIn("fast");
		});
		jobIdTd.bind("mouseleave", function(e) {
			$(this).children("div.user")
				.fadeOut("fast")
				.remove();
		});	
	});
}

function initClients() {

	//get the client state enum list

	$.ajax({
		type: "GET",
		url: enumsUrl,
		data: {type: clientStateEnumDataType},
		dataType: "json",
		async: false,
		success: function(json) {
			clientStateEnum = {};
			$.each(json, function(i, item) {
				clientStateEnum[item.Key] = item.Value;
			});
		}
	});

	//create the tables with the correct headers
	var clientTableData = {"aoColumns" : [], "bStateSave": true, "bAutoWidth": false};

	clientTableData.aoColumns.push({ "sTitle": "Index", "sWidth": "2%"});
	clientTableData.aoColumns.push({ "sTitle": "ID", "sWidth": "29%"});
	clientTableData.aoColumns.push({ "sTitle": "Connection", "sWidth": "35%"});
	clientTableData.aoColumns.push({ "sTitle": "State", "sWidth": "5%"});
	clientTableData.aoColumns.push({ "sTitle": "Job", "sWidth": "29%", "sClass": "job-id"});

	clientTableData["fnPreDrawCallback"] = function() {$("#clientlist1 tbody").find("*").unbind();};
	
	$("#clientlist1").dataTable(clientTableData);

    onTimerClients();
    
    addCoordinatorLog();
}

function addCoordinatorLog() {
	$("#coordinator-log").append(
		$("<a>").attr("href", config.webHost + ulogUrl + "?ulog=" + config.webHost + coordinatorLogUrl)
				.text("Open Coordinator Log")
				.attr("title", "Open Coordinator Log")
				.attr("target", "_blank")
	)
	
}