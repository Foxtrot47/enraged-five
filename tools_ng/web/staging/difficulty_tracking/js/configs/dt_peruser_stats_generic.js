var usersDict = getPlaythroughUsersAsDict();
var requestPValues;

var missionHashes = getMissionHashes();
var radioHashes = getRadioHashes();
var weaponHashes = getWeaponHashes();

var vehicleHashes = getVehicleHashes();

var fixed2 = function(d) {return d.toFixed(2); };
var fixed3 = function(d) {return d.toFixed(3); };
var offOnLabels = ["Off", "On"];
var bigScoreLabels = 
[
 	"Helicopter Lift",
 	"Traffic Control"
];
var bureauRaidLabels = 
[
 	"Helicopter Drop",
 	"Firecrew",
];
var lsHeistLabels = 
[
 	"Deep sea diving",
 	"Sink tanker",
];
var jewelStoreLabels = 
[
 	"High impact",
 	"Stealth",
];
var finalDesicionLabels = 
[
 	"NONE",
	"1. Killed Michael",
	"2. Killed Trevor",
	"3. Saved Michael and Trevor",
];

var gamers;

function initGamers() {
	gamers = {};
}

// Populates the dict with an entry for each gamer in the order they have been requested
function populateOrderedGamers() {
	initGamers();
	//console.log(requestPValues);
	
	if (requestPValues) {
		// Get the users from the requested post json object
		var orderedGamers = requestPValues.Pairs["GamerHandlePlatformPairs"]
											.split(",")
											.map(function(d) {
												return d.split("|")[0].substring(1);
											});
		orderedGamers.map(function(d) {
			var gamerTag = (usersDict[d]) ? usersDict[d] : d; // map the gamertags to names
			gamers[gamerTag] = {};
		});
	}
	
	return gamers;
}

function groupLatestProfileStats(data) {	
	populateOrderedGamers();
	
	$.each(data, function(i, d) {
		var gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		
		if (!gamers.hasOwnProperty(gamerTag))
			gamers[gamerTag] = {};
		
		$.each(d.Data, function(j, s) {
			gamers[gamerTag][s.StatName] = s.Value;
		});
	});

	return gamers;
}

function groupMultiplePerUserStats(endpoindsData, endpointProcessFunctions) {
	
	$.each(endpointProcessFunctions, function(i, endpointProcessFunction) {
		endpointProcessFunction(endpoindsData[i][0].response);
	});
	return gamers;
}

function getStatNames(gameTypes) {
	gameTypes = (gameTypes) ? gameTypes : config.gameTypes.slice(0, 1);
		
	var profStat = new ProfileStats();
	
	var statNames = [];
	$.each(profileStatList, function (i, profileStat) {
		statNames = statNames.concat(
			profStat.constructStatNames(profileStat, profileStat.name, gameTypes)
		);
	});
		
	return statNames.join(",");
}

function getValue(d) {
	return (d[this.name]) ? d[this.name] : "-";
}

function getCharValue(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ? d[getSPStatPrefix(i) + this.name] : "-";
}

function getNumValue(d) {
	return (d[this.name]) ? Number(d[this.name]) : 0;
}

function getCashValue(d) {
	return (d[this.name]) ? 
			("$ " + d3.format(",")(Number(d[this.name]))) : "-";
}

function getCharCashValue(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ? 
			("$ " + d3.format(",")(Number(d[getSPStatPrefix(i) + this.name]))) : "-";
}

function getOnOffValue(d) {
	return (d[this.name]) ? offOnLabels[d[this.name]] : "-";
}

function bigScoreValue(d) {
	return (d[this.name]) ? bigScoreLabels[d[this.name]] : "-";
}

function bureauRaidValue(d) {
	return (d[this.name]) ? bureauRaidLabels[d[this.name]] : "-";
}

function lsHeistValue(d) {
	return (d[this.name]) ? lsHeistLabels[d[this.name]] : "-";
}

function jewelStore(d) {
	return (d[this.name]) ? jewelStoreLabels[d[this.name]] : "-";
}

function finalDesicionValue(d) {
	return (d[this.name]) ? finalDesicionLabels[d[this.name]] : "-";
}

function missionHashValue(d) {
	return (missionHashes[d[this.name]]) ? missionHashes[d[this.name]] : d[this.name];
}

function radioHashValue(d) {
	return (radioHashes[d[this.name]]) ? radioHashes[d[this.name]] : d[this.name];
}

function weaponCharHashValue(d, i) {
	return (weaponHashes[d[getSPStatPrefix(i) + this.name]]) 
		? weaponHashes[d[getSPStatPrefix(i) + this.name]] 
		: d[getSPStatPrefix(i) + this.name];
}

function weaponHashValue(d) {
	return (weaponHashes[d[this.name]]) ? weaponHashes[d[this.name]] : d[this.name];
}

function vehicleHashCharValue(d, i) {
	return (vehicleHashes[d[getSPStatPrefix(i) + this.name]]) ?
				vehicleHashes[d[getSPStatPrefix(i) + this.name]] : d[getSPStatPrefix(i) + this.name];
}

function vehicleHashValue(d) {
	return (vehicleHashes[d[this.name]]) ? vehicleHashes[d[this.name]] : d[this.name];
}


function getValuePerCent(d, i) {
	return (d[this.name]) ? fixed2(Number(d[this.name])) + " %" : "-";
}

function getCharValuePerCent(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ? 
			fixed2(Number(d[getSPStatPrefix(i) + this.name])) + " %" : "-";
}

function getCharDistance(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ?
			fixed3(Number(d[getSPStatPrefix(i) + this.name])/1000) + " km" : "-";
} // This is meters

function getCharSpeed(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ? 
			fixed2(Number(d[getSPStatPrefix(i) + this.name])/1000) + " km/h" : "-";
} // this is meters / hour

function getTimeValue(d) {
	return (d[this.name]) ? formatSecsWithDays(Number(d[this.name])/1000) : "-";
} // this is millisecs

function getCharTimeValue(d, i) {
	return (d[getSPStatPrefix(i) + this.name]) ? 
			formatSecsWithDays(Number(d[getSPStatPrefix(i) + this.name])/1000) : "-";
} // this is millisecs

function getSPStatPrefix(i) {	
	if (i != null)
		return "SP" + i + "_";
	else
		return "";
}
