var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var fixed = function(d) {return d.toFixed(3);};
var usersDict = getPlaythroughUsersAsDict();

function generateUserEndpoints(pValues) {
	var pValuesArray = [];
	
	$.each(pValues.Pairs["GamerHandlePlatformPairs"].split(","), function(i, gamer) {
		pValues.Pairs["BuildIdentifiers"] = null;
		pValues.Pairs["PlatformNames"] = null;
		var copiedPvalues = $.extend(true, {}, pValues);
		copiedPvalues.Pairs["GamerHandlePlatformPairs"] = gamer;
		pValuesArray.push(copiedPvalues);
	});
	
	var endpointObject = {
		restUrl: config.restHost + config.vehiclesStats,
		restAsyncUrl: config.restHost + config.vehiclesStatsAsync + pValues.ForceUrlSuffix,
		pValues: pValuesArray,
	};
	
	return [endpointObject];
}

var statList = [
    {
    	name: "Car",
    	title: "Car",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "QuadBike",
    	title: "QuadBike",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Boat",
    	title: "Boats",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Bike",
    	title: "Bike",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Heli",
    	title: "Heli",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Plane",
    	title: "Plane",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Bicycle",
    	title: "Bicycle",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
    {
    	name: "Submarine",
    	title: "Submarine",
    	getValue: function(d) {return getMaxDistVehicleFromCategory(d, this.name)}, 
    },
];

var reportOptions = {
	//restEndpoint: config.vehiclesStats,
	//restEndpointAsync: config.vehiclesStatsAsync,
	multipleRequests: generateUserEndpoints,
		
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Favourite Vehicles",
	columnText: "User: ",

	processFunction: groupUserResults,
	
	reportArrayItems: statList,
};

function groupUserResults(data) {	
	var gamers = {};

	$.each(data[0], function(i, d) {
		//var gamerTag = JSON.parse(d.reqData)[1].Value.split("|")[0].split("<")[1];
		var gamerTag = JSON.parse(d.reqData)[0].Value.split("|")[0].split("<")[1];
		gamerTag = (usersDict[gamerTag]) ? usersDict[gamerTag] : gamerTag;
		
		gamers[(gamerTag) ? gamerTag : i] = d.response;
	});
	
	return gamers;
}

function getMaxDistVehicleFromCategory(data, category) {
	var favVehicle = "-";
	
	$.each(data, function(i, d) {
		if (d.CategoryName == category) {
			var sortedArray = d.PerVehicleStats.sort(
					function(a, b) {
						return (a.TotalDistanceDriven > b.TotalDistanceDriven) ? -1 : 1 
					}); 
			favVehicle = (sortedArray.length > 0) ? 
					sortedArray[0].VehicleName  + "<br/>(" +  sortedArray[0].TotalDistanceDriven/1000 + " km)" 
					: "-";
		}
		else 
			return;
	});
	
	return favVehicle;
}
