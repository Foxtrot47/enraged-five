var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

var profileStatList = [
	{
		name: "FAVORITE_WEAPON",
		title: "Favourite Weapon",
		getValue: weaponCharHashValue,
	},
	{
		name: "FAVORITE_WEAPON_HELDTIME",
		title: "Favourite Weapon Heldtime",
		getValue: getCharTimeValue,
	},
    {
    	name: "PISTOL_KILLS",
    	title: "Pistol kills",
    },
    {
    	name: "CMBTPISTOL_KILLS",
    	title: "Combat Pistol kills",
    },
    {
    	name: "APPISTOL_KILLS",
    	title: "AP Pistol kills",
    },
    {
    	name: "MICROSMG_KILLS",
    	title: "Micro SMG kills",
    },
    {
    	name: "SMG_KILLS",
    	title: "SMG kills",
    },
    {
    	name: "ASLTRIFLE_KILLS",
    	title: "Assult Rifle kills",
    },
    {
    	name: "CRBNRIFLE_KILLS",
    	title: "Carbine Rifle kills",
    },
    {
    	name: "ADVRIFLE_KILLS",
    	title: "Advanced Rifle kills",
    },
    {
    	name: "MG_KILLS",
    	title: "MG kills",
    },
    {
    	name: "CMBTMG_KILLS",
    	title: "Combat MG kills",
    },
    {
    	name: "PUMP_KILLS",
    	title: "Pump Shotgun kills",
    },
    {
    	name: "SAWNOFF_KILLS",
    	title: "Sawed-Off Shotgun kills",
    },
    {
    	name: "ASLTSHTGN_KILLS",
    	title: "Assault Shotgun kills",
    },
    {
    	name: "SNIPERRFL_KILLS",
    	title: "Sniper Rifle kills",
    },
    {
    	name: "HVYSNIPER_KILLS",
    	title: "Heavy Sniper kills",
    },
    {
    	name: "GRNLAUNCH_KILLS",
    	title: "Grenade Launcher kills",
    },
    {
    	name: "RPG_KILLS",
    	title: "RPG kills",
    },
    {
    	name: "MINIGUNS_KILLS",
    	title: "Minigun kills",
    },
    {
    	name: "GRENADE_KILLS",
    	title: "Grenade kills",
    },
    {
    	name: "SMKGRENADE_KILLS",
    	title: "Tear Gas kills",
    },
    {
    	name: "STKYBMB_KILLS",
    	title: "Sticky Bomb kills",
    },
    {
    	name: "MOLOTOV_KILLS",
    	title: "Molotov kills",
    },
    {
    	name: "KNIFE_KILLS",
    	title: "Knife kills",
    },
    {
    	name: "NIGHTSTICK_KILLS",
    	title: "Nightstick kills",
    },
    {
    	name: "CROWBAR_KILLS",
    	title: "Crowbar kills",
    },
    {
    	name: "HAMMER_KILLS",
    	title: "Hammer kills",
    },
    {
    	name: "BAT_KILLS",
    	title: "Baseball Bat kills",
    },
    {
    	name: "GCLUB_KILLS",
    	title: "Golf Club kills",
    },
    {
    	name: "UNARMED_KILLS",
    	title: "Unarmed kills",
    },
    {
    	name: "STUNGUN_KILLS",
    	title: "Stun Gun kills",
    },
    {
    	name: "RHINO_ROCKET_KILLS",
    	title: "Rhino Gun kills",
    },
    {
    	name: "BUZZARD_BULLET_KILLS",
    	title: "Buzzard Minigun kills",
    },
    {
    	name: "BUZZARD_ROCKET_KILLS",
    	title: "Buzzard Rocket kills",
    },
    {
    	name: "LAZER_BULLET_KILLS",
    	title: "Lazer Cannon kills",
    },
    {
    	name: "NUM_WEAPON_PICKUPS",
    	title: "Weapons collected",
    },
    {
    	name: "NUM_HEALTH_PICKUPS",
    	title: "Health packs collected",
    },
    /*
    {
    	name: "WEAP_ADDON_PURCH_0",
    	title: "Most popular addon (0)",
    },
    {
    	name: "WEAP_ADDON_PURCH_1",
    	title: "Most popular addon (1)",
    },
    {
    	name: "WEAP_ADDON_PURCH_2",
    	title: "Most popular addon (2)",
    },
    {
    	name: "WEAP_ADDON_PURCH_3",
    	title: "Most popular addon (3)",
    },
    */
];

var reportOptions = {
	restEndpoint: config.profileStatsPerUser,
	restEndpointAsync: config.profileStatsPerUserAsync,
	
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Weapons",
	columnText: "User: ",

	processFunction: groupLatestProfileStats,
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},
	hasExtraRestParams: [
	{
		key: "StatNames",
	    value: getStatNames,
	},
	],
	
	reportTabs: [
	{
		key: "Michael",
		value: 0
	},
	{
		key: "Franklin",
		value: 1
	},
	{
		key: "Trevor",
		value: 2
	},
	],	
	
	reportArrayItems: profileStatList.map(
				function(d) {
					d["requestSubString"] = "_";
					if (!d.hasOwnProperty("getValue"))
						d["getValue"] = getCharValue;
					
					return d;
				}),
};

//For manifest - b* #1693427
reportOptions["availableCharts"] = profileStatList.map(function(d) {
														d["types"] = [d.name];
														d["requestSubString"] = "_";
														d["singleOnly"] = true;
														//d.name = d.title;
														return d;});