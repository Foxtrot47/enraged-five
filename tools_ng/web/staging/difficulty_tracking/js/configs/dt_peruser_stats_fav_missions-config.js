var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields:
	[ 			// disabled header fields
	  	true, // build
		true, // platform
		false, // user
	],
};

//var fixed = function(d) {return d.toFixed(3);};
var usersDict = getPlaythroughUsersAsDict();

var statList = [
    {
    	name: "^(?!(AMB|CnC|MG|Odd|RE|RC|Special Ped|VOID|NET))",
    	title: "Main Story",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)},
    	
    },
    {
    	name: "^RC",
    	title: "Random Characters",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    {
    	name: "^RE",
    	title: "Random Events",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    {
    	name: "^MG",
    	title: "Mini Games",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },  
    {
    	name: "^Odd",
    	title: "Odd Jobs",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    /*
    {
    	name: "^AMB",
    	title: "Ambient",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    {
    	name: "^Special Ped",
    	title: "Special Peds",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    {
    	name: "^CnC",
    	title: "Cops n Crooks",
    	getValue: function(d) {return getFavouriteMissionByCategory(d, this.name)}, 
    },
    */
];

var reportOptions = {
	restEndpoint: config.userMissionAttempts,
	restEndpointAsync: config.userMissionAttemptsAsync,
		
	legendText: ""
				+ "Per User Stats"
				+ "<span class='arrow-text'>&nbsp;&nbsp;</span>"
				+ "Favourite Missions (Based on Total Attempts)",
	columnText: "User: ",
	
	storePValues: function(pValues) {
		// Store the pvalues locally for ordering the results
		requestPValues = pValues;
	},

	processFunction: groupUserResults,
	
	reportArrayItems: statList,
};

function groupUserResults(data) {	
	var gamers = populateOrderedGamers();
	
	$.each(data, function(i, d) {
		gamerTag = (usersDict[d.GamerHandle]) ? usersDict[d.GamerHandle] : d.GamerHandle;
		
		gamers[gamerTag] = d.Data;
	});
	
	return gamers;
}

function getFavouriteMissionByCategory(data, categoryRegExp) {
	var favMissionObj = {"MissionName": "", "TotalAttempts": 0};

	var re = new RegExp(categoryRegExp);
	
	$.each(data, function(i, d) {
		if (re.test(d.MissionName)) {
			favMissionObj = (d.TotalAttempts >= favMissionObj.TotalAttempts) ? d : favMissionObj;
		}
		else 
			return;
	});
	
	return (favMissionObj.MissionName) ? 
			(favMissionObj.MissionName + "<br />(" + favMissionObj.TotalAttempts + " Attempts)") : "-";
}
