var reportOptions = {
	// 1 is the results index for MainStory missions
	//processFunction: function(d) {return processMissionsReport(d, 1); },
		
	restEndpoint: config.playthroughsMissions,
	restEndpointAsync: config.playthroughsMissionsAsync,
	elementId: "line-area-chart",
	
	//backgroundColour: "#333333",
	backgroundColour: "#393939",
	areaColour: "#652626",
	areaCircleColour: "#9a2626",
	lineColour: "#ffffff",
	textColour: "#ffffff",
	gridColour: "#666666",
			
	name: function(d) { return d.MissionName; },
	//shortName: function(d) { return (d.MissionName).toUpperCase(); },
	fullName: function(d, build) { return ((build) ? d.MissionName + " (" + build + ")" : d.MissionName); },
	comments: function(d) {
		var cm = [];
		d.CheckpointAttemptStats.map(function(cp) {
			if (cp.Comments.length > 0) {
				cm.push({
					"checkpointName": cp.CheckpointName,
					"checkpointComments": cp.Comments,
				});
			}
		});
		return cm;
	},
	minValue: function(d) { return d.MinMissionTime / (1000 * 60); }, // Convert to minutes, time is in millisecs
	maxValue: function(d) { return d.MaxMissionTime / (1000 * 60); },
	value: function(d) { return d.AvgMissionTime / (1000 * 60); }, //
	
	orientation: "horizontal",
	margin: {top: 10, right: 10, bottom: 10, left: 30},
	//orientation: "vertical",
	//margin: {top: 10, right: 10, bottom: 10, left: 200},
	
	legend: {height: 30, width: 170, rectWidth: 18},
	
	legendDataConst : [
	       {label : "Min/Max Timings [mins]", colour: "#652626"}, // keep this colour in sync with areaColour
    ],
	              
	legendDataVar: {label : "Avg Timing [mins]", colour: "#ffffff"}, // keep this colour in sync with lineColour 
	
	valueTooltipContent: function(d, b) {
		var content = "<div class='title'>" + this.fullName(d, b) + "</div>"
				+ "<table>"
					+ "<tr><td>Min Timing :</td><td class='right'>" + formatMins(this.minValue(d)) + "</td></tr><br />"
					+ "<tr><td>Max Timing :</td><td class='right'> " + formatMins(this.maxValue(d)) + "</td></tr>"
					+ "<tr><td>Average Timing:</td><td class='right'>" + formatMins(this.value(d)) + "</td></tr>"
				+ "</table>";

		return content;
	},
	
	areaTooltipContent: function(d) {
		var content = "<div class='title'>" + d.fullName + "<br />" + "<br />" + "</div>"
				+ "<table>"
					+ "<tr><td>Overall Min Timing :</td><td class='right'>" + formatMins(d.min) + "</td></tr><br />"
					+ "<tr><td>Overall Max Timing :</td><td class='right'> " + formatMins(d.max) + "</td></tr>"
				+ "</table>";

		return content;
	},
	
	// Copied from mission_attempts-config
	onGraphClickOverlayContent: function(d, i) {return createOverlayContent(d,i)},
	              
};

var checkpointReportOptions = {
	elementId: "line-area-overlay-chart",
	backgroundColour: reportOptions.backgroundColour,
	areaColour: reportOptions.areaColour,
	areaCircleColour: reportOptions.areaCircleColour,
	lineColour: reportOptions.lineColour,
	textColour: reportOptions.textColour,
	gridColour: reportOptions.gridColour,
					
	name: function(d) { return d.CheckpointName; },
	fullName: function(d, build) { return ((build) ? d.CheckpointName + " (" + build + ")" : d.CheckpointName); },
	
	comments: function(d) { return d.Comments; },
	minValue: function(d) { return d.MinCheckpointTime / (1000 * 60); }, // Convert to minutes, time is in millisecs
	maxValue: function(d) { return d.MaxCheckpointTime / (1000 * 60); },
	value: function(d) { return d.AvgCheckpointTime / (1000 * 60); }, // Do not remove the Number func
			
	orientation: reportOptions.orientation,
	margin: reportOptions.margin,
			
	legend: reportOptions.legend,
			
	legendDataConst: reportOptions.legendDataConst,
	legendDataVar: reportOptions.legendDataVar,
		
	valueTooltipContent: reportOptions.valueTooltipContent,
	areaTooltipContent: reportOptions.areaTooltipContent,
	
	isOverlay: true,
	
	truncatedNameChars: 25,
}

var headerAndFilters  = {
	headerType: "header-dt", 	// social club filtering header
	disabledFields: [ 			// disabled header fields
	                 false, // build
	                 false, // platform
	                 false, // user
	],
};
