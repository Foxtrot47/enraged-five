var graphClass;
// Array that stores objects for each element returned via rest
var chartsData = [];

function initPage() {
	// function from generic.js, variable from config file
	initHeaderAndFilters(headerAndFilters);
	
	$("#content-body").append(
		$("<div>").attr("id", reportOptions.elementId)
	);
	
	initSvg(reportOptions.elementId);
	
	graphClass = new LinesPlusBivariateAreaGraph(reportOptions.elementId, "content-body");
	
	$("#filter").click(function() {
		generateCharts();
	});
	generateCharts();
}

function generateCharts() {
	var pValues = config.headerOptions[headerAndFilters.headerType].getParamValues();
	
	if (!$.isArray(pValues)) {
		pValues = [pValues];
	}
	
	var endpointObject = {
		restUrl: config.restHost + reportOptions.restEndpoint,
		restAsyncUrl: config.restHost + reportOptions.restEndpointAsync + pValues[0].ForceUrlSuffix,
		pValues: pValues,
	}
		
	var req = new ReportRequest(null, "json", null, config.restHost + config.reportsQueryAsync);
	req.sendMultipleAsyncRequest([endpointObject], function(response) {
		var chartsData = (reportOptions.processFunction) ? reportOptions.processFunction(response) : response;

		cleanSvg(reportOptions.elementId);
		graphClass.draw(chartsData, reportOptions);
		
		/*
		// Resize with delay
		$(window).bind("resize", function(e) {
			window.resizeEvt;
			$(window).resize(function() {
				clearTimeout(window.resizeEvt);
				window.resizeEvt = setTimeout(function() {
					cleanSvg(reportOptions.elementId);
					graphClass.draw(chartsData, reportOptions);
				}, 250);
			});
		});
		*/
		$(window).resize(function() {
			cleanSvg(reportOptions.elementId);
			graphClass.draw(chartsData, reportOptions);
		});
		
	});
	
} //end of generateCharts()

function processMissionsReport(json, categoryName, order) {
	// json[0] is the first endpoint (only one is available in this case)
	var data = {};
	$.each(json[0], function(i, result) {
		var extractedBuild = JSON.parse(result.reqData)[2].Value;
		
		var categoryArray = [];
		$.each(result.response, function(i, d) {
			if (d.CategoryName == categoryName) {
				categoryArray = d.MissionAttemptStats;
				return;
			}
		});
		
		if (order)
			data[(extractedBuild)? extractedBuild : i] = 
				categoryArray.sort(
						function(a,b) {return (a.MissionName > b.MissionName) ? 1 : -1}
				);
		else
			data[(extractedBuild)? extractedBuild : i] = categoryArray;
	});
	return data;
}

function createOverlayContent(d, missionIndex) {
	// d has the row dict data
	var currentMission =  d[Object.keys(d)[0]][missionIndex];
	var hasMultipleBuilds = ((Object.keys(d).length > 1) ? true : false);
	
	var content = "<div class='title'>" + reportOptions.fullName(currentMission) + "</div><br/>"
		+ "<div id=overlay-graph>"
			+ "<div id='" + checkpointReportOptions.elementId + "'></div>" 	
		+ "</div>"
		+ "<div class=table-wrapper>"
			+ "<table>";
	
	if (hasMultipleBuilds) {
		 var temp = "<thead><tr>"
				+ "<th style='width:5%'>Build</th>"
				+ "<th style='width:25%'>Checkpoint</th>"
				+ "<th style='width:15%'>User</th>"
				+ "<th style='width:5%'>Duration</th>"
				+ "<th style='width:50%'>Comment</td></tr></thead>";
		content += temp;
	}
	else {
		temp = "<thead><tr>"
			+ "<th style='width:25%'>Checkpoint</th>"
			+ "<th style='width:15%'>User</th>"
			+ "<th style='width:5%'>Duration</th>"
			+ "<th style='width:55%'>Comment</td>"
			+ "</tr></thead>";
		
		content += temp;
	}
	
	// Save the func cause this will be changed in the loop
	//var commentsFunc = this.comments;
	// Loop over the rowData dict
	$.each(d, function(key, value) {
		
		// Loop over the checkpoints of the given mission
		$.each(reportOptions.comments(value[missionIndex]), function(i, checkpointCommentsObj) {

			// Loop over the current checkpoint comments
			$.each(checkpointCommentsObj.checkpointComments, function(j, checkpointComment) {
				content += "<tr>";
				
				if (hasMultipleBuilds)
					content += "<td>" + key + "</td>";
				
				content += "<td>" + checkpointCommentsObj.checkpointName
						+ "</td><td>" + checkpointComment.Username
						+ "</td><td>" + formatSecs(checkpointComment.Duration/1000) // Duration is in millisecs
						+ "</td><td>" + checkpointComment.Comment
						+ "</td></tr>";
			});
		});
	});

	content += "</table></div>";
	
	// Give it some time to render and then draw the graph
	setTimeout(function() {drawOverlayGraph(d, missionIndex)}, 500);

	return content;
}

// helper functions for the graph
function drawOverlayGraph(d, i) {
	initSvg(checkpointReportOptions.elementId);
	//cleanSvg(checkpointReportOptions.elementId);
	var checkpointsGraphClass = new LinesPlusBivariateAreaGraph(checkpointReportOptions.elementId, "overlay-graph");
	checkpointsGraphClass.draw(processCheckpoints(d, i), checkpointReportOptions);
}

function processCheckpoints(rowData, index) {
	var data = {};
	$.each(rowData, function(build, missions) {
		data[build] = missions[index].CheckpointAttemptStats;
	});
	return data;
}

/*
function processMissionsReport(json) {
	var elementsArray = [];
	
	$.each(json, function(i, mission) {
		$.each(mission.CheckpointAttempts, function(j, cp) {
			var missionReportElement = {
				MissionName: mission.MissionName,
				CheckpointIndex: cp.CheckpointIndex,
				CheckpointName: cp.CheckpointName,
				MissionComments: mission.Comments,
				ProjectedAttempts: cp.ProjectedAttempts,
				ProjectedAttemptsMin: cp.ProjectedAttemptsMin,
				ProjectedAttemptsMax: cp.ProjectedAttemptsMax,
				NumberOfAttempts: cp.NumberOfAttempts,
				PlayersAttempted: cp.PlayersAttempted,
				MinCheckpointTime: cp.MinCheckpointTime,
				AvgCheckpointTime: cp.AvgCheckpointTime,
				MaxCheckpointTime: cp.MaxCheckpointTime,
			};
				
			elementsArray.push(missionReportElement);
		});
	});
	
	//console.log(elementsArray);
	return elementsArray;
}
*/
