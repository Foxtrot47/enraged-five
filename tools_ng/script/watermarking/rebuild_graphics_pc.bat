@echo off

echo ==========================
echo Rebuilding graphics_pc zip
echo ==========================
echo

%RS_TOOLSROOT%\script\texture\data_mk_graphics_pc_itd.bat

setenv.bat
%RS_TOOLSROOT%\ironlib\prompt.bat
%RS_TOOLSIR% %RS_TOOLSIRONLIB%\util\data_convert_file.rb --rebuild %RS_EXPORT%/textures/

echo Done