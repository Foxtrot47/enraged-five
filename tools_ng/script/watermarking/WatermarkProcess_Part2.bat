@echo off
TITLE WatermarkProcess_Part2
echo embedding and verify Watermark
set p/ WatermarkFile=Please enter the name of the cfg watermark file:
set p/ ExeLoc=Please enter the full address of the exe:
set p/ ExeName=Please enter the full name of the game exe:

echo.%WatermarkFile%
set WatermarkName=%WatermarkFile:_watermark.cfg=\%
echo.%WatermarkName%

%RS_TOOLSROOT%\script\watermarking\watermarker.py -embed %RS_TOOLSROOT%\script\watermarking\%WatermarkFile% %ExeLoc%\%ExeName% %RS_TOOLSROOT%\script\watermarking\Watermark_Output\Temp//

%RS_TOOLSROOT%\script\watermarking\watermarker.py -verify %RS_TOOLSROOT%\script\watermarking\Watermark_Output\Temp//%WatermarkName%\%ExeName% %RS_TOOLSROOT%\script\watermarking\Watermark_Output\