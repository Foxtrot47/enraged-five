@echo off
TITLE WatermarkProcess_Part1
echo Creating Watermark Images
set p/ WatermarkFile=Please enter the name of the cfg watermark file:

mkdir %RS_TOOLSROOT%\script\watermarking\Watermark_Output\Temp
%RS_TOOLSROOT%\script\watermarking\watermarker.py -image %RS_TOOLSROOT%\script\watermarking\%WatermarkFile% %RS_TOOLSROOT%\script\watermarking\Watermark_Output\Temp//

pause