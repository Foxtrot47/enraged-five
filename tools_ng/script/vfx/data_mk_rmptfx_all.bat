CALL setenv.bat >NUL
SET PTFX_OVERRIDE_DIR=%1
SET IPT_PATH=%RS_EXPORT%\data\effects\%PTFX_OVERRIDE_DIR%
SET CACHE_PATH=x:\cache

CALL del %IPT_PATH%\*.* /s /q
CALL del %CACHE_PATH%\*.* /s /q

IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

REM don't use XGE on the stub file - because all the paralell jobs need to copy stub.ipt somewhere so we need to make sure it exists.
REM CALL data_mk_rmptfx_ipt stub

%DOJOB% data_mk_rmptfx_ipt_core.bat %PTFX_OVERRIDE_DIR% 
REM CALL these ones because internally it will submit multiple jobs
CALL data_mk_rmptfx_ipt_cutscene.bat %PTFX_OVERRIDE_DIR%
CALL data_mk_rmptfx_ipt_dev.bat %PTFX_OVERRIDE_DIR%
REM CALL data_mk_rmptfx_ipt_entity.bat %PTFX_OVERRIDE_DIR%
CALL data_mk_rmptfx_ipt_rayfire.bat %PTFX_OVERRIDE_DIR%
CALL data_mk_rmptfx_ipt_script.bat %PTFX_OVERRIDE_DIR%
REM CALL data_mk_rmptfx_ipt_vehicle.bat %PTFX_OVERRIDE_DIR%
REM CALL data_mk_rmptfx_ipt_weapon.bat %PTFX_OVERRIDE_DIR%

%WAITJOBS%

IF "%USE_XGE%" == "" (
REM Caller needs to do this for XGE builds - since internally it'll start its own XGE build.
CALL data_mk_rmptfx_rpf.bat %PTFX_OVERRIDE_DIR%
)