@ECHO OFF


CD /d %~dp0

:: Set Title and Startup Text
TITLE Getting Latest VFX
ECHO+
ECHO Getting Latest VFX...
ECHO+

:: Initialise Variables
SET syncerror=0
SET TUSync=0
SET NGVFX=0
SET NGAsSETs=0
SET NGCommon=0
SET NGXbone=0
SET NGPS4=0
SET NGPC=0
SET NGTools=0
SET NGHeistsVFX=0
SET NGHeistsAssets=0
SET NGHeistsCommon=0
SET NGHeistsXbone=0
SET NGHeistsPS4=0
SET NGHeistsPC=0
SET CGHeistsVFX=0
SET CGHeistsAssets=0
SET CGHeistsCommon=0
SET CGHeistsXB360=0
SET CGHeistsPS3=0
SET NGCliffVFX=0
SET NGCliffAssets=0
SET NGCliffCommon=0
SET NGCliffXbone=0
SET NGCliffPS4=0
SET NGCliffPC=0
SET CGCliffVFX=0
SET CGCliffAssets=0
SET CGCliffCommon=0
SET CGCliffXB360=0
SET CGCliffPS3=0

:: Killing Game Processes
ECHO --------------------------------------------------
ECHO Killing SystrayRfs and RAG...
taskkill /IM SysTrayRfs.exe >nul 2>&1
taskkill /IM rag.exe >nul 2>&1
taskkill /IM ragApp.ex >nul 2>&1
ECHO --------------------------------------------------

ECHO+
ECHO+
ECHO Grabbing GTA V NG VFX Files...

:: Grab NG VFX Folder
ECHO --------------------------------------------------
ECHO Grabbing NG VFX Folder...
p4 sync //depot/gta5/art/ng/VFX/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGVFX=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab NG Exported VFX Asset Data
ECHO Grabbing Exported NG VFX Asset Data...
p4 sync //depot/gta5/assets_ng/export/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGAssets=1
	SET syncerror=1	
)
ECHO --------------------------------------------------

:: Grab NG Common Platform VFX Data
ECHO Grabbing NG Common Platform VFX Data...
p4 sync //depot/gta5/build/dev_ng/common/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCommon=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built VFX Data For Xbox One
ECHO Grabbing Built Xbox One Platform Data...
p4 sync //depot/gta5/build/dev_ng/xboxone/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGXbone=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built VFX Data For PS4
ECHO Grabbing Built PS4 Platform Data...
p4 sync //depot/gta5/build/dev_ng/ps4/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGPS4=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built VFX Data For PC
ECHO Grabbing Built PC Platform Data...
p4 sync //depot/gta5/build/dev_ng/x64/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGPC=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab VFX Tool Scripts
ECHO Grabbing VFX Tool Scripts...
p4 sync //depot/gta5/tools_ng/script/vfx/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGTools=1
	SET syncerror=1
)
ECHO --------------------------------------------------

ECHO+
ECHO+
ECHO Grabbing NG Heists DLC VFX Files...

:: Grab NG Heists DLC VFX Folder
ECHO --------------------------------------------------
ECHO Grabbing NG Heists DLC VFX Folder...
p4 sync //gta5_dlc/mpPacks/mpHeist/art/ng/vfx/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsVFX=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab NG Heists DLC Exported VFX Asset Data
ECHO Grabbing NG Heists DLC Exported VFX Asset Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/export/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsAssets=1
	SET syncerror=1	
)
ECHO --------------------------------------------------

:: Grab NG Heists DLC Common Platform VFX Data
ECHO Grabbing NG Heists DLC Common Platform VFX Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/common/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsCommon=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Heists DLC VFX Data For Xbox One
ECHO Grabbing NG Heists DLC Built Xbox One Platform Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/xboxone/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsXbone=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Heists DLC VFX Data For PS4
ECHO Grabbing NG Heists DLC Built PS4 Platform Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/ps4/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsPS4=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Heists DLC VFX Data For PC
ECHO Grabbing NG Heists DLC Built PC Platform Data...
p4 sync //gta5_dlc/mppacks/mpheist/build/dev_ng/x64/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGHeistsPC=1
	SET syncerror=1
)
ECHO --------------------------------------------------

ECHO+
ECHO+
ECHO Grabbing NG Clifford DLC VFX Files...

:: Grab NG Clifford DLC VFX Folder
ECHO --------------------------------------------------
ECHO Grabbing NG Clifford DLC VFX Folder...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/art/ng/vfx/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffVFX=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab NG Clifford DLC Exported VFX Asset Data
ECHO Grabbing NG Clifford DLC Exported VFX Asset Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/assets_ng/export/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffAssets=1
	SET syncerror=1	
)
ECHO --------------------------------------------------

:: Grab NG Clifford DLC Common Platform VFX Data
ECHO Grabbing NG Clifford DLC Common Platform VFX Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/common/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffCommon=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Clifford DLC VFX Data For Xbox One
ECHO Grabbing NG Clifford DLC Built Xbox One Platform Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/xboxone/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffXbone=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Clifford DLC VFX Data For PS4
ECHO Grabbing NG Clifford DLC Built PS4 Platform Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/ps4/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffPS4=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built NG Clifford DLC VFX Data For PC
ECHO Grabbing NG Clifford DLC Built PC Platform Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/x64/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET NGCliffPC=1
	SET syncerror=1
)
ECHO --------------------------------------------------

ECHO+
ECHO+
ECHO Grabbing CG Heists DLC VFX Files...

:: Grab CG Heists DLC VFX Folder
ECHO --------------------------------------------------
ECHO Grabbing CG Heists DLC VFX Folder...
p4 sync //gta5_dlc/mpPacks/mpHeist/art/vfx/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGHeistsVFX=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab CG Heists DLC Exported VFX Asset Data
ECHO Grabbing CG Heists DLC Exported VFX Asset Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/assets/export/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGHeistsAssets=1
	SET syncerror=1	
)
ECHO --------------------------------------------------

:: Grab CG Heists DLC Common Platform VFX Data
ECHO Grabbing CG Heists DLC Common Platform VFX Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev/common/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGHeistsCommon=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built CG Heists DLC VFX Data For Xbox 360
ECHO Grabbing CG Heists DLC Built Xbox 360 Platform Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev/xbox360/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGHeistsXB360=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built CG Heists DLC VFX Data For PS3
ECHO Grabbing CG Heists DLC Built PS3 Platform Data...
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev/ps3/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGHeistsPS3=1
	SET syncerror=1
)
ECHO --------------------------------------------------

ECHO+
ECHO+
ECHO Grabbing CG Clifford DLC VFX Files...

:: Grab CG Clifford DLC VFX Folder
ECHO --------------------------------------------------
ECHO Grabbing CG Clifford DLC VFX Folder...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/art/vfx/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGCliffVFX=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab CG Clifford DLC Exported VFX Asset Data
ECHO Grabbing CG Clifford DLC Exported VFX Asset Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/assets/export/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGCliffAssets=1
	SET syncerror=1	
)
ECHO --------------------------------------------------

:: Grab CG Clifford DLC Common Platform VFX Data
ECHO Grabbing CG Clifford DLC Common Platform VFX Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/common/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGCliffCommon=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built CG Clifford DLC VFX Data For Xbox 360
ECHO Grabbing CG Clifford DLC Built Xbox 360 Platform Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/xbox360/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGCliffXB360=1
	SET syncerror=1
)
ECHO --------------------------------------------------

:: Grab Built CG Clifford DLC VFX Data For PS3
ECHO Grabbing CG Clifford DLC Built PS3 Platform Data...
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/ps3/data/effects/...> %UserProfile%/Desktop/vfx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	SET CGCliffPS3=1
	SET syncerror=1
)
ECHO --------------------------------------------------

ECHO+
ECHO Gronst rating within expected tolerances.

::--- Error Checking --- 
IF %NGVFX% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG VFX Folder.
)
IF %NGAssets% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Exported Assets.
)
IF %NGCommon% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Common Data.
)
IF %NGXbone% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing Xbox One Platform Data.
)
IF %NGPS4% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing PS4 Platform Data.
)
IF %NGPC% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing PC Platform Data.
)
IF %NGTools% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing VFX Tool Scripts.
)
IF %NGHeistsVFX% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC VFX Folder.
)
IF %NGHeistsAssets% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Exported AsSETs.
)
IF %NGHeistsCommon% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Common Data.
)
IF %NGHeistsXbone% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Xbox One Platform Data.
)
IF %NGHeistsPS4% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC PS4 Platform Data.
)
IF %NGHeistsPC% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC PC Platform Data.
)
IF %CGHeistsVFX% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC VFX Folder.
)
IF %CGHeistsAssets% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Exported AsSETs.
)
IF %CGHeistsCommon% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Common Data.
)
IF %CGHeistsXB360% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Xbox 360 Platform Data.
)
IF %CGHeistsPS3% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC PS3 Platform Data.
)
IF %NGCliffVFX% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Clifford DLC VFX Folder.
)
IF %NGCliffAssets% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Exported AsSETs.
)
IF %NGCliffCommon% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Common Data.
)
IF %NGCliffXbone% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC Xbox One Platform Data.
)
IF %NGCliffPS4% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC PS4 Platform Data.
)
IF %NGCliffPC% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing NG Heists DLC PC Platform Data.
)
IF %CGCliffVFX% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Clifford DLC VFX Folder.
)
IF %CGCliffAssets% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Exported AsSETs.
)
IF %CGCliffCommon% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Common Data.
)
IF %CGCliffXB360% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC Xbox 360 Platform Data.
)
IF %CGCliffPS3% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing CG Heists DLC PS3 Platform Data.
)
IF %TUSync% EQU 1 (
	ECHO+
	ECHO WARNING: Problem Grabbing Title Update Folder.
)

::Sync Error Handling
IF %syncerror% EQU 0 (
	ECHO+
	ECHO Grab successful.
)
IF %syncerror% EQU 1 (
	ECHO+
	notepad %UserProfile%/Desktop/vfx_sync.txt
)
::---------------------------------------------------------------

ECHO+

PAUSE

:END
