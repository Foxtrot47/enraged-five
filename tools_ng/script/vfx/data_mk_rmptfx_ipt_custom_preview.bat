@ECHO OFF
CALL setenv.bat >NUL

IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET IPT_NAME=scr_prologue
SET IPT_PATH=%RS_EXPORT%\data\effects\%IPT_NAME%.ipt.zip
SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx\fxlists\core\core.texlist
REM SET SHARED_BASE=

%DOJOB% data_mk_rmptfx_ipt.bat %IPT_NAME% %SHARED_BASE%

REM Convert data (preview)
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\data_convert_file.rb --no-content --preview %IPT_PATH%

PAUSE
