@ECHO OFF
REM
REM data_mk_rmptfx_fxweather_itd.bat
REM Create the RMPTFX fxweather ITD and platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv_gen9.bat >NUL

SET ITD=fxweather
SET INPUT=%RS_PROJROOT%\art/ng/VFX/texture_dictionaries/%ITD%\
SET OUTPUT=%RS_EXPORT%\textures\%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

rem add --regentcs to the following line to force regeneration of tcs files
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%\util\data_mk_texture_dictionary.rb --filter *.dds --metadata %RS_ASSETS%\metadata\textures\vfx\txds\%ITD% --template %RS_ASSETS%\metadata\textures\templates\fx\Decal_Diffuse --output %OUTPUT% %INPUT%

IF ERRORLEVEL 1 GOTO ERROREND
%RS_TOOLSCONVERT% %OUTPUT%
GOTO END

:ERROREND

ECHO Texture dictionary construction failed.  See log.

:END
PAUSE

REM data_mk_rmptfx_fxweather_itd.bat
