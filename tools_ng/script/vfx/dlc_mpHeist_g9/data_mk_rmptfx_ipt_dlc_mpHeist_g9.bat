IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu_g9\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2

%DOJOB% data_mk_rmptfx_ipt_g9.bat wpn_flare %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_biolab_heist %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_carrier_heist %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_ornate_heist %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_prison_break_heist_station %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat veh_mounted_turrets_car %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat veh_mounted_turrets_heli %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_humane_fin %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_humane_key %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_narcotic_bike %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_narcotic_fin %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_prison_break %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat cut_pacific_fin %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_mp_tankbattle %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat fm_mission_controler %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%

