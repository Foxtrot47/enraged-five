IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu_g9\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2

%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_bike_adversary %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_bike_contraband %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat weap_pipebomb %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat weap_minismg %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_bike_contact %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat scr_bike_business %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt_g9.bat veh_sanctus %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
