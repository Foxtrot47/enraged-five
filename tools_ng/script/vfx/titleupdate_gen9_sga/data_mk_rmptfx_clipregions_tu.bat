@ECHO OFF
REM
REM data_mk_rmptfx_clipregions.bat
REM Create the RMPTFX clip region data file.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Luke Openshaw
REM

REM --------------------------------------------------------------------------
REM Configuration Data
REM   Hopefully this is the only stuff that will require changes moving from
REM   project to project.
REM --------------------------------------------------------------------------
CALL setenv_gen9_sga.bat

SETLOCAL

SET CLIP_SCRIPT=%CD%\createClipRegions.rb
SET FXTEX_PATH=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu_g9\textures\
SET CLIP_REGION_OUTPATH=%RS_TITLE_UPDATE%\common\data\effects
SET CLIP_REGIONS_DAT_FILE=ptxclipregions.dat
SET CLIP_REGIONS_DAT=%CLIP_REGION_OUTPATH%\%CLIP_REGIONS_DAT_FILE%

REM --------------------------------------------------------------------------
REM Checkout %CLIP_REGIONS_DAT%
REM --------------------------------------------------------------------------
PUSHD %CLIP_REGION_OUTPATH%
p4 edit %CLIP_REGIONS_DAT_FILE%
POPD

REM --------------------------------------------------------------------------
REM Run clip region script
REM   This writes out new versions of the textures to //regions// with clipable borders 
REM --------------------------------------------------------------------------
%RS_TOOLSRUBY% %CLIP_SCRIPT% --texdir=%FXTEX_PATH% --outdir=%CLIP_REGION_OUTPATH%
IF NOT %ERRORLEVEL% ==0 GOTO PYTHON_ERR

