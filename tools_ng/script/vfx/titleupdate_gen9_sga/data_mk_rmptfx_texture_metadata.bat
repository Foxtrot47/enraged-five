@ECHO OFF
REM 
REM File:: %RS_TOOLSROOT%/script/vfx/titleupdate_gen9_sga/data_mk_rmptfx_texture_metadata.bat
REM Description:: Create TCS templates for RMPTFX textures.
REM


CALL setenv_gen9_sga.bat >NUL
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%/util/vfx/data_mk_rmptfx_texture_metadata.rb --branch titleupdate_gen9_sga

PAUSE
