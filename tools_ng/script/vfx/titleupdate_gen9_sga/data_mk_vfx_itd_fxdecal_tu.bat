@ECHO OFF
REM
REM data_mk_rmptfx_fxdecal_itd.bat
REM Create the RMPTFX fxdecal ITD and platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv_gen9_sga.bat >NUL

SET ITD=fxdecal
SET INPUT=%RS_PROJROOT%\art/ng/VFX/texture_dictionaries/%ITD%/
SET OUTPUT=X:/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga/textures/%ITD%.itd.zip

:: Make sure the user is logged into perforce and sync the various things we need.
CALL %RS_TOOLSROOT%\script\util\check_p4_login.bat

p4 sync %INPUT%...
p4 sync %OUTPUT%
p4 edit %OUTPUT%

rem add --regentcs to the following line to force regeneration of tcs files
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%\util\data_mk_texture_dictionary.rb --filter *.dds --metadata x:\gta5\assets_gen9_sga\metadata\textures\vfx\txds\%ITD% --template x:\gta5\assets_gen9_sga\metadata\textures\templates\fx\Decal_Diffuse --output %OUTPUT% %INPUT%

for /F "tokens=*" %%i in ('dir /B %RS_ASSETS%\metadata\textures\vfx\txds\%ITD%\*.tcs') do (
	p4 add %RS_ASSETS%\metadata\textures\vfx\txds\%ITD%\%%i
)

IF ERRORLEVEL 1 GOTO ERROREND
%RS_TOOLSCONVERT% %OUTPUT%
GOTO END

:ERROREND

ECHO Texture dictionary construction failed.  See log.

:END
PAUSE

REM data_mk_rmptfx_fxdecal_itd.bat
