CALL setenv_gen9_sga.bat >NUL
SET PTFX_OVERRIDE_DIR=base
SET IPT_PATH=x:\gta5\assets_gen9_sga\titleupdate\dev_gen9_sga\patch\data\effects\%PTFX_OVERRIDE_DIR%
SET CACHE_PATH=x:\cache

CALL del %IPT_PATH%\*.* /s /q
CALL del %CACHE_PATH%\*.* /s /q

IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

REM don't use XGE on the stub file - because all the paralell jobs need to copy stub.ipt somewhere so we need to make sure it exists.
REM CALL data_mk_rmptfx_ipt stub

REM ===========================
REM Builds the Gen9 set of IPTs
REM ===========================

CALL data_mk_rmptfx_ipt_tu_g9.bat g9 %PTFX_OVERRIDE_DIR%

%WAITJOBS%

IF "%USE_XGE%" == "" (
REM Caller needs to do this for XGE builds - since internally it'll start its own XGE build.
CALL data_mk_rmptfx_rpf_g9.bat g9 %PTFX_OVERRIDE_DIR%
)
