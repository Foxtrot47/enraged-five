
IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx\fxlists\core\core.texlist
REM SET SHARED_BASE=

%DOJOB% data_mk_rmptfx_ipt.bat scr_michael4 %SHARED_BASE%