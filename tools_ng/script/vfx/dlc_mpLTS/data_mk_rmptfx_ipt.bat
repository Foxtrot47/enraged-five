@ECHO OFF
REM
REM data_mk_rmptfx_ipt.bat
REM Create the RMPTFX IPT and its resourced platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Luke Openshaw <luke.openshaw@rockstarnorth.com>
REM

REM --------------------------------------------------------------------------
REM Configuration Data
REM   Hopefully this is the only stuff that will require changes moving from
REM   project to project.
REM --------------------------------------------------------------------------
CALL setenv.bat >NUL
SETLOCAL
SET BRANCH=dev
SET IPT=%1

echo %*

SET PTFX_OVERRIDE_DIR=%2

SET EXCLUDE=%3
IF "%EXCLUDE%" NEQ "" (
	SET EXCLUDECMD=--excludetex=%3 --alwaysincludemodeltex
) ELSE (
	SET EXCLUDECMD=--alwaysincludemodeltex
)
SET OVERRIDECMD=--override=%PTFX_OVERRIDE_DIR%

SET IPT_FILENAME=x:\gta5_dlc\mpPacks\mpLTS\assets\export\data\effects\%PTFX_OVERRIDE_DIR%\%IPT%.ipt.zip

SET FXLIST_PATH=x:\gta5_dlc\mpPacks\mpLTS\art\vfx\rmptfx\fxlists
SET FXLIST_MASTER=x:\gta5_dlc\mpPacks\mpLTS\art\vfx\rmptfx\fxlists\%IPT%.fxlist
SET DEP_SCRIPT=%CD%\FindPtxDeps.py
REM --------------------------------------------------------------------------
REM Run Python Dependency Script
REM   This refreshes our lists in <art>/misc/rmptfx/fxlists which are read
REM   by our Ruby script later.  These scripts need to be writable for this
REM   step to work.
REM --------------------------------------------------------------------------
echo %RS_TOOLSPYTHON% %DEP_SCRIPT% %FXLIST_MASTER% %OVERRIDECMD% %EXCLUDECMD% 
%RS_TOOLSPYTHON% %DEP_SCRIPT% %FXLIST_MASTER% %OVERRIDECMD% %EXCLUDECMD%
IF NOT %ERRORLEVEL% ==0 GOTO PYTHON_ERR
REM --------------------------------------------------------------------------
REM Run Ruby IPT Build Script
REM --------------------------------------------------------------------------
SET CMDLINE=%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSBIN%\ironruby\bin\ir64.exe %RS_TOOLSROOT%\ironlib\util\vfx\data_mk_rmptfx_ipt.rb --branch %BRANCH% --fxlist %FXLIST_PATH% --ipt %IPT% --metadata x:\gta5\assets\metadata\textures\vfx\rmptfx\ --template x:\gta5\assets\metadata\textures\templates\fx\Particle_Diffuse --output %IPT_FILENAME% 
IF "%PTFX_OVERRIDE_DIR%" NEQ "base" (
	ECHO %PTFX_OVERRIDE_DIR%
	SET CMDLINE=%CMDLINE% --override %PTFX_OVERRIDE_DIR% 
)

ECHO %CMDLINE%
CALL %CMDLINE%

REM CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSBIN%\ironruby\bin\ir.exe %RS_TOOLSROOT%\ironlib\util\vfx\data_mk_rmptfx_ipt.rb --branch %BRANCH% --fxlist %FXLIST_PATH% --ipt %IPT% --metadata x:\gta5\assets\metadata\textures\vfx\rmptfx\ --template x:\gta5\assets\metadata\textures\templates\fx\Particle_Diffuse --output %IPT_FILENAME% --reconcile

IF NOT ERRORLEVEL 0 GOTO RUBY_ERR

GOTO END

REM --------------------------------------------------------------------------
REM Error message goto targets
REM --------------------------------------------------------------------------
:PYTHON_ERR
ECHO Error: python dependency script failed during execution: %ERRORLEVEL%
GOTO END

:RUBY_ERR
ECHO Error: data_mk_rmptfx_ipt.rb returned error code: %ERRORLEVEL%
GOTO END

:END

REM data_mk_rmptfx_ipt.bat

