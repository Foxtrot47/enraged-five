
IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET PTFX_OVERRIDE_DIR=%1

%DOJOB% data_mk_rmptfx_ipt.bat wpn_amrifle %PTFX_OVERRIDE_DIR% %SHARED_BASE%
