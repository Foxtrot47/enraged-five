IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2

%DOJOB% data_mk_rmptfx_ipt.bat cut_hs3f %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_ch_finale %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat weap_ch_weapons %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat weap_ch_vehicle_weapons %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%

