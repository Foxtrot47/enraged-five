
IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2

%DOJOB% data_mk_rmptfx_ipt.bat cut_casino_heist %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat cut_cia1 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat cut_paperman1 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat cut_russian2 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat cut_russian3 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat cut_russian5 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_dlc_clifford %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_dlc_assassinations %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_dlc_korean4 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat veh_stromberg %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat gad_clifford %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_dlc_cia4 %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%