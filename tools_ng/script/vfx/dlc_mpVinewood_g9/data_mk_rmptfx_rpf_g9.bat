@ECHO OFF
REM
REM data_mk_rmptfx_rpf.bat
REM Create the RMPTFX ZIP and its resourced platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Luke Openshaw
REM

REM --------------------------------------------------------------------------
REM Configuration Data
REM   Hopefully this is the only stuff that will require changes moving from
REM   project to project.
REM --------------------------------------------------------------------------
CALL setenv_gen9_sga.bat >NUL

SETLOCAL
SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2
SET BRANCH=dev_gen9_sga

SET PYTHONPATH=
SET OUTDIR=x:\gta5_dlc\mpPacks\mpVinewood\assets_gen9_disc\export\data\effects
IF %TAG% NEQ "" (
    SET TAG_DIR_OVERRIDE=%TAG%\
) ELSE (
    SET TAG_DIR_OVERRIDE=
)

IF "%PTFX_OVERRIDE_DIR%" == "base" (
    IF %TAG% NEQ "" (
        SET ZIP_FILENAME=%OUTDIR%\ptfx_%TAG%.zip
    ) ELSE (
        SET ZIP_FILENAME=%OUTDIR%\ptfx.zip
    )
) ELSE (
    IF %TAG% NEQ "" (
        SET ZIP_FILENAME=%OUTDIR%\ptfx_%PTFX_OVERRIDE_DIR%_%TAG%.zip
    ) ELSE (
        SET ZIP_FILENAME=%OUTDIR%\ptfx_%PTFX_OVERRIDE_DIR%.zip
    )
)

SET SOURCE_PATH=x:\gta5_dlc\mpPacks\mpVinewood\assets_gen9_disc\export\data\effects\%PTFX_OVERRIDE_DIR%\%TAG_DIR_OVERRIDE%
SET CLIP_SCRIPT=%CD%\createClipRegions.rb
SET FXTEX_PATH=x:\gta5_dlc\mpPacks\mpVinewood\art\ng\vfx\rmptfx_g9\textures\
SET CLIP_REGION_OUTPATH=x:\gta5_dlc\mpPacks\mpVinewood\build\dev_gen9_sga\common\data\effects
SET CLIP_REGIONS_DAT_FILE=ptxclipregions.dat
SET CLIP_REGIONS_DAT=%CLIP_REGION_OUTPATH%\%CLIP_REGIONS_DAT_FILE%

REM --------------------------------------------------------------------------
REM Checkout %CLIP_REGIONS_DAT%
REM --------------------------------------------------------------------------
PUSHD %CLIP_REGION_OUTPATH%
p4 edit %CLIP_REGIONS_DAT_FILE%
POPD

REM --------------------------------------------------------------------------
REM Checkout %ZIP_FILENAME%
REM --------------------------------------------------------------------------
PUSHD %OUTDIR%
p4 edit %ZIP_FILENAME%
POPD

REM --------------------------------------------------------------------------
REM Run clip region script
REM   This writes out new versions of the textures to //regions// with clipable borders 
REM --------------------------------------------------------------------------
REM %RS_TOOLSRUBY% %CLIP_SCRIPT% --texdir=%FXTEX_PATH% --outdir=%CLIP_REGION_OUTPATH%
REM IF NOT %ERRORLEVEL% ==0 GOTO PYTHON_ERR

REM --------------------------------------------------------------------------
REM Create the ZIP file
REM --------------------------------------------------------------------------
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --output=%ZIP_FILENAME% --uncompressed %SOURCE_PATH%\*.ipt.zip

REM --------------------------------------------------------------------------
REM Platform Conversion
REM --------------------------------------------------------------------------
echo %RS_TOOLSCONVERT% --branch %BRANCH% %ZIP_FILENAME%
%RS_TOOLSCONVERT% --branch %BRANCH% %ZIP_FILENAME%

PAUSE

REM data_mk_rmptfx_rpf.bat

