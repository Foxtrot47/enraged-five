@ECHO OFF
REM
REM data_mk_rmptfx_rpf.bat
REM Create the RMPTFX ZIP and its resourced platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM Luke Openshaw
REM

REM --------------------------------------------------------------------------
REM Configuration Data
REM   Hopefully this is the only stuff that will require changes moving from
REM   project to project.
REM --------------------------------------------------------------------------
CALL setenv.bat >NUL

SETLOCAL
SET PTFX_OVERRIDE_DIR=%1
SET BRANCH=dev

SET PYTHONPATH=
SET OUTDIR=x:\gta5_dlc\mpPacks\mpLuxe\assets\export\data\effects
IF "%PTFX_OVERRIDE_DIR%" == "base" (
	SET ZIP_FILENAME=%OUTDIR%\ptfx.zip
) ELSE (
	SET ZIP_FILENAME=%OUTDIR%\ptfx_%PTFX_OVERRIDE_DIR%.zip
)

SET SOURCE_PATH=x:\gta5_dlc\mpPacks\mpLuxe\assets\export\data\effects\%PTFX_OVERRIDE_DIR%\
SET CLIP_SCRIPT=%CD%\createClipRegions.rb
SET FXTEX_PATH=x:\gta5_dlc\mpPacks\mpLuxe\art\vfx\rmptfx\textures\
SET CLIP_REGION_OUTPATH=x:\gta5_dlc\mpPacks\mpLuxe\build\dev\common\data\effects
SET CLIP_REGIONS_DAT_FILE=ptxclipregions.dat
SET CLIP_REGIONS_DAT=%CLIP_REGION_OUTPATH%\%CLIP_REGIONS_DAT_FILE%

REM --------------------------------------------------------------------------
REM Checkout %CLIP_REGIONS_DAT%
REM --------------------------------------------------------------------------
PUSHD %CLIP_REGION_OUTPATH%
p4 edit %CLIP_REGIONS_DAT_FILE%
POPD

REM --------------------------------------------------------------------------
REM Checkout %ZIP_FILENAME%
REM --------------------------------------------------------------------------
PUSHD %OUTDIR%
p4 edit %ZIP_FILENAME%
POPD

REM --------------------------------------------------------------------------
REM Run clip region script
REM   This writes out new versions of the textures to //regions// with clipable borders 
REM --------------------------------------------------------------------------
REM %RS_TOOLSRUBY% %CLIP_SCRIPT% --texdir=%FXTEX_PATH% --outdir=%CLIP_REGION_OUTPATH%
REM IF NOT %ERRORLEVEL% ==0 GOTO PYTHON_ERR

REM --------------------------------------------------------------------------
REM Create the ZIP file
REM --------------------------------------------------------------------------
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --output=%ZIP_FILENAME% --uncompressed %SOURCE_PATH%\*.ipt.zip

REM --------------------------------------------------------------------------
REM Platform Conversion
REM --------------------------------------------------------------------------
%RS_TOOLSCONVERT% --branch dev %ZIP_FILENAME%

PAUSE

REM data_mk_rmptfx_rpf.bat

