IF "%USE_XGE%" NEQ "" (
	SET DOJOB=xgSubmit.exe /group=Build /allowremote=off /command
	SET WAITJOBS=xgWait.exe /group=Build
) ELSE (
	SET DOJOB=CALL
	SET WAITJOBS=
)

SET SHARED_BASE=%RS_PROJROOT%\art\ng\VFX\rmptfx_tu\fxlists\core\core.texlist
REM SET SHARED_BASE=

SET TAG=%1
SET PTFX_OVERRIDE_DIR=%2

p4 edit %RS_PROJROOT%\art\ng\VFX\rmptfx_tu\fxlists\core\core.texlist

%DOJOB% data_mk_rmptfx_ipt.bat core %TAG% %PTFX_OVERRIDE_DIR%

%DOJOB% data_mk_rmptfx_ipt.bat core_snow %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_director_mode %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_fm_mp_missioncreator	%TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat scr_mp_house %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%
%DOJOB% data_mk_rmptfx_ipt.bat pat_heist %TAG% %PTFX_OVERRIDE_DIR% %SHARED_BASE%


