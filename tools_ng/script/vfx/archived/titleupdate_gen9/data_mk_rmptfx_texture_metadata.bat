@ECHO OFF
REM 
REM File:: %RS_TOOLSROOT%/script/vfx/titleupdate_gen9/data_mk_rmptfx_texture_metadata.bat
REM Description:: Create TCS templates for RMPTFX textures.
REM


CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%/util/vfx/data_mk_rmptfx_texture_metadata.rb --branch dev_gen9

PAUSE
