#
# File:: %RS_TOOLSROOT%/script/vfx/createClipRegions.rb
# Description::
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 September 2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--texdir', '-t', OS::Getopt::REQUIRED, 'texture directory' ],
	[ '--outdir', '-o', OS::Getopt::REQUIRED, 'output directory' ]
]

#-----------------------------------------------------------------------------
# Entry-Point
#-----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	c = Pipeline::Config::instance( )
	p = c.project
	log = Log::new( OS::Path::get_basename( __FILE__ ) )
	begin
		opts, trailing = OS::Getopt::getopts( OPTIONS )

		texturedir = opts['texdir']
		outputdir = opts['outdir']
		toolsbin = Globals::instance.toolsbin

		textures = []
		region_textures_filename = OS::Path::combine( texturedir, 'regiontextures.txt' )
		if ( not File::exists?( region_textures_filename ) ) then
			log.error( "Regions texture input file does not exist (#{region_textures_filename})." )
			exit( 1 )
		end
		
		File::readlines( region_textures_filename ).each do |texline|

			elements = texline.split(" ")
			actName = elements[0]
			fname = actName + ".dds"
			outname = actName + "_regions.dds"
			rname = outname + ".clipregions"

			textures << rname

			params = "-atlasX=#{elements[1].strip} -atlasY=#{elements[2].strip} -clipthreshold=0.0001 -miplevels=1"
			com = "#{toolsbin}\\rageTextureConvert -in=#{texturedir}\\#{fname} -particleClipArea -out=#{texturedir}\\regions\\#{outname} #{params}"

			log.info( "Running: #{com}." )
			system(com)
		end

		File.open(OS::Path.combine(outputdir,"ptxclipregions.dat"),"w+") do |file|

			file.write("#{textures.length}\n")

			memsize = 0

			textures.each do |texture|

				lines = File.readlines(OS::Path.combine(texturedir,"regions",texture))
				elements = lines[0].split(" ")
				memsize = memsize + (elements[0].to_i * elements[1].to_i)
			end

			file.write("#{memsize}\n")

			textures.each do |texture|
				lines = File.readlines(OS::Path.combine(texturedir,"regions",texture))
				file.write("#{texture.split("_regions")[0]} ")
				file.write("#{lines[0]}\n")
			end
		end
	end
end

# %RS_TOOLSROOT%/script/vfx/createClipRegions.rb
