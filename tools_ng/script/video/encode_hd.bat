@echo off

rem GRS 15/3/2011
rem assumes that extensions are three characters...

CALL setenv.bat >NUL

set infile=%1%
set noext=%infile:~0,-4%
set outfile=%noext%.mp4
set cmd=%RS_TOOLSRUBY% %RS_TOOLSROOT%\script\video\encode.rb --profile=HD --input=%infile%

call %cmd%