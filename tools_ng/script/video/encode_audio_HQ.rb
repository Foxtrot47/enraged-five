#
# File:: encode.rb
# Description:: Encodes a video locally using ffmpeg
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 15/3/2011
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/start'
require 'pipeline/os/path'
require 'pipeline/os/getopt'

Pipeline::OS::Path.set_downcase_on_normalise(false)

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

config = Pipeline::Config.instance
globals = Pipeline::Globals.instance

OPTIONS = [
	[ '--profile', '-p', OS::Getopt::REQUIRED, 'profile to encode at: HD, SD, LD' ],
	[ '--input', '-i', OS::Getopt::REQUIRED, 'input' ],
	[ '--output', '-m', OS::Getopt::OPTIONAL, 'output' ]
]

ENV["FFMPEG_DATADIR"] = Pipeline::OS::Path.dos_format(Pipeline::OS::Path.combine(globals.toolsroot,"etc/video/ffmpeg"))

opts, trailing = OS::Getopt.getopts( OPTIONS )

in_file = ( nil == opts['input'] ) ? "X:/gta5/tools/local/wildlife.wmv" : opts['input']
out_file = ( nil == opts['output'] ) ? nil : opts['output']

if out_file == nil then

	out_file = OS::Path.remove_extension(in_file) + "_out.mp4"
end

in_file = OS::Path.normalise(in_file)
out_file = OS::Path.normalise(out_file)

if in_file == out_file then

	puts("converter cannot write over itself (#{in_file} over #{out_file}, quiting")
	STDIN.getc
	System.exit(0)
end

in_file = "\"#{in_file}\""
out_file = "\"#{out_file}\""

stream = ( nil == opts['profile'] ) ? 'LD' : opts['profile']
pad = nil
keep = false

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

if stream == "HD" then

    ffprofile = "hd"
    crfsetting = "18"
    framesize = "1280x720"
elsif stream == "SD" then

    ffprofile = "sd"
    crfsetting = "22"
    framesize = "640x360"
	
elsif stream == "LD" then

    ffprofile = "ld"
    crfsetting = "25"
    framesize = "320x180"

elsif stream == "HD_fullscreen" then

    ffprofile = "hd"
    crfsetting = "18"
    framesize = "960x720"
	pad = "1280:720:160:0:black"
elsif stream == "SD_fullscreen" then

    ffprofile = "sd"
    crfsetting = "22"
    framesize = "480x360"
	pad = "640:360:80:0:black"
elsif stream == "LD_fullscreen" then

    ffprofile = "ld"
    crfsetting = "25"
    framesize = "240x180"
	pad = "320:180:40:0:black"
	
elsif stream == "HD_keep" then

    ffprofile = "hd"
    crfsetting = "18"
	keep = true
elsif stream == "SD_keep" then

    ffprofile = "sd"
    crfsetting = "22"
	keep = true
elsif stream == "LD_keep" then

    ffprofile = "ld"
    crfsetting = "25"
	keep = true
end

if keep then

	ffmpeg_bin = "#{Pipeline::OS::Path.combine(globals.toolsbin,"video/ffmpeg")}.exe"
	ffmpeg_cmd = [ffmpeg_bin,'-i', in_file, '-pix_fmt', 'yuv420p', '-ar', '48000', '-b:a','192k', '-f', 'mp4', '-r', '30', '-vcodec', 'libx264', '-strict', 'experimental', '-acodec', 'aac', '-vpre', ffprofile, '-crf', crfsetting, '-y', out_file].join(" ")
else

	ffmpeg_bin = "#{Pipeline::OS::Path.combine(globals.toolsbin,"video/ffmpeg")}.exe"
	ffmpeg_cmd = [ffmpeg_bin,'-i', in_file, '-pix_fmt', 'yuv420p', '-ar', '48000', '-b:a','192k', '-f', 'mp4', '-r', '30', '-vcodec', 'libx264', '-strict', 'experimental', '-acodec', 'aac', '-s', framesize, '-vpre', ffprofile, '-crf', crfsetting, '-y', out_file].join(" ")
	ffmpeg_cmd = ffmpeg_cmd + " -vf pad=" + pad if pad != nil
end

puts "EXE: #{ffmpeg_bin}"	
puts("running #{ffmpeg_cmd}")

#status, stdout, stderr = Pipeline::OS.start(ffmpeg_cmd)
result = system(ffmpeg_cmd)
if (not result) then
	puts "FFMPEG failed."
	$stdin.gets
end