@ECHO OFF

CALL setenv.bat

SET INPUT=X:/gta5/art/debug/textureconversion/%~n1
SET OUTPUT=X:/gta5/assets/export/textures/test/%~n1.itd

if not exist x:\gta5\assets\export\textures\test mkdir x:\gta5\assets\export\textures\test
if not exist C:\dump mkdir C:\dump
del C:\dump\texconvlog_*.txt

ruby %RS_TOOLSLIB%\util\data_mk_generic_rpf.rb --project=%RS_PROJECT% --branch=dev_ng --filter=*.* --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE
