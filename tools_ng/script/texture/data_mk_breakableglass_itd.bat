@ECHO OFF
REM
REM data_mk_breakableglass_itd.bat
REM Create the breakable glass ITD and platform files.
REM
REM Anoop Thomas <anoop.thomas@rockstarnewengland.com>
REM

CALL setenv.bat
SET ITD=breakableglass

SET INPUT=%RS_PROJROOT%\art\ng\Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev_ng --filter=*.*  --metadata=%RS_PROJROOT%\assets_ng\metadata\textures\miscs\%ITD% --template=%RS_PROJROOT%\assets_ng\metadata\textures\templates\globals\SkipProcessing --output=%OUTPUT% %INPUT%

%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_rope_itd.bat
