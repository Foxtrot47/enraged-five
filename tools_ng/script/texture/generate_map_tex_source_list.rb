
#
# File:: script/generate_map_tex_source_list.rb
# Description::
# This generates a csv list of all the textures
# and their sources in order to be able to re-export them from max.
# It uses the list of tcs files as the master list of textures to re-export
# and searches through the scene xml files for their source including
# any merged textures maps (diffuse + alpha)
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 16th May 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require "pipeline/os/path.rb"
require "rexml/document"

class TextureInfo

  attr_accessor :out_name, :src

  def initialize( out_name )

    @out_name = out_name
    @src = nil
  end
end

c = Pipeline::Config::instance( )
p = c.project

tcs_dir = "#{p.root}/assets/metadata/textures/maps_half"
src_dir = "#{p.root}/art/Textures"
xml_dir = "#{p.root}/assets/export/levels"
out_file = "x:/out.csv"
notfound_file = "x:/notfound.csv"

texture_hash = Hash.new()
nosrc_hash = Hash.new()

dir = Dir.new(tcs_dir)

Dir.glob(tcs_dir +"/*.tcs" ).each { |entry|

  name = Pipeline::OS::Path::get_basename(entry)
  texture_hash[name] = TextureInfo.new(name)
}

Dir.glob(xml_dir + "/**/*.xml").each { |entry|

  puts(entry)

  file = File.new( entry )
  doc = REXML::Document.new file

  doc.root.each_element("//material/textures") { |elem|

    tex_types = Hash.new()

    elem.each_element("texture") { |tex_node|

      tex_types[tex_node.attributes["type"]] = tex_node.attributes["filename"]
    }

    if tex_types["Diffuse Texture Alpha"] and tex_types["Diffuse Texture"] then

      src = tex_types["Diffuse Texture"] + "+" + tex_types["Diffuse Texture Alpha"]
      dst = Pipeline::OS::Path::get_basename(tex_types["Diffuse Texture"]) + Pipeline::OS::Path::get_basename(tex_types["Diffuse Texture Alpha"])

      if texture_hash[dst] then

        if File.exists?(tex_types["Diffuse Texture"]) then

          if File.exists?(tex_types["Diffuse Texture Alpha"]) then

              texture_hash[dst].src = src
          else

            nosrc_hash[tex_types["Diffuse Texture Alpha"]] = true
          end
        else

          nosrc_hash[tex_types["Diffuse Texture"]] = true
        end
      end

      tex_types.delete("Diffuse Texture Alpha")
      tex_types.delete("Diffuse Texture")
    end

    tex_types.each {|key,value|

      nexr if value == nil
      next if value == ""
      
      src = value
      dst = Pipeline::OS::Path::get_basename(value)

      if texture_hash[dst] then

        if File.exists?(src) then

          texture_hash[dst].src = src
        else

          nosrc_hash[src] = true
        end
      end
    }
  }
}

count = 0

File.open(out_file,"w") { |file|

  texture_hash.each {|key,value|

    if value.src == nil then

      #puts("couldnt find src for #{key}")
      count = count + 1
    else

      file.write("#{value.src},#{key}\n")
    end
  }
}

File.open(notfound_file,"w") {|file|

  nosrc_hash.keys.sort.each{|key|

    file.write("#{key}\n")
  }
}

puts("couldnt find src for #{count} out of #{texture_hash.size} texture(s)")