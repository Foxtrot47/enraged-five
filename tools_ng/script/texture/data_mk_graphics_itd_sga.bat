@ECHO OFF
REM
REM data_mk_rope_itd.bat
REM Create the graphics ITD and platform files.
REM
REM Alex Hadjadj <alex.hadjadj@rockstarnorth.com>
REM

CALL setenv_gen9_sga.bat >NUL
SET ITD=graphics

SET INPUT=%RS_PROJROOT%\art\ng\Textures/%ITD%/
SET OUTPUT=%RS_ASSETS%/titleupdate/dev_gen9_sga/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=titleupdate_gen9_sga --filter=*.*  --metadata=%RS_PROJROOT%\assets_gen9_sga\metadata\textures\miscs\%ITD% --template=%RS_PROJROOT%\assets_gen9_sga\metadata\textures\templates\globals\SkipProcessing --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_graphics_itd.bat
