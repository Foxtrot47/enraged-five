@ECHO OFF
REM
REM data_mk_generic_itd.bat
REM Create the breakable glass ITD and platform files.
REM
REM Anoop Thomas <anoop.thomas@rockstarnewengland.com>
REM

set argC=0
for %%x in (%*) do Set /A argC+=1

IF %argC% NEQ 1 GOTO ERROR_HANDLER

CALL setenv.bat
SET ITD=%1

SET INPUT=%RS_PROJROOT%\art\Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev_ng --filter=*.*  --metadata=%RS_PROJROOT%\assets_ng\metadata\textures\miscs\%ITD% --template=%RS_PROJROOT%\assets_ng\metadata\textures\templates\globals\SkipProcessing --output=%OUTPUT% %INPUT%

%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_rope_itd.bat

GOTO EOF
:ERROR_HANDLER
	echo Incorrect Syntax:
	echo data_mk_generic_itd.bat -name of folder-

:EOF
