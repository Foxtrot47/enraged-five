@ECHO OFF
REM
REM data_mk_graphics_pc_itd.bat
REM Create the graphics_pc ITD and platform files.
REM
REM Alex Hadjadj <alex.hadjadj@rockstarnorth.com>
REM

CALL setenv.bat
SET ITD=graphics_pc

SET INPUT=%RS_PROJROOT%\art\ng\Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --filter=*.*  --metadata=%RS_PROJROOT%\assets_ng\metadata\textures\miscs\%ITD% --template=%RS_PROJROOT%\assets_ng\metadata\textures\templates\globals\SkipProcessing --output=%OUTPUT% %INPUT%

%RS_TOOLSCONVERT% --branch dev_ng %OUTPUT%

PAUSE

REM data_mk_graphics_pc_itd.bat
