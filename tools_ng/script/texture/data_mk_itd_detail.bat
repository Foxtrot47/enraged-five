@ECHO OFF
REM
REM data_mk_itd_detail.bat
REM Create the Detail Map ITD and platform files.
REM
REM Raymond Kerr <rkerr@rockstarsandiego.com>
REM

CALL setenv.bat
SET ITD=mapdetail
SET INPUT=%RS_PROJROOT%\art/Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

rem add --regentcs to the following line to force regeneration of tcs files
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev_ng --filter=*.* --metadata=%RS_PROJROOT%\assets_ng\metadata\textures\%ITD% --template=%RS_PROJROOT%\assets_ng\metadata\textures\templates\maps_other\DetailMap.tcp --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_itd_detail.bat
