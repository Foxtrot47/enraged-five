const urlParams = new URLSearchParams(window.location.search);
var tBodiesShown = 20;
var current_page = 0;
var current_id = 0;
var num_pages = 0;
const start_page = urlParams.get('page');
const start_id = urlParams.get('id');

function next_page()
{
    goto_page(current_page + 1);
}
function previous_page()
{
    goto_page(current_page + -1);
}
function goto_page(page, push_history=true)
{
    var pages = Array.from($("#nav a"));
    if (page < 0) { page = num_pages-1; }
    if (page >= num_pages) { page = 0; }
    current_page = page;
    clearSelection();
    $(pages[current_page]).trigger("click", {push_history});
}
function setParamState(dict) {
    if (history.pushState)
    {
        let searchParams = new URLSearchParams(window.location.search);
        for(key in dict)
        {
            searchParams.set(key, dict[key]);
        }
        var removals = []
        for (var key of searchParams.keys())
        {
            if(!(key in dict))
            {
                removals.push(key);
            }
        }
        for(i in removals)        
        {
            searchParams.delete(removals[i]);
        }
        let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
        window.history.pushState({ path: newurl }, '', newurl);
    }
}
function getUrlParams(key) {
    let searchParams = new URLSearchParams(window.location.search);
    return searchParams.get(key);
}

function clearSelection() {
    if (window.getSelection) { window.getSelection().removeAllRanges(); }
    else if (document.selection) { document.selection.empty(); }
}
function paginate() {
    $('.compare').append('<div id="nav"><p class="pages">Pages</p></div>');
    var tbodyTotal = $('#diff_container tbody').length;
    num_pages = Math.ceil(tbodyTotal / tBodiesShown);
    for (i = 0; i < num_pages; i++) {
        var pageNum = i + 1;
        $('#nav').append('<a data="' + i + '" style="cursor: pointer;">' + pageNum + '</a> ');
    }
    $('#diff_container tbody').hide();
    $('#diff_container tbody').slice(0, tBodiesShown).show();
    $('#nav a:first').addClass('active');
    $('#nav a').bind('click', function (event, args)
    {
        $('.diff_container').scrollTop(0);
        $('#nav a').removeClass('active');
        $(this).addClass('active');
        var currPage = $(this).attr('data');
        var pages = Array.from($("#nav a"));
        current_page = pages.indexOf(this);
        var startItem = currPage * tBodiesShown;
        var endItem = startItem + tBodiesShown;
        $('#diff_container tbody').css('opacity', '0.0').hide().slice(startItem, endItem).
            css('display', 'table-row-group').animate({
                opacity: 1
            }, 300);
        $(".next").css("height", $(".diff_container").height());
        $(".prev").css("height", $(".diff_container").height());
        clearSelection();
        if (!args || args["push_history"])
        {
            current_id = null;
            setParamState({"page": current_page + 1});
        }
    });
    $(".compare").css("display", "");
    $(".compare").css('display', 'block').animate({
        opacity: 1
    }, 300);
    $(".next").css("height", $(".diff_container").height());
    $(".prev").css("height", $(".diff_container").height());
    add_section_headers();
    if(start_page != null)
    {
        goto_page(parseInt(start_page)-1, false);
    }
    setTimeout(function () { focus_link(start_id); }, 300);
}

function focus_link(id)
{
    if (id != null) {
        var container = $(".diff_container")
        var anchor = $("#" + id);
        container.animate({
            scrollTop: anchor.position().top - (container.height() * 0.5)
        }, 300);
        var parent = anchor.parent();
        highlight(parent);
    }
}

window.onpopstate = function (event)
{
    var page = getUrlParams("page");
    if(page === null)
    {
        page = 1
    }
    else
    {
        page = parseInt(page);
    }
    var id = getUrlParams("id");
    if (current_page != page - 1)
    {
        goto_page(page - 1, false);
    }
    else if(page === undefined)
    {
        goto_page(0, false);
    }
    if (id && id != current_id)
    {
        focus_link(id);
    }
    current_id = id;
};
function highlight(element, count = 0, loops = 3) {
    $(element).animate({ "opacity": ".1" }, 300,
        function () {
            $(this).animate({ "opacity": "1" }, 300,
                function () {
                    count++;
                    if (count < loops) {
                        highlight(element, count, loops);
                    }
                }
            );
        });
}
function link_clicked(param)
{
    if(param.classList.contains("diff_header"))
    {
        current_id = param.id;
        setParamState({ "page": current_page + 1, "id": current_id })
        // potentially copy to clipboard
        // navigator.clipboard.writeText(window.location.href);
        focus_link(current_id);
    }
    else
    {
        var parent = param.parentNode;
        var table_row = parent.parentNode;
        var headers = $(table_row).children(".diff_header");
        for(i = 0; i < headers.length; i++)
        {
            var id = headers[i].id;
            if (id)
            {
                current_id = id;
                setParamState({"page":current_page+1, "id":current_id})
                // potentially copy to clipboard
                // navigator.clipboard.writeText(window.location.href);
                focus_link(current_id);
                break;
            }
        }
    }
}

function add_section_headers()
{
    var bodies = document.querySelectorAll('#diff_container tbody');
    for (i = 0; i < bodies.length; i++)
    {
        headers = bodies[i].querySelectorAll(".diff_header");
        if(headers.length > 2)
        {
            var formheader = "";
            var toheader = "";
            var flnum = parseInt(headers[0].textContent);
            var tlnum = parseInt(headers[1].textContent);
            for(ii = 0; ii < fromheaders.length; ii++)
            {
                var num = fromheaders[ii][0];
                if(num <= flnum)
                {
                    formheader = fromheaders[ii][1];
                }
                else
                {
                    break;
                }
            }
            for(ii = 0; ii < toheaders.length; ii++)
            {
                var num = toheaders[ii][0];
                if (num <= tlnum)
                {
                    toheader = toheaders[ii][1];
                }
                else
                {
                    break;
                }
            }
            if(formheader && i != 0)
            {
                bodies[i - 1].childNodes[0].childNodes[1].textContent = formheader;
            }
            if (toheader && i != 0)
            {
                bodies[i - 1].childNodes[0].childNodes[3].textContent = toheader;
            }
        }
    }
}

function reportWindowSize()
{
    $(".next").css("height", $(".diff_container").height());
    $(".prev").css("height", $(".diff_container").height());
}
window.onresize = reportWindowSize;

$(document).ready(function () {paginate();})

