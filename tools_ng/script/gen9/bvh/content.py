
import os
import argparse
from tkinter import Tcl

content_str = """
<?xml version="1.0" encoding="utf-8"?>
<CDataFileMgr__ContentsOfDataFileXml>
	<disabledFiles />
	<includedXmlFiles />
	<dataFiles>
    {datafiles}
    </dataFiles>
    <contentChangeSets>
        <Item>
			<changeSetName>CCS_BVHPATCH00_INIT</changeSetName>
			<filesToEnable>
			</filesToEnable>
		</Item>
		<Item>
			<changeSetName>{changeset}</changeSetName>
			{enabled}
		</Item>
	</contentChangeSets> 
    <patchFiles/>
</CDataFileMgr__ContentsOfDataFileXml>
"""
filename_str = "<filename>{filename}</filename>"
enable_str = """
        <filesToEnable>
        {files}
        </filesToEnable>"""
datafile_str = """<Item>
    {filename}
    <fileType>RPF_FILE</fileType>
    <disabled value=\"true\"/>
</Item>
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-mp", '--mount_path', metavar='mount:/directory/', type=str, required=True,
    help='mount path to use as a base path for the rpfs')

    parser.add_argument("-p", '--path', metavar='X:/directory/', type=str, required=True,
    help='path to the tiles')
    
    parser.add_argument("-cs", '--changeset', metavar='CCS', type=str, required=True,
    help='changeset to load mount the files against')

    parser.add_argument("-o", '--output', metavar='X:/directory/', type=str,
    help='output directory for content.xml')

    args = parser.parse_args()

    files = os.listdir(args.path)
    files = Tcl().call("lsort", "-dict", files)
    
    print("Adding:\n    "+"\n    ".join([os.path.join(args.path, f) for f in files]))
    filenames = [filename_str.format_map({"filename":  args.mount_path+f}) for f in files]
    enabled = enable_str.format_map({"files": "\n".join(filenames)})
    datafiles = []
    for file in filenames:
        datafiles.append(datafile_str.format_map({"filename": file}))
    
    content = content_str.format_map(
        {
            "datafiles": "\n".join(datafiles),
            "changeset": args.changeset,
            "enabled": enabled
        })
    file_path = os.path.join(args.output, "content.xml")
    with open(file_path, "w") as out:
        out.write(content)
        print(f"wrote {file_path}")
        
    pass