import datetime
import shlex
import subprocess
import rsgdnd.utils as utils

def login():
    import rsgdnd.login as login
    utils.log("Logging into perforce")
    while(True):
        try:
            details = login.get_details("rsg")
            p4command = "echo "+details["password"]+"| p4 login"
            subprocess.check_output(p4command, shell=True).decode("utf-8")
            return True
        except Exception:
            try:
                if login.write_details("rsg"):
                    continue
                else:
                    utils.log("Aborting login")
                    return False
            except:
                continue

def get_token():
    import rsgdnd.login as login
    while(True):
        try:
            details = login.get_details("rsg")
            password = details["password"]
            username = details["username"]
            p4command = f"echo {password}|p4 -p rsgperforce:1666 -u {username} login -p"
            token = subprocess.check_output(p4command, shell=True).decode("utf-8")
            return token.splitlines()[-1]
        except Exception:
            try:
                if login.write_details("rsg"):
                    continue
                else:
                    utils.log("Aborting login")
                    return False
            except:
                continue

def extract_user(line):
    if line.startswith("Change"):
        user = line.split("by ")[1].split("@")[0]
        return user
    return None

def extract_date(line):
    if line.startswith("Change"):
        date = datetime.date(*map(int, line.split("on ")[1].split(" by")[0].split()[0].split("/")))
        return date
    return None

def extract_changelist(line):
    if line.startswith("Change"):
        return line.split("Change ")[1].split(" on")[0]
    return None

@utils.file_cache("p4cache/print", 86400)
def print_file(path):
    p4command = "p4 print "+path
    utils.log("running: "+p4command)
    output = subprocess.check_output(p4command, shell=True, stderr=subprocess.STDOUT).decode("utf-8", errors="ignore")
    output = output.split("\n")
    output = [line for line in output if line.strip() != ""]
    return output

@utils.file_cache("p4cache/where", 86400)
def where(path):
    p4command = "p4 where "+path
    utils.log("running: "+p4command)
    output = subprocess.check_output(p4command, shell=True, stderr=subprocess.STDOUT).decode("utf-8", errors="ignore")
    return shlex.split(output)

def get_changes(branches, start=None, end=None, user=None):
    datestr = ""
    userstr = ""
    if start and end:
        start = str(start).replace("-", "/")
        end = str(end).replace("-", "/")
        datestr = "@"+start+","+end
        pass
    elif start:
        start = str(start).replace("-", "/")
        datestr = "@"+start+",@now"
    elif end:
        end = str(end).replace("-", "/")
        datestr = "@"+end
    if user:
        userstr = " -u "+user
    if branches:
        if datestr:
            p4command = "p4 changes"+userstr+" -t -s submitted -l "+f"{datestr} ".join(branches)+f"{datestr} "
        else:
            # fail safe only doing 100 if you don't specify a date range.
            p4command = "p4 changes"+userstr+" -t -m 100 -s submitted -l "+f"{datestr} ".join(branches)+f"{datestr} "
        utils.log("running: "+p4command)
        output = subprocess.check_output(p4command, shell=True, stderr=subprocess.STDOUT).decode("utf-8", errors="ignore")
        output = output.split("\n")
        output = [line for line in output if line.strip() != ""]
        return output
    else:
        return None
    pass