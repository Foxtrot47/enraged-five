branches = {
    "dev_ng": [
        "//depot/gta5/src/dev_ng/...",
        "//rage/gta5/dev_ng/rage/...",
        "//depot/gta5/script/dev_ng/...",
        "//depot/gta5/build/dev_ng/...",
        "//depot/gta5/titleupdate/dev_ng/..."
    ],
    "dev_ng_live": [
        "//depot/gta5/src/dev_ng_live/...",
        "//rage/gta5/dev_ng_live/rage/...",
        "//depot/gta5/script/dev_ng_live/...",
        "//depot/gta5/script/dev_ng_Live/...",
        "//depot/gta5/build/dev_ng_Live/...",
        "//depot/gta5/titleupdate/dev_ng_Live/..."
    ],
    "dev_gen9_sga": [
        "//depot/gta5/src/dev_gen9_sga/...",
        "//rage/gta5/dev_gen9_sga/rage/...",
        "//depot/gta5/script/dev_gen9_sga/...",
        "//depot/gta5/build/dev_gen9_sga/...",
        "//depot/gta5/titleupdate/dev_gen9_sga/..."
    ],
    "dev_gen9_sga_unstable": [
        "//depot/gta5/src/dev_gen9_sga_unstable/...",
        "//rage/gta5/dev_gen9_sga_unstable/rage/..."
    ],
    "dev_gen9_sga_live": [
        "//depot/gta5/src/dev_gen9_sga_live/...",
        "//rage/gta5/dev_gen9_sga_live/rage/...",
        "//depot/gta5/script/dev_gen9_sga_live/...",
        "//depot/gta5/build/dev_gen9_sga_live/...",
        "//depot/gta5/titleupdate/dev_gen9_sga_live/..."
    ],
    "assets_gen9": [
        "//depot/gta5/assets_gen9_disc/...",
        "//depot/gta5/assets_gen9_sga/..."
    ],
    "assets_ng": [
        "//depot/gta5/assets_ng/..."
    ],
    "dlc_g9ec": [
        "//gta5_dlc/mpPacks/mpG9EC/..."
    ],
    "dlc_fixer": [
        "//gta5_dlc/mpPacks/mpFixes01/...",
        "//gta5_dlc/mpPacks/mpFixes02/..."
    ]
}
