#!/usr/bin/env python
import os
import base64
import pickle
import getpass
import rsgdnd.utils as utils

details = {}

def obfuscate_password(password):
    password = password.swapcase()
    password = password[::-1]
    password = password[2:]+password[:2]
    password = base64.b64encode(password.encode("utf-8")).decode("utf-8")
    return password


def deobfuscate_password(password):
    password = base64.b64decode(password).decode("utf-8")
    password = password.swapcase()
    password = password[::-1]
    password = password[2:]+password[:2]
    return password


def write_details(filename):
    login = utils.confirm_prompt("login?")
    if login:
        username = getpass.getuser()
        username = input("username["+username+"]: ") or username
        password = getpass.getpass()
        details = {"username": username, "password": obfuscate_password(password)}
        _filename = filename
        if os.path.exists(_filename):
            os.remove(_filename)
        with open(_filename, "wb") as f_details:
            pickle.dump(details, f_details)
        # subprocess.check_call(["attrib","+H",_filename])
    else:
        utils.log("cancelled login.")
    return login

def get_details(filename):
    try:
        _filename = filename
        with open(_filename, "rb") as f_details:
            details = pickle.load(f_details)
        details["password"] = deobfuscate_password(details["password"])
        return details
    except Exception:
        utils.log("login details not found")
        if write_details(filename):
            return get_details(filename)
        else:
            return None

if __name__ == "__main__":
    details = get_details("test")
    if details and details == {"username": "test", "password": "testPass!"}:
        utils.log("logged in.")
    else:
        utils.log("failed login.")

    pass