import os
import rsgdnd.utils as utils

def dumptree_to_dict(lines, sizes=True):
    commonpath = os.path.commonpath([l for l in lines if l[0] != " "])
    commonpath = commonpath.replace("\\", "/")
    tree = { "commonpath": commonpath }

    current_depth = 0
    current = tree
    stack = []

    for line in lines:
        line = line.replace("\n", "")
        line, size = line.split(", ")
        line = line.replace(commonpath, "")
        depth = int(len(line.split("/")[0]) / 4)
        line = line.lstrip()

        while depth < current_depth:
            current = stack.pop()
            current_depth -= 1

        if ".rpf" in line:
            new = {}
            if sizes:
                new["size"] = int(size)
            current[line] = new
            stack.append(current)
            current = new
            current_depth = depth+1
        else:
            new = {}
            if sizes:
                new["size"] = int(size)
            current[line] = new
    return tree

def dict_to_treelines(tree, depth=0):
    lines = []
    for k in tree:
        if k in ["commonpath", "size"]:
            continue
        line = k
        if "commonpath" in tree:
            line = (tree["commonpath"]+k)

        line = line.rjust(len(line)+depth*4)

        if "size" in tree[k]:
            line = line + ", " + str(tree[k]["size"])

        lines.append(line)
        lines = lines+dict_to_treelines(tree[k], depth+1)
    return lines

def sort_dict(tree):
    tree = dict(sorted(tree.items()))
    for k in tree:
        if not isinstance(tree[k], dict):
            continue
        tree[k] = sort_dict(tree[k])
    return tree

def sort_file_rpfs(filename):
    with open (filename) as file:
        lines = file.readlines()

    tree = dumptree_to_dict(lines)
    for k in tree:
        if ".rpf" in k:
            tree[k] = sort_dict(tree[k])

    lines = dict_to_treelines(tree)
    with open (filename, "w") as file:
        file.write("\n".join(lines))

def merge_branch(tree, frombranch, tobranch, keep_from=False, sort=False):
    for k, v in tree[frombranch].items():
        if k in tree[tobranch]:
            utils.log(tobranch+" key collision!! "+k+" replacing, lets hope it's the same.")
        tree[tobranch][k] = v

    if not keep_from:
        del tree[frombranch]

    if sort:
        tree[tobranch] = dict(sorted(tree[tobranch].items()))

    return tree