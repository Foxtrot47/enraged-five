const urlParams = new URLSearchParams(window.location.search);
const start_filter = urlParams.get('filter');
const input = document.getElementById("bug_filter_input");
var buglist_cache = []
input.value = start_filter;

function sleepms(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function populate_buglist_cache()
{
    buglist_cache = [];
    var users = document.getElementsByClassName("user");
    for (var i = 0; i < users.length; i++)
    {
        entry = {
            "elem": users[i],
            "user": users[i].querySelector("#user"),
            "email": users[i].querySelector("#email"),
            "branches": []
        };
        var user = entry["user"].textContent;
        var email = entry["email"].title;
        var branches = users[i].getElementsByClassName("branch");
        for (var ii = 0; ii < branches.length; ii++)
        {
            var buglines = branches[ii].getElementsByClassName("bugline");
            var branch =
            {
                "elem": branches[ii],
                "branch": branches[ii].querySelector("#branch")
            };
            var bugs = [];
            for(var iii = 0; iii < buglines.length; iii++)
            {
                bugs.push(
                {
                    "bugline": buglines[iii],
                    "date": buglines[iii].querySelector("#date"),
                    "status": buglines[iii].querySelector("#status"),
                    "cl": buglines[iii].querySelector("#cl"),
                    "bug": buglines[iii].querySelector("#bug"),
                    "sum": buglines[iii].querySelector("#sum")
                });
                var date = bugs[iii]["date"].textContent;
                var status = bugs[iii]["status"].title;
                var cl = bugs[iii]["cl"].title;
                var bug = bugs[iii]["bug"].title;
                var sum = bugs[iii]["sum"].textContent;
                var br = branch["branch"].textContent;
                bugs[iii]["txtValue"] = br + " " + user + " " + email + " " + date + " " + status + " " + cl + " " + bug + " " + sum;
                bugs[iii]["txtValue"] = bugs[iii]["txtValue"].toUpperCase();
            }
            branch["bugs"] = bugs;
            entry["branches"].push(branch);
        }
        buglist_cache.push(entry);
    }
}
populate_buglist_cache();
var filtering = false
function filter_bugs(force) {
    if (input.value) {
        insertUrlParam("filter", input.value);
    }
    else {
        removeUrlParam("filter");
    }
    if (filtering && !force)
    {
        return;
    }
    var filter = input.value;
    var filters = [];
    if(filter)
    {
        filters = filter.split(" OR ");
        filters.forEach((elem, i) => { filters[i] = elem.split(" AND "); });
        filters.forEach((_, i) => {
            filters[i].forEach((elem, ii) => { filters[i][ii] = elem.toUpperCase(); });
        });
    }
    const apply_filter = async () =>
    {
        var users = buglist_cache;
        for(var i = 0; i < users.length; i++)
        {
            var branches = users[i]["branches"];
            var filtered_branches = [];
            for(var ii = 0; ii < branches.length; ii++)
            {
                var bugs = branches[ii]["bugs"];
                var filtered_bugs = [];
                for (var iii = 0; iii < bugs.length; iii++)
                {
                    var all_found = true;
                    if(filters)
                    {
                        var txtValue = bugs[iii]["txtValue"];
                        for(var s1 = 0; s1 < filters.length; s1++)
                        {
                            all_found = true;
                            for(var s2 = 0; s2 < filters[s1].length; s2++)
                            {
                                if (!txtValue.includes(filters[s1][s2]))
                                {
                                    all_found = false;
                                    break;
                                }
                            }
                            if(all_found)
                            {
                                break;
                            }
                        }
                    }
                    if (all_found)
                    {
                        bugs[iii]["bugline"].style.display = "";
                    }
                    else
                    {
                        filtered_bugs.push(bugs[iii]["bugline"]);
                    }
                }
                if (filtered_bugs.length != bugs.length)
                {
                    filtered_bugs.forEach(bug => bug.style.display = "none");
                    branches[ii]["elem"].style.display = "";
                }
                else
                {
                    filtered_branches.push(branches[ii]["elem"]);
                }
            }
            if(filtered_branches.length != branches.length)
            {
                filtered_branches.forEach(branch => branch.style.display = "none");
                users[i]["elem"].style.display = "";
            }
            else
            {
                users[i]["elem"].style.display = "none";
            }
            if(filter != input.value && !force)
            {
                return;
            }
            // Use this in cases where machines are slower.
            // if (!force)
            // {
            //     await sleepms(1);
            // }
        }
    }
    filtering = true;
    while(true)
    {
        apply_filter();
        if(filter == input.value)
        {
            break;
        }
        else
        {
            filter = input.value;
        }
    }
    filtering = false;
}
function removeUrlParam(key) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.delete(key);
        let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
        window.history.pushState({path: newurl}, '', newurl);
    }
}
function insertUrlParam(key, value) {
    if (history.pushState) {
        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(key, value);
        let newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchParams.toString();
        window.history.pushState({path: newurl}, '', newurl);
    }
}
window.onload = function(e)
{
    var date = document.getElementById("start_date");
    var calendar = document.getElementById("gotodate");
    calendar.value = date.textContent;
};
var show_owners = true;
function update_elements()
{
    (async () => {
        var response = await fetch(window.location.pathname + window.location.search + window.location.hash);
        switch (response.status) {
            // status "OK"
            case 200:
                var template = await response.text();
                var doc = new DOMParser().parseFromString(template, "text/html");
                var newelem = doc.getElementById("buglist");
                var oldelem = document.getElementById("buglist");
                oldelem.innerHTML = newelem.innerHTML;
                newelem = doc.getElementById("changeinfo");
                oldelem = document.getElementById("changeinfo");
                oldelem.innerHTML = newelem.innerHTML;
                var date = doc.getElementById("start_date");
                var calendar = document.getElementById("gotodate");
                calendar.value = date.textContent;
                populate_buglist_cache();
                filter_bugs(true);
                if(!show_owners) { only_bugs(); }
                console.log("bugs updated");
                break;
            case 404:
                break;
        }
    })();
}
setInterval(update_elements, 60000);
document.addEventListener("DOMContentLoaded", function(event) { 
        var scrollpos = sessionStorage.getItem('scrollpos');
        if (scrollpos) window.scrollTo(0, scrollpos);
        sessionStorage.removeItem("scrollpos");
    });

    window.onbeforeunload = function(e) {
        sessionStorage.setItem('scrollpos', window.scrollY);
    };
filter_bugs(true);
function only_bugs() {
    var userline = document.getElementsByClassName("user");
    var branch = document.getElementsByClassName("branch");
    var changeinfo = document.getElementById("changeinfo");
    changeinfo.style.display = "none";
    for (var i = 0; i < userline.length; i++) {
        userline[i].querySelector("#user").style.display = "none";
    }
    for (var i = 0; i < branch.length; i++) {
        branch[i].querySelector("#branch").style.display = "none";
    }
    var rules = document.getElementsByTagName("hr");
    for (var i = 0; i < rules.length; i++) {
        rules[i].style.display = "none";
    }
    var uls = document.getElementsByTagName("ul");
    for (var i = 0; i < uls.length; i++) {
        uls[i].style.margin = 0;
    }
}
function gotodate(e)
{
    let searchParams = new URLSearchParams(window.location.search);
    date = e.target.value.split("-")
    url = "stickbug-" + date[2] + date[1] + date[0] + ".html" + '?' + searchParams.toString();
    window.location.href = url;
}
