@ECHO OFF
REM
REM data_mk_tattoos_itd_zip.bat
REM
REM Rick Stirling <rick.stirling@rockstarnorth.com>, modified from a script by
REM David Muir <david.muir@rockstarnorth.com>

CALL setenv.bat

SET INPUT=%RS_PROJROOT%\art\peds\tattoos
SET OUTPUT=%RS_EXPORT%\models\cdimages\ped_tattoo_txds.zip

ECHO Building Texture Dictionaries...
FOR /D %%G IN (%INPUT%\*) DO (
	ECHO Creating %%G.itd.zip from 
	ECHO   %%G\*.dds
	ruby %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.dds --output=%%G.itd.zip %%G
)

ECHO Building Streamed Texture Dictionary RPF...
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.itd.zip --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

