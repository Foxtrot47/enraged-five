@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/test_content_tree.bat
REM Description:: 
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 16 November 2011
REM

CALL setenv.bat
%RS_TOOLSRUBY% test_content_tree.rb
PAUSE

REM Done.
