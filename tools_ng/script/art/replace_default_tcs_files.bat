@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/replace_default_tcs_files.bat
REM Description:: Replace map TCS files that specify Default template.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 23 December 2010
REM

CALL setenv.bat
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/art/replace_default_tcs_files.rb
PAUSE

REM %RS_TOOLSROOT%/script/art/replace_default_tcs_files.bat
