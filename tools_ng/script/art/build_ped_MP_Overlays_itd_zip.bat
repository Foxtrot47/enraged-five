@ECHO OFF
REM
REM data_mk_tattoos_itd_zip.bat
REM
REM Rick Stirling <rick.stirling@rockstarnorth.com>, modified from a script by
REM David Muir <david.muir@rockstarnorth.com>

CALL setenv.bat

SET INPUT=%RS_PROJROOT%\art\peds\MP_Overlays
SET OUTPUT=%RS_EXPORT%\models\cdimages\ped_MP_Overlay_txds.zip
SET METADATA=%RS_ASSETS%\metadata\textures\ped_mp_overlays
SET TEMPLATE=%RS_ASSETS%\metadata\textures\templates\peds\Diffuse
SET CONVERTER=rageTextureConvert.exe
SET CONVERTER_OPTIONS=-format A8R8G8B8



ECHO Converting TGA to DDS
FOR /D %%G IN (%INPUT%\*) DO (


	REM Convert texture to 32-bit ARGB DDS
	REM ECHO Converting Texture: %%G
	REM ECHO Input:  %%G\%%~NG.TGA
	REM ECHO Output: %%G\%%~NG.DDS
	
	%CONVERTER% -in %%G\%%~NG.TGA -out %%G\%%~NG.DDS %CONVERTER_OPTIONS% 
	if exist %%G\%%~NG_N.TGA (%CONVERTER% -in %%G\%%~NG_N.TGA -out %%G\%%~NG_N.DDS %CONVERTER_OPTIONS%)
	if exist %%G\%%~NG_S.TGA (%CONVERTER% -in %%G\%%~NG_S.TGA -out %%G\%%~NG_S.DDS %CONVERTER_OPTIONS%)
)




ECHO Building Texture Dictionaries from DDS Files
FOR /D %%G IN (%INPUT%\*) DO (
	ECHO Creating %%G.itd.zip from 
 	ECHO   %%G\*.DDS
	
	
	rem	ruby %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.DDS --metadata=%RS_PROJROOT%\assets\metadata\textures\%ITD% --template=%RS_PROJROOT%\assets\metadata\textures\templates\peds\FULL_Diffuse.tcp --output=%%G.itd.zip %%G 

	ruby %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.DDS --metadata=%METADATA% --template=%TEMPLATE% --output=%%G.itd.zip %%G 
)





ECHO Building Streamed Texture Dictionary RPF...
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.itd.zip --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

