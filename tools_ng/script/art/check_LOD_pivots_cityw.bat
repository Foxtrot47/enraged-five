@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/check_LOD_object_pivots_from_bbCentre.bat
REM Description::Generates a list of all objects that have their pivots over a threshold % of their largest dimension
REM
REM Author:: Adam Munson <adam.munson@rockstarnorth.com>
REM Date:: 13 July 2011
REM
REM Parameters:		e.g:
REM --project   	gta5
REM --threshold		0.4
REM --level		cptestbed
REM --group		downtown_01
REM --map		dt1_01
REM

CALL setenv.bat
%RS_TOOLSRUBY% %RS_TOOLSLIB%/util/max/check_LOD_object_pivot_distance.rb --project=%RS_PROJECT% --group=_cityw
PAUSE
