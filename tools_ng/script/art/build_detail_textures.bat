call setenv.bat

set ATLAS_FILE=%RS_PROJROOT%\art\Textures\detail\detailAtlas.txt
set ATLAS_OUTPUT_FILE=%RS_BUILDBRANCH%\common\data\detailAtlas.dds

IF NOT EXIST %ATLAS_FILE% ( echo Unable to find %ATLAS_FILE%.  Aborting process...
	goto failure
)

echo Checking out %ATLAS_OUTPUT_FILE%...
x:
cd %RS_PROJROOT%
p4 edit %ATLAS_OUTPUT_FILE% > NUL

%RS_TOOLSBIN%\rageTextureConvert.exe -makearray=%ATLAS_FILE% -out=%ATLAS_OUTPUT_FILE%

:failure
pause