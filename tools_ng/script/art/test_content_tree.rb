#
# File:: %RS_TOOLSROOT%/script/art/test_content_tree.rb
# Description:: Test the content tree loading.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 16 November 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'test/unit'

#-----------------------------------------------------------------------------
# Classes
#-----------------------------------------------------------------------------
class ContentLoadTest < ::Test::Unit::TestCase

    def setup()
        @p = Pipeline::Config::instance().project()
        assert( @p.load_config() )
    end
    
    def test_content_load()
        assert( @p.load_content() )
    end
end

# %RS_TOOLSROOT%/script/art/test_content_tree.rb
