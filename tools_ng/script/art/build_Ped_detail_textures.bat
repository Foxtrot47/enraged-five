call setenv.bat

set PED_ATLAS_FILE=%RS_PROJROOT%\art\peds\Ped_Detail_Maps\ped_detail_Atlas.txt
set PED_ATLAS_OUTPUT_FILE=%RS_PROJROOT%\build\dev\common\data\ped_detail_atlas.dds
set PED_ATLAS_OUTPUT_ARRAY_FILE=%RS_PROJROOT%\Art\peds\Ped_Detail_Maps\ped_detail_atlas_max.dds

IF NOT EXIST %PED_ATLAS_FILE% ( echo Unable to find %PED_ATLAS_FILE%.  Aborting process...
	goto failure
)
echo Checking out %PED_ATLAS_OUTPUT_FILE%...
x:
cd %RS_PROJROOT%
p4 edit %PED_ATLAS_OUTPUT_FILE% > NUL

%RS_TOOLSBIN%\rageTextureConvert.exe -makearray=%PED_ATLAS_FILE% -out=%PED_ATLAS_OUTPUT_FILE% -outarray=%PED_ATLAS_OUTPUT_ARRAY_FILE%

:failure
pause

