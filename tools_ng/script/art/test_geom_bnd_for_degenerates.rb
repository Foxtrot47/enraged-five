#	test_geom_bnd_for_degenerates.rb
#	Author: Gunnar Droege
# 	
#	testing bnd files in same folder for faces with triangles with more than 1 shared component.

Dir.foreach('./') do |filename|
	next if filename == '.' or filename == '..' or filename == File.basename(__FILE__)
	puts "loading #{filename}"

	text=File.open(filename).read
	text.gsub!(/\r\n?/, "\n")
	verts = []
	vertLineNumbers = []
	lineNumber = 1
	
	text.each_line do |line|
		if (/^v / =~ line) != nil then
			vertParts = line.split()
			vertLineNumbers << lineNumber
			verts << [vertParts[1].to_f, vertParts[2].to_f, vertParts[3].to_f]
		end
		if (/^tri / =~ line) != nil then
			formerVerts = []
		
			faceParts = line.split()
			facePartIndex = 1
			faceParts.each do |vertIndex|
				next if vertIndex=="tri" || vertIndex=="0"
				
				vertIndex = vertIndex.to_i
				
				thisVert = verts[vertIndex]
				formerVerts.each do | vertTuple |
					formerVertIndex = vertTuple[0]
					formerVert= vertTuple[1]
					matchingComponents = []
					for i in 0..2
						mycomponent = ((thisVert[i] * 1000).floor)/1000.0
						otheromponent = ((formerVert[i] * 1000).floor)/1000.0
						if mycomponent==otheromponent then
							matchingComponents << otheromponent
#							puts "matching component at #{vertIndex}"
						end
					end
					if matchingComponents.length>1 then
						puts "face in line #{lineNumber} has equal components at micrometer precision: \n\t#{thisVert.join(",")} at line #{vertLineNumbers[vertIndex]} and \n\t#{formerVert.join(",")} at line #{vertLineNumbers[formerVertIndex]}"
					end
				end
				formerVerts << [vertIndex, thisVert]
				facePartIndex += 1
				if facePartIndex>3 then
					break
				end
			end
		end
		lineNumber += 1
	end

end

gets()
