#
# File:: %RS_TOOLSROOT%/script/art/alidate_map_containers.rb
# Description:: Validate all map containers SceneXml..
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 June 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_map'
require 'pipeline/scm/perforce'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--level', '-l', Getopt::REQUIRED, 'process a single level map data (e.g. mdtestbed).' ],
	[ '--group', '-g', Getopt::REQUIRED, 'process a group (folder) of map data (e.g. _citye, downtown_01).' ],
	[ '--map', '-m', Getopt::REQUIRED, 'process a single map (e.g. v_int_1).' ],
]
	
#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		g_ProjectName = ENV['RS_PROJECT'] 
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Project.load_config( )
		g_Level = opts['level'].nil? ? nil : opts['level']
		g_Group = opts['group'].nil? ? nil : opts['group']
		g_Map = opts['map'].nil? ? nil : opts['map']
		
		Dir::chdir( g_Project.export ) do
			g_Perforce = SCM::Perforce.new
			g_Perforce.connect()
			begin
				g_Perforce.run_sync( OS::Path::combine( g_Project.export, '....xml' ) )
			rescue P4Exception => ex
				g_Log.exception( ex, "Perforce exception syncing.  May not have latest SceneXml data." )
			end
		end
		
		# Now for the main processing loop...
		ProjectUtil::data_map_for_each( g_Project, g_Level, g_Group ) do |mapzip|
			
			g_Log.info( "Validating #{mapzip.name}..." )
			ProjectUtil::data_map_validate( mapzip )
		end
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSLIB%/util/data_regenerate_map_metadata.rb
