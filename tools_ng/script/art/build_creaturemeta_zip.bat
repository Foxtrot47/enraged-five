@ECHO OFF
REM
REM build_creaturemeta_zip.bat
REM
REM Rick Stirling <rick.stirling@rockstarnorth.com>


CALL setenv.bat

SET INPUT=%RS_ASSETS%\anim\creaturemetadata
SET OUTPUT=%RS_ASSETS%\export\anim\creaturemetadata.zip


ECHO Building Creature Metadata Zip from .pso.meta files
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.pso.meta --output=%OUTPUT% %INPUT%

ECHO Creating platform data
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

