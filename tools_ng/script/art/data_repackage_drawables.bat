@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/data_repackage_drawables.bat
REM Description:: Repackage drawable dictionaries into drawables for LOD sibling merge.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 24 November 2011
REM

CALL setenv.bat
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/art/data_repackage_drawables.rb
PAUSE

REM %RS_TOOLSROOT%/script/art/data_repackage_drawables.bat
