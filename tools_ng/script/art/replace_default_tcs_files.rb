#
# File:: %RS_TOOLSROOT%/script/art/replace_default_tcs_files.rb
# Description:: Fix up map TCS file that have default template.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 27 May 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/path'
require 'rexml/document'
include Pipeline
include REXML

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
SOURCE_PATH = '$(metadata)/textures/maps_half'

#----------------------------------------------------------------------------
# Globals
#----------------------------------------------------------------------------
$process_count = 0
$process_count_normalmap = 0
$process_count_specularmap = 0
$process_count_diffusemap = 0
$process_errors = 0

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Process a single TCS file.
def process_tcs( log, p4, cl, filename )
	
	begin
		document = nil
		File::open( filename, 'r' ) do |fp|
			document = Document::new( fp )
		end
		
		skipProcessing = false
		document.elements.each( "/rage__CTextureConversionSpecification/skipProcessing" ) do |node|
			skipProcessing = true if ( 0 == "true".casecmp( node.attributes['value'] ) )
		end
			
		if ( not skipProcessing ) then
			document.elements.each( "/rage__CTextureConversionSpecification/parent" ) do |node|
				template = node.text
			
				if ( template.ends_with( 'Default' ) ) then
					log.info( "Processing: #{filename}..." )
					
					# Determine texture type; based on filename.
					basename = OS::Path::get_basename( filename )
					if ( basename.ends_with( '_n' ) or basename.ends_with( '_nrm' ) )
						# NormalMap
						node.text = "${RS_ASSETS}/metadata/textures/templates/maps_half/NormalMap"
						$process_count_normalmap += 1
							
					elsif ( basename.ends_with( '_s' ) or basename.ends_with( '_spec' ) )
						# Specular
						node.text = "${RS_ASSETS}/metadata/textures/templates/maps_half/Specular"
						$process_count_specularmap += 1
						
					else
						# Assume Diffuse
						node.text = "${RS_ASSETS}/metadata/textures/templates/maps_half/Diffuse"
						$process_count_diffusemap += 1
					end
					
					$process_count += 1
							
					p4.run_edit( filename )
					p4.run_reopen( '-c', cl.to_s, filename )
					File::open( filename, 'w' ) do |fp|
						document.write( fp )
					end
				end
			end
		end
	rescue Exception => ex
		
		log.error( "Failed to initialise TCS template: #{filename}." )
		log.exception( ex )
		log.info( "Press any key to continue..." )
		$stdin.gets
	end
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
g_AppName = OS::Path::get_filename( __FILE__ )
g_Log = Log.new( g_AppName )
begin
	g_Config = Pipeline::Config::instance( )
	g_Project = g_Config.projects[ENV['RS_PROJECT']]
	g_Project.load_config()
	
	g_Perforce = g_Project.scm
	g_cl = g_Perforce.create_changelist( "Replacing map TCS files pointing at Default template (automated)." )
	g_Project.in_env do |e|
		source_path = e.subst( SOURCE_PATH )
		g_Perforce.run_sync( source_path )
		
		tcs_files = OS::FindEx::find_files_recurse( 
			OS::Path::combine( source_path, '*.tcs' ) )
		g_Log.info( "Processing #{tcs_files.size} files." )
		puts "SOURCE: #{source_path}"
		tcs_files.each do |tcs_filename|
			
			# next unless ( tcs_filename.ends_with( '_s.tcs' ) ) #or
			#              tcs_filename.ends_with( '_nrm.tcs' ) )
			process_tcs( g_Log, g_Perforce, g_cl, tcs_filename )
		end
	end
	g_Perforce.run_revert( '-a', '-c', g_cl.to_s )
	g_Log.info( "Total files processed:        #{$process_count}." )
	g_Log.info( "Diffuse TCS files processed:  #{$process_count_diffusemap}" )
	g_Log.info( "Normal TCS files processed:   #{$process_count_normalmap}" )
	g_Log.info( "Specular TCS files processed: #{$process_count_specularmap}" )
	
rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex
	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )

ensure
	g_Perforce.disconnect( ) if ( g_Perforce.connected? )
end

# %RS_TOOLSROOT%/script/art/replace_default_tcs_files.rb
