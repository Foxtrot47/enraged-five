@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/validate_map_containers.bat
REM Description:: Pass all SceneXml files through the map validation process.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 30 June 2011
REM

CALL setenv.bat
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/art/validate_map_containers.rb
PAUSE

REM %RS_TOOLSROOT%/script/art/validate_map_containers.bat
