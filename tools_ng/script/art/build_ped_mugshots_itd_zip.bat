@ECHO OFF
REM
REM data_mk_tattoos_itd_zip.bat
REM
REM Rick Stirling <rick.stirling@rockstarnorth.com>, modified from a script by
REM David Muir <david.muir@rockstarnorth.com>

CALL setenv.bat

SET INPUT=%RS_PROJROOT%\art\Textures\PedMugShot
SET OUTPUT=%RS_EXPORT%\models\cdimages\ped_mugshot_txds.zip
SET METADATA=%RS_ASSETS%\metadata\textures\ped_mugshots
SET TEMPLATE=%RS_ASSETS%\metadata\textures\templates\peds\Streamed\Diffuse
SET CONVERTER=rageTextureConvert.exe
SET CONVERTER_OPTIONS=-format A8R8G8B8




ECHO Building Texture Dictionaries from DDS Files
FOR /D %%G IN (%INPUT%\*) DO (
	ECHO Creating %%G.itd.zip from 
 	ECHO   %%G\*.DDS
	
	
	rem	ruby %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.DDS --metadata=%RS_PROJROOT%\assets\metadata\textures\%ITD% --template=%RS_PROJROOT%\assets\metadata\textures\templates\peds\FULL_Diffuse.tcp --output=%%G.itd.zip %%G 

	ruby %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.DDS --metadata=%METADATA% --template=%TEMPLATE% --output=%%G.itd.zip %%G 
)





ECHO Building Streamed Texture Dictionary RPF...
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.itd.zip --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

