#
# File:: %RS_TOOLSROOT%/script/art/find_missing_tcs_files.rb
# Description:: Find missing map TCS files.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 May 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/log/log'
require 'pipeline/os/file'
require 'pipeline/os/path'
require 'rexml/document'
include Pipeline
include REXML

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
SOURCE_PATH = '$(assets)/maps/textures'
TCS_PATH = '$(metadata)/textures/maps_half'

#----------------------------------------------------------------------------
# Globals
#----------------------------------------------------------------------------
$missing_tcs_files = 0

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Process a single DDS file.
def process_dds( log, p4, dds_filename, tcs_path )
	
	dds_basename = OS::Path::get_basename( dds_filename )
	tcs_filename = OS::Path::combine( tcs_path, "#{dds_basename}.tcs" ) 
	
	if ( not File::exists?( tcs_filename ) ) then
		log.error( "Texture #{dds_filename} is missing a TCS (#{tcs_filename})." )
		$missing_tcs_files += 1
	end
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
g_AppName = OS::Path::get_filename( __FILE__ )
g_Log = Log.new( g_AppName )
begin
	g_Config = Pipeline::Config::instance( )
	g_Project = g_Config.projects[ENV['RS_PROJECT']]
	g_Project.load_config()
	
	g_Perforce = g_Project.scm
	g_Project.in_env do |e|
		source_path = e.subst( SOURCE_PATH )
		tcs_path = e.subst( TCS_PATH )
		g_Perforce.run_sync( OS::Path::combine( source_path, '...' ) )
		g_Perforce.run_sync( OS::Path::combine( tcs_path, '...' ) )
		
		dds_files = OS::FindEx::find_files_recurse( 
			OS::Path::combine( source_path, '*.dds' ) )
		g_Log.info( "Processing #{dds_files.size} files." )
		dds_files.each do |dds_filename|
			
			process_dds( g_Log, g_Perforce, dds_filename, tcs_path )
		end
	end
	g_Log.info( "Total TCS files missing: #{$missing_tcs_files}." )
	
rescue SystemExit => ex
	exit( ex.status )

rescue Exception => ex
	g_Log.exception( ex, "Unhandle exception during convert" )
	ex.backtrace.each do |m| g_Log.error( m ); end
	GUI::ExceptionDialog::show_dialog( ex, "#{g_AppName} unhandled exception" )

	# Only require Enter press when an exception has occurred.
	puts "\nPress Enter or close this window to continue..."
	$stdin.gets( )

ensure
	g_Perforce.disconnect( ) if ( g_Perforce.connected? )
end

# %RS_TOOLSROOT%/script/art/find_missing_tcs_files.rb
