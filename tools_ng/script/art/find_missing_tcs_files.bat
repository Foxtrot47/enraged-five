@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/find_missing_tcs_files.bat
REM Description:: Find missing map TCS files.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 30 May 2011
REM

CALL setenv.bat
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/art/find_missing_tcs_files.rb
PAUSE

REM %RS_TOOLSROOT%/script/art/find_missing_tcs_files.bat
