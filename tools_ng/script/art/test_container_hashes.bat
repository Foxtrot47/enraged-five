@ECHO OFF
REM File:: %RS_TOOLSROOT%/script/art/test_container_hashes.bat
REM Description:: 
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 22 February 2012
REM

CALL setenv.bat
%RS_TOOLSRUBY% test_container_hashes.rb
PAUSE

REM Done.
