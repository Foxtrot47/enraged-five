#
# File:: %RS_TOOLSROOT%/script/art/data_repackage_drawables.rb
# Description:: Repackage export drawable dictionary zips into drawables.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 23 November 2011
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_convert'
require 'pipeline/projectutil/data_map'
require 'pipeline/projectutil/data_zip'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--level', '-l', Getopt::REQUIRED, 'process a single level map data (e.g. mdtestbed).' ],
	[ '--group', '-g', Getopt::REQUIRED, 'process a group (folder) of map data (e.g. _citye, downtown_01).' ],
	[ '--map', '-m', Getopt::REQUIRED, 'process a single map (e.g. v_int_1).' ],
]

#----------------------------------------------------------------------------
# Functions
#----------------------------------------------------------------------------

# Repackage the mapzip file.
def repackage_mapzip( log, project, p4, mapzip )
    
    p4.run_sync( mapzip.filename )
    drawable_dictionary_list = ProjectUtil::data_zip_filelist( mapzip.filename ).collect do |filename|
        filename if ( File::fnmatch?( '*.idd.zip', filename ) )
    end
    drawable_dictionary_list.delete( nil )
    
    if ( 0 == drawable_dictionary_list.size )
        log.info( "Skipping map #{mapzip.name} as there are no drawable dictionaries." )
        return
    end
    
    log.info( "  Drawable Dictionaries: #{drawable_dictionary_list.size}" )
    config = Pipeline::Config::instance()
    cache_dir = OS::Path::combine( config.temp(), 'repackage_drawables', mapzip.name )
    files = ProjectUtil::data_zip_extract( mapzip.filename, cache_dir, true ) 
    edit_result = p4.run_edit( mapzip.filename ).shift
    if ( edit_result.is_a?( String ) and edit_result.include?( "can't edit exclusive file already opened" ) )
        log.warn( "Skipping because its exclusively checked out by someone else." )    
        return
    end    
    
    files_to_pack = []
    files.each do |filename|
        if ( File::fnmatch( '*.idd.zip', filename ) ) then
            # We'll unpack this and repack its components.
            drawable_cache_dir = OS::Path::combine( cache_dir, OS::Path::get_basename( filename ) )
            current_drawable = ''
            drawable_files = {} # Hash of new drawables to create. 
            ProjectUtil::data_zip_extract( filename, drawable_cache_dir, true ) do |file|
                if ( 0 != current_drawable.casecmp( OS::Path::get_directory( file ) ) ) then
                    # File in new drawable definition.
                    current_drawable = OS::Path::get_directory( file )
                    drawable_files[current_drawable] = [] \
                        unless ( drawable_files.has_key?( current_drawable ) )
                    drawable_files[current_drawable] << file
                else
                    # File in current drawable definition.
                    drawable_files[current_drawable] << file
                end
            end
            drawable_files.each_pair do |drawable, files|
                drawable_name = OS::Path::get_basename(drawable)
                log.info( "  Building drawable #{drawable_name}..." )
                drawable_filename = OS::Path::combine( cache_dir, "#{drawable_name}.idr.zip" )
                ProjectUtil::data_zip_create( drawable_filename, files, false )
                
                FileUtils::rm( filename ) if ( File::exists?( filename ) )
                files_to_pack << drawable_filename
            end
        else
            # We'll just repack this file as-is.
            files_to_pack << filename
        end
    end
    
    ProjectUtil::data_zip_create( mapzip.filename, files_to_pack )
   ## ProjectUtil::data_convert_file( mapzip.filename )
end

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
    log = Log::new( OS::Path::get_basename( __FILE__ ) )
    project = Pipeline::Config::instance().project
    project.load_config()
    
    opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] ) then
		puts OS::Getopt.usage( OPTIONS )
		exit( 1 )
	end
    level = opts['level'].nil? ? nil : opts['level']
	group = opts['group'].nil? ? nil : opts['group']
	mapname = opts['map'].nil? ? nil : opts['map']
    
    p4 = project.scm()
    begin
        ProjectUtil::data_map_for_each( project, level, group ) do |mapzip|
            map = mapzip.inputs[0]
            next unless ( map.exportdefinitions and map.exportinstances )
            
			if ( not mapname.nil? ) then
				next unless ( mapname == mapzip.name )
			end
        
            log.info( "Processing Map: #{mapzip.name}..." )
            repackage_mapzip( log, project, p4, mapzip ) \
                if ( File::exists?( mapzip.filename ) )
        end
    
    rescue Exception => ex
        log.exception( ex, 'Unhandled exception repacking drawable dictionaries.' )
    end
end

# %RS_TOOLSROOT%/script/art/data_repackage_drawables.rb
