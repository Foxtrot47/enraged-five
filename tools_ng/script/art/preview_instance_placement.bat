REM 
REM This script allows users to preview the instance placement for a particular .zip file.
REM Users can drop a particular .zip file onto this batch file to execute.
REM 

@echo off
call setenv.bat

if "%RS_PROJECT%"=="gta5" (
	%RS_TOOLSROOT%\ironlib\bin\maps\RSG.Pipeline.InstancePlacementProcessor.exe -preview -zip %1 -texture "%RS_ASSETS%\metadata\terrain\$(instance_category)*.tif"
) else (
	%RS_TOOLSROOT%\ironlib\bin\maps\RSG.Pipeline.InstancePlacementProcessor.exe -preview -zip %1 -texture "%RS_ASSETS%\metadata\terrain\$(instance_category)\%~n1.tif"
)

set IMAP_PREVIEW_DIR=%RS_EXPORT%\temp\imap_preview
 
pushd %IMAP_PREVIEW_DIR%
for /r %%i in (*) do (
call :append %%i
)
popd

if not defined IMAP_FILES (
	echo No files generated.  Nothing to preview.
	pause
	goto :eof
)

%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\data_convert_file.rb --preview %IMAP_FILES%

echo Previewing instance placement completed.
pause

:append
if defined IMAP_FILES (
    set IMAP_FILES=%IMAP_FILES% %1
) else (
    set IMAP_FILES=%1
)