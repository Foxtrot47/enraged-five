#
# File:: # %RS_TOOLSROOT%/script/art/test_container_hashes.rb
# Description:: Validate U16 container hashes.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 22 February 2012
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/log/log'
require 'pipeline/projectutil/data_map'
require 'pipeline/util/rage'
include Pipeline

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log.new( g_AppName )
	g_Config = Pipeline::Config::instance()
	begin
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		g_ProjectName = ENV['RS_PROJECT'] 
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Project.load_config( )
	    g_R = RageUtils::new( g_Project )	
        g_Hashes = {}
  
		# Now for the main processing loop...
		g_ExitCode = 0
        ProjectUtil::data_map_for_each( g_Project ) do |mapzip|
			
            name = mapzip.name
            hash = g_R.rage.atHash16U( name )
            
            if ( g_Hashes.has_key?( hash ) ) then
                g_Log.error( "Container #{name} hash collision with #{g_Hashes[hash]} (#{hash})." )
                g_ExitCode = 1
            else
                g_Hashes[hash] = name
            end
		end
        exit( g_ExitCode )
		
	rescue SystemExit => ex
		exit( ex.status )
	rescue Exception => ex
		
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")    
	end
end

# %RS_TOOLSROOT%/script/art/test_container_hashes.rb
