#!/usr/bin/env ruby
Dir.foreach(File.dirname(__FILE__)) do |f|
	puts ("loading #{f}")
	if (f == "." || f == ".." || f == File.basename(__FILE__) || f.index(".txt")!=nil) 
		puts ("skipping...")
		next
	end
	convertExe = ENV['RS_TOOLSROOT'] + "/bin/demesh/mesh_to_txtmesh.exe"
	args = " "+f+" "+f+".txt"
	system(convertExe+args)
end
gets()