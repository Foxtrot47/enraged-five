@ECHO OFF
C:
cd C:\Program Files\Autodesk\3ds Max 2012
echo Running batch metadata export...
3dsmax.exe -mxs "filein \"pipeline/export/peds/pedBatchExport.ms\"; RsPedBatchExport overwriteMeta:false metadataOnly:true"