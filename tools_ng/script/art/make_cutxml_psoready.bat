REM converts attribute lists into cutfAttribute format, if it hasn't already been done

call setenv.bat
for /R %RS_ASSETS%\cuts\ %%i in (*.cutxml) do (
	findstr cutfAttributes %%i > NUL
	if ERRORLEVEL 1 ( REM Really means >= 1
		echo converting: %%i
		%RS_TOOLSBIN%\batchsub.py -p %RS_TOOLSSCRIPT%\art\cutxml_ref_to_value.txt %%i
	) else (
		echo already converted: %%i
	)
)
more 