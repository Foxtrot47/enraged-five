#
# File:: convert_tcl_files.rb
# Description:: Utility script that will take an input directory, recursively search 
# for any .zip files, unpack them and then modify all TCL files to point to a different
# directory or template.  (e.g. remap texture templates to the "maps" directory instead of
# "maps_half."
#
# Author:: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
# Date::27 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/projectutil/data_zip'
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--input", "-i", OS::Getopt::REQUIRED, "Input directory." ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, "display usage information." ]
]
TRAILING_DESC = ''

ZIP_EXTENSION = ".zip"

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

class Helpers
	 
	def Helpers.extract_zip(input_file, basepath)

		input_filename = File.basename(input_file)
		temp_directory = OS::Path.combine(basepath, input_filename)
		
		if File.exists?(temp_directory) == false 
			FileUtils.mkdir_p(temp_directory)
		end
		
		#Extract the zip.

		FileUtils.chmod(0777, input_file)
		zip_files = []
		ProjectUtil::data_zip_extract( input_file, temp_directory, true ) do |filename|
			print "  #{filename}\n" 
			
			if File.extname(filename).casecmp(".zip") == 0 then
				zip_files << filename
			end				
		end
		
		toolstmp = Pipeline::Config::instance().temp()
		zip_files.each do |zip_file|
		
			print "Extracting sub-zip: #{zip_file}...\n"
			renamed_zip = OS::Path.combine(toolstmp, File.basename(zip_file))
			FileUtils.cp(zip_file, renamed_zip)
			File.delete(zip_file)
			
			print "Extracting...\n"
			extract_zip(renamed_zip, temp_directory)
			
			File.delete(renamed_zip)
		end	
	end
end

if ( __FILE__ == $0 ) then

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance( )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] or opts['input'].nil?) then
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 1 )
		end
		
		input_file_path = OS::Path::combine(opts['input'])
		input_files = OS::FindEx::find_files_recurse( input_file_path )
				
		OS::Path::set_downcase_on_normalise(false)
		
		input_files.each do |input_file|
		
			print "Processing #{input_file}\n"

			temp_directory = Pipeline::Config::instance().temp()
			Helpers::extract_zip(input_file, temp_directory)

			root_directory = OS::Path.combine(temp_directory, File.basename(input_file))
			tcl_files = OS::FindEx::find_files_recurse( OS::Path.combine(root_directory, "*.tcl") )
			tcl_files.each do |tcl_file|
			
			
				print "Processing #{tcl_file}...\n"
				lines = File.readlines(tcl_file)
				lines.each do |line|
					line.gsub!("maps_half", "maps")
				end
				
				FileUtils.chmod(0777, tcl_file)
				updated_tcl_file = File.new(tcl_file, "w")
				
				lines.each do |line|
					updated_tcl_file.write(line)
				end
				updated_tcl_file.close
			end
			
			#Package up all the children zips first.
			
			zip_dirs = OS::FindEx.find_dirs( root_directory )
			zip_dirs.each do |zip_dir|
				
				all_files = OS::FindEx::find_files_recurse( OS::Path.combine(zip_dir, "*"))

				temp_zip_path = OS::Path.combine(temp_directory, File.basename(zip_dir))
				ProjectUtil::data_zip_create( temp_zip_path, all_files )
				
				FileUtils.rm_rf(zip_dir)
				
				zip_dir = OS::Path.combine(File.dirname(zip_dir), File.basename(zip_dir))
				FileUtils.cp(temp_zip_path, zip_dir)
				File.delete(temp_zip_path)
			end
		
			root_files = OS::FindEx::find_files_recurse( OS::Path.combine(root_directory, "*") )

			ProjectUtil::data_zip_create( input_file, root_files )
			FileUtils.rm_rf(root_directory)
		end
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	end	
end
