#
# File:: %RS_TOOLSROOT%/script/art/build_textures_rar_outsource.rb
# Description::
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 25 June 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/scm/perforce'
include Pipeline
require 'date'

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project to grab textures for.' ],
	[ '--date', '-d', OS::Getopt::REQUIRED, 'date to copy files from.' ],
	[ '--output', '-o', OS::Getopt::REQUIRED, 'output path.' ],	
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]
INVALID_EXTENSIONS = [ 'psd' ]

#----------------------------------------------------------------------------
# Entry-Point
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	g_AppName = OS::Path::get_basename( __FILE__ )
	g_Log = Pipeline::Log::new( g_AppName )
	
	begin
		g_Config = Pipeline::Config::instance( )
		g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )
		g_ProjectName = g_Opts['project']
		g_When = Date::parse( g_Opts['date'] )
		g_Output = g_Opts['output']
		g_Project = g_Config.projects[g_ProjectName]
		g_Project.load_config( )
		
		g_InputPath = OS::Path::combine( g_Project.art, 'textures' )
		Dir::chdir( g_InputPath ) 
		g_P4 = SCM::Perforce.new( )
		g_P4.connect( )
			
		g_InputPathP4 = OS::Path::combine( g_P4.local2depot( g_InputPath ), '...' )
		g_Log.info( "Project: #{g_Project.uiname}" )
		g_Log.info( "Date:    #{g_When}" )
		g_Log.info( "Copying textures from: " )
		g_Log.info( "\t#{g_InputPath}" )
		g_Log.info( "\t#{g_InputPathP4}" )
		
		g_Log.info( "Syncing #{g_InputPathP4}" )
		g_P4.run_sync_with_block( g_InputPathP4 ) do |client_file, synced, errors|
			g_Log.info( "\t#{client_file}" )	
		end
		
		# Fstat the Perforce texture files to find which have been submitted since
		# the date specified.
		g_Log.info( "P4 Stat #{g_InputPathP4}" )
		g_FstatResult = g_P4.run_fstat( g_InputPathP4 )
		g_FstatResult.each do |fstat|
			fileDate = Date::parse( Time::at( fstat['headTime'].to_i ).to_s )
			next unless ( fileDate >= g_When )
			
			begin
				srcFile = OS::Path::normalise( fstat['clientFile'] )
				ext = OS::Path::get_extension( srcFile )
				next if ( INVALID_EXTENSIONS.include?( ext ) )
				
				relPath = srcFile.sub( OS::Path::combine( g_Project.art, 'textures' ), '' )
				dstFile = OS::Path::combine( g_Output, relPath )
				
				g_Log.info( "File: #{fileDate} #{srcFile}" )
				g_Log.info( "\tDestination: #{dstFile}" )
				FileUtils::mkdir_p( OS::Path::get_directory( dstFile ) )
				FileUtils::cp( srcFile, dstFile )
			rescue Exception => ex
			
				g_Log.exception( ex, 'Unhandled IO exception' )
			end
		end
		
		
	rescue Exception => ex	
		g_Log.exception( ex, 'Unhandled exception' )
	end
end

# %RS_TOOLSROOT%/script/art/build_textures_rar_outsource.rb
