@ECHO OFF
REM
REM File:: %RS_TOOLSROOT%/script/art/build_textures_rar_outsource.bat
REM Description::
REM
REM Author:: Adam Bown <adam.bown@rockstarnorth.com>
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 25 June 2010
REM

CALL setenv.bat

SET RAR_PATH=%ProgramFiles(x86)%\WinRAR\rar.exe
SET RAR_NAME=Textures-%1--
SET DESTINATION=x:\texturesformineloader
SET PASSWORD=P4r4d153

IF NOT EXIST %DESTINATION% MD %DESTINATION%
DEL %DESTINATION% /F /S /Q
RD %DESTINATION% /S /Q

MKDIR %DESTINATION%
%RS_TOOLSRUBY% %CD%\build_textures_rar_outsource.rb --project=%RS_PROJECT% --date=%1 --output=%DESTINATION%

ECHO
ECHO Creating a RAR of all files in %DESTINATION%...

"%RAR_PATH%" a -agMM-DD-YYYY -ep1 -hp%PASSWORD% "%DESTINATION%\%RAR_NAME%" "%DESTINATION%"

ECHO Done.
PAUSE

REM %RS_TOOLSROOT%/script/art/build_textures_rar_outsource.bat
