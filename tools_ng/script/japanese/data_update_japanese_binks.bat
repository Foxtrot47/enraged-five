@ECHO OFF
REM
REM Remove Bink Movies that should not be in the Japanese build.
REM
REM Author: David Muir <david.muir@rockstarnorth.com>
REM Date: 30 May 2013
REM 

CALL setenv.bat >NUL
PUSHD %RS_PROJROOT%

p4 sync //depot/gta5/build/japanese/xbox360/movies/...
p4 sync //depot/gta5/build/japanese/ps3/movies/...

REM Delete unused binks
p4 delete //depot/gta5/build/japanese/xbox360/movies/WZL_PWASSER.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/WZL_PWASSER.bik

p4 delete //depot/gta5/build/japanese/xbox360/movies/WZL_Serious_Cougar.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/WZL_Serious_Cougar.bik

p4 delete //depot/gta5/build/japanese/xbox360/movies/CNT_Rehab_Island.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/CNT_Rehab_Island.bik

p4 delete //depot/gta5/build/japanese/xbox360/movies/CUT_LESTER_2_INT.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/CUT_LESTER_2_INT.bik

p4 delete //depot/gta5/build/japanese/xbox360/movies/CUT_Lester_2.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/CUT_Lester_2.bik

p4 delete //depot/gta5/build/japanese/xbox360/movies/LO_RS_LOOP_2.bik
p4 delete //depot/gta5/build/japanese/ps3/movies/LO_RS_LOOP_2.bik

PAUSE
