@ECHO OFF
REM
REM Update the GTA5 Japanese Character Assets from //depot/gta5/assets/... to //depot/gta5/assets_japanese/...
REM This is careful to revert any changes from characters that are specific to GTA5 Japanese version.
REM
REM Author: David Muir <david.muir@rockstarnorth.com>
REM Date: 30 May 2013
REM 

CALL setenv.bat >NUL
SET BRANCHSPEC=gta5_assets_japanese
SET INTEGARGS=-s

PUSHD %RS_PROJROOT%

REM Integrate Metadata Definitions
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/definitions/... //...


REM Integrate Character Metadata
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/characters/... //...

REM Integrate Character Texture Metadata
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/templates/... //...
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/streamedpeds/...  //...
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/streamedpedprops/...  //...
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/pedprops/...  //...
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/cutspeds/...  //...
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/metadata/textures/componentpeds/... //...

REM Integrate Japanese%INTEGARGS%pecific Characters (and associated characters using prefixes)

REM CUTSCENE
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/levels/gta5/generic/cutspeds/... //...
p4 revert //depot/gta5/assets_japanese/export/levels/gta5/generic/cutspeds/cs_lazlow.ped.zip

REM COMPONENT
REM COMPONENT: SHARED ASSETS
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/cdimages/comp_peds_... //...

REM COMPONENT: A_F_Y
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/a_f_y... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/a_f_y_topless_01.ped.zip

REM COMPONENT: A_M_M
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/a_m_m... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/a_m_m_acult_01.ped.zip

REM COMPONENT: A_M_O
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/a_m_o... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/a_m_o_acult_01.ped.zip

REM COMPONENT: A_M_Y
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/a_m_y... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/a_m_y_acult_01.ped.zip

REM COMPONENT: CSB
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/csb... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/csb_stripper_01.ped.zip
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/csb_stripper_02.ped.zip

REM COMPONENT: S_F_Y
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/componentpeds/s_f_y... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/s_f_y_stripper_01.ped.zip
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/s_f_y_stripper_02.ped.zip
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/componentpeds/s_f_y_stripperlite.ped.zip

REM STREAMED
p4 integ -b %BRANCHSPEC% %INTEGARGS% //depot/gta5/assets/export/models/cdimages/streamedpeds/mp_... //...
p4 revert //depot/gta5/assets_japanese/export/models/cdimages/streamedpeds/mp_f_stripperlite_01.ped.zip

PAUSE
