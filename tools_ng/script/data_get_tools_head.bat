@ECHO OFF
REM
REM File:: data_get_tools_head.bat
REM Description::
REM
REM Author:: Jonny Rivers <jonny.rivers@rockstarnorth.com>
REM Date:: 12 July 2011
REM

CALL setenv.bat
TITLE Syncing latest %RS_PROJECT% tools and running installer...

p4 sync %RS_TOOLSROOT%/...

PAUSE

REM data_get_tools_head.bat
