import sys
from xml.dom.minidom import *

def ParserEnumXml(pXml, pGloDec, pEnumList, pProcList):

    IntArray = []
    FloatArray = []
    BoolArray = []
    StringArray = []
    VectorArray = []
    DateArray = [] 

    dom1 = parse(pXml)

    GlobalsDeclaration = ""
    EnumList = ""
    ProcedureList = ""

    TotalNumberOfStats = len(dom1.getElementsByTagName('stat'))
    NumberOfIntStats = 0
    NumberOfFloatStats = 0
    NumberOfBoolStats = 0
    NumberOfStringStats = 0
    NumberOfVectorStats = 0
    NumberOfDateStats = 0
    
    i = 0

    GlobalsDeclaration = GlobalsDeclaration + "USING \"Commands_Misc.sch\"\n"
    GlobalsDeclaration = GlobalsDeclaration + "USING \"Commands_stats.sch\"\n\n\n"
    ProcedureList = ProcedureList + "USING \"Commands_Misc.sch\"\n"
    ProcedureList = ProcedureList + "USING \"Globals.sch\"\n"
    ProcedureList = ProcedureList + "USING \"ScriptSavesEnumList.sch\"\n\n\n"

    for scriptStat in dom1.childNodes:
        for stats in scriptStat.childNodes:
            for node in stats.childNodes:
                if node.nodeName == "stat":
                    if node.attributes["Type"].value.upper() == "INT":
                        NumberOfIntStats = NumberOfIntStats + 1
                        IntArray.append(node)
                    elif node.attributes["Type"].value.upper() == "FLOAT":
                        NumberOfFloatStats = NumberOfFloatStats + 1
                        FloatArray.append(node)
                    elif node.attributes["Type"].value.upper() == "BOOL":
                        NumberOfBoolStats = NumberOfBoolStats + 1
                        BoolArray.append(node)
                    elif node.attributes["Type"].value.upper() == "STRING":
                        NumberOfStringStats = NumberOfStringStats + 1
                        StringArray.append(node)
                    elif node.attributes["Type"].value.upper() == "VECTOR":
                        NumberOfVectorStats = NumberOfVectorStats + 1
                        VectorArray.append(node)
                    elif node.attributes["Type"].value.upper() == "DATE":
                        NumberOfDateStats = NumberOfDateStats + 1
                        DateArray.append(node)

    
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_INT_SCRIPTSAVE " + str(NumberOfIntStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_FLOAT_SCRIPTSAVE " + str(NumberOfFloatStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_BOOL_SCRIPTSAVE " + str(NumberOfBoolStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_STRING_SCRIPTSAVE " + str(NumberOfStringStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_VECTOR_SCRIPTSAVE " + str(NumberOfVectorStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "CONST_INT MAX_NUM_DATE_SCRIPTSAVE " + str(NumberOfDateStats) + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "\n"
    GlobalsDeclaration = GlobalsDeclaration + "\n"

    GlobalsDeclaration = GlobalsDeclaration + "STRUCT MP_SAVE_DATA_STRUCT\n"
    GlobalsDeclaration = GlobalsDeclaration + "\tINT g_SaveData_INT_ScriptSaves[MAX_NUM_INT_SCRIPTSAVE] \n"
    GlobalsDeclaration = GlobalsDeclaration + "\tFLOAT g_SaveData_FLOAT_ScriptSaves[MAX_NUM_FLOAT_SCRIPTSAVE] \n"
    GlobalsDeclaration = GlobalsDeclaration + "\tBOOL g_SaveData_BOOL_ScriptSaves[MAX_NUM_BOOL_SCRIPTSAVE] \n"
    GlobalsDeclaration = GlobalsDeclaration + "\tTEXT_LABEL_31 g_SaveData_STRING_ScriptSaves[MAX_NUM_STRING_SCRIPTSAVE] \n"
    GlobalsDeclaration = GlobalsDeclaration + "\tVECTOR g_SaveData_VECTOR_ScriptSaves[MAX_NUM_VECTOR_SCRIPTSAVE] \n"
    GlobalsDeclaration = GlobalsDeclaration + "\tSTRUCT_STAT_DATE g_SaveData_DATE_ScriptSaves[MAX_NUM_DATE_SCRIPTSAVE] \n" 
    GlobalsDeclaration = GlobalsDeclaration + "ENDSTRUCT"
    
   
    GlobalsDeclaration = GlobalsDeclaration + "\n"

    
    for ITEM in IntArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"
            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF("
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"
    
        ProcedureList = ProcedureList + '\t\tREGISTER_' + ITEM.attributes["Type"].value.upper() + '_TO_SAVE('
        ProcedureList = ProcedureList + 'g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]"
        ProcedureList = ProcedureList + ', "' + ITEM.attributes["Name"].value.upper() + '")\n'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfIntStats - 1:
            ProcedureList = ProcedureList + '\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"

            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
            #EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0

    for ITEM in IntArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"


        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData."
        ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]" + " = " + ITEM.attributes["Default"].value + "\n"


        if i == NumberOfIntStats - 1:
            ProcedureList = ProcedureList + "ENDPROC\n"
            
        i = i + 1

    i = 0





    for ITEM in FloatArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"
            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF("
            ProcedureList = ProcedureList + 'g_savedMPGlobals.mpSaveData.' + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"
    
        ProcedureList = ProcedureList + '\t\tREGISTER_' + ITEM.attributes["Type"].value.upper() + '_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]"
        ProcedureList = ProcedureList + ', "' + ITEM.attributes["Name"].value.upper() + '")\n'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfFloatStats - 1:
            ProcedureList = ProcedureList + '\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"

            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
        #    EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0

    for ITEM in FloatArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"

        ProcedureList = ProcedureList + "\t"
        ProcedureList = ProcedureList + 'g_savedMPGlobals.mpSaveData.g_SaveData_' + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]" + " = " + ITEM.attributes["Default"].value + "\n"


        if i == NumberOfFloatStats - 1:
            ProcedureList = ProcedureList + "ENDPROC\n"
            
        i = i + 1

    i = 0

    


    for ITEM in BoolArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"
            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF("
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"
    
        ProcedureList = ProcedureList + '\t\tREGISTER_' + ITEM.attributes["Type"].value.upper() + '_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]"
        ProcedureList = ProcedureList + ', "' + ITEM.attributes["Name"].value.upper() + '")\n'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfBoolStats - 1:
            ProcedureList = ProcedureList + '\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"

            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
        #    EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0

    for ITEM in BoolArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"

        ProcedureList = ProcedureList + "\t"
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]"
        ProcedureList = ProcedureList + " = "
        if ITEM.attributes["Default"].value == "0":
            ProcedureList = ProcedureList + "FALSE\n"
        elif ITEM.attributes["Default"].value == "1":
            ProcedureList = ProcedureList + "TRUE\n"
        else:    
            ProcedureList = ProcedureList + ITEM.attributes["Default"].value + "\n"


        if i == NumberOfBoolStats - 1:
            ProcedureList = ProcedureList + "ENDPROC\n"
            
        i = i + 1

    i = 0




    for ITEM in StringArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"
            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF("
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"
    
        ProcedureList = ProcedureList + '\t\tREGISTER_TEXT_LABEL_31_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT(" + ITEM.attributes["Name"].value.upper() + ")]"
        ProcedureList = ProcedureList + ', "' + ITEM.attributes["Name"].value.upper() + '")\n'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfStringStats - 1:
            ProcedureList = ProcedureList + '\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"

            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
        #    EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0

    for ITEM in StringArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"

        ProcedureList = ProcedureList + "\t"
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")]" + " = " + "\"" + ITEM.attributes["Default"].value + "\"" + "\n"


        if i == NumberOfStringStats - 1:
            ProcedureList = ProcedureList + "ENDPROC\n"
            
        i = i + 1

    i = 0



    for ITEM in VectorArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"

            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF(g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"

            
        ProcedureList = ProcedureList + '\t\tSTART_SAVE_STRUCT_WITH_SIZE('
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")] , "
        ProcedureList = ProcedureList + "SIZE_OF("
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")]) , "
        ProcedureList = ProcedureList + "\"" 
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_FLOAT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].x , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".x" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_FLOAT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].y , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".y" + "\"" + ")"


        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_FLOAT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].z , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".z" + "\"" + ")"
        
            
        ProcedureList = ProcedureList + '\n\t\tSTOP_SAVE_STRUCT()'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfVectorStats - 1:
            ProcedureList = ProcedureList + '\n\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"

            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
        #    EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0

    for ITEM in VectorArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[" + "ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")]" + " = " + "<<" + ITEM.attributes["Default"].value + ">>" + "\n"


        if i == NumberOfVectorStats - 1:
            ProcedureList = ProcedureList + "ENDPROC\n"
            
        i = i + 1

    i = 0





    for ITEM in DateArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC INIT_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"
            EnumList = EnumList + "\n"
            EnumList = EnumList + "ENUM SCRIPTSAVE_" + ITEM.attributes["Type"].value.upper() + "\n"

            ProcedureList = ProcedureList + '\tSTART_SAVE_ARRAY_WITH_SIZE('
            ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData."
            ProcedureList = ProcedureList + "g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves, SIZE_OF(g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves), "
            ProcedureList = ProcedureList + "\"g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves\")\n"

            
        ProcedureList = ProcedureList + '\t\tSTART_SAVE_STRUCT_WITH_SIZE('
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")] , "
        ProcedureList = ProcedureList + "SIZE_OF("
        ProcedureList = ProcedureList + "g_savedMPGlobals.mpSaveData.g_SaveData_" + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")]) , "
        ProcedureList = ProcedureList + "\"" 
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].year , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".year" + "\"" + ")"
        
        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].month , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".month" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].day , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".day" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].hour , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".hour" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].minute , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".minute" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].seconds , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".seconds" + "\"" + ")"

        ProcedureList = ProcedureList + "\n" + '\t\t\tREGISTER_INT_TO_SAVE(g_savedMPGlobals.mpSaveData.g_SaveData_'
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].Milliseconds , "
        ProcedureList = ProcedureList + "\"" + ITEM.attributes["Name"].value.upper() + ".Milliseconds" + "\"" + ")"
        
            
        ProcedureList = ProcedureList + '\n\t\tSTOP_SAVE_STRUCT()'
        try:
            ProcedureList = ProcedureList + "// " + node.attributes["Comment"].value
        except:
            None
        
        EnumList = EnumList + "\t" + ITEM.attributes["Name"].value.upper() + ",\n"

        
        if i == NumberOfDateStats - 1:
            ProcedureList = ProcedureList + '\n\tSTOP_SAVE_ARRAY()\n'
            ProcedureList = ProcedureList + "ENDPROC\n"
            
            EnumList = EnumList + "\n\n\t" + "MAX_LIMIT_" + ITEM.attributes["Type"].value.upper()
            EnumList = EnumList + "\n" + "ENDENUM\n"
        #else:
        #    EnumList = EnumList + ",\n"
            
        i = i + 1

    i = 0


    for ITEM in DateArray:
        if i == 0:
            ProcedureList = ProcedureList + "\n"
            ProcedureList = ProcedureList + "PROC RESET_MP_SCRIPT_SAVES_" + ITEM.attributes["Type"].value.upper() + "()\n"

        ProcedureList = ProcedureList + "\n"
        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].year = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].month = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].day = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].hour = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].minute = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].seconds = 0 \n"

        ProcedureList = ProcedureList + "\tg_savedMPGlobals.mpSaveData.g_SaveData_"
        ProcedureList = ProcedureList + ITEM.attributes["Type"].value.upper() + "_ScriptSaves[ENUM_TO_INT("
        ProcedureList = ProcedureList + ITEM.attributes["Name"].value.upper() + ")].Milliseconds = 0 \n"

       
        if i == NumberOfDateStats - 1:
            ProcedureList = ProcedureList + "\nENDPROC\n"
            
        i = i + 1

    i = 0
                        

    ProcedureList = ProcedureList + "\n"
    ProcedureList = ProcedureList + "PROC INIT_ALL_MP_SCRIPT_SAVES()" + "\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_INT()\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_FLOAT()\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_BOOL()\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_STRING()\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_VECTOR()\n"
    ProcedureList = ProcedureList + "\tINIT_MP_SCRIPT_SAVES_DATE()\n"
    ProcedureList = ProcedureList + "ENDPROC\n"

    ProcedureList = ProcedureList + "\n"
    ProcedureList = ProcedureList + "PROC RESET_ALL_MP_SCRIPT_SAVES()" + "\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_INT()\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_FLOAT()\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_BOOL()\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_STRING()\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_VECTOR()\n"
    ProcedureList = ProcedureList + "\tRESET_MP_SCRIPT_SAVES_DATE()\n"
    ProcedureList = ProcedureList + "ENDPROC\n"  
    
                

    GlodDeclarationFile = open(pGloDec, "w")
    GlodDeclarationFile.write(GlobalsDeclaration)
    GlodDeclarationFile.close()

    EnumListFile = open(pEnumList, "w")
    EnumListFile.write(EnumList)
    EnumListFile.close()

    ProcedureListFile = open(pProcList, "w")
    ProcedureListFile.write(ProcedureList)
    ProcedureListFile.close()


def XmlToEnum():

    print len(sys.argv)
    ParserEnumXml(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])

XmlToEnum()


