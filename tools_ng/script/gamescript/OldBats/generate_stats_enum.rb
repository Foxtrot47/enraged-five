# Greg Smith
# Rockstar North
# 7/6/2010

# Generates stats enum pch file from source xml files

require 'pipeline/config/projects'
require 'pipeline/util/rage'
require 'xml'

config = Pipeline::Config.instance

p = config.project
p.load_config

source_xml_files = [ "#{p.common}/data/spStatsSetup.xml", "#{p.common}/data/mpstatssetup.xml" ]
target_pch_file = "#{p.root}/script/dev/core/game/data/stats_enums.sch"
target_sch_file_containing_init_function = "#{p.root}/script/dev/multiplayer/include/public/net_stat_generator.sch"

class Numeric
    def ones_complement(bits)
        self ^ ((1 << bits) - 1)
    end
end

class ScriptStat
	attr_reader :name, :Type, :Default, :ses, :OmitTimestamp, :UserData, :Owner, :Comment, :characterStat, :Online
	attr_writer :name, :Type, :Default, :ses, :OmitTimestamp, :UserData, :Owner, :Comment, :characterStat, :Online
end

class ScriptEnumGenerator

	def initialize()
	
		@stats = Array.new()
		@gen_int_stats = Array.new()
		@gen_float_stats = Array.new()
		@gen_bool_stats = Array.new()
		@gen_userid_stats = Array.new()
		@gen_string_stats = Array.new()
		@gen_label_stats = Array.new()
		@gen_date_stats = Array.new()
		@gen_vector_stats = Array.new()
		@player_int_stats = Array.new()
		@player_float_stats = Array.new()
		@player_bool_stats = Array.new()
		@player_userid_stats = Array.new()
		@player_string_stats = Array.new()
		@player_label_stats = Array.new()
		@player_date_stats = Array.new()
		@player_vector_stats = Array.new()
		@award_player_int_stats = Array.new()
		@award_player_float_stats = Array.new()
		@award_player_bool_stats = Array.new()
		@award_player_userid_stats = Array.new()
		@award_player_date_stats = Array.new()
		@award_general_int_stats = Array.new()
		@award_general_float_stats = Array.new()
		@award_general_bool_stats = Array.new()
		@award_general_userid_stats = Array.new()
		@award_general_date_stats = Array.new()
		@award_character_int_stats = Array.new()
		@award_character_float_stats = Array.new()
		@award_character_bool_stats = Array.new()
		@award_character_userid_stats = Array.new()
		@award_character_date_stats = Array.new()
		@int_stats = Array.new()
		@float_stats = Array.new()
		@bool_stats = Array.new()
		@userid_stats = Array.new()
		@string_stats = Array.new()
		@label_stats = Array.new()
		@date_stats = Array.new()
		@vector_stats = Array.new()
		@r = RageUtils::new( Pipeline::Config.instance.project )
	end

	def add_file( filename )
		
		doc = XML::Document.file(filename)			
		doc.root.find('stats/stat').each { |stat_node|
		
			ss = ScriptStat.new()
			ss.name = stat_node.attributes['Name']
			if ( ss.name.length > 32 ) then
				$stderr.puts "A stat name is over 32 characters long: #{ss.name}"
				Process.exit!(false)
			end

			ss.Type = stat_node.attributes['Type']
			if ( ss.Type != 'int' and ss.Type != 's64' and  ss.Type != 'float' and  ss.Type != 'bool' and ss.Type != 'string' and ss.Type != 'label' and  ss.Type != 'u8' and  ss.Type != 'u16' and ss.Type != 'u32' and  ss.Type != 'u64' and  ss.Type != 'packed' and  ss.Type != 'date' and  ss.Type != 'pos' and  ss.Type != 'userid' and  ss.Type != 'profilesetting') then
				$stderr.puts "A stat name has invalid type: #{ss.name} : #{ss.Type}"
				Process.exit!(false)
			end

			ss.Default = stat_node.attributes['Default']
			if ( ss.Default == '' ) then
				$stderr.puts "A stat name has invalid Default value: #{ss.name} : #{ss.Default}"
				Process.exit!(false)
			end

			ss.ses = stat_node.attributes['ses']
			if ( ss.ses == '' ) then
				$stderr.puts "A stat name has invalid ses value: #{ss.name} : #{ss.ses}"
				Process.exit!(false)
			end

			ss.OmitTimestamp = stat_node.attributes['OmitTimestamp']
			if ( ss.OmitTimestamp == '' ) then
				$stderr.puts "A stat name has invalid OmitTimestamp value: #{ss.name} : #{ss.OmitTimestamp}"
				Process.exit!(false)
			end

			ss.UserData = stat_node.attributes['UserData']
			if ( ss.UserData == '' ) then
				$stderr.puts "A stat name has invalid UserData value: #{ss.name} : #{ss.UserData}"
				Process.exit!(false)
			end

			ss.Owner = stat_node.attributes['Owner']
			if ( ss.Owner == '' ) then
				$stderr.puts "A stat name has invalid Owner value: #{ss.name} : #{ss.Owner}"
				Process.exit!(false)
			end

			ss.Comment = stat_node.attributes['Comment']
			if ( ss.Comment == '' ) then
			 	$stderr.puts "A stat name has invalid Comment value: #{ss.name} : #{ss.Comment}"
			 	Process.exit!(false)
			end

			if ( ss.Type == 'profilesetting' ) then 
			
			elsif ss.name.start_with?("MPGEN_")
				if	ss.name.start_with?("MPGEN_AWD_")
					name_without_prefix = ss.name[10, ss.name.length-1]
					case ss.Type
						when 'int', 's64', 'u8', 'u16', 'u32', 'u64', 'packed'
							@award_general_int_stats << ss
						when 'float'
							@award_general_float_stats << ss
						when 'bool'
							@award_general_bool_stats << ss
						when 'date'
							@award_general_date_stats << ss
						when 'userid'
							@award_general_userid_stats << ss
						else
							$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
							Process.exit!(false)
					end
				else
					name_without_prefix = ss.name[6, ss.name.length-1]
					case ss.Type
						when 'int', 's64', 'u8', 'u16', 'u32', 'u64', 'packed'
							@gen_int_stats << ss
						when 'float'
							@gen_float_stats << ss
						when 'bool'
							@gen_bool_stats << ss
						when 'string'
							@gen_string_stats << ss
						when 'label'
							@gen_label_stats << ss
						when 'date'
							@gen_date_stats << ss
						when 'pos'
							@gen_vector_stats << ss
						when 'userid'
							@gen_userid_stats << ss
						else
							$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
							Process.exit!(false)
					end
				end
			
			
			elsif	ss.name.start_with?("MPPLY_")
				if	ss.name.start_with?("MPPLY_AWD_")	
					name_without_prefix = ss.name[10, ss.name.length-1]
					case ss.Type
						when 'int', 's64', 'u8', 'u16', 'u32', 'u64' , 'packed'
							@award_player_int_stats << ss
						when 'float'
							@award_player_float_stats << ss
						when 'bool'
							@award_player_bool_stats << ss
						when 'date'
							@award_player_date_stats << ss
						when 'userid'
							@award_player_userid_stats << ss
						else
							$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
							Process.exit!(false)
					end
				else
					name_without_prefix = ss.name[6, ss.name.length-1]
					case ss.Type
						when 'int', 's64', 'u8', 'u16', 'u32', 'u64' , 'packed'
							@player_int_stats << ss
						when 'float'
							@player_float_stats << ss
						when 'bool'
							@player_bool_stats << ss
						when 'string'
							@player_string_stats << ss
						when 'label'
							@player_label_stats << ss
						when 'date'
							@player_date_stats << ss
						when 'pos'
							@player_vector_stats << ss
						when 'userid'
							@player_userid_stats << ss
						else
							$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
							Process.exit!(false)
					end

				end
			else
			
				ss.characterStat = stat_node.attributes['characterStat']
				if ( ss.characterStat == 'true' ) then
					ss.Online = stat_node.attributes['online']
					if ( ss.Online == 'true' ) then
					
						ss0 = ScriptStat.new()
						ss0.name = "MP0_#{ss.name}"
						ss0.Type = stat_node.attributes['Type']
						ss0.Default = stat_node.attributes['Default']
						ss0.ses = stat_node.attributes['ses']
						ss0.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss0.UserData = stat_node.attributes['UserData']
						ss0.Owner = stat_node.attributes['Owner']
						ss0.Comment = stat_node.attributes['Comment']
						@stats << ss0

						ss1 = ScriptStat.new()
						ss1.name = "MP1_#{ss.name}"
						ss1.Type = stat_node.attributes['Type']
						ss1.Default = stat_node.attributes['Default']
						ss1.ses = stat_node.attributes['ses']
						ss1.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss1.UserData = stat_node.attributes['UserData']
						ss1.Owner = stat_node.attributes['Owner']
						ss1.Comment = stat_node.attributes['Comment']
						@stats << ss1

						ss2 = ScriptStat.new()
						ss2.name = "MP2_#{ss.name}"
						ss2.Type = stat_node.attributes['Type']
						ss2.Default = stat_node.attributes['Default']
						ss2.ses = stat_node.attributes['ses']
						ss2.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss2.UserData = stat_node.attributes['UserData']
						ss2.Owner = stat_node.attributes['Owner']
						ss2.Comment = stat_node.attributes['Comment']
						@stats << ss2

						ss3 = ScriptStat.new()
						ss3.name = "MP3_#{ss.name}"
						ss3.Type = stat_node.attributes['Type']
						ss3.Default = stat_node.attributes['Default']
						ss3.ses = stat_node.attributes['ses']
						ss3.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss3.UserData = stat_node.attributes['UserData']
						ss3.Owner = stat_node.attributes['Owner']
						ss3.Comment = stat_node.attributes['Comment']
						@stats << ss3

						ss4 = ScriptStat.new()
						ss4.name = "MP4_#{ss.name}"
						ss4.Type = stat_node.attributes['Type']
						ss4.Default = stat_node.attributes['Default']
						ss4.ses = stat_node.attributes['ses']
						ss4.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss4.UserData = stat_node.attributes['UserData']
						ss4.Owner = stat_node.attributes['Owner']
						ss4.Comment = stat_node.attributes['Comment']
						@stats << ss4
						
					else
						ss0 = ScriptStat.new()
						ss0.name = "SP0_#{ss.name}"
						ss0.Type = stat_node.attributes['Type']
						ss0.Default = stat_node.attributes['Default']
						ss0.ses = stat_node.attributes['ses']
						ss0.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss0.UserData = stat_node.attributes['UserData']
						ss0.Owner = stat_node.attributes['Owner']
						ss0.Comment = stat_node.attributes['Comment']
						@stats << ss0

						ss1 = ScriptStat.new()
						ss1.name = "SP1_#{ss.name}"
						ss1.Type = stat_node.attributes['Type']
						ss1.Default = stat_node.attributes['Default']
						ss1.ses = stat_node.attributes['ses']
						ss1.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss1.UserData = stat_node.attributes['UserData']
						ss1.Owner = stat_node.attributes['Owner']
						ss1.Comment = stat_node.attributes['Comment']
						@stats << ss1

						ss2 = ScriptStat.new()
						ss2.name = "SP2_#{ss.name}"
						ss2.Type = stat_node.attributes['Type']
						ss2.Default = stat_node.attributes['Default']
						ss2.ses = stat_node.attributes['ses']
						ss2.OmitTimestamp = stat_node.attributes['OmitTimestamp']
						ss2.UserData = stat_node.attributes['UserData']
						ss2.Owner = stat_node.attributes['Owner']
						ss2.Comment = stat_node.attributes['Comment']
						@stats << ss2
					end
				else
					@stats << ss
				end
				
			end

			ss.characterStat = stat_node.attributes['characterStat']
			if ( ss.characterStat == 'true' ) then				
				ss.Online = stat_node.attributes['online']
				if ( ss.Online == 'true' ) then
					if	ss.name.start_with?("AWD_")		
						name_without_prefix = ss.name[4, ss.name.length-1]
						case ss.Type
							when 'int', 's64', 'u8', 'u16', 'u32', 'u64', 'packed'
								@award_character_int_stats << name_without_prefix
							when 'float'
								@award_character_float_stats << name_without_prefix
							when 'bool'
								@award_character_bool_stats << name_without_prefix
							when 'userid'
								@award_character_userid_stats << name_without_prefix								
							else
								$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
								Process.exit!(false)
						end
					else
						name_without_prefix = ss.name
						case ss.Type
							when 'int', 's64', 'u8', 'u16', 'u32', 'u64', 'packed'
								@int_stats << name_without_prefix
							when 'float'
								@float_stats << name_without_prefix
							when 'bool'
								@bool_stats << name_without_prefix
							when 'string'
								@string_stats << name_without_prefix
							when 'label'
								@label_stats << name_without_prefix
							when 'date'
								@date_stats << name_without_prefix
							when 'pos'
								@vector_stats << name_without_prefix
							when 'userid'
								@userid_stats << name_without_prefix
							else
								$stderr.puts "A stat name has unhandled type: #{ss.name} : #{ss.Type}"
								Process.exit!(false)
						end	
					end
				end				
			end
			
		}
	end
	
	
	def write_enum_of_type(output_file, uppercase_string, array_of_stats)
		output_file.write("ENUM MP_#{uppercase_string}_STATS\n")
		if array_of_stats.size > 0
				array_of_stats.each { |stat_name| output_file.write("\tMP_STAT_#{stat_name},\n") }
				output_file.write("\tMAX_NUM_MP_#{uppercase_string}_STATS\n")
		else
			
			output_file.write("\tMAX_NUM_MP_#{uppercase_string}_STATS = 1\n")
		end
		output_file.write("ENDENUM\n\n")
	end
	
	def write_enum_of_type_award(output_file, uppercase_string, array_of_stats)
		output_file.write("ENUM MP_#{uppercase_string}_AWARD\n")
		if array_of_stats.size > 0
				array_of_stats.each { |stat_name| output_file.write("\tMP_AWARD_#{stat_name},\n") }
				output_file.write("\tMAX_NUM_MP_#{uppercase_string}_AWARD\n")
		else
			
			output_file.write("\tMAX_NUM_MP_#{uppercase_string}_AWARD = 1\n")
		end
		output_file.write("ENDENUM\n\n")
	end
	
	
#	def write_array_of_type(output_file, string_with_uppercase_initial, array_of_stats)
#		uppercase_string = string_with_uppercase_initial.upcase
#		output_file.write("STATSENUM MP#{string_with_uppercase_initial}StatNamesArray[MAX_MP_#{uppercase_string}_STATS][MAX_NUM_CHARACTER_SLOTS]\n")
#	end

	def write_array_stats_initialisation_char(output_file, string_with_uppercase_initial, array_of_stats)
		if array_of_stats.size > 0
			array_of_stats.each do |stat_name|
				0.upto(4) do |player_index|
					output_file.write("\tMP#{string_with_uppercase_initial}StatNamesArray[MP_STAT_#{stat_name}][#{player_index}] = MP#{player_index}_#{stat_name}\n")
				end
			end
			output_file.write("\n\n")
		end
	end
	
	def write_array_award_initialisation_char(output_file, string_with_uppercase_initial, array_of_stats)
		if array_of_stats.size > 0
			array_of_stats.each do |stat_name|
				0.upto(4) do |player_index|
					output_file.write("\tMP#{string_with_uppercase_initial}StatNamesArray[MP_AWARD_#{stat_name}][#{player_index}] = MP#{player_index}_AWD_#{stat_name}\n")
				end
			end
			output_file.write("\n\n")
		end
	end
	
	def write_array_award_initialisation(output_file, string_with_uppercase_initial, array_of_stats)
		if array_of_stats.size > 0
			array_of_stats.each do |ss|
				output_file.write("\tMPAwd#{string_with_uppercase_initial}StatNamesArray[#{ss.name}_INDEX] = #{ss.name}\n")
			end
			output_file.write("\n\n")
		end
	end
	
	def write_array_stat_initialisation(output_file, string_with_uppercase_initial, array_of_stats)
		if array_of_stats.size > 0
			array_of_stats.each do |ss|
				output_file.write("\tMP#{string_with_uppercase_initial}StatNamesArray[#{ss.name}_INDEX] = #{ss.name}\n")
			end
			output_file.write("\n\n")
		end
	end
	
	def write_enum_with_hash( output_file, arrayofenums, uppercase_string, uppercase_string_title )
		output_file.write("ENUM #{uppercase_string_title}\n")
		if arrayofenums.size > 0
			arrayofenums.each { |ss|

			
                # Force result of atStringHash to be signed.
				# Script enums are signed.
				h = @r.rage.atStringHash(ss.name)
				if ( h > 0x7FFFFFFF ) then
					h = -(h - 1).ones_complement(32)
					
				end
			
				
				output_file.write("\t#{ss.name} = #{h}")	
				output_file.write(",") if ss != arrayofenums.last
				output_file.write(" //#{ss.Comment}") if (ss.Comment != nil) and (ss.Comment != "")
				output_file.write("\n")
			}
		else
			
			output_file.write("\tMP#{uppercase_string}_DUMMY_STAT = 1\n")	
		
		end
		output_file.write("ENDENUM\n\n\n")
	
	end
	
	def write_enum_awards( output_file, arrayofenums, uppercase_string, uppercase_string_title )
		output_file.write("ENUM #{uppercase_string_title}\n")
		if arrayofenums.size > 0
			arrayofenums.each { |ss|

				output_file.write("\t#{ss.name}_INDEX")
				
				output_file.write(",") if ss != arrayofenums.last
				output_file.write(" //#{ss.Comment}") if (ss.Comment != nil) and (ss.Comment != "")
				output_file.write("\n")
			}
		else
			
			output_file.write("\tMP#{uppercase_string}_DUMMY_STAT = 1\n")	
		
		end
		output_file.write("ENDENUM\n\n\n")
	
	end
	
	def write_enum_stats( output_file, arrayofenums, uppercase_string, uppercase_string_title )
		output_file.write("ENUM #{uppercase_string_title}\n")
		if arrayofenums.size > 0
			arrayofenums.each { |ss|

				output_file.write("\t#{ss.name}_INDEX")
				
				output_file.write(",") if ss != arrayofenums.last
				output_file.write(" //#{ss.Comment}") if (ss.Comment != nil) and (ss.Comment != "")
				output_file.write("\n")
			}
		else
			
			output_file.write("\tMP#{uppercase_string}_DUMMY_STAT = 1\n")	
		
		end
		output_file.write("ENDENUM\n\n\n")
	
	end
	
	
	def write_awards_consts(output_file, arrayofenums, uppercase_string)		
		output_file.write("CONST_INT MAX_NUM_#{uppercase_string}_AWARD #{arrayofenums.size} \n")	
	end
	
	def write_stat_consts(output_file, arrayofenums, uppercase_string)		
		output_file.write("CONST_INT MAX_NUM_#{uppercase_string}_STATS #{arrayofenums.size} \n")	
	end
	
	def write( filename )
		
		File.open(filename, "w+") { |f|
		
			#f.write("ENUM STATSENUM\n")
			
			write_enum_with_hash(f, @stats , "EMPTY", "STATSENUM")
			
			f.write("\n\n\n //////////////////MP STATS - DO NOT EDIT DIRECTLY, THESE ARE AUTOGENERATED, SPEAK TO BRENDA//////////////////////////////\n\n\n")
			#Stats
				
				#f.write("ENUM MP_GEN_INT_STATS\n")
				write_enum_with_hash(f, @gen_int_stats, "GEN_INT", "MPGEN_INT_STATS" )
				write_enum_with_hash(f, @gen_float_stats, "GEN_FLOAT" , "MPGEN_FLOAT_STATS")
				write_enum_with_hash(f, @gen_bool_stats, "GEN_BOOL", "MPGEN_BOOL_STATS" )
				write_enum_with_hash(f, @gen_string_stats, "GEN_STRING", "MPGEN_STRING_STATS" )
				write_enum_with_hash(f, @gen_label_stats, "GEN_LABEL", "MPGEN_LABEL_STATS" )
				write_enum_with_hash(f, @gen_date_stats, "GEN_DATE", "MPGEN_DATE_STATS" )
				write_enum_with_hash(f, @gen_vector_stats, "GEN_VECTOR", "MPGEN_VECTOR_STATS" )			
				write_enum_with_hash(f, @gen_userid_stats, "GEN_USERID", "MPGEN_USERID_STATS" )			

				#f.write("ENUM MP_PLAYER_STATS\n")
				write_enum_with_hash(f, @player_int_stats, "PLY_INT", "MPPLY_INT_STATS" )
				write_enum_with_hash(f, @player_float_stats, "PLY_FLOAT", "MPPLY_FLOAT_STATS" )
				write_enum_with_hash(f, @player_bool_stats, "PLY_BOOL", "MPPLY_BOOL_STATS" )
				write_enum_with_hash(f, @player_string_stats, "PLY_STRING", "MPPLY_STRING_STATS" )
				write_enum_with_hash(f, @player_label_stats, "PLY_LABEL", "MPPLY_LABEL_STATS" )
				write_enum_with_hash(f, @player_date_stats, "PLY_DATE" , "MPPLY_DATE_STATS")
				write_enum_with_hash(f, @player_vector_stats, "PLY_VECTOR", "MPPLY_VECTOR_STATS" )	
				write_enum_with_hash(f, @player_userid_stats, "PLY_USERID", "MPPLY_USERID_STATS" )
			
			write_enum_of_type(f, "INT", @int_stats)
			write_enum_of_type(f, "FLOAT", @float_stats)
			write_enum_of_type(f, "BOOL", @bool_stats)
			write_enum_of_type(f, "STRING", @string_stats)
			write_enum_of_type(f, "LABEL", @label_stats)
			write_enum_of_type(f, "DATE", @date_stats)
			write_enum_of_type(f, "VECTOR", @vector_stats)
			write_enum_of_type(f, "USERID", @userid_stats)

			write_enum_stats(f,  @player_int_stats, "PLY_INT_STAT_INDEX", "MPPLY_INT_STAT_INDEX")
			write_enum_stats(f,  @player_float_stats, "PLY_FLOAT_STAT_INDEX", "MPPLY_FLOAT_STAT_INDEX")
			write_enum_stats(f,  @player_bool_stats, "PLY_BOOL_STAT_INDEX", "MPPLY_BOOL_STAT_INDEX")
			write_enum_stats(f,  @player_string_stats, "PLY_STRING_STAT_INDEX", "MPPLY_STRING_STAT_INDEX")
			write_enum_stats(f,  @player_label_stats, "PLY_LABEL_STAT_INDEX", "MPPLY_LABEL_STAT_INDEX")
			write_enum_stats(f,  @player_date_stats, "PLY_DATE_STAT_INDEX", "MPPLY_DATE_STAT_INDEX")
			write_enum_stats(f,  @player_vector_stats, "PLY_VECTOR_STAT_INDEX", "MPPLY_VECTOR_STAT_INDEX")
			
			#write_enum_stats(f,  @gen_int_stats, "GEN_INT_STAT_INDEX", "MPGEN_INT_STAT_INDEX")
			#write_enum_stats(f,  @gen_float_stats, "GEN_FLOAT_STAT_INDEX", "MPGEN_FLOAT_STAT_INDEX")
			#write_enum_stats(f,  @gen_bool_stats, "GEN_BOOL_STAT_INDEX", "MPGEN_BOOL_STAT_INDEX")
			#write_enum_stats(f,  @gen_string_stats, "GEN_STRING_STAT_INDEX", "MPGEN_STRING_STAT_INDEX")
			#write_enum_stats(f,  @gen_label_stats, "GEN_LABEL_STAT_INDEX", "MPGEN_LABEL_STAT_INDEX")
			#write_enum_stats(f,  @gen_date_stats, "GEN_DATE_STAT_INDEX", "MPGEN_DATE_STAT_INDEX")
			#write_enum_stats(f,  @gen_vector_stats, "GEN_VECTOR_STAT_INDEX", "MPGEN_VECTOR_STAT_INDEX")
			
			
			write_stat_consts(f, @player_int_stats, "MPPLY_INT")
			write_stat_consts(f, @player_float_stats, "MPPLY_FLOAT")
			write_stat_consts(f, @player_bool_stats, "MPPLY_BOOL")
			write_stat_consts(f, @player_string_stats, "MPPLY_STRING")
			write_stat_consts(f, @player_label_stats, "MPPLY_LABEL")
			write_stat_consts(f, @player_date_stats, "MPPLY_DATE")
			write_stat_consts(f, @player_vector_stats, "MPPLY_VECTOR")
			write_stat_consts(f, @player_userid_stats, "MPPLY_USERID")
			
			write_stat_consts(f, @gen_int_stats, "MPGEN_INT")
			write_stat_consts(f, @gen_float_stats, "MPGEN_FLOAT")
			write_stat_consts(f, @gen_bool_stats, "MPGEN_BOOL")
			write_stat_consts(f, @gen_string_stats, "MPGEN_STRING")
			write_stat_consts(f, @gen_label_stats, "MPGEN_LABEL")
			write_stat_consts(f, @gen_date_stats, "MPGEN_DATE")
			write_stat_consts(f, @gen_vector_stats, "MPGEN_VECTOR")
			write_stat_consts(f, @gen_userid_stats, "MPGEN_USERID")
			
			f.write("\n\n\n //////////////////MP AWARDS - DO NOT EDIT DIRECTLY, THESE ARE AUTOGENERATED, SPEAK TO BRENDA//////////////////////////////\n\n\n")
			#Awards
			
			write_enum_with_hash(f, @award_player_int_stats, "PLY_INT_AWARD", "MPPLY_INT_AWARD" )
			write_enum_with_hash(f, @award_player_float_stats, "PLY_FLOAT_AWARD", "MPPLY_FLOAT_AWARD" )
			write_enum_with_hash(f, @award_player_bool_stats, "PLY_BOOL_AWARD", "MPPLY_BOOL_AWARD" )
			write_enum_with_hash(f, @award_player_date_stats, "PLY_DATE_AWARD", "MPPLY_DATE_AWARD" )
			write_enum_with_hash(f, @award_player_userid_stats, "PLY_USERID_AWARD", "MPPLY_USERID_AWARD" )
				
			write_enum_with_hash(f, @award_general_int_stats, "GEN_INT_AWARD", "MPGEN_INT_AWARD" )
			write_enum_with_hash(f, @award_general_float_stats, "GEN_FLOAT_AWARD" , "MPGEN_FLOAT_AWARD")
			write_enum_with_hash(f, @award_general_bool_stats, "GEN_BOOL_AWARD", "MPGEN_BOOL_AWARD" )	
			write_enum_with_hash(f, @award_general_date_stats, "GEN_DATE_AWARD", "MPGEN_DATE_AWARD" )	
			write_enum_with_hash(f, @award_general_userid_stats, "GEN_USERID_AWARD", "MPGEN_USERID_AWARD" )	
				
				write_enum_of_type_award(f, "INT" , @award_character_int_stats)
				write_enum_of_type_award(f, "FLOAT" , @award_character_float_stats  )
				write_enum_of_type_award(f, "BOOL", @award_character_bool_stats )	
				write_enum_of_type_award(f, "DATE", @award_character_date_stats )	
				write_enum_of_type_award(f, "USERID", @award_character_userid_stats )	

				write_enum_awards(f,  @award_player_int_stats, "PLY_INT_AWARD_INDEX", "MPPLY_INT_AWARD_INDEX")
				write_enum_awards(f,  @award_player_float_stats, "PLY_FLOAT_AWARD_INDEX", "MPPLY_FLOAT_AWARD_INDEX")
				write_enum_awards(f,  @award_player_bool_stats, "PLY_BOOL_AWARD_INDEX", "MPPLY_BOOL_AWARD_INDEX")
				write_enum_awards(f,  @award_player_date_stats, "PLY_DATE_AWARD_INDEX", "MPPLY_DATE_AWARD_INDEX")
				write_enum_awards(f,  @award_general_int_stats, "GEN_INT_AWARD_INDEX", "MPGEN_INT_AWARD_INDEX")
				write_enum_awards(f,  @award_general_float_stats, "GEN_FLOAT_AWARD_INDEX", "MPGEN_FLOAT_AWARD_INDEX")
				write_enum_awards(f,  @award_general_bool_stats, "GEN_BOOL_AWARD_INDEX", "MPGEN_BOOL_AWARD_INDEX")
				write_enum_awards(f,  @award_general_date_stats, "GEN_DATE_AWARD_INDEX", "MPGEN_DATE_AWARD_INDEX")
			
			
			write_awards_consts(f, @award_player_int_stats, "MPPLY_INT")
			write_awards_consts(f, @award_player_float_stats, "MPPLY_FLOAT")
			write_awards_consts(f, @award_player_bool_stats, "MPPLY_BOOL")
			write_awards_consts(f, @award_player_date_stats, "MPPLY_DATE")
			write_awards_consts(f, @award_player_userid_stats, "MPPLY_USERID")
			
			write_awards_consts(f, @award_general_int_stats, "MPGEN_INT")
			write_awards_consts(f, @award_general_float_stats, "MPGEN_FLOAT")
			write_awards_consts(f, @award_general_bool_stats, "MPGEN_BOOL")
			write_awards_consts(f, @award_general_date_stats, "MPGEN_DATE")
			write_awards_consts(f, @award_general_userid_stats, "MPGEN_USERID")
		
			#write_awards_consts(f, @award_character_int_stats, "MP_INT")
			#write_awards_consts(f, @award_character_float_stats, "MP_FLOAT")
			#write_awards_consts(f, @award_character_bool_stats, "MP_BOOL")
			
#			f.write("\nCONST_INT MAX_NUM_CHARACTER_SLOTS 5\n\n")

#			write_array_of_type(f, "Int", @int_stats)
#			write_array_of_type(f, "Float", @float_stats)
#			write_array_of_type(f, "Bool", @bool_stats)
#			write_array_of_type(f, "String", @string_stats)
#			write_array_of_type(f, "Date", @date_stats)
#			write_array_of_type(f, "Vector", @vector_stats)
		}
	
	end
	
	def write_init_function(filename)
	
		File.open(filename, "w+") { |f|
			f.write("\n\nPROC INIT_STATS()\n")
			#Write to the Net Stat Generator
			write_array_stats_initialisation_char(f, "Int", @int_stats)
			write_array_stats_initialisation_char(f, "Float", @float_stats)
			write_array_stats_initialisation_char(f, "Bool", @bool_stats)
			write_array_stats_initialisation_char(f, "String", @string_stats)
			write_array_stats_initialisation_char(f, "Label", @label_stats)
			write_array_stats_initialisation_char(f, "Date",@date_stats)
			write_array_stats_initialisation_char(f, "Vector", @vector_stats)
			write_array_stats_initialisation_char(f, "UserId", @userid_stats)

			#write_array_stat_initialisation(f, "PlyInt", @player_int_stats)
			#write_array_stat_initialisation(f, "PlyFloat", @player_float_stats)
			#write_array_stat_initialisation(f, "PlyBool", @player_bool_stats)
			#write_array_stat_initialisation(f, "PlyString", @player_string_stats)
			#write_array_stat_initialisation(f, "PlyLabel", @player_label_stats)
			#write_array_stat_initialisation(f, "PlyDate", @player_date_stats)
			#write_array_stat_initialisation(f, "PlyVector", @player_vector_stats)
			
			#write_array_stat_initialisation(f, "GenInt", @gen_int_stats)
			#write_array_stat_initialisation(f, "GenFloat", @gen_float_stats)
			#write_array_stat_initialisation(f, "GenBool", @gen_bool_stats)
			#write_array_stat_initialisation(f, "GenString", @gen_string_stats)
			#write_array_stat_initialisation(f, "GenLabel", @gen_label_stats)
			#write_array_stat_initialisation(f, "GenDate", @gen_date_stats)
			#write_array_stat_initialisation(f, "GenVector", @gen_vector_stats)
			
			write_array_award_initialisation_char(f, "AwdInt", @award_character_int_stats)
			write_array_award_initialisation_char(f, "AwdFloat", @award_character_float_stats)
			write_array_award_initialisation_char(f, "AwdBool",  @award_character_bool_stats)
			write_array_award_initialisation_char(f, "AwdDate",  @award_character_date_stats)
			write_array_award_initialisation_char(f, "AwdUserId",  @award_character_userid_stats)
			
			write_array_award_initialisation(f, "PlyInt", @award_player_int_stats)
			write_array_award_initialisation(f, "PlyFloat", @award_player_float_stats)
			write_array_award_initialisation(f, "PlyBool", @award_player_bool_stats)
			write_array_award_initialisation(f, "PlyDate", @award_player_date_stats)
			
			#write_array_award_initialisation(f, "GenInt", @award_general_int_stats)
			#write_array_award_initialisation(f, "GenFloat", @award_general_float_stats)
			#write_array_award_initialisation(f, "GenBool", @award_general_bool_stats)
			#write_array_award_initialisation(f, "GenDate", @award_general_date_stats)

			f.write("ENDPROC\n\n")
		}
	
	end
end

sg = ScriptEnumGenerator.new()

source_xml_files.each { |filename| 

	sg.add_file( filename )
}

sg.write(target_pch_file)

sg.write_init_function(target_sch_file_containing_init_function)