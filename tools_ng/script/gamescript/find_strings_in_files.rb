require 'find'

search_folder = "X:/gta5/script/dev"
input_file_name = "list_of_structs.txt"

$output_file_name = "list_of_structs_with_filenames.txt"
$natives_file_name = "structs_in_native_files.txt"
no_occurrences_output_file_name = "no_occurrences.txt"
multiple_occurrences_output_file_name = "multiple_occurrences.txt"


class InformationAboutOneStruct

	def initialize()
		@array_of_containing_filenames = Array.new()
		@array_of_full_lines = Array.new()
	end
	
	def add_full_line(full_line)
		@array_of_full_lines << full_line
	end

	def add_filename(filename)
		@array_of_containing_filenames << filename
	end
	
	def get_number_of_occurrences
		@array_of_containing_filenames.size
	end
	
	def get_filename(index)
		@array_of_containing_filenames[index]
	end
	
	def get_number_of_full_lines
		@array_of_full_lines.size
	end
	
	def get_full_line(index)
		@array_of_full_lines[index]
	end
end

# Create a hash of structs - the value is a class containing 
#	an array of filenames containing the declaration 
#	and an array of full lines read from the input file
hash_of_structs = {}

File.open(input_file_name, "r") { |f|
	f.each { |line|
		if line =~ /\bSTRUCT\b[ \t]+\b([\S]+)\b/i
			uppercase_struct_name = $1.upcase
			
			if hash_of_structs.include?(uppercase_struct_name) == false
				hash_of_structs[uppercase_struct_name] = InformationAboutOneStruct.new()
			end
			
			hash_of_structs[uppercase_struct_name].add_full_line(line)
		end
	}
}


# Get an array of files to search
files_to_search = Array.new()
 
Find.find(search_folder) do |f|
	files_to_search << f if f.match(/\.sch{0,1}\Z/)
end

#puts "Files to search\n"
#files_to_search.each { |filename|
#	puts filename
#}


# Search all files for all structs
files_to_search.each { |name_of_file_to_search|
	puts ("Checking #{name_of_file_to_search}\n")
		
	File.open(name_of_file_to_search, "r") { |f|
		text = f.read
		matching_structs = text.scan(/\bSTRUCT\b[ \t]+\b([\S]+)\b/i)

		matching_structs.each { |matching_struct|
			uppercase_struct_name = matching_struct[0].upcase
			
			if hash_of_structs.include?(uppercase_struct_name)
				hash_of_structs[uppercase_struct_name].add_filename(name_of_file_to_search)
			end
		}
	}
}

# Construct a hash which maps the filename to an array of struct names for the structs that are only declared in one file
$hash_of_files_to_structs = {}

hash_of_structs.each_value { |info|
	if info.get_number_of_occurrences == 1
		file_name_containing_struct = info.get_filename(0)
		
		if $hash_of_files_to_structs.include?(file_name_containing_struct) == false
			$hash_of_files_to_structs[file_name_containing_struct] = Array.new()
		end
		
		info.get_number_of_full_lines.times { |index|
			$hash_of_files_to_structs[file_name_containing_struct] << info.get_full_line(index)
		}
	end
}

$hash_of_files_to_structs.sort

def write_structs_that_appear_in_one_file(bWriteNativeStructs)
	if bWriteNativeStructs
		out_file_name = $natives_file_name
	else
		out_file_name = $output_file_name
	end

	File.open(out_file_name, "w") { |output_file|
		$hash_of_files_to_structs.each { |key, value_array|
		
			bWriteToFile = false
			if key.downcase.start_with?("x:/gta5/script/dev/shared/include/native/")
				if bWriteNativeStructs
					bWriteToFile = true
				end
			else
				if !bWriteNativeStructs
					bWriteToFile = true
				end
			end

			if bWriteToFile
				output_file.write("\n\n#{key}\n")
				value_array.each { |struct_name|
					output_file.write("#{struct_name}")
				}
			end
		}
	}
end

# Write all files that contain the only declaration of a struct
write_structs_that_appear_in_one_file(true)
write_structs_that_appear_in_one_file(false)

# Write the names of the structs whose declaration couldn't be found
File.open(no_occurrences_output_file_name, "w") { |no_occur_file|
	hash_of_structs.each_value { |info|
		info.get_number_of_full_lines.times { |index|
			no_occur_file.write("#{info.get_full_line(index)}") if info.get_number_of_occurrences == 0
		}
	}
}

# Write the names of the structs which are declared in more than one script file
File.open(multiple_occurrences_output_file_name, "w") { |multiple_occur_file|
	hash_of_structs.each { |key, info|
			if info.get_number_of_occurrences > 1
				multiple_occur_file.write("\n\n#{key}\n")
				info.get_number_of_full_lines.times { |index|
					multiple_occur_file.write("#{info.get_full_line(index)}")
				}
				info.get_number_of_occurrences.times { |file_index|
					multiple_occur_file.write("#{info.get_filename(file_index)}\n")
				}
			end
	}
}
