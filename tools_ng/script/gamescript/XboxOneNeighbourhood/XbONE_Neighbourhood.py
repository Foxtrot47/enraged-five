import sys
import os
import subprocess
import re
from xml.dom.minidom import *

try:
    from Tkinter import *
except ImportError:
    raise ImportError,"The Tkinter module is required to run this program."

	
class Application(Frame):
	# A GUI Appliation #
	
	
	
	def __init__(self, master):
		Frame.__init__(self, master)
		self.grid()
		
		self.ComputerSelected = IntVar()
		
		self.IP1 = StringVar()
		self.IP2 = StringVar()
		self.IP3 = StringVar()
		
		self.create_widgets()
		self.readRFS()
		
	def create_widgets(self):
		#create first button
		
		
		
		self.instruction = Label(self, text = "Enter IP address")
		self.instruction.grid(row = 0, column = 0, sticky = W)
		
		self.IPAddress1 = Entry(self)
		self.IPAddress1.grid(row = 1, column = 0, sticky = W)
		
		self.submit_buttonIP1 = Button(self, text = "Submit IP 1", command = self.revealIP1)
		self.submit_buttonIP1.grid(row = 1, column = 1, sticky = W)
					
		
		self.IPAddress2 = Entry(self)
		self.IPAddress2.grid(row = 2, column = 0, sticky = W)
		
		self.submit_buttonIP2 = Button(self, text = "Submit IP 2", command = self.revealIP2)
		self.submit_buttonIP2.grid(row = 2, column = 1, sticky = W)
		
		
				
		self.IPAddress3 = Entry(self)
		self.IPAddress3.grid(row =3, column = 0, sticky = W)
		
		self.submit_buttonIP3 = Button(self, text = "Submit IP 3", command = self.revealIP3)
		self.submit_buttonIP3.grid(row = 3, column = 1, sticky = W)
		
		
		
		self.label  = Label(self, text = "Button Bank")
		self.label.grid(row = 0, column = 7, sticky = W)
		
		self.button1 = Button(self, text = "Reboot Xbox")
		self.button1["text"] = "Reboot Xbox"
		self.button1["command"] = self.do_xbox_reboot
		self.button1.grid(row =1, column = 8, sticky = W)
		
		#create second button
		self.button2 = Button(self)
		self.button2.grid(row = 2, column =8, sticky = W)
		self.button2.configure(text = "Kill Rag & SysTray")
		self.button2["command"] = self.do_kill_rag
		
		
		self.button3 = Button(self)
		self.button3.grid(row = 3, column =8, sticky = W)
		self.button3.configure(text = "Provision Console")
		self.button3["command"] = self.do_provision
		
		self.button4 = Button(self)
		self.button4.grid(row = 4, column =8, sticky = W)
		self.button4.configure(text = "Kill Game")
		self.button4["command"] = self.do_kill_on_console
		
		self.button5 = Button(self)
		self.button5.grid(row = 5, column =8, sticky = W)
		self.button5.configure(text = "Set Sandbox")
		self.button5["command"] = self.Set_Sandbox_Id
		
		self.button6 = Button(self)
		self.button6.grid(row = 6, column =8, sticky = W)
		self.button6.configure(text = "Sanitize")
		self.button6["command"] = self.do_Sanitize
		
		
		
		
	def readIPConfig(self):
		p = subprocess.Popen('ipconfig /all', shell = True, stdout=subprocess.PIPE) 
		#p = subprocess.Popen(['cmd', '/C', 'ipconfig /all'], shell = True, stdout=subprocess.PIPE) 
		p.wait() 
		rawtxt = p.stdout.read() 
		#print rawtxt 

		p = re.findall(r'[0-9]+(?:\.[0-9]+){3}',rawtxt)
		pshort = p[:1]
		
		str1 = "PC IPConfig = "
		str1 = str1.join(pshort)
		str2 = "PC IPConfig = "
		strCombo = str2+str1
		
		self.label  = Label(self, text = strCombo)
		self.label.grid(row = 11, column = 0, columnspan=2, sticky = W)
		
		#print pshort
		
		
	def readRFS(self):
		if os.path.isfile('X:\\gta5\\titleupdate\\dev_ng\\xbo_loose\\rfs.dat'):
			RFSIP = open('X:\\gta5\\titleupdate\\dev_ng\\xbo_loose\\rfs.dat', 'r')
			str = RFSIP.read()
		
			self.label  = Label(self, text = "PC IP stored in rfs.dat = " + str)
			self.label.grid(row = 10, column = 0, columnspan=2, sticky = W)
		
			

		#self.button2 = Button(self)
		#self.button2.grid(row = 10, column =0, sticky = W)
		#self.button2.configure(text = "Check My IP")
		#self.button2["command"] = self.do_kill_rag

			RFSIP.close()
			
		else:
			self.label  = Label(self, text = "Xbox ONE Version is missing " )
			self.label.grid(row = 10, column = 0, columnspan=2, sticky = W)
		
		self.readIPConfig()
		#print str
		
	def updateComputerSelected(self):
		"""update the current default XBOX"""
		selection = self.ComputerSelected.get()
		
		if selection == 1:
			
			subprocess.Popen([r"C:\Program Files (x86)\Microsoft Durango XDK\bin\xbconnect.exe", self.IP1])
		if selection == 2:
			subprocess.Popen([r"C:\Program Files (x86)\Microsoft Durango XDK\bin\xbconnect.exe", self.IP2])
		if selection == 3:
			subprocess.Popen([r"C:\Program Files (x86)\Microsoft Durango XDK\bin\xbconnect.exe", self.IP3])
	
		
	def revealIP1(self):
		""" Display Message based on the password typed in"""
		self.IP1 = self.IPAddress1.get()
		
		Radiobutton(self,
						text = "Default Xbox IP 1",
						variable = self.ComputerSelected,
						value = 1,
						command = self.updateComputerSelected
						).grid(row = 1, column = 2, sticky = W)
		
		
		#self.textDisplayIP1.delete(0.0, END)
		#self.textDisplayIP1.insert(0.0, "IP 1 = " + IP1 + "\n")
		
		
	def revealIP2(self):
		""" Display Message based on the password typed in"""
		self.IP2 = self.IPAddress2.get()
		
		Radiobutton(self,
					text = "Default Xbox IP 2",
					variable = self.ComputerSelected,
					value = 2,
					command = self.updateComputerSelected
					).grid(row = 2, column = 2, sticky = W)
		
		#self.textDisplayIP2.delete(0.0, END)
		#self.textDisplayIP2.insert(0.0, "IP 2 = " + IP2 + "\n")

	def revealIP3(self):
		""" Display Message based on the password typed in"""
		self.IP3 = self.IPAddress3.get()
		
		Radiobutton(self,
					text = "Default Xbox IP 3",
					variable = self.ComputerSelected,
					value = 3,
					command = self.updateComputerSelected
					).grid(row = 3, column = 2, sticky = W)	
		
		#self.textDisplayIP3.delete(0.0, END)
		#self.textDisplayIP3.insert(0.0, "IP 3 = " + IP3 + "\n")	
		
		
	def do_Run_IPCONFIG(self):
		os.system(r"x:\gta5\build\dev_ng\xbo_scripts\reboot_console.bat")
		
	def do_xbox_reboot(self):
		os.system(r"x:\gta5\build\dev_ng\xbo_scripts\reboot_console.bat")
		
		
		
	def do_kill_rag(self):
		os.system(r"X:\gta5\tools_ng\script\gamescript\XboxOneNeighbourhood\KillRagSysTry.bat")
		
	def do_provision(self):
		os.system(r"X:\gta5\build\dev_ng\xbo_scripts\provision_console.bat")
		
	def do_kill_on_console(self):
		os.system(r"X:\gta5\build\dev_ng\xbo_scripts\kill_on_console.bat")
	
	def Set_Sandbox_Id(self):
		os.system(r"X:\gta5\build\dev_ng\xbo_scripts\SetSandboxIdToRKTR.1.bat")
	
	def do_Sanitize(self):
		os.system(r"X:\gta5\build\dev_ng\xbo_scripts\sanitize_console.bat")
	
def RunNeighbourHoodGUI():
	
	#os.system(r"x:\gta5\build\dev_ng\xbo_scripts\reboot_console.bat")
	
	root = Tk()
	root.focus_force()
	root.title("Very Basic Xbox ONE Neighbourhood")
	root.geometry("600x250")
	os.system("title Very Basic Xbox ONE Neighbourhood")
	
	#sys.stdout.write("\x1b]2;test\x07")
	
	app = Application(root)
	
	root.mainloop()
	
	

def RunNeighbourhood():

   
    RunNeighbourHoodGUI()

RunNeighbourhood()


