@ECHO OFF
REM File:: %RS_TOOLSROOT%\script\test\test_incredibuild_status.bat
REM Description:: 
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 27 November 2012
REM

CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\test\test_incredibuild_status.rb

PAUSE
