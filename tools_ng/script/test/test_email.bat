@ECHO OFF
REM File:: %RS_TOOLSROOT%\script\test\test_email.bat
REM Description:: 
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM Date:: 29 April 2013
REM

SET RECIPIENTS="derek@rockstarnorth.com,derek@rockstarnorth.com"
SET SUBJECT="test subject"
SET BODY="this is a body"

CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\email.rb --recipients %RECIPIENTS% --subject %SUBJECT%  --body %BODY% "BODY EXTRA 1" "THE END" 

PAUSE
