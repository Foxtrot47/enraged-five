@ECHO OFF
REM File:: %RS_TOOLSROOT%\script\test\test_studio_ipaddress.bat
REM Description:: 
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 23 November 2012
REM

CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\test\test_ipaddress_extensions.rb

PAUSE
