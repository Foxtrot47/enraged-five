@ECHO OFF
REM
REM File:: %RS_TOOLSROOT%/script/misc/data_mk_create_startup_itd.bat
REM Description:: Create texture dictionary of report textures. Dupe of David Muirs' original file %RS_TOOLSROOT%/script/misc/data_mk_create_introscrn_itd.bat
REM
REM Author:: Gareth Evans <gareth.evans@rockstarnorth.com>
REM Date:: 18 June 2012
REM

CALL setenv.bat >NUL
SET OUTPUT=%RS_EXPORT%\textures\startup.itd.zip
SET INPUT=%RS_ASSETS%\textures\reports
SET METADATA=%RS_ASSETS%\metadata\textures\miscs\reports
SET TEMPLATE=%RS_ASSETS%\metadata\textures\templates\reports\Default
SET CONVERTER=rageTextureConvert.exe
SET CONVERTER_OPTIONS=-format A8R8G8B8

PUSHD %INPUT%
p4 sync %OUTPUT%
p4 edit %OUTPUT%

FOR /F %%F IN ('DIR /B %INPUT%\*.jpg') DO (

	REM Convert texture to 32-bit ARGB DDS
	ECHO Converting Texture: %%F

	ECHO Input:  %%F
	ECHO Output: %%~nF.dds

	%CONVERTER% -in %%F -out %%~nF.dds %CONVERTER_OPTIONS%
)

P4 edit %RS_ASSETS%\metadata\textures\miscs\reports\*.tcs
P4 add %RS_ASSETS%\metadata\textures\miscs\reports\*.tcs

%RS_TOOLSRUBY% %RS_TOOLSLIB%/util/data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --output=%OUTPUT% --metadata=%METADATA% --template=%TEMPLATE% *.dds *.tcl
%RS_TOOLSCONVERT% %OUTPUT%

POPD
PAUSE
REM %RS_TOOLSROOT%/script/misc/data_mk_create_startup_itd.bat