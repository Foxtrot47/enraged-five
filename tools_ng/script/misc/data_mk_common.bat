set CONVERT=%RS_TOOLSROOT%\bin\ruby\bin\ruby.exe %RS_TOOLSROOT%\lib\util\data_convert_file.rb

cd/d x:\gta5\assets\export

title Converting textures . . .
%CONVERT% --recursive textures

if "%1"=="--full" (
	title Converting anim . . .
	%CONVERT% --recursive anim 
) else (
	pushd gta5
	title Converting anim\cutsprops . . .
	%CONVERT% levels\gta5\generic\cutsobjects.zip
	popd
)

title Converting models . . .
%CONVERT% --recursive models

pushd data

title Converting data\cdimages . . .
%CONVERT% --recursive cdimages

title Converting data\effects . . .
%CONVERT% --recursive effects

popd

pushd levels

pushd gta5

title Converting levels\gta5\generic . . .
%CONVERT% --recursive generic
title Converting levels\gta5\props . . .
%CONVERT% --recursive props
title Converting levels\gta5\cloudhats . . .
%CONVERT% --recursive cloudhats
title Converting levels\gta5\destruction . . .
%CONVERT% --recursive destruction
title Converting levels\gta5\vehiclemods . . .
%CONVERT% --recursive vehiclemods
title Converting levels\gta5\vehicles . . .
%CONVERT% --recursive vehicles

pushd interiors
title Converting levels\gta5\interiors\int_props . . .
%CONVERT% --recursive int_props
popd

popd

title Converting levels\generic . . .
%CONVERT% --recursive generic
popd
