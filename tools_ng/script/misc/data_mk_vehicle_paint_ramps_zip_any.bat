rem @ECHO OFF
REM
REM data_mk_vehicle_paint_ramps_zip_any.bat
REM Create the vehicle paint ramps ZIP.
REM
REM Albert Elwin <albert.elwin@rockstardundee.com>
REM

CALL %1

SET OUTPUT_DIR="%2"
SET INPUT=%RS_ASSETS%\textures\vehicles\paint_ramps
SET ITD_ZIP=%OUTPUT_DIR%\vehicle_paint_ramps.itd.zip
SET OUTPUT_ZIP=%OUTPUT_DIR%\vehicle_paint_ramps.zip

set OUTPUT_ZIP_ADDED=
if exist %OUTPUT_ZIP% set OUTPUT_ZIP_ADDED=1

p4 sync %INPUT%/...
if "%OUTPUT_ZIP_ADDED%"=="1" p4 sync %OUTPUT_ZIP%
if "%OUTPUT_ZIP_ADDED%"=="1" p4 edit %OUTPUT_ZIP%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --filter=*.* --metadata=%RS_ASSETS%\metadata\textures\vehicles\paint_ramps --template=%RS_ASSETS%\metadata\textures\templates\vehicles\PaintRamp --output=%ITD_ZIP% %INPUT%
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --output=%OUTPUT_ZIP% %ITD_ZIP%
REM %RS_TOOLSCONVERT% %OUTPUT_ZIP%

if "%OUTPUT_ZIP_ADDED%"=="" p4 add %OUTPUT_ZIP%
del %ITD_ZIP%

PAUSE

REM data_mk_vehicle_paint_ramps_zip_any.bat
