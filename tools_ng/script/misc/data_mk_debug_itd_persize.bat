@ECHO OFF
REM
REM data_mk_debug_itd_persize.bat
REM Create the debPersize ITD and platform files.
REM
REM Alex Hadjadj <alex.hadjadj@rockstarnorth.com>
REM

CALL setenv.bat
SET ITD=debPersize

SET INPUT=%RS_PROJROOT%\art\Debug/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_rpf.rb --project=%RS_PROJECT% --branch=dev --filter=*.* --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_rmptfx_fxdecal_itd.bat
