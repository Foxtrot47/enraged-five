REM @ECHO OFF
REM
REM File:: data_mk_scaleform_rpf_NG_TU.bat
REM Description::
REM
REM Author:: Gareth Evans <gareth.evans@rockstarnorth.com>
REM Date::
REM
CALL setenv.bat

SET GFX_ROOT=%RS_PROJROOT%"\UI\NG_TU\gfx"
SET ZIP_DIR=%1

REM IF NOT EXIST %GFX_ROOT% GOTO NODELETE
REM RD /S /Q %GFX_ROOT%
REM GOTO NODELETE

title %ZIP_DIR%

:NODELETE
REM For single ZIP/RPF builds:
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_2_rpf_NG_TU_LIVE.rb %ZIP_DIR%

REM For all ZIP/RPFs:
REM %RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_2_rpf_NG_TU.rb %*

PAUSE
REM data_mk_scaleform_rpf_NG_TU.bat
