@ECHO OFF
REM
REM data_mk_debug_itd_density.bat
REM Create the debDensity ITD and platform files.
REM
REM Alex Hadjadj <alex.hadjadj@rockstarnorth.com>
REM

CALL setenv.bat
SET ITD=debDensity

SET INPUT=%RS_PROJROOT%\art\Debug/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_rpf.rb --project=%RS_PROJECT% --branch=dev --filter=*.* --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_rmptfx_fxdecal_itd.bat
