@SETLOCAL
set CONVERT=%RS_TOOLSCONVERT%

findstr emailaddress \gta5\tools\local.xml | findstr builder > NUL
if not errorlevel 1 goto haveBuilder
findstr emailaddress \gta5\tools\local.xml
echo Add -builder to your username in this line of \gta5\tools\local.xml
pause

:haveBuilder

cd/d x:\gta5\assets\export

time/t > last_gta5.txt

if "%1"=="--justGta5" goto justGta5
if "%1"=="--incremental" goto justGta5

title Converting textures . . .
%CONVERT% --recursive textures

if "%1"=="--full" (
	title Converting anim . . .
	%CONVERT% --recursive anim 
) else (
	pushd gta5
	title Converting anim\cutsprops . . .
	%CONVERT% levels\gta5\generic\cutsobjects.zip
	popd
)

title Converting models . . .
%CONVERT% --recursive models

pushd data

title Converting data\cdimages . . .
%CONVERT% --recursive cdimages

title Converting data\effects . . .
%CONVERT% --recursive effects

popd


pushd levels

title Converting levels\generic . . .
%CONVERT% --recursive generic

popd

:justGta5

pushd levels

pushd gta5

for %%f in (*.zip) do (
	title Converting levels\gta\%%f . . .
	%CONVERT% %%f
)

if "%1" == "--incremental" (

	for %%f in (script _prologue) do (
		title Converting only levels\gta5\%%f . . . 
		%CONVERT% --recursive %%f
	)

) else (

	for /d %%f in (cloudhats destruction generic interiors outsource props script _prologue vehicles vehiclemods) do (
		title Converting levels\gta5\%%f . . . 
		%CONVERT% --recursive %%f
	)
)

for /d %%f in (_citye _cityw _hills) do (
	pushd %%f

	title Converting levels\gta5\%%f zips . . . 
	%CONVERT% *.zip

	for /d %%g in (*) do (
		title Converting levels\gta5\%%f\%%g . . . 
		%CONVERT% --recursive %%g
	)

	popd

	REM clean up the cache between major sections, it's about 150G total
	REM for all three platforms right now.
	rd /s/q x:\gta5\cache\convert
)

popd

popd

title Finished converting main gta5 level!
time/t >> last_gta5.txt
type last_gta5.txt
