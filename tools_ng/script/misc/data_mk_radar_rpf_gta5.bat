@ECHO OFF
REM
REM data_mk_radar_rpf_gta5.bat
REM Create the gta5 independent radar.rpf file and its platform equivalents.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat

SET LEVEL=gta5
SET OUTPUT=%RS_EXPORT%/levels/%LEVEL%/radar.rpf

%RS_TOOLSRUBY% %CD%/data_mk_radar_rpf.rb --project=%RS_PROJECT% --branch=dev --map=%LEVEL%
%RS_TOOLSRUBY% %RS_TOOLSLIB%/util/data_convert_file.rb %OUTPUT%
PAUSE

REM data_mk_radar_rpf_gta5.bat

