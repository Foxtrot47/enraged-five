#
# File:: generic_rpf_add.rb
# Description:: This script takes two parameters the first a single file that the user wants to 
#		pack into a rpf and the second being the rpf to pack the file into.
#
# Author:: David Evans <dave.evans@rockstarnorth.com>
# Date:: 16 December 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
Pipeline::Config::instance()::logtostdout = true

require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
require 'pipeline/projectutil/data_extract'
require 'pipeline/projectutil/data_convert'
require 'pipeline/scm/perforce'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
	[ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
	[ '--file', '-o', OS::Getopt::REQUIRED, 'the location of the file you wish to pack into a rpf.'],
	[ '--rpf', '-o', OS::Getopt::REQUIRED, 'the location of the rpf you wish to pack the file into.'],
	[ '--debug', '-d', OS::Getopt::BOOLEAN, 'display debug information.' ]
]

#---------------------------------------------------------------------
# Functions
#---------------------------------------------------------------------	
#
# name: does_rpf_exist
# Determines if the given path is a valid rpf path and whether or not it exists.
# This return true if it exitsts and needs to be extracted first or false if it just needs to be created
#
def does_rpf_exist( rpf_path )

	exists = false
	
	if File.exist? rpf_path then
	
		extension = OS::Path.get_extension( rpf_path )
		
		if extension == "rpf"
		
			exists = true
			
		end
	
	end
	
	exists
	
end

#
# name: extract_rpf
# This extracts the given rpf to the given location using the given rage utility, this
# returns a list of the extracted files
#
def extract_rpf( rpf_path, extraction_location, rage )

	extracted_files = ProjectUtil::data_extract_rpf( rage, rpf_path, extraction_location, true ) do |rpfname|
		puts "Extracting: #{rpfname}"
	end
	
	extracted_files
end

#
# name: create_rpf
# This creates an rpf file with the given files in it at the given location using
# the given rage utility
#
def create_rpf( rpf_path, files, rage )

	add_files = files

	puts( "\n\n\nAdd Files:" )
	add_files.each { | file |
		
		puts( "\n#{file}" )
		
	}

	rage.pack.start_uncompressed( )
	add_files.each { | file |
	
		filename = OS::Path::get_filename( file )
		rage.pack.add( file, filename )
		
	}
	
	rage.pack.save( rpf_path )
	rage.pack.close( )	

end

#
# name: delete_directory
# Deletes the given directory by first deleting all the files inside it.
#
def delete_directory( directory_location )

	finds = OS::FindEx::find_files( OS::Path::combine( directory_location, '*.*' ) )
	finds.each { | file |
		File.delete(file)
	}
	Dir.delete( directory_location )
	
end 

#
# name: create_scaleform_rpf
# Creates a rpf file named scaleform at the given path 
# it uses the two global arrays g_gfx_files and g_itd_files as input
#
def create_scaleform_rpf( rpf_filepath, changelist, p, r, g_gfx_list, g_idt_list, create_new_rpf )

	if ( create_new_rpf ) then
		# checkout the rpf file or mark it for add and put it inside the global changelist 
		p.run_edit_or_add( rpf_filepath )
		p.run_reopen( '-c', changelist, rpf_filepath )
	
		add_files = g_gfx_list + g_idt_list
		r.pack.start_uncompressed( )
		add_files.each { | file |
			filename = OS::Path::get_filename( file )
			r.pack.add( file, filename )
		}
	
		r.pack.save( rpf_filepath )
		r.pack.close( )
	else
		# checkout the rpf file or mark it for add and put it inside the global changelist 
		p.run_edit_or_add( rpf_filepath )
		p.run_reopen( '-c', changelist, rpf_filepath )
	
		temp_extraction_path = OS::Path::get_directory( rpf_filepath )
		temp_extraction_path = OS::Path::combine( temp_extraction_path, 'temp' )
		temp_extraction_path = OS::Path::normalise( temp_extraction_path )
		extracted_files = ProjectUtil::data_extract_rpf( r, rpf_filepath, temp_extraction_path, true ) do |rpfname|
			puts "Extracting: #{rpfname}"
		end
		
		add_files = g_gfx_list + g_idt_list
		
		extracted_files.each { | extracted_file | 
			extracted_filename = OS::Path::get_filename( extracted_file )
			
			add_file_result = true
			add_files.each { | add_file |
				add_filename = OS::Path::get_filename( add_file )
				if ( add_filename == extracted_filename ) then
					add_file_result = false
				end
			}
			if ( add_file_result == true ) then
				add_files.push( extracted_file )
			end
		}
		
		r.pack.start_uncompressed( )
		add_files.each { | file |
			filename = OS::Path::get_filename( file )
			r.pack.add( file, filename )
		}
	
		r.pack.save( rpf_filepath )
		r.pack.close( )	
		
		finds = OS::FindEx::find_files( OS::Path::combine( temp_extraction_path, '*.*' ) )
		finds.each { | file |
			File.delete(file)
		}
		Dir.delete( temp_extraction_path )
	end
end

#
# name: delete_pre_existing_changelists
# Delete any changelists in the users pending list that are empty and has the 
# description "g_changelist_description".
#
def delete_pre_existing_changelists ( p, changelist_description, l, debug_enabled )

	pending_list = p.run_changes('-l', '-s', 'pending', '-u', p.user, '-c', p.client )

	description_list = []
	pending_list.each { | changelist |
		if changelist_description <=> changelist['desc'] then
			p.run_change( '-d', changelist['change'] )
			l.info( "\nDeleting changelist #{changelist['change']} from pending list" )
		end
	}
end

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		print "Application Name = #{g_AppName} \n"
		g_Config = Pipeline::Config.instance( )
		g_Project = nil
		g_Branch = nil
		
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		
		g_debug_enabled = opts['debug'].nil? ? false : true
		
		if opts['file'] == nil then
			exit( 1 )
		end	
		g_file_location = opts['file']
		
		if opts['rpf'] == nil then
			exit( 2 )
		end
		g_rpf_location = opts['rpf']
		
		puts ( "File = #{g_file_location}" )
		puts ( "RPF = #{g_rpf_location}" )
		
		#---------------------------------------------------------------------
		# Get project name, instance, and test if it is enabled
		#---------------------------------------------------------------------
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 3 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 4 )
		end
		g_Log.info( "The project '#{g_ProjectName}' is enabled" ) if g_debug_enabled
		
		#---------------------------------------------------------------------
		# Load the project config and get the branch
		#---------------------------------------------------------------------
		g_Project.load_config( )
		g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
		branch_exists = ( g_Project.branches.has_key?( g_BranchName ) )
		if ( not branch_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not have branch named #{g_BranchName} defined."
			exit( 5 )
		end
		g_Log.info( "The branch '#{g_BranchName}' exists" ) if g_debug_enabled
		g_Branch = g_Project.branches[g_BranchName]
	
		#---------------------------------------------------------------------
		# Create the RAGE utility
		#---------------------------------------------------------------------
		rage = RageUtils.new( g_Project, g_BranchName )
		
		#---------------------------------------------------------------------
		# Preforce creation and setup
		#---------------------------------------------------------------------		
		Dir::chdir( g_Project.art )
		g_Perforce = Pipeline::SCM::Perforce.new( )
		g_Perforce.connect( )
		
		g_changelist_description = "Automatic rpf add"
		g_ChangelistNumber = g_Perforce.create_changelist( g_changelist_description )
		
		#---------------------------------------------------------------------
		# Grab latest version for the rpf file and the added file
		#---------------------------------------------------------------------
		g_Perforce.run_sync( "#{g_rpf_location}" )
		g_Perforce.run_sync( "#{g_file_location}" )

		#---------------------------------------------------------------------
		# Determine if the given rpf file exists and is a valid rpf file
		#---------------------------------------------------------------------	
		create_rpf = does_rpf_exist( g_rpf_location ) ? false : true
		puts ( "Create a new Rpf file = #{create_rpf}" )

		add_files = []
		add_files.push( g_file_location )

		if ( not create_rpf ) then
		
			g_Perforce.run_edit_or_add( g_rpf_location )
			g_Perforce.run_reopen( '-c', g_ChangelistNumber, g_rpf_location )
			
			extraction_location = OS::Path::get_directory( g_rpf_location )
			extraction_location = OS::Path::combine( extraction_location, 'temp' )
			extraction_location = OS::Path::normalise( extraction_location )
			extracted_files = extract_rpf( g_rpf_location, extraction_location, rage )
			extracted_files.each { | file |
			
				add_files.push( file )
			
			}
			
			create_rpf( g_rpf_location, add_files, rage )
			
			if ( not create_rpf ) then
			
				delete_directory( extraction_location )
					
			end
		
		else
		
			create_rpf( g_rpf_location, add_files, rage )
		
		end
		
		ProjectUtil::data_convert_file( g_rpf_location, true )

		print "\n\n"
	end 
end
	
# data_mk_scaleform_rpf.rb