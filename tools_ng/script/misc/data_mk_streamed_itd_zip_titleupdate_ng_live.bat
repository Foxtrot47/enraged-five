REM @ECHO OFF
REM
REM data_mk_streamed_itd_zip.bat
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat 

set RUBY

SET INPUT=x:\gta5\assets_ng\export\textures\level_design_titleupdate_live
SET OUTPUT=x:\gta5\assets_ng\titleupdate\dev_ng_live\textures\script_txds.zip

ECHO Building Texture Dictionaries...
FOR /D %%G IN (%INPUT%\*) DO (
	ECHO Creating %%G.itd.zip from 
	ECHO   %%G\*.dds
	%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --uncompressed --filter=*.* --output=%%G.itd.zip %%G
)

ECHO Building Streamed Texture Dictionary RPF...
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --uncompressed --filter=*.itd.zip --output=%OUTPUT% %INPUT%
CALL %RS_TOOLSCONVERT% --branch dev_ng_live %OUTPUT%

PAUSE

REM data_mk_streamed_itd_zip.bat
