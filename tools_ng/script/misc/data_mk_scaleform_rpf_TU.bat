REM @ECHO ON
REM
REM File:: data_mk_scaleform_rpf.bat
REM Description::
REM
REM Author:: David Evans <dave.evans@rockstarnorth.com>
REM Date::
REM
CALL setenv.bat

SET GFX_ROOT="X:\gta5\art\UI_TITLE_UPDATE\gfx"
SET ZIP_DIR=%1

REM IF NOT EXIST %GFX_ROOT% GOTO NODELETE
REM RD /S /Q %GFX_ROOT%
REM GOTO NODELETE

title %ZIP_DIR%

:NODELETE
REM For single ZIP/RPF builds:
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_rpf_TU.rb %ZIP_DIR%

REM For all ZIP/RPFs:
REM %RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_rpf.rb %*

PAUSE
REM data_mk_scaleform_rpf.bat
