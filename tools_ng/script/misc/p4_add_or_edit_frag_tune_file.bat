@echo off

REM ------------------------------------------------------------
REM Opens a file for ADD or EDIT in the default change list.
REM Requires one parameter -- the file name to add / edit.
REM ------------------------------------------------------------
call setenv.bat >nul

set FRAGMENT_FILE=%~1

p4 sync %FRAGMENT_FILE%
if exist %FRAGMENT_FILE% (
	p4 edit %FRAGMENT_FILE%
) else (
	p4 add %FRAGMENT_FILE%
)