@ECHO OFF
REM
REM CompareSvaFileVersionsForWetness.bat
REM
REM Gunnar Droege <gunnar.droege@rockstarnorth.com>
REM

CALL setenv.bat

SET input=x:/gta5/assets/export/levels/gta5/_citye/hollywood_01 
SET start_date=2013/06/20:15:10:03  
SET end_date=2013/07/01:15:10:03 
SET selectorFile=X:/gta5/tools_ng/script/misc/affectedContainers.txt 
SET output=x:/gta5/assets/reports/svaDifferences.csv

%RS_TOOLSROOT%\bin\util\CompareSvaFiles.exe -input %input% -start_date %start_date% -end_date %end_date% -selectorFile %selectorFile% -output %output%
PAUSE

REM CompareSvaFileVersionsForWetness.bat

