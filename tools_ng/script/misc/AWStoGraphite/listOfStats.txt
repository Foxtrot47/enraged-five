rsgscs.aws.ec2.network-in;NetworkIn;Sum;EC2
rsgscs.aws.ec2.network-out;NetworkOut;Sum;EC2
rsgscs.aws.ec2.cpu-utilization.max;CPUUtilization;Maximum;EC2
rsgscs.aws.ec2.cpu-utilization.avg;CPUUtilization;Average;EC2
rsgscs.aws.elb.requests;RequestCount;Sum;ELB
rsgscs.aws.elb.latency-avg;Latency;Average;ELB
rsgscs.aws.dynamo.rsg-cloudsaves-gtav.batch-write;SuccessfulRequestLatency;Average;Dynamo;BatchWriteItem
rsgscs.aws.dynamo.rsg-cloudsaves-gtav.get-item;SuccessfulRequestLatency;Average;Dynamo;GetItem
rsgscs.aws.dynamo.rsg-cloudsaves-gtav.read-capacity;ProvisionedReadCapacityUnits;Average;Dynamo
rsgscs.aws.dynamo.rsg-cloudsaves-gtav.consumed-write-capacity;ConsumedWriteCapacityUnits;Average;Dynamo
rsgscs.aws.dynamo.rsg-cloudsaves-gtav.consumed-read-capacity;ConsumedReadCapacityUnits;Average;Dynamo
rsgscs.aws.redis.DBSize.redisDB10;RedisDBSize10;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB9;RedisDBSize9;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB8;RedisDBSize8;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB7;RedisDBSize7;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB6;RedisDBSize6;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB5;RedisDBSize5;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB4;RedisDBSize4;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB3;RedisDBSize3;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB2;RedisDBSize2;Average;CUSTOM
rsgscs.aws.redis.DBSize.redisDB1;RedisDBSize1;Average;CUSTOM
rsgscs.aws.cloudsave.cpu-max;CSWEB Cluster Cpu Utilization MaxOfMax;Average;CUSTOM
rsgscs.aws.telemetry.cpu-max;TMWEB Cluster Cpu Utilization MaxOfMax;Average;CUSTOM
rsgscs.aws.cloudsave.cpu-total;CSWEB Cluster Cpu Utilization Total;Average;CUSTOM
rsgscs.aws.telemetry.cpu-total;TMWEB Cluster Cpu Utilization Total;Average;CUSTOM
rsgscs.aws.cloudsave.cpu-average;CSWEB Cluster Cpu Utilization Average;Average;CUSTOM
rsgscs.aws.telemetry.cpu-average;TMWEB Cluster Cpu Utilization Average;Average;CUSTOM
rsgscs.aws.elb.telemetry.4xx-error-percentage;TMWEB ELB 4xx Percentage;Average;CUSTOM
rsgscs.aws.elb.telemetry.5xx-error-percentage;TMWEB ELB 5xx Percentage;Average;CUSTOM
rsgscs.aws.elb.cloudsave.4xx-error-percentage;CSWEB ELB 4xx Percentage;Average;CUSTOM
rsgscs.aws.elb.cloudsave.5xx-error-percentage;CSWEB ELB 5xx Percentage;Average;CUSTOM
rsgscs.aws.cloudsave.4xx-error-percentage;CSWEB 4xx Percentage;Average;CUSTOM
rsgscs.aws.cloudsave.5xx-error-percentage;CSWEB 5xx Percentage;Average;CUSTOM
rsgscs.aws.telemetry.4xx-error-percentage;TMWEB 4xx Percentage;Average;CUSTOM
rsgscs.aws.telemetry.5xx-error-percentage;TMWEB 5xx Percentage;Average;CUSTOM
rsgscs.aws.twem.4xx-error-percentage;TWEM 4xx Percentage;Average;CUSTOM
rsgscs.aws.twem.5xx-error-percentage;TWEM 5xx Percentage;Average;CUSTOM
rsgscs.aws.elb.twem.4xx-error-percentage;TWEM ELB 4xx Percentage;Average;CUSTOM
rsgscs.aws.elb.twem.5xx-error-percentage;TWEM ELB 5xx Percentage;Average;CUSTOM
