@ECHO OFF
REM
REM data_mk_itd.bat
REM Create a generic ITD.ZIP and platform files.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM GTA5 version modified by Keith
REM

CALL setenv.bat
SET INPUT=%RS_ASSETS%/textures/%1/
SET OUTPUT=%RS_EXPORT%/textures/%1.itd.zip

IF "%1" == "" GOTO NODICT_ERROR

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.dds --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

GOTO DONE

:NODICT_ERROR
ECHO Error: no texture dictionary defined.
ECHO Usage: data_mk_itd.bat texdict_name
ECHO E.g. data_mk_itd.bat fonts

:DONE
PAUSE

:END

REM data_mk_itd.bat
