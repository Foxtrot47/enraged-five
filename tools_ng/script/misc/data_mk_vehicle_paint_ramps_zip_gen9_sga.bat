rem @ECHO OFF
REM
REM data_mk_vehicle_paint_ramps_zip_gen9_sga.bat
REM Create the vehicle paint ramps ZIP.
REM
REM Albert Elwin <albert.elwin@rockstardundee.com>
REM

call %~dp0\data_mk_vehicle_paint_ramps_zip_any.bat setenv_gen9_sga.bat x:\gta5\assets_gen9_sga\titleupdate\dev_gen9_sga\textures

REM data_mk_vehicle_paint_ramps_zip_gen9_sga.bat
