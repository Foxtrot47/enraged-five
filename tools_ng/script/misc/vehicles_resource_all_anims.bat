@ECHO OFF
REM
REM File:: vehicles_resource_all_anims.bat
REM Description::
REM
REM Author:: Gunnar Droege <gunnar.droege@rockstarnorth.com>
REM Date:: 13/6/12
REM

CALL setenv.bat

%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/vehicles_resource_all_anims.rb

PAUSE
REM vehicles_resource_all_anims.bat
