@ECHO OFF
REM
REM data_mk_waypoint_rpf.bat
REM Create the waypoint recordings RPF.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat
SET FILTER=*.iwr
SET INPUT=%RS_ASSETS%\recordings\waypoints
SET OUTPUT=%RS_EXPORT%\levels\%RS_PROJECT%
SET OUTPUT_FILE=%OUTPUT%\waypointrec.zip

PUSHD %INPUT%
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_sync.rb %INPUT%\...
POPD

PUSHD %OUTPUT%
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_sync.rb %OUTPUT_FILE%
IF %ERRORLEVEL% NEQ 0 ( GOTO :PERFORCE_ERROR )
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_edit.rb %OUTPUT_FILE% 
IF %ERRORLEVEL% NEQ 0 ( GOTO :PERFORCE_ERROR )
POPD

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --filter=%FILTER% --output=%OUTPUT_FILE% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT_FILE%

PAUSE


SET PLATFORM=ps3
CALL :COPY_FUNC

SET PLATFORM=xbox360
CALL :COPY_FUNC

PAUSE

GOTO :EOF

:PERFORCE_ERROR

echo An error has occurred in Perforce. Unable to continue. 
PAUSE

GOTO :EOF

:COPY_FUNC

SET SOURCE_FILE=%RS_BUILDBRANCH%\%PLATFORM%\levels\%RS_PROJECT%\waypointrec.rpf
SET DEST_FOLDER=%RS_PROJROOT%\build\release\%PLATFORM%\levels\%RS_PROJECT%
SET DEST_FILE=%DEST_FOLDER%\waypointrec.rpf


IF NOT EXIST %SOURCE_FILE% GOTO :EOF

IF NOT EXIST %DEST_FOLDER% MKDIR %DEST_FOLDER%

IF NOT EXIST %DEST_FILE% GOTO :NO_DEST_FILE

attrib -r %DEST_FILE%

:NO_DEST_FILE

echo Copying %SOURCE_FILE% to %DEST_FILE%

copy %SOURCE_FILE% %DEST_FILE%

IF NOT EXIST %DEST_FILE% GOTO :EOF

attrib +r %DEST_FILE%

GOTO :EOF


