#
# File:: data_mk_radar_rpf.rb
# Description:: Make map radar RPF.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 30 September 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/config/project'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/environment'
require 'pipeline/util/rage'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--project', '-p', OS::Getopt::REQUIRED, 'project data to convert (e.g. jimmy, gta4_jpn).' ],
	[ '--branch', '-b', OS::Getopt::REQUIRED, 'project branch to convert (e.g. dev, dev_migrate).' ],
	[ '--map', '-m', OS::Getopt::REQUIRED, 'map name (e.g. island, desert, alpine).' ],
	[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information.' ]
]

#-----------------------------------------------------------------------------
# Functions
#-----------------------------------------------------------------------------

#
# Create an RPF ITD content node for each DDS file in a directory, adding the
# DDS file as the RPFs only input file.
#
def create_itd_per_dds( r, project, branch, itd_output, path )
	files = OS::FindEx::find_files( OS::Path::combine( path, '*.dds' ) )
	files.each do |file|
		itd_filename = OS::Path::replace_ext( file, 'itd' )
		
		r.pack.start( )
		r.pack.add( file, OS::Path::get_filename( file ) )
		r.pack.save( itd_filename )
		r.pack.close( )
		
		puts "built: #{itd_filename}"
		itd_output << itd_filename
	end
end

#
# Create a single RPF ITD content node for all DDS files in a directory, adding
# the DDS files as the RPFs inputs.
#
def create_itd_from_path( r, project, branch, name, path )
	itd_filename = OS::Path::combine( path, "#{name}.itd" )
	
	r.pack.start( )
	files = OS::FindEx::find_files( OS::Path::combine( path, '*.dds' ) )
	files.each do |file|
		filename = OS::Path::get_filename( file )
		filepath = OS::Path::get_directory( file )
		
		r.pack.add( file, filename )
	end
	r.pack.save( itd_filename )
	r.pack.close( )
	
	puts "built: #{itd_filename}"
	itd_filename
end

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_ProjectName = ''
		g_Project = nil
		g_Branch = ''
		g_Config = Pipeline::Config.instance( ) 
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] ) then
			puts OS::Getopt.usage( OPTIONS )
			exit( 1 )
		end
		
		g_ProjectName = ( nil == opts['project'] ) ? '' : opts['project']
		project_exists = ( g_Config.projects.has_key?( g_ProjectName ) )
		if ( not project_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not exist or its configuration is unreadable."
			exit( 2 )
		end
		g_Project = g_Config.projects[ g_ProjectName ]
		if ( not g_Project.enabled ) then
			puts "\nError project: #{g_ProjectName} is not enabled on this machine.  Re-run installer."
			exit( 3 )
		end
		g_Project.load_config( )
		g_BranchName = ( nil == opts['branch'] ) ? g_Project.default_branch : opts['branch']
		branch_exists = ( g_Project.branches.has_key?( g_BranchName ) )
		if ( not branch_exists ) then
			puts OS::Getopt.usage( OPTIONS )
			puts "\nError project: #{g_ProjectName} does not have branch named #{g_BranchName} defined."
			exit( 4 )
		end
		g_Branch = g_Project.branches[g_BranchName]
		g_MapName = opts['map']
		
		# Create ConvertSystem
		r = RageUtils.new( g_Project, g_BranchName )
		
		# Create paths
		radar_path = OS::Path::combine( g_Project.art, 'misc', 'maps', g_MapName, 'radar' )
		map_high_path = OS::Path::combine( g_Project.art, 'misc', 'maps', g_MapName, 'map_high' )
		map_low_path = OS::Path::combine( g_Project.art, 'misc', 'maps', g_MapName, 'map_low' )
		
		# Build individual texture dictionaries
		map_radar_itds = []
		map_high_itds = []

		create_itd_per_dds( r, g_Project, g_Branch, map_radar_itds, radar_path )
		create_itd_per_dds( r, g_Project, g_Branch, map_high_itds, map_high_path )
		map_low_itd = create_itd_from_path( r, g_Project, g_Branch, 'map_low', map_low_path )
				
		# Build radar.img
		image_filename = OS::Path::combine( g_Branch.independent, 'levels', g_MapName, 'radar.rpf' )
		if ( not ::File::writable?( image_filename ) ) then
		
			puts "#{image_filename} is not checked out.  Aborting." 
			exit( 5 )
		end
		
		r.pack.start_uncompressed( )
		map_radar_itds.each do |itd| 
			r.pack.add( itd, OS::Path::get_filename( itd ) )
		end
		map_high_itds.each do |itd| 
			r.pack.add( itd, OS::Path::get_filename( itd ) )
		end
		r.pack.add( map_low_itd, OS::Path::get_filename( map_low_itd ) )
		r.pack.save( image_filename )
		puts "Saved: #{image_filename}"
		r.pack.close( )
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")
	end 
end

# data_mk_radar_rpf.rb
