@ECHO OFF
REM
REM data_mk_streamed_itd_zip.bat
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat >NUL

SET INPUT=x:\gta5_dlc\mpPacks\mpPilot\assets_ng\textures\level_design
SET OUTPUT=x:\gta5_dlc\mpPacks\mpPilot\assets_ng\export\textures\script_txds.zip

ECHO Building Texture Dictionaries...
FOR /D %%G IN (%INPUT%\*) DO (
	ECHO Creating %%G.itd.zip from 
	ECHO   %%G\*.dds
	%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --uncompressed --filter=*.* --output=%%G.itd.zip %%G
)

ECHO Building Streamed Texture Dictionary RPF...
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --uncompressed --filter=*.itd.zip --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_streamed_itd_zip.bat
