@ECHO OFF
REM
REM data_mk_paths_gta5_rpf.bat
REM Create the paths image.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat

SET FILTER=*.ind
SET INPUT=%RS_BUILDBRANCH%/common/data/paths/
SET OUTPUT=%RS_EXPORT%/levels/testbed/paths.zip

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=%FILTER% --output=%OUTPUT% %INPUT%
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_paths_gta5_rpf.bat

