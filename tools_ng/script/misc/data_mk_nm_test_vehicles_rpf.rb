#
# File:: jimmy/bin/misc/data_mk_nm_test_vehicles_rpf.rb
# Description:: Updates NM Test level's independent vehicles.rpf file.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 9 November 2009
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/path'
require 'pipeline/projectutil/data_extract'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

# Vehicle names to include in the RPF file.
VEHICLES = [
	'custom',			# custom.rbs		- Ragebuilder requirement
	'custom_finish',	# custom_finish.rbs - Ragebuilder requirement
	'vehshare',	
	'vehshare_truck',
	'furzen',
]

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		c = Pipeline::Config::instance( )
		project = c.projects['jimmy']
		project.load_config( )
		project.load_content( nil, Pipeline::Project::ContentType::OUTPUT )
		vehiclerpfs_content = project.content.find_by_script( "'vehiclesrpf' == content.xml_type" )
		vehiclerpf_nmtest = nil
		nmtest_output_dir = OS::Path::combine( 'x:/streamJimmy/vehicles', 'nm_test' )
			
		r = RageUtils::new( project )
		
		# Extract all our indpendent vehicle RPFs (this will include the
		# nm_test levels vehicle RPF).
		extracted_files = []
		vehiclerpfs_content.each do |vehiclerpf|
			# Find nm_test
			if ( vehiclerpf.filename.include?( 'nm_test' ) ) then
				puts "Found nm_test: #{vehiclerpf.filename}"
				vehiclerpf_nmtest = vehiclerpf
			end
			
			puts "Extracting #{vehiclerpf.filename}..."		
			extracted_files += ProjectUtil::data_extract_rpf( r, vehiclerpf.filename, nmtest_output_dir, true ) do |filename|
				puts "\t#{filename}"
			end
		end
		
		# Find the desired vehicles within our extracted vehicles list.
		extracted_files.each do |filename|
			next unless ( File::exists?( filename ) )
			basename = OS::Path::get_basename( filename )
			next if ( VEHICLES.include?( basename ) )
			
			FileUtils::remove( filename )
		end
		
		files = OS::FindEx::find_files( OS::Path::combine( nmtest_output_dir, '*.*' ) )
		r.pack.start( )
		files.each do |filename|
			r.pack.add( filename, OS::Path::get_filename( filename ) )
		end
		r.pack.save( vehiclerpf_nmtest.filename )
		r.pack.close( )
		
	rescue Exception => ex
	
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end
end

# jimmy/bin/misc/data_mk_nm_test_vehicles_rpf.rb
