require 'mysql'

def with_db

	#dbh = Mysql.real_connect('ediw-toolsb1','system','rdsgokuhero8e5rg','greg_smith_dev')
	dbh = Mysql::new('ediw-toolsb1','system','rdsgokuhero8e5rg','greg_smith_dev')
	
	begin
	
		yield dbh
	ensure
		
		dbh.close
	end
end

class DebugNode

	attr_reader :type, :coord_x, :coord_y, :coord_z, :label, :creation_number, :box_index
	attr_writer :type, :coord_x, :coord_y, :coord_z, :label, :creation_number, :box_index
end

#!/usr/bin/ruby -w
# chat.rb
require 'gserver'
class DebugNodeServer < GServer

	def initialize(port=20606, host=GServer::DEFAULT_HOST)
	
		@clients = []
		super(port, host, Float::MAX, $stderr, true)
	end

	def init_system( msg )
  
		puts("init")
  
		with_db do |db|

			db.query('create table DebugNodeMeta (version INTEGER)')
			db.query('create table DebugNodeUser (id INTEGER PRIMARY KEY, name TEXT)')
			db.query('create table DebugNodeSet (id INTEGER PRIMARY KEY, userid INTEGER REFERENCES DebugNodeUser(id) ON DELETE CASCADE, nodecount INTEGER, name TEXT, creation_num INTEGER, node_creation_num INTEGER)')
			db.query('create table DebugNode (setid INTEGER REFERENCES DebugNodeSet(id) ON DELETE CASCADE, type INTEGER, coordx FLOAT, coordy FLOAT, coordz FLOAT, name TEXT, create_num INTEGER, box_index INTEGER)')
			db.query('insert into DebugNodeUser(id, name) values (0, "Greg")')
		end
	end

	def make_safe_label( str )
	
		str.strip!
		str = str[1...(str.size - 1)]
		str.strip!
		
		"'#{str}'"	
	end
	
	def save_set( msg )

		puts("save_set")
		
		with_db do |db|
			
			msgtoks = msg.split(',')
			
			userid = msgtoks[1]
			index = msgtoks[2]
			nodes_size = msgtoks[3]
			name = msgtoks[4]
			creation_number = msgtoks[5]
			node_creation_number = msgtoks[6].to_i		
		
			name = make_safe_label(name)
		
			db.query("delete from DebugNodeSet where id = #{index}")
			db.query("delete from DebugNode where setid = #{index}")
		
			query = "insert into DebugNodeSet values (#{index}, #{userid}, #{nodes_size}, #{name}, #{creation_number}, #{node_creation_number})"
			puts(query)			
			db.query(query)
			
			array_index = 7
			
			0.upto(node_creation_number - 1) { |i|
							
				dn = DebugNode.new()
				
				dn.type = msgtoks[array_index]
				array_index = array_index + 1
				dn.coord_x = msgtoks[array_index]
				array_index = array_index + 1
				dn.coord_y = msgtoks[array_index]
				array_index = array_index + 1
				dn.coord_z = msgtoks[array_index]
				array_index = array_index + 1
				dn.label = msgtoks[array_index]
				array_index = array_index + 1
				dn.creation_number = msgtoks[array_index]
				array_index = array_index + 1
				dn.box_index = msgtoks[array_index]
				array_index = array_index + 1
								
				dn.label = make_safe_label(dn.label)
								
				query = "insert into DebugNode values (#{index}, #{dn.type}, #{dn.coord_x}, #{dn.coord_y}, #{dn.coord_z}, #{dn.label}, #{dn.creation_number}, #{dn.box_index})"
				puts(query)			
				db.query(query)
			}
		
		end
	end

	def load_set( msg, sock )
		
		puts("load_set")
		
		msgtoks = msg.split(',')
		userid = msgtoks[1]
		index = msgtoks[2]	
		
		msg = "/loaded_set, "
	
		with_db do |db|
		
			db.query_with_result = true
			res = db.query("select * from DebugNodeSet where id = #{index}")
						
			res.each { |x|
			
				x.each { |i| msg << "#{i}, " }	
			}
		end
		
		with_db do |db|
						
			db.query_with_result = true
			res = db.query("select * from DebugNode where setid = #{index}")
			
			res.each { |x|
						
				x[1...x.size].each { |i| msg << "#{i}, " }	
			}
		end
		
		puts("sending back...")
		sock.print(msg)
	
		#puts(msg)
	end

	def delete_set( msg )

		puts("delete_set")
	
		msgtoks = msg.split(',')
		userid = msgtoks[1]
		index = msgtoks[2]
	
		with_db do |db|
		
			db.query('delete from DebugNodeSet where id = #{index}')
			db.query('delete from DebugNode where setid = #{index}')
		end
	end
  
	def serve(sock)
	
		begin
		
			@clients << sock
			hostname = sock.peeraddr[2] || sock.peeraddr[3]
	  
			until sock.eof? do
			#while true do
						
				message = sock.gets.chomp
				msgtoks = message.split(',')

				puts("#{msgtoks[0]}")
				
				begin
		
					case msgtoks[0]
					
						when "/init" then init_system(message)
						when "/save_set" then save_set(message)
						when "/load_set" then load_set(message, sock)
						when "/delete_set" then delete_set(message)
					end
				
				rescue
				end
			end
			
		rescue
		
			puts("there was an exception")
		ensure
	
			@clients.delete(sock) 
		end
	end
end

server = DebugNodeServer.new(20606,"EDIW-GSMITH.rockstar.t2.corp")
server.start(-1)
server.join