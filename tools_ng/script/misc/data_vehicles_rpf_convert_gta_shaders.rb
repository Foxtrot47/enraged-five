#
# File:: gta5/bin/misc/data_mk_nm_test_vehicles_rpf.rb
# Description:: updates entity.type file
#
# Author:: Gunnar Droege <gunnar.droege@rockstarnorth.com>
# Date:: 6 September 2010
#

#----------------------------------------------------------------------------
# Uses
#----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/path'
#require 'pipeline/resourcing/convert_pack'
#require 'pipeline/resourcing/convert'
require 'pipeline/util/rage_pack'
require 'pipeline/projectutil/data_extract'
include Pipeline

#----------------------------------------------------------------------------
# Constants
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
# Implementation
#----------------------------------------------------------------------------
if ( __FILE__ == $0 ) then
	
	begin
		c = Pipeline::Config::instance( )
		project = c.projects['gta5']
		project.load_config( )
		project.load_content( nil, Pipeline::Project::ContentType::OUTPUT )
		vehiclerpfs_content = []
		if (ARGV.length<1) then
			vehiclerpfs_content = project.content.find_by_script( "'vehiclesrpf' == content.xml_type" )
		else
			# arguments with rpfs to convert
			ARGV.each do |arg|
				puts ("\""+arg+"\"")
				tempRpfs = project.content.find_by_script( "'"+arg+"' == content.name")
				puts tempRpfs.to_s
				next unless tempRpfs.length>=1
				tempRpfs.each do |tempRpf|
					if File::exists?( tempRpf.filename ) then #&& tempRpf.filename.include?( 'nm_test' )
						vehiclerpfs_content << tempRpf
					end
				end
			end
		end
		if (vehiclerpfs_content.length<1) then
			puts "Nae found content."
		end
		vehiclerpf_nmtest = nil
		nmtest_output_dir = OS::Path::combine( 'x:/streamGTA5/vehicles', 'rpfBuildTemp' )
			
		r = RageUtils::new( project )
		
		# Extract all our indpendent vehicle RPFs (this will include the
		# nm_test levels vehicle RPF).
		vehiclerpfs_content.each do |vehiclerpf|
			puts "===>Extracting #{vehiclerpf.filename}..."		
			extracted_files = []
			extracted_files += ProjectUtil::data_extract_rpf( r, vehiclerpf.filename, nmtest_output_dir, true ) do |filename|
				puts "\t#{filename}"
			end
		
			puts "===>Extracting single drawables/fragments"
			
			# Extract all the drawables
			extracted_files.each do |iftfilename|
				next unless ( File::exists?( iftfilename ) && /(\.ift)|(\.idr)/.match(iftfilename) )
				
				# Extract
				basename = OS::Path::get_basename( iftfilename )
				extracted_sub_files = []
				temp_output_dir = OS::Path::combine( nmtest_output_dir, basename )
				Dir.mkdir(temp_output_dir, 777)
				extracted_sub_files += ProjectUtil::data_extract_generic( r, r.pack, iftfilename, temp_output_dir, true ) #do |tempfilename|
					#puts "\t#{tempfilename}"
				#end
				
				# Find entity file
				entityFile = OS::FindEx::find_files( OS::Path::combine( temp_output_dir, 'entity.type' ) )
				if entityFile.length()==0 then
					puts "#{temp_output_dir} has no entity.type file"
					FileUtils.rm_rf(temp_output_dir)
					next
				end

				#Search for gta_ string and replace
				File.open(entityFile[0], 'r+') do |f|   # open file for update
					lines = f.readlines           # read into array of lines
					lines.each do |it|            # modify lines
						it.gsub!(/\tgta_/, "\t")
					end
					f.pos = 0                     # back to start
					f.print lines                 # write out modified lines
					f.truncate(f.pos)             # truncate to new length
				end                               # file is automatically closed
				
				# Create new ift
				allFiles = OS::FindEx::find_files( OS::Path::combine( temp_output_dir, '*.*' ) )
				#FileUtils.chmod_R(0777, allFiles, :verbose => true, :force => true)
				r.pack.start( )
				allFiles.each do |filename|
					# puts "Pack file: #{filename}"
					r.pack.add( filename, OS::Path::get_filename( filename ) )
				end
				puts "save file #{vehiclerpf.filename}"
				r.pack.save( iftfilename )
				r.pack.close( )
				FileUtils.chmod_R(0777, temp_output_dir, :verbose => true, :force => true)
				#FileUtils::remove( temp_output_dir+"\\" )
				FileUtils.rm_rf(temp_output_dir)
			end

			#build new rpf
			files = OS::FindEx::find_files( OS::Path::combine( nmtest_output_dir, '*.*' ) )
			r.pack.start( )
			files.each do |filename|
				# puts "Pack file: #{filename}"
				r.pack.add( filename, OS::Path::get_filename( filename ) )
			end
			puts "save file #{vehiclerpf.filename}"
			r.pack.save( vehiclerpf.filename )
			r.pack.close( )
			# clean directory
			files.each do |filename|
				FileUtils::remove( filename )
			end
#			Dir.delete( nmtest_output_dir )
		end
	rescue Exception => ex
	
		puts "Unhandled exception: #{ex.message}"
		puts ex.backtrace.join( "\n" )
	end
end

# gta5/bin/misc/data_mk_nm_test_vehicles_rpf.rb
