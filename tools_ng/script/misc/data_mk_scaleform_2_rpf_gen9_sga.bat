REM @ECHO OFF
REM
REM File:: # Update for gen9 Sep 2021 GE.bat
REM Description::
REM
REM Author:: Gareth Evans <gareth.evans@rockstarnorth.com>
REM Date::09/21
REM
CALL setenv.bat

SET GFX_ROOT=%RS_PROJROOT%"\UI\NG_TU\gfx"
SET ZIP_DIR=%1

REM IF NOT EXIST %GFX_ROOT% GOTO NODELETE
REM RD /S /Q %GFX_ROOT%
REM GOTO NODELETE

title %ZIP_DIR%

:NODELETE
REM For single ZIP/RPF builds:
%RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_2_rpf_gen9_sga.rb %ZIP_DIR%

REM For all ZIP/RPFs:
REM %RS_TOOLSRUBY% %RS_TOOLSROOT%/script/misc/data_mk_scaleform_2_rpf_gen9_sga.rb %*

PAUSE
REM data_mk_scaleform_2_rpf_gen9_sga.bat
