@ECHO OFF
REM
REM data_mk_rope_itd.bat
REM Create the rope ITD and platform files.
REM
REM Alex Hadjadj <alex.hadjadj@rockstarnorth.com>
REM

CALL setenv.bat
SET ITD=rope

SET INPUT=%RS_PROJROOT%\art\Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_itd_zip.rb --project=%RS_PROJECT% --branch=dev --filter=*.*  --metadata=%RS_PROJROOT%\assets\metadata\textures\miscs\%ITD% --template=%RS_PROJROOT%\assets\metadata\textures\templates\globals\Diffuse --output=%OUTPUT% %INPUT%

%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

REM data_mk_rope_itd.bat
