@setlocal
call data_mk_common.bat %*
pushd levels

pushd gta5
title Converting levels\gta5\outsource . . .
%CONVERT% --recursive outsource
title Converting levels\gta5\interiors . . .
%CONVERT% --recursive interiors
popd

title Converting levels\testbed . . .
%CONVERT% --recursive testbed
title data_mk_testbed complete.
popd
