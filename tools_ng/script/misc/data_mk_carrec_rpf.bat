@ECHO OFF
REM
REM data_mk_carrec_rpf.bat
REM Create the car recordings ZIP.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM GTA5 version modified by Keith
REM

CALL setenv.bat >NUL
SET FILTER=*.ivr
SET INPUT=x:\gta5\assets\recordings\car
SET OUTPUT=x:\gta5\assets\titleupdate\patch\data\cdimages
SET OUTPUT_FILE=%OUTPUT%\carrec.zip

PUSHD %INPUT%
p4 sync %INPUT%\...
POPD

PUSHD %OUTPUT%
p4 edit %OUTPUT_FILE%
POPD

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --filter=%FILTER% --output=%OUTPUT_FILE% %INPUT%
%RS_TOOLSCONVERT% --branch titleupdate %OUTPUT%

pause
