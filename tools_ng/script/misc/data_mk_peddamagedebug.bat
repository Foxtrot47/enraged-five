@ECHO OFF
REM
REM data_mk_peddamagedebug_itd.bat
REM Create the peddamagedebug ITD and platform files.
REM
REM Steve Reed <steve.reed@rockstarsandiego.com>
REM

CALL setenv.bat
SET ITD=peddamagedebug

SET INPUT=%RS_PROJROOT%\art\Textures/%ITD%/
SET OUTPUT=%RS_EXPORT%/textures/%ITD%.itd.zip

p4 sync %INPUT%...
p4 edit %OUTPUT%

rem add --regentcs to the following line to force regeneration of tcs files
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%\util\data_mk_texture_dictionary.rb --filter *.dds --metadata %RS_PROJROOT%\assets\metadata\textures\misc\%ITD% --template %RS_PROJROOT%\assets\metadata\textures\templates\misc\Diffuse --output %OUTPUT% %INPUT%

IF ERRORLEVEL 1 GOTO ERROREND
%RS_TOOLSCONVERT% %OUTPUT%
GOTO END

:ERROREND

ECHO Texture dictionary construction failed.  See log.

:END
PAUSE

REM data_mk_peddamagedebug_itd.bat
