require 'pipeline/os/file'
require 'pipeline/util/string'
#require 'pipeline/content/content_core'
#require 'pipeline/projectutil/data_zip'
require 'pipeline/projectutil/data_convert'
require 'pipeline/config/project'
require 'pipeline/gui/messagebox'
include Pipeline


##########################################
#variables
##########################################


	proj = Pipeline::Config.instance.project
	proj.load_config()
	animDir = OS::Path.combine(proj.assets, "vehicles/anims")
	animOutDir = OS::Path.combine(proj.export, "levels/gta5/vehicles")
	vehicle_name = 'rapidGT2'

	config = Pipeline::Config.instance
	$p4 = SCM::Perforce.new()
	$p4.port = config.sc_server
	$p4.client = config.sc_workspace
	$p4.user = config.sc_username
	$p4.connect()

begin

	#pre packing cleanup 
	files = OS::FileUtilsEx::delete_files( OS::Path::combine( animOutDir, 'va_*.zip' ) )
	
	dirlist = OS::FindEx.find_dirs(animDir)
	dirlist.each do | dir |

		dirParts = OS::Path.get_directories( dir )
		vehName = dirParts[dirParts.length-1]
	
		icdInputs = Array.new()
		
		puts dir
		anim_files = OS::FindEx::find_files( OS::Path::combine( dir, '*.anim' ) )
		anim_files.each do |file|
			puts "Adding Anim: #{file}"
			 icdInputs <<  file 
		end

		clip_files =  OS::FindEx::find_files( OS::Path::combine( dir, '*.clip' ) )
		clip_files.each do |file|
			puts "Adding Clip: #{file}"
			icdInputs <<  file 
		end
		
		wholePath = OS::Path.combine(animOutDir, "va_"+vehName+".icd.zip")
		$p4.run_edit_or_add(wholePath)
		$p4.run_reopen('-t', 'ubinary', wholePath)
		modsZip = ProjectUtil::data_zip_create( wholePath, icdInputs, true)

	end

	if Pipeline::GUI::MessageBox::question( 'Build', 'Do you want to build the image or preview?' ) then
		rebuild = true
		preview = Pipeline::GUI::MessageBox::question( 'Preview/Image', 'Yes - preview, No - image' )
		if (preview) then
			files = OS::FindEx::find_files( OS::Path::combine( animOutDir, 'va_*.zip' ) )
			files.each do |file|
				puts "#{file}\n"
			end
			load_content = false
			ProjectUtil::data_convert_file( files, rebuild, preview, load_content)
		else
			ProjectUtil::data_convert_file( animOutDir, rebuild, preview)
		end
	end
	
rescue Exception => ex
	puts "Unhandled exception: #{ex.message}"
	GUI::ExceptionDialog::show_dialog( ex, 'Unhandled exception in vehicle animation resourcing,' )
end
