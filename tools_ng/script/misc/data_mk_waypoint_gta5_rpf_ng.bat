@ECHO OFF
REM
REM data_mk_waypoint_gta5_rpf.bat
REM Create the waypoint recordings RPF.
REM
REM David Muir <david.muir@rockstarnorth.com>
REM

CALL setenv.bat
SET FILTER=*.iwr
SET INPUT=x:\gta5\assets_ng\recordings\waypoints
SET OUTPUT=x:\gta5\assets_ng\export\levels\gta5
SET OUTPUT_FILE=%OUTPUT%\waypointrec.zip

PUSHD %INPUT%
p4 sync %INPUT%\...
POPD

PUSHD %OUTPUT%
p4 edit %OUTPUT_FILE%
POPD

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --filter=%FILTER% --output=%OUTPUT_FILE% %INPUT%
%RS_TOOLSCONVERT%  --branch dev_ng %OUTPUT_FILE%

PAUSE
