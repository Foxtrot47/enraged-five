@echo off
for /r %%d in (.) do (
	echo Directory %%d . . .
	pushd %%d
	for %%i in (*.zip) do (
		echo . Processing %%i . . .
		p4 edit %%i
		mkdir tmp_%%i
		cd tmp_%%i
		unzip ../%%i
		zip -X -r -m -n zip ../%%i *
		cd ..
		rd tmp_%%i
	)
	popd
)
