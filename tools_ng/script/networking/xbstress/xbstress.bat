@echo off
if "%DurangoXDK%" == "" GOTO NOXDK

:XDK
cd /d %DurangoXDK%\bin
xbstress %*
GOTO END

:NOXDK
echo No XDK detected in environment variable "DurangoXDK"
GOTO END

:END
pause