@echo off
if "%DurangoXDK%" == "" GOTO NOXDK

:XDK
cd /d %DurangoXDK%\bin
xbrun /o d:\setproxy.exe direct
GOTO END

:NOXDK
echo No XDK detected in environment variable "DurangoXDK"
GOTO END

:END
pause