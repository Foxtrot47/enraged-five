@echo off
if "%DurangoXDK%" == "" GOTO NOXDK

set IPADDRESS=""
set ip_address_string="IPv4 Address"
for /f "usebackq tokens=2 delims=:" %%f in (`ipconfig ^| findstr /c:%ip_address_string%`) do (
    set IPADDRESS=%%f
    goto SETUP
)

:SETUP
if "%IPADDRESS%" == "" GOTO NOIPADDRESS
if NOT EXIST %USERPROFILE%\Desktop\FiddlerRoot.cer GOTO NOFIDDLER

:XDK
cd /d %DurangoXDK%\bin
xbcp "%DurangoXDK%\bin\setproxy.exe" xd:\
xbcp "%DurangoXDK%\bin\certmgr.exe" xd:\
xbcp "%USERPROFILE%\Desktop\FiddlerRoot.cer" xd:\
xbrun /o d:\certmgr.exe -add -all d:\FiddlerRoot.cer -s -r localmachine root
xbrun /o d:\setproxy.exe manual %IPADDRESS%:8888
GOTO END

:NOFIDDLER
echo Could not find Fiddler root certificate:
echo 	%USERPROFILE%\Desktop\FiddlerRoot.cer
echo.
echo Open Fiddler and run Tools - Fiddler Options - HTTPS - Export Root Certificate to Desktop
echo.

GOTO END

:NOIPADDRESS
echo Could not detect IP address
GOTO END

:NOXDK
echo No XDK detected in environment variable "DurangoXDK"
GOTO END

:END
pause