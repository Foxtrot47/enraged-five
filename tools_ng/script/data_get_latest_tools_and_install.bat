@ECHO OFF
REM
REM File:: data_get_tools_and_install.bat
REM Description::
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 12 July 2011
REM

CALL setenv.bat
TITLE Syncing latest %RS_PROJECT% tools and running installer...

p4 sync %RS_TOOLSROOT%/...

CALL %RS_TOOLSROOT%\install.bat

PAUSE

REM data_get_tools_and_install.bat
