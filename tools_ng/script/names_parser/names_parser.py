import sys

if len(sys.argv) < 2:
	print ("\nMissing args. Usage is:\n   DLCNames_parser.py pack to search")
	print ("eg. DLCNames_parser.py mpStunt will search \n	X:\\gta5_dlc\\mpPacks\\mpStunt\\assets_ng\\gametext\\americanDLCNames.txt")
	sys.exit(0)

inputPath = sys.argv[1]

tagList = open(inputPath)
textTag = ''
iFoundTags = 0
outStringWithDescriptions = ''
outStringNoDescriptions = ''
outFileWithDescriptions = sys.argv[1]+'_WithDescriptions.csv'
outFileNoDescriptions = sys.argv[1]+'_TagsOnly.csv'

print '\nParsing\t',inputPath

tag = False

for tagRow in tagList:
	if (tag):
		outStringWithDescriptions += str(tagRow).replace(',','')
		tag = False
	if (tagRow.find('[') > -1):
		if (tagRow.find('DUMMYDLCNAMES') < 0):
			iFoundTags += 1
			tagName = tagRow[tagRow.find('[')+1:tagRow.find(']')]
			outStringWithDescriptions += str(tagName)+','
			outStringNoDescriptions += str(tagName)+'\n'
			#print tagName
			tag = True

out = open (outFileWithDescriptions,"w")			
out.write(outStringWithDescriptions)
out.close()
out = open (outFileNoDescriptions,"w")			
out.write(outStringNoDescriptions)
out.close()
		
print 'Found',iFoundTags,'tags'
print '\nTags and descriptions written to \t'+outFileWithDescriptions
print '\nTags WITHOUT descriptions written to \t'+outFileNoDescriptions