@ECHO OFF

REM -------------------------------------------------------------------------
REM Usage:
REM
REM   names_parser.bat
REM     This will prompt for a file path to process.
REM
REM   names_parser.bat mpApartment
REM     This is compatible with the original version and will attempt to
REM     process the pack "X:\gta5_dlc\mpPacks\%1\assets_ng\gametext\americanDLCNames.txt"
REM
REM   names_parser.bat <file>
REM     This will process the txt file passed in.
REM     This also would allow you to drag'n'drop a file path onto this
REM     batch script from File Explorer.
REM -------------------------------------------------------------------------

SETLOCAL
SET PATH=%PATH%;C:\Python27
SET PYTHONHOME=C:\Python27\
SET PYTHONPATH=C:\Python27\;C:\Python27\Lib
SET PYTHON=%PYTHONHOME%\python.exe
SET PIP=%PYTHONHOME%\scripts\pip.exe

SET DEFAULT_PACK_FILEPATH=X:\gta5_dlc\mpPacks\%1\assets_ng\gametext\americanDLCNames.txt

ECHO %GTA5_AMERICAN_DLC_NAMES%

PUSHD %~dp0

IF EXIST %PYTHON% (
    REM %PYTHON% --version
    REM %PIP% --version

    IF EXIST "%1" (
        ECHO Parameter exists as a file; will process "%1"
        CALL %PYTHON% .\names_parser.py "%1"
        GOTO :END
    )
    
    IF [%1]==[] (
        ECHO No parameter specified so we will prompt user.
        POWERSHELL .\names_parser_select_file.ps1

        GOTO :END
    )
    
    ECHO Parameter does not exist so assuming a MP Pack - %DEFAULT_PACK_FILEPATH%
    CALL %PYTHON% .\names_parser.py %DEFAULT_PACK_FILEPATH%
    GOTO :END

) ELSE (
    ECHO Python 2.7 not found. Aborting. 1>&2
    EXIT /B 1
)

:END
ECHO Cleaning up.
POPD

ENDLOCAL
