@ECHO OFF

CALL setenv.bat

REM SET INPUT=%RS_ASSETS%\audio\occlusion
REM SET OUTPUT=%RS_ASSETS%\export\audio\

REM DIR %INPUT%\*.xml /b > filelist.txt
REM PUSHD %INPUT%
REM %RS_TOOLSBIN%\ragebuilder_0378.exe %RS_TOOLSLIB%\util\ragebuilder\build_rpf.rbs -output %OUTPUT%\occlusion.rpf -build x:/gta5/build/dev -filelist %INPUT%\filelist.txt -shader x:/gta5/build/dev/common/shaders -shaderdb x:/gta5/build/dev/common/shaders/db -platform xenon bgscripts.xml donothing.xsc util_tfs_main01.xsc
REM POPD
REM DEL filelist.txt

SET INPUT=%RS_ASSETS%\export\audio\occlusion
SET OUTPUT=%RS_ASSETS%\export\audio\occlusion.zip

ECHO Building Audio Metadata Zip from  .pso.meta files
%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\data_mk_generic_zip.rb --project=%RS_PROJECT% --uncompressed --filter=*.meta --output=%OUTPUT% %INPUT%

ECHO Creating platform data
%RS_TOOLSCONVERT% %OUTPUT%

PAUSE

