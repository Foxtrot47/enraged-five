@echo off

echo Administrative permissions required. Detecting permissions...

net session >nul 2>&1
if %ERRORLEVEL% == 0 (
	echo Success: Administrative permissions confirmed.
	set RETURN_CODE=0
) else (
	echo Failure: Current permissions inadequate.
	set RETURN_CODE=1
)
exit /B %RETURN_CODE%

