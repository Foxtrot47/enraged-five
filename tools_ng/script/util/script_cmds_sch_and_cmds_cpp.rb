#
# Creates a formated list of commands that can be used to parse GTA script files using the old command format, swapping for the new commands 
#
# Author:: Thomas French
# Date:: 1 Oct 2008
#

#-----------------------------------------------------------------------------
# Function
#-----------------------------------------------------------------------------
#old command  - command previous to any change
#updated commands - commands that have char -> ped, car -> vehicle and vector change
#final commands -. had names standardised as well as updated

# changes from the old script commands to new: char -> ped, car -> vehicle, commmands return by value, format of commands has changed to be come standardied
#This uses 3 files to create the list, the GTA header file, the latest up to date header file and the relevamt user created commands audit file 
#Two sets of classes are created gta commands and the latest header files these are parsed form the relevant header files
#the old list then has all its char -ped substituions etc 
#this list is then run against the audited list (the audited list conatins of updated to final commands) to convert updated commands to final commands 
#The lis is chekced against the final command list (generated from our finla header file) any commands that dont mtach the final are maeked as being removed

REGEXP_UNDERLINE = (/_/)
REGEXP_COMMENT_LINES = /^[ \t]*\/\/.*/i

REGEXP_EXTRA_INFO = (/~XP/) 
REGEXP_PARAM_SWAP = (/~SP/)

REGEXP_REMOVE_CMD = (/~RC/)

REGEXP_LEGACY_CMD = (/~ac/i)

PED = "ped"
VEHICLE = "vehicle"

#these arrays should match in order
CPP_FILES = 	['X:\jimmy\src\dev\game\script\commands_audio.cpp',  
				'X:\jimmy\src\dev\game\script\commands_brains.cpp',
				'X:\jimmy\src\dev\game\script\commands_clock.cpp',
				'X:\jimmy\src\dev\game\script\commands_cutscene.cpp',
				'X:\jimmy\src\dev\game\script\commands_debug.cpp',  
				'X:\jimmy\src\dev\game\script\commands_fire.cpp',
				'X:\jimmy\src\dev\game\script\commands_gang.cpp',
				'X:\jimmy\src\dev\game\script\commands_graphics.cpp',
				'X:\jimmy\src\dev\game\script\commands_hud.cpp',
				'X:\jimmy\src\dev\game\script\commands_misc.cpp',  
				'X:\jimmy\src\dev\game\script\commands_object.cpp',
				'X:\jimmy\src\dev\game\script\commands_pad.cpp',
				'X:\jimmy\src\dev\game\script\commands_path.cpp',
				'X:\jimmy\src\dev\game\script\commands_ped.cpp',
				'X:\jimmy\src\dev\game\script\commands_player.cpp',
				'X:\jimmy\src\dev\game\script\commands_script.cpp',
				'X:\jimmy\src\dev\game\script\commands_task.cpp',
				'X:\jimmy\src\dev\game\script\commands_vehicle.cpp',  
				'X:\jimmy\src\dev\game\script\commands_weapon.cpp',
				'X:\jimmy\src\dev\game\script\commands_zone.cpp',
				'X:\jimmy\src\dev\game\script\commands_streaming.cpp',
				'X:\jimmy\src\dev_cam_commands\game\script\commands_camera.cpp'
				]

			
					
JIMMY_HEADER_FILES =['X:\jimmy\script\dev\native\include\commands_audio.sch',
					'X:\jimmy\script\dev\native\include\commands_brains.sch',
					'X:\jimmy\script\dev\native\include\commands_clock.sch',
					'X:\jimmy\script\dev\native\include\commands_cutscene.sch',
					'X:\jimmy\script\dev\native\include\commands_debug.sch',
					'X:\jimmy\script\dev\native\include\commands_fire.sch',
					'X:\jimmy\script\dev\native\include\commands_gang.sch',
					'X:\jimmy\script\dev\native\include\commands_graphics.sch',
					'X:\jimmy\script\dev\native\include\commands_hud.sch',
					'X:\jimmy\script\dev\native\include\commands_misc.sch',
					'X:\jimmy\script\dev\native\include\commands_object.sch',
					'X:\jimmy\script\dev\native\include\commands_pad.sch',
					'X:\jimmy\script\dev\native\include\commands_path.sch',
					'X:\jimmy\script\dev\native\include\commands_ped.sch',
					'X:\jimmy\script\dev\native\include\commands_player.sch',
					'X:\jimmy\script\dev\native\include\commands_script.sch',
					'X:\jimmy\script\dev\native\include\commands_task.sch',
					'X:\jimmy\script\dev\native\include\commands_vehicle.sch',
					'X:\jimmy\script\dev\native\include\commands_weapon.sch',
					'X:\jimmy\script\dev\native\include\commands_zone.sch',
					'X:\jimmy\script\dev\native\include\commands_streaming.sch',
					'X:\jimmy\script\dev_cam_commands\native\include\commands_camera.sch'
					
]				
			
#class that represents the commands structure, 
class ScriptCommand
	attr_accessor :funcname
	
	def initialize(name) #, oldparams, fncname)
		@funcname = name				#string function name
	end
	
end


def class_test_open_and_read_input_file (source_files, check_cpp_format)
data = []   #defines a hash object
func_type = []
		begin
			source_files.each do |inc_path|		#look thorugh all our data files and parse them
				File.open( inc_path ) do |fp|	#open each file at a time  parse
					fp.each do |line|
						# Skip blank lines
						line.chomp!
						next if ( 0 == line.size )
						
						#puts line
						
						#Skip comment lines
						next if( line =~ REGEXP_COMMENT_LINES)
						if check_cpp_format
							char_func =	(/([_]?([R][E][G][I][S][T][E][R])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(line)
							
							if not char_func.nil?
								tempdata = ( /([A-Z_0-9]+([\s]*))\,/.match(line) ) 
							end
						else
							tempdata = ( /([A-Z_0-9]+([\s]*))\(/.match(line) ) #extracts the command name from the header file as it has no space in the name uses the bracket to terminate
						end
							
						if not tempdata.nil? # we now have a command name	
							command_name = tempdata[1]
							command_name.chomp!
							command_name.strip!
						
							data << ScriptCommand.new(command_name)
							puts command_name + "$"
						end
					end	
				end
			end
	
		rescue Exception => ex
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure			
			#fp.close()
		end
	
	
	return data
end




#we  formatted list of up to date functions so that it can be parsed 
def create_output (update_command, new_command, update)
	

		if (update == true)		#fix this flag stoppping things getting printed
			command = update_command.funcname + " 	-> 	" +  new_command.funcname + " ~match"
		else
			command = update_command.funcname + " -> 	" +  new_command.funcname  + " ~nocpp "
		end
	
		
	return command 
end

# creates the list and writes it into a file 
def create_new_updated_list (cpp_cmd, sch_cmd, output_file, format_output )
	
	final_cmd_list = []
	
	begin
		sch_cmd.each_with_index do |sch_cmd_line, sch_cmd_index|
			altered = false
			included = false
			cpp_cmd.each_with_index do |cpp_cmd_line, cpp_cmd_index|
				#checks that the updated commands are the sasme as the final command 
				if (cpp_cmd_line.funcname.chomp.strip == sch_cmd_line.funcname.chomp.strip)
					final_cmd_list << create_output(sch_cmd_line, cpp_cmd_line, true)	#keep the char to ped substituition 
					altered = true	
				end
			end
			
			#include all commands including the removed ones
			if not altered
				final_cmd_list << create_output(sch_cmd_line, sch_cmd_line, false)
			end
		end
	
	
	
		begin 
			File.open( output_file , "w" ) do |fp|
				if not (format_output)
					final_cmd_list.each do |line|		
						fp.puts(line)			#use puts because the carrige returns have been removed by the chomp functions when formulating strings
					end
				else	
					final_cmd_list.each do |line|		
						if(line =~ /~nocpp/i) 
							fp.puts(line)			#place all the removed commands at the bottom
						end	
					end
					
					final_cmd_list.each do |line|		
						if not (line =~ /~nocpp/i) 
							if not (line =~ /~match/i) 
								fp.puts(line) 			#place all the removed commands at the bottom
							end
						end	
					end	
					
					final_cmd_list.each do |line|		
						if (line =~ /~match/i)  
							fp.puts(line)			#use puts because the carrige returns have been removed by the chomp functions when formulating strings
						end
					end
				end	

			end	
		end
	end
end	


#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
	sch_commands = []
	cpp_commands = []
	outputfile = 'X:\jimmy\bin\util\cpp_vs_sch_cmd_list.txt'
	
	run = 1
	
	while (run == 1)
	
			cpp_commands = class_test_open_and_read_input_file(CPP_FILES, true)
			sch_commands = class_test_open_and_read_input_file(JIMMY_HEADER_FILES, false)

			create_new_updated_list(cpp_commands, sch_commands, outputfile, true)
		run = 0	
		#end
	end
	
end


