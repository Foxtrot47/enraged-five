@ECHO OFF
REM 
REM Builds Release 32 bit max plugins.
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

call %RS_TOOLSROOT%\install.bat /switch

CALL setenv.bat
SET MAX_VERSION=2011
TITLE %RS_PROJECT% Build Max %MAX_VERSION% Debug Plugins
SET RAGE_DIR=%RS_PROJROOT%\src\dev\rage

PUSHD %RS_TOOLSLIB%\util
ruby build_max_plugins.rb --project=%RS_PROJECT% --branch=dev --dev --buildconfig="ToolReleaseMax2011|Win32" --buildfolder="current" --current_local_path=current/max2011/Plugins/ --dest_local_path=current/max2011/Plugins/ %RAGE_DIR%\base\tools\dcc\max\rexMax\rexMax_2008.sln %RS_TOOLSSRC%\dcc\max\3dsmax_Plugins_2008.sln
SET ERR=%ERRORLEVEL%
echo Errorlevel is %ERR%
POPD
EXIT %ERR%
REM BuildMaxPlugins.bat

