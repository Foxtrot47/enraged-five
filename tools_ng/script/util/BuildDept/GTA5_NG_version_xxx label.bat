::****************************************************************
:: GTA5 Create and Populate Version label 						**
:: Updated: 									28/03/2014		**
:: Edits:										Add DLC			**
:: Last edited by:							Ross McKinstray		**
::****************************************************************

@echo off
echo.

cd /d %~dp0

TITLE GTA5 label - Create and populate GTA5_NG_version_xxx label
ECHO GTA5 label - Create and populate GTA5_NG_version_xxx label

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

:: read version.txt in dev_build for label.
set ver=NUL
if exist X:\gta5\build\dev_ng\common\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\build\dev_ng\common\data\version.txt) DO (
		set ver=%%G
		GOTO SETLABEL
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAIN
)
	
:SETLABEL
	echo.
	echo Create and set new version label.
	echo.
	set verLabel=GTA5_NG_version_%ver%
	echo verLabel = %verLabel%
	echo ver = %ver%
	echo You are about to create a new label called %verLabel% is this right? 
	echo Close this window if not, or press a key to confirm...
	pause
	p4 label -o -t gta5ngtemplate %verLabel% | p4 label -i
	echo.
	pause
GOTO MAIN

:MAIN
echo.
echo UPDATING LABELLED BUILD
echo.

::Label GTA5 NG
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/export/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/GameText/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/maps/ParentTxds.xml
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/metadata/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/processed/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/textures/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/vehicles/...
P4 labelsync -l %verLabel% //depot/gta5/build/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/src/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/script/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/xlast/...
P4 labelsync -l %verLabel% //depot/gta5/tools_ng/...
P4 labelsync -l %verLabel% //rage/gta5/dev_ng/...

::Label DLC
P4 labelsync -l %verLabel% //gta5_dlc/mpPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/spPacks/...

::Label Liberty NG
P4 labelsync -l %verLabel% //gta5_liberty/build/dev_ng/...
P4 labelsync -l %verLabel% //gta5_liberty/Script/...
P4 labelsync -l %verLabel% //gta5_liberty/tools/...
POPD

pause