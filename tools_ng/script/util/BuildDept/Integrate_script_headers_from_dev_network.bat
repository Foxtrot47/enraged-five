@echo off

pushd %RS_CODEBRANCH%\game\script_headers

:: Integrate the code side script headers over to the script side
p4 integrate -i -d //depot/gta5/src/dev_network/game/script_headers/*.sch //depot/gta5/script/dev_network/core/common/native/*.sch

:: Merge the code changes with any scripter side edits.
p4 resolve -am //depot/gta5/script/dev_network/core/common/native/*.sch

:: revert any unchanged files & unwanted
p4 revert -a //depot/gta5/script/dev_network/core/common/native/*.sch

p4 revert //depot/gta5/script/dev_network/core/common/native/model_enums.sch
p4 revert //depot/gta5/script/dev_network/core/common/native/stack_sizes.sch
p4 revert //depot/gta5/script/dev_network/core/common/native/stats_enums.sch
p4 revert //depot/gta5/script/dev_network/core/common/native/weapon_enums.sch

del /F X:/gta5/script/dev_network/core/common/native/model_enums.sch
del /F X:/gta5/script/dev_network/core/common/native/stack_sizes.sch
del /F X:/gta5/script/dev_network/core/common/native/stats_enums.sch
del /F X:/gta5/script/dev_network/core/common/native/weapon_enums.sch

:: These files are in a different place.
p4 integrate -i -d //depot/gta5/src/dev_network/game/script_headers/stack_sizes.sch //depot/gta5/script/dev_network/core/game/data/stack_sizes.sch
p4 integrate -i -d //depot/gta5/src/dev_network/game/script_headers/weapon_enums.sch //depot/gta5/script/dev_network/core/game/data/weapon_enums.sch

:: Merge the code changes with any scripter side edits.
p4 resolve -am //depot/gta5/script/dev_network/core/game/data/*.sch

:: revert if unchanged
p4 revert -a //depot/gta5/script/dev_network/core/game/data/*.sch


pause