@echo off
echo.

cd /d %~dp0

:: checkout
p4 edit //depot/gta5/build/dev_ng/ps4/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/ps4/data/lang/american_REL.rpf
p4 edit //depot/gta5/build/dev_ng/x64/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/x64/data/lang/american_REL.rpf
p4 edit //depot/gta5/build/dev_ng/xboxone/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/xboxone/data/lang/american_REL.rpf


:: build
pushd X:\gta5\assets_ng\GameText\American\
	american.bat
popd
