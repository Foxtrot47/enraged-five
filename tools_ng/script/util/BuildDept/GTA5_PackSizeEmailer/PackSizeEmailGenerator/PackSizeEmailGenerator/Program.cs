﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace PackSizeEmailGenerator
{
    enum currentstate
    {
        PreFileBlock,
        FileBlock,
        PostFileBlock
    }

    internal static class Globals
    {
        internal static int NumberOfPrefixes { get; set; }
        internal static Dictionary<string, string> prefixes { get; set; }
        internal static string toolsBase = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
        internal static string prefixLocation = Path.Combine(toolsBase, "script\\util\\BuildDept\\GTA5_PackSizeEmailer\\prefixes.txt");
        internal static string tableTemplateLocation = Path.Combine(toolsBase, "script\\util\\BuildDept\\GTA5_PackSizeEmailer\\tabletemplate.html");
        internal static string rowTemplateLocation = Path.Combine(toolsBase, "script\\util\\BuildDept\\GTA5_PackSizeEmailer\\rowtemplate.html");
    }
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists(Globals.prefixLocation))
            {
                Globals.prefixes = Prefixgeneration.GeneratePrefixes(Globals.prefixLocation);
                string mailBody = "";
                foreach (string arg in args)
                {
                    MailHelper mailHelper = new MailHelper();

                    if (File.Exists(arg))
                    {
                        WriteMessage(string.Format("Checking file {0}", arg));
                        List<DataBlock> filesData = new List<DataBlock>();
                        string[] contents = File.ReadAllLines(arg);
                        currentstate curState = currentstate.PreFileBlock;
                        string patchname = "";
                        foreach (string line in contents)
                        {
                            switch (curState)
                            {
                                case currentstate.PreFileBlock:
                                    if (line.Trim().ToLower().StartsWith("# files"))
                                    {
                                        curState = currentstate.FileBlock;
                                    }
                                    if ((line.Trim().ToLower().StartsWith("name:")) && (patchname == ""))
                                    {
                                        patchname = line.Replace("Name:", string.Empty).Trim();
                                    }
                                    break;
                                case currentstate.FileBlock:
                                    DataBlock newData = new DataBlock();
                                    if (!string.IsNullOrWhiteSpace(line))
                                    {
                                        string[] lineParts = line.Split(new string[] { "Size:" }, StringSplitOptions.RemoveEmptyEntries);
                                        if (lineParts.Length != 2)
                                        {
                                            WriteError(string.Format("Line {0} does not have size component", Array.IndexOf(contents, line).ToString()));
                                            break;
                                        }

                                        newData.data = lineParts[0].Trim();
                                        newData.size = lineParts[1].Trim();

                                        filesData.Add(newData);
                                    }
                                    break;
                            }
                        }
                        if (patchname == "")
                        {
                            patchname = arg.Split(new char[] { '\\' }).Last().Replace("_ng.txt", string.Empty);
                        }

                        string newBody = mailHelper.GenerateLists(patchname, filesData);
                        mailBody = mailBody + newBody;
                    }
                }

                GenerateMail(mailBody);
            }
            else
            {
                WriteError(string.Format("File {0} with prefixes missing", Globals.prefixLocation));
            }
        }
        static void WriteError(string input)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(input);
        }

        static void WriteMessage(string input)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(input);
        }

        static void GenerateMail(string body)
        {
            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
            mailItem.To = "test@rockstarnorth.com";
            mailItem.Subject = "Test";
            mailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
            mailItem.HTMLBody = body;
            try { mailItem.Display(false); }
            catch (Exception e) { Console.WriteLine(e.InnerException.ToString()); }
        }
    }

    internal class MailHelper
    {
        internal string GenerateLists(string patchname, List<DataBlock> inputBlocks)
        {
            Dictionary<string, List<DataBlock>> outputBlocks = new Dictionary<string, List<DataBlock>>();

            foreach (string prefix in Globals.prefixes.Keys)
            {
                outputBlocks.Add(prefix, new List<DataBlock>());
            }
            outputBlocks.Add("other", new List<DataBlock>());

            foreach (DataBlock input in inputBlocks)
            {
                bool categorised = false;
                foreach (string regexString in Globals.prefixes.Keys)
                {
                    if (Regex.Match(input.data, regexString).Success)
                    {
                        categorised = true;
                        outputBlocks[regexString].Add(input);
                    }
                }
                if(!categorised)
                {
                    outputBlocks["other"].Add(input);
                }
            }

            return GenerateMailBody(patchname, outputBlocks);
        }

        internal string GenerateMailBody(string patchname, Dictionary<string, List<DataBlock>> togenerate)
        {
            string body = "<p><b>" + patchname + "</b></p><br/>";

            foreach(string key in togenerate.Keys)
            {
                if (togenerate[key].Count > 0)
                {
                    string tableContent = File.ReadAllText(Globals.tableTemplateLocation);
                    string rowTemplate = File.ReadAllText(Globals.rowTemplateLocation);
                    string rowContent = "";

                    List<DataBlock> sortedCurList = togenerate[key].OrderBy(x => long.Parse(x.size)).ToList();
                    sortedCurList.Reverse();

                    foreach (DataBlock curBlock in sortedCurList)
                    {
                        string newTemplate = rowTemplate;

                        newTemplate = newTemplate.Replace("[data]", curBlock.data).Replace("[value]", curBlock.size);

                        rowContent = rowContent + newTemplate;
                    }
                    string title = "";

                    if (key == "other")
                    {
                        title = "Other";
                    }
                    else
                    {
                        title = Globals.prefixes[key];
                    }
                    tableContent = tableContent.Replace("[title]", title).Replace("[rows]", rowContent) + "<br/>";

                    body = body + tableContent;
                }
            }

            return body;
        }
    }

    internal static class Prefixgeneration
    {
        internal static Dictionary<string, string> GeneratePrefixes(string inputFile)
        {
            Dictionary<string, string> output = new Dictionary<string, string>();

            string[] contents = File.ReadAllLines(inputFile);

            foreach(string line in contents)
            {
                string[] lineparts = line.Split(new char[] { ',' });
                if (lineparts.Length != 2)
                {
                    Console.WriteLine("Error.... line without comma seperated mapping in prefix");
                    continue;
                }
                output.Add(lineparts[0].Trim(), lineparts[1].Trim());

            }

            return output;
        }
    }
    class DataBlock
    {
        internal string data { get; set; }
        internal string size { get; set; }
    }
}
