set builddir=%RS_BUILDBRANCH%

copy %builddir%\game_durango_beta.cmp %builddir%\game_durango_beta_xboxone_data.cmp
copy %builddir%\game_durango_beta.exe %builddir%\game_durango_beta_xboxone_data.exe
copy %builddir%\game_durango_beta.pdb %builddir%\game_durango_beta_xboxone_data.pdb

copy %builddir%\game_durango_bankrelease.cmp %builddir%\game_durango_bankrelease_xboxone_data.cmp
copy %builddir%\game_durango_bankrelease.exe %builddir%\game_durango_bankrelease_xboxone_data.exe
copy %builddir%\game_durango_bankrelease.pdb %builddir%\game_durango_bankrelease_xboxone_data.pdb

copy %builddir%\game_durango_release.cmp %builddir%\game_durango_release_xboxone_data.cmp
copy %builddir%\game_durango_release.exe %builddir%\game_durango_release_xboxone_data.exe
copy %builddir%\game_durango_release.pdb %builddir%\game_durango_release_xboxone_data.pdb