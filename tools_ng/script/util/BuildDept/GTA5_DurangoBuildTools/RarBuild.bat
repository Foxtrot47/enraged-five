set currentdate=%DATE:~6,4%-%DATE:~3,2%-%DATE:~0,2%
set winrarpath=C:\Program Files\WinRAR\winrar.exe

if not exist "%winrarpath%" call :error "Can't find WinRar executable [%winrarpath%]"

set sourcepath=%1
set destinationpath=%2
set password=%3

del %2 /Q
mkdir %2

"%winrarpath%" a -hp%password% -r -ilograrerrors.txt -m1 -MT6 -v8g %destinationpath%\%currentdate%_DGO.rar %sourcepath%

:error
echo.
title Failed running WinRar %exetype%
echo Error launching WinRar!
echo.
echo. ^> %~1
echo.
pause
exit 1