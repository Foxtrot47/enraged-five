-rag
-rdrdebugcamera
-noautoload
-noprofilesignin

#Debug
-output
-logfile=XG:\gta5_durango_log.txt
-all_log=debug1
-all_tty=fatal

#Smoketest
//-runscript=fps_test
//-GPUTimers

#PIX capture
//-debugstart=-873.182617,-84.088509,37.868462,0.547063,0.822681,0.154656,-875.596069,-0.837092,0.537645,0.101072,-87.140129,-0.000000,-0.184754,0.982785,39.264812,20.000000

#Audio
//-noaudiohardware
//-noaudio

#Graphics
//-width=1920
//-height=1080
//-multisample=2