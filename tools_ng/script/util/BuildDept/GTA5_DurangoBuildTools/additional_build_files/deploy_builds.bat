@echo off

set builddir=%RS_BUILDBRANCH%
set toolpath=%DurangoXDK%\bin\
set fullpackagename=GTA-V_1.0.0.0_x64__zjr0dfhgjwvde

echo Ensuring command line contains boot to the hdd..

echo -forceboothdd >> new_v.txt
type build_commandlines.txt >> new_v.txt

echo WARNING, ABOUT TO REPLACE x:\v.txt WITH BUILD COMMAND LINES. 
echo MAKE A COPY IF YOU WISH TO KEEP THE CURRENT ONE BEFORE CONTINUING
pause

copy new_v.txt x:\v.txt
del new_v.txt

echo Updating loose folder..

del todeploy\rfs.dat
call ip_address.bat > todeploy\rfs.dat

echo Killing any currently running versions
"%toolpath%"\xbapp terminate %fullpackagename%

echo Deploying build...

"%toolpath%"\xbdel {%fullpackagename%}:\game_durango_bankrelease.exe
"%toolpath%"\xbdel {%fullpackagename%}:\game_durango_beta.exe
"%toolpath%"\xbdel {%fullpackagename%}:\game_durango_release.exe
"%toolpath%"\xbdel {%fullpackagename%}:\rfs.dat

"%toolpath%"\xbapp deploy todeploy /v
"%toolpath%"\xbapp debug %fullpackagename%

taskkill /F /IM rag.exe
taskkill /F /IM ragApp.exe
start .\tools\rag.exe

taskkill /F /IM systrayRfs.exe
start .\tools\systrayRfs.exe

echo Deploy complete
pause