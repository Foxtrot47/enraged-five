@echo off

set builddir=%RS_BUILDBRANCH%
set codedir=%RS_CODEBRANCH%
set toolsdir=%RS_TOOLSROOT%

set outputpath=%1

echo !!! Make sure you have built beta, bankrelease and release + the shaders, press any key to continue !!!
pause

echo Copying pdbs..

del %outputpath%\pdbs /Q
mkdir %outputpath%\pdbs 

copy %codedir%\game\VS_Project\Durango\Build\bankrelease\game_durango_bankrelease.pdb %outputpath%\pdbs\
copy %codedir%\game\VS_Project\Durango\Build\beta\game_durango_beta.pdb %outputpath%\pdbs\
copy %codedir%\game\VS_Project\Durango\Build\release\game_durango_release.pdb %outputpath%\pdbs\

echo Copying common data...

mkdir %outputpath%\todeploy\

robocopy %builddir%\common\data\ %outputpath%\todeploy\common\data\ /MIR /ZB /MT:6

del %outputpath%\todeploy\common\shaders\preload.list /Q
copy %builddir%\common\shaders\preload.list %outputpath%\todeploy\common\shaders\preload.list
robocopy %builddir%\common\shaders\durango\ %outputpath%\todeploy\common\shaders\durango\ /MIR /ZB /MT:6
robocopy %builddir%\common\shaders\db %outputpath%\todeploy\common\shaders\db\ /MIR /ZB /MT:6

echo Copying platform data...

REM robocopy %builddir%\x64\ %outputpath%\todeploy\x64\ /MIR /ZB /MT:6
REM robocopy %builddir%\update\ %outputpath%\todeploy\update\ /MIR /ZB /MT:6

robocopy %builddir%\xboxone\ %outputpath%\todeploy\xboxone /MIR /ZB /MT:6

robocopy %codedir%\game\VS_Project\Durango\Layout\Image\Loose %outputpath%\todeploy

echo Copying additional build files...

del %outputpath%\*.* /Q

copy %builddir%\xbo_scripts\ip_address.bat %outputpath%\ 
copy %builddir%\xbo_scripts\kill_on_console.bat %outputpath%\
copy %builddir%\xbo_scripts\provision_console.bat %outputpath%\
copy %builddir%\xbo_scripts\reboot_console.bat %outputpath%\
copy %builddir%\xbo_scripts\remove_from_console.bat %outputpath%\
copy %toolsdir%\script\util\BuildDept\GTA5_DurangoBuildTools\additional_build_files\*.*  %outputpath%\

echo Copying tools...

mkdir %outputpath%\tools\
del %outputpath%\tools\*.* /Q

taskkill /F /IM rag.exe
taskkill /F /IM ragApp.exe
taskkill /F /IM systrayRfs.exe

copy x:\gta5\tools_ng\bin\rag %outputpath%\tools\
copy x:\gta5\tools_ng\bin\SysTrayRfs.exe %outputpath%\tools\

echo "!!! Build Complete! Make sure the build run of a kit !!!"
pause