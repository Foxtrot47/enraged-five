::************************************************************************
:: GTA5 dev_Live TU prebuild copier													**
:: Updated: 				08/10/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		dev_Live TU Prebuid copier					**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkDataLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_TU_PreBuild_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\titleupdate\dev_Live
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\dev_Live_prebuild"

:: Set a lock file when data copy is in progress
if not exist %copydir% (
	mkdir %copydir%
)
echo locking dir > %copydir%\lock.txt
echo locking dir


echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating QA TU folder ## >> %outFile%

echo -- Started QA TU Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /S /PURGE %builddir%\dlc_patch\ %copydir%\dlc_patch\ *.*
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.xml
:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_eu_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_us_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_final.*

robocopy %builddir%\ %copydir%\ *.exe


echo GTA5 TU Data updated 
echo -- GTA5 TU Data updated --  >> %outFile%


::unlock
echo removing lock
erase /F /Q %copydir%\lock.txt


pause
exit
