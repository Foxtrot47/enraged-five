@ECHO OFF

cd /d %~dp0

TITLE %RS_PROJECT% GTA5: Getting spPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting spPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::
p4 sync //gta5_dlc/spPacks/*/*/Gametext/... 2>> %RS_TOOLSROOT%/logs/sp_dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/... 2>> %RS_TOOLSROOT%/logs/sp_dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/*.* 2>> %RS_TOOLSROOT%/logs/sp_dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::==========================================::



::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/sp_dlc_sync.txt
)


)
::---------------------------------------------------------------

pause


