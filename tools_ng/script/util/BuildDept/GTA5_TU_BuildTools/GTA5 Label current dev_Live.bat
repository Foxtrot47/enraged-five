::********************************************************
:: Labelling scripts for dev_Live							**
:: Last edited on: 				04/10/2013				**
:: Last edited by:			Ross McKinstray				**
:: Last changes:			Initial version 			**
::********************************************************

echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING LABELLED BUILD
echo.

P4 labelsync -l GTA5_network_demo //depot/gta5/assets/GameText/dev_Live/...
P4 labelsync -l GTA5_network_demo //depot/gta5/src/dev_Live/...
P4 labelsync -l GTA5_network_demo //depot/gta5/script/dev_Live/...
P4 labelsync -l GTA5_network_demo //depot/gta5/xlast/...
P4 labelsync -l GTA5_network_demo //rage/gta5/dev_Live/...
P4 labelsync -l GTA5_network_demo //depot/gta5/titleupdate/dev_Live/...
P4 labelsync -l GTA5_network_demo //gta5_dlc/mpPacks/mpLuxe2/...

POPD

pause