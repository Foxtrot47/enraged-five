::************************************************************************
:: GTA5 builder - Copy Ps3 Patches to Distribution - GTA5_TU_Prebuild	**
:: Updated: 				08/01/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0
:Ask
echo Would you like to copy Xbox360 Patches to the Distribution and X Directories ?(Y/N)
set INPUT=
set /P INPUT=Input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto no
echo Incorrect input & goto Ask

:yes
call %RS_TOOLSROOT%\bin\setenv.bat

rem Set the xdk environment variables
pushd %xedk%\bin\win32\
call xdkvars.bat
popd
xbcp HDD:\$TitleUpdate\545408A7\*.* X:\gta5\patches\xbox360\$TitleUpdate\545408A7\
echo Press any key to continue and copy patches to the distribution drive.
pause
robocopy X:\gta5\patches\xbox360\$TitleUpdate\545408A7\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Xbox_pkgs\$TitleUpdate\545408A7\ *.*
pause

:no
exit
