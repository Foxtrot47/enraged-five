::************************************************************
:: NewPrebBuildCode labelling scripts for TUs 				**
::	(for pre-prebuild syncing on multiple build machines)	**
:: Last edited on: 				17/08/2013					**
:: Last edited by:			Ross McKinstray					**
:: Last changes:			Initial version 				**
::************************************************************

echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING LABELLED BUILD
echo.

P4 labelsync -l GTA5_TU_NewPrebuildCode //depot/gta5/src/dev_network/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //depot/gta5/xlast/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //depot/gta5/tools/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //rage/gta5/dev_network/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //ps3sdk/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //depot/gta5/assets/GameText/TitleUpdate/...
P4 labelsync -l GTA5_TU_NewPrebuildCode //depot/gta5/script/dev_network/...
POPD

pause