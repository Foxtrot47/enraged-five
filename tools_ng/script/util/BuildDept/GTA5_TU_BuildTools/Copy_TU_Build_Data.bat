::************************************************************************
:: GTA5 QA TU copier													**
:: Updated: 				06/12/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		dlc_patch									**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkDataLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Network_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\titleupdate
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate"

:: Set a lock file when data copy is in progress
if not exist %copydir% (
	mkdir %copydir%
)
echo locking dir > %copydir%\lock.txt
echo locking dir


echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating QA TU folder ## >> %outFile%

echo -- Started QA TU Update -- >> %outFile%
robocopy /S /PURGE %builddir%\dev\common\ %copydir%\dev\common\ *.*
robocopy /S /PURGE %builddir%\dev\ps3\ %copydir%\dev\ps3\ *.*
robocopy /S /PURGE %builddir%\dev\xbox360\ %copydir%\dev\xbox360\ *.*
robocopy /S /PURGE %builddir%\dev\TROPDIR\ %copydir%\dev\TROPDIR\ *.*
robocopy /S /PURGE %builddir%\dev\dlc_patch\ %copydir%\dev\dlc_patch\ *.*
robocopy %builddir%\dev\ %copydir%\dev\ *.sfo
robocopy %builddir%\dev\ %copydir%\dev\ *.xml
:: -- Copy exes -- ::
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_psn_release_snc.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_psn_master_eu_snc.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_psn_master_us_snc.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_xenon_release.*
robocopy /PURGE %builddir%\dev\ %copydir%\dev\ game_xenon_final.*

robocopy %builddir%\dev\ %copydir%\dev\ *.exe


:: -- Japanese TU -- ::
robocopy /S /PURGE %builddir%\Japanese\common\ %copydir%\Japanese\common\ *.*
robocopy /S /PURGE %builddir%\Japanese\ps3\ %copydir%\Japanese\ps3\ *.*
robocopy /S /PURGE %builddir%\Japanese\xbox360\ %copydir%\Japanese\xbox360\ *.*
robocopy /S /PURGE %builddir%\Japanese\TROPDIR\ %copydir%\Japanese\TROPDIR\ *.*
robocopy /S /PURGE %builddir%\Japanese\dlc_patch\ %copydir%\Japanese\dlc_patch\ *.*
robocopy %builddir%\Japanese\ %copydir%\Japanese\ *.sfo
robocopy %builddir%\Japanese\ %copydir%\Japanese\ *.xml
:: -- Copy exes -- ::
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_psn_release_snc.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_psn_master_jp_snc.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_xenon_release.*
robocopy /PURGE %builddir%\Japanese\ %copydir%\Japanese\ game_xenon_final.*



echo GTA5 TU Data updated 
echo -- GTA5 TU Data updated --  >> %outFile%


::unlock
echo removing lock
erase /F /Q %copydir%\lock.txt


pause
exit
