::********************************************************************
:: GTA5 dev_Live Create and Populate Version label 					**
:: Updated: 								04/10/2013				**
:: Edits:									Initial version			**
:: Last edited by:							Ross McKinstray			**
::********************************************************************

@echo off
echo.

cd /d %~dp0

TITLE GTA5 dev_Live label - Create and populate GTA5_TitleUpdate_vxxx label
ECHO GTA5 dev_Live label - Create and populate GTA5_TitleUpdate_vxxx label

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

:: read version.txt for label.
set ver=NUL
if exist X:\gta5\titleupdate\dev_Live\common\data\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\dev_Live\common\data\version.txt) DO (
		set ver=%%G
		GOTO SETLABEL
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAIN
)
	
:SETLABEL
	echo.
	echo Create and set new version label.
	echo.
	set verLabel=GTA5_TitleUpdate_v%ver%
	echo verLabel = %verLabel%
	echo ver = %ver%
	echo You are about to create a new label called %verLabel% is this right? 
	echo Close this window if not, or press a key to confirm...
	pause
	p4 label -o -t gta5_TU_template %verLabel% | p4 label -i
	echo.
	pause
GOTO MAIN

:MAIN
echo.
echo UPDATING LABELLED BUILD
echo.

P4 labelsync -l %verLabel% //depot/gta5/assets/GameText/dev_Live/...
P4 labelsync -l %verLabel% //depot/gta5/src/dev_Live/...
P4 labelsync -l %verLabel% //depot/gta5/script/dev_Live/...
P4 labelsync -l %verLabel% //depot/gta5/xlast/...
P4 labelsync -l %verLabel% //depot/gta5/tools/...
P4 labelsync -l %verLabel% //rage/gta5/dev_Live/...
P4 labelsync -l %verLabel% //rage/gta5/Japanese_Live/...
P4 labelsync -l %verLabel% //ps3sdk/...
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/dev_Live/...
P4 labelsync -l %verLabel% //gta5_dlc/mpPacks/mpLuxe2/...
P4 labelsync -l %verLabel% //gta5_dlc/Packages/mpLuxe2/ps3/...
P4 labelsync -l %verLabel% //gta5_dlc/Packages/mpLuxe2/xbox360/...
POPD

pause
