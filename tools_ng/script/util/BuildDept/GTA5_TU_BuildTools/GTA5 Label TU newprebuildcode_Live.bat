::************************************************************
:: NewPrebBuildCode labelling scripts for TUs 				**
::	(for pre-prebuild syncing on multiple build machines)	**
:: Last edited on: 				17/08/2013					**
:: Last edited by:			Ross McKinstray					**
:: Last changes:			Initial version 				**
::************************************************************

echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING LABELLED BUILD
echo.

P4 labelsync -l GTA5_TU_NewPrebuildCode_Live //depot/gta5/src/dev_Live/...
P4 labelsync -l GTA5_TU_NewPrebuildCode_Live //depot/gta5/xlast/...
P4 labelsync -l GTA5_TU_NewPrebuildCode_Live //rage/gta5/dev_Live/...
P4 labelsync -l GTA5_TU_NewPrebuildCode_Live //ps3sdk/...
POPD

pause