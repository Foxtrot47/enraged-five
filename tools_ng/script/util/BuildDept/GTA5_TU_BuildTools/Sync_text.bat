::************************************************************************
:: GTA5 builder - Check out dev_network 360 exes - Build Tools			**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@echo off
if "%1" neq "" goto :GrabType
break>>X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt

:GrabType
set INPUT=
set INPUT=%1

set Forced=
Set BUILDDIR=""
Goto :SearchBaseType

:SearchBaseType
SET searchValue1=Force
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
IF ERRORLEVEL 1 ( 
set Forced=
) ELSE ( 
set Forced=-f
)


SET searchValue1=DevNetwork
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( 
	SET BUILDDIR=dev_live
) ELSE ( 
	SET BUILDDIR=Dev_Network
)

SET searchValue1=DataDevNetwork
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( 
	Echo .
) ELSE ( 
	SET BUILDDIR=Dev
)

SET searchValue1=TextDevNetwork
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( 
	Echo .
) ELSE ( 
	SET BUILDDIR=TitleUpdate
)


Echo %Forced%%BUILDDIR%
Goto :Grab




:Grab
SET searchValue1=Text
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) ELSE ( 
	@echo on
	p4 sync %Forced%  //depot/gta5/assets/GameText/%BUILDDIR%/...
	p4 sync %Forced%  //depot/gta5/assets/GameText/scripts/...
	p4 changes -m 1 -s submitted //depot/gta5/assets/GameText/%BUILDDIR%/... >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% //depot/gta5/assets/GameText/%BUILDDIR%/
	Goto :quit
)

SET searchValue1=Script
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else (
	@echo on
	p4 sync %Forced%  //depot/gta5/script/%BUILDDIR%/...
	p4 changes -m 1 -s submitted //depot/gta5/script/%BUILDDIR%/... >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% //depot/gta5/script/%BUILDDIR%/
	Goto :quit
)


SET searchValue1=Data
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else (
	@echo on
	p4 sync %Forced%  //depot/gta5/titleupdate/%BUILDDIR%/...
	p4 changes -m 1 -s submitted //depot/gta5/titleupdate/%BUILDDIR%/... >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% //depot/gta5/titleupdate/%BUILDDIR%/
	Goto :quit
)

SET searchValue1=JPNDevNetwork
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else ( 
	@echo on
	p4 sync %Forced%  //depot/gta5/titleupdate/Japanese/...
	@echo off
	Echo %Forced% //depot/gta5/titleupdate/Japanese/
	Goto :quit
)
	
SET searchValue1=JPN
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else ( 
	@echo on
	p4 sync %Forced%  //depot/gta5/titleupdate/Japanese_Live/...
	@echo off
	Echo %Forced% //depot/gta5/titleupdate/Japanese_Live/
	Goto :quit
)
	
	
SET searchValue1=Source
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else ( 
	@echo on
	p4 sync %Forced%  //depot/gta5/src/%BUILDDIR%/...
	
	p4 changes -m 1 -s submitted //depot/gta5/src/%BUILDDIR%/... >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% //depot/gta5/src/%BUILDDIR%/
	Goto :quit
)

SET searchValue1=Xlast
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
if ERRORLEVEL 1 ( Echo .
) else ( 
	@echo on
	p4 sync %Forced%  //depot/gta5/xlast/
	@echo off
	Echo %Forced% //depot/gta5/xlast/
	Goto :quit
)


SET searchValue1=Rage
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
IF ERRORLEVEL 1 ( Echo .
) ELSE ( 
	@echo on
	p4 sync %Forced%  //rage/gta5/%BUILDDIR%/...
	p4 changes -m 1 -s submitted //rage/gta5/%BUILDDIR%/... >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% //depot/rage/gta5/%BUILDDIR%/
	Goto :quit
)

SET searchValue1=All
SET INPUT|FINDSTR /b "INPUT="|FINDSTR /i %searchValue1% >nul
IF ERRORLEVEL 1 ( Echo .  
) ELSE ( 
	p4 sync %Forced% //rage/gta5/dev_live/...
	p4 sync %Forced% //depot/gta5/src/dev_live/...
	p4 sync %Forced% //depot/gta5/titleupdate/dev_live/...
	p4 sync %Forced% //depot/gta5/script/dev_live/...
	p4 sync %Forced% //depot/gta5/assets/GameText/dev_live/...
	Goto :quit
)

:quit
