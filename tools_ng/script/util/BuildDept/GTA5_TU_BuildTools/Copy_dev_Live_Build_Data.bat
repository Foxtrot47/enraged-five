::************************************************************************
:: GTA5 QA TU copier													**
:: Updated: 				06/12/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		dev_Live									**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkDataLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Network_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\titleupdate\dev_Live
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\dev_Live"
set jpnbuilddir=X:\gta5\titleupdate\Japanese_Live
set jpncopydir="N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Japanese_Live"


:: Set a lock file when data copy is in progress
if not exist %copydir% (
	mkdir %copydir%
)
echo locking dir > %copydir%\lock.txt
echo locking dir


echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating QA TU folder ## >> %outFile%

echo -- Started QA TU Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /S /PURGE %builddir%\dlc_patch\ %copydir%\dlc_patch\ *.*
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.xml
:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_eu_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_us_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_final.*

robocopy %builddir%\ %copydir%\ *.exe


:: -- Japanese TU -- ::
robocopy /S /PURGE %jpnbuilddir%\common\ %jpncopydir%\common\ *.*
robocopy /S /PURGE %jpnbuilddir%\ps3\ %jpncopydir%\ps3\ *.*
robocopy /S /PURGE %jpnbuilddir%\xbox360\ %jpncopydir%\xbox360\ *.*
robocopy /S /PURGE %jpnbuilddir%\TROPDIR\ %jpncopydir%\TROPDIR\ *.*
robocopy /S /PURGE %jpnbuilddir%\dlc_patch\ %jpncopydir%\dlc_patch\ *.*
robocopy %jpnbuilddir%\ %jpncopydir%\ *.sfo
robocopy %jpnbuilddir%\ %jpncopydir%\ *.xml
:: -- Copy exes -- ::
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_psn_beta_snc.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_psn_release_snc.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_psn_master_jp_snc.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_xenon_beta.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_xenon_bankrelease.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_xenon_release.*
robocopy /PURGE %jpnbuilddir%\ %jpncopydir%\ game_xenon_final.*



echo GTA5 dev_Live Data updated 
echo -- GTA5 dev_Live Data updated --  >> %outFile%


::unlock
echo removing lock
erase /F /Q %copydir%\lock.txt


pause
exit
