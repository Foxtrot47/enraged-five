::************************************************************************
:: GTA5 builder - Copy Ps3 Patches to Distribution - GTA5_TU_Prebuild	**
:: Updated: 				22/12/2014						            **
:: Edits: 					Heistsagain									**
:: Last edited by:			Ross McK	  				            	**
::************************************************************************

@Echo off
TITLE %~nx0
:Ask
echo Would you like to copy 1.26 PS3 Patches to the Distribution drive ?(Y/N)
set INPUT=
set /P INPUT=Input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto no
echo Incorrect input & goto Ask

:yes
robocopy X:\gta5\patches\ps3\EU\01.26\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\ EP1004-BLES01807_00-GTAVTITLEUPDATER-A0126-V0100.pkg
robocopy X:\gta5\patches\ps3\EU\01.26_HDD\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\ EP1004-NPEB01283_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
robocopy X:\gta5\patches\ps3\JAPAN\01.22_HDD\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\ JP0230-NPJB00516_00-GTAVDIGITALDOWNL-A0122-V0100.pkg
robocopy X:\gta5\patches\ps3\JAPAN\02.23\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\ JP0230-BLJM61019_00-GTAVTITLEUPDATER-A0223-V0200.pkg
robocopy X:\gta5\patches\ps3\US\01.26\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\ UP1004-BLUS31156_00-GTAVTITLEUPDATER-A0126-V0100.pkg
robocopy X:\gta5\patches\ps3\US\01.26_HDD\ N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\ UP1004-NPUB31154_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
pause

:no
exit

::N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\
::X:\gta5\patches\ps3\EU\01.12\EP1004-BLES01807_00-GTAVTITLEUPDATER-A0110-V0100.pkg
::X:\gta5\patches\ps3\EU\01.12_HDD\EP1004-NPEB01283_00-GTAVDIGITALDOWNL-A0110-V0100.pkg

::N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\
::X:\gta5\patches\ps3\JAPAN\01.06_HDD\JP0230-NPJB00516_00-GTAVDIGITALDOWNL-A0105-V0100.pkg
::X:\gta5\patches\ps3\JAPAN\02.07\JP0230-BLJM61019_00-GTAVTITLEUPDATER-A0206-V0200.pkg

::N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\
::X:\gta5\patches\ps3\US\01.12\UP1004-BLUS31156_00-GTAVTITLEUPDATER-A0110-V0100.pkg
::X:\gta5\patches\ps3\US\01.12_HDD\UP1004-NPUB31154_00-GTAVDIGITALDOWNL-A0110-V0100.pkg