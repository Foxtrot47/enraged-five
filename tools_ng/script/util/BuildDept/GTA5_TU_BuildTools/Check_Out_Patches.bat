::************************************************************************
:: GTA5 builder - Check out patch files - Build Tools   				**
:: Updated: 				18/02/2014	    					        **
:: Edits: 					Heists 	    								**
:: Last edited by:			Gemma Mc	           						**
::************************************************************************
@echo off
TITLE %~nx0
@echo on
p4 edit //depot/gta5/patches/ps3/EU/01.26/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/EU/01.26/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/EU/01.26/EP1004-BLES01807_00-GTAVTITLEUPDATER-A0126-V0100.pkg
p4 edit //depot/gta5/patches/ps3/EU/01.26/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/ps3/EU/01.26_HDD/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/EU/01.26_HDD/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/EU/01.26_HDD/EP1004-NPEB01283_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
p4 edit //depot/gta5/patches/ps3/EU/01.26_HDD/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/ps3/JAPAN/01.22_HDD/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/JAPAN/01.22_HDD/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/JAPAN/01.22_HDD/JP0230-NPJB00516_00-GTAVDIGITALDOWNL-A0122-V0100.pkg
p4 edit //depot/gta5/patches/ps3/JAPAN/01.22_HDD/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/ps3/JAPAN/02.23/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/JAPAN/02.23/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/JAPAN/02.23/JP0230-BLJM61019_00-GTAVTITLEUPDATER-A0223-V0200.pkg
p4 edit //depot/gta5/patches/ps3/JAPAN/02.23/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/ps3/US/01.26/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/US/01.26/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/US/01.26/UP1004-BLUS31156_00-GTAVTITLEUPDATER-A0126-V0100.pkg
p4 edit //depot/gta5/patches/ps3/US/01.26/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/ps3/US/01.26_HDD/build/USRDIR/EBOOT.BIN
p4 edit //depot/gta5/patches/ps3/US/01.26_HDD/build/USRDIR/update.rpf
p4 edit //depot/gta5/patches/ps3/US/01.26_HDD/UP1004-NPUB31154_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
p4 edit //depot/gta5/patches/ps3/US/01.26_HDD/build/TROPDIR/NPWR03885_00/TROPHY.TRP
::************************************************************************
p4 edit //depot/gta5/patches/xbox360/$TitleUpdate/545408A7/tu11000001_00000000
p4 edit //depot/gta5/patches/xbox360/$TitleUpdate/545408A7/tu21000001_00000000
p4 edit //depot/gta5/patches/xbox360/$TitleUpdate/545408A7/tu31000001_00000000
p4 edit //depot/gta5/patches/xbox360/$TitleUpdate/545408A7/tu41000001_00000000
::************************************************************************
p4 edit //depot/gta5/patches/xbox360/JPN/545408A7.cab
p4 edit //depot/gta5/patches/xbox360/JPN/update/disc001/default.xex
p4 edit //depot/gta5/patches/xbox360/JPN/update/disc001/update.rpf
p4 edit //depot/gta5/patches/xbox360/JPN/update/disc002/default.xex
p4 edit //depot/gta5/patches/xbox360/JPN/update/disc002/update.rpf
::************************************************************************
p4 edit "//depot/gta5/patches/xbox360/JPN - GonD/545408A7.cab"
p4 edit "//depot/gta5/patches/xbox360/JPN - GonD/update/default.xex"
p4 edit "//depot/gta5/patches/xbox360/JPN - GonD/update/update.rpf"
::************************************************************************
p4 edit "//depot/gta5/patches/xbox360/W - GonD/545408A7.cab"
p4 edit "//depot/gta5/patches/xbox360/W - GonD/update/default.xex"
p4 edit "//depot/gta5/patches/xbox360/W - GonD/update/update.rpf"
::************************************************************************
p4 edit //depot/gta5/patches/xbox360/W/545408A7.cab
p4 edit //depot/gta5/patches/xbox360/W/update/disc001/default.xex
p4 edit //depot/gta5/patches/xbox360/W/update/disc001/update.rpf
p4 edit //depot/gta5/patches/xbox360/W/update/disc002/default.xex
p4 edit //depot/gta5/patches/xbox360/W/update/disc002/update.rpf

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out the patch files!
	pause
) ELSE (
	echo patch files have been checked out successfully !
)
pause