::************************************************************************
:: GTA5 builder - 			Build DLC Text		 - GTA5_TU_Prebuild		**
:: Updated: 				22\12\2014						            **
:: Edits: 					Heists							            **
:: Last edited by:			Ross McK	 				            	**
::************************************************************************
@Echo off
TITLE %~nx0
@Echo on
::**************************************************************************
cd /d %~dp0

::%SystemRoot%\explorer.exe "X:\gta5\titleupdate\Japanese_Live\xbox360\patch\data\lang\"
::%SystemRoot%\explorer.exe "X:\gta5\titleupdate\Japanese_Live\ps3\patch\data\lang\"
::%SystemRoot%\explorer.exe "X:\gta5\titleupdate\Japanese_Live\ps3\patch\levels\gta5\script\"
::%SystemRoot%\explorer.exe "X:\gta5\titleupdate\Japanese_Live\xbox360\patch\levels\gta5\script\"

echo WARNING: Please make sure the Update.RPF and executable are correct for each sku then press any key to continue...
Pause

start X:\gta5\patches\ps3\EU\01.26\buildEUR.bat
start X:\gta5\patches\ps3\EU\01.26_HDD\buildEUR.bat
start X:\gta5\patches\ps3\JAPAN\01.22_HDD\buildJPN.bat
start X:\gta5\patches\ps3\JAPAN\02.23\buildJPN.bat
start X:\gta5\patches\ps3\US\01.26\buildUS.bat
start X:\gta5\patches\ps3\US\01.26_HDD\buildUS.bat

%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\EU\01.26\"
%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\EU\01.26_HDD\"
%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\JAPAN\01.22_HDD\"
%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\JAPAN\02.23\"
%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\US\01.26\"
%SystemRoot%\explorer.exe "X:\gta5\patches\ps3\US\01.26_HDD\"
pause