::****************************************************************
:: GTA5 Symbol Backup batch file								**
:: Updated: 				11/11/2013							**
:: Last edited by:			Ross McKinstray						**
:: Last edits:				Switch Japanese to Japanese_Live	**
::****************************************************************
@echo off
echo.
cd /d %~dp0

set copydir=N:\RSGEDI\Distribution\QA_Build\gta5
set builddir=X:\gta5\titleupdate\dev_Live
set path="C:\Program Files (x86)\WinRAR\";%path%

set ver=NUL
FOR /F "eol=# skip=3" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO MAIN
	
:MAIN

if exist %copydir%\PS3_Symbols\Version%ver%_US (
	echo This version folder already exists, please manually back up these symbols!
) ELSE (
	echo Copying symbols to the network
	mkdir "%copydir%/PS3_Symbols/Version%ver%_US"
	rar a %copydir%/PS3_Symbols/Version%ver%_US/symbols.rar c:\spu_debug\*.elf
	rar a %copydir%/PS3_Symbols/Version%ver%_US/symbols.rar %builddir%\*.self
)




pause

