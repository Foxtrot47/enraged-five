@echo off
echo.

cd /d %~dp0

p4 edit //depot/gta5/titleupdate/dev/ps3/patch/data/lang/american.rpf
p4 edit //depot/gta5/titleupdate/dev/ps3/patch/data/lang/american_rel.rpf
p4 edit //depot/gta5/titleupdate/dev/xbox360/patch/data/lang/american.rpf
p4 edit //depot/gta5/titleupdate/dev/xbox360/patch/data/lang/american_rel.rpf
p4 edit //depot/gta5/titleupdate/dev/x64/patch/data/lang/american.rpf
p4 edit //depot/gta5/titleupdate/dev/x64/patch/data/lang/american_rel.rpf

pushd X:\gta5\assets\GameText\TitleUpdate\American\
	american.bat
popd

