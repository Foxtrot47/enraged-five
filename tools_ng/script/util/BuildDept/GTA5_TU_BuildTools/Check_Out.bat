::************************************************************************
:: GTA5 builder - Check out dev_network ps3 exes - Build Tools   		**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************
@Echo off
TITLE %~nx0
echo %1% 
echo %2%
::Scripts
IF "%1%"=="CB_Default" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev_Live/xbox360/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/dev_Live/xbox360/patch/levels/gta5/script/script_rel.rpf
				p4 edit //depot/gta5/titleupdate/dev_Live/ps3/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/dev_Live/ps3/patch/levels/gta5/script/script_rel.rpf
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev/xbox360/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/dev/xbox360/patch/levels/gta5/script/script_rel.rpf
				p4 edit //depot/gta5/titleupdate/dev/ps3/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/dev/ps3/patch/levels/gta5/script/script_rel.rpf
			@echo off
		)
)



IF "%1%"=="CB_Clifford" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/ps3/levels/gta5/script/script_DEBUG.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/ps3/levels/gta5/script/script_RELEASE.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/xbox360/levels/gta5/script/script_DEBUG.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/xbox360/levels/gta5/script/script_RELEASE.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets/export/levels/gta5/script/script_RELEASE.zip
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets/export/levels/gta5/script/script_DEBUG.zip
			@echo off
		) ELSE (
			@echo on
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/ps3/levels/gta5/script/script_DEBUG.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/ps3/levels/gta5/script/script_RELEASE.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/xbox360/levels/gta5/script/script_DEBUG.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/build/dev/xbox360/levels/gta5/script/script_RELEASE.rpf
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets/export/levels/gta5/script/script_RELEASE.zip
				p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets/export/levels/gta5/script/script_DEBUG.zip
			@echo off
		)
)


IF "%1%"=="CB_Japanese" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese_Live/xbox360/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/Japanese_Live/xbox360/patch/levels/gta5/script/script_rel.rpf
				p4 edit //depot/gta5/titleupdate/Japanese_Live/ps3/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/Japanese_Live/ps3/patch/levels/gta5/script/script_rel.rpf
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese/xbox360/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/Japanese/xbox360/patch/levels/gta5/script/script_rel.rpf
				p4 edit //depot/gta5/titleupdate/Japanese/ps3/patch/levels/gta5/script/script.rpf
				p4 edit //depot/gta5/titleupdate/Japanese/ps3/patch/levels/gta5/script/script_rel.rpf
			@echo off
		)
)

:: Exes
IF "%1%"=="CB_PS3" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_release_snc.sym
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_release_snc.self
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_release_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_us_snc.sym
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_us_snc.self
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_us_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_eu_snc.sym
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_eu_snc.self
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_master_eu_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_beta_snc.sym
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_beta_snc.self
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_beta_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_bankrelease_snc.sym
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_bankrelease_snc.self
				p4 edit //depot/gta5/titleupdate/dev_Live/game_psn_bankrelease_snc.cmp
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev/game_psn_release_snc.sym
				p4 edit //depot/gta5/titleupdate/dev/game_psn_release_snc.self
				p4 edit //depot/gta5/titleupdate/dev/game_psn_release_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev/game_psn_beta_snc.sym
				p4 edit //depot/gta5/titleupdate/dev/game_psn_beta_snc.self
				p4 edit //depot/gta5/titleupdate/dev/game_psn_beta_snc.cmp
				p4 edit //depot/gta5/titleupdate/dev/game_psn_bankrelease_snc.sym
				p4 edit //depot/gta5/titleupdate/dev/game_psn_bankrelease_snc.self
				p4 edit //depot/gta5/titleupdate/dev/game_psn_bankrelease_snc.cmp
			@echo off
		)
)

IF "%1%"=="CB_XBOX" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.exe
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.map
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.pdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.xdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_final.xex
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.exe
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.map
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.pdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.xdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_release.xex
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.cmp
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.exe
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.map
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.pdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.xdb
				p4 edit //depot/gta5/titleupdate/dev_Live/game_xenon_bankrelease.xex
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.cmp
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.exe
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.map
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.pdb
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.xdb
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_release.xex
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.cmp
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.exe
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.map
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.pdb
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.xdb
				p4 edit //depot/gta5/titleupdate/dev/game_xenon_bankrelease.xex
			@echo off
		)
)

IF "%1%"=="CB_PS3_JPN" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_release_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_release_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_release_snc.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_master_jp_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_master_jp_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_master_jp_snc.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_beta_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_beta_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_beta_snc.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_bankrelease_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_bankrelease_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_psn_bankrelease_snc.cmp
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_release_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_release_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_release_snc.cmp
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_beta_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_beta_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_beta_snc.cmp
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_bankrelease_snc.sym
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_bankrelease_snc.self
				p4 edit //depot/gta5/titleupdate/Japanese/game_psn_bankrelease_snc.cmp
			@echo off
		)
)

IF "%1%"=="CB_XBOX_JPN" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.exe
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.map
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.pdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.xdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_final.xex
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.exe
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.map
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.pdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.xdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_release.xex
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.cmp
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.exe
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.map
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.pdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.xdb
				p4 edit //depot/gta5/titleupdate/Japanese_Live/game_xenon_bankrelease.xex
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.cmp
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.exe
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.map
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.pdb
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.xdb
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_release.xex
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.cmp
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.exe
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.map
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.pdb
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.xdb
				p4 edit //depot/gta5/titleupdate/Japanese/game_xenon_bankrelease.xex
			@echo off
		)
)
:: Version
IF "%1%"=="CB_Version" (
		IF "%2%"=="False" (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev_Live/common/data/version.txt
			
			@echo off
		) ELSE (
			@echo on
				p4 edit //depot/gta5/titleupdate/dev/common/data/version.txt
			@echo off
		)
)


IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out %1% files!
	pause
	EXIT 
) 

