::************************************************************************
:: GTA5 builder - Copy Ps3 Patches to Distribution - GTA5_TU_Prebuild	**
:: Updated: 				12\12\2014	            					**
:: Edits: 					Heists	 	           						**
:: Last edited by:			Graham Rust            						**
::************************************************************************

@Echo off
TITLE %~nx0
:Ask
echo Would you like to copy the current gen DLC to Distro or QA Distoro directories  ?(D/Q)
set INPUT=
set /P INPUT=Input: %=%
IF /I "%INPUT%"=="d" goto Distro
IF /I "%INPUT%"=="q" goto DistroQA
echo Incorrect input & goto Ask

:Distro
robocopy X:\gta5_dlc\Packages\mpLuxe2\xbox360\bin\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\xbox\Content\0000000000000000\545408A7\00000002\ 545408A70CCFDF12
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\JPN JP0230-BLJM61019_00-GTAVDLCMPLUXEPT2.pkg
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\EU EP1004-BLES01807_00-GTAVDLCMPLUXEPT2.pkg
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\US UP1004-BLUS31156_00-GTAVDLCMPLUXEPT2.pkg
pause
exit

:DistroQA
robocopy X:\gta5_dlc\Packages\mpLuxe2\xbox360\bin\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\PreBuild\xbox\Content\0000000000000000\545408A7\00000002\ 545408A70CCFDF12
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\PreBuild\ps3\JPN JP0230-BLJM61019_00-GTAVDLCMPLUXEPT2.pkg
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\PreBuild\ps3\EU EP1004-BLES01807_00-GTAVDLCMPLUXEPT2.pkg
robocopy X:\gta5_dlc\Packages\mpLuxe2\ps3\pkgs\ N:\RSGEDI\Distribution\QA_Build\gta5\dlc\PreBuild\ps3\US UP1004-BLUS31156_00-GTAVDLCMPLUXEPT2.pkg
pause
exit

