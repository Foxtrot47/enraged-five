::****************************************************************
:: GTA5 builder - Grab prebuild code - GTA5_TU_NewPrebuildCode	**
:: Updated: 				24/08/2013							**
:: Edits: 					Activate this oen for use			**
:: Last edited by:			Ross McKinstray						**
::****************************************************************

@echo off
echo.

TITLE GTA5 TU NEWPREBUILDCODE LIVE - For build team use
ECHO GTA5 TU NEWPREBUILDCODE LIVE - For build team use	

rem Set the xdk environment variables
pushd %xedk%\bin\win32\
call xdkvars.bat
xbreboot
popd



echo kill rag & systray
tasklist | find /I "rag.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM rag.exe /T /F
)
tasklist | find /I "ragApp.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM ragApp.exe /T /F
)
tasklist | find /I "systrayrfs.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM systrayrfs.exe /T /F
)

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

if not exist %RS_TOOLSROOT%/logs (
	mkdir %RS_TOOLSROOT%/logs
)


echo ###################################################
echo ### Syncing GTA5_TU_NewPrebuildCode_Live game and rage code ###
echo ###################################################
p4 sync //depot/gta5/src/dev_Live/...@GTA5_TU_NewPrebuildCode_Live 2> %RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during game src code grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
	pause
) ELSE (
	echo Game code grabbed successfully.
)

echo.
p4 sync //rage/gta5/dev_Live/...@GTA5_TU_NewPrebuildCode_Live 2>> %RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during rage src code grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
	pause
) ELSE (
	echo Rage code grabbed successfully.
)

echo.
p4 sync //depot/gta5/xlast/...@GTA5_TU_NewPrebuildCode_Live 2>> %RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during xlast grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/TU_prebuild_code_sync.txt
	pause
) ELSE (
	echo Xlast grabbed successfully.
)


pause
exit





