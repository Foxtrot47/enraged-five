::************************************************************************
:: GTA5 builder - Copy Ps3 Patches to Distribution - GTA5_TU_Prebuild	**
:: Updated: 				12/12/2013						            **
:: Edits: 					Heist								        **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0
:Ask
echo Would you like to create a new FTP for the dlc Directories ?(L/N/C)
set INPUT=
set /P INPUT=Input: %=%
If /I "%INPUT%"=="l" goto live
If /I "%INPUT%"=="n" goto net
If /I "%INPUT%"=="c" goto no
echo Incorrect input & goto Ask

:live
call %RS_TOOLSROOT%\bin\setenv.bat

robocopy /E X:\FTP\Template X:\FTP\Template_Copy *.*
robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\EU\ X:\FTP\Template_Copy\dlc\ps3 EP1004-BLES01807_00-GTAVDLCMPLUXEPT2.pkg
robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\JPN\ X:\FTP\Template_Copy\dlc\ps3 JP0230-BLJM61019_00-GTAVDLCMPLUXEPT2.pkg
robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\US\ X:\FTP\Template_Copy\dlc\ps3 UP1004-BLUS31156_00-GTAVDLCMPLUXEPT2.pkg
robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\xbox\Content\0000000000000000\545408A7\00000002\ X:\FTP\Template_Copy\dlc\xbox360\Content\0000000000000000\545408A7\00000002 545408A70CCFDF12
robocopy /E N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\dev_Live\ X:\FTP\Template_Copy\drivex\gta5\titleupdate\dev_Live\ *.*
robocopy /E N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\dev_Live_US\ X:\FTP\Template_Copy\drivex\gta5\titleupdate\dev_Live_US\ *.*
robocopy /E N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Japanese_Live\ X:\FTP\Template_Copy\drivex\gta5\titleupdate\Japanese_Live\ *.*
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\EU\ *.*
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\JPN\ *.*
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\US\ *.*
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Xbox_pkgs\$TitleUpdate\545408A7\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\xbox360\$TitleUpdate\545408A7\ *.*


echo Please enter a Build number to copy from the directory:
set INPUT=
set /P INPUT=Input: %=%
Ren X:\FTP\Template_Copy Dev_Live_v%INPUT%_TU

echo Copy Complete!
pause
exit

::echo Please enter a Build number to copy from the directory:
::set INPUT=
::set /P INPUT=Input: %=%
::robocopy N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs_logging\v%INPUT%\ X:\FTP\Template_Copy\ps3_pkgs_logging\v%INPUT%\ *.*
::Ren X:\FTP\Template_Copy Dev_Live_v%INPUT%_TU

::echo Copy Complete!
::pause
::exit

:net
call %RS_TOOLSROOT%\bin\setenv.bat

robocopy /E X:\FTP\Template X:\FTP\Template_Copy *.*
Del X:\FTP\drivex\gta5\titleupdate\Japanese_Live
RMDIR /s /q X:\FTP\drivex\gta5\titleupdate\Japanese_Live
Del X:\FTP\drivex\gta5\titleupdate\dev_Live
RMDIR /s /q X:\FTP\drivex\gta5\titleupdate\dev_Live
Rename X:\FTP\Template_Copy\drivex\gta5\titleupdate\Japanese_Live Japanese
Ren X:\FTP\Template_Copy\drivex\gta5\titleupdate\dev_Live dev
Del X:\FTP\Template_Copy\ps3_360_TU_pkgs
RMDIR /s /q X:\FTP\Template_Copy\ps3_360_TU_pkgs
Del X:\FTP\Template_Copy\ps3_pkgs_logging
RMDIR /s /q X:\FTP\Template_Copy\ps3_pkgs_logging
::robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\EU\ X:\FTP\Template_Copy\dlc\ps3 EP1004-BLES01807_00-GTAVDLCMPLUXEPT2.pkg
::robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\JPN\ X:\FTP\Template_Copy\dlc\ps3 JP0230-BLJM61019_00-GTAVDLCMPLUXEPT2.pkg
::robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\ps3\US\ X:\FTP\Template_Copy\dlc\ps3 UP1004-BLUS31156_00-GTAVDLCMPLUXEPT2.pkg
robocopy N:\RSGEDI\Distribution\QA_Build\gta5\dlc\xbox\Content\0000000000000000\545408A7\00000002\ X:\FTP\Template_Copy\dlc\xbox360\Content\0000000000000000\545408A7\00000002 545408A70CCFDF12
robocopy /E N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\dev\ X:\FTP\Template_Copy\drivex\gta5\titleupdate\dev\ *.*
robocopy /E N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Japanese\ X:\FTP\Template_Copy\drivex\gta5\titleupdate\Japanese\ *.*
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\EU\ EP1004-BLES01807_00-GTAVTITLEUPDATER-A0126-V0100.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\EU\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\EU\ EP1004-NPEB01283_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\JPN\ JP0230-NPJB00516_00-GTAVDIGITALDOWNL-A0122-V0100.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\JPN\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\JPN\ JP0230-BLJM61019_00-GTAVTITLEUPDATER-A0223-V0200.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\US\ UP1004-BLUS31156_00-GTAVTITLEUPDATER-A0126-V0100.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\PS3_pkgs\US\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\ps3\US\ UP1004-NPUB31154_00-GTAVDIGITALDOWNL-A0126-V0100.pkg
robocopy /S N:\RSGEDI\Distribution\QA_Build\gta5\titleupdate\Xbox_pkgs\$TitleUpdate\545408A7\ X:\FTP\Template_Copy\ps3_360_TU_pkgs\xbox360\$TitleUpdate\545408A7\ *.*

echo Please enter a Build number to copy from the directory:
set INPUT=
set /P INPUT=Input: %=%
Ren X:\FTP\Template_Copy Dev_Network_v%INPUT%_TU

echo Copy Complete!
pause
exit




:no
exit

