::************************************************************************
:: GTA5 builder - Delete CMP - GTA5_TU_Prebuild							**
:: Updated: 				29/05/2014						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

::*****Select a branch*****
:Ask
echo Please select a Branch DevLive/DevNet ?(L/N)
set INPUT=
set /P INPUT=Input: %=%
If /I "%INPUT%"=="l" goto Live 
If /I "%INPUT%"=="n" goto Net
If /I "%INPUT%"=="DevLive" goto Live 
If /I "%INPUT%"=="DevNet" goto Net


echo Incorrect input & goto Ask

::*****Set the Build directory to the Dev Branch *****
:Net
set BUILDDIR=X:\gta5\titleupdate\dev
set JAPANESE="0"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo dev Build Successfully Set!
)
echo Are you sure you want to remove the cmp files?
pause
goto reduce

::*****Set the Build directory to the Dev Branch *****
:Live
set BUILDDIR=X:\gta5\titleupdate\dev_live
set JAPANESE="0"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo dev Build Successfully Set!
)
echo Are you sure you want to remove the cmp files?
pause
goto reduce


::***** delete cmp files *****
:reduce
echo Deleting CMP files...
@Echo on
DEL "%BUILDDIR%\*.cmp" /F
Pause



