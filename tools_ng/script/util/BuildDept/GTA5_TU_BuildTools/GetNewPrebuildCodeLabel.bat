::****************************************************
:: GTA5 builder - Grab GTA5_NewPreBuildCode					**
:: Updated: 				28/08/2012				**
:: Edits: 					new		**
:: Last edited by:			Ross McKinstray			**
::****************************************************

@echo off
echo.

TITLE PRE-BUILD GTAV - Grab GTA5_NewPreBuildCode
ECHO PRE-BUILD GTAV - Grab GTA5_NewPreBuildCode

rem Set the xdk environment variables
pushd %xedk%\bin\win32\
call xdkvars.bat
xbreboot
popd



echo kill rag & systray
tasklist | find /I "rag.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM rag.exe /T /F
)
tasklist | find /I "ragApp.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM ragApp.exe /T /F
)
tasklist | find /I "systrayrfs.exe" > NUL
IF %ERRORLEVEL% EQU 0 (
                taskkill /IM systrayrfs.exe /T /F
)

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

echo.
echo ##############################################
echo ### Grabbing Latest Tools from perforce ######
echo ##############################################
::p4 sync //depot/gta5/tools/...@[GTA5]_TOOLS_CURRENT 2> %RS_TOOLSROOT%/logs/tools_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	echo WARNING: Errors were reported during the grab.
::	echo You may have to force grab them through perforce.
::	echo Close the error log txt file and press any key to continue with grab.
::	%RS_TOOLSROOT%/logs/tools_sync.txt
::	pause
::) ELSE (
::	echo Tools grabbed successfully.
::)

::SYNC_CODE
echo ################################################
echo ### Syncing GTA5_NewPreBuildCode game and rage code ###
echo ################################################
p4 sync //depot/gta5/src/dev/...@GTA5_NewPreBuildCode 2> %RS_TOOLSROOT%/logs/code_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during game src code grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/code_sync.txt
	pause
) ELSE (
	echo Game code grabbed successfully.
)

echo.
p4 sync //rage/gta5/dev/...@GTA5_NewPreBuildCode 2> %RS_TOOLSROOT%/logs/rage_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during rage src code grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/rage_sync.txt
	pause
) ELSE (
	echo Rage code grabbed successfully.
)

echo.
p4 sync //depot/gta5/xlast/...@GTA5_NewPreBuildCode 2> %RS_TOOLSROOT%/logs/xlast_sync.txt
IF %ERRORLEVEL% EQU 1 (
	echo WARNING: Errors during xlast grab
	echo You may have to force grab through perforce.
	echo Close the error log txt file and press any key to continue with grab.
	%RS_TOOLSROOT%/logs/xlast_sync.txt
	pause
) ELSE (
	echo Xlast grabbed successfully.
)


pause
exit





