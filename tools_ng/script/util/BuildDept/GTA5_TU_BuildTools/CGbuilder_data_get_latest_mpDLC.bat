@ECHO OFF

cd /d %~dp0

TITLE %RS_PROJECT% GTA5: Getting mpPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting mpPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::
p4 sync //gta5_dlc/mpPacks/*/*/Gametext/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpApartment/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpArmy/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpCNC/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpRelationships/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSports/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatches/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev/...#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //gta5_dlc/mpPacks/mpApartment/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpArmy/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpCNC/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpRelationships/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSports/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatches/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*#head 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::==========================================::



::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)


pause


