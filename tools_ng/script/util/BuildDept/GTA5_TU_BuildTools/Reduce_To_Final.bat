::************************************************************************
:: GTA5 builder - Reduce dev_Live Build to final - GTA5_TU_Prebuild		**
:: Updated: 				12/11/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

::*****Select a branch*****
:Ask
echo Please select a Branch DevLive/DevNet JapaneseLive/JapaneseNet ?(L/N/JL/JN)
set INPUT=
set /P INPUT=Input: %=%
If /I "%INPUT%"=="l" goto Live 
If /I "%INPUT%"=="n" goto Net
If /I "%INPUT%"=="jl" goto JapaneseLive 
If /I "%INPUT%"=="jn" goto JapaneseNet
If /I "%INPUT%"=="DevLive" goto Live 
If /I "%INPUT%"=="DevNet" goto Net
If /I "%INPUT%"=="JapaneseLive" goto JapaneseLive 
If /I "%INPUT%"=="JapaneseNet" goto JapaneseNet

echo Incorrect input & goto Ask

::*****Set the Build directory to the DevLive Branch *****
:Live
set BUILDDIR=X:\gta5\titleupdate\dev_Live
set JAPANESE="0"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo dev_Live Build Successfully Set!
)
echo Are you sure you want to continue with reducing the dev_Live directory to final data?
pause
goto reduce

::*****Set the Build directory to the Dev Branch *****
:Net
set BUILDDIR=X:\gta5\titleupdate\dev
set JAPANESE="0"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo dev Build Successfully Set!
)
echo Are you sure you want to continue with reducing the dev directory to final data?
pause
goto reduce

::*****Set the Build directory to the Japanese_Live Branch *****
:JapaneseLive
set BUILDDIR=X:\gta5\titleupdate\Japanese_Live
set JAPANESE="1"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo Japanese_Live Build Successfully Set!
)
echo Are you sure you want to continue with reducing the Japanese_Live directory to final data?
pause
goto reduce

::*****Set the Build directory to the Japanese Branch *****
:JapaneseNet
set BUILDDIR=X:\gta5\titleupdate\Japanese
set JAPANESE="1"
pushd %BUILDDIR%
IF %ERRORLEVEL% EQU 1 (
	echo BuildBranch does not exist
) ELSE (
	echo Japanese_Dev Build Successfully Set!
)
echo Are you sure you want to continue with reducing the Japanese_Dev directory to final data?
pause
goto reduce

::***** delete and copy optimised shaders to final directories *****
:reduce
echo Copying Optimized shaders to final...
@Echo on
DEL "%BUILDDIR%\common\shaders\psn_final\*.*" /F
COPY "%BUILDDIR%\common\shaders\psn_optimized_final\*.*" "%BUILDDIR%\common\shaders\psn_final\"
@Echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem copying the optimized shaders check that psn_optimized_final shaders directory is not empty and that psn_final does not contain files!
	pause
) ELSE (
	echo Shaders successfully coppied!
)
Pause

::***** delete superfluous debug scripts and rename _REL to script *****
echo Deleting and renaming final scripts...

@Echo on
DEL "%BUILDDIR%\ps3\patch\levels\gta5\script\script.rpf" /F
Rename "%BUILDDIR%\ps3\patch\levels\gta5\script\script_REL.rpf" "script.rpf"
DEL "%BUILDDIR%\xbox360\patch\levels\gta5\script\script.rpf" /F
Rename "%BUILDDIR%\xbox360\patch\levels\gta5\script\script_REL.rpf" "script.rpf"
@Echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem either deleting or renaming the script files!
	pause
) ELSE (
	echo Scripts reduced successfully!
)
Pause

::***** delete superfluous debug language RPF's and rename _REL to <language>.rpf *****
echo Deleting and renaming final texts...
if %JAPANESE%=="1" (
@Echo on
Del "%BUILDDIR%\ps3\patch\data\lang\american.rpf"/F
Del "%BUILDDIR%\ps3\patch\data\lang\Japanese.rpf"/F
Rename "%BUILDDIR%\ps3\patch\data\lang\american_REL.rpf" "american.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\Japanese_REL.rpf" "Japanese.rpf"
Del "%BUILDDIR%\xbox360\patch\data\lang\american.rpf"/F
Del "%BUILDDIR%\xbox360\patch\data\lang\Japanese.rpf"/F
Rename "%BUILDDIR%\xbox360\patch\data\lang\american_REL.rpf" "american.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\Japanese_REL.rpf" "Japanese.rpf"
@Echo off
	IF %ERRORLEVEL% EQU 1 (
		echo There was a problem either deleting or renaming the text files!
		pause
	) ELSE (
		echo Text reduced successfully!
	)
Pause
) ELSE (
@Echo on
@Echo on
Del "%BUILDDIR%\ps3\patch\data\lang\american.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\chinese.rpf" /F 
Del "%BUILDDIR%\ps3\patch\data\lang\french.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\german.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\italian.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\korean.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\mexican.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\polish.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\portuguese.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\russian.rpf" /F
Del "%BUILDDIR%\ps3\patch\data\lang\spanish.rpf" /F
Rename "%BUILDDIR%\ps3\patch\data\lang\american_REL.rpf" "american.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\chinese_REL.rpf" "chinese.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\french_REL.rpf" "french.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\german_REL.rpf" "german.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\italian_REL.rpf" "italian.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\korean_REL.rpf" "korean.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\mexican_REL.rpf" "mexican.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\polish_REL.rpf" "polish.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\portuguese_REL.rpf" "portuguese.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\russian_REL.rpf" "russian.rpf"
Rename "%BUILDDIR%\ps3\patch\data\lang\spanish_REL.rpf" "spanish.rpf"
Del "%BUILDDIR%\xbox360\patch\data\lang\american.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\chinese.rpf" /F 
Del "%BUILDDIR%\xbox360\patch\data\lang\french.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\german.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\italian.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\korean.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\mexican.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\polish.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\portuguese.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\russian.rpf" /F
Del "%BUILDDIR%\xbox360\patch\data\lang\spanish.rpf" /F
Rename "%BUILDDIR%\xbox360\patch\data\lang\american_REL.rpf" "american.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\chinese_REL.rpf" "chinese.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\french_REL.rpf" "french.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\german_REL.rpf" "german.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\italian_REL.rpf" "italian.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\korean_REL.rpf" "korean.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\mexican_REL.rpf" "mexican.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\polish_REL.rpf" "polish.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\portuguese_REL.rpf" "portuguese.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\russian_REL.rpf" "russian.rpf"
Rename "%BUILDDIR%\xbox360\patch\data\lang\spanish_REL.rpf" "spanish.rpf"
@Echo off
@Echo off
	IF %ERRORLEVEL% EQU 1 (
		echo There was a problem either deleting or renaming the text files!
		pause
	) ELSE (
		echo Text reduced successfully!
	)
Pause
)

::***** delete superfluous debug files *****
echo Deleting superfluous debug files...
@Echo on
Del "%BUILDDIR%\common\data\ScaleformValidMethods.xml "/F
@Echo off
	IF %ERRORLEVEL% EQU 1 (
		echo There was a problem deleting other debug files!
		pause
	) ELSE (
		echo Debug removed successfully!
	)
Pause
@Echo on



::***** Open root directories*****
%SystemRoot%\explorer.exe "%BUILDDIR%\xbox360\patch\data\lang\"
%SystemRoot%\explorer.exe "%BUILDDIR%\ps3\patch\data\lang\"
%SystemRoot%\explorer.exe "%BUILDDIR%\ps3\patch\levels\gta5\script\"
%SystemRoot%\explorer.exe "%BUILDDIR%\xbox360\patch\levels\gta5\script\"

echo Reduce_To_Final now complete please check the open root directories and validate their contents!
pause


