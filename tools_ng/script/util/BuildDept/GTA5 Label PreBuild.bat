::************************************************
:: GTA5 Label Prebuild batch file               **
:: Updated: 				20/09/2012			**
:: Last edits:				Remove network exes **
:: Last edited by:			Ross McKinstray		**
::************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING PREBUILD LABEL
echo.

P4 labelsync -l GTA5_PreBuild //depot/gta5/assets/export/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/assets/GameText/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/build/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/src/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/script/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/xlast/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/tools/...
P4 labelsync -l GTA5_PreBuild //rage/gta5/dev/...
P4 labelsync -l GTA5_PreBuild //ps3sdk/...
POPD

echo Copy PreBuild exes to network folder.

if not exist N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\ (
	mkdir N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\
)

::robocopy /PURGE C:\spu_debug\ N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\spu_debug\ *.*
XCOPY /I /R /Y C:\spu_debug\*.* N:\RSGEDI\Distribution\QA_Build\gta5\PreBuildExes\spu_debug\*.*


erase /F /Q N:\RSGEDI\Distribution\QA_Build\gta5\PreBuildExes\*.bat
erase /F /Q N:\RSGEDI\Distribution\QA_Build\gta5\PreBuildExes\*.snproj


pause