@echo off

:: The below variables can be edited to control menu defaults
:: ---------------------------------------------------------------------------------------

::  Default Branch
set branch=dev_ng_Live
:: Auto-sync P4. This will sync script, text and shader source from p4 automatically
set autoP4=Yes
:: Clear out old files from the results folder first
set cleanResultsFolder=Yes

::  Enter the pack and patch names here. See https://hub.gametools.dev/display/RSGGTAV/DLC+Packs
set dlcPackName=mpSum2
set dlcPatchDay=patchDay27NG
::  Search the above
set search_DLC_and_PatchDay=No

::  Search update.rpf? 
set search_UpdateRPF=No
::  Search update2.rpf? 
set search_Update2RPF=No
::  Search update_debug.rpf? 
set search_Update_DebugRPF=No
::  Search common.rpf? 
set search_common_RPF=No
::  Search script source 
set search_script_source=No
:: Search text source, shaders
set search_text_and_shaders=No
:: Search bintext
set search_game_binary=No
:: Search script strings
set search_scriptStrings=No

:: Search platforms
set search_x64=Yes
set search_orbis=No
set search_durango=No
set search_scarlett=No
set search_prospero=No

:: Previous Retail Build
:: With the resilio directory structure there's no easy way to guess this. To a script it's just some arbitrary non-latest dev_ng_Live build, so for now we hard-code it
set defaultPreviousRetailPCBuild=N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\pc\2022-05-11_GTAV_PC_Patch_2628_2-dev_ng_live\exes\master






:: ---------------------------------------------------------------------------------------
:: Do not edit anything below this line unless you know what you're doing
:: --------------------------------------------------------------------------------------------------------------
set haveWarnedAboutAutoClear=No
set searchGen=gen8
set crossGenDiffHacks=No
set settingsFile=buildsearch-settings-v1.txt
set keywordScannerPath=X:\gta5\docs\QA\Technical\KeywordScanner\
set sensitiveKeywordsToolPath=X:\gta5\tools_ng\script\util\BuildDept\CS_SensitiveKeywordSearch\
set exePathCurrent=none
set exePathPrev=none
set expectedP4Port=RSGPERFORCE:1666
set divider=---------------------------------------------------------------------------
set xboneArchivePath=N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live_archive\
set xboneDevLivePath=N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live
set ps4ArchivePath=N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live_archive\
set ps4DevLivePath=N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live
set resilio=N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\pc\

:: --------------------------------------------------------------------------------------------------------------
SETLOCAL ENABLEDELAYEDEXPANSION
pushd %~dp0
if not exist "X:\KEYWORDEXTRACT" mkdir X:\KEYWORDEXTRACT
GOTO start




:checkToolsRoot
if not '%RS_TOOLSROOT%'=='X:\gta5\tools_ng' (
	cls
	echo %divider%
	echo   Check RS_TOOLSROOT
	echo %divider%
	echo.
	echo  RS_TOOLSROOT is set to %RS_TOOLSROOT% but we expected X:\gta5\tools_ng
	echo.
	echo  Double-check GTAV tools are installed correctly. 
	echo  Attempting to process GTAV RPFs with RDR or Americas tools WILL fail
	echo.
	echo 
	pause
)
GOTO :EOF

:checkP4Port
IF /I '%P4PORT%'=='%expectedP4Port%' (
	goto :EOF
) else (
	set P4PORT=%expectedP4Port%
)
GOTO :EOF

:checkP4Session
echo Checking P4 login...
p4 opened -s
if errorlevel 1 (
	echo %divider%
	echo.
	echo               Your perforce server connection has failed
	echo.
	echo              Verify P4PORT is set to rsgperforce:1666 then
	echo              log into P4V and restart the shelve assistant
	echo.
	echo %divider%
	pause
	exit
)
cls
GOTO :EOF

:checkIfRSGEDI
set isRSGEDI=No
	for /F "tokens=1-2 delims=-" %%a in ('echo %computername%') do (
		if '%%a'=='EDIW' (
			set isRSGEDI=Yes
		)
		if '%%a'=='EDIL' (
			set isRSGEDI=Yes
		)
	)
IF /I '%isRSGEDI%'=='No' (
	cls
	echo %divider%
	echo    Warning: This tool uses files from N:\RSGEDI\Distribution\QA_Build\
	echo %divider%
	echo.
	echo  In checking your computer name it doesn't seem to start EDIW or EDIL
	echo  If you're running this tool from outside the North network please be aware
	echo  that it accesses a number of dirs in N:\RSGEDI\Distribution\QA_Build\
	echo.
	echo  %xboneArchivePath%
	echo  %xboneDevLivePath%
	echo  %ps4ArchivePath%
	echo  %ps4DevLivePath%
	echo.
	echo  Even if you can't access these locations the following two scan types should
	echo  still work as they only rely on perforce:
	echo   - Text and Shaders
	echo   - Script Strings
	echo.
	pause
)
GOTO :EOF


:start

CALL :checkToolsRoot
CALL :checkP4Port
CALL :checkP4Session
CALL :checkIfRSGEDI
CALL :loadSettings
:: ---------------------------------------------
:: ----------- Menu and TU selection -----------
:: ---------------------------------------------
:: Ideally this would be refactored to remove reliance on GOTOs
	:menuStart
	cls
	set INPUT=
	CALL :saveSettings
	TITLE Sensitive Keyword Search Automator
	if not '%RS_TOOLSROOT%'=='X:\gta5\tools_ng' (
		echo %divider%
		echo.
		echo       Re-install GTAV tools before proceeding
		echo       RS_TOOLSROOT is %RS_TOOLSROOT%
		echo.
	)
	REM echo %divider%
	REM echo                   Sensitive Keyword Search Automator
	echo %divider%
	echo        Current DLC pack: %dlcPackName%
	echo      Current Patch pack: %dlcPatchDay%
	echo        Auto P4 sync (4): %autoP4%
	echo   Clean result dirs (C): %cleanResultsFolder%
	REM echo Hacky cross-gen diffs (H): %crossGenDiffHacks%
	echo %divider%
	echo                     First Pass			Secondary Checks
	echo                   --------------		----------------
	echo             update RPFs (U): %search_UpdateRPF%		 PC Master Binary (B): %search_game_binary%
	echo   Current Patch and DLC (P): %search_DLC_and_PatchDay%		   Script Strings (S): %search_scriptStrings% (needs external input)
	echo        Text and Shaders (T): %search_text_and_shaders%		
	echo.
	echo                     Platforms
	echo                   -------------
	echo            x64 windows (PX): %search_x64%		
	echo                  Orbis (PO): %search_orbis%	
	echo                Durango (PD): %search_durango%	
	echo               Scarlett (PS): %search_scarlett%
	echo               Prospero (PP): %search_prospero%
	echo.
	echo %divider%
	echo Please select an option:
	echo   1 - Search current TU
	echo   2 - Search archived TU
	SET INPUT=
	SET /P INPUT=Input selection: 
	if /I '%INPUT%'=='1' CALL :runSearches Current
	if /I '%INPUT%'=='2' CALL :runSearches Archive
	if /I '%INPUT%'=='D' CALL :toggleSearchUpdateDebug
	if /I '%INPUT%'=='P' CALL :toggleSearchDLCandPatch
	if /I '%INPUT%'=='CR' CALL :toggleSearchCommonRPF
	if /I '%INPUT%'=='4' CALL :toggleAutoP4
	if /I '%INPUT%'=='R' CALL :toggleSearchScriptSource
	if /I '%INPUT%'=='C' CALL :togglecleanResultsFolder
	if /I '%INPUT%'=='H' CALL :toggleCrossGenDiffHacks
	if /I '%INPUT%'=='T' CALL :toggleSearchTextShadersAndScript
	if /I '%INPUT%'=='U' CALL :toggleSearchUpdate
	::if /I '%INPUT%'=='U2' CALL :toggleSearchUpdate2
	if /I '%INPUT%'=='B' CALL :toggleSearchBinary
	if /I '%INPUT%'=='S' CALL :toggleSearchScriptStrings
	if /I '%INPUT%'=='PX' CALL :toggleSearchX64
	if /I '%INPUT%'=='PO' CALL :toggleSearchOrbis
	if /I '%INPUT%'=='PD' CALL :toggleSearchDurango
	if /I '%INPUT%'=='PS' CALL :toggleSearchScarlett
	if /I '%INPUT%'=='PP' CALL :toggleSearchProspero
	CALL :RemoveImpossiblePlatformChoices
	GOTO menuStart

	:gen9Unsupported
	cls
	echo %divider%
	echo.
	echo      This feature isn't supported on gen9 yet, sorry
	echo                 Script will now exit
	echo.
	echo %divider%
	pause
	exit
	GOTO :EOF

	:runSearches
	set noSearches=Yes
	set noPlatforms=Yes
	for %%a in (%search_UpdateRPF% %search_DLC_and_PatchDay% %search_game_binary% %search_scriptStrings% %search_text_and_shaders%) do (
		if '%%a'=='Yes' set noSearches=No
	)
	if '%noSearches%'=='Yes' (
		cls
		echo %divider%
		echo  No searches
		echo %divider%
		echo  You must enable at least one search type to proceed.
		echo.
		pause
		GOTO :EOF
	) 
	for %%a in (%search_x64% %search_orbis% %search_durango% %search_scarlett% %search_prospero%) do (
		if '%%a'=='Yes' set noPlatforms=No
	)
	if '%noPlatforms%'=='Yes' (
		cls
		echo %divider%
		echo  No platforms
		echo %divider%
		echo  You must select at least one platform to proceed.
		echo.
		pause
		GOTO :EOF
	)
	if '%1'=='Current' call :selectBranchForCurrentRun
	if '%1'=='Archive' call :selectTUforArchiveRun
	GOTO :EOF

	:RemoveImpossiblePlatformChoices
		IF /I '%searchGen%'=='gen9' (
			set search_x64=No
			set search_orbis=No
			set search_durango=No
		) else (
			set search_scarlett=No
			set search_prospero=No
		)
	GOTO :EOF

	:selectTUforArchiveRun
	if /I '%searchGen%'=='gen9' (
		:: For archived runs we need to know the perforce TU backup location.
		:: This allows us to later sync script, text source, shaders, etc.
		cls
		p4 dirs //depot/gta5/titleupdate/GEN9_TU_Sub*
		echo.
		echo From this list enter the TU NUMBER you require. eg for GEN9_TU_Sub_2_0 just enter 2
		echo.
	) else (
		cls
		echo %divider%
		echo.
		echo Referring to the build list on https://hub.gametools.dev/x/DAGeAg enter
		echo the backup branch you require, without any prefix or _0 suffix.
		echo.
		echo eg. v2372-dev_ng_live Los Santos Tuners is NG_TU_Sub_39_0 so simply enter 39
		echo.
		echo %divider%
		echo.
		p4 dirs //depot/gta5/titleupdate/NG_TU_Sub_3*
		p4 dirs //depot/gta5/titleupdate/NG_TU_Sub_4*
		echo.
	)
	SET /P INPUT=Input selection:
	if /I '%searchGen%'=='gen9' (
		set branch=GEN9_TU_Sub_%INPUT%_0
	) else (
		set branch=NG_TU_Sub_%INPUT%_0
	)
	CALL :archivedRun
	GOTO :EOF

	:selectBranchForCurrentRun
	IF /I '%searchGen%'=='gen8' (
		set b1=Dev_NG
		set b2=Dev_NG_Live
	) else (
		set b1=dev_gen9_sga
		set b2=dev_gen9_sga_live
	)
	echo 1. Search %b1%
	echo 2. Search %b2%
	SET INPUT=
	SET /P INPUT=Input selection: 
	if '%INPUT%'=='1' set branch=%b1%&& call :currentRun
	if '%INPUT%'=='2' set branch=%b2%&& call :currentRun
	SET INPUT=
	GOTO :EOF
:: -END-OF---- Menu and TU selection -----------





:: ------------------------------------
:: ----------- Menu Toggles -----------
:: ------------------------------------
	:toggleSearchUpdateDebug
	IF /I '%search_Update_DebugRPF%'=='Yes' (
		set search_Update_DebugRPF=No
		CALL :warnAboutAutoClear
	) else (
			set search_Update_DebugRPF=Yes
			set search_UpdateRPF=No
			set search_Update2RPF=No
	)
	goto :EOF

	:toggleSearchUpdate
	IF /I '%search_UpdateRPF%'=='Yes' (
		set search_UpdateRPF=No
		set search_Update2RPF=No
		CALL :warnAboutAutoClear
	) else (
			set search_UpdateRPF=Yes
			set search_Update2RPF=Yes
			set search_Update_DebugRPF=No
	)
	goto :EOF
	
::	:toggleSearchUpdate2
::	IF /I '%search_Update2RPF%'=='Yes' (
::		set search_Update2RPF=No
::		set search_UpdateRPF=No
::		CALL :warnAboutAutoClear
::	) else (
::			set search_UpdateRPF=Yes
::			set search_Update2RPF=Yes
::			set search_Update_DebugRPF=No
::	)
::	goto :EOF

	:toggleSearchBinary
	IF /I '%search_game_binary%'=='Yes' (
		set search_game_binary=No
		CALL :warnAboutAutoClear
	) else (
			set search_game_binary=Yes
			set search_orbis=No
			set search_durango=No
			set search_scarlett=No
			set search_prospero=No
			set search_x64=Yes
			set searchGen=gen8
	)
	goto :EOF

	:toggleSearchDLCandPatch
	IF /I '%search_DLC_and_PatchDay%'=='Yes' (
		set search_DLC_and_PatchDay=No
		CALL :warnAboutAutoClear
	) else (
			set search_DLC_and_PatchDay=Yes
	)
	goto :EOF
	
	:toggleSearchCommonRPF
	IF /I '%search_common_RPF%'=='Yes' (
		set search_common_RPF=No
		CALL :warnAboutAutoClear
	) else (
			set search_common_RPF=Yes
	)
	goto :EOF

	:toggleSearchTextShadersAndScript
	IF /I '%search_text_and_shaders%'=='Yes' (
		set search_text_and_shaders=No
		CALL :warnAboutAutoClear
	) else (
			set search_text_and_shaders=Yes
	)
	goto :EOF

	:toggleSearchScriptSource
	IF /I '%search_script_source%'=='Yes' (
		set search_script_source=No
		CALL :warnAboutAutoClear
	) else (
			set search_script_source=Yes
	)
	goto :EOF

	:toggleSearchScriptStrings
	IF /I '%search_scriptStrings%'=='Yes' (
		set search_scriptStrings=No
		CALL :warnAboutAutoClear
	) else (
			set search_scriptStrings=Yes
	)
	goto :EOF

	:toggleAutoP4
	IF /I '%autoP4%'=='Yes' (
		set autoP4=No
	) else (
			set autoP4=Yes
	)
	goto :EOF

	:toggleSearchX64
	IF /I '%search_x64%'=='Yes' (
		set search_x64=No
		CALL :warnAboutAutoClear
	) else (
		set search_x64=Yes
		set searchGen=gen8
		set search_scarlett=No
		set search_prospero=No
	)
	goto :EOF

	:toggleSearchOrbis
	IF /I '%search_orbis%'=='Yes' (
		set search_orbis=No
		CALL :warnAboutAutoClear
	) else (
		set search_orbis=Yes
		set searchGen=gen8
		set search_scarlett=No
		set search_prospero=No
		set search_game_binary=No
	)
	goto :EOF

	:toggleSearchDurango
	IF /I '%search_durango%'=='Yes' (
		set search_durango=No
		CALL :warnAboutAutoClear
	) else (
		set search_durango=Yes
		set searchGen=gen8
		set search_scarlett=No
		set search_prospero=No
		set search_game_binary=No
	)
	goto :EOF

	:toggleSearchScarlett
	IF /I '%search_scarlett%'=='Yes' (
		set search_scarlett=No
		CALL :warnAboutAutoClear
	) else (
		set search_scarlett=Yes
		set searchGen=gen9
		set search_orbis=No
		set search_durango=No
		set search_x64=No
		set search_game_binary=No
	)
	goto :EOF

	:toggleSearchProspero
	IF /I '%search_prospero%'=='Yes' (
		set search_prospero=No
		CALL :warnAboutAutoClear
	) else (
		set search_prospero=Yes
		set searchGen=gen9
		set search_orbis=No
		set search_durango=No
		set search_x64=No
		set search_game_binary=No
	)
	goto :EOF

	:toggleCrossGenDiffHacks
	IF /I '%crossGenDiffHacks%'=='Yes' (
		set crossGenDiffHacks=No
	) else (
			set crossGenDiffHacks=Yes
	)
	goto :EOF

	:togglecleanResultsFolder
	IF /I '%cleanResultsFolder%'=='Yes' (
		set cleanResultsFolder=No
		CALL :warnAboutAutoClear
	) else (
			set cleanResultsFolder=Yes
	)
	goto :EOF
:: -END OF---- MENU TOGGLES -----------





:: ------------------------------------------
:: ----------- Misc Functionality -----------
:: ------------------------------------------
	:warnAboutAutoClear
		IF /I '%cleanResultsFolder%'=='Yes' (
			IF /I '%haveWarnedAboutAutoClear%'=='No' (
				echo 
				cls
				color 3
				echo %divider%
				echo                   WARNING Auto-clear is still enabled
				echo.
				echo     The entire results folder will be cleared during the next search!
				echo     To preserve previous search results disable auto-clear with C
				echo %divider%
				pause
				color 7
				set haveWarnedAboutAutoClear=Yes
			)
		)
	goto :EOF

	:printCurrentSearchToScreen
		:: Call this before starting a search to display a big banner in the console log
		echo %divider%
		echo %divider%
		echo 	Working on %branch% %currentSearchActivity%
		echo %divider%
		echo %divider%
	GOTO :EOF

	:clearResultsDir
		cls
		set currentSearchActivity=Deleting old results
		CALL :printCurrentSearchToScreen
		del /Q %cd%\results\%branch%\*.txt
	GOTO :EOF
	
	:promptForMasterBinaryDirs
		:: To start, we try to guess the most recent PC deg_ng_live build by running a directory listing and taking the last one. This becomes guessedCurrentPCBuild
		:: We then ask the user if they'd like to provide a different one, they can paste in a directory or just press enter to accept the guess
		set guessedCurrentPCBuild=unknown
		for /F "tokens=1-4 delims=-" %%a in ('dir /b /O N %resilio%') do (
			if '%%d'=='dev_ng_live' (
				set guessedCurrentPCBuild=%resilio%%%a-%%b-%%c-%%d\exes\master
			)
		)
		cls
		echo %divider%
		echo.
		echo  To search game master exes you must link to the directory containing the
		echo          current exe, plus one you'd like to diff against
		echo.
		echo %divider%
		echo.
		echo  We first need the path containing the latest game_win64_master.exe
		echo.
		echo  Press enter to accept the default path,
		echo    %guessedCurrentPCBuild%
		echo  or enter a different one below
		echo.
		set INPUT=
		SET /P INPUT=Path: 
		if '%INPUT%'=='' (
			set exePathCurrent=%guessedCurrentPCBuild%
		) else (
			set exePathCurrent=%INPUT%
		)
		echo.
		:: Now ask them for the current retail directory. defaultPreviousRetailPCBuild is hard-coded up top for now, unfortunately.
		cls
		echo %divider%
		echo.
		echo  Now we need the path containing RETAIL game_win64_master.exe
		echo.
		echo  Press enter to accept the default path,
		echo    %defaultPreviousRetailPCBuild%
		echo  or enter a different one below
		echo.
		set INPUT=
		SET /P INPUT=Path:
		if '%INPUT%'=='' (
			set exePathPrev=%defaultPreviousRetailPCBuild%
		) else (
			set exePathPrev=%INPUT%
		)
		set INPUT=
	GOTO :EOF

	:saveSettings
	(
		echo %search_x64%
		echo %search_orbis%
		echo %search_durango%
		echo %search_scarlett%
		echo %search_prospero%
	) > %settingsFile%
	GOTO :EOF

	:loadSettings
		if exist %settingsFile% (
			< %settingsFile% (
				set /p search_x64=
				set /p search_orbis=
				set /p search_durango=
				set /p search_scarlett=
				set /p search_prospero=
			)
		)
		IF /i '%search_scarlett%' =='Yes' (
			set searchgen=gen9
		)
		IF /i '%search_prospero%' =='Yes' (
			set searchgen=gen9
		)
	GOTO :EOF

:: -END OF---- Misc Functionality -----------





:: ----------------------------------------------
:: ----------- RPF Copy functionality -----------
:: ----------------------------------------------
	:printWarningAboutCopyingUpdateRPF
		:: If we are searching a PC build or a backed-up console build we need to ask ths user for update.rpf as it isn't in a predictable location
		:: This simply displays the warning. see promptToCopyUpdateRPF for associated copy logic
		cls
		color 6
		echo %divider%
		echo  Before proceeding we need %rpfName% (also update2 RPF, where applicable) copied
		IF /I '%search_durango%'=='Yes' echo  to X:\temp\xbox\
		IF /I '%search_orbis%'=='Yes' echo  to X:\temp\ps4\
		IF /I '%search_x64%'=='Yes' echo  to X:\temp\PC\
		IF /I '%search_scarlett%'=='Yes' echo  to X:\temp\Scarlett\
		IF /I '%search_prospero%'=='Yes' echo  to X:\temp\Prospero\
		echo.
		echo  On the next screen you will be asked to provide a build folder to auto-copy from.
		echo  You can copy-paste it from the list, paste in any other valid build folder
		echo  or choose to instead manually copy the RPF. In that case don't paste anything
		echo  and just press enter
		echo.
		echo %divider%
		pause
		color 7
	GOTO :EOF

	:promptToCopyUpdateRPF
		REM Ideally, this would be re-worked.
		:: If we've chosen to search update.rpf or update_debug.rpf we might need to copy it into x:\temp\, depending on search
		:: For latest dev_ng or dev_ng_live we only need to do this for the PC build as console RPFs are in a known location
		:: For other build we need to do this for all chosen platforms
		IF '%search_Update_DebugRPF%'=='Yes' (
			goto needToCopy
		) 
		IF '%search_UpdateRPF%'=='Yes' (
			goto needToCopy
		)
		goto :EOF
		
		:needToCopy
		set rpfName=none
		IF '%search_Update_DebugRPF%'=='Yes' (
			set rpfName=update_debug.rpf
		) 
		IF '%search_UpdateRPF%'=='Yes' (
			set rpfName=update.rpf
		)
		:: If we're searching a current dev_ng or dev_ng_live build we know where to get console RPFs from and can skip this
		:: IF /I '%branch%'=='dev_ng' goto skipConsoleUpdateRPFCopy
		:: IF /I '%branch%'=='dev_ng_Live' goto skipConsoleUpdateRPFCopy

		
		:: released dev_ng_live builds for both gen8 consoles are archived in their own specific location. So for both xboneArchivePath and ps4ArchivePath we need to print their sub-dirs
		IF /I '%search_durango%'=='Yes' (
			CALL :printWarningAboutCopyingUpdateRPF
			cls
			echo %divider%
			echo   XBOX ONE %rpfName%
			echo %divider%
			set dst=X:\temp\xbox\
			set src=%xboneArchivePath%
			echo %xboneDevLivePath%
			call :listBuildDirs
			call :requestBuildAndCopyRPF
		)
		IF /I '%search_orbis%'=='Yes' (
			CALL :printWarningAboutCopyingUpdateRPF
			cls
			echo %divider%
			echo   PLAYSTATION 4 %rpfName%
			echo %divider%
			set dst=X:\temp\ps4\
			set src=%ps4ArchivePath%
			echo %ps4DevLivePath%
			call :listBuildDirs
			call :requestBuildAndCopyRPF
		)
		:: Gen9 consoles are a little different. Archived builds are always inside the dev_gen9_sga_Live dir so we always just list its subdirs
		IF /I '%search_scarlett%'=='Yes' (
			CALL :printWarningAboutCopyingUpdateRPF
			cls
			echo %divider%
			echo   XBSX %rpfName%
			echo %divider%
			set dst=X:\temp\Scarlett\
			:: Hack to handle archived gen9 searches
			set src=N:\Projects\GTA5\Package\dev_gen9_sga_Live\
			call :listBuildDirs
			set src=N:\Projects\GTA5\Package\%branch%\
			call :listBuildDirs
			call :requestBuildAndCopyRPF Scarlett
		)
		IF /I '%search_prospero%'=='Yes' (
			CALL :printWarningAboutCopyingUpdateRPF
			cls
			echo %divider%
			echo   PS5 %rpfName%
			echo %divider%
			set dst=X:\temp\Prospero\
			:: Hack to handle archived gen9 searches
			set src=N:\Projects\GTA5\Package\dev_gen9_sga_Live\
			call :listBuildDirs
			set src=N:\Projects\GTA5\Package\%branch%\
			call :listBuildDirs
			call :requestBuildAndCopyRPF Prospero
		)
		:skipConsoleUpdateRPFCopy
		IF /I '%search_x64%'=='Yes' (
			CALL :printWarningAboutCopyingUpdateRPF
			cls
			set dst=X:\temp\PC\
			set src=%resilio%
			call :listBuildDirs
			echo %divider%
			call :requestBuildAndCopyRPF
		)
		color 7
	goto :EOF

	:requestBuildAndCopyRPF
		color 7
		SET INPUT=
		SET /P INPUT=Path:
		IF /I '%INPUT%'=='' (
			echo Assuming manual copy...
		) ELSE (
			IF '%searchgen%'=='gen9' (
				call :copyUpdateRPFs %INPUT%\%1
			) ELSE (
				call :copyUpdateRPFs %INPUT%
			)
		)
		
	goto :EOF
	
	:copyUpdateRPFs
		echo Deleting old RPFs in %dst%...
		del /Q %dst%*.rpf
		IF /I '%dst%'=='X:\temp\PC\' (
			:: PC Build
			robocopy %1\Data\update\ %dst% %rpfName%
			IF /I '%search_Update2RPF%'=='Yes' (
				IF /I '%rpfName%'=='update.rpf' (
					robocopy %1\Data\update\ %dst% update2.rpf
				) else (
					robocopy %1\Data\update\ %dst% update2_debug.rpf
				)
			)
		) ELSE (
			:: Console build
			robocopy %1\game\update\ %dst% %rpfName%
			IF /I '%search_Update2RPF%'=='Yes' (
				IF /I '%rpfName%'=='update.rpf' (
					robocopy %1\game\update\ %dst% update2.rpf
				) else (
					robocopy %1\game\update\ %dst% update2_debug.rpf
				)
			)
		)
	goto :EOF

	:listBuildDirs
		for /f "delims=" %%i in ('dir /B /AD %src%') do (
			echo %src%%%i
		)
	goto :EOF
:: -END OF---- RPF Copy functionality -----------






::--------------------------------------------------------------------
::
::  ###  ####   #### #   # ##### #   # ##### ###     ####  #   # ##  #
:: #   # #   # #     #   #   #   #   # #     #  #    #   # #   # # # #
:: ##### ####  #     #####   #    # #  ###   #   #   ####  #   # # # #
:: #   # #  #  #     #   #   #    # #  #     #  #    #  #  #   # # # #
:: #   # #   #  #### #   # #####   #   ##### ###     #   #  ###  #  ##
::
::--------------------------------------------------------------------
:archivedRun
	cls
	:: Search Flow
	call :PromptToCopyUpdateRPF
	if /I '%cleanResultsFolder%'=='Yes' call :clearResultsDir
	CALL :runTextAndShaders
	CALL :runScriptSource
	CALL :runUpdateRPFSearches
	IF /I '%search_scriptStrings%'=='Yes' CALL :RunScriptStringSearch
	CALL :Finished
GOTO :EOF

GOTO Finished
:: We should never hit this but fall through to "Finished" if we do
::
:: -END-OF------ Archived Run -----------------------------------------










::--------------------------------------------------------------------
::
::  #### #   # ####  ####  ##### ##  # #####   ####  #   # ##  #
:: #     #   # #   # #   # #     # # #   #     #   # #   # # # #
:: #     #   # ####  ####  ###   # # #   #     ####  #   # # # #
:: #     #   # #  #  #  #  #     # # #   #     #  #  #   # # # #
::  ####  ###  #   # #   # ##### #  ##   #     #   #  ###  #  ##
::
::--------------------------------------------------------------------

:currentRun
cls

:: Search Flow
call :PromptToCopyUpdateRPF
if /I '%cleanResultsFolder%'=='Yes' call :clearResultsDir
IF /I '%search_scriptStrings%'=='Yes' CALL :RunScriptStringSearch
if /I '%search_game_binary%'=='Yes' call :promptForMasterBinaryDirs
IF /I '%search_game_binary%'=='Yes' CALL :RunMasterBinarySearch
CALL :runCommonRPFSearch
CALL :runTextAndShaders
CALL :runDLCTextSource
CALL :runScriptSource
CALL :runUpdateRPFSearches
CALL :runDLCRPFSearches
CALL :Finished
GOTO :EOF


:: ------------------
:: ###   #      ###
:: #  #  #     #
:: #  #  #     #
:: ###   ####   ###
:: ------------------
:: DLC searches are exclusive to current TU runs. 
:: There's no need to check old DLC packs


:runDLCRPFSearches
::DLC RPFS
IF '%search_DLC_and_PatchDay%'=='Yes' (
	IF /I '%search_orbis%'=='Yes' (	
	:: PS4
		set platform=ps4
		IF /I '%autoP4%'=='Yes' (
			CALL :p4SyncDLCandPatch
		)
		CALL :runOneDLCRPFSearch
	)
	IF /I '%search_durango%'=='Yes' (	
	:: Xbox One
		set platform=xboxone
		IF /I '%autoP4%'=='Yes' (
			CALL :p4SyncDLCandPatch
		)
		CALL :runOneDLCRPFSearch
	)
	IF /I '%search_x64%'=='Yes' (	
	:: X64
		set platform=x64
		IF /I '%autoP4%'=='Yes' (
			CALL :p4SyncDLCandPatch
		)
		CALL :runOneDLCRPFSearch
	)
		IF /I '%search_prospero%'=='Yes' (	
	:: Prospero
		set platform=ps5
		IF /I '%autoP4%'=='Yes' (
			CALL :p4SyncDLCandPatch
		)
		CALL :runOneDLCRPFSearch
	)
		IF /I '%search_scarlett%'=='Yes' (	
	:: Scarlett
		set platform=xbsx
		IF /I '%autoP4%'=='Yes' (
			CALL :p4SyncDLCandPatch
		)
		CALL :runOneDLCRPFSearch
	)
)
GOTO :EOF

:p4SyncDLCandPatch
	p4 sync //depot/gta5/titleupdate/%branch%/%platform%/dlcPacks/%dlcPackName%/...
	p4 sync //depot/gta5/titleupdate/%branch%/%platform%/dlcPacks/%dlcPatchDay%/...
goto :EOF

:runOneDLCRPFSearch
	:: DLC pack
	TITLE Checking %platform% %dlcPackName%
	set currentSearchActivity=%platform% %dlcPackName%
	CALL :printCurrentSearchToScreen
	BuildsKeyWordSearchTool.exe -contents -filenames -searchExcludes -resources -path "X:/gta5/titleupdate/%branch%/%platform%/dlcPacks/%dlcPackName%" -result "%branch%/%dlcPackName%_dlcrpfs_%platform%"

	:: Patch pack
	TITLE Checking %platform% %dlcPatchDay%
	set currentSearchActivity=%platform% %dlcPatchDay%
	CALL :printCurrentSearchToScreen
	BuildsKeyWordSearchTool.exe -contents -filenames -searchExcludes -resources -path "X:/gta5/titleupdate/%branch%/%platform%/dlcPacks/%dlcPatchDay%" -result "%branch%/%dlcPatchDay%_dlcrpfs_%platform%"
GOTO :EOF

:runDLCTextSource
	IF /I '%searchGen%'=='gen9' (
		set assetsFolder=assets_gen9_disc
		REM TODO ^ Expect this to change
	) else (
		set assetsFolder=assets_ng
	)
	IF /I '%search_text_and_shaders%'=='Yes' (
		IF /I '%autoP4%'=='Yes' (
		set currentSearchActivity=Auto P4 Sync
			CALL :printCurrentSearchToScreen
			p4 sync //gta5_dlc/mpPacks/%dlcPackName%/%assetsFolder%/gametext/American/...
			p4 sync //gta5_dlc/mpPacks/%dlcPackName%/%assetsFolder%/gametext/americanDLCNames.txt
		)
		::DLC TEXT SOURCE
		set currentSearchActivity=DLC Text Source
		CALL :printCurrentSearchToScreen
		TITLE Checking dlc gametext [##------------]
		BuildsKeyWordSearchTool.exe -contents -filenames -path "X:/gta5_dlc/mpPacks/%dlcPackName%/%assetsFolder%/gametext/" -result "%branch%/%dlcPackName%_gametext"
	)
GOTO :EOF
::
:: -END-OF------ Current Run -----------------------------------------








:: -----------------------------------------
::
::  ####  ###  #   # #   #  ###  ##  #
:: #     #   # ## ## ## ## #   # # # #
:: #     #   # # # # # # # #   # # # #
:: #     #   # #   # #   # #   # # # #
::  ####  ###  #   # #   #  ###  #  ##
::
:: -----------------------------------------
::

:runCommonRPFSearch
IF /I '%search_common_RPF%'=='Yes' (
	echo Copy common.RPF now
	pause
	BuildsKeyWordSearchTool.exe -contents -filenames -searchExcludes -resources -path "X:/gta5/tools_ng/script/util/BuildDept/CS_SensitiveKeywordSearch/inputs/common/" -result "%branch%/commonRPF"
)
GOTO :EOF


:runUpdateRPFSearches
	::UPDATE RPF
	IF /I '%search_UpdateRPF%'=='Yes' (
		set updateRPFName=update
		CALL :runAllplatformUpdateRPF
	)
	IF /I '%search_Update_DebugRPF%'=='Yes' (
		set updateRPFName=update_debug
		CALL :runAllplatformUpdateRPF
	)
GOTO :EOF

:runAllplatformUpdateRPF
	IF /I '%search_durango%'=='Yes' (
		set src=X:\temp\Xbox
		set platform=xbox
		IF /I '%search_Update2RPF%'=='Yes' (
			CALL :runUpdateAndUpdate2RPFs
		) ELSE (
			CALL :runSingleUpdateRPFSearch
		)
	)
	IF /I '%search_orbis%'=='Yes' (	
	::PS4
		set src=X:\temp\PS4
		set platform=ps4
		IF /I '%search_Update2RPF%'=='Yes' (
			CALL :runUpdateAndUpdate2RPFs
		) ELSE (
			CALL :runSingleUpdateRPFSearch
		)
	)
	IF /I '%search_x64%'=='Yes' (	
	::PC
		set src=X:\temp\PC
		set platform=PC
		IF /I '%search_Update2RPF%'=='Yes' (
			CALL :runUpdateAndUpdate2RPFs
		) ELSE (
			CALL :runSingleUpdateRPFSearch
		)
	)
	IF /I '%search_scarlett%'=='Yes' (	
		set src=X:\temp\Scarlett
		set platform=Scarlett
		CALL :runUpdateAndUpdate2RPFs
	)
	IF /I '%search_Prospero%'=='Yes' (	
		set src=X:\temp\Prospero
		set platform=Prospero
		CALL :runUpdateAndUpdate2RPFs
	)
GOTO :EOF

:runUpdateAndUpdate2RPFs
:: This handles the fact gen9 has two update RPFs and this script was originally designed assuming just one
	CALL :runSingleUpdateRPFSearch
	IF /I '%updateRPFName%'=='update' (
		set updateRPFName=update2
		CALL :runSingleUpdateRPFSearch
		set updateRPFName=update
	) else (
		set updateRPFName=update2_debug
		CALL :runSingleUpdateRPFSearch
		set updateRPFName=update_debug
	)
GOTO :EOF

:runSingleUpdateRPFSearch
	set dest=X:\gta5\tools_ng\script\util\BuildDept\CS_SensitiveKeywordSearch\inputs\%updateRPFName%
	set platNameForResults=%platform%
	:: update RPF
	TITLE Checking %platform% %updateRPFName%.rpf
	set currentSearchActivity=%platform% %updateRPFName%.rpf
	CALL :printCurrentSearchToScreen
	robocopy %src% %dest% %updateRPFName%.rpf
	
	IF /I '%crossGenDiffHacks%'=='Yes' (
		set platNameForResults=platform
	) ELSE (
		set platNameForResults=%platform%
	)
	
	BuildsKeyWordSearchTool.exe -contents -filenames -searchExcludes -resources -path "X:/gta5/tools_ng/script/util\BuildDept/CS_SensitiveKeywordSearch/inputs/%updateRPFName%/" -result "%branch%/%updateRPFName%_rpf_%platNameForResults%"
	
	del /Q /S "X:/gta5/tools_ng/script/util\BuildDept/CS_SensitiveKeywordSearch/inputs/%updateRPFName%"
GOTO :EOF

:RunMasterBinarySearch
	set currentSearchActivity=Master Binary search
	CALL :printCurrentSearchToScreen

	if not exist "X:\TEMP\CurrentEXE" mkdir X:\TEMP\CurrentEXE
	if not exist "X:\TEMP\PrevRetailEXE" mkdir X:\TEMP\PrevRetailEXE
	DEL /Q X:\TEMP\CurrentEXE\*
	DEL /Q X:\TEMP\PrevRetailEXE\*
	robocopy %exePathCurrent% X:\TEMP\CurrentEXE game_win64_master.exe
	robocopy %exePathPrev% X:\TEMP\PrevRetailEXE game_win64_master.exe

	p4 sync //depot/gta5/docs/QA/Technical/KeywordScanner/...

	CD /D %keywordScannerPath%
	for /F "tokens=1-4 delims=/" %%a in ('echo %DATE%') do (
		set currentDate=%%c-%%b-%%a
	)
	KeywordScanner -p=X:\TEMP\CurrentEXE\ -c=X:\TEMP\PrevRetailEXE\game_win64_master.exe -ft=exe -sl=2 -o=X:\temp\ -k=X:\gta5\tools_ng\script\util\BuildDept\CS_SensitiveKeywordSearch\inputs\keywords.txt
	CD /D %sensitiveKeywordsToolPath%
	robocopy X:\temp\%currentDate% %cd%/results/%branch%/ *.txt
GOTO :EOF


:RunScriptStringSearch
	:: SCRIPT STRING
	set emptyInput=Yes
	for /f %%a in ('dir /b X:\gta5\tools_ng\script\util\BuildDept\CS_SensitiveKeywordSearch\inputs\scriptStrings') do (
		set emptyInput=No
	)
	if '%emptyInput%'=='Yes' (
		cls
		echo %divider%
		echo    This search requires additional input files
		echo %divider%
		echo.
		echo  See the StringSearch section of hub.gametools.dev/display/RSGQA/GTAV+-+Sensitive+Keyword+Diff+Tools
		echo.
		echo 
		pause
		set INPUT=
		GOTO :EOF
	) else (
		cls
		set currentSearchActivity=Script Strings
		CALL :printCurrentSearchToScreen
		BuildsKeyWordSearchTool.exe -contents -path "X:/gta5/tools_ng/script/util/BuildDept/CS_SensitiveKeywordSearch/inputs/scriptStrings" -result "%branch%/scriptStrings"
		set INPUT=
	)
GOTO :EOF

	
:runScriptSource
	IF /I '%searchGen%'=='gen9' (
		set assetsFolder=assets_gen9_sga
	) else (
		set assetsFolder=assets_ng
	)
	IF /I '%search_script_source%'=='Yes' (
		IF /I '%autoP4%'=='Yes' (
			set currentSearchActivity=Auto P4 Sync
			CALL :printCurrentSearchToScreen
			p4 sync //depot/gta5/script/%branch%/...
		)
		::::SCRIPT SOURCE
		set currentSearchActivity=Script Source
		CALL :printCurrentSearchToScreen
		TITLE Checking scripts [###-----------]
		BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:/gta5/script/%branch%" -result "%branch%/script_source"
	)
GOTO :EOF


:runTextAndShaders
	IF /I '%searchGen%'=='gen9' (
		set assetsFolder=assets_gen9_sga
	) else (
		set assetsFolder=assets_ng
	)
	IF /I '%search_text_and_shaders%'=='Yes' (
		IF /I '%autoP4%'=='Yes' (
			set currentSearchActivity=Auto P4 Sync
			CALL :printCurrentSearchToScreen
			p4 sync //depot/gta5/%assetsFolder%/gametext/%branch%/American/...
			p4 sync //depot/gta5/%assetsFolder%/gametext/%branch%/ActivityFeed/...
			p4 sync //depot/gta5/%assetsFolder%/gametext/%branch%/DLC/...
			p4 sync //depot/gta5/%assetsFolder%/gametext/%branch%/errorcodes/american.txt
			p4 sync //depot/gta5/%assetsFolder%/gametext/%branch%/Trophies/...
			p4 sync //depot/gta5/src/%branch%/game/shader_source/...
			p4 sync //depot/gta5/src/%branch%/game/shaders/...
			p4 sync //depot/gta5/titleupdate/%branch%/common/shaders/fxdb/...
		)
		:: Gen9 builds include win32 fxdbs, and there's a 10GB file in there. That chokes the keyword process so for now, just delete them
		:: Not ideal, we could do with a better solution
		del /q X:\gta5\titleupdate\%branch%\common\shaders\fxdb\*win32*
		::TEXT SOURCE
		if exist X:/gta5/%assetsFolder%/gametext/%branch% ( :: Hack to deal with branch variable being set to NG_TU_Sub_etc... when searching backed-up TUs
			set currentSearchActivity=Text Source
			CALL :printCurrentSearchToScreen
			TITLE Checking gametext [#-------------]
			IF /I '%crossGenDiffHacks%'=='Yes' (
				:: Change our output format here to remove the branch and just write results to "gametext_content", etc
				:: Should allow cross-gen diffs. Otherwise the two files would have different names
				BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:/gta5/%assetsFolder%/gametext/%branch%" -result "%branch%/gametext"
			) else (
				BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:/gta5/%assetsFolder%/gametext/%branch%" -result "%branch%/%assetsFolder%_gametext"
			)
		)
		::::SHADER SOURCE
		set currentSearchActivity=Shader Source
		CALL :printCurrentSearchToScreen
		TITLE Checking Shader Srouce [###-----------]
		BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:\gta5\src\%branch%\game\shader_source" -result "%branch%/shader_source"

		::::SHADERS
		set currentSearchActivity=Shaders
		CALL :printCurrentSearchToScreen
		TITLE Checking Shaders [#######-------]
		BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:\gta5\src\%branch%\game\shaders" -result "%branch%/shaders"
		
		IF /I '%searchGen%'=='gen9' (
			::::FXDB
			set currentSearchActivity=FXDB
			CALL :printCurrentSearchToScreen
			TITLE Checking Shaders [###########---]
			BuildsKeyWordSearchTool.exe -contents -filenames -ambiguous -path "X:\gta5\titleupdate\%branch%\common\shaders\fxdb" -result "%branch%/fxdb"
		)
	)
GOTO :EOF
:: -END-OF- Common Search Functionality ---------





:Finished
TITLE FINISHED!
echo 
pause
GOTO :EOF