::****************************************************************
:: NG GTA5 Symbol Backup batch file								**
:: Updated: 						01/12/2014					**
:: Edits:							Added Japanese SKU			**
:: Last edited by:					Graham Rust					**
::****************************************************************

@echo off
echo.
cd /d %~dp0

set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\Symbols_NG
if "%1%"=="TU" (
set builddir=X:\gta5\titleupdate\dev_ng
)
if "%1%"=="Temp" (
set builddir=X:\gta5\titleupdate\dev_temp
)
if "%1%"=="TU_JPN" (
set builddir=X:\gta5\titleupdate\Japanese_ng
)
if "%1%"=="Temp_JPN" (
set builddir=X:\gta5\titleupdate\Japanese_ng_Live
)
if "%1%"=="NGLIVE_JPN" (
set builddir=X:\gta5\titleupdate\Japanese_ng_Live
)
if "%1%"=="NGLIVE" (
set builddir=X:\gta5\titleupdate\dev_ng_Live
)


set path="C:\Program Files (x86)\WinRAR\";%path%

set ver=NUL
FOR /F "eol=# skip=3" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO MAIN
	
:MAIN

if exist %copydir%\version%ver% (
	echo This version folder already exists, please manually back up these symbols!
) ELSE (
	echo Copying symbols to the network
	mkdir "%copydir%/version%ver%"
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_durango_*.*
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_orbis_*.*
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_win64_*.*
)




pause

