@echo off
echo.

cd /d %~dp0

if "%1%"=="Temp" (
set titleupdatepath=//depot/gta5/Titleupdate/dev_Temp
set TextPath=X:\gta5\assets_ng\GameText\dev_Temp
)
if "%1%"=="TU" (
set titleupdatepath=//depot/gta5/Titleupdate/dev_ng
set TextPath=X:\gta5\assets_ng\GameText\dev_ng
)
if "%1%"=="NGLIVE" (
set titleupdatepath=//depot/gta5/Titleupdate/dev_ng_live
set TextPath=X:\gta5\assets_ng\GameText\dev_ng_live
)

:: checkout
p4 edit %titleupdatepath%/ps4/data/lang/american.rpf
p4 edit %titleupdatepath%/ps4/data/lang/american_REL.rpf
p4 edit %titleupdatepath%/x64/data/lang/american.rpf
p4 edit %titleupdatepath%/x64/data/lang/american_REL.rpf
p4 edit %titleupdatepath%/xboxone/data/lang/american.rpf
p4 edit %titleupdatepath%/xboxone/data/lang/american_REL.rpf


:: build
pushd %TextPath%\American\
	american.bat
popd
