::****************************************************************
:: Grab Prebuild transferral Label								**
:: Updated: 									09/01/2014		**
:: Edits:										new				**
:: Last edited by:								Graham rust		**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo Grabbing GTA5_NG_PrePreBuildCode label...
echo.

::Sync GTA5 Prebuild Takeover label
IF "%1%"=="NG" (
P4 sync //depot/gta5/assets_ng/gametext/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer  
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="TU" (
P4 sync //depot/gta5/assets_ng/gametext/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer 
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="NGLIVE" (
P4 sync //depot/gta5/assets_ng/gametext/dev_ng_Live/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_ng_Live/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_ng_Live/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_ng_Live/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer 
P4 sync //rage/gta5/dev_ng_Live/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="Temp" (
P4 sync //depot/gta5/assets_ng/gametext/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer
P4 sync //rage/gta5/dev_temp/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="NGLib" (
P4 sync //depot/gta5/assets_ng/gametext/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer 
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_liberty/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="TULib" (
P4 sync //depot/gta5/assets_ng/gametext/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer 
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_liberty/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PreBuild_Transfer 
)

IF "%1%"=="TempLib" (
P4 sync //depot/gta5/assets_ng/gametext/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/script/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/src/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/titleupdate/dev_temp/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //depot/gta5/xlast/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpHeist/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/mpPacks/mpPatchesNG/assets_ng/GameText/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild_Transfer
P4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild_Transfer 
P4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild_Transfer 
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_PreBuild_Transfer 
P4 sync //rage/gta5/dev_temp/...@GTA5_NG_PreBuild_Transfer 
)



POPD

pause