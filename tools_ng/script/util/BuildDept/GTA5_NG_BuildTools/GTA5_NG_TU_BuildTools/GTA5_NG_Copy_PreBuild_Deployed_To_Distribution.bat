::************************************************************************
:: GTA5 builder - Copy Deployed to Distro - GTA5_TU_Prebuild	        **
:: Updated: 				03/11/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0


if "%1%"=="TempXBOX1" (


robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\ *.*

POPD
pause
exit
)

if "%1%"=="TUXBOX1" (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\ *.*

POPD
pause
exit
)

if "%1%"=="NGLIVEXBOX1" (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\ *.*

POPD
pause
exit
)

if "%1%"=="TUPS4" (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\ *.*

POPD
pause
exit
)

if "%1%"=="NGLIVEPS4" (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\ *.*

POPD
pause
exit
)

if "%1%"=="TempPS4" (

robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\ *.*


POPD
pause
exit
)