::****************************************************************
:: GTA5 Create and Populate Version label 						**
:: Updated: 						01/12/2014					**
:: Edits:							Added Japanese SKU			**
:: Last edited by:					Graham Rust			 		**
::****************************************************************
setlocal enabledelayedexpansion
Set-ExecutionPolicy RemoteSigned
Set-ExecutionPolicy RemoteSigned CurrentUser

@echo off
echo.

cd /d %~dp0


if "%1%"=="TU" (
	set Branch=dev_ng
)

if "%1%"=="Temp" (
	set Branch=dev_temp
)

if "%1%"=="NGLIVE" (
	set Branch=dev_ng_Live
)

TITLE GTA5 label - Create and populate GTA5_NG_TU_version_xxx label
ECHO GTA5 label - Create and populate GTA5_NG_TU_version_xxx label

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

:: read version.txt in dev_build for label.
set ver=NUL

if exist X:\gta5\titleupdate\%Branch%\common\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO SETLABEL
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAIN
)
	
:SETLABEL
	echo.
	echo Create and set new version label.
	echo.
	set verLabel=GTA5_NG_version_%ver%
	echo verLabel = %verLabel%
	echo ver = %ver%
	echo You are about to create a new label called %verLabel% is this right? 
	echo Close this window if not, or press a key to confirm...
	pause
	p4 label -o -t gta5ngtemplate %verLabel% | p4 label -i
	echo.
	pause
	set version=NUL
	::Removing the old version label (20 reveisions prior.)
	set "replace=%USERNAME%"
	set /a versionNumber=%ver%
	set /a "OldVer=versionNumber-20"
	set oldVerLabel=GTA5_NG_version_%oldVer%-dev_ng_Live
	p4 labels|findstr /r "%oldVerLabel%"
	IF ERRORLEVEL 1 GOTO NEXTLABEL
		echo Unloading %oldVerLabel% - to reload call "p4 reload -l %oldVerLabel%"
		p4 label -o %oldVerLabel%>X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt
		goto SETLABELOWNERANDUNLOAD
	
:NEXTLABEL
	set oldVerLabel=GTA5_NG_version_%oldVer%-dev_ng
	p4 labels|findstr /r "%oldVerLabel%"
	IF ERRORLEVEL 1 GOTO MAIN
		echo Unloading %oldVerLabel% - to reload call "p4 reload -l %oldVerLabel%"
		p4 label -o %oldVerLabel%>X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt
		goto SETLABELOWNERANDUNLOAD

:SETLABELOWNERANDUNLOAD
	set "search=graham.rust"
	set "search2=buildernorth"
	set "search3=gemma.mccord"
	set "search4=kristopher.williams"
	set "search5=neil.walker"
	set "search6=ross.mckinstray"
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search2%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search3%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search4%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search5%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search6%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	p4 label -i <"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt"
	P4 unload -l %oldVerLabel%
	GOTO MAIN
	
	
	
	


:MAIN
echo.
echo UPDATING LABELLED BUILD
echo.

if "%1%"=="TU" (
::Label GTA5 NG
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/export/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/GameText/dev_ng/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/maps/ParentTxds.xml
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/metadata/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/processed/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/textures/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/vehicles/...
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/src/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/script/dev_ng/...
P4 labelsync -l %verLabel% //depot/gta5/xlast/...
P4 labelsync -l %verLabel% //depot/gta5/tools_ng/...
P4 labelsync -l %verLabel% //rage/gta5/dev_ng/...

P4 tag -l %verLabel% //depot/gta5/build/dev_ng/x64/...

::Label DLC
P4 labelsync -l %verLabel% //gta5_dlc/mpPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/spPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/patchPacks/...

POPD
pause
exit
)

if "%1%"=="TU_JPN" (
::Label GTA5 NG
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/Japanese_ng/...


POPD
pause
exit
)

if "%1%"=="Temp" (
::Label GTA5 NG
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/export/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/GameText/dev_temp/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/maps/ParentTxds.xml
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/metadata/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/processed/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/textures/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/vehicles/...
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/dev_temp/...
P4 labelsync -l %verLabel% //depot/gta5/src/dev_temp/...
P4 labelsync -l %verLabel% //depot/gta5/script/dev_temp/...
P4 labelsync -l %verLabel% //depot/gta5/xlast/...
P4 labelsync -l %verLabel% //depot/gta5/tools_ng/...
P4 labelsync -l %verLabel% //rage/gta5/dev_temp/...

P4 tag -l %verLabel% //depot/gta5/build/dev_ng/x64/...

::Label DLC
P4 labelsync -l %verLabel% //gta5_dlc/mpPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/spPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/patchPacks/...

POPD
pause
exit
)

if "%1%"=="Temp_JPN" (
::Label GTA5 NG
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/Japanese_ng_live/...


POPD
pause
exit
)

if "%1%"=="NGLIVE" (
::Label GTA5 NG
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/export/...
P4 labelsync -l %verLabel% //depot/gta5/assets_ng/GameText/dev_ng_Live/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/maps/ParentTxds.xml
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/metadata/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/processed/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/textures/...
::P4 labelsync -l %verLabel% //depot/gta5/assets_ng/titleupdate/vehicles/...
P4 labelsync -l %verLabel% //depot/gta5/titleupdate/dev_ng_Live/...
P4 labelsync -l %verLabel% //depot/gta5/src/dev_ng_Live/...
P4 labelsync -l %verLabel% //depot/gta5/script/dev_ng_Live/...
P4 labelsync -l %verLabel% //depot/gta5/xlast/...
P4 labelsync -l %verLabel% //depot/gta5/tools_ng/...
P4 labelsync -l %verLabel% //rage/gta5/dev_ng_Live/...

P4 tag -l %verLabel% //depot/gta5/build/dev_ng/x64/...

::Label DLC
P4 labelsync -l %verLabel% //gta5_dlc/mpPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/spPacks/...
P4 labelsync -l %verLabel% //gta5_dlc/patchPacks/...

POPD
pause
exit
)


