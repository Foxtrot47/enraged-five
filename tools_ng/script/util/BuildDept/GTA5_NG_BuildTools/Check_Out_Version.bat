::************************************************************************
:: GTA5 builder - Check out dev_network version.txt - Build Tools		**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************
@Echo off
TITLE %~nx0
@setlocal enableextensions enabledelayedexpansion

set str1=%1


::-set build branch paths
set buildpath=//depot/gta5/build/dev_ng
set temppath=//depot/gta5/titleupdate/dev_Temp
set titleupdatepath=//depot/gta5/titleupdate/dev_ng
set NGLIVEpath=//depot/gta5/titleupdate/dev_ng_live

::-set Japanese branch paths
if not x%str1:_JPN=%==x%str1% (
set str1=%str1:_JPN=%
::-set build branch paths
set buildpath=//depot/gta5/build/Japanese_ng
set temppath=//depot/gta5/titleupdate/Japanese_ng_Live
set titleupdatepath=//depot/gta5/titleupdate/Japanese_ng
set NGLIVEpath=//depot/gta5/titleupdate/Japanese_ng_Live
)

IF "%str1%"=="Version" (
@echo on

p4 edit %buildpath%/common/data/version.txt

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out the version.txt!
	pause
	EXIT 
)
)
IF "%str1%"=="Version_TU" (
@echo on

p4 edit %titleupdatepath%/common/data/version.txt

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out the version.txt!
	pause
	EXIT 
)
)
IF "%str1%"=="Version_Temp" (
@echo on

p4 edit %temppath%/common/data/version.txt

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out the version.txt!
	pause
	EXIT 
)
)
IF "%str1%"=="Version_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/data/version.txt

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out the version.txt!
	pause
	EXIT 
)
)

