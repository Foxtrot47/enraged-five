::************************************************************************
:: GTA5 builder - Check out dev_network ps3 exes - Build Tools   		**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************
@Echo off
TITLE %~nx0
@setlocal enableextensions enabledelayedexpansion

set str1=%1
set buildpath=//depot/gta5/build/dev_ng
set temppath=//depot/gta5/titleupdate/dev_temp
set titleupdatepath=//depot/gta5/titleupdate/dev_ng
set NGLIVEpath=//depot/gta5/titleupdate/dev_ng_live

if not x%str1:_JPN=%==x%str1% (
set str1=%str1:_JPN=%
set buildpath=//depot/gta5/build/Japanese_ng
set temppath=//depot/gta5/titleupdate/Japanese_ng_live
set titleupdatepath=//depot/gta5/titleupdate/Japanese_ng
set NGLIVEpath=//depot/gta5/titleupdate/Japanese_ng_live
)

IF "%str1%"=="Durango_Masters" (
@echo on

p4 edit %buildpath%/game_durango_master.cmp
p4 edit %buildpath%/game_durango_master.exe
p4 edit %buildpath%/game_durango_master.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Masters" (
@echo on

p4 edit %buildpath%/game_orbis_master.cmp
p4 edit %buildpath%/game_orbis_master.elf
p4 edit %buildpath%/game_orbis_master.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_TU_Masters" (
@echo on

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango" (
@echo on

p4 edit %buildpath%/game_durango_beta.cmp
p4 edit %buildpath%/game_durango_beta.exe
p4 edit %buildpath%/game_durango_beta.pdb

p4 edit %buildpath%/game_durango_bankrelease.cmp
p4 edit %buildpath%/game_durango_bankrelease.exe
p4 edit %buildpath%/game_durango_bankrelease.pdb

p4 edit %buildpath%/game_durango_release.cmp
p4 edit %buildpath%/game_durango_release.exe
p4 edit %buildpath%/game_durango_release.pdb

p4 edit %buildpath%/game_durango_final.cmp
p4 edit %buildpath%/game_durango_final.exe
p4 edit %buildpath%/game_durango_final.pdb
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)


IF "%str1%"=="Orbis" (
@echo on

p4 edit %buildpath%/game_orbis_beta.cmp
p4 edit %buildpath%/game_orbis_beta.elf
p4 edit %buildpath%/game_orbis_beta.map

p4 edit %buildpath%/game_orbis_bankrelease.cmp
p4 edit %buildpath%/game_orbis_bankrelease.elf
p4 edit %buildpath%/game_orbis_bankrelease.map

p4 edit %buildpath%/game_orbis_release.cmp
p4 edit %buildpath%/game_orbis_release.elf
p4 edit %buildpath%/game_orbis_release.map

p4 edit %buildpath%/game_orbis_final.cmp
p4 edit %buildpath%/game_orbis_final.elf
p4 edit %buildpath%/game_orbis_final.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64" (
@echo on

p4 edit %buildpath%/game_win64_beta.cmp
p4 edit %buildpath%/game_win64_beta.exe
p4 edit %buildpath%/game_win64_beta.map
p4 edit %buildpath%/game_win64_beta.pdb

p4 edit %buildpath%/game_win64_bankrelease.cmp
p4 edit %buildpath%/game_win64_bankrelease.exe
p4 edit %buildpath%/game_win64_bankrelease.map
p4 edit %buildpath%/game_win64_bankrelease.pdb

p4 edit %buildpath%/game_win64_release.cmp
p4 edit %buildpath%/game_win64_release.exe
p4 edit %buildpath%/game_win64_release.map
p4 edit %buildpath%/game_win64_release.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out X64 exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_TU_Masters" (
@echo on

p4 edit %titleupdatepath%/game_durango_master.cmp
p4 edit %titleupdatepath%/game_durango_master.exe
p4 edit %titleupdatepath%/game_durango_master.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_TU_Masters" (
@echo on

p4 edit %titleupdatepath%/game_orbis_master.cmp
p4 edit %titleupdatepath%/game_orbis_master.elf
p4 edit %titleupdatepath%/game_orbis_master.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_TU_Masters" (
@echo on

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_TU" (
@echo on

p4 edit %titleupdatepath%/game_durango_beta.cmp
p4 edit %titleupdatepath%/game_durango_beta.exe
p4 edit %titleupdatepath%/game_durango_beta.pdb

p4 edit %titleupdatepath%/game_durango_bankrelease.cmp
p4 edit %titleupdatepath%/game_durango_bankrelease.exe
p4 edit %titleupdatepath%/game_durango_bankrelease.pdb

p4 edit %titleupdatepath%/game_durango_release.cmp
p4 edit %titleupdatepath%/game_durango_release.exe
p4 edit %titleupdatepath%/game_durango_release.pdb

p4 edit %titleupdatepath%/game_durango_final.cmp
p4 edit %titleupdatepath%/game_durango_final.exe
p4 edit %titleupdatepath%/game_durango_final.pdb
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_TU" (
@echo on

p4 edit %titleupdatepath%/game_orbis_beta.cmp
p4 edit %titleupdatepath%/game_orbis_beta.elf
p4 edit %titleupdatepath%/game_orbis_beta.map

p4 edit %titleupdatepath%/game_orbis_bankrelease.cmp
p4 edit %titleupdatepath%/game_orbis_bankrelease.elf
p4 edit %titleupdatepath%/game_orbis_bankrelease.map

p4 edit %titleupdatepath%/game_orbis_release.cmp
p4 edit %titleupdatepath%/game_orbis_release.elf
p4 edit %titleupdatepath%/game_orbis_release.map

p4 edit %titleupdatepath%/game_orbis_final.cmp
p4 edit %titleupdatepath%/game_orbis_final.elf
p4 edit %titleupdatepath%/game_orbis_final.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_TU" (
@echo on

p4 edit %titleupdatepath%/game_win64_beta.cmp
p4 edit %titleupdatepath%/game_win64_beta.exe
p4 edit %titleupdatepath%/game_win64_beta.map
p4 edit %titleupdatepath%/game_win64_beta.pdb

p4 edit %titleupdatepath%/game_win64_bankrelease.cmp
p4 edit %titleupdatepath%/game_win64_bankrelease.exe
p4 edit %titleupdatepath%/game_win64_bankrelease.map
p4 edit %titleupdatepath%/game_win64_bankrelease.pdb

p4 edit %titleupdatepath%/game_win64_release.cmp
p4 edit %titleupdatepath%/game_win64_release.exe
p4 edit %titleupdatepath%/game_win64_release.map
p4 edit %titleupdatepath%/game_win64_release.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out X64 exe files!
	pause
	EXIT 
)
)
IF "%str1%"=="Durango_Temp_Masters" (
@echo on

p4 edit %temppath%/game_durango_master.cmp
p4 edit %temppath%/game_durango_master.exe
p4 edit %temppath%/game_durango_master.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Temp_Masters" (
@echo on

p4 edit %temppath%/game_orbis_master.cmp
p4 edit %temppath%/game_orbis_master.elf
p4 edit %temppath%/game_orbis_master.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_Temp_Masters" (
@echo on

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_Temp" (
@echo on

p4 edit %temppath%/game_durango_beta.cmp
p4 edit %temppath%/game_durango_beta.exe
p4 edit %temppath%/game_durango_beta.pdb

p4 edit %temppath%/game_durango_bankrelease.cmp
p4 edit %temppath%/game_durango_bankrelease.exe
p4 edit %temppath%/game_durango_bankrelease.pdb

p4 edit %temppath%/game_durango_release.cmp
p4 edit %temppath%/game_durango_release.exe
p4 edit %temppath%/game_durango_release.pdb

p4 edit %temppath%/game_durango_final.cmp
p4 edit %temppath%/game_durango_final.exe
p4 edit %temppath%/game_durango_final.pdb
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Temp" (
@echo on

p4 edit %temppath%/game_orbis_beta.cmp
p4 edit %temppath%/game_orbis_beta.elf
p4 edit %temppath%/game_orbis_beta.map

p4 edit %temppath%/game_orbis_bankrelease.cmp
p4 edit %temppath%/game_orbis_bankrelease.elf
p4 edit %temppath%/game_orbis_bankrelease.map

p4 edit %temppath%/game_orbis_release.cmp
p4 edit %temppath%/game_orbis_release.elf
p4 edit %temppath%/game_orbis_release.map

p4 edit %temppath%/game_orbis_final.cmp
p4 edit %temppath%/game_orbis_final.elf
p4 edit %temppath%/game_orbis_final.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_Temp" (
@echo on

p4 edit %temppath%/game_win64_beta.cmp
p4 edit %temppath%/game_win64_beta.exe
p4 edit %temppath%/game_win64_beta.map
p4 edit %temppath%/game_win64_beta.pdb

p4 edit %temppath%/game_win64_bankrelease.cmp
p4 edit %temppath%/game_win64_bankrelease.exe
p4 edit %temppath%/game_win64_bankrelease.map
p4 edit %temppath%/game_win64_bankrelease.pdb

p4 edit %temppath%/game_win64_release.cmp
p4 edit %temppath%/game_win64_release.exe
p4 edit %temppath%/game_win64_release.map
p4 edit %temppath%/game_win64_release.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out X64 exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_Live_Masters" (
@echo on

p4 edit %NGLIVEpath%/game_durango_master.cmp
p4 edit %NGLIVEpath%/game_durango_master.exe
p4 edit %NGLIVEpath%/game_durango_master.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Live_Masters" (
@echo on

p4 edit %NGLIVEpath%/game_orbis_master.cmp
p4 edit %NGLIVEpath%/game_orbis_master.elf
p4 edit %NGLIVEpath%/game_orbis_master.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_TU_Masters" (
@echo on

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/game_durango_beta.cmp
p4 edit %NGLIVEpath%/game_durango_beta.exe
p4 edit %NGLIVEpath%/game_durango_beta.pdb

p4 edit %NGLIVEpath%/game_durango_bankrelease.cmp
p4 edit %NGLIVEpath%/game_durango_bankrelease.exe
p4 edit %NGLIVEpath%/game_durango_bankrelease.pdb

p4 edit %NGLIVEpath%/game_durango_release.cmp
p4 edit %NGLIVEpath%/game_durango_release.exe
p4 edit %NGLIVEpath%/game_durango_release.pdb

p4 edit %NGLIVEpath%/game_durango_final.cmp
p4 edit %NGLIVEpath%/game_durango_final.exe
p4 edit %NGLIVEpath%/game_durango_final.pdb
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/game_orbis_beta.cmp
p4 edit %NGLIVEpath%/game_orbis_beta.elf
p4 edit %NGLIVEpath%/game_orbis_beta.map

p4 edit %NGLIVEpath%/game_orbis_bankrelease.cmp
p4 edit %NGLIVEpath%/game_orbis_bankrelease.elf
p4 edit %NGLIVEpath%/game_orbis_bankrelease.map

p4 edit %NGLIVEpath%/game_orbis_release.cmp
p4 edit %NGLIVEpath%/game_orbis_release.elf
p4 edit %NGLIVEpath%/game_orbis_release.map

p4 edit %NGLIVEpath%/game_orbis_final.cmp
p4 edit %NGLIVEpath%/game_orbis_final.elf
p4 edit %NGLIVEpath%/game_orbis_final.map

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/game_win64_beta.cmp
p4 edit %NGLIVEpath%/game_win64_beta.exe
p4 edit %NGLIVEpath%/game_win64_beta.map
p4 edit %NGLIVEpath%/game_win64_beta.pdb

p4 edit %NGLIVEpath%/game_win64_bankrelease.cmp
p4 edit %NGLIVEpath%/game_win64_bankrelease.exe
p4 edit %NGLIVEpath%/game_win64_bankrelease.map
p4 edit %NGLIVEpath%/game_win64_bankrelease.pdb

p4 edit %NGLIVEpath%/game_win64_release.cmp
p4 edit %NGLIVEpath%/game_win64_release.exe
p4 edit %NGLIVEpath%/game_win64_release.map
p4 edit %NGLIVEpath%/game_win64_release.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out X64 exe files!
	pause
	EXIT 
)
)

IF "%str1%"=="X64_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/game_win64_beta.cmp
p4 edit %NGLIVEpath%/game_win64_beta.exe
p4 edit %NGLIVEpath%/game_win64_beta.map
p4 edit %NGLIVEpath%/game_win64_beta.pdb

p4 edit %NGLIVEpath%/game_win64_bankrelease.cmp
p4 edit %NGLIVEpath%/game_win64_bankrelease.exe
p4 edit %NGLIVEpath%/game_win64_bankrelease.map
p4 edit %NGLIVEpath%/game_win64_bankrelease.pdb

p4 edit %NGLIVEpath%/game_win64_release.cmp
p4 edit %NGLIVEpath%/game_win64_release.exe
p4 edit %NGLIVEpath%/game_win64_release.map
p4 edit %NGLIVEpath%/game_win64_release.pdb

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out X64 exe files!
	pause
	EXIT 
)
)

