::************************************************************************
:: GTA5 builder - Check out dev_network ps3 exes - Build Tools   		**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************
@Echo off
TITLE %~nx0
@setlocal enableextensions enabledelayedexpansion

set str1=%1


::-set build branch paths
set buildpath=//depot/gta5/build/dev_ng
set temppath=//depot/gta5/titleupdate/dev_Temp
set titleupdatepath=//depot/gta5/titleupdate/dev_ng
set NGLIVEpath=//depot/gta5/titleupdate/dev_ng_live

::-set Japanese branch paths
if not x%str1:_JPN=%==x%str1% (
set str1=%str1:_JPN=%
::-set build branch paths
set buildpath=//depot/gta5/build/Japanese_ng
set temppath=//depot/gta5/titleupdate/dev_Temp
set titleupdatepath=//depot/gta5/titleupdate/Japanese_ng
set NGLIVEpath=//depot/gta5/titleupdate/Japanese_ng_live
)

IF "%str1%"=="Durango" (
@echo on

p4 edit %buildpath%/common/shaders/durango/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Orbis" (
@echo on

p4 edit %buildpath%/common/shaders/orbis/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_30" (
@echo on

p4 edit %buildpath%/common/shaders/win32_30/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30 shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_40" (
@echo on

p4 edit %buildpath%/common/shaders/win32_40/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40 shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_40_lq" (
@echo on

p4 edit %buildpath%/common/shaders/win32_40_lq/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_lq shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="win32_nvstereo" (
@echo on

p4 edit %buildpath%/common/shaders/win32_nvstereo/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_nvstereo shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Durango_Final" (
@echo on

p4 edit %buildpath%/common/shaders/durango_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out durango_final shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Orbis_Final" (
@echo on

p4 edit %buildpath%/common/shaders/orbis_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out orbis_final shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_30_Final" (
@echo on

p4 edit %buildpath%/common/shaders/win32_30_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30_final shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_40_Final" (
@echo on

p4 edit %buildpath%/common/shaders/win32_40_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_final shader files!
	pause
	EXIT 
)
)

:: TU Shaders

IF "%str1%"=="Durango_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/durango/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/orbis/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_30_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_30/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_40/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_lq_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_40_lq/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_lq shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="win32_nvstereo_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_nvstereo/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_nvstereo shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_Final_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/durango_final/...
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out durango_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Final_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/orbis_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out orbis_final shader files!
	pause
	EXIT 
)
)
IF "%str1%"=="Win32_30_Final_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_30_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_Final_TU" (
@echo on

p4 edit %titleupdatepath%/common/shaders/win32_40_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_final shader files!
	pause
	EXIT 
)
)

:: Temp Shaders

IF "%str1%"=="Durango_Temp" (
@echo on

p4 edit %temppath%/common/shaders/durango/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Temp" (
@echo on

p4 edit %temppath%/common/shaders/orbis/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_30_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_30/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_40/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_lq_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_40_lq/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_lq shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="win32_nvstereo_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_nvstereo/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_nvstereo shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_Final_Temp" (
@echo on

p4 edit %temppath%/common/shaders/durango_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out durango_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Final_Temp" (
@echo on

p4 edit %temppath%/common/shaders/orbis_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out orbis_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_30_Final_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_30_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_Final_Temp" (
@echo on

p4 edit %temppath%/common/shaders/win32_40_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_final shader files!
	pause
	EXIT 
)
)
:: NGLIVE Shaders

IF "%str1%"=="Durango_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/durango/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Durango shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/orbis/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Orbis shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_30_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_30/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_40/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40 shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_lq_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_40_lq/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_lq shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="win32_nvstereo_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_nvstereo/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_nvstereo shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Durango_Final_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/durango_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out durango_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Orbis_Final_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/orbis_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out orbis_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_30_Final_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_30_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_30_final shader files!
	pause
	EXIT 
)
)

IF "%str1%"=="Win32_40_Final_NGLIVE" (
@echo on

p4 edit %NGLIVEpath%/common/shaders/win32_40_final/...

@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out win32_40_final shader files!
	pause
	EXIT 
)
)
