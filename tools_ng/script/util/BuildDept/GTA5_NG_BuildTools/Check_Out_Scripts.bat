::************************************************************************
:: GTA5 builder - Check out dev_ng scripts	 - Build Tools   			**
:: Updated: 				16/10/2014						            **
:: Edits: 					Path changes					            **
:: Last edited by:			Ross McKinstray  			            	**
::************************************************************************
@Echo off
TITLE %~nx0
@setlocal enableextensions enabledelayedexpansion

set str1=%1

::-set build branch paths
set buildpath=//depot/gta5/build/dev_ng
set exportpath=//depot/gta5/assets_ng/export
::-set TempTU branch paths
set Temppath=//depot/gta5/titleupdate/dev_Temp
set Tempexportpath=//depot/gta5/assets_ng/titleupdate/dev_Temp
::-set titleupdate branch paths
set titleupdatepath=//depot/gta5/titleupdate/dev_ng
set TUexportpath=//depot/gta5/assets_ng/titleupdate/dev_ng
::-set Dev_ng_Live branch paths
set ngLivepath=//depot/gta5/titleupdate/dev_ng_live
set ngLiveexportpath=//depot/gta5/assets_ng/titleupdate/dev_ng_live

::-set Japanese branch paths
if not x%str1:_JPN=%==x%str1% (
set str1=%str1:_JPN=%
::-set build branch paths
set buildpath=//depot/gta5/build/Japanese_ng
set exportpath=//depot/gta5/assets_ng/export
::-set TempTU branch paths
set Temppath=//depot/gta5/titleupdate/Japanese_ng_live
set Tempexportpath=//depot/gta5/assets_ng/titleupdate/dev_Temp
::-set NGLiveupdate branch paths
set ngLivepath=//depot/gta5/titleupdate/Japanese_ng_live
set ngLiveexportpath=//depot/gta5/assets_ng/titleupdate/dev_ng_live
::-set titleupdate branch paths
set titleupdatepath=//depot/gta5/titleupdate/Japanese_ng
set TUexportpath=//depot/gta5/assets_ng/titleupdate/dev_ng
)




IF "%str1%"=="Default" (
@echo on

p4 edit %buildpath%/xboxone/levels/gta5/script/script.rpf
p4 edit %buildpath%/xboxone/levels/gta5/script/script_rel.rpf
p4 edit %buildpath%/ps4/levels/gta5/script/script.rpf
p4 edit %buildpath%/ps4/levels/gta5/script/script_rel.rpf
p4 edit %buildpath%/x64/levels/gta5/script/script.rpf
p4 edit %buildpath%/x64/levels/gta5/script/script_rel.rpf
p4 edit %exportpath%/levels/gta5/script/script.zip
p4 edit %exportpath%/levels/gta5/script/script_rel.zip
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Default script files!
	pause
	EXIT 
)
)

IF "%str1%"=="Default_TU" (
@echo on

p4 edit %titleupdatepath%/xboxone/levels/gta5/script/script.rpf
p4 edit %titleupdatepath%/xboxone/levels/gta5/script/script_rel.rpf
p4 edit %titleupdatepath%/ps4/levels/gta5/script/script.rpf
p4 edit %titleupdatepath%/ps4/levels/gta5/script/script_rel.rpf
p4 edit %titleupdatepath%/x64/levels/gta5/script/script.rpf
p4 edit %titleupdatepath%/x64/levels/gta5/script/script_rel.rpf
p4 edit %TUexportpath%/levels/gta5/script/script.zip
p4 edit %TUexportpath%/levels/gta5/script/script_rel.zip
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Default script files!
	pause
	EXIT 
)
)

IF "%str1%"=="Default_Temp" (
@echo on

p4 edit %Temppath%/xboxone/levels/gta5/script/script.rpf
p4 edit %Temppath%/xboxone/levels/gta5/script/script_rel.rpf
p4 edit %Temppath%/ps4/levels/gta5/script/script.rpf
p4 edit %Temppath%/ps4/levels/gta5/script/script_rel.rpf
p4 edit %Temppath%/x64/levels/gta5/script/script.rpf
p4 edit %Temppath%/x64/levels/gta5/script/script_rel.rpf
p4 edit %Tempexportpath%/levels/gta5/script/script.zip
p4 edit %Tempexportpath%/levels/gta5/script/script_rel.zip
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Default script files!
	pause
	EXIT 
)
)

IF "%str1%"=="Default_NGLIVE" (
@echo on

p4 edit %ngLivepath%/xboxone/levels/gta5/script/script.rpf
p4 edit %ngLivepath%/xboxone/levels/gta5/script/script_rel.rpf
p4 edit %ngLivepath%/ps4/levels/gta5/script/script.rpf
p4 edit %ngLivepath%/ps4/levels/gta5/script/script_rel.rpf
p4 edit %ngLivepath%/x64/levels/gta5/script/script.rpf
p4 edit %ngLivepath%/x64/levels/gta5/script/script_rel.rpf
p4 edit %ngLiveexportpath%/levels/gta5/script/script.zip
p4 edit %ngLiveexportpath%/levels/gta5/script/script_rel.zip
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Default script files!
	pause
	EXIT 
)
)


IF "%str1%"=="Clifford" (
@echo on

p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/ps4/levels/gta5/script/script_debug.rpf
p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/ps4/levels/gta5/script/script_release.rpf
p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/x64/levels/gta5/script/script_release.rpf
p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/x64/levels/gta5/script/script_debug.rpf
p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/xboxone/levels/gta5/script/script_release.rpf
p4 edit //gta5_dlc/sppacks/dlc_agenttrevor/build/dev_ng/xboxone/levels/gta5/script/script_debug.rpf
p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets_ng/export/levels/gta5/script/script_DEBUG.zip
p4 edit //gta5_dlc/spPacks/dlc_agentTrevor/assets_ng/export/levels/gta5/script/script_RELEASE.zip
@echo off
IF %ERRORLEVEL% EQU 1 (
	echo There was a problem checking out Clifford script files!
	pause
	EXIT 
)
)
)
