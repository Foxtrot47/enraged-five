::****************************************************************
:: GRab PrePreBuild Code										**
:: Updated: 									21/08/2014		**
:: Edits:										new				**
:: Last edited by:								Ross McKinstray	**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo Grabbing GTA5_NG_PrePreBuildCode label...
echo.

::Label GTA5 NG
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/xlast/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PrePreBuildCode


POPD

pause