::************************************************************************
:: GTA5 builder - Check out dev_network 360 exes - Build Tools			**
:: Updated: 				12/11/2013						            **
:: Edits: 					Dev_Live mod					            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************
break>>X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Sync_textOutput.txt
@echo off
@echo %2
@echo %1

set Forced=%2
set P4Path=%1

Goto :Grab


:Grab
	@echo on
	p4 sync %Forced%  %P4Path%
	p4 changes -m 1 -s submitted %P4Path% >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Sync_textOutput.txt
	@echo off
	Echo %Forced% %P4Path%
	Goto :quit
