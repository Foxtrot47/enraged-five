﻿using System.Collections.Generic;

namespace MasterCompileTool
{
    class SequentialCompileSetting : IMasterCompileSetting
    {
        public IEnumerable<ICommand> CompileCommand { get; }
        public IEnumerable<ICommand> PostCommand { get; }
        public string Name { get; }

        internal SequentialCompileSetting(IEnumerable<ICommand> Compile, IEnumerable<ICommand> Post, string name)
        {
            CompileCommand = Compile;
            PostCommand = Post;
            Name = name;
        }
    }
}
