﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterCompileTool
{
    internal class EmailSetting
    {
        internal IList<string> Recipients { get; private set; }
        internal IList<string> CCs { get; private set; }
        internal IList<string> BCCs { get; private set; }
        internal string Subject { get; private set; }
        internal string Body { get; private set; }
        internal bool AttachAttachments { get; private set; }
        internal bool EmailOnError { get; private set; }
        internal bool EmailOnAny { get; private set; }
        internal string senderEmail { get; private set; }
        internal string senderName { get; private set; }

        internal EmailSetting()
        {
            Recipients = new List<string>();
            CCs = new List<string>();
            BCCs = new List<string>();
        }
        internal void SetRecipients(IList<string> _recipients)
        {
            Recipients = _recipients;
        }

        internal void AddRecipient(string _recipient)
        {
            Recipients.Add(_recipient);
        }
        internal void SetCCs(IList<string> _ccs)
        {
            CCs = _ccs;
        }

        internal void AddCC(string _ccs)
        {
            CCs.Add(_ccs);
        }
        internal void SetBCCs(IList<string> _bccs)
        {
            BCCs = _bccs;
        }

        internal void AddBCC(string _bccs)
        {
            BCCs.Add(_bccs);
        }
        internal void SetSubject(string _subject)
        {
            Subject = _subject;
        }
        internal void SetBody(string _body)
        {
            Body = _body;
        }

        internal void SetAttachments(bool _attachattachments)
        {
            AttachAttachments = _attachattachments;
        }
        internal void SetEmailOnError(bool _shouldemail)
        {
            EmailOnError = _shouldemail;
        }
        internal void SetEmailOnAny(bool _shouldemail)
        {
            EmailOnAny = _shouldemail;
        }

        internal void SetSenderName(string _sender)
        {
            senderName = _sender;
        }
        internal void SetSenderEmail(string _sender)
        {
            senderEmail = _sender;
        }
    }
}
