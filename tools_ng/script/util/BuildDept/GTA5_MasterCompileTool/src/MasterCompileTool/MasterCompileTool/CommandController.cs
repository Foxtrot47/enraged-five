﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System;

namespace MasterCompileTool
{
    internal class CommandController
    {
        private bool RunCommand(ICommand CmdToRun)
        {
            try
            {
                string OutputFile = Path.Combine(SubstUtility.executableDirectory, $"{CmdToRun.OutputName}.txt");
                string cmdToRun = SubstUtility.SubstString(CmdToRun.CommandToRun);
                string args = SubstUtility.SubstString(CmdToRun.Arguments);
                Process proc = new Process();
                proc.StartInfo.WorkingDirectory = Directory.GetParent(cmdToRun).FullName;
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.Arguments = $"/c \"{cmdToRun} {args} > \"{OutputFile}\"\"";
                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.UseShellExecute = false;
                MasterCompileToolApp.Current.AppLogs.Message($"Starting process {cmdToRun} {args} > \"{OutputFile}\"");
                proc.Start();
                proc.WaitForExit();
                MasterCompileToolApp.Current.AppLogs.Message($"Finished process {cmdToRun} {args} > \"{OutputFile}\"");
                if (proc.ExitCode != 0 && !CmdToRun.SuccessValues.Contains(proc.ExitCode))
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Errored process {cmdToRun} {args} exited with code {proc.ExitCode}");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                MasterCompileToolApp.Current.AppLogs.Error($"Errors occured trying to run cmd {CmdToRun.CommandToRun}: {e.Message}");
                return false;
            }

        }
        private Task<bool> RunSimultaneousCommand(ICommand CmdToRun)
        {
            Task<bool> curTask = Task.Run<bool>(() => RunCommand(CmdToRun));
            return curTask;
        }
        private bool RunSequentialCompile(IEnumerable<ICommand> sequentialCommands)
        {
            List<bool> CompileResults = new List<bool>();

            foreach (ICommand curCommand in sequentialCommands)
            {
                CompileResults.Add(RunCommand(curCommand));
            }

            return !CompileResults.Contains(false);
        }
        private bool RunParallelCompile(IEnumerable<ICommand> parallelCommands)
        {
            List<Task<bool>> CompileTasks = new List<Task<bool>>();
            List<bool> CompileResults = new List<bool>();

            foreach (ICommand curCommand in parallelCommands)
            {
                CompileTasks.Add(RunSimultaneousCommand(curCommand));
            }

            foreach (Task<bool> CompileTask in CompileTasks)
            {
                CompileResults.Add(CompileTask.Result);
            }
            return !CompileResults.Contains(false);
        }
        private bool RunBatchCommand(IEnumerable<ICommand> commands)
        {
            if (commands.First().RunType == RunTypeEnum.Sequential)
            {
                return RunSequentialCompile(commands);
            }
            else
            {
                return RunParallelCompile(commands);
            }
        }
        private bool RunCommandProcessing(IEnumerable<ICommand> commands)
        {
            bool noErroredProcesses = true;
            List<ICommand> batchCommands = new List<ICommand>();
            foreach (ICommand curCommand in commands)
            {
                if (batchCommands.Count == 0)
                {
                    batchCommands.Add(curCommand);
                }
                else
                {
                    if (curCommand.RunType == batchCommands[0].RunType)
                    {
                        batchCommands.Add(curCommand);
                    }
                    else
                    {
                        bool result = RunBatchCommand(batchCommands);
                        if (noErroredProcesses == true)
                        {
                            noErroredProcesses = result;
                        }
                        batchCommands = new List<ICommand>();
                        batchCommands.Add(curCommand);
                    }
                }
            }
            if (batchCommands.Count > 0)
            {
                bool result = RunBatchCommand(batchCommands);
                if (noErroredProcesses == true)
                {
                    noErroredProcesses = result;
                }
            }
            return noErroredProcesses;
        }
        private bool RunMasterCompile(IMasterCompileSetting masterCompiler)
        {
            bool result = RunCommandProcessing(masterCompiler.CompileCommand);
            bool postResult = RunCommandProcessing(masterCompiler.PostCommand);
            return (result == false) ? result : postResult;
        }
        private bool RunSequentialMasterCompile(IEnumerable<IMasterCompileSetting> commands)
        {
            bool result = true;
            foreach (IMasterCompileSetting curCommand in commands)
            {
                bool curResult = RunMasterCompile(curCommand);
                if (result == true)
                {
                    result = curResult;
                }
            }
            return result;
        }
        private bool RunParallelMasterCompile(IEnumerable<IMasterCompileSetting> commands)
        {
            bool result = true;
            List<Task<bool>> tasks = new List<Task<bool>>();
            foreach (IMasterCompileSetting curCommand in commands)
            {
                tasks.Add(Task.Run(() => RunMasterCompile(curCommand)));
            }
            foreach (Task<bool> task in tasks)
            {
                bool curResult = task.Result;
                if (result == true)
                {
                    result = curResult;
                }
            }
            return result;
        }
        private bool RunBatchMasterCompile(IEnumerable<IMasterCompileSetting> commands)
        {
            if (commands.First().GetType() == typeof(SequentialCompileSetting))
            {
                return RunSequentialMasterCompile(commands);
            }
            else
            {
                return RunParallelMasterCompile(commands);
            }
        }
        internal bool RunMasterProcessing(IEnumerable<IMasterCompileSetting> masterSetting)
        {
            bool result = true;
            List<IMasterCompileSetting> batchMasters = new List<IMasterCompileSetting>();
            foreach (IMasterCompileSetting curmaster in masterSetting)
            {
                if (batchMasters.Count == 0)
                {
                    batchMasters.Add(curmaster);
                }
                else
                {
                    if (curmaster.GetType() == batchMasters[0].GetType())
                    {
                        batchMasters.Add(curmaster);
                    }
                    else
                    {
                        bool curResult = RunBatchMasterCompile(batchMasters);
                        if (result == true)
                        {
                            result = curResult;
                        }
                        batchMasters = new List<IMasterCompileSetting>();
                        batchMasters.Add(curmaster);
                    }
                }
            }
            if (batchMasters.Count > 0)
            {
                bool curResult = RunBatchMasterCompile(batchMasters);
                if (result == true)
                {
                    result = curResult;
                }
            }
            return result;
        }
    }
}
