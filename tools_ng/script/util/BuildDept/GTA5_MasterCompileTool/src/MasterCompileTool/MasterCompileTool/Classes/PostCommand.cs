﻿using System.Collections.Generic;

namespace MasterCompileTool
{
    class PostCommand : ICommand
    {
        public string CommandToRun { get; }
        public string Arguments { get; }
        public string OutputName { get; }
        public IEnumerable<int> SuccessValues { get; }
        public RunTypeEnum RunType { get; set; }

        internal PostCommand(string cmd, string args, string output, RunTypeEnum run, IEnumerable<int> SuccessVals = null)
        {
            CommandToRun = cmd;
            Arguments = args;
            OutputName = output;
            RunType = run;
            SuccessValues = SuccessVals;
        }
    }
}
