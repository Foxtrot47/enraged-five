﻿using System.Collections.Generic;

namespace MasterCompileTool
{
    interface ICommand
    {
        string CommandToRun { get; }
        string Arguments { get; }
        string OutputName { get; }
        IEnumerable<int> SuccessValues { get; }
        RunTypeEnum RunType { get; }
    }
}
