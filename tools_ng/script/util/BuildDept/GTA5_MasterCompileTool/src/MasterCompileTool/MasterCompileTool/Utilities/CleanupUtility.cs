﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MasterCompileTool
{
    internal static class CleanupUtility
    {
        internal static void DeleteFiles(IEnumerable<string> filesToDelete)
        {
            foreach(string fileToDelete in filesToDelete)
            {
                if(File.Exists(fileToDelete))
                {
                    File.Delete(fileToDelete);
                }
            }
        }
    }
}
