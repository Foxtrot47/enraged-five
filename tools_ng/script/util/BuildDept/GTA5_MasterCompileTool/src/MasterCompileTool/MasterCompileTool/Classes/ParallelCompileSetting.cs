﻿using System.Collections.Generic;

namespace MasterCompileTool
{
    class ParallelCompileSetting : IMasterCompileSetting
    {
        public IEnumerable<ICommand> CompileCommand { get; }
        public IEnumerable<ICommand> PostCommand { get; }
        public string Name { get; }

        internal ParallelCompileSetting(IEnumerable<ICommand> Compile, IEnumerable<ICommand> Post, string name)
        {
            CompileCommand = Compile;
            PostCommand = Post;
            Name = name;
        }
    }
}
