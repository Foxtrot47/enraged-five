﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Configuration;
using RSG.Editor.Controls;
using System.Threading.Tasks;
using System.IO;
using RSG.Base.Logging;
using RSG.Base;

namespace MasterCompileTool
{
    internal class MasterCompileToolApp : RsConsoleApplication
    {
        private static MasterCompileToolApp _sInstance;
        private string runType;
        internal static MasterCompileToolApp Current => _sInstance;
        internal RSG.Base.Logging.Universal.IUniversalLog AppLogs => Log;
        internal string Studio => _sInstance.CommandOptions.Config.CurrentStudio.Name;

        MasterCompileToolApp()
        {
            _sInstance = this;
        }

        [STAThread]
        static int Main(string[] args)
        {
            MasterCompileToolApp app = new MasterCompileToolApp();
            app.runType = args[0];
            return (app.Run());
        }

        protected override int ConsoleMain()
        {
            string logFile = LogFactory.ApplicationLogFilename;
            IBranch curBranch = this.CommandOptions.Branch;
            SubstUtility.SetUpBranch(curBranch);
            ToolSetting curSetting = MasterCompileParser.LoadSetting(Path.Combine(SubstUtility.executableDirectory, $"Settings_{Studio}.xml"));
            IEnumerable<IMasterCompileSetting> settings = curSetting.MasterCompileSettings[runType];
            IEnumerable<string> commandAttachments = settings.SelectMany(s => s.CompileCommand).Select(p => Path.Combine(SubstUtility.executableDirectory, $"{p.OutputName}.txt"));
            commandAttachments = commandAttachments.Concat(settings.SelectMany(s => s.PostCommand).Select(p => Path.Combine(SubstUtility.executableDirectory, $"{p.OutputName}.txt")));
            commandAttachments = commandAttachments.Concat(new List<string> { Path.Combine(SubstUtility.executableDirectory, Path.GetFileName(logFile)) });
            CleanupUtility.DeleteFiles(commandAttachments);
            if (settings.Count() > 0)
            {
                Dictionary<string, List<IMasterCompileSetting>> settingsMappings = new Dictionary<string, List<IMasterCompileSetting>>();
                foreach (IMasterCompileSetting setting in settings)
                {
                    if (!settingsMappings.ContainsKey(setting.Name))
                    {
                        settingsMappings[setting.Name] = new List<IMasterCompileSetting>();
                    }
                    settingsMappings[setting.Name].Add(setting);
                }
                bool noErrors = true;
                List<Task<bool>> controlTasks = new List<Task<bool>>();
                CommandController cmdController = new CommandController();
                foreach (string settingKey in settingsMappings.Keys)
                {
                    controlTasks.Add(Task.Run(() => cmdController.RunMasterProcessing(settingsMappings[settingKey])));
                }
                foreach (Task<bool> curControlTask in controlTasks)
                {
                    bool curResult = curControlTask.Result;
                    if (noErrors == true)
                    {
                        noErrors = curResult;
                    }
                }
                LogFactory.FlushApplicationLog();
                File.Copy(logFile, Path.Combine(SubstUtility.executableDirectory, Path.GetFileName(logFile)));
                foreach (EmailSetting curEmail in curSetting.Emails[runType])
                {
                    EmailController.EmailSend(curEmail, commandAttachments, noErrors);
                }

                return (RSG.Base.OS.ExitCode.Success);
            }
            else
            {
                Log.Error("No settings found to run.");
                return (RSG.Base.OS.ExitCode.Failure);
            }
        }
    }
}
