﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RSG.Configuration;
using System.IO;
using System.Reflection;

namespace MasterCompileTool
{
    internal static class SubstUtility
    {
        internal static IBranch Branch { get; private set; }
        internal static string executableDirectory { get; private set; }
        private static Dictionary<string, string> SubstMapping;

        internal static void SetUpBranch(IBranch _branch)
        {
            Branch = _branch;
            executableDirectory = Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
            string cwd = Directory.GetCurrentDirectory();
            SubstMapping = new Dictionary<string, string>
            {
                { "$(assets)", Branch.Assets },
                { "$(branch)", Branch.Name },
                { "$(build)", Branch.Build},
                { "$(cwd)", cwd },
                { "$(metadata)", Branch.Metadata },
                { "$(rage)", Branch.RageCode },
                { "$(script)", Branch.Script },
                { "$(src)", Branch.Code },
                { "$(studio)", MasterCompileToolApp.Current.Studio },
                { "$(version)", SetVersion(cwd) }
            };
        }

        internal static string SubstString(string input, bool caseSensitive = false)
        {
            string output = (caseSensitive) ? input : input.ToLower();
            foreach(string key in SubstMapping.Keys)
            {
                output = output.Replace(key, SubstMapping[key]);
            }
            return output;
        }

        private static string SetVersion(string cwd)
        {
            string version = "";
            string cwdName = new DirectoryInfo(cwd).Name;
            Regex versionCheck = new Regex("[0-9]+\\.[0-9]+", RegexOptions.IgnoreCase);
            MasterCompileToolApp.Current.AppLogs.Message($"Checking for game_version.txt in subdirectories of {cwd}");
            string relativeGameVersion = Directory.GetFiles(cwd, "game_version.txt", SearchOption.AllDirectories).FirstOrDefault();
            if(relativeGameVersion != null)
            {
                MasterCompileToolApp.Current.AppLogs.Message($"game_version.txt located {relativeGameVersion}");
                version = ExtractVersionFromFile(relativeGameVersion);
            }
            else
            {
                MasterCompileToolApp.Current.AppLogs.Message($"No game_version.txt found in {cwd} or its subdirectories.");
            }
            
            if (version == "")
            {
                MasterCompileToolApp.Current.AppLogs.Message("Version not set. Attempting to set from parent directory.");
                if (versionCheck.Match(cwdName).Success)
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Version being set from directory {cwdName}.");
                    version = cwdName.ToLower();
                }
                else
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Could not determine version from parent folder {cwdName}");
                }
            }
            if(version == "")
            {
                MasterCompileToolApp.Current.AppLogs.Message("Version not set. Attempting to set from version.txt from harddrive.");
                string FileToParse = "";
                string baseversionFile = $"{Branch.Build}/common/data/version.txt";
                if (File.Exists(baseversionFile))
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Found version.txt located {baseversionFile}");
                    FileToParse = baseversionFile;
                }
                else
                {
                    MasterCompileToolApp.Current.AppLogs.Error("Could not find version txt file.");
                    return null;
                }
                version = ExtractVersionFromFile(FileToParse);
            }
            if(version == "")
            {
                MasterCompileToolApp.Current.AppLogs.Message("Version not set. Setting to a default string VERSIONNOTFOUND");
                version = "VERSIONNOTFOUND";
            }
            return version;
        }
        private static string ExtractVersionFromFile(string FileToParse)
        {
            Regex versionCheck = new Regex("([0-9]+\\.[0-9]+|[0-9]+-)", RegexOptions.IgnoreCase);
            MasterCompileToolApp.Current.AppLogs.Message($"Getting contents of file {FileToParse}");
            string[] contents = File.ReadAllLines(FileToParse);
            string version = "";
            int status = -1;

            foreach (string content in contents)
            {
                if (status == -1)
                {
                    if (content.ToLower().Contains("[version_number]"))
                    {
                        MasterCompileToolApp.Current.AppLogs.Message("Located version number flag line");
                        status = 0;
                        continue;
                    }
                }
                if (status == 0)
                {
                    if (versionCheck.Match(content).Success && !content.Trim().StartsWith("#"))
                    {
                        MasterCompileToolApp.Current.AppLogs.Message($"Located potential version number line. Setting version to {content}");
                        version = content.Split('-').First();
                        status = 1;
                        break;
                    }
                }
            }
            return version;
        }
    }
}
