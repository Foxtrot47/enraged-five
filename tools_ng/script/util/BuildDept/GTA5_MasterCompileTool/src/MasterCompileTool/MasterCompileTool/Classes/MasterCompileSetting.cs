﻿using System.Collections.Generic;

namespace MasterCompileTool
{
    interface IMasterCompileSetting
    {
        IEnumerable<ICommand> CompileCommand { get; }
        IEnumerable<ICommand> PostCommand { get; }
        string Name { get; }
    }
}
