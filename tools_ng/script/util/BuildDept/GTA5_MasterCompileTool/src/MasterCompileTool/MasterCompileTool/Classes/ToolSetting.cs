﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterCompileTool
{
    internal class ToolSetting
    {
        internal IDictionary<string, List<IMasterCompileSetting>> MasterCompileSettings { get; private set; }
        internal IDictionary<string, List<EmailSetting>> Emails { get; private set; }

        internal ToolSetting(Dictionary<string, List<IMasterCompileSetting>> _masterCompile, Dictionary<string, List<EmailSetting>> _email)
        {
            MasterCompileSettings = _masterCompile;
            Emails = _email;
        }
    }
}
