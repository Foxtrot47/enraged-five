﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System;

namespace MasterCompileTool
{
    internal static class EmailController
    {
        internal static void EmailSend(EmailSetting emailToSend, IEnumerable<string> commandAttachments, bool buildNoErrors)
        {
            if (buildNoErrors == false && (emailToSend.EmailOnError == false && emailToSend.EmailOnAny == false))
            {
                MasterCompileToolApp.Current.AppLogs.Message("Build has errored and email set to do not email on error... skipping.");
                return;
            }
            if (buildNoErrors == true && emailToSend.EmailOnError == true)
            {
                MasterCompileToolApp.Current.AppLogs.Message("Build has no errors and email set to email on error... skipping.");
                return;
            }
            try
            {
                MasterCompileToolApp.Current.AppLogs.Message("Creating email");
                MailMessage mail = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.rockstar.t2.corp";
                mail.Subject = SubstUtility.SubstString(emailToSend.Subject, true);
                foreach (string recipient in emailToSend.Recipients)
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Adding recipient {recipient}");
                    mail.To.Add(recipient);
                }
                foreach (string recipient in emailToSend.CCs)
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Adding cc {recipient}");
                    mail.CC.Add(recipient);
                }
                foreach (string recipient in emailToSend.BCCs)
                {
                    MasterCompileToolApp.Current.AppLogs.Message($"Adding bcc {recipient}");
                    mail.Bcc.Add(recipient);
                }
                MasterCompileToolApp.Current.AppLogs.Message($"Setting sender to {emailToSend.senderName}({emailToSend.senderEmail})");
                mail.From = new MailAddress(emailToSend.senderEmail, emailToSend.senderName);
                mail.Body = SubstUtility.SubstString(emailToSend.Body, true);
                mail.IsBodyHtml = true;
                if (emailToSend.AttachAttachments)
                {
                    foreach (string commandAttachment in commandAttachments)
                    {
                        MasterCompileToolApp.Current.AppLogs.Message($"Adding attachment {commandAttachment}");
                        if (File.Exists(commandAttachment))
                        {
                            mail.Attachments.Add(new Attachment(commandAttachment));
                        }
                        else
                        {
                            MasterCompileToolApp.Current.AppLogs.Warning($"Cannot find attachment {commandAttachment}");
                        }
                    }
                }
                smtp.Send(mail);
            }
            catch(Exception e)
            {
                MasterCompileToolApp.Current.AppLogs.Error($"Error during email creation: {e.Message}");
            }
        }
    }
}
