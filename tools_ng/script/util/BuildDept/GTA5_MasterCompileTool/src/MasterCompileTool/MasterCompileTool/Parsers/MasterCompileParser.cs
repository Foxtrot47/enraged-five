﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace MasterCompileTool
{
    internal static class MasterCompileParser
    {
        private static readonly string _settingTag = "masterdef";
        private static readonly string _overallSettingTag = "MasterSetting";
        private static readonly string _settingName = "name";
        private static readonly string _platform = "platform";
        private static readonly string _type = "type";
        private static readonly string _compilecommands = "compilecommands";
        private static readonly string _compilecommand = "compilecommand";
        private static readonly string _function = "program";
        private static readonly string _arguments = "args";
        private static readonly string _outputName = "outputfilename";
        private static readonly string _postcommands = "postcommands";
        private static readonly string _postcommand = "postcommand";
        private static readonly string _emailer = "email";
        private static readonly string _recipient = "addrecipient";
        private static readonly string _cc = "addcc";
        private static readonly string _bcc = "addbcc";
        private static readonly string _emailsubject = "subject";
        private static readonly string _emailbody = "body";
        private static readonly string _addAttachments = "attachments";
        private static readonly string _emailOnAny = "emailonany";
        private static readonly string _emailOnError = "emailonerror";
        private static readonly string _senderName = "name";
        private static readonly string _senderAddress = "sender";
        private static readonly string _exitCode = "exitcode";

        internal static ToolSetting LoadSetting(string file)
        {
            Dictionary<string, List<IMasterCompileSetting>> ParsedSettings = new Dictionary<string, List<IMasterCompileSetting>>();
            Dictionary<string, List<EmailSetting>> parsedEmails = new Dictionary<string, List<EmailSetting>>();
            try
            {
                XDocument settingsDoc = XDocument.Load(file);
                foreach (XElement OverallSetting in settingsDoc.Root.Elements(_overallSettingTag))
                {
                    string settingName = OverallSetting.Attribute(_settingName).Value;
                    ParsedSettings[settingName] = new List<IMasterCompileSetting>();
                    parsedEmails[settingName] = new List<EmailSetting>();
                    IEnumerable<XElement> Settings = OverallSetting.Elements(_settingTag);

                    foreach (XElement Setting in Settings)
                    {
                        
                        IMasterCompileSetting curSetting;
                        string platform = Setting.Attribute(_platform).Value;
                        RunTypeEnum runType = (RunTypeEnum)Enum.Parse(typeof(RunTypeEnum), Setting.Attribute(_type).Value);
                        List<CompileCommand> compileCommands = new List<CompileCommand>();
                        foreach (XElement compileCommand in Setting.Element(_compilecommands).Elements(_compilecommand))
                        {
                            string function = compileCommand.Element(_function).Value;
                            string arguments = (compileCommand.Element(_arguments) != null) ? compileCommand.Element(_arguments).Value : "";
                            string outputFileName = compileCommand.Element(_outputName).Value.Replace(' ', '_');
                            RunTypeEnum functionRunType = (RunTypeEnum)Enum.Parse(typeof(RunTypeEnum), compileCommand.Attribute(_type).Value);
                            List<int> exitcodes = new List<int>();
                            if (compileCommand.Elements(_exitCode) != null)
                            {
                                exitcodes = compileCommand.Elements(_exitCode).Select(e => int.Parse(e.Value)).ToList();
                            }
                            compileCommands.Add(new CompileCommand(function, arguments, outputFileName, functionRunType, exitcodes));
                        }
                        List<PostCommand> postCommands = new List<PostCommand>();
                        foreach (XElement postCommand in Setting.Element(_postcommands).Elements(_postcommand))
                        {
                            string function = postCommand.Element(_function).Value;
                            string arguments = (postCommand.Element(_arguments) != null) ? postCommand.Element(_arguments).Value : "";
                            string outputFileName = postCommand.Element(_outputName).Value.Replace(' ', '_');

                            RunTypeEnum functionRunType = (RunTypeEnum)Enum.Parse(typeof(RunTypeEnum), postCommand.Attribute(_type).Value);
                            List<int> exitcodes = new List<int>();
                            if (postCommand.Elements(_exitCode) != null)
                            {
                                exitcodes = postCommand.Elements(_exitCode).Select(e => int.Parse(e.Value)).ToList();
                            }
                            postCommands.Add(new PostCommand(function, arguments, outputFileName, functionRunType, exitcodes));
                        }
                        if (runType == RunTypeEnum.Simultaneous)
                        {
                            curSetting = new ParallelCompileSetting(compileCommands, postCommands, platform);
                            ParsedSettings[settingName].Add(curSetting);
                        }
                        else
                        {
                            curSetting = new SequentialCompileSetting(compileCommands, postCommands, platform);
                            ParsedSettings[settingName].Add(curSetting);
                        }
                    }

                    IEnumerable<XElement> Emails = OverallSetting.Elements(_emailer);

                    foreach (XElement email in Emails)
                    {
                        EmailSetting curEmail = new EmailSetting();

                        if (email.Elements(_recipient) != null)
                        {
                            List<string> recipients = new List<string>();
                            foreach (XElement recipient in email.Elements(_recipient))
                            {
                                if (!string.IsNullOrWhiteSpace(recipient.Value))
                                {
                                    recipients.Add(recipient.Value);
                                }
                            }
                            if (recipients.Count > 0)
                            {
                                curEmail.SetRecipients(recipients);
                            }
                        }

                        if (email.Elements(_cc) != null)
                        {
                            List<string> ccs = new List<string>();
                            foreach (XElement cc in email.Elements(_cc))
                            {
                                if (!string.IsNullOrWhiteSpace(cc.Value))
                                {
                                    ccs.Add(cc.Value);
                                }
                            }
                            if (ccs.Count > 0)
                            {
                                curEmail.SetCCs(ccs);
                            }
                        }

                        if (email.Elements(_bcc) != null)
                        {
                            List<string> bccs = new List<string>();
                            foreach (XElement bcc in email.Elements(_bcc))
                            {
                                if (!string.IsNullOrWhiteSpace(bcc.Value))
                                {
                                    bccs.Add(bcc.Value);
                                }
                            }
                            if (bccs.Count > 0)
                            {
                                curEmail.SetBCCs(bccs);
                            }
                        }
                        curEmail.SetSubject(email.Element(_emailsubject).Value);
                        curEmail.SetBody(email.Element(_emailbody).Value);

                        if (email.Attribute(_addAttachments) != null)
                        {
                            curEmail.SetAttachments(bool.Parse(email.Attribute(_addAttachments).Value));
                        }
                        else
                        {
                            curEmail.SetAttachments(false);
                        }
                        if (email.Attribute(_emailOnError) != null)
                        {
                            curEmail.SetEmailOnError(bool.Parse(email.Attribute(_emailOnError).Value));
                        }
                        else
                        {
                            curEmail.SetEmailOnError(false);
                        }
                        if (email.Attribute(_emailOnAny) != null)
                        {
                            curEmail.SetEmailOnAny(bool.Parse(email.Attribute(_emailOnAny).Value));
                        }
                        else
                        {
                            curEmail.SetEmailOnAny(false);
                        }
                        curEmail.SetSenderEmail(email.Element(_senderAddress).Value);
                        curEmail.SetSenderName(email.Element(_senderAddress).Attribute(_senderName).Value);

                        parsedEmails[settingName].Add(curEmail);
                    }
                }
            }
            catch(Exception e)
            {
                MasterCompileToolApp.Current.AppLogs.Error("Unable to parse settings file.");
                MasterCompileToolApp.Current.AppLogs.Error(e.Message);
                ParsedSettings = new Dictionary<string, List<IMasterCompileSetting>>();
                parsedEmails = new Dictionary<string, List<EmailSetting>>();
            }

            return new ToolSetting(ParsedSettings, parsedEmails);
        }
    }
}
