@echo off
pushd %~dp0
if "%cd%" == "%RS_TOOLSROOT%\script\util\BuildDept\GTA5_MasterCompileTool\bin" goto samelocationerror
echo.Running Master Compiler.
call %RS_TOOLSROOT%\script\util\BuildDept\GTA5_MasterCompileTool\bin\MasterCompileTool.exe %1
goto complete

:samelocationerror
echo.Bat file appears to be running from the tool folder. Please copy it into a relevant location then run again.
pause

:complete
popd