@echo off
setlocal

set runtool=%DurangoXDK%\bin\xbapp.exe
set conntool=%DurangoXDK%\bin\xbconnect.exe

title Installing Durnago DLC...
echo.

if not exist "%runtool%" call :error "Xbox One XDK or support tools not installed as expected"
if errorlevel 1 exit /b %errorlevel%

cd /d %~dp0
set deploylist=
if not "%1"=="" (
if exist "%1" (
set deploylist=%1
goto multideploy
) else (
set defaddr=%1
goto setaddr
)
) else (
if exist "deploylist-xb1.txt" set deploylist=deploylist-xb1.txt
)
for /f usebackq^ tokens^=2^ delims^=^" %%a in (`"%conntool%"`) do if not "%%a"=="" set defaddr=%%a
if "%defaddr%"=="" call :error "Could not find default console; check that you are properly connected."
echo.
echo Enter the address of the Xbox One console to install to, 
set /p conaddr= or press [Enter] for the default console [%defaddr%]: 
:setaddr
if "%conaddr%"=="" set conaddr=%defaddr%

:install
title Installing GTA5 DLC for Xbox One to [%conaddr%]...
echo.
echo Installing GTA5 DLC for Xbox One to [%conaddr%]...
echo.
echo.
set starttime=%date% %time%

::INSTALL THE DLC TO THE CONSOLE
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.BUSINESS2_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.CHRISTMAS_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.MPBEACH_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.MPBUSINESS_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.HIPSTER_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.VALENTINES_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC.MPHEISTS_1.0.0.0_neutral__fy653z9hy7gqj
"%runtool%" install /x:%conaddr% "%cd%\GTAV.DLC._1.0.0.0_neutral__fy653z9hy7gqj

if not %errorlevel%==0 call :error "Failed to install DLC"
if not %errorlevel%==0 exit /b %errorlevel%

title Installing GTA5 DLC for Xbox One to [%conaddr%]...
echo.
echo Started:  %starttime%
echo Finished: %date% %time%
echo.
echo DLC installed successfully

echo.
pause
exit 0

:error
echo.
echo Error Installing Durango DLC!
echo.
echo. ^> %~1
echo.
pause
exit 1

:multideploy
::for /f "eol=#" %%a in (%deploylist%) do (
::if not "%%a"=="" (
::echo.
::echo Initiating deployment to [%%a]...
::start "" "%~dpnx0" %%a
::)
::)
echo.
pause
exit 0
