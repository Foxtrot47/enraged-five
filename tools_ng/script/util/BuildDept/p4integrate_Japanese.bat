@ECHO OFF
TITLE GTAV - Auto copy of "dev" data to "Japanese"
ECHO GTAV - auto copy of "dev" data to "Japanese"

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat


	p4 copy -v //depot/gta5/build/dev/common/... //depot/gta5/build/Japanese/common/...
	p4 copy -v //depot/gta5/build/dev/ps3/... //depot/gta5/build/Japanese/ps3/...
	p4 copy -v //depot/gta5/build/dev/xbox360/... //depot/gta5/build/Japanese/xbox360/...
	p4 copy -v //depot/gta5/build/dev/TROPDIR/... //depot/gta5/build/Japanese/TROPDIR/...

:: add exes integrate also, we'll remove this when building japan only exes	
	p4 copy -v //depot/gta5/build/dev/game_psn_beta_snc.* //depot/gta5/build/Japanese/game_psn_beta_snc.*
	p4 copy -v //depot/gta5/build/dev/game_psn_bankrelease_snc.* //depot/gta5/build/Japanese/game_psn_bankrelease_snc.*
	p4 copy -v //depot/gta5/build/dev/game_psn_release_snc.* //depot/gta5/build/Japanese/game_psn_release_snc.*
	p4 copy -v //depot/gta5/build/dev/game_psn_final_snc.* //depot/gta5/build/Japanese/game_psn_final_snc.*
	
	p4 copy -v //depot/gta5/build/dev/game_xenon_beta.* //depot/gta5/build/Japanese/game_xenon_beta.*
	p4 copy -v //depot/gta5/build/dev/game_xenon_bankrelease.* //depot/gta5/build/Japanese/game_xenon_bankrelease.*
	p4 copy -v //depot/gta5/build/dev/game_xenon_release.* //depot/gta5/build/Japanese/game_xenon_release.*
	p4 copy -v //depot/gta5/build/dev/game_xenon_final.* //depot/gta5/build/Japanese/game_xenon_final.*

::submit
	p4 submit -d "Auto-copy of head "dev" data to "Japanese"" //depot/gta5/build/Japanese/...


exit
