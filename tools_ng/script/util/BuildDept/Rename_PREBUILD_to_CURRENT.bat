::************************************************************************
:: Set active build folder												**
:: Updated: 				02/02/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		-											**
::************************************************************************
@echo off
echo.

echo Setting active CURRENT build.

:: === directory setup === ::
set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB_TEST
set ntwrkToolsDir=N:\RSGEDI\Distribution\QA_Build\gta5\tools
set currbuild=%baseNtwrkPath%\CURRENT
set newbuild=%baseNtwrkPath%\PREBUILD


:: === Read version.txt === ::
set ver=NUL
if exist %curbuild%\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (%currbuild%\dev\common\data\version.txt) DO (
		set ver=%%G
		GOTO PRINT
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
	pause
	exit
)
	
	
:PRINT
echo Current QA build is Version=%ver%
GOTO RENAME


:RENAME
if exist %currbuild%\ (
	RENAME %currbuild% "Version%ver%"
)
if exist %newbuild%\ (
	RENAME %newbuild% "CURRENT"
)


pause
exit


