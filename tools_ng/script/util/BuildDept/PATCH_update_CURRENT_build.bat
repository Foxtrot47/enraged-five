::************************************************************************
:: Patch update CURRENT build (direct)														**
:: Updated: 				02/02/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		-											**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\CURRENT\dev

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Patching CURRENT build ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat

echo ###########################################
echo ## Patching CURRENT Build			    ####
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started CURRENT build patch update -- >> %outFile%

robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\pc\ %copydir%\pc\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_final_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_win32_beta_dx11.*
echo CURRENT build updated 
echo -- CURRENT build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
::erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

echo Finished Updating CURRENT build!
echo -- Finished Updating CURRENT build! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
:: echo Sending completion email...
:: call C:\GTA5_QACopier\Blat.bat


pause
exit


