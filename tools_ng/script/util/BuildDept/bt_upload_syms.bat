@echo off
setlocal enabledelayedexpansion

:: Run this directly for interactive mode, or pass in <branch> and <version> for immediate upload
:: <branch> is usually dev_gen9_sga or dev_gen9_sga_live; jpn branches are identical; try to avoid redundant uploads
:: <version> is the version number without the 'v' eg, 180.0

set project=gta5
set branches=dev_gen9_sga dev_gen9_sga_live
set branch=
set version=

if not "%~1"=="" set branch=%~1
if not "%~2"=="" set version=%~2
if not "%version%"=="" goto :pkgversionset

echo Available packages for %project%:
for %%b in (%branches%) do (
echo.
echo.  %%b
set pkgv_%%b=180.0
pushd n:\projects\%project%\package\%%b
for /d %%d in (v*) do (
set pkgname=%%d
set pkgname=!pkgname:-%%b=!
set pkgname=!pkgname:v=!
set pkgv_%%b=!pkgname!
echo.    !pkgname!
)
popd
)

echo.&&echo --------&&echo.
echo. 1 - dev_gen9_sga
echo. 2 - dev_gen9_sga_live
echo.
if "%branch%"=="" set branch=dev_gen9_sga
set /p branchnum=Select package branch [1]: 
if "%branchnum%"=="2" set branch=dev_gen9_sga_live

set /p version=Package version number [!pkgv_%branch%!]: 
if "%version%"=="" set version=!pkgv_%branch%!

:pkgversionset
set platpath=n:\projects\%project%\package\%branch%\v%version%-%branch%\scarlett

if not exist "%platpath%" echo.&&echo *** Error: Package not found [%platpath%]&&pause&&exit 1 /b

set zipname=btsym_%project%_%branch%_%version%_master.zip
set zipname_final=btsym_%project%_%branch%_%version%_final.zip

if exist "%temp%\%zipname%" del /f /q "%temp%\%zipname%"
if exist "%temp%\%zipname_final%" del /f /q "%temp%\%zipname_final%"

echo.
pushd "%platpath%\pdb"
zip -D -0 "%temp%\%zipname%" "game_scarlett_master_clang.pdb"
zip -D -0 "%temp%\%zipname_final%" "game_scarlett_final_clang.pdb"
popd
pushd "%platpath%\game"
zip -D -0 "%temp%\%zipname%" "game_scarlett_master_clang.exe"
zip -D -0 "%temp%\%zipname_final%" "game_scarlett_final_clang.exe"
popd
echo.

pushd "%temp%"
echo on
curl -v -k --data-binary "@%zipname%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=c2a3a39a1a9835555605d2f0c25f318bd6f0630970e953f5f290a88ee256c40a"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
curl -v -k --data-binary "@%zipname_final%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=c2a3a39a1a9835555605d2f0c25f318bd6f0630970e953f5f290a88ee256c40a"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
@echo off
popd

del /f /q "%temp%\%zipname%"
del /f /q "%temp%\%zipname_final%"
popd

echo.&&echo Symbol upload completed.
pause
