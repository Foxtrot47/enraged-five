::************************************************
:: GTA5 Label levdes PT batch file              **
:: Updated: 				20/04/2012			**
:: Last edited by:			Ross McKinstray		**
::************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING LABELLED BUILD
echo.

P4 labelsync -l GTAV_current_LD_milestone //depot/gta5/build/release/...
P4 labelsync -l GTAV_current_LD_milestone //depot/gta5/src/release/...
P4 labelsync -l GTAV_current_LD_milestone //depot/gta5/xlast/...
P4 labelsync -l GTAV_current_LD_milestone //depot/gta5/tools/...
P4 labelsync -l GTAV_current_LD_milestone //rage/gta5/release/...
P4 labelsync -l GTAV_current_LD_milestone //ps3sdk/...
POPD

pause