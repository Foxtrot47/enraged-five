::************************************************
:: NG GTA5 Symbol Backup batch file				**
:: Updated: 				14/04/2014			**
:: Last edited by:			Ross McKinstray		**
:: Last edits:				new for NG			**
::************************************************
@echo off
echo.
cd /d %~dp0

set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\Symbols_NG
set builddir=X:\gta5\build\dev_ng
set path="C:\Program Files (x86)\WinRAR\";%path%

set ver=NUL
FOR /F "eol=# skip=3" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO MAIN
	
:MAIN

if exist %copydir%\version%ver% (
	echo This version folder already exists, please manually back up these symbols!
) ELSE (
	echo Copying symbols to the network
	mkdir "%copydir%/version%ver%"
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_durango_*.*
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_orbis_*.*
	rar a %copydir%/version%ver%/symbols.rar %builddir%\game_win64_*.*
)




pause

