@echo off

set branch=%~1

:: Read version.txt for version number.
FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%branch%\common\data\version.txt) DO (
	set ver=%%G
	GOTO GAME_VERSION
)

:GAME_VERSION
echo %ver% > X:\gta5\build\disk_images\gen9\package\%branch%\gta5-prospero.update\game_version.txt
echo %ver% > X:\gta5\build\disk_images\gen9\package\%branch%\gta5-scarlett.update\game_version.txt

set project=gta5
set location=edi

set intchk=n

set platform=prospero
if exist "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt" del /f "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt"
if exist "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt" del /f "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt"
if /i "%intchk%"=="y" (
echo.&&echo Creating manifests for %project%/%branch%/%platform% in [%location%]...
pushd "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update"
"%~dp0recursive-hash-36.exe" "game" "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt"
"%~dp0recursive-hash-36.exe" "tools" "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt"
popd
)
echo.&&echo Staging %project%/%branch%/%platform% to [%location%]...
robocopy "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update" "n:\transfer\rsg%location%\resilio\%project%\gen9_packaged_builds\%branch%\PREBUILD_TU\%platform%" /mir /dcopy:t /xd .sync installer /xf bugs.log changes.log gta5-prospero.update_update_packlog.txt gta5-prospero.update_update_report.txt gta5-prospero.update_update_stats.txt
robocopy "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update" "n:\Projects\GTA5\Package\%branch%\PREBUILD\%platform%" /mir /dcopy:t /xd .sync installer /xf bugs.log changes.log gta5-prospero.update_update_packlog.txt gta5-prospero.update_update_report.txt gta5-prospero.update_update_stats.txt

set platform=scarlett
if exist "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt" del /f "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt"
if exist "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt" del /f "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt"
if /i "%intchk%"=="y" (
echo.&&echo Creating manifests for %project%/%branch%/%platform% in [%location%]...
pushd "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update"
"%~dp0recursive-hash-36.exe" "game" "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-game.txt"
"%~dp0recursive-hash-36.exe" "tools" "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update\manifest-tools.txt"
popd
)
echo.&&echo Staging %project%/%branch%/%platform% to [%location%]...
robocopy "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update" "n:\transfer\rsg%location%\resilio\%project%\gen9_packaged_builds\%branch%\PREBUILD_TU\%platform%" /mir /dcopy:t /xd .sync installer /xf bugs.log changes.log gta5-scarlett.update_update_packlog.txt gta5-scarlett.update_update_report.txt gta5-scarlett.update_update_stats.txt 
robocopy "x:\%project%\build\disk_images\gen9\package\%branch%\gta5-%platform%.update" "n:\Projects\GTA5\Package\%branch%\PREBUILD\%platform%" /mir /dcopy:t /xd .sync installer /xf bugs.log changes.log gta5-scarlett.update_update_packlog.txt gta5-scarlett.update_update_report.txt gta5-scarlett.update_update_stats.txt 

echo.&&echo Done.

:: Exit with explicit success instead of using robocopy's return code
exit 0