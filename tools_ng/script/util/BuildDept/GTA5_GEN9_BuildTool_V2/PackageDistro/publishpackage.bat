@echo off
setlocal enabledelayedexpansion
for /f "skip=1" %%t in ('wmic os get localdatetime') do if not defined filestamp set filestamp=%%t
for /f "usebackq tokens=3,4,* delims=: " %%a in (`netstat -n ^| find ":3389" ^| find "ESTABLISHED"`) do if not "%%b"=="" if "%%a"=="3389" set addr=%%b
for /f "usebackq tokens=1-3,*" %%a in (`ping -a -n 1 %addr%`) do if /i "%%a"=="pinging" set conn=%%b %%c
if "%conn%"=="" set conn=%computername%
if not "%1"=="" (
set branch=%~1
set version=%~2
)
if "%project%"=="" set project=gta5
if "%branch%"=="" set branch=dev_gen9_sga
if "%version%"=="" set version=
if "%platform%"=="" set platform=
if "%origin%"=="" set origin=gen9_packaged_builds
title Publishing a build...
set trigname=.temp
echo.&&echo.
echo Available studios:
echo.
set studios=all
set allstudios=
if exist "n:\transfer\rsgdnd\resilio\%project%\%origin%\%branch%" if exist \\rsgdndrlo1\%project%_package$ echo dnd&&set allstudios=%allstudios% dnd
::if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%" if exist \\rsgedirlo1\%project%_package$ echo edi&&set allstudios=%allstudios% edi
if exist "n:\transfer\rsgind\resilio\%project%\%origin%\%branch%" if exist \\rsgindrlo1\%project%_package$ echo ind&&set allstudios=%allstudios% ind
if exist "n:\transfer\rsgldn\resilio\%project%\%origin%\%branch%" if exist \\rsgldnrlo1\%project%_package$ echo ldn&&set allstudios=%allstudios% ldn
if exist "n:\transfer\rsglds\resilio\%project%\%origin%\%branch%" if exist \\rsgldsrlo1\%project%_package$ echo lds&&set allstudios=%allstudios% lds
::if exist "n:\transfer\rsglic\resilio\%project%\%origin%\%branch%" if exist \\rsglicdfs2\%project%_package$ echo lic&&set allstudios=%allstudios% lic
if exist "n:\transfer\rsglin\resilio\%project%\%origin%\%branch%" if exist \\rsglinrlo1\%project%_package$ echo lin&&set allstudios=%allstudios% lin
if exist "n:\transfer\rsgnwe\resilio\%project%\%origin%\%branch%" if exist \\rsgnwerlo1\%project%_package$ echo nwe&&set allstudios=%allstudios% nwe
if exist "n:\transfer\rsgnyc\resilio\%project%\%origin%\%branch%" if exist \\rsgnycrlo1\%project%_package$ echo nyc&&set allstudios=%allstudios% nyc
if exist "n:\transfer\rsgsan\resilio\%project%\%origin%\%branch%" if exist \\rsgsanrlo1\%project%_package$ echo san&&set allstudios=%allstudios% san
if exist "n:\transfer\rsgtor\resilio\%project%\%origin%\%branch%" if exist \\rsgtorrlo1\%project%_package$ echo tor&&set allstudios=%allstudios% tor
::echo.&&echo Enter studios (comma or space separated, or 'all' or [Enter] for all):
::set /p studios=:
:getversion
::echo.&&echo Enter build version (typically as 'v9999.00', but can be any valid name):
::set /p version=:
::if "%version%"=="" echo.&&echo Must enter a build version or name.&&goto :getversion
set platforms=all
set allplatforms=
echo.&&echo Available platforms:
if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\ps4" echo ps4&&set allplatforms=%allplatforms% ps4
if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\x64" echo x64&&set allplatforms=%allplatforms% x64
if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\xboxone" echo xboxone&&set allplatforms=%allplatforms% xboxone
if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\yeti" echo yeti&&set allplatforms=%allplatforms% yeti
if /i "%project%"=="gta5" if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\PREBUILD_TU\prospero" echo ps5 / prospero&&set allplatforms=%allplatforms% prospero
if /i "%project%"=="gta5" if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\PREBUILD_TU\scarlett" echo xbs / scarlett&&set allplatforms=%allplatforms% scarlett
if /i not "%project%"=="gta5" if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\PREBUILD_TU\ps5" echo ps5&&set allplatforms=%allplatforms% prospero
if /i not "%project%"=="gta5" if exist "n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\PREBUILD_TU\xbs" echo xbs&&set allplatforms=%allplatforms% scarlett
if "%allplatforms%"=="" echo unknown&&goto:publishit
::echo.&&echo Enter platforms (comma or space separated, or 'all' or [Enter] for all):
::set /p platforms=:
:publishit
echo.&&echo Do you wish to perform an integrity check after the publish is completed?
set /p dochk= [y/n] 
if "%dochk%"=="" set dochk=y
if /i "%dochk%"=="n" set dochk=
if "%studios%"=="" set studios=all
if /i "%studios%"=="all" set studios=%allstudios%
set studios=%studios:,= %
set studios=%studios:;= %
set studios=%studios:	= %
set studios=%studios:  = %
if "%platforms%"=="" set platforms=%allplatforms%
if /i "%platforms%"=="all" set platforms=%allplatforms%
set platforms=%platforms:,= %
set platforms=%platforms:;= %
set platforms=%platforms:	= %
set platforms=%platforms:  = %
if /i "%project%"=="gta5" set platforms=%platforms:xbss=scarlett%
if /i "%project%"=="gta5" set platforms=%platforms:xbsx=scarlett%
if /i "%project%"=="gta5" set platforms=%platforms:xbs=scarlett%
if /i "%project%"=="gta5" set platforms=%platforms:ps5=prospero%
if /i not "%project%"=="gta5" set platforms=%platforms:xbss=xbs%
if /i not "%project%"=="gta5" set platforms=%platforms:xbsx=xbs%
if /i not "%project%"=="gta5" set platforms=%platforms:scarlett=xbs%
if /i not "%project%"=="gta5" set platforms=%platforms:prospero=ps5%
echo.&&echo.&&echo Publishing build [%version%] to..
echo.
for %%s in (%studios%) do (
if /i "%%s"=="dnd" set tgtpath=\\rsgdndrlo1\%project%_package$\%branch%
::if /i "%%s"=="edi" set tgtpath=\\rsgedirlo1\%project%_package$\%branch%
if /i "%%s"=="ind" set tgtpath=\\rsgindrlo1\%project%_package$\%branch%
if /i "%%s"=="ldn" set tgtpath=\\rsgldnrlo1\%project%_package$\%branch%
if /i "%%s"=="lds" set tgtpath=\\rsgldsrlo1\%project%_package$\%branch%
if /i "%%s"=="lic" set tgtpath=\\rsglicdfs2\%project%_package$\%branch%
if /i "%%s"=="lin" set tgtpath=\\rsglinrlo1\%project%_package$\%branch%
if /i "%%s"=="nwe" set tgtpath=\\rsgnwerlo1\%project%_package$\%branch%
if /i "%%s"=="nyc" set tgtpath=\\rsgnycrlo1\%project%_package$\%branch%
if /i "%%s"=="san" set tgtpath=\\rsgsanrlo1\%project%_package$\%branch%
if /i "%%s"=="tor" set tgtpath=\\rsgtorrlo1\%project%_package$\%branch%
if not exist "!tgtpath!\.status" md "!tgtpath!\.status"
if not exist "!tgtpath!\%version%" md "!tgtpath!\%version%"
if "%platforms%"=="all" (
echo %%s
if exist "!tgtpath!\%version%\.publish" del /f /q "!tgtpath!\%version%\.publish"
echo src=%project%\%origin%\%branch%\PREBUILD_TU\>"!tgtpath!\%version%\%trigname%"
echo rqus=%username%>>"!tgtpath!\%version%\%trigname%"
echo rqpb=%computername%>>"!tgtpath!\%version%\%trigname%"
if not "%addr%"=="" echo rqad=%addr%>>"!tgtpath!\%version%\%trigname%"
if not "%conn%"=="" echo rqcn=%conn%>>"!tgtpath!\%version%\%trigname%"
rem if not "%dochk%"=="" echo manifestsrc=game>>"!tgtpath!\%version%\%trigname%"
rem if not "%dochk%"=="" echo manifestchk=manifest-game.txt>>"!tgtpath!\%version%\%trigname%"
ren "!tgtpath!\%version%\%trigname%" .publish
) else (
for %%p in (%platforms%) do (
echo %%s [%%p]
if not exist "!tgtpath!\%version%\%%p" md "!tgtpath!\%version%\%%p"
if exist "!tgtpath!\%version%\%%p\.publish" del /f /q "!tgtpath!\%version%\%%p\.publish"
echo src=%project%\%origin%\%branch%\PREBUILD_TU\%%p>"!tgtpath!\%version%\%%p\%trigname%"
echo rqus=%username%>>"!tgtpath!\%version%\%%p\%trigname%"
echo rqpb=%computername%>>"!tgtpath!\%version%\%%p\%trigname%"
if not "%addr%"=="" echo rqad=%addr%>>"!tgtpath!\%version%\%%p\%trigname%"
if not "%conn%"=="" echo rqcn=%conn%>>"!tgtpath!\%version%\%%p\%trigname%"
if not "%dochk%"=="" if exist n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\%%p\PREBUILD_TU\manifest-game.txt echo manifestsrc=game>>"!tgtpath!\%version%\%%p\%trigname%"
if not "%dochk%"=="" if exist n:\transfer\rsgedi\resilio\%project%\%origin%\%branch%\%%p\PREBUILD_TU\manifest-game.txt echo manifestchk=manifest-game.txt>>"!tgtpath!\%version%\%%p\%trigname%"
ren "!tgtpath!\%version%\%%p\%trigname%" .publish
)))

echo.&&echo Triggers have been set.
echo Publishing will begin shortly at those locations.
echo.
pause
exit 0

