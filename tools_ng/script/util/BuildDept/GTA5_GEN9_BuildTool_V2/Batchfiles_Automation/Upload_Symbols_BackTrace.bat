@echo off
setlocal enabledelayedexpansion

@echo off
echo.

set startdir=%~dp0

if "%1%"=="dev_gen9_sga" (
set builddir=X:\gta5\titleupdate\dev_gen9_sga
)
if "%1%"=="dev_gen9_sga_live" (
set builddir=X:\gta5\titleupdate\dev_gen9_sga_live
)
if "%1%"=="Japanese_gen9_sga" (
set builddir=X:\gta5\titleupdate\Japanese_gen9_sga
)
if "%1%"=="Japanese_gen9_sga_live" (
set builddir=X:\gta5\titleupdate\Japanese_gen9_sga
)
FOR /F "eol=# skip=3 delims=-" %%G IN (%builddir%\common\data\version.txt) DO (
	set version=%%G
	GOTO PRINT
)

:PRINT
echo version=%version%
GOTO :UploadSymbols

:UploadSymbols

set platpath=%builddir%
set branch=%1%
set project=gta5

if not exist "%platpath%" echo.&&echo *** Error: Package not found [%platpath%]&&pause&&exit 1 /b

set zipname=btsym_%project%_%branch%_%version%_master.zip
set zipname_final=btsym_%project%_%branch%_%version%_final.zip

if exist "%temp%\%zipname%" del /f /q "%temp%\%zipname%"
if exist "%temp%\%zipname_final%" del /f /q "%temp%\%zipname_final%"

echo.
pushd "%platpath%"
zip -D -0 "%temp%\%zipname%" "game_scarlett_master_clang.pdb"
zip -D -0 "%temp%\%zipname_final%" "game_scarlett_final_clang.pdb"
popd
pushd "%platpath%"
zip -D -0 "%temp%\%zipname%" "game_scarlett_master_clang.exe"
zip -D -0 "%temp%\%zipname_final%" "game_scarlett_final_clang.exe"
popd
echo.

pushd "%temp%"
echo on
curl -v -k --data-binary "@%zipname%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=c2a3a39a1a9835555605d2f0c25f318bd6f0630970e953f5f290a88ee256c40a"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
curl -v -k --data-binary "@%zipname_final%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=c2a3a39a1a9835555605d2f0c25f318bd6f0630970e953f5f290a88ee256c40a"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
@echo off
popd

del /f /q "%temp%\%zipname%"
del /f /q "%temp%\%zipname_final%"
popd

echo.&&echo Symbol upload completed.
