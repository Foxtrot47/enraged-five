:: npConfig Updater ::

@echo off

set branch=%1
set tag=%2

if [%branch%]==[] set branch=dev_gen9_sga
if not [%tag%]==[] set tag="?npConfigTagId=%2"

echo.
echo branch = %branch% 
echo tag = %tag%
echo.

set base_url=https://s2s.sp-int.playstation.net/api
set auth=Basic MTYzMjBiNWQtNmEwNi00YTQyLTgxNTAtMzBiOTlhNWI0OTdkOlhzaXV3TEZYaURJVTN3WHI=

echo. 
echo Getting access token...
echo. 

for /f tokens^=1-4^ delims^=^" %%A in ('curl -X POST %base_url%/authz/v3/oauth/token -H "Authorization:%auth%" -d "grant_type=client_credentials&scope=psn:backoffice"') do (
	set token=%%~D
)

echo.
echo Access token = %token% 
echo. 
echo Getting npConfig files...
echo. 

curl --output eu_digital.zip %base_url%/gemsTool/v1/titles/PPSA01721_00/npConfig%tag% -H "Authorization:Bearer %token%"
curl --output us_digital.zip %base_url%/gemsTool/v1/titles/PPSA03420_00/npConfig%tag% -H "Authorization:Bearer %token%"
curl --output jp_digital.zip %base_url%/gemsTool/v1/titles/PPSA03421_00/npConfig%tag% -H "Authorization:Bearer %token%"
curl --output eu_disk.zip %base_url%/gemsTool/v1/titles/PPSA04263_00/npConfig%tag% -H "Authorization:Bearer %token%"
curl --output us_disk.zip %base_url%/gemsTool/v1/titles/PPSA04264_00/npConfig%tag% -H "Authorization:Bearer %token%"
curl --output jp_disk.zip %base_url%/gemsTool/v1/titles/PPSA04265_00/npConfig%tag% -H "Authorization:Bearer %token%"

for %%x in (eu_digital us_digital jp_digital eu_disk us_disk jp_disk) do (

	echo.
	echo Checking out files...
	echo.

	P4 edit //depot/gta5/titleupdate/%branch%/ps5_config/sce_sys_%%x/trophy2/...
	P4 edit //depot/gta5/titleupdate/%branch%/ps5_config/sce_sys_%%x/uds/...
	P4 edit //depot/gta5/titleupdate/%branch%/ps5_config/sce_sys_%%x/nptitle.dat
	P4 edit //depot/gta5/titleupdate/%branch%/ps5_config/sce_sys_%%x/param_cp_values.json
	
	echo.
	echo Extracting and updating files...
	echo.
	
	unzip -o %%x.zip -d "X:\gta5\titleupdate\%branch%\ps5_config\sce_sys_%%x"
	
	robocopy /s /e X:\gta5\titleupdate\%branch%\ps5_config\sce_sys_%%x\sce_sys X:\gta5\titleupdate\%branch%\ps5_config\sce_sys_%%x
	
	echo.
	echo Cleaning up...
	echo.
	
	rmdir /s /q X:\gta5\titleupdate\%branch%\ps5_config\sce_sys_%%x\sce_sys
	del %%x.zip
)
