@echo off
setlocal

set config=beta
if NOT [%1] == [] set config=%1

if "%2%"=="dev_gen9_sga" set branch=dev_gen9_sga
if "%2%"=="dev_gen9_sga_live" set branch=dev_gen9_sga_live
if "%2%"=="dev_gen9_sga_merge" set branch=dev_gen9_sga_merge
if "%2%"=="japanese_gen9_sga" set branch=japanese_gen9_sga
if "%2%"=="japanese_gen9_sga_live" set branch=japanese_gen9_sga_live

if ["%GameDK%"] == [""] echo GDK isn't installed, please install the latest recommended version and try again && exit /b 1

path %GameDK%\bin;%path%

pushd %~dp0..\..\..\..\..\..\titleupdate\%branch%\

set MSconfig=scarlett_loose\MicrosoftGame.Config
call :fullpath MSconfig %MSconfig%
if NOT exist %MSconfig% echo Failed to find %MSconfig% && goto :error

for /f "tokens=1,2,3" %%a in ('X:\gta5\titleupdate\%branch%\launchers\GetFamilyName %MSconfig%') DO set fullname=%%a&&set family=%%b&& set status=%%c
if [%fullname%] == [] echo failed to get fullname (%status%) && goto :error
if [%fullname%] == [error] echo failed to get fullname (%status%) && goto :error
if [%family%] == [] echo failed to get family (%status%) && goto :error
if [%family%] == [error] echo failed to get family (%status%) && goto :error

echo Terminate %fullname%
xbapp terminate %fullname%

set dest=xD:\Drives\Development\%family%

xbmkdir %dest%
xbcp scarlett_loose\*.* %dest%\
xbcp game_scarlett_%config%.* %dest%\
xbcp commandline.txt %dest%\
if exist c:\rfs.dat xbcp c:\rfs.dat %dest%\

call :fullpath cmdline ..\commandline.txt
echo Running game_scarlett_%config%.exe
xbapp launch %dest%\game_scarlett_%config%.exe @%cmdline% -unattended || goto :error


waitfor /t 150 buildcache

xbapp terminate 

popd
exit /b 0

:fullpath
set %1=%~dpnx2
exit /b 0

:error
popd
echo FAILED
pause
exit /b 1