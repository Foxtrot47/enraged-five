echo off
echo.

set branch=%1

if [%branch%]==[] set branch=dev_gen9_sga

pushd %~dp0..\..\..\..\..\..\tools_ng\bin\gen9\RPFChecker\

RpfChecker -dump_tree X:\gta5\build\disk_images\gen9\_packlist\prospero\prospero_%branch%_tree.txt X:\gta5\build\disk_images\gen9\package\%branch%\gta5-prospero.update\game
RpfChecker -dump_tree X:\gta5\build\disk_images\gen9\_packlist\scarlett\scarlett_%branch%_tree.txt X:\gta5\build\disk_images\gen9\package\%branch%\gta5-scarlett.update\game

popd 

exit 0