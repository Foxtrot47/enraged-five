echo
 
set build=Build
set tubuild=Tu Build
set rage=Rage
set source=Source
set script=Script
set text=Text
set metadata=MetaData
set export=Export
set parenttxds=ParentTxds
set audio=Audio
set dlc=mpChristmas3
if exist X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt (
	del "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
	:: Strip the CL numbers from the sync.txt and put into a copy and paste format for prebuild mail. 
:BuildCL 
	FOR /F "tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %build%: %%G > "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto TUBUildCL
		)
:TUBuildCL
	FOR /F "skip=1 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %tubuild%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto SourceCL
		)
:SourceCL
	FOR /F "skip=2 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %source%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto RageCL
		)
:RageCL
	FOR /F "skip=3 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %rage%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto ScriptCL
		)
:ScriptCL
	FOR /F "skip=4 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %script%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto TextCL
		)
:TextCL
	FOR /F "skip=5 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %text%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto MetaDataCL
		)
:MetaDataCL
	FOR /F "skip=6 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %metadata%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto ExportCL
		)
:ExportCL
	FOR /F "skip=7 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %export%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto ParentTxdsCL
		)
:ParentTxdsCL
	FOR /F "skip=8 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %parenttxds%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto AudioCL
		)
:AudioCL
	FOR /F "skip=9 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %audio%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto DlcCL
		)
:DlcCL
	FOR /F "skip=10 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %dlc%: %%G >> "N:\RSGEDI\Build Department\gtav_gen9\CL_numbers.txt"
		goto end
		)
) ELSE (
		echo.No sync text file has been found.
)
:end

xcopy "X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero.update\changes.log" "N:\RSGEDI\Build Department\gtav_gen9\changes.log" /Y


