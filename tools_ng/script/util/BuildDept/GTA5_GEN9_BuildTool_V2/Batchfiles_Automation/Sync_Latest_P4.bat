:****************************************************************
:: Sync Latest Data												**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo
echo

PUSHD %RS_PROJROOT%

for /f "tokens=2" %%i in ('p4 changes -m1 -s submitted') DO set latest=%%i

echo.
echo Syncing Latest Data to %latest%
echo.
if "%1%"=="dev_gen9_sga" (
::Sync GTA5 Gen9 SGA
P4 sync //depot/gta5/build/dev_gen9_sga/....@%latest%
P4 sync //depot/gta5/titleupdate/dev_gen9_sga/....@%latest%
P4 sync //depot/gta5/src/dev_gen9_sga/...@%latest%
P4 sync //rage/gta5/dev_gen9_sga/...@%latest%
P4 sync //depot/gta5/script/dev_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/metadata/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml@%latest%
P4 sync //gta5_dlc/mpPacks/mpSum2/build/dev_gen9_sga/...@%latest%
P4 sync //gta5_dlc/mpPacks/mpSum2/assets_gen9_disc/gametext\...@%latest%
P4 sync //gta5_dlc/patchPacks/patchDay27NG/build\dev_gen9_sga\...@%latest%
P4 sync //depot/gta5/build/disk_images/gen9/...@%latest%

::Cleanup Old Sync File
del X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt

:: List Changelists Synced
P4 changes -m1 //depot/gta5/build/dev_gen9_sga/...#have > "X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt"
P4 changes -m1 //depot/gta5/titleupdate/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/src/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //rage/gta5/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/script/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/metadata/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/titleupdate/dev_gen9_sga/*/audio/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt
POPD
pause
exit
)

if "%1%"=="dev_gen9_sga_live" (
::Sync GTA5 Gen9 SGA LIVE
P4 sync //depot/gta5/build/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/src/dev_gen9_sga_live/...@%latest%
P4 sync //rage/gta5/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/script/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/metadata/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml@%latest%
P4 sync //gta5_dlc/mpPacks/mpSum2/build/dev_gen9_sga/...@%latest%
P4 sync //gta5_dlc/mpPacks/mpSum2/build/dev_gen9_sga/...@%latest%
P4 sync //gta5_dlc/mpPacks/mpSum2/assets_gen9_disc/gametext\...@%latest%
P4 sync //gta5_dlc/patchPacks/patchDay27NG/build\dev_gen9_sga\...@%latest%
P4 sync //depot/gta5/build/disk_images/gen9/...@%latest%

::Cleanup Old Sync File
del X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt

:: List Changelists Synced
P4 changes -m1 //depot/gta5/build/dev_gen9_sga_live/...#have > "X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt"
P4 changes -m1 //depot/gta5/titleupdate/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/src/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //rage/gta5/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/script/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/metadata/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml #have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/titleupdate/dev_gen9_sga/*/audio/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_live_sync.txt
POPD
pause
exit
)

if "%1%"=="Japanese_gen9_sga" (
::Sync GTA5 Japanese Gen9 SGA
P4 sync //depot/gta5/build/Japanese_gen9_sga/...@GTA5_JPN_GEN9_PreBuild
P4 sync //depot/gta5/titleupdate/Japanese_gen9_sga/...@GTA5_JPN_GEN9_PreBuild
P4 sync //depot/gta5/src/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //rage/gta5/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/script/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/metadata/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/Japanese_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml@%latest%
P4 sync //depot/gta5/build/disk_images/gen9/...@%latest%

::Cleanup Old Sync File
del X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt

:: List Changelists Synced
P4 changes -m1 //depot/gta5/build/Japanese_gen9_sga/...#have > "X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt"
P4 changes -m1 //depot/gta5/titleupdate/Japanese_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/src/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //rage/gta5/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/script/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/metadata/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/titleupdate/Japanese_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml #have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_sync.txt
POPD
pause
exit
)

if "%1%"=="Japanese_gen9_sga_live" (
::Sync GTA5 Japanese Gen9 SGA
P4 sync //depot/gta5/build/Japanese_gen9_sga_live/...@GTA5_JPN_GEN9_PreBuild
P4 sync //depot/gta5/titleupdate/Japanese_gen9_sga_live/...@GTA5_JPN_GEN9_PreBuild
P4 sync //depot/gta5/src/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //rage/gta5/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/script/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga_live/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/metadata/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/Japanese_gen9_sga/...@%latest%
P4 sync //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml@%latest%
P4 sync //depot/gta5/build/disk_images/gen9/...@%latest%

::Cleanup Old Sync File
del X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt

:: List Changelists Synced
P4 changes -m1 //depot/gta5/build/Japanese_gen9_sga_live/...#have > "X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt"
P4 changes -m1 //depot/gta5/titleupdate/Japanese_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/src/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //rage/gta5/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/script/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/GameText/dev_gen9_sga_live/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/metadata/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/titleupdate/Japanese_gen9_sga/...#have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
P4 changes -m1 //depot/gta5/assets_gen9_sga/maps/ParentTxds.xml #have >> X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\Japanese_gen9_sga_live_sync.txt
POPD
pause
exit
)
