@echo off
echo.

set startdir=%~dp0

set ps5UploadTool=%SCE_PROSPERO_SDK_DIR%\host_tools\bin\prospero-symupload.exe
set gdkUploadTool=%ProgramFiles(x86)%\Windows Kits\10\Debuggers\x64\symstore.exe
set uploadDir=\\rsgdndnas1.rockstar.t2.corp\rsgdnd_symbolserver$\

if "%1%"=="dev_gen9_sga" (
set builddir=X:\gta5\titleupdate\dev_gen9_sga
)
if "%1%"=="dev_gen9_sga_live" (
set builddir=X:\gta5\titleupdate\dev_gen9_sga_live
)
if "%1%"=="Japanese_gen9_sga" (
set builddir=X:\gta5\titleupdate\Japanese_gen9_sga
)
FOR /F "eol=# skip=3 delims=-" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO :UploadProsperoSymbols

:UploadProsperoSymbols
"%ps5UploadTool%" add /f %builddir%\*.elf /s %uploadDir% /compress /tag %ver% /o

:UploadScarletSymbols
"%gdkUploadTool%" add /f %builddir%\game_scarlett*.pdb /s %uploadDir% /t GTAO /v %ver% /compress /o 
"%gdkUploadTool%" add /f %builddir%\game_scarlett*.exe /s %uploadDir% /t GTAO /v %ver% /compress /o

:UploadPCSymbols
"%gdkUploadTool%" add /f %builddir%\game_win64*.pdb /s %uploadDir% /t GTAO /v %ver% /compress /o
"%gdkUploadTool%" add /f %builddir%\game_win64*.exe /s %uploadDir% /t GTAO /v %ver% /compress /o

