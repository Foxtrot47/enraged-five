::************************************************************************
:: GTA5 builder - Copy Deployed to Distro - GTA5_TU_Prebuild	        **
:: Updated: 				03/11/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)
if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)
if "%1%"=="Japanese_gen9_sga_live" (
	set Branch=Japanese_gen9_sga_live
)
:: Read version.txt for version number.
FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
	set ver=%%G
	GOTO GAME_VERSION
)

:GAME_VERSION
echo %ver% > X:\gta5\build\disk_images\gen9\package\%1%\gta5-prospero\game_version.txt
echo %ver% > X:\gta5\build\disk_images\gen9\package\%1%\gta5-scarlett\game_version.txt

:COPY
set VarOne=%1% 
set VarTwo=%2%


if %VarOne%==dev_gen9_sga if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==dev_gen9_sga if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\game\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\tools\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\pdb\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\game\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\tools\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\pdb\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-prospero\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\japanese_gen9_sga\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\game\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\tools\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\pdb\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\japanese_gen9_sga\gta5-scarlett\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\japanese_gen9_sga\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==Japanese_gen9_sga_live if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga_live\PREBUILD\game_version.txt

POPD
exit 0
)

if %VarOne%==Japanese_gen9_sga_live if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\game\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\tools\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\pdb\  /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga_live\PREBUILD\game_version.txt

POPD
exit 0
)