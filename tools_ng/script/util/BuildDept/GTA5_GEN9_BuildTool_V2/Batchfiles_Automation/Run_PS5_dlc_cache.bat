@echo off
setlocal


set config=beta
set sku=sce_sys_eu_digital
if NOT [%1] == [] set config=%1
if "%2%"=="dev_gen9_sga" set branch=dev_gen9_sga
if "%2%"=="dev_gen9_sga_live" set branch=dev_gen9_sga_live
if "%2%"=="dev_gen9_sga_merge" set branch=dev_gen9_sga_merge
if "%2%"=="japanese_gen9_sga" set branch=japanese_gen9_sga

if NOT [%3] == [] set dlcarg1=%3
if NOT [%4] == [] set dlcarg2=%4

pushd %~dp0..\..\..\..\..\..\titleupdate\%branch%\

call :fullpath cmdline commandline.txt
echo Setting sku to %sku%
powershell -command "./ps5_config/mergeparams.ps1 %sku%"
prospero-pub-cmd gp5_dir_add --src_path ps5_config\%sku% --dst_path sce_sys --force ps5_config\configuration.gp5 || goto :error
echo Running game_prospero_%config%.elf
prospero-run /exitOnText:"MainTransition" /gp5File:ps5_config\configuration.gp5 /elf game_prospero_%config%.elf /arg @%cmdline% -unattended -StraightIntoFreemode -extracontent=%dlcarg1% -buildDLCCacheData=%dlcarg2% || goto :error

echo Kill game_prospero_%config%.elf
prospero-run /debug /kill /elf game_prospero_%config%.elf

popd
exit /b 0

:fullpath
set %1=%~dpnx2
exit /b 0

:error
popd
echo **FAILED: %ERRORLEVEL%
exit /b %ERRORLEVEL%