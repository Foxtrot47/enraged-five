::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				23/03/2021						            **
:: Edits: 					Add GEN9 and numver version copy            **
:: Last edited by:			Neil Walker  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)

if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)

if "%1%"=="Japanese_gen9_sga_live" (
	set Branch=Japanese_gen9_sga_live
)
:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set version=v%%G
		echo %version%
		GOTO NETWORKCOPY
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg 1.0-%Branch%):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO NETWORKCOPY

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%


echo Copying %VarTwo% %ver% to the network:

if %VarOne%==dev_gen9_sga if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero.update\installer\ N:\Projects\GTA5\Package\dev_gen9_sga\%version%\prospero\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==dev_gen9_sga if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett.update\installer\ N:\Projects\GTA5\Package\dev_gen9_sga\%version%\scarlett\installer\ /purge /s


POPD
pause
exit 0
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero.update\installer\ N:\Projects\GTA5\Package\dev_gen9_sga_live\%version%\prospero\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett.update\installer\ N:\Projects\GTA5\Package\dev_gen9_sga_live\%version%\scarlett\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero.update\installer\ N:\Projects\GTA5\Package\Japanese_gen9_sga\%version%\prospero\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett.update\installer\ N:\Projects\GTA5\Package\Japanese_gen9_sga\%version%\scarlett\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==Japanese_gen9_sga_live if %VarTwo%==Prospero (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-prospero.update\installer\ N:\Projects\GTA5\Package\Japanese_gen9_sga_live\%version%\prospero\installer\ /purge /s

POPD
pause
exit 0
)

if %VarOne%==Japanese_gen9_sga_live if %VarTwo%==Scarlett (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga_live\gta5-scarlett.update\installer\ N:\Projects\GTA5\Package\Japanese_gen9_sga_live\%version%\scarlett\installer\ /purge /s

POPD
pause
exit 0
)