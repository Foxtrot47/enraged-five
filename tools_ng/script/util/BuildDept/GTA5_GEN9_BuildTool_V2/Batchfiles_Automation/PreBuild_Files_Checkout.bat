::****************************************************************
:: GTA5 Label GEN9 Prebuild batch file							**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING GEN9 PREBUILD LABEL
echo.
if "%1%"=="dev_gen9_sga" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/game_*_*.cmp
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/game_*_*.elf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/game_*_*.exe
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/game_*_*.pdb
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/patch/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/levels/gta5/script/script*.rpf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/dlcPacks/mpChristmas3*/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/dlcPacks/patchDay28*/...
P4 edit //gta5_dlc/mpPacks/mpChristmas3/build/dev_gen9_sga/*/data/lang/...

POPD
exit
)

if "%1%"=="dev_gen9_sga_release" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/build/disk_images/gen9/_packlist/*/*_dev_gen9_sga_tree.txt

POPD
exit
)

if "%1%"=="dev_gen9_sga_live" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/game_*_*.cmp
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/game_*_*.elf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/game_*_*.exe
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/game_*_*.pdb
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/*/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/*/patch/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/*/levels/gta5/script/script*.rpf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/dlcPacks/mpChristmas3*/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/*/dlcPacks/patchDay28*/...
P4 edit //gta5_dlc/mpPacks/mpChristmas3/build/dev_gen9_sga/*/data/lang/...

POPD
exit
)

if "%1%"=="dev_gen9_sga_live_release" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/build/disk_images/gen9/_packlist/*/*_dev_gen9_sga_live_tree.txt

POPD
exit
)

if "%1%"=="Japanese_gen9_sga" (
::Checkout GTA5 JAPANESE GEN9 SGA
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga/common/data/version.txt
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga/*/levels/gta5/script/script*.rpf
POPD
exit
)

if "%1%"=="Japanese_gen9_sga_live" (
::Checkout GTA5 JAPANESE GEN9 SGA LIVE
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga_live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga_live/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/titleupdate/Japanese_gen9_sga_live/*/levels/gta5/script/script*.rpf
POPD
exit
)

if "%1%"=="dev_gen9_sga_merge" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/game_*_*.cmp
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/game_*_*.elf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/game_*_*.exe
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/game_*_*.pdb
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/*/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/*/patch/data/lang/american*.rpf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/*/levels/gta5/script/script*.rpf
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/*/dlcPacks/mpSum2/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_merge/*/dlcPacks/patchDay27NG/...
P4 edit //gta5_dlc/mpPacks/mpSum2/build/dev_gen9_sga/*/data/lang/...

POPD
exit
)