:****************************************************************
:: Sync Latest Data												**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo
echo

PUSHD %RS_PROJROOT%

for /f "tokens=2" %%i in ('p4 changes -m1 -s submitted') DO set latest=%%i

echo.
echo Syncing Latest DLC Data to %latest%
echo.
p4 sync x:\gta5_dlc\mpPacks\mpAirraces\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpAirraces\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpAirraces\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpApartment\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpApartment\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpApartment\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpArc1\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpArc1\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpArc1\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpAssault\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpAssault\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpAssault\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpBattle\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBattle\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBattle\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpBattle_base_mpChristmas2017\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBeach\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBeach\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBeach\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpBiker\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBiker\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBiker\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpBiker_base_mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBiker_base_mplowrider\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBusiness\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBusiness\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBusiness\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpBusiness2\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBusiness2\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpBusiness2\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpChristmas\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2017\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2017\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2017\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2018\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2018\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpChristmas2018\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpExecutive\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpExecutive\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpExecutive\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpExecutive_base_mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpGunrunning\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpGunrunning\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpGunrunning\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpHalloween\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHalloween\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHalloween\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpHeist3\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist3\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist3\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpHeist4\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist4\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHeist4\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpHipster\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHipster\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpHipster\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpImportExport\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpImportExport\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpImportExport\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpImportExport_base_mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpIndependence\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpIndependence\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpIndependence\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpJanuary2016\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpJanuary2016\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpJanuary2016\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpLowrider2\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLowrider2\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLowrider2\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpLowrider\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLowrider\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLowrider\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpLTS\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLTS\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLTS\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpLuxe2\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLuxe2\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLuxe2\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpLuxe\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLuxe\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpLuxe\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpPatchesNG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpPatchesNG\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpPilot\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpPilot\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpPilot\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpReplay\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpReplay\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpReplay\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpSecurity\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSecurity\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSecurity\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpSmuggler\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSmuggler\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSmuggler\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpSmuggler_base_mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSpecialRaces\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSpecialRaces\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSpecialRaces\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpStunt\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpStunt\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpStunt\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpSum\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSum\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpSum\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpTuner\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpTuner\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpTuner\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpValentines2\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpValentines2\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpValentines2\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpValentines\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpValentines\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpValentines\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpVinewood\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpVinewood\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpVinewood\*.xml
p4 sync x:\gta5_dlc\mpPacks\mpVinewood_base_mpHeist\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpxmas_604490\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpxmas_604490\*.xml
p4 sync x:\gta5_dlc\spPacks\spUpgrade\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\spPacks\spUpgrade\*.xml


p4 sync x:\gta5_dlc\patchPacks\patchDay10NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay10NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay11NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay11NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay12NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay12NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay13NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay13NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay14NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay14NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay15NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay15NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay16NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay16NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay17NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay17NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay18NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay18NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay19NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay19NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay1NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay1NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay20NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay20NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay21NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay21NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay22NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay22NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay23NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay23NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay24NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay24NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay25NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay25NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay26NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay26NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay2bNG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay2bNG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay2NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay2NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay3NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay3NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay4NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay4NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay5NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay5NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay6NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay6NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay7NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay7NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay8NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay8NG\*.xml
p4 sync x:\gta5_dlc\patchPacks\patchDay9NG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDay9NG\*.xml

p4 sync x:\gta5_dlc\mpPacks\mpG9EC\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpG9EC\assets_gen9_disc\gametext\...@%latest%
p4 sync x:\gta5_dlc\mpPacks\mpG9EC\*.xml

p4 sync x:\gta5_dlc\patchPacks\patchDayG9ECNG\build\dev_gen9_sga\...@%latest%
p4 sync x:\gta5_dlc\patchPacks\patchDayG9ECNG\*.xml


POPD
pause
exit
)