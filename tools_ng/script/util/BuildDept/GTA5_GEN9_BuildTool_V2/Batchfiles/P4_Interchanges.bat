::****************************************************************
:: Sync Latest Data												**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo off

setlocal enabledelayedexpansion

:SETBRANCHSPEC
echo.&&echo --------&&echo.
echo. "1. dev_ng -> dev_gen9_sga"
echo. "2. dev_ng_live -> dev_gen9_sga_live"
echo. "3. dev_gen9_sga -> dev_gen9_sga_live"
echo. "4. dev_gen9_sga_live -> dev_gen9_sga"
echo.
set /p branchnum=Select branch mapping: 
if "%branchnum%"=="1" set branchspec=gta5_dev_ng_to_dev_gen9_sga
if "%branchnum%"=="2" set branchspec=gta5_dev_ng_live_to_dev_gen9_sga_live
if "%branchnum%"=="3" set branchspec=gta5_dev_gen9_sga_to_dev_gen9_sga_live
if "%branchnum%"=="4" set branchspec=gta5_dev_gen9_sga_live_to_dev_gen9_sga
if "%branchnum%"=="" echo.&&echo You must enter a valid branch mapping.&&goto :SETBRANCHSPEC


:INTERCHANGES
echo.&&echo --------&&echo.
p4 interchanges -f -b %branchspec%

:INTERCHANGES
echo.&&echo --------&&echo.
goto :SETBRANCHSPEC


pause
