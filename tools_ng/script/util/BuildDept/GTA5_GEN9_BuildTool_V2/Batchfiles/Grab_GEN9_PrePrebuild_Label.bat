::****************************************************************
:: GRab PrePreBuild Code										**
:: Updated: 									21/08/2014		**
:: Edits:										new				**
:: Last edited by:								Ross McKinstray	**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo Grabbing GTA5_GEN9_PreBuild label...
echo.

IF "%1%"=="dev_gen9_sga" (
P4 sync //depot/gta5/build/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/titleupdate/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/src/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/script/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/tools_ng/...@GTA5_GEN9_PreBuild
P4 sync //rage/gta5/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/assets_gen9_sga/gametext/dev_gen9_sga/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga/...@GTA5_GEN9_PreBuild
)
POPD
pause
exit

IF "%1%"=="dev_gen9_sga_live" (
P4 sync //depot/gta5/build/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/src/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/script/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/tools_ng/...@GTA5_GEN9_PreBuild
P4 sync //rage/gta5/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync /depot/gta5/assets_gen9_sga/gametext/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
P4 sync //depot/gta5/assets_gen9_sga/titleupdate/dev_gen9_sga_live/...@GTA5_GEN9_PreBuild
POPD
pause
exit
