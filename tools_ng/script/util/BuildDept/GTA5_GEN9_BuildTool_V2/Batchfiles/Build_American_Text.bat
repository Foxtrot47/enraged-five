@echo off
echo.

cd /d %~dp0

if "%1%"=="dev_gen9_sga" (
set titleupdatepath=//depot/gta5/Titleupdate/dev_gen9_sga
set TextPath=X:\gta5\assets_gen9_sga\GameText\dev_gen9_sga
)
if "%1%"=="dev_gen9_sga_live" (
set titleupdatepath=//depot/gta5/Titleupdate/dev_gen9_sga_live
set TextPath=X:\gta5\assets_gen9_sga\GameText\dev_gen9_sga_live
)
:: checkout

p4 edit %titleupdatepath%/ps5/data/lang/american.rpf
p4 edit %titleupdatepath%/ps5/data/lang/american_REL.rpf
p4 edit %titleupdatepath%/x64/data/lang/american.rpf
p4 edit %titleupdatepath%/x64/data/lang/american_REL.rpf
p4 edit %titleupdatepath%/xbsx/data/lang/american.rpf
p4 edit %titleupdatepath%/xbsx/data/lang/american_REL.rpf


:: build
pushd %TextPath%\American\
	american.bat
popd
