::****************************************************************
:: GTA5 Label GEN9 Prebuild batch file							**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING GEN9 PREBUILD LABEL
echo.
if "%1%"=="dev_gen9_sga" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/ps5_config/sce_sys*/trophy2/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/ps5_config/sce_sys*/uds/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/ps5_config/sce_sys*/nptitle.dat
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/ps5_config/sce_sys*/param_cp_values.json


POPD
exit
)

if "%1%"=="dev_gen9_sga_live" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/ps5_config/sce_sys*/trophy2/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/ps5_config/sce_sys*/uds/...
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/ps5_config/sce_sys*/nptitle.dat
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/ps5_config/sce_sys*/param_cp_values.json


POPD
exit
)
