::****************************************************************
:: GTA5 Label GEN9 Prebuild batch file							**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING GEN9 PREBUILD LABEL
echo.
if "%1%"=="dev_gen9_sga" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/configuration.ps4path
P4 edit //depot/gta5/titleupdate/dev_gen9_sga/commandline.txt
P4 edit //depot/gta5/script/dev_gen9_sga/BGNG/tools/CreateRPF.bat
P4 edit //depot/gta5/script/dev_gen9_sga/Tools/GTA5_*.bat
P4 edit //depot/gta5/script/dev_gen9_sga/singleplayer/buildtype/*/Compile*ScriptImg.bat
P4 edit//depot/gta5/script/dev_gen9_sga/singleplayer/GTA5_SP.scproj


POPD
exit
)

if "%1%"=="dev_gen9_sga_live" (
::Checkout GTA5 GEN9 SGA
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/configuration.ps4path
P4 edit //depot/gta5/titleupdate/dev_gen9_sga_live/commandline.txt
P4 edit //depot/gta5/script/dev_gen9_sga_live/BGNG/tools/CreateRPF.bat
P4 edit //depot/gta5/script/dev_gen9_sga_live/Tools/GTA5_*.bat
P4 edit //depot/gta5/script/dev_gen9_sga_live/singleplayer/buildtype/*/Compile*ScriptImg.bat
P4 edit //depot/gta5/script/dev_gen9_sga_live/singleplayer/GTA5_SP.scproj
P4 edit //depot/gta5/assets_gen9_sga/gametext/dev_gen9_sga_live/*/*.bat


POPD
exit
)
