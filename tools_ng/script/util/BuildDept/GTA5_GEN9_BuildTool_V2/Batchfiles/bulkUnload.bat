@echo off
for /f "tokens=*" %%a in (labelsToBulkUnload.txt) do (
  echo unloading %%a
  call unloadLabel.bat %%a
  pause
)