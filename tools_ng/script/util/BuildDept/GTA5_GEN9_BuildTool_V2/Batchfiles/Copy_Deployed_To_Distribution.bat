::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				23/03/2021						            **
:: Edits: 					Add GEN9 and numver version copy            **
:: Last edited by:			Neil Walker  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)

if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO NETWORKCOPY
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg 1.0-%Branch%):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO NETWORKCOPY

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%
set VarThree=%3%

echo Copying %VarTwo% %ver% to the network, press enter to continue:
set /p approve=

if %VarOne%==dev_gen9_sga if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\pdb\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\dev_gen9_sga_live\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\tools\ /purge /s 
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\pdb\ /purge /s 
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\dev_gen9_sga_live\v%ver%\ *.options


POPD
pause
exit
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps5\Japanese_gen9_sga\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\pdb\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbsx\Japanese_gen9_sga\v%ver%\ *.options

POPD
pause
exit
)