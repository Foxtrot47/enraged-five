::************************************************************************
:: GTA5 builder - Build Gen9 Packages	 						        **
:: Updated: 				23/03/2021						            **
:: Edits: 					Add GEN9 and numver version copy            **
:: Last edited by:			Neil Walker  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set branch=dev_gen9_sga
	set region=eu
	set basedir=N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\
	set baselayoutfile=N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\game\GTA5.gp5
	set lastpassedpkg=N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\installer\GTAV_ROW-A0100-V0100.pkg
)

if "%1%"=="dev_gen9_sga_live" (
	set branch=dev_gen9_sga_live
	set region=eu
	set basedir=N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\
	set baselayoutfile=N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\game\GTA5.gp5
	set lastpassedpkg="N:\Projects\GTA5\Package\dev_gen9_sga_live\v171.8-dev_gen9_sga_live\prospero\installer\GTAV_ROW-A0100-V0100.pkg
)

if "%1%"=="Japanese_gen9_sga" (
	set branch=japanese_gen9_sga
	set region=jp
	set basedir=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\
	set baselayoutfile=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\game\GTA5.gp5
	set lastpassedpkg=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\installer\GTAV_JPN-A0100-V0100.pkg
)

if "%1%"=="Japanese_gen9_sga_live" (
	set branch=japanese_gen9_sga_live
	set region=jp
	set basedir=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\
	set baselayoutfile=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\game\GTA5.gp5
	set lastpassedpkg=N:\Projects\GTA5\Package\japanese_gen9_sga_live\v171.8-japanese_gen9_sga_live\prospero\installer\GTAV_JPN-A0100-V0100.pkg
)

if "%2%"=="Prospero" (
	set platform=prospero
)

if "%2%"=="Scarlett" (
	set platform=scarlett
)


echo Building gta5 %branch% %platform% package.  
echo Add addtional commandlines? eg. -clean -platformpackage
echo See HUB page for full details - https://hub.gametools.dev/display/RSGGG9/Packager
set /p args=: 

pushd X:\gta5\build\disk_images\gen9
bin\packager -project gta5 -branch %branch% -targets %platform% %args% -tu -basedir %basedir% -basefile %baselayoutfile% -prior %lastpassedpkg% -region %region% -master
popd
pause
exit