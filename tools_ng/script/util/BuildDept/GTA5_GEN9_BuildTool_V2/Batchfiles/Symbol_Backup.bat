::****************************************************************
:: NG GTA5 Symbol Backup batch file								**
:: Updated: 						01/12/2014					**
:: Edits:							Add check for winrar rather than just assume			**
:: Last edited by:					Kristopher Williams				**
::****************************************************************

@echo off
echo.
cd /d %~dp0
SETLOCAL ENABLEEXTENSIONS

set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\Symbols_NG
if "%1%"=="dev_ng" (
set builddir=X:\gta5\titleupdate\dev_ng
)
if "%1%"=="dev_temp" (
set builddir=X:\gta5\titleupdate\dev_temp
)
if "%1%"=="Japanese_ng" (
set builddir=X:\gta5\titleupdate\Japanese_ng
)
if "%1%"=="Japanese_ng_Live" (
set builddir=X:\gta5\titleupdate\Japanese_ng_Live
)
if "%1%"=="Japanese_ng_Live" (
set builddir=X:\gta5\titleupdate\Japanese_ng_Live
)
if "%1%"=="dev_ng_Live" (
set builddir=X:\gta5\titleupdate\dev_ng_Live
)

if exist "C:\Program Files (x86)\7-Zip\7z.exe" set winrarPath="C:\Program Files (x86)\7-Zip\"
if exist "C:\Program Files\7-Zip\7z.exe" set winrarPath="C:\Program Files\7-Zip\"

IF NOT DEFINED winrarPath goto NoWinRAR

set path=%winrarPath%;%path%

set ver=NUL
FOR /F "eol=# skip=3" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO MAIN

:MAIN

if exist %copydir%\version%ver% (
	echo This version folder already exists, please manually back up these symbols!
) ELSE (
	echo Copying symbols to the network
	mkdir "%copydir%/version%ver%"
	7z a %copydir%/version%ver%/symbols.zip %builddir%\game_durango_*.*
	7z a %copydir%/version%ver%/symbols.zip %builddir%\game_orbis_*.*
	7z a %copydir%/version%ver%/symbols.zip %builddir%\game_win64_*.*
)
goto finish

:NoWinRAR
echo Could not locate winrar
goto finish

:finish
echo Complete
ENDLOCAL
pause

