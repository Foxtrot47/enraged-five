::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				23/03/2021						            **
:: Edits: 					Add GEN9 and numver version copy            **
:: Last edited by:			Neil Walker  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)

if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)

:: Read version.txt for version number.
FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
	set ver=%%G
	GOTO GAME_VERSION
)

:GAME_VERSION
echo %ver% > X:\gta5\build\disk_images\gen9\package\%1%\gta5-prospero\game_version.txt
echo %ver% > X:\gta5\build\disk_images\gen9\package\%1%\gta5-scarlett\game_version.txt

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%
set VarThree=%3%

echo Copying %VarOne%%VarTwo% Prebuild to Resilio, press enter to continue:
set /p approve=

if %VarOne%==dev_gen9_sga if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-prospero\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\prospero\game_version.txt

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\pdb\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\pdb\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga\gta5-scarlett\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga\PREBUILD\scarlett\game_version.txt

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-prospero\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\prospero\game_version.txt

POPD
pause
exit
)

if %VarOne%==dev_gen9_sga_live if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\tools\ /purge /s 
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\pdb\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\pdb\ /purge /s 
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\dev_gen9_sga_live\gta5-scarlett\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\dev_gen9_sga_live\PREBUILD\scarlett\game_version.txt

POPD
pause
exit
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Prospero if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\FakeLauncher\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\ *.ps1
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-prospero\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\prospero\game_version.txt

POPD
pause
exit
)

if %VarOne%==Japanese_gen9_sga if %VarTwo%==Scarlett if %VarThree%==ww (

robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\game\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\game\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\tools\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\tools\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\pdb\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\pdb\ /purge /s
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\ *.bat
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\ *.xml
robocopy X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\ N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\ *.options
copy /Y X:\gta5\build\disk_images\gen9\package\Japanese_gen9_sga\gta5-scarlett\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\gen9_packaged_builds\Japanese_gen9_sga\PREBUILD\scarlett\game_version.txt

POPD
pause
exit
)