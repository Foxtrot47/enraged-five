::************************************************************************
:: GTA5 builder - Copy package to local							        **
:: Updated: 				01/03/2022						            **
:: Edits: 													            **
:: Last edited by:			Andy Herd	  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)

if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)

if "%1%"=="Japanese_gen9_sga_live" (
	set Branch=Japanese_gen9_sga_live
)

:GRABVERSION
P4 sync //depot/gta5/titleupdate/%Branch%/common/data/version.txt@GTA5_GEN9_PreBuild

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set version=v%%G
		echo.%version%
		GOTO CHECKNETWORK4PACKAGE
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO END
)

:CHECKNETWORK4PACKAGE
if exist N:\Projects\GTA5\Package\%Branch%\%version%\ (
	if not exist N:\Projects\GTA5\Package\%Branch%\%version%\installer\ (
		GOTO LOCALCOPY
	) ELSE (
		echo.Master already present
		GOTO END
	)
)	ELSE (
	echo.Package %version% not found on network. 
	GOTO END
)

:LOCALCOPY
robocopy N:\Projects\GTA5\Package\%Branch%\%version%\ X:\gta5\build\disk_images\gen9\package\%Branch%\%version%\ /purge /s

:END
POPD
exit 0
)
