::************************************************************************
:: GTA5 builder - Build masters							        		**
:: Updated: 				01/03/2022						            **
:: Edits: 													            **
:: Last edited by:			Andy Herd	  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_gen9_sga" (
	set Branch=dev_gen9_sga
)

if "%1%"=="dev_gen9_sga_live" (
	set Branch=dev_gen9_sga_live
)

if "%1%"=="Japanese_gen9_sga" (
	set Branch=Japanese_gen9_sga
)

if "%1%"=="Japanese_gen9_sga_live" (
	set Branch=Japanese_gen9_sga_live
)

if "%2%"=="prospero" (
	set Platform=prospero
)

if "%2%"=="scarlett" (
	set Platform=scarlett
)

:GRABVERSION
P4 sync //depot/gta5/titleupdate/%Branch%/common/data/version.txt@GTA5_GEN9_PreBuild

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set version=v%%G
		echo.%version%
		GOTO BUILDMASTERS
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO END
)

:BUILDMASTERS
echo.Building masters for %Branch% %version%

if "%Platform%"=="prospero" (
	X:\gta5\build\disk_images\gen9\package\%Branch%\%version%\prospero\MakeProsperoInstallPack_ROW.bat
)

if "%Platform%"=="scarlett" (
	X:\gta5\build\disk_images\gen9\package\%Branch%\%version%\scarlett\MakeScarlettInstallPack_ROW.bat
)

:END
POPD
exit 0
)
