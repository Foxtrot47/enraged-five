﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using RSG.Editor.Controls;
using RSG.Editor;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;

namespace BuildToolV2
{
    /// <summary>
    /// async operation for exiting not used currently but is a good implementation, unfortuantely thread tracking lost scope so not usable
    /// </summary>
    public static class FileOperatorsAsync
    {
        public static Task WaitForExitAsync(this Process process, CancellationToken cancellationToken = default(CancellationToken))
        {
            var task = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => task.TrySetResult(null);
            if (cancellationToken != default(CancellationToken))
            {
                cancellationToken.Register(() => { task.TrySetCanceled(); });
            }
            return task.Task;
        }
    }

    public  class FileOperators
    {
        public static RsWindow outputWindow;
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        public string returnValue = "";
        public bool SuccessfullBuild = false;
        public System.Diagnostics.Process proc = null;
        public CompilerOperators copsRef;
        private readonly BackgroundWorker Fileworker = new BackgroundWorker();
        public List<string> runBatFileArgs = new List<string>();
        public string batfileReference;
        public CancellationToken token;
        public bool processExited = false;
        

        /// <summary>
        /// Opens a file browser and retrieves the string name from selection.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string OpenFileBrowserDialogueAndRetrieveFileStringName(string path)
        {
            string fileName = "";
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            if (Directory.Exists(path))
            {
                dlg.InitialDirectory = path;
            }
            else
            {
                dlg.InitialDirectory = @"C:\";
            }
            
            dlg.DefaultExt = ".bat";
            dlg.Filter = "Bat Files (*.bat)|*.bat;Text Files (*.txt)|*.txt;All Files (*.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                fileName = dlg.FileName;
            }
            return fileName;
        }

        /// <summary>
        /// opens a file browser and returns string name from filtered list
        /// </summary>
        /// <param name="path">path to open</param>
        /// <param name="AcceptFiltersOnly"> filters enabled boolean </param>
        /// <returns></returns>
        public string OpenFileBrowserDialogueAndRetrieveFileStringName(string path,bool AcceptFiltersOnly)
        {
            string fileName = "";
            var dialog = new RsSelectFolderDialog(new Guid());
            if (Directory.Exists(path))
            {
                dialog.InitialDirectory = path;
            }
            else
            {
                dialog.InitialDirectory = @"C:\";
            }
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                fileName = dialog.FileName;
            }
            return fileName;
        }

        /// <summary>
        /// copys a list of files to a directory
        /// </summary>
        /// <param name="SourceDirectory"></param>
        /// <param name="destinationDirectoryName"></param>
        /// <param name="includeList">specifies which files to include </param>

        public void CopyListofFiles(string SourceDirectory, string destinationDirectoryName, List<string> includeList)
        {
            DirectoryInfo dir = new DirectoryInfo(SourceDirectory);
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + SourceDirectory);
            }
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destinationDirectoryName))
            {
                Directory.CreateDirectory(destinationDirectoryName);
            }
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                bool triggered = false;
                foreach (string includeString in includeList)
                {
                    if (file.Name.Contains(includeString) == true)
                    {
                        triggered = true;
                        break;
                    }
                }
                if (triggered)
                {
                    string temppath = Path.Combine(destinationDirectoryName, file.Name);
                    file.CopyTo(temppath, false);
                }
            }
        }

        /// <summary>
        /// copys a whole directory
        /// </summary>
        /// <param name="sourceDirName">Source directory</param>
        /// <param name="destDirName">Destination directory</param>
        /// <param name="copySubDirs">Copy sub directories?</param>
        /// <param name="useExclusions">Exlude files?</param>
        /// <param name="exclusions">Exclusions to use</param>
        public void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, bool useExclusions, List<string> exclusions)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            bool triggered = false;
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }
            
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (useExclusions)
                {
                    triggered = false;
                    if (exclusions != null)
                    {
                        foreach (string exclusionString in exclusions)
                        {
                            if (file.Name.Contains(exclusionString) == true)
                            {
                                triggered = true; 
                            }
                        }
                        if (triggered)
                        {
                            continue;
                        }
                        string temppath = Path.Combine(destDirName, file.Name);
                        file.CopyTo(temppath, false);
                    }
                }
                else
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, false);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    if (useExclusions)
                    {
                        triggered = false;
                        if (exclusions != null) 
                        {
                            foreach (string exclusionString in exclusions)
                            {
                                if (subdir.Name.Contains(exclusionString) == true)
                                {
                                    triggered = true; 
                                }
                            }
                            if (triggered)
                            {
                                continue;
                            }
                            string temppath = Path.Combine(destDirName, subdir.Name);
                            DirectoryCopy(subdir.FullName, temppath, copySubDirs, useExclusions, exclusions);
                        }
                    }
                    else
                    {
                        string temppath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, temppath, copySubDirs, useExclusions, exclusions);
                    }
                }
            }
        }
        /// <summary>
        /// runs batch files
        /// </summary>
        /// <param name="batfile"> Batfile</param>
        /// <param name="args">List of args</param>
        public void RunBatfile(string batfile, List<string> args)
        {
            runBatFileArgs.Clear();
            if (args != null)
            {
                runBatFileArgs = args;
            }
            batfileReference = batfile;
            Fileworker.WorkerSupportsCancellation = true;
            Fileworker.DoWork += new DoWorkEventHandler(Worker_RunBat);
            Fileworker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunBat_Completed);
            try
            {
                Fileworker.RunWorkerAsync();
            }
            catch
            {

            }

        }
        /// <summary>
        /// deletes original file, renames target file (_temp to non temp file.)
        /// </summary>
        /// <param name="originalFileName"></param>
        /// <param name="targetFilename"></param>
        public void DeleteAndRenameCopiedFilename(String originalFileName, string targetFilename)
        {
            File.Delete(originalFileName);
            File.Move(targetFilename, originalFileName);
            File.Delete(targetFilename);
        }

        
        /// <summary>
        ///  checks to see if a file is locked via stream operaion(non thread safe)
        /// </summary>
        /// <param name="file"> file to check</param>
        /// <returns></returns>
        public bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (Exception)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        /// <summary>
        /// worker to run batch files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Worker_RunBat(object sender, DoWorkEventArgs e)
        {
            ProcessStartInfo DataStartInfo = new ProcessStartInfo(batfileReference, string.Join(" ", runBatFileArgs));
            Process BatfileProcess = new Process();
            try
            {
                BatfileProcess.StartInfo = DataStartInfo;
                BatfileProcess.Start();
            }
            catch (Exception BatchFileException )
            {
                Console.WriteLine(BatchFileException.InnerException.ToString());
            }
            if (Fileworker.CancellationPending == true)
            {
                e.Cancel = true;
                batfileReference = "";
                runBatFileArgs.Clear();
            }
        }

        /// <summary>
        /// on completion of batfile worker reports and clears up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunBat_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            batfileReference = "";
            runBatFileArgs.Clear();
            Fileworker.DoWork -= new DoWorkEventHandler(Worker_RunBat);
            Fileworker.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(worker_RunBat_Completed);
        }

        /// <summary>
        /// executes command line runner synchronous without ouputfile
        /// </summary>
        /// <param name="command"></param>
        public void ExecuteCommandSync(object command)
        {
            try
            {
                proc = null;
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                Process proctemp = new System.Diagnostics.Process();
                proc = proctemp;
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.RedirectStandardError = true;
                procStartInfo.StandardOutputEncoding = Encoding.UTF8;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                proc.StartInfo = procStartInfo;
                proc.EnableRaisingEvents = true;
                proc.Exited += new EventHandler(setboolprocessExited);
                proc.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler((sender,e) => OutputHandler(sender,e,"output"));
                proc.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler((sender, e) => OutputHandler(sender, e, "error"));
                proc.Start();
                proc.BeginOutputReadLine();
                proc.BeginErrorReadLine();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);// Log the exception
            }
        }

        /// <summary>
        /// executes command line runner synchronous with ouputfile
        /// </summary>
        /// <param name="command"></param>
        /// <param name="NoOutput"></param>
        public void ExecuteCommandSync(object command,bool NoOutput)
        {
            try
            {
                proc = null;
                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                Process proctemp = new System.Diagnostics.Process();
                proc = proctemp;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                proc.StartInfo = procStartInfo;
                proc.EnableRaisingEvents = true;
                proc.Exited += new EventHandler(setboolprocessExited);
                proc.Start();
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);// Log the exception
            }
        }
        /// <summary>
        /// has process exited for checking completion on batfile task
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void setboolprocessExited(object sender, System.EventArgs e)
        {
            processExited = true;
        }

        /// <summary>
        /// Output hander for batchfile processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="outLine"></param>
        /// <param name="type"></param>
        public void OutputHandler(object sender, System.Diagnostics.DataReceivedEventArgs outLine,string type)
        {
            if (outLine.Data != null)
            {
                string output = System.Environment.NewLine + outLine.Data.ToString();
                string file = null;
                if (outputWindow != null)
                {
                    file = (outputWindow as OutputWindow).logfile;
                }
                System.Windows.Media.Color color;
                if (type == "error")
                {
                    color = System.Windows.Media.Colors.Red;
                } else {
                    color = System.Windows.Media.Colors.White;
                }

                if (output.Contains("error:")|| output.Contains("fatal error") ) { color = System.Windows.Media.Colors.Red; } 
                if (output.Contains("warning:")) { color = System.Windows.Media.Colors.Yellow; } 
                
                if ((outputWindow as OutputWindow) != null)
                {
                    if ((outputWindow as OutputWindow).Visibility == Visibility.Visible)
                    {
                        (outputWindow as OutputWindow).updatestring(output, false, color);
                    } else {
                        (outputWindow as OutputWindow).ToggleWindowVisibility(true);
                        (outputWindow as OutputWindow).updatestring(output, false, color);
                    }
                    if (file != null)
                    {
                        WriteToLogviaStreamWriter(file, output);
                    }
                }
            }
        }
        /// <summary>
        /// write output to log via streamwriter
        /// </summary>
        /// <param name="file"></param>
        /// <param name="datatowrite"></param>
        public void WriteToLogviaStreamWriter(string file, string datatowrite)
        {
            if (System.IO.File.Exists(file))
            {
                FileInfo fileinfo = new FileInfo(file);
                bool test = IsFileLocked(fileinfo);
                if (test == false) {
                    using (StreamWriter sw = new StreamWriter(file,true))
                    {
                        sw.WriteLine(datatowrite);
                        sw.Close();
                    }
                }
            }
            else
            {
                System.IO.File.Create(file).Close();
                using (StreamWriter sw = new StreamWriter(file,true))
                {
                    sw.WriteLine(datatowrite);
                    sw.Close();
                }
            }
        }
        /// <summary>
        /// stops output
        /// </summary>
        public void StopOuput()
        {
            try
            {
                proc.CancelOutputRead();
                proc.CancelErrorRead();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException.ToString());
            }
        }


        /// <summary>
        /// checks thread for completion
        /// </summary>
        /// <param name="threadToCheck"></param>
        /// <returns></returns>
        public async Task<bool> CheckThread(Thread threadToCheck)
        {
            bool threadCompleting = false;
            try { 

                if (threadToCheck != null)
                {
                    while (!threadCompleting)
                    {
                        if (threadToCheck != null)
                        {
                            if (threadToCheck.ThreadState == System.Threading.ThreadState.Stopped)
                            {
                                threadCompleting = true;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException.ToString());
                threadCompleting = true;
            }
            await Task.Delay(0).ConfigureAwait(false);
            return threadCompleting;

        }

        /// <summary>
        ///  async command runner with output window reference and update.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="localOuputWindow"></param>
        /// <returns></returns>
        public async Task<bool> ExecuteCommandAsync(string command,RsWindow localOuputWindow)
        {
            bool complete = false;
            try
            {
                outputWindow = localOuputWindow;
                Thread objThread = new Thread(new ParameterizedThreadStart(ExecuteCommandSync));
                objThread.IsBackground = true;
                objThread.SetApartmentState(ApartmentState.STA);
                objThread.Priority = ThreadPriority.AboveNormal;
                objThread.Start(command);
                await CheckComplete().ConfigureAwait(false);
                await Task.Delay(500).ConfigureAwait(false);
                StopOuput();
                await CheckThread(objThread).ConfigureAwait(false);
                (outputWindow as OutputWindow).updatestring("", true, System.Windows.Media.Colors.Red);
                complete = true;
                processExited = false;
                return complete;
            }
            catch (ThreadStartException objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
            catch (ThreadAbortException objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
        }

        /// <summary>
        /// executes the command for the async runner with optional logging.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="nologging"></param>
        /// <returns></returns>
        public async Task<bool> ExecuteCommandAsync(string command,bool nologging)
        {
            bool complete = false;
            try
            {
                Thread objThread = new Thread(new ParameterizedThreadStart(ExecuteCommandSync));
                objThread.IsBackground = true;
                objThread.SetApartmentState(ApartmentState.STA);
                objThread.Priority = ThreadPriority.AboveNormal;
                objThread.Start(command);
                await CheckComplete().ConfigureAwait(false);
                await Task.Delay(500).ConfigureAwait(false);
                StopOuput();
                await CheckThread(objThread).ConfigureAwait(false);
                complete = true;
                processExited = false;
                return complete;
            }
            catch (ThreadStartException objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
            catch (ThreadAbortException objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);
                complete = true;
                return complete;
            }
        }

//         public async Task<bool> CheckComplete()
//         {
//             try
//             {
//                 await proc.WaitForExitAsync().ConfigureAwait(false);
//             }
//             catch (Exception e)
//             {
//                 Console.WriteLine(e.InnerException.ToString());
//             }
//             return SuccessfullBuild = true;
//         }

        /// <summary>
        /// check if task has been completed for process (commandline running task
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckComplete()
        {
            await Task.Run(() =>
            {
                while (processExited == false)
                {

                }
                SuccessfullBuild = true;
            }).ConfigureAwait(false);
            return SuccessfullBuild = true;
        
        }

        /// <summary>
        /// delegate for running processes for the command runner
        /// </summary>
        /// <param name="command"> reference command object</param>
        /// <returns></returns>
        public static string ExecuteCommandSyncDelegate(ref object command)
        {
            string returnValue="";
            try
            {

                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                returnValue = proc.StandardOutput.ReadToEnd();
                
            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);                // Log the exception
            }
            return returnValue;
        }

        /// <summary>
        /// commandline delegate for running with output and optional logging enabled.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="outputfile"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public static string ExecuteCommandSyncDelegate(ref object command, string outputfile, bool output)
        {
            string returnValue = "";
            try
            {

                System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                if (output)
                {

                }
                returnValue = proc.StandardOutput.ReadToEnd();

            }
            catch (Exception objException)
            {
                Console.WriteLine(objException);                // Log the exception
            }
            return returnValue;
        }

    }

    public delegate string AsyncExecuteCommandSyncDelegate(ref object command);

    // pattern for running async command delegate
    public class RunAsyncPattern
    {
        ManualResetEvent waiter = new ManualResetEvent(true);

        public void StoreValueFromCmdExecution(IAsyncResult result)
        {
            object command = "";
            AsyncExecuteCommandSyncDelegate executeCommandDelegate = (AsyncExecuteCommandSyncDelegate)((AsyncResult)result).AsyncDelegate;
            string stringResult = (string) result.AsyncState;
            string stringAnswer = executeCommandDelegate.EndInvoke(ref command, result);
            waiter.Set();
        }

        /// <summary>
        /// callback for delegate 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string ExecuteCommandUsingCallback(string command)
        {
            object commandObject = command;
            AsyncExecuteCommandSyncDelegate executeCommandDelegate = new AsyncExecuteCommandSyncDelegate(FileOperators.ExecuteCommandSyncDelegate);
            string temp = "";
            IAsyncResult result = executeCommandDelegate.BeginInvoke(ref commandObject,null,null);
            while (!result.IsCompleted)
            {
                result.AsyncWaitHandle.WaitOne(10000, false);
            }
            result.AsyncWaitHandle.Close();
            temp = executeCommandDelegate.EndInvoke(ref commandObject, result);
            return temp;

        }
    }
}
