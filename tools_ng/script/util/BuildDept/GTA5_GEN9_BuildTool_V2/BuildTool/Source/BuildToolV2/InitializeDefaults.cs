﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using RSG.Editor.Controls;
using RSG.Configuration;




namespace BuildToolV2
{
    /// <summary>
    /// Convert depot directory to branch name 
    /// </summary>
    public class DepotToCLListConverison
    {
        private string depotDirectory {get; set;}
        private string clBranchName { get; set; }

        public DepotToCLListConverison() { }
        public DepotToCLListConverison(string _depotDirectory,string _clDirectory)
        {
            depotDirectory = _depotDirectory;
            clBranchName = _clDirectory;
        }
        public string getdepotDirectory()
        {
            return depotDirectory;
        }
        public string getCLDirectory()
        {
            return clBranchName;
        }
        public void setObject(string _depotDirectory, string _clDirectory)
        {
            depotDirectory = _depotDirectory;
            clBranchName = _clDirectory;
        }
    }
    /// <summary>
    /// depot directory class
    /// </summary>
    public class DepotDirectory
    {
        private string environmentVariableName{get;set;}
        private string depotDirectory{get;set;}
        public DepotDirectory(string _environmentVariableName, string _depotDirectory)
        {
            environmentVariableName = _environmentVariableName;
            depotDirectory = _depotDirectory;
        }
        public string GetDepotEnvironmentVariableName(){
            return environmentVariableName;
        }

        public string GetDepotDirectory()
        {
            CommandOptions options = (Application.Current as RsApplication).CommandOptions;
            string tempDepotDirectory = "";
            string branchName = "";
            
            if (depotDirectory.Contains("gametext") || depotDirectory.Contains("lang"))
            {
                if (string.Equals(options.Branch.Project.Name.ToString(), "gta5", StringComparison.OrdinalIgnoreCase))
                {
                    branchName = options.Branch.Name;
                    if (branchName == "titleupdate_ng")
                    {
                        branchName = "dev_ng";
                    }
                    if (branchName == "dev_temp")
                    {
                        branchName = "dev_temp";
                    }
                    if (branchName == "titleupdate_gen9_sga")
                    {
                        branchName = "dev_gen9_sga";
                    }
                    if (branchName == "titleupdate_gen9_sga_live")
                    {
                        branchName = "dev_gen9_sga_live";
                    }
                    if (branchName == "titleupdate_gen9_sga_merge")
                    {
                        branchName = "dev_gen9_sga_merge";
                    }
                    if (branchName == "titleupdate_japanese_gen9_sga_")
                    {
                        branchName = "japanese_gen9_sga";
                    }
                    if (depotDirectory.EndsWith(branchName) == false)
                    {
                        tempDepotDirectory = depotDirectory;

                        while (tempDepotDirectory.EndsWith(branchName) == false && tempDepotDirectory.Length > 0)
                        {
                            tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                        }
                        return tempDepotDirectory;
                    }
                    else
                    {
                        tempDepotDirectory = depotDirectory;
                        return tempDepotDirectory;
                    }
                }
                else
                {
                    if (depotDirectory.EndsWith("American") == true)
                    {
                        tempDepotDirectory = depotDirectory;

                        while (tempDepotDirectory.EndsWith("American") == true && tempDepotDirectory.Length > 0)
                        {
                            tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                        }
                    }
                    else
                    {
                        if (depotDirectory.EndsWith("scripts") == true)
                        {
                            tempDepotDirectory = depotDirectory;

                            while (tempDepotDirectory.EndsWith("scripts") == true && tempDepotDirectory.Length > 0)
                            {
                                tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                            }
                        }
                        else
                        {
                            tempDepotDirectory = depotDirectory;
                        }
                    }
                }
            }
            else
            {
                if ((depotDirectory.EndsWith("data") == true) && depotDirectory.EndsWith("metadata") == false)
                {
                    tempDepotDirectory = depotDirectory;
                    while ((tempDepotDirectory.EndsWith("data")) == true && tempDepotDirectory.Length > 0)
                    {
                        tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                    }
                }
                else
                {
                    if ((depotDirectory.EndsWith("anim") == true))
                    {
                        tempDepotDirectory = depotDirectory;
                        while ((tempDepotDirectory.EndsWith("anim")) == true && tempDepotDirectory.Length > 0)
                        {
                            tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                        }
                    }else{
                            tempDepotDirectory = depotDirectory;
                    }
                }
            }

            if (string.Equals(options.Branch.Project.Name.ToString(), "americas", StringComparison.OrdinalIgnoreCase))
            {
                if (depotDirectory.Contains("script") && depotDirectory.Contains("scripts") == false)
                {
                    while ((tempDepotDirectory.EndsWith("dev")) == true && tempDepotDirectory.Length > 0)
                    {
                        tempDepotDirectory = tempDepotDirectory.Remove(tempDepotDirectory.LastIndexOf('/'));
                    }
                }

            }

            return tempDepotDirectory;
        }
        public string GetEnvironmentVariableName(){
            return environmentVariableName;
        }

    }

    /// <summary>
    /// environment variable class to handle as an object
    /// </summary>
    public class EnvironmentVariable
    {
        public string environmentVariableName {get;set;}
        public string environmentVariableContent {get; set;}
        public string environmentVariableLocalName {get; set;}
        public string environmentVariableCLName { get; set; }

        public EnvironmentVariable(string name, string localName, string clName)
        {
            environmentVariableName = name;
            environmentVariableLocalName = localName;
            environmentVariableCLName = clName;
            environmentVariableContent = @""+ GetEnvironmentVariableContentFromName(name);
        }

        /// <summary>
        /// gets the environment variable from a variety of sources
        /// </summary>
        /// <param name="environmentVariableName"></param>
        /// <returns></returns>
        public string GetEnvironmentVariableContentFromName(string environmentVariableName)
        {
            string content = "";
            CommandOptions options = (Application.Current as RsApplication).CommandOptions;
            try 
            { 
                content = System.Environment.GetEnvironmentVariable(environmentVariableName, EnvironmentVariableTarget.User).ToString();

                if (content.ToLower().Contains("rage"))
                {
                    //rdr3 hack for getting build companion to replace rage
                    if (string.Equals(options.Branch.Project.Name.ToString(), "rdr3", StringComparison.OrdinalIgnoreCase))
                    {
                        string companionbranch = options.Branch.Build.Replace("build", "build_companion");
                        content = companionbranch;
                    }
                }
            }
            catch
            {
                if (environmentVariableName.Equals("RS_BUILDROOT"))
                {
                    string branch = "";
                    if (options.Branch.Name == "titleupdate_ng") { branch = "dev_ng"; }
                    else if (options.Branch.Name.ToLower() == "dev_ng") { branch = "dev_ng"; }
                    else if (options.Branch.Name.ToLower() == "dev_temp") { branch = "dev_temp"; }
                    else if (options.Branch.Name.ToLower() == "dev_ng_live") { branch = "dev_ng_Live"; }
                    else if (options.Branch.Name.ToLower() == "japanese_ng_live") { branch = "dev_ng_Live"; }
                    else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga") { branch = "dev_gen9_sga"; }
                    else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga_live") { branch = "dev_gen9_sga_live"; }
                    else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga_merge") { branch = "dev_gen9_sga_merge"; }
                    else if (options.Branch.Name.ToLower() == "titleupdate_japanese_gen9_sga") { branch = "japanese_gen9_sga"; }
                    else { branch = options.Branch.Name; }
                    if (!string.IsNullOrWhiteSpace(branch))
                    {
                        content = options.CoreProject.RootDirectory + @"\build\" + branch;
                    }
                }
                if (environmentVariableName.Contains("Export"))
                {
                    content = options.Branch.Export;
                }
                if(environmentVariableName.Contains("Meta"))
                {
                    content = options.Branch.Metadata;
                }
                if (environmentVariableName.Contains("ParentTxds"))
                {
                    content = options.Branch.Assets + @"\maps\ParentTxds.xml";
                }
                if (environmentVariableName.Contains("Text"))
                {
                    if (string.Equals(options.Branch.Project.Name.ToString(),"gta5", StringComparison.OrdinalIgnoreCase))
                    {
                        string branch = "";
                        if (options.Branch.Name == "titleupdate_ng") { branch = "dev_ng"; }
                        else if (options.Branch.Name.ToLower() == "dev_ng") { branch = "dev_ng"; }
                        else if (options.Branch.Name.ToLower() == "dev_temp") { branch = "dev_temp"; }
                        else if (options.Branch.Name.ToLower() == "dev_ng_live") { branch = "dev_ng_Live"; }
                        else if (options.Branch.Name.ToLower() == "japanese_ng_live") { branch = "dev_ng_Live"; }
                        else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga") { branch = "dev_gen9_sga"; }
                        else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga_live") { branch = "dev_gen9_sga_live"; }
                        else if (options.Branch.Name.ToLower() == "titleupdate_gen9_sga_merge") { branch = "dev_gen9_sga_merge"; }
                        else if (options.Branch.Name.ToLower() == "titleupdate_japanese_gen9_sga") { branch = "japanese_gen9_sga"; }
                        else { branch = options.Branch.Name; }
                        if (!string.IsNullOrWhiteSpace(branch))
                        {
                            content = options.Branch.Text + @"\" + branch;
                        }
                        else
                        {
                            content = options.Branch.Text;
                        }
                    }
                    else
                    {
                        content = options.Branch.Text;
                    }
                }

                if (environmentVariableLocalName.Contains("ParentTxds"))
                {
                   content = options.Branch.Assets + @"\maps\ParentTxds.xml";
                }
            }
            return content;
        }
        public string GetEnvironmentVariableCLName()
        {
            return environmentVariableCLName;
        }
        public string GetEnvironmentVariableLocalName(){
            return environmentVariableLocalName;
        }
    }

    public partial class InitializeDefaults
    {
        public string currentAppDirectory="";
        public string BugstarProjectID = "";
        public string XMLSettingsFile="";
        public string ToolsInstallBranch = "";
        public string ScriptBranch = "";
        public string BuildBranch = "";
        public string TUBuildBranch = "";
        public string CodeBranch = "";
        public string RageBranch = "";
        public string ExportBranch = "";
        public string AdditionalCLSString = "";
        public XMLOperators xmlOps = new XMLOperators();
        public FileOperators fileOps = new FileOperators();
        public PerforceOperators perfOps = new PerforceOperators();
        public List<EnvironmentVariable> environmentVariables;
        public List<System.Windows.Controls.Label> lblList = new List<System.Windows.Controls.Label>();
        public static List<DepotDirectory> depotDirectoryList;
        public static List<DepotToCLListConverison> CLDirectoryLookupList;
        public static System.Windows.Controls.TextBox AdditionalCLS = new System.Windows.Controls.TextBox();
        
        /// <summary>
        /// Initializes initial settings for environment variables etc 
        /// </summary>
        /// <returns></returns>
        public bool Initialize_Default_Settings()
        {
            CLDirectoryLookupList = new List<DepotToCLListConverison>();
            bool TriggerEnvironmentVariableSave = false;
            bool SaveEnvironmentVariables = false;
            currentAppDirectory = Get_Current_Directory();
            XMLSettingsFile = currentAppDirectory + "settings.xml";
            depotDirectoryList = new List<DepotDirectory>();
            SetEnvironmentVariables();
            List<string> xmlTags = xmlOps.GetListOfTagContents("Environment", XMLSettingsFile);
            CommandOptions options = (Application.Current as RsApplication).CommandOptions;
            BugstarProjectID = xmlOps.GetElementFromXMLFile("BugstarProjectID", "Setup", XMLSettingsFile);

            if (string.Equals(xmlOps.GetElementFromXMLFile("IsFirstBoot", "Setup", XMLSettingsFile),"Yes",StringComparison.OrdinalIgnoreCase))
            {
                TriggerEnvironmentVariableSave = true;
                xmlOps.WriteStringToElement("IsFirstBoot", "Setup", "No", XMLSettingsFile);
            }
            else
            {
                foreach (EnvironmentVariable envVar in environmentVariables)
                {
                    if (CheckListForVar(xmlTags, envVar.environmentVariableContent) == false)
                    {
                        if (RsMessageBox.Show(Application.Current.MainWindow, "Your Environment Variables do not match the ones stored in settings.xml\nWould you like this to be automatically updated?\nNote: 'No' will Quit this program to desktop", "Initialization", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            Application.Current.Shutdown();
                        }
                        else
                        {
                            TriggerEnvironmentVariableSave = true;
                            break;
                        }
                    }
                }
                if (xmlOps.GetElementFromXMLFile("ToolsProject", "Setup", XMLSettingsFile) == "" || xmlOps.GetElementFromXMLFile("ToolsProject", "Setup", XMLSettingsFile) != options.Project.Name.ToString())
                {
                    if (RsMessageBox.Show(Application.Current.MainWindow, "Your Environment Variables do not match the ones stored in settings.xml\nWould you like this to be automatically updated?\nNote: 'No' will Quit this program to desktop \nIf this problem persists please check: \n - Environment Variables \n - Tools Install Branch \n - Perforce Branch Access ", "Initialization", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    {
                        Application.Current.Shutdown();
                    }
                    else
                    {
                        TriggerEnvironmentVariableSave = true;
                    }
                }
                xmlTags = xmlOps.GetListOfChildrenfromParent("Depot", XMLSettingsFile);
                foreach (string str in xmlTags)
                {
                    depotDirectoryList.Add(new DepotDirectory(str, xmlOps.GetElementFromXMLFile(str,"Depot", XMLSettingsFile)));
                }
                CheckDepotDirectoriesforErrors(depotDirectoryList);
                if (TriggerEnvironmentVariableSave) 
                { 
                    SaveEnvironmentVariables = true;
                }

            }
            if (SaveEnvironmentVariables == true)
            {
                xmlOps.WriteStringToElement("ToolsProject", "Setup", options.Project.Name.ToString(), XMLSettingsFile);
                foreach (EnvironmentVariable envVar in environmentVariables)
                {
                    xmlOps.WriteStringToElement(envVar.environmentVariableLocalName, "Environment", envVar.environmentVariableContent, XMLSettingsFile);
                }
                
                List<DepotDirectory> depotDirectories = new List<DepotDirectory>();
                depotDirectories = GetDepotDirectories();

                foreach (DepotDirectory depotDir in depotDirectories)
                {
                    xmlOps.WriteStringToElement(depotDir.GetEnvironmentVariableName(), "Depot", depotDir.GetDepotDirectory(), XMLSettingsFile);
                }
            }
            SetP4SyncLabels(lblList);
            SetCLtoDepotConversionList(CLDirectoryLookupList);
            return true;
        }

        /// <summary>
        /// Sets additional CL references
        /// </summary>
        /// <param name="additionalCLs"></param>
        public void setAdditionalCLInitialReference(System.Windows.Controls.TextBox additionalCLs)
        {
            AdditionalCLS = additionalCLs;
        }

        /// <summary>
        /// Gets the stored additional CL references
        /// </summary>
        /// <returns></returns>
        public string GetAdditionalCLString()
        {
            AdditionalCLSString = AdditionalCLS.Text;
            return AdditionalCLSString;
        }

        /// <summary>
        /// sets teh conversion of depot branches to the changelist branches (branches belonging to the changelists)
        /// </summary>
        /// <param name="conversionList"></param>
        public void SetCLtoDepotConversionList(List<DepotToCLListConverison> conversionList)
        {
            List<string> listOfCLBranches = xmlOps.GetListOfChildrenfromParent("CurrentCLData", XMLSettingsFile);
            foreach (DepotDirectory DD in depotDirectoryList)
            {
                foreach (string str in listOfCLBranches)
                {
                    string tempStr = str;
                    if (str == "Data")
                    {
                        tempStr = "Build";
                    }
                    if (string.Equals(DD.GetEnvironmentVariableName(), tempStr + "Root"))
                    {
                        conversionList.Add(new DepotToCLListConverison(DD.GetDepotDirectory(), tempStr));
                    }
                }
            }
        }

        /// <summary>
        /// Returns list of changelist directories 
        /// </summary>
        /// <returns></returns>
        public List<DepotToCLListConverison> GetCLtoDepotConversionList()
        {
            return CLDirectoryLookupList;
        }

        /// <summary>
        /// gets the current working directory of the application 
        /// </summary>
        /// <returns></returns>
        public string Get_Current_Directory()
        {
            string appLocation = "";
            appLocation = AppDomain.CurrentDomain.BaseDirectory;
            return appLocation;
        }

        /// <summary>
        /// Checks a list for a particular string reference
        /// </summary>
        /// <param name="list">String list</param>
        /// <param name="str">string to check</param>
        /// <returns></returns>
        public bool CheckListForVar(List<string> list, string str)
        {
            if (list.Contains(@""+str) == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// checks each depot directory and validates if its correct 
        /// </summary>
        /// <param name="depotDirectoryList"></param>
        public void CheckDepotDirectoriesforErrors(List<DepotDirectory> depotDirectoryList)
        {
            StringBuilder errorString  = new StringBuilder();
            List<String> errors = new List<String>();

            foreach (DepotDirectory DD in depotDirectoryList)
            {
                if (DD.GetDepotDirectory().Contains("Error"))
                {
                    errors.Add(DD.GetDepotEnvironmentVariableName());
                }
            }
            if(errors.Count>0)
            {
                errorString.Append("Workspace errors present in your Perforce configuration, please correct and restart this application.");
                foreach(string err in errors){
                    errorString.Append(", "+ err);
                }
                xmlOps.WriteStringToElement("IsFirstBoot", "Setup", "yes", XMLSettingsFile);
                RsMessageBox.Show(errorString.ToString());
            }
        }

        /// <summary>
        /// gets a list of stored depot directories
        /// </summary>
        /// <returns></returns>
        public List<DepotDirectory> GetDepotDirectoryList()
        {
            return depotDirectoryList;
        }

        /// <summary>
        /// gets the list of depot directories from environment variables (conversions take place)
        /// </summary>
        /// <returns></returns>
        public List<DepotDirectory> GetDepotDirectories()
        {
            depotDirectoryList = new List<DepotDirectory>();
            foreach (EnvironmentVariable envar in environmentVariables)
            {
                if (envar.environmentVariableContent.Contains("."))
                {
                    depotDirectoryList.Add(new DepotDirectory(envar.environmentVariableLocalName.ToString(),ConvertBranch(envar.environmentVariableContent,false)));
                }
                else 
                { 
                    depotDirectoryList.Add(new DepotDirectory(envar.environmentVariableLocalName.ToString(), ConvertBranch(envar.environmentVariableContent,true)));
                }
            }
            CheckDepotDirectoriesforErrors(depotDirectoryList);
            return depotDirectoryList;
        }

        /// <summary>
        /// sets up environment variables 
        /// </summary>
        public void SetEnvironmentVariables()
        {
            CommandOptions options = (Application.Current as RsApplication).CommandOptions;
            environmentVariables= new List<EnvironmentVariable>();
            environmentVariables.Add(new EnvironmentVariable("RS_BUILDROOT", "BuildRoot", "Build"));
            environmentVariables.Add(new EnvironmentVariable("RS_BUILDBRANCH", "TUBuildRoot", "TUBuild"));
            environmentVariables.Add(new EnvironmentVariable("RAGE_DIR", "RageRoot", "Rage"));
            environmentVariables.Add(new EnvironmentVariable("RS_CODEBRANCH", "CodeRoot", "Source"));
            environmentVariables.Add(new EnvironmentVariable("RS_SCRIPTBRANCH", "ScriptRoot", "Script"));
            environmentVariables.Add(new EnvironmentVariable("Text", "TextRoot", "Text"));
            environmentVariables.Add(new EnvironmentVariable("Meta", "MetaDataRoot", "MetaData"));
            environmentVariables.Add(new EnvironmentVariable("Export", "ExportRoot", "Export"));
            environmentVariables.Add(new EnvironmentVariable("RS_ASSETS", "ParentTxdsRoot", "ParentTxds"));
            environmentVariables.Add(new EnvironmentVariable("RS_TOOLSROOT", "ToolsRoot", "Tools"));
        }

        /// <summary>
        /// returns a list of environment variables 
        /// </summary>
        /// <returns></returns>
        public List<EnvironmentVariable> GetEnvironmentVariables()
        {
            SetEnvironmentVariables();
            return environmentVariables;
        }

        /// <summary>
        /// sets a reference to the label 
        /// </summary>
        public void SetCLListsReferences()
        {
            System.Windows.Controls.Label testLable = new System.Windows.Controls.Label();
        }
        /// <summary>
        /// reuse and saves the list of labels (for use with syncing)
        /// </summary>
        /// <param name="list"></param>
        public void SetLabelListVariable(List<System.Windows.Controls.Label> list)
        {
            lblList = list ;
        }

        /// <summary>
        /// Matches the name of the branch to the label of which to update with 
        /// </summary>
        /// <param name="list"></param>
        public void SetP4SyncLabels(List<System.Windows.Controls.Label> list)
        {
            foreach (System.Windows.Controls.Label lbl in list)
            {
                foreach (string name in xmlOps.GetListOfChildrenfromParent("CurrentCLData", XMLSettingsFile))
                {
                    if (lbl.Name.Contains(name))
                    {
                        lbl.Content = xmlOps.GetElementFromXMLFile(name, "CurrentCLData", XMLSettingsFile);
                    }
                }
            }
        }

        /// <summary>
        /// Converts the branch from non ending depot branches to one with ellipses
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="endWithElipses"></param>
        /// <returns></returns>
        public string ConvertBranch(string branch,bool endWithElipses)
        {
            List<string> branchList = new List<string>();
            string convertedBranch = "";
            if (endWithElipses)
            {
                branchList.Add(@"" + branch + @"\...");
                convertedBranch = perfOps.GetDepotPathFromLocalPath(branchList);
            }else{
                branchList.Add(@"" + branch);
                convertedBranch = perfOps.GetDepotPathFromLocalPath(branchList);
            }
            return convertedBranch;
        }

    }

}



