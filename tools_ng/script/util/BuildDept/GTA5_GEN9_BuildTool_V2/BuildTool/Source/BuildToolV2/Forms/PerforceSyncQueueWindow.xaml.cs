﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using RSG.Editor.Controls;
using System.Timers;
using System.ComponentModel; // CancelEventArgs
using System.Windows.Threading;

namespace BuildToolV2
{

    delegate void UpdateStringCallbackDelegate(SyncItem SyncItem);
    delegate void UpdateTextCallbackDelegate(StringBuilder stringref);
    delegate void UpdateListCallbackDelegate(List<SyncItem> SyncItemList);
    delegate void RemovefromListAndSyncNextDelegate();
    delegate void UpdateSyncWindowRef(PerforceSyncWindow P4SyncWindow);
    delegate void CancelSync();
    delegate Task<bool> SetListofFilesInWindowDelegate();
    delegate void CloseQueueWindow(object source, ElapsedEventArgs e);
    /// <summary>
    /// Interaction logic for PerforceSyncWindow.xaml
    /// </summary>
    public partial class PerforceSyncQueueWindow : RsMainWindow
    {
        public PerforceSyncWindow P4SyncWindow = null;
        protected volatile List<SyncItem> SyncItemList = new List<SyncItem>();
        bool force = false;
        bool syncing = false;
        System.Timers.Timer time;
        Thread SyncThread;
        Thread threadUI;
        StringBuilder fileListBuilder;
        Dispatcher SyncQueueUIDispatcher;
        ButtonOperators buttonRef;
        string xmlFile = "";

        bool locked = false;
        
        /// <summary>
        ///  sets up a queue of items to sync
        /// </summary>
        /// <param name="ListOfSyncItems"> items to sync defined by list generated from main menu, source, script etc.</param>
        /// <param name="ThreadUI"> UI Thread for managing multithreaded system</param>
        /// <param name="UIDispatcher"> Dispatcher to pass messages</param>
        /// <param name="BRef">ButtonOperators script call functionality (non instanced in this case)</param>
        /// <param name="XmlFile">stettings.xml contains all relevant information </param>
        public PerforceSyncQueueWindow(List<SyncItem> ListOfSyncItems, Thread ThreadUI, Dispatcher UIDispatcher, ButtonOperators BRef, string XmlFile)
        {
            InitializeComponent();
            xmlFile = XmlFile;
            buttonRef = BRef;
            SyncItemList = ListOfSyncItems;
            force = ListOfSyncItems.First().force;
            time = new System.Timers.Timer();
            time.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            time.Interval = 500;
            fileListBuilder = new StringBuilder();
            SetListofFilesInWindow();
            time.Enabled = true;
            threadUI = ThreadUI;
            SyncQueueUIDispatcher = UIDispatcher;
        }
        /// <summary>
        /// sync method to set the files within the queue also used when queue item list is reduced 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SetListofFilesInWindow()
        {
//             if (!locked)
//             {
                List<SyncItem> TempSyncItemList = new List<SyncItem>();
                TempSyncItemList = SyncItemList;

                if (TempSyncItemList != null)
                {
                    if (TempSyncItemList.Count > 0)
                    {
                        int count = 0;
                        fileListBuilder = new StringBuilder("");

                        foreach (SyncItem syncItem in TempSyncItemList.ToList())
                        {
                            locked = true;
                            count++;
                            fileListBuilder.Append("" + count + " - " + syncItem.DepotRef.ToString() + "\n");
                            if (this.Dispatcher.CheckAccess() == true)
                            {
                                updateContentofTextBox(fileListBuilder);
                            }
                            else
                            {
                                SetListofFilesInWindowDelegate updateListofFilesDelegate = new SetListofFilesInWindowDelegate(SetListofFilesInWindow);
                                this.Dispatcher.Invoke(updateListofFilesDelegate);
                            }
                        }
                        locked = false;
                    }
                    else
                    {
                        if (this.Dispatcher.CheckAccess() == false)
                        {
                            updateContentofTextBox(new StringBuilder(""));
                        }
                    }
                }
                return true;
/*            }*/
//             else
//             {
//                 //await Task.Delay().ConfigureAwait(false);
//                 await SetListofFilesInWindow().ConfigureAwait(false);
//                 return true;
//             }
        }
        /// <summary>
        /// removes reference to the sync window for p4 syncs
        /// </summary>
        public void NullifyWindowRef()
        {
            P4SyncWindow = null;
            RemoveFirstItemfromList();
        }
        /// <summary>
        /// timed event for syncing the files in the queue, checks to see if still syncing then if not starts syncing the next file
        /// </summary>
        /// <param name="source"> reference for closing window (passes the window to close)</param>
        /// <param name="e"> event timer arg </param>
        public async void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (SyncItemList.Count > 0)
            {

                if (syncing == false)
                {
                    List<SyncItem> TempSyncItemList = new List<SyncItem>();
                    TempSyncItemList = SyncItemList;
                    //await Task.Delay(100);
                    if (TempSyncItemList.Count > 0)
                    {
                        syncing = true;
                        try {
                            await SyncFirstInListAsync(TempSyncItemList).ConfigureAwait(false);
                        }
                        catch (Exception p4exceptionresult)
                        {
                            MessageBoxResult dlg = MessageBox.Show(p4exceptionresult.Message + " Please log into Perforce and reopen this application", "Perforce Error", MessageBoxButton.OK);
                           if (dlg == MessageBoxResult.OK) 
                           {
                                System.Windows.Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                                System.Windows.Application.Current.Shutdown();
                                Environment.Exit(0); 
                            }
                        }
                        
                        //RemoveFirstItemfromList();
                        //SetListofFilesInWindow();
                    }
                    if (xmlFile != "") { 
                        buttonRef.UpdateCurrentCLBranch();
                    }
                }
            }
            else
            {
                buttonRef.NullifyWindowRef();
                if (this.Dispatcher.CheckAccess() == false)
                {
                    CloseQueueWindow CloseWindowCallback = new CloseQueueWindow(OnTimedEvent);
                    this.Dispatcher.Invoke(CloseWindowCallback, source, e);
                }
                else
                {
                    System.Windows.Threading.Dispatcher.ExitAllFrames();
                    time.Close();
                    time.Dispose();
                    Close();
                }

                //  await Dispatcher.BeginInvoke(new Action(() => this.Close()));
            }
        }
        /// <summary>
        ///  Starts the sync list
        /// </summary>
        /// <param name="listOfSyncItems"> list of items to sync </param>
        /// <returns></returns>
        public async Task<bool> SyncFirstInListAsync(List<SyncItem> listOfSyncItems)
        {
            syncing = true;
            buttonRef.UpdateCurrentSyncingImage(listOfSyncItems[0].ToolReference);
            bool result = await SyncFileAsync(listOfSyncItems[0]).ConfigureAwait(false);
            await Task.Delay(100).ConfigureAwait(false);
            syncing = false;
            return syncing;
        }
        /// <summary>
        /// processes the request to sync the file reports back once complete
        /// </summary>
        /// <param name="syncItem"> individual branch to sync</param>
        /// <returns>complete status (tells the list to continue on after the timer runs down)</returns>
        async Task<bool> SyncFileAsync(SyncItem syncItem)
        {
            bool taskComplete = false;
            if (P4SyncWindow == null)
            {
                //               UIDispatcherRef.Invoke(DispatcherPriority.Render, new Action(() =>
                //                {
                SyncThread = new Thread(() =>
                {
                    NewP4SyncWindow(syncItem, this, SyncQueueUIDispatcher,xmlFile);
                });
                SyncThread.Name = "SyncThread";
                SyncThread.SetApartmentState(ApartmentState.STA);
                SyncThread.Start();
                bool threadsafe = await WaitforThreadToStartAndGetWindow().ConfigureAwait(false);
                taskComplete = await P4SyncWindow.Sync(syncItem).ConfigureAwait(false);

                //               }));
            }
            return taskComplete;
        }
        /// <summary>
        /// instantiates a new sync window (passing the parent window reference and queue for dispatcher)
        /// </summary>
        /// <param name="syncItem"> perforce distro/item to sync to sync </param>
        /// <param name="P4QWindow">queue window reference for closing down sync window reference</param>
        /// <param name="SyncQueueDispatcher">dispatcher for messages</param>
        /// <param name="XmlFile">settings.xml</param>
        public void NewP4SyncWindow(SyncItem syncItem, PerforceSyncQueueWindow P4QWindow, Dispatcher SyncQueueDispatcher, string XmlFile)
        {

            PerforceSyncWindow dialogueWindowTemp = new PerforceSyncWindow(syncItem, P4QWindow, SyncQueueDispatcher,XmlFile);
            dialogueWindowTemp.ShowDialog();
          //  System.Windows.Threading.Dispatcher.Run();

        }
        /// <summary>
        /// async task for checking if p4sync window is showing
        /// </summary>
        /// <returns></returns>
        public async Task<bool> WaitforThreadToStartAndGetWindow()
        {
            while (P4SyncWindow == null)
            {
                await Task.Delay(0).ConfigureAwait(false);
            }
            Console.WriteLine("syncwindow shown");
            return true;
        }

        /// <summary>
        /// setup of p4sync window reference (used in closing etc)
        /// </summary>
        /// <param name="P4SyncWindowref"> current p4 sync window reference </param>
        public void UpdateCurrentP4SyncWindow(PerforceSyncWindow P4SyncWindowref)
        {
            P4SyncWindow = new PerforceSyncWindow();
            P4SyncWindow  = P4SyncWindowref;
        }
        /// <summary>
        /// Updates the list of sync items with any update from the main menu 
        /// </summary>
        /// <param name="syncItem">object for distro or item to sync path and relevant branch name and option for forcing the file</param>
        public async void AddFileOrFolderToSyncList(SyncItem syncItem)
        {
            if (SyncItemList.Contains(syncItem) == false)
            {
                if (SyncList.Dispatcher.CheckAccess() == false)
                {
                    if (P4SyncWindow != null)
                    {
                        UpdateStringCallbackDelegate UpdateListCallback = new UpdateStringCallbackDelegate(AddFileOrFolderToSyncList);
                        this.Dispatcher.Invoke(UpdateListCallback, syncItem);
                    }
                }
                else
                {
                    if (SyncItemList.Contains(syncItem) == false)
                    {
                        SyncItemList.Add(syncItem);
                        await SetListofFilesInWindow().ConfigureAwait(true);
                    }
                    if (syncing == false)
                    {
                        await SyncFirstInListAsync(SyncItemList).ConfigureAwait(false);
                    }
                }
            }
        }
        /// <summary>
        /// adding a list of items to the synclist 
        /// </summary>
        /// <param name="SyncItemListRef">list of objects for distro or item to sync path and relevant branch name and option for forcing the file</param>
        public async void AddFileOrFolderToSyncList(List<SyncItem> SyncItemListRef)
        {
            if (SyncList.Dispatcher.CheckAccess() == false)
            {
                UpdateListCallbackDelegate UpdateListCallback = new UpdateListCallbackDelegate(AddFileOrFolderToSyncList);
                this.Dispatcher.Invoke(UpdateListCallback, SyncItemList);

            }
            else
            {
                foreach (SyncItem fileref in SyncItemListRef)
                {
                    if (SyncItemList.Contains(fileref) == false)
                    {
                        SyncItemList.Add(fileref);
     
                    }
                }
                await SetListofFilesInWindow().ConfigureAwait(true);
                if (syncing == false)
                {
                    await SyncFirstInListAsync(SyncItemList).ConfigureAwait(false);
                }
            }
        }
      
        /// <summary>
        /// updating text for dispatcher friendly reference 
        /// </summary>
        /// <param name="contentstringref"></param>
        public void updateContentofTextBox(StringBuilder contentstringref)
        {

               if (this.Dispatcher.CheckAccess() == false)
                {
                    UpdateTextCallbackDelegate updateText = new UpdateTextCallbackDelegate(updateContentofTextBox);
                    this.Dispatcher.Invoke(updateText, contentstringref);
                }
                else
                {
                    SyncList.Content = contentstringref;
                }
            
        }

        /// <summary>
        ///  unused cancel functionality (P4API version in use no longer allows cancellation)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_P4SyncQueueCancel(object sender, RoutedEventArgs e)
        {
            SyncItemList.Clear();
            time.Dispose();
            threadUI.Abort();
        }



        /// <summary>
        /// syncs the next item in the list
        /// </summary>
        public async void SyncNext()
        {
            if (SyncItemList.Count > 0)
            {
                if (syncing != true)
                {
                    if (SyncItemList.Count > 0)
                    {
                        await SyncFirstInListAsync(SyncItemList).ConfigureAwait(false);
                    }
                    else
                    {
                    }
                }
            }
        }
        /// <summary>
        /// removes the first item in the sync list 
        /// </summary>
        public async void RemoveFirstItemfromList()
        {
            if (SyncItemList.Count > 0)
            {
                SyncItemList.RemoveAt(0);
                await SetListofFilesInWindow().ConfigureAwait(true);
            }
            //await Task.Delay(1).ConfigureAwait(false);
        }




        /// <summary>
        /// Unused close feature prevented possible window ref and re-instancing of p4sync window (possible future development - needs to be thread safe and dispatcher aware, note: cannot use default dispatcher)
        /// </summary>
        /// <param name="d"></param>
        public void closeCurrentSyncWindow(Dispatcher d)
        {
            SyncQueueUIDispatcher = d;

//             P4SyncWindow.StopThread();
//             d.BeginInvoke(new Action(() => P4SyncWindow.Close()));
//             SyncThread.Interrupt();
//             SyncThread.Abort();
        }







    }
}


