﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;
using System.Net;
using System.Threading;
using System.Collections;

namespace BuildToolV2
{
    /// <summary>
    /// object class to handle element data from parent xml tag
    /// </summary>
    public class ElementData
    {
        public string element { get; set; }
        public string data { get; set; }

        public ElementData()
        {
        }
        public ElementData(string _element, string _data)
        {
            element = _element;
            data = _data;
        }

        public void SetElementAndData(string _element, string _data)
        {
            element = _element;
            data = _data;
        }

    }
    public partial class XMLOperators
    {
        public FileOperators fileOps = new FileOperators();
        public static List<ElementData> listOfElementAndData = new List<ElementData>();


        /// <summary>
        /// Gets the element data from xml file 
        /// </summary>
        /// <param name="elementName">element</param>
        /// <param name="parentName">parent</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public string GetElementFromXMLFile(string elementName, string parentName,string xmlFile)
        {
            string elementContents = "";
            XmlTextReader reader = new XmlTextReader(xmlFile);
            if (parentName != "")
            {
                reader.ReadToDescendant(parentName);
                if (reader.ReadToFollowing(elementName))
                {
                    elementContents = reader.ReadElementContentAsString();
                }
            }
            else
            {
                if (elementName != "")
                {
                    reader.ReadToDescendant(elementName);
                    elementContents = reader.ReadElementContentAsString();
                }
            }
            reader.Close();
#if BV2GTA5
            if (Globals.isJapanese)
            {
                string tempContents = elementContents.Replace("dev_", "japanese_");
                if ((File.Exists(tempContents))|| (Directory.Exists(tempContents)))
                {
                    elementContents = elementContents.Replace("dev_", "japanese_");
                }
            }
#endif

            return elementContents;
        }

        /// <summary>
        /// gets a list of sub element data from the parent element
        /// </summary>
        /// <param name="parentName">parent element</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns>List of strings for element tag names</returns>
        public List<string> GetListOfTagContents(string parentName, string xmlFile)
        {
            List<string> listOfTags = new List<string>();
            XDocument xDoc = new XDocument();
            using (var sr = new StreamReader(xmlFile))
            {
                xDoc = XDocument.Load(sr);
            }
            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                    IEnumerable<XElement> elements = group.Elements();
                    foreach (var elem in elements)
                    {
                        listOfTags.Add(elem.Value.ToString());
                    }
                }

            }
            return listOfTags;
        }

        /// <summary>
        /// gets a list of sub element name data from the parent element
        /// </summary>
        /// <param name="parentName">parent element</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public List<string> GetListOfChildrenfromParent(string parentName, string xmlFile)
        {
            List<string> listOfTags = new List<string>();
            XDocument xDoc = new XDocument();
            using (var sr = new StreamReader(xmlFile))
            {
                xDoc = XDocument.Load(sr);
            }
            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                    IEnumerable<XElement> elements = group.Elements();
                    foreach (var elem in elements)
                    {
                        listOfTags.Add(elem.Name.ToString());
                    }
                }
            }
            return listOfTags;
        }

        /// <summary>
        /// gets a list of child element data object from parent
        /// </summary>
        /// <param name="parentName">parent element</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns>a list of element data containing name and data contents of sub elements</returns>
        public List<ElementData> GetListOfChildrenAndDatafromParent(string parentName, string xmlFile)
        {
            listOfElementAndData = new List<ElementData>();
            XDocument xDoc = new XDocument();
            using (var sr = new StreamReader(xmlFile))
            {
                xDoc = XDocument.Load(sr);
            }
            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                    IEnumerable<XElement> elements = group.Elements();
                    foreach (var elem in elements)
                    {
                        listOfElementAndData.Add(new ElementData(elem.Name.ToString(), elem.Value.ToString()));
                    }
                }
            }
            return listOfElementAndData;
        }

        /// <summary>
        /// gets a list of child attributes from parent field 
        /// </summary>
        /// <param name="parentName">parent </param>
        /// <param name="dataStream">streamreader (xml parsed data)</param>
        /// <returns>a list of element data containing name and data contents of sub elements</returns>
        public List<ElementData> GetListOfChildrenAndAttributesfromParent(string parentName, StreamReader dataStream)
        {
            listOfElementAndData = new List<ElementData>();
            XDocument xDoc = new XDocument();
            using (var sr = dataStream)
            {

                xDoc = XDocument.Load(sr);
            }
            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                    IEnumerable<XAttribute> attributes = group.Attributes();
                    listOfElementAndData.Add(new ElementData(attributes.ElementAt(0).Value.ToString(), attributes.ElementAt(1).Value.ToString()));
                }
            }
            return listOfElementAndData;
        }

        /// <summary>
        /// write element data to xmlfile
        /// </summary>
        /// <param name="elementName">sub element to write to</param>
        /// <param name="parentName">parent element (owns elementName)</param>
        /// <param name="writeData">Data to write</param>
        /// <param name="xmlFile">settings.xml</param>
        public void WriteStringToElement(string elementName,string parentName,string writeData, string xmlFile)
        {
            XDocument xDoc = new XDocument();
            using (var sr = new StreamReader(xmlFile))
            {
                xDoc = XDocument.Load(sr);
            }
            
            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                   IEnumerable<XElement> elements = group.Elements();
                   foreach (var elem in elements)
                   {
                       if (elem.Name.ToString() == elementName)
                       {
                            elem.SetValue(writeData);
                            while (fileOps.IsFileLocked(new FileInfo(xmlFile)))
                            {
                                Thread.Sleep(1);
                            }
                            xDoc.Save(xmlFile);
                            return;
                       }
                   }
                }
            }
        }

        /// <summary>
        /// async patter for writing element data to xmlfile
        /// </summary>
        /// <param name="elementName">sub element to write to</param>
        /// <param name="parentName">parent element (owns elementName)</param>
        /// <param name="writeData">Data to write</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns>returns successful write attempt</returns>
        public async Task<bool> WriteStringToElementAsync(string elementName, string parentName, string writeData, string xmlFile)
        {
            XDocument xDoc = new XDocument();
            bool successful = false;
            using (var sr = new StreamReader(xmlFile))
            {
                xDoc = XDocument.Load(sr);
            }

            IEnumerable<XElement> settings = xDoc.Root.Elements();
            foreach (var group in settings)
            {
                if (group.Name.ToString() == parentName)
                {
                    IEnumerable<XElement> elements = group.Elements();
                    foreach (var elem in elements)
                    {
                        if (elem.Name.ToString() == elementName)
                        {
                            elem.SetValue(writeData);
                            while (fileOps.IsFileLocked(new FileInfo(xmlFile)))
                            {
                                Thread.Sleep(1);
                            }
                            xDoc.Save(xmlFile);
                            successful = true;
                            break;
                        }
                    }
                }
            }
            await Task.Delay(0).ConfigureAwait(false);
            return successful;
        }

        /// <summary>
        /// write child elements from one parent to another 
        /// </summary>
        /// <param name="Parent1">source parent</param>
        /// <param name="Parent2">tareget parent</param>
        /// <param name="xmlFile">settings.xml</param>
        public void WriteChildrenfromParentToParent(string Parent1, string Parent2, string xmlFile)
        {
            List<string> childList = new List<string>();
            List<string> childList2 = new List<string>();
            childList = GetListOfChildrenfromParent(Parent1,xmlFile);
            childList2 = GetListOfChildrenfromParent(Parent2, xmlFile);
            foreach(string str in childList)
            {
                foreach (string str2 in childList2)
                {
                    if(str == str2)
                    {
                        WriteStringToElement(str2, Parent2, GetElementFromXMLFile(str, Parent1, xmlFile), xmlFile);
                    }
                }
            }
        }

        /// <summary>
        /// converts a list of strings to preformatted xml string (used in B* bug request)
        /// </summary>
        /// <param name="strList">list of strings to convert</param>
        /// <returns>string with the format of xml data</returns>
        public string ListToXMLString(List<string> strList)
        {
            string XmlString = "";
            XmlString = "<BugIds>";
            foreach (string ListItem in strList)
            {
                XmlString += "<id>" + ListItem + "</id>";
            }
            XmlString += "</BugIds>";

            return XmlString;
        }

        /// <summary>
        /// gets the field values from xml data from B* interface 
        /// </summary>
        /// <param name="file"> file to search</param>
        /// <param name="BugListRef">litst of bug references</param>
        /// <param name="StatusFilter">status filter for B* search</param>
        /// <returns></returns>
        public List<Bug> GetFieldValuesfromXMLList(string file, List<Bug> BugListRef, string StatusFilter)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(file);//Load the xml document
                XmlNodeList XmlList = xmlDoc.GetElementsByTagName("bug");
                foreach (XmlElement xmlElement in XmlList)
                {
                    string bugnumber = "";
                    string summary = "";
                    string bugclass = "";
                    string project = "";
                    string status = "";
                    foreach (XmlElement xmlElement1 in xmlElement.ChildNodes)
                    {
                        if (xmlElement1.Name == "id")
                        {
                            bugnumber = xmlElement1.InnerText;
                        }
                        if (xmlElement1.Name == "summary")
                        {
                            summary = xmlElement1.InnerText;
                        }
                        if (xmlElement1.Name == "category")
                        {
                            bugclass = xmlElement1.InnerText;
                        }
                        if (xmlElement1.Name == "projectId")
                        {
                            project = xmlElement1.InnerText;
                        }
                        if (xmlElement1.Name == "state")
                        {
                            status = xmlElement1.InnerText;
                        }
                    }
                    if (status != StatusFilter) {
                       Bug newBug = new Bug(Convert.ToInt32(bugnumber), summary, bugclass, project, status);
                        BugListRef.Add(newBug);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
            }
            return BugListRef;
        }
    }
    
   
}

