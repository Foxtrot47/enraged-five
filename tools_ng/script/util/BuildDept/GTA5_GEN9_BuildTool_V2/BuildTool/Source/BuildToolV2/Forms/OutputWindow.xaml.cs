﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Editor.Controls;
using System.Timers;
using System.IO;
using System.Windows.Threading;

namespace BuildToolV2
{
    //delegate void UpdateTextBoxCallbackDelegate(string stringToAppend, bool clear,Color color);
    delegate void UpdateTextBoxCallbackDelegate(string stringToAppend);
    delegate void WriteLogsToFileDelegate(string file, string stringToAppend);
    delegate void CloseOutputWindow();
    delegate string GetCurrentTextfromOutputDelegate();
    delegate void ClearContentsDelegate();
    delegate void HideDelegate(bool isWindowVisible);
    /// <summary>
    /// foo class for appending text to a document applying colour and overriding text. (unused in current batch output handler but can be re-introduced if per text feature is fixed)
    /// </summary>
    public static class foo {
    public static void AppendText(this RichTextBox box, string text, string color)
        {
            BrushConverter bc = new BrushConverter();
            if (!string.IsNullOrWhiteSpace(text))
            {
                TextRange tr = new TextRange(box.Document.ContentEnd, box.Document.ContentEnd);
                if (tr != null)
                {
                    tr.Text = text;
                    try
                    {
                        tr.ApplyPropertyValue(TextElement.ForegroundProperty, bc.ConvertFromString(color));
                    }
                    catch (FormatException) { }
                }
            }
        }
    }

    /// <summary>
    /// Interaction logic for OutputWindow.xaml
    /// </summary>
    public partial class OutputWindow : RsMainWindow
    {


        public bool window_closing = false;
        public CompilerOperators compops;
        public string logfile;
        System.Timers.Timer time;
        string outputupdatetext;
        /// <summary>
        /// Init of new output window for use with the compiler and filehandling output feature
        /// </summary>
        /// <param name="compOpRef">compiler operator reference avoids ciclical reference</param>
        /// <param name="logFile">log file location reference to write to</param>
        public OutputWindow(CompilerOperators compOpRef, string logFile)
        {
            InitializeComponent();
            compops = compOpRef;
            logfile = logFile;
            time = new System.Timers.Timer();
            time.Elapsed += new ElapsedEventHandler(UpdateStringAsChunkofText);
            time.Interval = 500;
            time.Start();
        }
        /// <summary>
        /// change logfile reference
        /// </summary>
        /// <param name="updatedlogFile"></param>
        public void updateoutputfilereference(string updatedlogFile)
        {
            logfile = updatedlogFile;
        }
        /// <summary>
        /// Hide rather than close as deref window within multithread was dangerous
        /// </summary>
        public void CloseOutputWindow()
        {
                this.Visibility = Visibility.Hidden;
//                 if (this.Dispatcher.CheckAccess() == false)
//                 {
//                     CloseOutputWindow CloseWindowCallback = new CloseOutputWindow(CloseOutputWindow);
//                     this.Dispatcher.Invoke(CloseWindowCallback);
//                 }
//                 else
//                 {   
//                     window_closing = true;
//                     (this as OutputWindow).Close();
//                 }
        }
        /// <summary>
        /// gets the current text from the output text
        /// </summary>
        /// <returns></returns>
        public string GetCurrentTextfromOutput ()
        {
            string outputtext = "";
            if (this.IsVisible)
            {
                if (Output_TextBox.Dispatcher.CheckAccess() == false)
                {
                    GetCurrentTextfromOutputDelegate OutputCallback = new GetCurrentTextfromOutputDelegate(GetCurrentTextfromOutput);
                    Dispatcher.Invoke(OutputCallback);
                }
                else
                {
                    outputtext = Output_TextBox.ToString();
                }
            }
            return outputtext;
        }


        /// <summary>
        /// timed event calls strings to chunk text rather than per line output as this was laggy.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public async void UpdateStringAsChunkofText (object source, ElapsedEventArgs e)
        {
            if (outputupdatetext!="")
            {
                if (!window_closing)
                {
                    if ((this as OutputWindow).IsVisible == true)
                    {
                        updateText(outputupdatetext);

                        //                     if (Output_TextBox.Dispatcher.CheckAccess() == false)
                        //                     {
                        //                         UpdateTextBoxCallbackDelegate UpdateListCallback = new UpdateTextBoxCallbackDelegate(updatestring);
                        //                         this.Dispatcher.Invoke(UpdateListCallback, stringToAppendToTextBox, clear, color);
                        //                     }
                        //                     else
                        //                     {
                        //                         if (clear == true)
                        //                         {
                        //                             Output_TextBox.Document.Blocks.Clear();
                        //                         }
                        //                         else
                        //                         {
//                         Output_TextBox.AppendText(stringToAppendToTextBox, color.ToString());
//                         compops.CheckIfBuildSuccessfull(stringToAppendToTextBox);
//                         Output_TextBox.ScrollToEnd();


                        //                         }
                        //                     }
                    }
                    else
                    {
                        ToggleWindowVisibility(true);
                        updateText(outputupdatetext);
                    }
                }
            }
        }
        /// <summary>
        /// updates text via delegate allowing for windowfocus events to not cause exception, used by chunk method so its not per output line from compiler output
        /// </summary>
        /// <param name="stringToAppendToTextBox"></param>
        public void updateText(string stringToAppendToTextBox)
        {
            if (Output_TextBox.Dispatcher.CheckAccess() == false)
            {
                UpdateTextBoxCallbackDelegate UpdateListCallback = new UpdateTextBoxCallbackDelegate(updateText);
                this.Dispatcher.Invoke(UpdateListCallback, stringToAppendToTextBox);
            }
            else
            {
                Output_TextBox.AppendText(outputupdatetext,"white");
                Output_TextBox.ScrollToEnd();
                outputupdatetext = "";
            }
        }
        /// <summary>
        /// clears the text if required stopping the timer also updates the string to append to the textbox within the chunk method above.
        /// </summary>
        /// <param name="stringToAppendToTextBox"></param>
        /// <param name="clear"></param>
        /// <param name="color"></param>
        public void updatestring(string stringToAppendToTextBox,bool clear,Color color)
        {
            if (clear==true)
            {
                time.Stop();
                ClearContents();
                outputupdatetext = "";
            }
            else
            {
                if (time != null)
                {
                    if (time.Enabled == false)
                    {
                        time.Start();
                    }
                }
                outputupdatetext += stringToAppendToTextBox;
                compops.CheckIfBuildSuccessfull(stringToAppendToTextBox);
            }
        }
        /// <summary>
        ///  reusable window so visibility is toggled rather than window closing (this toggles window visibility)
        /// </summary>
        /// <param name="isWindowVisible"></param>
        public void ToggleWindowVisibility(bool isWindowVisible)
        {
            if (this.Dispatcher.CheckAccess() == false)
            {
                HideDelegate HideCallback = new HideDelegate(ToggleWindowVisibility);
                this.Dispatcher.Invoke(HideCallback, isWindowVisible);
            }
            else
            {
                if (isWindowVisible==true)
                {
                    if ((this as OutputWindow).Visibility != System.Windows.Visibility.Visible)
                    {
                        (this as OutputWindow).Visibility = System.Windows.Visibility.Visible;
                    }
                }
                else
                {
                    if ((this as OutputWindow).Visibility != System.Windows.Visibility.Collapsed)
                    {
                        (this as OutputWindow).Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
            }
        }

        /// <summary>
        ///  clears the contents of the document in the RTF text box
        /// </summary>
        public void ClearContents()
        {
            if ((this as OutputWindow).IsVisible == true)
            {
                if (Output_TextBox.Dispatcher.CheckAccess() == false)
                {
                    ClearContentsDelegate clearcontentscallback = new ClearContentsDelegate(ClearContents);
                    this.Dispatcher.Invoke(clearcontentscallback);
                }
                else
                {
                    Output_TextBox.Document.Blocks.Clear();
                }
            }
            time.Stop();
        }
        /// <summary>
        /// clear ref to window (for use on re-init of window called from delegate on other threads but is dangerous, use sparingly and only if you understand the outcome)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void window_Closed(object sender, EventArgs e)
        {
            compops.ow = null;
        }
        /// <summary>
        /// checks file lock status for writing logs
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                if (File.Exists(file.FullName))
                {
                    stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
                }
                
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }


        /// <summary>
        /// writes logs to file
        /// </summary>
        /// <param name="file">file to write to</param>
        /// <param name="sb">string for writing</param>
        public void WriteLogsToFile(string file, string sb)
        {
            if (Dispatcher.CheckAccess() == false)
            {
                WriteLogsToFileDelegate WriteLogsToFileCallback = new WriteLogsToFileDelegate(WriteLogsToFile);
                this.Dispatcher.Invoke(WriteLogsToFileCallback, file, sb);
            }
            else 
            {
                try
                {
                    if (System.IO.File.Exists(file)==false)
                    {
                        System.IO.File.Create(file);
                    }
                    FileInfo fileInfo = new FileInfo(file);
                    int count = 0;
                    while (IsFileLocked(fileInfo) != false)
                    {
                        count++;
                        if (count == 300)
                        {
                            break;
                        }
                    }
                    if (IsFileLocked(fileInfo) == false)
                    {
                        System.IO.File.WriteAllText(file, sb.ToString());
                    }
                    
                }
                catch
                {

                }
            }
        }
        /// <summary>
        /// sets compile window to null from compileroperator if window closed rather than visibility toggled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_Closed(object sender, System.ComponentModel.CancelEventArgs e)
        {
            compops.ow = null;
        }
    }
}

