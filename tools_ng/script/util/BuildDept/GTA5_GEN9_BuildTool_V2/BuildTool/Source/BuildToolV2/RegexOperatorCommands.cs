﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;

namespace BuildToolV2
{
    public class RegexOperatorCommands
    {
        /// <summary>
        /// tests a string for a regex pattern both in string format and returns the singular result
        /// </summary>
        /// <param name="regularExpression">regex pattern</param>
        /// <param name="originalText">source text</param>
        /// <returns></returns>
        public string RegexText(string regularExpression, string originalText)
        {
            string matchedText = "";

            Regex regExpression = new Regex(regularExpression, RegexOptions.IgnoreCase);
            MatchCollection matchCol = regExpression.Matches(originalText);
            foreach (Match match in matchCol)
            {
                return matchedText = match.Value.ToString();
            }
            return matchedText;
        }

        /// <summary>
        ///  tests a string for a regex pattern both in string format and returns a list of results
        /// </summary>
        /// <param name="regularExpression"></param>
        /// <param name="originalText"></param>
        /// <returns></returns>
        public List<string> RegexTextAndReturnList(string regularExpression, string originalText)
        {
            List<string> matchedText = new List<string>();

            Regex regExpression = new Regex(regularExpression, RegexOptions.IgnoreCase);
            MatchCollection matchCol = regExpression.Matches(originalText);
            foreach (Match match in matchCol)
            {
                if (matchedText.Contains(match.Value.ToString()) == false)
                {
                    matchedText.Add(match.Value.ToString());
                }
            }
            return matchedText;
        }

        /// <summary>
        /// looks for a specific regex pattern within a string then removes the spericiied characets from the string and returns the results
        /// </summary>
        /// <param name="input">intput string</param>
        /// <param name="regularExpression">regex pattern</param>
        /// <param name="charToRemove">character to remove</param>
        /// <returns></returns>
        public string RegexAndRemoveChar(string input, string regularExpression, char charToRemove)
        {
            string returnString = RegexText(regularExpression, input);
            returnString = returnString.Trim(charToRemove);
            return returnString;
        }

        /// <summary>
        /// looks for a specific regex pattern withing a string then removes the match and returns the resulting string
        /// </summary>
        /// <param name="input"></param>
        /// <param name="regularExpression"></param>
        /// <returns></returns>
        public string RegexAndRemoveMatch(string input, string regularExpression)
        {
            string removalString = RegexText(regularExpression, input);
            string returnString = input.Replace(removalString,"");
            return returnString;
        }

        /// <summary>
        /// looks for the button name and removes anything before _
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public string RegexButtonNameToElement(System.Windows.Controls.Button button)
        {
            string element = RegexAndRemoveChar(button.Name.ToString(), @"(_\w+)", '_');
            return element;
        }
        /// <summary>
        /// looks for the button name and removes anything after _
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public string RegexButtonNameToParent(System.Windows.Controls.Button button)
        {
            string parent = RegexAndRemoveChar(button.Name.ToString(), @"(\w+_)", '_');
            return parent;
        }

    }
}
