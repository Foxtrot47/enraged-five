﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Editor.Controls;
using RSG.Base.Logging.Universal;

namespace BuildToolV2
{
    class BuildToolV2App : RsApplication
    {
        [STAThread]
        public static void Main(string[] args)
        {
            BuildToolV2App app = new BuildToolV2App();
            OnEntry(app, ApplicationMode.Multiple);
        }

        protected override System.Windows.Window CreateMainWindow()
        {
            LogHelper.g_Log = Log;
            return new MainWindow();
        }
    }

    public static class LogHelper
    {
        public static IUniversalLog g_Log { get; set; }
    }

    public static class Globals
    {
        public static bool isJapanese { get; set; }
    }
}
