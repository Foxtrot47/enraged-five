﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using RSG.Editor.Controls;
using RSG.Editor.Controls.Perforce;
using Perforce;
using System.Text.RegularExpressions;
using System.Net.Mail;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;
    

namespace BuildToolV2
{
    /// <summary>
    /// update current CL Branch (UI Update)
    /// </summary>
    delegate void UpdateCurrentCLBranchDelagate();
    delegate void UpdateCurrentCLBranchDelagate2(List<System.Windows.Controls.Label> _ListOfLabels, string xmlFile); 
    delegate void UpdateCurrentSycingImage(string BranchName); 
    /// <summary>
    /// Sync Item Class to store Items to sync 
    /// </summary>
    public class SyncItem
    {
        public string ToolReference { get; set; }
        public string LocalRef { get; set; }
        public string DepotRef { get; set; }
        public string ClientRef { get; set; }
        public bool force { get; set; }
        public SyncItem(string _ToolReference, string _LocalRef,string _DepotRef,string _ClientRef, bool _Force)
        {
            ToolReference = _ToolReference;
            LocalRef = _LocalRef;
            DepotRef = _DepotRef;
            ClientRef = _ClientRef;
            force = _Force;
        }
    }
    /// <summary>
    /// checkout item class for used in file reference
    /// </summary>
    public class CheckOutItem
    {
        public List<string> fileOrFolderList { get; set; }
        public string xmlReference { get; set; }
        public List<string> typeRefList { get; set; }
        public string friendlyReference { get; set; }

        public CheckOutItem(List<string> _FileOrFolder, string _XmlReference, string _TypeRef, string _FriendlyReference)
        {
            fileOrFolderList = _FileOrFolder;
            xmlReference = _XmlReference;
            typeRefList = CSVStringToListConversion(_TypeRef);
            friendlyReference = _FriendlyReference;
        }

        public List<string> CSVStringToListConversion(string CSVString)
        {
            string[] CSVStringArray = CSVString.Split(',');
            List<string> typeReferenceList = new List<string>();
            typeReferenceList = CSVStringArray.ToList<string>();
            return typeReferenceList;
        }

    }
    /// <summary>
    /// Handling class for most features 
    /// </summary>
    public partial class ButtonOperators
    {
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        public XMLOperators xmlOps = new XMLOperators();
        public FileOperators fileOps = new FileOperators();
        public PerforceOperators perfOps = new PerforceOperators();
        public CompilerOperators CompilerOps = new CompilerOperators();
        public Bugstar bugstarOps = new Bugstar();
        public InitializeDefaults ini = new InitializeDefaults();
        public RegexOperatorCommands regexOps = new RegexOperatorCommands();
        public List<Label> listOfLabels = new List<Label>();
        public List<SyncItem> ListOfSyncItems = new List<SyncItem>();
        public List<ListBox> ListOfListBoxes = new List<ListBox>();
        public List<CompilerObject> CompilerObjectList = new List<CompilerObject>();
        public List<ShaderObject> ShaderObjectList = new List<ShaderObject>();
        public PerforceSyncQueueWindow p4SyncQueueWindow;
        static Thread threadUI = null;
        public List<System.Windows.Forms.PictureBox> LocalListOfPictureBoxes;
        Dispatcher MainUIDispatcher;
        string xmlFileref ="";
        List<string> listOfErrors = new List<string>();
        /// <summary>
        /// setup xml reference for settings.xml
        /// </summary>
        /// <param name="XmlFileRef"></param>
        public void setxmlRef(string XmlFileRef)
        {
            xmlFileref = XmlFileRef;
        }
        /// <summary>
        /// attempt at integrating headers via P4 (older p4api implementation is bad, newer one will work muchbetter TODO)
        /// </summary>
        public void integrateScriptHeader()
        {
            perfOps.integrate_and_resolve("//depot/gta5/src/dev_ng/game/script_headers/commands_hud.sch", "//depot/gta5/script/dev_ng/core/common/native/commands_hud.sch");
        }

        /// <summary>
        /// Sync branch per button reference
        /// </summary>
        /// <param name="button"> button reference so we can differentiate which branch we are sinking</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <param name="listOfLabels">reference for which sync to reference</param>
        /// <param name="force">force grab?</param>
        /// <param name="UIDispatcher">dispatcher for passing messages to and from the main menu</param>
        public void SyncBranch(Button button, string xmlFile, List<Label> listOfLabels, bool force, Dispatcher UIDispatcher, List<System.Windows.Forms.PictureBox> listOfPictureBoxs)
        {
            LocalListOfPictureBoxes = new List<System.Windows.Forms.PictureBox>();
            LocalListOfPictureBoxes = listOfPictureBoxs;
            xmlFileref = xmlFile;
            MainUIDispatcher = UIDispatcher;
            List<string> fileorFolderList = new List<string>();

//             xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), regOps.RegexButtonNameToParent(button), xmlFile);
//             if (@"" + GetXmlStringvalueFromButtonName(button, xmlFile) != "")
//             {
//                 foreach (Label lbl in listOfLabels)
//                 {
//                     if (lbl.Name.Substring(0, lbl.Name.Length - 2) == regOps.RegexButtonNameToElement(button))
//                     {
//                         lbl.Content = perfOps.GetHeadRevisionAndCLofBranchOrFile(@"" + DetermineBranch(GetXmlStringvalueFromButtonName(button, xmlFile)), force);
//                         xmlOps.WriteStringToElement(regOps.RegexButtonNameToElement(button), "CurrentCLData", lbl.Content.ToString(), xmlFile);
//                     }
//                 }
//             }

            if (p4SyncQueueWindow != null)
            {
                xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), regOps.RegexButtonNameToParent(button), xmlFile);
                if (@"" + GetXmlStringvalueFromButtonName(button, xmlFile) != "")
                {
                    foreach (Label lbl in listOfLabels)
                    {
                        if (lbl.Name.Substring(0, lbl.Name.Length - 2) == regOps.RegexButtonNameToElement(button))
                        {
                            string buttonRef = GetXmlStringvalueFromButtonName(button, xmlFile);
                            SyncItem syncItem = new SyncItem(regOps.RegexButtonNameToElement(button), xmlOps.GetElementFromXMLFile(buttonRef, "Environment", xmlFile), xmlOps.GetElementFromXMLFile(buttonRef, "Depot", xmlFile), "", force);
                            ListOfSyncItems.Add(syncItem);
                            if (p4SyncQueueWindow != null)
                            {
                                p4SyncQueueWindow.AddFileOrFolderToSyncList(ListOfSyncItems.Last());
                            }
                        }
                    }
                }

            }else{
                xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), regOps.RegexButtonNameToParent(button), xmlFile);
                if (@"" + GetXmlStringvalueFromButtonName(button, xmlFile) != "")
                {
                    string comparer = regOps.RegexButtonNameToElement(button);
                    foreach (Label lbl in listOfLabels)
                    {
                        if (comparer == "companion")
                        {
                            comparer = "Rage";
                        }
                        if (lbl.Name.Substring(0, lbl.Name.Length - 2) == comparer)
                        {
                            ButtonOperators bOps = this;
                            string buttonRef = GetXmlStringvalueFromButtonName(button, xmlFile);
                            fileorFolderList.Add(DetermineBranch(buttonRef));
                            SyncItem syncItem = new SyncItem(regOps.RegexButtonNameToElement(button), xmlOps.GetElementFromXMLFile(buttonRef, "Environment", xmlFile), xmlOps.GetElementFromXMLFile(buttonRef, "Depot", xmlFile), "", force);
                            ListOfSyncItems.Add(syncItem);
                            threadUI = new Thread(() => {
                                NewP4SyncQueueWindow(ListOfSyncItems, threadUI, UIDispatcher, bOps, xmlFile);
                              //  System.Windows.Threading.Dispatcher.Run();
                            });
                            threadUI.Name = "SyncQueueThread";
                            threadUI.SetApartmentState(ApartmentState.STA);
                            threadUI.Start();
                        }
                    }
                    if (comparer == "Other")
                    {
                        ButtonOperators bOps = this;
                        string buttonRef = GetXmlStringvalueFromButtonName(button, xmlFile);
                        fileorFolderList.Add(DetermineBranch(buttonRef));
                        SyncItem syncItem = new SyncItem(regOps.RegexButtonNameToElement(button), xmlOps.GetElementFromXMLFile(buttonRef, "Environment", xmlFile), xmlOps.GetElementFromXMLFile(buttonRef, "Depot", xmlFile), "", force);
                        ListOfSyncItems.Add(syncItem);
                        threadUI = new Thread(() =>
                        {
                            NewP4SyncQueueWindow(ListOfSyncItems, threadUI, UIDispatcher, bOps, xmlFile);
                            //  System.Windows.Threading.Dispatcher.Run();
                        });
                        threadUI.Name = "SyncQueueThread";
                        threadUI.SetApartmentState(ApartmentState.STA);
                        threadUI.Start();
                    }
                }
            }
        }
        /// <summary>
        /// check out files to new CL
        /// </summary>
        /// <param name="FileorFolders">Files to checkout in form of filespec</param>
        /// <param name="CLDescriptions">CL description, so its not just "saved by perforce"</param>
        public void CheckoutFilestoNewCL(IList<string> FileorFolders, string CLDescriptions)
        {
            perfOps.checkoutFiles(FileorFolders, CLDescriptions);
        }

        public void UpdateCurrentCLBranch2(List<System.Windows.Controls.Label>_ListOfLabels, string xmlFile)
        {
            UpdateCurrentCLBranchDelagate2 UpdateUICallback = new UpdateCurrentCLBranchDelagate2(UpdateCurrentCLBranch2);
            if (Application.Current.Dispatcher.CheckAccess() == false)
            {
                Application.Current.Dispatcher.Invoke(UpdateUICallback);
            }
            else
            {
                foreach (Label lbl in _ListOfLabels)
                {
                    string labelName = lbl.Name.Substring(0, lbl.Name.Length - 2);
                    string localBranchName = xmlOps.GetListOfChildrenfromParent("CurrentCLData", xmlFile).Find(x => x.Contains(labelName));
                    if (localBranchName != null)
                    {
                        lbl.Content = xmlOps.GetElementFromXMLFile(localBranchName, "CurrentCLData", xmlFile);
                    }
                }
            }
        }
        /// <summary>
        /// update current cl branch 
        /// </summary>
        public void UpdateCurrentCLBranch()
        {

            UpdateCurrentCLBranchDelagate UpdateUICallback = new UpdateCurrentCLBranchDelagate(UpdateCurrentCLBranch);
            if(Application.Current.Dispatcher.CheckAccess() == false)
            {
                Application.Current.Dispatcher.Invoke(UpdateUICallback);
            }
            else{
                foreach (System.Windows.Forms.PictureBox pb in LocalListOfPictureBoxes)
                {
                    pb.ImageLocation = null;
                }
                foreach (Label lbl in listOfLabels)
                {
                    string labelName = lbl.Name.Substring(0, lbl.Name.Length - 2);
                    string localBranchName = xmlOps.GetListOfChildrenfromParent("CurrentCLData", xmlFileref).Find(x => x.Contains(labelName));
                    if (localBranchName!=null)
                    {
                        lbl.Content = xmlOps.GetElementFromXMLFile(localBranchName, "CurrentCLData", xmlFileref);
                    }
                }
            }
        }

        public void UpdateCurrentSyncingImage(string branchname)
        {

            UpdateCurrentSycingImage UpdateUICallback = new UpdateCurrentSycingImage(UpdateCurrentSyncingImage);
            if (Application.Current.Dispatcher.CheckAccess() == false)
            {
                Application.Current.Dispatcher.Invoke(()=> UpdateUICallback(branchname));
            }
            else
            {
                foreach (System.Windows.Forms.PictureBox pb in LocalListOfPictureBoxes)
                {
                    string pbname = pb.Text;
                    if (pbname.Contains(branchname))
                    {
                        pb.ImageLocation = System.IO.Directory.GetCurrentDirectory() + @"\Forms\Loading.gif";
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// get batfile from settings.xml
        /// </summary>
        /// <param name="element">reference to where batfile is in xml</param>
        /// <param name="parent">parent to where the reference for the element is in xml</param>
        /// <param name="xmlRef">settings.xml</param>
        /// <returns>returns batfile reference </returns>
        public string retrieveBatFileFromXmlReference(string element, string parent, string xmlRef)
        {
            string batFile = "";
            
            if (xmlOps.GetElementFromXMLFile(element, parent, xmlRef).Split(';').Count() > 3)
            {
                if (xmlOps.GetElementFromXMLFile(element, parent, xmlRef).Split(';')[2] == " ")
                {
                    batFile = xmlOps.GetElementFromXMLFile(element, parent, xmlRef).Split(';')[1];
                }
                else
                {
                    batFile = xmlOps.GetElementFromXMLFile(xmlOps.GetElementFromXMLFile(element, parent, xmlRef).Split(';')[2], "Environment", xmlRef) + xmlOps.GetElementFromXMLFile(element, parent, xmlRef).Split(';')[1];
                }
                
            }
            return batFile;

        }

        /// <summary>
        /// remove reference to p4syncQueue window
        /// </summary>
        public void NullifyWindowRef()
        {
            p4SyncQueueWindow = null;
        }

        /// <summary>
        /// set perforce window queue reference 
        /// </summary>
        /// <param name="p4SQW"></param>
        public void SetWindowRef(PerforceSyncQueueWindow p4SQW)
        {
            p4SyncQueueWindow = p4SQW;
            Console.WriteLine(p4SyncQueueWindow);
        }

        /// <summary>
        /// setup new P4SyncQueueWindow 
        /// </summary>
        /// <param name="SyncItemList">List of items to sync</param>
        /// <param name="threadUI">Thread for UI reference </param>
        /// <param name="UIDispatcher">Dispatcher for passing messages</param>
        /// <param name="BOps">Buttong operators (reference so that we can call functions from this)</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns>PerforceSyncQueueWindow window</returns>
        public PerforceSyncQueueWindow NewP4SyncQueueWindow(List<SyncItem> SyncItemList, Thread threadUI, Dispatcher UIDispatcher,ButtonOperators BOps, string xmlFile)
        {
            PerforceSyncQueueWindow p4SyncQueueWindowTemp = new PerforceSyncQueueWindow(SyncItemList, threadUI, UIDispatcher, BOps, xmlFile);
            this.SetWindowRef(p4SyncQueueWindowTemp);
            p4SyncQueueWindowTemp.ShowDialog();
            return p4SyncQueueWindowTemp;
        }

        /// <summary>
        /// Sync All branches in list
        /// </summary>
        /// <param name="buttonList">list of buttons in reference</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <param name="listOfLabels">reference to update the CL numbers stored in main menu sync</param>
        /// <param name="force"> force grab?</param>
        /// <param name="UIDispatcher">dispatcher for passing messages</param>
        public void SyncAllBranchesInList(List<Button> buttonList, string xmlFile, List<Label> listOfLabels, bool force, Dispatcher UIDispatcher, List<System.Windows.Forms.PictureBox> ListofPictureBoxes)
        {
            ListOfSyncItems = new List<SyncItem>();
            LocalListOfPictureBoxes = new List<System.Windows.Forms.PictureBox>();
            LocalListOfPictureBoxes = ListofPictureBoxes;

            foreach (Button button in buttonList)
            {

                List<string> fileorFolderList = new List<string>();
                string buttonRef = GetXmlStringvalueFromButtonName(button, xmlFile);
                fileorFolderList.Add(@"" + DetermineBranch(buttonRef));
                string buttonReference = GetXmlStringvalueFromButtonName(button, xmlFile);
                SyncItem syncItem = new SyncItem(regOps.RegexButtonNameToElement(button), xmlOps.GetElementFromXMLFile(buttonRef, "Environment", xmlFile), xmlOps.GetElementFromXMLFile(buttonRef, "Depot", xmlFile), "", force);
                ListOfSyncItems.Add(syncItem);
                //SyncBranch(button, xmlFile, listOfLabels, force);
//                xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), regOps.RegexButtonNameToParent(button), xmlFile);
//                if (@"" + GetXmlStringvalueFromButtonName(button, xmlFile) != "")
//                {
//                }
            }
            if (p4SyncQueueWindow == null)
            {
                threadUI = new Thread(() => {
                    NewP4SyncQueueWindow(ListOfSyncItems, threadUI, UIDispatcher, this,xmlFile);
                    //  System.Windows.Threading.Dispatcher.Run();
                });
                threadUI.Name = "SyncQueueThread";
                threadUI.SetApartmentState(ApartmentState.STA);
                threadUI.Start();
            }
            else
            {
                p4SyncQueueWindow.AddFileOrFolderToSyncList(ListOfSyncItems);
            }

        }
        /// <summary>
        /// save old CL list to settings.xml
        /// </summary>
        /// <param name="xmlFile"></param>
        public void SaveOld(string xmlFile)
        {
            xmlOps.WriteChildrenfromParentToParent("OldCLData", "BackupCLData", xmlFile);
            xmlOps.WriteChildrenfromParentToParent("CurrentCLData", "OldCLData", xmlFile);
        }
        /// <summary>
        /// updating current cl data
        /// </summary>
        /// <param name="xmlFile"></param>
        public void SaveXMLList(string xmlFile)
        {
            List<string> childList = new List<string>();
            childList = xmlOps.GetListOfChildrenfromParent("CurrentCLData", xmlFile);

        }

        /// <summary>
        /// determine branch from reference setting \\ to \ and \... making it nicer to read and not break scripts
        /// </summary>
        /// <param name="branchinput"></param>
        /// <returns>string branch</returns>
        public string DetermineBranch(string branchinput)
        {
            string branch = "";
            foreach (EnvironmentVariable enVar in ini.GetEnvironmentVariables())
            {
                if (branchinput.Contains(enVar.environmentVariableLocalName))
                {
                    branch = enVar.environmentVariableContent;
                }
            }
            if (Regex.IsMatch(branchinput, "(^X:)|(^x:)"))
            {
                branch = branchinput + @"\...";
            }
            if (Regex.IsMatch(branchinput,@"(^\/\/)"))
            {
                branch = branchinput;
            }
            if (branchinput.Contains("//depot"))
            {
                branch = branchinput;
            }

            if (Regex.IsMatch(branch, "(^X:)|(^x:)"))
            {
                if (Regex.IsMatch(branch, "[...]") == false)
                {
                    branch = branch + @"\...";
                }
            }
            if (Regex.IsMatch(branch, @"(^\/\/)"))
            {
                if (Regex.IsMatch(branch, "[...]") == false)
                {
                    branch = branch + @"\...";
                }
            }
            if (branch.Contains("//depot"))
            {
                if (Regex.IsMatch(branch, "[...]") == false)
                {
                    branch = branch + @"\...";
                }
            }

            return branch;

        }

        /// <summary>
        /// not used (can be linked to standard buttons though.
        /// </summary>
        /// <param name="button"></param>
        /// <param name="xmlFile"></param>
                
        public void ButtonPress(Button button, string xmlFile)
        {
        }

        /// <summary>
        /// get the patch batfile from file reference 
        /// </summary>
        /// <param name="button">button reference </param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public string GetPatchBatfileFromReference(Button button, string xmlFile)
        {
            string patchBatfile = "";
            string orbisPatchDirectory = "";
            string durangoPatchDirectory = "";
            string patchDirectoryRoot= xmlOps.GetElementFromXMLFile("PatchDirectory", "Setup", xmlFileref);
            string branch = xmlOps.GetElementFromXMLFile("TUBuildRoot", "Environment", xmlFileref).Split('\\').Last();
            string DurangoPatchDirectoryRef = xmlOps.GetElementFromXMLFile("DurangoPatchDirectory", "Setup", xmlFileref);
            string OrbisPatchDirectoryRef = xmlOps.GetElementFromXMLFile("OrbisPatchDirectory", "Setup", xmlFileref);
            string buttonName = button.Content.ToString();
            List<string> underscoreReservedWordsList = new List<string>();
            List<string> buttonNameComponents = buttonName.Split(' ').ToList<string>();
            orbisPatchDirectory = patchDirectoryRoot + @"ps4\package\" + branch + @"\" + OrbisPatchDirectoryRef+@"\";
            durangoPatchDirectory = patchDirectoryRoot + @"xboxone\package\" + branch + @"\" + DurangoPatchDirectoryRef + @"\";
            List<string> batfileList = new List<string>();
            underscoreReservedWordsList.Add("SubMaster");
            underscoreReservedWordsList.Add("EU");
            underscoreReservedWordsList.Add("US");
            underscoreReservedWordsList.Add("JA");

            if((buttonNameComponents.Contains("JA"))|| (buttonNameComponents.Contains("Japanese")))
            {
                branch = branch.Replace("dev_", "Japanese_");
                orbisPatchDirectory = orbisPatchDirectory.Replace("dev_", "Japanese_");
                durangoPatchDirectory = durangoPatchDirectory.Replace("dev_", "Japanese_");
            }
            if(buttonNameComponents.Contains("Orbis"))
            {
                patchBatfile = orbisPatchDirectory+"Make";
                foreach(string str in buttonNameComponents)
                {
                    if (underscoreReservedWordsList.Contains(str))
                    {
                        patchBatfile=patchBatfile+"_"+str;
                    }else{
                        patchBatfile=patchBatfile+str;
                    }
                }
                patchBatfile=patchBatfile+".bat";

            }
            if(buttonNameComponents.Contains("Durango"))
            {
                patchBatfile = durangoPatchDirectory + "Make";
                foreach(string str in buttonNameComponents)
                {
                    if(str.ToLower() == "japanese")
                    {
                        patchBatfile = patchBatfile + "_JA";
                        continue;
                    }
                    if (underscoreReservedWordsList.Contains(str))
                    {
                        patchBatfile=patchBatfile+"_"+str;
                    }else{
                        patchBatfile=patchBatfile+str;
                    }
                }
                patchBatfile=patchBatfile+".bat";

            }
            return patchBatfile;

        }
        /// <summary>
        /// get patch directory from button reference
        /// </summary>
        /// <param name="button"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public string getPatchDirectoryFromButton(Button button, string xmlFile)
        {
            string patchDirectory = "";
            string patchDirectoryRoot = xmlOps.GetElementFromXMLFile("PatchDirectory", "Setup", xmlFileref);
            string DurangoPatchDirectoryRef = xmlOps.GetElementFromXMLFile("DurangoPatchDirectory", "Setup", xmlFileref);
            string OrbisPatchDirectoryRef = xmlOps.GetElementFromXMLFile("OrbisPatchDirectory", "Setup", xmlFileref);
            string branch = xmlOps.GetElementFromXMLFile("TUBuildRoot", "Environment", xmlFileref).Split('\\').Last();
            if (button.Content.ToString().Contains("Orbis"))
            {
                if (button.Content.ToString().Contains("Japanese"))
                {
                    branch = branch.Replace("dev", "Japanese");
                    patchDirectory = patchDirectoryRoot + @"ps4\package\" + branch + @"\" + OrbisPatchDirectoryRef + @"\";
                }
                else 
                { 
                    patchDirectory = patchDirectoryRoot + @"ps4\package\" + branch + @"\" + OrbisPatchDirectoryRef + @"\";
                }
            }
            if (button.Content.ToString().Contains("Durango"))
            {
                if (button.Content.ToString().Contains("Japanese"))
                {
                    branch = branch.Replace("dev", "Japanese");
                    patchDirectory = patchDirectoryRoot + @"xboxone\package\" + branch + @"\" + DurangoPatchDirectoryRef + @"\";
                }
                else
                {
                    patchDirectory = patchDirectoryRoot + @"xboxone\package\" + branch + @"\" + DurangoPatchDirectoryRef + @"\";
                }
            }
            return patchDirectory;
        }
        /// <summary>
        /// Create the backup patch directory
        /// </summary>
        /// <param name="button"></param>
        public void CreateBackupPatchDirectory(Button button)
        {
            string winrarLocation = "";
            string patchDirectory = getPatchDirectoryFromButton(button, xmlFileref);
            if(button.Content.ToString().ToLower().Contains("japanese"))
            {
                patchDirectory = patchDirectory.Replace("dev_", "japanese_");
            }
            string replacement = patchDirectory.Split('\\').Reverse().Skip(1).Take(1).FirstOrDefault() + "\\";
            patchDirectory = patchDirectory.Replace(replacement, "");

            winrarLocation = locateWinrar();
            if(string.IsNullOrWhiteSpace(winrarLocation))
            {
                MessageBox.Show("Could not locate winrar", "WinRAR not found", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (System.IO.Directory.Exists(patchDirectory))
            {
                BackupPatchWindow backupPatchWindow = new BackupPatchWindow(patchDirectory, button.Content.ToString(), xmlFileref, winrarLocation);
                backupPatchWindow.Show();
            }
            else {
                RsMessageBox.Show(Application.Current.MainWindow, "Directory does not exist: " + patchDirectory, "Error", MessageBoxButton.OK);
            }
        }
        /// <summary>
        /// set xml reference 
        /// </summary>
        /// <param name="element">parameter to write</param>
        /// <param name="parent">parent for child "element" to write to</param>
        /// <param name="data">data to write</param>
        /// <param name="xmlFile">settings.xml</param>
        public void SetXMLReference(string element, string parent, string data, string xmlFile)
        {
            xmlOps.WriteStringToElement(regOps.RegexAndRemoveChar(element, @"(_\w+)", '_'), regOps.RegexAndRemoveChar(parent, @"(\w+_)", '_'), data, xmlFile);
        }
        /// <summary>
        /// set name and file for button reference in prebuild and Release
        /// </summary>
        /// <param name="button_name"></param>
        public void SetNameAndFile(string button_name)
        {
            ButtonNameWindow BNW = new ButtonNameWindow(xmlFileref, button_name);
            BNW.Show();
        }


        /// <summary>
        /// get xml string value from button name.
        /// </summary>
        /// <param name="button"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public string GetXmlStringvalueFromButtonName(Button button, string xmlFile)
        {
            string stringValue = "";
            stringValue = xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), regOps.RegexButtonNameToParent(button), xmlFile);
            return stringValue;
        }

        /// <summary>
        /// get xml string value from button name and parent.
        /// </summary>
        /// <param name="button"></param>
        /// <param name="parent"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public string GetXmlStringvalueFromButtonName(Button button, string parent, string xmlFile)
        {
            string stringValue = "";
            stringValue = xmlOps.GetElementFromXMLFile(regOps.RegexButtonNameToElement(button), parent, xmlFile);
            return stringValue;
        }

        /// <summary>
        /// login to bugstar service
        /// </summary>
        /// <param name="username">password</param>
        /// <param name="password">username</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public bool LoginToBugstar(string username, string password, string xmlFile)
        {
//          return bugstarOps.BugstarLogin(username, password, new Uri (@"https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/")); //offical one
            return bugstarOps.BugstarLogin(username, password, new Uri (xmlOps.GetElementFromXMLFile("BugstarURL","Setup", @""+xmlFile)),xmlFile);
        }

        /// <summary>
        /// Get bug info from perforce Cl delta
        /// </summary>
        /// <param name="xmlFile">Settings.xml</param>
        /// <param name="project">Project reference</param>
        /// <returns></returns>
        public List<Bug> GetBugInformationFromPerforceCLReferences(string xmlFile, string project)
        {
//            bugstarOps.GetBugNumbersFromOldCLVsNewCLLists(xmlFile,ini.GetAdditionalCLString());
            List<string> CLList;
            List<string> BugDescriptionList = new List<string>();
            List<Bug> buglist = new List<Bug>();

            foreach (DepotDirectory DB in ini.GetDepotDirectoryList().ToList<DepotDirectory>())
            {
                CLList = new List<string>();
                List<EnvironmentVariable> environmentList = new List<EnvironmentVariable>();
                environmentList = ini.GetEnvironmentVariables();
                string CLBranch = "";
                foreach (EnvironmentVariable enVar in environmentList)
                {
                    if (enVar.GetEnvironmentVariableLocalName() == DB.GetDepotEnvironmentVariableName())
                    {
                        CLBranch = enVar.GetEnvironmentVariableCLName();
                        break;
                    }
                }
                string oldCLData = xmlOps.GetElementFromXMLFile(CLBranch, "OldCLData", @"" + xmlFile);
                if (oldCLData != xmlOps.GetElementFromXMLFile(CLBranch, "CurrentCLData", @"" + xmlFile))
                {
                    string correctDBDepot = (DB.GetDepotDirectory().ToString().EndsWith("/"))? DB.GetDepotDirectory().ToString() + "..." : (!DB.GetDepotDirectory().ToString().EndsWith("/..."))? DB.GetDepotDirectory().ToString() + " /..." : DB.GetDepotDirectory().ToString();

                    CLList = regOps.RegexTextAndReturnList("("+xmlOps.GetElementFromXMLFile("BugstarRegexPattern", "Setup", @"" + xmlFile)+")", perfOps.GetChangesBetweenTwoCLs(correctDBDepot, Int32.Parse(xmlOps.GetElementFromXMLFile(CLBranch, "OldCLData", @"" + xmlFile)),Int32.Parse(xmlOps.GetElementFromXMLFile(CLBranch, "CurrentCLData", @"" + xmlFile))));
                }
                if(CLList!=null||CLList.Count<1)
                {
                    foreach (string str in CLList)
                    {
                        if (BugDescriptionList.Contains(str) == false) { 
                            BugDescriptionList.Add(str);
                        }
                    }
                }
            }
            string additionalCLs = ini.GetAdditionalCLString();
            if (additionalCLs != "")
            {
                List<string> AdditionlCLList = new List<string>();
                AdditionlCLList.AddRange(regOps.RegexTextAndReturnList(xmlOps.GetElementFromXMLFile("CLRegexPattern", "Setup", @"" + xmlFile), additionalCLs));
                foreach (string str in AdditionlCLList)
                {
                    BugDescriptionList.AddRange(regOps.RegexTextAndReturnList("(" + xmlOps.GetElementFromXMLFile("BugstarRegexPattern", "Setup", @"" + xmlFile) + ")", perfOps.GetChangesFromOneCL(str)));
                }
            }
            if (BugDescriptionList.Count() > 0) {
                string xmlBugList = xmlOps.ListToXMLString(BugDescriptionList);
                buglist = bugstarOps.GetBugInfoViaXML(xmlBugList, new Uri(xmlOps.GetElementFromXMLFile("BugstarURL", "Setup", @"" + xmlFile)), project,xmlFile);
            }
            else
            {
                buglist = null;
            }

            return buglist;


        }
        /// <summary>
        /// Generate PreBuild Report based between two cl number deltas getting info from p4 description for each cl per branch 
        /// </summary>
        /// <param name="project">Game Project</param>
        /// <param name="xmlFile">settings.xml</param>
        public void GeneratePrebuildReport(string project, string xmlFile)
        {
            List<Bug> buglist = new List<Bug>();
            StringBuilder emailbody = new StringBuilder();

            buglist = GetBugInformationFromPerforceCLReferences(xmlFile, project);
            if (buglist != null)
            {
                if (buglist.Count <= 0)
                {
                    System.Windows.MessageBox.Show("Zero results returned.");
                }
                else
                {
                    foreach (Bug bug in buglist.ToList<Bug>())
                    {
                        if (bug.project != project)
                        {
                            buglist.Remove(bug);
                        }
                    }
                    emailbody = PrebuildReportEmailGenerator(buglist, xmlFile);
                    Outlook.Application outlookApp = new Outlook.Application();
                    Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                    mailItem.To = "NGPrebuild@rockstarnorth.com";
                    mailItem.Subject = "Prebuild Updated - " + System.DateTime.Now.TimeOfDay.ToString("hh\\:mm");
                    mailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = emailbody.ToString();
                    try { mailItem.Display(false); }
                    catch (Exception e) { Console.WriteLine(e.InnerException.ToString()); }
                    finally{}
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Zero results returned.");
            }
        }
        /// <summary>
        /// generates prebuild email based off of bug information 
        /// </summary>
        /// <param name="buglist">buglist</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public StringBuilder PrebuildReportEmailGenerator(List<Bug> buglist, string xmlFile)
        {
            StringBuilder emailString = new StringBuilder();
            emailString.Append(xmlOps.GetElementFromXMLFile("BugstarReportEmail", "Setup", xmlFile));//Getting the body of the email
            //adding the CL numbers
            List<ElementData> CLList = new List<ElementData>();
            CLList = xmlOps.GetListOfChildrenAndDatafromParent("CurrentCLData",xmlFile);
            emailString.AppendLine("<BR>");
            foreach (ElementData ed in CLList)
            {
                if (ed.element != "Tools")
                {
                    if (ed.element == "Rage")
                    {
                        if (xmlOps.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile) == "rdr3")
                        {
                            emailString.AppendLine("Companion" + ": " + ed.data + "<BR>");
                        }
                        else
                        {
                            emailString.AppendLine(ed.element + ": " + ed.data + "<BR>");
                        }
                    }
                    else
                    {
                        emailString.AppendLine(ed.element + ": " + ed.data + "<BR>");
                    }
                }
            }
            emailString.AppendLine("<BR>");
            emailString.AppendLine("<b>Fixes: </b><BR>");
            emailString.AppendLine("<UL>");
            //Adding the bug information 

            buglist = buglist.OrderBy(o => o.bugclass).ThenBy(o => o.summary).ToList();
            string currentBugClass = "";
            foreach (Bug bug in buglist)
            {
                if (bug.status != "DEV") {
                    if (bug.bugclass == currentBugClass)
                    {
                        emailString.AppendLine("<LI><a href=" + "url:bugstar:" + bug.bugNumber + ">" + bug.bugNumber + "</a>" + " - " + bug.summary);
                    }
                    else
                    {
                        emailString.AppendLine("</UL><BR>");
                        currentBugClass = bug.bugclass;
                        emailString.AppendLine("<b>" + bug.bugclass + " CLASS </b><BR>");
                        emailString.AppendLine("<UL>");
                        emailString.AppendLine("<LI><a href=" + "url:bugstar:" + bug.bugNumber + ">" + bug.bugNumber + "</a>" + " - " + bug.summary);
                    }
                    
                }
            }
            emailString.AppendLine("</UL></html></body>");
            return emailString;

        }

        /// <summary>
        /// class to keep the cookie alive
        /// </summary>
        /// <param name="dueTime">due time for renewal of cookie</param>
        /// <param name="interval">time it takes to trigger</param>
        /// <param name="token">token for calcelling</param>
        /// <param name="xmlFile">settings.xml</param>
        public async void AsyncKeepBugstarCookieAlive(TimeSpan dueTime, TimeSpan interval, CancellationToken token,string xmlFile)
        {
            if (dueTime > TimeSpan.Zero)
            {
                await Task.Delay(dueTime, token).ConfigureAwait(false);
            }
            while (!token.IsCancellationRequested)
            {
                bugstarOps.KeepCookieAlive(new Uri(xmlOps.GetElementFromXMLFile("BugstarURL", "Setup", @"" + xmlFile)));
                if (interval > TimeSpan.Zero)
                {
                    await Task.Delay(interval, token).ConfigureAwait(false);
                }
            }
        }
        /// <summary>
        /// class to copy data from settings.xml to textboxes listed.
        /// </summary>
        /// <param name="TBList">text box list</param>
        /// <param name="parent">parent element</param>
        /// <param name="stringToRemove">string to remove from tb_list reference (BTN_ etc)</param>
        /// <param name="xmlFile">settings.xml</param>
        public void CopyXMLtoTextBox(List<System.Windows.Controls.TextBox> TBList,string parent,string stringToRemove,string xmlFile)
        {
            List<ElementData> elementList = new List<ElementData>();
            elementList = xmlOps.GetListOfChildrenAndDatafromParent(parent,xmlFile);
            foreach (System.Windows.Controls.TextBox tb in TBList)
            {
                if (tb.Name.Contains(stringToRemove))
                {
                    foreach(ElementData ed in elementList){
                        if (tb.Name.ToString().Substring(stringToRemove.Length, tb.Name.ToString().Length - stringToRemove.Length) == ed.element.ToString())
                        {
                            tb.Text = ed.data;
                        }
                    }
                }
            }  
        }

        /// <summary>
        /// Updates a list of elements within a parent element from a list
        /// </summary>
        /// <param name="TBList">List of textboxes</param>
        /// <param name="parent"></param>
        /// <param name="stringToRemove"></param>
        /// <param name="xmlFile"></param>
        public void updateXMLParentWithList(List<System.Windows.Controls.TextBox> TBList, string parent, string stringToRemove, string xmlFile)
        {
            List<ElementData> elementList = new List<ElementData>();
            elementList = xmlOps.GetListOfChildrenAndDatafromParent(parent, xmlFile);
            foreach (System.Windows.Controls.TextBox tb in TBList)
            {
                if (tb.Name.Contains(stringToRemove))
                {
                    foreach (ElementData ed in elementList)
                    {
                        if (tb.Name.ToString().Substring(stringToRemove.Length, tb.Name.ToString().Length - stringToRemove.Length) == ed.element.ToString())
                        {
                            xmlOps.WriteStringToElement(ed.element, parent, tb.Text, xmlFile);
                        }
                    }
                }
            } 
        }

        /// <summary>
        /// writes list from one parent to another parent in xml
        /// </summary>
        /// <param name="Parent1"></param>
        /// <param name="Parent2"></param>
        /// <param name="xmlFile"></param>
        public void RestoreBackupCLData(string Parent1, string Parent2, string xmlFile)
        {
            xmlOps.WriteChildrenfromParentToParent(Parent1, Parent2, xmlFile);
        }

        /// <summary>
        /// Generate Compiler options from workspace data
        /// </summary>
        /// <param name="XmlRef"></param>
        public void PopulateCompilerOptions(string XmlRef)
        {
            string LocalDirectory =@""+ xmlOps.GetElementFromXMLFile("TUBuildRoot", "Environment", XmlRef);
            string Project = xmlOps.GetElementFromXMLFile("ToolsProject", "Setup", XmlRef);
            CompilerOps.InitialiseCompilerOptions(Project, xmlFileref, LocalDirectory);
            ListBox FirstListBox = ListOfListBoxes.Find(x => x.Name.Contains("Compiler_Source"));
            CompilerObjectList = CompilerOps.GetListOfExecutablesfromBranch(LocalDirectory);
            foreach (CompilerObject compilerObject in CompilerObjectList)
            {
               FirstListBox.Items.Add(""+compilerObject.solutionPlatform+"_"+ compilerObject.solutionConfiguration);
            }
            FirstListBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));

            ListBox SecondListBox = ListOfListBoxes.Find(x => x.Name.Contains("Compiler_Shader"));
            ShaderObjectList = CompilerOps.GetListOfShadersfromBranch(LocalDirectory+@"\common\shaders\");
            foreach (ShaderObject shaderObject in ShaderObjectList)
            {
                string joiner = "";
                if (shaderObject.solutionConfiguration != "") { joiner = "_"; } else { joiner = ""; }
                if (shaderObject.executableFileName.ToString().Contains("final"))
                {

                    SecondListBox.Items.Add("" + shaderObject.solutionPlatform + joiner + shaderObject.solutionConfiguration + "_Shader");
                }
                else
                {
                    SecondListBox.Items.Add("" + shaderObject.solutionPlatform + joiner + shaderObject.solutionConfiguration + "_Shader");
                }
                
            }
            SecondListBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));

        }
        /// <summary>
        /// get items for the build compiler options 
        /// </summary>
        /// <param name="BuildItemsList">list of string build items</param>
        /// <returns></returns>
        public List<CompilerObject> GetBuildItems(List<string> BuildItemsList)
        {
            List<CompilerObject> BuildCompilerObjectList = new List<CompilerObject>();
            foreach (string str in BuildItemsList)
            {
                BuildCompilerObjectList.Add(CompilerOps.GetCompilerObjectFromItem(str, CompilerObjectList));
            }
            return BuildCompilerObjectList;
        }

        /// <summary>
        /// Set build items flag for compile types 
        /// </summary>
        /// <param name="Flag">flag for compile</param>
        /// <param name="CompilerObjectList">list of compiler objects</param>
        /// <returns></returns>
        public List<CompilerObject> SetBuildItemsCompileTypeFlag(string Flag, List<CompilerObject> CompilerObjectList)
        {
            List<CompilerObject> compilerObjectList = new List<CompilerObject>();
            List<CompilerObject> compilerObjectListStore = new List<CompilerObject>();
            compilerObjectList = CompilerObjectList;

            foreach (CompilerObject compilerObject in compilerObjectList)
            {
                if (Flag == @"/Build")
                {
                    compilerObject.updateCompileType(@"/Build");
                    compilerObjectListStore.Add(compilerObject);
                }
                else if (Flag == @"/Rebuild")
                {
                    compilerObject.updateCompileType(@"/Rebuild");
                    compilerObjectListStore.Add(compilerObject);
                }
            }
            return compilerObjectListStore;
        }

        /// <summary>
        /// Shader compile item types
        /// </summary>
        /// <param name="Flag">flag to compile</param>
        /// <param name="ShaderItemList"> shader object list to compile</param>
        /// <returns></returns>
        public List<ShaderObject> SetshadertemsCompileTypeFlag(string Flag, List<ShaderObject> ShaderItemList)
        {
               List<ShaderObject> shaderItemList = new List<ShaderObject>();
               List<ShaderObject> shaderItemListStore = new List<ShaderObject>();
               shaderItemList = ShaderItemList;

               foreach (ShaderObject ShaderItem in shaderItemList)
               {
                   if (Flag == "rebuild")
                   {
                       ShaderObject shaderObjectTemp = new ShaderObject(ShaderItem.executableFileName, ShaderItem.project, ShaderItem.solutionPlatform, ShaderItem.solutionConfiguration);
                       shaderObjectTemp.updateCompileType("rebuild");
                       shaderItemListStore.Add(shaderObjectTemp);
                       if (xmlOps.GetElementFromXMLFile("RebuildThenBuildShader", "Setup", xmlFileref)=="true") {
                           ShaderObject shaderObjectTemp2 = new ShaderObject(ShaderItem.executableFileName, ShaderItem.project, ShaderItem.solutionPlatform, ShaderItem.solutionConfiguration);
                           shaderObjectTemp2.updateCompileType("build");
                           shaderItemListStore.Add(shaderObjectTemp2);
                       }
                   }
                   else
                   {
                       ShaderObject shaderObjectTemp = new ShaderObject(ShaderItem.executableFileName, ShaderItem.project, ShaderItem.solutionPlatform, ShaderItem.solutionConfiguration);
                       shaderObjectTemp.updateCompileType("build");
                       shaderItemListStore.Add(shaderObjectTemp);
                   }

               }
//             {
//                 if (Flag == "Build")
//                 {
//                     shaderItemList.updateCompileType(@"/Build");
//                     shaderItemListStore.Add(ShaderItem);
//                 }
//                 else
//                 {
//                     if (Flag == "Rebuild")
//                     {
// 
//                         if (CurrentSolutionPlatform != compilerObject.solutionPlatform)
//                         {
//                             uniquePlatform = true;
//                         }
//                         //add rebuild then build for first project
//                         if (uniquePlatform == true)
//                         {
//                             CompilerObject tempCompilerObject = compilerObject;
//                             tempCompilerObject.updateCompileType(@"/Rebuild");
//                             compilerObjectListStore.Add(tempCompilerObject);
//                             tempCompilerObject.updateCompileType(@"/Build");
//                             compilerObjectListStore.Add(tempCompilerObject);
//                             uniquePlatform = false;
//                         }
//                         else
//                         {
//                             compilerObject.updateCompileType(@"/Build");
//                             compilerObjectListStore.Add(compilerObject);
//                         }
//                     }
//                 }
//             }
               return shaderItemListStore;
        }

        /// <summary>
        /// Get list of shader items from a list of strings
        /// </summary>
        /// <param name="ShaderItemList">list of string references for shader items.</param>
        /// <returns></returns>
        public List<ShaderObject> GetShaderItems(List<string> ShaderItemList)
        {
            List<ShaderObject> ShaderCompilerObjectList = new List<ShaderObject>();
            foreach (string str in ShaderItemList)
            {
                ShaderCompilerObjectList.Add(CompilerOps.GetShaderObjectFromItem(str, ShaderObjectList));
            }
            return ShaderCompilerObjectList;
        }

        /// <summary>
        /// generate a list of both shaders and executables and compile 
        /// </summary>
        /// <param name="Flag">compiler flag</param>
        /// <param name="ShaderList">list of shaders to compile</param>
        /// <param name="CompileList">list of shaders to compile</param>
        /// <returns>returns task complete</returns>
        public async Task<bool> CompileListOfShadersAndExes(string Flag,List<ShaderObject> ShaderList, List<CompilerObject> CompileList)
        {

            bool complete = false;
            string LogDirectory = System.IO.Directory.GetCurrentDirectory() + xmlOps.GetElementFromXMLFile("LogDir", "Setup",xmlFileref);
           
            List<ShaderObject> TempShaderList = new List<ShaderObject>();
            TempShaderList = ShaderList.OrderByDescending(o => o.executableFileName).ToList();
            List<CompilerObject> TempCompileList = new List<CompilerObject>();
            TempCompileList = CompileList.OrderBy(o => o.solutionPlatform).ThenBy(i=>i.objectweight).ToList();
            List<string> Errors = new List<string>();
            if (ShaderList.Count > 0)
            {
                complete = await CompileListOfShaderObjects(Flag, TempShaderList);
            }
            if (CompileList.Count > 0)
            {
                if (Flag == "rebuild") 
                {
                    complete = await CompileListOfCompilerObjects("/Rebuild", TempCompileList);
                }
                if (Flag == "build")
                {
                    complete = await CompileListOfCompilerObjects("/Build", TempCompileList);
                }
            }
            await Task.Run(() =>
            {
                checkTextForErrors(LogDirectory).ConfigureAwait(false);
            });
            DisplayBuildErrors(listOfErrors, LogDirectory);
            return complete;
        }

        /// <summary>
        /// display build errors on completion
        /// </summary>
        /// <param name="errorList"> list of errors </param>
        /// <param name="logDirectory"> log directory </param>
        public void DisplayBuildErrors(List<string> errorList,string logDirectory)
        {
            if (errorList.Count == 1)
            {
                if (errorList.First() == "Build Complete")
                {
                    if (RsMessageBox.Show(Application.Current.MainWindow,"Build completed with no errors", "Build Complete", MessageBoxButton.OK) == MessageBoxResult.OK)
                    {
                    }
                }
                else
                {
                    if (RsMessageBox.Show(Application.Current.MainWindow, "The Following Error(s) have been observed in the logs: \n" + errorList.First().ToString() + "Would you like to open the log directory?", "CompileError", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        System.Diagnostics.Process.Start(logDirectory);
                    }
                }
            }
            else
            {
                if (errorList.Count >= 1)
                {
                    StringBuilder SB = new StringBuilder();
                    foreach (string str in errorList)
                    {
                        SB.AppendLine(str);
                    }
                    if (RsMessageBox.Show(Application.Current.MainWindow, "The Following Error(s) have been observed in the logs: \n" + SB + "Would you like to open the log directory?", "CompileError", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        System.Diagnostics.Process.Start(logDirectory);
                    }
                }
            }
        }

        /// <summary>
        ///  checks text output for errors (compiler)
        /// </summary>
        /// <param name="LogDirectory">Log directory</param>
        /// <returns>true if error occurs</returns>
        public  async Task<bool> checkTextForErrors(string LogDirectory)
        {
            bool errored = false;
            listOfErrors.Clear();
            bool success = false;
            foreach (var textfile in System.IO.Directory.GetFiles(LogDirectory, "*.txt"))
            {
                try
                {
                    List<string> stringList = new List<string>();
                    stringList = System.IO.File.ReadAllLines(textfile, Encoding.Default).ToList<string>();

                    int test = stringList.Count;
                    if (test >= 20)
                    {
                        test = stringList.Count - 20;
                    }
                    for (int i = test; i < stringList.Count - 1; i++)
                    {
                        string line = stringList[i];
                        if (String.IsNullOrEmpty(line))
                        {
                            continue;
                        }

                        if (String.IsNullOrEmpty(regexOps.RegexText("Build terminated at user request", line)) == false)
                        {
                            listOfErrors.Add("Build terminated at user request " + System.IO.Path.GetFileName(textfile) + "\n");
                            errored = true;
                            success = false;
                        }

                        if (String.IsNullOrEmpty(regexOps.RegexText("Build succeeded", line)) == false)
                        {
                            string compileoutput = regexOps.RegexText(@"Build: \d succeeded, \d failed, \d up-to-date, \d skipped", line);
                            if (String.IsNullOrEmpty(compileoutput) == false)
                            {
                                string tempoutput = compileoutput.Replace("Build: ", "").Split(',')[1];
                                int result;
                                Int32.TryParse(regexOps.RegexText(@"\d+", tempoutput), out result);
                                if (result > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }
                            compileoutput = regexOps.RegexText(@"Build: \d succeeded, \d failed, \d skipped", line);
                            if (String.IsNullOrEmpty(compileoutput) == false)
                            {
                                string tempoutput = compileoutput.Replace("Build: ", "").Split(',')[1];
                                int result;
                                Int32.TryParse(regexOps.RegexText(@"\d+", tempoutput), out result);
                                if (result > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }


                            if (String.IsNullOrEmpty(regexOps.RegexText(@"\d Error", line)) == false)
                            {
                                int errorResult;
                                Int32.TryParse(regexOps.RegexText(@"\d", line), out errorResult);
                                if (errorResult > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }

                            if (String.IsNullOrEmpty(regexOps.RegexText(@"error :", line)) == false)
                            {
                                if (listOfErrors.Contains("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n") == false)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                            }

                            if ((String.IsNullOrEmpty(regexOps.RegexText(@"FAILED.", line)) == false)&&(String.IsNullOrEmpty(regexOps.RegexText(@".*CONNECT TO PRIMARY COORDINATOR", line))))
                            {
                                if (listOfErrors.Contains("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n") == false)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                            }

                        }
                        else
                        {
                            string compileoutput = regexOps.RegexText(@"Build: \d succeeded, \d failed, \d up-to-date, \d skipped", line);
                            if (String.IsNullOrEmpty(compileoutput) == false)
                            {
                                string tempoutput = compileoutput.Replace("Build: ", "").Split(',')[1];
                                int result;
                                Int32.TryParse(regexOps.RegexText(@"\d+", tempoutput), out result);
                                if (result > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }

                            compileoutput = regexOps.RegexText(@"Build: \d succeeded, \d failed, \d skipped", line);
                            if (String.IsNullOrEmpty(compileoutput) == false)
                            {
                                string tempoutput = compileoutput.Replace("Build: ", "").Split(',')[1];
                                int result;
                                Int32.TryParse(regexOps.RegexText(@"\d+", tempoutput), out result);
                                if (result > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }

                            if (String.IsNullOrEmpty(regexOps.RegexText(@"\d+ Error", line)) == false)
                            {
                                int errorResult;
                                Int32.TryParse(regexOps.RegexText(@"\d+", line), out errorResult);
                                if (errorResult > 0)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                                else
                                {
                                    success = true;
                                }
                            }
                            if (String.IsNullOrEmpty(regexOps.RegexText(@"error :", line)) == false)
                            {
                                if (listOfErrors.Contains("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n") == false)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                            }

                            if ((String.IsNullOrEmpty(regexOps.RegexText(@"FAILED.", line)) == false) && (String.IsNullOrEmpty(regexOps.RegexText(@".*CONNECT TO PRIMARY COORDINATOR", line))))
                            {
                                if (listOfErrors.Contains("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n") == false)
                                {
                                    listOfErrors.Add("Error(s) found in " + System.IO.Path.GetFileName(textfile) + "\n");
                                    errored = true;
                                    success = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            if (errored == false)
            {
                if (success == false)
                {
                    listOfErrors.Add("Build did not complete successfully");
                }
                else
                {
                    listOfErrors.Add("Build Complete");
                }
            }
            await Task.Delay(0).ConfigureAwait(false);
            return errored;
        }

        /// <summary>
        /// Generate a list of compiler objects from list
        /// </summary>
        /// <param name="Flag"> compiler flag </param>
        /// <param name="CompilerOjectList"> compiler object string list of items</param>
        /// <returns></returns>
        public async Task<bool> CompileListOfCompilerObjects(string Flag, List<CompilerObject> CompilerOjectList)
        {
            List<CompilerObject> tempCompilerList = new List<CompilerObject>();
            tempCompilerList = SetBuildItemsCompileTypeFlag(Flag, CompilerOjectList);
            bool complete = await CompilerOps.CompileSelectedConfigurations(tempCompilerList, ini.Get_Current_Directory().Remove(ini.Get_Current_Directory().Length - 1, 1)).ConfigureAwait(false);
            return complete;
        }

        /// <summary>
        /// Generate a list of shader objects from list
        /// </summary>
        /// <param name="Flag">compiler flag </param>
        /// <param name="ShaderObjectList"> compiler object string list of items</param>
        /// <returns></returns>
        public async Task<bool> CompileListOfShaderObjects(string Flag, List<ShaderObject> ShaderObjectList)
        {
            List<ShaderObject> tempShaderList = new List<ShaderObject>();
            tempShaderList = SetshadertemsCompileTypeFlag(Flag, ShaderObjectList);
            bool complete = await CompilerOps.CompileSelectedShader(Flag, tempShaderList, ini.Get_Current_Directory()).ConfigureAwait(false);
            return complete;
        }

        /// <summary>
        /// check the log files in a directory for error and warn user
        /// </summary>
        /// <param name="LocationOfLogs">log location</param>
        /// <returns></returns>
        public List<string> CheckCompiledLogsForErrorsAndDisplay(string LocationOfLogs)
        {
            List<string> ErrorsList = new List<string>();

            CheckFilesInDirectoryForString(LocationOfLogs, "txt", "", "Number of errors");

            //Number of errors
            if (ErrorsList.Count == 0)
            {
                ErrorsList.Add("No Errors Reported");
            }
            return ErrorsList;
        }

        /// <summary>
        /// stop compile flag (clears the rest of the compile list) still finishes current compile
        /// </summary>
        /// <param name="stopCompile"></param>
        public void SetStopCompile(bool stopCompile)
        {
            CompilerOps.SetStopCompileFlag(stopCompile);
        }


        /// <summary>
        /// check all files in a directory for string reference 
        /// </summary>
        /// <param name="directory">directory to seach in</param>
        /// <param name="fileType">file type</param>
        /// <param name="FileName">file to check</param>
        /// <param name="searchstring">string reference</param>
        /// <returns>returns hits via list in string</returns>
        public List<string> CheckFilesInDirectoryForString(string directory, string fileType, string FileName, string searchstring)
        {
            string[] arrayOfTextFilesinDirectory = new string[System.IO.Directory.GetFiles(directory, fileType).Length];
            arrayOfTextFilesinDirectory = System.IO.Directory.GetFiles(directory, "*" + fileType);
            List<string> listOfTextFilesinDirectory = arrayOfTextFilesinDirectory.ToList();
            List<string> errorList = new List<string>();
            if (listOfTextFilesinDirectory != null)
            {
                foreach (string filename in listOfTextFilesinDirectory)
                {
                    if (filename.Contains(FileName) && FileName != "")
                    {
                        try
                        {
                            string fileToRead = System.IO.File.ReadAllText(@"" + filename);
                            Regex rx = new Regex(searchstring, RegexOptions.None);
                            MatchCollection mc = rx.Matches(fileToRead);
                            foreach (Match m in mc)
                            {
                                errorList.Add (m.ToString() + " in " + filename.Replace(directory, "") + "\n");
                            }
                        }
                        catch (System.IO.IOException)
                        {
                        }
                    }
                    if (FileName == "")
                    {
                        try
                        {
                            string fileToRead = System.IO.File.ReadAllText(@"" + filename);
                            Regex rx = new Regex(searchstring, RegexOptions.None);
                            MatchCollection mc = rx.Matches(fileToRead);
                            foreach (Match m in mc)
                            {
                                errorList.Add(m.ToString() + " in " + filename.Replace(directory, "") + "\n");
                            }
                        }
                        catch (System.IO.IOException)
                        {
                        }

                    }
                }

            }
            return errorList;
        }

        /// <summary>
        /// get list of files to check out from reference of branch from workspace
        /// </summary>
        /// <returns>check out items</returns>
        public List<CheckOutItem> GetListOfFileToCheckOut()
        {
            List<string> CheckoutItemTypesList = new List<string>(xmlOps.GetListOfChildrenfromParent("Checkout", xmlFileref));
            List<CheckOutItem> checkoutItemList = new List<CheckOutItem>();
            string branch = xmlOps.GetElementFromXMLFile("TUBuildRoot", "Environment", xmlFileref);

            foreach (string itemType in CheckoutItemTypesList.ToList<string>())
            {
                string item = xmlOps.GetElementFromXMLFile(itemType, "Checkout", xmlFileref);
      
                if (itemType.Contains("Shaders"))
                {

                    List<string> ListOfFiles = new List<string>();
                    if (System.IO.Directory.Exists(branch))
                    {
                        ListOfFiles = System.IO.Directory.GetDirectories(branch, itemType, System.IO.SearchOption.AllDirectories).ToList<string>();
                    }
                    if (ListOfFiles.Count > 0) { 
                        ListOfFiles = System.IO.Directory.GetDirectories(ListOfFiles.First()).ToList<string>();
                        foreach (string str in ListOfFiles.ToList<string>())
                        {
                            foreach (string itemRef in item.Split(',').ToList<string>())
                            {
                                if (str.Contains(itemRef)==false)
                                {
                                    List<string> fileOrFolderList = new List<string>();
                                    if (str.EndsWith("/"))
                                    {
                                        fileOrFolderList.Add(str + @"...");
                                    }
                                    else
                                    {
                                        fileOrFolderList.Add(str + @"/...");
                                    }
                                    string FriendlyName = System.IO.Path.GetFileName(str);
                                    CheckOutItem checkoutItem = new CheckOutItem(fileOrFolderList, itemType, item, FriendlyName);
                                    checkoutItemList.Add(checkoutItem);
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (string itemRef in item.Split(',').ToList<string>())
                    {
                        List<string> ListOfFiles = new List<string>();
                        if (itemRef.ToLower().Contains('.'))
                        {
                            if (System.IO.Directory.Exists(branch))
                            {
                                ListOfFiles = System.IO.Directory.GetFiles(branch, itemRef.ToLower(), System.IO.SearchOption.AllDirectories).ToList<string>();
                            }
                        }
                        else
                        {
                            if (System.IO.Directory.Exists(branch))
                            {
                                ListOfFiles = System.IO.Directory.GetFiles(branch, "*." + itemRef.ToLower()).ToList<string>();
                            }
                        }

                        foreach (string str in ListOfFiles.ToList<string>())
                        {
                            List<string> fileOrFolderList = new List<string>();
                            fileOrFolderList.Add(str);
                            string FriendlyName = "";
                            if (itemType.Contains("Script"))
                            {
                                string[] strA = Regex.Split(str,@"\\");
                                FriendlyName = strA[6] + "_" + System.IO.Path.GetFileNameWithoutExtension(str).ToLower();
                            }
                            else
                            {
                                if (itemType.Contains("other") == true)
                                {
                                    string test = "";
                                    test = itemType;
                                }
                                FriendlyName = System.IO.Path.GetFileNameWithoutExtension(str).ToLower();
                            }


                            CheckOutItem checkoutItem = new CheckOutItem(fileOrFolderList, itemType, item, FriendlyName);
                            checkoutItemList.Add(checkoutItem);
                        }
                    }
                }

            }
            if (xmlOps.GetElementFromXMLFile("RageRoot", "Environment", xmlFileref).Contains("companion"))
            {
                branch = xmlOps.GetElementFromXMLFile("RageRoot", "Environment", xmlFileref);
                foreach (string itemType in CheckoutItemTypesList.ToList<string>())
                {
                    string item = xmlOps.GetElementFromXMLFile(itemType, "Checkout", xmlFileref);
                    foreach (string itemRef in item.Split(',').ToList<string>())
                    {
                        List<string> ListOfFiles = new List<string>();
                        if (itemRef.ToLower().Contains('.'))
                        {
                            if (System.IO.Directory.Exists(branch)) { 
                                ListOfFiles = System.IO.Directory.GetFiles(branch, itemRef.ToLower(), System.IO.SearchOption.AllDirectories).ToList<string>();
                            }
                        }
                        else
                        {
                            if (System.IO.Directory.Exists(branch))
                            {
                                ListOfFiles = System.IO.Directory.GetFiles(branch, "*." + itemRef.ToLower()).ToList<string>();
                            }
                        }

                        foreach (string str in ListOfFiles.ToList<string>())
                        {
                            List<string> fileOrFolderList = new List<string>();
                            fileOrFolderList.Add(str);
                            string FriendlyName = "";
                            if (itemType.Contains("Script"))
                            {
                                string[] strA = Regex.Split(str, @"\\");
                                FriendlyName = strA[2].Split('_').Last() + "_" + System.IO.Path.GetFileNameWithoutExtension(str).ToLower();
                            }
                            else
                            {
                                if (itemType.ToLower().Contains("other") == true)
                                {
                                    string[] strA = Regex.Split(str, @"\\");
                                    FriendlyName = strA[2].Split('_').Last() + "_" + System.IO.Path.GetFileNameWithoutExtension(str).ToLower();
                                }
                                else
                                {
                                    FriendlyName = System.IO.Path.GetFileNameWithoutExtension(str).ToLower();
                                }
                            }


                            CheckOutItem checkoutItem = new CheckOutItem(fileOrFolderList, itemType, item, FriendlyName);
                            checkoutItemList.Add(checkoutItem);
                        }
                    }
                }
            }
  
            foreach (CheckOutItem item in checkoutItemList.ToList())
            {

                Console.WriteLine(item.xmlReference);
                foreach(string str in item.fileOrFolderList)
                {
                    Console.WriteLine(""+str);
                }
                Console.WriteLine("");
                foreach (string refstr in item.typeRefList)
                {
                    Console.Write("" + refstr + ",");
                }
                Console.WriteLine("  " + item.friendlyReference);
            }
            return checkoutItemList;
        }

        /// <summary>
        /// Backups then clears the build log directory
        /// </summary>
        /// <param name="xmlFile"></param>
        public void BackupThenClearBuildLogs(string xmlFile)
        {
            foreach (string File in System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory()+xmlOps.GetElementFromXMLFile("LogDir", "Setup", xmlFile)))
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(File);
                if(fileOps.IsFileLocked(fileInfo)==false)
                {
                    try
                    {
                        System.IO.File.Copy(File, System.IO.Path.GetDirectoryName(File) + @"\Backup\" + System.IO.Path.GetFileName(File), true);
                        System.IO.File.Delete(File);
                    }
                    catch
                    {
                    }
                }
            }
        }

        /// <summary>
        /// gets the args from xml data
        /// </summary>
        /// <param name="buttonName">Button name used for element refrence</param>
        /// <param name="parent">parent reference for element</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns>args space delineated</returns>
        internal string retrieveArgsForBatfile(string buttonName, string parent, string xmlFile)
        {

            string args = "";
            if (xmlOps.GetElementFromXMLFile(buttonName, parent, xmlFile).Split(';').Last() == "none")
            {
                args = "";
            }
            else
            {
                args = xmlOps.GetElementFromXMLFile(buttonName, parent, xmlFile).Split(';').Last();
            }

            return args;

        }
        /// <summary>
        /// Find and return winrar location
        /// </summary>
        /// <returns>Returns winrar location or empty string if not found</returns>
        private string locateWinrar()
        {
            string winrar = "";

            if (File.Exists("C:\\Program Files (x86)\\7-Zip\\7z.exe"))
            {
                winrar = "C:\\PROGRA~2\\7-Zip\\7z.exe";
            }
            else if (File.Exists("C:\\Program Files\\7-Zip\\7z.exe"))
            {
                winrar = "C:\\PROGRA~1\\7-Zip\\7z.exe";
            }
            else
            {
                MessageBox.Show("Could not locate 7-zip", "7-zip not found", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return winrar;
        }
    }


}



