﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Threading;

namespace BuildToolV2
{
    /// <summary>
    /// Interaction logic for BackupPatchWindow.xaml
    /// </summary>
    /// 
    delegate void CloseWindowDelegate();
    public partial class BackupPatchWindow : RsMainWindow
    {
        public string version;
        public string password = "";
        public string patchButtonName = "";
        public XMLOperators xmlOps = new XMLOperators();
        public FileOperators fileOps = new FileOperators();
        public string xmlFileref;
        string patchDirectoryRoot = "";
        string branch = "";
        string BackupDirectoryPath = "";
        string BackupRootDirectory = "";
        string platform = "";
        string patchDirectory = "";
        string winrarLocation = "";

        /// <summary>
        /// Setup new backup patch window 
        /// </summary>
        /// <param name="_patchDirectory"> path to the patch directory as string</param>
        /// <param name="_patchButtonName">string button name for path uses</param>
        /// <param name="_xmlFileref">string reference for settings.xml</param>
        public BackupPatchWindow(string _patchDirectory,string _patchButtonName,string _xmlFileref, string winrar)
        {
            patchButtonName = _patchButtonName;
            patchDirectoryRoot = xmlOps.GetElementFromXMLFile("PatchDirectory", "Setup", _xmlFileref);
            branch = xmlOps.GetElementFromXMLFile("BuildRoot", "Environment", _xmlFileref).Split('\\').Last();
            winrarLocation = winrar;
            if (_patchButtonName.Contains("Japanese"))
            {
               branch = branch.Replace("dev", "Japanese");
            }
            patchDirectory = _patchDirectory;
            xmlFileref=_xmlFileref;
            InitializeComponent();
        }

        /// <summary>
        /// set paths and references from window, password etc and rar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Backup_Button_Click_Next(object sender, RoutedEventArgs e)
        {
            LogHelper.g_Log.MessageCtx("Backup_Button_Click_Next", "Beginning patch rar for distribution");
            LogHelper.g_Log.MessageCtx("Backup_Button_Click_Next", "WinRAR location {0}", winrarLocation);
            string DurangoPatchDirectoryRef = xmlOps.GetElementFromXMLFile("DurangoPatchDirectory", "Setup", xmlFileref);
            string OrbisPatchDirectoryRef = xmlOps.GetElementFromXMLFile("OrbisPatchDirectory", "Setup", xmlFileref);
            //float.TryParse(VersionNumber.Text.ToString(), out version);
            version = VersionNumber.Text.ToString();
            List<string> exclusionList = new List<string>();
            List<string> includeList = new List<string>();
            exclusionList.Add(xmlOps.GetElementFromXMLFile("OrbisPatchFilesExclude", "Setup", xmlFileref));
            includeList.Add(xmlOps.GetElementFromXMLFile("OrbisPatchFilesExclude", "Setup", xmlFileref));
            exclusionList.Add(xmlOps.GetElementFromXMLFile("DurangoPatchDirectoryExclude", "Setup", xmlFileref));
            bool archiveWanted = (bool)ArchiveWanted.IsChecked;
            if (RarAndPasswordProtect.IsChecked == true)
            {
                LogHelper.g_Log.MessageCtx("Backup_Button_Click_Next", "Password detected {0}", Password.Text.ToString());
                password = Password.Text.ToString();
            }
            if (patchButtonName.Contains("Orbis"))
            {
                platform = "ps4";
            }
            if (patchButtonName.Contains("Durango"))
            {
                platform = "xboxone";
            }
            BackupDirectoryPath = fileOps.OpenFileBrowserDialogueAndRetrieveFileStringName("",true);

            BackupRootDirectory = BackupDirectoryPath + @"\" + branch + "_" + patchButtonName.Replace(" Japanese","") + "_Deploy_v" + version;
            LogHelper.g_Log.MessageCtx("Backup_Button_Click_Next", "Backup root directory {0}", BackupRootDirectory);
            if (BackupDirectoryPath == "")
            {
                return;
            }
            if (patchDirectory.Contains(DurangoPatchDirectoryRef))
            {
                patchDirectory.Replace(DurangoPatchDirectoryRef + @"\", "");
            }
            if (Directory.Exists(BackupRootDirectory + @"\" + platform + @"\package\" + branch))
            {
                try
                {
                    Directory.Delete(BackupRootDirectory + @"\" + platform + @"\package\" + branch, true);
                }
                catch(Exception fileException)
                {
                    RsMessageBox.Show(fileException.Message.ToString());
                }
            }else
            {
                Directory.CreateDirectory(BackupRootDirectory + @"\" + platform + @"\package\" + branch);
            }
            ChangeUI();
            await DirectoryCopy(patchDirectory, BackupRootDirectory + @"\" + platform + @"\package\" + branch, true, true, exclusionList).ConfigureAwait(false);
            await IncludedFileCopy(patchDirectory, BackupRootDirectory + @"\" + platform + @"\", includeList).ConfigureAwait(false);
            await Task.Delay(1).ConfigureAwait(false);
            if (archiveWanted)
            {
                Directory.CreateDirectory(BackupRootDirectory + "_Temp");
                await RarDirectory(password, BackupRootDirectory + @"_Temp\" + BackupRootDirectory.Split('\\').Last().ToString() + ".rar", BackupRootDirectory).ConfigureAwait(false);
                await Task.Delay(1000).ConfigureAwait(false);
                Directory.Move(BackupRootDirectory, BackupRootDirectory + "_");
                await Task.Delay(1000).ConfigureAwait(false);
                Directory.Move(BackupRootDirectory + @"_Temp", BackupRootDirectory);
            }
            closewindow();
       }
        
        /// <summary>
        /// close window callback
        /// </summary>
        public void closewindow()
        {
            if (this.Dispatcher.CheckAccess() == false)
            {
                CloseWindowDelegate CloseWindowCallback = new CloseWindowDelegate(closewindow);
                this.Dispatcher.Invoke(CloseWindowCallback);
            }
            else
            {
                Close();
            }
        }
        /// <summary>
        /// rar directory 
        /// </summary>
        /// <param name="localPassword">string password </param>
        /// <param name="OutputDirectory">string output directory for rar</param>
        /// <param name="InputDirectory">string input directory for package source</param>
        /// <returns></returns>
        public async Task<bool> RarDirectory (string localPassword, string OutputDirectory, string InputDirectory)
        {
            if (string.IsNullOrWhiteSpace(localPassword))
            {
                await fileOps.ExecuteCommandAsync(winrarLocation + " a -mx0 -v1g -t7z -mmt " + OutputDirectory + " " + InputDirectory, true).ConfigureAwait(false);
            }
            else
            {
                await fileOps.ExecuteCommandAsync(winrarLocation + " a -p" + localPassword + " -mx0 -v1g -t7z -mmt " + OutputDirectory + " " + InputDirectory, true).ConfigureAwait(false);
            }
            return true;
        }
        /// <summary>
        /// copy directory to backup location 
        /// </summary>
        /// <param name="patchDirectory">string patch directory path</param>
        /// <param name="BackupRootDirectory">string root directory for backup path</param>
        /// <param name="subdirs">bool for subdir definition (usually set to true)</param>
        /// <param name="useExclusions">Bool to turn on exclusions setup in settings.xml</param>
        /// <param name="exclusionList">List(String) exclusion list generated from settings.xml  </param>
        /// <returns></returns>
        public async Task<bool> DirectoryCopy(string patchDirectory, string BackupRootDirectory, bool subdirs, bool useExclusions, List<string> exclusionList)
        {
            await Task.Run(() =>
            {
                fileOps.DirectoryCopy(patchDirectory, BackupRootDirectory , subdirs, useExclusions, exclusionList);
            }).ConfigureAwait(false);

            return true;
        }
        /// <summary>
        /// filecopy for patch backup
        /// </summary>
        /// <param name="patchDirectory">string source patch directory path </param>
        /// <param name="BackupRootDirectory">string root directory for backup path</param>
        /// <param name="includeList">files to include</param>
        /// <returns></returns>
        public async Task<bool> IncludedFileCopy(string patchDirectory, string BackupRootDirectory, List<string> includeList)
        {
            await Task.Run(() =>
            {
                fileOps.CopyListofFiles(patchDirectory, BackupRootDirectory, includeList);
            }).ConfigureAwait(false);

            return true;
        }
        /// <summary>
        /// update UI Elements to swap to loading bar and notice
        /// </summary>
        public void ChangeUI()
        {
            VersionNumber.Visibility = Visibility.Hidden;
            VersionNumberLabel.Visibility = Visibility.Hidden;
            AcceptNewButtonName.Visibility = Visibility.Hidden;
            RarAndPasswordProtect.Visibility = Visibility.Hidden;
            Password.Visibility = Visibility.Hidden;
            PasswordLabel.Visibility = Visibility.Hidden;
            ArchiveWanted.Visibility = Visibility.Hidden;
            Notice.Visibility = Visibility.Visible;
            LoadingBar.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// password protect 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RarAndPasswordProtect_Checked(object sender, RoutedEventArgs e)
        {
            if (RarAndPasswordProtect.IsChecked == true)
            {
                Password.IsEnabled = true;
            }
            else
            {
                Password.IsEnabled = false;
            }
        }

        private void ArchiveBackup_Checked(object sender, RoutedEventArgs e)
        {
            if (ArchiveWanted.IsChecked == true)
            {
                RarAndPasswordProtect.IsEnabled = true;
                Password.IsEnabled = true;
            }
            else
            {
                RarAndPasswordProtect.IsEnabled = false;
                Password.IsEnabled = false;
            }
        }
    }
}
