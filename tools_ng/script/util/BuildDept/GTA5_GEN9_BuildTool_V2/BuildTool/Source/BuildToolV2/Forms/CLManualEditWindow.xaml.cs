﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace BuildToolV2
{
    /// <summary>
    /// Interaction logic for CLManualEditWindow.xaml
    /// </summary>
    public partial class CLManualEditWindow : RsMainWindow
    {
        private ButtonOperators m_ButtonOperators = new ButtonOperators();
        public static List<TextBox> CurrentTBList = new List<TextBox>();
        public static List<TextBox> PreviousTBList = new List<TextBox>();
        private XMLOperators m_xmlOperator = new XMLOperators();
        public static string xmlFile = "";
        public List<System.Windows.Controls.Label> ListOfLabelsLocal;

        /// <summary>
        /// init for manually editing the cl numbers for b* report usage
        /// </summary>
        /// <param name="xmlSettingsFile">settings.xml changes</param>
        public CLManualEditWindow(string xmlSettingsFile, List<System.Windows.Controls.Label> ListOfLabels)
        {
            InitializeComponent();
            CurrentTBList.Add(Current_Build);
            CurrentTBList.Add(Current_TUBuild);
            CurrentTBList.Add(Current_Rage);
            CurrentTBList.Add(Current_Source);
            CurrentTBList.Add(Current_Script);
            CurrentTBList.Add(Current_Text);
            CurrentTBList.Add(Current_MetaData);
            CurrentTBList.Add(Current_Export);
            CurrentTBList.Add(Current_ParentTxds);
            CurrentTBList.Add(Current_Tools);
            PreviousTBList.Add(Previous_Build);
            PreviousTBList.Add(Previous_TUBuild);
            PreviousTBList.Add(Previous_Rage);
            PreviousTBList.Add(Previous_Source);
            PreviousTBList.Add(Previous_Script);
            PreviousTBList.Add(Previous_Text);
            PreviousTBList.Add(Previous_MetaData);
            PreviousTBList.Add(Previous_Export);
            PreviousTBList.Add(Previous_ParentTxds);
            PreviousTBList.Add(Previous_Tools);
            xmlFile = xmlSettingsFile;
            ListOfLabelsLocal = ListOfLabels;
            m_ButtonOperators.CopyXMLtoTextBox(CurrentTBList, "CurrentCLData", "Current_", xmlSettingsFile);
            m_ButtonOperators.CopyXMLtoTextBox(PreviousTBList, "OldCLData", "Previous_", xmlSettingsFile);
            if (m_xmlOperator.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile) == "rdr3")
            {
                Current_Rage_Label.Content = "Companion: ";
                Current_Rage_Label.SetValue(Canvas.LeftProperty, 35.0);
                Previous_Rage_Label.Content = "Companion: ";
                Previous_Rage_Label.SetValue(Canvas.LeftProperty, 293.0);
            }

        }
        /// <summary>
        /// update current cl with data from form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Update_Current_CL(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.updateXMLParentWithList(CurrentTBList, "CurrentCLData", "Current_", xmlFile);
            m_ButtonOperators.UpdateCurrentCLBranch2(ListOfLabelsLocal, xmlFile);
        }
        /// <summary>
        /// update previous with cl data from form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Update_Previous_CL(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.updateXMLParentWithList(PreviousTBList, "OldCLData", "Previous_", xmlFile);
        }
    }
}

