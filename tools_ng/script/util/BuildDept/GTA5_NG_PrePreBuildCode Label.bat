::****************************************************************
:: GTA5 Label NG Prebuild batch file							**
:: Updated: 									29/07/2014		**
:: Edits:										new				**
:: Last edited by:								Ross McKinstray	**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING GTA5_NG_PrePreBuildCode LABEL
echo.

::Label GTA5 NG
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/src/dev_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/xlast/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //rage/gta5/dev_ng/...


POPD

pause