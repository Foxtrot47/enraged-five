::************************************************************************
:: GTA5 builder - Copy Deployed to Distro - GTA5_TU_Prebuild	        **
:: Updated: 				03/11/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

set VarOne=%1% 
set VarTwo=%2%


if %VarOne%==dev_temp if %VarTwo%==Orbis (

robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_temp\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_temp if %VarTwo%==Durango (

robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_temp\ Deploy*.*


POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\ Deploy*.*

POPD
pause
exit

)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_network if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_network\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_network\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_network\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_network if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_network\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_network\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_network\ Deploy*.*

POPD
pause
exit
)