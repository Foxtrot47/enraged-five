::****************************************************************
:: GTA5 Current Build label script								**
:: Updated: 									29/10/2014		**
:: Edits:		Still need to label x64 in old build/dev_ng		**
:: Last edited by:							Ross McKinstray		**
::****************************************************************


if "%1%"=="TU" (
@echo off
PUSHD %RS_PROJROOT%
@echo on
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/common/...
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/pc/...
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/x64/...
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_beta.bat
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_beta.cmp
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_beta.exe
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_beta.map
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_beta.pdb
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_bankrelease.bat
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_bankrelease.cmp
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_bankrelease.exe
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_bankrelease.map
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_bankrelease.pdb
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_release.bat
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_release.cmp
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_release.exe
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_release.map
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/game_win64_release.pdb
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/*.dll
p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/titleupdate/dev_ng/GTAVLauncher*.exe

P4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/assets_ng/gametext/dev_ng/...
P4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/assets_ng/titleupdate/dev_ng/...
P4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/assets_ng/metadata/...

P4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/src/dev_ng/...
P4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/tools_ng/...
P4 labelsync -l [CB]_GTAV_current_PC //rage/gta5/dev_ng/...
P4 labelsync -l [CB]_GTAV_current_PC //gta5_dlc/mpPacks/...
P4 labelsync -l [CB]_GTAV_current_PC //gta5_dlc/spPacks/...
P4 labelsync -l [CB]_GTAV_current_PC //gta5_dlc/patchPacks/...

p4 labelsync -l [CB]_GTAV_current_PC //depot/gta5/build/dev_ng/x64/...


POPD
pause
)

if "%1%"=="Temp" (
@echo off
PUSHD %RS_PROJROOT%
@echo on
POPD
pause
)