::****************************************************************
:: GTA5 Label NG Prebuild batch file							**
:: Updated: 									29/07/2014		**
:: Edits:										new				**
:: Last edited by:								Ross McKinstray	**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING GTA5_NG_PrePreBuildCode LABEL
echo.

set var1=%1%

echo %var1%

IF "%var1%"=="dev_ng" (
::Label GTA5 NG
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/src/dev_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/xlast/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //rage/gta5/dev_ng/...
)

IF "%var1%"=="dev_temp" (
::Label GTA5 NG
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/src/dev_temp/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/xlast/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //rage/gta5/dev_temp/...
)

IF "%var1%"=="dev_ng_Live" (
::Label GTA5 NG
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/src/dev_ng_Live/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/xlast/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PrePreBuildCode //rage/gta5/dev_ng_Live/...
)


POPD

pause