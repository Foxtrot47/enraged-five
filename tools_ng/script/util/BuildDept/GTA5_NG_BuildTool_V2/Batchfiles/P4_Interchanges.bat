::****************************************************************
:: Sync Latest Data												**
:: Updated: 						07/04/2021					**
:: Edits:														**
:: Last edited by:					Neil Walker					**
::****************************************************************

@echo off

setlocal enabledelayedexpansion

:SETBRANCHSPEC
echo.&&echo --------&&echo.
echo. "1. dev_ng -> dev_ng_live"
echo. "2. dev_ng_live -> dev_ng"
echo.
set /p branchnum=Select branch mapping: 
if "%branchnum%"=="1" set branchspec=dev_ng_to_dev_ng_live
if "%branchnum%"=="2" set branchspec=dev_ng_live_to_dev_ng
if "%branchnum%"=="" echo.&&echo You must enter a valid branch mapping.&&goto :SETBRANCHSPEC


:INTERCHANGES
echo.&&echo --------&&echo.
p4 interchanges -f -b %branchspec%

:INTERCHANGES
echo.&&echo --------&&echo.
goto :SETBRANCHSPEC


pause
