@echo off

setlocal enabledelayedexpansion

PUSHD %RS_PROJROOT%


:SETBRANCHSPEC
echo.&&echo --------&&echo.
echo. "1. dev_ng -> dev_ng_live"
echo. "2. dev_ng_live -> dev_ng"
echo.
set /p branchnum=Select branch mapping: 
if "%branchnum%"=="1" set branchspec=dev_ng_to_dev_ng_live
if "%branchnum%"=="2" set branchspec=dev_ng_live_to_dev_ng
if "%branchnum%"=="" echo.&&echo You must enter a valid branch mapping.&&goto :SETBRANCHSPEC

:SETCHANGELIST
echo.&&echo --------&&echo.
set /p changelist=Enter the changelist number to merge: 
if "%changelist%"=="" echo.&&echo You must enter a changelist number.&&goto :SETCHANGELIST

:CREATECHANGELIST
echo.&&echo --------&&echo.
p4 change -o > temp.txt

type temp.txt | findstr /v depot | findstr /v rage | findstr /v gta5_dlc | findstr /v p4triggers > temp2.txt

for /f "tokens=1,* delims=]" %%a in (temp2.txt) do (
    set foo=%%a
    set foo=!foo:^<enter description here^>=Merging CL %changelist% with %branchspec%!
    echo !foo! >> changelist.txt)

p4 change -i < changelist.txt 

:GETWORKSPACE
p4 client -o > workspace.txt

for /f "tokens=2 skip=35" %%G in (workspace.txt) do (
    set workspace=%%G
	goto SETCHANGELIST
	)

:SETCHANGELIST
p4 changes -c %workspace% -s pending >pendingchangelist.txt

for /f "tokens=2" %%G in (pendingchangelist.txt) do (
    set pendingchangelist=%%G
	goto INTEGRATION
	)

:INTEGRATION
echo.&&echo --------&&echo.
p4 integrate -c %pendingchangelist% -b %branchspec% @%changelist%,@%changelist%

p4 resolve -am -c %pendingchangelist%

p4 shelve -c %pendingchangelist%

del temp.txt
del temp2.txt
del changelist.txt
del pendingchangelist.txt
del workspace.txt

pause
