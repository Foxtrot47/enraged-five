::****************************************************************
:: GRab PrePreBuild Code										**
:: Updated: 									21/08/2014		**
:: Edits:										new				**
:: Last edited by:								Ross McKinstray	**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo Grabbing GTA5_NG_PrePreBuildCode label...
echo.

::Label GTA5 NG
if "%1%"=="dev_Temp" (
P4 sync //depot/gta5/src/dev_Temp/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/xlast/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //rage/gta5/dev_Temp/...@GTA5_NG_PrePreBuildCode
POPD
pause
exit
)
if "%1%"=="dev_ng" (
P4 sync //depot/gta5/src/dev_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/xlast/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //rage/gta5/dev_ng/...@GTA5_NG_PrePreBuildCode
POPD
pause
exit
)
if "%1%"=="dev_ng_Live" (
P4 sync //depot/gta5/src/dev_ng_Live/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/xlast/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //rage/gta5/dev_ng_Live/...@GTA5_NG_PrePreBuildCode
POPD
pause
exit
)
if "%1%"=="dev_ng_network" (
P4 sync //depot/gta5/src/dev_ng_network/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/xlast/...@GTA5_NG_PrePreBuildCode
P4 sync //depot/gta5/tools_ng/...@GTA5_NG_PrePreBuildCode
P4 sync //rage/gta5/dev_ng_network/...@GTA5_NG_PrePreBuildCode
POPD
pause
exit
)