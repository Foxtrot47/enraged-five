::************************************************************************
:: GTA5 builder - Copy Deployed to Distro - GTA5_TU_Prebuild	        **
:: Updated: 				03/11/2013						            **
:: Edits: 					new								            **
:: Last edited by:			Graham Rust  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

set VarOne=%1% 
set VarTwo=%2%
set VarThree=%3%


if %VarOne%==dev_temp if %VarTwo%==Orbis if %VarThree%==ww (

robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_temp\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_temp\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_temp if %VarTwo%==Durango if %VarThree%==ww (

robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_temp\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_temp\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_temp\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_temp\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Orbis if %VarThree%==ww (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng\ Deploy*.*

POPD
pause
exit

)

if %VarOne%==dev_ng if %VarTwo%==Durango if %VarThree%==ww (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Orbis if %VarThree%==ww (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Durango if %VarThree%==ww (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Durango if %VarThree%==jpn (
pause 
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_Live if %VarTwo%==Orbis if %VarThree%==jpn (
pause 
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\ Deploy*.*
POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango if %VarThree%==jpn (
pause 
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Orbis if %VarThree%==jpn (
pause 
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==Japanese_ng if %VarTwo%==Orbis (
pause 
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==Japanese_ng if %VarTwo%==Durango (
pause 
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==Japanese_ng_live if %VarTwo%==Durango (
pause 
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\Japanese_ng_live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT_JPN\dev_ng_live\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==Japanese_ng_live if %VarTwo%==Orbis (
pause 
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\Japanese_ng_live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current_jpn\dev_ng_live\ Deploy*.*
POPD
pause
exit
)

if %VarOne%==dev_ng_network if %VarTwo%==Durango if %VarThree%==ww (
pause 
robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_network\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_network\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_network\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_network\ Deploy*.*

POPD
pause
exit
)

if %VarOne%==dev_ng_network if %VarTwo%==Orbis if %VarThree%==ww (
pause 
robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_network\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_network\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_network\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_network\ Deploy*.*
POPD
pause
exit
)