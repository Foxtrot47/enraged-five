@echo off

::pushd %RS_CODEBRANCH%\game\script_headers - moved this to the selection

if "%1%"=="dev_ng" (
pushd X:\gta5\script\dev_ng\game\script_headers
set scripttitleupdatepath=//depot/gta5/script/dev_ng
set sourcetitleupdatepath=//depot/gta5/src/dev_ng
set localDir = X:/gta5/script/dev_ng
)
if "%1%"=="dev_temp" (
pushd X:\gta5\script\dev_temp\game\script_headers
set scripttitleupdatepath=//depot/gta5/script/dev_temp
set sourcetitleupdatepath=//depot/gta5/src/dev_temp
set localDir = X:/gta5/script/dev_temp

)
if "%1%"=="dev_ng_Live" (
pushd X:\gta5\script\dev_ng_Live\game\script_headers
set scripttitleupdatepath=//depot/gta5/script/dev_ng_Live
set sourcetitleupdatepath=//depot/gta5/src/dev_ng_Live
set localDir = X:/gta5/script/dev_ng_Live

)


:: Integrate the code side script headers over to the script side
p4 integrate -i -d %sourcetitleupdatepath%/game/script_headers/*.sch %scripttitleupdatepath%/core/common/native/*.sch

:: Merge the code changes with any scripter side edits.
p4 resolve -am %scripttitleupdatepath%/core/common/native/*.sch

:: revert any unchanged files & unwanted
p4 revert -a %scripttitleupdatepath%/core/common/native/*.sch

p4 revert %scripttitleupdatepath%/core/common/native/model_enums.sch
p4 revert %scripttitleupdatepath%/core/common/native/stack_sizes.sch
p4 revert %scripttitleupdatepath%/core/common/native/stats_enums.sch
p4 revert %scripttitleupdatepath%/core/common/native/weapon_enums.sch

if exist "%localDir%/core/common/native/model_enums.sch" (
	del /F "%localDir%/core/common/native/model_enums.sch"
)	
if exist "%localDir%/core/common/native/stack_sizes.sch" (
	del /F "%localDir%/core/common/native/stack_sizes.sch"
)
if exist "%localDir%/core/common/native/stats_enums.sch" (
	del /F "%localDir%/core/common/native/stats_enums.sch"
)
if exist "%localDir%/core/common/native/weapon_enums.sch" (
	del /F "%localDir%/core/common/native/weapon_enums.sch"
)

:: These files are in a different place.
p4 integrate -i -d %sourcetitleupdatepath%/game/script_headers/stack_sizes.sch %scripttitleupdatepath%/core/game/data/stack_sizes.sch
p4 integrate -i -d %sourcetitleupdatepath%/game/script_headers/weapon_enums.sch %scripttitleupdatepath%/core/game/data/weapon_enums.sch

:: Merge the code changes with any scripter side edits.
p4 resolve -am %scripttitleupdatepath%/core/game/data/*.sch

:: revert if unchanged
p4 revert -a %scripttitleupdatepath%/core/game/data/*.sch


pause