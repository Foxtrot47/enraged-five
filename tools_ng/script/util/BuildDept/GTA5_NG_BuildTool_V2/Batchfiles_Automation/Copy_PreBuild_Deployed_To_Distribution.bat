::************************************************************************
:: GTA5 builder - Copy Deployed to Distro - GTA5_TU_Prebuild	        **
:: Updated: 				18/01/2022						            **
:: Edits: 					new								            **
:: Last edited by:			Updated for gen8			            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng_live" (
	set Branch=dev_ng_live
)

:: Read version.txt for version number.
FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
	set ver=%%G
	GOTO GAME_VERSION
)

:GAME_VERSION
echo %ver% > X:\gta5\patches\ps4\package\%1%\gta5-orbis\game_version.txt
echo %ver% > X:\gta5\patches\xboxone\package\%1%\gta5-durango\game_version.txt

:COPY
set VarOne=%1% 
set VarTwo=%2%


if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\ *.options
copy /Y X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\pdb\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\ *.options
copy /Y X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\ *.options
copy /Y X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng_Live\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\game\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\tools\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\pdb\  /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\ *.options
copy /Y X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game_version.txt N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng_Live\game_version.txt

POPD
exit
)

