::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				18/01/2022						            **
:: Edits: 					Updated for gen8				            **
:: Last edited by:			Andy Herd	 				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng_live" (
	set Branch=dev_ng_live
)

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO NETWORKCOPY
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg 1.0-%Branch%):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO NETWORKCOPY

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%


echo Copying %VarTwo% %ver% to the network:

if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\installer\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng\v%ver%\installer\ /purge /s

POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\installer\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng\v%ver%\installer\ /purge /s


POPD
pause
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\installer\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\current\dev_ng_Live\v%ver%\installer\ /purge /s

POPD
pause
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\installer\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\CURRENT\dev_ng_Live\v%ver%\installer\ /purge /s

POPD
pause
exit
)
