::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				17/01/2022						            **
:: Edits: 					Updated for Orbis/Durango		            **
:: Last edited by:			Andy Herd					            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng" (
	set Branch=dev_ng_live
)

:: Read version.txt for version number.
FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
	set ver=%%G
	GOTO GAME_VERSION
)

:GAME_VERSION
echo %ver% > X:\gta5\patches\ps4\package\%1%\gta5-orbis\game_version.txt
echo %ver% > X:\gta5\patches\xboxone\package\%1%\gta5-durango\game_version.txt

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%

echo Copying %VarOne%%VarTwo% Prebuild to Resilio:

if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\ *.options
copy /Y X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\orbis\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\pdb\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\pdb\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\ *.options
copy /Y X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\PREBUILD\durango\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\ *.options
copy /Y X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\orbis\game_version.txt

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\ *.ps1
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\ *.options
copy /Y X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game_version.txt N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\PREBUILD\durango\game_version.txt

POPD
exit
)
