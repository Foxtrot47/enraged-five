::****************************************************************
:: GTA5 DEV_NG Checkout prebuild files							**
:: Updated: 						21/06/2022					**
:: Edits:														**
:: Last edited by:					Andy Herd					**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

if "%1%"=="dev_ng" (
::Checkout GTA5 DEV_NG
P4 edit //depot/gta5/titleupdate/dev_ng/game_*_*.cmp
P4 edit //depot/gta5/titleupdate/dev_ng/game_*_*.elf
P4 edit //depot/gta5/titleupdate/dev_ng/game_*_*.exe
P4 edit //depot/gta5/titleupdate/dev_ng/game_*_*.pdb
P4 edit //depot/gta5/titleupdate/dev_ng/game_*_*.map
P4 edit //depot/gta5/titleupdate/dev_ng/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_ng/*/data/lang/american*.rpf
P4 edit //depot/gta5/titleupdate/dev_ng/*/patch/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_ng/*/levels/gta5/script/script*.rpf
P4 edit //depot/gta5/titleupdate/dev_ng/*/dlcPacks/mpChristmas3*/...
P4 edit //depot/gta5/titleupdate/dev_ng/*/dlcPacks/patchDay28*/...
P4 edit //gta5_dlc/mpPacks/mpChristmas3/build/dev_ng/*/data/lang/american*.rpf

:: Remove files that are not required
p4 revert //depot/gta5/titleupdate/dev_ng/game_win64_steambeta.cmp
p4 revert //depot/gta5/titleupdate/dev_ng/game_win64_steambeta.exe
p4 revert //depot/gta5/titleupdate/dev_ng/game_win64_steambeta.pdb
p4 revert //depot/gta5/titleupdate/dev_ng/game_win64_steambeta.map

POPD
exit
)

if "%1%"=="dev_ng_release" (
::Checkout GTA5 DEV_NG Release files
REM P4 edit //depot/gta5/titleupdate/dev_ng/common/data/version.txt
REM P4 edit //depot/gta5/titleupdate/dev_ng/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/patches/*/_packlist/dev_ng_tree.txt

POPD
exit
)

if "%1%"=="dev_ng_live" (
::Checkout GTA5 DEV_NG_LIVE
P4 edit //depot/gta5/titleupdate/dev_ng_Live/game_*_*.cmp
P4 edit //depot/gta5/titleupdate/dev_ng_Live/game_*_*.elf
P4 edit //depot/gta5/titleupdate/dev_ng_Live/game_*_*.exe
P4 edit //depot/gta5/titleupdate/dev_ng_Live/game_*_*.pdb
P4 edit //depot/gta5/titleupdate/dev_ng_Live/game_*_*.map
P4 edit //depot/gta5/titleupdate/dev_ng_Live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_ng_Live/*/data/lang/american*.rpf
P4 edit //depot/gta5/titleupdate/dev_ng_Live/*/patch/data/lang/...
P4 edit //depot/gta5/titleupdate/dev_ng_Live/*/levels/gta5/script/script*.rpf
P4 edit //depot/gta5/titleupdate/dev_ng_Live/*/dlcPacks/mpSum2*/...
P4 edit //depot/gta5/titleupdate/dev_ng_Live/*/dlcPacks/patchDay27*/...
P4 edit //gta5_dlc/mpPacks/mpSum2/build/dev_ng/*/data/lang/american*.rpf

:: Remove files that are not required
p4 revert //depot/gta5/titleupdate/dev_ng_Live/game_win64_steambeta.cmp
p4 revert //depot/gta5/titleupdate/dev_ng_Live/game_win64_steambeta.exe
p4 revert //depot/gta5/titleupdate/dev_ng_Live/game_win64_steambeta.pdb
p4 revert //depot/gta5/titleupdate/dev_ng_Live/game_win64_steambeta.map

POPD
exit
)

if "%1%"=="dev_ng_live_release" (
::Checkout GTA5 DEV_NG Release files
P4 edit //depot/gta5/titleupdate/dev_ng_Live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/dev_ng_Live/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/patches/*/_packlist/dev_ng_live_tree.txt


POPD
exit
)

if "%1%"=="Japanese_ng" (
::Checkout GTA5 JAPANESE_NG
P4 edit //depot/gta5/titleupdate/Japanese_ng/common/data/version.txt
P4 edit //depot/gta5/titleupdate/Japanese_ng/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/titleupdate/Japanese_ng/*/levels/gta5/script/script*.rpf
POPD
exit
)


if "%1%"=="Japanese_ng_live" (
::Checkout GTA5 JAPANESE_NG_LIVE
P4 edit //depot/gta5/titleupdate/Japanese_ng_live/common/data/version.txt
P4 edit //depot/gta5/titleupdate/Japanese_ng_live/common/data/gta5_cache_*.dat
P4 edit //depot/gta5/titleupdate/Japanese_ng_live/*/levels/gta5/script/script*.rpf
POPD
exit
)