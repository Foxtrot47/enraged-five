::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				18/01/2022						            **
:: Edits: 					Updated for gen8				            **
:: Last edited by:			Andy Herd	  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng_live" (
	set Branch=dev_ng_live
)

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO NETWORKCOPY
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg 1.0-%Branch%):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO NETWORKCOPY

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%
set VarThree=%3%

echo Copying %VarTwo% %ver% to the network, press enter to continue:

if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\pdb\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\FakeLauncher\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\dev_ng_live\v%ver%\ *.options

POPD
pause
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\tools\ /purge /s 
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\pdb\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\pdb\ /purge /s 
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\dev_ng_live\v%ver%\ *.options

POPD
pause
exit
)
