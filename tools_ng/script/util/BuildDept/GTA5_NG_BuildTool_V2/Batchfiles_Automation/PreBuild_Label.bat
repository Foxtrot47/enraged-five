::****************************************************************
:: GTA5 Label NG Prebuild batch file							**
:: Updated: 						12/01/2022					**
:: Edits:														**
:: Last edited by:					Andy Herd					**
::****************************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING NG PREBUILD LABEL
echo.
if "%1%"=="dev_ng" (
::Label GTA5 DEV NG
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/build/dev_ng/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/titleupdate/dev_ng/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/src/dev_ng/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/script/dev_ng/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PreBuild //rage/gta5/dev_ng/...

POPD
exit
)

if "%1%"=="dev_ng_live" (
::Label GTA5 DEV NG
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/build/dev_ng_live/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/titleupdate/dev_ng_live/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/src/dev_ng_live/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/script/dev_ng_live/...
P4 labelsync -l GTA5_NG_PreBuild //depot/gta5/tools_ng/...
P4 labelsync -l GTA5_NG_PreBuild //rage/gta5/dev_ng_live/...

POPD
exit
)
