::************************************************************************
:: GTA5 builder - Copy Package to Distro 						        **
:: Updated: 				18/01/2022						            **
:: Edits: 					Updated for gen8    					    **
:: Last edited by:			Andy Herd	  				            	**
::************************************************************************

@Echo off
TITLE %~nx0

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng_live" (
	set Branch=dev_ng_live
)

:AUTOSETVERSION
:: Read version.txt for version number.
if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO NETWORKCOPY
		)
)	ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg 1.0-%Branch%):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO NETWORKCOPY

:NETWORKCOPY
set VarOne=%1% 
set VarTwo=%2%
set VarThree=%3%

echo Copying %VarTwo% %ver% to Resilio:

if %VarOne%==dev_ng if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Orbis_Deploy_v%ver%\orbis\ *.options

POPD
exit
)

if %VarOne%==dev_ng if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\tools\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\pdb\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\pdb\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng\dev_ng_Durango_Deploy_v%ver%\durango\ *.options

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Orbis (

robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\game\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\FakeLauncher\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\FakeLauncher\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\tools\ /purge /s
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\ *.bat
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\ *.ps1
robocopy X:\gta5\patches\ps4\package\dev_ng_Live\gta5-orbis\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Orbis_Deploy_%ver%\orbis\ *.options

POPD
exit
)

if %VarOne%==dev_ng_live if %VarTwo%==Durango (

robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\game\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\game\ /purge /s
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\tools\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\tools\ /purge /s 
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\pdb\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\pdb\ /purge /s 
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\ *.bat
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\ *.xml
robocopy X:\gta5\patches\xboxone\package\dev_ng_Live\gta5-durango\ N:\transfer\RSGEDI\Resilio\gta5\packaged_builds\Dev_ng_Live\dev_ng_Live_Durango_Deploy_v%ver%\durango\ *.options

POPD
exit
)

POPD
exit
)