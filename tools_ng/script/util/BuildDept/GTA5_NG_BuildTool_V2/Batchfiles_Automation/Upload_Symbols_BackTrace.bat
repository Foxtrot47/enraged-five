@echo off
setlocal enabledelayedexpansion

@echo off
echo.

set startdir=%~dp0

if "%1%"=="dev_ng" (
set builddir=X:\gta5\titleupdate\dev_ng
)
if "%1%"=="dev_ng_live" (
set builddir=X:\gta5\titleupdate\dev_ng_live
)
FOR /F "eol=# skip=3 delims=-" %%G IN (%builddir%\common\data\version.txt) DO (
	set version=%%G
	GOTO PRINT
)

:PRINT
echo version=%version%
GOTO :UploadSymbols

:UploadSymbols

set branch=%1%
set platpath=X:\gta5\patches\xboxone\package\%branch%\gta5-durango\game
set project=gta5

if not exist "%platpath%" echo.&&echo *** Error: Package not found [%platpath%]&&pause&&exit 1 /b

set zipname=btsym_%project%_%branch%_%version%_master.zip
set zipname_final=btsym_%project%_%branch%_%version%_final.zip

if exist "%temp%\%zipname%" del /f /q "%temp%\%zipname%"
if exist "%temp%\%zipname_final%" del /f /q "%temp%\%zipname_final%"

echo.
pushd "%platpath%"
zip -D -0 "%temp%\%zipname%" "game_durango_master.pdb" "game_durango_master.exe"
zip -D -0 "%temp%\%zipname_final%" "game_durango_final.pdb" "game_durango_final.exe"
popd
echo.

pushd "%temp%"
echo on
curl -v -k --data-binary "@%zipname%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=6ed875de37828c51830dbabef6ec7bcf5bac3ce7bbf5834a538278fc0450eaca"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
curl -v -k --data-binary "@%zipname_final%" --ssl-no-revoke "https://paradise.sp.backtrace.io:6098/post?format=symbols&tag=%version%&token=6ed875de37828c51830dbabef6ec7bcf5bac3ce7bbf5834a538278fc0450eaca"
@if errorlevel 1 @echo off&&echo.&&echo *** Error: Failed to upload symbol data&&pause&&exit 1 /b
@echo off
popd

del /f /q "%temp%\%zipname%"
del /f /q "%temp%\%zipname_final%"
popd

echo.&&echo Symbol upload completed.
