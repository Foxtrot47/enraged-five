echo off
echo.

set branch=%1

if [%branch%]==[] set branch=dev_ng

pushd %~dp0..\..\..\..\..\..\tools_ng\bin\gen9\RPFChecker\

RpfChecker -dump_tree X:\gta5\patches\ps4\_packlist\%branch%_tree.txt X:\gta5\patches\ps4\package\%branch%\gta5-orbis\game
RpfChecker -dump_tree X:\gta5\patches\xboxone\_packlist\%branch%_tree.txt X:\gta5\patches\xboxone\package\%branch%\gta5-durango\game

popd 

exit 0