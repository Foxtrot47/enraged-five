::****************************************************************
:: GTA5 Create and Populate Version label 						**
:: Updated: 						12/01/2021					**
:: Edits:														**
:: Last edited by:					Andy Herd			 		**
::****************************************************************

setlocal enabledelayedexpansion
Set-ExecutionPolicy RemoteSigned
Set-ExecutionPolicy RemoteSigned CurrentUser

@echo off
echo.

cd /d %~dp0


if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

TITLE GTA5 label - Create and populate GTA5_DEV_NG version label
ECHO GTA5 label - Create and populate GTA5_DEV_NG version label

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

:: read version.txt in build for label.
set ver=NUL

if exist X:\gta5\titleupdate\%Branch%\common\data\version.txt (
	:: Read the version number from the version.txt
	:: Parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3 delims=-" %%G IN (X:\gta5\titleupdate\%Branch%\common\data\version.txt) DO (
		set ver=%%G
		GOTO SETLABEL
	)
) ELSE (
		echo.No version number found in version.txt. Please enter version number manually.
		GOTO MANUALLYSETVERSION
)
	
:MANUALLYSETVERSION
echo.&&echo Enter the version number of the package (eg "1.0"):
set /p ver=:
if "%ver%"=="" echo.&&echo You must enter a version number.&&goto :MANUALLYSETVERSION
GOTO SETLABEL
	
:SETLABEL
if "%Branch%"=="dev_ng" (
	set BranchUpperCase=DEV_NG
)

echo.
echo Create and set new version label.
echo.
set verLabel=GTA5_%BranchUpperCase%_Version_%ver%
echo verLabel = %verLabel%
echo ver = %ver%
echo You are about to create a new label called %verLabel% is this correct? 
echo Close this window if not, or press a key to confirm...
p4 label -o -t gta5ngtemplate %verLabel% | p4 label -i
echo.
set version=NUL
::Removing the old version label (20 reveisions prior.)
set "replace=%USERNAME%"
set /a versionNumber=%ver%
set /a "OldVer=versionNumber-20.0"
set oldVerLabel=GTA5_%BranchUpperCase%_Version_%oldVer%.0
p4 labels|findstr /r "%oldVerLabel%"
IF ERRORLEVEL 1 GOTO MAIN
	echo Unloading %oldVerLabel% - to reload call "p4 reload -l %oldVerLabel%"
	p4 label -o %oldVerLabel%>X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt
	goto SETLABELOWNERANDUNLOAD
	

:SETLABELOWNERANDUNLOAD
	set "search2=buildernorth"
	set "search3=gemma.mccord"
	set "search4=kristopher.williams"
	set "search5=neil.walker"
	set "search6=ross.mckinstray"
	set "search7=michael.moffat"
	set "search7="svcrsggbauto"
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search2%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search3%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search4%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search5%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search6%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	for /f "delims=" %%i in ('type "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ^& break ^> "X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" ') do (
		set "line=%%i"
		setlocal enabledelayedexpansion
		set "line=!line:%search6%=%replace%!"
		>>"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt" echo(!line!
		endlocal
	)
	p4 label -i <"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\GTA5_NG_TU_BuildTools\VersionOutput.txt"
	P4 unload -l %oldVerLabel%
	GOTO MAIN


:MAIN
echo.
echo UPDATING LABELLED BUILD
echo.

if "%1%"=="dev_ng" (
::Label GTA5 DEV_NG
P4 labelsync -l %verlabel% //depot/gta5/build/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/titleupdate/dev_ng/...
P4 labelsync -l %verlabel% //rage/gta5/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/src/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/script/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/titleupdate/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/GameText/dev_ng/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/maps/ParentTxds.xml
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/metadata/...
P4 labelsync -l %verlabel% //gta5_dlc/mpPacks/mpSum2/build/dev_ng/...
P4 labelsync -l %verlabel% //gta5_dlc/patchPacks/patchDay27NG/build/dev_ng/...

POPD
pause
exit
)

if "%1%"=="dev_ng_live" (
::Label GTA5 DEV_NG LIVE
P4 labelsync -l %verlabel% //depot/gta5/build/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/titleupdate/dev_ng_live/...
P4 labelsync -l %verlabel% //rage/gta5/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/src/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/script/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/titleupdate/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/GameText/dev_ng_live/...
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/maps/ParentTxds.xml
P4 labelsync -l %verlabel% //depot/gta5/assets_ng/metadata/...
P4 labelsync -l %verlabel% //gta5_dlc/mpPacks/mpSum2/build/dev_ng/...
P4 labelsync -l %verlabel% //gta5_dlc/patchPacks/patchDay27NG/build/dev_ng/...

POPD
pause
exit
)
