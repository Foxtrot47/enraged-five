echo off
echo.

if "%1%"=="dev_ng" (
	set Branch=dev_ng
)

if "%1%"=="dev_ng_live" (
	set Branch="dev_ng=dev_ng_live"
)

X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3 -platforms win64 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3 -platforms ps4 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3 -platforms xboxone -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3_G9EC -platforms win64 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3_G9EC -platforms ps4 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks mpChristmas3_G9EC -platforms xboxone -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28NG -platforms win64 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28NG -platforms ps4 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28NG -platforms xboxone -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28G9ECNG -platforms win64 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28G9ECNG -platforms ps4 -branches %Branch%
X:\gta5\tools_ng\bin\gen9\DLCCompiler\DLCCompilerCLI.exe -tupacks patchday28G9ECNG -platforms xboxone -branches %Branch%

exit