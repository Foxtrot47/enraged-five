echo
 
set source=Source
set rage=Rage
set script=Script
set text=Text
set data=Data
set metadata=Metadata
set export=Export
set audio=Audio
set dlc=mpSum2

if exist X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt (
	del "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
	:: Strip the CL numbers from the sync.txt and put into a copy and paste format for prebuild mail. 
:SourceCL 
	FOR /F "tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %source%: %%G > "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto RageCL
		)
:RageCL
	FOR /F "skip=1 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %rage%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto ScriptCL
		)
:ScriptCL
	FOR /F "skip=3 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %script%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto TextCL
		)
:TextCL
	FOR /F "skip=4 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %text%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto DataCL
		)
:DataCL
	FOR /F "skip=5 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %data%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto MetadataCL
		)
:MetadataCL
	FOR /F "skip=6 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %metadata%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto ExportCL
		)
:ExportCL
	FOR /F "skip=7 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %export%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto AudioCL
		)
:AudioCL
	FOR /F "skip=8 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTool_V2\BuildTool\dev_ng_live_sync.txt) DO (
		echo %audio%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto DlcCL
		)
:DlcCL
	FOR /F "skip=9 tokens=2" %%G IN (X:\gta5\tools_ng\script\util\BuildDept\GTA5_GEN9_BuildTool_V2\BuildTool\dev_gen9_sga_sync.txt) DO (
		echo %dlc%: %%G >> "N:\RSGEDI\Build Department\gtav_ng\CL_numbers_Live.txt"
		goto end
		)
) ELSE (
		echo.No sync text file has been found.
)

:end
