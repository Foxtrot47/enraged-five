﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;
using System.Threading;
using RSG.Configuration;
using System.Text.RegularExpressions;


namespace BuildToolV2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : RsMainWindow
    {
        private InitializeDefaults m_InitializeDefaults = new InitializeDefaults();
        private ButtonOperators m_ButtonOperators = new ButtonOperators();
        private FileOperators m_FileOperators = new FileOperators();
        private Bugstar m_Bugstar = new Bugstar();
        XMLOperators m_xmlOperators = new XMLOperators();
        public List<EnvironmentVariable> environmentVariables = new List<EnvironmentVariable>();
        public List<System.Windows.Controls.Label> lblList = new List<System.Windows.Controls.Label>();
        public List<System.Windows.Controls.Button> syncButtonList = new List<System.Windows.Controls.Button>();
        public List<System.Windows.Controls.TextBox> bugstarReportTextBoxList = new List<System.Windows.Controls.TextBox>();
        public System.Windows.Controls.PasswordBox passwordBox = new System.Windows.Controls.PasswordBox();
        public List<System.Windows.Controls.Button> bugstarReportButtonList = new List<System.Windows.Controls.Button>();
        public List<System.Windows.Controls.Button> prebuildButtonList = new List<System.Windows.Controls.Button>();
        public List<System.Windows.Controls.Button> releaseButtonList = new List<System.Windows.Controls.Button>();
        public List<System.Windows.Controls.ListBox> checkoutListBoxList = new List<System.Windows.Controls.ListBox>();
        public List<System.Windows.Forms.PictureBox> pictureBoxList= new List<System.Windows.Forms.PictureBox>();
        public List<ListBox> ListOfListBoxes = new List<ListBox>();
        public TextBox bugstarAdditionalCLs = new TextBox();
        public static string xmlFile = "";
        public Dispatcher UIDispatcher;
        public bool alreaydonCorrectTab = false;
        public List<string> OriginalCompilerListListboxItemList = new List<string>();
        public List<string> OriginalShaderListListboxItemList = new List<string>();
        public List<CheckOutItem> CheckoutItemList = new List<CheckOutItem>();
        public List<string> CodeCheckoutList = new List<string>();
        public List<string> OtherCheckoutList = new List<string>();
        public List<string> ShadersCheckoutList = new List<string>();
        public string currentTab = "";
        public bool doFocusEvent = false;



        public MainWindow()
        {
            UIDispatcher = Dispatcher.CurrentDispatcher;
            InitializeComponent();
            Globals.isJapanese = false;
            EventManager.RegisterClassHandler(typeof(Window), Window.PreviewMouseDownEvent, new MouseButtonEventHandler(OnPreviewMouseDown));
            // add a list of buttons to reference (used for functions with name specific feature)
            if (m_InitializeDefaults.Initialize_Default_Settings())
            {
                environmentVariables = m_InitializeDefaults.environmentVariables;
                lblList.Add(ToolsCL);
                lblList.Add(RageCL);
                lblList.Add(SourceCL);
                lblList.Add(ScriptCL);
                lblList.Add(TextCL);
                lblList.Add(DataCL);
                lblList.Add(MetaDataCL);
                lblList.Add(ExportCL);
                syncButtonList.Add(Sync_Rage);
                syncButtonList.Add(Sync_Source);
                syncButtonList.Add(Sync_Script);
                syncButtonList.Add(Sync_Text);
                syncButtonList.Add(Sync_Data);
                syncButtonList.Add(Sync_MetaData);
                syncButtonList.Add(Sync_Export);
                syncButtonList.Add(Sync_Tools);
                ListOfListBoxes.Add(Compiler_Source);
                ListOfListBoxes.Add(Compiler_Target);
                ListOfListBoxes.Add(Compiler_Shader);
                checkoutListBoxList.Add(Chechout_Executables);
                checkoutListBoxList.Add(Chechout_Shaders);
                checkoutListBoxList.Add(Chechout_Others);
                bugstarAdditionalCLs = Bugstar_Additional_CLs;
                bugstarReportButtonList.Add(Bugstar_Report);
                bugstarReportButtonList.Add(Bugstar_Login);
                passwordBox = Bugstar_Password;
                bugstarReportTextBoxList.Add(Bugstar_Username);
                m_ButtonOperators.listOfLabels = lblList;
                m_InitializeDefaults.SetLabelListVariable(lblList);
                m_InitializeDefaults.SetP4SyncLabels(lblList);
                m_InitializeDefaults.setAdditionalCLInitialReference(Bugstar_Additional_CLs);
                prebuildButtonList.Add(Prebuild_Button1);
                prebuildButtonList.Add(Prebuild_Button2);
                prebuildButtonList.Add(Prebuild_Button3);
                prebuildButtonList.Add(Prebuild_Button4);
                prebuildButtonList.Add(Prebuild_Button5);
                prebuildButtonList.Add(Prebuild_Button6);
                prebuildButtonList.Add(Prebuild_Button7);
                prebuildButtonList.Add(Prebuild_Button8);
                prebuildButtonList.Add(Prebuild_Button9);
                prebuildButtonList.Add(Prebuild_Button10);
                prebuildButtonList.Add(Prebuild_Button11);
                prebuildButtonList.Add(Prebuild_Button12);
                prebuildButtonList.Add(Prebuild_Button13);
                prebuildButtonList.Add(Prebuild_Button14);
                prebuildButtonList.Add(Prebuild_Button15);
                prebuildButtonList.Add(Prebuild_Button16);
                prebuildButtonList.Add(Prebuild_Button17);
                prebuildButtonList.Add(Prebuild_Button18);
                prebuildButtonList.Add(Prebuild_Button19);
                prebuildButtonList.Add(Prebuild_Button20);
                releaseButtonList.Add(Release_Button1);
                releaseButtonList.Add(Release_Button2);
                releaseButtonList.Add(Release_Button3);
                releaseButtonList.Add(Release_Button4);
                releaseButtonList.Add(Release_Button5);
                releaseButtonList.Add(Release_Button6);
                releaseButtonList.Add(Release_Button7);
                releaseButtonList.Add(Release_Button8);
                releaseButtonList.Add(Release_Button9);
                releaseButtonList.Add(Release_Button10);
                releaseButtonList.Add(Release_Button11);
                releaseButtonList.Add(Release_Button12);
                releaseButtonList.Add(Release_Button13);
                releaseButtonList.Add(Release_Button14);
                releaseButtonList.Add(Release_Button15);
                releaseButtonList.Add(Release_Button16);
                releaseButtonList.Add(Release_Button17);
                releaseButtonList.Add(Release_Button18);
                releaseButtonList.Add(Release_Button19);
                releaseButtonList.Add(Release_Button20);
                m_Bugstar.SetUIElements(bugstarReportButtonList, passwordBox, bugstarReportTextBoxList, bugstarAdditionalCLs);
                pictureBoxList.Add(SyncingRage);
                pictureBoxList.Add(SyncingSource);
                pictureBoxList.Add(SyncingScript);
                pictureBoxList.Add(SyncingText);
                pictureBoxList.Add(SyncingData);
                pictureBoxList.Add(SyncingMetaData);
                pictureBoxList.Add(SyncingExport);
                pictureBoxList.Add(SyncingTools);
            }
            xmlFile = m_InitializeDefaults.XMLSettingsFile;
            m_ButtonOperators.setxmlRef(xmlFile);

            SetUpCheckoutList();

            SetButtonVisibilityFromXML(prebuildButtonList);
            SetButtonVisibilityFromXML(releaseButtonList);
            if (m_xmlOperators.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile) == "rdr3")
            {
                Sync_Rage.Content = "Companion";
                RageLable.Content = "Companion";
                syncButtonList.Add(Sync_Other);
                HideJapaneseCheckBox();
            }
            else
            {
                Sync_Other.Visibility = Visibility.Hidden;
            }


            foreach (System.Windows.Forms.PictureBox pb in pictureBoxList)
            {
                pb.ImageLocation = System.IO.Directory.GetCurrentDirectory() + @"\Forms\Loading.gif";
                pb.ImageLocation = null; 
            }
        }

        public void SetUpCheckoutList()
        {
            CheckoutItemList = m_ButtonOperators.GetListOfFileToCheckOut();
            foreach (CheckOutItem checkoutitem in CheckoutItemList)
            {
                checkRefAndAddToList(CodeCheckoutList, checkoutitem, "Code");
                checkRefAndAddToList(ShadersCheckoutList, checkoutitem, "Shaders");
                checkRefAndAddToList(OtherCheckoutList, checkoutitem, "Script");
                checkRefAndAddToList(OtherCheckoutList, checkoutitem, "Version");
                checkRefAndAddToList(OtherCheckoutList, checkoutitem, "Other");
            }
            CodeCheckoutList.Sort();
            ShadersCheckoutList.Sort();
            OtherCheckoutList.Sort();
            Chechout_Executables.ItemsSource = CodeCheckoutList;
            Chechout_Shaders.ItemsSource = ShadersCheckoutList;
            Chechout_Others.ItemsSource = OtherCheckoutList;
        }

        static void OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender.GetType().ToString() == "BuildToolV2.MainWindow"){
                WindowCollection windowList = System.Windows.Application.Current.Windows;
            
                foreach (Window w in windowList)
                {
                    if (w!=null)
                    {
                        if (w.IsVisible)
                        {
                            if (w != Application.Current.MainWindow)
                            {
                                w.Focus();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// set visibility from list (init)
        /// </summary>
        /// <param name="ButtonList">List of buttons from main menu init</param>
        public void SetButtonVisiblityfromBool(List<Button> ButtonList)
        {
            foreach (Button btn in ButtonList)
            {
                if (btn.IsVisible == false)
                {
                    btn.Visibility = Visibility.Visible;
                }
            }
        }
        /// <summary>
        /// set visibility from the xml reference in the settings.xml
        /// </summary>
        /// <param name="ButtonList">List of buttons from main menu init</param>
        public void SetButtonVisibilityFromXML(List<Button> ButtonList)
        {
            foreach (Button btn in ButtonList)
            {
                string data = m_xmlOperators.GetElementFromXMLFile(btn.Name, "Buttons", xmlFile);
                if (data == "")
                {
                    btn.Visibility = Visibility.Hidden;
                }
                else
                {
                    btn.Visibility = Visibility.Visible;
                }
                SetButtonNameFromXml(btn);
            }
        }
        /// <summary>
        /// checks reference from settings.xml and adds checkout item (used in p4 sync)
        /// </summary>
        /// <param name="stringListRef">List(string) xmlreference to the list of items to checkout</param>
        /// <param name="CheckoutitemRef">checkout item object for comparing checkout items</param>
        /// <param name="stringref">string reference to item to checkout</param>
        public void checkRefAndAddToList(List<string> stringListRef, CheckOutItem CheckoutitemRef, string stringref)
        {
            if (CheckoutitemRef.xmlReference == stringref)
            {
                if (stringListRef.Contains(CheckoutitemRef.friendlyReference) == false)
                {
                    stringListRef.Add(CheckoutitemRef.friendlyReference);
                }
            }
        }

        // Button Functions
        /// <summary>
        /// Syncs reference from button name, compares name vs "friendly name" and then syncs branch via SyncItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Sync(object sender, RoutedEventArgs e)
        {
            if (ChangeXML.IsChecked == true)
            {
                m_ButtonOperators.SetXMLReference(((Button)sender).Name, ((Button)sender).Name, m_FileOperators.OpenFileBrowserDialogueAndRetrieveFileStringName(""), m_InitializeDefaults.XMLSettingsFile);
            }
            else
            {
                if (xmlFile == "")
                {
                    xmlFile = m_InitializeDefaults.XMLSettingsFile;
                    m_ButtonOperators.SyncBranch((Button)sender, xmlFile, m_ButtonOperators.listOfLabels, Force_Grab.IsChecked.Value, UIDispatcher,pictureBoxList);
                }
                else
                {
                    m_ButtonOperators.SyncBranch((Button)sender, xmlFile, m_ButtonOperators.listOfLabels, Force_Grab.IsChecked.Value, UIDispatcher, pictureBoxList);
                    
                }
            }
        }
        /// <summary>
        /// syncs all in the list from button reference list (defined above in the init)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Sync_All(object sender, RoutedEventArgs e)
        {
            if (ChangeXML.IsChecked == true)
            {
                m_ButtonOperators.SetXMLReference(((Button)sender).Name, ((Button)sender).Name, m_FileOperators.OpenFileBrowserDialogueAndRetrieveFileStringName(""), m_InitializeDefaults.XMLSettingsFile);
            }
            else
            {
                m_ButtonOperators.SyncAllBranchesInList(syncButtonList, m_InitializeDefaults.XMLSettingsFile, m_ButtonOperators.listOfLabels, Force_Grab.IsChecked.Value, UIDispatcher, pictureBoxList);
            }
        }
        /// <summary>
        /// instantiates the edit cl number window for use in manually changing cl numbers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Bugstar_Edit_CL_Numbers(object sender, RoutedEventArgs e)
        {
            Window Edit_CL_Numbers = new CLManualEditWindow(xmlFile, lblList);
            Edit_CL_Numbers.Show();
        }
        /// <summary>
        /// logs into b* using cookie authentication generated via trational password parsing also triggers the B* keep alive so that the cookie doesnt get stale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_BugstarLogin(object sender, RoutedEventArgs e)
        {
            string username = Bugstar_Username.Text;
            string password = Bugstar_Password.Password;
            if (m_ButtonOperators.LoginToBugstar(username, password, m_InitializeDefaults.XMLSettingsFile) == true)
            {
                Bugstar_Username.IsEnabled = false;
                Bugstar_Password.IsEnabled = false;
                Bugstar_Login.IsEnabled = false;
                Bugstar_Report.IsEnabled = true;
                InitialiseBugstarKeepAlive();

            }
            else
            {
                Bugstar_Username.IsEnabled = true;
                Bugstar_Password.IsEnabled = true;
                Bugstar_Login.IsEnabled = true;
                Bugstar_Report.IsEnabled = false;
            }

        }
        /// <summary>
        /// updates the generated button name from xml file 
        /// </summary>
        /// <param name="button"></param>
        public void SetButtonNameFromXml(Button button)
        {
            string data = m_xmlOperators.GetElementFromXMLFile(button.Name, "Buttons", xmlFile);
            if (data == "")
            {
                if (button.Name.Contains("Prebuild_"))
                {
                    button.Content = button.Name.Remove(0, 9);
                }
                if (button.Name.Contains("Release_"))
                {
                    button.Content = button.Name.Remove(0, 8);
                }
            }
            else
            {
//                int splitcount = 1;
                button.Content = data.Split(';').First();
            }
        }
        /// <summary>
        /// runs batchfiles from button name reference within the settings.xml file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_RunBatch(object sender, RoutedEventArgs e)
        {

/*            m_ButtonOperators.integrateScriptHeader();*/
            if (ChangeXML.IsChecked == true)
            {
                m_ButtonOperators.SetNameAndFile(((Button)sender).Name);
                doFocusEvent = true;
                //m_ButtonOperators.SetXMLReference(((Button)sender).Name,"Buttons", m_FileOperators.OpenFileBrowserDialogueAndRetrieveFileStringName(), m_InitializeDefaults.XMLSettingsFile);
            }
            else
            {
                string batfilePath = m_ButtonOperators.retrieveBatFileFromXmlReference(((Button)sender).Name, "Buttons", m_InitializeDefaults.XMLSettingsFile);
                string args = m_ButtonOperators.retrieveArgsForBatfile(((Button)sender).Name, "Buttons", m_InitializeDefaults.XMLSettingsFile);
                if (args.Contains("Branch"))
                {
                   args = args.Replace("Branch", m_xmlOperators.GetElementFromXMLFile("BuildRoot", "Environment", m_InitializeDefaults.XMLSettingsFile).Split('\\').Last());
                }
                if (batfilePath != "")
                {
                    if (System.IO.File.Exists(batfilePath))
                    {
                        m_FileOperators.RunBatfile(batfilePath, args.Split(' ').ToList());
                    }
                }
            }
        }
        /// <summary>
        /// runs batchfiles from button name (patch specific)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_RunBatchForPatches(object sender, RoutedEventArgs e)
        {
            if (System.IO.File.Exists(m_ButtonOperators.GetPatchBatfileFromReference(sender as Button, xmlFile)))
            {
                m_FileOperators.RunBatfile(m_ButtonOperators.GetPatchBatfileFromReference(sender as Button, xmlFile), null);
            }
        }
        /// <summary>
        /// opens patch directories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_OpenPatchDirectory(object sender, RoutedEventArgs e)
        {
            if (System.IO.Directory.Exists(m_ButtonOperators.getPatchDirectoryFromButton(sender as Button, xmlFile)))
            {
                System.Diagnostics.Process.Start(m_ButtonOperators.getPatchDirectoryFromButton(sender as Button, xmlFile), null);
            }
        }
        /// <summary>
        /// starts the patch backup process (reference passed through by sending button)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_BackupPatchDirectory(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.CreateBackupPatchDirectory(sender as Button);
        }

        
        /// <summary>
        /// updates all buttons in prebuild and release's visibility on refocus (means the buttons will always have hte most up to date visibility settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FocusChanged(object sender, System.EventArgs e)
        {
            if (ChangeXML.IsChecked == true && doFocusEvent == true)
            {
                SetButtonVisibilityFromXML(prebuildButtonList);
                SetButtonVisiblityfromBool(prebuildButtonList);
                SetButtonVisibilityFromXML(releaseButtonList);
                SetButtonVisiblityfromBool(releaseButtonList);
                doFocusEvent = false;
            }
        }
        /// <summary>
        /// sets label content
        /// </summary>
        /// <param name="label"></param>
        /// <param name="content"></param>
        public void SetLabelContent(System.Windows.Controls.Label label, string content)
        {
            label.Content = content;
        }
        /// <summary>
        /// saved the old cl information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Button_Click_SaveOld(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.SaveOld(m_InitializeDefaults.XMLSettingsFile);
        }
        /// <summary>
        /// updates content on tab control change (allows updated content if tool is still syncing directories)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem ti = TabControl.SelectedItem as TabItem;
            if (ti != null)
            {
                if (e.OriginalSource == this.TabControl)
                {
                    if (ti.Name == "CompilerTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        HideEditXml();
                        if (OriginalCompilerListListboxItemList.Count() == 0)
                        {
                            m_ButtonOperators.ListOfListBoxes = ListOfListBoxes;
                            m_ButtonOperators.PopulateCompilerOptions(m_InitializeDefaults.XMLSettingsFile);
                            foreach (string str in Compiler_Source.Items)
                            {
                                OriginalCompilerListListboxItemList.Add(str);
                            }
                        }
                        if (OriginalShaderListListboxItemList.Count() == 0)
                        {
                            foreach (string str in Compiler_Shader.Items)
                            {
                                OriginalShaderListListboxItemList.Add(str);
                            }
                        }
                    }
                    else if (ti.Name == "CheckoutTab")
                    {
                        currentTab = ti.Name;
                        ShowJapaneseCheckBox();
                        HideEditXml();
                    }
                    else if (ti.Name == "ReleaseTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        ShowEditXml();
                    }
                    else if (ti.Name == "PreBuildTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        ShowEditXml();
                    }
                    else if (ti.Name == "BugstarTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        HideEditXml();
                    }
                    else if (ti.Name == "PatchesTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        HideEditXml();
                    }
                    else if (ti.Name == "SyncTab")
                    {
                        currentTab = ti.Name;
                        HideJapaneseCheckBox();
                        HideEditXml();
                    }
                    else
                    {
                        HideJapaneseCheckBox();
                        ShowEditXml();
                    }
                }
            }
        }
        /// <summary>
        /// inits the process for keeping the B* cookie alive 
        /// </summary>
        private void InitialiseBugstarKeepAlive()
        {
            m_ButtonOperators.AsyncKeepBugstarCookieAlive(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(30), CancellationToken.None, m_InitializeDefaults.XMLSettingsFile);
        }
        /// <summary>
        /// Generates a prebuild email report from CL numbers and data mining.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_BugstarReport(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.GeneratePrebuildReport(m_InitializeDefaults.BugstarProjectID, m_InitializeDefaults.XMLSettingsFile);
        }
        /// <summary>
        /// restores backed up cl in formation in case of accidental old backup overwritten with new cl data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Bugstar_Restore_CL_Numbers(object sender, RoutedEventArgs e)
        {
            if (RsMessageBox.Show(Application.Current.MainWindow, "Are you sure you want to restore the old values?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                m_ButtonOperators.RestoreBackupCLData("BackupCLData", "OldCLData", xmlFile);
            }
        }
        /// <summary>
        /// on enter for b* login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyDown_Enter_Bugstar_Password(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;
                Button_Click_BugstarLogin(sender, e);
            }
        }
        /// <summary>
        /// Build selected shaders/exes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click_Compiler_Build(object sender, RoutedEventArgs e)
        {
            List<string> ExeItemList = new List<string>();
            List<string> ShaderItemList = new List<string>();
            m_ButtonOperators.BackupThenClearBuildLogs(xmlFile);
            foreach (var str in Compiler_Target.Items)
            {
                if (str.ToString().Contains("Shader") == true)
                {
                    ShaderItemList.Add(str.ToString());
                }
                else
                {
                    ExeItemList.Add(str.ToString());
                }
            }
            List<CompilerObject> ExeObjectList = new List<CompilerObject>();
            List<ShaderObject> ShaderObjectList = new List<ShaderObject>();
            ExeObjectList = m_ButtonOperators.GetBuildItems(ExeItemList);
            ShaderObjectList = m_ButtonOperators.GetShaderItems(ShaderItemList);
            await m_ButtonOperators.CompileListOfShadersAndExes("build", ShaderObjectList, ExeObjectList).ConfigureAwait(false);
            
//             m_ButtonOperators.CompileListOfShaderObjects("build", ShaderObjectList);
//             m_ButtonOperators.CompileListOfCompilerObjects("/Build", ExeObjectList);
        }
        /// <summary>
        /// Rebuild selected shaders/exes (rebuild then build is determined by setup.xml as some projects do not need a build directly after)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Compiler_RebuildThenBuildBuild_Click(object sender, RoutedEventArgs e)
        {
            List<string> ExeItemList = new List<string>();
            List<string> ShaderItemList = new List<string>();
            m_ButtonOperators.BackupThenClearBuildLogs(xmlFile);
            foreach (var str in Compiler_Target.Items)
            {
                if (str.ToString().Contains("Shader") == true)
                {
                    ShaderItemList.Add(str.ToString());
                }
                else
                {
                    ExeItemList.Add(str.ToString());
                }
            }

            List<CompilerObject> ExeObjectList = new List<CompilerObject>();
            List<ShaderObject> ShaderObjectList = new List<ShaderObject>();
            ExeObjectList = m_ButtonOperators.GetBuildItems(ExeItemList);
            ShaderObjectList = m_ButtonOperators.GetShaderItems(ShaderItemList);
            // 
            //             m_ButtonOperators.CompileListOfShaderObjects("rebuild", ShaderObjectList);
            //             m_ButtonOperators.CompileListOfCompilerObjects("/Rebuild", ExeObjectList);
            await m_ButtonOperators.CompileListOfShadersAndExes("rebuild", ShaderObjectList, ExeObjectList).ConfigureAwait(false);
        }
        /// <summary>
        /// add individual compiler solution to the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_Source_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            if (lb.SelectedItem != null)
            {
                Compiler_Target.Items.Add(lb.SelectedItem);
                lb.Items.Remove(lb.SelectedItem);
            }
            Compiler_Source.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }
        /// <summary>
        /// removes individual compiler solution from the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_Target_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            if (lb.SelectedItem != null)
            {
                if (OriginalCompilerListListboxItemList.Contains(lb.SelectedItem.ToString()))
                {
                    Compiler_Source.Items.Add(lb.SelectedItem);
                    lb.Items.Remove(lb.SelectedItem);
                }
                else
                {
                    if (OriginalShaderListListboxItemList.Contains(lb.SelectedItem.ToString()))
                    {
                        Compiler_Shader.Items.Add(lb.SelectedItem);
                        lb.Items.Remove(lb.SelectedItem);
                    }
                }

            }
            Compiler_Source.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Shader.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }
        /// <summary>
        /// adds multiple compiler solutions to the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_AddMultipleExes_Click(object sender, RoutedEventArgs e)
        {
            ListBox lb = Compiler_Source;
            List<string> ListItem = new List<string>();
            foreach (var item in lb.Items)
            {
                if (lb.SelectedItems.Contains(item))
                {
                    ListItem.Add((string)item);
                    Compiler_Target.Items.Add(item);
                }
            }
            foreach (string listItem in ListItem)
            {
                lb.Items.Remove(listItem);
            }
            Compiler_Source.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }
        /// <summary>
        /// removes multiple compiler solution from the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_RemoveMultiple_Click(object sender, RoutedEventArgs e)
        {
            ListBox lb = Compiler_Target;
            List<string> ListItem = new List<string>();
            foreach (var item in lb.Items)
            {
                if (lb.SelectedItems.Contains(item))
                {
                    ListItem.Add((string)item);
                    if (OriginalCompilerListListboxItemList.Contains((string)item))
                    {
                        Compiler_Source.Items.Add(item);
                    }
                    else
                    {
                        if (OriginalShaderListListboxItemList.Contains((string)item))
                        {
                            Compiler_Shader.Items.Add(item);
                        }
                    }

                }
            }
            foreach (string listItem in ListItem)
            {
                lb.Items.Remove(listItem);
            }
            Compiler_Source.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Shader.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }
        /// <summary>
        /// adds individual shader solution on double click to the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_Shader_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            if (lb.SelectedItem != null)
            {
                Compiler_Target.Items.Add(lb.SelectedItem);
                lb.Items.Remove(lb.SelectedItem);
            }
            Compiler_Shader.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }

        /// <summary>
        /// adds multiple shader solutions to the "build" table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_AddMultipleShaders_Click(object sender, RoutedEventArgs e)
        {
            ListBox lb = Compiler_Shader;
            List<string> ListItem = new List<string>();
            foreach (var item in lb.Items)
            {
                if (lb.SelectedItems.Contains(item))
                {
                    ListItem.Add((string)item);
                    Compiler_Target.Items.Add(item);
                }
            }
            foreach (string listItem in ListItem)
            {
                lb.Items.Remove(listItem);
            }
            Compiler_Shader.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
            Compiler_Target.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Ascending));
        }

        /// <summary>
        /// cancels future compiles and waits for current solution to complete (safest option as msbuild could leave hanging files) you can also cancel IB build to do this faster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Compiler_Cancel_Click(object sender, RoutedEventArgs e)
        {
            m_ButtonOperators.SetStopCompile(true);
        }
        /// <summary>
        /// checks out selected items from all three tables (deselect required, overriden table function to select only one table allows multiple selections)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checkout_Click(object sender, RoutedEventArgs e)
        {
            List<string> TotalItems = new List<string>();
            TotalItems.AddRange(CodeCheckoutList);
            TotalItems.AddRange(ShadersCheckoutList);
            TotalItems.AddRange(OtherCheckoutList);
            List<string> SelectedItemsList = new List<string>();
            List<CheckOutItem> CheckoutItemListTempStore = new List<CheckOutItem>();
            foreach (ListBox lb in checkoutListBoxList.ToList<ListBox>())
            {
                foreach (string LBItem in lb.Items)
                {
                    if (lb.SelectedItems.Contains(LBItem))
                    {
                        SelectedItemsList.Add(LBItem);
                        if (m_xmlOperators.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile) == "rdr3")
                        {
                            if (LBItem.ToLower().Contains("script") )
                            {
                                SelectedItemsList.Add("parseddata");
                            }
                        }
                    }
                }
            }

            List<string> FilesToCheckout = new List<string>();
            foreach (string str in SelectedItemsList)
            {
                foreach (CheckOutItem checkoutItem in CheckoutItemList)
                {
                    LogHelper.g_Log.MessageCtx("FileCheckout", "Checkout item {0}", checkoutItem.friendlyReference);
                    if (checkoutItem.friendlyReference == str)
                    {
                        CheckoutItemListTempStore.Add(checkoutItem);
                        if (FilesToCheckout.Contains(checkoutItem.friendlyReference) == false)
                        {
                            FilesToCheckout.AddRange(CheckoutItemListTempStore.Last().fileOrFolderList);
                        }
                    }
                }
            }
            if (FilesToCheckout.Count > 0)
            {
                Thread thread = new Thread(() => { m_ButtonOperators.CheckoutFilestoNewCL(FilesToCheckout, "Prebuild Update"); });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Name = "checkoutthread";
                thread.Start();
            }
        }
        /// <summary>
        /// helper for checking out unfocused fields from tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checkout_Unhighlight_Executables_Click(object sender, MouseButtonEventArgs e)
        {
            (sender as ListBox).SelectedItem = null;
        }
        /// <summary>
        /// helper for checking out unfocused fields from tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checkout_Unhighlight_Others_Click(object sender, MouseButtonEventArgs e)
        {
            (sender as ListBox).SelectedItem = null;
        }
        /// <summary>
        /// helper for checking out unfocused fields from tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Checkout_Unhighlight_Shaders_Click(object sender, MouseButtonEventArgs e)
        {
            (sender as ListBox).SelectedItem = null;
        }
        /// <summary>
        /// set visibility of buttons from settings.xml reference
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeXML_Checked(object sender, RoutedEventArgs e)
        {
            if (ChangeXML.IsChecked == true)
            {
                SetButtonVisiblityfromBool(prebuildButtonList);
                SetButtonVisiblityfromBool(releaseButtonList);
            }
            else
            {
                SetButtonVisibilityFromXML(prebuildButtonList);
                SetButtonVisibilityFromXML(releaseButtonList);
            }
        }
        /// <summary>
        /// nothing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Patches_Durango_Click(object sender, RoutedEventArgs e)
        {

        }
        /// <summary>
        /// closing handling completed in an event however if needed run kill features in here (IB etc could be an option)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsMainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        /// <summary>
        /// closed so shutdown ALL threads and application handlers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RsMainWindow_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            System.Windows.Application.Current.Shutdown();
            Environment.Exit(0);
        }

        private void Button_Click_CopyToClipboard(object sender, RoutedEventArgs e)
        {

            if (m_xmlOperators.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile) == "rdr3")
            {
                Clipboard.SetText(RageLable.Content + " " + RageCL.Content + Environment.NewLine + SourceLable.Content + " " + SourceCL.Content + Environment.NewLine + ScriptLable.Content + " " + ScriptCL.Content + Environment.NewLine + TextLable.Content + " " + TextCL.Content + Environment.NewLine + DataLable.Content + " " + DataCL.Content + Environment.NewLine + MetaDataLable.Content + " " + MetaDataCL.Content + Environment.NewLine + ExportLable.Content + " " + ExportCL.Content);
            }
            else
            {
                Clipboard.SetText(RageLable.Content + " " + RageCL.Content + Environment.NewLine + SourceLable.Content + " " + SourceCL.Content + Environment.NewLine + ScriptLable.Content + " " + ScriptCL.Content + Environment.NewLine + TextLable.Content + " " + TextCL.Content + Environment.NewLine + DataLable.Content + " " + DataCL.Content + Environment.NewLine + MetaDataLable.Content + " " + MetaDataCL.Content + Environment.NewLine + ExportLable.Content + " " + ExportCL.Content);
            }
        }

        private void JapaneseModifier_Checked(object sender, RoutedEventArgs e)
        {
            Globals.isJapanese = true;
            SetUpCheckoutList();
        }

        private void JapaneseModifier_Unchecked(object sender, RoutedEventArgs e)
        {
            Globals.isJapanese = false;
            SetUpCheckoutList();
        }

        private void HideJapaneseCheckBox()
        {
            Globals.isJapanese = false;
            if((bool)JapaneseModifier.IsChecked)
            {
                SetUpCheckoutList();
            }
            if (JapaneseModifier.Visibility == Visibility.Visible)
            {
                JapaneseModifier.IsChecked = false;
                JapaneseModifier.Visibility = Visibility.Hidden;
            }
        }

        private void ShowJapaneseCheckBox()
        {
#if BV2GTA5
            if (JapaneseModifier.Visibility == Visibility.Hidden)
            {
                JapaneseModifier.Visibility = Visibility.Visible;

            }
#endif
        }

        private void ShowEditXml()
        {
            if (ChangeXML.Visibility == Visibility.Hidden)
            {
                ChangeXML.IsChecked = false;
                ChangeXML.Visibility = Visibility.Visible;
            }
        }

        private void HideEditXml()
        {
            if (ChangeXML.Visibility == Visibility.Visible)
            {
                ChangeXML.IsChecked = false;
                ChangeXML.Visibility = Visibility.Hidden;
            }
        }
    }
}