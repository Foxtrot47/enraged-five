﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; 
using System.Threading;
using System.Windows;

namespace BuildToolV2
{
 /// <summary>
 /// compiler object for generating easy to handle compilation operations
 /// </summary>
    public class CompilerObject
    {
        
        public string executableFileName { get; set; }
        //where do we want to put this in the list of objects (allows sorting by weight for list operations)
        public int objectweight { get; set; }
        public string project { get; set; }
        public string solutionPlatform { get; set; }
        public string solutionConfiguration { get; set; }
        public string compileType { get; set; }
        public CompilerObject(string _executableFileName, string _project, string _solutionPlatform, string _solutionConfiguration)
        {
            executableFileName = _executableFileName;
            project = _project;
            solutionPlatform = _solutionPlatform;
            solutionConfiguration = _solutionConfiguration;
            objectweight = GetWeightingfromSolutionConfiguration(_solutionConfiguration);
        }
        public CompilerObject(CompilerObject donorObject)
        {
            executableFileName = donorObject.executableFileName;
            project = donorObject.project;
            solutionPlatform = donorObject.solutionPlatform;
            solutionConfiguration = donorObject.solutionConfiguration;
            objectweight = donorObject.objectweight;
        }

        public void updateCompileType(string _compileType)
        {
            compileType = _compileType;
        }

        private int GetWeightingfromSolutionConfiguration(string _solutionConfiguration)
        {
            int weighting = 0;
            if (_solutionConfiguration == "beta")
            {
                weighting = 1;
            }
            if (_solutionConfiguration == "bankrelease")
            {
                weighting = 2;
            }
            if (_solutionConfiguration == "release")
            {
                weighting = 3;
            }
            if (_solutionConfiguration == "final")
            {
                weighting = 4;
            }
            if (_solutionConfiguration == "master")
            {
                weighting = 5;
            }
            return weighting;
        }

    }
    /// <summary>
    /// Shader object for compiling 
    /// </summary>
    public class ShaderObject
    {
        public string executableFileName { get; set; }
        public string objectweight { get; set; }
        public string project { get; set; }
        public string solutionPlatform { get; set; }
        public string solutionConfiguration { get; set; }
        public string compileType { get; set; }
        public ShaderObject(string _executableFileName, string _project, string _solutionPlatform, string _solutionConfiguration)
        {
            executableFileName = _executableFileName;
            project = _project;
            solutionPlatform = _solutionPlatform;
            solutionConfiguration = _solutionConfiguration;
        }
        public void updateCompileType(string _compileType)
        {
            compileType = _compileType;
        }
    }

    /// <summary>
    /// compiler operations class 
    /// </summary>
    public class CompilerOperators
    {
        public string project = "";
        public string XmlRef = "";
        public string LocalDirectory = "";
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        public FileOperators fileOps = new FileOperators();
        public XMLOperators xmlOps = new XMLOperators();
        public OutputWindow ow;
        static bool SuccessfulBuild = false;
        string CompilerCompletionFlags = "";
        public bool StopCompile = false;

        /// <summary>
        /// init compiler options
        /// </summary>
        /// <param name="_project">game project</param>
        /// <param name="_XmlRef">Settings.xml</param>
        /// <param name="_Directory">Directory</param>
        public void InitialiseCompilerOptions(string _project, string _XmlRef, string _Directory)
        {
            project = _project;
            XmlRef = _XmlRef;
            LocalDirectory = _Directory;
            CompilerCompletionFlags = xmlOps.GetElementFromXMLFile("CompilerCompletionFlags", "Setup", XmlRef);
        }

        /// <summary>
        /// Gets a list of executables from a branch
        /// </summary>
        /// <param name="_LocalDirectory"> directory to search</param>
        /// <returns> compiler objects populated with info for each executable found</returns>
        public List<CompilerObject> GetListOfExecutablesfromBranch(string _LocalDirectory)
        {
            List<CompilerObject> CompilerObjectList = new List<CompilerObject>();
            if (Directory.Exists(_LocalDirectory)) 
            { 
                foreach(string str in Directory.GetFiles(Path.GetFullPath(_LocalDirectory).Replace("\\", @"\"), "*.elf"))
                {
                    CompilerObjectList.Add(new CompilerObject(str, project, "orbis", Path.GetFileNameWithoutExtension(str).Replace("game_orbis_", "")));
                }

                foreach (string str in Directory.GetFiles(Path.GetFullPath(_LocalDirectory).Replace("\\", @"\"), "*.exe"))
                {
                    if (str.Contains("durango"))
                    {
                        CompilerObjectList.Add(new CompilerObject(str, project, "durango", Path.GetFileNameWithoutExtension(str).Replace("game_durango_", "")));
                    }
                    else
                    {
                        CompilerObjectList.Add(new CompilerObject(str, project, "x64", Path.GetFileNameWithoutExtension(str).Replace("game_win64_", "")));
                    }
                }
            }

            return CompilerObjectList;
        }

        /// <summary>
        /// generates a list of shader objecst grabbed from branch
        /// </summary>
        /// <param name="_LocalDirectory"> directory to search</param>
        /// <returns>List of shader object info</returns>
        public List<ShaderObject> GetListOfShadersfromBranch(string _LocalDirectory)
        {
            List<ShaderObject> ShaderObjectList = new List<ShaderObject>();
            List<String> RawListOfDirectories = new List<String>();
            if (Directory.Exists(_LocalDirectory))
            {
               RawListOfDirectories=Directory.GetDirectories(_LocalDirectory).ToList();
            }
            foreach (string str in RawListOfDirectories)
            {
                if (str.ToString().Contains("source") == false && str.ToString().Contains("debug") == false && str.ToString().Contains("db")==false)
                {
                    string ShaderPlatform = string.Empty;
                    string ShaderConfiguration = string.Empty;
                    if (str.ToString().Contains("final"))
                    {
                        ShaderPlatform = new DirectoryInfo(str).Name.Replace("_final", string.Empty);
                        if (ShaderPlatform.Contains("fx_"))
                        {
                            ShaderPlatform = ShaderPlatform.Replace("fx_", "");
                        }
                        ShaderConfiguration = "final";
                    }
                    else
                    {
                        ShaderPlatform = new DirectoryInfo(str).Name;
                        if (ShaderPlatform.Contains("fx_"))
                        {
                            ShaderPlatform = ShaderPlatform.Replace("fx_", "");
                        }
                        ShaderConfiguration = "dev";
                    }
                    ShaderObjectList.Add(new ShaderObject(str,project,ShaderPlatform,ShaderConfiguration));
                }
            }

            return ShaderObjectList;
        }

        /// <summary>
        /// Gets a compiler object from individual item 
        /// </summary>
        /// <param name="item">name of file and compiler object reference</param>
        /// <param name="CompilerObjectList">list to add to.</param>
        /// <returns></returns>
        public CompilerObject GetCompilerObjectFromItem(string item, List<CompilerObject> CompilerObjectList)
        {
            string platform = regOps.RegexAndRemoveChar(item,@"\w{0,}_",'_');
            string configuration = regOps.RegexAndRemoveChar(item, @"_\w{0,}", '_');

            CompilerObject compilerObjectRef = CompilerObjectList.Find(x => x.solutionConfiguration == configuration && x.solutionPlatform == platform);
            return compilerObjectRef;
        }

        /// <summary>
        /// gets a shader object from individual item reference 
        /// </summary>
        /// <param name="item">name of file/folder for shader object reference</param>
        /// <param name="ShaderObjectList">List to update for shader object</param>
        /// <returns></returns>
        public ShaderObject GetShaderObjectFromItem(string item, List<ShaderObject> ShaderObjectList)
        {
            string platform = regOps.RegexAndRemoveMatch(item, @"_\w{0,}_\w{0,}");
            string configuration = regOps.RegexAndRemoveChar(item, @"_\w{0,}", '_');
            if (item.Replace(@"_Shader", "").EndsWith("final"))
            {
                configuration = "final";
                platform = item.Replace(@"_Shader", "").Replace(@"_final", "");
            }
            else
            {
                configuration = "dev";
                platform = item.Replace(@"_Shader", "").Replace(@"_dev","");
            }
            

            ShaderObject compilerObjectRef = ShaderObjectList.Find(x => x.solutionConfiguration == configuration && x.solutionPlatform == platform);
            return compilerObjectRef;
        }

        /// <summary>
        /// compiles the selected config (from main menu xml)
        /// </summary>
        /// <param name="compilerObjectList">list of objects to compile</param>
        /// <param name="currentAppDirectory">current directory</param>
        /// <returns></returns>
        public async Task<bool> CompileSelectedConfigurations(List<CompilerObject> compilerObjectList, string currentAppDirectory)
        {
            LogHelper.g_Log.MessageCtx("Compiler Operator", "Compiling selected configurations");
            string Platform = "";
            //string BuildType = "";
            string Config = "";
            string BuildBranch = "";
            string Solution = "";
            //string shaderPath = "";
            string MSBuildSetting = "";
            string CompilerFlags = "";
            string LogDir = "";
            string compilerUsesMSBuild = "";
            string CompileTwice = xmlOps.GetElementFromXMLFile("CompilerBuildTwice", "Setup", XmlRef);
            string compileType = "";

            List<string> CompileTwiceExclusion = (xmlOps.GetElementFromXMLFile("CompilerBuildTwiceExclusion", "Setup", XmlRef)).Split(',').ToList<string>();
            List<CompilerObject> tempCompileObjectList = new List<CompilerObject>();
            List<CompilerObject> LocalCompilerObjectList = new List<CompilerObject>();
            LocalCompilerObjectList = compilerObjectList;

            

            if (ow == null)
            {
                LogHelper.g_Log.MessageCtx("Compiler Operator", "Output window is null");
                ow = new OutputWindow(this,"");
                ow.Show();
            }
            else
            {
                LogHelper.g_Log.MessageCtx("Compiler Operator", "Output window is not null");
                ow.ToggleWindowVisibility(true);
            }
            if (CompileTwice == "true")
            {
                LogHelper.g_Log.MessageCtx("Compiler Operator", "Compile twice is true");
                foreach (CompilerObject cObj in LocalCompilerObjectList)
                {
                    if (CompileTwiceExclusion.Contains(cObj.solutionPlatform + "_" + cObj.solutionConfiguration) == false)
                    {
                        tempCompileObjectList.Add(cObj);
                    }
                }
                foreach (CompilerObject ctempObj in tempCompileObjectList)
                {
                    if (LocalCompilerObjectList.Contains(ctempObj))
                    {
                        CompilerObject newObject = new CompilerObject(ctempObj);
                        newObject.compileType = @"/Build";
                        LocalCompilerObjectList.Insert(LocalCompilerObjectList.IndexOf(ctempObj) + 1, newObject);
                    }
                }
            }
            foreach(CompilerObject compilerObject in LocalCompilerObjectList)
            {
                LogHelper.g_Log.MessageCtx("Compiler Operator", "Compiling {0} on platform {1}", compilerObject.solutionConfiguration, compilerObject.solutionPlatform);
                if (StopCompile != true)
                {
                    if (ow == null)
                    {
                        ow = new OutputWindow(this,"");
                        ow.Show();
                    }
                    if (ow.Visibility != Visibility.Visible)
                    {
                        ow.ToggleWindowVisibility(true);
                    }

                    Platform = compilerObject.solutionPlatform;
                    Config = compilerObject.solutionConfiguration;
                    BuildBranch = xmlOps.GetElementFromXMLFile("BuildRoot", "Environment", XmlRef) + @"\";
                    Solution = xmlOps.GetElementFromXMLFile("CodeRoot", "Environment", XmlRef) + @"\game\VS_Project\" + xmlOps.GetElementFromXMLFile("SolutionCompiler", "Setup", XmlRef);
                    CompilerFlags = xmlOps.GetElementFromXMLFile("CompilerFlags", "Setup", XmlRef);
                    LogDir = currentAppDirectory + xmlOps.GetElementFromXMLFile("LogDir", "Setup", XmlRef) +"//" + compilerObject.solutionPlatform + "_" + compilerObject.solutionConfiguration + "_Log" + ".txt";
                    compilerUsesMSBuild = xmlOps.GetElementFromXMLFile("CompilerUsesMSBuild", "Setup", XmlRef);
                    compileType = compilerObject.compileType;

                    if(compilerUsesMSBuild!="")
                    {
                        MSBuildSetting = @" /" + compilerUsesMSBuild;
                    }else{
                        MSBuildSetting = compilerUsesMSBuild;
                    }

                    ow.updateoutputfilereference(LogDir);
                    await fileOps.ExecuteCommandAsync("BuildConsole.exe " + CompilerFlags + BuildBranch + " " + Solution + MSBuildSetting + " "+compileType + " /cfg=" + '\u0022' + Config + "|" + Platform + '\u0022', ow).ConfigureAwait(false);
               //     await WaitForSuccessCompletionInCompilerOutput().ConfigureAwait(false);
                    //await Task.Delay(2000).ConfigureAwait(false);
                    if (ow != null)
                    {
                        //fileOps.StopOuput();
                        //ow.WriteLogsToFile(LogDir, ow.Output_TextBox.ToString());
                        //fileOps.nullifyProcess();
                       // await Task.Delay(200).ConfigureAwait(false);
                       // ow.ClearContents();

                    }
                }
                else
                {
                    if (ow != null&& ow.Visibility==System.Windows.Visibility.Visible)
                    {
                        LogHelper.g_Log.MessageCtx("Compiler Operator", "Stop Compile detected");
                        fileOps.StopOuput();
                   //   ow.WriteLogsToFile(currentAppDirectory + xmlOps.GetElementFromXMLFile("LogDir", "Setup", XmlRef) + compilerObject.solutionPlatform + "_" + compilerObject.solutionConfiguration + "_Log" + ".txt", ow.Output_TextBox.ToString());
                        //fileOps.nullifyProcess();
                        ow.ClearContents();
                        await Task.Delay(200).ConfigureAwait(false);
                        StopCompile = false;
                    }
                    break;
                }

                await Task.Delay(2000).ConfigureAwait(true);
            }
            ow.ToggleWindowVisibility(false);
            bool complete = true;
            return complete;
        }
        
        /// <summary>
        /// checks string output from compile if build is successful 
        /// </summary>
        /// <param name="str"></param>
        public void CheckIfBuildSuccessfull(string str)
        {
            foreach (string strRef in CompilerCompletionFlags.Split(','))
            {
                if (str.Contains(strRef))
                {
                    SuccessfulBuild = true;
                    break;
                }
            }
        }

        /// <summary>
        /// async waits for a successful compile from output
        /// </summary>
        /// <returns>successful compilation (true/false)</returns>
        public async Task<bool> WaitForSuccessCompletionInCompilerOutput()
        {
            while (SuccessfulBuild == false)
            {
                if (StopCompile == true)
                {
                    break;
                }
            }
/*            await Task.Delay(0).ConfigureAwait(false);*/
            return true;
        }

        /// <summary>
        /// stops the compile list but lets teh current compile continue (can cancel via IB interface)
        /// </summary>
        /// <param name="stopCompile"></param>
        public void SetStopCompileFlag(bool stopCompile)
        {
            StopCompile = stopCompile;
        }


        /// <summary>
        /// compile a list of shaders
        /// </summary>
        /// <param name="Flag">compilation flag</param>
        /// <param name="ShaderObjectList">shader object </param>
        /// <param name="currentAppDirectory">current applicaiton directory</param>
        /// <returns></returns>
        public async Task<bool> CompileSelectedShader(string Flag, List<ShaderObject> ShaderObjectList, string currentAppDirectory)
        {
            //string Platform = "";
            //string BuildType = "";
            //string Config = "";
            string Solution = "";
            string LogDir = "";
            List<string> ListofBatFiles = new List<string>();
            string BatFileLocation = xmlOps.GetElementFromXMLFile("CodeRoot", "Environment", XmlRef) + @"/game/shader_source/VS_Project/batch/";
            ListofBatFiles = Directory.GetFiles(BatFileLocation).ToList();

            string SpecialCases = xmlOps.GetElementFromXMLFile("ShaderSpecialCases", "Setup", XmlRef);
            string[] ArrayofSpecialCases = SpecialCases.Split(',');

            if (ow == null)
            {
                ow = new OutputWindow(this,"");
                ow.Show();
            }
            else
            {
                ow.ToggleWindowVisibility(true);
            }

            foreach (ShaderObject shaderObject in ShaderObjectList.ToList<ShaderObject>())
            {
                if (StopCompile != true)
                {
                    SuccessfulBuild = false;
                    bool specialCase = false;
                    if (ow == null)
                    {
                        ow = new OutputWindow(this,"");
                        ow.Show();
                    }
                    ow.ToggleWindowVisibility(true);
                    for (int i = 0; i <= ArrayofSpecialCases.Length - 1; i++)
                    {
                        if (i % 2 == 0)
                        {
                            if (shaderObject.solutionPlatform == ArrayofSpecialCases[i])
                            {
                                specialCase = true;
                                Solution = ArrayofSpecialCases[i + 1];
                            }
                        }
                    }
                    foreach (string str in ListofBatFiles)
                    {
                        if (specialCase)
                        {
                            if (str.ToString().Contains(shaderObject.compileType))
                            {
                                if (str.ToString().Contains(Solution))
                                {
                                    Solution = str.ToString();
                                    specialCase = false;
                                }
                            }

                        }
                        else
                        {
                            if (str.ToString().Contains(shaderObject.compileType))
                            {
                                if (str.ToString().Contains(shaderObject.solutionPlatform))
                                {
                                    Solution = str.ToString();
                                    break;
                                }
                            }
                        }
                    }
                    LogDir = currentAppDirectory + xmlOps.GetElementFromXMLFile("LogDir", "Setup", XmlRef) + "//" +"Shader_"+ shaderObject.solutionPlatform + "_" + shaderObject.solutionConfiguration + "_Log" + ".txt";
                    ow.updateoutputfilereference(LogDir);
                    await fileOps.ExecuteCommandAsync(Solution + " " + shaderObject.solutionConfiguration, ow).ConfigureAwait(false);
                    //await WaitForSuccessCompletionInCompilerOutput().ConfigureAwait(false);
                    if (ow != null)
                    {
               //         fileOps.StopOuput();
                 //       ow.WriteLogsToFile(currentAppDirectory + xmlOps.GetElementFromXMLFile("LogDir", "Setup", XmlRef) + shaderObject.solutionPlatform + "_" + shaderObject.solutionConfiguration + "_Log" + ".txt", ow.Output_TextBox.ToString());
                   //     ow.ClearContents();
                     //   await Task.Delay(200).ConfigureAwait(false);
                    //    ow.ToggleWindowVisibility(false);
                      //  await Task.Delay(1000).ConfigureAwait(false);
                        //SuccessfulBuild = false;
                        specialCase = false;
                    }
                }
                else
                {
                    if (ow != null && ow.Visibility == System.Windows.Visibility.Visible)
                    {
                        fileOps.StopOuput();
                        ow.WriteLogsToFile(currentAppDirectory + xmlOps.GetElementFromXMLFile("LogDir", "Setup", XmlRef) + shaderObject.solutionPlatform + "_" + shaderObject.solutionConfiguration + "_Log" + ".txt", ow.Output_TextBox.ToString());
                        ow.ClearContents();
                        await Task.Delay(200).ConfigureAwait(false);
                        ow.ToggleWindowVisibility(false);
                        await Task.Delay(1000).ConfigureAwait(false);
                        SuccessfulBuild = false;
                    }
                    break;
                }
                await Task.Delay(2000).ConfigureAwait(true);
            }
            ow.ToggleWindowVisibility(false);
            bool complete = true;
            return complete;
        }
    }
}



