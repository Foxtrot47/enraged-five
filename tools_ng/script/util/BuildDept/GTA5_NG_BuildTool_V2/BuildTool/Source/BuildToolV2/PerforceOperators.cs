﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RSG.Editor.Controls.Perforce;
using RSG.Editor.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Input;
using RSG.Base;
using global::Perforce.P4;
using RSG.Interop.Perforce;
using System.ComponentModel;



namespace BuildToolV2
{
 /// <summary>
 /// class for passing perforce errors and CL Numbers via an object
 /// </summary>
    public class CLAndErrorReportObject
    {
        public List<string> PerforceErrors { get; set; }
        public List<string> PerforceInfos { get; set; }
        public int CLNumber { get; set; }

        public CLAndErrorReportObject(List<string> _PerforceErrors, List<string> _PerforceInfos, int _CLNumber)
        {
            PerforceErrors = _PerforceErrors;
            PerforceInfos = _PerforceInfos;
            CLNumber = _CLNumber;
        }

        public CLAndErrorReportObject()
        {
        }
    }

    public partial class PerforceOperators
    {
        public List<string> P4SyncQueue;
        public PerforceService perforceService = new PerforceService();
        public FileOperators fileOps = new FileOperators();
        public RunAsyncPattern asyncOps = new RunAsyncPattern();
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        //Thread thread;
        //volatile bool stopSync = false;
        //int CLNumber = 0;
        bool complete = false;
        // bg worker test
        private readonly BackgroundWorker worker = new BackgroundWorker();
        public IList<string> listOfLocations = new List<string>();
        public IList<string> ListofLocalLocations = new List<string>();
        public bool force = false;
        public CLAndErrorReportObject CLAndReport;
        public SyncItem syncItem;

        /// <summary>
        /// attempt at canceling the p4 sync (not supported with the current p4api
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CancelSync(){
            worker.CancelAsync();
            await Task.Delay(0).ConfigureAwait(false);
            return true;
//             stopSync = true;
//             thread.Interrupt();
//             return true;
        }

        /// <summary>
        /// another attempt at cancelling p4 sync (not supported with current P4API)
        /// </summary>
        /// <returns></returns>
        public async Task<bool> StopAll()
        {
            //thread.Abort();
            await Task.Delay(0).ConfigureAwait(false);
            return true;
        }

        /// <summary>
        /// worker for syncing latest
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Worker_Sync(object sender, DoWorkEventArgs e)
        {
            var temp = perforceService.GetLatestWithOptionsAndReturnChangelistNumber(perforceService.ConvertPathsToFilespec(syncItem.ClientRef,syncItem.DepotRef,syncItem.LocalRef),syncItem.force, true) ;
            CLAndReport = new CLAndErrorReportObject(temp.PerforceErrors, temp.PerforceInfo, temp.CLNumber);
            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Integrating and resolve attempt (TODO - make it work)
        /// </summary>
        /// <param name="sourcefileorfolder"></param>
        /// <param name="targetfileorfolder"></param>
        public void integrate_and_resolve(string sourcefileorfolder, string targetfileorfolder)
        {

                listOfLocations.Clear();
                listOfLocations.Add(@"" + sourcefileorfolder);
            
                if (sourcefileorfolder.Contains("X:") || sourcefileorfolder.Contains("x:"))
                {
                    ListofLocalLocations = listOfLocations;
                }
                else
                {
                    ListofLocalLocations = perforceService.GetLocalPathsFromDepotPaths(listOfLocations);
                }
                
                ClientPath path = new ClientPath(targetfileorfolder);
                FileSpec targetfile = new FileSpec(path ,null);
                perforceService.integrate(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(ListofLocalLocations), targetfile);
                perforceService.Resolve(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(ListofLocalLocations));

        }

        /// <summary>
        /// gets the client branch from the workspace reference 
        /// </summary>
        /// <param name="WorspaceRef"></param>
        /// <returns></returns>
        public string getClientBranchfromWorkspaceRef(string WorspaceRef)
        {
            string ClientBranch ="";
            ClientBranch = perforceService.GetClientPathsFromWorkspace(WorspaceRef);
            return ClientBranch;
        }

        /// <summary>
        /// runs on completion of the worker allows us to continue with async functions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunSync_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            complete = true;
            if (CLAndReport != null)
            {
                if (CLAndReport.PerforceInfos.Count > 0)
                {
                    RsMessageBox info = new RsMessageBox();
                    info.Caption = "Perforce Infos";
                    info.Text = string.Join("\r\n", CLAndReport.PerforceInfos);
                    info.AddButton("OK", 0, true, true);
                    info.ShowMessageBox();
                }
                if (CLAndReport.PerforceErrors.Count > 0)
                {
                    RsMessageBox errors = new RsMessageBox();
                    errors.Caption = "Perforce Errors";
                    errors.Text = string.Join("\r\n", CLAndReport.PerforceErrors);
                    errors.AddButton("OK", 0, true, true);
                    errors.ShowMessageBox();
                }
            }
        }
// 
//         public async Task<int> GetLatestRevisionAsync(SyncItem _syncItem)
//         {
//             CLAndErrorReportObject clnumber;
//             clnumber = await GetLatestRevisionAsync(_syncItem).ConfigureAwait(true);
//             return clnumber.CLNumber;
//         }

        /// <summary>
        /// gets the latest revision and returns a cl number and report of errors via an object
        /// </summary>
        /// <param name="_syncItem"></param>
        /// <returns></returns>
        public async Task<CLAndErrorReportObject> GetLatestRevisionAsync(SyncItem _syncItem)
        {
            CLAndReport = new CLAndErrorReportObject();
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                syncItem = _syncItem;
                worker.DoWork += Worker_Sync;
                worker.RunWorkerCompleted += worker_RunSync_Completed;
                worker.WorkerSupportsCancellation = true;
                worker.RunWorkerAsync();
            });
            while (complete != true)
            {

            }
            await Task.Delay(1).ConfigureAwait(false);
            return CLAndReport;
                
        }

//         public async Task<int> GetLatestRevisionAsync(string fileOrFolder, bool force)
//         {
// 
//             bool complete = false;
//             IList<string> listOfLocations = new List<string>();
//             listOfLocations.Add(@"" + fileOrFolder);
//             IList<string> ListofLocalLocations = new List<string>();
//             int CLNumber = 0;
//             Exception ex = null;
// 
//             if (fileOrFolder.Contains("X:")||fileOrFolder.Contains("x:"))
//             {
//                 ListofLocalLocations = listOfLocations;
//             }
//             else
//             {
//                 ListofLocalLocations = perforceService.GetLocalPathsFromDepotPaths(listOfLocations);
//             }
//             try
//             {  
//                 if (force) 
//                 {
//                     thread = new Thread(() => {
//                          //CLNumber = perforceService.GetLatestWithOptionsAndReturnChangelistNumber(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(listOfLocations), true,true);
//                          CLNumber = perforceService.GetLatestWithOptionsAndReturnChangelistNumber(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(listOfLocations), force, true);
//                          while (stopSync)
//                          {
//                              if (!complete)
//                              {
//                                  perforceService.CancelSync();
//                                  P4Exception.Throw(ErrorSeverity.E_FATAL, "cancel Sync");
//                                  complete = true;
//                              }
//                          }
//                     });
//                     thread.SetApartmentState(ApartmentState.STA);
//                     thread.Start();
//                     thread.Join();
// 
//                     /*thread = new Thread(() => { perforceService.GetLatestWithOptions(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(listOfLocations), true); });
//                     thread.Start();
//                     */
//                //     await CheckThread(thread).ConfigureAwait(false);
//                     complete = true;
// 
// 
//                 }
//                 else
//                 {
//                     complete = false;
//                         thread = new Thread(() => {
// 
//                             CLNumber = perforceService.GetLatestWithOptionsAndReturnChangelistNumber(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(listOfLocations), force, true);
//                             while (stopSync)
//                             {
//                                 if (!complete)
//                                 {
// //                                  perforceService.CancelSync();
//                                     P4Exception.Throw(ErrorSeverity.E_FATAL,"cancel Sync");
//                                     complete = true;
//                                 }
//                             }
//                         });
//                         thread.SetApartmentState(ApartmentState.STA);
//                         thread.Start();
//                         thread.Join();
//                         //await CheckThread(thread).ConfigureAwait(false);
//                         complete = true;
// 
//                 }
//             }
//             catch
//             {
//                 complete = true;
//                 return CLNumber;
//             }
// 
//             if(thread!=null){
//                 complete = await CheckThread(thread).ConfigureAwait(false);
//                 stopSync = true;
//                 complete = true;
//             }
//                     //await Task.Run(() => { GetLatestRevision(ListofLocalLocations); });
//                     /*
//                     thread = new Thread(() => { GetLatestRevision(ListofLocalLocations);});
//                     thread.Start();
//                     await CheckThread(thread).ConfigureAwait(false);
//                      */
//             if (CLNumber == null)
//             {
//                 CLNumber = 0;
//             }
// 
//             return CLNumber;
//         }



        /// <summary>
        /// checks the sync thread for completion 
        /// </summary>
        /// <param name="threadToCheck"></param>
        /// <returns></returns>
        public async Task<bool> CheckThread(Thread threadToCheck)
        {
            bool threadCompleting = false;
            if (threadToCheck != null)
            {
                while (!threadCompleting)
                {
                    if (threadToCheck != null)
                    {
                        if (threadToCheck.ThreadState == ThreadState.Stopped)
                        {
                            threadCompleting = true;
                        }
                        await Task.Delay(0).ConfigureAwait(false);
                    }
                }
            }
            return threadCompleting;
        }

        /// <summary>
        /// gets the latest revision returns exceptions 
        /// </summary>
        /// <param name="fileOrFolder"></param>
        /// <param name="force"></param>
        /// <returns></returns>
        public Exception GetLatestRevision(string fileOrFolder, bool force)
        {


            IList<string> listOfLocations = new List<string>();
            listOfLocations.Add(@"" + fileOrFolder);
            IList<string> ListofLocalLocations = new List<string>();
            if (fileOrFolder.Contains("X:") || fileOrFolder.Contains("x:"))
            {
                ListofLocalLocations = listOfLocations;
            }
            else
            {
                ListofLocalLocations = perforceService.GetLocalPathsFromDepotPaths(listOfLocations);
            }
            try
            {
                if (force)
                {
   //                 perforceService.GetLatestWithOptionsAndReturnChangelistNumber(perforceService.GetLocalPathsFromDepotPathsAndReturnFileSpec(listOfLocations), true);

                    return null;
                }
                else
                {
                    GetLatestRevision(ListofLocalLocations);
                    return null;
                }
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        /// <summary>
        /// creates a perforce sync queue window for syncing multiple items
        /// </summary>
        public void CreateP4SyncQueue()
        {
            P4SyncQueue = new List<string>();
        }

        /// <summary>
        /// Clears the P4 Queue
        /// </summary>
        public void DestroyP4SyncQueue()
        {
            P4SyncQueue.Clear();
        }

        /// <summary>
        /// processes P4 Sync queue 
        /// </summary>
        /// <returns></returns>
        public bool ProcessP4SyncQueue()
        {
            bool queueEmpty = true;
            if (P4SyncQueue != null)
            {
                foreach (string folderFileName in P4SyncQueue)
                {
                    //if ()
                }
                int queueLength = P4SyncQueue.Count();
                if (queueLength > 0)
                {
                    
                    queueLength--;

                }
                else
                {
                    queueEmpty = true;
                }
            }
            return queueEmpty;
        }

        /// <summary>
        /// checks out the a file or folder
        /// </summary>
        /// <param name="FileorFolder"></param>
        /// <param name="CLDescription"></param>

        public void checkoutFiles(IList<string> FileorFolder,string CLDescription)
        {
            LogHelper.g_Log.MessageCtx("PerforceOperators", "Checking out {0} files", FileorFolder.Count.ToString());
            var temp = perforceService.CheckoutFiles(FileorFolder, CLDescription);
            CLAndErrorReportObject report = new CLAndErrorReportObject(temp.PerforceErrors, temp.PerforceInfo, temp.CLNumber);

            if (report.PerforceInfos.Count > 0)
            {
                RsMessageBox info = new RsMessageBox();
                info.Caption = "Perforce Infos";
                info.Text = string.Join("\r\n", report.PerforceInfos);
                info.AddButton("OK", 0, true, true);
                info.ShowMessageBox();
            }
            if (report.PerforceErrors.Count > 0)
            {
                RsMessageBox errors = new RsMessageBox();
                errors.Caption = "Perforce Errors";
                errors.Text = string.Join("\r\n", report.PerforceErrors);
                errors.AddButton("OK", 0, true, true);
                errors.ShowMessageBox();
            }
        }

        /// <summary>
        /// Gets the latest revision (doesnt return cl number)
        /// </summary>
        /// <param name="filesOrFolders"></param>
        public void GetLatestRevision(IList<string> filesOrFolders)
        {
            LogHelper.g_Log.MessageCtx("PerforceOperators", "Getting latest revision on {0} files", filesOrFolders.Count.ToString());
            IList <string> ListofLocalLocations = perforceService.GetLocalPathsFromDepotPaths(filesOrFolders);
            perforceService.GetLatest(filesOrFolders);
        }

        /// <summary>
        /// gets the depot path from a workspace or local file reference
        /// </summary>
        /// <param name="filesOrFolder">IList for p4 compat (can be a list of one file)</param>
        /// <returns></returns>
        public string GetDepotPathFromLocalPath(IList<string> filesOrFolder)
        {
            LogHelper.g_Log.MessageCtx("PerforceOperators", "Getting depot path from local path {0}", filesOrFolder[0]);
            IList <string> ListofDepotLocations = perforceService.GetDepotPathsFromLocalPaths(filesOrFolder);
            return ListofDepotLocations[0].ToString();
        }

        /// <summary>
        /// Gets the head revision and changelist number of a branch
        /// </summary>
        /// <param name="fileOrFolder">path to sync</param>
        /// <param name="force">force sync</param>
        /// <returns></returns>
        public string GetHeadRevisionAndCLofBranchOrFile(string fileOrFolder,bool force)
        {
            string CLRevision = "";
            Exception ex = null;
            Thread thread = new Thread(() => { ex = GetLatestRevision(fileOrFolder, force); });
            if (fileOrFolder.Contains("x:") || fileOrFolder.Contains("X:"))
            {
                CLRevision = regOps.RegexText(@"[0-9]{8}|[0-9]{7}", asyncOps.ExecuteCommandUsingCallback("p4 changes -m 1 -s submitted " + fileOrFolder + @"\..."));
            }
            else
            {
                CLRevision = regOps.RegexText(@"[0-9]{8}|[0-9]{7}", asyncOps.ExecuteCommandUsingCallback("p4 changes -m 1 -s submitted " + fileOrFolder));
            }
            thread.Name = "SyncThread";
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            //PerforceSyncWindow dialogueWindow = new PerforceSyncWindow(thread);
            //dialogueWindow.SetBranchNameInWindow(fileOrFolder);
            //dialogueWindow.Show();
            if (ex != null)
            {
                PerforceErrorWindow errorWindow = new PerforceErrorWindow();
                errorWindow.SetError(""+ex.Message);
                errorWindow.Show();
            }
            return CLRevision;
        }

        /// <summary>
        /// gets the changelists between two cl numbers for a specific branch
        /// </summary>
        /// <param name="depotBranch">branch to check against</param>
        /// <param name="changeListOne">start cl</param>
        /// <param name="changeListTwo">end cl</param>
        /// <returns></returns>
        public string GetChangesBetweenTwoCLs(string depotBranch,int changeListOne, int changeListTwo)
        {
            List<string> branch = new List<string>();
            branch.Add(depotBranch);
            List<string> BugstarNumberList = new List<string>();
            IList<Changelist> CLResults = new List<Changelist>();
         //   CLResults = asyncOps.ExecuteCommandUsingCallback("p4 changes -l -s submitted " + depotBranch + "@" + changeListOne + "," + changeListTwo);
            CLResults = perforceService.GetChangesBetweenTwoCLsViaFileorFolder(branch, changeListOne, changeListTwo);
            StringBuilder Results = new StringBuilder();
            if (CLResults!=null)
            {
                foreach (Changelist cl in CLResults)
                {
                    Results.Append(cl.Description);
                }
            }
            return Results.ToString();
        }

        /// <summary>
        /// Gets the change description from one cl list
        /// </summary>
        /// <param name="ChangeList">changelist number</param>
        /// <returns></returns>
        public string GetChangesFromOneCL(string ChangeList)
        {
            string CLResults = "";
            CLResults = asyncOps.ExecuteCommandUsingCallback("p4 describe -s submitted " + ChangeList);
            return CLResults;
        }

        /// <summary>
        /// gets head revision of branch and returns a list of changelist numbers of a branch
        /// </summary>
        /// <param name="fileOrFolder"></param>
        /// <returns></returns>
        public List<string> GetHeadRevisionAndCLofBranchOrFile(IList<string> fileOrFolder)
        {
            List<string> CLRevision = new List<string>();
            foreach (string str in fileOrFolder)
            {
                CLRevision.Add(regOps.RegexText(@"[0-9]{8}|[0-9]{7}", asyncOps.ExecuteCommandUsingCallback("p4 changes -m 1 -s submitted " + str)));
            }
            Thread thread = new Thread(() => { GetLatestRevision(fileOrFolder); });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return CLRevision;
        }

    }
}

