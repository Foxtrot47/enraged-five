﻿#pragma checksum "..\..\..\..\Forms\OutputWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2DEE1E1E519E6D38AE755ABCBA60D496"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RSG.Editor;
using RSG.Editor.Controls;
using RSG.Editor.Controls.AttachedDependencyProperties;
using RSG.Editor.Controls.Chromes;
using RSG.Editor.Controls.Commands;
using RSG.Editor.Controls.Converters;
using RSG.Editor.Controls.Extensions;
using RSG.Editor.Controls.Helpers;
using RSG.Editor.Controls.Media;
using RSG.Editor.Controls.Presenters;
using RSG.Editor.Controls.Resources;
using RSG.Editor.Controls.Themes;
using RSG.Editor.Model;
using RSG.Editor.SharedCommands;
using RSG.Editor.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BuildToolV2 {
    
    
    /// <summary>
    /// OutputWindow
    /// </summary>
    public partial class OutputWindow : RSG.Editor.Controls.RsMainWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 7 "..\..\..\..\Forms\OutputWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RichTextBox Output_TextBox;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BuildToolV2;component/forms/outputwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Forms\OutputWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\..\..\Forms\OutputWindow.xaml"
            ((BuildToolV2.OutputWindow)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.window_Closed);
            
            #line default
            #line hidden
            
            #line 5 "..\..\..\..\Forms\OutputWindow.xaml"
            ((BuildToolV2.OutputWindow)(target)).Closed += new System.EventHandler(this.window_Closed);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Output_TextBox = ((System.Windows.Controls.RichTextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

