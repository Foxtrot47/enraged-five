﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Editor.Controls;

namespace BuildToolV2
{
    /// <summary>
    /// Interaction logic for ButtonNameWindow.xaml
    /// </summary>
    /// 

    public partial class ButtonNameWindow : RsMainWindow
    {
        public string xmlfilereference;
        public string element;
        public XMLOperators m_xmlOperators = new XMLOperators();
        public FileOperators m_fileOperator = new FileOperators();
        /// <summary>
        /// init of a new window for renaming main menu buttons.
        /// </summary>
        /// <param name="Xmlfilereference">string - settings.xml file</param>
        /// <param name="Element">string - defines the element to be replaced</param>
        public ButtonNameWindow(string Xmlfilereference, string Element)
        {
            InitializeComponent();
            xmlfilereference = Xmlfilereference;
            element = Element;
            foreach (string str in m_xmlOperators.GetListOfChildrenfromParent("Environment", xmlfilereference)){
                EnvironmentComboBox.Items.Add(str.Replace("Root", ""));
            }
        }
        /// <summary>
        /// process changes for button naming including references to batchfile, args and branch specifics
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_Next(object sender, RoutedEventArgs e)
        {
            Char c = '\u005c';
            string localPath = "";
            if (ButtonName.Text != "")
            {
                string arguments = "";
                string branchname = "";
                if (EnvironmentAsArgsCheckbox.IsChecked == true)
                {
                    branchname = "Branch";
                }
                string spacer ="";
                if (branchname!=""){ spacer=" ";}else{} 
                arguments = branchname + spacer + Args.Text;

                if (EnvVarCheckbox.IsChecked == false)
                {
                    m_xmlOperators.WriteStringToElement(element, "Buttons", ButtonName.Text + ";" + m_fileOperator.OpenFileBrowserDialogueAndRetrieveFileStringName("") + ";" + " ;" + arguments, xmlfilereference);
                    Close();
                }
                else
                {
                    string Environmentpath = "";
                    string EnvironmentpathName = "";
                    foreach (string str in m_xmlOperators.GetListOfChildrenfromParent("Environment", xmlfilereference))
                    {
                        if (str.Contains(EnvironmentComboBox.SelectedItem.ToString()))
                        {
                            Environmentpath = @""+m_xmlOperators.GetElementFromXMLFile(str, "Environment", xmlfilereference);
                            EnvironmentpathName = str;
                        }
                    }


                    localPath = System.Text.RegularExpressions.Regex.Replace(@"" + m_fileOperator.OpenFileBrowserDialogueAndRetrieveFileStringName(Environmentpath), String.Join(""+c+c, Environmentpath.Split(c).ToArray(), 0, Environmentpath.Split(c).Length), "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    m_xmlOperators.WriteStringToElement(element, "Buttons", ButtonName.Text + ";" + @"" + localPath + ";" + @"" + EnvironmentpathName + ";" + arguments, xmlfilereference);
                    Close();
                }
            }
            else
            {
                if (RsMessageBox.Show(Application.Current.MainWindow, "Entering nothing into the name field will hide the button and remove all its batfile links are you sure you want to continue?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    m_xmlOperators.WriteStringToElement(element, "Buttons", "", xmlfilereference);
                    Close();
                }
            }
        }
        /// <summary>
        /// use environment variable reference (removes path, allows branch unspecific batfile use)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnvVarCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (EnvVarCheckbox.IsChecked == true)
            {
                EnvironmentComboBox.IsEnabled = true;
            }
            else
            {
                EnvironmentComboBox.SelectedIndex = -1;
                EnvironmentComboBox.IsEnabled = false;
            }
        }
    }
}
