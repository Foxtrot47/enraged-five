﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Editor.Controls;
using System.Timers;

namespace BuildToolV2
{
    /// <summary>
    /// Interaction logic for PerforceErrorWindow.xaml
    /// </summary>
    public partial class PerforceErrorWindow : RsMainWindow
    {

        public PerforceErrorWindow()
        {
            InitializeComponent();
        }

        public void SetError (string error)
        {
            ErrorTextBlock.Text= error;
        }

    }
}
