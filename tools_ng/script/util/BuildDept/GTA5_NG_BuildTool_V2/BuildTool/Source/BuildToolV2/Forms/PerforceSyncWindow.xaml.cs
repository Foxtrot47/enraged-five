﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using RSG.Editor.Controls;
using System.Timers;
using System.ComponentModel; // CancelEventArgs
using System.Windows.Threading;

namespace BuildToolV2
{

    delegate void CloseWindow(object source, ElapsedEventArgs e);
    public delegate void CloseDelagate(); 
    /// <summary>
    /// Interaction logic for PerforceSyncWindow.xaml
    /// </summary>
    public partial class PerforceSyncWindow : RsMainWindow
    {
        bool SyncComplete = false;
        PerforceSyncWindow p4syncWindow;
        public PerforceOperators perfOps = new PerforceOperators();
        //Thread thread;
        System.Timers.Timer time;
        PerforceSyncQueueWindow p4SyncQueueWindow;
        SyncItem syncItem;
        //volatile bool shutdown = false;
        Dispatcher SyncDispatcher;
        Dispatcher SyncQueueDispatcher;
        public XMLOperators xmlOps = new XMLOperators();
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        int CLNumber = 0;
        string xmlFile = "";
        public PerforceSyncWindow()
        {
        }
        /// <summary>
        /// init p4 sync window (child to perforce syncqueue window)
        /// </summary>
        /// <param name="SyncItem"> object - distro item to sync</param>
        /// <param name="p4Q">perforcequeueWindow reference - parent window</param>
        /// <param name="SyncQueueDispatcherRef">p4queue window dispatcher for communication</param>
        /// <param name="XmlFile">settings.xml</param>
        public PerforceSyncWindow(SyncItem SyncItem, PerforceSyncQueueWindow p4Q, Dispatcher SyncQueueDispatcherRef,string XmlFile)
        {
            InitializeComponent();
            time = new System.Timers.Timer();
            time.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            time.Interval = 100;
            time.Enabled = true;
            p4syncWindow = this;
            p4SyncQueueWindow = p4Q;
            syncItem = SyncItem;
            SyncQueueDispatcher = SyncQueueDispatcherRef;
            SyncDispatcher = Application.Current.Dispatcher;
            Branch.Text = syncItem.ClientRef;
            xmlFile = XmlFile;

/*            RunThread();*/
        }
        /// <summary>
        /// timed event for closing the sync window after a successful sync
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            time.Interval = 100;
            if (SyncComplete)
            {
                //Dispatcher.BeginInvoke(new Action(() => p4SyncQueueWindow.RemovefromListAndSyncNext(fileOrFolder)));
                //SyncDispatcher.Invoke((MethodInvoker) delegate() {p4SyncQueueWindow.P4SyncWindow.Close();});

                if (this.Dispatcher.CheckAccess() == false)
                {
                    CloseWindow CloseWindowCallback = new CloseWindow(OnTimedEvent);
                    this.Dispatcher.Invoke(CloseWindowCallback, source, e);
                }
                else
                {
                    System.Windows.Threading.Dispatcher.ExitAllFrames();
                    time.Close();
                    time.Dispose();
                    p4SyncQueueWindow.NullifyWindowRef();
                    SyncComplete = false;
                    Close();
                }
               // Dispatcher.BeginInvoke(new Action(() => this.Close()));
            }
        }
        /// <summary>
        /// checks threadstate and reports if thread state is running.
        /// </summary>
        /// <param name="thread"></param>
        /// <returns></returns>
        public bool IsThreadRunning(Thread thread)
        {
            if (thread != null)
            {
                if (thread.ThreadState != ThreadState.Stopped || thread.ThreadState != ThreadState.Aborted)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }else{
                return false;
            }
            
        }

        /// <summary>
        /// syncs the object sync item 
        /// </summary>
        /// <param name="syncItem">object for reference to the item to sync</param>
        /// <returns></returns>
        public async Task<bool> Sync(SyncItem syncItem)
        {
            syncItem.ClientRef = perfOps.getClientBranchfromWorkspaceRef(syncItem.LocalRef.ToString());
            SyncComplete = await SyncTask(syncItem).ConfigureAwait(false);
            return SyncComplete;
        }
        /// <summary>
        /// task for syncing the item 
        /// </summary>
        /// <param name="syncItem"></param>
        /// <returns></returns>
        public async Task<bool> SyncTask(SyncItem syncItem)
        {
            CLAndErrorReportObject reportObject;

            reportObject = await perfOps.GetLatestRevisionAsync(syncItem).ConfigureAwait(false);
            CLNumber = reportObject.CLNumber;
            if (CLNumber != 0)
            {
                await xmlOps.WriteStringToElementAsync(syncItem.ToolReference, "CurrentCLData", CLNumber.ToString(), xmlFile).ConfigureAwait(false);
            }            
            
            return true;
        }

        /// <summary>
        /// sets branchname in window
        /// </summary>
        /// <param name="branchName"></param>
        public void SetBranchNameInWindow (string branchName)
        {
            BranchLabel.Content = branchName;
            AnimatedDot.Content = "...";
            BranchLabel.HorizontalContentAlignment = HorizontalAlignment.Center;
        }
        /// <summary>
        /// perforce cancelation doesnt function well in this version of p4api implementation. perhaps future version suppor the ctrl+c file operation 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void P4SyncCancel_OnClick(object sender, RoutedEventArgs e)
        {
            SyncComplete = true;
            time.Interval = 1;
            await perfOps.CancelSync().ConfigureAwait(false);
        }
        /// <summary>
        /// checks to see if the window is ready to start the process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            p4SyncQueueWindow.UpdateCurrentP4SyncWindow(this);
            await Task.Delay(0).ConfigureAwait(false);
        }


//         async Task<bool> RunThread()
//         {
//             try
//             {
//                 while (!shutdown)
//                 {
//                     await Task.Delay(100).ConfigureAwait(false);
//                 }
//                 throw new System.ArgumentException("ThreadTerminated");
//             }
//             catch (Exception e)
//             {
//             }
//             return true;
//         }
// 
//         public void StopThread()
//         {
//             shutdown = true;
//         }
    }
}
