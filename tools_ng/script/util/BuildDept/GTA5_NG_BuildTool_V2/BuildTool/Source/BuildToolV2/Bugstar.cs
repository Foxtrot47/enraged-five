﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Net;
using System.Timers;

namespace BuildToolV2
{
    /// <summary>
    /// Bug class that holds the bugNumber,summary, and bugclass.
    /// </summary>
    public class Bug
    {
        public int bugNumber { get; set; }
        public string summary { get; set; }
        public string bugclass { get; set; }
        public string project { get; set; }
        public string status { get; set; }
        public Bug(int BugNumber, string Summary, string BugClass, string Project, string Status)
        {
            bugNumber = BugNumber;
            summary = Summary;
            bugclass = BugClass;
            project = Project;
            status = Status;
            
        }
    }
    /// <summary>
    /// login object containing response and bool to verify and pass back
    /// </summary>
    public class Login
    {
        private bool successfulLogin { get; set;}
        private string httpWebResponse { get; set; }

        public Login(bool _successfulLogin, string _httpWebRequest)
        {
            successfulLogin = _successfulLogin;
            httpWebResponse = _httpWebRequest;
        }
        public bool IsLoginSuccessful()
        {
            return successfulLogin;            
        }
        public string GetWebRequest()
        {
            if (httpWebResponse != "")
            {
                return httpWebResponse;
            }
            else
            {
                httpWebResponse = "";
                return httpWebResponse;
            }

        }
        public void SetIsSuccessfulLogin(bool _isSuccessfulLogin, string _httpWebRequest)
        {
            successfulLogin = _isSuccessfulLogin;
            httpWebResponse = _httpWebRequest;
        }

    }
    public partial class Bugstar
    {
        public CookieContainer cookieContainer;
        public enum method { GET, POST }
        public XMLOperators XMLOps = new XMLOperators();
        public RegexOperatorCommands regOps = new RegexOperatorCommands();
        public PerforceOperators perfOps = new PerforceOperators();
        public string storedUsername = "";
        public string storedPassword = "";
        public string storedxmlFile = "";
        public List<Bug> storedBuglist = new List<Bug>();
        public static List<System.Windows.Controls.Button> localBugStarButtonList = new List<System.Windows.Controls.Button>();
        public static System.Windows.Controls.PasswordBox localPasswordBox = new System.Windows.Controls.PasswordBox();
        public static List<System.Windows.Controls.TextBox> localUsernameBoxList = new List<System.Windows.Controls.TextBox>();
        public static System.Windows.Controls.TextBox additionalCLs = new System.Windows.Controls.TextBox();

        private static SecurityProtocolType currentSPT = SecurityProtocolType.Tls | SecurityProtocolType.Tls11| SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
        /// <summary>
        /// sets UI elements from reference 
        /// </summary>
        /// <param name="bugstarButtonList"></param>
        /// <param name="passwordBox"></param>
        /// <param name="usernameBoxList"></param>
        /// <param name="AdditionalCLs"></param>
        public void SetUIElements(List<System.Windows.Controls.Button> bugstarButtonList, System.Windows.Controls.PasswordBox passwordBox, List<System.Windows.Controls.TextBox> usernameBoxList, System.Windows.Controls.TextBox AdditionalCLs)
        {
            additionalCLs = AdditionalCLs;
            localBugStarButtonList = bugstarButtonList;
            localPasswordBox = passwordBox;
            localUsernameBoxList = usernameBoxList;

        }
        /// <summary>
        /// creates a cookie container 
        /// </summary>
        /// <param name="uri">uri for the cookie source (B* url ref in settings.xml)</param>
        /// <param name="_method">method of cookie (get etc)</param>
        /// <param name="contentType">content type definition</param>
        /// <param name="setCookie">bool to see if this is a check or set cookie.</param>
        /// <returns>web request containing cookie container</returns>
        public HttpWebRequest SetCookieContainer(Uri uri, method _method, string contentType,bool setCookie = true)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.CreateDefault(uri);
            if (contentType != "")
            {
                httpWebRequest.ContentType = contentType;
            }
            httpWebRequest.Method = _method.ToString();
            if (setCookie)
            {
                httpWebRequest.CookieContainer = cookieContainer;
            }
            return httpWebRequest;
        }
        /// <summary>
        /// checks the cookie for authentication 
        /// </summary>
        /// <param name="uri">uri for the cookie source (B* url ref in settings.xml)</param>
        /// <param name="_method">method of cookie (get etc)</param>
        /// <param name="contentType">content type definition</param>
        /// <param name="cookieContainer">cookie to compareand authenticate</param>
        /// <returns></returns>
        public HttpWebRequest TestCookie (Uri uri, method _method, string contentType, CookieContainer cookieContainer)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uri);
            httpWebRequest.CookieContainer = cookieContainer;
            if (contentType != "")
            {
                httpWebRequest.ContentType = contentType;
            }
            httpWebRequest.Method = _method.ToString();
            foreach (Cookie StoredCookie in cookieContainer.GetCookies(uri))
            {
                foreach (Cookie webRequestCookie in httpWebRequest.CookieContainer.GetCookies(uri))
                {
                    if (StoredCookie.Value == webRequestCookie.Value)
                    {
                        Console.WriteLine("value is equal");
                    }
                    else
                    {
                        Console.WriteLine("value is not equal");
                    }                
                }
            }
            return httpWebRequest;

        }
        /// <summary>
        /// legacy login for passing password and generating cookie authentication
        /// </summary>
        /// <param name="httpwebrequest">webrequest for parsing</param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Login IsLegacyLoginSuccessful(HttpWebRequest httpwebrequest, string username, string password)
        {
			string postLoginResponseString = "";
            Login successfulLogin = new Login(false, "");
            try
            {
                Byte[] _outBuffer = System.Text.Encoding.ASCII.GetBytes("j_username=" + username + "%40rockstar.t2.corp&j_password=" + password);  //store in byte buffer  
                httpwebrequest.Timeout = 10000;
                Stream _str = httpwebrequest.GetRequestStream();
                _str.Write(_outBuffer, 0, _outBuffer.Length); //update form
                _str.Close();

                using (HttpWebResponse PostLoginResponse = (HttpWebResponse)httpwebrequest.GetResponse())
                { // get the request response (should say if we get an login error "login.jsp?login_error=1")
                    if (PostLoginResponse.ResponseUri.ToString().Contains("login_error=1"))
                    {
                        successfulLogin.SetIsSuccessfulLogin(false, PostLoginResponse.ResponseUri.ToString());
                        postLoginResponseString = PostLoginResponse.ResponseUri.ToString();
                        httpwebrequest.Abort();
                        return successfulLogin;
                    }
                    else
                    {
                        successfulLogin.SetIsSuccessfulLogin(true, PostLoginResponse.ResponseUri.ToString());
                        postLoginResponseString = PostLoginResponse.ResponseUri.ToString();
                        return successfulLogin;
                    }
                }
            }
            catch (WebException wex)
            {
                System.Windows.MessageBox.Show(wex.Status.ToString());
                successfulLogin.SetIsSuccessfulLogin(false, postLoginResponseString);
                return successfulLogin;
            }
        }
        /// <summary>
        /// bug class definition
        /// </summary>
        public enum BugClassEnum { A, B, C, D, TASK, TODO, WISH, TRACK };
        
        /// <summary>
        /// check if legacy login was successfully processed via web request 
        /// </summary>
        /// <param name="httpWebRequest">parsed web request from login</param>
        /// <returns></returns>
        public bool IsLoginSuccessful(HttpWebRequest httpWebRequest)
        {
            httpWebRequest.Timeout = 5000;
            using (HttpWebResponse getPrjResponse = (HttpWebResponse)httpWebRequest.GetResponse()) { 
                if (getPrjResponse.ResponseUri.AbsolutePath.Contains(@"login_error=1"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// cookie authentication 
        /// </summary>
        /// <param name="UriStr">uri to check cookie against</param>
        /// <param name="xmlFile">settings.xml contains cookie reference for B* cookie reference</param>
        /// <returns>true if cookie is ok false if not</returns>
        public bool IsCookieStillValid(Uri UriStr, string xmlFile)
        {
            bool isValid = true;
            try{
                CookieContainer oldCookieContainer = new CookieContainer();
                cookieContainer = new CookieContainer();
                Cookie oldCookie = new Cookie();
                oldCookie.Domain = XMLOps.GetElementFromXMLFile("BugstarSessionDomain", "Setup", xmlFile);
                oldCookie.Name = XMLOps.GetElementFromXMLFile("BugstarSessionName", "Setup", xmlFile);
                oldCookie.Value = XMLOps.GetElementFromXMLFile("BugstarSessionValue", "Setup", xmlFile);
                oldCookie.Path = XMLOps.GetElementFromXMLFile("BugstarSessionPath", "Setup", xmlFile);
                oldCookieContainer.Add(oldCookie);
                cookieContainer = oldCookieContainer;
                HttpWebRequest cookieTest = TestCookie(UriStr, method.GET, "", cookieContainer);
                cookieTest.Timeout = 5000;
                using (WebResponse webResponse =  cookieTest.GetResponse())
                {
                    if (webResponse.ResponseUri.ToString().Contains("login"))
                    {
                        isValid = false;
                    }
                }
            }
            catch (WebException wex)
            {
                Console.WriteLine("Error connecting to Bugstar Rest services, - CheckCookieIsStillValid " + wex.Message.ToString());
                isValid = false;
            }
            return isValid;

        }
        /// <summary>
        /// sets project for B* reference 
        /// </summary>
        /// <param name="httpProjectGet"></param>
        /// <param name="xmlFile"></param>
        public void SetBugstarProjectInXML(HttpWebResponse httpProjectGet, string xmlFile)
        {
            Stream responsestream = httpProjectGet.GetResponseStream();
            StreamReader str = new StreamReader(responsestream);
            List<ElementData> elementdataList = new List<ElementData>();
            elementdataList = XMLOps.GetListOfChildrenAndAttributesfromParent("project", str);
            string ToolsProject = XMLOps.GetElementFromXMLFile("ToolsProject", "Setup", xmlFile);
            string BugstarProject = "";
            string BugstarProjectID = "";
            if (ToolsProject.Contains(BugstarProjects.gta5.ToString()))
            {
                BugstarProject = "GTA 5 DLC";
            }
            if (ToolsProject.Contains(BugstarProjects.rdr3.ToString()))
            {
                BugstarProject = "Redemption2";
            }
            if (ToolsProject.Contains(BugstarProjects.americas.ToString()))
            {
                BugstarProject = "Americas";
            }
            foreach(ElementData ed in elementdataList)
            {
                if (ed.data == BugstarProject)
                {
                    BugstarProjectID = ed.element;
                }
            }
            str.Close();
            XMLOps.WriteStringToElement("BugstarProject","Setup",BugstarProject,xmlFile);
            XMLOps.WriteStringToElement("BugstarProjectID","Setup",BugstarProjectID,xmlFile);
        }

        public enum BugstarProjects {rdr3,americas,gta5}
        /// <summary>
        /// login to bugstar
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="uriStr">B* rest uri</param>
        /// <param name="xmlFile">settings.xml</param>
        /// <returns></returns>
        public bool BugstarLogin(string username, string password, Uri uriStr, string xmlFile)
        {
            storedxmlFile = xmlFile;
            storedUsername = username;
            storedPassword = password;
            cookieContainer = new CookieContainer();
            ServicePointManager.SecurityProtocol = currentSPT;
            try
            {
                if (IsCookieStillValid(new Uri(uriStr + "rest/Projects"),xmlFile)==false)
                {
                    cookieContainer = new CookieContainer();
                    HttpWebRequest httpProjectGet = SetCookieContainer(new Uri(uriStr + "rest/Projects"), method.GET, "", true);//Getting a temporary Cookie 
                    HttpWebRequest httpPost = SetCookieContainer(new Uri(uriStr + "j_spring_security_check"), method.POST, "application/x-www-form-urlencoded");//correcting setting cookie to POST Request
                    Login isLegacyLoginSuccessful = IsLegacyLoginSuccessful(httpPost, storedUsername, password);//Setting up login using POST Request
                    string LegacyLoginResponce = isLegacyLoginSuccessful.GetWebRequest();
                    httpPost.Timeout = 5000;
                    if (LegacyLoginResponce.Contains("login_error=1") == false)
                    {
                        foreach (Cookie c in httpProjectGet.CookieContainer.GetCookies(httpProjectGet.RequestUri))
                        {
                            XMLOps.WriteStringToElement("BugstarSessionDomain", "Setup", c.Domain.ToString(), xmlFile);
                            XMLOps.WriteStringToElement("BugstarSessionName", "Setup", c.Name.ToString(), xmlFile);
                            XMLOps.WriteStringToElement("BugstarSessionValue", "Setup", c.Value.ToString(), xmlFile);
                            XMLOps.WriteStringToElement("BugstarSessionPath", "Setup", c.Path.ToString(), xmlFile);
                        }
                        using (HttpWebResponse response = (HttpWebResponse)httpProjectGet.GetResponse())
                        {
                            SetBugstarProjectInXML(response,xmlFile);
                        }
                        return IsLoginSuccessful(SetCookieContainer(new Uri(uriStr + "rest/Projects/1546/CurrentUser"), method.GET, ""));

                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Username or Password Incorrect");
                        

                        foreach (System.Windows.Controls.Button b in localBugStarButtonList) 
                        {
                            if (b.Name.Contains("Bugstar_Report"))
                            {
                                b.IsEnabled = false;
                            }
                            else
                            {
                                b.IsEnabled = true ;
                            }
                        }
                        localPasswordBox.IsEnabled = true;
                        foreach (System.Windows.Controls.TextBox tb in localUsernameBoxList) { tb.IsEnabled = true; }
                        return false;
                    }
                }

                else
                {
                    return true;
                }
            }
            catch (WebException wex)
            {
                System.Windows.MessageBox.Show("There has been an error connecting to the bugstar rest servers \n Error: " + wex.Message.ToString());
                return false;
            }
        }

        /// <summary>
        /// cookie refresh
        /// </summary>
        /// <param name="uriStr">b* rest interface</param>
        /// <returns></returns>
        public bool KeepCookieAlive(Uri uriStr)
        {
            bool refreshedCookie = false;
            try
            {
                HttpWebRequest httpProjectGet = SetCookieContainer(new Uri(uriStr + "rest/Projects"), method.GET, "");
                refreshedCookie = true;
            }
            catch (WebException wex)
            {
                System.Windows.MessageBox.Show(wex.Status.ToString());
                LoginToBugstar(storedxmlFile);
                refreshedCookie = false;
            }
            return refreshedCookie;
        }
        /// <summary>
        /// b* login request 
        /// </summary>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public bool LoginToBugstar(string xmlFile)
        {
            return BugstarLogin(storedUsername, storedPassword,new Uri(XMLOps.GetElementFromXMLFile("BugstarURL", "Setup", @"" + xmlFile)), xmlFile);
        }

        /// <summary>
        /// regex buttons between
        /// </summary>
        /// <param name="DistroLocation"></param>
        /// <param name="ChangeListOne"></param>
        /// <param name="ChangeListTwo"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public List<string> GetBugNumbersBetweenTwoCLs(string DistroLocation, string ChangeListOne, string ChangeListTwo, string RegexPattern)
        {
            List<string> CLInformation = new List<string>();
            if (ChangeListOne != ChangeListTwo) {
                CLInformation = regOps.RegexTextAndReturnList(RegexPattern, perfOps.GetChangesBetweenTwoCLs(DistroLocation, Int32.Parse(ChangeListOne), Int32.Parse(ChangeListTwo)));
            }
            return CLInformation;
        }
        /// <summary>
        /// retrieve bug nubers from additional cls 
        /// </summary>
        /// <param name="AdditionalCLs">cls from mainmenu</param>
        /// <param name="CLRegexPattern">regex for cl pattern</param>
        /// <param name="BugRegexPattern">bug regex pattern </param>
        /// <returns></returns>
        public List<string> GetBugNumbersFromAdditionalCLs(string AdditionalCLs,string CLRegexPattern, string BugRegexPattern)
        {
            List<string> bugNumbers = new List<string>();
            List<string> CLNumbers = new List<string>();
            CLNumbers = regOps.RegexTextAndReturnList(CLRegexPattern, AdditionalCLs);
            foreach (string clNumber in CLNumbers)
            {
                bugNumbers.AddRange(regOps.RegexTextAndReturnList(BugRegexPattern, perfOps.GetChangesFromOneCL(clNumber)));
            }
            return bugNumbers;
        }
        /// <summary>
        /// gets the cl description for each cl between the two commited cl numbers within a specific branch and extracts bug numbers from the description.
        /// </summary>
        /// <param name="xmlFile">settings.xml</param>
        /// <param name="additionalCLs">addingtion cl numbers</param>
        /// <returns></returns>
        public List<string> GetBugNumbersFromOldCLVsNewCLLists(string xmlFile,string additionalCLs)
        {
            StringBuilder teststring = new StringBuilder();
            List<ElementData> ListOfElementsAndDataCurrentCL = new List<ElementData>();
            List<ElementData> ListOfElementsAndDataOLDCL = new List<ElementData>();
            List<string> ListOfEnvironmentBranches = new List<string>();
            ListOfElementsAndDataCurrentCL = XMLOps.GetListOfChildrenAndDatafromParent("CurrentCLData", xmlFile);
            ListOfElementsAndDataOLDCL = XMLOps.GetListOfChildrenAndDatafromParent("OldCLData", xmlFile);
            ListOfEnvironmentBranches = XMLOps.GetListOfChildrenfromParent("Environment", xmlFile);
            List<string> stringList = new List<string>();

            int i=0;
            foreach (ElementData elementData in ListOfElementsAndDataCurrentCL)
            {
                if (elementData.element == ListOfElementsAndDataOLDCL[i].element)
                {
                    foreach (string environment in ListOfEnvironmentBranches)
                    {
                        string environmentConversion="";
                        switch(environment){
                            case "CodeRoot":
                                environmentConversion = "Source";
                                break;
                            case "BuildRoot":
                                environmentConversion = "Data";
                                break;
                            case "ScriptRoot":
                                environmentConversion = "Script";
                                break;
                            case "RageRoot":
                                environmentConversion = "Rage";
                                break;
                            case "TextRoot":
                                environmentConversion = "Text";
                                break;
                            default:
                                break;
                        }

                        if (elementData.element.ToString() == environmentConversion)
                        {
                            stringList.AddRange(GetBugNumbersBetweenTwoCLs(XMLOps.GetElementFromXMLFile(environment, "Environment", xmlFile), elementData.data.ToString(), ListOfElementsAndDataOLDCL[i].data, XMLOps.GetElementFromXMLFile("BugstarRegexPattern", "Setup", xmlFile)));
                            break;
                        }
                    }
                }
                i++;                
            }
            if (additionalCLs != "")
            {
                stringList.AddRange(GetBugNumbersFromAdditionalCLs(additionalCLs, XMLOps.GetElementFromXMLFile("CLRegexPattern", "Setup", xmlFile), XMLOps.GetElementFromXMLFile("BugstarRegexPattern", "Setup", xmlFile)));
            }
            return stringList;
        }

        /// <summary>
        /// gets the bug info by passing the CL numbers via xml to B* rest interface
        /// </summary>
        /// <param name="xmlList"></param>
        /// <param name="postUri"></param>
        /// <param name="projects"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public List<Bug> GetBugInfoViaXML(string xmlList, Uri postUri, string projects,string xmlFile)
        {
            String BugDetails = "";
            //make sure the bugnumber is valid first
            //send the info to the rest serivice so that the cookie can get authenticated.
            List<Bug> Buglist = new List<Bug>();
            if (IsCookieStillValid(new Uri(postUri + "rest/Projects"), xmlFile))
            {
                BugDetails = RetrieveBugInformation(xmlList, postUri, projects, xmlFile);
            }
            else
            {
                if (LoginToBugstar(xmlFile)) { 
                    BugDetails = RetrieveBugInformation(xmlList, postUri, projects, xmlFile);
                }
            }
            Buglist = XMLOps.GetFieldValuesfromXMLList(BugDetails, Buglist, XMLOps.GetElementFromXMLFile("BugstarStatusFilter", "Setup", xmlFile));
            Console.WriteLine("Buglist Created");
            storedBuglist = Buglist;
            return Buglist;
        }
        /// <summary>
        /// gets bug info from individual cl numbers
        /// </summary>
        /// <param name="xmlList"></param>
        /// <param name="postUri"></param>
        /// <param name="projects"></param>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public string RetrieveBugInformation(string xmlList, Uri postUri, string projects, string xmlFile)
        {
            String BugDetails = "";
            List<Bug> Buglist = new List<Bug>();
            HttpWebRequest httpPost = SetCookieContainer(new Uri(postUri.OriginalString + "rest/Bugs/BugsById?fields=Id,Summary,Category,Project,State"), method.POST, "application/xml");
            byte[] postData = Encoding.UTF8.GetBytes(xmlList);
            httpPost.ContentLength = postData.Length;
            httpPost.Timeout = 10000;

            try
            {
                Stream dataStream = httpPost.GetRequestStream();
                dataStream.Write(postData, 0, postData.Length);
                dataStream.Close();
                using (HttpWebResponse PostLoginResponse = (HttpWebResponse)httpPost.GetResponse())
                {
                    if (PostLoginResponse.ResponseUri.OriginalString.Contains("login"))
                    {
                        if (LoginToBugstar(xmlFile))
                        {
                            HttpWebResponse getBugResponse = (HttpWebResponse)httpPost.GetResponse();
                            HttpStatusCode responseStatusCode = getBugResponse.StatusCode;
                            //get the bug info from the html stream coming from rest urlresponse.
                            Stream BugStream = getBugResponse.GetResponseStream();
                            StreamReader str = new StreamReader(BugStream);
                            // write info to string
                            BugDetails = str.ReadToEnd();
                            str.Close();
                        }
                    }
                    else
                    {
                        HttpWebResponse getBugResponse = (HttpWebResponse)httpPost.GetResponse();
                        HttpStatusCode responseStatusCode = getBugResponse.StatusCode;
                        //get the bug info from the html stream coming from rest urlresponse.
                        Stream BugStream = getBugResponse.GetResponseStream();
                        StreamReader str = new StreamReader(BugStream);
                        // write info to string
                        BugDetails = @""+str.ReadToEnd();
                        str.Close();
                    }
                }
            }
            catch (WebException wex)
            {
                System.Windows.MessageBox.Show("There has been an error connecting to the bugstar rest servers \n Error: " + wex.Status.ToString());
            }
            httpPost.Abort();
            return BugDetails;
        }
        /// <summary>
        /// returns sorted buglist for reference in other parts of code.
        /// </summary>
        /// <returns></returns>
        public List<Bug> retrieveBuglist()
        {
            return storedBuglist;
        }


        
    }


}
