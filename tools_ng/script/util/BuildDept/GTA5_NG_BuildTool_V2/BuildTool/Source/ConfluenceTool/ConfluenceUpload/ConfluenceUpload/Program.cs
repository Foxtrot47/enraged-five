﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConfluenceAPI;
using Newtonsoft.Json.Linq;

namespace ConfluenceUpload
{
    public static class Globals
    {
        public static string changeListPrefix = "* ";

    }
    class Program
    {
        static void Main(string[] args)
        {
            releaseInfo curRelease = new releaseInfo();
            Confluence _confluence = null;

            curRelease = parseArgs(args);

            curRelease = populateReleaseInfo(curRelease);

            if (string.IsNullOrWhiteSpace(curRelease.buildInfopage))
            {
                ErrorMessage("Cannot find a supplied build info page for confluence use. -confluence 123456 to enter build page");
            }
            else if (string.IsNullOrWhiteSpace(curRelease.bugstarversion))
            {
                ErrorMessage("Cannot find a supplied bugstar version. -bugstarversion v12340dev_ng to enter bugstar");
            }
            else
            {
                string credential = "c3ZjcnNnY29uZmx1ZW5jZXdlYjpqdW5rLU5RZWhUUHE";
                bool isNewVersion = true;
                bool isChanged = false;
                _confluence = new Confluence(credential);
                _confluence.FillCookieJar();

                JArray attachmentInfo = _confluence.GetJsonAttachmentAsJArray(curRelease.buildInfopage, "BuildInfo.json");

                foreach(JToken jsonVersion in attachmentInfo)
                {
                    JObject jsonObject = (JObject)jsonVersion;

                    if(jsonObject.Property("bugstarVer").Value.ToString().ToLower() == curRelease.bugstarversion.ToLower())
                    {
                        isNewVersion = false;
                    }
                }

                if(isNewVersion)
                {
                    attachmentInfo.AddFirst(generateVersion(curRelease));
                    isChanged = true;
                }

                if(isChanged)
                {
                    _confluence.JsonToAttachment(curRelease.buildInfopage, "BuildInfo.json", attachmentInfo.ToString(), "application/json");
                }

            }
        }
        //get a trailing value behind an arg
        static string getArgValue(string[] args, string valueToReference)
        {
            string output = "";
            if (Array.IndexOf(args, valueToReference) < (args.Count() - 1))
            {
                output = args[Array.IndexOf(args, valueToReference) + 1];
            }

            return output;
        }
        /// <summary>
        /// Parse args for all potential options and construct the realise info object of the supplied info
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static releaseInfo parseArgs(string[] args)
        {
            releaseInfo newRelease = new releaseInfo();

            if (args.Length == 2)
            {
                newRelease.branch = args[0];
                newRelease.buildInfopage = args[1];
            }

            return newRelease;
        }
        static releaseInfo populateReleaseInfo(releaseInfo curRelease)
        {
            string versionNum = getEnteredValue("Version Number");

            curRelease.code = getEnteredValue("Code CL");
            curRelease.rage = getEnteredValue("Rage CL");
            curRelease.script = getEnteredValue("Script CL");
            curRelease.text = getEnteredValue("Text CL");
            curRelease.data = getEnteredValue("Data CL");
            curRelease.audio = getEnteredValue("Audio CL");

            string dateOfBuild = getEnteredValue("Date of build (- seperated e.g. 14-04-17)");
            if (!string.IsNullOrEmpty(dateOfBuild))
            {
                curRelease.buildname = "QA_" + dateOfBuild;
            }
            else
            {
                curRelease.buildname = "QA_" + DateTime.Now.ToString("dd-MM-yy");
            }

            curRelease.label = "GTA5_NG_version_" + versionNum + "-" + curRelease.branch;
            curRelease.description = "QA build: " + curRelease.branch + " branch";
            curRelease.bugstarversion = versionNum + "-" + curRelease.branch;
            return curRelease;
        }
        static string getEnteredValue(string valueToAsk)
        {
            InfoMessage(string.Format("Enter value for {0}", valueToAsk));
            string input = Console.ReadLine();
            return input.Trim().Replace("\r", string.Empty).Replace("\r", string.Empty);
        }

        static void ErrorMessage(string input)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine(input);
        }
        static void InfoMessage(string input)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(input);
        }

        static JObject generateVersion(releaseInfo newVersion)
        {
            JObject toReturn = new JObject();
            toReturn.Add(new JProperty("bugstarVer", newVersion.bugstarversion));
            toReturn.Add(new JProperty("build", newVersion.buildname));

            string changelists = "";

            if(!string.IsNullOrWhiteSpace(newVersion.code))
            {
                changelists = changelists + Globals.changeListPrefix + "Source: " + newVersion.code + "\n";
            }
            if (!string.IsNullOrWhiteSpace(newVersion.rage))
            {
                changelists = changelists + Globals.changeListPrefix + "Rage: " + newVersion.rage + "\n";
            }
            if (!string.IsNullOrWhiteSpace(newVersion.script))
            {
                changelists = changelists + Globals.changeListPrefix + "Script: " + newVersion.script + "\n";
            }
            if (!string.IsNullOrWhiteSpace(newVersion.text))
            {
                changelists = changelists + Globals.changeListPrefix + "Text: " + newVersion.text + "\n";
            }
            if (!string.IsNullOrWhiteSpace(newVersion.data))
            {
                changelists = changelists + Globals.changeListPrefix + "Data: " + newVersion.data + "\n";
            }
            if (!string.IsNullOrWhiteSpace(newVersion.audio))
            {
                changelists = changelists + Globals.changeListPrefix + "Audio: " + newVersion.audio + "\n";
            }

            toReturn.Add(new JProperty("changelists", changelists));


            toReturn.Add(new JProperty("label", newVersion.label));

            toReturn.Add(new JProperty("description", newVersion.description));

            

            return toReturn;
        }
    }

    public class releaseInfo
    {
        public string buildInfopage { get; set; }
        public string code { get; set; }
        public string script { get; set; }
        public string rage { get; set; }
        public string text { get; set; }
        public string data { get; set; }
        public string audio { get; set; }
        public string bugstarversion { get; set; }
        public string buildname { get; set; }
        public string label { get; set; }
        public string description { get; set; }

        public string branch { get; set; }
    }
}
