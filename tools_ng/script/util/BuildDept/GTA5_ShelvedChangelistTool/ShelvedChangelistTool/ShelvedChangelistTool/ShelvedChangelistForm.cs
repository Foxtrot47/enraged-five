﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices; //for the pushing messages from background threads
using System.Reflection; //for the embedded resource processing
using System.Security.Cryptography;

namespace ShelvedChangelistTool
{

    public partial class ShelvedChangelistForm : Form
    {
        #region Variables
        private List<string> addedFiles = new List<string>(); //list for storing files marked for add
        private List<string> unshelvedFiles = new List<string>(); //list for storing files unshelved
        private List<AssetInfo> assetsToBuild = new List<AssetInfo>(); //list for storing assets that need building
        private Dictionary<string, string> fileActions = new Dictionary<string, string>();
        private List<CopyFile> filesToCopy = new List<CopyFile>(); //List for storing files for copying and where they are
        private bool isCmd; //boolean to check whether command line args are being supplied
        private string[] changelists; //array to store changelist number
        private string[] types; //array to store which config to build
        private string[] formats; //array to store which console building for
        private string errorMsg; //string to store the error message to put in email
        private string additionalErrorMsg; //additional string to expand error message
        private bool errorDetected; //boolean to flag when a error has occurred
        private string user; //string for storing the username for use in email sending
        private string buildLogFullpath; //for storing folder + file to create a full path
        private string codeOutput; //string for storing the threaded console output
        private bool bool_Shaders = false; //bool flag to notify if shaders need to be built
        private string buildRebuild; //string to store whether to build or rebuild
        private string envProject; //String to store the envProject for use with the bugstar copying
        private string bStar; //string for the bugstar folder to use in copying
        private string syncLabel = ""; //string to be used to sync the label to
        private Studio thisStudio; //variable for storing studio information
        private List<string> preExistingFiles = new List<string>();
                
        currentState curState = currentState.none; //variable for storing which stage program is at for error checking
        
        enum currentState //enum of all of the states the build can be in
        {
            none,
            prebuild,
            syncing,
            unshelve,
            resolve,
            scripts,
            assets,
            shaders,
            build,
            cleanup,
            copying
        }

        private static string[] SYNCFOLDERS = {
            "\\src\\dev\\...@",
            "\\script\\dev\\...@",
            "\\assets\\export\\...@",
            "\\assets\\metadata\\...@",
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\...@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\ps4\\...@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\x64\\...@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\xboxone\\...@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\ps4\\...@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\game_win64*@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\game_orbis*@"),
            (Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\game_durango*@")
                                              };//global array for folder to sync this will get added to projectroot to work out what files to sync
        private static Dictionary<String, String> STUDIOABBREVIATIONS = new Dictionary<String, String>()
        {   {"rockstarnorth.com","rsgedi"},
            {"rockstarlondon.com", "rsgldn"},
            {"rockstargames.com", "rsgnyc"},
            {"rockstartoronto.com", "rsgtor"},
            {"rockstarsandiego.com", "rsgsan"},
            {"rockstarleeds.com", "rsglds"},
            {"rockstarlincoln.com", "rsglin"},
            {"rockstarnewengland.com", "rsgnwe"}
            }; //global dictionary for holding the studio abbreviations
        #endregion

        #region Constructors
        public ShelvedChangelistForm() //Constructor for non cmd line
        {
            InitializeComponent(); //Ronseal
            getEnviromentProj(); //calculate which project we are in form the environment variables
            updateTitlebar(); //update titlebar
            updateForm("build"); //update the side bar text to say build
            types = null; //set some starting variables
            formats = null;
            isCmd = false;
            createBuildLogs(); //create the build logs folder and file
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
        } 
        public ShelvedChangelistForm(string[] args) //constructor for cmdline
        {
            InitializeComponent(); //Ronseal
            getEnviromentProj(); //calculate which project we are working with
            isCmd = true; //set cmd flag so that all functions know what we are doing
            errorDetected = false; //set default errordetected variable
            this.Visible = false; //hide the gui as it is not needed
            createBuildLogs(); //create the build logs folder and file
            ParseArgs(args); //parse through the supplied args and populate all the variables from them
        }
        #endregion

        #region FunctionsUsedToStartBuild
        private void ShelvedChangelistForm_Load(object sender, EventArgs e) //check on load for isCmd if so start generating
        {
            if (isCmd)
            {
                if (syncLabel != "")
                {
                    syncCodeDirectories(syncLabel);
                }
                thisStudio = calculateStudio(user);
                mainRoutine();

                if (errorDetected)
                {
                    if ((additionalErrorMsg != null) || (additionalErrorMsg != ""))
                    {
                        errorMsg = errorMsg + "\r\n" + additionalErrorMsg;
                    }
                    sendErrorEmail(errorMsg); //This will send an error based email to the builder
                }
                if (!errorDetected)
                {
                    sendConfirmationEmail(); //This will send a confirmation email to the builder
                }

                if (errorDetected)
                {
                    Environment.ExitCode = 1;
                }
                Application.Exit();
            }
            else
            {
                try
                {
                    pb_DoWork.Hide();
                    fixCurrentWorkingDirectory();
                    this.FormBorderStyle = FormBorderStyle.FixedSingle;
                    if (envProject != "GTA5")
                    {
                        updateLabelOptions();
                    }
                    if (envProject == "GTA5")
                    {
                        this.tabRemoteControl.SelectedTab = this.tabLocal;
                    }
                    updateTitlebar();
                    displayPatchNotes();
                }
                catch
                {
                }
            }
        }
        private void b_GO_Click(object sender, EventArgs e) //function to generate code from button press (only triggers when not using command line)
        {
            resetVariables();
            createBuildLogs();
            pb_DoWork.Style = ProgressBarStyle.Marquee;
            pb_DoWork.MarqueeAnimationSpeed = 50;
            curState = currentState.none;
            pb_DoWork.Show();
            b_GO.Enabled = false;
            
            if (cb_rebuild.Checked)
            {
                buildRebuild = "-rebuild";
            }
            else
            {
                buildRebuild = "-build";
            }
            if (cb_shader.Checked)
            {
                bool_Shaders = true;
            }
            else
            {
                bool_Shaders = false;
            }
            
            if ((envProject != "GTA5") && (this.tabRemoteControl.SelectedTab == this.tabRemote))
            {
                if (this.tabSyncType.SelectedTab == this.tabLabel)
                {
                    if (this.lBox_syncLabel.SelectedItem == null)
                    {
                        MessageBox.Show("You need to select a label for the buildmachine to sync to.");
                        unlockUI();
                        return;
                    }
                }
                if (this.tabSyncType.SelectedTab == this.tabNumber)
                {
                    double test;
                    if ((this.tbSyncChangelist.Text == null) || (this.tbSyncChangelist.Text == "") || (!double.TryParse(this.tbSyncChangelist.Text, out test)))
                    {
                        MessageBox.Show("Changelists to sync to must be a number");
                        unlockUI();
                        return;
                    }
                }
            
                prebuildChecks();
                if (!errorDetected)
                {
                    generateConsoleFile();
                }
                if (!errorDetected)
                {
                    runConsole();
                }
                if (errorDetected)
                {
                    updateOutput("\r\n[error] An error was detected during the build please check the output.");
                }
            }
            else
            {
                mainRoutine(); //run the main steps through the unshelve and build procedure
            
                if (errorDetected)
                {
                    string errorText = "";
                    switch (curState)
                    {
                        case currentState.prebuild:
                            updateOutput("\r\n=====ERROR ON PREBUILD=====");
                            errorText = "An error has occured during prebuild checks";
                            break;
                        case currentState.unshelve:
                            updateOutput("\r\n=====ERROR WHILST UNSHELVING=====");
                            errorText = "An error has occured during unshelving";
                            break;
                        case currentState.shaders:
                            updateOutput("\r\n====ERROR WHILST BUILDING SHADERS====");
                            errorText = "An error has occured whilst building the shaders";
                            break;
                        case currentState.build:
                            updateOutput("\r\n=====ERROR WHILST BUILDING=====");
                            errorText = "An error has occured during building";
                            break;
                        case currentState.resolve:
                            updateOutput("\r\n=====ERROR WHILST RESOLVING=====");
                            errorText = "An error has occured during resolving";
                            break;
                        case currentState.scripts:
                            updateOutput("\r\n=====ERROR WHILST BUILDING SCRIPTS=====");
                            errorText = "An error has occured during script building";
                            break;
                        case currentState.copying:
                            updateOutput("\r\n=====ERROR WHILST COPYING=====");
                            errorText = "An error has occured during copying";
                            break;
                        case currentState.cleanup:
                            updateOutput("\r\n=====ERROR WHILST REVERTING AND CLEARING MARKED FOR ADD=====");
                            errorText = "An error has occured during cleaning up";
                            break;
                        case currentState.none:
                            updateOutput("\r\n=====ERROR FROM THE VERY START=====");
                            errorText = "An error has occured before it even starts";
                            break;
                    }
            
                    MessageBox.Show(errorText);
                }
                updateOutput("\r\n=====================================================");
                if (!errorDetected)
                {
                    updateOutput("\r\n================BUILD SUCCESS=======================");
                    System.Media.SystemSounds.Exclamation.Play();
                }
                else
                {
                    updateOutput("\r\n=================BUILD FAILED========================");
                    System.Media.SystemSounds.Beep.Play();
                }
                updateOutput("\r\n=====================================================");
            }
            unlockUI();
        }
        #endregion

        #region MainRoutine
        private void prebuildChecks() //function to check for options to make sure they are selected
        {
            curState = currentState.prebuild;
            updateOutput("\r\n[BUILDSTEP-PREBUILD.STARTED]");
            if (isCmd)
            {
                if (changelists == null)
                {
                    errorMsg = "[error] An error has occured during prebuild whereby no changelists were supplied. Did you supply the command line -cl 12345678,12345677,12345676 etc?";
                    updateOutput(errorMsg);
                    errorDetected = true;
                }
                if (types == null)
                {
                    errorMsg = "[error] An error has occured during prebuild whereby no code types were supplied. Did you supply the command line -types Beta,Release etc?";
                    updateOutput(errorMsg);
                    errorDetected = true;
                }
                if (formats == null)
                {
                    errorMsg = "[error] An error has occured during prebuild whereby no formats were supplied. Did you supply the command line -formats Orbis,Durango etc?";
                    updateOutput(errorMsg);
                    errorDetected = true;
                }
                if ((user == "") || (user == null))
                {
                    errorMsg = "[error] An error has occured during prebuild whereby no user was supplied.";
                    updateOutput(errorMsg);
                    errorDetected = true;
                }
                if ((bStar == "") || (bStar == null))
                {
                    errorMsg = "[error] An error has occured during prebuild whereby no copy location was supplied.";
                    updateOutput(errorMsg);
                    errorDetected = true;
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(tb_Changelist.Text))
                {
                    MessageBox.Show("[error] Please enter at least one changelist.");
                    errorDetected = true;
                }
                changelists = tb_Changelist.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                var chkBoxesCode = gb_Exes.Controls.OfType<CheckBox>().Select(c => c.Checked);
                var chkBoxesType = gb_Format.Controls.OfType<CheckBox>().Select(t => t.Checked);
                if (chkBoxesCode.Contains(true) && (chkBoxesType.Contains(true)))
                {
                    //do nothing
                }
                else if ((chkBoxesType.Contains(true)) && (cb_shader.Checked))
                {
                    MessageBox.Show("No Exe type Selected will just built shaders for selected platform");
                }
                else if (chkBoxesCode.Contains(true))
                {
                    MessageBox.Show("[error] No format selected");
                    errorDetected = true;
                }
                else if (chkBoxesType.Contains(true))
                {
                    MessageBox.Show("[error] No code build selected");
                    errorDetected = true;
                }
                else
                {
                    MessageBox.Show("[error] Neither code build or format selected");
                    errorDetected = true;
                }
                if (!validateChangelists())
                {
                    errorDetected = true;
                }
            }
            updateOutput("\r\n[BUILDSTEP-PREBUILD.FINISHED]");
        }
        private void unshelveFile() //function to unshelve files from a given changelist
        {
            //set state
            curState = currentState.unshelve;
            updateOutput("\r\n[BUILDSTEP-UNSHELVE.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            if (!string.IsNullOrWhiteSpace(tb_Changelist.Text) || (isCmd))
            {
                foreach (string c in changelists)
                {
                    //start new process for running the command lines to follow
                    Process p4Unshelve = new Process();
                    //Create start info to be populated shortly
                    ProcessStartInfo p4StartInfo = new ProcessStartInfo();

                    //Override standard start info with made startinfo
                    p4Unshelve.StartInfo = getStartInfo("cmd.exe");
                    //start process
                    p4Unshelve.Start();
                    //create msg to send to text box
                    string msgToGet = "Attempting to get changelist " + c + "\n";
                    //send msg to text box
                    updateOutput(msgToGet);


                    //Create overriden input and output
                    StreamWriter p4Input = p4Unshelve.StandardInput;
             
                    //start using overriden input
                    using (p4Input = p4Unshelve.StandardInput)
                    {
                            //make string for command line unshelve and push to input
                            string textToUnshelve = "p4 unshelve -s " + c;
                            p4Input.WriteLine(textToUnshelve);
                    }
                    //close stream (must be neat unt tidy ^_^)
                    p4Input.Close();
                    //write output to text box after storing (for regexing)
                    string output = p4Unshelve.StandardOutput.ReadToEnd();
                    
                    updateOutput(output);

                    //continue being neat unt tidy
                    p4Unshelve.WaitForExit();
                    p4Unshelve.Close();

                    //check output for unshelved if not found then nothing was unshelved
                    string regexFail = @"(.*)#.*unshelved.*";
                    MatchCollection matchFails = checkOutputForCollection(output, regexFail);
                    if (matchFails.Count == 0)
                    {
                        if (isCmd)
                        {
                            errorMsg = "[error]An error has occured during unshelving. One of the supplied changelists contain no files to unshelve.";
                        }
                        updateOutput("\r\n[error] no files in shelved changelist [error]");
                        errorDetected = true;
                    }
                    else
                    {
                        string num = "\r\nTotal files unshelved: " + matchFails.Count.ToString() + "\n";
                        updateOutput(num);
                        foreach (Match m in matchFails)
                        {
                            unshelvedFiles.Add(m.Groups[1].Value);
                            string regexAdd = @"(.*)#.*unshelved.*add";
                            Match add = checkOutputFor(m.ToString(), regexAdd);
                            if (add.Success)
                            {
                                addedFiles.Add(add.Groups[1].Value);
                            }

                        }
                    }
                }
            }
            else
            {
                if (!isCmd)
                {
                    MessageBox.Show("[error] No Changelists entered");
                }
                errorDetected = true;
            }
            updateOutput("\r\n[BUILDSTEP-UNSHELVE.FINISHED]");
            stopStopWatch(ref stpwatch);
            
        }
        private void resolveChangelist() //function to resolve files
        {
            //set state
            curState = currentState.resolve;
            updateOutput("\r\n[BUILDSTEP-RESOLVE.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            //create msg to send to text box
            string msgToResolve = "Resolving unshelved\n";
            //send msg to text box
            updateOutput(msgToResolve);
            foreach (string file in unshelvedFiles)
            {
                //start new process for running the command lines to follow
                Process p4Resolve = new Process();

                //Override standard start info with made startinfo
                p4Resolve.StartInfo = getStartInfo("cmd.exe");
                p4Resolve.StartInfo.RedirectStandardError = true;
                //start process
                p4Resolve.Start();

                //Create overriden input and output
                StreamWriter p4Input = p4Resolve.StandardInput;

                //start using overriden input
                using (p4Input = p4Resolve.StandardInput)
                {
                    bool fileAdded = false;
                    foreach (string aFile in addedFiles)
                    {
                        if (aFile == file)
                        {
                            fileAdded = true;
                        }
                    }
                    if (fileAdded == false)
                    {
                        string textToResolve = calculateResolveAction(file);
                        p4Input.WriteLine(textToResolve);
                    }
                }
                while (!p4Resolve.StandardOutput.EndOfStream)
                {
                    string line = p4Resolve.StandardOutput.ReadLine();
                    Match p4LineChk = checkOutputFor(line, "(p4 resolve -ay.*)");
                    if (p4LineChk.Success)
                    {
                        updateOutput("\r\n " + p4LineChk.Groups[1].Value);
                    }
                }
                string[] stdErrors = p4Resolve.StandardError.ReadToEnd().Split('\n');

                foreach (string stdError in stdErrors)
                {
                    if (!String.IsNullOrWhiteSpace(stdError))
                    {
                        Match stdErrorMatch = checkOutputFor(stdError, @".*- no file.* to resolve");
                        if (!stdErrorMatch.Success)
                        {
                            updateOutput(("[error]:" + stdError));
                            errorDetected = true;
                        }
                    }
                }
                //close stream (must be neat unt tidy ^_^)
                p4Input.Close();
                //write output to text box after storing (for regexing)
                string output = p4Resolve.StandardOutput.ReadToEnd();

                updateOutput(output);

                //continue being neat unt tidy
                p4Resolve.WaitForExit();
                p4Resolve.Close();
            }
            updateOutput("\r\n[BUILDSTEP-RESOLVE.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void buildShaders() //function to start shader background worker
        {
            if (bool_Shaders)
            {
                updateOutput("\r\n[BUILDSTEP-SHADERS.STARTED]");
                curState = currentState.shaders;
                Stopwatch stpwatch = Stopwatch.StartNew();
                ShaderInfo sdrInfo = new ShaderInfo();
                sdrInfo.shaderDir = Environment.GetEnvironmentVariable("RS_CODEBRANCH") + @"\game\shader_source\VS_Project\batch\";
                if (isCmd)
                {
                    foreach (string f in formats)
                    {                       
                        sdrInfo.format = f;
                        if (buildRebuild == "-rebuild")//set build type depending on whether rebuild is selected
                        {
                            sdrInfo.type = "rebuild";
                        }
                        else
                        {
                            sdrInfo.type = "build";
                        }
                        string updateTxt = "\r\n" + sdrInfo.type + "ing shaders for " + f;
                        updateOutput(updateTxt);
                        updateOutput("\r\nShader info: " + sdrInfo.shaderDir + "," + sdrInfo.type + "," + sdrInfo.format);
                        bgWorkerShader.RunWorkerAsync(sdrInfo);
                        do
                        {
                            Application.DoEvents();
                        } while (bgWorkerShader.IsBusy == true);
                    }
                }
                else
                {
                    var chkBoxFormat = gb_Format.Controls.OfType<CheckBox>();

                    foreach (var cf in chkBoxFormat)
                    {
                        if (cf.Checked)
                        {
                            sdrInfo.format = cf.Text;
                            if (buildRebuild == "-rebuild")//set build type depending on whether rebuild is selected
                            {
                                sdrInfo.type = "rebuild";
                            }
                            else
                            {
                                sdrInfo.type = "build";
                            }
                            string updateTxt = "\r\n" + sdrInfo.type + "ing shaders for " + cf.Text;
                            updateOutput(updateTxt);
                            updateOutput("\r\nShader info: " + sdrInfo.shaderDir + "," + sdrInfo.type + "," + sdrInfo.format);
                            bgWorkerShader.RunWorkerAsync(sdrInfo);
                            int step = 1;
                            do
                            {
                                step = holdingPattern("Building Shaders", step);
                                Application.DoEvents();
                                System.Threading.Thread.Sleep(200);
                            } while (bgWorkerShader.IsBusy == true);
                        }
                    }
                }
                
                updateOutput("\r\n[BUILDSTEP-SHADERS.FINISHED]");
                stopStopWatch(ref stpwatch);
            }
            
        } 
        private void buildCode() //function to build code TEMP build only orbis beta currently
        {
            if (types.Contains("NoTypesSelected"))
            {
                return;
            }
            curState = currentState.build;
            updateOutput("\r\n[BUILDSTEP-BUILDING.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            string environment = "dev_ng";
            string solution = Environment.GetEnvironmentVariable("RS_CODEBRANCH")+@"\game\VS_Project\game_2015_unity.sln";
            string type = "";
            if (buildRebuild == "-rebuild")
            {
                type = "/rebuild";
            }
            else
            {
                type = "/build";
            }
            string config = "Beta";
            string platform = "Orbis";
            string buildBranch = Environment.GetEnvironmentVariable("RS_BUILDBRANCH");
            if (isCmd)
            {
                CodeInfo cdInfo = new CodeInfo();
                cdInfo.env = environment;
                cdInfo.sol = solution;
                cdInfo.type = "/clean";
                cdInfo.config = config;
                cdInfo.platform = platform;
                cdInfo.runNum = 1;
                bgWorkerCode.RunWorkerAsync(cdInfo);

                do
                {
                    Application.DoEvents();
                } while (bgWorkerCode.IsBusy);

                for (int i = 0; i < types.Length; i++)
                {
                    for (int j = 0; j < formats.Length; j++)
                    {
                        config = types[i];
                        platform = formats[j];

                        cdInfo = new CodeInfo();
                        cdInfo.env = environment;
                        cdInfo.sol = solution;
                        cdInfo.type = type;
                        cdInfo.config = config;
                        cdInfo.platform = platform;
                        cdInfo.runNum = 1;                        
                        bgWorkerCode.RunWorkerAsync(cdInfo);
                        
                        do
                        {
                            Application.DoEvents();
                        } while (bgWorkerCode.IsBusy);
                        cdInfo.runNum = 2;
                        cdInfo.type = "/build";
                        bgWorkerCode.RunWorkerAsync(cdInfo);
                                                
                        do
                        {
                            Application.DoEvents();
                        } while (bgWorkerCode.IsBusy);
                        calculateFiles(cdInfo, buildBranch);
                    }
                }

            }
            else
            {
                var chkBox = gb_Exes.Controls.OfType<CheckBox>();
                var chkBoxFormat = gb_Format.Controls.OfType<CheckBox>();

                foreach (var cf in chkBoxFormat)
                {
                    if (cf.Checked)
                    {
                        platform = cf.Text;
                        foreach (var cb in chkBox)
                        {
                            if (cb.Checked)
                            {

                                config = cb.Text;
                                CodeInfo cdInfo = new CodeInfo();
                                cdInfo.env = environment;
                                cdInfo.sol = solution;
                                cdInfo.type = type;
                                cdInfo.config = config;
                                cdInfo.platform = platform;
                                cdInfo.runNum = 1;
                               
                                bgWorkerCode.RunWorkerAsync(cdInfo);
                                int step = 1;
                                do
                                {
                                    step = holdingPattern("Building Code", step);
                                    Application.DoEvents();
                                    System.Threading.Thread.Sleep(200);
                                    
                                } while (bgWorkerCode.IsBusy);
                                cdInfo.runNum = 2;
                                cdInfo.type = "/build"; //Always build second time never rebuild
                                bgWorkerCode.RunWorkerAsync(cdInfo);
                                                                
                                do
                                {
                                    step = holdingPattern("Building Code", step);
                                    Application.DoEvents();
                                    System.Threading.Thread.Sleep(200);
                                    
                                } while (bgWorkerCode.IsBusy);
                                calculateFiles(cdInfo, buildBranch);
                            }
                        }
                    }
                }
            }
            updateOutput("\r\n[BUILDSTEP-BUILDING.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void cleanupChangelist() //function to revert files and delete local added files
        {
            updateOutput("\r\n[BUILDSTEP-CLEANUP.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            if (unshelvedFiles.Count > 0) //check if there is anyfiles to cleanup if not skip
            {
                foreach (string uFile in unshelvedFiles)
                {
                    //start new process for running the command lines to follow
                    Process p4Cleanup = new Process();

                    //Override standard start info with made startinfo
                    p4Cleanup.StartInfo = getStartInfo("cmd.exe");
                    //start process
                    p4Cleanup.Start();

                    //Create overriden input and output
                    StreamWriter p4Input = p4Cleanup.StandardInput;
                    
                    //start using overriden input
                    using (p4Input = p4Cleanup.StandardInput)
                    {

                            string textToRevert = "p4 revert " + uFile;
                            p4Input.WriteLine(textToRevert);
                    }

                    //close stream (must be neat unt tidy ^_^)
                    p4Input.Close();
                    //write output to text box after storing (for regexing)
                    string output = p4Cleanup.StandardOutput.ReadToEnd();

                    updateOutput(output);

                    //continue being neat unt tidy
                    p4Cleanup.WaitForExit();
                    p4Cleanup.Close();

                    foreach (string aFile in addedFiles)
                    {

                        string temp = "";
                        if (envProject == "RDR3")
                        {
                            temp = aFile.Replace("//rdr3/", "X:\\rdr3\\");
                        }
                        else if (envProject == "Americas")
                        {
                            temp = aFile.Replace("//americas/", "X:\\americas\\");
                        }
                        else
                        {
                            temp = aFile.Replace("//depot/", "X:\\");
                        }
                        string final = temp.Replace("/", "\\");
                        if (File.Exists(final))
                        {
                            string txt = "\r\nDeleting " + final;
                            updateOutput(txt);
                            File.Delete(final);
                        }
                    }
                }
            }
            updateOutput("\r\n[BUILDSTEP-CLEANUP.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void copyBuiltExes() //function to copy the built exes to [TEMP]
        {
            curState = currentState.copying;
            if (filesToCopy.Count == 0)
            {
                return;
            }
            updateOutput("\r\n[BUILDSTEP-COPYING.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            
            if ((bStar != "")&&(bStar != null)) //if not running command line skip copying
            {
                bgWorkerCopy.RunWorkerAsync(); //Start background worker for copying the built exes etc.
                int step = 1;
                do
                {
                    step = holdingPattern("Copying Files", step); //update titlebar to animate whilst waiting
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(50);
                } while (bgWorkerCopy.IsBusy == true);
            }
            updateOutput("\r\n[BUILDSTEP-COPYING.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void buildScripts() //function to detect whether script file is unshelved and build scripts if needed
        {
            curState = currentState.scripts;
            bool scripts = false;
            foreach (string uF in unshelvedFiles)
            {
                Match containsScript = checkOutputFor(uF, @"\/script\/.*\.sc.*");
                if (containsScript.Success)
                {
                    scripts = true;
                }
            }

            if (scripts)
            {
                updateOutput("\r\n[BUILDSTEP-SCRIPTS.STARTED]");
                bgWorkerScriptBuilder.RunWorkerAsync();
                int step = 1;
                do
                {
                    step = holdingPattern("Building Scripts", step);
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(200);
                } while (bgWorkerScriptBuilder.IsBusy == true);

                updateOutput("\r\n[BUILDSTEP-SCRIPTS.FINISHED]");
            }
        }
        private void buildAssets()
        {
            curState = currentState.assets;
            foreach (string uF in unshelvedFiles)
            {
                Match containsLang = checkOutputFor(uF, @"\/assets.*\/export\/data\/lang\/.*");
                if (containsLang.Success)
                {
                    string langFolder = new DirectoryInfo(convertAssetP4FileToLocal(uF)).Parent.FullName;

                    assetsToBuild.Add(new AssetInfo(langFolder, "--recursive --rebuild"));
                    continue;
                }
                Match containsAsset = checkOutputFor(uF, @"\/assets.*\/export\/data\/.*\.meta");
                if (containsAsset.Success)
                {
                    assetsToBuild.Add(new AssetInfo(convertAssetP4FileToLocal(uF), "--rebuild"));
                }

            }
            if (assetsToBuild.Count > 0)
            {
                updateOutput("\r\n[BUILDSTEP-ASSETBUILD.STARTED]");
                bgWorkerAssetBuilder.RunWorkerAsync();

                do
                {
                    Application.DoEvents();
                } while (bgWorkerAssetBuilder.IsBusy);
                updateOutput("\r\n[BUILDSTEP-ASSETBUILD.FINISHED]");
            }
        }
        private void cleanupWorkspace()
        {
            curState = currentState.cleanup;
            updateOutput("\r\n[BUILDSTEP-WORKSPACECLEANUP.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            foreach(CopyFile fTC in filesToCopy)
            {
                updateOutput("Current from location: " + fTC.fromLocation + " filename: " + fTC.filename);
                if (syncLabel != "")
                {
                    string fileForCleanup = "";
                    //if((fTC.fromLocation.EndsWith(@"\")) ||(fTC.filename.StartsWith(@"\")))
                    //{
                        fileForCleanup = fstatGetDepotLocation(System.IO.Path.Combine(fTC.fromLocation,fTC.filename));
                    //}
                    //else
                    //{
                    //    fileForCleanup = fstatGetDepotLocation(fTC.fromLocation + "\\" + fTC.filename);
                    //}
                    if (fileForCleanup == "")
                    {
                        updateOutput(("\r\n[error]: Could not find file " + fTC.fromLocation + fTC.filename + " with fstat"));
                    }
                    else
                    {
                        //start new process for running the command lines to follow
                        Process p4Cleanup = new Process();
                        //Create start info to be populated shortly
                        ProcessStartInfo p4StartInfo = new ProcessStartInfo();
                        
                        //Override standard start info with made startinfo
                        p4Cleanup.StartInfo = getStartInfo("cmd.exe");
                        p4Cleanup.StartInfo.RedirectStandardError = true;
                        //start process
                        p4Cleanup.Start();

                        //Create overriden input and output
                        StreamWriter p4Input = p4Cleanup.StandardInput;

                        //start using overriden input
                        using (p4Input = p4Cleanup.StandardInput)
                        {
                            //make string for command line unshelve and push to input
                            string textToCleanup = "p4 sync -f " + fileForCleanup + "@" + syncLabel;
                            p4Input.WriteLine(textToCleanup);
                        }
                        //close stream (must be neat unt tidy ^_^)
                        p4Input.Close();

                        //write output to text box after storing (for regexing)
                        string output = p4Cleanup.StandardOutput.ReadToEnd();

                        string[] errors = p4Cleanup.StandardError.ReadToEnd().Split('\n');
                        foreach (string e in errors)
                        {
                            if (!String.IsNullOrWhiteSpace(e))
                            {
                                Match noFile = checkOutputFor(e, @"- no such file");
                                if (!noFile.Success)
                                {
                                    updateOutput("[error]" + e);
                                }
                            }
                        }
                        updateOutput(output);

                        //continue being neat unt tidy
                        p4Cleanup.WaitForExit();
                        p4Cleanup.Close();
                    }
                }
            }
            updateOutput("\r\n[BUILDSTEP-WORKSPACECLEANUP.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void mainRoutine() //function which runs through all the functions in order
        {
            //set bool for use of checking each step for success
            if (!errorDetected)
            {
                prebuildChecks();
            }
            if (isCmd)
            {
                populatePreExistingFiles();
            }
            //progress through the unshelve and build process checking bool each step
            if (!errorDetected)
            {
                unshelveFile(); //run unshelve and store outcome
            }
            if (!errorDetected)
            {
                resolveChangelist();
            }
            if (!errorDetected)
            {
                buildScripts();
            }
            if (envProject == "RDR3")
            {
                if (!errorDetected)
                {
                    buildAssets();
                }
            }
            if ((!errorDetected) && (isCmd))
            {
                buildVcproj();
            }
            //if ((!errorDetected) && (isCmd))
            //{
            //    buildParsers();
            //}
            if (!errorDetected)
            {
                buildShaders();
                if (!isCmd)
                {
                    this.Text = "Shaders Complete";
                }
            }
            if (!errorDetected)
            {
                buildCode(); //run build and store outcome
                if (!isCmd)
                {
                    this.Text = "Code Complete";
                }
            }
            if (!errorDetected)
            {
                copyBuiltExes(); //copy the exes and store outcome
            }
            if (isCmd)
            {
                cleanupWorkspace();
            }
            copyLogs(); //always copy the logs
            cleanupChangelist();//always run cleanup
            if (isCmd)
            {
                validateExistingFiles();
            }
        }
        private void copyLogs() //function to copy the built exes to [TEMP]
        {
            curState = currentState.copying;
            filesToCopy = new List<CopyFile>();
            string buildLogFolder = Environment.GetEnvironmentVariable("RS_TOOLSROOT"); //calculate by project where the build log folder is going to be
            if ((envProject == "RDR3") || (envProject == "Americas"))
            {
                buildLogFolder = buildLogFolder + @"\script\util\BuildDept\RDR_BuildTools\BuildTool\SupportingFiles\ShelvedChangelist\";
            }
            else
            {
                buildLogFolder = buildLogFolder + @"\script\util\BuildDept\GTA5_ShelvedChangelistTool\Buildlog\";
            }
            filesToCopy.Add(new CopyFile(buildLogFolder, "Buildlog.txt"));
            updateOutput("\r\n[BUILDSTEP-COPYLOGS.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();

            if ((bStar != "") && (bStar != null)) //if not running command line skip copying
            {
                bgWorkerCopy.RunWorkerAsync(); //Start background worker for copying the built exes etc.
                int step = 1;
                do
                {
                    step = holdingPattern("Copying Logs", step); //update titlebar to animate whilst waiting
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(50);
                } while (bgWorkerCopy.IsBusy == true);
            }
            updateOutput("\r\n[BUILDSTEP-COPYLOGS.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void buildParsers()
        {
            updateOutput("\r\n[BUILDSTEP-PARSERS.STARTED]");
            Process buildParsers = new Process();
            buildParsers.StartInfo = getStartInfo("X:\\rdr3\\tools\\release\\script\\coding\\rebuildparser.bat");
            buildParsers.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\script\\coding";


            buildParsers.Start();
            string output = "";
            while (!buildParsers.StandardOutput.EndOfStream)
            {
                string line = buildParsers.StandardOutput.ReadLine();
                //regex for wanted lines
                output += line + "\r\n";
                Match mchComplete = checkOutputFor(line, @"p4 edit (.*)", true);
                Match mchAdded = checkOutputFor(line, @"p4 add (.*)", true);
                if (mchComplete.Success)
                {
                    unshelvedFiles.Add(mchComplete.Groups[1].Value);
                }
                if (mchAdded.Success)
                {
                    unshelvedFiles.Add(mchAdded.Groups[1].Value);
                    addedFiles.Add(mchAdded.Groups[1].Value);
                }
                updateOutput("[Parsers] " + line);
                Application.DoEvents();
            }

            buildParsers.WaitForExit();
            buildParsers.Close();

            updateOutput(output);
            updateOutput("\r\n[BUILDSTEP-PARSERS.FINISHED]");
        }
        private void buildVcproj()
        {
            updateOutput("\r\n[BUILDSTEP-VCPROJ.STARTED]");
            Process buildVcproj = new Process();
            buildVcproj.StartInfo = getStartInfo("X:\\rdr3\\src\\dev\\game\\rebuild_game_vcprojs.bat  ");
            buildVcproj.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_CODEBRANCH") + "\\game";
            buildVcproj.StartInfo.RedirectStandardError = true;


            buildVcproj.Start();
            string output = "";
            while (!buildVcproj.StandardOutput.EndOfStream)
            {
                string line = buildVcproj.StandardOutput.ReadLine();
                //regex for wanted lines
                output += line + "\r\n";
                Match mchComplete = checkOutputFor(line, @"p4 edit (.*)", true);
                if (mchComplete.Success)
                {
                    unshelvedFiles.Add(mchComplete.Groups[1].Value);
                }
                Match mchAdded = checkOutputFor(line, @"p4 add (.*)", true);
                if (mchAdded.Success)
                {
                    unshelvedFiles.Add(mchAdded.Groups[1].Value);
                    addedFiles.Add(mchAdded.Groups[1].Value);
                }

                updateOutput(line);
                Application.DoEvents();
            }
            string[] stdErrors = buildVcproj.StandardError.ReadToEnd().Split('\n');

            foreach (string stdError in stdErrors)
            {
                if (!String.IsNullOrWhiteSpace(stdError))
                {
                    Match stdErrorMatch1 = checkOutputFor(stdError, @"is not on head revision have");
                    Match stdErrorMatch2 = checkOutputFor(stdError, @"Unity compilation unit error");
                    Match stdErrorMatch3 = checkOutputFor(stdError, @"exiting with exit code 1");
                    if ((!stdErrorMatch1.Success) && (!stdErrorMatch2.Success) && (!stdErrorMatch3.Success))
                    {
                        updateOutput(("[error]:" + stdError));
                        errorDetected = true;
                    }
                }
            }

            buildVcproj.WaitForExit();
            buildVcproj.Close();

            updateOutput(output);
            updateOutput("\r\n[BUILDSTEP-VCPROJ.FINISHED]");
        }
        #endregion

        #region MiscFunctions
        private void syncCodeDirectories(string labelWanted) //sync the code directories in SYNCFOLDERS to a supplied label
        {
            //set state
            curState = currentState.syncing;
            updateOutput("\r\n[BUILDSTEP-SYNCING.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            for (int i = 0; i < SYNCFOLDERS.Length; i++)
            {
                updateOutput("Syncing " + SYNCFOLDERS[i]);
                //start new process for running the command lines to follow
                Process p4Sync = new Process();
                //Create start info to be populated shortly
                ProcessStartInfo p4StartInfo = new ProcessStartInfo();
                string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");

                //Override standard start info with made startinfo
                p4Sync.StartInfo = getStartInfo("cmd.exe");
                p4Sync.StartInfo.RedirectStandardError = true;
                //start process
                p4Sync.Start();

                //Create overriden input
                StreamWriter p4Input = p4Sync.StandardInput;

                //start using overriden input
                using (p4Input = p4Sync.StandardInput)
                {
                    //make string for command line syncs and push to input
                    string textToSync = "";
                    if (SYNCFOLDERS[i].Replace('/', '\\').StartsWith(projectroot.Replace('/', '\\')))
                    {
                        textToSync = "p4 sync " + SYNCFOLDERS[i] + labelWanted;
                    }
                    else
                    {
                        textToSync = "p4 sync " + projectroot + SYNCFOLDERS[i] + labelWanted;
                    }
                    p4Input.WriteLine(textToSync);
                }
                //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
                p4Input.Close();
                //write output to text box after storing (for regexing)
                string output = p4Sync.StandardOutput.ReadToEnd();
                string[] stdErrors = p4Sync.StandardError.ReadToEnd().Split('\n');

                //continue being neat unt tidy
                p4Sync.WaitForExit();
                p4Sync.Close();

                updateOutput(output);

                foreach (string stdError in stdErrors)
                {
                    if (!String.IsNullOrWhiteSpace(stdError))
                    {
                        Match stdErrorMatch = checkOutputFor(stdError, @".*@.* - file\(s\) up-to-date");
                        if (!stdErrorMatch.Success)
                        {
                            updateOutput(("[error]:" + stdError));
                            errorDetected = true;
                        }
                    }
                }
                
            }
            updateOutput("\r\n[BUILDSTEP-SYNCING.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void buildSingleCode(string enviro, string sol, string codetype, string conf, string plat, int tryNum) //function to build code
        {
            codeOutput = "";
            //start new process for running the command lines to follow
            Process buildExes = new Process();
            //Override standard start info with made startinfo
            buildExes.StartInfo = getStartInfo("cmd.exe");
           
            
            //start process
            buildExes.Start();
            string envOrbissdk = "";
            string nativeenv = "";
            if (envProject == "GTA5")
            {
                string envString = Environment.GetEnvironmentVariable("SCE_ROOT_DIR") + @"\ORBIS SDKs\1.700";
                string progString = Environment.GetEnvironmentVariable("PROGRAMFILES(X86)")+@"SCE\ORBIS SDKs\1.700";
                if (Directory.Exists(envString))
                {
                    envOrbissdk = "/SETENV=\"SCE_ORBIS_SDK_DIR="+envString+"\" ";
                }
                else if (Directory.Exists(progString))
                {
                    envOrbissdk = "/SETENV=\"SCE_RSCE_ORBIS_SDK_DIROOT_DIR="+progString+"\" ";
                }
                else if (Directory.Exists(@"C:\Program Files (x86)\SCE\ORBIS SDKs\1.700"))
                {
                    envOrbissdk = "/SETENV=\"SCE_ORBIS_SDK_DIR=C:\\Program Files (x86)\\SCE\\ORBIS SDKs\\1.700\" ";
                }
                else if (Directory.Exists(@"C:\Orbis\1.700"))
                {
                    envOrbissdk = "/SETENV=\"SCE_ORBIS_SDK_DIR=C:\\Orbis\\1.700\" ";
                }
                else
                {
                    errorDetected = true;
                    updateOutput("[error]: You appear to be trying to build GTA5 but do not have orbis sdk 1.7 installed");
                    return;
                }
            }

            if (envProject != "GTA5")
            {
                nativeenv = "/SETENV=_IsNativeEnvironment=true ";
            }
            string rsBuildbranch = Environment.GetEnvironmentVariable("RS_BUILDBRANCH");
            string processstring = "BuildConsole.exe /SETENV=" + rsBuildbranch + " " + envOrbissdk + nativeenv + sol + " " + codetype + " /cfg=\"" + conf + "|" + plat + "\" /msbuildargs=\"/nodeReuse:false\" /usemsbuild";
            //Create overriden input and output
            StreamWriter exeInput = buildExes.StandardInput;            
            if (tryNum == 2)
            {
                updateOutput("\r\nStarting second build");
            }
            else
            {
                updateOutput("\r\nStarting First build");
            }
        
            //start using overriden input
            using (exeInput = buildExes.StandardInput)
            {
                
                string message = "\r\nBeginning building of "+plat+" with config "+conf+"\r\n";
                updateOutput(message);
                //make string for command line unshelve and push to input
                exeInput.WriteLine(processstring);
            }

            //write output to text box after storing (for regexing)

            while (!buildExes.StandardOutput.EndOfStream)
            {
                string line = buildExes.StandardOutput.ReadLine();
                if (tryNum == 1)
                {
                    line = line + "\r\n";
                }
                if (tryNum == 2)
                {
                    string stringChk = @"(error : .*)";
                    Match chkOutput = checkOutputFor(line, stringChk);
                    if (chkOutput.Success)
                    {
                        errorMsg = errorMsg + "<br>" + chkOutput.Groups[1].Value;
                        errorDetected = true;
                    }
                    line = line + "\r\n";
                    codeOutput += line;
                }
                Match chkEdit = checkOutputFor(line, @"p4 edit (.*)");
                if (chkEdit.Success)
                {
                    unshelvedFiles.Add(chkEdit.Groups[1].Value);
                }
                Match chkAdd = checkOutputFor(line, @"p4 add (.*)");
                if (chkAdd.Success)
                {
                    unshelvedFiles.Add(chkAdd.Groups[1].Value);
                    addedFiles.Add(chkAdd.Groups[1].Value);
                }
                updateOutput(line);
            }
                       
            //continue being neat unt tidy
            buildExes.WaitForExit();
                      
            if (tryNum == 2)
            {

                string stringChk = @"[1-9] Error\(s\)";
                Match chkOutput = checkOutputFor(codeOutput, stringChk);
                if (chkOutput.Success)
                {
                    errorMsg = errorMsg + "<br>[error] From output of second code build failures were detected.";
                    errorDetected = true;
                }
                string successString = @"Build succeeded";
                Match successChk = checkOutputFor(codeOutput, successString);
                Match noErrorsChk = checkOutputFor(codeOutput, @"0 Error\(s\)");
                if ((!successChk.Success)&&(!successChk.Success))
                {
                    errorMsg = errorMsg+"<br>[error] From output of second code build game did not compile correctly";
                    errorDetected = true;
                }
            }
            exeInput.Close();            
            buildExes.Close();
            
        }
        private void buildSingleShader(string command, bool dev) //function to run supplied shader bat file and parse output for failures
        {
            Process buildShaders = new Process();
            buildShaders.StartInfo = getStartInfo(command);
            buildShaders.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_CODEBRANCH") + "\\game\\shader_source\\VS_Project\\batch";
            if (dev)
            {
                buildShaders.StartInfo.Arguments += " dev";
            }
            buildShaders.Start();

            while (!buildShaders.StandardOutput.EndOfStream)
            {
                string line = buildShaders.StandardOutput.ReadLine();
                //regex for wanted lines
                
                Match mchComplete = checkOutputFor(line, @"Completed .*", true);
                
                Match errChk = checkOutputFor(line, @"error .*", true);
                //append to make it all on seperate lines
                line = "\r\n" + line;
                if (mchComplete.Success)
                {
                    updateOutput(line);
                }
                if (errChk.Success)
                {
                    errorDetected = true;
                    updateOutput(("\r\n[error] " + line));
                    additionalErrorMsg += line;
                }
                Application.DoEvents();
            }

            buildShaders.WaitForExit();
            buildShaders.Close();
        }
        private Match checkOutputFor(string output, string checkString) //return a match from a supplied regex string
        {
            Regex checkForFail = new Regex(checkString);
            Match failChk = checkForFail.Match(output);
            
            return failChk;
        }
        private Match checkOutputFor(string output, string checkString, bool ignorecase) //return a match from a supplied regex string
        {
            Regex checkForFail = new Regex("");
            if (ignorecase)
            {
                checkForFail = new Regex(checkString, RegexOptions.IgnoreCase);
            }
            else
            {
                checkForFail = new Regex(checkString);
            }
            Match failChk = checkForFail.Match(output);
            return failChk;
        }
        private MatchCollection checkOutputForCollection(string output, string checkString) //return a collection of matches from a supplied regex string
        {
            Regex regexFail = new Regex(checkString);
            MatchCollection matchFails = regexFail.Matches(output);

            return matchFails;
        }
        private ProcessStartInfo getStartInfo(string process) //return a process start info used by all processes
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();

            //Popular start info with cmd.exe and overriding standard input and output
            startInfo.FileName = process;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
           
            return startInfo;
        } 
        private void sendErrorEmail(string errMsg) //Function to send a high importance email containing the build error message
        {
            updateOutput("[BUILDSTEP-Sending email] Started");
            string subject = "Error during your shelve build of changelist " + changelists[0];
            string body = "<p>Hi,</p><br><p>An error appears to have been found whilst building your shelf build<br>Log file should be located " + @"url:N:\projects\" + envProject + @"\GameLogs\" + thisStudio.studioAbr + @"\" + bStar + "</p><table border=\"1\" cellspacing=\"1\" cellpadding=\"10\" width=\"100%\"><tr valign=\"TOP\"><td align=\"left\">" + errMsg + "</b></td></tr>";
            string contact = thisStudio.email;
            if (contact != "")
            {
                sendEmail(contact, subject, body, MailPriority.High);
            }
            updateOutput("[BUILDSTEP-Sending email] Finished");
        }
        private void sendConfirmationEmail() //Function to send a normal importance email containing the build success and details
        {
            updateOutput("[BUILDSTEP-Sending email] Started");
            string subject = "Build of changelist " + changelists[0] + " is now finished.";
            //string body = "Hi there,\r\nYour build has been completed and is located " + @"url:N:\projects\" + envProject + @"\GameLogs\" + thisStudio.studioAbr +@"\" + bStar;
            string body = "<p>Hi,</p><br><p>Your build has finished and is located <b>" + @"url:N:\projects\" + envProject + @"\GameLogs\" + thisStudio.studioAbr + @"\" + bStar + "</b></p><br><p>Regards</p><p>Friendly Neighbourhood Builder</p>";
            string contact = thisStudio.email;
            if (contact != "")
            {
                sendEmail(contact, subject, body, MailPriority.Normal);
            }
            updateOutput("[BUILDSTEP-Sending email] Finished");
        } 
        private void sendEmail(string contact, string subject, string body, MailPriority importance) //construct an email and send it
        {
            updateOutput(("Contact is: " + contact));
            MailMessage message = new System.Net.Mail.MailMessage();
            MailAddress frm = new MailAddress("DoNotReply@rockstarnorth.com");
            message.From = frm;
            message.To.Add(contact);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            message.Priority = importance;

            using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.rockstar.t2.corp"))
            {
                smtp.UseDefaultCredentials = true;
                smtp.Send(message);
            } 
        }
        private void updateOutput(string updateTxt) //function to update txt and output tb if not in cmd mode
        {
            updateTxt = pruneOutput(updateTxt);
            if (isCmd)
            {
                Match errChk = checkOutputFor(updateTxt, @"\[error\].*");
                if (errChk.Success)
                {
                    Console.Error.WriteLine(updateTxt);
                }
                else
                {
                    Console.WriteLine(updateTxt);
                }

            }
            else
            {
                if (tb_output.InvokeRequired) //if not in ui-thread
                {
                    this.Invoke(new Action<string>(updateOutput), new object[] { updateTxt });
                    return;
                }
                tb_output.AppendText(updateTxt); //if not using command line display to text box
            }

            using (StreamWriter updater = File.AppendText(buildLogFullpath))
            {
                updater.Write(updateTxt); //add to file the text log
            }
                        
        }
        private void updateOutput(string updateTxt, bool newLine) //function to update txt and output tb if not in cmd mode
        {
            updateTxt = pruneOutput(updateTxt);
            updateTxt += "\r\n";
            if (isCmd)
            {
                Console.WriteLine(updateTxt);
            }
            else
            {
                if (tb_output.InvokeRequired) //if not in ui-thread
                {
                    this.BeginInvoke(new Action<string>(updateOutput), new object[] { updateTxt });
                    return;
                }
                tb_output.AppendText(updateTxt); //if not using command line display to text box
            }

            using (StreamWriter updater = File.AppendText(buildLogFullpath))
            {
                updater.Write(updateTxt); //add to file the text log
            }

        }   
        private void stopStopWatch(ref Stopwatch watch) //function to stop a stopwatch then output the elapsed time
        {
            string elapsed = watch.Elapsed.ToString();
            watch.Stop();
            elapsed = "\r\nTime taken: " + elapsed + "\r\n";
            updateOutput(elapsed);
        }
        private void updateForm(string buildType)//redraw side label depending on rebuild or build
        {
            string updateTxt = "";
            if (buildType == "rebuild")
            {
                updateTxt = "R\r\nE\r\nB\r\nU\r\nI\r\nL\r\nD"; //change text to rebuild

            }
            else
            {
                updateTxt = "\r\nB\r\nU\r\nI\r\nL\r\nD"; //change text to build

            }
            lb_build.Text = updateTxt;
            if (envProject != "GTA5")
            {
                this.tabRemoteControl.Visible = true;
            }
        }
        private void ParseArgs(string[] args) //Function to parse through input args when using commandline args
        {
            for (int i = 0; i < args.Length; i++)
            {
                int j = i + 1;
                if (args[i] == "-cl")
                {
                    try
                    {

                        Match numChk = checkOutputFor(args[j], @"(.*\d)");
                        if (numChk.Success)
                        {
                            changelists = args[j].Split(',');
                        }
                        else
                        {
                            throw new InvalidOperationException("-cl is not followed by a changelist number");
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                else if (args[i] == "-types")
                {
                    try
                    {

                        Match typeChk = checkOutputFor(args[j], @"(Beta|Release|Debug|BankRelease|NoTypesSelected)");
                        if (typeChk.Success)
                        {
                            types = args[j].Split(',');
                        }
                        else
                        {
                            throw new InvalidOperationException("-Types is not followed by Beta, Release, Debug or BankRelease");
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                else if (args[i] == "-formats")
                {
                    try
                    {
                        Match formChk = checkOutputFor(args[j], @"(Orbis|Durango|X64)");
                        if (formChk.Success)
                        {
                            formats = args[j].Split(',');
                        }
                        else
                        {
                            throw new InvalidOperationException("-formats is not followed by Orbis, Durango or Win64");
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                else if (args[i] == "-user")
                {
                    try
                    {

                        Match userChk = checkOutputFor(args[j], @"(-.*)");
                        if (!userChk.Success)
                        {
                            user = args[j];
                        }
                        else
                        {
                            throw new InvalidOperationException("-user is not followed by username");
                        }
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        additionalErrorMsg = e.Message;
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                else if (args[i] == "-bugstar")
                {
                    try
                    {

                        Match bstarChk = checkOutputFor(args[j], @"(.* .*)");
                        if (!bstarChk.Success)
                        {
                            bStar = args[j];
                        }
                        else
                        {
                            throw new InvalidOperationException("-bugstar the folder contains a whitespace");
                        }
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        additionalErrorMsg = e.Message;
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                else if (args[i] == "-rebuild")
                {
                    buildRebuild = args[i];
                }
                else if (args[i] == "-build")
                {
                    buildRebuild = args[i];
                }
                else if (args[i] == "-shaders")
                {
                    bool_Shaders = true;
                }
                else if (args[i] == "-synclabel")
                {
                    try
                    {

                        Match labelChk = checkOutputFor(args[j], @"(-.*)");
                        if (!labelChk.Success)
                        {
                            syncLabel = args[j];
                        }
                        else
                        {
                            throw new InvalidOperationException("-synclabel is not followed by a label");
                        }
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        additionalErrorMsg = e.Message;
                        errorDetected = true;
                    }
                    catch (InvalidOperationException e)
                    {
                        errorDetected = true;
                        additionalErrorMsg = e.Message;
                    }
                }
                if (errorDetected)
                {
                    errorMsg = "The command line args detected do not fit the correct format";
                    for (int k = 0; k < args.Length; k++)
                    {
                        errorMsg += " " + args[k];
                    }
                    updateOutput(errorMsg);
                }
            }
        }
        private int holdingPattern(string stepString,int step) //function to iterate the fullstops on the titlebar of the form
        {
            string newString = stepString;
            for (int i = 0; i < step; i++)
            {
                newString = newString + ".";
            }
            
            this.Text = newString;
            if (step > 5)
            {
                step = 1;
            }
            else
            {
                step+=1;
            }
            return step;
        }
        private void createBuildLogs() //Function to create folder and files for build log file
        {
            string buildLogFolder = Environment.GetEnvironmentVariable("RS_TOOLSROOT"); //calculate by project where the build log folder is going to be
            try//check for folder to put build log in. If it does exist create then try and make/overwrite the file.
            {
                if ((envProject == "RDR3") || (envProject == "Americas"))
                {
                    buildLogFolder = buildLogFolder + @"\script\util\BuildDept\RDR_BuildTools\BuildTool\SupportingFiles\ShelvedChangelist\";
                }
                else
                {
                    buildLogFolder = buildLogFolder + @"\script\util\BuildDept\GTA5_ShelvedChangelistTool\Buildlog\";
                }

                buildLogFullpath = buildLogFolder + @"Buildlog.txt";
                if (!Directory.Exists(buildLogFolder))
                {
                    Directory.CreateDirectory(buildLogFolder);
                }
                File.WriteAllText(buildLogFullpath, string.Empty);
            }
            catch (DirectoryNotFoundException) //Folder doesnt exist for it to create file in 
            {
                errorDetected = true;
                errorMsg = "Directory not found for the build log file to be made in. Attempted to create file in " + buildLogFolder;
            }
        }
        private void getEnviromentProj() //Work out envProject variable from environment variable
        {
            switch (Environment.GetEnvironmentVariable("RS_PROJECT"))
            {
                case ("gta5"):
                    envProject = "GTA5";
                    break;
                case ("rdr3"):
                    envProject = "RDR3";
                    break;
                case ("americas"):
                    envProject = "Americas";
                    break;
            }
        }
        private void calculateFiles(CodeInfo cdInfo,string buildBranch) //calculate files to add from a supplied codeinfo and build branch
        {
            if (!errorDetected) //if no error detected proceed to add files to copy
            {
                CopyFile aFile = new CopyFile();
                aFile.fromLocation = buildBranch;
                string genericFile = "";
                switch (cdInfo.platform)
                {
                    case ("X64"):
                        genericFile = @"game_win64_" + cdInfo.config.ToLower();
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".exe")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".cmp")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".map")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".pdb")));
                        break;
                    case ("Orbis"):
                        genericFile = @"game_orbis_" + cdInfo.config.ToLower();
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".elf")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".cmp")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".map")));
                        break;
                    case ("Durango"):
                        genericFile = @"game_durango_" + cdInfo.config.ToLower();
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".exe")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".cmp")));
                        filesToCopy.Add(new CopyFile(buildBranch, (genericFile + ".pdb")));
                        break;
                }
            }
        }
        private void updateTitlebar() //function to set the titlebar to default title with project added
        {
            string temp = "Shelved Changelist Tool " + envProject;

            if (envProject != "GTA5")
            {
                this.tabRemoteControl.Visible = true;
                if (this.tabRemoteControl.SelectedTab == this.tabLocal)
                {
                    temp = "LOCAL " + temp;
                }
                else
                {
                    temp = "REMOTE " + temp;
                }
            }
            this.Text = temp;
        }
        private void displayPatchNotes() //function to load in and display custommessagebox of the contents
        {
            var assembly = Assembly.GetExecutingAssembly();
            var embeddedTextFile = "ShelvedChangelistTool.changeHistory.txt";
            string contents = "";
            using (Stream embeddedStream = assembly.GetManifestResourceStream(embeddedTextFile))
            using (StreamReader embeddedReader = new StreamReader(embeddedStream))
            {
                contents = embeddedReader.ReadToEnd();
            }
            CustomMessageBox.Show("New to this version", contents);
        }
        private void resetVariables() //function to reset variables ready for another run
        {
            tb_output.Text = "";
            errorDetected = false;
            b_GO.Enabled = false;
            codeOutput = "";
            bStar = tb_bugstar.Text;
            addedFiles.Clear();
            unshelvedFiles.Clear();
            filesToCopy.Clear();
            fileActions.Clear();
        }
        private void cb_rebuild_CheckedChanged(object sender, EventArgs e) //upon changing rebuild checkbox update form
        {
            if (cb_rebuild.Checked)
            {
                updateForm("rebuild");
            }
            else
            {
                updateForm("build");
            }
        }
        private Studio calculateStudio(string username) //calculate the email and studio abbreviation from the username
        {

            //start new process for running the command lines to follow
            Process p4UserCalc = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();
            string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");
            //Override standard start info with made startinfo
            p4UserCalc.StartInfo = getStartInfo("cmd.exe");
            //start process
            p4UserCalc.Start();

            //Create overriden input
            StreamWriter p4Input = p4UserCalc.StandardInput;

            //start using overriden input
            using (p4Input = p4UserCalc.StandardInput)
            {
                //make string for command line syncs and push to input
                string userRetrieval = "p4 user -o " + username;
                p4Input.WriteLine(userRetrieval);

            }
            //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string output = p4UserCalc.StandardOutput.ReadToEnd();

            string email = parseOutputForUser(output);

            //continue being neat unt tidy
            p4UserCalc.WaitForExit();
            p4UserCalc.Close();


            Studio temp = new Studio();
            temp.email = email;
            temp.studioAbr = temp.calculateStudioAbr(temp.email, STUDIOABBREVIATIONS);
            return temp;

        }
        private string parseOutputForUser(string output) //parse the output of the perforce call p4 user -o for the email and return
        {
            string[] splitOutput = output.Split('\n');
            foreach (string sO in splitOutput)
            {
                Match splitOutputMatch = checkOutputFor(sO, @"Email:\s*(\S*@.*\.com)");
                if (splitOutputMatch.Success)
                {
                    return splitOutputMatch.Groups[1].Value;
                }
            }
            updateOutput("[error]: Email could not be found");
            return "";
        }
        private void updateLabelOptions() //get three most recent labels and populate the lBox_syncLabel
        {
            this.tabSyncType.Visible = true;

            List<String> labels = getLabels();
            for (int i = (labels.Count-1); i >= (labels.Count - 3); i--)
            {
                this.lBox_syncLabel.Items.Add(labels[i]);
            }
        }
        private List<String> getLabels() //get list of labels from p4 and return
        {
            List<String> labels = new List<String>();
            //start new process for running the command lines to follow
            Process p4Labels = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();
            string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");
            //Override standard start info with made startinfo
            p4Labels.StartInfo = getStartInfo("cmd.exe");
            //start process
            p4Labels.Start();
            
            //Create overriden input
            StreamWriter p4Input = p4Labels.StandardInput;
            
            //start using overriden input
            using (p4Input = p4Labels.StandardInput)
            {
                string textToSync = "";
                //make string for command line syncs and push to input
                textToSync = "p4 labels -e RDR3_DEV_VERSION*";
                p4Input.WriteLine(textToSync);
            }
            //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string output = p4Labels.StandardOutput.ReadToEnd();
            foreach (string o in output.Split('\n'))
            {
                Match labelMatch = checkOutputFor(o, @"Label (.*) [0-9]{4}");
                if (labelMatch.Success)
                {
                    labels.Add(labelMatch.Groups[1].Value);
                }
            }
            
            //continue being neat unt tidy
            p4Labels.WaitForExit();
            p4Labels.Close();

            return labels;
        }
        private void tabRemoteControl_Selected(object sender, TabControlEventArgs e) //Switch ui to show whether build will be remote or local"
        {
            updateTitlebar();
            if (this.tabRemoteControl.SelectedTab == this.tabLocal)
            {
                this.BackColor = Color.FromKnownColor(KnownColor.ControlLight);
                this.tabSyncType.Visible = false;
            }
            else
            {
                this.BackColor = Color.FromKnownColor(KnownColor.Control);
                this.tabSyncType.Visible = true;
            }
        }
        private void getLabelDetails(string chosenLabel) // DEBUG get details of label DEBUG
        {
            //start new process for running the command lines to follow
            Process p4Labels = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();
            string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");
            //Override standard start info with made startinfo
            p4Labels.StartInfo = getStartInfo("cmd.exe");
            //start process
            p4Labels.Start();

            //Create overriden input
            StreamWriter p4Input = p4Labels.StandardInput;

            //start using overriden input
            using (p4Input = p4Labels.StandardInput)
            {
                string textToSync = "";
                //make string for command line syncs and push to input
                textToSync = "p4 label -o " + chosenLabel;
                p4Input.WriteLine(textToSync);

            }
            //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string output = p4Labels.StandardOutput.ReadToEnd();
            foreach (string o in output.Split('\n'))
            {
                updateOutput(o);
            }

            //continue being neat unt tidy
            p4Labels.WaitForExit();
            p4Labels.Close();
        }
        private void lBox_syncLabel_SelectedIndexChanged(object sender, EventArgs e) //DEBUG Function to get label details upon selecting one from lBox DEBUG
        {
            //getLabelDetails((string)this.lBox_syncLabel.SelectedItem);
        }
        private void fixCurrentWorkingDirectory() //Check whether the current working directory is where the executing assembly is located if not set the current working 
        {
            string curDirectory = Directory.GetCurrentDirectory();
            string exeDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (curDirectory != exeDirectory)
            {
                Directory.SetCurrentDirectory(exeDirectory);
            }
        }
        private void unlockUI()  //Unlock locked ui elements
        {
            b_GO.Enabled = true;
            updateTitlebar();
            pb_DoWork.Style = ProgressBarStyle.Continuous;
            pb_DoWork.MarqueeAnimationSpeed = 0;
            pb_DoWork.Hide();
        }
        private string getHash(string fileName)
        {
            var md5 = MD5.Create();
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return Encoding.Default.GetString(md5.ComputeHash(stream));
            }
        }
        private List<FileDiff> getFileDiffs(List<string> toSearchFolders)
        {
            List<FileDiff> outDiffs = new List<FileDiff>();
            
            foreach (string tSF in toSearchFolders)
            {
                string[] fileToSearch = Directory.GetFiles(tSF, "*.fxc");
                foreach (string fTS in fileToSearch)
                {
                    outDiffs.Add(new FileDiff(getHash(fTS), fTS)); 
                }
            }

            return outDiffs;
        }
        private void calculateFileChanges(List<FileDiff> inList)
        {
            foreach (FileDiff iL in inList)
            {
                if (iL.before != getHash(iL.fileName))
                {
                    filesToCopy.Add(new CopyFile(Directory.GetParent(iL.fileName).FullName, new FileInfo(iL.fileName).Name, Directory.GetParent(iL.fileName).Name));
                }
            }
        }
        private bool validateChangelists()
        {
            SingleFileInfo details = new SingleFileInfo();
            if (this.tabRemoteControl.SelectedTab == this.tabRemote)
            {
                details.remote = true;
            }
            else
            {
                details.remote = false;
            }
            bgWorkerSingleFile.RunWorkerAsync(details);
            do
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(200);

            } while (bgWorkerSingleFile.IsBusy);
            if (details.filesToNotify != "")
            {
                CustomMessageBox.Show("Unshelved Files", "The following files have been unshelved in YOUR perforce as they are build data that may be needed for testing the shelf:\r\n" + details.filesToNotify);
            }
            return details.errors;

        }
        private string pruneOutput(string updateTxt)
        {
            string curDirectory = Directory.GetCurrentDirectory().Replace(@"\", @"\\") + @"\>";
            updateTxt = Regex.Replace(updateTxt, curDirectory, "");
            return updateTxt;
        }
        private string fstatGetDepotLocation(string localpath)
        {
            //start new process for running the command lines to follow
            Process p4Fstat = new Process();

            //Override standard start info with made startinfo
            p4Fstat.StartInfo = getStartInfo("cmd.exe");
            //start process
            p4Fstat.Start();

            //Create overriden input and output
            StreamWriter p4Input = p4Fstat.StandardInput;

            //start using overriden input
            using (p4Input = p4Fstat.StandardInput)
            {
                p4Input.WriteLine(("p4 fstat " + localpath));
            }

            //close stream (must be neat unt tidy ^_^)
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string output = p4Fstat.StandardOutput.ReadToEnd();
            Match chk = checkOutputFor(output, @"\.\.\. depotFile\s+(.*)");
            updateOutput(chk.Groups[1].Value);
            //updateOutput(output);

            //continue being neat unt tidy
            p4Fstat.WaitForExit();
            p4Fstat.Close();
            if (!chk.Success)
            {
                return "";
            }
            else
            {
                return chk.Groups[1].Value;
            }
        }
        private void generateFileActions(string describe)
        {
            
            foreach(string s in describe.Trim().Split('\n'))
            {
                Match file = checkOutputFor(s, @"(//.*)#\w+ (\w+)");
                if (file.Groups.Count > 1)
                {
                    fileActions.Add(file.Groups[1].Value, file.Groups[2].Value);
                }
            }
        }
        private bool unshelveAndResolveSingleFile(string cl, string file)
        {
            bool noErrors = true;
            //start new process for running the command lines to follow
            Process p4Unshelve = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();

            //Override standard start info with made startinfo
            p4Unshelve.StartInfo = getStartInfo("cmd.exe");
            p4Unshelve.StartInfo.RedirectStandardError = true;
            //start process
            p4Unshelve.Start();
            //create msg to send to text box
            string msgToGet = "Attempting to get file  " + file + "\n";
            //send msg to text box
            updateOutput(msgToGet);

            //Create overriden input and output
            StreamWriter p4Input = p4Unshelve.StandardInput;

            //start using overriden input
            using (p4Input = p4Unshelve.StandardInput)
            {
                //make string for command line unshelve and push to input
                string textToUnshelve = "p4 unshelve -s " + cl + " -f " + file;
                updateOutput(textToUnshelve);
                p4Input.WriteLine(textToUnshelve);
            }
            //close stream (must be neat unt tidy ^_^)
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string output = p4Unshelve.StandardOutput.ReadToEnd();
            string errors = p4Unshelve.StandardError.ReadToEnd();
            if (errors.Length > 0)
            {
                updateOutput(("[error]:" + errors));
                noErrors = false;
            }
            updateOutput(output);

            //continue being neat unt tidy
            p4Unshelve.WaitForExit();
            p4Unshelve.Close();

            string localFilename = "";
            bool localFileAdded = false;

            string regexFail = @"(.*)#.*unshelved.*";
            Match matchFails = checkOutputFor(output, regexFail);
            if (!matchFails.Success)
            {
                updateOutput("\r\n[error] no files in shelved changelist [error]");
                errorDetected = true;
            }
            else
            {
                localFilename = matchFails.Groups[1].Value;
                string regexAdd = @".*#.*unshelved.*add";
                Match add = checkOutputFor(matchFails.Value, regexAdd);
                if (add.Success)
                {
                    localFileAdded = true;
                }
            }
            if (localFilename != "")
            {
                //start new process for running the command lines to follow
                Process p4Resolve = new Process();

                //Override standard start info with made startinfo
                p4Resolve.StartInfo = getStartInfo("cmd.exe");
                p4Resolve.StartInfo.RedirectStandardError = true;
                //start process
                p4Resolve.Start();

                //Create overriden input and output
                p4Input = p4Resolve.StandardInput;

                //start using overriden input
                using (p4Input = p4Resolve.StandardInput)
                {
                    if (localFileAdded == false)
                    {
                        string textToResolve = calculateResolveAction(localFilename);
                        p4Input.WriteLine(textToResolve);
                    }
                }
                output = p4Resolve.StandardOutput.ReadToEnd();
                string[] stdErrors = p4Resolve.StandardError.ReadToEnd().Split('\n');

                foreach (string stdError in stdErrors)
                {
                    if (!String.IsNullOrWhiteSpace(stdError))
                    {
                        Match stdErrorMatch = checkOutputFor(stdError, @".*- no file.* to resolve");
                        if (!stdErrorMatch.Success)
                        {
                            updateOutput(("[error]:" + stdError));
                            noErrors = false;
                        }
                    }
                }
                //close stream (must be neat unt tidy ^_^)
                p4Input.Close();

                updateOutput(output);

                //continue being neat unt tidy
                p4Resolve.WaitForExit();
                p4Resolve.Close();
            }

            return noErrors;
        }
        private string calculateResolveAction(string file)
        {
            string p4Command = "";
            if (fileActions.Keys.Contains(file))
            {
                if(fileActions[file] == "integrate")
                {
                    p4Command = "p4 resolve -at " + file;
                }
                else
                {
                    p4Command = "p4 resolve -ay " + file;
                }
            }
            else
            {
                p4Command = "p4 resolve -ay " + file;
            }
            return p4Command;
        }
        private string convertAssetP4FileToLocal(string inFile)
        {
            string outFile = "";
            Match cutOffHead = checkOutputFor(inFile, @"\/\/.*(\/assets\/.*)");
            if (cutOffHead.Success)
            {
                outFile = Environment.GetEnvironmentVariable("RS_PROJROOT") + cutOffHead.Groups[1].Value.Replace('/', '\\');
                return outFile;
            }
            else
            {
                errorDetected = true;
                updateOutput("[error]: None asset passed to convertAssetP4FileToLocal");
                return inFile;
            }
        }
        private void populatePreExistingFiles()
        {
            //set state
            updateOutput("\r\n[BUILDSTEP-PREEXIST.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            updateOutput("Checking For Prexisting files");
            //start new process for running the command lines to follow
            Process p4Exist = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();
            string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");

            //Override standard start info with made startinfo
            p4Exist.StartInfo = getStartInfo("cmd.exe");
            p4Exist.StartInfo.RedirectStandardError = true;
            //start process
            p4Exist.Start();

            //Create overriden input
            StreamWriter p4Input = p4Exist.StandardInput;

            //start using overriden input
            using (p4Input = p4Exist.StandardInput)
            {
                //make string for command line syncs and push to input
                p4Input.WriteLine("p4 opened -c default");
            }
            //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string[] output = p4Exist.StandardOutput.ReadToEnd().Split('\n');
            string[] stdErrors = p4Exist.StandardError.ReadToEnd().Split('\n');

            //continue being neat unt tidy
            p4Exist.WaitForExit();
            p4Exist.Close();

            foreach (string line in output)
            {
                Match fileFound = checkOutputFor(line, @"(.*)#[0-9]+ -");
                if (fileFound.Success)
                {
                    preExistingFiles.Add(fileFound.Groups[1].Value);
                }
            }

            foreach (string stdError in stdErrors)
            {
                if (!String.IsNullOrWhiteSpace(stdError))
                {
                    Match stdErrorMatch = checkOutputFor(stdError, @"File\(s\) not opened on this client");
                    if (!stdErrorMatch.Success)
                    {
                        updateOutput(("[error]:" + stdError));
                        errorDetected = true;
                    }
                }
            }
            updateOutput("\r\n[BUILDSTEP-PREEXIST.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        private void validateExistingFiles()
        {
            //set state
            updateOutput("\r\n[BUILDSTEP-POSTEXIST.STARTED]");
            Stopwatch stpwatch = Stopwatch.StartNew();
            updateOutput("Checking For Post Existing files");
            //start new process for running the command lines to follow
            Process p4Exist = new Process();
            //Create start info to be populated shortly
            ProcessStartInfo p4StartInfo = new ProcessStartInfo();
            string projectroot = Environment.GetEnvironmentVariable("RS_PROJROOT");

            //Override standard start info with made startinfo
            p4Exist.StartInfo = getStartInfo("cmd.exe");
            p4Exist.StartInfo.RedirectStandardError = true;
            //start process
            p4Exist.Start();

            //Create overriden input
            StreamWriter p4Input = p4Exist.StandardInput;

            //start using overriden input
            using (p4Input = p4Exist.StandardInput)
            {
                //make string for command line syncs and push to input
                p4Input.WriteLine("p4 opened -c default");
            }
            //close stream (need an ascii art why not zoidberg? (V) (;,,;) (V))
            p4Input.Close();
            //write output to text box after storing (for regexing)
            string[] output = p4Exist.StandardOutput.ReadToEnd().Split('\n');
            string[] stdErrors = p4Exist.StandardError.ReadToEnd().Split('\n');

            //continue being neat unt tidy
            p4Exist.WaitForExit();
            p4Exist.Close();
            List<string> postExistingFiles = new List<string>();
            foreach (string line in output)
            {
                Match fileFound = checkOutputFor(line, @"(.*)#[0-9]+ -");
                if (fileFound.Success)
                {
                    postExistingFiles.Add(fileFound.Groups[1].Value);
                }
            }

            foreach (string stdError in stdErrors)
            {
                if (!String.IsNullOrWhiteSpace(stdError))
                {
                    Match stdErrorMatch = checkOutputFor(stdError, @"File\(s\) not opened on this client",true);
                    if (!stdErrorMatch.Success)
                    {
                        updateOutput(("[error]:" + stdError));
                        errorDetected = true;
                    }
                }
            }

            if (postExistingFiles.Count > 0)
            {
                foreach (string newFile in postExistingFiles)
                {
                    if(!preExistingFiles.Contains(newFile))
                    {
                        //start new process for running the command lines to follow
                        Process p4Cleanup = new Process();

                        //Override standard start info with made startinfo
                        p4Cleanup.StartInfo = getStartInfo("cmd.exe");
                        p4Cleanup.StartInfo.RedirectStandardError = true;
                        //start process
                        p4Cleanup.Start();

                        //Create overriden input and output
                        p4Input = p4Cleanup.StandardInput;

                        //start using overriden input
                        using (p4Input = p4Cleanup.StandardInput)
                        {

                            string textToRevert = "p4 revert " + newFile;
                            p4Input.WriteLine(textToRevert);
                        }

                        //close stream (must be neat unt tidy ^_^)
                        p4Input.Close();
                        //write output to text box after storing (for regexing)
                        string cleanupOutput = p4Cleanup.StandardOutput.ReadToEnd();
                        string[] stdCleanupErrors = p4Cleanup.StandardError.ReadToEnd().Split('\n');
                        foreach (string stdError in stdCleanupErrors)
                        {
                            if (!String.IsNullOrWhiteSpace(stdError))
                            {
                                Match stdErrorMatch = checkOutputFor(stdError, @"File\(s\) not opened on this client", true);
                                if (!stdErrorMatch.Success)
                                {
                                    updateOutput(("[error]:" + stdError));
                                    errorDetected = true;
                                }
                            }
                        }
                        updateOutput(cleanupOutput);

                        //continue being neat unt tidy
                        p4Cleanup.WaitForExit();
                        p4Cleanup.Close();
                    }
                }
            }
            updateOutput("\r\n[BUILDSTEP-POSTEXIST.FINISHED]");
            stopStopWatch(ref stpwatch);
        }
        #endregion

        #region FunctionsforRemote
        private void generateConsoleFile() //function to generate a script file from the supplied options
        {
            string genConsoleLocation = Environment.CurrentDirectory;
            genConsoleLocation += "\\SupportingFiles\\ShelvedChangelist\\generatedConsole.console";
            if (File.Exists(genConsoleLocation)) // if file already exists blank it out
            {
                File.WriteAllText(genConsoleLocation, string.Empty);
            }
            using (StreamWriter consoleFileWriter = File.AppendText(genConsoleLocation)) //open stream for writing out the console file
            {
                consoleFileWriter.Write("connect rsgsancom1\r\n"); //add to file the text log
                string buildToken = "";
                string changelistString = tb_Changelist.Text.Replace(Environment.NewLine, ",");
                var chkBox = gb_Exes.Controls.OfType<CheckBox>();
                var chkBoxFormat = gb_Format.Controls.OfType<CheckBox>();
                string consoleFormats = "";
                string consoleTypes = "";
                string username = Environment.UserName;
                
                foreach (var cf in chkBoxFormat)
                {
                    if (cf.Checked)
                    {
                        if (consoleFormats != "")
                        {
                            consoleFormats += ",";
                        }
                        consoleFormats += cf.Text;
                    }
                }

                foreach (var cb in chkBox)
                {
                    if (cb.Checked)
                    {
                        if (consoleTypes != "")
                        {
                            consoleTypes += ",";
                        }
                        consoleTypes += cb.Text;
                    }
                }
                if (consoleTypes == "")
                {
                    consoleTypes = "NoTypesSelected";
                }
                string syncValue = "";
                if (this.tabSyncType.SelectedTab == this.tabNumber)
                {
                    syncValue = this.tbSyncChangelist.Text;
                }
                else
                {
                    syncValue = (string)this.lBox_syncLabel.SelectedItem;
                }

                string shaders = "";
                if (cb_shader.Checked)
                {
                    shaders = "-shaders";
                }
                else
                {
                    shaders = "-noshader";
                }
                string bugstarFolder = tb_bugstar.Text;
                buildToken = "run \"Shelve QA CodeBuilder\" \"#SHELVEDCHANGELIST=" + changelistString + " #QAEXETYPES=" + consoleTypes + " #QAEXEPLATFORMS=" + consoleFormats + " #QAUSERREQUESTED=" + username + " #QACOPYFOLDER=" + bugstarFolder + " #QABUILDORREBUILD=" + buildRebuild + " #QASYNCLABEL=" + syncValue + " #QASHADERS=" + shaders + "\"\r\n";
                updateOutput("\r\n" + buildToken);
                consoleFileWriter.Write(buildToken);
                consoleFileWriter.Write("disconnect\r\n");
                consoleFileWriter.Write("exit");
            }
        }
        private void runConsole() //Function to call and run the automationconsole with the generated script file
        {
            string genConsoleLocation = Environment.CurrentDirectory;
            genConsoleLocation += "\\SupportingFiles\\ShelvedChangelist\\generatedConsole.console";
            string consoleCommandLine = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\bin\\automation\\RSG.Automation.Console.exe --automationbranch prod --script \"" + genConsoleLocation + "\"";
            bgWorkerRemoteBuilder.RunWorkerAsync(consoleCommandLine);
            do
            {
                Application.DoEvents();
            } while (bgWorkerRemoteBuilder.IsBusy);
        }
        #endregion

        #region Backgroundworkers
        //Codebuilder
        private void bgWorkerCode_DoWork(object sender, DoWorkEventArgs e) //background worker work for building code
        {
            
            CodeInfo temp = new CodeInfo((CodeInfo)e.Argument);
            buildSingleCode(temp.env,temp.sol,temp.type,temp.config,temp.platform,temp.runNum);
            
        }
        private void bgWorkerCode_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
        //Shaderbuilder
        private void bgWorkerShader_DoWork(object sender, DoWorkEventArgs e) //build shaders (note 4 builds for pc only
        {
            ShaderInfo shd = (ShaderInfo)e.Argument;
            List<string> bldBats = new List<string>();
            List<FileDiff> fileDiffs = new List<FileDiff>();
            List<string> foldersToSearch = new List<string>();
            switch(shd.format)
            {
                case "Orbis":
                    bldBats.Add(("rsm_" + shd.type + "_orbis.bat"));
                    foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\orbis"));
                    foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\orbis_final"));
                    break;
                case "Durango":
                    bldBats.Add(("rsm_" + shd.type + "_durango.bat"));
                    foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\durango"));
                    foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\durango_final"));
                    break;
                case "X64":
                    if(envProject == "GTA5")
                    {
                        bldBats.Add(("rsm_" + shd.type + "_win32.bat"));
                        bldBats.Add(("rsm_" + shd.type + "_win32_40.bat"));
                        bldBats.Add(("rsm_"+shd.type+"_win32_nvstereo.bat"));
                        bldBats.Add(("rsm_"+shd.type+"_win32_40_lq.bat"));
                        foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\win32"));
                        foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\win32_40"));
                        foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\win32_nvstereo"));
                        foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\win32_40_lq"));
                    }
                    else
                    {
                        bldBats.Add(("rsm_"+shd.type+"_win32_50.bat"));
                        foldersToSearch.Add((Environment.GetEnvironmentVariable("RS_BUILDBRANCH") + "\\common\\shaders\\win32_50"));
                    }
                    break;
            }
            fileDiffs = getFileDiffs(foldersToSearch);
            foreach (string bld in bldBats)
            {
                string shaderProcess = shd.shaderDir + bld;
                if ((envProject == "RDR3") || (envProject == "Americas"))
                {
                    buildSingleShader(shaderProcess, true);
                }
                else
                {
                    buildSingleShader(shaderProcess, false);
                }
            }
            calculateFileChanges(fileDiffs);
        }
        private void bgWorkerShader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        //Robocopy
        private void bgWorkerCopy_DoWork(object sender, DoWorkEventArgs e) //copy files using background worker
        {
            string copyToLocation = "";
            updateOutput("Copying " + filesToCopy.Count.ToString());
            foreach (CopyFile cf in filesToCopy)
            {
                //start new process for running the command lines to follow
                Process copyExe = new Process();

                //Override standard start info with made startinfo
                copyExe.StartInfo = getStartInfo("cmd.exe");
                //start process
                copyExe.Start();
                string destinationStudio = "";
                if (thisStudio != null)
                {
                    destinationStudio = thisStudio.studioAbr;
                }

                if (destinationStudio == "")
                {
                    destinationStudio = "rsgedi";
                }
                copyToLocation = @"N:\projects\" + envProject + @"\GameLogs\" + destinationStudio + @"\" + bStar;
                if (cf.subFolder != "")
                {
                    copyToLocation += @"\" + cf.subFolder;
                }
                string copyFromLocation = Environment.GetEnvironmentVariable("RS_BUILDBRANCH");
                string processstring = "robocopy.exe ";
                //Create overriden input and output
                StreamWriter exeInput = copyExe.StandardInput;
                //start using overriden input
                using (exeInput = copyExe.StandardInput)
                {                        
                        processstring = "robocopy.exe " + cf.fromLocation + " " + copyToLocation + " " + cf.filename + " /PURGE";
                        exeInput.WriteLine(processstring);

                        updateOutput(("\r\nCopying file " + cf.filename));
                }
                
                //write output to text box after storing (for regexing)
                string firstoutput = copyExe.StandardOutput.ReadToEnd();

                //updateOutput(firstoutput);
                copyExe.WaitForExit();

                exeInput.Close();
                copyExe.Close();

                Match foundFail = checkOutputFor(firstoutput, @".*failed");
                if (foundFail.Success)
                {
                    updateOutput(("[error]: An error occured copying " + cf.filename));
                    errorDetected = true;
                }
            }
            if ((!errorDetected) && (!isCmd))
            {
                updateOutput(("\r\n====Files Copied to: " + copyToLocation + "===="));
                Process.Start("explorer.exe", copyToLocation);
            }
        }
        private void bgWorkerCopy_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }
        //RemoteBuilder
        private void bgWorkerRemoteBuilder_DoWork(object sender, DoWorkEventArgs e)
        {
            string textToRun = (string)e.Argument;

            Process remoteExes = new Process();

            //Override standard start info with made startinfo
            remoteExes.StartInfo = getStartInfo("cmd.exe");

            //start process
            remoteExes.Start();
            string[] acceptedMessages = new string[] {@"error",@"failed",@"Connecting",@"Connected",@"Executing",@"Found",@"Disconnected", @"exit"
            };
            //Create overriden input and output
            StreamWriter remoteInput = remoteExes.StandardInput;

            //start using overriden input
            using (remoteInput = remoteExes.StandardInput)
            {
                remoteInput.WriteLine(textToRun);
            }
            //close stream (must be neat unt tidy ^_^)
            remoteInput.Close();

            while (!remoteExes.StandardOutput.EndOfStream)
            {
                string line = remoteExes.StandardOutput.ReadLine();

                foreach (string aM in acceptedMessages)
                {
                    Match messageChk = checkOutputFor(line, aM, true);
                    if (messageChk.Success)
                    {
                        updateOutput(line, true);
                    }
                }

                Match exitCode = checkOutputFor(line, "exit code ([0-9])");
                if (exitCode.Success)
                {
                    if (exitCode.Groups.Count > 0)
                    {
                        if (exitCode.Groups[1].Value != "0")
                        {
                            updateOutput(("Error detected..." + exitCode.Groups[1].Value));
                            errorDetected = true;

                        }
                        else if (exitCode.Groups[1].Value == "0")
                        {
                            updateOutput("------------------------\r\nCOMMAND SUCCESSFULLY SENT\r\n------------------------", true);
                        }
                    }
                }
            }
            
            //continue being neat unt tidy
            remoteExes.WaitForExit();
            remoteExes.Close();
        }
        private void bgWorkerRemoteBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        //ScriptBuilder
        private void bgWorkerScriptBuilder_DoWork(object sender, DoWorkEventArgs e)
        {
            string scriptRPF = Environment.GetEnvironmentVariable("RS_BUILDBRANCH");
            List<FileDiff> scriptRPFDiffs = new List<FileDiff>();
            List<string> rpfLocations = new List<string> {  ("\\ps4\\levels\\"+envProject.ToLower()+"\\script\\script.rpf"),
                                                            ("\\x64\\levels\\"+envProject.ToLower()+"\\script\\script.rpf"),
                                                            ("\\xboxone\\levels\\"+envProject.ToLower()+"\\script\\script.rpf")
            };

            foreach (string rL in rpfLocations)
            {
                scriptRPFDiffs.Add(new FileDiff(getHash((scriptRPF + rL)), (scriptRPF + rL)));
            }

            Process scriptBuild = new Process();

            //Override standard start info with made startinfo
            scriptBuild.StartInfo = getStartInfo("cmd.exe");
            scriptBuild.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT")+"\\bin\\RageScriptEditor\\";
            //start process
            scriptBuild.Start();

            //Create overriden input and output
            StreamWriter scriptInput = scriptBuild.StandardInput;
            string processString = Environment.GetEnvironmentVariable("RS_TOOLSROOT")+"\\bin\\RageScriptEditor\\IncrediBuildScriptProject.exe --config Debug --scproj "+Environment.GetEnvironmentVariable("RS_SCRIPTBRANCH")+"\\RDR3.scproj --Build";
            //start using overriden input
            using (scriptInput = scriptBuild.StandardInput)
            {
                scriptInput.WriteLine(processString);
            }
            //close stream (must be neat unt tidy ^_^)
            scriptInput.Close();

            while (!scriptBuild.StandardOutput.EndOfStream)
            {
                string line = scriptBuild.StandardOutput.ReadLine();
                Match rpfLine = checkOutputFor(line, @"Packing.*build.*\.rpf.*");
                if (rpfLine.Success)
                {
                    updateOutput(("\r\n" + line));
                }
                Match exitCode = checkOutputFor(line, @"[^0] failed");
                if (exitCode.Success)
                {
                    updateOutput(("\r\n[error]: " + line));
                    errorDetected = true;
                }
            }
            //continue being neat unt tidy
            scriptBuild.WaitForExit();
            scriptBuild.Close();

            foreach (FileDiff fD in scriptRPFDiffs)
            {
                if (getHash(fD.fileName) != fD.before)
                {
                    Match calculatePlatform = checkOutputFor(fD.fileName, @"(build|titleupdate)\\.*\\(ps4|x64|xboxone)\\.*");

                    if (calculatePlatform.Groups.Count > 2)
                    {
                        filesToCopy.Add(new CopyFile(new DirectoryInfo(fD.fileName).Parent.FullName, new FileInfo(fD.fileName).Name, ("scripts\\" + calculatePlatform.Groups[2].Value)));
                    }
                }
            }
        }
        private void bgWorkerScriptBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        //assetBuilder
        private void bgWorkerAssetBuilder_DoWork(object sender, DoWorkEventArgs e)
        {
            if (assetsToBuild.Count == 0)
            {
                return;
            }

            foreach (AssetInfo aTB in assetsToBuild)
            {
                Process assetBuild = new Process();

                //Override standard start info with made startinfo
                assetBuild.StartInfo = getStartInfo("cmd.exe");
                assetBuild.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\ironlib\\lib\\";
                //start process
                assetBuild.Start();

                //Create overriden input and output
                StreamWriter assetInput = assetBuild.StandardInput;
                string processString = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\ironlib\\lib\\RSG.Pipeline.Convert.exe " + aTB.action + " " + aTB.fileName;
                //start using overriden input
                using (assetInput = assetBuild.StandardInput)
                {
                    assetInput.WriteLine(processString);
                }
                //close stream (must be neat unt tidy ^_^)
                assetInput.Close();

                string output = assetBuild.StandardOutput.ReadToEnd();
                string[] lines = output.Split('\n');
                int state = 0;
                foreach (string line in lines)
                {
                    updateOutput(line + "\r\n");
                    if(state == 0)
                    {
                        if (checkOutputFor(line, @"The following target outputs were produced during the build").Success)
                        {
                            state = 1;
                        }
                    }
                    else if (state == 1)
                    {
                        Match fileCheck = checkOutputFor(line, @"(x:.*\.\w+)");
                        if (fileCheck.Success)
                        {
                            string outputPath = Regex.Replace(new DirectoryInfo(fileCheck.Groups[1].Value).Parent.FullName, @"[a-zA-Z]:\\", "") + "\\";
                            filesToCopy.Add(new CopyFile(new DirectoryInfo(fileCheck.Groups[1].Value).Parent.FullName, new FileInfo(fileCheck.Groups[1].Value).Name, outputPath));
                        }
                        else
                        {
                            state = 2;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                //continue being neat unt tidy
                assetBuild.WaitForExit();
                assetBuild.Close();
            }
        }
        private void bgWorkerAssetBuilder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void bgWorkerSingleFile_DoWork(object sender, DoWorkEventArgs e)
        {
            SingleFileInfo input = (SingleFileInfo)e.Argument;
            input.errors = true;
            input.filesToNotify = "";
            foreach (string c in changelists)
            {
                //start new process for running the command lines to follow
                Process p4Validate = new Process();
                //Create start info to be populated shortly
                ProcessStartInfo p4StartInfo = new ProcessStartInfo();

                //Override standard start info with made startinfo
                p4Validate.StartInfo = getStartInfo("cmd.exe");
                p4Validate.StartInfo.RedirectStandardError = true;
                //start process
                p4Validate.Start();

                //create msg to send to text box
                string msgToGet = "\r\nAttempting to validate changelist " + c + "\n";
                //send msg to text box
                updateOutput(msgToGet);

                //Create overriden input and output
                StreamWriter p4Input = p4Validate.StandardInput;

                //start using overriden input
                using (p4Input = p4Validate.StandardInput)
                {
                    //make string for command line unshelve and push to input
                    string textToUnshelve = "p4 describe -s -S " + c;
                    p4Input.WriteLine(textToUnshelve);
                }
                //close stream (must be neat unt tidy ^_^)
                p4Input.Close();
                //write output to text box after storing (for regexing)
                string output = p4Validate.StandardOutput.ReadToEnd();
                string errors = "";
                errors = p4Validate.StandardError.ReadToEnd();
                updateOutput(errors);

                //continue being neat unt tidy
                p4Validate.WaitForExit();
                p4Validate.Close();
                string shelvedFiles = "";
                try
                {
                    shelvedFiles = output.Split(new string[] { "Shelved files ..." }, StringSplitOptions.None)[1];
                }
                catch (IndexOutOfRangeException)
                {
                    input.errors = false;
                }
                if (!checkOutputFor(shelvedFiles, @"//\w").Success)
                {
                    input.errors = false;
                    updateOutput("\r\nChangelist " + c + " contains no files");
                }
                if (input.errors)
                {
                    generateFileActions(shelvedFiles);
                }
                if (!isCmd && input.remote && input.errors)
                {
                    foreach (string file in fileActions.Keys)
                    {
                        if (checkOutputFor(file, @"//.*/build/.*").Success)
                        {
                            if (!input.filesToNotify.Contains(file))
                            {
                                input.errors = unshelveAndResolveSingleFile(c, file);
                                input.filesToNotify += (file + "\r\n");
                            }
                        }
                    }
                }
                if (errors != "")
                {
                    input.errors = false;
                }
            }
        }
        private void bgWorkerSingleFile_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        #endregion
    }

    #region Classes
    class CodeInfo //class for storing the codeinfo object used for background thread
    {
        public CodeInfo(CodeInfo c)
        {
            env = c.env;
            sol = c.sol;
            type = c.type;
            config = c.config;
            platform = c.platform;
            runNum = c.runNum;
        }
        public CodeInfo()
        {
            env = "";
            sol = "";
            type = "";
            config = "";
            platform = "";
            runNum = 0;
        }
        public string env {get;set;}
        public string sol{get;set;}
        public string type{get;set;}
        public string config{get;set;}
        public string platform{get;set;}
        public int runNum{get;set;}
    }
    class ShaderInfo //class for storing the shaderinfo object used for background thread
    {
        public string shaderDir { get; set; }
        public string format {get;set;}
        public string type { get; set; }
    }
    class CopyFile //class for storing the details for a file to copy
    {
        public CopyFile(string from, string file, string sub) //Constructor to set supplied variables
        {
            fromLocation = from;
            filename = file;
            subFolder = sub;
        }
        public CopyFile(string from, string file) //Constructor to set supplied variables
        {
            fromLocation = from;
            filename = file;
            subFolder = "";
        }
        public CopyFile() //Constructor to set empty variables
        {
            fromLocation = "";
            filename = "";
            subFolder = "";
        }
        public string subFolder { get; set; }
        public string fromLocation {get; set;}
        public string filename {get;set;}
    }
    class Studio //class for storing details pertaining to the users studio
    {
        public Studio()
        {
        }
        public string studioAbr { get; set; }
        public string email { get; set; }

        public string calculateStudioAbr(string userEmail, Dictionary<String, String> STUDIOABBREVIATIONS)
        {

            Regex checkForStudio = new Regex(@".*@(.*\.com)");
            Match abrChk = checkForStudio.Match(userEmail);
            if (abrChk.Success)
            {
                foreach (KeyValuePair<String, String> kvp in STUDIOABBREVIATIONS)
                {
                    if (kvp.Key == abrChk.Groups[1].Value)
                    {
                        return kvp.Value;
                    }
                }
            }

            return "";
        }
    }
    class FileDiff
    {
        public string before { get; set; }
        public string fileName { get; set; }

        public FileDiff()
        {
        }

        public FileDiff(string b, string fN)
        {
            before = b;
            fileName = fN;
        }
    }
    class AssetInfo
    {
        public string fileName { get; set; }
        public string action { get; set; }
        public AssetInfo()
        {
            fileName = "";
            action = "";
        }

        public AssetInfo(string fN, string a)
        {
            fileName = fN;
            action = a;
        }
    }
    class SingleFileInfo
    {
        public bool errors { get; set; }
        public string filesToNotify { get; set; }
        public bool remote { get; set; }
    }
    #endregion
}
