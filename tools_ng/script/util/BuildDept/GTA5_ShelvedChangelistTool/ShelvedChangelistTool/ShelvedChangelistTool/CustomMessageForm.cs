﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShelvedChangelistTool
{
    internal partial class CustomMessageForm : Form
    {
        public CustomMessageForm()
        {
            InitializeComponent();
        }

        public CustomMessageForm(string title, string description)
        {
            InitializeComponent();

            this.Text = title;
            this.tb_msg.Text = description;
        }

        private void b_ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

    public static class CustomMessageBox
    {
        public static void Show(string title, string description)
        {
            using(var form = new CustomMessageForm(title, description))
            {
                form.ShowDialog();
            }
        }
    }
}
