﻿namespace ShelvedChangelistTool
{
    partial class ShelvedChangelistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShelvedChangelistForm));
            this.tabSyncType = new System.Windows.Forms.TabControl();
            this.tabLabel = new System.Windows.Forms.TabPage();
            this.lBox_syncLabel = new System.Windows.Forms.ListBox();
            this.tabNumber = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.tbSyncChangelist = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Changelist = new System.Windows.Forms.TextBox();
            this.b_GO = new System.Windows.Forms.Button();
            this.tb_output = new System.Windows.Forms.TextBox();
            this.gb_Exes = new System.Windows.Forms.GroupBox();
            this.cb_debug = new System.Windows.Forms.CheckBox();
            this.cb_Release = new System.Windows.Forms.CheckBox();
            this.cb_BRelease = new System.Windows.Forms.CheckBox();
            this.cb_beta = new System.Windows.Forms.CheckBox();
            this.gb_Format = new System.Windows.Forms.GroupBox();
            this.cb_X64 = new System.Windows.Forms.CheckBox();
            this.cb_Orbis = new System.Windows.Forms.CheckBox();
            this.cb_Durango = new System.Windows.Forms.CheckBox();
            this.bgWorkerCode = new System.ComponentModel.BackgroundWorker();
            this.lb_Bugstar = new System.Windows.Forms.Label();
            this.tb_bugstar = new System.Windows.Forms.TextBox();
            this.cb_shader = new System.Windows.Forms.CheckBox();
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.cb_rebuild = new System.Windows.Forms.CheckBox();
            this.bgWorkerShader = new System.ComponentModel.BackgroundWorker();
            this.lb_build = new System.Windows.Forms.Label();
            this.pb_DoWork = new System.Windows.Forms.ProgressBar();
            this.bgWorkerCopy = new System.ComponentModel.BackgroundWorker();
            this.bgWorkerRemoteBuilder = new System.ComponentModel.BackgroundWorker();
            this.tabRemoteControl = new System.Windows.Forms.TabControl();
            this.tabRemote = new System.Windows.Forms.TabPage();
            this.tabLocal = new System.Windows.Forms.TabPage();
            this.bgWorkerScriptBuilder = new System.ComponentModel.BackgroundWorker();
            this.bgWorkerAssetBuilder = new System.ComponentModel.BackgroundWorker();
            this.bgWorkerSingleFile = new System.ComponentModel.BackgroundWorker();
            this.tabSyncType.SuspendLayout();
            this.tabLabel.SuspendLayout();
            this.tabNumber.SuspendLayout();
            this.gb_Exes.SuspendLayout();
            this.gb_Format.SuspendLayout();
            this.tabRemoteControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabSyncType
            // 
            this.tabSyncType.Controls.Add(this.tabLabel);
            this.tabSyncType.Controls.Add(this.tabNumber);
            this.tabSyncType.Location = new System.Drawing.Point(39, 65);
            this.tabSyncType.Name = "tabSyncType";
            this.tabSyncType.SelectedIndex = 0;
            this.tabSyncType.Size = new System.Drawing.Size(173, 100);
            this.tabSyncType.TabIndex = 15;
            this.tabSyncType.Visible = false;
            // 
            // tabLabel
            // 
            this.tabLabel.Controls.Add(this.lBox_syncLabel);
            this.tabLabel.Location = new System.Drawing.Point(4, 22);
            this.tabLabel.Name = "tabLabel";
            this.tabLabel.Padding = new System.Windows.Forms.Padding(3);
            this.tabLabel.Size = new System.Drawing.Size(165, 74);
            this.tabLabel.TabIndex = 0;
            this.tabLabel.Text = "Label";
            this.tabLabel.UseVisualStyleBackColor = true;
            // 
            // lBox_syncLabel
            // 
            this.lBox_syncLabel.FormattingEnabled = true;
            this.lBox_syncLabel.Location = new System.Drawing.Point(5, 3);
            this.lBox_syncLabel.Name = "lBox_syncLabel";
            this.lBox_syncLabel.Size = new System.Drawing.Size(157, 69);
            this.lBox_syncLabel.TabIndex = 13;
            this.lBox_syncLabel.SelectedIndexChanged += new System.EventHandler(this.lBox_syncLabel_SelectedIndexChanged);
            // 
            // tabNumber
            // 
            this.tabNumber.Controls.Add(this.label2);
            this.tabNumber.Controls.Add(this.tbSyncChangelist);
            this.tabNumber.Location = new System.Drawing.Point(4, 22);
            this.tabNumber.Name = "tabNumber";
            this.tabNumber.Padding = new System.Windows.Forms.Padding(3);
            this.tabNumber.Size = new System.Drawing.Size(165, 74);
            this.tabNumber.TabIndex = 1;
            this.tabNumber.Text = "CL Number";
            this.tabNumber.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Changelist number to sync to";
            // 
            // tbSyncChangelist
            // 
            this.tbSyncChangelist.Location = new System.Drawing.Point(47, 30);
            this.tbSyncChangelist.Name = "tbSyncChangelist";
            this.tbSyncChangelist.Size = new System.Drawing.Size(100, 20);
            this.tbSyncChangelist.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter changelists to unshelve";
            // 
            // tb_Changelist
            // 
            this.tb_Changelist.Location = new System.Drawing.Point(217, 26);
            this.tb_Changelist.Multiline = true;
            this.tb_Changelist.Name = "tb_Changelist";
            this.tb_Changelist.Size = new System.Drawing.Size(100, 168);
            this.tb_Changelist.TabIndex = 1;
            this.toolTips.SetToolTip(this.tb_Changelist, "Changelists you want to unshelve. Enter one per line");
            // 
            // b_GO
            // 
            this.b_GO.Location = new System.Drawing.Point(497, 168);
            this.b_GO.Name = "b_GO";
            this.b_GO.Size = new System.Drawing.Size(75, 23);
            this.b_GO.TabIndex = 2;
            this.b_GO.Text = "Go";
            this.b_GO.UseVisualStyleBackColor = true;
            this.b_GO.Click += new System.EventHandler(this.b_GO_Click);
            // 
            // tb_output
            // 
            this.tb_output.AcceptsReturn = true;
            this.tb_output.Location = new System.Drawing.Point(52, 200);
            this.tb_output.MaximumSize = new System.Drawing.Size(520, 275);
            this.tb_output.Multiline = true;
            this.tb_output.Name = "tb_output";
            this.tb_output.ReadOnly = true;
            this.tb_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_output.Size = new System.Drawing.Size(520, 275);
            this.tb_output.TabIndex = 3;
            this.toolTips.SetToolTip(this.tb_output, "Output window of the build");
            this.tb_output.WordWrap = false;
            // 
            // gb_Exes
            // 
            this.gb_Exes.Controls.Add(this.cb_debug);
            this.gb_Exes.Controls.Add(this.cb_Release);
            this.gb_Exes.Controls.Add(this.cb_BRelease);
            this.gb_Exes.Controls.Add(this.cb_beta);
            this.gb_Exes.Location = new System.Drawing.Point(334, 26);
            this.gb_Exes.Name = "gb_Exes";
            this.gb_Exes.Size = new System.Drawing.Size(107, 116);
            this.gb_Exes.TabIndex = 4;
            this.gb_Exes.TabStop = false;
            this.gb_Exes.Text = "Exes Required";
            this.toolTips.SetToolTip(this.gb_Exes, "Checkboxes for code times you want to build");
            // 
            // cb_debug
            // 
            this.cb_debug.AutoSize = true;
            this.cb_debug.Location = new System.Drawing.Point(11, 91);
            this.cb_debug.Name = "cb_debug";
            this.cb_debug.Size = new System.Drawing.Size(58, 17);
            this.cb_debug.TabIndex = 3;
            this.cb_debug.Text = "Debug";
            this.cb_debug.UseVisualStyleBackColor = true;
            // 
            // cb_Release
            // 
            this.cb_Release.AutoSize = true;
            this.cb_Release.Location = new System.Drawing.Point(11, 67);
            this.cb_Release.Name = "cb_Release";
            this.cb_Release.Size = new System.Drawing.Size(65, 17);
            this.cb_Release.TabIndex = 2;
            this.cb_Release.Text = "Release";
            this.cb_Release.UseVisualStyleBackColor = true;
            // 
            // cb_BRelease
            // 
            this.cb_BRelease.AutoSize = true;
            this.cb_BRelease.Location = new System.Drawing.Point(11, 44);
            this.cb_BRelease.Name = "cb_BRelease";
            this.cb_BRelease.Size = new System.Drawing.Size(90, 17);
            this.cb_BRelease.TabIndex = 1;
            this.cb_BRelease.Text = "BankRelease";
            this.cb_BRelease.UseVisualStyleBackColor = true;
            // 
            // cb_beta
            // 
            this.cb_beta.AutoSize = true;
            this.cb_beta.Location = new System.Drawing.Point(11, 21);
            this.cb_beta.Name = "cb_beta";
            this.cb_beta.Size = new System.Drawing.Size(48, 17);
            this.cb_beta.TabIndex = 0;
            this.cb_beta.Text = "Beta";
            this.cb_beta.UseVisualStyleBackColor = true;
            // 
            // gb_Format
            // 
            this.gb_Format.Controls.Add(this.cb_X64);
            this.gb_Format.Controls.Add(this.cb_Orbis);
            this.gb_Format.Controls.Add(this.cb_Durango);
            this.gb_Format.Location = new System.Drawing.Point(462, 26);
            this.gb_Format.Name = "gb_Format";
            this.gb_Format.Size = new System.Drawing.Size(110, 116);
            this.gb_Format.TabIndex = 5;
            this.gb_Format.TabStop = false;
            this.gb_Format.Text = "Format";
            this.toolTips.SetToolTip(this.gb_Format, "Check boxes for which format you want to build for");
            // 
            // cb_X64
            // 
            this.cb_X64.AutoSize = true;
            this.cb_X64.Location = new System.Drawing.Point(10, 81);
            this.cb_X64.Name = "cb_X64";
            this.cb_X64.Size = new System.Drawing.Size(45, 17);
            this.cb_X64.TabIndex = 2;
            this.cb_X64.Text = "X64";
            this.cb_X64.UseVisualStyleBackColor = true;
            // 
            // cb_Orbis
            // 
            this.cb_Orbis.AutoSize = true;
            this.cb_Orbis.Location = new System.Drawing.Point(10, 50);
            this.cb_Orbis.Name = "cb_Orbis";
            this.cb_Orbis.Size = new System.Drawing.Size(50, 17);
            this.cb_Orbis.TabIndex = 1;
            this.cb_Orbis.Text = "Orbis";
            this.cb_Orbis.UseVisualStyleBackColor = true;
            // 
            // cb_Durango
            // 
            this.cb_Durango.AutoSize = true;
            this.cb_Durango.Location = new System.Drawing.Point(10, 21);
            this.cb_Durango.Name = "cb_Durango";
            this.cb_Durango.Size = new System.Drawing.Size(67, 17);
            this.cb_Durango.TabIndex = 0;
            this.cb_Durango.Text = "Durango";
            this.cb_Durango.UseVisualStyleBackColor = true;
            // 
            // bgWorkerCode
            // 
            this.bgWorkerCode.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerCode_DoWork);
            this.bgWorkerCode.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerCode_RunWorkerCompleted);
            // 
            // lb_Bugstar
            // 
            this.lb_Bugstar.AutoSize = true;
            this.lb_Bugstar.Location = new System.Drawing.Point(324, 174);
            this.lb_Bugstar.Name = "lb_Bugstar";
            this.lb_Bugstar.Size = new System.Drawing.Size(58, 13);
            this.lb_Bugstar.TabIndex = 6;
            this.lb_Bugstar.Text = "B* Number";
            // 
            // tb_bugstar
            // 
            this.tb_bugstar.Location = new System.Drawing.Point(389, 171);
            this.tb_bugstar.MaximumSize = new System.Drawing.Size(100, 20);
            this.tb_bugstar.MinimumSize = new System.Drawing.Size(100, 20);
            this.tb_bugstar.Name = "tb_bugstar";
            this.tb_bugstar.Size = new System.Drawing.Size(100, 20);
            this.tb_bugstar.TabIndex = 7;
            // 
            // cb_shader
            // 
            this.cb_shader.AutoSize = true;
            this.cb_shader.Location = new System.Drawing.Point(345, 148);
            this.cb_shader.Name = "cb_shader";
            this.cb_shader.Size = new System.Drawing.Size(65, 17);
            this.cb_shader.TabIndex = 8;
            this.cb_shader.Text = "Shaders";
            this.toolTips.SetToolTip(this.cb_shader, "Check to build shader for the selected formats");
            this.cb_shader.UseVisualStyleBackColor = true;
            // 
            // cb_rebuild
            // 
            this.cb_rebuild.AutoSize = true;
            this.cb_rebuild.Location = new System.Drawing.Point(472, 148);
            this.cb_rebuild.Name = "cb_rebuild";
            this.cb_rebuild.Size = new System.Drawing.Size(68, 17);
            this.cb_rebuild.TabIndex = 9;
            this.cb_rebuild.Text = "Rebuild?";
            this.toolTips.SetToolTip(this.cb_rebuild, "Check to switch to rebuild mode");
            this.cb_rebuild.UseVisualStyleBackColor = true;
            this.cb_rebuild.CheckedChanged += new System.EventHandler(this.cb_rebuild_CheckedChanged);
            // 
            // bgWorkerShader
            // 
            this.bgWorkerShader.WorkerReportsProgress = true;
            this.bgWorkerShader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerShader_DoWork);
            this.bgWorkerShader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerShader_RunWorkerCompleted);
            // 
            // lb_build
            // 
            this.lb_build.AutoSize = true;
            this.lb_build.BackColor = System.Drawing.Color.Transparent;
            this.lb_build.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_build.ForeColor = System.Drawing.Color.Silver;
            this.lb_build.Location = new System.Drawing.Point(-3, 32);
            this.lb_build.Name = "lb_build";
            this.lb_build.Size = new System.Drawing.Size(51, 59);
            this.lb_build.TabIndex = 10;
            this.lb_build.Text = "B";
            this.lb_build.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_DoWork
            // 
            this.pb_DoWork.Cursor = System.Windows.Forms.Cursors.No;
            this.pb_DoWork.Location = new System.Drawing.Point(52, 171);
            this.pb_DoWork.Name = "pb_DoWork";
            this.pb_DoWork.Size = new System.Drawing.Size(138, 23);
            this.pb_DoWork.TabIndex = 11;
            // 
            // bgWorkerCopy
            // 
            this.bgWorkerCopy.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerCopy_DoWork);
            this.bgWorkerCopy.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerCopy_RunWorkerCompleted);
            // 
            // bgWorkerRemoteBuilder
            // 
            this.bgWorkerRemoteBuilder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerRemoteBuilder_DoWork);
            this.bgWorkerRemoteBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerRemoteBuilder_RunWorkerCompleted);
            // 
            // tabRemoteControl
            // 
            this.tabRemoteControl.Controls.Add(this.tabRemote);
            this.tabRemoteControl.Controls.Add(this.tabLocal);
            this.tabRemoteControl.Location = new System.Drawing.Point(2, 1);
            this.tabRemoteControl.Name = "tabRemoteControl";
            this.tabRemoteControl.SelectedIndex = 0;
            this.tabRemoteControl.Size = new System.Drawing.Size(120, 20);
            this.tabRemoteControl.TabIndex = 14;
            this.tabRemoteControl.Visible = false;
            this.tabRemoteControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabRemoteControl_Selected);
            // 
            // tabRemote
            // 
            this.tabRemote.Location = new System.Drawing.Point(4, 22);
            this.tabRemote.Name = "tabRemote";
            this.tabRemote.Padding = new System.Windows.Forms.Padding(3);
            this.tabRemote.Size = new System.Drawing.Size(112, 0);
            this.tabRemote.TabIndex = 0;
            this.tabRemote.Text = "Remote";
            this.tabRemote.UseVisualStyleBackColor = true;
            // 
            // tabLocal
            // 
            this.tabLocal.Location = new System.Drawing.Point(4, 22);
            this.tabLocal.Name = "tabLocal";
            this.tabLocal.Padding = new System.Windows.Forms.Padding(3);
            this.tabLocal.Size = new System.Drawing.Size(112, 0);
            this.tabLocal.TabIndex = 1;
            this.tabLocal.Text = "Local";
            this.tabLocal.UseVisualStyleBackColor = true;
            // 
            // bgWorkerScriptBuilder
            // 
            this.bgWorkerScriptBuilder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerScriptBuilder_DoWork);
            this.bgWorkerScriptBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerScriptBuilder_RunWorkerCompleted);
            // 
            // bgWorkerAssetBuilder
            // 
            this.bgWorkerAssetBuilder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerAssetBuilder_DoWork);
            this.bgWorkerAssetBuilder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerAssetBuilder_RunWorkerCompleted);
            // 
            // bgWorkerSingleFile
            // 
            this.bgWorkerSingleFile.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerSingleFile_DoWork);
            this.bgWorkerSingleFile.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerSingleFile_RunWorkerCompleted);
            // 
            // ShelvedChangelistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 500);
            this.Controls.Add(this.tabSyncType);
            this.Controls.Add(this.tabRemoteControl);
            this.Controls.Add(this.pb_DoWork);
            this.Controls.Add(this.lb_build);
            this.Controls.Add(this.cb_rebuild);
            this.Controls.Add(this.cb_shader);
            this.Controls.Add(this.tb_bugstar);
            this.Controls.Add(this.lb_Bugstar);
            this.Controls.Add(this.gb_Format);
            this.Controls.Add(this.gb_Exes);
            this.Controls.Add(this.tb_output);
            this.Controls.Add(this.b_GO);
            this.Controls.Add(this.tb_Changelist);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(600, 538);
            this.MinimumSize = new System.Drawing.Size(600, 538);
            this.Name = "ShelvedChangelistForm";
            this.Text = "Tool for unshelving and building";
            this.Load += new System.EventHandler(this.ShelvedChangelistForm_Load);
            this.tabSyncType.ResumeLayout(false);
            this.tabLabel.ResumeLayout(false);
            this.tabNumber.ResumeLayout(false);
            this.tabNumber.PerformLayout();
            this.gb_Exes.ResumeLayout(false);
            this.gb_Exes.PerformLayout();
            this.gb_Format.ResumeLayout(false);
            this.gb_Format.PerformLayout();
            this.tabRemoteControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Changelist;
        private System.Windows.Forms.Button b_GO;
        private System.Windows.Forms.TextBox tb_output;
        private System.Windows.Forms.GroupBox gb_Exes;
        private System.Windows.Forms.CheckBox cb_debug;
        private System.Windows.Forms.CheckBox cb_Release;
        private System.Windows.Forms.CheckBox cb_BRelease;
        private System.Windows.Forms.CheckBox cb_beta;
        private System.Windows.Forms.GroupBox gb_Format;
        private System.Windows.Forms.CheckBox cb_X64;
        private System.Windows.Forms.CheckBox cb_Orbis;
        private System.Windows.Forms.CheckBox cb_Durango;
        private System.ComponentModel.BackgroundWorker bgWorkerCode;
        private System.Windows.Forms.Label lb_Bugstar;
        private System.Windows.Forms.TextBox tb_bugstar;
        private System.Windows.Forms.CheckBox cb_shader;
        private System.Windows.Forms.ToolTip toolTips;
        private System.ComponentModel.BackgroundWorker bgWorkerShader;
        private System.Windows.Forms.CheckBox cb_rebuild;
        private System.Windows.Forms.Label lb_build;
        private System.Windows.Forms.ProgressBar pb_DoWork;
        private System.ComponentModel.BackgroundWorker bgWorkerCopy;
        private System.ComponentModel.BackgroundWorker bgWorkerRemoteBuilder;
        private System.Windows.Forms.ListBox lBox_syncLabel;
        private System.Windows.Forms.TabControl tabRemoteControl;
        private System.Windows.Forms.TabPage tabRemote;
        private System.Windows.Forms.TabPage tabLocal;
        private System.Windows.Forms.TabPage tabLabel;
        private System.Windows.Forms.TabPage tabNumber;
        private System.Windows.Forms.TabControl tabSyncType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbSyncChangelist;
        private System.ComponentModel.BackgroundWorker bgWorkerScriptBuilder;
        private System.ComponentModel.BackgroundWorker bgWorkerAssetBuilder;
        private System.ComponentModel.BackgroundWorker bgWorkerSingleFile;
    }
}

