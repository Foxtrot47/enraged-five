::************************************************
:: GTA5 Dongled Symbol Backup batch file		**
:: Updated: 				04/03/2013			**
:: Last edited by:			Ross McKinstray		**
:: Last edits:				Raring the symbols	**
::************************************************
@echo off
echo.
cd /d %~dp0

set copydir=N:\RSGEDI\Distribution\QA_Build\gta5
set builddir=X:\gta5\build\dev
set path="C:\Program Files (x86)\WinRAR\";%path%

set ver=NUL
FOR /F "eol=# skip=3" %%G IN (%builddir%\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
echo version=%ver%
GOTO MAIN
	
:MAIN


if not exist %copydir%\PS3_Symbols\Version%ver% (
	echo Folder does not exist! Have you backed up the undongled exes??
) ELSE (
	echo Copying symbols to the network
	rar a %copydir%/PS3_Symbols/Version%ver%/Dongled_symbols.rar %builddir%\*.self
)




pause

