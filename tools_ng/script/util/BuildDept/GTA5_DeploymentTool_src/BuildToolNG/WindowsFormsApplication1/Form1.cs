﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Windows.Forms;
using System.Threading.Tasks;


namespace BuildDeploymentTool
{
/// <summary>
/// Last Modified by Graham.Rust, Added features such as CL->B*, Sync, and Check out. Rejigged buttons to point to xml for the rest of tabbed fields. Added all supportive code for all functions.
/// </summary>
    public partial class BuildDeploymentMenu : Form
    {
        //create an empty list for the new buglist.
        public List<Bug> BugList = new List<Bug>();
        public BuildDeploymentMenu()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Scans through an xml file looking for specific fields and then returns the data within that field.
        /// </summary>
        public List<Bug> GetFieldValuesfromXMLList(string file, List<Bug> BugListRef)
        {
            //we want to return nothing if the string doesnt exist
            //instantiate a new xml document 
            XmlDocument xmlDoc = new XmlDocument();
            //check to see if the xml file provided exists
            if (File.Exists(file))
            {
                // Error out gracefully if the xml data is a mess
                try
                {
                    xmlDoc.Load(file);//Load the xml document

                    XmlNodeList XmlList = xmlDoc.GetElementsByTagName("bug");

                    //Loop through the bugs
                    foreach (XmlElement xmlElement in XmlList)
                    {
                        string bugnumber = "";
                        string summary = "";
                        string bugclass = "";
                        string project = "";
                        string status = "";
                        foreach (XmlElement xmlElement1 in xmlElement.ChildNodes)
                        {
                            if (xmlElement1.Name == "id")
                            {
                                bugnumber = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "summary")
                            {
                                summary = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "category")
                            {
                                bugclass = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "projectId")
                            {
                                project = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "state")
                            {
                                status = xmlElement1.InnerText;
                            }
                        }
                        Bug newBug = new Bug(Convert.ToInt32(bugnumber), summary, bugclass, project, status);
                        BugListRef.Add(newBug);
                    }
                }
                catch
                {
                    if (file != "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml")//hack to prevent spamming of xml check if b* returns garbled mess.
                    {
                        MessageBox.Show("Please make sure your Xml is correct.", "Important Message");
                    }
                }
            }
            return BugListRef;
        }

        public string GetFieldValuefromXML(string file, string field, string name)
        {
            //we want to return nothing if the string doesnt exist
            string fieldValue = "";
            //instantiate a new xml document 
            XmlDocument xmlDoc = new XmlDocument();
            //check to see if the xml file provided exists
            if (File.Exists(file))
            {
                // Error out gracefully if the xml data is a mess
                try
                {
                    xmlDoc.Load(file);//Load the xml document
                    XmlNodeList parentNode = xmlDoc.GetElementsByTagName(field); //Define which field you are interested in and return the list
                    foreach (XmlNode childNode in parentNode)//Go through each field entry
                    {
                        if (childNode != null)
                        {
                            fieldValue = childNode.SelectSingleNode(name).InnerText;//get the specific childnode via "name" and return the data from it (in this case its passing back the batfile path+name as a string value.)
                        }
                    }
                }
                catch
                {
                    if (file != "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml")//hack to prevent spamming of xml check if b* returns garbled mess.
                    {
                        MessageBox.Show("Please make sure your Xml is correct.", "Important Message");
                    }
                }
            }
            return fieldValue;//return the batfile string name.
        }

        // NG Buttons 
        private void BTN_Integrate_Script_Headers_Click(object sender, EventArgs e)
        {
            //Getfieldvaluefromxlm loads the xml passes the value of the button name and then retrieves back the associated batfile (this one is for debug)
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml","ButtonConfig",((Button)sender).Name));
            //Actually get the vlues from the xml file and set it to the string
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            //Run the batfile that was retrieved from "GetFieldValuefromXML"
            Process.Start(ProcessString);
        }

        private void BTN_Clear_Debug_Script_Files_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"1");
            Process.Start(ProcessString, ((Button)sender).Name + "1");
            //special case where we want to launch two batfiles
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"2");
            Process.Start(ProcessString2, ((Button)sender).Name+"2");
        }

        private void BTN_Build_NG_American_Text_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Randomise_Natives_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Run_Scan_Natives_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Label_New_NG_PreBuild_Code_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"NG");
        }

        private void BTN_Label_NG_Prebuild_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Orbis_deployed_Prebuild_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Durango_Deployed_Prebuild_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Prebuild_mpPacks_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Prebuild_DLC_packages_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Grab_NG_Prebuild_Code_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_to_DFSR_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Label_Current_NG_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"1"));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
            Process.Start(ProcessString);
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"2"));
            String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
            Process.Start(ProcessString2);
        }

        private void BTN_Label_NG_version_xxx_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Orbis_Deployed_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Durango_Deployed_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Current_mpPacks_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Current_DLC_packages_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Backup_Symbols_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        //TU Release buttons
        private void BTN_Label_Current_NG_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "TU_JPN");
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
                Process.Start(ProcessString2, "TU_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "TU");
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
                Process.Start(ProcessString2, "TU");
            }
        }

        private void BTN_Label_NG_version_xxx_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU");
            }

        }
        private void BTN_Copy_Orbis_Deployed_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TUPS4_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TUPS4");
            }
        }
        private void BTN_Copy_Durango_Deployed_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TUXBOX1_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TUXBOX1");
            }
        }
        private void BTN_Copy_Current_mpPacks_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TU");
        }
        private void BTN_Copy_Current_DLC_packages_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "TU");
        }
        private void BTN_Backup_Symbols_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU");
            }

        }
        private void BTN_Copy_to_DFSR_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TU");
        }
        private void BTN_Integrate_Script_Headers_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TU");
        }
        private void BTN_Clear_Debug_Script_Files_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
            Process.Start(ProcessString, ((Button)sender).Name + "1");
            //special case where we want to launch two batfiles
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
            Process.Start(ProcessString2, ((Button)sender).Name + "2");
        }
        private void BTN_Build_NG_American_Text_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TU");
        }
        private void BTN_Randomise_Natives_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }
        private void BTN_Run_Scan_Natives_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }
        private void BTN_Label_New_NG_PreBuild_Code_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TU");
        }
        private void BTN_Label_NG_Prebuild_TU_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TU");
            }

        }
        private void BTN_Copy_Orbis_deployed_Prebuild_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "TUPS4");
        }
        private void BTN_Copy_Durango_Deployed_Prebuild_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TUXBOX1");
        }
        private void BTN_Copy_Prebuild_mpPacks_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }
        private void BTN_Copy_Prebuild_DLC_packages_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }
        private void BTN_Grab_NG_Prebuild_Code_TU_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "TU");
        }
        // dev_Temp Buttons
        private void BTN_Integrate_Script_Headers_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Clear_Debug_Script_Files_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
            Process.Start(ProcessString, ((Button)sender).Name + "1");
            //special case where we want to launch two batfiles
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
            Process.Start(ProcessString2, ((Button)sender).Name + "2");
        }

        private void BTN_Build_NG_American_Text_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Randomise_Natives_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Run_Scan_Natives_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Label_New_NG_PreBuild_Code_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Label_NG_Prebuild_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp_JPN");
            }
            else 
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp");
            }
        }

        private void BTN_Copy_Orbis_deployed_Prebuild_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TempPS4");
        }

        private void BTN_Copy_Durango_Deployed_Prebuild_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"TempXBOX1");
        }

        private void BTN_Copy_Prebuild_mpPacks_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Copy_Prebuild_DLC_packages_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Grab_NG_Prebuild_Code_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Label_Current_NG_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "Temp_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "Temp");

            }
        }

        private void BTN_Label_NG_version_xxx_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp");
            }
        }

        private void BTN_Copy_Orbis_Deployed_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TempPS4_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "TempPS4");
            }
        }

        private void BTN_Copy_Durango_Deployed_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString,"TempXBOX1_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString,"TempXBOX1");
            }
        }

        private void BTN_Copy_Current_mpPacks_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Copy_Current_DLC_packages_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        private void BTN_Backup_Symbols_Temp_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "Temp");
            }
        }

        private void BTN_Copy_to_DFSR_Temp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString,"Temp");
        }

        // UTIL Buttons
        private void BTN_Sync_latest_DLC_folders_Click(object sender, EventArgs e)
        {
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"_TU"+"1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU" + "1");
                Process.Start(ProcessString);
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"_TU"+"2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU" + "2");
                Process.Start(ProcessString2);
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString);
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
                Process.Start(ProcessString2);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp" + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp" + "1");
                Process.Start(ProcessString);
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp" + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp" + "2");
                Process.Start(ProcessString2);
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE" + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE" + "1");
                Process.Start(ProcessString);
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE" + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE" + "2");
                Process.Start(ProcessString2);
            }

        }

        private void BTN_Launch_DLCTextBuilder_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString);
        }

        private void BTN_Launch_ContentStar_TM_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString);
        }

        private void BTN_REBUILD_Win32_30_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString,"dev");
        }

        private void BTN_REBUILD_Win32_40_LQ_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name+"_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString,"dev");
        }

        private void BTN_REBUILD_Win32_NV_Stereo_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString,"dev");
        }

        private void BTN_build_Win32_30_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString,"dev");
        }

        private void BTN_build_Win32_40_LQ_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            } 
            Process.Start(ProcessString, "dev");
        }

        private void BTN_build_Win32_NV_Stereo_shaders_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString,"dev");
        }

        private void BTN_CHECK_SHADERS_Click(object sender, EventArgs e)
        {
            String ProcessString = "";
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
            }
            Process.Start(ProcessString);
        }
        /// <summary>
        /// Syncs to the head revision of a branch using batfiles, passing parameters and returns a string for the changelist number.
        /// </summary>
        public string StartSyncProcess(string FileLocation, string Forced, int timeout)
        {
            System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Sync_textOutput.txt", "");//wipe the textfile that stores the sync data
            string CLNumber = "";
            string[] SyncArgs = new string[] { FileLocation,Forced}; //Args passed through to the batfile %1, %2
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\sync_text.bat", string.Join(" ", SyncArgs));//start the sync process (i.e run the batfile with the args)
            {
                if (Temp.WaitForExit(timeout))//allow the batfile to finish its job (if it takes too long a timeout will cancel it and the sync will have to start again)
                {
                    string CLNumberOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Sync_textOutput.txt");//read the textfile 
                    CLNumber = Regex.Match(CLNumberOutput, @"\d+").Value;//grab the changelist 
                }
            }
            return CLNumber;//return the changelist string.
        }

        private void button_Rage_Click(object sender, EventArgs e)
        {
            try
            {
                string RageSourceLocation = "";
                if (RB_TU.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_TU");
                }
                if (RB_NG.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage");
                }
                if (RB_Temp.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_NGLIVE");
                }
                string RageCL = StartSyncProcess(RageSourceLocation, ForceChecked, 30000);
                TB_Rage2.Text = RageCL;
                TB_Rage.Text = RageCL;
                if (CBXlast.Checked)
                {
                    StartSyncProcess("//depot/gta5/xlast/...", ForceChecked, 30000);
                }
            }
            catch
            {

            }
        }

        private void button_Src_Click(object sender, EventArgs e)
        {
            try
            {
                string SourceSourceLocation = "";
                if (RB_TU.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_TU");
                }
                if (RB_NG.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source");
                }
                if (RB_Temp.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_NGLIVE");
                }
                string SrcCL = StartSyncProcess(SourceSourceLocation, ForceChecked, 30000);
                TB_Src2.Text = SrcCL;
                TB_Src.Text = SrcCL;
                if (CBXlast.Checked)
                {
                    StartSyncProcess("//depot/gta5/xlast/...", ForceChecked, 30000);
                }
            }
            catch { }

        }

        private void button_script_Click(object sender, EventArgs e)
        {
            try {
                string ScriptSourceLocation = "";
                if (RB_TU.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_TU");
                }
                if (RB_NG.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script");
                }
                if (RB_Temp.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_NGLIVE");
                }
                string ScriptCL = StartSyncProcess(ScriptSourceLocation, ForceChecked, 30000);
                TB_Script2.Text = ScriptCL;
                TB_Script.Text = ScriptCL;
            } catch { }
        }

        private void button_text_Click(object sender, EventArgs e)
        {
            try
            {
                string TextSourceLocation = "";
                if (RB_TU.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_TU");
                }
                if (RB_NG.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text");
                }
                if (RB_Temp.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_NGLIVE");
                }
                string TextCL = StartSyncProcess(TextSourceLocation, ForceChecked, 30000);
                TB_Text2.Text = TextCL;
                TB_Text.Text = TextCL;
            }
            catch { }
        }

        private void button_data_Click(object sender, EventArgs e)
        {
            try
            {
                string DataSourceLocation = "";
                if (RB_TU.Checked)
                {
                    if (CB_JPN.Checked)
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_TU_JPN");
                    }
                    else
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_TU");
                    }
                }
                if (RB_NG.Checked)
                {
                    DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data");
                }
                if (RB_Temp.Checked)
                {
                    if (CB_JPN.Checked)
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_Temp_JPN");
                    }
                    else
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_Temp");
                    }
                }
                if (RB_NGLIVE.Checked)
                {
                    if (CB_JPN.Checked)
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_NGLIVE_JPN");
                    }
                    else
                    {
                        DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_NGLIVE");
                    }
                }
                string DataCL = StartSyncProcess(DataSourceLocation, ForceChecked, 180000);
                TB_Data2.Text = DataCL;
                TB_Data.Text = DataCL;
                String BaseSourceLocation = "";
                BaseSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Base_Temp");
                string BaseCL = StartSyncProcess(BaseSourceLocation, ForceChecked, 180000);
            }
            catch { }
        }

        private void BTN_Export_Click(object sender, EventArgs e)
        {
            try
            {
                string ExportSourceLocation = "";
                if (RB_TU.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_TU");
                }
                if (RB_NG.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export");
                }
                if (RB_Temp.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_TU");
                }
                if (RB_NGLIVE.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_NGLIVE");
                }
                string ExportCL = StartSyncProcess(ExportSourceLocation, ForceChecked, 180000);
                TB_Export.Text = ExportCL;
            }
            catch { }
        }

        private void BTN_Metadata_Click(object sender, EventArgs e)
        {
            try
            {
                string MetaDataSourceLocation = "";
                if (RB_TU.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_TU");
                }
                if (RB_NG.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData");
                }
                if (RB_Temp.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_NGLIVE");
                }
                string MetaDataCL = StartSyncProcess(MetaDataSourceLocation, ForceChecked, 180000);
                TB_Metadata.Text = MetaDataCL;
            }
            catch { }
        }

        private void BTN_Tools_NG_Click(object sender, EventArgs e)
        {
            try
            {
                string ToolsSource = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Tools");
                StartSyncProcess(ToolsSource, ForceChecked, 30000);
            }
            catch { }
        }


        /// <summary>
        /// special sync option that runs all syncs
        /// </summary>
        private void button_All_Click(object sender, EventArgs e)
        {
            try
            {
                //Rage
                string RageSourceLocation = "";
                if (RB_TU.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_TU");
                }
                if (RB_NG.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage");
                }
                if (RB_Temp.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    RageSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Rage_NGLIVE");
                }
                string RageCL = StartSyncProcess(RageSourceLocation, ForceChecked, 30000);
                TB_Rage2.Text = RageCL;
                TB_Rage.Text = RageCL;

                //Source
                string SourceSourceLocation = "";
                if (RB_TU.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_TU");
                }
                if (RB_NG.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source");
                }
                if (RB_Temp.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    SourceSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Source_NGLIVE");
                }
                string SrcCL = StartSyncProcess(SourceSourceLocation, ForceChecked, 30000);
                TB_Src2.Text = SrcCL;
                TB_Src.Text = SrcCL;

                //Xlast
                StartSyncProcess("//depot/gta5/xlast/...", ForceChecked, 30000);

                //Text
                string TextSourceLocation = "";
                if (RB_TU.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_TU");
                }
                if (RB_NG.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text");
                }
                if (RB_Temp.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    TextSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Text_NGLIVE");
                }
                string TextCL = StartSyncProcess(TextSourceLocation, ForceChecked, 30000);
                TB_Text2.Text = TextCL;
                TB_Text.Text = TextCL;

                //Script
                string ScriptSourceLocation = "";
                if (RB_TU.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_TU");
                }
                if (RB_NG.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script");
                }
                if (RB_Temp.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    ScriptSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Script_NGLIVE");
                }
                string ScriptCL = StartSyncProcess(ScriptSourceLocation, ForceChecked, 30000);
                TB_Script2.Text = ScriptCL;
                TB_Script.Text = ScriptCL;

                //Tools
                StartSyncProcess("//depot/gta5/tools_ng/...", ForceChecked, 180000);

                //Metadata
                string MetaDataSourceLocation = "";
                if (RB_TU.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_TU");
                }
                if (RB_NG.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData");
                }
                if (RB_Temp.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    MetaDataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_MetaData_NGLIVE");
                }
                string MetaDataCL = StartSyncProcess(MetaDataSourceLocation, ForceChecked, 180000);
                TB_Metadata.Text = MetaDataCL;

                //Export
                string ExportSourceLocation = "";
                if (RB_TU.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_TU");
                }
                if (RB_NG.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export");
                }
                if (RB_Temp.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_TU");
                }
                if (RB_NGLIVE.Checked)
                {
                    ExportSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Export_NGLIVE");
                }
                string ExportCL = StartSyncProcess(ExportSourceLocation, ForceChecked, 180000);
                TB_Export.Text = ExportCL;

                //Data
                string DataSourceLocation = "";
                if (RB_TU.Checked)
                {
                    DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_TU");
                }
                if (RB_NG.Checked)
                {
                    DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data");
                }
                if (RB_Temp.Checked)
                {
                    DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    DataSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Data_NGLIVE");
                }
                string DataCL = StartSyncProcess(DataSourceLocation, ForceChecked, 180000);
                TB_Data2.Text = DataCL;
                TB_Data.Text = DataCL;
                
                string BaseSourceLocation = "";
                if (RB_TU.Checked)
                {
                    BaseSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Base_TU");
                }
                if (RB_NG.Checked)
                {
                    BaseSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Base");
                }
                if (RB_Temp.Checked)
                {
                    BaseSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Base_Temp");
                }
                if (RB_NGLIVE.Checked)
                {
                    BaseSourceLocation = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Sync_Base_NGLIVE");
                }
                string BaseCL = StartSyncProcess(BaseSourceLocation, ForceChecked, 180000);

            }
            catch { }           
            
        }

        /// <summary>
        /// Writes the contents of the changelist boxes to a text file 
        /// </summary>
        private void BtnWriteToFile_Click(object sender, EventArgs e)
        {
            CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLList.txt");//Wipe the existing textfile
            string[] lines = { "TimeStamp: " + System.DateTime.Now, "Rage: " + TB_Rage.Text, "Src: " + TB_Src.Text, "Script: " + TB_Script.Text, "Text: " + TB_Text.Text, "Data: " + TB_Data.Text, "Export: " + TB_Export.Text, "Metadata: " + TB_Metadata.Text }; //collate all the "current" strings together 
            System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\CLList.txt", lines);//writes to the file
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLList.txt");//opens the textfile.
            TB_Rage2.Text = TB_Rage.Text;
            TB_Src2.Text = TB_Src.Text;
            TB_Script2.Text = TB_Script.Text;
            TB_Text2.Text = TB_Text.Text;
            TB_Data2.Text = TB_Data.Text;
            LBL_TimeStamp2.Text = System.DateTime.Now.ToString();

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            string[] lines = { "TimeStamp: " + System.DateTime.Now, "Rage: " + TB_Rage.Text, "Src: " + TB_Src.Text, "Script: " + TB_Script.Text, "Text: " + TB_Text.Text, "Data: " + TB_Data.Text, "Export: " + TB_Export.Text, "Metadata: " + TB_Metadata.Text }; //collate all the "old" strings together 
            CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\OldCLList.txt");
            System.IO.File.WriteAllLines(@"X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\OldCLList.txt", lines);
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\OldCLList.txt");
            // updated the "previous" changelist fields within the CL->B* tab.
            TB_Rage1.Text = TB_Rage.Text;
            TB_Src1.Text = TB_Src.Text;
            TB_Script1.Text = TB_Script.Text;
            TB_Text1.Text = TB_Text.Text;
            TB_Data1.Text = TB_Data.Text;
            LBL_TimeStamp1.Text = System.DateTime.Now.ToString();
        }

        /// <summary>
        /// logs into bugstar rest interface.
        /// </summary>
        
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if ((TBUser.Text != "" || TBUser.Text != null) && (TBPassword.Text != "" || TBPassword.Text != null))// checks the user and password for standard blank/null 
            {
                cookieContainer = new CookieContainer(); // creates a new cookie to contain your secure one created below.

                //get the user and password
                BtnLogin.Enabled = false;
                string username = TBUser.Text;
                string password = TBPassword.Text;

                //get the project setup so that we can request a response from the rest service
                Uri getUri = new Uri(uriStr + "rest/Projects");
                HttpWebRequest httpProjectGet = (HttpWebRequest)HttpWebRequest.Create(getUri);
                httpProjectGet.Method = "GET";

                httpProjectGet.CookieContainer = cookieContainer;//create the temporary cookie from project request.

                //setup the user with password authentication
                try{
                    HttpWebResponse getProjectResponse = (HttpWebResponse)httpProjectGet.GetResponse();
                    Stream stream = getProjectResponse.GetResponseStream();
                    String responseuri = getProjectResponse.ResponseUri.OriginalString;
                    String sessionid = responseuri.Substring(responseuri.LastIndexOf("jsessionid=") + "jsessionid=".Length);
                
                    //send the info to the rest serivice so that the cookie can get authenticated.
                    Uri postUri = new Uri(uriStr + "j_spring_security_check");
                    HttpWebRequest httpPost = (HttpWebRequest)HttpWebRequest.Create(postUri);

                    httpPost.CookieContainer = cookieContainer;
                    httpPost.ContentType = "application/x-www-form-urlencoded";
                    httpPost.Method = "POST";
                
                    //Use the bog standard Auth request with our new cookie so that we can deny or allow the user to go past this point.
                    String _authString = "j_username=" + username + "%40rockstar.t2.corp&j_password=" + password;
                    Byte[] _outBuffer = System.Text.Encoding.ASCII.GetBytes(_authString);  //store in byte buffer           
                    Stream _str = httpPost.GetRequestStream();
                    _str.Write(_outBuffer, 0, _outBuffer.Length); //update form
                    _str.Close();
                    HttpWebResponse PostLoginResponse = (HttpWebResponse)httpPost.GetResponse(); // get the request response (should say if we get an login error "login.jsp?login_error=1")
                    if (PostLoginResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                    {
                        //Deny the user access and prompt for password/user change
                        MessageBox.Show("Username or password Incorrect", "Important Message");
                        BtnLogin.Enabled = true;
                        TBUser.Enabled = true;
                        TBPassword.Enabled = true;
                        BTNReport.Enabled = false;
                        httpProjectGet.Abort();
                        httpPost.Abort();
                    }
                    else
                    {
                        //Allow the user access then poll the server to see if we get a response with our new cookie
                        var strBytearray = System.Text.Encoding.Default.GetString(_outBuffer);
                        Console.WriteLine(strBytearray);

                        HttpWebResponse postResponse = (HttpWebResponse)httpPost.GetResponse();
                        Stream postStream = postResponse.GetResponseStream();

                        StreamReader poststr = new StreamReader(postStream);

                        Console.WriteLine(poststr.ReadToEnd());

                        postResponseUri = postResponse.ResponseUri.OriginalString;
                        String postSessionId = postResponseUri.Substring(postResponseUri.LastIndexOf("jsessionid=") + "jsessionid=".Length);
                        String responseCode = postResponse.StatusCode.ToString();
                        Console.WriteLine(responseCode);

                   
                        StreamReader str = new StreamReader(postStream);
                        Console.WriteLine(str.ReadToEnd());

                        Uri projectUri = new Uri(uriStr + "rest/Projects/1546/CurrentUser");
                        HttpWebRequest httpPrjGet = (HttpWebRequest)HttpWebRequest.Create(projectUri);
                        httpPrjGet.Method = "GET";
                        httpPrjGet.CookieContainer = cookieContainer;

                        HttpWebResponse getPrjResponse = (HttpWebResponse)httpPrjGet.GetResponse();
                        Stream projectStream = getPrjResponse.GetResponseStream();
                        StreamReader prjstr = new StreamReader(projectStream);
                        //redundant try catch for checking if the user login has failed ... can never be too careful!
                        string OutputPrjstr = prjstr.ReadToEnd();
                        if (postResponseUri.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                        {
                            MessageBox.Show("Username or password Incorrect", "Important Message");
                            BtnLogin.Enabled = true;
                            TBUser.Enabled = true;
                            TBPassword.Enabled = true;
                            BTNReport.Enabled = false;
                        }
                        else
                        {
                            paused = false;
                            timer1.Enabled = true;
                            BtnLogin.Enabled = false;
                            TBUser.Enabled = false;
                            TBPassword.Enabled = false;
                            BTNReport.Enabled = true;
                            minutes = 60;

                        }
                    }
                }
                catch
                {
                    MessageBox.Show("B* rest services are down, please try again later.", "Important Message");
                    BtnLogin.Enabled = true;
                    TBUser.Enabled = true;
                    TBPassword.Enabled = true;
                    BTNReport.Enabled = false;
                    httpProjectGet.Abort();
                }
            }
            else
            {
            }

        }

        // every 60 minutes or so renew the cookie.
        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((minutes == 0) && (hours == 0) && (seconds == 0))
            {
                Console.WriteLine("timer ended");
                paused = true;
                timer1.Enabled = false;
                BtnLogin.Enabled = true;
                TBUser.Enabled = true;
                TBPassword.Enabled = true;
                
                BTNReport.Enabled = false;
            }
            else
            {
                if (seconds < 1)
                {
                    seconds = 60;
                    if (minutes == 0) { minutes = 59; } else { minutes -= 1; }
                }
                else
                {
                    seconds -= 1;
                }
            }

            if ((minutes == 10) && (hours == 0) && (seconds == 0) && timer1.Enabled == true)
            {
                GetBugInfo(2030358);
                minutes = 60;
            }

            
        }
        /// <summary>
        /// This function retrieves the bug summary from an xml when pulling information from the b* rest services)
        /// </summary>
        public string GetBugInfo(int BugNumber)
        {
            String BugDetails = "";
            //make sure the bugnumber is valid first
            if (BugNumber != 0)
            {
                Uri bugUri = new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Bugs/" + BugNumber);//give rest the bug Url
                HttpWebRequest httpPrjGet = (HttpWebRequest)HttpWebRequest.Create(bugUri);
                httpPrjGet.Method = "GET";
                httpPrjGet.CookieContainer = cookieContainer;
                // get the response stream in xml form (after checking the returned response url is ok)
                try{
                    if (!httpPrjGet.GetResponse().ResponseUri.OriginalString.Contains("login.jsp?login_error=1"))
                    {
                        HttpWebResponse getBugResponse = (HttpWebResponse)httpPrjGet.GetResponse();
                        HttpStatusCode responseStatusCode = getBugResponse.StatusCode;
                        if (getBugResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                        {
                            MessageBox.Show("Username or password Incorrect", "Important Message");
                            BtnLogin.Enabled = true;
                            TBUser.Enabled = true;
                            TBPassword.Enabled = true;
                            BTNReport.Enabled = false;
                            httpPrjGet.Abort();
                            timer1.Enabled = false;
                            minutes = 0;
                            seconds = 0;
                            return "";
                        }
                        else
                        {
                            //get the bug info from the html stream coming from rest urlresponse.
                            Stream BugStream = getBugResponse.GetResponseStream();
                            StreamReader str = new StreamReader(BugStream);
                            // write info to string
                            BugDetails = str.ReadToEnd();
                        }
                    }
                }catch{

                }
            }
            return BugDetails;
        }

        public string ListToXMLString(List<int> intList)
        {
            string XmlString = "";
            XmlString = "<BugIds>";
            foreach (int ListItem in intList)
            {
                XmlString += "<id>" + ListItem + "</id>";
            }
            XmlString += "</BugIds>";

            return XmlString;
        }

        public string GetBugInfoViaXML(string xmlList)
        {
            String BugDetails = "";
            //make sure the bugnumber is valid first
                //send the info to the rest serivice so that the cookie can get authenticated.
                Uri postUri = new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Bugs/BugsById?fields=Id,Summary,Category,Project,State");
                HttpWebRequest httpPost = (HttpWebRequest)HttpWebRequest.Create(postUri);
                
                httpPost.ContentType = "application/xml";
                httpPost.Method = "POST";
                httpPost.CookieContainer = cookieContainer;


                byte[] postData = Encoding.UTF8.GetBytes(xmlList);    
                httpPost.ContentLength = postData.Length;
                Stream dataStream = httpPost.GetRequestStream();
                dataStream.Write(postData, 0, postData.Length);
                dataStream.Close();


                
                try
                {
                    HttpWebResponse PostLoginResponse = (HttpWebResponse)httpPost.GetResponse();
                    if (!httpPost.GetResponse().ResponseUri.OriginalString.Contains("login.jsp?login_error=1"))
                    {
                        HttpWebResponse getBugResponse = (HttpWebResponse)httpPost.GetResponse();
                        HttpStatusCode responseStatusCode = getBugResponse.StatusCode;
                        if (getBugResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                        {
                            MessageBox.Show("Username or password Incorrect", "Important Message");
                            BtnLogin.Enabled = true;
                            TBUser.Enabled = true;
                            TBPassword.Enabled = true;
                            BTNReport.Enabled = false;
                            httpPost.Abort();
                            timer1.Enabled = false;
                            minutes = 0;
                            seconds = 0;
                            return "";
                        }
                        else
                        {
                            //get the bug info from the html stream coming from rest urlresponse.
                            Stream BugStream = getBugResponse.GetResponseStream();
                            StreamReader str = new StreamReader(BugStream);
                            // write info to string
                            BugDetails = str.ReadToEnd();
                        }
                    }
                }
                catch
                {

                }
              return BugDetails;
        }

        public enum BugClassEnum { A, B, C, D, TASK, TODO, WISH, TRACK};

        public void GetBugnumbersfromfields()
        {
            bugstarNumbers.Clear();
            string[] rageArgs;
            string[] textArgs;
            string[] sourceArgs;
            string[] dataArgs;
            string[] scriptArgs;
            // read the arguments into a string to be provited to the batfile to retrieve the bug descriptions
            if (CB_TU_MNU.Checked == true)
            {
                File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt").Close();
                rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//rage/gta5/dev_ng/" };// test2.txt is superfluious just now
                textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/assets_ng/gametext/dev_ng/" };
                sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/src/dev_ng/" };
                if (CB_JPN.Checked)
                {
                    dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/Japanese_ng/" };
                }
                else
                {
                    dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/dev_ng/" };
                }

                scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/script/dev_ng/" };
            }
            else
            {
                if (CB_NGLIVE.Checked == true)
                {
                    File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt").Close();
                    rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//rage/gta5/dev_ng_live/" };// test2.txt is superfluious just now
                    textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/assets_ng/gametext/dev_ng_live/" };
                    sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/src/dev_ng_live/" };
                    if (CB_JPN.Checked)
                    {
                        dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/Japanese_ng_Live/" };
                    }
                    else
                    {
                        dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/dev_ng_live/" };
                    }
                    scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/script/dev_ng_live/" };
                }
                else
                {
                    File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt").Close();
                    rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//rage/gta5/dev_temp/" };// test2.txt is superfluious just now
                    textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/assets_ng/gametext/dev_temp/" };
                    sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/src/dev_temp/" };
                    if (CB_JPN.Checked)
                    {
                        dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/Japanese_ng_Live/" };
                    }
                    else
                    {
                        dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/dev_temp/" };
                    }
                    scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/script/dev_temp/" };
                }

            }
            ProcessStartInfo SourceStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", sourceArgs));//prep the process with the args above.
            ProcessStartInfo RageStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", rageArgs));
            ProcessStartInfo ScriptStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", scriptArgs));
            ProcessStartInfo TextStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", textArgs));
            ProcessStartInfo DataStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", dataArgs));
            Process SourceGrab = new Process();//assign the process (so we can track this later)
            Process RageGrab = new Process();
            Process ScriptGrab = new Process();
            Process TextGrab = new Process();
            Process DataGrab = new Process();

            if (TB_Rage1.Text != TB_Rage2.Text)
            {
                RageGrab.StartInfo = RageStartInfo;
                RageGrab.Start();//start the process (i.e. the batfile which outputs the data to the textfile)
                RageGrab.WaitForExit(20000);//give the process time to complete
            }
            if (TB_Src1.Text != TB_Src2.Text)
            {
                SourceGrab.StartInfo = SourceStartInfo;
                SourceGrab.Start();
                SourceGrab.WaitForExit(20000);

            }
            if (TB_Text1.Text != TB_Text2.Text)
            {
                TextGrab.StartInfo = TextStartInfo;
                TextGrab.Start();
                TextGrab.WaitForExit(20000);

            }
            if (TB_Data1.Text != TB_Data2.Text)
            {
                DataGrab.StartInfo = DataStartInfo;
                DataGrab.Start();
                DataGrab.WaitForExit(20000);

            }
            if (TB_Script1.Text != TB_Script2.Text)
            {
                ScriptGrab.StartInfo = ScriptStartInfo;
                ScriptGrab.Start();
                ScriptGrab.WaitForExit(20000);
            }

            //retrieving the changelists B* numbers from the "Extra text field in CL-B* tab
            List<string> otherCLList = new List<string>();
            //regeg pattern for CL numbers (will need to change once we beat through 5-6, potentially this need to be exposed in the xml document so it can be set.
            otherCLList = RegexPatternRecogniser(TB_Extra.Text, "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])|([5-9][0-9][0-9][0-9][0-9][0-9][0-9])", 100);
            // go through each changlist in the list provided from the above command
            foreach (string CLNumber in otherCLList)
            {
                if (CLNumber != null && CLNumber != "")
                {
                    if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt"))
                    {
                        // get the descriptions of the individual changelists from p4
                        ProcessStartInfo otherCLListStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLIndividualExtractor.bat", CLNumber);
                        Process otherCLListP = new Process();
                        otherCLListP.StartInfo = otherCLListStartInfo;
                        otherCLListP.Start();
                        otherCLListP.WaitForExit(20000);
                    }
                }
            }
            //read up to this point for the output of the textfile.
            SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt");

            // Find all the B* numbers within the output generated from the file.
            string pattern = @"\b[2-4][0-9][0-9][0-9][0-9][0-9][0-9]\b";
            // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

            Regex rx = new Regex(pattern, RegexOptions.None);
            MatchCollection mc = rx.Matches(SrcCLOutput);
            //   string[] test = new string[300];
            string[] EmailString = new string[300];
            //  int i = 0;

            foreach (Match m in mc)
            {
                if (CheckIntListContains(Convert.ToInt32(m.Value), bugstarNumbers))
                {
                    //test[i] = "url:bugstar:" + m.Value;
                    bugstarNumbers.Add(Convert.ToInt32(m.Value));
                    // i++;
                }
            }
        }
        

        /// <summary>
        /// Uses the New and old shangelists to get the descriptions for each changelist within a branch then filters out the B* numbers from that list using a regular expression.
        /// Once we have the list of B* numbers we then poll B* rest services for the description using GetBugInfo()
        /// After all the data has been processed we collate the information into a text document and email using CreateOutlookEmail()
        /// </summary>
        private void BTNReport_Click(object sender, EventArgs e)
        {
            
            //prompt the user as this will render the program inoperable until it completes its task.
            DialogResult dialogResult = MessageBox.Show("Do you wish to continue? you will not be able to interact with the tool until the report is complete. ", "Important", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // start the email string
                BugStringBuilder = new StringBuilder();
                BugStringBuilder.Append("Updated: <br><br>");
                BugStringBuilder.Append("•	Streamed prebuild updated <br>");
                BugStringBuilder.Append("•	Deployable PS4/XboxOne prebuilds updated <br>");
                BugStringBuilder.Append(@"o	N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD\dev_ng <br>");
                BugStringBuilder.Append(@"o	N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build\dev_ng <br><br>");
                BugStringBuilder.Append("Rage: " + TB_Rage2.Text + "<br>");
                BugStringBuilder.Append("Src: " + TB_Src2.Text + "<br>");
                BugStringBuilder.Append("Script: " + TB_Script2.Text + "<br>");
                BugStringBuilder.Append("Text: " + TB_Text2.Text + "<br>");
                if (TB_Extra.Text != "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data2.Text + "<br>");
                    BugStringBuilder.Append("Added: " + TB_Extra.Text + "<br><br>");
                }
                else if (TB_Extra.Text == "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data2.Text + "<br><br>");
                }
                BugStringBuilder.Append("Fixes: <br>");
                BugStringBuilder.Append("<ul>");// end the email string just after Fixes 

                GetBugnumbersfromfields();
                
                //getting the bug details from bugstar list.
                CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml");
                XmlBugList = "";
                XmlBugList = ListToXMLString(bugstarNumbers);
                System.IO.File.WriteAllText(@"X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", GetBugInfoViaXML(XmlBugList));
                BugList = GetFieldValuesfromXMLList ("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml",BugList);

                //System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", test);
                CheckDeleteandCreateFile(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt");
                bool hasAClass = false, hasBClass = false, hasCClass = false, hasDClass = false, hasTaskClass = false, hasToDoClass = false, hasWishClass = false, hasTrackClass = false;
                BugList = BugList.OrderBy(o => o.bugclass).ThenBy(o => o.summary).ToList();
                string CLListHelper = "Rage :" + TB_Rage2.Text + "\n" + "Source: " + TB_Src2.Text + "\n" + "Script: " + TB_Script2.Text + "\n" + "Text: " + TB_Text2.Text + "\n" + "Data: " + TB_Data2.Text + "\n" + "\n";
                System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", CLListHelper);
                foreach (Bug bug in BugList)
                {
                    if (bug.project != "69367" && bug.status!="DEV")
                    {
                        //For creating the subheadings within the report
                        if ((bug.bugclass == BugClassEnum.A.ToString()) & !(hasAClass == true))
                        {
                            //Adding subheadings 
                            hasAClass = true;
                            BugStringBuilder.Append("A Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "A Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.B.ToString()) & !(hasBClass == true))
                        {
                            hasBClass = true;
                            BugStringBuilder.Append("B Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "B Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.C.ToString()) & !(hasCClass == true))
                        {
                            hasCClass = true;
                            BugStringBuilder.Append("C Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "C Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.D.ToString()) & !(hasDClass == true))
                        {
                            hasDClass = true;
                            BugStringBuilder.Append("D Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "D Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TASK.ToString()) & !(hasTaskClass == true))
                        {
                            hasTaskClass = true;
                            BugStringBuilder.Append("Task Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Task Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TODO.ToString()) & !(hasToDoClass == true))
                        {
                            hasToDoClass = true;
                            BugStringBuilder.Append("ToDo Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "ToDo Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.WISH.ToString()) & !(hasWishClass == true))
                        {
                            hasWishClass = true;
                            BugStringBuilder.Append("Wish Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Wish Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TRACK.ToString()) & !(hasTrackClass == true))
                        {
                            hasTrackClass = true;
                            BugStringBuilder.Append("Track Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Track Class \n");
                        }
                        //End of creating the subheadings within the report

                        //assign each string line to the bug details from the bug class.
                        BugStringBuilder.Append("<li><a href='" + "url:Bugstar:" + bug.bugNumber + "'>" + bug.bugNumber + "</a>" + " - " + bug.summary + "</li>");
                        string TextBugHelper = "url:Bugstar:" + bug.bugNumber + " - " + bug.summary + "\n";
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", TextBugHelper);
                    }

                }
                hasAClass = false; hasBClass = false; hasCClass = false; hasDClass = false; hasTaskClass = false; hasToDoClass = false; hasWishClass = false; hasTrackClass = false;

                // write all data to the textfile
                //System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", test);
                Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BugOutput.txt");
                //System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", ListToXMLString(bugstarNumbers));
                BugStringBuilder.Append("<ul>");
                //Create the email.
                                
                try
                {
                    CreateOutlookEmail();
                }
                catch { }
                BugList.Clear();
                //bugstarNumbers.Clear();

            }
            else if (dialogResult == DialogResult.No)
            {
                BugList.Clear();
            }
        }

    

        /*
        private void BTNReport_Click(object sender, EventArgs e)
        {
            //prompt the user as this will render the program inoperable until it completes its task.
            DialogResult dialogResult = MessageBox.Show("Do you wish to continue? you will not be able to interact with the tool until the report is complete. ", "Important", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // start the email string
                BugStringBuilder = new StringBuilder();
                BugStringBuilder.Append("Hi, <br><br>");
                BugStringBuilder.Append("The NG_TU Prebuild has been updated in Perforce and Distribution <br><br>");
                BugStringBuilder.Append("Rage: " + TB_Rage.Text + "<br>");
                BugStringBuilder.Append("Src: " + TB_Src.Text + "<br>");
                BugStringBuilder.Append("Script: " + TB_Script.Text + "<br>");
                BugStringBuilder.Append("Text: " + TB_Text.Text + "<br>");
                if (TB_Extra.Text != "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data.Text + "<br>");
                    BugStringBuilder.Append("Added: " + TB_Extra.Text + "<br><br>");
                }
                else if (TB_Extra.Text == "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data.Text + "<br><br>");
                }
                BugStringBuilder.Append("Fixes: <br>");
                BugStringBuilder.Append("<ul>");// end the email string just after Fixes 
                string[] rageArgs;
                string[] textArgs;
                string[] sourceArgs;
                string[] dataArgs;
                string[] scriptArgs;
                // read the arguments into a string to be provited to the batfile to retrieve the bug descriptions
                if (CB_Temp.Checked == false)
                {
                    File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt").Close();
                    rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//rage/gta5/dev_ng/" };// test2.txt is superfluious just now
                    textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/assets_ng/GameText/" };
                    sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/src/dev_ng/" };
                    dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/titleupdate/dev_ng/" };
                    scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/script/dev_ng/" };
                }
                else
                {
                    File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt").Close();
                    rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//rage/gta5/dev_temp/" };// test2.txt is superfluious just now
                    textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/assets_ng/GameText_Temp/" };
                    sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/src/dev_temp/" };
                    dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/titleupdate/dev_temp/" };
                    scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\Test2.txt", "//depot/gta5/script/dev_temp/" };

                }
                ProcessStartInfo SourceStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", sourceArgs));//prep the process with the args above.
                ProcessStartInfo RageStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", rageArgs));
                ProcessStartInfo ScriptStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", scriptArgs));
                ProcessStartInfo TextStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", textArgs));
                ProcessStartInfo DataStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLExtractor.bat", string.Join(" ", dataArgs));
                Process SourceGrab = new Process();//assign the process (so we can track this later)
                Process RageGrab = new Process();
                Process ScriptGrab = new Process();
                Process TextGrab = new Process();
                Process DataGrab = new Process();

                if (TB_Rage1.Text != TB_Rage2.Text)
                {
                    RageGrab.StartInfo = RageStartInfo;
                    RageGrab.Start();//start the process (i.e. the batfile which outputs the data to the textfile)
                    RageGrab.WaitForExit(20000);//give the process time to complete
                }
                if (TB_Src1.Text != TB_Src2.Text)
                {
                    SourceGrab.StartInfo = SourceStartInfo;
                    SourceGrab.Start();
                    SourceGrab.WaitForExit(20000);

                }
                if (TB_Text1.Text != TB_Text2.Text)
                {
                    TextGrab.StartInfo = TextStartInfo;
                    TextGrab.Start();
                    TextGrab.WaitForExit(20000);

                }
                if (TB_Data1.Text != TB_Data2.Text)
                {
                    DataGrab.StartInfo = DataStartInfo;
                    DataGrab.Start();
                    DataGrab.WaitForExit(20000);

                }
                if (TB_Script1.Text != TB_Script2.Text)
                {
                    ScriptGrab.StartInfo = ScriptStartInfo;
                    ScriptGrab.Start();
                    ScriptGrab.WaitForExit(20000);
                }

                //retrieving the changelists B* numbers from the "Extra text field in CL-B* tab
                string[] otherCLList = new string[100];
                //regeg pattern for CL numbers (will need to change once we beat through 5-6, potentially this need to be exposed in the xml document so it can be set.
                otherCLList = RegexPatternRecogniser(TB_Extra.Text, "[5-6][0-9][0-9][0-9][0-9][0-9][0-9]", 100);
                // go through each changlist in the list provided from the above command
                foreach (string CLNumber in otherCLList)
                {
                    if (CLNumber != null && CLNumber != "")
                    {
                        if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt"))
                        {
                            // get the descriptions of the individual changelists from p4
                            ProcessStartInfo otherCLListStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\CLIndividualExtractor.bat", CLNumber);
                            Process otherCLListP = new Process();
                            otherCLListP.StartInfo = otherCLListStartInfo;
                            otherCLListP.Start();
                            otherCLListP.WaitForExit(20000);
                        }
                    }
                }
                //read up to this point for the output of the textfile.
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt");

                // Find all the B* numbers within the output generated from the file.
                string pattern = "[0-4][0-9][0-9][0-9][0-9][0-9][0-9]";
                // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                //   string[] test = new string[300];
                string[] EmailString = new string[300];
                List<int> bugstarNumbers = new List<int>();
                //  int i = 0;

                foreach (Match m in mc)
                {
                    if (CheckIntListContains(Convert.ToInt32(m.Value), bugstarNumbers))
                    {
                        //test[i] = "url:bugstar:" + m.Value;
                        bugstarNumbers.Add(Convert.ToInt32(m.Value));
                        // i++;
                    }
                }


                // Go through the list of bugnumbers and append the bug descriptions to the bugnumbers for both the text file and email.
                foreach (int bugNumber in bugstarNumbers)
                {
                    if (bugNumber != 0)
                    {
                        //Create the bugdetails xml
                        CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml");
                        //Get the bug info from the GetBugInfo()
                        string BugInfo = GetBugInfo(bugNumber);
                        if (BugInfo != "")// gets the bug information and if its not empty
                        {
                            //Write the info to the xmlfile
                            System.IO.File.WriteAllText(@"X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", BugInfo);
                            //Get the summary and class fields from the xmldata
                            string BugClass = GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", "bug", "category");
                            string Bugdescription2 = GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", "bug", "summary");
                            //Add a new "Bug" with a B* number, Description and bug class
                            Bug TempBug = new Bug(bugNumber, Bugdescription2, BugClass);
                            //add the bug to the list
                            BugList.Add(TempBug);
                        }
                    }
                }
                //System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", test);
                CheckDeleteandCreateFile(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt");
                bool hasAClass = false, hasBClass = false, hasCClass = false, hasDClass = false, hasTaskClass = false, hasToDoClass = false, hasWishClass = false, hasTrackClass = false;
                BugList = BugList.OrderBy(o => o.bugclass).ThenBy(o => o.summary).ToList();
                string CLListHelper = "Rage :" + TB_Rage2.Text + "\n" + "Source: " + TB_Src2.Text + "\n" + "Script: " + TB_Script2.Text + "\n" + "Text: " + TB_Text2.Text + "\n" + "Data: " + TB_Data2.Text + "\n" + "\n";
                System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", CLListHelper);
                foreach (Bug bug in BugList)
                {
                    //For creating the subheadings within the report
                    if ((bug.bugclass == BugClassEnum.A.ToString()) & !(hasAClass == true))
                    {
                        //Adding subheadings 
                        hasAClass = true;
                        BugStringBuilder.Append("A Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "A Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.B.ToString()) & !(hasBClass == true))
                    {
                        hasBClass = true;
                        BugStringBuilder.Append("B Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "B Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.C.ToString()) & !(hasCClass == true))
                    {
                        hasCClass = true;
                        BugStringBuilder.Append("C Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "C Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.D.ToString()) & !(hasDClass == true))
                    {
                        hasDClass = true;
                        BugStringBuilder.Append("D Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "D Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.TASK.ToString()) & !(hasTaskClass == true))
                    {
                        hasTaskClass = true;
                        BugStringBuilder.Append("Task Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Task Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.TODO.ToString()) & !(hasToDoClass == true))
                    {
                        hasToDoClass = true;
                        BugStringBuilder.Append("ToDo Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "ToDo Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.WISH.ToString()) & !(hasWishClass == true))
                    {
                        hasWishClass = true;
                        BugStringBuilder.Append("Wish Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Wish Class \n");
                    }
                    if ((bug.bugclass == BugClassEnum.TRACK.ToString()) & !(hasTrackClass == true))
                    {
                        hasTrackClass = true;
                        BugStringBuilder.Append("Track Class");
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", "Track Class \n");
                    }
                    //End of creating the subheadings within the report

                    //assign each string line to the bug details from the bug class.
                    BugStringBuilder.Append("<li><a href='" + "url:Bugstar:" + bug.bugNumber + "'>" + bug.bugNumber + "</a>" + " - " + bug.summary + "</li>");
                    string TextBugHelper = "url:Bugstar:" + bug.bugNumber + " - " + bug.summary + "\n";
                    System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", TextBugHelper);

                }
                hasAClass = false; hasBClass = false; hasCClass = false; hasDClass = false; hasTaskClass = false; hasToDoClass = false; hasWishClass = false; hasTrackClass = false;

                // write all data to the textfile
                //System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", test);
                Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BugOutput.txt");
                System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BugOutput.txt", ListToXMLString(bugstarNumbers));
                BugStringBuilder.Append("<ul>");
                //Create the email.
                XmlBugList = "";
                XmlBugList = ListToXMLString(bugstarNumbers);


                try
                {
                    CreateOutlookEmail();
                }
                catch { }
                BugList.Clear();
                bugstarNumbers.Clear();

            }
            else if (dialogResult == DialogResult.No)
            {
                BugList.Clear();
            }

        }

        */

        /// <summary>
        /// returns an array of strings from the input provided
        /// </summary>

        public List<string>RegexPatternRecogniser(string input, string regexPattern, int arrayLength)
        {
            List<string> returnValues = new List<string>();
            int i = 0;
            string pattern = regexPattern;
            Regex rx = new Regex(pattern, RegexOptions.None);
            MatchCollection mc = rx.Matches(input);
            foreach (Match m in mc)
            {
                returnValues.Add(m.Value);
                i++;
            }
            return returnValues;
        }

        private void BTNCheck_Click(object sender, EventArgs e)
        {

        }

        private void CBDev_Net_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CB_Force_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_Force.Checked)
            {
                ForceChecked = "-f";
            }
            else
            {
                ForceChecked = "";
            }
        }

        private void CBXlast_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CheckDeleteandCreateFile(string file)
        {
            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
            System.IO.File.Create(file).Dispose();

        }

        private void BuildDeploymentMenu_Load(object sender, EventArgs e)
        {
            List<CheckBox> CheckBoxList = new List<CheckBox>();
            CheckBoxList.Add(CB_NGLIVE_MNU);
            CheckBoxList.Add(CB_Temp_MNU);
            CheckBoxList.Add(CB_TU_MNU);

            XB1PlatformList = new List<CheckBox>();
            PS4PlatformList = new List<CheckBox>();
            X64PlatformList = new List<CheckBox>();
            BuildTypeList = new List<RadioButton>();
            ExecutableList = new List<CheckBox>();
            AutoBuildOptions = new List<CheckBox>();
            ShaderBuildOptions = new List<CheckBox>();
            AutoBuildOptions.Add(CB_O_Dev_S);
            AutoBuildOptions.Add(CB_O_Final_S);
            AutoBuildOptions.Add(CB_O_Beta);
            AutoBuildOptions.Add(CB_O_BankRelease);
            AutoBuildOptions.Add(CB_O_Release);
            AutoBuildOptions.Add(CB_O_Final);
            AutoBuildOptions.Add(CB_O_Master);
            AutoBuildOptions.Add(CB_D_Dev_S);
            AutoBuildOptions.Add(CB_D_Final_S);
            AutoBuildOptions.Add(CB_D_Beta);
            AutoBuildOptions.Add(CB_D_BankRelease);
            AutoBuildOptions.Add(CB_D_Release);
            AutoBuildOptions.Add(CB_D_Final);
            AutoBuildOptions.Add(CB_D_Master);
            AutoBuildOptions.Add(CB_Y_Win32_40_Dev);
            AutoBuildOptions.Add(CB_Y_Win32_40_Final);
            AutoBuildOptions.Add(CB_Y_Win32_30_Dev);
            AutoBuildOptions.Add(CB_Y_Win32_30_Final);
            AutoBuildOptions.Add(CB_Y_Win32_40_lq_Dev);
            AutoBuildOptions.Add(CB_Y_Win32_nvstereo_Dev);
            AutoBuildOptions.Add(CB_Y_Beta);
            AutoBuildOptions.Add(CB_Y_BankRelease);
            AutoBuildOptions.Add(CB_Y_Release);
            AutoBuildOptions.Add(CB_Y_Final);
            AutoBuildOptions.Add(CB_Y_Master);
            PS4PlatformList.Add(CB_O_Beta);
            PS4PlatformList.Add(CB_O_BankRelease);
            PS4PlatformList.Add(CB_O_Release);
            PS4PlatformList.Add(CB_O_Final);
            PS4PlatformList.Add(CB_O_Master);
            XB1PlatformList.Add(CB_D_Beta);
            XB1PlatformList.Add(CB_D_BankRelease);
            XB1PlatformList.Add(CB_D_Release);
            XB1PlatformList.Add(CB_D_Final);
            XB1PlatformList.Add(CB_D_Master);
            X64PlatformList.Add(CB_Y_Beta);
            X64PlatformList.Add(CB_Y_BankRelease);
            X64PlatformList.Add(CB_Y_Release);
            X64PlatformList.Add(CB_Y_Final);
            X64PlatformList.Add(CB_Y_Master);
            BuildTypeList.Add(RB_Build);
            BuildTypeList.Add(RB_Clean);
            BuildTypeList.Add(RB_Rebuild);
            ShaderBuildOptions.Add(CB_O_Dev_S);
            ShaderBuildOptions.Add(CB_O_Final_S);
            ShaderBuildOptions.Add(CB_D_Dev_S);
            ShaderBuildOptions.Add(CB_D_Final_S);
            ShaderBuildOptions.Add(CB_Y_Win32_40_Dev);
            ShaderBuildOptions.Add(CB_Y_Win32_40_Final);
            ShaderBuildOptions.Add(CB_Y_Win32_30_Dev);
            ShaderBuildOptions.Add(CB_Y_Win32_30_Final);
            ShaderBuildOptions.Add(CB_Y_Win32_40_lq_Dev);
            ShaderBuildOptions.Add(CB_Y_Win32_nvstereo_Dev);


            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\UserSettings.xml"))
            {
                foreach (CheckBox cb in CheckBoxList)
                {
                    if (GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\UserSettings.xml", "UserData", "BranchSelected") == cb.Name)
                    {
                        cb.Checked = true;
                    }
                    if (GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\UserSettings.xml", "UserData", "BranchSelected") == cb.Name + "_" + CB_JPN.Name)
                    {
                        cb.Checked = true;
                        CB_JPN.Checked = true;
                    }
                }
            }
            else
            {
                System.IO.File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\UserSettings.xml");
            }
            

            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "PreBuild") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_PreBuild);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Release") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Release);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Utilities") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Utilities);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Check_Out") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_CheckOut);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Sync") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Sync);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "CLB") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_CLTOB); 
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "TUPreBuild") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_PreBuild_TU);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "TURelease") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Release_TU);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Patches") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Patches);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Compiler") == "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Remove(TP_Compilers);
            }


            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\CLList.txt"))
            {
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\CLList.txt");

                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])|([5-9][0-9][0-9][0-9][0-9][0-9][0-9])";
                // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[1500];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                        case 0:
                            TB_Rage.Text = m.Value;
                            TB_Rage2.Text = m.Value;
                            break;
                        case 1:
                            TB_Src.Text = m.Value;
                            TB_Src2.Text = m.Value;
                            break;
                        case 2:
                            TB_Script.Text = m.Value;
                            TB_Script2.Text = m.Value;
                            break;
                        case 3:
                            TB_Text.Text = m.Value;
                            TB_Text2.Text = m.Value;
                            break;
                        case 4:
                            TB_Data.Text = m.Value;
                            TB_Data2.Text = m.Value;
                            break;
                        case 5:
                            TB_Export.Text = m.Value;
                            break;
                        case 6:
                            TB_Metadata.Text = m.Value;
                            break;
                    }
                    i++;
                }
                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx2 = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
                MatchCollection mc2 = rx2.Matches(SrcCLOutput);
                test = new string[1500];

                foreach (Match m in mc2)
                {
                    LBL_TimeStamp2.Text = m.Value;
                }

            }
            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\OldCLList.txt"))
            {
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\OldCLList.txt");

                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])|([5-9][0-9][0-9][0-9][0-9][0-9][0-9])";
                // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[1500];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                        case 0:
                            TB_Rage1.Text = m.Value;
                            break;
                        case 1:
                            TB_Src1.Text = m.Value;
                            break;
                        case 2:
                            TB_Script1.Text = m.Value;
                            break;
                        case 3:
                            TB_Text1.Text = m.Value;
                            break;
                        case 4:
                            TB_Data1.Text = m.Value;
                            break;
                    }
                    i++;
                }

                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx3 = new Regex(pattern, RegexOptions.None);
                MatchCollection mc3 = rx3.Matches(SrcCLOutput);
                test = new string[1500];

                foreach (Match m in mc3)
                {
                    LBL_TimeStamp1.Text = m.Value;
                }

            }
            ChangeBuildToolTabEnabled();

        }

        private void CBManualEntry_CheckedChanged(object sender, EventArgs e)
        {
            if (TB_Rage1.Enabled)
            {
                toggled = false;
            }
            else
            {
                toggled = true;
            }
            TB_Rage1.Enabled = toggled;
            TB_Rage2.Enabled = toggled;
            TB_Src1.Enabled = toggled;
            TB_Src2.Enabled = toggled;
            TB_Script1.Enabled = toggled;
            TB_Script2.Enabled = toggled;
            TB_Text1.Enabled = toggled;
            TB_Text2.Enabled = toggled;
            TB_Data1.Enabled = toggled;
            TB_Data2.Enabled = toggled;
            TB_Extra.Enabled = toggled;
        }

        private void BTN_Check_Out_Click(object sender, EventArgs e)
        {
            if (RB_NG_TitleUpdate.Checked)
            {
                if (CB_Durango_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Durango_TU" + Japanese);
                }
                if (CB_Orbis_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Orbis_TU" + Japanese);
                }
                if (CB_X64_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "X64_TU" + Japanese);
                }
                if (CB_MasterExes.Checked)
                {
                    string peramiters;
                    string ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    if (CB_Durango_Exes.Checked)
                    {
                        peramiters ="Durango_TU_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_Orbis_Exes.Checked)
                    {
                        peramiters ="Orbis_TU_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_X64_Exes.Checked)
                    {
                       //peramiters ="X64_TU_Masters" + Japanese;
                       //Process.Start(ProcessString, peramiters);
                    }
                    
                }
                if (CB_Default.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Default_TU" + Japanese);
                }
                if (CB_Clifford.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Clifford" + Japanese);
                }
                if (CB_Version.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Version");
                    Process.Start(ProcessString, "Version_TU" + Japanese);
                }
                if (CB_Durango.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_TU" + Japanese);
                }
                if (CB_Orbis.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_TU" + Japanese);
                }
                if (CB_Win32_30.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_TU" + Japanese);
                }
                if (CB_Win32_40.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_TU" + Japanese);
                }
                if (CB_Win32_40_lq.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_lq_TU" + Japanese);
                }
                if (CB_win32_nvstereo.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "win32_nvstereo_TU" + Japanese);
                }
                if (CB_Durango_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_Final_TU" + Japanese);
                }
                if (CB_Orbis_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_Final_TU" + Japanese);
                }
                if (CB_Win32_30_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_Final_TU" + Japanese);
                }
                if (CB_Win32_40_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_Final_TU" + Japanese);
                }
            }
            if (RB_Dev_NG.Checked)
            {
                if (CB_Durango_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Durango" + Japanese);
                }
                if (CB_Orbis_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Orbis" + Japanese);
                }
                if (CB_X64_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "X64" + Japanese);
                }
                if (CB_MasterExes.Checked)
                {
                    string peramiters;
                    string ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    if (CB_Durango_Exes.Checked)
                    {
                        peramiters = "Durango_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_Orbis_Exes.Checked)
                    {
                        peramiters = "Orbis_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_X64_Exes.Checked)
                    {
                        //peramiters ="X64_TU_Masters" + Japanese;
                        //Process.Start(ProcessString, peramiters);
                    }

                }
                if (CB_Default.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Default" + Japanese);
                }
                if (CB_Clifford.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Clifford" + Japanese);
                }
                if (CB_Version.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Version");
                    Process.Start(ProcessString, "Version" + Japanese);
                }
                if (CB_Durango.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango" + Japanese);
                }
                if (CB_Orbis.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis" + Japanese);
                }
                if (CB_Win32_30.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30" + Japanese);
                }
                if (CB_Win32_40.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40" + Japanese);
                }
                if (CB_Win32_40_lq.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_lq" + Japanese);
                }
                if (CB_win32_nvstereo.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "win32_nvstereo" + Japanese);
                }
                if (CB_Durango_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_Final" + Japanese);
                }
                if (CB_Orbis_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_Final" + Japanese);
                }
                if (CB_Win32_30_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_Final" + Japanese);
                }
                if (CB_Win32_40_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_Final" + Japanese);
                }
            }
            if (RB_NG_NGLIVE.Checked)
            {
                if (CB_Durango_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Durango_NGLIVE" + Japanese);
                }
                if (CB_Orbis_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Orbis_NGLIVE" + Japanese);
                }
                if (CB_X64_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "X64_NGLIVE" + Japanese);
                }
                if (CB_MasterExes.Checked)
                {
                    string peramiters;
                    string ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    if (CB_Durango_Exes.Checked)
                    {
                        peramiters = "Durango_Live_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_Orbis_Exes.Checked)
                    {
                        peramiters = "Orbis_Live_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_X64_Exes.Checked)
                    {
                        //peramiters ="X64_TU_Masters" + Japanese;
                        //Process.Start(ProcessString, peramiters);
                    }

                }
                if (CB_Default.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Default_NGLIVE" + Japanese);
                }
                if (CB_Clifford.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Clifford_NGLIVE" + Japanese);
                }
                if (CB_Version.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Version");
                    Process.Start(ProcessString, "Version_NGLIVE" + Japanese);
                }
                if (CB_Durango.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_NGLIVE" + Japanese);
                }
                if (CB_Orbis.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_NGLIVE" + Japanese);
                }
                if (CB_Win32_30.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_NGLIVE" + Japanese);
                }
                if (CB_Win32_40.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_NGLIVE" + Japanese);
                }
                if (CB_Win32_40_lq.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_lq_NGLIVE" + Japanese);
                }
                if (CB_win32_nvstereo.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "win32_nvstereo_NGLIVE" + Japanese);
                }
                if (CB_Durango_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_Final_NGLIVE" + Japanese);
                }
                if (CB_Orbis_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_Final_NGLIVE" + Japanese);
                }
                if (CB_Win32_30_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_Final_NGLIVE" + Japanese);
                }
                if (CB_Win32_40_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_Final_NGLIVE" + Japanese);
                }
            }
            if (RB_NG_Temp.Checked)
            {
                if (CB_Durango_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Durango_Temp" + Japanese);
                }
                if (CB_Orbis_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "Orbis_Temp" + Japanese);
                }
                if (CB_X64_Exes.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    Process.Start(ProcessString, "X64_Temp" + Japanese);
                }
                if (CB_MasterExes.Checked)
                {
                    string peramiters;
                    string ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Executables");
                    if (CB_Durango_Exes.Checked)
                    {
                        peramiters = "Durango_Temp_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_Orbis_Exes.Checked)
                    {
                        peramiters = "Orbis_Temp_Masters" + Japanese;
                        Process.Start(ProcessString, peramiters);
                    }
                    if (CB_X64_Exes.Checked)
                    {
                        //peramiters ="X64_TU_Masters" + Japanese;
                        //Process.Start(ProcessString, peramiters);
                    }

                }
                if (CB_Default.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Default_Temp" + Japanese);
                }
                if (CB_Clifford.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Check_Out_Scripts");
                    Process.Start(ProcessString, "Clifford" + Japanese);
                }
                if (CB_Version.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Version");
                    Process.Start(ProcessString, "Version_Temp" + Japanese);
                }
                if (CB_Durango.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_Temp" + Japanese);
                }
                if (CB_Orbis.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_Temp" + Japanese);
                }
                if (CB_Win32_30.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_Temp" + Japanese);
                }
                if (CB_Win32_40.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_Temp" + Japanese);
                }
                if (CB_Win32_40_lq.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_lq_Temp" + Japanese);
                }
                if (CB_win32_nvstereo.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "win32_nvstereo_Temp" + Japanese);
                }
                if (CB_Durango_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Durango_Final_Temp" + Japanese);
                }
                if (CB_Orbis_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Orbis_Final_Temp" + Japanese);
                }
                if (CB_Win32_30_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_30_Final_Temp" + Japanese);
                }
                if (CB_Win32_40_Final.Checked)
                {
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Shaders");
                    Process.Start(ProcessString, "Win32_40_Final_Temp" + Japanese);
                }

            }
            CB_Win32_40_Final.Checked = false; CB_Win32_30_Final.Checked = false; CB_Orbis_Final.Checked = false; CB_Durango_Final.Checked = false; CB_win32_nvstereo.Checked = false; CB_Win32_40_lq.Checked = false; CB_Win32_40.Checked = false; CB_Win32_30.Checked = false; CB_Orbis.Checked = false; CB_Durango.Checked = false; CB_Version.Checked = false; CB_Clifford.Checked = false; CB_Default.Checked = false; CB_X64_Exes.Checked = false; CB_Orbis_Exes.Checked = false; CB_Durango_Exes.Checked = false; CB_MasterExes.Checked = false;
        }

        private void BTN_Check_Out_Scripts_Click(object sender, EventArgs e)
        {

        }

        private void BTN_Version_Click(object sender, EventArgs e)
        {

        }

        public void DeleteFilesInDirectory(string directory, string fileType)
        {
            try
            {
                string[] arrayOfTextFilesinDirectory = new string[Directory.GetFiles(directory, fileType).Length];
                arrayOfTextFilesinDirectory = Directory.GetFiles(directory, "*" + fileType);
                List<string> listOfTextFilesinDirectory = arrayOfTextFilesinDirectory.ToList();
                foreach (string filename in listOfTextFilesinDirectory)
                {
                    if (!Directory.Exists(directory + @"\Backup\")) Directory.CreateDirectory(directory + @"\Backup\");
                    string destination = directory + @"\Backup\" + ((filename.Replace(directory, "")).Replace("/", ""));
                    System.IO.File.Copy(filename, destination, true);
                    System.IO.File.Delete(filename);
                }
            }
            catch (IOException)
            {
            }

        }

        public bool CheckLogsForError(string directory, string fileType, string sku, string searchstring)
        {
            string[] arrayOfTextFilesinDirectory = new string[Directory.GetFiles(directory,fileType).Length];
            arrayOfTextFilesinDirectory = Directory.GetFiles(directory,"*"+fileType);
            List<string> listOfTextFilesinDirectory = arrayOfTextFilesinDirectory.ToList();
            string errorList = "";
            if (listOfTextFilesinDirectory != null)
            {
                foreach (string filename in listOfTextFilesinDirectory)
                {
                    if (filename.Contains(sku)&&sku!="") { 
                        try
                        {
                            string fileToRead = System.IO.File.ReadAllText(@""+filename);
                            Regex rx = new Regex(searchstring, RegexOptions.None);
                            MatchCollection mc = rx.Matches(fileToRead);
                            foreach (Match m in mc)
                            {
                                errorList += m.ToString() + " in " + filename.Replace(directory, "") + "\n";
                            }
                        }
                        catch (IOException)
                        {
                        }
                    }
                    if (sku == "")
                    {
                        try
                        {
                            string fileToRead = System.IO.File.ReadAllText(@"" + filename);
                            Regex rx = new Regex(searchstring, RegexOptions.None);
                            MatchCollection mc = rx.Matches(fileToRead);
                            foreach (Match m in mc)
                            {
                                errorList += m.ToString() + " in " + filename.Replace(directory, "") + "\n";
                            }
                        }
                        catch (IOException)
                        {
                        }
                            
                    }
                }

            }
            if (errorList!="")
            {
                errorLog = errorList;
                return true;
            }else{
                errorLog = "";
                return false;
            }
        }

        private void BTN_Shaders_Click(object sender, EventArgs e)
        {
 
        }
        /// <summary>
        /// Create an email from report data
        /// </summary>
        private bool CreateOutlookEmail()
        {
            //If outlook falls over catch
            try
            {
                // new email
                Type type = Type.GetTypeFromCLSID(new Guid("0006F03A-0000-0000-C000-000000000046")); //Outlook.Application
                if (type == null) return false;
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                mailItem.Subject = "NG Prebuild Updated: - " + DateTime.Now.ToString("HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                mailItem.To = "";
                // use the string from the report in the main body.
                mailItem.HTMLBody = BugStringBuilder.ToString();
                mailItem.Importance = Outlook.OlImportance.olImportanceLow;
                mailItem.Display(true);
                return true;
            }
            catch 
            {
                MessageBox.Show("Please close the existing new email before attempting to create a new one ", "Important Message");
                return false;
            }
        }
        public bool CheckIntListContains(int iBugnumber, List<int> intList)
        {
            bool canAdd = true;
            foreach (int bugNumber in intList)
            {
                if (bugNumber == iBugnumber)
                {
                    canAdd = false;
                }
            }
            return canAdd;
        }

        private void TP_PreBuild_TU_Click(object sender, EventArgs e)
        {

        }

        private void CB_Japanese_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_Japanese.Checked)
            {
                Japanese = "_JPN";
            }
            else if (CB_Japanese.Checked == false)
            {
                Japanese = "";
            }
        }

        private void CB_TU_MNU_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_TU_MNU.Checked)
            {
                CB_Temp_MNU.Checked = false;
                CB_NGLIVE_MNU.Checked = false;
                ChangeBuildToolTabEnabled();
            }
        }

        private void CB_Temp_MNU_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_Temp_MNU.Checked)
            {
                CB_NGLIVE_MNU.Checked = false;
                CB_TU_MNU.Checked = false;
                ChangeBuildToolTabEnabled();
            }
        }
        private void CB_NGLIVE_MNU_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_NGLIVE_MNU.Checked)
            {
                CB_Temp_MNU.Checked = false;
                CB_TU_MNU.Checked = false;
                ChangeBuildToolTabEnabled();
            }
        }
        
        private void ChangeBuildToolTabEnabled()
        {
            string Japanese = "";
            if (CB_JPN.Checked)
            {
                Japanese = "_" + CB_JPN.Name;
            }

            BuildDeploymentToolTabControl.TabPages.Remove(TP_PreBuild_TU);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Release_TU);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_PreBuild_Temp);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Release_Temp);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_PreBuild_NG_Live);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Release_NG_Live);

            if (CB_TU_MNU.Checked)
            {
                if (BuildDeploymentToolTabControl.TabPages.Contains(TP_PreBuild_TU) == false)
                {
                    BuildDeploymentToolTabControl.TabPages.Add(TP_PreBuild_TU);
                    BuildDeploymentToolTabControl.TabPages.Add(TP_Release_TU);
                    RB_Util_TU.Checked = true;
                    RB_NG_TitleUpdate.Checked = true;
                    RB_TU.Checked = true;
                    CB_Temp.Checked = false;
                    CB_NGLIVE.Checked = false;

                }
                userSettings = "<UserData><BranchSelected>" + CB_TU_MNU.Name + Japanese + "</BranchSelected></UserData>";
            }
            if (CB_Temp_MNU.Checked)
            {
                if (BuildDeploymentToolTabControl.TabPages.Contains(TP_PreBuild_Temp) == false)
                {
                    BuildDeploymentToolTabControl.TabPages.Add(TP_PreBuild_Temp);
                    BuildDeploymentToolTabControl.TabPages.Add(TP_Release_Temp);
                    RB_Util_Temp.Checked = true;
                    RB_NG_Temp.Checked = true;
                    RB_Temp.Checked = true;
                    CB_Temp.Checked = true;
                    CB_NGLIVE.Checked = false;

                }
                userSettings = "<UserData><BranchSelected>" + CB_Temp_MNU.Name + Japanese + "</BranchSelected></UserData>";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                if (BuildDeploymentToolTabControl.TabPages.Contains(TP_PreBuild_NG_Live) == false)
                {
                    BuildDeploymentToolTabControl.TabPages.Add(TP_PreBuild_NG_Live);
                    BuildDeploymentToolTabControl.TabPages.Add(TP_Release_NG_Live);
                    RB_Util_NGLIVE.Checked = true;
                    RB_NG_NGLIVE.Checked = true;
                    RB_NGLIVE.Checked = true;
                    CB_Temp.Checked = false;
                    CB_NGLIVE.Checked = true;

                }
                userSettings = "<UserData><BranchSelected>" + CB_NGLIVE_MNU.Name + Japanese + "</BranchSelected></UserData>";
            }

            System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\UserSettings.xml", userSettings);

            BuildDeploymentToolTabControl.TabPages.Remove(TP_Utilities);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_CheckOut);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Sync);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_CLTOB);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Patches);
            BuildDeploymentToolTabControl.TabPages.Remove(TP_Compilers); 
            
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Utilities") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_Utilities);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Check_Out") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_CheckOut);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Sync") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_Sync);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "CLB") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_CLTOB); 
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Patches") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_Patches);
            }
            if (GetFieldValuefromXML("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\BuildToolButtonMapping.xml", "ButtonConfig", "Compiler") != "Disabled")
            {
                BuildDeploymentToolTabControl.TabPages.Add(TP_Compilers);
/*
                PlatformList = new List<RadioButton>();
                PlatformList.Add(RB_DurangoPlatform);
                PlatformList.Add(RB_OrbisPlatform);
                PlatformList.Add(RB_X64Platform);
*/
                BuildTypeList = new List<RadioButton>();
                BuildTypeList.Add(RB_Build);
                BuildTypeList.Add(RB_Rebuild);
                BuildTypeList.Add(RB_Clean);
/*
                ExecutableList = new List<RadioButton>();
                ExecutableList.Add(RB_Beta);
                ExecutableList.Add(RB_BankRelease);
                ExecutableList.Add(RB_Release);
                ExecutableList.Add(RB_Final);
                ExecutableList.Add(RB_Master);
                ExecutableList.Add(RB_All);
                ExecutableList.Add(RB_AllSansMaster);

                AutoBuildOptions = new List<CheckBox>();
                AutoBuildOptions.Add(CB_X64_Platform);
                AutoBuildOptions.Add(CB_Orbis_Platform);
                AutoBuildOptions.Add(CB_Durango_Platform);
            
*/
            }

        }
        private void OnApplicationExit(object sender, EventArgs e)
        {

        }
        private void BTN_Refresh_Click(object sender, EventArgs e)
        {

            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\OldCLList.txt"))
            {
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\OldCLList.txt");
                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])|([5-9][0-9][0-9][0-9][0-9][0-9][0-9])";
                // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[1500];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                        case 0:
                            TB_Rage1.Text = m.Value;
                            break;
                        case 1:
                            TB_Src1.Text = m.Value;
                            break;
                        case 2:
                            TB_Script1.Text = m.Value;
                            break;
                        case 3:
                            TB_Text1.Text = m.Value;
                            break;
                        case 4:
                            TB_Data1.Text = m.Value;
                            break;
                    }
                    i++;
                }
                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx2 = new Regex(pattern, RegexOptions.None);
                MatchCollection mc2 = rx2.Matches(SrcCLOutput);
                test = new string[1500];

                foreach (Match m in mc2)
                {
                    LBL_TimeStamp1.Text = m.Value;
                }

            }

        
            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\CLList.txt"))
            {
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\CLList.txt");
 
                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])|([5-9][0-9][0-9][0-9][0-9][0-9][0-9])";
                // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[1500];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                        case 0:
                            TB_Rage2.Text = m.Value;
                            break;
                        case 1:
                            TB_Src2.Text = m.Value;
                            break;
                        case 2:
                            TB_Script2.Text = m.Value;
                            break;
                        case 3:
                            TB_Text2.Text = m.Value;
                            break;
                        case 4:
                            TB_Data2.Text = m.Value;
                            break;
                    }
                    i++;
                }
                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx2 = new Regex(pattern, RegexOptions.None);
                MatchCollection mc2 = rx2.Matches(SrcCLOutput);
                test = new string[1500];

                foreach (Match m in mc2)
                {
                    LBL_TimeStamp2.Text = m.Value;
                }

            }

        }

        private void CB_JPN_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                CB_Japanese.Checked = true;
            }
            else
            {
                CB_Japanese.Checked = false;
            }
            ChangeBuildToolTabEnabled();

        }

        private void BTN_Label_Prebuild_Transfer_Click(object sender, EventArgs e)
        {
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
                if (CB_Liberty.Checked == false)
                {
                    Process.Start(ProcessString, "TU");
                }else{
                    Process.Start(ProcessString, "TULib");
                }
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + ""));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "");
                if (CB_Liberty.Checked == false)
                {
                    Process.Start(ProcessString, "NG");
                }else{
                    Process.Start(ProcessString, "NGLib");
                }
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
                if (CB_Liberty.Checked == false)
                {
                   Process.Start(ProcessString, "Temp");
                }else{
                   Process.Start(ProcessString, "TempLib");
                }
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
                if (CB_Liberty.Checked == false)
                {
                    Process.Start(ProcessString, "NGLIVE");
                }
                else
                {
                    Process.Start(ProcessString, "NGLIVELib");
                }
            }
        }

        private void BTN_Sync_Prebuild_Transfer_Label_Click(object sender, EventArgs e)
        {
            if (RB_Util_TU.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_TU");
                if (CB_Liberty.Checked == false)
                {
                    Process.Start(ProcessString,"TU");
                }else{
                   Process.Start(ProcessString, "TULib");
                }
            }
            if (RB_Util_NG.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + ""));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "");
                if (CB_Liberty.Checked == false)
                {
                   Process.Start(ProcessString,"NG");
                }else{
                   Process.Start(ProcessString, "NGLib");
                }
            }
            if (RB_Util_Temp.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_Temp");
                if (CB_Liberty.Checked==false)
                {
                    Process.Start(ProcessString,"Temp");
                }else{
                    Process.Start(ProcessString, "TempLib");
                }
            }
            if (RB_Util_NGLIVE.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "_NGLIVE");
                if (CB_Liberty.Checked == false)
                {
                    Process.Start(ProcessString, "NGLIVE");
                }
                else
                {
                    Process.Start(ProcessString, "NGLIVELib");
                }
            }
        }

        private void panel20_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BTN_Deploy_Click(object sender, EventArgs e)
        {
            string RBName ="";
            if (RB_Orbis.Checked)
            {
                RBName ="_"+RB_Orbis.Name;
            }
            if (RB_OrbisBeta.Checked)
            {
                RBName = "_" + RB_OrbisBeta.Name;
            }
            if (RB_OrbisBank.Checked)
            {
                RBName = "_" + RB_OrbisBank.Name;
            }
            if (RB_OrbisRelease.Checked)
            {
                RBName = "_" + RB_OrbisRelease.Name;
            }
            if (RB_OrbisFinal.Checked)
            {
                RBName = "_" + RB_OrbisFinal.Name;
            }
            if (RB_Durango.Checked)
            {
                RBName = "_" + RB_Durango.Name;
            }
            if (RB_DurangoBeta.Checked)
            {
                RBName = "_" + RB_DurangoBeta.Name;
            }
            if (RB_DurangoBank.Checked)
            {
                RBName = "_" + RB_DurangoBank.Name;
            }
            if (RB_DurangoRelease.Checked)
            {
                RBName = "_" + RB_DurangoRelease.Name;
            }
            if (RB_DurangoFinal.Checked)
            {
                RBName = "_" + RB_DurangoFinal.Name;
            }
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {

                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_PackOrbisPatch_Click(object sender, EventArgs e)
        {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("An error occurred attempting to run the pack orbis batchfile", "Important Message");
                }
        }

        private void BTN_PackDurangoPatch_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            try
            {
                Process.Start(ProcessString);
            }
            catch
            {
                MessageBox.Show("An error occurred attempting to run the pack durango batchfile", "Important Message");
            }
        }

        private void BTN_MakeDurangoInstallPack_Click(object sender, EventArgs e)
        {
            string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {

                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_MakeDurangoInstallPackSubMaster_Click(object sender, EventArgs e)
        {
                        string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {

                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_MakeOrbisInstallPack_EU_Click(object sender, EventArgs e)
        {
                        string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_MakeOrbisInstallPack_US_Click(object sender, EventArgs e)
        {
            string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_MakeOrbisInstallPack_JA_Click(object sender, EventArgs e)
        {
            string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            else
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {

                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_RShortcut_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            try
            {
                Process.Start(ProcessString);
            }
            catch
            {
                MessageBox.Show("R* Batfile missing from tools (or needs renaming in buildtoolbuttonmapping)", "Important Message");
            }
        }

        private void BTN_DurangoDir_Click(object sender, EventArgs e)
        {
            string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {

                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                try
                {
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_OrbisDir_Click(object sender, EventArgs e)
        {
            string RBName = "";
            if (CB_TU_MNU.Checked)
            {
                RBName += "_TU";
            }
            if (CB_Temp_MNU.Checked)
            {
                RBName += "_Temp";
            }
            if (CB_NGLIVE_MNU.Checked)
            {
                RBName += "_NGLIVE";
            }
            if (CB_JPN.Checked)
            {
                RBName += "_JPN";
            }
            if (CB_Temp_MNU.Checked && CB_TU_MNU.Checked)
            {
                MessageBox.Show("Both Temp and TU is selected please select only one branch.", "Important Message");
            }
            else
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName));
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + RBName);
                    Process.Start(ProcessString);
                }
                catch
                {
                    MessageBox.Show("Patch directory does not exist or deployment batchfiles are missing", "Important Message");
                }
            }
        }

        private void BTN_SyncPatchDirectory_Click(object sender, EventArgs e)
        {
            try
            {
                string PatchDirectory = "";
                PatchDirectory = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_SyncPatchDirectory_PS4");
                string PatchCL = StartSyncProcess(PatchDirectory, ForceChecked, 180000);
                PatchDirectory = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_SyncPatchDirectory_XB1");
                string PatchCL2 = StartSyncProcess(PatchDirectory, ForceChecked, 180000);
            }
            catch { }
        }

        private void BTN_Integrate_Script_Headers_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Clear_Debug_Script_Files_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
            Process.Start(ProcessString, ((Button)sender).Name + "1");
            //special case where we want to launch two batfiles
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
            String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
            Process.Start(ProcessString2, ((Button)sender).Name + "2");
        }

        private void BTN_Build_NG_American_Text_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Randomise_Natives_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Run_Scan_Natives_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);

        }

        private void BTN_Label_New_NG_PreBuild_Code_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Label_NG_Prebuild_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE");
            }
        }

        private void BTN_Copy_Orbis_deployed_Prebuild_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVEPS4");
        }

        private void BTN_Copy_Durango_Deployed_Prebuild_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVEXBOX1");
        }

        private void BTN_Copy_Prebuild_mpPacks_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Copy_Prebuild_DLC_packages_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString);
        }

        private void BTN_Grab_NG_Prebuild_Code_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Copy_to_DFSR_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Label_Current_NG_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "NGLIVE_JPN");
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
                Process.Start(ProcessString2, "NGLIVE_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1"));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "1");
                Process.Start(ProcessString, "NGLIVE");
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2"));
                String ProcessString2 = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name + "2");
                Process.Start(ProcessString2, "NGLIVE");
            }
        }

        private void BTN_Label_NG_version_xxx_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE");
            }
        }

        private void BTN_Copy_Orbis_Deployed_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVEPS4_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVEPS4");
            }
        }

        private void BTN_Copy_Durango_Deployed_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVEXBOX1_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVEXBOX1");
            }
        }

        private void BTN_Copy_Current_mpPacks_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Copy_Current_DLC_packages_NGLIVE_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
            String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
            Process.Start(ProcessString, "NGLIVE");
        }

        private void BTN_Backup_Symbols_NGLIVE_Click(object sender, EventArgs e)
        {
            if (CB_JPN.Checked)
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE_JPN");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name));
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                Process.Start(ProcessString, "NGLIVE");
            }
        }

        private void RB_Util_Temp_CheckedChanged(object sender, EventArgs e)
        {

        }
/*
        private void BTN_Build_Click (object sender, EventArgs e)
        {
            string Platform="";
            string BuildType="";
            string Executable="";
            string Environment="";
            string Solution="";
            Process temp;
            string logDirectory = @"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Logs\BuildLogs";
            DeleteFilesInDirectory(logDirectory, "*.txt");
            if (PlatformList!=null)
            {
                foreach (RadioButton RB_Platform in PlatformList)
                {
                    if (RB_Platform.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_Platform.Name);
                    }

                }
            }
            if (BuildTypeList != null)
            {
                foreach (RadioButton RB_BuildType in BuildTypeList)
                {
                    if (RB_BuildType.Checked == true)
                    {
                        BuildType = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_BuildType.Name);
                    }

                }
            }
            if (ExecutableList != null)
            {
                foreach (RadioButton RB_Executable in ExecutableList)
                {
                    if (RB_Executable.Checked == true)
                    {
                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_Executable.Name);
                    }

                }
            }
            if (CB_TU_MNU.Checked==true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG");
            }
            if (CB_Temp_MNU.Checked == true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_Temp");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_Temp");
            }
            if (CB_NGLIVE_MNU.Checked == true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG_Live");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG_Live");
            }
            Debug.WriteLine("" + Environment + Solution + BuildType + Executable + Platform + "");

            if (Executable.Contains("All") == false)
            {
                String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                temp.WaitForExit(4000000);
            }
            else
            {
                if (Executable == "All")
                {
                    Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Beta");
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                    if (temp.WaitForExit(4000000))
                    {
                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_BankRelease");
                        if (temp.WaitForExit(4000000))
                        {
                            temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                            if (temp.WaitForExit(4000000))
                            {
                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Release");
                                if (temp.WaitForExit(4000000))
                                {
                                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                    if (temp.WaitForExit(4000000))
                                    {
                                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Final");
                                        if (temp.WaitForExit(4000000))
                                        {
                                            temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                            if (temp.WaitForExit(4000000))
                                            {
                                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                                Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Master");
                                                if (temp.WaitForExit(4000000))
                                                {
                                                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                                    if (temp.WaitForExit(4000000))
                                                    {
                                                        if (RB_Durango.Checked == false) 
                                                        {
                                                            temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (Executable == "AllSansMaster")
                {
                    Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Beta");
                    String ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                    if (temp.WaitForExit(4000000))
                    {
                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_BankRelease");
                        if (temp.WaitForExit(4000000))
                        {
                            temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                            if (temp.WaitForExit(4000000))
                            {
                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Release");
                                if (temp.WaitForExit(4000000))
                                {
                                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                    if (temp.WaitForExit(4000000))
                                    {
                                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_Final");
                                        if (temp.WaitForExit(4000000))
                                        {
                                            temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                            if (temp.WaitForExit(4000000))
                                            {
                                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Directory.Exists(logDirectory))
            {
                if (CheckLogsForError(logDirectory, ".txt", "", "[1-9] failed|[1-9][1-9] failed|[1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9][1-9] failed"))
                {
                    string dErrorReport = "Errors Reported:" + "\n" + errorLog;
                    DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                }
                else
                {
                    if (CheckLogsForError(logDirectory, ".txt", "", "[2-3] succeeded") == false)
                    {
                        string dErrorReport = "Compiler did not complete, please check output, or if this is your first build please build again." + "\n";
                        DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                    }
                }
            }
        }
        */
        public bool BuildPlatform(string ProcessString, string Perameters)
        {
            Process temp;
            temp = Process.Start(ProcessString, Perameters);
            if (temp.WaitForExit(4000000))
            {
                temp = Process.Start(ProcessString, Perameters);
                if(temp.WaitForExit(4000000))
                {
                    return true;
                }
            }
            return false;
        }

        public bool LaunchProcessandWait (string ProcessString, string Perameters, int DelayInMS)
        {
            Process temp;
            temp = Process.Start(ProcessString, Perameters);
            if (temp.WaitForExit(DelayInMS))
            {
                return true;
            }
            return false;
        }
        


        public enum ConfigTypeEnum { Beta, Release, BankRelease, Final, Master };

        public int Get_Platform_Delay_Time(string platform)
        {
            string tempString = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", platform);
            int intValue = 0;
            int.TryParse(tempString, out intValue);
            return intValue;
        }

/*
        private void BTN_AutoBuild_Click(object sender, EventArgs e)
        {
            string logDirectory = @"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\Logs\BuildLogs";
            DeleteFilesInDirectory(logDirectory, "*.txt");
            string Platform = "";
            string BuildType = "";
            string Executable = "";
            string Environment = "";
            string Solution = "";
            string Peramiters = "";
            Process temp;
            string ProcessString;
            string shaderPath="";

            if (PlatformList != null)
            {
                foreach (RadioButton RB_Platform in PlatformList)
                {
                    if (RB_Platform.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_Platform.Name);
                    }
                }
            }
            if (BuildTypeList != null)
            {
                foreach (RadioButton RB_BuildType in BuildTypeList)
                {
                    if (RB_BuildType.Checked == true)
                    {
                        BuildType = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_BuildType.Name);
                    }

                }
            }
            if (ExecutableList != null)
            {
                foreach (RadioButton RB_Executable in ExecutableList)
                {
                    if (RB_Executable.Checked == true)
                    {
                        Executable = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", RB_Executable.Name);
                    }

                }
            }
            if (CB_TU_MNU.Checked == true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG");
                shaderPath = @"X:\gta5\src\" + GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RebuildShaderDev_ng") + @"\game\shader_source\VS_Project\batch\";
            }
            if (CB_Temp_MNU.Checked == true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_Temp");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_Temp");
                shaderPath = @"X:\gta5\src\" + GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RebuildShaderDev_Temp") + @"\game\shader_source\VS_Project\batch\";
            }
            if (CB_NGLIVE_MNU.Checked == true)
            {
                Solution = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG_Live");
                Environment = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG_Live");
                shaderPath = @"X:\gta5\src\" + GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RebuildShaderDev_ng_Live") + @"\game\shader_source\VS_Project\batch\";
            }

            if (CB_Shaders.Checked == true)
            {

                foreach (CheckBox CB in AutoBuildOptions)
                {
                    if (CB == CB_Durango_Platform && CB.Checked == true)
                    {
                       Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_DurangoPlatform");
                       LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_durango.bat", "dev", 40000000);
                       LaunchProcessandWait(@"" + shaderPath + "rsm_build_durango.bat", "dev", 40000000);
                       LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_durango.bat", "final", 40000000);
                       LaunchProcessandWait(@"" + shaderPath + "rsm_build_durango.bat", "final", 40000000);
                       
                    }
                    if (CB == CB_Orbis_Platform && CB.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_OrbisPlatform");
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_orbis.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_orbis.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_orbis.bat", "final", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_orbis.bat", "final", 40000000);
                    }
                    if (CB == CB_X64_Platform && CB.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_X64Platform");
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_win32.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_win32.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_win32_40.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_win32_40.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_win32_40_lq.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_win32_40_lq.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_rebuild_win32_nvstereo.bat", "dev", 40000000);
                        LaunchProcessandWait(@"" + shaderPath + "rsm_build_win32_nvstereo.bat", "dev", 40000000);
                    }

                    ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Build");

                    foreach (string ConfigType in Enum.GetNames(typeof(ConfigTypeEnum)))
                    {
                        if (CB.Checked == true) { 
                            if (CB_Masters.Checked == false)
                            {
                                if (ConfigType.Contains("Master") == false)
                                {
                                    Peramiters = "" + Environment + "," + Solution + "," + "/build" + "," + ConfigType + "," + Platform + "";
                                    BuildPlatform(ProcessString, Peramiters);
                                }
                            }
                            else
                            {
                                if (CB == CB_Durango_Platform) 
                                {
                                    Peramiters = "" + Environment + "," + Solution + "," + "/build" + "," + ConfigType + "," + Platform + "";
                                    LaunchProcessandWait(ProcessString, Peramiters, 4000000);
                                }
                                else
                                {
                                    Peramiters = "" + Environment + "," + Solution + "," + "/build" + "," + ConfigType + "," + Platform + "";
                                    BuildPlatform(ProcessString, Peramiters);
                                }
                            }
                        }

                    }
                    
                }
            }
            else
            {


                foreach (CheckBox CB in AutoBuildOptions)
                {
                    if (CB == CB_Durango_Platform && CB.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_DurangoPlatform");

                    }
                    if (CB == CB_Orbis_Platform && CB.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_OrbisPlatform");

                    }
                    if (CB == CB_X64_Platform && CB.Checked == true)
                    {
                        Platform = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "RB_X64Platform");

                    }
                    ProcessString = GetFieldValuefromXML(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_NG_BuildTools\BuildToolButtonMapping.xml", "ButtonConfig", "BTN_Build");
                    foreach (string ConfigType in Enum.GetNames(typeof(ConfigTypeEnum)))
                    {
                        if (CB.Checked == true)
                        {
                            if (CB_Masters.Checked == false)
                            {
                                if (ConfigType.Contains("Master") == false)
                                {
                                    Peramiters = "" + Environment + "," + Solution + "," + "/build" + "," + ConfigType + "," + Platform + "";
                                    BuildPlatform(ProcessString, Peramiters);
                                }
                            }
                            else
                            {
                                if (CB == CB_Durango_Platform)
                                {
                                    Peramiters = "" + Environment + "," + Solution + "," + "/build" + "," + ConfigType + "," + Platform + "";
                                    LaunchProcessandWait(ProcessString, Peramiters, 4000000);
                                }
                            }
                        }

                    }
                }
                
            }
            if (Directory.Exists(logDirectory))
            {
                if (CheckLogsForError(logDirectory, ".txt", "", "[1-9] failed|[1-9][1-9] failed|[1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9][1-9] failed"))
                {
                    string dErrorReport = "Errors Reported:" + "\n" + errorLog;
                    DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                }
                else
                {
                    if (CheckLogsForError(logDirectory, ".txt", "", "[2-3] succeeded") == false)
                    {
                        string dErrorReport = "Compiler did not complete, please check output, or if this is your first build please build again." + "\n";
                        DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                    }
                }
            }
           }
        */
        async Task TaskDelay(object sender, string platform)
        {
            int intValue = 0;
            if (platform == "Orbis")
            {
                intValue = Get_Platform_Delay_Time("Compiler_Delay_Orbis");
            }
            if (platform == "x64")
            {
                intValue = Get_Platform_Delay_Time("Compiler_Delay_x64");
            }
            if (platform == "Durango")
            {
                intValue = Get_Platform_Delay_Time("Compiler_Delay_Durango");
            }

            ((Button)sender).Enabled = false;
            await Task.Delay(intValue);
        }

        private async void BTN_Build_Click (object sender, EventArgs e)
        {
            if (building == false)
            {

                string Platform = "";
                string BuildType = "";
                string Executable = "";
                string Environment = "";
                string Solution = "";
                string shaderPath = "";
                Process temp;
                string logDirectory = SupportingFilePath + @"Logs\BuildLogs";
                DeleteFilesInDirectory(logDirectory, "*.txt");

                if (BuildTypeList != null)
                {
                    foreach (RadioButton RB_BuildType in BuildTypeList)
                    {
                        if (RB_BuildType.Checked == true)
                        {
                            BuildType = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", RB_BuildType.Name);
                        }

                    }
                }

                if (CB_TU_MNU.Checked == true)
                {
                    Solution = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG");
                    Environment = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG");
                    shaderPath = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "ShaderBranchDev_ng");
                }
                if (CB_Temp_MNU.Checked == true)
                {
                    Solution = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Solution_Temp");
                    Environment = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Environment_Temp");
                    shaderPath = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "ShaderBranchDev_Temp");
                }
                if (CB_NGLIVE_MNU.Checked == true)
                {
                    Solution = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Solution_NG_Live");
                    Environment = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "Environment_NG_Live");
                    shaderPath = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "ShaderBranchDev_ng_Live");
                }

                Debug.WriteLine("" + Environment + Solution + BuildType + Executable + Platform + "");
                bool breakout = false;
                String ProcessString = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", ((Button)sender).Name);
                foreach (CheckBox CB_AutoBuildOption in AutoBuildOptions)
                {
                    if (breakout == true)
                    {
                        break;
                    }

                    foreach (ConfigTypeEnum executable in Enum.GetValues(typeof(ConfigTypeEnum)))
                    {
                        if (CB_AutoBuildOption.Name.Contains(executable.ToString()))
                        {
                            Executable = executable.ToString();
                        }
                    }

                    if (CB_AutoBuildOption.Checked)
                    {
                        if (PS4PlatformList.Contains(CB_AutoBuildOption))
                        {
                            Platform = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "CB_Orbis_Platform");
                        }
                        if (XB1PlatformList.Contains(CB_AutoBuildOption))
                        {
                            Platform = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "CB_Durango_Platform");
                        }
                        if (X64PlatformList.Contains(CB_AutoBuildOption))
                        {
                            Platform = GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", "CB_X64_Platform");
                        }
                        if (ShaderBuildOptions.Contains(CB_AutoBuildOption))
                        {
                            if (CB_AutoBuildOption.Name.Contains("Dev"))
                            {
                                if (RB_ShadersReBuildAndBuild.Checked)
                                {
                                    LaunchProcessandWait(@"" + shaderPath + "" + GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", CB_AutoBuildOption.Name + "_R"), "dev", 40000000);
                                }
                                LaunchProcessandWait(@"" + shaderPath + "" + GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", CB_AutoBuildOption.Name + "_B"), "dev", 40000000);
                            }
                            if (CB_AutoBuildOption.Name.Contains("Final"))
                            {
                                if (RB_ShadersReBuildAndBuild.Checked)
                                {
                                    LaunchProcessandWait(@"" + shaderPath + "" + GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", CB_AutoBuildOption.Name + "_R"), "final", 40000000);
                                }
                                LaunchProcessandWait(@"" + shaderPath + "" + GetFieldValuefromXML(SupportingFilePath + "BuildToolButtonMapping.xml", "ButtonConfig", CB_AutoBuildOption.Name + "_B"), "final", 40000000);
                            }

                        }
                        else
                        {
                            if (RB_Rebuild.Checked)
                            {
                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + "/clean" + "," + Executable + "," + Platform + "");
                                if (temp.WaitForExit(4000000))
                                {
                                    await TaskDelay(sender, Platform);
                                    temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + "/build" + "," + Executable + "," + Platform + "");
                                    if (temp.WaitForExit(4000000))
                                    {
                                        await TaskDelay(sender, Platform);
                                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + "/build" + "," + Executable + "," + Platform + "");
                                        if (temp.WaitForExit(4000000))
                                        {
                                            await TaskDelay(sender, Platform);
                                            string dErrorReport = "Rebuild Complete, please select the build option and selected SKU's" + "\n";
                                            DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                                            if (dialogResult == DialogResult.OK)
                                            {
                                                breakout = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                building = true;
                                if (temp.WaitForExit(4000000))
                                {
                                    await TaskDelay(sender, Platform);
                                    if (CB_AutoBuildOption != CB_D_Master) { 
                                        temp = Process.Start(ProcessString, "" + Environment + "," + Solution + "," + BuildType + "," + Executable + "," + Platform + "");
                                        building = true;
                                        if (temp.WaitForExit(4000000))
                                        {
                                            await TaskDelay(sender, Platform);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    building = false;
                }



                if (Directory.Exists(logDirectory))
                {
                    if (CheckLogsForError(logDirectory, ".txt", "", "[1-9] failed|[1-9][1-9] failed|[1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9] failed|[1-9][1-9][1-9][1-9][1-9] failed"))
                    {
                        string dErrorReport = "Errors Reported:" + "\n" + errorLog;
                        DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                    }
                    else
                    {
                        if (CheckLogsForError(logDirectory, ".txt", "", "[1-9] succeeded") == false)
                        {
                            string dErrorReport = "Compiler did not complete, please check output, or if this is your first build please build again." + "\n";
                            DialogResult dialogResult = MessageBox.Show(dErrorReport, "Important", MessageBoxButtons.OK);
                        }
                    }
                }
            }
            ((Button)sender).Enabled = true;
        }

        private void BTN_OrbisExes_ALL_Click(object sender, EventArgs e)
        {
            var listofCheckboxesingroup = GB_Orbis.Controls.OfType<CheckBox>();
            if (OrbisExesAllChecked == false)
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("O"))
                    {
                        CB.Checked = true;
                    }
                }
                OrbisExesAllChecked = true;
                BTN_OrbisExes_ALL.Text = "Clear";
            }
            else
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("O"))
                    {
                        CB.Checked = false;
                    }
                }
                OrbisExesAllChecked = false;
                BTN_OrbisExes_ALL.Text = "All";
            }
        }

        private void BTN_DurangoExes_ALL_Click(object sender, EventArgs e)
        {
            var listofCheckboxesingroup = GB_Durango.Controls.OfType<CheckBox>();
            if (DurangoExesAllChecked == false)
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("D"))
                    {
                        CB.Checked = true;
                    }
                }
                DurangoExesAllChecked = true;
                BTN_DurangoExes_ALL.Text = "Clear";
            }
            else
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("D"))
                    {
                        CB.Checked = false;
                    }
                }
                DurangoExesAllChecked = false;
                BTN_DurangoExes_ALL.Text = "All";
            }
        }

        private void BTN_X64Exes_ALL_Click(object sender, EventArgs e)
        {
            var listofCheckboxesingroup = GB_X64.Controls.OfType<CheckBox>();
            if (X64ExesAllChecked == false)
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("Y"))
                    {
                        CB.Checked = true;
                    }
                }
                X64ExesAllChecked = true;
                BTN_X64Exes_ALL.Text = "Clear";
            }
            else
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    if (CB.Name.Contains("Y"))
                    {
                        CB.Checked = false;
                    }
                }
                X64ExesAllChecked = false;
                BTN_X64Exes_ALL.Text = "All";
            }
        }

        private void BTN_Shaders_All_Click(object sender, EventArgs e)
        {
            var listofCheckboxesingroup = GB_Shader_Compile.Controls.OfType<CheckBox>();
            if (ShadersAllChecked == false)
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    CB.Checked = true;
                }
                ShadersAllChecked = true;
                BTN_Shaders_All.Text = "Clear";
            }
            else
            {
                foreach (CheckBox CB in listofCheckboxesingroup)
                {
                    CB.Checked = false;
                }
                ShadersAllChecked = false;
                BTN_Shaders_All.Text = "All";
            }
        }








     }

    /// <summary>
    /// Bug class that holds the bugNumber,summary, and bugclass.
    /// </summary>
    public class Bug
    {
        public int bugNumber { get; set; }
        public string summary { get; set; }
        public string bugclass { get; set; }
        public string project { get; set; }
        public string status { get; set; }
        public Bug(int BugNumber, string Summary, string BugClass, string Project, string Status)
        {
            bugNumber = BugNumber;
            summary = Summary;
            bugclass = BugClass;
            project = Project;
            status = Status;
            
        }
    }



 }


