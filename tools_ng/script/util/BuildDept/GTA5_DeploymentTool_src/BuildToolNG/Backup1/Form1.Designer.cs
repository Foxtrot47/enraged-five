﻿namespace BuildDeploymentTool
{
    partial class BuildDeploymentMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildDeploymentMenu));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button25 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.Prebuild = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.Build_Tools = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button22 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button43 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button54 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 337);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(197, 22);
            this.button2.TabIndex = 1;
            this.button2.Text = "Prepare duplicate QA build";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(11, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 21);
            this.button1.TabIndex = 0;
            this.button1.Text = "Prepare duplicate CURRENT build";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Location = new System.Drawing.Point(22, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(446, 320);
            this.panel2.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.button32);
            this.panel4.Controls.Add(this.button31);
            this.panel4.Controls.Add(this.button14);
            this.panel4.Controls.Add(this.button26);
            this.panel4.Controls.Add(this.button27);
            this.panel4.Controls.Add(this.button28);
            this.panel4.Location = new System.Drawing.Point(232, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(186, 283);
            this.panel4.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Prebuild/Current build release";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(6, 209);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(172, 29);
            this.button32.TabIndex = 36;
            this.button32.Text = "Update PreBuild TU build";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(6, 30);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(172, 32);
            this.button31.TabIndex = 35;
            this.button31.Text = "Label TU NewPreBuildCode";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(6, 173);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(172, 29);
            this.button14.TabIndex = 33;
            this.button14.Text = "Update QA TU build";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(6, 66);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(172, 31);
            this.button26.TabIndex = 26;
            this.button26.Text = "Label PreBuild TU";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(6, 100);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(172, 32);
            this.button27.TabIndex = 27;
            this.button27.Text = "Label Current TU";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(6, 135);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(172, 32);
            this.button28.TabIndex = 28;
            this.button28.Text = "Label TU version xxx";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.button29);
            this.panel5.Controls.Add(this.button30);
            this.panel5.Location = new System.Drawing.Point(7, 208);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(186, 102);
            this.panel5.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "PS3 TU symbol backup";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(7, 28);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(172, 29);
            this.button29.TabIndex = 29;
            this.button29.Text = "Backup TU PS3 symbols";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(7, 63);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(172, 34);
            this.button30.TabIndex = 30;
            this.button30.Text = "Backup TU PS3 symbols DONGLED";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(139, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Dev_Network / Title Update";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.button25);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.button33);
            this.panel7.Controls.Add(this.button34);
            this.panel7.Controls.Add(this.button7);
            this.panel7.Location = new System.Drawing.Point(7, 27);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(186, 175);
            this.panel7.TabIndex = 12;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(7, 100);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(172, 32);
            this.button25.TabIndex = 14;
            this.button25.Text = "TU Build \"American\" text";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Script Utilities";
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(7, 65);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(172, 32);
            this.button33.TabIndex = 12;
            this.button33.Text = "dev_net Clear Debug Script Files";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(7, 30);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(172, 32);
            this.button34.TabIndex = 11;
            this.button34.Text = "dev_net Integrate script_headers";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(7, 135);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(172, 32);
            this.button7.TabIndex = 32;
            this.button7.Text = "Dev_network run_scannatives";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(11, 559);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(197, 52);
            this.button11.TabIndex = 10;
            this.button11.Text = "Update duplicate CURRENT build with latest local data and set as PREBUILD";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(11, 533);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(197, 22);
            this.button10.TabIndex = 9;
            this.button10.Text = "Update QA drive PREBUILD";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(11, 509);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(197, 20);
            this.button9.TabIndex = 8;
            this.button9.Text = "Label GTA5_Prebuild";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(11, 486);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(197, 19);
            this.button8.TabIndex = 7;
            this.button8.Text = "Label GTA5NewPrebuildCode";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button11);
            this.panel3.Controls.Add(this.button6);
            this.panel3.Controls.Add(this.button10);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.button9);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.button24);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.Prebuild);
            this.panel3.Controls.Add(this.button13);
            this.panel3.Controls.Add(this.button23);
            this.panel3.Controls.Add(this.button21);
            this.panel3.Controls.Add(this.button12);
            this.panel3.Controls.Add(this.button20);
            this.panel3.Controls.Add(this.button19);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button18);
            this.panel3.Controls.Add(this.button17);
            this.panel3.Controls.Add(this.button16);
            this.panel3.Controls.Add(this.button15);
            this.panel3.Location = new System.Drawing.Point(779, 39);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(218, 617);
            this.panel3.TabIndex = 2;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(11, 437);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(197, 22);
            this.button6.TabIndex = 14;
            this.button6.Text = "Build \"American\" text";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 244);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Old Script Utilities";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(11, 413);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(197, 21);
            this.button5.TabIndex = 13;
            this.button5.Text = "Build model_enums";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(11, 131);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(197, 25);
            this.button24.TabIndex = 22;
            this.button24.Text = "Update N: drive platform data";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(11, 389);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(197, 21);
            this.button4.TabIndex = 12;
            this.button4.Text = "Clear Debug Script Files";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(11, 363);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(197, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Integrate script_headers";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Prebuild
            // 
            this.Prebuild.AutoSize = true;
            this.Prebuild.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Prebuild.ForeColor = System.Drawing.Color.Black;
            this.Prebuild.Location = new System.Drawing.Point(74, 468);
            this.Prebuild.Name = "Prebuild";
            this.Prebuild.Size = new System.Drawing.Size(66, 15);
            this.Prebuild.TabIndex = 3;
            this.Prebuild.Text = "Old Prebuild";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(11, 286);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(197, 23);
            this.button13.TabIndex = 24;
            this.button13.Text = "run_scannative";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(11, 217);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(197, 21);
            this.button23.TabIndex = 21;
            this.button23.Text = "Update CURRENT build (direct patch)";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(11, 188);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(197, 24);
            this.button21.TabIndex = 20;
            this.button21.Text = "Update QA build (direct patch)";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(11, 260);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(197, 23);
            this.button12.TabIndex = 23;
            this.button12.Text = "Rebuild psn_final shaders";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(11, 161);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(197, 22);
            this.button20.TabIndex = 19;
            this.button20.Text = "Updated QA dev_build folder";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(11, 106);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(197, 21);
            this.button19.TabIndex = 18;
            this.button19.Text = "Rename PREBUILD to CURRENT for build release";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 244);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Old Build Utilities";
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(11, 80);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(197, 23);
            this.button18.TabIndex = 17;
            this.button18.Text = "Backup PS3 symbols DONGLED";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(11, 54);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(197, 23);
            this.button17.TabIndex = 16;
            this.button17.Text = "Backup PS3 symbols";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(11, 30);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(197, 21);
            this.button16.TabIndex = 15;
            this.button16.Text = "Label GTA5_[ALL]_version_XXX";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(11, 7);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(197, 20);
            this.button15.TabIndex = 14;
            this.button15.Text = "Label [CB]_GTAV_current";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // Build_Tools
            // 
            this.Build_Tools.AutoSize = true;
            this.Build_Tools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Build_Tools.ForeColor = System.Drawing.Color.Black;
            this.Build_Tools.Location = new System.Drawing.Point(841, 21);
            this.Build_Tools.Name = "Build_Tools";
            this.Build_Tools.Size = new System.Drawing.Size(93, 15);
            this.Build_Tools.TabIndex = 4;
            this.Build_Tools.Text = "Old Build Release";
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(518, 567);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(217, 48);
            this.panel6.TabIndex = 10;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(528, 621);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(197, 35);
            this.button22.TabIndex = 34;
            this.button22.Text = "template";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Location = new System.Drawing.Point(22, 338);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(446, 318);
            this.panel1.TabIndex = 13;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.button35);
            this.panel8.Controls.Add(this.button36);
            this.panel8.Controls.Add(this.button37);
            this.panel8.Controls.Add(this.button38);
            this.panel8.Controls.Add(this.button39);
            this.panel8.Controls.Add(this.button40);
            this.panel8.Location = new System.Drawing.Point(232, 28);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(186, 283);
            this.panel8.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Prebuild/Current build release";
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(6, 209);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(172, 29);
            this.button35.TabIndex = 36;
            this.button35.Text = "Update PreBuild dev_Live";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(6, 30);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(172, 32);
            this.button36.TabIndex = 35;
            this.button36.Text = "--------";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(6, 173);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(172, 29);
            this.button37.TabIndex = 33;
            this.button37.Text = "Update QA dev_Live build";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(6, 66);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(172, 31);
            this.button38.TabIndex = 26;
            this.button38.Text = "Label PreBuild dev_Live";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(6, 100);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(172, 32);
            this.button39.TabIndex = 27;
            this.button39.Text = "Label Current dev_Live";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(6, 135);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(172, 32);
            this.button40.TabIndex = 28;
            this.button40.Text = "Label dev_Live version xxx";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(179, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Dev_Live TU";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.button41);
            this.panel9.Controls.Add(this.button42);
            this.panel9.Location = new System.Drawing.Point(14, 209);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(186, 102);
            this.panel9.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "PS3 TU symbol backup";
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(7, 28);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(172, 29);
            this.button41.TabIndex = 29;
            this.button41.Text = "Backup TU PS3 symbols";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(7, 63);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(172, 34);
            this.button42.TabIndex = 30;
            this.button42.Text = "Backup TU PS3 symbols DONGLED";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button43);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.button44);
            this.panel10.Controls.Add(this.button45);
            this.panel10.Controls.Add(this.button46);
            this.panel10.Location = new System.Drawing.Point(14, 28);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(186, 175);
            this.panel10.TabIndex = 16;
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(7, 100);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(172, 32);
            this.button43.TabIndex = 14;
            this.button43.Text = "dev_Live Build \"American\" text";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(57, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Script Utilities";
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(7, 65);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(172, 32);
            this.button44.TabIndex = 12;
            this.button44.Text = "Clear Debug Script Files";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(7, 30);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(172, 32);
            this.button45.TabIndex = 11;
            this.button45.Text = "Integrate script_headers";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(7, 135);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(172, 32);
            this.button46.TabIndex = 32;
            this.button46.Text = "run_scannatives";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // panel11
            // 
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.button57);
            this.panel11.Controls.Add(this.button58);
            this.panel11.Controls.Add(this.button56);
            this.panel11.Controls.Add(this.button55);
            this.panel11.Controls.Add(this.button54);
            this.panel11.Controls.Add(this.label13);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.button53);
            this.panel11.Controls.Add(this.button52);
            this.panel11.Controls.Add(this.button51);
            this.panel11.Controls.Add(this.button50);
            this.panel11.Controls.Add(this.button49);
            this.panel11.Controls.Add(this.button48);
            this.panel11.Controls.Add(this.button47);
            this.panel11.Controls.Add(this.label11);
            this.panel11.Location = new System.Drawing.Point(484, 13);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(289, 548);
            this.panel11.TabIndex = 35;
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(54, 148);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(201, 35);
            this.button54.TabIndex = 44;
            this.button54.Text = "Rebuild Win32_30 shaders";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(115, 368);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 15);
            this.label13.TabIndex = 43;
            this.label13.Text = "Build Release";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(131, 235);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 15);
            this.label12.TabIndex = 42;
            this.label12.Text = "Prebuild";
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(54, 425);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(201, 35);
            this.button53.TabIndex = 41;
            this.button53.Text = "Label NG version xxx";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(54, 389);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(201, 35);
            this.button52.TabIndex = 40;
            this.button52.Text = "Label NG Current";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(54, 189);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(201, 35);
            this.button51.TabIndex = 39;
            this.button51.Text = "Copy to DFSR";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(54, 253);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(201, 30);
            this.button50.TabIndex = 38;
            this.button50.Text = "Label NG Prebuild";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(54, 109);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(201, 35);
            this.button49.TabIndex = 37;
            this.button49.Text = "Build NG American Text";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(54, 27);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(201, 35);
            this.button48.TabIndex = 36;
            this.button48.Text = "Integrate script_headers";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(54, 68);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(201, 35);
            this.button47.TabIndex = 35;
            this.button47.Text = "Clear Debug Script Files";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.DodgerBlue;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(120, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "DEV_NG";
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(54, 286);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(201, 32);
            this.button55.TabIndex = 45;
            this.button55.Text = "Copy Orbis deployed Prebuild";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(54, 324);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(201, 32);
            this.button56.TabIndex = 46;
            this.button56.Text = "Copy Durango deployed Prebuild";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(54, 501);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(201, 32);
            this.button57.TabIndex = 48;
            this.button57.Text = "Copy Durango deployed CURRENT";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(54, 463);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(201, 32);
            this.button58.TabIndex = 47;
            this.button58.Text = "Copy Orbis deployed CURRENT";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // BuildDeploymentMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 668);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.Build_Tools);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BuildDeploymentMenu";
            this.Text = "Build Deployment Tool";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label Prebuild;
        private System.Windows.Forms.Label Build_Tools;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
    }
}

