﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace BuildDeploymentTool
{
    public partial class BuildDeploymentMenu : Form
    {
        public BuildDeploymentMenu()
        {
            InitializeComponent();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label NewPreBuildCode.bat");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label PreBuild.bat");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_PREbuild.bat");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label Current.bat");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_[ALL]_version_xxx label.bat");
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup.bat");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup_Dongled.bat");
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Rename_PREBUILD_to_CURRENT.bat");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Duplicate_CURRENT.bat");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Duplicate_QA_build.bat");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Integrate_script_headers.bat");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev\\Clear Testbeds Debug Scripts.bat");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev\\enum_maker.bat");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Build_American_Text.bat");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Update_COPIED_CURRENT.bat");
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_dev_build.bat");
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Patch_QA_build.bat");
        }

        private void button23_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PATCH_update_CURRENT_build.bat");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Rebuild psn_final shaders.bat");
        }

        private void button24_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_Network_Data.bat");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev\\run_scannative.bat");
        }

        private void button34_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Integrate_script_headers_from_dev_network.bat");
        }

        private void button33_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_network\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev_network\\Clear Testbeds Debug Scripts.bat");
        }

        private void button25_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\TU_Build_American_Text.bat");
        }

        private void button26_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label PreBuild TU.bat");
        }

        private void button27_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label current TU.bat");
        }

        private void button28_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label TU version_xxx.bat");
        }

        private void button29_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup_TU.bat");
        }

        private void button30_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup_Dongled_TU.bat");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_network\\run_scannative.bat");
        }

        private void button31_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label TU newprebuildcode.bat");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_TU_Build_Data.bat");
        }

        private void button32_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_TU_PreBuild_Data.bat");
        }

        private void button45_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\Integrate_script_headers_from_dev_Live.bat");
        }

        private void button44_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_Live\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev_Live\\Clear Testbeds Debug Scripts.bat");
        }

        private void button43_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\dev_Live_Build_American_Text.bat");
        }

        private void button46_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_Live\\run_scannative.bat");
        }

        private void button41_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup_dev_Live.bat");
        }

        private void button42_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\PS3_Symbol_Backup_Dongled_dev_Live.bat");
        }

        private void button38_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label PreBuild dev_Live.bat");
        }

        private void button39_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label current dev_Live.bat");
        }

        private void button40_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5 Label dev_Live version_xxx.bat");
        }

        private void button37_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_dev_Live_Build_Data.bat");
        }

        private void button35_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools\\script\\util\\BuildDept\\GTA5_QACopier\\Copy_dev_Live_PreBuild_Data.bat");
        }

        private void button48_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\Integrate_script_headers_from_dev_ng.bat");
        }

        private void button47_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_ng\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev_ng\\Clear Testbeds Debug Scripts.bat");
        }

        private void button49_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\Build_American_Text.bat");
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\Build_American_Update_Text.bat");
        }

        private void button50_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_PreBuild Label.bat");
        }

        private void button51_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_QACopier\\DFSR_Copy_Transfer_Data.bat");
        }

        private void button52_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_Current Label.bat");
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5 Label Current PC.bat");
        }

        private void button53_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_version_xxx label.bat");
        }

        private void button54_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\src\\dev_ng\\game\\shader_source\\VS_Project\\batch\\rsm_rebuild_win32.bat", "dev");
        }

        private void button55_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_QACopier\\Deployed_Orbis_prebuild_copier.bat");
        }

        private void button56_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_QACopier\\Deployed_Durango_prebuild_copier.bat");
        }

        private void button58_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_QACopier\\Deployed_Orbis_CURRENT_copier.bat");
        }

        private void button57_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_QACopier\\Deployed_Durango_CURRENT_copier.bat");
        }
     }
 }
