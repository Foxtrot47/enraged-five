﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using System.Net;
using System.IO;
using System.Security.Permissions;
using Outlook = Microsoft.Office.Interop.Outlook;


namespace BuildDeploymentTool
{
    
    public partial class BuildDeploymentMenu : Form
    {
        public List<Bug> BugList = new List<Bug>();
        public BuildDeploymentMenu()
        {
            InitializeComponent();
        }

        private void button34_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Integrate_script_headers_from_dev_network.bat");
        }

        private void button33_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_network\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev_network\\Clear Testbeds Debug Scripts.bat");
        }

        private void button25_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\TU_Build_American_Text.bat");
        }

        private void button26_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label PreBuild TU.bat");
        }

        private void button27_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label current TU.bat");
        }

        private void button28_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label TU version_xxx.bat");
        }

        private void button29_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\PS3_Symbol_Backup_TU.bat");
        }

        private void button30_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\PS3_Symbol_Backup_Dongled_TU.bat");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_network\\run_scannative.bat");
        }

        private void button31_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label TU newprebuildcodeandscript.bat");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_TU_Build_Data.bat");
        }

        private void button32_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_TU_PreBuild_Data.bat");
        }

        private void button45_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Integrate_script_headers_from_dev_Live.bat");
        }

        private void button44_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_Live\\Clear Debug Script Files.bat");
            Process.Start("X:\\gta5\\script\\dev_Live\\Clear Testbeds Debug Scripts.bat");
        }

        private void button43_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\dev_Live_Build_American_Text.bat");
        }

        private void button46_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\script\\dev_Live\\run_scannative.bat");
        }

        private void button41_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\PS3_Symbol_Backup_dev_Live.bat");
        }

        private void button42_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\PS3_Symbol_Backup_dev_Live_US.bat");
        }

        private void button38_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label PreBuild dev_Live.bat");
        }

        private void button39_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label current dev_Live.bat");
        }

        private void button40_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label dev_Live version_xxx.bat");
        }

        private void button37_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_dev_Live_Build_Data.bat");
        }

        private void button35_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_dev_Live_PreBuild_Data.bat");
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button36_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\GTA5 Label TU newprebuildcode_Live.bat");
        }

        private void Build_PS3_Patches_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Build_PS3_Patches.bat");
        }

        private void Copy_PS3_Patches_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_PS3_Patches_To_Distribution.bat");
        }

        private void Copy_360_Patches_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_360_Patches_To_Distribution.bat");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_Patches.bat");
        }

        private void Copy_DLC_to_Distro_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Copy_DLC_To_Build_Directories.bat");
        }

        private void Copy_Edats_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_And_Copy_Edat.bat");
        }

        private void Checkout_PS3_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_PS3.bat");
        }

        private void Checkout_360_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_360.bat");
        }

        private void Checkout_Scripts_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_Scripts.bat");
        }

        private void Checkout_Version_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out_Version.bat");
        }

        private void Create_and_Copy_FTP_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Create_and_Copy_FTP_directory.bat");    
        }

        private void Delete_CMP_Click(object sender, EventArgs e)
        {
             Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\DeleteCMP.bat");
        }

        private void Reduce_To_Final_Click(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Reduce_To_Final.bat");
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\TU_GetNewPrebuildCodeAndScriptLabel.bat");
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\TU_GetNewPrebuildCodeLabelLive.bat");   
        }

        private void BuildDeploymentMenu_Load(object sender, EventArgs e)
        {            
            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\CLList.txt"))
            {

                if (CB_Dev_Live.Checked == true)
                {
                    Build_Deployment_Tool.TabPages.Remove(Dev_Net);
                    RB_Dev_Live.Checked = true;
                    CBDev_Net.Checked = false;
                    CB_Dev_Network.Checked = false;
                }
                
                
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\CLList.txt");

                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "[5-9][0-9][0-9][0-9][0-9][0-9][0-9]";

                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[300];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                    case 0 :
                            TB_Rage.Text = m.Value;
                            TB_Rage2.Text = m.Value;
                    	break;
                    case 1 :
                            TB_Src.Text = m.Value;
                            TB_Src2.Text = m.Value;
                    	break;
                    case 2 :
                            TB_Script.Text = m.Value;
                            TB_Script2.Text = m.Value;
                    	break;
                    case 3 :
                            TB_Text.Text = m.Value;
                            TB_Text2.Text = m.Value;
                    	break;
                    case 4 :
                            TB_Data.Text = m.Value;
                            TB_Data2.Text = m.Value;
                    	break;
                    }
                    i++;
                }
                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx2 = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
                MatchCollection mc2 = rx2.Matches(SrcCLOutput);
                test = new string[1500];
                foreach (Match m in mc2)
                {
                    LBL_OldCLTime.Text = m.Value;
                }
            
            }
            if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\OldCLList.txt"))
            {
                SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\OldCLList.txt");

                //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
                string pattern = "[5-9][0-9][0-9][0-9][0-9][0-9][0-9]";
                
                Regex rx = new Regex(pattern, RegexOptions.None);
                MatchCollection mc = rx.Matches(SrcCLOutput);
                string[] test = new string[300];
                int i = 0;

                foreach (Match m in mc)
                {
                    switch (i)
                    {
                        case 0:
                            TB_Rage1.Text = m.Value;
                            break;
                        case 1:
                            TB_Src1.Text = m.Value;
                            break;
                        case 2:
                            TB_Script1.Text = m.Value;
                            break;
                        case 3:
                            TB_Text1.Text = m.Value;
                            break;
                        case 4:
                            TB_Data1.Text = m.Value;
                            break;
                    }
                    i++;
                }
                pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
                Regex rx3 = new Regex(pattern, RegexOptions.None);
                MatchCollection mc3 = rx3.Matches(SrcCLOutput);
                test = new string[1500];

                foreach (Match m in mc3)
                {
                    LBL_NewCLTime.Text = m.Value;
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button_text_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Text" + DevNetworkChecked);
            if (Temp.WaitForExit(300000))
            {
                if (System.IO.File.Exists("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Sync_textOutput.txt"))
                {
                    TextCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    TextCL = Regex.Match(TextCLOutput, @"\d+").Value;
                    TB_Text.Text = TextCL;
                    TB_Text2.Text = TextCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                }
            }
        }

        private void button_script_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Script" + DevNetworkChecked);
            {
                if (Temp.WaitForExit(300000))
                {
                    ScriptCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    ScriptCL = Regex.Match(ScriptCLOutput, @"\d+").Value;
                    TB_Script.Text = ScriptCL;
                    TB_Script2.Text = ScriptCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                }
            }
        }

        private void button_data_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Data" + DevNetworkChecked);
            {
                if (Temp.WaitForExit(300000))
                {
                    DataCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    DataCL = Regex.Match(DataCLOutput, @"\d+").Value;
                    TB_Data.Text = DataCL;
                    TB_Data2.Text = DataCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                    if (CBJPN.Checked)
                    {
                        Process Temp2 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "JPN" + DevNetworkChecked);
                    }
                }
            }
        }

        private void button_Src_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Source" + DevNetworkChecked + XlastChecked);
            {
                if (Temp.WaitForExit(300000))
                {
                    SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    SrcCL = Regex.Match(SrcCLOutput, @"\d+").Value;
                    TB_Src2.Text = SrcCL;
                    TB_Src.Text = SrcCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                    if (CBXlast.Checked)
                    {
                        Process Temp2 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Xlast" + DevNetworkChecked);
                    }
                }
            }
        }

        private void button_Rage_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Rage" + DevNetworkChecked);
            {
                if (Temp.WaitForExit(300000))
                {
                    RageCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    RageCL = Regex.Match(RageCLOutput, @"\d+").Value;
                    TB_Rage2.Text = RageCL;
                    TB_Rage.Text = RageCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                }
            }
        }

        private void button_All_Click(object sender, EventArgs e)
        {
            Process Temp = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Rage" + DevNetworkChecked);
            {
                if (Temp.WaitForExit(300000))
                {
                    RageCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                    RageCL = Regex.Match(RageCLOutput, @"\d+").Value;
                    TB_Rage.Text = RageCL;
                    TB_Rage2.Text = RageCL;
                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                    Process Temp2 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Source" + DevNetworkChecked + XlastChecked);
                    {
                        if (Temp2.WaitForExit(300000))
                        {
                            SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                            SrcCL = Regex.Match(SrcCLOutput, @"\d+").Value;
                            TB_Src.Text = SrcCL;
                            TB_Src2.Text = SrcCL;
                            System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                            Process Temp3 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Data" + DevNetworkChecked);
                            {
                                if (Temp3.WaitForExit(300000))
                                {
                                    DataCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                                    DataCL = Regex.Match(DataCLOutput, @"\d+").Value;
                                    TB_Data.Text = DataCL;
                                    TB_Data2.Text = DataCL;
                                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                                    Process Temp4 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Script" + DevNetworkChecked);
                                    {
                                        if (Temp4.WaitForExit(300000))
                                        {
                                            ScriptCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                                            ScriptCL = Regex.Match(ScriptCLOutput, @"\d+").Value;
                                            TB_Script.Text = ScriptCL;
                                            TB_Script2.Text = ScriptCL;
                                            System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                                            Process Temp5 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Text" + DevNetworkChecked);
                                            if (Temp5.WaitForExit(300000))
                                            {
                                                if (System.IO.File.Exists("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Sync_textOutput.txt"))
                                                {
                                                    TextCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt");
                                                    TextCL = Regex.Match(TextCLOutput, @"\d+").Value;
                                                    TB_Text.Text = TextCL;
                                                    TB_Text2.Text = TextCL;
                                                    System.IO.File.WriteAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Sync_textOutput.txt", "");
                                                    if (CBXlast.Checked)
                                                    {
                                                        Process Temp6 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "Xlast" + DevNetworkChecked);
                                                    }
                                                    if (CBJPN.Checked)
                                                    {
                                                        Process Temp7 = Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\sync_text.bat", ForceChecked + "JPN" + DevNetworkChecked);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        private void CB_Force_CheckedChanged(object sender, EventArgs e)
        {
            if (CB_Force.Checked)
            {
                ForceChecked = "Force";
            }
            else
            {
                ForceChecked = "";
            }

        }

        private void CBDev_Net_CheckedChanged(object sender, EventArgs e)
        {
            if (CBDev_Net.Checked)
            {
                DevNetworkChecked = "DevNetwork";
            }
            else
            {
                DevNetworkChecked = "";
            }
        }

        private void TB_ClList_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void BtnWriteToFile_Click(object sender, EventArgs e)
        {
            CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLList.txt");
            string[] lines = { "TimeStamp: " +System.DateTime.Now,"Rage: " + TB_Rage.Text, "Src: " + TB_Src.Text, "Script: " + TB_Script.Text, "Text: "+ TB_Text.Text, "Data: "+TB_Data.Text};
            LBL_NewCLTime.Text = System.DateTime.Now.ToString();
            // WriteAllLines creates a file, writes a collection of strings to the file, 
            // and then closes the file.
            System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\CLList.txt", lines);
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLList.txt");
            

        }

        private void CBXlast_CheckedChanged(object sender, EventArgs e)
        {
            if (CBXlast.Checked)
            {
                XlastChecked = "Xlast";
            }
            else
            {
                XlastChecked = "";
            }
        }

        private void CBGameTextScript_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CBJPN_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
        }


        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if ((TBUser.Text!="" || TBUser.Text!=null)&&(TBPassword.Text!=""||TBPassword.Text!=null))
            {
                cookieContainer = new CookieContainer();
                                
                BtnLogin.Enabled = false;
                string username = TBUser.Text;
                string password = TBPassword.Text;

                Uri getUri = new Uri(uriStr + "rest/Projects");
                HttpWebRequest httpProjectGet = (HttpWebRequest)HttpWebRequest.Create(getUri);
                httpProjectGet.Method = "GET";
                
                httpProjectGet.CookieContainer = cookieContainer;

                HttpWebResponse getProjectResponse = (HttpWebResponse)httpProjectGet.GetResponse();
                Stream stream = getProjectResponse.GetResponseStream();
                String responseuri = getProjectResponse.ResponseUri.OriginalString;
                String sessionid =  responseuri.Substring(responseuri.LastIndexOf("jsessionid=") + "jsessionid=".Length);

                Uri postUri = new Uri(uriStr + "j_spring_security_check");
                HttpWebRequest httpPost = (HttpWebRequest)HttpWebRequest.Create(postUri);

                httpPost.CookieContainer = cookieContainer;
                httpPost.ContentType = "application/x-www-form-urlencoded";
                httpPost.Method = "POST";
                                     
                String _authString = "j_username=" + username + "%40rockstar.t2.corp&j_password=" + password;
                //create authentication string
                Byte[] _outBuffer = System.Text.Encoding.ASCII.GetBytes(_authString);  //store in byte buffer           
                Stream _str = httpPost.GetRequestStream();
                _str.Write(_outBuffer, 0, _outBuffer.Length); //update form
                _str.Close();
                HttpWebResponse PostLoginResponse = (HttpWebResponse)httpPost.GetResponse();
                // httpPost.ContentLength = _outBuffer.Length;
                if (PostLoginResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                {
                    MessageBox.Show("Username or password Incorrect", "Important Message");
                    BtnLogin.Enabled = true;
                    TBUser.Enabled = true;
                    TBPassword.Enabled = true;
                    LBLTimer.Visible = false;
                    BTNReport.Enabled = false;
                    httpProjectGet.Abort();
                    httpPost.Abort();
                }
                else
                {


                    var strBytearray = System.Text.Encoding.Default.GetString(_outBuffer);
                    Console.WriteLine(strBytearray);

                    HttpWebResponse postResponse = (HttpWebResponse)httpPost.GetResponse();
                    Stream postStream = postResponse.GetResponseStream();

                    StreamReader poststr = new StreamReader(postStream);

                    Console.WriteLine(poststr.ReadToEnd());

                    postResponseUri = postResponse.ResponseUri.OriginalString;
                    String postSessionId = postResponseUri.Substring(postResponseUri.LastIndexOf("jsessionid=") + "jsessionid=".Length);
                    String responseCode = postResponse.StatusCode.ToString();
                    Console.WriteLine(responseCode);


                    StreamReader str = new StreamReader(postStream);
                    // String str = str.ReadToEnd();
                    Console.WriteLine(str.ReadToEnd());

                    Uri projectUri = new Uri(uriStr + "rest/Projects/1546/CurrentUser");
                    HttpWebRequest httpPrjGet = (HttpWebRequest)HttpWebRequest.Create(projectUri);
                    httpPrjGet.Method = "GET";
                    // httpPrjGet.Accept = "*/*";          


                    // httpPrjGet.ContentType = "application/x-www-form-urlencoded";
                    // httpPrjGet.Headers.Add("Cookie:" + postSessionId.Trim());

                    httpPrjGet.CookieContainer = cookieContainer;

                    HttpWebResponse getPrjResponse = (HttpWebResponse)httpPrjGet.GetResponse();
                    Stream projectStream = getPrjResponse.GetResponseStream();
                    StreamReader prjstr = new StreamReader(projectStream);

                    string OutputPrjstr = prjstr.ReadToEnd();
                    if (postResponseUri.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                    {
                        MessageBox.Show("Username or password Incorrect", "Important Message");
                        BtnLogin.Enabled = true;
                        TBUser.Enabled = true;
                        TBPassword.Enabled = true;
                        LBLTimer.Visible = false;
                        BTNReport.Enabled = false;
                    }
                    else
                    {
                        paused = false;
                        timer1.Enabled = true;
                        BtnLogin.Enabled = false;
                        TBUser.Enabled = false;
                        TBPassword.Enabled = false;
                        LBLTimer.Visible = true;
                        BTNReport.Enabled = true;
                        minutes = 60;

                    }
                }
            } 
            else
            {
            }
            

        }
        private string RetrieveBugNumber(int BugNumber)
        {
            return null;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            if((minutes == 0) && (hours == 0) && (seconds == 0)){
                Console.WriteLine("timer ended");
                paused = true;
                timer1.Enabled = false;
                BtnLogin.Enabled = true;
                TBUser.Enabled = true;
                TBPassword.Enabled = true;
                LBLTimer.Visible = false;
                BTNReport.Enabled = false;
            }else{
                if(seconds < 1){
                    seconds = 60;
                    if (minutes == 0){minutes = 59;}else{minutes -= 1;}
                }else{
                    seconds -= 1;
                }
            }

            if ((minutes == 10) && (hours == 0) && (seconds == 0)&&timer1.Enabled == true)
            {
                GetBugInfo(2030358);
                minutes = 60;
            }

            LBLTimer.Text = "Timer: " + minutes + ":" + seconds;
        }

        private void CBManualEntry_CheckedChanged(object sender, EventArgs e)
        {
            if (TB_Rage1.Enabled)
            {
                toggled = false;
            }
            else
            {
                toggled = true;
            }
            TB_Rage1.Enabled = toggled;
            TB_Rage2.Enabled = toggled;
            TB_Src1.Enabled = toggled;
            TB_Src2.Enabled = toggled;
            TB_Script1.Enabled = toggled;
            TB_Script2.Enabled = toggled;
            TB_Text1.Enabled = toggled;
            TB_Text2.Enabled = toggled;
            TB_Data1.Enabled = toggled;
            TB_Data2.Enabled = toggled;
            TB_Extra.Enabled = toggled;
        }


        private void Save_Click(object sender, EventArgs e)
        {
            string[] lines = { "TimeStamp: " + System.DateTime.Now, "Rage: " + TB_Rage.Text, "Src: " + TB_Src.Text, "Script: " + TB_Script.Text, "Text: " + TB_Text.Text, "Data: " + TB_Data.Text };
            CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\OldCLList.txt");
            System.IO.File.WriteAllLines(@"X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\OldCLList.txt", lines);
            Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\OldCLList.txt");
            TB_Rage1.Text = TB_Rage.Text;
            TB_Src1.Text = TB_Src.Text;
            TB_Script1.Text = TB_Script.Text;
            TB_Text1.Text = TB_Text.Text;
            TB_Data1.Text = TB_Data.Text;
            LBL_OldCLTime.Text = System.DateTime.Now.ToString();
        }
        
        public enum BugClassEnum { A, B, C, D, TASK, TODO, WISH, TRACK};

        private void BTNReport_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you wish to continue? you will not be able to interact with the tool until the report is complete. ", "Important", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                File.Create(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt").Close();
                BugStringBuilder = new StringBuilder();
                BugStringBuilder.Append("Hi, <br><br>");
                if (CB_Dev_Network.Checked)
                {
                    BugStringBuilder.Append("The Dev_Network Prebuild has been updated in Perforce and Distribution <br><br>");
                }
                else
                {
                    BugStringBuilder.Append("The Dev_Live Prebuild has been updated in Perforce and Distribution <br><br>");
                }

                BugStringBuilder.Append("Rage: " + TB_Rage.Text + "<br>");
                BugStringBuilder.Append("Src: " + TB_Src.Text + "<br>");
                BugStringBuilder.Append("Script: " + TB_Script.Text + "<br>");
                BugStringBuilder.Append("Text: " + TB_Text.Text + "<br>");
                if (TB_Extra.Text != "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data.Text + "<br>");
                    BugStringBuilder.Append("Added: " + TB_Extra.Text + "<br><br>");
                }
                else if (TB_Extra.Text == "")
                {
                    BugStringBuilder.Append("Data: " + TB_Data.Text + "<br><br>");
                }
                BugStringBuilder.Append("Fixes: <br>");
                BugStringBuilder.Append("<ul>");
                GetBugnumbersfromfields();

                //getting the bug details from bugstar list.
                CheckDeleteandCreateFile("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml");
                XmlBugList = "";
                XmlBugList = ListToXMLString(bugstarNumbers);
                System.IO.File.WriteAllText(@"X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", GetBugInfoViaXML(XmlBugList));
                BugList = GetFieldValuesfromXMLList("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml", BugList);
                CheckDeleteandCreateFile(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt");
                bool hasAClass = false, hasBClass = false, hasCClass = false, hasDClass = false, hasTaskClass = false, hasToDoClass = false, hasWishClass = false, hasTrackClass = false;
                BugList = BugList.OrderBy(o => o.bugclass).ThenBy(o => o.summary).ToList();
                foreach (Bug bug in BugList)
                {
                    //For creating the subheadings within the report
                    if (bug.project != "69367" && bug.status != "DEV")
                    {
                        if ((bug.bugclass == BugClassEnum.A.ToString()) & !(hasAClass == true))
                        {
                            //Adding subheadings 
                            hasAClass = true;
                            BugStringBuilder.Append("A Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "A Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.B.ToString()) & !(hasBClass == true))
                        {
                            hasBClass = true;
                            BugStringBuilder.Append("B Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "B Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.C.ToString()) & !(hasCClass == true))
                        {
                            hasCClass = true;
                            BugStringBuilder.Append("C Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "C Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.D.ToString()) & !(hasDClass == true))
                        {
                            hasDClass = true;
                            BugStringBuilder.Append("D Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "D Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TASK.ToString()) & !(hasTaskClass == true))
                        {
                            hasTaskClass = true;
                            BugStringBuilder.Append("Task Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "Task Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TODO.ToString()) & !(hasToDoClass == true))
                        {
                            hasToDoClass = true;
                            BugStringBuilder.Append("ToDo Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "ToDo Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.WISH.ToString()) & !(hasWishClass == true))
                        {
                            hasWishClass = true;
                            BugStringBuilder.Append("Wish Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "Wish Class \n");
                        }
                        if ((bug.bugclass == BugClassEnum.TRACK.ToString()) & !(hasTrackClass == true))
                        {
                            hasTrackClass = true;
                            BugStringBuilder.Append("Track Class");
                            System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", "Track Class \n");
                        }
                        //End of creating the subheadings within the report
                    }

                    if (bug.project != "69367" && bug.status != "DEV")
                    {
                        //assign each string line to the bug details from the bug class.
                        BugStringBuilder.Append("<li><a href='" + "url:Bugstar:" + bug.bugNumber + "'>" + bug.bugNumber + "</a>" + " - " + bug.summary + "</li>");
                        string TextBugHelper = "url:Bugstar:" + bug.bugNumber + " - " + bug.summary + "\n";
                        System.IO.File.AppendAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", TextBugHelper);
                    }

                }
                hasAClass = false; hasBClass = false; hasCClass = false; hasDClass = false; hasTaskClass = false; hasToDoClass = false; hasWishClass = false; hasTrackClass = false;
              //  System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt", test);
                Process.Start("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\BugOutput.txt");
                BugStringBuilder.Append("<ul>");
                CreateOutlookEmail();
                bugstarNumbers.Clear();
                BugList.Clear();
            }
            else if (dialogResult == DialogResult.No)
            {
          
            }
        }
        
        //Gets the bug information and from int BugNumber and returns a string BugDetails
        public string GetBugInfo(int BugNumber)
        {
            string BugDetails = "";
            if ( BugNumber != 0)
            {
                Uri bugUri = new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Bugs/" + BugNumber);
                HttpWebRequest httpPrjGet = (HttpWebRequest)HttpWebRequest.Create(bugUri);
                httpPrjGet.Method = "GET";
                httpPrjGet.CookieContainer = cookieContainer;
                HttpWebResponse getBugResponse = (HttpWebResponse)httpPrjGet.GetResponse();

                if(getBugResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                {
                    MessageBox.Show("Username or password Incorrect", "Important Message");
                    BtnLogin.Enabled = true;
                    TBUser.Enabled = true;
                    TBPassword.Enabled = true;
                    LBLTimer.Visible = false;
                    BTNReport.Enabled = false;
                    httpPrjGet.Abort();
                    timer1.Enabled = false;
                    minutes = 0;
                    seconds = 0;
                    return "";
                }else
                {
                    Stream BugStream = getBugResponse.GetResponseStream();
                    StreamReader str = new StreamReader(BugStream);
                    // write info to string
                    BugDetails = str.ReadToEnd();
                }
            }
            return BugDetails;
        }

        private void CheckDeleteandCreateFile(string file)
        {
            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
            System.IO.File.Create(file).Dispose();

        }

        public string GetFieldValuefromXML(string file, string field, string name)
        {
            string fieldValue = "";
            XmlDocument xmlDoc = new XmlDocument();
            if (File.Exists(file)) { 
                xmlDoc.Load(file);
                XmlNodeList parentNode = xmlDoc.GetElementsByTagName(field);
                foreach (XmlNode childNode in parentNode)
                {
                    fieldValue = childNode.SelectSingleNode(name).InnerText;
                }
            }
            return fieldValue;
        }

        public void CheckBoxExtraFields(string CLNumber) 
        {


        }

        public string[] RegexPatternRecogniser(string input, string regexPattern, int arrayLength)
        {
            string[] returnValues = new string[arrayLength];
            int i = 0;
            string pattern = regexPattern;
            Regex rx = new Regex(pattern, RegexOptions.None);
            MatchCollection mc = rx.Matches(input);
            foreach (Match m in mc)
            {
                returnValues[i] = m.Value;
                i++;
            }
            return returnValues;
        }


        private void SyncPage_Click(object sender, EventArgs e)
        {

        }
        private void CreateOutlookEmail()
        {
            try
            {
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook.MailItem mailItem = (Outlook.MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                if (CB_Dev_Network.Checked)
                {
                    mailItem.Subject = "Dev_Network Prebuild Updated";
                }
                else
                {
                    mailItem.Subject = "Dev_Live Prebuild Updated";
                }
                mailItem.To = "";
                mailItem.HTMLBody = BugStringBuilder.ToString();
                mailItem.Importance = Outlook.OlImportance.olImportanceLow;
                mailItem.Display(true);
            }
            catch
            {
                MessageBox.Show("Please close the existing new email before attempting to create a new one ", "Important Message");
            }
        }
        public bool CheckIntListContains(int iBugnumber, List<int> intList)
        {
            bool canAdd = true;
            foreach (int bugNumber in intList)
            {
                if (bugNumber == iBugnumber)
                {
                    canAdd = false;
                }
            }
            return canAdd;

        }

        /// <summary>
        /// Scans through an xml file looking for specific fields and then returns the data within that field.
        /// </summary>
        public List<Bug> GetFieldValuesfromXMLList(string file, List<Bug> BugListRef)
        {
            //we want to return nothing if the string doesnt exist
            //instantiate a new xml document 
            XmlDocument xmlDoc = new XmlDocument();
            //check to see if the xml file provided exists
            if (File.Exists(file))
            {
                // Error out gracefully if the xml data is a mess
                try
                {
                    xmlDoc.Load(file);//Load the xml document

                    XmlNodeList XmlList = xmlDoc.GetElementsByTagName("bug");

                    //Loop through the bugs
                    foreach (XmlElement xmlElement in XmlList)
                    {
                        string bugnumber = "";
                        string summary = "";
                        string bugclass = "";
                        string project = "";
                        string status = "";
                        foreach (XmlElement xmlElement1 in xmlElement.ChildNodes)
                        {
                            if (xmlElement1.Name == "id")
                            {
                                bugnumber = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "summary")
                            {
                                summary = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "category")
                            {
                                bugclass = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "projectId")
                            {
                                project = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "state")
                            {
                                status = xmlElement1.InnerText;
                            }
                        }
                        Bug newBug = new Bug(Convert.ToInt32(bugnumber), summary, bugclass, project, status);
                        BugListRef.Add(newBug);
                    }
                }
                catch
                {
                    if (file != "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml")//hack to prevent spamming of xml check if b* returns garbled mess.
                    {
                        MessageBox.Show("Please make sure your Xml is correct.", "Important Message");
                    }
                }
            }
            return BugListRef;
        }
        public List<XMLFile> GetFieldValuesfromXMLList(string file)
        {
            List<XMLFile> XMLFiledata = new List<XMLFile>();
            //we want to return nothing if the string doesnt exist
            //instantiate a new xml document 
            XmlDocument xmlDoc = new XmlDocument();
            //check to see if the xml file provided exists
            if (File.Exists(file))
            {
                // Error out gracefully if the xml data is a mess
                try
                {
                    xmlDoc.Load(file);//Load the xml document

                    XmlNodeList XmlList = xmlDoc.GetElementsByTagName("File");

                    //Loop through the bugs
                    foreach (XmlElement xmlElement in XmlList)
                    {
                        string Name = "";
                        string Location = "";
                        foreach (XmlElement xmlElement1 in xmlElement.ChildNodes)
                        {
                            if (xmlElement1.Name == "Name")
                            {
                                Name = xmlElement1.InnerText;
                            }
                            if (xmlElement1.Name == "Location")
                            {
                                Location = xmlElement1.InnerText;
                            }
                        }
                        XMLFile newXmlFile = new XMLFile(Name, Location);
                        XMLFiledata.Add(newXmlFile);
                    }
                }
                catch
                {
                    if (file != "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_NG_BuildTools\\bugdetails.xml")//hack to prevent spamming of xml check if b* returns garbled mess.
                    {
                        MessageBox.Show("Please make sure your Xml is correct.", "Important Message");
                    }
                }
            }
            return XMLFiledata;
        }
        
        public string ListToXMLString(List<int> intList)
        {
            string XmlString = "";
            XmlString = "<BugIds>";
            foreach (int ListItem in intList)
            {
                XmlString += "<id>" + ListItem + "</id>";
            }
            XmlString += "</BugIds>";

            return XmlString;
        }

        public string GetBugInfoViaXML(string xmlList)
        {
            String BugDetails = "";
            //make sure the bugnumber is valid first
            //send the info to the rest serivice so that the cookie can get authenticated.
            Uri postUri = new Uri("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/rest/Bugs/BugsById?fields=Id,Summary,Category,Project,State");
            HttpWebRequest httpPost = (HttpWebRequest)HttpWebRequest.Create(postUri);

            httpPost.ContentType = "application/xml";
            httpPost.Method = "POST";
            httpPost.CookieContainer = cookieContainer;


            byte[] postData = Encoding.UTF8.GetBytes(xmlList);
            httpPost.ContentLength = postData.Length;
            Stream dataStream = httpPost.GetRequestStream();
            dataStream.Write(postData, 0, postData.Length);
            dataStream.Close();
                                    
            try
            {
                HttpWebResponse PostLoginResponse = (HttpWebResponse)httpPost.GetResponse();
                if (!httpPost.GetResponse().ResponseUri.OriginalString.Contains("login.jsp?login_error=1"))
                {
                    HttpWebResponse getBugResponse = (HttpWebResponse)httpPost.GetResponse();
                    HttpStatusCode responseStatusCode = getBugResponse.StatusCode;
                    if (getBugResponse.ResponseUri.OriginalString.Contains("https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/login.jsp?login_error=1"))
                    {
                        MessageBox.Show("Username or password Incorrect", "Important Message");
                        BtnLogin.Enabled = true;
                        TBUser.Enabled = true;
                        TBPassword.Enabled = true;
                        BTNReport.Enabled = false;
                        httpPost.Abort();
                        timer1.Enabled = false;
                        minutes = 0;
                        seconds = 0;
                        return "";
                    }
                    else
                    {
                        //get the bug info from the html stream coming from rest urlresponse.
                        Stream BugStream = getBugResponse.GetResponseStream();
                        StreamReader str = new StreamReader(BugStream);
                        // write info to string
                        BugDetails = str.ReadToEnd();
                    }
                }
            }
            catch
            {

            }
            return BugDetails;
        }

        public void GetBugnumbersfromfields()
        {
            bugstarNumbers.Clear();
            string[] rageArgs;
            string[] textArgs;
            string[] sourceArgs;
            string[] dataArgs;
            string[] scriptArgs;
            if (CB_Dev_Network.Checked == true)
            {
                rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//rage/gta5/dev_network/" };
                textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/assets/GameText/TitleUpdate/" };
                sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/src/dev_network/" };
                dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/dev/" };
                scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/script/dev_network/" };
            }
            else
            {
                rageArgs = new string[] { "", TB_Rage1.Text, TB_Rage2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//rage/gta5/dev_Live/" };
                textArgs = new string[] { "", TB_Text1.Text, TB_Text2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/assets/GameText/dev_Live/" };
                sourceArgs = new string[] { "", TB_Src1.Text, TB_Src2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/src/dev_Live/" };
                dataArgs = new string[] { "", TB_Data1.Text, TB_Data2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/titleupdate/dev_Live/" };
                scriptArgs = new string[] { "", TB_Script1.Text, TB_Script2.Text, "X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Test2.txt", "//depot/gta5/script/dev_Live/" };
            }

            ProcessStartInfo SourceStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLExtractor.bat", string.Join(" ", sourceArgs));
            ProcessStartInfo RageStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLExtractor.bat", string.Join(" ", rageArgs));
            ProcessStartInfo ScriptStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLExtractor.bat", string.Join(" ", scriptArgs));
            ProcessStartInfo TextStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLExtractor.bat", string.Join(" ", textArgs));
            ProcessStartInfo DataStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLExtractor.bat", string.Join(" ", dataArgs));
            Process SourceGrab = new Process();
            Process RageGrab = new Process();
            Process ScriptGrab = new Process();
            Process TextGrab = new Process();
            Process DataGrab = new Process();


            if (TB_Rage1.Text != TB_Rage2.Text)
            {
                RageGrab.StartInfo = RageStartInfo;
                RageGrab.Start();
                RageGrab.WaitForExit(20000);
            }
            if (TB_Src1.Text != TB_Src2.Text)
            {
                SourceGrab.StartInfo = SourceStartInfo;
                SourceGrab.Start();
                SourceGrab.WaitForExit(20000);

            }
            if (TB_Text1.Text != TB_Text2.Text)
            {
                TextGrab.StartInfo = TextStartInfo;
                TextGrab.Start();
                TextGrab.WaitForExit(20000);

            }
            if (TB_Data1.Text != TB_Data2.Text)
            {
                DataGrab.StartInfo = DataStartInfo;
                DataGrab.Start();
                DataGrab.WaitForExit(20000);

            }
            if (TB_Script1.Text != TB_Script2.Text)
            {
                ScriptGrab.StartInfo = ScriptStartInfo;
                ScriptGrab.Start();
                ScriptGrab.WaitForExit(20000);
            }
            string[] otherCLList = new string[100];
            otherCLList = RegexPatternRecogniser(TB_Extra.Text, "[5-6][0-9][0-9][0-9][0-9][0-9][0-9]", 100);
            foreach (string CLNumber in otherCLList)
            {
                if (CLNumber != null && CLNumber != "")
                {
                    if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt"))
                    {
                        ProcessStartInfo otherCLListStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\CLIndividualExtractor.bat", CLNumber);
                        Process otherCLListP = new Process();
                        otherCLListP.StartInfo = otherCLListStartInfo;
                        otherCLListP.Start();
                        otherCLListP.WaitForExit(20000);
                    }
                }
            }
            SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\BugOutput.txt");

            //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
            string pattern = "[1-4][0-9][0-9][0-9][0-9][0-9][0-9]";


            Regex rx = new Regex(pattern, RegexOptions.None);
            MatchCollection mc = rx.Matches(SrcCLOutput);
            //   string[] test = new string[300];
            string[] EmailString = new string[300];
            bugstarNumbers = new List<int>();
            //  int i = 0;
            
            foreach (Match m in mc)
            {
                if (CheckIntListContains(Convert.ToInt32(m.Value), bugstarNumbers))
                {
                    //test[i] = "url:bugstar:" + m.Value;
                    bugstarNumbers.Add(Convert.ToInt32(m.Value));
                    // i++;
                }
            }
        }
     /// <summary>
     /// Bug class that holds the bugNumber,summary, and bugclass.
     /// </summary>
     public class Bug
     {
         public int bugNumber { get; set; }
         public string summary { get; set; }
         public string bugclass { get; set; }
         public string project { get; set; }
         public string status { get; set; }
         public Bug(int BugNumber, string Summary, string BugClass, string Project, string Status)
         {
             bugNumber = BugNumber;
             summary = Summary;
             bugclass = BugClass;
             project = Project;
             status = Status;
         }

     }
     public class XMLFile
     {
         public string Name { get; set; }
         public string Location { get; set; }
         public XMLFile(string name, string location)
         {
             Name = name;
             Location = location;
         }

     }

     private void BTN_Check_Out_Click(object sender, EventArgs e)
     {

         if (CB_Default.Checked)
         {
             string[] COArgs = new string[] { CB_Default.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_Japanese.Checked)
         {
             string[] COArgs = new string[] { CB_Japanese.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_Clifford.Checked)
         {
             string[] COArgs = new string[] { CB_Clifford.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_PS3.Checked)
         {
             string[] COArgs = new string[] { CB_PS3.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_PS3_JPN.Checked)
         {
             string[] COArgs = new string[] { CB_PS3_JPN.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_XBOX.Checked)
         {
             string[] COArgs = new string[] { CB_XBOX.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_XBOX_JPN.Checked)
         {
             string[] COArgs = new string[] { CB_XBOX_JPN.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
         if (CB_Version.Checked)
         {
             string[] COArgs = new string[] { CB_Version.Name, "" + RB_Dev_Net.Checked };
             ProcessStartInfo CoStartInfo = new ProcessStartInfo("X:\\gta5\\tools_ng\\script\\util\\BuildDept\\GTA5_TU_BuildTools\\Check_Out.bat", string.Join(" ", COArgs));
             Process Co = new Process();
             Co.StartInfo = CoStartInfo;
             Co.Start();
         }
     }

     private void BTN_CheckTimeStamps_Click(object sender, EventArgs e)
     {
         RT_TimeStampCheckerName.Clear();
         RT_TimeStampCheckerTime.Clear();
         List<XMLFile> XMLList = new List<XMLFile>();
         XMLList = GetFieldValuesfromXMLList(@""+strFileName);
         var timeDiff =0.0000001d ;
         int i = 0;
         RT_TimeStampCheckerName.AppendText("");
         RT_TimeStampCheckerTime.AppendText("");
         DateTime TimeOfFirstFile = DateTime.Now;

         foreach (XMLFile xmlfile in XMLList)
         {
             if (i == 0)
             {
                 TimeOfFirstFile = File.GetLastWriteTime(xmlfile.Location);
             }
             i+=1;
             if (i % 2 !=0)
             {
                 RT_TimeStampCheckerName.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 RT_TimeStampCheckerName.SelectionColor = Color.Black;
                 RT_TimeStampCheckerName.AppendText(xmlfile.Name + "\n");
                 int firstcharindex = RT_TimeStampCheckerName.GetFirstCharIndexOfCurrentLine();
                 int currentline = RT_TimeStampCheckerName.GetLineFromCharIndex(firstcharindex);
                 string currentlinetext = RT_TimeStampCheckerName.Lines[currentline];
                 RT_TimeStampCheckerName.Select(firstcharindex, currentlinetext.Length);
                 RT_TimeStampCheckerName.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 RT_TimeStampCheckerName.SelectionColor = System.Drawing.Color.Black;

                 try
                 {
                     timeDiff = (File.GetLastWriteTime(@"" + xmlfile.Location) - TimeOfFirstFile).TotalHours;
                     RT_TimeStampCheckerTime.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                     RT_TimeStampCheckerTime.SelectionColor = Color.Black;
                     if (timeDiff >= 0.6 || timeDiff < 0)
                     {
                         RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Red;
                     }
                     if (timeDiff <= 0.6 && timeDiff >= 0)

                     {
                         RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Green;
                     }
 
                     RT_TimeStampCheckerTime.AppendText(File.GetLastWriteTime(xmlfile.Location) + "\n");
                     
                 }
                 catch 
                 {
                     RT_TimeStampCheckerTime.AppendText("Error With File" + "\n");
                 }

                 int firstcharindex2 = RT_TimeStampCheckerTime.GetFirstCharIndexOfCurrentLine();
                 int currentline2 = RT_TimeStampCheckerTime.GetLineFromCharIndex(firstcharindex);
                 string currentlinetext2 = RT_TimeStampCheckerTime.Lines[currentline2];
                 RT_TimeStampCheckerTime.Select(firstcharindex2, currentlinetext2.Length);
                 RT_TimeStampCheckerTime.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 if (timeDiff >= 0.6 || timeDiff < 0)
                 {
                     RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Red;
                 }
                 if (timeDiff <= 0.6 && timeDiff >= 0)
                 {
                     RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Green;
                 }
             }else
             {
                 RT_TimeStampCheckerName.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 RT_TimeStampCheckerName.SelectionColor = Color.Gray;
                 RT_TimeStampCheckerName.AppendText(@"" + xmlfile.Name + "\n");
                 int firstcharindex = RT_TimeStampCheckerName.GetFirstCharIndexOfCurrentLine();
                 int currentline = RT_TimeStampCheckerName.GetLineFromCharIndex(firstcharindex);
                 string currentlinetext = RT_TimeStampCheckerName.Lines[currentline];
                 RT_TimeStampCheckerName.Select(firstcharindex, currentlinetext.Length);
                 RT_TimeStampCheckerName.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 RT_TimeStampCheckerName.SelectionColor = System.Drawing.Color.Gray;


                 try
                 {
                     timeDiff = (File.GetLastWriteTime(@""+xmlfile.Location) - TimeOfFirstFile).TotalHours;
                     RT_TimeStampCheckerTime.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                     RT_TimeStampCheckerTime.SelectionColor = Color.Gray;
                     if (timeDiff >= 0.6 || timeDiff < 0)
                     {
                         RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Red;
                     }
                     if (timeDiff <= 0.6 && timeDiff >= 0)
                     {
                         RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Green;
                     }
 
                     RT_TimeStampCheckerTime.AppendText(File.GetLastWriteTime(xmlfile.Location) + "\n");
                 }
                 catch
                 {

                     RT_TimeStampCheckerTime.AppendText("Error With File" + "\n");
                 }
                 int firstcharindex2 = RT_TimeStampCheckerTime.GetFirstCharIndexOfCurrentLine();
                 int currentline2 = RT_TimeStampCheckerTime.GetLineFromCharIndex(firstcharindex);
                 string currentlinetext2 = RT_TimeStampCheckerTime.Lines[currentline2];
                 RT_TimeStampCheckerTime.Select(firstcharindex2, currentlinetext2.Length);
                 RT_TimeStampCheckerTime.SelectionFont = new Font(RT_TimeStampCheckerName.Font, FontStyle.Bold);
                 if (timeDiff >= 0.6 || timeDiff < 0)
                 {
                     RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Red;
                 }
                 if (timeDiff <= 0.6 && timeDiff >= 0)
                 {
                     RT_TimeStampCheckerTime.SelectionColor = System.Drawing.Color.Green;
                 }
             }
         }
     }


     private void TP_TSChecker_Click(object sender, EventArgs e)
     {

     }

     private void BTN_LoadXML_Click(object sender, EventArgs e)
     {

         //First, declare a variable to hold the user’s file selection.
         string input = string.Empty;

         //Create a new instance of the OpenFileDialog because it's an object.
         OpenFileDialog dialog = new OpenFileDialog();

         //Now set the file type
         dialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";

         //Set the starting directory and the title.
         dialog.InitialDirectory = @"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools"; dialog.Title = "Select a text file";

         //Present to the user.
         if (dialog.ShowDialog() == DialogResult.OK)
         {
             strFileName = dialog.FileName;
         }
         if (strFileName == "")
         {

         }
      }

     private void CB_Dev_Live_CheckedChanged(object sender, EventArgs e)
     {
         if (CB_Dev_Live.Checked==true)
         {
             RB_Dev_Live.Checked = true;
             CB_Dev_Net.Checked = false;
             RB_Dev_Net.Checked = false;
             CBDev_Net.Checked = false;
             CB_Dev_Network.Checked = false;
             Build_Deployment_Tool.TabPages.Remove(Dev_live);
             Build_Deployment_Tool.TabPages.Remove(Dev_Net);
             Build_Deployment_Tool.TabPages.Remove(Tools);
             Build_Deployment_Tool.TabPages.Remove(TP_CheckOut);
             Build_Deployment_Tool.TabPages.Remove(SyncPage);
             Build_Deployment_Tool.TabPages.Remove(TbCLtoBugstar);
             Build_Deployment_Tool.TabPages.Remove(TP_TSChecker);
             Build_Deployment_Tool.TabPages.Add(Dev_live);
             Build_Deployment_Tool.TabPages.Add(Tools);
             Build_Deployment_Tool.TabPages.Add(TP_CheckOut);
             Build_Deployment_Tool.TabPages.Add(SyncPage);
             Build_Deployment_Tool.TabPages.Add(TbCLtoBugstar);
             Build_Deployment_Tool.TabPages.Add(TP_TSChecker);
         }
         if (CB_Dev_Live.Checked==false)
         {
             CB_Dev_Net.Checked = true;
         }
     }

     private void CB_Dev_Net_CheckedChanged(object sender, EventArgs e)
     {
         if (CB_Dev_Net.Checked == true)
         {
             CB_Dev_Live.Checked = false;
             RB_Dev_Net.Checked = true;
             CBDev_Net.Checked = true;
             CB_Dev_Network.Checked = true;
             Build_Deployment_Tool.TabPages.Remove(Dev_live);
             Build_Deployment_Tool.TabPages.Remove(Dev_Net);
             Build_Deployment_Tool.TabPages.Remove(Tools);
             Build_Deployment_Tool.TabPages.Remove(TP_CheckOut);
             Build_Deployment_Tool.TabPages.Remove(SyncPage);
             Build_Deployment_Tool.TabPages.Remove(TbCLtoBugstar);
             Build_Deployment_Tool.TabPages.Remove(TP_TSChecker);
             Build_Deployment_Tool.TabPages.Add(Dev_Net);
             Build_Deployment_Tool.TabPages.Add(Tools);
             Build_Deployment_Tool.TabPages.Add(TP_CheckOut);
             Build_Deployment_Tool.TabPages.Add(SyncPage);
             Build_Deployment_Tool.TabPages.Add(TbCLtoBugstar);
             Build_Deployment_Tool.TabPages.Add(TP_TSChecker);
         }
         if (CB_Dev_Net.Checked == false)
         {
             CB_Dev_Live.Checked = true;
         }
     }

     private void BTN_Refresh_Old_CL_Click(object sender, EventArgs e)
     {

         if (System.IO.File.Exists(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\OldCLList.txt"))
         {
             SrcCLOutput = System.IO.File.ReadAllText(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\OldCLList.txt");

             //  Regex pattern = new Regex("[0-4][0-9][0-9][0-9][0-9][0-9][0-9]");
             string pattern = "[5-9][0-9][0-9][0-9][0-9][0-9][0-9]";
             // System.IO.File.WriteAllLines(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Test2.txt", textoutput);

             Regex rx = new Regex(pattern, RegexOptions.None);
             MatchCollection mc = rx.Matches(SrcCLOutput);
             string[] test = new string[1500];
             int i = 0;

             foreach (Match m in mc)
             {
                 switch (i)
                 {
                     case 0:
                         TB_Rage1.Text = m.Value;
                         break;
                     case 1:
                         TB_Src1.Text = m.Value;
                         break;
                     case 2:
                         TB_Script1.Text = m.Value;
                         break;
                     case 3:
                         TB_Text1.Text = m.Value;
                         break;
                     case 4:
                         TB_Data1.Text = m.Value;
                         break;
                 }
                 i++;
             }
             pattern = @"[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9][\s][0-9][0-9][:][0-9][0-9][:][0-9][0-9]";
             Regex rx2 = new Regex(pattern, RegexOptions.None);
             MatchCollection mc2 = rx2.Matches(SrcCLOutput);
             test = new string[1500];

             foreach (Match m in mc2)
             {
                 LBL_OldCLTime.Text = m.Value;
             }

         }
     }

     private void BTN_DLCTEXTBUILDER_Click(object sender, EventArgs e)
     {
         Process Temp = Process.Start(@"X:\gta5\tools_ng\bin\DLCTextBuilder\DLCTextBuilder.exe");
     }

     private void BTN_CONTENTSTAR_Click(object sender, EventArgs e)
     {
         Process Temp = Process.Start(@"X:\gta5\tools_ng\bin\ContentStar\ContentStar.exe");
     }

     private void BTN_SYNCDLC_Click(object sender, EventArgs e)
     {
         Process.Start(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\CGbuilder_data_get_latest_spDLC.bat");
         Process.Start(@"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\CGbuilder_data_get_latest_mpDLC.bat");
     }
    }


 }
