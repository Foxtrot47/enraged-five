﻿using System.Net;
using System.Text;
using System.Collections.Generic;

namespace BuildDeploymentTool
{
    partial class BuildDeploymentMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        string strFileName = @"X:\gta5\tools_ng\script\util\BuildDept\GTA5_TU_BuildTools\Dev_Live_TimeStampSetup.xml";
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildDeploymentMenu));
            this.panel4 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button25 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button33 = new System.Windows.Forms.Button();
            this.IntegrateScriptHeadersDevNetwork = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button43 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.Build_Deployment_Tool = new System.Windows.Forms.TabControl();
            this.Dev_live = new System.Windows.Forms.TabPage();
            this.Dev_Net = new System.Windows.Forms.TabPage();
            this.Tools = new System.Windows.Forms.TabPage();
            this.Build_Panel = new System.Windows.Forms.Panel();
            this.BTN_SYNCDLC = new System.Windows.Forms.Button();
            this.BTN_CONTENTSTAR = new System.Windows.Forms.Button();
            this.BTN_DLCTEXTBUILDER = new System.Windows.Forms.Button();
            this.Create_and_Copy_FTP = new System.Windows.Forms.Button();
            this.Reduce_To_Final = new System.Windows.Forms.Button();
            this.Delete_CMP = new System.Windows.Forms.Button();
            this.Build_Label = new System.Windows.Forms.Label();
            this.DLC_Panel = new System.Windows.Forms.Panel();
            this.Copy_Edats = new System.Windows.Forms.Button();
            this.Copy_DLC_to_Distro = new System.Windows.Forms.Button();
            this.DLC_Label = new System.Windows.Forms.Label();
            this.Patches_Panel = new System.Windows.Forms.Panel();
            this.Checkout_Patches = new System.Windows.Forms.Button();
            this.Copy_360_Patches = new System.Windows.Forms.Button();
            this.Copy_PS3_Patches = new System.Windows.Forms.Button();
            this.Build_PS3_Patches = new System.Windows.Forms.Button();
            this.Patches_Label = new System.Windows.Forms.Label();
            this.TP_CheckOut = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BTN_Check_Out = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_XBOX_JPN = new System.Windows.Forms.CheckBox();
            this.CB_PS3_JPN = new System.Windows.Forms.CheckBox();
            this.CB_XBOX = new System.Windows.Forms.CheckBox();
            this.CB_PS3 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CB_Clifford = new System.Windows.Forms.CheckBox();
            this.CB_Japanese = new System.Windows.Forms.CheckBox();
            this.CB_Default = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.RB_Dev_Net = new System.Windows.Forms.RadioButton();
            this.RB_Dev_Live = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CB_Version = new System.Windows.Forms.CheckBox();
            this.SyncPage = new System.Windows.Forms.TabPage();
            this.CBJPN = new System.Windows.Forms.CheckBox();
            this.CBXlast = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Save = new System.Windows.Forms.Button();
            this.BtnWriteToFile = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextLabel = new System.Windows.Forms.Label();
            this.TB_Data = new System.Windows.Forms.TextBox();
            this.TB_Src = new System.Windows.Forms.TextBox();
            this.TB_Rage = new System.Windows.Forms.TextBox();
            this.TB_Script = new System.Windows.Forms.TextBox();
            this.TB_Text = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_All = new System.Windows.Forms.Button();
            this.button_Rage = new System.Windows.Forms.Button();
            this.button_Src = new System.Windows.Forms.Button();
            this.button_data = new System.Windows.Forms.Button();
            this.button_script = new System.Windows.Forms.Button();
            this.button_text = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CBDev_Net = new System.Windows.Forms.CheckBox();
            this.CB_Force = new System.Windows.Forms.CheckBox();
            this.TbCLtoBugstar = new System.Windows.Forms.TabPage();
            this.LBL_NewCLTime = new System.Windows.Forms.Label();
            this.LBL_OldCLTime = new System.Windows.Forms.Label();
            this.BTN_Refresh_Old_CL = new System.Windows.Forms.Button();
            this.CB_Dev_Network = new System.Windows.Forms.CheckBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TB_Extra = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.TB_Data2 = new System.Windows.Forms.TextBox();
            this.TB_Src2 = new System.Windows.Forms.TextBox();
            this.TB_Rage2 = new System.Windows.Forms.TextBox();
            this.TB_Script2 = new System.Windows.Forms.TextBox();
            this.TB_Text2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TB_Data1 = new System.Windows.Forms.TextBox();
            this.TB_Src1 = new System.Windows.Forms.TextBox();
            this.TB_Rage1 = new System.Windows.Forms.TextBox();
            this.TB_Script1 = new System.Windows.Forms.TextBox();
            this.TB_Text1 = new System.Windows.Forms.TextBox();
            this.BTNReport = new System.Windows.Forms.Button();
            this.CBManualEntry = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LBRestLogin = new System.Windows.Forms.Label();
            this.LBLTimer = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.TBUser = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.TP_TSChecker = new System.Windows.Forms.TabPage();
            this.BTN_LoadXML = new System.Windows.Forms.Button();
            this.RT_TimeStampCheckerTime = new System.Windows.Forms.RichTextBox();
            this.BTN_CheckTimeStamps = new System.Windows.Forms.Button();
            this.RT_TimeStampCheckerName = new System.Windows.Forms.RichTextBox();
            this.ToolTipPatches = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CB_Dev_Live = new System.Windows.Forms.CheckBox();
            this.CB_Dev_Net = new System.Windows.Forms.CheckBox();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.Build_Deployment_Tool.SuspendLayout();
            this.Dev_live.SuspendLayout();
            this.Dev_Net.SuspendLayout();
            this.Tools.SuspendLayout();
            this.Build_Panel.SuspendLayout();
            this.DLC_Panel.SuspendLayout();
            this.Patches_Panel.SuspendLayout();
            this.TP_CheckOut.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SyncPage.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.TbCLtoBugstar.SuspendLayout();
            this.panel3.SuspendLayout();
            this.TP_TSChecker.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button3);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.button32);
            this.panel4.Controls.Add(this.button31);
            this.panel4.Controls.Add(this.button14);
            this.panel4.Controls.Add(this.button26);
            this.panel4.Controls.Add(this.button27);
            this.panel4.Controls.Add(this.button28);
            this.panel4.Location = new System.Drawing.Point(240, 21);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(186, 283);
            this.panel4.TabIndex = 14;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 244);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(172, 32);
            this.button3.TabIndex = 38;
            this.button3.Text = "Sync New Prebuild Code";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Prebuild/Current build release";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(6, 209);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(172, 32);
            this.button32.TabIndex = 36;
            this.button32.Text = "Update PreBuild TU build";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(6, 30);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(172, 32);
            this.button31.TabIndex = 35;
            this.button31.Text = "Label TU NewPreBuildCode";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(6, 171);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(172, 32);
            this.button14.TabIndex = 33;
            this.button14.Text = "Update QA TU build";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(6, 66);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(172, 31);
            this.button26.TabIndex = 26;
            this.button26.Text = "Label PreBuild TU";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(6, 100);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(172, 32);
            this.button27.TabIndex = 27;
            this.button27.Text = "Label Current TU";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(6, 135);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(172, 32);
            this.button28.TabIndex = 28;
            this.button28.Text = "Label TU version xxx";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.button29);
            this.panel5.Location = new System.Drawing.Point(22, 202);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(186, 102);
            this.panel5.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(172, 32);
            this.button1.TabIndex = 32;
            this.button1.Text = "Backup US PS3 symbols";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "PS3 TU symbol backup";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(7, 28);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(172, 32);
            this.button29.TabIndex = 29;
            this.button29.Text = "Backup TU PS3 symbols";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.button25);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.button33);
            this.panel7.Controls.Add(this.IntegrateScriptHeadersDevNetwork);
            this.panel7.Controls.Add(this.button7);
            this.panel7.Location = new System.Drawing.Point(22, 21);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(186, 175);
            this.panel7.TabIndex = 12;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(7, 100);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(172, 32);
            this.button25.TabIndex = 14;
            this.button25.Text = "Build \"American\" text";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(58, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Script Utilities";
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(7, 65);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(172, 32);
            this.button33.TabIndex = 12;
            this.button33.Text = "Clear Debug Script Files";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // IntegrateScriptHeadersDevNetwork
            // 
            this.IntegrateScriptHeadersDevNetwork.Location = new System.Drawing.Point(7, 30);
            this.IntegrateScriptHeadersDevNetwork.Name = "IntegrateScriptHeadersDevNetwork";
            this.IntegrateScriptHeadersDevNetwork.Size = new System.Drawing.Size(172, 32);
            this.IntegrateScriptHeadersDevNetwork.TabIndex = 11;
            this.IntegrateScriptHeadersDevNetwork.Text = "Integrate script_headers";
            this.IntegrateScriptHeadersDevNetwork.UseVisualStyleBackColor = true;
            this.IntegrateScriptHeadersDevNetwork.Click += new System.EventHandler(this.button34_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(7, 135);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(172, 32);
            this.button7.TabIndex = 32;
            this.button7.Text = "run_scannatives";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button4);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.button35);
            this.panel8.Controls.Add(this.button36);
            this.panel8.Controls.Add(this.button37);
            this.panel8.Controls.Add(this.button38);
            this.panel8.Controls.Add(this.button39);
            this.panel8.Controls.Add(this.button40);
            this.panel8.Location = new System.Drawing.Point(240, 21);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(186, 283);
            this.panel8.TabIndex = 17;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(6, 244);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(172, 32);
            this.button4.TabIndex = 39;
            this.button4.Text = "Sync New Prebuild Code";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Prebuild/Current build release";
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(6, 209);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(172, 32);
            this.button35.TabIndex = 36;
            this.button35.Text = "Update PreBuild dev_Live";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(6, 30);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(172, 32);
            this.button36.TabIndex = 35;
            this.button36.Text = "Label New PreBuild Code";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(6, 171);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(172, 32);
            this.button37.TabIndex = 33;
            this.button37.Text = "Update QA dev_Live build";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(6, 65);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(172, 32);
            this.button38.TabIndex = 26;
            this.button38.Text = "Label PreBuild dev_Live";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(6, 100);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(172, 32);
            this.button39.TabIndex = 27;
            this.button39.Text = "Label Current dev_Live";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(6, 135);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(172, 32);
            this.button40.TabIndex = 28;
            this.button40.Text = "Label dev_Live version xxx";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.button41);
            this.panel9.Controls.Add(this.button42);
            this.panel9.Location = new System.Drawing.Point(22, 202);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(186, 102);
            this.panel9.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "PS3 TU symbol backup";
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(7, 28);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(172, 32);
            this.button41.TabIndex = 29;
            this.button41.Text = "Backup TU PS3 symbols";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(7, 63);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(172, 32);
            this.button42.TabIndex = 30;
            this.button42.Text = "Backup US PS3 symbols";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button43);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.button44);
            this.panel10.Controls.Add(this.button45);
            this.panel10.Controls.Add(this.button46);
            this.panel10.Location = new System.Drawing.Point(22, 21);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(186, 175);
            this.panel10.TabIndex = 16;
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(7, 100);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(172, 32);
            this.button43.TabIndex = 14;
            this.button43.Text = "Build \"American\" text";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Script Utilities";
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(7, 65);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(172, 32);
            this.button44.TabIndex = 12;
            this.button44.Text = "Clear Debug Script Files";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(7, 30);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(172, 32);
            this.button45.TabIndex = 11;
            this.button45.Text = "Integrate script_headers";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(7, 135);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(172, 32);
            this.button46.TabIndex = 32;
            this.button46.Text = "run_scannatives";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // Build_Deployment_Tool
            // 
            this.Build_Deployment_Tool.Controls.Add(this.Dev_live);
            this.Build_Deployment_Tool.Controls.Add(this.Dev_Net);
            this.Build_Deployment_Tool.Controls.Add(this.Tools);
            this.Build_Deployment_Tool.Controls.Add(this.TP_CheckOut);
            this.Build_Deployment_Tool.Controls.Add(this.SyncPage);
            this.Build_Deployment_Tool.Controls.Add(this.TbCLtoBugstar);
            this.Build_Deployment_Tool.Controls.Add(this.TP_TSChecker);
            this.Build_Deployment_Tool.Location = new System.Drawing.Point(12, 12);
            this.Build_Deployment_Tool.Name = "Build_Deployment_Tool";
            this.Build_Deployment_Tool.SelectedIndex = 0;
            this.Build_Deployment_Tool.Size = new System.Drawing.Size(454, 351);
            this.Build_Deployment_Tool.TabIndex = 14;
            // 
            // Dev_live
            // 
            this.Dev_live.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Dev_live.Controls.Add(this.panel8);
            this.Dev_live.Controls.Add(this.panel10);
            this.Dev_live.Controls.Add(this.panel9);
            this.Dev_live.Location = new System.Drawing.Point(4, 22);
            this.Dev_live.Name = "Dev_live";
            this.Dev_live.Padding = new System.Windows.Forms.Padding(3);
            this.Dev_live.Size = new System.Drawing.Size(446, 325);
            this.Dev_live.TabIndex = 0;
            this.Dev_live.Text = "Dev_Live";
            this.Dev_live.UseVisualStyleBackColor = true;
            // 
            // Dev_Net
            // 
            this.Dev_Net.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Dev_Net.Controls.Add(this.panel4);
            this.Dev_Net.Controls.Add(this.panel7);
            this.Dev_Net.Controls.Add(this.panel5);
            this.Dev_Net.Location = new System.Drawing.Point(4, 22);
            this.Dev_Net.Name = "Dev_Net";
            this.Dev_Net.Padding = new System.Windows.Forms.Padding(3);
            this.Dev_Net.Size = new System.Drawing.Size(446, 325);
            this.Dev_Net.TabIndex = 1;
            this.Dev_Net.Text = "Dev_Net";
            this.Dev_Net.UseVisualStyleBackColor = true;
            // 
            // Tools
            // 
            this.Tools.Controls.Add(this.Build_Panel);
            this.Tools.Controls.Add(this.DLC_Panel);
            this.Tools.Controls.Add(this.Patches_Panel);
            this.Tools.Location = new System.Drawing.Point(4, 22);
            this.Tools.Name = "Tools";
            this.Tools.Padding = new System.Windows.Forms.Padding(3);
            this.Tools.Size = new System.Drawing.Size(446, 325);
            this.Tools.TabIndex = 2;
            this.Tools.Text = "Tools";
            this.Tools.UseVisualStyleBackColor = true;
            // 
            // Build_Panel
            // 
            this.Build_Panel.Controls.Add(this.BTN_SYNCDLC);
            this.Build_Panel.Controls.Add(this.BTN_CONTENTSTAR);
            this.Build_Panel.Controls.Add(this.BTN_DLCTEXTBUILDER);
            this.Build_Panel.Controls.Add(this.Create_and_Copy_FTP);
            this.Build_Panel.Controls.Add(this.Reduce_To_Final);
            this.Build_Panel.Controls.Add(this.Delete_CMP);
            this.Build_Panel.Controls.Add(this.Build_Label);
            this.Build_Panel.Location = new System.Drawing.Point(240, 21);
            this.Build_Panel.Name = "Build_Panel";
            this.Build_Panel.Size = new System.Drawing.Size(186, 283);
            this.Build_Panel.TabIndex = 2;
            // 
            // BTN_SYNCDLC
            // 
            this.BTN_SYNCDLC.Location = new System.Drawing.Point(7, 65);
            this.BTN_SYNCDLC.Name = "BTN_SYNCDLC";
            this.BTN_SYNCDLC.Size = new System.Drawing.Size(172, 32);
            this.BTN_SYNCDLC.TabIndex = 25;
            this.BTN_SYNCDLC.Text = "Sync Latest DLC";
            this.ToolTipPatches.SetToolTip(this.BTN_SYNCDLC, "Removes all non final files from your Dev_live directory");
            this.BTN_SYNCDLC.UseVisualStyleBackColor = true;
            this.BTN_SYNCDLC.Click += new System.EventHandler(this.BTN_SYNCDLC_Click);
            // 
            // BTN_CONTENTSTAR
            // 
            this.BTN_CONTENTSTAR.Location = new System.Drawing.Point(7, 135);
            this.BTN_CONTENTSTAR.Name = "BTN_CONTENTSTAR";
            this.BTN_CONTENTSTAR.Size = new System.Drawing.Size(172, 32);
            this.BTN_CONTENTSTAR.TabIndex = 24;
            this.BTN_CONTENTSTAR.Text = "ContentStar";
            this.ToolTipPatches.SetToolTip(this.BTN_CONTENTSTAR, "Removes all non final files from your Dev_live directory");
            this.BTN_CONTENTSTAR.UseVisualStyleBackColor = true;
            this.BTN_CONTENTSTAR.Click += new System.EventHandler(this.BTN_CONTENTSTAR_Click);
            // 
            // BTN_DLCTEXTBUILDER
            // 
            this.BTN_DLCTEXTBUILDER.Location = new System.Drawing.Point(7, 100);
            this.BTN_DLCTEXTBUILDER.Name = "BTN_DLCTEXTBUILDER";
            this.BTN_DLCTEXTBUILDER.Size = new System.Drawing.Size(172, 32);
            this.BTN_DLCTEXTBUILDER.TabIndex = 23;
            this.BTN_DLCTEXTBUILDER.Text = "DLCTextBuilder";
            this.ToolTipPatches.SetToolTip(this.BTN_DLCTEXTBUILDER, "Removes all non final files from your Dev_live directory");
            this.BTN_DLCTEXTBUILDER.UseVisualStyleBackColor = true;
            this.BTN_DLCTEXTBUILDER.Click += new System.EventHandler(this.BTN_DLCTEXTBUILDER_Click);
            // 
            // Create_and_Copy_FTP
            // 
            this.Create_and_Copy_FTP.Location = new System.Drawing.Point(7, 209);
            this.Create_and_Copy_FTP.Name = "Create_and_Copy_FTP";
            this.Create_and_Copy_FTP.Size = new System.Drawing.Size(172, 32);
            this.Create_and_Copy_FTP.TabIndex = 22;
            this.Create_and_Copy_FTP.Text = "Create Transfer Directory";
            this.ToolTipPatches.SetToolTip(this.Create_and_Copy_FTP, "Creates and Copies the contents of the current build to a local Transfer director" +
                    "y (e:)");
            this.Create_and_Copy_FTP.UseVisualStyleBackColor = true;
            this.Create_and_Copy_FTP.Click += new System.EventHandler(this.Create_and_Copy_FTP_Click);
            // 
            // Reduce_To_Final
            // 
            this.Reduce_To_Final.Location = new System.Drawing.Point(7, 244);
            this.Reduce_To_Final.Name = "Reduce_To_Final";
            this.Reduce_To_Final.Size = new System.Drawing.Size(172, 32);
            this.Reduce_To_Final.TabIndex = 21;
            this.Reduce_To_Final.Text = "Reduce to Final";
            this.ToolTipPatches.SetToolTip(this.Reduce_To_Final, "Removes all non final files from your Dev_live directory");
            this.Reduce_To_Final.UseVisualStyleBackColor = true;
            this.Reduce_To_Final.Click += new System.EventHandler(this.Reduce_To_Final_Click);
            // 
            // Delete_CMP
            // 
            this.Delete_CMP.Location = new System.Drawing.Point(7, 30);
            this.Delete_CMP.Name = "Delete_CMP";
            this.Delete_CMP.Size = new System.Drawing.Size(172, 32);
            this.Delete_CMP.TabIndex = 20;
            this.Delete_CMP.Text = "Delete CMP Files";
            this.ToolTipPatches.SetToolTip(this.Delete_CMP, "Deletes the CMP files from your local directory");
            this.Delete_CMP.UseVisualStyleBackColor = true;
            this.Delete_CMP.Click += new System.EventHandler(this.Delete_CMP_Click);
            // 
            // Build_Label
            // 
            this.Build_Label.AutoSize = true;
            this.Build_Label.Location = new System.Drawing.Point(77, 8);
            this.Build_Label.Name = "Build_Label";
            this.Build_Label.Size = new System.Drawing.Size(30, 13);
            this.Build_Label.TabIndex = 15;
            this.Build_Label.Text = "Build";
            // 
            // DLC_Panel
            // 
            this.DLC_Panel.Controls.Add(this.Copy_Edats);
            this.DLC_Panel.Controls.Add(this.Copy_DLC_to_Distro);
            this.DLC_Panel.Controls.Add(this.DLC_Label);
            this.DLC_Panel.Location = new System.Drawing.Point(22, 203);
            this.DLC_Panel.Name = "DLC_Panel";
            this.DLC_Panel.Size = new System.Drawing.Size(186, 102);
            this.DLC_Panel.TabIndex = 1;
            // 
            // Copy_Edats
            // 
            this.Copy_Edats.Location = new System.Drawing.Point(7, 62);
            this.Copy_Edats.Name = "Copy_Edats";
            this.Copy_Edats.Size = new System.Drawing.Size(172, 32);
            this.Copy_Edats.TabIndex = 17;
            this.Copy_Edats.Text = "Copy Edats";
            this.ToolTipPatches.SetToolTip(this.Copy_Edats, "Checks out the current DLC edats and copies them to the PS3 patch directory");
            this.Copy_Edats.UseVisualStyleBackColor = true;
            this.Copy_Edats.Click += new System.EventHandler(this.Copy_Edats_Click);
            // 
            // Copy_DLC_to_Distro
            // 
            this.Copy_DLC_to_Distro.Location = new System.Drawing.Point(7, 27);
            this.Copy_DLC_to_Distro.Name = "Copy_DLC_to_Distro";
            this.Copy_DLC_to_Distro.Size = new System.Drawing.Size(172, 32);
            this.Copy_DLC_to_Distro.TabIndex = 16;
            this.Copy_DLC_to_Distro.Text = "Copy DLC";
            this.ToolTipPatches.SetToolTip(this.Copy_DLC_to_Distro, "Copies DLC to the disribution drive");
            this.Copy_DLC_to_Distro.UseVisualStyleBackColor = true;
            this.Copy_DLC_to_Distro.Click += new System.EventHandler(this.Copy_DLC_to_Distro_Click);
            // 
            // DLC_Label
            // 
            this.DLC_Label.AutoSize = true;
            this.DLC_Label.Location = new System.Drawing.Point(79, 7);
            this.DLC_Label.Name = "DLC_Label";
            this.DLC_Label.Size = new System.Drawing.Size(28, 13);
            this.DLC_Label.TabIndex = 15;
            this.DLC_Label.Text = "DLC";
            this.DLC_Label.Click += new System.EventHandler(this.label2_Click);
            // 
            // Patches_Panel
            // 
            this.Patches_Panel.Controls.Add(this.Checkout_Patches);
            this.Patches_Panel.Controls.Add(this.Copy_360_Patches);
            this.Patches_Panel.Controls.Add(this.Copy_PS3_Patches);
            this.Patches_Panel.Controls.Add(this.Build_PS3_Patches);
            this.Patches_Panel.Controls.Add(this.Patches_Label);
            this.Patches_Panel.Location = new System.Drawing.Point(22, 21);
            this.Patches_Panel.Name = "Patches_Panel";
            this.Patches_Panel.Size = new System.Drawing.Size(186, 176);
            this.Patches_Panel.TabIndex = 0;
            // 
            // Checkout_Patches
            // 
            this.Checkout_Patches.Location = new System.Drawing.Point(7, 135);
            this.Checkout_Patches.Name = "Checkout_Patches";
            this.Checkout_Patches.Size = new System.Drawing.Size(172, 32);
            this.Checkout_Patches.TabIndex = 19;
            this.Checkout_Patches.Text = "Checkout Patches";
            this.ToolTipPatches.SetToolTip(this.Checkout_Patches, "Check\'s out the latest patches in perforce");
            this.Checkout_Patches.UseVisualStyleBackColor = true;
            this.Checkout_Patches.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Copy_360_Patches
            // 
            this.Copy_360_Patches.Location = new System.Drawing.Point(7, 100);
            this.Copy_360_Patches.Name = "Copy_360_Patches";
            this.Copy_360_Patches.Size = new System.Drawing.Size(172, 32);
            this.Copy_360_Patches.TabIndex = 18;
            this.Copy_360_Patches.Text = "Copy 360 Patches";
            this.ToolTipPatches.SetToolTip(this.Copy_360_Patches, "Copies the 360 patches to the distribution drive\r\n");
            this.Copy_360_Patches.UseVisualStyleBackColor = true;
            this.Copy_360_Patches.Click += new System.EventHandler(this.Copy_360_Patches_Click);
            // 
            // Copy_PS3_Patches
            // 
            this.Copy_PS3_Patches.Location = new System.Drawing.Point(7, 65);
            this.Copy_PS3_Patches.Name = "Copy_PS3_Patches";
            this.Copy_PS3_Patches.Size = new System.Drawing.Size(172, 32);
            this.Copy_PS3_Patches.TabIndex = 17;
            this.Copy_PS3_Patches.Text = "Copy PS3 patches ";
            this.ToolTipPatches.SetToolTip(this.Copy_PS3_Patches, "Copies the ps3 patches to the distribution drive\r\n");
            this.Copy_PS3_Patches.UseVisualStyleBackColor = true;
            this.Copy_PS3_Patches.Click += new System.EventHandler(this.Copy_PS3_Patches_Click);
            // 
            // Build_PS3_Patches
            // 
            this.Build_PS3_Patches.Location = new System.Drawing.Point(7, 30);
            this.Build_PS3_Patches.Name = "Build_PS3_Patches";
            this.Build_PS3_Patches.Size = new System.Drawing.Size(172, 32);
            this.Build_PS3_Patches.TabIndex = 16;
            this.Build_PS3_Patches.Text = "Build PS3 Patches";
            this.ToolTipPatches.SetToolTip(this.Build_PS3_Patches, "Builds PS3 patches ");
            this.Build_PS3_Patches.UseVisualStyleBackColor = true;
            this.Build_PS3_Patches.Click += new System.EventHandler(this.Build_PS3_Patches_Click);
            // 
            // Patches_Label
            // 
            this.Patches_Label.AutoSize = true;
            this.Patches_Label.Location = new System.Drawing.Point(70, 8);
            this.Patches_Label.Name = "Patches_Label";
            this.Patches_Label.Size = new System.Drawing.Size(46, 13);
            this.Patches_Label.TabIndex = 15;
            this.Patches_Label.Text = "Patches";
            // 
            // TP_CheckOut
            // 
            this.TP_CheckOut.Controls.Add(this.panel6);
            this.TP_CheckOut.Location = new System.Drawing.Point(4, 22);
            this.TP_CheckOut.Name = "TP_CheckOut";
            this.TP_CheckOut.Size = new System.Drawing.Size(446, 325);
            this.TP_CheckOut.TabIndex = 5;
            this.TP_CheckOut.Text = "Check Out";
            this.TP_CheckOut.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.BTN_Check_Out);
            this.panel6.Controls.Add(this.groupBox3);
            this.panel6.Controls.Add(this.groupBox1);
            this.panel6.Controls.Add(this.groupBox4);
            this.panel6.Controls.Add(this.groupBox2);
            this.panel6.Location = new System.Drawing.Point(52, 88);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(342, 148);
            this.panel6.TabIndex = 5;
            // 
            // BTN_Check_Out
            // 
            this.BTN_Check_Out.Location = new System.Drawing.Point(232, 72);
            this.BTN_Check_Out.Name = "BTN_Check_Out";
            this.BTN_Check_Out.Size = new System.Drawing.Size(106, 72);
            this.BTN_Check_Out.TabIndex = 4;
            this.BTN_Check_Out.Text = "Check Out";
            this.BTN_Check_Out.UseVisualStyleBackColor = true;
            this.BTN_Check_Out.Click += new System.EventHandler(this.BTN_Check_Out_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_XBOX_JPN);
            this.groupBox3.Controls.Add(this.CB_PS3_JPN);
            this.groupBox3.Controls.Add(this.CB_XBOX);
            this.groupBox3.Controls.Add(this.CB_PS3);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(108, 142);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Exes";
            // 
            // CB_XBOX_JPN
            // 
            this.CB_XBOX_JPN.AutoSize = true;
            this.CB_XBOX_JPN.Location = new System.Drawing.Point(6, 110);
            this.CB_XBOX_JPN.Name = "CB_XBOX_JPN";
            this.CB_XBOX_JPN.Size = new System.Drawing.Size(76, 17);
            this.CB_XBOX_JPN.TabIndex = 4;
            this.CB_XBOX_JPN.Text = "Xbox_JPN";
            this.CB_XBOX_JPN.UseVisualStyleBackColor = true;
            // 
            // CB_PS3_JPN
            // 
            this.CB_PS3_JPN.AutoSize = true;
            this.CB_PS3_JPN.Location = new System.Drawing.Point(6, 79);
            this.CB_PS3_JPN.Name = "CB_PS3_JPN";
            this.CB_PS3_JPN.Size = new System.Drawing.Size(72, 17);
            this.CB_PS3_JPN.TabIndex = 3;
            this.CB_PS3_JPN.Text = "PS3_JPN";
            this.CB_PS3_JPN.UseVisualStyleBackColor = true;
            // 
            // CB_XBOX
            // 
            this.CB_XBOX.AutoSize = true;
            this.CB_XBOX.Location = new System.Drawing.Point(6, 48);
            this.CB_XBOX.Name = "CB_XBOX";
            this.CB_XBOX.Size = new System.Drawing.Size(50, 17);
            this.CB_XBOX.TabIndex = 2;
            this.CB_XBOX.Text = "Xbox";
            this.CB_XBOX.UseVisualStyleBackColor = true;
            // 
            // CB_PS3
            // 
            this.CB_PS3.AutoSize = true;
            this.CB_PS3.Location = new System.Drawing.Point(6, 19);
            this.CB_PS3.Name = "CB_PS3";
            this.CB_PS3.Size = new System.Drawing.Size(46, 17);
            this.CB_PS3.TabIndex = 1;
            this.CB_PS3.Text = "PS3";
            this.CB_PS3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CB_Clifford);
            this.groupBox1.Controls.Add(this.CB_Japanese);
            this.groupBox1.Controls.Add(this.CB_Default);
            this.groupBox1.Location = new System.Drawing.Point(117, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(108, 99);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Script";
            // 
            // CB_Clifford
            // 
            this.CB_Clifford.AutoSize = true;
            this.CB_Clifford.Location = new System.Drawing.Point(6, 79);
            this.CB_Clifford.Name = "CB_Clifford";
            this.CB_Clifford.Size = new System.Drawing.Size(58, 17);
            this.CB_Clifford.TabIndex = 2;
            this.CB_Clifford.Text = "Clifford";
            this.CB_Clifford.UseVisualStyleBackColor = true;
            // 
            // CB_Japanese
            // 
            this.CB_Japanese.AutoSize = true;
            this.CB_Japanese.Location = new System.Drawing.Point(6, 48);
            this.CB_Japanese.Name = "CB_Japanese";
            this.CB_Japanese.Size = new System.Drawing.Size(72, 17);
            this.CB_Japanese.TabIndex = 1;
            this.CB_Japanese.Text = "Japanese";
            this.CB_Japanese.UseVisualStyleBackColor = true;
            // 
            // CB_Default
            // 
            this.CB_Default.AutoSize = true;
            this.CB_Default.Location = new System.Drawing.Point(6, 19);
            this.CB_Default.Name = "CB_Default";
            this.CB_Default.Size = new System.Drawing.Size(60, 17);
            this.CB_Default.TabIndex = 0;
            this.CB_Default.Text = "Default";
            this.CB_Default.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.RB_Dev_Net);
            this.groupBox4.Controls.Add(this.RB_Dev_Live);
            this.groupBox4.Location = new System.Drawing.Point(231, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(108, 64);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Branch";
            // 
            // RB_Dev_Net
            // 
            this.RB_Dev_Net.AutoSize = true;
            this.RB_Dev_Net.Location = new System.Drawing.Point(6, 40);
            this.RB_Dev_Net.Name = "RB_Dev_Net";
            this.RB_Dev_Net.Size = new System.Drawing.Size(68, 17);
            this.RB_Dev_Net.TabIndex = 1;
            this.RB_Dev_Net.Text = "Dev_Net";
            this.RB_Dev_Net.UseVisualStyleBackColor = true;
            // 
            // RB_Dev_Live
            // 
            this.RB_Dev_Live.AutoSize = true;
            this.RB_Dev_Live.Checked = true;
            this.RB_Dev_Live.Location = new System.Drawing.Point(6, 17);
            this.RB_Dev_Live.Name = "RB_Dev_Live";
            this.RB_Dev_Live.Size = new System.Drawing.Size(71, 17);
            this.RB_Dev_Live.TabIndex = 0;
            this.RB_Dev_Live.TabStop = true;
            this.RB_Dev_Live.Text = "Dev_Live";
            this.RB_Dev_Live.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CB_Version);
            this.groupBox2.Location = new System.Drawing.Point(117, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(108, 45);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // CB_Version
            // 
            this.CB_Version.AutoSize = true;
            this.CB_Version.Location = new System.Drawing.Point(6, 13);
            this.CB_Version.Name = "CB_Version";
            this.CB_Version.Size = new System.Drawing.Size(81, 17);
            this.CB_Version.TabIndex = 3;
            this.CB_Version.Text = "Version.text";
            this.CB_Version.UseVisualStyleBackColor = true;
            // 
            // SyncPage
            // 
            this.SyncPage.Controls.Add(this.CBJPN);
            this.SyncPage.Controls.Add(this.CBXlast);
            this.SyncPage.Controls.Add(this.panel2);
            this.SyncPage.Controls.Add(this.panel1);
            this.SyncPage.Controls.Add(this.CBDev_Net);
            this.SyncPage.Controls.Add(this.CB_Force);
            this.SyncPage.Location = new System.Drawing.Point(4, 22);
            this.SyncPage.Name = "SyncPage";
            this.SyncPage.Size = new System.Drawing.Size(446, 325);
            this.SyncPage.TabIndex = 3;
            this.SyncPage.Text = "Sync";
            this.SyncPage.UseVisualStyleBackColor = true;
            this.SyncPage.Click += new System.EventHandler(this.SyncPage_Click);
            // 
            // CBJPN
            // 
            this.CBJPN.AutoSize = true;
            this.CBJPN.Location = new System.Drawing.Point(256, 284);
            this.CBJPN.Name = "CBJPN";
            this.CBJPN.Size = new System.Drawing.Size(72, 17);
            this.CBJPN.TabIndex = 23;
            this.CBJPN.Text = "Japanese";
            this.CBJPN.UseVisualStyleBackColor = true;
            this.CBJPN.CheckedChanged += new System.EventHandler(this.CBJPN_CheckedChanged);
            // 
            // CBXlast
            // 
            this.CBXlast.AutoSize = true;
            this.CBXlast.Location = new System.Drawing.Point(204, 284);
            this.CBXlast.Name = "CBXlast";
            this.CBXlast.Size = new System.Drawing.Size(49, 17);
            this.CBXlast.TabIndex = 22;
            this.CBXlast.Text = "Xlast";
            this.CBXlast.UseVisualStyleBackColor = true;
            this.CBXlast.CheckedChanged += new System.EventHandler(this.CBXlast_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Save);
            this.panel2.Controls.Add(this.BtnWriteToFile);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TextLabel);
            this.panel2.Controls.Add(this.TB_Data);
            this.panel2.Controls.Add(this.TB_Src);
            this.panel2.Controls.Add(this.TB_Rage);
            this.panel2.Controls.Add(this.TB_Script);
            this.panel2.Controls.Add(this.TB_Text);
            this.panel2.Location = new System.Drawing.Point(230, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(192, 250);
            this.panel2.TabIndex = 21;
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(10, 205);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(172, 32);
            this.btn_Save.TabIndex = 23;
            this.btn_Save.Text = "Save current data";
            this.ToolTipPatches.SetToolTip(this.btn_Save, "Syncs latest Rage branches");
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // BtnWriteToFile
            // 
            this.BtnWriteToFile.Location = new System.Drawing.Point(10, 170);
            this.BtnWriteToFile.Name = "BtnWriteToFile";
            this.BtnWriteToFile.Size = new System.Drawing.Size(172, 32);
            this.BtnWriteToFile.TabIndex = 22;
            this.BtnWriteToFile.Text = "Write to file";
            this.ToolTipPatches.SetToolTip(this.BtnWriteToFile, "Syncs latest Rage branches");
            this.BtnWriteToFile.UseVisualStyleBackColor = true;
            this.BtnWriteToFile.Click += new System.EventHandler(this.BtnWriteToFile_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(66, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Changelists";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(34, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Data";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Source";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Rage";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Script";
            // 
            // TextLabel
            // 
            this.TextLabel.AutoSize = true;
            this.TextLabel.Location = new System.Drawing.Point(36, 115);
            this.TextLabel.Name = "TextLabel";
            this.TextLabel.Size = new System.Drawing.Size(28, 13);
            this.TextLabel.TabIndex = 16;
            this.TextLabel.Text = "Text";
            this.TextLabel.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // TB_Data
            // 
            this.TB_Data.Location = new System.Drawing.Point(69, 137);
            this.TB_Data.Name = "TB_Data";
            this.TB_Data.Size = new System.Drawing.Size(100, 20);
            this.TB_Data.TabIndex = 8;
            // 
            // TB_Src
            // 
            this.TB_Src.Location = new System.Drawing.Point(69, 62);
            this.TB_Src.Name = "TB_Src";
            this.TB_Src.Size = new System.Drawing.Size(100, 20);
            this.TB_Src.TabIndex = 7;
            // 
            // TB_Rage
            // 
            this.TB_Rage.Location = new System.Drawing.Point(69, 37);
            this.TB_Rage.Name = "TB_Rage";
            this.TB_Rage.Size = new System.Drawing.Size(100, 20);
            this.TB_Rage.TabIndex = 6;
            // 
            // TB_Script
            // 
            this.TB_Script.Location = new System.Drawing.Point(69, 87);
            this.TB_Script.Name = "TB_Script";
            this.TB_Script.Size = new System.Drawing.Size(100, 20);
            this.TB_Script.TabIndex = 5;
            // 
            // TB_Text
            // 
            this.TB_Text.Location = new System.Drawing.Point(69, 112);
            this.TB_Text.Name = "TB_Text";
            this.TB_Text.Size = new System.Drawing.Size(100, 20);
            this.TB_Text.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_All);
            this.panel1.Controls.Add(this.button_Rage);
            this.panel1.Controls.Add(this.button_Src);
            this.panel1.Controls.Add(this.button_data);
            this.panel1.Controls.Add(this.button_script);
            this.panel1.Controls.Add(this.button_text);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(22, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 250);
            this.panel1.TabIndex = 1;
            // 
            // button_All
            // 
            this.button_All.Location = new System.Drawing.Point(7, 205);
            this.button_All.Name = "button_All";
            this.button_All.Size = new System.Drawing.Size(172, 32);
            this.button_All.TabIndex = 21;
            this.button_All.Text = "All";
            this.ToolTipPatches.SetToolTip(this.button_All, "Syncs All latest");
            this.button_All.UseVisualStyleBackColor = true;
            this.button_All.Click += new System.EventHandler(this.button_All_Click);
            // 
            // button_Rage
            // 
            this.button_Rage.Location = new System.Drawing.Point(7, 30);
            this.button_Rage.Name = "button_Rage";
            this.button_Rage.Size = new System.Drawing.Size(172, 32);
            this.button_Rage.TabIndex = 20;
            this.button_Rage.Text = "Rage";
            this.ToolTipPatches.SetToolTip(this.button_Rage, "Syncs latest Rage branches");
            this.button_Rage.UseVisualStyleBackColor = true;
            this.button_Rage.Click += new System.EventHandler(this.button_Rage_Click);
            // 
            // button_Src
            // 
            this.button_Src.Location = new System.Drawing.Point(7, 65);
            this.button_Src.Name = "button_Src";
            this.button_Src.Size = new System.Drawing.Size(172, 32);
            this.button_Src.TabIndex = 19;
            this.button_Src.Text = "Source";
            this.ToolTipPatches.SetToolTip(this.button_Src, "Syncs latest Source branches");
            this.button_Src.UseVisualStyleBackColor = true;
            this.button_Src.Click += new System.EventHandler(this.button_Src_Click);
            // 
            // button_data
            // 
            this.button_data.Location = new System.Drawing.Point(7, 170);
            this.button_data.Name = "button_data";
            this.button_data.Size = new System.Drawing.Size(172, 32);
            this.button_data.TabIndex = 18;
            this.button_data.Text = "Data";
            this.ToolTipPatches.SetToolTip(this.button_data, "Syncs latest Data branches");
            this.button_data.UseVisualStyleBackColor = true;
            this.button_data.Click += new System.EventHandler(this.button_data_Click);
            // 
            // button_script
            // 
            this.button_script.Location = new System.Drawing.Point(7, 100);
            this.button_script.Name = "button_script";
            this.button_script.Size = new System.Drawing.Size(172, 32);
            this.button_script.TabIndex = 17;
            this.button_script.Text = "Script";
            this.ToolTipPatches.SetToolTip(this.button_script, "Syncs latest Script branches");
            this.button_script.UseVisualStyleBackColor = true;
            this.button_script.Click += new System.EventHandler(this.button_script_Click);
            // 
            // button_text
            // 
            this.button_text.Location = new System.Drawing.Point(7, 135);
            this.button_text.Name = "button_text";
            this.button_text.Size = new System.Drawing.Size(172, 32);
            this.button_text.TabIndex = 16;
            this.button_text.Text = "Text";
            this.ToolTipPatches.SetToolTip(this.button_text, "Syncs latest Text branches");
            this.button_text.UseVisualStyleBackColor = true;
            this.button_text.Click += new System.EventHandler(this.button_text_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(67, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Branches";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // CBDev_Net
            // 
            this.CBDev_Net.AutoSize = true;
            this.CBDev_Net.Location = new System.Drawing.Point(29, 284);
            this.CBDev_Net.Name = "CBDev_Net";
            this.CBDev_Net.Size = new System.Drawing.Size(92, 17);
            this.CBDev_Net.TabIndex = 3;
            this.CBDev_Net.Text = "Dev_Network";
            this.CBDev_Net.UseVisualStyleBackColor = true;
            this.CBDev_Net.CheckedChanged += new System.EventHandler(this.CBDev_Net_CheckedChanged);
            // 
            // CB_Force
            // 
            this.CB_Force.AutoSize = true;
            this.CB_Force.Location = new System.Drawing.Point(122, 284);
            this.CB_Force.Name = "CB_Force";
            this.CB_Force.Size = new System.Drawing.Size(79, 17);
            this.CB_Force.TabIndex = 2;
            this.CB_Force.Text = "Force Grab";
            this.CB_Force.UseVisualStyleBackColor = true;
            this.CB_Force.CheckedChanged += new System.EventHandler(this.CB_Force_CheckedChanged);
            // 
            // TbCLtoBugstar
            // 
            this.TbCLtoBugstar.Controls.Add(this.LBL_NewCLTime);
            this.TbCLtoBugstar.Controls.Add(this.LBL_OldCLTime);
            this.TbCLtoBugstar.Controls.Add(this.BTN_Refresh_Old_CL);
            this.TbCLtoBugstar.Controls.Add(this.CB_Dev_Network);
            this.TbCLtoBugstar.Controls.Add(this.label24);
            this.TbCLtoBugstar.Controls.Add(this.label23);
            this.TbCLtoBugstar.Controls.Add(this.TB_Extra);
            this.TbCLtoBugstar.Controls.Add(this.label22);
            this.TbCLtoBugstar.Controls.Add(this.label21);
            this.TbCLtoBugstar.Controls.Add(this.label20);
            this.TbCLtoBugstar.Controls.Add(this.label19);
            this.TbCLtoBugstar.Controls.Add(this.label18);
            this.TbCLtoBugstar.Controls.Add(this.TB_Data2);
            this.TbCLtoBugstar.Controls.Add(this.TB_Src2);
            this.TbCLtoBugstar.Controls.Add(this.TB_Rage2);
            this.TbCLtoBugstar.Controls.Add(this.TB_Script2);
            this.TbCLtoBugstar.Controls.Add(this.TB_Text2);
            this.TbCLtoBugstar.Controls.Add(this.label13);
            this.TbCLtoBugstar.Controls.Add(this.label14);
            this.TbCLtoBugstar.Controls.Add(this.label15);
            this.TbCLtoBugstar.Controls.Add(this.label16);
            this.TbCLtoBugstar.Controls.Add(this.label17);
            this.TbCLtoBugstar.Controls.Add(this.TB_Data1);
            this.TbCLtoBugstar.Controls.Add(this.TB_Src1);
            this.TbCLtoBugstar.Controls.Add(this.TB_Rage1);
            this.TbCLtoBugstar.Controls.Add(this.TB_Script1);
            this.TbCLtoBugstar.Controls.Add(this.TB_Text1);
            this.TbCLtoBugstar.Controls.Add(this.BTNReport);
            this.TbCLtoBugstar.Controls.Add(this.CBManualEntry);
            this.TbCLtoBugstar.Controls.Add(this.panel3);
            this.TbCLtoBugstar.Location = new System.Drawing.Point(4, 22);
            this.TbCLtoBugstar.Name = "TbCLtoBugstar";
            this.TbCLtoBugstar.Size = new System.Drawing.Size(446, 325);
            this.TbCLtoBugstar.TabIndex = 4;
            this.TbCLtoBugstar.Text = "CL -> B*";
            this.TbCLtoBugstar.UseVisualStyleBackColor = true;
            // 
            // LBL_NewCLTime
            // 
            this.LBL_NewCLTime.AutoSize = true;
            this.LBL_NewCLTime.Location = new System.Drawing.Point(137, 30);
            this.LBL_NewCLTime.Name = "LBL_NewCLTime";
            this.LBL_NewCLTime.Size = new System.Drawing.Size(65, 13);
            this.LBL_NewCLTime.TabIndex = 57;
            this.LBL_NewCLTime.Text = "NewCLTime";
            // 
            // LBL_OldCLTime
            // 
            this.LBL_OldCLTime.AutoSize = true;
            this.LBL_OldCLTime.Location = new System.Drawing.Point(20, 30);
            this.LBL_OldCLTime.Name = "LBL_OldCLTime";
            this.LBL_OldCLTime.Size = new System.Drawing.Size(59, 13);
            this.LBL_OldCLTime.TabIndex = 56;
            this.LBL_OldCLTime.Text = "OldCLTime";
            // 
            // BTN_Refresh_Old_CL
            // 
            this.BTN_Refresh_Old_CL.Location = new System.Drawing.Point(35, 249);
            this.BTN_Refresh_Old_CL.Name = "BTN_Refresh_Old_CL";
            this.BTN_Refresh_Old_CL.Size = new System.Drawing.Size(110, 25);
            this.BTN_Refresh_Old_CL.TabIndex = 55;
            this.BTN_Refresh_Old_CL.Text = "Refresh Old CL";
            this.BTN_Refresh_Old_CL.UseVisualStyleBackColor = true;
            this.BTN_Refresh_Old_CL.Click += new System.EventHandler(this.BTN_Refresh_Old_CL_Click);
            // 
            // CB_Dev_Network
            // 
            this.CB_Dev_Network.AutoSize = true;
            this.CB_Dev_Network.Location = new System.Drawing.Point(230, 285);
            this.CB_Dev_Network.Name = "CB_Dev_Network";
            this.CB_Dev_Network.Size = new System.Drawing.Size(92, 17);
            this.CB_Dev_Network.TabIndex = 54;
            this.CB_Dev_Network.Text = "Dev_Network";
            this.CB_Dev_Network.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(84, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 13);
            this.label24.TabIndex = 53;
            this.label24.Text = "Manual CL Entry";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(22, 218);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 52;
            this.label23.Text = "Extra";
            // 
            // TB_Extra
            // 
            this.TB_Extra.Enabled = false;
            this.TB_Extra.Location = new System.Drawing.Point(57, 215);
            this.TB_Extra.Name = "TB_Extra";
            this.TB_Extra.Size = new System.Drawing.Size(367, 20);
            this.TB_Extra.TabIndex = 51;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(123, 189);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "...";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(123, 156);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "...";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(123, 123);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 13);
            this.label20.TabIndex = 48;
            this.label20.Text = "...";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(123, 90);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "...";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(123, 57);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 13);
            this.label18.TabIndex = 46;
            this.label18.Text = "...";
            // 
            // TB_Data2
            // 
            this.TB_Data2.Enabled = false;
            this.TB_Data2.Location = new System.Drawing.Point(150, 182);
            this.TB_Data2.Name = "TB_Data2";
            this.TB_Data2.Size = new System.Drawing.Size(55, 20);
            this.TB_Data2.TabIndex = 45;
            // 
            // TB_Src2
            // 
            this.TB_Src2.Enabled = false;
            this.TB_Src2.Location = new System.Drawing.Point(150, 83);
            this.TB_Src2.Name = "TB_Src2";
            this.TB_Src2.Size = new System.Drawing.Size(55, 20);
            this.TB_Src2.TabIndex = 44;
            // 
            // TB_Rage2
            // 
            this.TB_Rage2.Enabled = false;
            this.TB_Rage2.Location = new System.Drawing.Point(150, 50);
            this.TB_Rage2.Name = "TB_Rage2";
            this.TB_Rage2.Size = new System.Drawing.Size(55, 20);
            this.TB_Rage2.TabIndex = 43;
            // 
            // TB_Script2
            // 
            this.TB_Script2.Enabled = false;
            this.TB_Script2.Location = new System.Drawing.Point(150, 116);
            this.TB_Script2.Name = "TB_Script2";
            this.TB_Script2.Size = new System.Drawing.Size(55, 20);
            this.TB_Script2.TabIndex = 42;
            // 
            // TB_Text2
            // 
            this.TB_Text2.Enabled = false;
            this.TB_Text2.Location = new System.Drawing.Point(150, 149);
            this.TB_Text2.Name = "TB_Text2";
            this.TB_Text2.Size = new System.Drawing.Size(55, 20);
            this.TB_Text2.TabIndex = 41;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 185);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Data";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Source";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Rage";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 119);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "Script";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "Text";
            // 
            // TB_Data1
            // 
            this.TB_Data1.Enabled = false;
            this.TB_Data1.Location = new System.Drawing.Point(57, 182);
            this.TB_Data1.Name = "TB_Data1";
            this.TB_Data1.Size = new System.Drawing.Size(55, 20);
            this.TB_Data1.TabIndex = 35;
            // 
            // TB_Src1
            // 
            this.TB_Src1.Enabled = false;
            this.TB_Src1.Location = new System.Drawing.Point(57, 83);
            this.TB_Src1.Name = "TB_Src1";
            this.TB_Src1.Size = new System.Drawing.Size(55, 20);
            this.TB_Src1.TabIndex = 34;
            // 
            // TB_Rage1
            // 
            this.TB_Rage1.Enabled = false;
            this.TB_Rage1.Location = new System.Drawing.Point(57, 50);
            this.TB_Rage1.Name = "TB_Rage1";
            this.TB_Rage1.Size = new System.Drawing.Size(55, 20);
            this.TB_Rage1.TabIndex = 33;
            // 
            // TB_Script1
            // 
            this.TB_Script1.Enabled = false;
            this.TB_Script1.Location = new System.Drawing.Point(57, 116);
            this.TB_Script1.Name = "TB_Script1";
            this.TB_Script1.Size = new System.Drawing.Size(55, 20);
            this.TB_Script1.TabIndex = 32;
            // 
            // TB_Text1
            // 
            this.TB_Text1.Enabled = false;
            this.TB_Text1.Location = new System.Drawing.Point(57, 149);
            this.TB_Text1.Name = "TB_Text1";
            this.TB_Text1.Size = new System.Drawing.Size(55, 20);
            this.TB_Text1.TabIndex = 31;
            // 
            // BTNReport
            // 
            this.BTNReport.Enabled = false;
            this.BTNReport.Location = new System.Drawing.Point(35, 280);
            this.BTNReport.Name = "BTNReport";
            this.BTNReport.Size = new System.Drawing.Size(110, 25);
            this.BTNReport.TabIndex = 31;
            this.BTNReport.Text = "Create Report";
            this.BTNReport.UseVisualStyleBackColor = true;
            this.BTNReport.Click += new System.EventHandler(this.BTNReport_Click);
            // 
            // CBManualEntry
            // 
            this.CBManualEntry.AutoSize = true;
            this.CBManualEntry.Location = new System.Drawing.Point(331, 285);
            this.CBManualEntry.Name = "CBManualEntry";
            this.CBManualEntry.Size = new System.Drawing.Size(88, 17);
            this.CBManualEntry.TabIndex = 27;
            this.CBManualEntry.Text = "Manual Entry";
            this.CBManualEntry.UseVisualStyleBackColor = true;
            this.CBManualEntry.CheckedChanged += new System.EventHandler(this.CBManualEntry_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LBRestLogin);
            this.panel3.Controls.Add(this.LBLTimer);
            this.panel3.Controls.Add(this.lblUser);
            this.panel3.Controls.Add(this.TBUser);
            this.panel3.Controls.Add(this.lblPassword);
            this.panel3.Controls.Add(this.TBPassword);
            this.panel3.Controls.Add(this.BtnLogin);
            this.panel3.Location = new System.Drawing.Point(230, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(192, 114);
            this.panel3.TabIndex = 26;
            // 
            // LBRestLogin
            // 
            this.LBRestLogin.AutoSize = true;
            this.LBRestLogin.Location = new System.Drawing.Point(43, 9);
            this.LBRestLogin.Name = "LBRestLogin";
            this.LBRestLogin.Size = new System.Drawing.Size(111, 13);
            this.LBRestLogin.TabIndex = 31;
            this.LBRestLogin.Text = "Bugstar Service Login";
            // 
            // LBLTimer
            // 
            this.LBLTimer.AutoSize = true;
            this.LBLTimer.Location = new System.Drawing.Point(24, 91);
            this.LBLTimer.Name = "LBLTimer";
            this.LBLTimer.Size = new System.Drawing.Size(36, 13);
            this.LBLTimer.TabIndex = 30;
            this.LBLTimer.Text = "Timer:";
            this.LBLTimer.Visible = false;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(0, 36);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(58, 13);
            this.lblUser.TabIndex = 29;
            this.lblUser.Text = "Username:";
            // 
            // TBUser
            // 
            this.TBUser.Location = new System.Drawing.Point(60, 32);
            this.TBUser.Name = "TBUser";
            this.TBUser.Size = new System.Drawing.Size(130, 20);
            this.TBUser.TabIndex = 28;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(2, 62);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 27;
            this.lblPassword.Text = "Password:";
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(59, 58);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.Size = new System.Drawing.Size(130, 20);
            this.TBPassword.TabIndex = 29;
            this.TBPassword.UseSystemPasswordChar = true;
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(113, 85);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(76, 25);
            this.BtnLogin.TabIndex = 30;
            this.BtnLogin.Text = "Login";
            this.ToolTipPatches.SetToolTip(this.BtnLogin, "Syncs latest Rage branches");
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // TP_TSChecker
            // 
            this.TP_TSChecker.Controls.Add(this.BTN_LoadXML);
            this.TP_TSChecker.Controls.Add(this.RT_TimeStampCheckerTime);
            this.TP_TSChecker.Controls.Add(this.BTN_CheckTimeStamps);
            this.TP_TSChecker.Controls.Add(this.RT_TimeStampCheckerName);
            this.TP_TSChecker.Location = new System.Drawing.Point(4, 22);
            this.TP_TSChecker.Name = "TP_TSChecker";
            this.TP_TSChecker.Size = new System.Drawing.Size(446, 325);
            this.TP_TSChecker.TabIndex = 6;
            this.TP_TSChecker.Text = "TS Checker";
            this.TP_TSChecker.UseVisualStyleBackColor = true;
            this.TP_TSChecker.Click += new System.EventHandler(this.TP_TSChecker_Click);
            // 
            // BTN_LoadXML
            // 
            this.BTN_LoadXML.Location = new System.Drawing.Point(227, 289);
            this.BTN_LoadXML.Name = "BTN_LoadXML";
            this.BTN_LoadXML.Size = new System.Drawing.Size(75, 23);
            this.BTN_LoadXML.TabIndex = 4;
            this.BTN_LoadXML.Text = "Load XML";
            this.BTN_LoadXML.UseVisualStyleBackColor = true;
            this.BTN_LoadXML.Click += new System.EventHandler(this.BTN_LoadXML_Click);
            // 
            // RT_TimeStampCheckerTime
            // 
            this.RT_TimeStampCheckerTime.Location = new System.Drawing.Point(227, 15);
            this.RT_TimeStampCheckerTime.Name = "RT_TimeStampCheckerTime";
            this.RT_TimeStampCheckerTime.Size = new System.Drawing.Size(203, 268);
            this.RT_TimeStampCheckerTime.TabIndex = 2;
            this.RT_TimeStampCheckerTime.Text = "";
            // 
            // BTN_CheckTimeStamps
            // 
            this.BTN_CheckTimeStamps.Location = new System.Drawing.Point(308, 289);
            this.BTN_CheckTimeStamps.Name = "BTN_CheckTimeStamps";
            this.BTN_CheckTimeStamps.Size = new System.Drawing.Size(121, 23);
            this.BTN_CheckTimeStamps.TabIndex = 1;
            this.BTN_CheckTimeStamps.Text = "Check Timestamps";
            this.BTN_CheckTimeStamps.UseVisualStyleBackColor = true;
            this.BTN_CheckTimeStamps.Click += new System.EventHandler(this.BTN_CheckTimeStamps_Click);
            // 
            // RT_TimeStampCheckerName
            // 
            this.RT_TimeStampCheckerName.Location = new System.Drawing.Point(15, 15);
            this.RT_TimeStampCheckerName.Name = "RT_TimeStampCheckerName";
            this.RT_TimeStampCheckerName.Size = new System.Drawing.Size(203, 268);
            this.RT_TimeStampCheckerName.TabIndex = 0;
            this.RT_TimeStampCheckerName.Text = "";
            // 
            // ToolTipPatches
            // 
            this.ToolTipPatches.AutoPopDelay = 5000;
            this.ToolTipPatches.InitialDelay = 100;
            this.ToolTipPatches.ReshowDelay = 100;
            this.ToolTipPatches.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // CB_Dev_Live
            // 
            this.CB_Dev_Live.AutoSize = true;
            this.CB_Dev_Live.Checked = true;
            this.CB_Dev_Live.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CB_Dev_Live.Location = new System.Drawing.Point(45, 369);
            this.CB_Dev_Live.Name = "CB_Dev_Live";
            this.CB_Dev_Live.Size = new System.Drawing.Size(72, 17);
            this.CB_Dev_Live.TabIndex = 15;
            this.CB_Dev_Live.Text = "Dev_Live";
            this.CB_Dev_Live.UseVisualStyleBackColor = true;
            this.CB_Dev_Live.CheckedChanged += new System.EventHandler(this.CB_Dev_Live_CheckedChanged);
            // 
            // CB_Dev_Net
            // 
            this.CB_Dev_Net.AutoSize = true;
            this.CB_Dev_Net.Location = new System.Drawing.Point(123, 369);
            this.CB_Dev_Net.Name = "CB_Dev_Net";
            this.CB_Dev_Net.Size = new System.Drawing.Size(69, 17);
            this.CB_Dev_Net.TabIndex = 16;
            this.CB_Dev_Net.Text = "Dev_Net";
            this.CB_Dev_Net.UseVisualStyleBackColor = true;
            this.CB_Dev_Net.CheckedChanged += new System.EventHandler(this.CB_Dev_Net_CheckedChanged);
            // 
            // BuildDeploymentMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 391);
            this.Controls.Add(this.CB_Dev_Net);
            this.Controls.Add(this.CB_Dev_Live);
            this.Controls.Add(this.Build_Deployment_Tool);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BuildDeploymentMenu";
            this.Text = "Build Deployment Tool";
            this.Load += new System.EventHandler(this.BuildDeploymentMenu_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.Build_Deployment_Tool.ResumeLayout(false);
            this.Dev_live.ResumeLayout(false);
            this.Dev_Net.ResumeLayout(false);
            this.Tools.ResumeLayout(false);
            this.Build_Panel.ResumeLayout(false);
            this.Build_Panel.PerformLayout();
            this.DLC_Panel.ResumeLayout(false);
            this.DLC_Panel.PerformLayout();
            this.Patches_Panel.ResumeLayout(false);
            this.Patches_Panel.PerformLayout();
            this.TP_CheckOut.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.SyncPage.ResumeLayout(false);
            this.SyncPage.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.TbCLtoBugstar.ResumeLayout(false);
            this.TbCLtoBugstar.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.TP_TSChecker.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        List<int> bugstarNumbers = new List<int>();
        public StringBuilder BugStringBuilder = new StringBuilder("");
        public string XmlBugList;
        public CookieContainer cookieContainer;
        public string uriStr = "https://rest.bugstar.rockstargames.com:8443/BugstarRestService-1.0/";
        public string postResponseUri = "";
        public int seconds;
        public int minutes;
        public int hours;
        public bool paused;
        public bool toggled;
        public string ForceChecked = "";
        public string DevNetworkChecked = "";
        public string XlastChecked = "";
        public string GameTextScriptChecked = "";
        public string TextCLOutput = "";
        public string TextCL = "";
        public string OldTextCL = "";
        public string DataCLOutput = "";
        public string DataCL = "";
        public string OldDataCL = "";
        public string SrcCLOutput = "";
        public string SrcCL = "";
        public string OldSrcCL = "";
        public string ScriptCLOutput = "";
        public string ScriptCL = "";
        public string OldScriptCL = "";
        public string RageCLOutput = "";
        public string RageCL = "";
        public string OldRageCL = "";
        public string usernamePassword;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button IntegrateScriptHeadersDevNetwork;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.TabPage Dev_live;
        private System.Windows.Forms.TabPage Dev_Net;
        private System.Windows.Forms.TabPage Tools;
        private System.Windows.Forms.TabControl Build_Deployment_Tool;
        private System.Windows.Forms.Panel Patches_Panel;
        private System.Windows.Forms.Button Build_PS3_Patches;
        private System.Windows.Forms.Label Patches_Label;
        private System.Windows.Forms.Button Copy_PS3_Patches;
        private System.Windows.Forms.ToolTip ToolTipPatches;
        private System.Windows.Forms.Button Copy_360_Patches;
        private System.Windows.Forms.Panel DLC_Panel;
        private System.Windows.Forms.Button Copy_Edats;
        private System.Windows.Forms.Button Copy_DLC_to_Distro;
        private System.Windows.Forms.Label DLC_Label;
        private System.Windows.Forms.Button Checkout_Patches;
        private System.Windows.Forms.Panel Build_Panel;
        private System.Windows.Forms.Label Build_Label;
        private System.Windows.Forms.Button Reduce_To_Final;
        private System.Windows.Forms.Button Delete_CMP;
        private System.Windows.Forms.Button Create_and_Copy_FTP;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage SyncPage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_Rage;
        private System.Windows.Forms.Button button_Src;
        private System.Windows.Forms.Button button_data;
        private System.Windows.Forms.Button button_script;
        private System.Windows.Forms.Button button_text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox CB_Force;
        private System.Windows.Forms.CheckBox CBDev_Net;
        private System.Windows.Forms.Button button_All;
        private System.Windows.Forms.TextBox TB_Data;
        private System.Windows.Forms.TextBox TB_Src;
        private System.Windows.Forms.TextBox TB_Rage;
        private System.Windows.Forms.TextBox TB_Script;
        private System.Windows.Forms.TextBox TB_Text;
        private System.Windows.Forms.Label TextLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button BtnWriteToFile;
        private System.Windows.Forms.CheckBox CBXlast;
        private System.Windows.Forms.CheckBox CBJPN;
        private System.Windows.Forms.TabPage TbCLtoBugstar;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.TextBox TBUser;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label LBLTimer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label LBRestLogin;
        private System.Windows.Forms.Button BTNReport;
        private System.Windows.Forms.CheckBox CBManualEntry;
        private System.Windows.Forms.TextBox TB_Data2;
        private System.Windows.Forms.TextBox TB_Src2;
        private System.Windows.Forms.TextBox TB_Rage2;
        private System.Windows.Forms.TextBox TB_Script2;
        private System.Windows.Forms.TextBox TB_Text2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TB_Data1;
        private System.Windows.Forms.TextBox TB_Src1;
        private System.Windows.Forms.TextBox TB_Rage1;
        private System.Windows.Forms.TextBox TB_Script1;
        private System.Windows.Forms.TextBox TB_Text1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox TB_Extra;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.CheckBox CB_Dev_Network;
        private System.Windows.Forms.TabPage TP_CheckOut;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BTN_Check_Out;
        private System.Windows.Forms.RadioButton RB_Dev_Net;
        private System.Windows.Forms.RadioButton RB_Dev_Live;
        private System.Windows.Forms.CheckBox CB_Clifford;
        private System.Windows.Forms.CheckBox CB_Japanese;
        private System.Windows.Forms.CheckBox CB_Default;
        private System.Windows.Forms.CheckBox CB_XBOX_JPN;
        private System.Windows.Forms.CheckBox CB_PS3_JPN;
        private System.Windows.Forms.CheckBox CB_XBOX;
        private System.Windows.Forms.CheckBox CB_PS3;
        private System.Windows.Forms.CheckBox CB_Version;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabPage TP_TSChecker;
        private System.Windows.Forms.Button BTN_CheckTimeStamps;
        private System.Windows.Forms.RichTextBox RT_TimeStampCheckerName;
        private System.Windows.Forms.RichTextBox RT_TimeStampCheckerTime;
        private System.Windows.Forms.Button BTN_LoadXML;
        private System.Windows.Forms.CheckBox CB_Dev_Live;
        private System.Windows.Forms.CheckBox CB_Dev_Net;
        private System.Windows.Forms.Label LBL_NewCLTime;
        private System.Windows.Forms.Label LBL_OldCLTime;
        private System.Windows.Forms.Button BTN_Refresh_Old_CL;
        private System.Windows.Forms.Button BTN_CONTENTSTAR;
        private System.Windows.Forms.Button BTN_DLCTEXTBUILDER;
        private System.Windows.Forms.Button BTN_SYNCDLC;
    }
}

