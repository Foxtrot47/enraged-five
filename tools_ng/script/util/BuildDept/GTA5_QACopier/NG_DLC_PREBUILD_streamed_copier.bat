::************************************************************************
:: GTA5 NG DLC streamed Copier											**
:: Updated: 				19/05/2014									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Add Heist									**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5_ng_dlc_copier.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5_dlc
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\dlc\PreBuild"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Copying ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ######################################################
echo ## GTA5 streamed NG DLC updating...		         ##
echo ######################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%
robocopy /S /PURGE %builddir%\mpPacks\mpBeach\build\dev_ng\ %copydir%\mpPacks\mpBeach\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpBusiness\build\dev_ng\ %copydir%\mpPacks\mpBusiness\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpBusiness2\build\dev_ng\ %copydir%\mpPacks\mpBusiness2\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpChristmas\build\dev_ng\ %copydir%\mpPacks\mpChristmas\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpHipster\build\dev_ng\ %copydir%\mpPacks\mpHipster\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpValentines\build\dev_ng\ %copydir%\mpPacks\mpValentines\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpHeist\build\dev_ng\ %copydir%\mpPacks\mpHeist\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpPilot\build\dev_ng\ %copydir%\mpPacks\mpPilot\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\mpPacks\mpIndependence\build\dev_ng\ %copydir%\mpPacks\mpIndependence\build\dev_ng\ *.*

robocopy /S /PURGE %builddir%\spPacks\dlc_agentTrevor\build\dev_ng\ %copydir%\spPacks\dlc_agentTrevor\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\spPacks\spUpgrade\build\dev_ng\ %copydir%\spPacks\spUpgrade\build\dev_ng\ *.*

robocopy /S /PURGE %builddir%\mpPacks\mpChristmas2\build\dev_ng\ %copydir%\mpPacks\mpChristmas2\build\dev_ng\ *.*

robocopy /S /PURGE %builddir%\patchPacks\patchDay1NG\build\dev_ng\ %copydir%\patchPacks\patchDay1NG\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\patchPacks\patchDay2NG\build\dev_ng\ %copydir%\patchPacks\patchDay2NG\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\patchPacks\patchDay2bNG\build\dev_ng\ %copydir%\patchPacks\patchDay2bNG\build\dev_ng\ *.*
robocopy /S /PURGE %builddir%\patchPacks\patchDay3NG\build\dev_ng\ %copydir%\patchPacks\patchDay3NG\build\dev_ng\ *.*


echo GTA5 streamed NG DLC updated 
echo -- GTA5 streamed NG DLC updated --  >> %outFile%


echo Finished 
echo -- Finished -- >> %outFile%
echo Finish >> %statusFile%

pause
exit
