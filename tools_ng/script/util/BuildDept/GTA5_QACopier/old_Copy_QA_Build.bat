@echo off

:: === SETUP STUFF === ::
:: --- don't need just now for gta5 --- ::
:: echo Delay the start of the copy allow version script to complete
:: @ping 127.0.0.1 -n 2 -w 1000 > nul
:: @ping 127.0.0.1 -n %1% -w 1000> nul

set ver=NUL
if exist N:\RSGEDI\Distribution\QA_Build\gta5\build\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (N:\RSGEDI\Distribution\QA_Build\gta5\build\dev\common\data\version.txt) DO (
		set ver=%%G
		GOTO PRINT
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAIN
)
	
:PRINT
echo version=%ver%
GOTO MAIN

	
:MAIN
set outFile=C:\GTA5copyLog.txt

::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev

echo %builddir%
echo ^%builddir% > %outFile%
echo Start > %statusFile%
echo ## Running Copy_QA_build.bat ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
call C:\GTA5_QACopier\Blat.bat


:: === COPY THE BUILD === ::
echo ###########################################
echo ## Copying QA GTA5 build...              ##
echo ###########################################

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\ (
	erase /F /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\*.*
	rmdir /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\
)
if %ERRORLEVEL% EQU 1 (
	echo Error when deleting incomplete build copies!!
	echo !! Error when deleting incomplete build copies !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
	exit
)

:: === COPYING THE QA BUILD === ::
echo ################################################
echo ## Copy Latest QA PS3 Version to Distribution ##
echo ################################################
echo -- Started Copying the Latest QA Version to Distribution drive (PS3...) -- >> %outFile%
XCOPY %builddir%\common\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\common\ /E /Y /R /H
XCOPY %builddir%\metadata\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\metadata\ /E /Y /R /H
XCOPY %builddir%\ps3\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\ps3\ /E /Y /R /H
XCOPY %builddir%\TROPDIR\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\TROPDIR\ /E /Y /R /H

::--- Copy PS3 disk files ---::
robocopy %builddir%\ N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\ *.sfo
robocopy %builddir%\ N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\ *.png

:: -- Copy exes -- ::
XCOPY %builddir%\game_psn_beta_snc.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.* /Y /R /H
XCOPY %builddir%\game_psn_bankrelease_snc.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.* /Y /R /H
XCOPY %builddir%\game_psn_release_snc.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.* /Y /R /H

if %ERRORLEVEL% EQU 1 (
	echo Error when copying QA PS3 build!!
	echo !! Error when copying QA PS3 build !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
	exit
)
echo QA PS3 build copied successfully
echo -- QA PS3 build copied successfully --  >> %outFile%


echo ################################################
echo ## Copy Latest QA 360 Version to Distribution ##
echo ################################################
echo -- Started Copying the Latest QA Version to Distribution drive (360...) -- >> %outFile%
:: XCOPY %builddir%\common\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\common\ /E /Y /R /H
:: XCOPY %builddir%\metadata\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\metadata\ /E /Y /R /H
XCOPY %builddir%\xbox360\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\xbox360\ /E /Y /R /H

:: -- Copy exes -- ::
XCOPY %builddir%\game_xenon_beta.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.* /Y /R /H
XCOPY %builddir%\game_xenon_bankrelease.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.* /Y /R /H

if %ERRORLEVEL% EQU 1 (
	echo Error when copying QA 360 build!!
	echo !! Error when copying QA 360 build !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
	exit
)
echo QA 360 build copied successfully
echo -- QA 360 build copied successfully --  >> %outFile%

erase /F /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.bat
erase /F /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.idb
erase /F /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\*.ib_pdb_index
if %ERRORLEVEL% EQU 1 (
	echo Error deleting build root dir bats/idbs!!
	echo !! Error deleting build root dir bats/idbs !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
)


echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%
XCOPY C:\spu_debug\*.*  N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\spu_debug\ /E /Y /R /H
if %ERRORLEVEL% EQU 1 (
	echo Error copying spu_debug folder !!
	echo !! Error copying spu_debug folder !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
)
echo spu_debug folder copied successfully
echo -- spu_debug folder copied successfully --  >> %outFile%


:: === SET AS ACTIVE BUILD === ::
echo ###########################################################
echo ## Renaming old build to prevbuild and new data as build ##
echo ###########################################################
echo -- Setting new build data as active build -- >> %outFile%

:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\previous_build\ (
:: 	erase /F /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build\*.*
:: 	rmdir /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build
:: )
:: #######################################################::
::== NEED A WAY TO READ BUILD VERSIONS TO BACKUP BUILDS ==::
:: #######################################################::
:: --- if the version.txt parse failed then it'll rename to "VersionNUL" ---::
if exist N:\RSGEDI\Distribution\QA_Build\gta5\build\ (
	RENAME N:\RSGEDI\Distribution\QA_Build\gta5\build "Version%ver%"
)
if %ERRORLEVEL% EQU 1 (
	echo Error when renaming QA builds!! 
	echo Someone may have been accessing the old files, check the folders and rename manually if required.
	echo !! Error when renaming QA builds !! >> %outFile%
	echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
)

if exist N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\ (
	RENAME N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB "build"
)
if %ERRORLEVEL% EQU 1 (
	echo Error when renaming QA builds!! 
	echo Someone may have been accessing the old files, check the folders and rename manually if required.
	echo !! Error when renaming QA builds !! >> %outFile%
	echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
	echo Error > %statusFile%
	call C:\GTA5_QACopier\Blat.bat
	pause
)
echo Successfully renamed the QA folders ready for use
echo -- Successfully renamed the QA folders ready for use -- >> %outFile%

echo Finished Copying QA Builds!
echo -- Finished Copying QA Builds! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
echo Sending completion email...
call C:\GTA5_QACopier\Blat.bat



pause
exit


