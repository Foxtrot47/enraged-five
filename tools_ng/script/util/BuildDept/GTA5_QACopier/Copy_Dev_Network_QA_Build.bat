::****************************************************************************
:: GTA5 QA dev_network copy script											**
:: Updated: 				06/12/2012										**
:: Last edited by:			Ross McKinstray									**
:: Latest update to:  		add 360 data and exes, remove metadata folder	**
::								added spu_debug copy also					**
::****************************************************************************
@echo off
echo.


set builddir=X:\gta5\build\dev_network
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\dev_network

:: === COPY THE BUILD === ::

echo ################################################
echo ## Copy Latest QA PS3 Version to Distribution ##
echo ################################################
echo -- Started Copying the Latest QA Version to Distribution drive (PS3...) -- 
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*


:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*

erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.idb
erase /F /Q %copydir%\*.ib_pdb_index
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml


echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... 
robocopy /PURGE C:\spu_debug\ N:\RSGEDI\Distribution\QA_Build\gta5\dev_network_spu\spu_debug *.*
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*

echo dev_network spu_debug folder copied 
echo Finished Copying QA Builds!
echo -- Finished Copying Dev_Network QA Build! -- 


pause



