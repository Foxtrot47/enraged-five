::************************************************************************
:: GTA5 Durango deployed build prebuild copier							**
:: Updated: 				21/11/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Initial version 							**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5_Orbis_deployed_copier.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\disk_images\orbis\package\dev_ng\gta5-orbis
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\dev_ps4\pre-build"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Copying ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ######################################################
echo ## GTA5 PS4 deployable prebuild updating...         ##
echo ######################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%
robocopy /S /PURGE %builddir%\game\ %copydir%\game\ *.*
robocopy /S /PURGE %builddir%\tools\ %copydir%\tools\ *.*
robocopy %builddir% %copydir% DeployOrbis.bat




echo GTA5 PS4 deployable prebuild updated 
echo -- GTA5 PS4 deployable prebuild updated --  >> %outFile%


echo Finished 
echo -- Finished -- >> %outFile%
echo Finish > %statusFile%

pause
exit
