::************************************************************************
:: Duplicate QA build													**
:: Updated: 				02/02/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		-											**
::************************************************************************
@echo off
echo.

set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB_QA

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%
echo Start > %statusFile%
echo ## Duplicating existing QA build ## > %outFile%

:: === BLAT STUFF STARTING COPY === ::
call C:\GTA5_QACopier\Blat.bat

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist %ntwrkNewDir%\ (
	erase /F /S /Q %ntwrkNewDir%\*.*
	rmdir /S /Q %ntwrkNewDir%\
)

:: === MAKE A COPY OF THE CURRENT QA FOLDER === ::
echo ################################################
echo ## Make a Duplicate of the Current QA Folder  ##
echo ################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.

echo -- Duplicating Current QA Version -- >> %outFile%

robocopy /PURGE /S /DCOPY:T %ntwrkDir% %ntwrkNewDir% *.*
echo Successfully Duplicated Current QA Folder 
echo -- Successfully Duplicated Current QA Folder --  >> %outFile%

echo should've created new folder with existing timestamps

pause

exit


