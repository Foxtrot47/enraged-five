::************************************************************
:: GTA5 QA Build copy script								**
:: Updated: 				10/01/2013						**
:: Last edited by:			Ross McKinstray					**
:: Latest update to:  		Add 360 release exes to copy	**
::************************************************************

@echo off
echo

:: === SETUP STUFF === ::
:: --- don't need just now for gta5 --- ::
:: echo Delay the start of the copy allow version script to complete
:: @ping 127.0.0.1 -n 2 -w 1000 > nul
:: @ping 127.0.0.1 -n %1% -w 1000> nul

:: === directory setup === ::
set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB
set ntwrkToolsDir=N:\RSGEDI\Distribution\QA_Build\gta5\tools
:: set ntwrkArchive=%baseNtwrkPath%\__TEST_ntwrkArchive\build\dev
:: N:\RSGEDI\Distribution\QA_Build\gta5\

:: === Read version.txt === ::
set ver=NUL
if exist %ntwrkDir%\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (%ntwrkDir%\dev\common\data\version.txt) DO (
		set ver=%%G
		GOTO PRINT
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAIN
)
	
:PRINT
echo Current QA build is Version=%ver%
GOTO MAIN

	
:MAIN
set outFile=C:\GTA5copyLog.txt

::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

::echo %localBuildDir%
::echo ^%localBuildDir% > %outFile%
echo Start > %statusFile%
echo ## Running New_Copy_QA_build.bat ## > %outFile%

:: === BLAT STUFF STARTING COPY === ::
call C:\GTA5_QACopier\Blat.bat


:: === UPDATE THE QA TOOLS FOLDER === ::
echo ###########################################
echo ## Starting Update of QA tools... 		  ##
echo ###########################################

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******

echo Copying tools to QA folder
echo -- Copying tools to QA folder -- >> %outFile%
robocopy %localToolsDir%\bin\memvisualize\ %ntwrkToolsDir%\bin\memvisualize\ *.*
robocopy %localToolsDir%\bin\QAxbWatson\ %ntwrkToolsDir%\bin\QAxbWatson\ *.*
robocopy /S %localToolsDir%\bin\rag\ %ntwrkToolsDir%\bin\rag\ *.*
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setenv.bat"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setx.exe"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setx_d.htm"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "SysTrayRfs.exe"
robocopy /S %localToolsDir%\etc\rag %ntwrkToolsDir%\etc\rag *.*

echo Successfully copied tools to QA tools folder 
echo -- Successfully copied tools to QA tools folder  --  >> %outFile%


:: === COPY THE BUILD === ::
echo ###########################################
echo ## Starting Update of QA GTA5 build...   ##
echo ###########################################

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist %ntwrkNewDir%\ (
	erase /F /S /Q %ntwrkNewDir%\*.*
	rmdir /S /Q %ntwrkNewDir%\
)
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
::	echo Error when deleting incomplete build copies!!
::	echo !! Error when deleting incomplete build copies !! >> %outFile%
::	echo Error > %statusFile%
::	call C:\GTA5_QACopier\Blat.bat
::	pause
::	exit
::)

:: === MAKE A COPY OF THE CURRENT QA FOLDER === ::
echo ################################################
echo ## Make a Duplicate of the Current QA Folder  ##
echo ################################################

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******

:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.

echo -- Duplicating Current QA Version -- >> %outFile%
robocopy /PURGE /S /DCOPY:T %ntwrkDir% %ntwrkNewDir% *.*
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when copying dev_build QA PS3 build!!
:: 	echo !! Error when copying dev_build QA PS3 build !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: pause
:: exit
:: )
echo Successfully Duplicated Current QA Folder 
echo -- Successfully Duplicated Current QA Folder --  >> %outFile%

echo should've created new folder with existing timestamps
pause

:: === COPYING THE QA BUILD === ::
echo ################################################
echo ## Update New Files to New QA Folder (PS3)    ##
echo ################################################
echo -- Started QA Update (PS3...) -- >> %outFile%
robocopy /S /PURGE %localBuildDir%\common\ %ntwrkNewDir%\dev\common\ *.*
robocopy /S /PURGE %localBuildDir%\metadata\ %ntwrkNewDir%\dev\metadata\ *.*
robocopy /S /PURGE %localBuildDir%\ps3\ %ntwrkNewDir%\dev\ps3\ *.*
robocopy /S /PURGE %localBuildDir%\TROPDIR\ %ntwrkNewDir%\dev\TROPDIR\ *.*

::--- Copy PS3 disk files ---::
robocopy %localBuildDir%\ %ntwrkNewDir%\dev\ *.sfo
robocopy %localBuildDir%\ %ntwrkNewDir%\dev\ *.png

:: -- Copy exes -- ::
robocopy /PURGE %localBuildDir%\ %ntwrkNewDir%\dev\ game_psn_beta_snc.*
robocopy /PURGE %localBuildDir%\ %ntwrkNewDir%\dev\ game_psn_bankrelease_snc.*
robocopy /PURGE %localBuildDir%\ %ntwrkNewDir%\dev\ game_psn_release_snc.*
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when copying QA PS3 build!!
:: 	echo !! Error when copying QA PS3 build !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: pause
:: exit
:: )
echo PS3 QA build updated 
echo -- PS3 QA build updated --  >> %outFile%

echo #################################################
echo ## Update New Files to New QA Folder (Xbox360) ##
echo #################################################
echo -- Started QA Update (Xbox360...) -- >> %outFile%
robocopy /S /PURGE %localBuildDir%\xbox360\ %ntwrkNewDir%\dev\xbox360\ *.*

:: -- Copy exes -- ::
robocopy /PURGE %localBuildDir%\ %ntwrkNewDir%\dev\ game_xenon_beta.*
robocopy /PURGE %localBuildDir%\ %ntwrkNewDir%\dev\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when updating dev_build QA 360 build!!
:: 	echo !! Error when updating dev_build QA 360 build !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: 	exit
:: )
echo Xbox 360 QA build updated 
echo -- Xbox 360 QA build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
erase /F /Q %ntwrkNewDir%\dev\*.bat
erase /F /Q %ntwrkNewDir%\dev\*.snproj
erase /S /F /Q %ntwrkNewDir%\dev\buglist_*.xml
erase /F /Q %ntwrkNewDir%\dev\common\data\gta5_cache_*.dat

:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error deleting qa dev_build root dir bats/idbs!!
:: 	echo !! Error deleting qa dev_build root dir bats/idbs !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )


echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%

robocopy /PURGE C:\spu_debug\ %ntwrkNewDir%\dev\spu_debug\ *.*
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error copying dev_build spu_debug folder !!
:: 	echo !! Error copying dev_build spu_debug folder !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )
echo spu_debug folder copied successfully
echo -- spu_debug folder copied successfully --  >> %outFile%


echo build update complete, check it worked and check timestamps
pause


:: === SET AS ACTIVE BUILD === ::
echo ###########################################################
echo ## Renaming old build to prevbuild and new data as build ##
echo ###########################################################
echo -- Setting new build data as active build -- >> %outFile%

:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\previous_build\ (
:: 	erase /F /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build\*.*
:: 	rmdir /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build
:: )
:: #######################################################::
::== NEED A WAY TO READ BUILD VERSIONS TO BACKUP BUILDS ==::
:: #######################################################::
:: --- if the version.txt parse failed then it'll rename to "VersionNUL" ---::
if exist %ntwrkDir%\ (
	RENAME %ntwrkDir% "Version%ver%"
)
:: if %ERRORLEVEL% EQU 1 (
:: remove these error checks for now as the appear to flag false negatives... ?
:: echo Error when renaming QA builds!! 
:: echo Someone may have been accessing the old files, check the folders and rename manually if required.
:: 	echo !! Error when renaming QA builds !! >> %outFile%
:: 	echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )

if exist %ntwrkNewDir%\ (
	RENAME %ntwrkNewDir% "build"
)
:: if %ERRORLEVEL% EQU 1 (
	:: echo Error when renaming QA builds!! 
	:: echo Someone may have been accessing the old files, check the folders and rename manually if required.
	:: echo !! Error when renaming QA builds !! >> %outFile%
	:: echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
	:: echo Error > %statusFile%
	:: call C:\GTA5_QACopier\Blat.bat
	:: pause
:: )

echo Successfully renamed the QA folders ready for use
echo -- Successfully renamed the QA folders ready for use -- >> %outFile%

echo Finished Copying QA Builds!
echo -- Finished Copying QA Builds! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
echo Sending completion email...
call C:\GTA5_QACopier\Blat.bat



pause
exit


