@echo off

:: === SETUP STUFF === ::
:: --- don't need just now for gta5 --- ::
:: echo Delay the start of the copy allow version script to complete
:: @ping 127.0.0.1 -n 2 -w 1000 > nul
:: @ping 127.0.0.1 -n %1% -w 1000> nul

set outFile=C:\GTA5copyLog.txt

::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev

echo %builddir%
echo ^%builddir% > %outFile%
echo Start > %statusFile%
echo ## Running Copy_QA_build.bat ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
call C:\GTA5_QACopier\Blat.bat


:: === COPY THE BUILD === ::
echo ###########################################
echo ## Copying QA GTA5 build...              ##
echo ###########################################

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\ (
	erase /F /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\*.*
	rmdir /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\
)
if %ERRORLEVEL% EQU 1 (
	echo Error when deleting incomplete build copies!!
	echo !! Error when deleting incomplete build copies !! >> %outFile%
	echo Error > %statusFile%
	call C:\LANCopier\Blat.bat
	pause
	exit
)

echo No errors...

pause
