@echo off
echo.

set ver=NUL
:: set /A counter=1
:: set /P version= <X:\gta5\build\dev\common\data\version.txt
:: echo %version%
echo %ver%
:: Nasty, nasty hack to read the version number from the version.txt
:: parses the .txt, skips 3 lines then breaks the for loop with a goto
FOR /F "eol=# skip=3" %%G IN (X:\gta5\build\dev\common\data\version.txt) DO (
::	@echo %%G
	set ver=%%G
	GOTO PRINT
::	set test=%%G
::	set /A counter+=1
::	echo version=%ver%
::	echo counter=%counter%
)

:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\build2\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
::	FOR /F "eol=# skip=3" %%G IN (N:\RSGEDI\Distribution\QA_Build\gta5\build\dev\common\data\version.txt) DO (
::		set ver=%%G
::		GOTO PRINT
::	)
::) ELSE (
::	echo No Build Folder to Read Version... ignoring.
::GOTO MAIN
::)



:PRINT
echo version=%ver%
::echo %test%
::echo %counter%

	
:: :MAIN
:: echo reached main.


pause


:: FOR /F "tokens=1,3 delims=," %%G IN (weather.txt) DO @echo %%G %%H
:: 
:: Syntax
::         FOR /F ["options"] %%parameter IN (filenameset) DO command 
::       
::         FOR /F ["options"] %%parameter IN ("Text string to process") DO command
:: 		
:: Key   
::    options:
::       delims=xxx   The delimiter character(s) (default = a space)
:: 
::      skip=n       A number of lines to skip at the beginning of the file. 
::                    (default = 0)
::  
::       eol=;        Character at the start of each line to indicate a comment 
::                    The default is a semicolon ; 
:: 
::       tokens=n     Specifies which numbered items to read from each line 
::                   (default = 1)
::           
::      usebackq     Specify `back quotes`:                        
::                    - Use double quotes to quote long file names in filenameset.
::                    - Use single quotes for 'Text string to process'
::                      (useful if the text string contains double quotes)
:: 
::   Filenameset    A set of one or more files. Wildcards may be used. 
::                  If (filenameset) is a period character (.) then FOR will
::                   loop through every file in the folder.
:: 
::    command        The command to carry out, including any                  
::                   command-line parameters.
::   
::    %%parameter    A replaceable parameter:               
::                   in a batch file use %%G (on the command line %G)