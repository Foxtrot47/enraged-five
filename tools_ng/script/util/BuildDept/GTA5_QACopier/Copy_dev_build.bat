::************************************************************************
:: GTA5 QA DEV_Build copy script										**
:: Updated: 				24/05/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		master eu / us				**
::************************************************************************
@echo off
echo.

:: === SETUP STUFF === ::
:: --- don't need just now for gta5 --- ::
:: echo Delay the start of the copy allow version script to complete
:: @ping 127.0.0.1 -n 2 -w 1000 > nul
:: @ping 127.0.0.1 -n %1% -w 1000> nul

:: set ver=NUL
:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\build\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
:: 	FOR /F "eol=# skip=3" %%G IN (N:\RSGEDI\Distribution\QA_Build\gta5\build\dev\common\data\version.txt) DO (
:: 		set ver=%%G
:: 		GOTO PRINT
:: 	)
:: ) ELSE (
:: 	echo No Build Folder to Read Version... ignoring.
:: GOTO MAIN
:: )
	
:: :PRINT
:: echo version=%ver%
:: GOTO MAIN

	
:: :MAIN
set outFile=C:\GTA5copyLog.txt

::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\dev_build\dev

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Running Copy_dev_build.bat ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat


:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 QA dev_build...  PS3    ##
echo ###########################################

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******

:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started QA Dev_Build Update (PS3...) -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*

::--- Copy PS3 disk files ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
::robocopy /PURGE %builddir%\ %copydir%\ game_psn_final_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_eu_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_us_snc.*

:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when copying dev_build QA PS3 build!!
:: 	echo !! Error when copying dev_build QA PS3 build !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: pause
:: exit
:: )
echo dev_build PS3 QA updated 
echo -- dev_build PS3 QA updated --  >> %outFile%


echo ###########################################
echo ## Updating GTA5 QA dev_build...  360    ##
echo ###########################################
echo -- Started QA Dev_Build Update (360...) -- >> %outFile%
:: XCOPY %builddir%\common\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\common\ /E /Y /R /H
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_final.*

:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when updating dev_build QA 360 build!!
:: 	echo !! Error when updating dev_build QA 360 build !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: 	exit
:: )
echo dev_build QA 360 build updated 
echo -- dev_build QA 360 build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error deleting qa dev_build root dir bats/idbs!!
:: 	echo !! Error deleting qa dev_build root dir bats/idbs !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )


echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*
:: remove these error checks for now as the appear to flag false negatives... ?
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error copying dev_build spu_debug folder !!
:: 	echo !! Error copying dev_build spu_debug folder !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )
echo dev_build spu_debug folder copied 
echo -- dev_build spu_debug folder copied --  >> %outFile%


:: === SET AS ACTIVE BUILD === ::
:: echo ###########################################################
:: echo ## Renaming old build to prevbuild and new data as build ##
:: echo ###########################################################
:: echo -- Setting new build data as active build -- >> %outFile%
:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\previous_build\ (
:: 	erase /F /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build\*.*
:: 	rmdir /S /Q N:\RSGEDI\Distribution\QA_Build\gta5\build
:: )
:: #######################################################::
::== NEED A WAY TO READ BUILD VERSIONS TO BACKUP BUILDS ==::
:: #######################################################::
:: --- if the version.txt parse failed then it'll rename to "VersionNUL" ---::
:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\build\ (
:: 	RENAME N:\RSGEDI\Distribution\QA_Build\gta5\build "Version%ver%"
:: )
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when renaming QA builds!! 
:: 	echo Someone may have been accessing the old files, check the folders and rename manually if required.
::	echo !! Error when renaming QA builds !! >> %outFile%
:: 	echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )

:: if exist N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\ (
:: 	RENAME N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB "build"
:: )
:: if %ERRORLEVEL% EQU 1 (
:: 	echo Error when renaming QA builds!! 
:: 	echo Someone may have been accessing the old files, check the folders and rename manually if required.
:: 	echo !! Error when renaming QA builds !! >> %outFile%
:: 	echo !! Someone may have been accessing the old files, check the folders and rename manually if required. !! >> %outFile%
:: 	echo Error > %statusFile%
:: 	call C:\GTA5_QACopier\Blat.bat
:: 	pause
:: )
:: echo Successfully renamed the QA folders ready for use
:: echo -- Successfully renamed the QA folders ready for use -- >> %outFile%

echo Finished Updating QA Dev_build!
echo -- Finished Updating QA Dev_build! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
:: echo Sending completion email...
:: call C:\GTA5_QACopier\Blat.bat


pause
exit


