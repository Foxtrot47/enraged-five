::************************************************************************
:: GTA5 NG DLC packages copier											**
:: Updated: 				02/05/2014									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Add Hipster									**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5_ng_dlc_copier.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5_dlc\Packages
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\dlc"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Copying ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ######################################################
echo ## GTA5 streamed NG DLC updating...    		     ##
echo ######################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%

::PS4 EURO pkgs
::robocopy %builddir%\mpBeach\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpBusiness\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpBusiness2\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpChristmas\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpHipster\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpValentines\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpHeist\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpIndependence\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg
robocopy %builddir%\dlc_agentTrevor\ps4\pkgs\EU_subItems\ %copydir%\ps4\ *.pkg

::PS4 US pkgs
::robocopy %builddir%\mpBeach\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpBusiness\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpBusiness2\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpChristmas\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpHipster\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpValentines\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpHeist\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
::robocopy %builddir%\mpIndependence\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg
robocopy %builddir%\dlc_agentTrevor\ps4\pkgs\US_subItems\ %copydir%\ps4\ *.pkg

::XBOX ONE pkgs
::robocopy %builddir%\mpBeach\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.MPBEACH_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpBusiness\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.MPBUSINESS_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpBusiness2\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.BUSINESS2_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpChristmas\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.CHRISTMAS_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpHipster\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.HIPSTER_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpValentines\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.VALENTINES_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpHeist\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.MPHEISTS_1.0.0.0_neutral__fy653z9hy7gqj
::robocopy %builddir%\mpIndependence\xboxone\bin\dev\ %copydir%\xbone\ GTAV.DLC.INDEPENDENCE_1.0.0.0_neutral__fy653z9hy7gqj
robocopy %builddir%\dlc_agentTrevor\xboxone\bin\dev\ww\ %copydir%\xbone\ GTAV.DLC.CLIFFORD_1.0.0.0_neutral__fy653z9hy7gqj


echo GTA5 NG DLC updating updated 
echo -- GTA5 NG DLC updating --  >> %outFile%


echo Finished 
echo -- Finished -- >> %outFile%
echo Finish >> %statusFile%

pause
exit
