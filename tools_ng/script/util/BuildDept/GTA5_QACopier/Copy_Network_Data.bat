::************************************************************************
:: GTA5 Network Data copy script										**
:: Updated: 				15/04/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Adding locking mechanism					**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkDataLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Network_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir="N:\RSGEDI\Build Data\Latest\gta5\build\dev"

:: Set a lock file when data copy is in progress
if not exist %copydir% (
	mkdir %copydir%
)
echo locking dir > %copydir%\lock.txt
echo locking dir


echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating PREBUILD folder ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 Network Data...         ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started QA PreBuild Update -- >> %outFile%
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\x64\ %copydir%\x64\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*


echo GTA5 Network Data updated 
echo -- GTA5 Network Data updated --  >> %outFile%


echo Finished Updating GTA5 Network Data!
echo -- Finished Updating GTA5 Network Data! -- >> %outFile%
echo Finish > %statusFile%

::unlock
echo removing lock
erase /F /Q %copydir%\lock.txt


pause
exit
