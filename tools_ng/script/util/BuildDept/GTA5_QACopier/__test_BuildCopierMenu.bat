::************************************************************
:: GTA5 QA Build copy script								**
:: Updated: 				26/01/2013						**
:: Last edited by:			Ross McKinstray					**
:: Latest update to:  		create							**
::************************************************************

@echo off
echo



:: === directory setup === ::
set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB_TEST
set ntwrkToolsDir=N:\RSGEDI\Distribution\QA_Build\gta5\tools
set currbuild=%baseNtwrkPath%\CURRENT
set newbuild=%baseNtwrkPath%\PREBUILD


:: === Read version.txt === ::
set ver=NUL
if exist %curbuild%\ (
	:: Nasty, nasty hack to read the version number from the version.txt
	:: parses the .txt, skips 3 lines then breaks the for loop with a goto
	FOR /F "eol=# skip=3" %%G IN (%currbuild%\dev\common\data\version.txt) DO (
		set ver=%%G
		GOTO PRINT
	)
) ELSE (
	echo No Build Folder to Read Version... ignoring.
GOTO MAINMENU
)
	
:PRINT
echo Current QA build is Version=%ver%
GOTO MAINMENU


::*************************************************************************************************************
:MAINMENU
cls
echo.
echo ############################
echo ### MAIN BUILD MENU			 ####
echo ############################
echo.
echo ------------------------------------------------------------
echo 1) Copy existing CURRENT build.
echo 2) Copy existing QA build.
echo ------------------------------------------------------------
echo 3) Go to PREBUILD MENU
::echi 4) 
::echo 5) 
echo ------------------------------------------------------------
echo 6) Rename CURRENT to backed up version label.
echo 7) Rename PREBUILD to CURRENT
echo 8) Rename copied QA build to QA build.
echo ------------------------------------------------------------
echo 9) Patch copy CURRENT build.
echo.
echo Input your selection then press enter...

set /P Option=

if /I %option%==1 goto COPY_EXISTING_CURRENT
if /I %option%==2 goto COPY_EXISTING_QA
if /I %option%==3 goto PREBUILDMENU
::if /I %option%==4 goto 
::if /I %option%==5 goto 
if /I %option%==6 goto RENAME_CURRENT_FOR_BACKUP
if /I %option%==7 goto RENAME_PREBUILD_TO_CURRENT
if /I %option%==8 goto RENAME_NEW-QA_TO_QA
if /I %option%==9 goto PATCH_CURRENT
echo Error: Invalid Selection
pause
goto MAINMENU

::*************************************************************************************************************

:PREBUILDMENU
cls
echo.
echo ############################
echo ### PREBUILD BUILD MENU ####
echo ############################
echo.
echo --- Frequent options ----------------------------------------------------------
echo 1) Label Code with NewPreBuildCode.
echo 2) Update GTA5_PreBuild label.
echo 3) Update PREBUILD with latest.
echo -------------------------------------------------------------------------------
echo 4) Update existing COPIED_CURRENT with new data and rename to PREBUILD.
::echo 5) 
::echo 6) 
::echo 7) 
::echo 8) 
echo 9) Return to main menu.
echo.
echo Input your selection then press enter...

set /P Option=

if /I %option%==1 goto LABEL_NEWPREBUILDCODE
if /I %option%==2 goto LABEL_GTA5_PREBUILD
if /I %option%==3 goto UPDATE_PREBUILD
if /I %option%==4 goto UPDATE_COPIED_CURRENT_WITH_PREBUILD
::if /I %option%==5 goto 
::if /I %option%==6 goto 
::if /I %option%==7 goto 
::if /I %option%==8 goto 
if /I %option%==9 goto MAINMENU
echo Error: Invalid Selection
pause
goto PREBUILDMENU

	
::*************************************************************************************************************
:COPY_EXISTING_CURRENT
set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\dev_build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB_NEW

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%
echo Start > %statusFile%
echo ## Duplicating existing CURRENT build ## > %outFile%

:: === BLAT STUFF STARTING COPY === ::
::call C:\GTA5_QACopier\Blat.bat

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist %ntwrkNewDir%\ (
	erase /F /S /Q %ntwrkNewDir%\*.*
	rmdir /S /Q %ntwrkNewDir%\
)

:: === MAKE A COPY OF THE CURRENT QA FOLDER === ::
echo ################################################
echo ## Make a Duplicate of the CURRENT Folder  ##
echo ################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.

echo -- Duplicating CURRENT Version -- >> %outFile%

robocopy /PURGE /S /DCOPY:T %ntwrkDir% %ntwrkNewDir% *.*
echo Successfully Duplicated CURRENT Folder 
echo -- Successfully Duplicated CURRENT Folder --  >> %outFile%

echo should've created new folder with existing timestamps

pause
GOTO MAINMENU

::*************************************************************************************************************
:COPY_EXISTING_QA
set baseNtwrkPath=N:\RSGEDI\Distribution\QA_Build\gta5\
set localBuildDir=X:\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkDir=%baseNtwrkPath%\build
set ntwrkNewDir=%baseNtwrkPath%\COPYING_DONOTGRAB_QA

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%
echo Start > %statusFile%
echo ## Duplicating existing QA build ## > %outFile%

:: === BLAT STUFF STARTING COPY === ::
call C:\GTA5_QACopier\Blat.bat

echo Deleting any incomplete build copies
echo -- Deleting any incomplete build copies -- >> %outFile%
:: === Delete any partial build copies === ::
if exist %ntwrkNewDir%\ (
	erase /F /S /Q %ntwrkNewDir%\*.*
	rmdir /S /Q %ntwrkNewDir%\
)

:: === MAKE A COPY OF THE CURRENT QA FOLDER === ::
echo ################################################
echo ## Make a Duplicate of the Current QA Folder  ##
echo ################################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.

echo -- Duplicating Current QA Version -- >> %outFile%

robocopy /PURGE /S /DCOPY:T %ntwrkDir% %ntwrkNewDir% *.*
echo Successfully Duplicated Current QA Folder 
echo -- Successfully Duplicated Current QA Folder --  >> %outFile%

echo should've created new folder with existing timestamps

pause
GOTO MAINMENU
::*************************************************************************************************************
:RENAME_CURRENT_FOR_BACKUP

if exist %currbuild%\ (
	RENAME %currbuild% "Version%ver%"
)

pause
GOTO MAINMENU

::*************************************************************************************************************
:RENAME_PREBUILD_TO_CURRENT


if exist %newbuild%\ (
	RENAME %newbuild% "CURRENT"
)

pause
GOTO MAINMENU

::*************************************************************************************************************
:RENAME_NEW-QA_TO_QA

pause
GOTO MAINMENU

::*************************************************************************************************************
:PATCH_CURRENT
set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\CURRENT\dev

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Patching CURRENT build ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat

echo ###########################################
echo ## Patching CURRENT Build			    ####
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started CURRENT build patch update -- >> %outFile%

robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\pc\ %copydir%\pc\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_win32_beta_dx11.*
echo CURRENT build updated 
echo -- CURRENT build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
::erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

echo Finished Updating CURRENT build!
echo -- Finished Updating CURRENT build! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
:: echo Sending completion email...
:: call C:\GTA5_QACopier\Blat.bat

pause
GOTO MAINMENU


::*************************************************************************************************************
:UPDATE_COPIED_CURRENT_WITH_PREBUILD
set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB_NEW\dev
set basecopydir=N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB_NEW

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating COPIED CURRENT with latest data ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Updating COPIED CURRENT with latest data -- >> %outFile%

robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\pc\ %copydir%\pc\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_win32_beta_dx11.*
echo COPIED CURRENT build updated 
echo -- COPIED CURRENT build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
::erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

echo Finished Updating COPIED CURRENT build with new data!
echo -- Finished Updating COPIED CURRENT build with new data! -- >> %outFile%
echo Finish > %statusFile%

::-------- rename COPIED CURRENT to PREBUILD
if exist %basecopydir%\ (
	RENAME %basecopydir% "PREBUILD"
)
echo.
echo COPIED CURRENT build renamed to PREBUILD

pause	

:: === BLAT STUFF === ::
:: echo Sending completion email...
:: call C:\GTA5_QACopier\Blat.bat

pause
GOTO PREBUILDMENU

::*************************************************************************************************************
:UPDATE_PREBUILD
set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\PREBUILD\dev

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating PREBUILD folder ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 QA PREbuild...  PS3    ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started QA PreBuild Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\pc\ %copydir%\pc\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_win32_beta_dx11.*

echo prebuild QA 360 build updated 
echo -- prebuild QA 360 build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*
echo prebuild spu_debug folder copied 
echo -- prebuild spu_debug folder copied --  >> %outFile%
::-----------------

echo Finished Updating QA prebuild!
echo -- Finished Updating QA prebuild! -- >> %outFile%
echo Finish > %statusFile%

pause

GOTO PREBUILDMENU

::*************************************************************************************************************
:PATCH_CURRENT

pause

GOTO PREBUILDMENU

::*************************************************************************************************************
:LABEL_NEWPREBUILDCODE

PUSHD %RS_PROJROOT%
echo.
echo UPDATING PREBUILD LABEL
echo.
P4 labelsync -l GTA5_NewPreBuildCode //depot/gta5/src/dev/...
P4 labelsync -l GTA5_NewPreBuildCode //rage/gta5/dev/...
P4 labelsync -l GTA5_NewPreBuildCode //depot/gta5/xlast/...
POPD

pause

GOTO PREBUILDMENU

::*************************************************************************************************************
:LABEL_GTA5_PREBUILD
PUSHD %RS_PROJROOT%
echo.
echo UPDATING PREBUILD LABEL
echo.
P4 labelsync -l GTA5_PreBuild //depot/gta5/assets/export/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/assets/GameText/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/build/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/src/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/script/dev/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/xlast/...
P4 labelsync -l GTA5_PreBuild //depot/gta5/tools/...
P4 labelsync -l GTA5_PreBuild //rage/gta5/dev/...
P4 labelsync -l GTA5_PreBuild //ps3sdk/...
POPD

echo Copy PreBuild exes to network folder.

if not exist N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\ (
	mkdir N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\
)

::robocopy /PURGE C:\spu_debug\ N:\RSGEDI\Distribution\QA_build\gta5\PreBuildExes\spu_debug\ *.*
XCOPY /I /R /Y C:\spu_debug\*.* N:\RSGEDI\Distribution\QA_Build\gta5\PreBuildExes\spu_debug\*.*

pause

GOTO PREBUILDMENU


pause
exit


