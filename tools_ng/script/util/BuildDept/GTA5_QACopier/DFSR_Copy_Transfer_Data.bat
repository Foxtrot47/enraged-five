::************************************************************************
:: DFSR - GTA5 Network Transfer copy script								**
:: Updated: 				30/06/2014									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Add sce_module folder						**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkTransferLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev_ng
set copydir="T:\GTA5NG\build\dev_ng"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating DFSR folder ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 Transfer Data...         ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps4\ %copydir%\ps4 *.*
robocopy /S /PURGE %builddir%\sce_companion_httpd\ %copydir%\sce_companion_httpd *.*
robocopy /S /PURGE %builddir%\sce_sys\ %copydir%\sce_sys *.*
robocopy /S /PURGE %builddir%\sce_module\ %copydir%\sce_module *.*
robocopy /S /PURGE %builddir%\update\ %copydir%\update *.*
robocopy /S /PURGE %builddir%\x64\ %copydir%\x64\ *.* /XD "Social Club SDK"
robocopy /S /PURGE %builddir%\xbo_loose\ %copydir%\xbo_loose *.*
robocopy /S /PURGE %builddir%\xbo_scripts\ %copydir%\xbo_scripts *.*
robocopy /S /PURGE %builddir%\xboxone\ %copydir%\xboxone *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll
robocopy %builddir%\ %copydir%\ *.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_durango_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_durango_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_durango_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_durango_final.*
robocopy /PURGE %builddir%\ %copydir%\ game_orbis_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_orbis_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_orbis_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_orbis_final.*
robocopy /PURGE %builddir%\ %copydir%\ game_win64_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_win64_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_win64_beta.*



erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /S /F /Q %copydir%\*_ross.mckinstray.xml
erase /S /F /Q %copydir%\commandline*.txt
erase /S /F /Q %copydir%\args*.txt
erase /S /F /Q %copydir%\*.log

echo GTA5 Transfer Data updated 
echo -- GTA5 Transfer Data updated --  >> %outFile%


echo Finished Updating GTA5 Transfer Data!
echo -- Finished Updating GTA5 Network Data! -- >> %outFile%
echo Finish > %statusFile%

pause
exit
