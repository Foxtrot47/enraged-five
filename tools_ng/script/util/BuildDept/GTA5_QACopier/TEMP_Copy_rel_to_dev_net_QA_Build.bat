@echo off

set builddir=X:\gta5\build\release
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\dev_network

:: === COPY THE BUILD === ::

echo ################################################
echo ## Copy Latest QA PS3 Version to Distribution ##
echo ################################################
echo -- Started Copying the Latest QA Version to Distribution drive (PS3...) -- 
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\metadata\ %copydir%\metadata\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*


:: -- Copy exes -- ::
robocopy %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy %builddir%\ %copydir%\ game_psn_bankrelease_snc.*

erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.idb
erase /F /Q %copydir%\*.ib_pdb_index
erase /F /Q %copydir%\*.snproj

echo Finished Copying QA Builds!
echo -- Finished Copying Dev_Network QA Build! -- 


pause



