::************************************************************************
:: GTA5 Network Transfer copy script									**
:: Updated: 				15/02/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Add copying of common and exes				**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5NetworkTransferLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir="N:\RSGEDI\Distribution\Transfers\Outbound\Automatic\GTA5Build\dev"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating PREBUILD folder ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 Transfer Data...         ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\x64\ %copydir%\x64\ *.* 
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_final_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_win64_beta.*
::robocopy /PURGE %builddir%\ %copydir%\ game_win32_bankrelease_dx11.*

erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml


echo GTA5 Transfer Data updated 
echo -- GTA5 Transfer Data updated --  >> %outFile%


echo Finished Updating GTA5 Transfer Data!
echo -- Finished Updating GTA5 Network Data! -- >> %outFile%
echo Finish > %statusFile%

pause
exit
