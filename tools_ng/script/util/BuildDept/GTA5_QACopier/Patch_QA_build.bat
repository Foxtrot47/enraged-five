::************************************************************************************
:: GTA5 QA DEV_Build copy script													**
:: Updated: 				24/05/2013												**
:: Last edited by:			Ross McKinstray											**
:: Latest update to:  		master eu / us											**
::************************************************************************************
@echo off
echo.
	
:: :MAIN
set outFile=C:\GTA5copyLog.txt

::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\build\dev
set localToolsDir=X:\gta5\tools
set ntwrkToolsDir=N:\RSGEDI\Distribution\QA_Build\gta5\tools

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Running Copy_dev_build.bat ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat


:: === UPDATE THE QA TOOLS FOLDER === ::
echo ###########################################
echo ## Starting Update of QA tools... 		  ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
echo Copying tools to QA folder
echo -- Copying tools to QA folder -- >> %outFile%
robocopy %localToolsDir%\bin\memvisualize\ %ntwrkToolsDir%\bin\memvisualize\ *.*
robocopy %localToolsDir%\bin\QAxbWatson\ %ntwrkToolsDir%\bin\QAxbWatson\ *.*
robocopy /S %localToolsDir%\bin\rag\ %ntwrkToolsDir%\bin\rag\ *.*
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setenv.bat"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setx.exe"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "setx_d.htm"
robocopy %localToolsDir%\bin %ntwrkToolsDir%\bin "SysTrayRfs.exe"
robocopy /S %localToolsDir%\etc\rag %ntwrkToolsDir%\etc\rag *.*

echo Successfully copied tools to QA tools folder 
echo -- Successfully copied tools to QA tools folder  --  >> %outFile%


:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 QA dev_build...  PS3    ##
echo ###########################################

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******

:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started QA Dev_Build Update (PS3...) -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*

::--- Copy PS3 disk files ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
::robocopy /PURGE %builddir%\ %copydir%\ game_psn_final_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_eu_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_master_us_snc.*

echo dev_build PS3 QA updated 
echo -- dev_build PS3 QA updated --  >> %outFile%


echo ###########################################
echo ## Updating GTA5 QA dev_build...  360    ##
echo ###########################################
echo -- Started QA Dev_Build Update (360...) -- >> %outFile%
:: XCOPY %builddir%\common\*.* N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB\dev\common\ /E /Y /R /H
robocopy /S /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_final.*

echo dev_build QA 360 build updated 
echo -- dev_build QA 360 build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml
erase /S /F /Q %copydir%\*_ross.mckinstray.xml
erase /S /F /Q %ntwrkToolsDir%\Parser.SanScript.dll

echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*


echo dev_build spu_debug folder copied 
echo -- dev_build spu_debug folder copied --  >> %outFile%



echo Finished Updating QA build!
echo -- Finished Updating QA build! -- >> %outFile%
echo Finish > %statusFile%

:: === BLAT STUFF === ::
:: echo Sending completion email...
:: call C:\GTA5_QACopier\Blat.bat


pause
exit


