::************************************************************************************
:: GTA5 Durango deployed build prebuild copier										**
:: Updated: 				10/12/2013												**
:: Last edited by:			Ross McKinstray											**
:: Latest update to:  		Don't include my local rfs.dat in package.				**
:: 							plus copy ip batch file in same folder as deploy script **
::************************************************************************************
@echo off
echo.

set outFile=C:\GTA5_Durango_.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_Transfer_Data\statusFile.txt
echo. > %statusFile%

set builddir="X:\gta5\build\disk_images\durango\package\dev_ng\gta5-durango"
set copydir="N:\RSGEDI\Distribution\QA_Build\gta5\dev_xbone\PREBUILD"
set XBOtools="X:\gta5\build\dev_ng"

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Copying ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 Transfer Data...         ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started GTA5 Transfer Update -- >> %outFile%
robocopy /S /PURGE %builddir%\game\ %copydir%\game\ *.* /xf "rfs.dat"
robocopy /S /PURGE %builddir%\tools\ %copydir%\tools\ *.*
robocopy /PURGE %XBOtools%\xbo_scripts %copydir%\xbo_scripts\ *.bat
robocopy %XBOtools%\xbo_scripts\ %copydir%\ ip_address.bat
robocopy %builddir% %copydir% DeployDurango*.bat

erase /F /Q %copydir%\game\rfs.dat


echo PREBUILD - GTA5 Durango deployable current updated 
echo -- PREBUILD - GTA5 Durango deployable current updated --  >> %outFile%


echo PREBUILD - GTA5 Durango deployable current updated
echo -- PREBUILD - GTA5 Durango deployable current updated -- >> %outFile%
echo Finish > %statusFile%

pause
exit
