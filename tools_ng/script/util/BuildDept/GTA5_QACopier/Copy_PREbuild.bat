::************************************************************************
:: GTA5 QA PreBuild copy script											**
:: Updated: 				12/04/2013									**
:: Last edited by:			Ross McKinstray								**
:: Latest update to:  		Add copying of PC data/exes					**
::************************************************************************
@echo off
echo.

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\PREBUILD\dev

::lock the folder
if not exist %copydir% (
	mkdir %copydir%
)
echo locking dir > %copydir%\lock.txt
echo locking dir
pause



echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating PREBUILD folder ## >> %outFile%

:: === ROBOCOPY THE BUILD === ::
echo ###########################################
echo ## Updating GTA5 QA PREbuild...  PS3    ##
echo ###########################################
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Started QA PreBuild Update -- >> %outFile%
robocopy /S /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /PURGE /MT:12 %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /PURGE /MT:12 %builddir%\x64\ %copydir%\x64\ *.* /XD "Social Club SDK"
robocopy /S /PURGE /MT:12 %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*


echo prebuild QA 360 build updated 
echo -- prebuild QA 360 build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml
erase /S /F /Q %copydir%\*_ross.mckinstray.xml

echo ################################################
echo ## Copy spu_debug folder to Distribution      ##
echo ################################################
echo -- Started Copying spu_debug folder to QA build folder... -- >> %outFile%
robocopy /PURGE C:\spu_debug\ %copydir%\spu_debug\ *.*
echo prebuild spu_debug folder copied 
echo -- prebuild spu_debug folder copied --  >> %outFile%
::-----------------

echo Finished Updating QA prebuild!
echo -- Finished Updating QA prebuild! -- >> %outFile%
echo Finish > %statusFile%

echo removing lock
erase /F /Q %copydir%\lock.txt


pause
exit


