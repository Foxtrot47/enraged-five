@echo off
echo.

cd /d %~dp0

:: checkout
p4 edit //depot/gta5/build/dev_ng/update/ps4/patch/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/update/ps4/patch/data/lang/american_REL.rpf
p4 edit //depot/gta5/build/dev_ng/update/x64/patch/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/update/x64/patch/data/lang/american_REL.rpf
p4 edit //depot/gta5/build/dev_ng/update/xboxone/patch/data/lang/american.rpf
p4 edit //depot/gta5/build/dev_ng/update/xboxone/patch/data/lang/american_REL.rpf


:: build
pushd X:\gta5\assets_ng\GameText\TitleUpdate\American\
	american.bat
popd

