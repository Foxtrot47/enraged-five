::********************************************************************************
:: Update COPIED_CURRENT with new local PREBUILD data and rename to PREBUILD	**
:: Updated: 				02/02/2013											**
:: Last edited by:			Ross McKinstray										**
:: Latest update to:  		-													**
::********************************************************************************
@echo off
echo.

set outFile=C:\GTA5copyLog.txt
::--- Set and create/clear the status file. Blat.bat uses this for varying subject headers ---::
set statusFile=C:\GTA5_QACopier\statusFile.txt
echo. > %statusFile%

set builddir=X:\gta5\build\dev
set copydir=N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB_NEW\dev
set basecopydir=N:\RSGEDI\Distribution\QA_Build\gta5\COPYING_DONOTGRAB_NEW

echo Copying %builddir% to %copydir%
echo Copying ^%builddir% to %copydir% > %outFile%
echo Start > %statusFile%
echo ## Updating COPIED CURRENT with latest data ## >> %outFile%

:: === BLAT STUFF STARTING COPY === ::
:: call C:\GTA5_QACopier\Blat.bat

:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /PURGE removes file/folders from destination that no longer exist in source.
echo -- Updating COPIED CURRENT with latest data -- >> %outFile%

robocopy /S /MT:12 /PURGE %builddir%\common\ %copydir%\common\ *.*
robocopy /S /MT:12 /PURGE %builddir%\ps3\ %copydir%\ps3\ *.*
robocopy /S /MT:12 /PURGE %builddir%\x64\ %copydir%\x64\ *.*
robocopy /S /MT:12 /PURGE %builddir%\xbox360\ %copydir%\xbox360\ *.*
robocopy /S /MT:12 /PURGE %builddir%\TROPDIR\ %copydir%\TROPDIR\ *.*
robocopy /PURGE /MT:12 C:\spu_debug\ %copydir%\spu_debug\ *.*

::--- Copy PS3 disk files & PC bink dlls ---::
robocopy %builddir%\ %copydir%\ *.sfo
robocopy %builddir%\ %copydir%\ *.png
robocopy %builddir%\ %copydir%\ binkw*.dll

:: -- Copy exes -- ::
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_beta.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_bankrelease.*
robocopy /PURGE %builddir%\ %copydir%\ game_xenon_release.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_beta_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_bankrelease_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_release_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_psn_final_snc.*
robocopy /PURGE %builddir%\ %copydir%\ game_win64_beta.*

echo COPIED CURRENT build updated 
echo -- COPIED CURRENT build updated --  >> %outFile%

:: delete the run .bat and intermediate exe files that qa DONOTWANT!
::erase /F /Q %copydir%\*.bat
erase /F /Q %copydir%\*.snproj
erase /S /F /Q %copydir%\buglist_*.xml
erase /F /Q %copydir%\common\data\gta5_cache_*.*
erase /S /F /Q %copydir%\BugsNeedingAssigned_*.xml

echo Finished Updating COPIED CURRENT build with new data!
echo -- Finished Updating COPIED CURRENT build with new data! -- >> %outFile%
echo Finish > %statusFile%

::-------- rename COPIED CURRENT to PREBUILD
if exist %basecopydir%\ (
	RENAME %basecopydir% "PREBUILD"
)
echo.
echo COPIED CURRENT build renamed to PREBUILD


pause
exit


