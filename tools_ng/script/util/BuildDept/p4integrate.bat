@ECHO OFF
TITLE GTAV - Auto integrate 
ECHO GTAV - auto integrate 

PUSHD %RS_PROJROOT%
call %RS_TOOLSROOT%\bin\setenv.bat

:: Nasty, nasty hack to read the version number from the version.txt
:: parses the .txt, skips 3 lines then breaks the for loop with a goto
p4 sync -f //depot/gta5/build/dev/common/data/version.txt
FOR /F "eol=# skip=3" %%G IN (X:\gta5\build\dev\common\data\version.txt) DO (
	set ver=%%G
	GOTO PRINT
)

:PRINT
	echo version=%ver%
GOTO MAIN

:MAIN
	Echo data
::	p4 integrate -i -d //depot/gta5/assets/export/...@GTA5_[ALL]_version_%ver% //depot/gta5/build/release/export/...
	p4 integrate -i -d //depot/gta5/build/dev/...@GTA5_[ALL]_version_%ver% //depot/gta5/build/release/...
	p4 resolve -at //depot/gta5/build/release/...
	p4 submit -d "GTA5_[ALL]_version_%ver% - Auto-integrate of latest build to release branch" //depot/gta5/build/release/...

	Echo code
	p4 integrate -i -d //depot/gta5/src/dev/...@GTA5_[ALL]_version_%ver% //depot/gta5/src/release/...
	p4 resolve -at //depot/gta5/src/release/...
	%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_submit.rb --port=rsgedip4s1:1666 --client=EDIW-RMCKINS4 --description="GTA5_[ALL]_version_%ver% - Auto-integrate of latest build game src to release branch$$Buddy:$No-one" x:/gta5/src/release/...

	Echo rage
	p4 -c EDIW-RMCKINS4 -p rsgedip4s1:1666 integrate -i -d //rage/gta5/dev/...@GTA5_[ALL]_version_%ver% //rage/gta5/release/...
	p4 -c EDIW-RMCKINS4 -p rsgedip4s1:1666 resolve -at //rage/gta5/release/...
	%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_submit.rb --port=rsgedip4s1:1666 --client=EDIW-RMCKINS4 --description="GTA5_[ALL]_version_%ver% - Auto-integrate of latest build rage src to release branch$$Buddy:$No-one" x:/gta5/src/release/rage/...

exit
