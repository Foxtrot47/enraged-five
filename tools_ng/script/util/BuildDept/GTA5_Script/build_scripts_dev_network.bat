@echo off
call setenv.bat > nul

setlocal EnableDelayedExpansion
set BRANCH=dev_network
set CONTINUE_BUILDING=1

:: GTA 5 project
set SCRIPT_PROJECT=%RS_PROJROOT%\script\%BRANCH%\singleplayer\gta5_SP.scproj
set BUILD_CONFIGURATION=DEBUG
call :PerformBuild
if %CONTINUE_BUILDING%==0 goto :eof

set BUILD_CONFIGURATION=RELEASE
call :PerformBuild
if %CONTINUE_BUILDING%==0 goto :eof

:: Agent Trevor project
set SCRIPT_PROJECT=%RS_PROJROOT%\script\%BRANCH%\dlcscripts\dlcAgentTrevor\dlcAgentTrevor.scproj
set BUILD_CONFIGURATION=DEBUG
call :PerformBuild
if %CONTINUE_BUILDING%==0 goto :eof

set BUILD_CONFIGURATION=RELEASE
call :PerformBuild
if %CONTINUE_BUILDING%==0 goto :eof

:: Done building stuff.  Jump to the end of the batch script.
goto :eof


:PerformBuild

echo Building %SCRIPT_PROJECT% - %BUILD_CONFIGURATION%
%RS_TOOLSBIN%\RageScriptEditor\IncrediBuildScriptProject.exe %BUILD_CONFIGURATION% %SCRIPT_PROJECT% -FullBuild
if errorlevel 1 (
	echo Build failed.  Do you wish to continue building the other configurations?
	set /P USER_CHOICE=Continue (Y/N^)^? 
	
	if /I "!USER_CHOICE!" EQU "N" (
		set CONTINUE_BUILDING=0
		echo Aborting script.
	)
)
goto :eof
