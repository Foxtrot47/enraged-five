#!python2
# Name: buildmail_to_wiki.py
# Author: Iain Downie <iain.downie@rockstarnorth.com>
# Purpose: Reformat build release email information to wikitext for speedy archival at
# https://devstar.rockstargames.com/wiki/index.php/GTA5_build_version_history etc.

import os
import sys
import webbrowser
import datetime
i = datetime.datetime.now()


# Get the email source text.
try:
	with open(sys.argv[1], 'r') as infile:
		email_source = [x for x in infile.xreadlines()]
except LookupError:
	print'\n'
	sys.exit('Error: You must pass a .txt copy of a build release email as\nan argument from within the same directory as the script.')


# Initialise variables
branch, buildbranch, build_ver, code_cls_line, date, date_comp, date_text = ('unknown' for x in range(7))
num_added_cls, num_info, num_issues, num_notes = (0 for x in range(4))
game_edit, rage_edit = (False for x in range(2))


# Get & set the build/branch identifier variations
for line in email_source:
	line = line.lower()
	if 'building from' in line or 'compiled from' in line or 'runs from' in line:
		buildbranch = line
		if 'dev_live' in buildbranch[16:24]:
			branch = 'Dev_live'
			build_ver = 'TU'
		elif 'dev_net' in buildbranch[16:24]:
			branch = 'Dev_network'
			build_ver = 'TU'
		elif 'dev_ng_live' in buildbranch:
			branch = 'Dev_ng_Live'
			build_ver = 'NG'
		elif 'dev_ng' in buildbranch:
			branch = 'Dev_ng'
			build_ver = 'NG'
		elif 'dev_temp' in buildbranch:
			branch = 'Dev_temp'
			build_ver = 'NG'
		break

# Static strings
header1 = '==Build notes=='
header2 = '==Build info=='
header3 = '==Known issues=='
footer  = 'Back to [[GTA5 build version history]]'


# Construct footer wikilink depending on build.
if build_ver == 'NG':
	footer = 'Back to [[GTA5 '+str(build_ver)+' build version history]]'
if build_ver == 'TU':
	footer = 'Back to [[GTA5 build version history]]'


# Instruct the user and have them specify section lengths to aid formatting
print '\n*************************************************************************************'
print '* buildmail_to_wiki - reformat build release emails to wikitext for speedy archival *'
print '*************************************************************************************\n'
'''
Usage: Pass a .txt copy of a build release email as argument from the same directory.
Details needed from the user: the length (in lines) of the notes and info sections.
details.txt and summary.txt will be written out and opened for copying to the wiki.
'''

num_notes = raw_input('How many bulleted lines are in the \'Build notes\' section?  ')
num_info = raw_input('How many bulleted lines are in the \'Build info\' section?  ')


# Clumsy special case removal for B* 1661863 info
for x in email_source:
	if '\x95' in x and 'shelved' in x and '1661863' in x:
		email_source.remove(x)

bullet = ('\x95', '\x97', 'o')

# Construct and format body, then ensure there is a space after Bugstar URLs
text_untreated = [ x.replace('\x95','* ') for x in email_source if x.startswith(bullet) ]

text_list = []

for x in text_untreated:
	text_list.append(x.replace('\t','').replace('         ','').replace('\n','').replace('* 1', '* bugstar:1').replace('* 2', '* bugstar:2').replace('\x95','* ').replace('\x97','** '))

for index, line in enumerate(text_list):
	if line.startswith('o'):
		text_list[index] = '** '+line[1:]
	if 'bugstar:' in line:
		text_list[index] = line.replace(' - ','-').replace('-',' - ').replace('  ',' ')


# Count how many bugs we have listed under 'Known issues' and remove empty bullets
for x in text_list:
	if x == '* ':
		text_list.remove(x)
	if 'bugstar:' in x:
		num_issues += 1


# Use derived/specified values to correctly format the body text
text_list.insert(0,header1)
text_list.insert(int(num_notes)+1,'\n')
text_list.insert(int(num_notes)+2,header2)
text_list.insert((int(num_notes)+int(num_info)+3),'\n')
text_list.insert((int(num_notes)+int(num_info)+4),header3)


# Trim fluff from the end and append footer
trimlist = 0
body_approx_len = int(num_notes) + int(num_info) + int(num_issues) + 6

for x in text_list[int(body_approx_len - 1):]:
	if 'bugstar:' not in x:
		trimlist += 1

desired_length = len(text_list) - int(trimlist)
text_list = text_list[:int(desired_length)]

text_list.append('\n')
text_list.append(footer)


# Get the long version name for use in the summary
for x in text_list:
	if 'labelled as' in x.lower():
		label = ''.join(x.split(':')[-1]).strip().replace(' ','')
		ver_no = ''.join([ y for y in label[8:] if y >= "0" and y <= "9" ])
		
		if '.' in x:
			ver_no = ver_no[:-1]+'.'+ver_no[-1]
#		break


# Get the code CL details line
for x in text_list:
	if 'Game CL' in x:
		code_cls_line = x
		break


# Convert the line to an ordered list of CL number constituent integers
code_cls_raw = [ x for x in code_cls_line if x >= "0" and x <= "9" ]
code_cls_raw = ''.join(code_cls_raw)
game_cl = code_cls_raw[0:7] # The first 7 digits are the game CL
rage_cl = code_cls_raw[7:14] # The second 7 digits are the rage CL

# From the 15th digit onwards we are in uncharted territory. Breaking this
# remainder down into however many 7-digit strings should give us each of
# the additional CL numbers. Currently assume arbitrary max of 10 added CLs.

added_cls_raw = []
tmp = [21,28,35,42,49,56,63,70,77,84,91,98,105,112,119,126,133,140,147,154,161,168,175,182]

for t in tmp:
	if len(code_cls_raw) >= t:
		added_cls_raw.append(code_cls_raw[t-7:t])

num_added_cls = len(added_cls_raw)
extra_cls_list = [ x for x in added_cls_raw if len(str(x)) == 7 ]


# Function to inspect additional code CL(s) for game- or rage-ness
def inspect_code_additions(code_additions):
	global game_edit
	global rage_edit
	# Check for game and/or rage code files
	for x in code_additions:
		for y in x:
			if '/src/dev' in y:
				game_edit = True
			if '//rage/' in y:
				rage_edit = True


# Use external P4 CLI commands to get affected files in additional CLs
extra_code = []
if num_added_cls > 0:
	for x in extra_cls_list:
		os.system( 'p4 describe -s '+str(x)+' > extra_cl_'+str(x)+'.txt' )
		cl_detail_source = open('extra_cl_'+str(x)+'.txt', 'r')
		cl_detail = [ x for x in cl_detail_source.readlines() ]
		extra_code.append(cl_detail)
		file.close(cl_detail_source)
	os.system( 'del extra_cl_*.txt' ) # Clean up the extra CL text files


# Construct build release (NB: not grab) date info for summary
# Add a 0 before the day and/or month if in the single digits
date_comp = str(i.day)+'-'+str(i.month)+'-'+str(i.year)

if len(date_comp) == 9 and date_comp[1] == '-':
	date = '0'+str(date_comp)
elif len(date_comp) == 8:
	date = '0'+str(date_comp[0])+str(date_comp[1])+'0'+str(date_comp[2:])
elif len(date_comp) < 10 and date_comp[1] != '-':
	date = date_comp[:3]+'0'+date_comp[3:]
else:
	date = date_comp


# Constuct and format summary
if len(text_list) > 0:
	try:
		summary = ['|-', '| [[v'+str(ver_no)+'_'+str(build_ver)+']]', '| QA_'+str(date), '| #'+str(game_cl), '| #'+str(rage_cl), '| '+label, '| QA build. '+str(branch)+' branch.', '|-']
	except NameError:
		print '\n'
		sys.exit('ERROR: Did you copy the body text of a build release email into email.txt?')


# Append (+) to game and or rage code CL listings as per return value of inspect_code_additions()
inspect_code_additions(extra_code)

if game_edit and not rage_edit:
	summary[3] = summary[3]+' (+)'

if rage_edit and not game_edit:
	summary[4] = summary[4]+' (+)'

if game_edit and rage_edit:
	summary[3] = summary[3]+' (+)'
	summary[4] = summary[4]+' (+)'


# Output details to text file
with open('details.txt', 'w') as details_wiki:
	for x in text_list:
		print>>details_wiki, x
	print '\nBuild details saved to details.txt.'

# Output summary to text file
with open('summary.txt', 'w') as summary_wiki:
	for x in summary:
		print>>summary_wiki, x
	print 'Build summary saved to summary.txt\n.'

# Open the wiki pages & text files
print '\nOpening files & pages.'
if build_ver == 'NG':
	webbrowser.open_new_tab('https://devstar.rockstargames.com/wiki/index.php?title=GTA5_NG_build_version_history&action=edit')
	webbrowser.open_new_tab('https://devstar.rockstargames.com/wiki/index.php?title=v'+ver_no+'_NG&action=edit')

if build_ver != 'NG':
	webbrowser.open_new_tab('https://devstar.rockstargames.com/wiki/index.php?title=GTA5_build_version_history&action=edit')
	webbrowser.open_new_tab('https://devstar.rockstargames.com/wiki/index.php?title=v'+ver_no+'_TU&action=edit')

os.system( 'details.txt' )
os.system( 'summary.txt' )


# EOF