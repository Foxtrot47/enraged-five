::************************************************
:: GTA5 Label Prebuild batch file               **
:: Updated: 				30/08/2012			**
:: Last edited by:			Ross McKinstray		**
:: Last change:			Added labelling of xlast **
::************************************************

@echo off
echo

PUSHD %RS_PROJROOT%

echo.
echo UPDATING PREBUILD LABEL
echo.


P4 labelsync -l GTA5_NewPreBuildCode //depot/gta5/src/dev/...
P4 labelsync -l GTA5_NewPreBuildCode //rage/gta5/dev/...
P4 labelsync -l GTA5_NewPreBuildCode //depot/gta5/xlast/...
POPD



pause