@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% PreBuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% PreBuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_PREBUILD_LABEL_NAME%  2> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/ps3/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/TROPDIR/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/game_psn_beta_snc.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/game_psn_bankrelease_snc.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/game_psn_release_snc.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/*.sfo@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
p4 sync  %PERFORCE_ROOT%/build/dev/*.png@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
	exit /B
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/ps3_prebuild_sync.txt
	pause
	exit
)

exit