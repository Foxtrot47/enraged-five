@echo off
for /f "tokens=1-3 delims=/ " %%i in ("%date%") do (
     set day=%%i
     set month=%%j
     set year=%%k
)
set datestr=%year%/%month%/%day%:07:30:00
echo.Syncing to date and time %datestr%
p4 sync //gta5_dlc/mpPacks/mpArc1/build/...@%datestr%
p4 sync //gta5_dlc/mpPacks/mpArc1/*.*@%datestr%
p4 sync //gta5_dlc/mpPacks/mpHeist3/build/...@%datestr%
p4 sync //gta5_dlc/mpPacks/mpHeist3/*.*@%datestr%
pause