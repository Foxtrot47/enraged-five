@echo off
call setenv.bat

set BRANCH=rdr3_dev_to_rdr3_dev_migrate
set OUTPUT_PATH=%RS_TOOLSROOT%\tmp\%BRANCH%\

%RS_TOOLSBIN%\IntegrationReporter.exe -cc -branch %BRANCH% -destination %RS_PROJROOT% -xslpath %RS_TOOLSCONFIG%\IntegrationReporter -intermediatefile %OUTPUT_PATH%\integration_data.xml -outputpath %OUTPUT_PATH% -norevert