#===================================================
# cutscene_concatBatch.py
# Author: Mike Wilson <mike.wilson@rockstartoronto.com>
# Batch build concat scenes
# example: cutscene_concatBatch.py -file "file.txt"
#===================================================   

import os
import sys
import string

PROJECT = os.environ['RS_PROJECT']
PROJECTROOT = os.environ['RS_PROJROOT']
TOOLSROOT = os.environ['RS_TOOLSROOT']

batchCutListFile = ""
cutListFile = ""
logFile = ""
statusLogFile = open(TOOLSROOT + "\\logs\\cutscene\\concatBatch.txt", "w")

for i in range(0, len(sys.argv)):
    if "-file" in sys.argv[i]:
        try:
            batchCutListFile = sys.argv[i+1]
            
        except :
            statusLogFile.write("ERROR: Paramater error")
            sys.exit()

# read the file, it should be a list of cutlist files
batchCutListFileStream = open(batchCutListFile, "r")
for fileLine in batchCutListFileStream.readlines():
    cutListFile = string.rstrip(fileLine)
    
    # if the line is not blank
    if cutListFile != "":
	
	sceneName = os.path.splitext(os.path.basename(cutListFile))[0]
	scenePath = PROJECTROOT + "\\assets\\cuts\\" + sceneName

	# create log
	logFile = TOOLSROOT + "\\logs\\cutscene\\" + sceneName + "_concat_batch.ulog"

	# run animconcatdriver, build concat
	animConcatDriverPath = TOOLSROOT + "\\bin\\anim\\animconcatdriver.exe " + cutListFile + " " + logFile
	returnCode = os.system(animConcatDriverPath)
	if returnCode != 0:
		statusLogFile.write("FAILED: " + scenePath + "\n")
		statusLogFile.write("\t" + animConcatDriverPath + "\n")
		statusLogFile.write("LOG: " + logFile + "\n")
		continue

	# checkout or add cutxml/obj folder
	os.system("p4 add " + (scenePath + "\data.cutxml"))
	os.system("p4 edit " + (scenePath + "\data.cutxml"))
	os.system("p4 edit " + (scenePath + "\data_stream.cutxml"))
	os.system("p4 edit " + (scenePath + "\obj\..."))

	# checkout or add .cutxml and cutbin files
	os.system("p4 add " + (scenePath + ".cutxml"))
	os.system("p4 add " + (scenePath + ".cutbin"))
	os.system("p4 edit " + (scenePath + ".cutxml"))
	os.system("p4 edit " + (scenePath + ".cutbin"))

	# run cutscene.rb, build obj folder
	cutsceneRubyPath = TOOLSROOT + "\\lib\\util\\motionbuilder\\cutscene.rb " + '--build --inputDir=' + scenePath + '  --sectionMethod="2"  --sceneName=' + sceneName + ' --ulog=' + logFile
	returnCode = os.system(cutsceneRubyPath)
	if returnCode != 0:
		# revert and force sync if the build fails, leaves the obj in a valid state
		os.system("p4 revert -f " + (scenePath + "\obj\..."))
		os.system("p4 sync -f " + (scenePath + "\obj\..."))
		statusLogFile.write("FAILED: " + scenePath + "\n")
		statusLogFile.write("\t" + cutsceneRubyPath + "\n")
		statusLogFile.write("LOG: " + logFile + "\n")
		continue

	# add obj folder - pick up any new files
	os.system("p4 add " + (scenePath + "\obj\..."))

	# pack the obj up into icd
	packFilePath = TOOLSROOT + "\\lib\\pipeline\\resourcing\\gateways\\pack_file.rb " + "--project=" + PROJECT + " --rootsrcdir=" + scenePath + " --animsrcdir=" + scenePath + "\\obj" + " --scenename=" + sceneName + " --destdir=x:\gta5\art\anim\export_mb_compressed --outputdir=cuts/ --sectioned"
	returnCode = os.system(packFilePath)

	if returnCode == 0:
		statusLogFile.write("SUCCESS: " + scenePath + "\n")
		
statusLogFile.close

# build cuts.rpf
imageFilePath = TOOLSROOT + "\\lib\\pipeline\\resourcing\\gateways\\image_file.rb " + "--project=" + PROJECT + " --srcdir=cuts --imagename=cuts --exportsrcdir=" + PROJECTROOT + "\\assets\\cuts" + " --cutscene"
#returnCode = os.system(imageFilePath)