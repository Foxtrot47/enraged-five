@ECHO OFF
REM 
REM Test Farm Generation
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

CALL setenv.bat
TITLE Test Farm Generation %RS_PROJECT% 

echo.
echo https://devstar.rockstargames.com/wiki/index.php/Test_Farm
echo.

PUSHD %RS_TOOLSLIB%\util
ruby test_farm_gen.rb %RS_TOOLSCONFIG%/testfarm/test_farm.xml
POPD

echo.
echo You will then find a number of freshly generated batch files in N:\RSGEDI\Builders\testfarm 
echo.
echo The batch files are of 4 varieties; 
echo.
echo * everything_retry n.bat : retries the everything capture
echo * everything_ n.bat : simply does the everything capture 
echo * maponly_retry n.bat : retries the maponly capture 
echo * maponly_ n.bat : simply does the maponly capture 
echo. 
echo. NB. maponly excludes water regions. 
echo.
echo *** Now you can notify farm clients which batch file they should run. ***
echo.
echo.
echo eg connect to devkit in targetmanager then run maponly_retry1.bat
echo * normally you wouldn't run the likes of maponly1.bat, this doesn't auto retry.
echo.

pause

REM TestFarm.bat
