@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

::-- Batch file title bar
TITLE Getting GTA5_NG_Current label for PS4...
ECHO Getting GTA5_NG_Current label for PS4...

REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	::-- Set studio sync -f paths
	::for /f %%i in ('%RS_TOOLSIRONLIB%\bin\RSG.Pipeline.WhatIsMyStudio.exe') do set studio=%%i
	::echo Studio is %studio%

	:: Old
		::for /f "tokens=*" %%a in ( 
		::'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
		::) do ( 
		::set studio=%%a
		::) 
		::echo/%%studio%%=%studio% 

	::if %studio%==northbarclay set networkbuildpath="N:\RSGEDI\Build Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==toronto set networkbuildpath="N:\RSGTOR\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==sandiego set networkbuildpath="N:\RSGSAN\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==newengland set networkbuildpath="N:\RSGNWE\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==leeds set networkbuildpath="N:\RSGLDS\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==london set networkbuildpath="N:\RSGLDN\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==newyork set networkbuildpath="N:\RSGNYC\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==glencove set networkbuildpath="N:\RSGLIC\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%=="" (
	::	echo Error: No studio set!!
	::	pause
	::	exit
	::)

	::echo NETWORK BUILD PATH SET: %networkbuildpath%
	::--------

::--- extra setup
set localbuildpath=X:\gta5\build\dev_ng
set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	:: - sync -f local network stored platform data -
	:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
	:: ROBOCOPY info. /S copies non-empty subfolders, /DCOPY:T keeps source timestamps
	::robocopy /S /DCOPY:T %networkbuildpath%\ps4\ %localbuildpath%\ps4\ *.* 2> %RS_TOOLSROOT%/logs/ps4_sync.txt

::--- Get non-platform data from p4 still ---
p4 sync -f //depot/gta5/build/dev_ng/common/...@GTA5_NG_Current 2> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/build/dev_ng/sce_companion_httpd/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/build/dev_ng/sce_sys/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/build/dev_ng/sce_module/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
p4 sync -f //depot/gta5/build/dev_ng/ps4/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/build/dev_ng/game_orbis_*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/assets_ng/export/levels/gta5/....xml@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



:: Grab NG TU

::TitleUpdate - DEV_NG
p4 sync -f //depot/gta5/titleupdate/dev_ng/common/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/dlc_patch/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/ps4/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/sce_sys/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/*.xml@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/*.prx@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/game_orbis_*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //depot/gta5/titleupdate/dev_ng/_launchgame.bat@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



if "%BUILD_MACHINE%"=="1" (
	goto:END
)


::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps4_sync.txt
)




)
::---------------------------------------------------------------

pause

:END
