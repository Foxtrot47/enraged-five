@ECHO OFF
REM 
REM Starts the AssetBuilder SHELL - a remote shell into the server - not the actual asset builder.
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

CALL setenv.bat
TITLE %RS_PROJECT% Asset Builder Remote Shell - dev
SET ASSETBUILDER=\util\AssetBuilder

PUSHD %RS_TOOLSLIB%\%ASSETBUILDER%
%RS_TOOLSRUBY% AssetBuilder_shell.rb --server=%1

ECHO Asset builder shell has quit.
PAUSE

REM AssetBuildShell.bat
