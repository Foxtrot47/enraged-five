@echo off
setlocal enableextensions

rem -------------------------------------------------------------
rem -- This file is not intended to be called directly,        --
rem -- it is for internal use by the runtime exception handler --
rem -------------------------------------------------------------



set srcfile=%1
set dstdir=%2

echo Copying Durango coredump "%srcfile%" to host PC "%dstdir%"
echo.

rem -- Check that the Durango XDK has been correctly setup
if "%DurangoXDK%" equ "" (
	echo DurangoXDK environment variable not set
	goto Error
)
echo Durango XDK check OK

rem -- Make sure the directory for destiation exists
rem -- This relies on cmd extensions being enabled
echo Creating destination directory "%dstdir%"
(cd "%dstdir%" || md "%dstdir%") >nul 2>&1
if errorlevel 1 (
	echo Unable to create destination directory "%dstdir%"
	goto Error
)

rem -- Copy the file
"%DurangoXDK%bin\xbcp" /x/title "%srcfile%" "%dstdir%"
if errorlevel 1 (
	echo Copy failed
	goto Error
)

rem -- Copy has succeeded, so safe to delete file off console
"%DurangoXDK%bin\xbdel" /x/title "%srcfile%"
exit 0


:Error
pause
exit 1
