@ECHO OFF
REM :: Author: David.Muir@rockstarnorth.com


CALL setenv >NUL

SET ERROR_FILENAME=x:\sync_errors.txt 
SET ERROR=0

PUSHD %RS_PROJROOT%

SETLOCAL
ECHO Compiling changelists...
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\cutscene_changes.rb --src_dir %RS_PROJROOT%\assets\cuts --out_file %RS_PROJROOT%\build_changelists\changes_%TIME:~0,2%_%TIME:~3,2%_%TIME:~6,2%.txt
ENDLOCAL

P4 sync -f %RS_PROJROOT%/assets/cuts/... 2> %ERROR_FILENAME%

IF %ERRORLEVEL% NEQ 0 SET ERROR=1

ECHO Rebuilding all cutscenes...
%RS_TOOLSRUBY% %RS_TOOLSLIB%/pipeline/resourcing/gateways/image_file.rb --project=%RS_PROJECT% --srcdir=cuts --imagename=cuts --exportsrcdir=%RS_PROJROOT%\assets\cuts --rebuild --cutscene

IF %ERROR% NEQ 0 %ERROR_FILENAME%

PAUSE