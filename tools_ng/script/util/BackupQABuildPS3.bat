@ECHO OFF

:: v1.0
:: Batch process to move backup version to the server 

:: v1.1
:: Added individual xex paths so it doesn't transfer non required files to save time

ECHO Copy GTAV platform Data and Update Current Label


TITLE %RS_PROJECT% dev: Moving labelled build for ps4 to Server for Backup...


ECHO N:\RSGLDN\Build_Data\Latest\gta5\build\dev\common\data\version.txt

:buildverify
:: Parsing the studio name for the network and xex paths
for /f "tokens=*" %%a in ( 
'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
) do ( 
set studio=%%a
) 
if %studio%==north set networkbuildpath=N:RSGEDI\Projects\GTAV\Builds\Storage
if %studio%==toronto set networkbuildpath="N:\RSGTOR\Projects\GTAV\Builds\Storage
if %studio%==sandiego set networkbuildpath=N:\RSGSAN\Projects\GTAV\Builds\Storage
if %studio%==newengland set networkbuildpath=N:\RSGNWE\Projects\GTAV\Builds\Storage
if %studio%==leeds set networkbuildpath=N:\RSGLDS\Projects\GTAV\Builds\Storage
if %studio%==london set networkbuildpath=N:\RSGLDN\Projects\GTAV\Builds\Storage
if %studio%==newyork set networkbuildpath=N:\RSGNYC\Projects\GTAV\Builds\Storage
:NETWORK BUILD PATH SET: %networkbuildpath%

setlocal ENABLEDELAYEDEXPANSION
::Parsing the version.txt for new version number
set vidx=0
for /F "tokens=*" %%A in (X:\gta5\build\dev\common\data\version.txt) do (
    SET /A vidx=!vidx! + 1
    set var!vidx!=%%A
)
set var

::Commandline
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ commandline.txt

::PARAM.sfo
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ PARAM.sfo

::Bank Release
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_bankrelease_SNC.bat
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_bankrelease_SNC.cmp
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_bankrelease_SNC.idb
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_bankrelease_SNC.self
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_bankrelease_SNC.sym

::Beta
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_beta_SNC.bat
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_beta_SNC.cmp
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_beta_SNC.idb
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_beta_SNC.self
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_beta_SNC.sym

::Release
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_release_SNC.bat
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_release_SNC.cmp
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_release_SNC.idb
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_release_SNC.self
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_PS3\ game_psn_release_SNC.sym

::PS3 Data
robocopy /S /DCOPY:T X:\gta5\build\dev\ps3     %networkbuildpath%\QAversion%var3%_PS3\ps3 

::PS3 Common
robocopy /S /DCOPY:T X:\gta5\build\dev\common\ %networkbuildpath%\QAversion%var3%_PS3\common\



pause