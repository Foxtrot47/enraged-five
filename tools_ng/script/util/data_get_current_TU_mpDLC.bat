@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% GTA5: Getting Current mpPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting Current mpPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::
p4 sync -f //gta5_dlc/mpPacks/mpBeach/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpChristmas/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpValentines/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpBusiness/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpBusiness2/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpHipster/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpIndependence/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpPilot/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLTS/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpHeist/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpArmy/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpChristmas2/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLuxe/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLuxe2/build/dev/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::==========================================::


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause


