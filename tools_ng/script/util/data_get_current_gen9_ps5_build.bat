@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

::-- Batch file title bar
TITLE Getting GTA5_GEN9_Current label for PS5...
ECHO Getting GTA5_GEN9_Current label for PS5...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


::--- Grab GEN9 Base Build ---
p4 sync //depot/gta5/build/dev_gen9_sga/common/...@GTA5_GEN9_Current 2> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_gen9_sga/ps5/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::--- Grab GEN9 TU Build ---
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/common/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/dlc_patch/*/common/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/dlc_patch/*/ps5/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/dlc_patch/*/*.xml...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/launchers/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/ps5/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/ps5_config/...@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/*.xml@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/*.txt@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga/game_prospero_*.*@GTA5_GEN9_Current 2>> %RS_TOOLSROOT%/logs/ps5_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



if "%BUILD_MACHINE%"=="1" (
	goto:END
)


::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	start notepad %RS_TOOLSROOT%/logs/ps5_sync.txt
)

::---------------------------------------------------------------
if %SKIP_PAUSE% NEQ 1 ( pause )

:END
