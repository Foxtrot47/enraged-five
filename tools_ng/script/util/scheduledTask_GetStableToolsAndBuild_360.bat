@ECHO OFF
ECHO %RS_PROJECT% : Running scheduled task...

rem Get current labelled tools...
CALL setenv.bat >NUL
CALL data_get_project_info.bat >NUL

TITLE %RS_PROJECT% : Syncing current labelled tools...
PUSHD %RS_PROJROOT%

ECHO Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

CALL %RS_TOOLSROOT%\script\util\check_p4_login.bat
p4 sync %PERFORCE_ROOT%/tools/...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME%

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
)

rem Get 360 build...
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/xbox360/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_xenon_bankrelease.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_xenon_beta.*@%PERFORCE_CURRENT_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/360_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
)

p4 sync %PERFORCE_ROOT%/art/textures/...
p4 sync %PERFORCE_ROOT%/art/models/props/...

pause