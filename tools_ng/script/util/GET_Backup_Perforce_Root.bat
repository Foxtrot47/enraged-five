@echo off

setlocal

title Copying Backup perforce root to Local Machine

:: v1.0
:: pass in the build version number when prompted e.g 325, 326
:: and then the sku e.g 360, ps3, pc.

:: v1.1
:: Adds if we want the mp exes and cmd line

:: v1.2
:: Fixed exception bug if sku version was not on the sever but proceeded anyway
     
:: v1.3
:: goto mpgrab and install were the wrong way round if no mp xexs were on the server

:: v1.4
:: Edits to make a global batch file for all studios to use from central location

:: v1.5
:: Added archived folder, didnt need to see every single version

:: v1.6
:: Fixed archived folder bug

set version=1.6

:buildverify
:: Parsing the studio name for the network and xex paths
for /f "tokens=*" %%a in ( 
'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
) do ( 
set studio=%%a
) 
if %studio%==north set networkbuildpath=N:RSGEDI\Projects\GTAV\Builds\Storage
if %studio%==toronto set networkbuildpath="N:\RSGTOR\Projects\GTAV\Builds\Storage
if %studio%==sandiego set networkbuildpath=N:\RSGSAN\Projects\GTAV\Builds\Storage
if %studio%==newengland set networkbuildpath=N:\RSGNWE\Projects\GTAV\Builds\Storage
if %studio%==leeds set networkbuildpath=N:\RSGLDS\Projects\GTAV\Builds\Storage
if %studio%==london set networkbuildpath=N:\RSGLDN\Projects\GTAV\Builds\Storage
if %studio%==newyork set networkbuildpath=N:\RSGNYC\Projects\GTAV\Builds\Storage
:NETWORK BUILD PATH SET: %networkbuildpath%

:MpXEXVerify
if %studio%==north set mpxexpath=N:\RSGEDI\Projects\GTAV\Builds\XEXs
if %studio%==toronto set mpxexpath=N:\RSGTOR\Projects\GTAV\Builds\XEXs
if %studio%==sandiego set mpxexpath=N:\RSGSAN\PProjects\GTAV\Builds\XEXs
if %studio%==newengland set mpxexpath=N:\RSGNWE\Projects\GTAV\Builds\XEXs
if %studio%==leeds set mpxexpath=N:\RSGLDS\Projects\GTAV\Builds\XEXs
if %studio%==london set mpxexpath=N:\RSGLDN\Projects\GTAV\Builds\XEXs
if %studio%==newyork set mpxexpath=N:\RSGNYC\Projects\GTAV\Builds\XEXs
:MP XEX PATH SET: %mpxexpath%

:ARCHPath
if %studio%==north set ARCHPath=N:\RSGEDI\Projects\GTAV\Builds\Storage\Archived_Versions
if %studio%==toronto set ARCHPath=N:\RSGTOR\Projects\GTAV\Builds\Storage\Archived_Versions
if %studio%==sandiego set ARCHPath=N:\RSGSAN\PProjects\GTAV\Builds\Storage\Archived_Versions
if %studio%==newengland set ARCHPath=N:\RSGNWE\Projects\GTAV\Builds\Storage\Archived_Versions
if %studio%==leeds set ARCHPath=N:\RSGLDS\Projects\GTAV\Builds\Storage\Archived_Versions
if %studio%==london set ARCHPath=N:\RSGLDN\Projects\GTAV\Builds\Storage\Archived_Versions
if %studio%==newyork set ARCHPath=N:\RSGNYC\Projects\GTAV\Builds\Storage\Archived_Versions
:MP XEX PATH SET: %ARCHPath%

::Title                                                                                     
echo.
echo _____/\\\\\\\\\\\\__/\\\\\\\\\\\\\\\_____/\\\\\\\\\______       
echo  ___/\\\//////////__\///////\\\/////____/\\\\\\\\\\\\\____     
echo  R__/\\\___________________\/\\\________/\\\/////////\\\____    
echo   S_\/\\\____/\\\\\\\_______\/\\\_______\/\\\_______\/\\\____    
echo    G_\/\\\___\/////\\\_______\/\\\_______\/\\\\\\\\\\\\\\\_____   
echo     L_\/\\\_______\/\\\_______\/\\\_______\/\\\/////////\\\_____   
echo      D_\/\\\_______\/\\\_______\/\\\_______\/\\\_______\/\\\_____ 
echo       N_\//\\\\\\\\\\\\/________\/\\\_______\/\\\_______\/\\\_____
echo         __\////////////__________\///________\///________\///_Revert v%version%
echo	         \___________________ !! Warning !! _________________/			                   
echo.
echo	                    -  You are about to overwrite dev -
echo 		   - Are you sure you want to do this? -

echo.
echo                     !!  ALL VERSION ARE UNDONGLED  !!	  
echo                     !!     FOR STUDIO USE ONLY     !!	    
	       
:version 
:: Asking user for the build version
echo.
set BUILDVERSION=%1
if [%BUILDVERSION%]==[] set /p BUILDVERSION=   Please enter the version number you want to INSTALL (e.g 325, 326): 

:sku
:: Asking user for the sku they require
echo.
set BUILDSKU=%1
if [%BUILDSKU%]==[] set /p BUILDSKU=   Please enter the specific SKU you want (e.g xbox360, ps3, x64): 

:: Do we have the required build and sku saved on the server
echo.
if exist "%networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%" goto mpinstalled
echo                ------------------------------------------
echo                !! Package does not exist on the server !!
echo                ------------------------------------------
echo                      The only versions we have are:
echo.

FOR /f "tokens=*" %%i in ('DIR %networkbuildpath% /a:d /b ') DO (
    ECHO    %%i
)

	goto ARCHversion

:mpinstalled
:: Do we have the required multiplayer xexs on the server
set MPEXE=%1
if [%MPEXE%]==[] set /p MPEXE=   Do you want Multiplayer exes and commandline? (e.g y or n): 
if [%MPEXE%]==[n] goto install

echo.
if exist "%mpxexpath%\v%BUILDVERSION%" goto mpgrab
echo                ------------------------------------------
echo                !!  MP Exes do not exist on the server  !!
echo                ------------------------------------------
echo            These might not have been copied over yet - see Ian.
echo. 		 
echo          	        The only exes we have are:

FOR /f "tokens=*" %%i in ('DIR %mpxexpath% /a:d /b ') DO (
    ECHO %%i
)
echo.
	goto sorry

:Archived
:: Asking user if they want to see archived versions
echo.
set ARCHrequest=%1
if [%ARCHrequest%]==[] set /p ARCHrequest=   Do you want to paruse archived versions? (e.g y or n): 
if [%ARCHrequest%]==[n] goto end
echo.
echo                ------------------------------------------
echo                !!          Archived Versions           !!
echo                ------------------------------------------
echo.
FOR /f "tokens=*" %%i in ('DIR %ARCHPath% /a:d /b ') DO (
    ECHO %%i
)
echo.

:ARCHversion
:: Asking user for the archived version they require
echo.
set ARCHVERSION=%1
if [%ARCHVERSION%]==[] set /p ARCHVERSION=   Please enter the archived version number you want to INSTALL (e.g 325, 326): 

:sku
:: Asking user for the sku they require
echo.
set ARCHBUILDSKU=%1
if [%ARCHBUILDSKU%]==[] set /p ARCHBUILDSKU=   Please enter the specific SKU you want (e.g xbox360, ps3, x64): 

if exist "%ARCHPath%\QAversion%ARCHVERSION%_%ARCHBUILDSKU%" goto ARCHinstall

echo                ------------------------------------------
echo                !! Package does not exist on the server !!
echo                ------------------------------------------
echo                          We only have:
FOR /f "tokens=*" %%i in ('DIR %ARCHPath% /a:d /b ') DO (
    ECHO %%i
)
echo.

	goto end

:install
:: Copying the version plus sku WITHOUT multiplayer xexs
title Copying QAversion%BUILDVERSION%_%BUILDSKU% Perforce Root without MP Executables
	robocopy     /DCOPY:T  %networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%\                X:\gta5\build\dev\

title Copying QAversion%BUILDVERSION%_%BUILDSKU% Data
	robocopy  /S /DCOPY:T  %networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%\%BUILDSKU%\     X:\gta5\build\dev\%BUILDSKU%\

title Copying QAversion%BUILDVERSION%_%BUILDSKU% Common
	robocopy  /S /DCOPY:T  %networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%\common\         X:\gta5\build\dev\common\

	goto done


:mpgrab
:: Copying the version plus sku WITH multiplayer xexs
title Copying QAversion%BUILDVERSION%_%BUILDSKU% Perforce Root with MP Executables
	robocopy      /DCOPY:T %mpxexpath%\v%BUILDVERSION%                                            X:\gta5\build\dev\

title Copying QAversion%BUILDVERSION%_%BUILDSKU% Data
	robocopy  /S /DCOPY:T  %networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%\%BUILDSKU%\      X:\gta5\build\dev\%BUILDSKU%\

title Copying QAversion%BUILDVERSION%_%BUILDSKU% Common
	robocopy  /S /DCOPY:T  %networkbuildpath%\QAversion%BUILDVERSION%_%BUILDSKU%\common\          X:\gta5\build\dev\common\

	goto done

:ARCHinstall
:: Copying the archived version plus sku WITHOUT multiplayer xexs
title Copying QAversion%ARCHVERSION%_%ARCHBUILDSKU% Package without MP Executables
	robocopy    /DCOPY:T  %ARCHPath%\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\                 X:\gta5\build\QAPackages\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\

title Copying QAversion%ARCHVERSION%_%ARCHBUILDSKU% Data
	robocopy /S /DCOPY:T  %ARCHPath%\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\%ARCHBUILDSKU%\      X:\gta5\build\QAPackages\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\%ARCHBUILDSKU%\

title Copying QAversion%ARCHVERSION%_%ARCHBUILDSKU% Common
	robocopy /S /DCOPY:T  %ARCHPath%\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\common\          X:\gta5\build\QAPackages\QAversion%ARCHVERSION%_%ARCHBUILDSKU%\common\

	goto done	
	
:sorry
::Bailing out
echo.
echo                ----------------------------------------
echo                *     Sorry We don't have these yet    *
echo                ----------------------------------------

	goto end

:done
:: Completion message for X transfer
echo.
echo                --------------------------------------------------------------------
echo                *                 %BUILDVERSION%_%BUILDSKU% Transfer    Successful!              *
echo                --------------------------------------------------------------------

:open
:: Opening the transferred folder after copy has been completed
echo                --------------------------------------------------------------------
echo                *                   Continue Opening Folder Root                   *
echo                --------------------------------------------------------------------

%SystemRoot%\explorer.exe "X:\gta5\build\dev\"

	goto end
:end
pause



