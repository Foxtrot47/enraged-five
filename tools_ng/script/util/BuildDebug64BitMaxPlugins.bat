@ECHO OFF
REM 
REM Builds Debug 64 bit max plugins.
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

call %RS_TOOLSROOT%\install.bat /switch

CALL setenv.bat
SET MAX_VERSION=2011
TITLE %RS_PROJECT% Build Max %MAX_VERSION% Debug Plugins
SET RAGE_DIR=%RS_PROJROOT%\src\dev\rage

PUSHD %RS_TOOLSLIB%\util
ruby build_max_plugins.rb --project=%RS_PROJECT% --branch=dev --buildconfig="ToolDebugMax2011|x64" --buildfolder="debug" --current_local_path=current/max2011/Plugins/x64/ --dest_local_path=debug/max2011/Plugins/x64/ %RAGE_DIR%\base\tools\dcc\max\rexMax\rexMax_2008.sln %RS_TOOLSSRC%\dcc\max\3dsmax_Plugins_2008.sln
SET ERR=%ERRORLEVEL%
echo Errorlevel is %ERR%
POPD
EXIT %ERR%
REM BuildMaxPlugins.bat

