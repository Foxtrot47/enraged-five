@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA_NG_PreBuild label for PS4...
ECHO Getting GTA_NG_PreBuild label for PS4...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab main build.
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_PreBuild 2> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_companion_httpd/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_module/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

:: Grab NG TU

::TitleUpdate - DEV_NG
p4 sync //depot/gta5/titleupdate/dev_ng/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.prx@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps4_prebuild_sync.txt
)


)
::---------------------------------------------------------------

pause

:END
