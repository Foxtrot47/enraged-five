@ECHO OFF

:: v1.0
:: Batch process to move backup version to the server 

:: v1.1
:: Added individual xex paths so it doesn't transfer non required files to save time

ECHO Copy GTAV platform Data and Update Current Label


TITLE %RS_PROJECT% dev: Moving labelled build for x64 to Server for Backup...


ECHO N:\RSGLDN\Build_Data\Latest\gta5\build\dev\common\data\version.txt

:buildverify
:: Parsing the studio name for the network and xex paths
for /f "tokens=*" %%a in ( 
'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
) do ( 
set studio=%%a
) 
if %studio%==north set networkbuildpath=N:RSGEDI\Projects\GTAV\Builds\Storage
if %studio%==toronto set networkbuildpath="N:\RSGTOR\Projects\GTAV\Builds\Storage
if %studio%==sandiego set networkbuildpath=N:\RSGSAN\Projects\GTAV\Builds\Storage
if %studio%==newengland set networkbuildpath=N:\RSGNWE\Projects\GTAV\Builds\Storage
if %studio%==leeds set networkbuildpath=N:\RSGLDS\Projects\GTAV\Builds\Storage
if %studio%==london set networkbuildpath=N:\RSGLDN\Projects\GTAV\Builds\Storage
if %studio%==newyork set networkbuildpath=N:\RSGNYC\Projects\GTAV\Builds\Storage
:NETWORK BUILD PATH SET: %networkbuildpath%

setlocal ENABLEDELAYEDEXPANSION
::Parsing the version.txt for new version number
set vidx=0
for /F "tokens=*" %%A in (X:\gta5\build\dev\common\data\version.txt) do (
    SET /A vidx=!vidx! + 1
    set var!vidx!=%%A
)
set var

::Commandline
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ commandline.txt

::Bank Release
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.bat
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.cmp
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.exe
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.idb
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.map
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.pdb

::Beta
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.bat
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.cmp
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.exe
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.idb
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.map
robocopy /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_x64\ game_win64_bankrelease.pdb

::x64 Data
robocopy /S /DCOPY:T X:\gta5\build\dev\x64     %networkbuildpath%\QAversion%var3%_x64\x64

::x64 Common
robocopy /S /DCOPY:T X:\gta5\build\dev\common\ %networkbuildpath%\QAversion%var3%_x64\common\



pause