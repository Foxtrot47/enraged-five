@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA5_NG_AUTOMATION_SCRIPTBUILDER_SUCCESS label for scripts...
ECHO Getting GTA5_NG_AUTOMATION_SCRIPTBUILDER_SUCCESS label for scripts...

set syncerror=0

p4 sync %PERFORCE_ROOT%/script/dev_ng/...@GTA5_NG_AUTOMATION_SCRIPTBUILDER_SUCCESS
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
)


)
::---------------------------------------------------------------

pause

:END