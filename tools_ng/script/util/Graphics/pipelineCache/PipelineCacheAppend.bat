@ECHO OFF
echo %*

REM	RS_BUILDBRANCH=	x:\gta5\build\dev_gen9_sga_live
REM					X:\gta5\titleupdate\dev_gen9_sga\common\shaders
REM CALL set ShaderPath=%%RS_BUILDBRANCH:%RS_PROJECT%=%RS_PROJECT%\titleupdate%%
CALL set ShaderPath=%RS_BUILDBRANCH:build=titleupdate%

if [%1]==[] goto :eof
:loop

REM Set filename=%~1
REM For %%A in ("%filename%") do (
REM    Set Name=%%~nxA
REM )
REM For loop caused issues w/ certain paths and didn't seem necessary? Getting name directly from the param now.
Set Name=%~nx1
set str1=%Name%

if not x%str1:WarmupCache=%==x%str1% (
	echo.Appending %Name% to warmup cache
	if exist %ShaderPath%\common\shaders\%Name% (
		%RS_TOOLSROOT%\bin\shadertools_gen9\mergepipelinecaches.exe %ShaderPath%\common\shaders\%Name% %1   %ShaderPath%\common\shaders\%Name%
	) else (
		%RS_TOOLSROOT%\bin\shadertools_gen9\mergepipelinecaches.exe %ShaderPath%\common\shaders\%Name% %1  
	)
)

shift
if not [%1]==[] goto loop
