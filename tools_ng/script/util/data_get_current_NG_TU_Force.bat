@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...
ECHO %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

:: Grab NG TitleUpdate
p4 sync -f //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_Current 2> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

REM 25/11/2015 - Ross McK - comment out dev_Temp sync
::p4 sync //depot/gta5/titleupdate/dev_Temp/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/NG_TU_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause

:END
