@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA5_NG_PreBuild label for X64 PC...
ECHO Getting GTA5_NG_PreBuild label for X64 PC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab main build
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_Prebuild 2> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/x64/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_win64_beta*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_win64_bankrelease.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_win64_release.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/*.dll@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

:: Grab NG TU
::p4 sync //depot/gta5/titleupdate/dev_Temp/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/x64/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/*.dll@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/*.txt@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/game_win64_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)

:: Grab NG
p4 sync //depot/gta5/titleupdate/dev_ng/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/x64/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.dll@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.txt@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/game_win64_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



:: Grab DLC compat packs

::mpChristmas3
if exist x:\gta5_dlc\mpPacks\mpChristmas3 (
p4 sync //gta5_dlc/mpPacks/mpChristmas3/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpChristmas3 (
p4 sync //gta5_dlc/mpPacks/mpChristmas3/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)



::patchDay28NG
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay28NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay28NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/x64_pc_prebuild_sync.txt
)

)
::---------------------------------------------------------------

pause

:END
exit