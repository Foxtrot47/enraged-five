@ECHO OFF
REM 
REM Machine Analysis
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

CALL setenv.bat
TITLE Machine Analysis %RS_PROJECT% 

PUSHD %RS_TOOLSLIB%\util
ruby machine_analysis.rb --project=gta5 --branch=dev
POPD

pause

REM MachineAnalysis.bat
