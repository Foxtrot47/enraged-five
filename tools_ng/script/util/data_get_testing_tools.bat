@ECHO OFF
CALL setenv.bat >NUL
CALL data_get_project_info.bat >NUL

SET TESTING_LABEL=%PERFORCE_TESTING_TOOLS_LABEL_NAME%

TITLE %RS_PROJECT% : Syncing %TESTING_LABEL%...
PUSHD %RS_PROJROOT%

ECHO Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/tools/...@%TESTING_LABEL%
p4 sync %PERFORCE_ROOT%/assets/metadata/definitions/...@%TESTING_LABEL%

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	pause
)

