@echo off

SET PROPS_EXE=%~dp0..\..\..\bin\PropertySheetViewer.exe
if not defined RS_TOOLSCONFIG (
	@echo.
	if NOT EXIST "%PROPS_EXE%" echo missing "%PROPS_EXE%" && exit /b 1
	"%PROPS_EXE%" showenvironment
	for /F "tokens=*" %%i in ('%PROPS_EXE% dumpenvironment') do set %%i
	@echo.
)

%RS_TOOLSBIN%\ProjectGeneratorGen9\ProjectGenerator.exe --nopopups %*