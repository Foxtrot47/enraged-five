REM
REM Builds a library as used by the game tools.
REM

@echo off
@echo.
@echo ========================================================================= 
@echo ===================== PROJECT GENERATOR 3 STARTED ======================= 


SET PROPS_EXE=%~dp0..\..\..\bin\PropertySheetViewer.exe
if not defined RS_TOOLSCONFIG (
	@echo.
	if NOT EXIST "%PROPS_EXE%" echo missing "%PROPS_EXE%" && exit /b 1
	"%PROPS_EXE%" showenvironment
	for /F "tokens=*" %%i in ('%PROPS_EXE% dumpenvironment') do set %%i
	@echo.
)

call %RS_TOOLSROOT%\script\util\projGen9\sync.bat

@echo.
call %RS_TOOLSROOT%\script\util\projGen9\generate.bat %RS_TOOLSCONFIG%\projgen9\gameTools.build %*
@echo.

@echo ===================== PROJECT GENERATOR 3 COMPLETE ======================
@echo =========================================================================
@echo.