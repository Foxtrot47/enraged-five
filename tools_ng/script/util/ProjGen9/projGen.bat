@echo off
SETLOCAL
@echo.
@echo =============================================================================
@echo === PG3 ProjGen : Recursive Project Generation (IronRuby)                 === 
@echo === https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 ===
@echo.

REM sync to latest project generator config and binaries - only during development, do not remove I'll change sync.bat when no longer reqd.
call %RS_TOOLSROOT%\script\util\projGen9\sync.bat

SET PROPS_EXE=%~dp0..\..\..\bin\PropertySheetViewer.exe
if NOT EXIST "%PROPS_EXE%" echo missing "%PROPS_EXE%" && exit /b 1
"%PROPS_EXE%" showenvironment
for /F "tokens=*" %%i in ('%PROPS_EXE% dumpenvironment') do set %%i

REM -----------------------------------------------------------------------------------------------------
REM Call an ironruby script that recursively scans for projgen.config files and all recognised makefiles
REM - it then produces a series of local *.makefiles files. 
REM - each file contains the correct build directive to build the makefiles with the appropriate settings
REM -----------------------------------------------------------------------------------------------------

call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%/ironlib/util/projGen9/findMakefiles.rb --exclude \/game\/

REM -----------------------------------------------------------------------------------------------------
REM for each *.makefiles file convert with the project generator
REM - DW: TODO project generator should be able to handle multiple 'makefile' files?!
REM -----------------------------------------------------------------------------------------------------

for %%f in (*.makefiles) do (
	title projGen Converting %%f 
	echo.
	echo --- projGen Converting %%f --- 
	echo.
	echo     %RS_TOOLSROOT%\bin\projectgeneratorGen9\projectgenerator.exe --changelist --verify %%f
	echo.
 	%RS_TOOLSROOT%\bin\projectgeneratorGen9\projectgenerator.exe --changelist --verify %%f
	erase %%f 2>nul	
)

@echo.
@echo === PG3 ProgGenComplete ===
@echo ===========================
@echo.

:END
ENDLOCAL
