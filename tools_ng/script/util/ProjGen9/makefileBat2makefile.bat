@echo off
@echo.
@echo =============================================================================
@echo === PG3 ProjGen : Makefile.Bat conversion to .makefile (IronRuby)         === 
@echo === https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 ===
@echo.

SET PROPS_EXE=%~dp0..\..\..\bin\PropertySheetViewer.exe
if NOT EXIST "%PROPS_EXE%" echo missing "%PROPS_EXE%" && exit /b 1
"%PROPS_EXE%" showenvironment
for /F "tokens=*" %%i in ('%PROPS_EXE% dumpenvironment') do set %%i

REM -----------------------------------------------------------------------------------------------------
REM Call an ironruby script that recursively scans for makefile.bat files 
REM - it then produces a series of local *.makefile files. 
REM -----------------------------------------------------------------------------------------------------

call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%/ironlib/util/projGen/makefilebat_to_makefile.rb --exclude \/game\/

@echo.
@echo === PG3 ProgGenComplete ===
@echo ===========================
@echo.

:END