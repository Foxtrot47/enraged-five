goto END
@echo off
REM
REM Syncs to various files before project generation commences.
REM

if defined PROJGEN_SYNCED goto END
set PROJGEN_SYNCED="true"

@echo - Sync to latest project generator binaries. 
p4 sync %RS_TOOLSBIN%\projectgeneratorGen9\... 2>&1 || goto :Error
@echo.
@echo - Sync to latest project generator configuration files. 
p4 sync %RS_TOOLSCONFIG%\projgen9\... 2>&1 || goto :Error
@echo.
@echo - Sync to latest project generator script files. 
p4 sync %RS_TOOLSROOT%\script\util\projgen9\... 2>&1 || goto :Error
@echo.

:END
exit /b 0

:Error
exit /b 1