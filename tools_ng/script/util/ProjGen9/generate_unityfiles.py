import os

def main():
    print("=============================================================================")
    print("FINDING ALL MAKEFILES AND GENERATING .UNITY FILES\n")
    in_files = os.listdir(os.curdir)
    for in_file in in_files:
        if os.path.isdir(in_file):
            make_files([in_file+"/"+x for x in os.listdir(os.curdir+"/"+in_file)])
    print("\nDONE\n")
    print("=============================================================================")


def make_files(in_files):
     for in_file in in_files:
        if in_file.endswith(".makefile"):
            name = in_file.split(".makefile")[0]+".unity"
            if not os.path.exists(name):
                with open(name, "w") as new_file:
                    new_file.write("set enabled")
                print("writing "+name)

main()



        
