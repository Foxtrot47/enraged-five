@ECHO OFF
REM
REM File:: codebuilder_ps3.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 18th August 2009

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=codebuilder
set PROJECT=%RS_PROJECT%
set PLATFORM=ps3

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%_game_%PLATFORM%_nonunity_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

