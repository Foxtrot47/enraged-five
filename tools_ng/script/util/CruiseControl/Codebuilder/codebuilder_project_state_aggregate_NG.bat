@echo off

xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_codebuilder_NG.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_codebuilder_NG.txt

REM echo CODEBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb^
 --title="NextGen Codebuilders"^
 --webserver=%WEBSERVER%^
 --prev_report="codebuilder_project_state_aggregate"^
 --next_report="toolbuilder_project_state_aggregate"^
 --dest_filename="codebuilder_%RS_PROJECT%_project_state_aggregate_NG.xml"^
 --common="codebuilder_%RS_PROJECT%_dev_ng_"^
 --categories="orbis_unity_2012 durango_unity_2012 x64_unity_2012"^
 --sub_categories="debug beta bankrel final release"^
 codebuilder_%RS_PROJECT%_dev_ng_orbis_unity_2012_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ng_durango_unity_2012_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ng_x64_unity_2012_build_beta/project_state.xml


REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe codebuilder_%RS_PROJECT%_project_state_aggregate_NG.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o codebuilder_%RS_PROJECT%_project_state_aggregate_NG.html
REM echo codebuilder_project_state_aggregate.bat completed.

copy codebuilder_%RS_PROJECT%_project_state_aggregate_NG.html \\%WEBSERVER%\custom_reports$\codebuilder_project_state_aggregate_NG.html