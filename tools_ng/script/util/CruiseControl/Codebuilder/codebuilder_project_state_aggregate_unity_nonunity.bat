@echo off

xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_codebuilder.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_codebuilder.txt

REM echo CODEBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb^
 --title="VS2008/VS2010 Code Builder"^
 --webserver=%WEBSERVER%^
 --prev_report="codetester_project_state_aggregate"^
 --next_report="codebuilder_project_state_aggregate_NG"^
 --dest_filename="codebuilder_%RS_PROJECT%_project_state_aggregate.xml"^
 --common="codebuilder_%RS_PROJECT%_dev_"^
 --categories="ps3_unity 360_unity_2010 360_unity win32_unity x64_unity ps3 360 win32DX11_unity win32"^
 --sub_categories="debug beta bankrel final release"^
 codebuilder_%RS_PROJECT%_dev_xbox360_unity_2010_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_xbox360_unity_2010_build_bankrelease/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ps3_unity_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ps3_unity_build_bankrelease/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ps3_unity_build_release/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ps3_unity_build_final/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_win32DX11_unity_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_x64_unity_build_beta/project_state.xml^
 codebuilder_%RS_PROJECT%_dev_ps3_build_beta/project_state.xml

REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe codebuilder_%RS_PROJECT%_project_state_aggregate.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate_unity_nonunity.xsl -o codebuilder_%RS_PROJECT%_project_state_aggregate.html
REM echo codebuilder_project_state_aggregate.bat completed.

copy codebuilder_%RS_PROJECT%_project_state_aggregate.html \\%WEBSERVER%\custom_reports$\codebuilder_project_state_aggregate.html