@echo off

echo POST_LOG_PROJECT_STATE.BAT is building project state for project %1

ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state.rb --project_name=%1 --dest_filename="..\project_state.xml" --src_history="..\history.xml" --src_modifications="..\mods\modifications.xml,..\modifications.xml" --src_modifications_pending="..\modificationsPending.xml" --error_detect="Binary_Stats_Error Error:\s(.*)(Large\ssize|Text\s\(Code\)|Previous|Current|Delta|Status|Symbol) %RS_TOOLSROOT/etc/CruiseControl/general/studio_custom.xml|binary_stats_custom_email 3600" .

echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe ..\project_state.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state.xsl -o ..\project_state.html


echo ************* Running a post log script ******************
echo this will copy into the build dir the log file
ruby %5 --logfiles=%6 --dest=%7

echo ************* Running a post log script ******************
echo this publishes to perforce the build directory.
echo %2 %3 %4

ruby %2 --filespec=%3 --publish_folder=%4 --enable_checkin

echo post_log_project_transform completed.