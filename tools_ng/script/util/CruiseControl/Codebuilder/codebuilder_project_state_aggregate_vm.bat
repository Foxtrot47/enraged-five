@echo off

REM echo CODEBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --dest_filename="codebuilder_%RS_PROJECT%_project_state_aggregate_vm.xml" --common="codebuilder_%RS_PROJECT%_dev_" --categories="ps3_unity_vm 360_unity_vm ps3_unity 360_unity win32_unity ps3 360 win32" --sub_categories="debug beta bankrelease final release" codebuilder_%RS_PROJECT%_dev_xbox360_unity_build_beta/project_state.xml codebuilder_%RS_PROJECT%_dev_xbox360_unity_build_bankrelease/project_state.xml codebuilder_%RS_PROJECT%_dev_ps3_unity_build_beta/project_state.xml codebuilder_%RS_PROJECT%_dev_ps3_unity_build_bankrelease/project_state.xml codebuilder_%RS_PROJECT%_dev_ps3_unity_build_release/project_state.xml codebuilder_%RS_PROJECT%_dev_xbox360_build_beta/project_state.xml codebuilder_%RS_PROJECT%_dev_xbox360_build_bankrelease/project_state.xml codebuilder_%RS_PROJECT%_dev_win32_build_beta/project_state.xml codebuilder_%RS_PROJECT%_dev_ps3_unity_vm_build_beta/project_state.xml codebuilder_%RS_PROJECT%_dev_xbox360_unity_vm_build_beta/project_state.xml

REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe codebuilder_%RS_PROJECT%_project_state_aggregate_vm.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o codebuilder_%RS_PROJECT%_project_state_aggregate_vm.html
REM echo codebuilder_project_state_aggregate.bat completed.
xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_codebuilder_vm.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_codebuilder_vm.txt
copy codebuilder_%RS_PROJECT%_project_state_aggregate_vm.html \\%WEBSERVER%\custom_reports$\codebuilder_project_state_aggregate_vm.html