@ECHO OFF
REM
REM File:: 
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 21st November 2012

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=codebuilder
set PROJECT=%RS_PROJECT%
set PLATFORM=x64
set BUILD=beta
set UNITY=_unity

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%_game_build_rebuild%UNITY%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

