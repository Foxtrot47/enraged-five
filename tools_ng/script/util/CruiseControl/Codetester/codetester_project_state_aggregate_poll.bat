@echo off
n:
cd N:\RSGEDI\Builders\CruiseControl\live
echo CODETESTER_PROJECT_STATE_AGGREGATE_POLL.BAT is aggregating projects by polling
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate_poll.rb --run="%RS_TOOLSROOT%\script\util\cruisecontrol\codetester\codetester_project_state_aggregate.bat"
echo codetester_project_state_aggregate_poll.bat completed.