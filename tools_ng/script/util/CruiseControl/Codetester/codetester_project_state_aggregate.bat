@echo off

xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_codetester.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_codetester.txt

REM echo CODETESTER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --title="Codetest Pipeline" --webserver=%WEBSERVER% --prev_report="scriptbuilder_project_state_aggregate" --next_report="codebuilder_project_state_aggregate" --dest_filename="codetester_%RS_PROJECT%_project_state_aggregate.xml" --common="code_test_pipeline" --categories="" --sub_categories="" codebuilder_%RS_PROJECT%_dev_ps3_unity_build_bankrelease/project_state.xml codetester_%RS_PROJECT%_dev_ps3_bankrelease_%RS_PROJECT%_stats/project_state.xml

REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe codetester_%RS_PROJECT%_project_state_aggregate.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o codetester_%RS_PROJECT%_project_state_aggregate.html
REM echo codetester_project_state_aggregate.bat completed.

copy codetester_%RS_PROJECT%_project_state_aggregate.html \\%WEBSERVER%\custom_reports$\codetester_project_state_aggregate.html