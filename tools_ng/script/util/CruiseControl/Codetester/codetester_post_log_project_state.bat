@echo off

echo CODETESTER_POST_LOG_PROJECT_STATE.BAT is building project state for project %1

ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state.rb --ignore_path=cruisecontrol_builds --project_name=%1 --dest_filename="..\project_state.xml" --src_history="..\history.xml" --src_modifications="..\modifications.xml" --src_modifications_pending="..\modificationsPending.xml" --custom_error_classify="no_new_exe no_new_exe ^Error\s(.*)NO\sNEW\sEXECUTABLE\sTO\sTEST, no_smoke_start no_smoke_start ^Error(.*)THE\sSMOKE\sTEST\sDID\sNOT\sSTART,no_smoke_finish no_smoke_finish ^Error(.*)THE\sSMOKE\sTEST\sSTARTED\sBU\sDID\sNOT\sCOMPLETE" .

echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe ..\project_state.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state.xsl -o ..\project_state.html


echo ************* Running a post log script ******************
echo this will copy into the build dir the log file 
echo running %5 --logfiles=%6 --dest=%7
ruby %5 --logfiles=%6 --dest=%7

echo ************* Running a post log script ******************
echo this publishes to perforce the build directory.
echo running %2 --filespec=%3 --publish_folder=%4 --enable_checkin

ruby %2 --filespec=%3 --publish_folder=%4 --enable_checkin

echo codetester_post_log_project_transform completed.