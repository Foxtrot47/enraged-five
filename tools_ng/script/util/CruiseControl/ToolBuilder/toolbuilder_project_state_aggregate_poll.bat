@echo off
call setenv
n:
cd N:\RSGEDI\Builders\CruiseControl\live
echo TOOLBUILDER_PROJECT_STATE_AGGREGATE_POLL.BAT is aggregating projects by polling
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate_poll.rb --run="%RS_TOOLSROOT%\script\util\cruisecontrol\toolbuilder\toolbuilder_project_state_aggregate.bat" --poll_interval="30"
echo toolbuilder_project_state_aggregate_poll.bat completed.