@echo off

xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_toolbuilder.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_toolbuilder.txt

REM echo TOOLBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb 	^
	--title="Tool Builder" ^
	--webserver=%WEBSERVER% ^
	--prev_report="codebuilder_project_state_aggregate_NG" ^
	--next_report="scriptbuilder_project_state_aggregate" ^
	--dest_filename="toolbuilder_%RS_PROJECT%_project_state_aggregate.xml" ^
	--common="toolbuilder_%RS_PROJECT%_dev_" ^
	%RS_TOOLSROOT%/etc/cruisecontrol/toolbuilder/targets/toolbuilder_targets.xml

REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe toolbuilder_%RS_PROJECT%_project_state_aggregate.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o toolbuilder_%RS_PROJECT%_project_state_aggregate.html
REM echo toolbuilder_project_state_aggregate.bat completed.

copy toolbuilder_%RS_PROJECT%_project_state_aggregate.html \\%WEBSERVER%\custom_reports$\toolbuilder_project_state_aggregate.html