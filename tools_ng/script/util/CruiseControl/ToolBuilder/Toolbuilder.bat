@ECHO OFF
REM
REM File:: toolbuilder.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date::20th June 2011

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=toolbuilder

REM ======>START SERVER
pushd %RS_TOOLSBIN%\%CC%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

