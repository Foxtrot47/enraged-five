@ECHO OFF
REM
REM File:: toolbuilder_make.bat
REM Description:: Generates Cruise Control configuration xml
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date::20th June 2011

CALL %RS_TOOLSROOT%\bin\setenv.bat

set TARGET_DIR=%RS_TOOLSCONFIG%\cruisecontrol\toolbuilder\targets
set CCTRAY_DIR=%RS_TOOLSCONFIG%\cruisecontrol\cctray

ruby %RS_TOOLSROOT%\lib\util\cruisecontrol\cc_make.rb --target_dir=%TARGET_DIR% --cctray=%CCTRAY_DIR%\cctray-settings-toolsbuilder.xml --submit %TARGET_DIR%\targets.xml

SET ERR=%ERRORLEVEL%
echo Errorlevel is %ERR%
EXIT %ERR%


