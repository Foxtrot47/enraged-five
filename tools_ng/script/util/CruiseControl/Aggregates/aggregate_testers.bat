@ECHO OFF
REM
REM File:: aggregate_testers.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 6th September 2010

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl

set MAPTESTER_GRID_SIZE=100
set X1=-3500
set X2=-500
set X3=1500
set X4=4500

set Y1=-4000
set Y2=2000
set Y3=7500

if "%COMPUTERNAME%"=="EDIW-CC-4" 	goto CC4
if "%COMPUTERNAME%"=="EDIW-DWARD1" 	goto WARD
if "%COMPUTERNAME%"=="EDIW-JRIVERS" 	goto RIVERS
if "%COMPUTERNAME%"=="EDIW-DMUIR3" 	goto MUIR
if "%COMPUTERNAME%"=="EDIW-GSMITH" 	goto SMITH
if "%COMPUTERNAME%"=="EDIW-LUKEW7" 	goto OPENSHAW
if "%COMPUTERNAME%"=="EDIW-MTASCHLE" 	goto TASCHLER
goto END

:WARD
echo RUNNING WARD MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.20.227"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_20_227"
set MAPTESTER_SX1=%X1%
set MAPTESTER_EX1=%X2%
set MAPTESTER_SY1=%Y1%
set MAPTESTER_EY1=%Y2%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:RIVERS
echo RUNNING RIVERS MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.20.232"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_20_232"
set MAPTESTER_SX1=%X2%
set MAPTESTER_EX1=%X3%
set MAPTESTER_SY1=%Y1%
set MAPTESTER_EY1=%Y2%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:OPENSHAW
echo RUNNING OPENSHAW MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.20.208"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_20_208"
set MAPTESTER_SX1=%X3%
set MAPTESTER_EX1=%X4%
set MAPTESTER_SY1=%Y1%
set MAPTESTER_EY1=%Y2%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:TASCHLER
echo RUNNING TASCHLER MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.20.202"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_20_202"
set MAPTESTER_SX1=%X1%
set MAPTESTER_EX1=%X2%
set MAPTESTER_SY1=%Y2%
set MAPTESTER_EY1=%Y3%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:MUIR
echo RUNNING MUIR MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.20.200"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_20_200"
set MAPTESTER_SX1=%X2%
set MAPTESTER_EX1=%X3%
set MAPTESTER_SY1=%Y2%
set MAPTESTER_EY1=%Y3%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:SMITH
echo RUNNING SMITH MAPTESTER 
set MAPTESTER_ADHOC_IP_PS3="10.11.21.03"
set MAPTESTER_ADHOC_LOG_IP_PS3="%COMPUTERNAME%_10_11_21_03"
set MAPTESTER_SX1=%X2%
set MAPTESTER_EX1=%X3%
set MAPTESTER_SY1=%Y2%
set MAPTESTER_EY1=%Y3%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\maptester\maptester_adhoc_ps3_root.xml
goto END



:CC4
echo RUNNING CC4 MAPTESTER 
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\aggregates\aggregate_testers_root.xml
goto END



popd

:END
pause
REM ===>CRUISE CONTROL EXIT



