@ECHO OFF
REM
REM File:: aggregate_gamestats.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM date:: 1st June 2012

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl

REM ======>START SERVER
pushd %RS_TOOLSBIN%\%CC%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\aggregates\aggregate_gamestats_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

