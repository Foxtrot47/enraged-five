@ECHO OFF
REM
REM File:: aggregate_testers.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 6th September 2010

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\aggregates\aggregate_testers_head_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT



