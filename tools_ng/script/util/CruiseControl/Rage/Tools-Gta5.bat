@ECHO OFF
REM
REM File:: tool-gta5.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 18th August 2009

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=tools
set PROJECT=rage
set BRANCH=gta5
set SHARED_DIR=%RS_TOOLSCONFIG%\%CC%\%PROJECT%\shared

REM ======>START SERVER
pushd %RS_TOOLSBIN%\%CC%
ECHO %CD%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT%\%PROJECT_TYPE%\%PROJECT%_%PROJECT_TYPE%_%BRANCH%.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

