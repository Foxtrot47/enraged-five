#
# File:: cctray.rb
# Description:: Launches a tray for Cruise Control
#
# Author:: Derek Ward <derek.ward@rockstarnorth.com>
# Date:: 15th October 2009
#

#-----------------------------------------------------------------------------
# Uses / Requires
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
CONFIG = Pipeline::Config::instance()

OPTIONS = [
	[ "--type", "-t", Getopt::REQUIRED, "Type of tray required." ],
	[ "--help", "-h", Getopt::BOOLEAN, "display usage information." ],
	[ "--rename_exe", "-r", Getopt::BOOLEAN, "renames the executable before launching it ( if it can) in order that it is clear which process launched which tray ( for cctray management )" ]
]
	
#-----------------------------------------------------------------------------
# Application entry point
#-----------------------------------------------------------------------------
begin
	#---------------------------------------------------------------------
	# Parse Command Line.
	#---------------------------------------------------------------------
	opts, trailing = OS::Getopt.getopts( OPTIONS )
	if ( opts['help'] )
		puts OS::Getopt.usage( OPTIONS )
		puts ("Press Enter to continue...")
		$stdin.getc( )				
		exit( 1 )
	end	
	
	if ( opts['type'] == nil )
		$stderr.puts("Error:\n you need to supply a tray name you wish to use.")
		puts("Press Enter to continue...")
		$stdin.getc( )	
		exit( -1 )
	end

	#---------------------------------------------------------------------------------------
	# check X Drive is mapped - since Cruise Control uses explicit settings
	# that refer to the X drive ( we can;t get around this at present with env variables ) 
	# we need to make sure the user knows to have their X drive mapped at startup. 
	# Otherwise this tray will throw an exception.
	#---------------------------------------------------------------------------------------	
	drive = "X:"
	if (not ::File::directory?("#{drive}/"))
		$stderr.puts("Error :\n Ensure CCTray is launched after\n #{drive} drive has been mounted.")
		puts("Press Enter to continue...")
		$stdin.getc( )	
		exit( -1 )
	end
	
	toolsroot = CONFIG.toolsroot
	if (not ::File::directory?("#{toolsroot}/"))
		$stderr.puts("Error :\n #{toolsroot}\n could not be found, have you installed tools?")
		puts("Press Enter to continue...")
		$stdin.getc( )	
		exit( -1 )
	end	
	
	cctray_type = opts['type']

	# handle rename of cctray exe so we can kill a specific process name in order to restart it.	
	rename_exe  = opts['rename_exe']	
	cctray_exe = 'cctray.exe'
	cctray_exe_path = Pipeline::OS::Path.combine( CONFIG.toolsbin, 'CruiseControl')
	if (rename_exe)
		puts "renaming..."
		new_cctray_exe = cctray_exe.sub(".exe","_#{cctray_type}.exe")
	
		src = Pipeline::OS::Path.combine(cctray_exe_path,cctray_exe)
		dst = Pipeline::OS::Path.combine(cctray_exe_path,new_cctray_exe)
		FileUtils::cp( src, dst ) if ( File.exists?( src ) )
		
		if ( File.exists?( dst ) )			
			cctray_exe = new_cctray_exe
			File.chmod(0664, dst)
		end
	end
	
	cctray_exe_path = Pipeline::OS::Path.combine( CONFIG.toolsbin, 'CruiseControl', cctray_exe )
	cctray_settings_path = Pipeline::OS::Path.combine( CONFIG.toolsconfig, 'CruiseControl', 'cctray', "cctray-settings-#{cctray_type}.xml" )
	puts "#{cctray_exe_path} #{cctray_settings_path}"
	
	if (not ::File::exists?("#{cctray_exe_path}"))
		$stderr.puts("Error :\n #{cctray_exe_path}\n - cctray executable file not found.")
		puts("Press Enter to continue...")
		$stdin.getc( )	
		exit( -1 )
	end	
		
	if (not ::File::exists?("#{cctray_settings_path}"))
		$stderr.puts("Error :\n #{cctray_settings_path}\n cctray config file not found.")
		puts("Press Enter to continue...")
		$stdin.getc( )	
		exit( -1 )
	end	
	
	#-------------------------------------------------------------------------------------------
	# Kick off the tray, to get the GUI program to return once launched you need to call 'start'
	#-------------------------------------------------------------------------------------------
	system "start #{cctray_exe_path} #{cctray_settings_path}"		
	
rescue Exception => ex
	$stderr.puts "Unhandled exception: #{ex.message}"
	$stderr.puts "Backtrace:"
	ex.backtrace.each { |m| $stderr.puts "\t#{m}" }
	exit -1
end

exit 0


