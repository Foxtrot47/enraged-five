@echo off
TITLE Launching CCTray 

IF [%1]==[] GOTO USAGE

REM ====================================
REM ====Wait for drive to get mapped====
REM ====================================

echo Please wait checking drive mapping of X drive...
SET Counter1 = 0
SET loops1   = 1000000
:LOOPSTART1
 	IF NOT EXIST X:\nul ping -n 3 127.0.0.1 > NUL
	IF EXIST X:\nul GOTO LOOPEND1
	set /a Counter1=%Counter1% + 1
	IF "%Counter1%" < %loops1% GOTO LOOPSTART1
:LOOPEND1

CALL setenv.bat

REM ===============================================
REM ====Check that these critical folders exist====
REM ===============================================

IF NOT EXIST X:\nul GOTO NO_TOOLSDRIVE
IF NOT EXIST X:\nul  GOTO NO_TOOLSROOT

REM =======================================
REM ====At last - run the feckin script====
REM =======================================
 
echo Invoking ruby...

IF [%2]==[] GOTO HERE
echo Renamed
%RS_TOOLSRUBY% cctray.rb --type=%1 --rename_exe
GOTO THERE

:HERE
echo not renamed
%RS_TOOLSRUBY% cctray.rb --type=%1
:THERE

REM ==============================
REM ====Error handling follows====
REM ==============================

IF ERRORLEVEL 0 GOTO OK
GOTO TRAYFAILED


:NO_TOOLSDRIVE
echo Error: There is a problem accessing your X drive. How is it mapped? ( CCTray will not launch )
pause
GOTO END



:NO_TOOLSROOT
echo Error: There is a problem accessing folder %RS_TOOLSROOT% ( CCTray will not launch )
pause
GOTO END



:TRAYFAILED
echo *********************************************************
echo Error: There was a problem launching Ruby or with CCTray.
echo *********************************************************
pause
GOTO END



:USAGE
echo *********************************************************
echo Error: Usage - pass the type of tray you require.
echo *********************************************************
pause
GOTO END



:OK
:END
EXIT /B %ERRORLEVEL%
