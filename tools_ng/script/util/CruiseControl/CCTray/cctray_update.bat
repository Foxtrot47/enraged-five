@echo off
TITLE Updating CCTray 
CALL setenv.bat

SET BUILD=%1

%RS_TOOLSLIB%/util/CruiseControl/cctray_update.rb --cctray=%BUILD%
@%RS_TOOLSROOT%/script/util/CruiseControl/cctray/cctray.bat %BUILD% renamed

:END
EXIT /B %ERRORLEVEL%
