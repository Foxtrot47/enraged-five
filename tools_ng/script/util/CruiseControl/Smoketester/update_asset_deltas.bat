@ECHO OFF
REM Builds the asset deltas xlsx file, from captured delta data filenames.
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 28th June 2012

CALL %RS_TOOLSROOT%\bin\setenv.bat

if "%1"=="PC" goto PC
if "%1"=="PS3" goto PS3 
if "%1"=="XBOX360" goto XBOX360
goto END

:PS3
ruby %RS_TOOLSLIB%\util\SmokeTest\update_asset_deltas.rb --row="2" --xlsfilename="%RS_PROJROOT%\docs\code\BuildState\%2\AssetDeltas.xlsx" %RS_PROJROOT%\docs\code\BuildState\%2\PS3\ModuleInfoCompare.txt %RS_PROJROOT%\docs\code\BuildState\%2\PS3\SceneCostsCompare.txt
goto END

:XBOX360
ruby %RS_TOOLSLIB%\util\SmokeTest\update_asset_deltas.rb --row="4" --xlsfilename="%RS_PROJROOT%\docs\code\BuildState\%2\AssetDeltas.xlsx" %RS_PROJROOT%\docs\code\BuildState\%2\Xbox360\ModuleInfoCompare.txt %RS_PROJROOT%\docs\code\BuildState\%2\Xbox360\SceneCostsCompare.txt
goto END

:PC
ruby %RS_TOOLSLIB%\util\SmokeTest\update_asset_deltas.rb --row="6" --xlsfilename="%RS_PROJROOT%\docs\code\BuildState\%2\AssetDeltas.xlsx" %RS_PROJROOT%\docs\code\BuildState\%2\PC\ModuleInfoCompare.txt %RS_PROJROOT%\docs\code\BuildState\%2\PC\SceneCostsCompare.txt
goto END


:END

REM ===>EXIT



