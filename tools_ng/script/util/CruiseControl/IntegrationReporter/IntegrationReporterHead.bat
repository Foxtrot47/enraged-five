@ECHO OFF
REM
REM File:: IntegrationReporter.bat
REM Description:: Launches Cruise Control Server - this is to run from the 'head' game.
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 10th June 2011

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=head
set PROJECT=integration_reporter
set BRANCH=dev

REM ======>START SERVER
pushd %RS_TOOLSBIN%\%CC%
ECHO %CD%
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT%\%PROJECT%_%PROJECT_TYPE%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT

