@echo off

xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_scriptbuilder.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_scriptbuilder.txt

REM echo SCRIPTBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --title="Script Builder" --webserver=%WEBSERVER% --prev_report="toolbuilder_project_state_aggregate" --next_report="codetester_project_state_aggregate" --dest_filename="scriptbuilder_%RS_PROJECT%_project_state_aggregate.xml" --common="scriptbuilder_%RS_PROJECT%_dev_" --categories="nmtest_dev testbed" --sub_categories="debug release" scriptbuilder_%RS_PROJECT%_dev_debug/project_state.xml scriptbuilder_%RS_PROJECT%_dev_release/project_state.xml scriptbuilder_%RS_PROJECT%_dev_nmtest_dev/project_state.xml scriptbuilder_%RS_PROJECT%_dev_testbed_debug/project_state.xml

REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe scriptbuilder_%RS_PROJECT%_project_state_aggregate.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o scriptbuilder_%RS_PROJECT%_project_state_aggregate.html
REM echo scriptbuilder_project_state_aggregate.bat completed.

copy scriptbuilder_%RS_PROJECT%_project_state_aggregate.html \\%WEBSERVER%\custom_reports$\scriptbuilder_project_state_aggregate.html
