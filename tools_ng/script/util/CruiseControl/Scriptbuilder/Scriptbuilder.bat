@ECHO OFF
REM
REM File:: scriptbuilder.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 2nd November 2009

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=scriptbuilder
set PROJECT=%RS_PROJECT%

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT



