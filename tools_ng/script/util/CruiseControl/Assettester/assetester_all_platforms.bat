@ECHO OFF
REM
REM File:: assetester_xbox360.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 17th December 2009

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=assettester
set PROJECT=%RS_PROJECT%
set PLATFORM=all_platforms

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%_game_%PLATFORM%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT



