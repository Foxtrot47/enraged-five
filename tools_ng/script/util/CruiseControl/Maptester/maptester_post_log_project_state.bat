@echo off

echo MAPTESTER_POST_LOG_PROJECT_STATE.BAT is building project state for project %1

echo ************* Running a post log script ******************
echo this will copy into the build dir the log file 
echo running %5 --logfiles=%6 --dest=%7
ruby %5 --logfiles=%6 --dest=%7

echo ************* Running a post log script ******************
echo this publishes to perforce the build directory.
echo running %2 --filespec=%3 --publish_folder=%4 --enable_checkin

ruby %2 --filespec=%3 --publish_folder=%4 --enable_checkin

echo maptester_post_log_project_state.bat completed.