@ECHO OFF
REM
REM File:: assetbuilder_rebuild_make.bat
REM Description:: Generates Cruise Control configuration xml
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date::2nd September 2011

CALL %RS_TOOLSROOT%\bin\setenv.bat

set TARGET_DIR=%RS_TOOLSCONFIG%\cruisecontrol\assetbuilder\rebuilds
set CCTRAY_DIR=%RS_TOOLSCONFIG%\cruisecontrol\cctray

ruby %RS_TOOLSROOT%\lib\util\cruisecontrol\cc_make_assetbuilder_rebuild.rb --target_dir=%TARGET_DIR% --cctray=%CCTRAY_DIR%\cctray-settings-assetbuild%1.xml --submit --variant=%1 %TARGET_DIR%\rebuilds.xml

SET ERR=%ERRORLEVEL%
echo Errorlevel is %ERR%
REM EXIT %ERR%


