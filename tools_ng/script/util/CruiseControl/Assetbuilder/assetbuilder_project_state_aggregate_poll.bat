@echo off
CALL setenv.bat >NUL

n:
cd N:\RSGEDI\Builders\CruiseControl\live
echo ASSETBUILDER_PROJECT_STATE_AGGREGATE_POLL.BAT is aggregating projects by polling
%RS_TOOLSRUBY% %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate_poll.rb --run="%RS_TOOLSROOT%\script\util\cruisecontrol\assetbuilder\assetbuilder_project_state_aggregate.bat" --poll_interval="5"
echo assetbuilder_project_state_aggregate_poll.bat completed.