@echo off

echo ASSETBUILDER_POST_LOG_PROJECT_STATE.BAT is building project state for project %1

ruby %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state.rb --ignore_path=cruisecontrol_builds --project_name=%1 --dest_filename="..\project_state.xml" --src_history="..\history.xml" --src_modifications="..\modifications.xml" --src_modifications_pending="..\modificationsPending.xml" --custom_error_classify="skipped skipped ^Error\s.*SKIPPED\sCL\s-\sNO\sBUILD\sHAS\sTAKEN\sPLACE" .

echo Transform project_state.xml to Project_state.html
REM %RS_TOOLSROOT%\bin\nxslt2.exe ..\project_state.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state.xsl -o ..\project_state.html

echo ASSETBUILDER_post_log_project_transform completed.