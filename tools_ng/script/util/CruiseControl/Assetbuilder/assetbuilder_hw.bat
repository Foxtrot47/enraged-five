@ECHO OFF
REM
REM File:: assetbuilder.bat
REM Description:: Launches Cruise Control Server
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM date:: 10th May 2010

CALL %RS_TOOLSROOT%\bin\setenv.bat

set CC=CruiseControl
set PROJECT_TYPE=assetbuilder
set PROJECT=%RS_PROJECT%
set ASSETBUILDER_TYPE=_hw

REM ======>START SERVER
pushd %RS_TOOLSBIN%\CruiseControl
"%RS_TOOLSBIN%\%CC%\ccnet.exe" -config:%RS_TOOLSCONFIG%\%CC%\%PROJECT_TYPE%\%PROJECT_TYPE%%ASSETBUILDER_TYPE%_root.xml
popd

pause
REM ===>CRUISE CONTROL EXIT



