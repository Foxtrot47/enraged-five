@echo off
CALL setenv.bat >NUL

ECHO XML Starlet...
xmlstarlet sel -t -m //* -o "" -v "@dashboard" %RS_TOOLSROOT%\etc\cruisecontrol\general\studio_custom.xml > webserver_%RS_PROJECT%_assetbuilder.txt
SET /P WEBSERVER=<webserver_%RS_PROJECT%_assetbuilder.txt

ECHO Aggregate build...
REM echo ASSETBUILDER_PROJECT_STATE_AGGREGATE.BAT is aggregating project
%RS_TOOLSRUBY% %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --title="Asset Builder" --webserver=%WEBSERVER% --prev_report="assetbuilder_project_state_aggregate_user" --next_report="codebuilder_project_state_aggregate" --dest_filename="assetbuilder_%RS_PROJECT%_project_state_aggregate_orig.xml" --common="_assetbuilder_%RS_PROJECT%_dev" assetbuilder_%RS_PROJECT%_dev/project_state.xml assetbuilder_%RS_PROJECT%_dev_hw/project_state.xml assetbuilder_%RS_PROJECT%_dev_hw5/project_state.xml
%RS_TOOLSRUBY% %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --title="Asset Builder" --webserver=%WEBSERVER% --prev_report="assetbuilder_project_state_aggregate_user" --next_report="codebuilder_project_state_aggregate" --dest_filename="assetbuilder_%RS_PROJECT%_project_state_aggregate.xml" --common="_assetbuilder_%RS_PROJECT%_dev"  --events="assetbuilder_%RS_PROJECT%_dev_hw/events.xml" assetbuilder_%RS_PROJECT%_dev_hw/project_state.xml assetbuilder_%RS_PROJECT%_dev_hw5/project_state.xml
%RS_TOOLSRUBY% %RS_TOOLSROOT%/lib/util/cruisecontrol/change_report_project_state_aggregate.rb --title="Asset Builder (User)" --webserver=%WEBSERVER% --prev_report="codetester_project_state_aggregate" --next_report="assetbuilder_project_state_aggregate" --dest_filename="assetbuilder_%RS_PROJECT%_project_state_aggregate_user.xml" --common="_assetbuilder_%RS_PROJECT%_dev" --events="assetbuilder_%RS_PROJECT%_dev_user/events.xml" assetbuilder_%RS_PROJECT%_dev_user/project_state.xml

ECHO XSLT...
REM echo Transform project_state.xml to Project_state.html
%RS_TOOLSROOT%\bin\nxslt2.exe assetbuilder_%RS_PROJECT%_project_state_aggregate_orig.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate.xsl -o assetbuilder_%RS_PROJECT%_project_state_aggregate_orig.html
%RS_TOOLSROOT%\bin\nxslt2.exe assetbuilder_%RS_PROJECT%_project_state_aggregate.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate_detailed.xsl -o assetbuilder_%RS_PROJECT%_project_state_aggregate.html
%RS_TOOLSROOT%\bin\nxslt2.exe assetbuilder_%RS_PROJECT%_project_state_aggregate_user.xml %RS_TOOLSROOT%/etc/cruisecontrol/project_state_aggregate_detailed.xsl -o assetbuilder_%RS_PROJECT%_project_state_aggregate_user.html

ECHO COPY...
REM echo assetbuilder_project_state_aggregate.bat completed.

copy assetbuilder_%RS_PROJECT%_project_state_aggregate_orig.html \\%WEBSERVER%\custom_reports$\assetbuilder_project_state_aggregate_orig.html
copy assetbuilder_%RS_PROJECT%_project_state_aggregate.html \\%WEBSERVER%\custom_reports$\assetbuilder_project_state_aggregate.html 
copy assetbuilder_%RS_PROJECT%_project_state_aggregate_user.html \\%WEBSERVER%\custom_reports$\assetbuilder_project_state_aggregate_user.html
