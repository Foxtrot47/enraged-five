@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_sync.rb %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/data/cdimages...zip@%PERFORCE_CURRENT_LABEL_NAME%  %PERFORCE_ROOT%/build/dev/metadata/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/ps3/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/TROPDIR/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_beta_snc.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_bankrelease_snc.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_release_snc.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.sfo@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.png@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/script/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/tools/... %PERFORCE_ROOT%/Docs/... %PERFORCE_ROOT%/assets/Dialogue/... %PERFORCE_ROOT%/assets/recordings/... 


shutdown -s -t 1