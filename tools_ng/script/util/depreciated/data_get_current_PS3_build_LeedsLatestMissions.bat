@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

%RS_TOOLSRUBY% %RS_TOOLSLIB%\util\perforce\p4_sync.rb %PERFORCE_ROOT%/script/dev/singleplayer/include/public/RC_Helper_Functions.sch %PERFORCE_ROOT%/script/dev/singleplayer/include/globals/randomChar_globals.sch %PERFORCE_ROOT%/script/dev/singleplayer/include/public/RC_launcher_public.sch %PERFORCE_ROOT%/script/dev/singleplayer/scripts/RandomChar/... %PERFORCE_ROOT%/assets/export/levels/gta5/waypointrec.zip %PERFORCE_ROOT%/build/dev/ps3/levels/gta5/waypointrec.rpf %PERFORCE_ROOT%/build/dev/common/data/levels/gta5/ambient_Leeds.ipl %PERFORCE_ROOT%/script/dev/singleplayer/include/private/randomChar/... %PERFORCE_ROOT%/build/dev/common/data/script/xml/SPDebug/... %PERFORCE_ROOT%/build/dev/common/text/... %PERFORCE_ROOT%/assets/GameText/American/... %PERFORCE_ROOT%/assets/export/data/cdimages/... %PERFORCE_ROOT%/script/dev/singleplayer/scripts/Ambient/Rampages/... %PERFORCE_ROOT%/script/dev/shared/include/public/rampage_public.sch %PERFORCE_ROOT%/script/dev/shared/include/public/rampagev2_include.sch %PERFORCE_ROOT%/script/dev/shared/include/public/rampagev3_include.sch %PERFORCE_ROOT%/script/dev/shared/include/public/rgeneral_include.sch %PERFORCE_ROOT%/script/dev/singleplayer/scripts/Ambient/MichaelEvents/...


IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	pause
)

