@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% release: Getting %PERFORCE_MILESTONE_LABEL_NAME% labelled build for Xbox360...
ECHO %RS_PROJECT% release: Getting %PERFORCE_MILESTONE_LABEL_NAME% labelled build for Xbox360...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/common/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/export/...xml@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/export/...ide@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/xbox360/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/game_xenon_bankrelease.*@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/game_xenon_beta.*@%PERFORCE_MILESTONE_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/360_release_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/360_release_sync.txt
	pause
)

