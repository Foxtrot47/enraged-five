@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% MP Play: Getting %PERFORCE_NETWORK_DEMO_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% MP Play: Getting %PERFORCE_NETWORK_DEMO_LABEL_NAME% labelled build for PS3...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


p4 sync %PERFORCE_ROOT%/build/mp_play/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/install/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt 
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/game_psn_release_snc.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/*.sfo@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/*.png@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/mp_play/commandline.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps3_devnet_sync.txt
)


pause
