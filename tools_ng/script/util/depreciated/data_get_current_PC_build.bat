@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PC...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build for PC...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/pc/...@%PERFORCE_CURRENT_LABEL_NAME%  %PERFORCE_ROOT%/build/dev/game_win32_beta_dx11.*@%PERFORCE_CURRENT_LABEL_NAME%  %PERFORCE_ROOT%/build/dev/game_win32_beta_dx11_*.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/binkw32.dll@%PERFORCE_CURRENT_LABEL_NAME%  %PERFORCE_ROOT%/build/dev/binkw64.dll@%PERFORCE_CURRENT_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/pc_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/pc_sync.txt
	pause
)