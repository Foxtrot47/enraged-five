@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting [CB]_GTAV_current_XBO label for Xbox One...
ECHO Getting [CB]_GTAV_current_XBO label for Xbox One...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync //depot/gta5/build/dev_ng/common/...@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/dev_ng/x64/...@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/dev_ng/xbo_loose/...@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/dev_ng/xbo_scripts/...@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/dev_ng/game_durango_beta.*@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_durango_bankrelease.*@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_durango_release.*@[CB]_GTAV_current_XBO
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/Current_XBO.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)

)
::---------------------------------------------------------------

pause

:END
