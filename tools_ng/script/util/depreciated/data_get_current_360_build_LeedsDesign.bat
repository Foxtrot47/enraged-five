@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/data/cdimages...zip@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/metadata/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/xbox360/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_xenon_*.*@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/script/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/tools/... %PERFORCE_ROOT%/Docs/... %PERFORCE_ROOT%/assets/Dialogue/... %PERFORCE_ROOT%/assets/recordings/... 2> %RS_TOOLSROOT%/logs/360_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/360_sync.txt
	pause
)

