@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% release: Getting %PERFORCE_MILESTONE_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% release: Getting %PERFORCE_MILESTONE_LABEL_NAME% labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/common/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/export/...xml@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/export/...ide@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/ps3/...@%PERFORCE_MILESTONE_LABEL_NAME%  %PERFORCE_ROOT%/build/release/TROPDIR/...@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/game_psn_beta_snc.*@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/game_psn_bankrelease_snc.*@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/release/game_psn_release_snc.*@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.sfo@%PERFORCE_MILESTONE_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.png@%PERFORCE_MILESTONE_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/ps3_release_sync.txt
 
IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/ps3_release_sync.txt
	pause
)

