@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE [Preview] %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build [Preview]
ECHO [Preview] %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build [Preview]

p4 sync -n %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/xbox360/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/ps3/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/TROPDIR/...@%PERFORCE_CURRENT_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.*@%PERFORCE_CURRENT_LABEL_NAME%

pause
