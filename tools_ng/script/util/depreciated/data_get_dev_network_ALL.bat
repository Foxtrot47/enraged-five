@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% MP dev_network: Getting %PERFORCE_NETWORK_DEMO_LABEL_NAME% labelled build...
ECHO %RS_PROJECT% MP dev_network: Getting %PERFORCE_NETWORK_DEMO_LABEL_NAME% labelled build...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev_network/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev_network/xbox360/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev_network/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev_network/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev_network/*.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/devnet_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/devnet_sync.txt
)

pause