@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% MP dev: Getting %PERFORCE_CURRENT_NETWORK_LABEL_NAME% labelled build for PS3...
ECHO %RS_PROJECT% MP dev: Getting %PERFORCE_CURRENT_NETWORK_LABEL_NAME% labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/assets/export/...ide@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/metadata/...@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/ps3/...@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/TROPDIR/...@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_beta_snc.*@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_bankrelease_snc.*@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/game_psn_release_snc.*@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.sfo@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% %PERFORCE_ROOT%/build/dev/*.png@%PERFORCE_CURRENT_NETWORK_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/ps3_network_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/ps3_network_sync.txt
	pause
)

