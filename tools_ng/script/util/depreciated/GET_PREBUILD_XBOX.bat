@ECHO OFF

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Prebuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for Xbox360...
ECHO %RS_PROJECT% Prebuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for Xbox360...

set networkbuildpath=N:\RSGEDI\Distribution\QA_Build\gta5\PREBUILD\dev
set localbuildpath=X:\gta5\build\dev
set syncerror=0

:: CHECK FOR LOCK FILE 
if exist %networkbuildpath%\lock.txt (
	echo The Build_Data is currently updating for a new release. Please try again later...
	echo Exiting...
	pause
	exit
) else (
	goto CONTINUE
)


:CONTINUE
if not exist %RS_TOOLSROOT%/logs/ (
	mkdir %RS_TOOLSROOT%/logs/
)

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

:: - Sync local network stored platform data -
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /DCOPY:T keeps source timestamps
robocopy /S /DCOPY:T %networkbuildpath%\xbox360\ %localbuildpath%\xbox360\ *.* 2> %RS_TOOLSROOT%/logs/360_DATA_sync.txt

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_PREBUILD_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/assets/export/...xml@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/game_xenon_beta.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/game_xenon_bankrelease.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/game_xenon_release.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

 
::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/360_sync.txt
	notepad %RS_TOOLSROOT%/logs/360_data_sync.txt
)

::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
	echo Starting Rag proxy.
	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (
start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus
)
::---------------------------------------------------------------

if "%SKIP_PAUSE%"=="1" (
	goto:POST_PAUSE
)
pause
:POST_PAUSE