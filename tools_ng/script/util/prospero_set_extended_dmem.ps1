$extraDmem = 4096
$xmlPath = $env:USERPROFILE+'\prosperoSettings.xml'
&'prospero-ctrl.exe' 'settings' 'export' $xmlPath
[xml]$xml = Get-Content -Path $xmlPath
$node = $xml.SelectNodes("/settings/setting") | Where-Object {$_.GetAttribute("key") -eq "0x78280100"};
Write-Host 'Writing extended DMEM setting.'
$node.SetAttribute("value",[string]$extraDmem)
$xml.Save($xmlPath)
&'prospero-ctrl.exe' 'settings' 'import' $xmlPath 