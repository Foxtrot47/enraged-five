@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA_NG_PreBuild label for PS4...
ECHO Getting GTA_NG_PreBuild label for PS4...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab main build.
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_PreBuild 2> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_companion_httpd/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_module/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

:: Grab NG TU

::TitleUpdate - dev_ng_Live
p4 sync //depot/gta5/titleupdate/dev_ng_Live/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/*.prx@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab DLC compat packs

::Beach
p4 sync //gta5_dlc/mpPacks/mpBeach/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Christmas
p4 sync //gta5_dlc/mpPacks/mpChristmas/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::Christmas2
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Valentine's
p4 sync //gta5_dlc/mpPacks/mpValentines/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business
p4 sync //gta5_dlc/mpPacks/mpBusiness/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business2
p4 sync //gta5_dlc/mpPacks/mpBusiness2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Hipster
p4 sync //gta5_dlc/mpPacks/mpHipster/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpIndependence
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpHeist
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPilot
p4 sync //gta5_dlc/mpPacks/mpPilot/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLTS
p4 sync //gta5_dlc/mpPacks/mpLTS/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe2
p4 sync //gta5_dlc/mpPacks/mpLuxe2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpReplay
p4 sync //gta5_dlc/mpPacks/mpReplay/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpReplay/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLowrider
p4 sync //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpHalloween
p4 sync //gta5_dlc/mpPacks/mpHalloween/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHalloween/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpApartment
p4 sync //gta5_dlc/mpPacks/mpApartment/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpApartment/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLowrider2
p4 sync //gta5_dlc/mpPacks/mpLowrider2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpExecutive
p4 sync //gta5_dlc/mpPacks/mpExecutive/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpExecutive/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpstunt
p4 sync //gta5_dlc/mpPacks/mpstunt/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpstunt/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpBiker
p4 sync //gta5_dlc/mpPacks/mpBiker/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBiker/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpImportExport
p4 sync //gta5_dlc/mpPacks/mpImportExport/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpImportExport/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpSpecialRaces
p4 sync //gta5_dlc/mpPacks/mpSpecialRaces/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSpecialRaces/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpGunRunning
p4 sync //gta5_dlc/mpPacks/mpGunRunning/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpGunRunning/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpSmuggler
p4 sync //gta5_dlc/mpPacks/mpSmuggler/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSmuggler/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpChristmas2017
p4 sync //gta5_dlc/mpPacks/mpChristmas2017/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2017/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpAssault
p4 sync //gta5_dlc/mpPacks/mpAssault/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpAssault/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpBattle
p4 sync //gta5_dlc/mpPacks/mpBattle/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBattle/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpChristmas2018
p4 sync //gta5_dlc/mpPacks/mpChristmas2018/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2018/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPatchesNG
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::CLIFFORD
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::spUpgrade
p4 sync //gta5_dlc/spPacks/spUpgrade/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/spUpgrade/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay1NG
p4 sync //gta5_dlc/patchPacks/patchDay1NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay1NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2NG
p4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2bNG
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay3NG
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay4NG
p4 sync //gta5_dlc/patchPacks/patchDay4NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay4NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay5NG
p4 sync //gta5_dlc/patchPacks/patchDay5NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay5NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay6NG
p4 sync //gta5_dlc/patchPacks/patchDay6NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay6NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay7NG
p4 sync //gta5_dlc/patchPacks/patchDay7NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay7NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay8NG
p4 sync //gta5_dlc/patchPacks/patchDay8NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay8NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay9NG
p4 sync //gta5_dlc/patchPacks/patchDay9NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay9NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay10NG
p4 sync //gta5_dlc/patchPacks/patchDay10NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay10NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay11NG
p4 sync //gta5_dlc/patchPacks/patchDay11NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay11NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay12NG
p4 sync //gta5_dlc/patchPacks/patchDay12NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay12NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay13NG
p4 sync //gta5_dlc/patchPacks/patchDay13NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay13NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay14NG
p4 sync //gta5_dlc/patchPacks/patchDay14NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay14NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay15NG
p4 sync //gta5_dlc/patchPacks/patchDay15NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay15NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay16NG
p4 sync //gta5_dlc/patchPacks/patchDay16NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay16NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay17NG
p4 sync //gta5_dlc/patchPacks/patchDay17NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay17NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay18NG
p4 sync //gta5_dlc/patchPacks/patchDay18NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay18NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay19NG
p4 sync //gta5_dlc/patchPacks/patchDay19NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay19NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay20NG
p4 sync //gta5_dlc/patchPacks/patchDay20NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay20NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
)


)
::---------------------------------------------------------------

pause

:END
