@ECHO OFF

:: v1.0
:: Batch process to move backup version to the server 

:: v1.1
:: Added individual xex paths so it doesn't transfer non required files to save time

ECHO Copy GTAV platform Data and Update Current Label


TITLE %RS_PROJECT% dev: Moving labelled build for 360 to Server for Backup...


ECHO N:\RSGLDN\Build_Data\Latest\gta5\build\dev\common\data\version.txt

:buildverify
:: Parsing the studio name for the network and xex paths
for /f "tokens=*" %%a in ( 
'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
) do ( 
set studio=%%a
) 
if %studio%==north set networkbuildpath=N:RSGEDI\Projects\GTAV\Builds\Storage
if %studio%==toronto set networkbuildpath="N:\RSGTOR\Projects\GTAV\Builds\Storage
if %studio%==sandiego set networkbuildpath=N:\RSGSAN\Projects\GTAV\Builds\Storage
if %studio%==newengland set networkbuildpath=N:\RSGNWE\Projects\GTAV\Builds\Storage
if %studio%==leeds set networkbuildpath=N:\RSGLDS\Projects\GTAV\Builds\Storage
if %studio%==london set networkbuildpath=N:\RSGLDN\Projects\GTAV\Builds\Storage
if %studio%==newyork set networkbuildpath=N:\RSGNYC\Projects\GTAV\Builds\Storage
:NETWORK BUILD PATH SET: %networkbuildpath%

setlocal ENABLEDELAYEDEXPANSION
::Parsing the version.txt for new version number
set vidx=0
for /F "tokens=*" %%A in (X:\gta5\build\dev\common\data\version.txt) do (
    SET /A vidx=!vidx! + 1
    set var!vidx!=%%A
)
set var

::Commandline
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ commandline.txt

::Bank Release
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.bat
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.cmp
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.map
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.pdb
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.xdb
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.xex
robocopy  /DCOPY:T X:\gta5\build\dev\  %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_bankrelease.exe

::Beta
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.bat
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.cmp
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.exe
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.map
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.pdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.xdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_beta.xex

::Final
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.cmp
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.exe
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.map
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.pdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.xdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_final.xex

::Release
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.bat
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.cmp
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.exe
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.map
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.pdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.xdb
robocopy  /DCOPY:T X:\gta5\build\dev\ %networkbuildpath%\QAversion%var3%_xbox360\ game_xenon_release.xex

::Xbox360 Data
robocopy /S /DCOPY:T X:\gta5\build\dev\xbox360 %networkbuildpath%\QAversion%var3%_xbox360\xbox360\

::Xbox360 Common
robocopy /S /DCOPY:T X:\gta5\build\dev\common\ %networkbuildpath%\QAversion%var3%_xbox360\common\



pause