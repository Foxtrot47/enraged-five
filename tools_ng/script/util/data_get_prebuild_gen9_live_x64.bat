@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

::-- Batch file title bar
TITLE Getting GTA5_GEN9_PreBuild label for PC...
ECHO Getting GTA5_GEN9_PreBuild label for PC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

::--- Grab GEN9 Base Build ---
p4 sync //depot/gta5/build/dev_gen9_sga_live/common/...@GTA5_GEN9_PreBuild 2> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_gen9_sga_live/x64/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::--- Grab GEN9 TU Build ---
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/common/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/dlc_patch/*/common/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/dlc_patch/*/x64/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/dlc_patch/*/*.xml...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/launchers/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/xbsx_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/x64/...@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/*.xml@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/*.dll@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/*.txt@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/game_win64_*.*@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_gen9_sga_live/_launchgame.bat@GTA5_GEN9_PreBuild 2>> %RS_TOOLSROOT%/logs/x64_pc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	start notepad %RS_TOOLSROOT%/logs/x64_pc_sync.txt
)

::---------------------------------------------------------------
if %SKIP_PAUSE% NEQ 1 ( pause )

:END
