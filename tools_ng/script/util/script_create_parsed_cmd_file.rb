#
# Creates a formated list of commands that can be used to parse GTA script files using the old command format, swapping for the new commands 
#
# Author:: Thomas French
# Date:: 1 Oct 2008
#

#-----------------------------------------------------------------------------
# Function
#-----------------------------------------------------------------------------
#old command  - command previous to any change
#updated commands - commands that have char -> ped, car -> vehicle and vector change
#final commands -. had names standardised as well as updated

# changes from the old script commands to new: char -> ped, car -> vehicle, commmands return by value, format of commands has changed to be come standardied
#This uses 3 files to create the list, the GTA header file, the latest up to date header file and the relevamt user created commands audit file 
#Two sets of classes are created gta commands and the latest header files these are parsed form the relevant header files
#the old list then has all its char -ped substituions etc 
#this list is then run against the audited list (the audited list conatins of updated to final commands) to convert updated commands to final commands 
#The lis is chekced against the final command list (generated from our finla header file) any commands that dont mtach the final are maeked as being removed

REGEXP_UNDERLINE = (/_/)
REGEXP_COMMENT_LINES = /^[ \t]*\/\/.*/i

REGEXP_EXTRA_INFO = (/~XP/) 
REGEXP_PARAM_SWAP = (/~SP/)

REGEXP_REMOVE_CMD = (/~RC/)

REGEXP_LEGACY_CMD = (/~ac/i)

PED = "ped"
VEHICLE = "vehicle"

#these arrays should match in order

AUDIT_FILES = [ 'X:\docs\code\Script\Commands Audit\Audio Commands Audit.txt', 
				'X:\docs\code\Script\Commands Audit\Brains Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Clock Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Cutscene Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Debug  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Fire  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Gang  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Graphics  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\HUD  Commands Audit.txt', 
				'X:\docs\code\Script\Commands Audit\Misc  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Object  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Pad  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Path  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Ped  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Player  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Script  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Task  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Vehicle  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Weapon  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Zone  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Streaming  Commands Audit.txt',
				'X:\docs\code\Script\Commands Audit\Camera Commands Audit.txt'
				]
GTA_HEADER_FILES = ['X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_audio.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_brains.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_clock.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_cutscene.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_debug.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_fire.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_gang.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_graphics.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_hud.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_misc.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_object.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_pad.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_path.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_ped.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_player.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_script.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_task.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_vehicle.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_weapon.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_zone.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_streaming.sch',
					'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_camera.sch'
]				
					
JIMMY_HEADER_FILES =['X:\jimmy\script\dev\native\include\commands_audio.sch',
					'X:\jimmy\script\dev\native\include\commands_brains.sch',
					'X:\jimmy\script\dev\native\include\commands_clock.sch',
					'X:\jimmy\script\dev\native\include\commands_cutscene.sch',
					'X:\jimmy\script\dev\native\include\commands_debug.sch',
					'X:\jimmy\script\dev\native\include\commands_fire.sch',
					'X:\jimmy\script\dev\native\include\commands_gang.sch',
					'X:\jimmy\script\dev\native\include\commands_graphics.sch',
					'X:\jimmy\script\dev\native\include\commands_hud.sch',
					'X:\jimmy\script\dev\native\include\commands_misc.sch',
					'X:\jimmy\script\dev\native\include\commands_object.sch',
					'X:\jimmy\script\dev\native\include\commands_pad.sch',
					'X:\jimmy\script\dev\native\include\commands_path.sch',
					'X:\jimmy\script\dev\native\include\commands_ped.sch',
					'X:\jimmy\script\dev\native\include\commands_player.sch',
					'X:\jimmy\script\dev\native\include\commands_script.sch',
					'X:\jimmy\script\dev\native\include\commands_task.sch',
					'X:\jimmy\script\dev\native\include\commands_vehicle.sch',
					'X:\jimmy\script\dev\native\include\commands_weapon.sch',
					'X:\jimmy\script\dev\native\include\commands_zone.sch',
					'X:\jimmy\script\dev\native\include\commands_streaming.sch',
					'X:\jimmy\script\dev_cam_commands\native\include\commands_camera.sch'
]				
				
PARSED_FILES = [	'X:\jimmy\bin\util\data\script\parsedCommandsAudio.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsBrains.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsClock.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsCutscene.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsDebug.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsFire.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsGang.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsGraphics.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsHud.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsMisc.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsObject.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsPad.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsPath.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsPed.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsPlayer.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsScript.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsTask.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsVehicle.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsWeapon.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsZone.txt',
					'X:\jimmy\bin\util\data\script\parsedCommandsStreaming.txt', 
					'X:\jimmy\bin\util\data\script\parsedCommandsCamera.txt'		
					]				
				
				
				


#DATA_FILES = 'X:\docs\code\Script\Commands Audit\Ped  Commands Audit.txt'

#UPDATED_HEADER_FILES = 'X:\jimmy\script\dev\native\include\commands_ped.sch'

#OLD_HEADER_FILES = 'X:\tools_release\gta_bin\RageScriptCompiler\include_scripts\commands_ped.sch'

#Temp_out = 'X:\docs\code\Script\OutputTestSCFile.txt'

#class that represents the commands structure, 
class ScriptCommand
	attr_accessor :functiontype, :funcname, :params, :alteredfuncname, :updatedtoken, :bisvalidcmd, :subname, :bvaildsub #, :oldfuncname, :oldparams
	
	def initialize(function, name, params) #, oldparams, fncname)
		@functiontype = function		#bool that is set true if the command is a function   is the member variable
		@funcname = name				#string function name
		@params = params				#number of params in the function
	end
	
	def updatefuncname(newfuncname)
		@alteredfuncname = newfuncname	#new function name after char -> ped or car -> vehicle or a standard naming convention change
	end
	
	def substiutedname(subcmdname)
		@subname = subcmdname
	end
	
	def checksub (bcheck)	
		@bvaildsub = false
	end
	
	def updatefunctoken(updttoken)
		@updatedtoken = updttoken		#stores any token that have been added to the commands audit file 
	end
	
	def setvalidcmd(validcmd)
		@bisvalidcmd = false
	end	
end


#creates a hashed  dictionary of the command standardised function
def open_and_read_dictionary_file (auditfile)
command_names = {}   #defines a hash object

		begin
			#DATA_FILES.each do |inc_path|		#look thorugh all our data files and parse them
				File.open(auditfile) do |fp|	#open each file at a time  parse
					fp.each do |line|
						# Skip blank lines
						next if ( 0 == line.size )
						
						#Skip comment lines
						next if ( line =~ REGEXP_COMMENT_LINES)
							
						#throw Exception.new( 'testing' )
						#puts fp.lineno	
						if (line =~ REGEXP_UNDERLINE)
							command = line.split('->')		#splits the command on the symbol to create our regular expression 
							
							#r = Regexp.new(command[0].strip.upcase, true )  #inlcudes white space
							
							new_command = command[0].strip.upcase
							
							command_names[new_command] = command[1].strip.upcase	#fill the  object using the regular expression as the key 
							
							#puts new_command+"$"
							#puts command[1]
						
						end	
					end
				end
			#end
		rescue Exception => ex
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure			
			#fp.close()
		end
	return command_names
end

#swaps char and car to ped and vehicle 
def substitute_ped_and_vehicle (command_name, command_stuc )
	
	#char_func = (/[_][CHAR]+/).match(command)		#needs moded does deal with set_money_carried 
	#char_funca =  (/[CHAR]+[_]+/).match(command)
	char_func =	(/([_]?([c][h][a][r])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(command_name)
	
	if not char_func.nil? 
		update_command = command_name.gsub("CHAR", "PED")	#swap char tp ped
		command_stuc.bvaildsub = true
		#puts update_command + "$"
	else
		update_command = command_name
	end
	
	char_func =	(/([_]?([c][h][a][r][s])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(update_command)
	
	if not char_func.nil? 
		update_command = update_command.gsub("CHARS", "PEDS")	#swap char tp ped
		command_stuc.bvaildsub = true
	end
	
	
	veh_func = (/([_]?([v][e][h])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(update_command)
	
	if not veh_func.nil?
		update_command = update_command.gsub("VEH", "VEHICLE")	#swap car to vehicle
		command_stuc.bvaildsub = true
		
	end
	
	car_func = (/([_]?([c][a][r])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(update_command)		# check for valid instances of car
	
	if not car_func.nil?
		update_command = update_command.gsub("CAR", "VEHICLE")	#swap car to vehicle
		command_stuc.bvaildsub = true
	
	end
	
	car_func = (/([_]?([c][a][r][s])([^a-z]+|$))(($)|([\s]?[_]?))/i).match(update_command)
	
	if not car_func.nil?
		update_command = update_command.gsub("CARS", "VEHICLES")	#swap car to vehicle
		command_stuc.bvaildsub = true
		
	end
	
	update_command.chomp!
	update_command.strip!
	return update_command
end

#runs the substitute code
def swaps_ped_and_vehicle(input_data)
	begin
		input_data.each_with_index do |line, index|
			
			temp_command = line.funcname
			temp_command = temp_command.strip
			
			command = substitute_ped_and_vehicle(temp_command, line)
			
			line.alteredfuncname = command
			
			if (line.bvaildsub)		#keep track of the subed ped to char etc 
				line.subname = command
				#puts line.subname + "$"
			end
			
			#puts line.alteredfuncname + "$"
		
		end
	end
end

#fills the command objects with a parased file
def class_test_open_and_read_input_file (source_files)
data = []   #defines a hash object
func_type = []
		begin
			source_files.each do |inc_path|		#look thorugh all our data files and parse them
				File.open( inc_path ) do |fp|	#open each file at a time  parse
					fp.each do |line|
						# Skip blank lines
						line.chomp!
						next if ( 0 == line.size )
						
						#puts line
						
						#Skip comment lines
						next if( line =~ REGEXP_COMMENT_LINES)
							tempdata = ( /([A-Z_0-9]+([\s]*))\(/.match(line) ) #extracts the command name from the header file as it has no space in the name uses the bracket to terminate
							
						if not tempdata.nil? # we now have a command name	
							command_name = tempdata[1]
							command_name.chomp!
							command_name.strip!
						
							
							params = line.count ","
							params +=1
							
							#check that the command is a function
							func_type = ( /([\s]+[FUNC]+[\s])/i.match(line) ) 
								
							if not func_type.nil?
								functype = true
							else
								functype = false
							end
							data << ScriptCommand.new(functype,  command_name, params)
						end
					end	
				end
			end
	
		rescue Exception => ex
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure			
			#fp.close()
		end
	
	
	return data
end

#strips out the tokens from the parsed standard dictionary stores in the command class
def strip_out_symbols (command, func)
	if (command =~ (/~sp/i))
		command = command.gsub("~SP", "" )
		string = " ~SP"
		func.updatedtoken = string 		 
	end
	
	if (command =~ (/~xp/i))
		command = command.gsub("~XP", "" )
		string = " ~XP"
		func.updatedtoken = string 			
	end
	
	if (command =~ (/~rc/i))
		command = command.gsub("~RC", "" )		
	end
	
	if (command =~ (/[(]/i))
		command = command.gsub("(", "" )		
	end
	
	command.chomp!
	command.strip!
	
	return command
end

# update the list from the commands audit list so that all command names are standardised
def edit_updated_command_with_swapped_commands (updated_cmd_dict, input_data )
	begin	
	
		input_data.each_with_index do |line, index|
			updated_cmd_dict.each_pair do |regexp,replace|
			#	temp_rexp = Regexp.new(strip_out_symbols(regexp))		#strip out the bracket read in from the regular exprssion 
				
				#if (line.alteredfuncname =~ regexp)
				if (line.alteredfuncname.chomp.strip ==  regexp.chomp.strip  ) 	
					replace = strip_out_symbols(replace, line)
					replace.chomp.strip!
					line.alteredfuncname = line.alteredfuncname.gsub(regexp, replace) 
				end
			end
		puts line.funcname
		puts line.alteredfuncname
		
		end	
	end
end

#we  formatted list of up to date functions so that it can be parsed 
def create_output (update_command, new_command, update, removed_from_gta, keep_updated_cmd )
	

		if (update == true)		#fix this flag stoppping things getting printed
			if not (update_command.functiontype) #original command type
				if (new_command.functiontype)
					#the new command returns by value
					returns_by_value = "	~rbv"
		
				else
					returns_by_value = ""
				end
			else
				returns_by_value = ""
			end	
				
			if not (update_command.params == new_command.params)
				if not (returns_by_value=~ /~rbv/i )
					params_different = "	~xp"
					
				else
					params_different = ""
				end
			else
				params_different = ""
			end	
			if (update_command.updatedtoken == nil )
				update_command.updatedtoken = ""
			end
			
			if keep_updated_cmd
				#puts update_command.subname 
				command = update_command.subname + " 	-> 	" +  new_command.funcname + update_command.updatedtoken
			else
				#puts update_command.funcname
				command = update_command.funcname + " 	-> 	" +  new_command.funcname + returns_by_value + params_different + update_command.updatedtoken
			end
		else
			if not ( removed_from_gta) 
				removed_command = "	~rc"
			else
	
				removed_command = "	~TR"
			end
			command = update_command.funcname + " -> " +  new_command.funcname  + removed_command
		
		end
	
		
	return command 
end

# creates the list and writes it into a file 
def create_new_updated_list (updated_cmd, new_cmd, removed_cmd, output_file, format_output, inlcude_all )
	
	final_cmd_list = []
	
	begin
		updated_cmd.each_with_index do |ucline, udindex|
				altered = false
				included = false
				new_cmd.each_with_index do |ncline, ncindex|
					#checks that the updated commands are the sasme as the final command 
					
					if (ucline.alteredfuncname.chomp.strip == ncline.funcname.chomp.strip)
						if (ucline.bvaildsub)
							if not (ucline.subname.chomp.strip == ncline.funcname.chomp.strip)
								final_cmd_list << create_output(ucline, ncline, true, false, true )	#keep the char to ped substituition 
							end
						end
					
						if not (ucline.alteredfuncname.chomp.strip == ucline.funcname.chomp.strip)	#ignore function names  that havent changed from orginal to new
							final_cmd_list << create_output(ucline, ncline, true, false, false )		#keep updated form of the command
						end
						altered = true
					end
				end
				
				#include all commands including the removed ones
				if inlcude_all
					if not altered
						ucline.alteredfuncname = ucline.funcname
						removed_cmd.each_with_index do |rcline, rcindex|
							if (ucline.alteredfuncname.chomp.strip == rcline.funcname.chomp.strip)
								final_cmd_list << create_output(ucline, ucline, false, true, false )
								included = true
							end
						end
						if not included
							final_cmd_list << create_output(ucline, ucline, false, false, false )
						end
					end
				end	
		end
	
	
	
		begin 
			File.open( output_file , "w" ) do |fp|
					if not (format_output)
						final_cmd_list.each do |line|		
							fp.puts(line)			#use puts because the carrige returns have been removed by the chomp functions when formulating strings
						end
					else	
						final_cmd_list.each do |line|		
							if (line =~ /~rbv/i)  
								fp.puts(line)			#use puts because the carrige returns have been removed by the chomp functions when formulating strings
							end
						end
						
						final_cmd_list.each do |line|		
							if(line =~ /~xp/i) 
								if not (line =~ /~rbv/i) 
									fp.puts(line)			#place all the removed commands at the bottom
								end
							end	
						end
						
						final_cmd_list.each do |line|		
							if(line =~ /~sp/i) 
								fp.puts(line)			#place all the removed commands at the bottom
							end	
						end
					
						final_cmd_list.each do |line|		
							if not(line =~ /~xp/i) 
								if not (line =~ /~rbv/i) 
									if not (line =~ /~sp/i) 
										if not (line =~ /~rc/i)
											if not (line =~ /~TR/i)
												fp.puts(line)			#place all the removed commands at the bottom
											end
										end
									end
								end
							end	
						end
						
						final_cmd_list.each do |line|		
							if(line =~ /~rc/i) 
								fp.puts(line)			#place all the removed commands at the bottom
							end	
						end
					
						final_cmd_list.each do |line|		
							if(line =~ /~TR/i) 
								fp.puts(line)			#place all the removed commands at the bottom
							end	
						end
					end
			end	
		end
	end
end	


def check_user_input (user_input)
	
	selection = 99
	
	if (user_input == "audio" )
		selection = 0
	end

	if (user_input == "brains" )
		selection = 1
	end

	if (user_input == "clock" )
		selection = 2
	end

	if (user_input == "cutscene" )
		selection = 3
	end

	if (user_input == "debug" )
		selection = 4
	end

	if (user_input == "fire" )
		selection = 5
	end
	if (user_input == "gang" )
		selection = 6
	end
	if (user_input == "graphics" )
		selection = 7
	end
	if (user_input == "hud" )
		selection = 8
	end
	if (user_input == "misc" )
		selection = 9
	end
	if (user_input == "object" )
		selection = 10
	end
	if (user_input == "pad" )
		selection = 11
	end
	if (user_input == "path" )
		selection = 12
	end
	
	if (user_input == "ped" )
		selection = 13
	end
	
	if (user_input == "player" )
		selection = 14
	end
	if (user_input == "script" )
		selection = 15
	end
	if (user_input == "task" )
		selection = 16
	end
	if (user_input == "vehicle" )
		selection = 17
	end
	if (user_input == "weapon" )
		selection = 18
	end
	
	if (user_input == "zone" )
		selection = 19
	end
	
	if (user_input == "streaming" )
		selection = 20
	end
	
	if (user_input == "camera" )
		selection = 21
	end
	
return selection
end

def selectlisttype (input)
	
	if (input == "1")
		answer = true
	else
		if (input == "2" )
			answer = false
		else
			answer = true
		end
	end
	
return answer
end 

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
	final_commands = []
	updated_commands = []
	removed_commands = []
	select = 99
	
	# create the command dictionary for the updated comamnds
	removed_files = 'X:\docs\code\Script\Commands Audit\Removed Script Commands Audit.txt' 

	#command_dictionary = open_and_read_dictionary_file(audit_files) 

	run = 1
	
	while (run == 1)
		puts "Enter the command name"
		puts ""
		user_input = $stdin.gets.chomp
		
		if(user_input == "q")
			run = 0
		end
		select = check_user_input(user_input)

		puts "1 = full_list 2 = parsed_list "
		puts ""
		list_type = $stdin.gets.chomp
		
		list = selectlisttype(list_type)
		
		if not (select == 99 )
			audit_files = AUDIT_FILES[select]
			old_header_file	= GTA_HEADER_FILES[select]
			jimmy_header_file = JIMMY_HEADER_FILES[select]
			input = PARSED_FILES[select]
			
			command_dictionary = open_and_read_dictionary_file(audit_files) 
				
			#grab the old and new commands 
			final_commands = class_test_open_and_read_input_file(jimmy_header_file)
			updated_commands = class_test_open_and_read_input_file(old_header_file)
			removed_commands = class_test_open_and_read_input_file(removed_files)

			swaps_ped_and_vehicle(updated_commands)
			edit_updated_command_with_swapped_commands(command_dictionary, updated_commands )
			create_new_updated_list(updated_commands, final_commands,removed_commands,input, true, list)
			
			select = 99
		else
			puts("invalid selection")
		end
	end
	
end


