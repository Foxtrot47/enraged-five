@ECHO OFF
REM File:: %RS_TOOLSROOT%\script\util\data_clear_logs.bat
REM Description:: Clear tools log directory.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 7 November 20112
REM

CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %TOOLSIR% %RS_TOOLSROOT%\ironlib\util\data_clear_logs.rb

