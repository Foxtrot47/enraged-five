@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% GTA5: Getting mpPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting mpPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::
p4 sync //gta5_dlc/mpPacks/mpApartment/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpArmy/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpCNC/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHalloween/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSports/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatches/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLowrider2/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpExecutive/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpstunt/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpbiker/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpimportexport/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpAirraces/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpGunRunning/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpSpecialRaces/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/spUpgrade/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay7NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpxmas_604490/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/dlc_anim_test/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpJanuary2016/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines2/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay9NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay10NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay11NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay12NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay13NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay14NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay15NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay16NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::==========================================::


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause


