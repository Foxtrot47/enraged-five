@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Trailer dev: Getting Trailer labelled build for PS3...
ECHO %RS_PROJECT% Trailer dev: Getting Trailer labelled build for PS3...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/build/dev/common/...@[CB]_GTAV_Trailer_Build   %PERFORCE_ROOT%/build/dev/ps3/...@[CB]_GTAV_Trailer_Build  %PERFORCE_ROOT%/build/dev/game_psn_beta_snc.*@[CB]_GTAV_Trailer_Build %PERFORCE_ROOT%/build/dev/game_psn_bankrelease_snc.*@%[CB]_GTAV_Trailer_Build %PERFORCE_ROOT%/build/dev/game_psn_release_snc.*@[CB]_GTAV_Trailer_Build %PERFORCE_ROOT%/build/dev/*.sfo@[CB]_GTAV_Trailer_Build %PERFORCE_ROOT%/build/dev/*.png@[CB]_GTAV_Trailer_Build 

pause