REM
REM Builds a C# executable project
REM

@echo off

if not defined RS_TOOLSCONFIG (
	@echo.
	call setenv
	@echo.
)

@echo.
call %RS_TOOLSROOT%\script\util\projGen\generate.bat %*
@echo.
