@echo off
REM
REM Syncs to various files before project generation commences.
REM

if defined PROJGEN_SYNCED goto END
set PROJGEN_SYNCED="true"

@echo - Sync to latest project generator binaries. 
p4 sync %RS_TOOLSBIN%\projectgenerator\... 2>&1
@echo.
@echo - Sync to latest project generator configuration files. 
p4 sync %RS_TOOLSCONFIG%\projgen\... 2>&1
@echo.
@echo - Sync to latest project generator script files. 
p4 sync %RS_TOOLSROOT%\script\util\projgen\... 2>&1
@echo.

:END