REM
REM Builds a library for rage base src
REM

@echo off
@echo.
@echo ========================================================================= 
@echo ===================== PROJECT GENERATOR 3 STARTED ======================= 
@echo = https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 = 


if not defined RS_TOOLSCONFIG (
	@echo.
	call setenv
	@echo.
)

call %RS_TOOLSROOT%\script\util\projGen\sync.bat

@echo.
call %RS_TOOLSROOT%\script\util\projGen\generate.bat %*
@echo.

@echo ===================== PROJECT GENERATOR 3 COMPLETE ======================
@echo =========================================================================
@echo.
