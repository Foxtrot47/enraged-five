@echo off
@echo.
@echo =============================================================================
@echo === PG3 ProjGen : Makefile.txt C# recursive generations (IronRuby)        === 
@echo === https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 ===
@echo.

call setenv

ECHO.
ECHO ************
ECHO *** LIBS ***
ECHO ************
ECHO.

  set LIBS_DIRS=(RSG.Pipeline.Services^
		 RSG.Pipeline.Processor.Vehicle^
		 RSG.Pipeline.Processor.Texture^
		 RSG.Pipeline.Processor.Platform^
		 RSG.Pipeline.Processor.Material^
		 RSG.Pipeline.Processor.Map^
		 RSG.Pipeline.Processor.Common^
		 RSG.Pipeline.Processor.Animation.Networks^
 		 RSG.Pipeline.Processor.Animation.InGame^
 		 RSG.Pipeline.Processor.Animation.Cutscene^
		 RSG.Pipeline.Processor.Animation^
		 RSG.Pipeline.InstancePlacementProcessor^
		 RSG.Pipeline.FunctionalTests^
		 RSG.Pipeline.Engine^
		 RSG.Pipeline.Automation.Common^
		 RSG.Pipeline.Processor.Map.InstancePlacement)

  FOR %%I IN %LIBS_DIRS% DO (

    pushd %RAGE_DIR%\framework\tools\src\Libs\%%I
    echo     ------ %%I ------
    call %RS_TOOLSSCRIPT%\util\ProjGen\generateMakefile.bat
    echo     ------ %%I COMPLETE ------
    popd
  )

ECHO.
ECHO *****************
ECHO *** BASE LIBS ***
ECHO *****************
ECHO.


  set BASE_DIRS=(RSG.Rage)

  FOR %%I IN %BASE_DIRS% DO (
    echo     ------ %%I ------    
    pushd %RAGE_DIR%\base\tools\libs\%%I
    call %RS_TOOLSSCRIPT%\util\ProjGen\generateMakefile.bat
    echo     ------ %%I COMPLETE ------
    popd
  )
  
ECHO.
ECHO ***********
ECHO *** CLI ***
ECHO ***********
ECHO.

  set CLI_DIRS=(RSG.Pipeline.Animation.ClipGroup^
  		RSG.Pipeline.Animation.Coalesce^
	  	RSG.Pipeline.Animation.Compress^
	  	RSG.Pipeline.Animation.ClipDictionary^
	  	RSG.Pipeline.Animation.Cutscene.CutsceneConcatenation^
	  	RSG.Pipeline.Animation.Cutscene.CutsceneConcatenationLighting^
	  	RSG.Pipeline.Animation.Cutscene.FinaliseCutfile^
	  	RSG.Pipeline.Animation.Cutscene.InjectDof^
	  	RSG.Pipeline.Animation.Cutscene.MergeExternal^
	  	RSG.Pipeline.Animation.Cutscene.MergeFacial^
	  	RSG.Pipeline.Animation.Cutscene.MergeLights^
	  	RSG.Pipeline.Animation.Cutscene.MergeSubtitle^
	  	RSG.Pipeline.Animation.Cutscene.PostProcess^
	  	RSG.Pipeline.Animation.Cutscene.Section^
	  	RSG.Pipeline.Animation.Cutscene.SliceDice^
	  	RSG.Pipeline.Animation.DictionaryMetadata^
	  	RSG.Pipeline.Animation.Postprocess^
	  	RSG.Pipeline.Automation.ClientWorker^
	  	RSG.Pipeline.Automation.Console^
	  	RSG.Pipeline.Automation.ServerHost^
	        RSG.Pipeline.AssetPack^
	  	RSG.Pipeline.AssetProcessor^
	  	RSG.Pipeline.BoundsProcessor^
	  	RSG.Pipeline.CollisionProcessor^
	  	RSG.Pipeline.Convert^
	  	RSG.Pipeline.LeakTest^
	  	RSG.Pipeline.MapAssetCombine ^
		RSG.Pipeline.MapExportMetadata ^
	  	RSG.Pipeline.MaterialPresetConverter^
	  	RSG.Pipeline.Rage.FragmentGenerator^
	  	RSG.Pipeline.RpfCreate^
	  	RSG.Pipeline.RpfManifest^
	  	RSG.Pipeline.SoakTest^
	  	RSG.Pipeline.TextureExport^
	  	RSG.Pipeline.ZipCreate^
	  	RSG.Pipeline.ZipCreate^
	  	RSG.Pipeline.ZipExtract)
  FOR %%I IN %CLI_DIRS% DO (
    pushd %RAGE_DIR%\framework\tools\src\cli\%%I
    echo     ------ %%I ------
    call %RS_TOOLSSCRIPT%\util\ProjGen\generateMakefile.bat
    echo     ------ %%I COMPLETE ------
    popd
  )

@echo.
@echo === PG3 Generate Makefiles Complete ===
@echo =======================================
@echo.

:END
pause
