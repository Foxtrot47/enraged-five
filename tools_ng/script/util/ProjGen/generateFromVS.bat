@echo off

if not defined RS_TOOLSCONFIG (
	@echo.
	call setenv
	@echo.
)

%RS_TOOLSBIN%\ProjectGenerator\ProjectGenerator.exe --nopopups %*