@echo off
SETLOCAL
@echo.
@echo =============================================================================
@echo === PG3 ProjGen : Recursive Project Generation (IronRuby)                 === 
@echo === https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3 ===
@echo.

REM sync to latest project generator config and binaries - only during development, do not remove I'll change sync.bat when no longer reqd.
call %RS_TOOLSROOT%\script\util\projGen\sync.bat

call setenv

REM -----------------------------------------------------------------------------------------------------
REM Call an ironruby script that recursively scans for projgen.config files and all recognised makefiles
REM - it then produces a series of local *.makefiles files. 
REM - each file contains the correct build directive to build the makefiles with the appropriate settings
REM -----------------------------------------------------------------------------------------------------

call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%/ironlib/util/projGen/findMakefiles.rb --exclude \/game\/

REM -----------------------------------------------------------------------------------------------------
REM for each *.makefiles file convert with the project generator
REM - DW: TODO project generator should be able to handle multiple 'makefile' files?!
REM -----------------------------------------------------------------------------------------------------

for %%f in (*.makefiles) do (
	title projGen Converting %%f 
	echo.
	echo --- projGen Converting %%f --- 
	echo.
	echo     %RS_TOOLSROOT%\bin\projectgenerator\projectgenerator.exe --changelist --verify %%f
	echo.
 	%RS_TOOLSROOT%\bin\projectgenerator\projectgenerator.exe --changelist --verify %%f
	erase %%f 2>nul	
)

@echo.
@echo === PG3 ProgGenComplete ===
@echo ===========================
@echo.

:END
ENDLOCAL
