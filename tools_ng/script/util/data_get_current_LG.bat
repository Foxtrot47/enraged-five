@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting [CB]_GTAV_current labelled build...
ECHO %RS_PROJECT% dev: Getting [CB]_GTAV_current labelled build...

REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	::for /f %%i in ('%RS_TOOLSIRONLIB%\bin\RSG.Pipeline.WhatIsMyStudio.exe') do set studio=%%i
	::echo Studio is %studio%

	::if %studio%==northbarclay set networkbuildpath="N:\RSGEDI\Build Data\Latest\gta5\build\dev"
	::if %studio%==toronto set networkbuildpath="N:\RSGTOR\Build_Data\Latest\gta5\build\dev"
	::if %studio%==sandiego set networkbuildpath="N:\RSGSAN\Build_Data\Latest\gta5\build\dev"
	::if %studio%==newengland set networkbuildpath="N:\RSGNWE\Build_Data\Latest\gta5\build\dev"
	::if %studio%==leeds set networkbuildpath="N:\RSGLDS\Build_Data\Latest\gta5\build\dev"
	::if %studio%==london set networkbuildpath="N:\RSGLDN\Build_Data\Latest\gta5\build\dev"
	::if %studio%==newyork set networkbuildpath="N:\RSGNYC\Build_Data\Latest\gta5\build\dev"
	::if %studio%==glencove set networkbuildpath="N:\RSGLIC\Build_Data\Latest\gta5\build\dev"

	::echo NETWORK BUILD PATH SET: %networkbuildpath%

	::set localbuildpath=X:\gta5\build\dev

set syncerror=0

if not exist %RS_TOOLSROOT%/logs/ (
	mkdir %RS_TOOLSROOT%/logs/
)

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	:: - Sync local network stored platform data -
	:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
	:: ROBOCOPY info. /S copies non-empty subfolders, /DCOPY:T keeps source timestamps
	::robocopy /S /DCOPY:T %networkbuildpath%\ps3\ %localbuildpath%\ps3\ *.* 2> %RS_TOOLSROOT%/logs/build_DATA_sync.txt
	::robocopy /S /DCOPY:T %networkbuildpath%\xbox360\ %localbuildpath%\xbox360\ *.* 2>> %RS_TOOLSROOT%/logs/build_DATA_sync.txt

p4 sync %PERFORCE_ROOT%/xlast/...@[CB]_GTAV_current 2> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/common/...@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
p4 sync %PERFORCE_ROOT%/build/dev/ps3/...@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/xbox360/...@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/assets/export/...xml@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/TROPDIR/...@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/*.*@[CB]_GTAV_current 2>> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync %PERFORCE_ROOT%/titleupdate/dev/*.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/dlc_patch/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/xbox360/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/*.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/dlc_patch/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/xbox360/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


p4 sync //gta5_dlc/mpPacks/mpBeach/build/dev/...@GTA5_network_demo 2> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe2/build/dev/...@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe2/*.*@GTA5_network_demo 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
if "%BUILD_MACHINE%"=="1" (
	goto:END
)



::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/build_sync.txt
REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
::	notepad %RS_TOOLSROOT%/logs/build_DATA_sync.txt
	notepad %RS_TOOLSROOT%/logs/tu_sync.txt
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)

::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
	echo Starting Rag proxy.
	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (
start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus
)
::---------------------------------------------------------------

if "%SKIP_PAUSE%"=="1" (
	goto:POST_PAUSE
)
pause
:POST_PAUSE