@ECHO OFF
REM 
REM Publish codebuilder binaries
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

CALL setenv.bat
TITLE Publish Binaries %RS_PROJECT% 

PUSHD %RS_TOOLSLIB%\util
REM --enable_checkin
ruby publish_binaries.rb --filespec=N:\RSGEDI\Builders\CruiseControl\live\codebuilder_gta5_dev_xbox360_build_beta\builds\latest\*.* --publish_folder=%RS_ASSETS%\cruisecontrol_builds\codebuilder\%1
POPD

pause

REM Publish_binaries.bat
