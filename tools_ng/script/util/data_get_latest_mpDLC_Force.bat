@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% GTA5: Getting mpPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting mpPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::

p4 sync -f //gta5_dlc/mpPacks/mpChristmas3/build/dev_ng/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


p4 sync -f //gta5_dlc/patchPacks/patchDay28NG/build/... 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::==========================================::


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause


