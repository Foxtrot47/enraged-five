#
# File:: data_get_labelled_build.rb
# Description:: Project-specific get labelled build.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 19 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/path'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
PROJECT_KEY = 'jimmy'
CONFIG = Pipeline::Config::instance()
SCRIPT_PATH = Pipeline::OS::Path.combine( CONFIG.toolsbin, 'util', 'data_get_labelled_build.rb' )

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

    command = "ruby #{SCRIPT_PATH} --project=#{PROJECT_KEY}"
    exec( command )
end

# data_convert_platform_local.rb
