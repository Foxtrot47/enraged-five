@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...
ECHO %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


p4 sync %PERFORCE_ROOT%/titleupdate/dev/*.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/dlc_patch/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/xbox360/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/*.*@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/common/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/dlc_patch/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/ps3/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/TROPDIR/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/xbox360/...@%PERFORCE_NETWORK_DEMO_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpMapWorking (
p4 sync //gta5_dlc/mpPacks/mpMapWorking/build/dev/... >> %RS_TOOLSROOT%/logs/TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpMapWorking (
p4 sync //gta5_dlc/mpPacks/mpMapWorking/*.* >> %RS_TOOLSROOT%/logs/TU_sync.txt
if errorlevel 1 set syncerror=1
)

if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/tu_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause

:END
