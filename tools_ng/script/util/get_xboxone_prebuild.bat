@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA5_NG_PreBuild label for Xbox One...
ECHO Getting GTA5_NG_PreBuild label for Xbox One...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab Main Build
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_Prebuild 2> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xboxone/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xbo_loose/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xbo_scripts/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_durango_*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab NG TU
::TitleUpdate - DEV_NG
p4 sync //depot/gta5/titleupdate/dev_ng/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xbo_loose/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xbo_scripts/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xboxone/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/game_durango_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::TitleUpdate - DEV_TEMP
::p4 sync //depot/gta5/titleupdate/dev_Temp/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/xbo_loose/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/xbo_scripts/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/xboxone/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/game_durango_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //depot/gta5/titleupdate/dev_Temp/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)

:: Grab DLC compat packs


::mptuner
if exist x:\gta5_dlc\mpPacks\mpTuner (
p4 sync //gta5_dlc/mpPacks/mpTuner/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpTuner (
p4 sync //gta5_dlc/mpPacks/mpTuner/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)

::mpPatchesNG
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


::CLIFFORD
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::spUpgrade
p4 sync //gta5_dlc/spPacks/spUpgrade/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/spUpgrade/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



::patchDay23NG
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay25NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay25NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
)

)
::---------------------------------------------------------------

pause

:END
