@ECHO OFF
REM Bootstrap.bat
REM Description : General Purpose Control Logic for bootstrapping a process.
REM 		  - uses an ironruby script to handle process exit codes
REM		  - process exits codes should use RSG.Pipeline.Core.Constants
REM Author : Derek Ward <derek@rockstarnorth.com>
REM Date : 19 March 2013

SET CALLER=%0 
SET RS_BOOTSTRAP_NAME=%1
SHIFT
SET BOOTSTRAP_SCRIPT=%1
SHIFT
SET PROCESS=%1
SHIFT

SET ARGS=%1
:ARG_LOOP
SHIFT
if [%1]==[] GOTO :EXIT_ARG_LOOP
SET ARGS=%ARGS% %1
GOTO ARG_LOOP
:EXIT_ARG_LOOP

ECHO *************************************************************************************
ECHO BOOTSTRAP %CALLER% (%RS_BOOTSTRAP_NAME%)
ECHO BOOTSTRAP_SCRIPT %BOOTSTRAP_SCRIPT%
ECHO PROCESS %PROCESS%
ECHO ARGS %ARGS%
ECHO *************************************************************************************

SET /a RESUME_COUNT=0

GOTO START

:RESUME
ECHO.
ECHO *************************************************************************************
ECHO %PROCESS% %ARGS% 
ECHO resume #%RESUME_COUNT% %date%%time%
ECHO *************************************************************************************
ECHO.
GOTO :START_PROCESS

:START
ECHO.
ECHO *************************************************************************************
ECHO %PROCESS% %ARGS%
ECHO start %date%%time%
ECHO *************************************************************************************
ECHO.
GOTO :START_PROCESS

:START_PROCESS
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %PROCESS% %ARGS%
SET PROCESS_RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %PROCESS_RETCODE%

IF %PROCESS_RETCODE%==-532462766 (	

	ECHO.
	ECHO *************************************************************************************
	ECHO Unhandled Exception, %PROCESS% %ARGS%
	ECHO Bootstrapping will now stop.
	ECHO *************************************************************************************
	SET BOOTSTRAP_RETCODE=%PROCESS_RETCODE%
	GOTO :EXIT
)

:BOOTSTRAP
CALL %RS_TOOLSROOT%/ironlib/prompt.bat %RS_TOOLSIR% %BOOTSTRAP_SCRIPT% %ARGS% %PROCESS_RETCODE%
SET BOOTSTRAP_RETCODE=%ERRORLEVEL%
ECHO %BOOTSTRAP_SCRIPT% RETCODE: %BOOTSTRAP_RETCODE%

:SYNC TO TOOLS (IF REQUIRED)
CALL %RS_TOOLSROOT%/tmp/%RS_BOOTSTRAP_NAME%/p4_automation_tools_sync.bat

IF %BOOTSTRAP_RETCODE%==1 (
	SET /a RESUME_COUNT=RESUME_COUNT+1
	GOTO :RESUME
)

:EXIT
ECHO.
ECHO *************************************************************************************
ECHO %PROCESS% (%BOOTSTRAP_SCRIPT%) exiting
ECHO *************************************************************************************
ECHO.
ping -n 10 127.0.0.1 > nul
EXIT /B %BOOTSTRAP_RETCODE%