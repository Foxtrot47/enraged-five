@ECHO OFF

CALL %RS_TOOLSROOT%\script\util\check_admin_privileges.bat
IF ERRORLEVEL 1 (
	IF "%1"=="error" (
		GOTO ERROR_END
	) ELSE (
		powershell "saps -FilePath %0 -ArgumentList error -Verb runas"
		exit /b
	)
)
cd /d %~dp0
set basesyncerror=0
CALL data_get_current_x64_build.bat nopause

p4 sync //depot/gta5/build/dev_ng/common/shaders/...#head 2>> %RS_TOOLSROOT%/logs/x64_pc_sync_shader.txt
IF %ERRORLEVEL% EQU 1 (
	set basesyncerror=1
)

IF %basesyncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %basesyncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	start notepad %RS_TOOLSROOT%/logs/x64_pc_sync_shader.txt
)
pause

:ERROR_END
:END_BAT