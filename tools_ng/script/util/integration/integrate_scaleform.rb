#
# File:: integrate_scaleform.rb
# Description:: Utility script for handling the Scaleform portion of our integrations.
#
# Author:: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
# Date::18 June 2012
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/projectutil/data_zip'
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/file'
require 'pipeline/os/path'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ "--input", "-i", OS::Getopt::REQUIRED, "Input directory." ],
	[ "--output", "-o", OS::Getopt::REQUIRED, "Output directory." ],
	[ "--help", "-h", OS::Getopt::BOOLEAN, "display usage information." ]
]
TRAILING_DESC = ''

FLA_EXTENSION = ".fla"
XFL_EXTENSION = ".xfl"
ZIP_EXTENSION = ".zip"
ORIGINAL_CONTENTS_DIR = "source"
ZIP_CONTENTS_DIR = "zip_contents"

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

	#-------------------------------------------------------------------------
	# Entry-Point
	#-------------------------------------------------------------------------

	begin
		g_AppName = File::basename( __FILE__, '.rb' )
		g_Config = Pipeline::Config.instance( )
	
		#---------------------------------------------------------------------
		# Parse Command Line
		#---------------------------------------------------------------------
		opts, trailing = OS::Getopt.getopts( OPTIONS )
		if ( opts['help'] or opts['input'].nil? or opts['output'].nil?) then
			puts OS::Getopt.usage( OPTIONS, { 'files' => TRAILING_DESC } )
			exit( 1 )
		end
		
		output_directory = opts['output']
		if File.exists?(output_directory) == true then
			FileUtils.rm_rf(output_directory)
		end
		FileUtils.mkdir_p(output_directory)
		
		input_file_path = OS::Path::combine(opts['input'], "*" + FLA_EXTENSION)
		input_files = []
		
		Find.find("#{opts['input']}\\") do |f|
			extension = File.extname f
			if extension.casecmp(FLA_EXTENSION) == 0 then
				input_files << f
			end
		end
		
		OS::Path::set_downcase_on_normalise(false)
		
		input_files.each do |input_file|
		
			print "Processing #{input_file}\n"
			input_file_name = File.basename(input_file, File.extname(input_file))
			#First copy to the output folder.
			sub_directory = File.dirname(input_file).gsub(opts['input'], "")
			output_dir = OS::Path.combine(output_directory, sub_directory)
			output_filename = OS::Path.combine(output_dir, input_file_name) + ZIP_EXTENSION
			
			FileUtils.mkdir_p(output_dir)
			print "Output File: #{output_filename}\n"
			FileUtils.cp(input_file, output_filename)
			FileUtils.chmod(0777, output_filename)

			#Extract the zip.
			zip_contents_directory = OS::Path.combine(output_directory, sub_directory, input_file_name)
			ProjectUtil::data_zip_extract( output_filename, zip_contents_directory, true ) do |filename|
				print "  #{filename}\n" 
			end
			
			xfl_artifact_exceptions = []
			xfl_artifact_exceptions << OS::Path.combine(zip_contents_directory, input_file_name + XFL_EXTENSION)
			
			xfl_artifacts = OS::FindEx::find_files( OS::Path::combine( zip_contents_directory, "*" + XFL_EXTENSION) )
			xfl_artifacts.each do |xfl_artifact|
			
				delete = true
				xfl_artifact_exceptions.each do |exception|
					
					if xfl_artifact.casecmp(exception) == 0 then
						
						delete = false
						break
					end
				end
				
				if delete == true then
					
					print "Deleting the artifact #{xfl_artifact}.\n"
					File.delete(xfl_artifact)
				end
			end
			
			File.delete(output_filename)
		end
		
	rescue Exception => ex
		exit( ex.status ) if ( 'exit' == ex.message )
	
		puts "\n#{g_AppName} unhandled exception: #{ex.message}"
		puts ex.backtrace.join("\n\t")		
	end	
end
