@ECHO OFF
REM 
REM Clears tmp directory of codebuilding artifacts
REM Derek Ward <derek.ward@rockstarnorth.com>
REM

CALL setenv.bat
TITLE %RS_PROJECT% clear tmp

PUSHD %TMP%
erase make*.*
erase msbuild*.*
erase *.rsp
erase *.tmp
erase *.exec*
POPD
