@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for Xbox360...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for Xbox360...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_PREBUILD_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/360_prebuild_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_prebuild_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/xbox360/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_prebuild_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/game_xenon_bankrelease.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_prebuild_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/game_xenon_beta.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/360_prebuild_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
	exit /B
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	%RS_TOOLSROOT%/logs/360_prebuild_sync.txt
	pause
	exit
)

exit