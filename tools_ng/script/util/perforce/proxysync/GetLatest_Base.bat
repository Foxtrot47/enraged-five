@echo off

if "%SYNC_PLATFORM%" == "" (
	echo Please specify SYNC_PLATFORM [x64, ps4, xboxone].
	goto end
)

if "%SYNC_TITLE%" == "gta5_liberty" (
  set SYNC_TITLE=gta5
  set UPDATE_DEPOT_ROOT=//gta5_liberty/
)

if "%SYNC_TITLE%" == "gta5" (
  set DEPOT_ROOT=//depot/gta5/
  goto Start
)

if "%SYNC_TITLE%" == "rdr3" (
  set DEPOT_ROOT=//rdr3/
  goto Start
)
   
echo Please set SYNC_TITLE to the title [gta5 or rdr3].
goto end



:Start

if "%LATEST_CL_FILE%" == "" (
	echo Please specify your latest CL file as LATEST_CL_FILE
	goto end
)

if "%LOG_DIR%" == "" (
	echo No LOG_DIR set for remote logs.  Please specify LOG_DIR as a remote path.  Defaulting to local
	set LOG_DIR=X:\
)

set LOG_FILE=%LOG_DIR%%USERNAME%_proxy_sync.log

set LOCAL_ERROR_LOG=%TEMP%\p4grab.log

rem Delete the previous logfile
touch %LOCAL_ERROR_LOG%
del /q /f %LOCAL_ERROR_LOG%


rem Grab latest CL

IF NOT EXIST "%LATEST_CL_FILE%" (
	echo !!!%LATEST_CL_FILE% doesn't exist - something broke, or your network is down.
	GOTO End
)

FOR /F %%i in (%LATEST_CL_FILE%) DO (
	SET LAST_CL=%%i
)

if "%LAST_CL%" == "" (
	echo An error occurred while trying to grab the latest CL. Please contact IT and Krehan.
	goto End
)

if "%LAST_CL%" == "@ECHO OFF" (
	echo There's a temporary error on the server side. This will resolve in a few minutes, please try again then. Contact IT if this problem persists.
	goto End
)

if "%LAST_CL%" == "ECHO is OFF." (
	echo There's a temporary error on the server side. This will resolve in a few minutes, please try again then. Contact IT if this problem persists.
	goto End
)

set LABEL_SUFFIX=@%LAST_CL%
set CODE_LABEL_SUFFIX=%LABEL_SUFFIX%

if "%SYNC_HEAD_ON_CODE%" == "1" (
	set CODE_LABEL_SUFFIX=
)

echo >>%LOG_FILE% %DATE% %TIME% Getting latest code, CL %LAST_CL%


if "%SYNC_RESET_SYSTRAYRFS%" == "1" (
	echo Resetting SysTrayRFS...
	X:\%SYNC_TITLE%\tools_ng\bin\sysTrayRfs.exe -reset
)

if "%SYNC_LEVELS_FOLDER_ONLY%" == "1" (
	for %%A IN (%SYNC_PLATFORM%) DO (
		title Syncing LEVEL assets for %%A
		echo Syncing %%A LEVELS FOLDER ONLY to CL %LAST_CL%...
		p4 sync %DEPOT_ROOT%build/dev_ng/%%A/levels/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	)
	msg %USERNAME% You now have latest LEVEL assets as of CL %LAST_CL%. Congratulations.
	goto End
)

if "%SYNC_SKIP_CODE%" == "1" (
	echo >>%LOG_FILE% %DATE% %TIME% Skipping code
	goto CodeDone
)

echo >>%LOG_FILE% %DATE% %TIME% Getting latest code, CL %LAST_CL%

echo Syncing code to CL %LAST_CL%...
title Syncing Code....

x:
pushd \%SYNC_TITLE%\src\dev_ng\rage
p4 sync //rage/%SYNC_TITLE%/...%CODE_LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"

if "%SYNC_RESOLVE%" == "1" (
	p4 resolve -am //rage/%SYNC_TITLE%/... 2>>"%LOCAL_ERROR_LOG%"
)

popd

pushd \%SYNC_TITLE%
p4 sync %DEPOT_ROOT%src/...%CODE_LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"

p4 sync %DEPOT_ROOT%tools_ng/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
p4 sync %DEPOT_ROOT%xlast/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"


if "%SYNC_RESOLVE%" == "1" (
	p4 resolve -am %DEPOT_ROOT%src/... 2>>"%LOCAL_ERROR_LOG%"
)

popd

if "%SYNC_REBUILD_PARSER%" == "1" (
	pushd \%SYNC_TITLE%\src\dev_ng
	if "%SYNC_TITLE%" == "gta5" (
		echo Invoking parser file builder for GTA5
		start %RS_TOOLSROOT%\script\coding\buildparser.bat
	) else (
		echo Invoking parser file rebuilder...
		start %RS_TOOLSROOT%\script\coding\rebuild_parser_files.rb
	)
)

pushd x:\%SYNC_TITLE%\src\dev_ng\game
p4 sync //ps3sdk/... 2>>"%LOCAL_ERROR_LOG%"

:CodeDone

if "%SYNC_SKIP_ASSETS%" == "1" (
	echo >>%LOG_FILE% %DATE% %TIME% Skipping assets
	goto AssetsDone
)

echo >>%LOG_FILE% %DATE% %TIME% Getting latest assets, CL %LAST_CL%
echo Syncing assets to CL %LAST_CL%

title CODE IS UP-TO-DATE - Start compiling! Grabbing assets now...

if "%SYNC_SCRIPTS%" == "1" (
	p4 sync %DEPOT_ROOT%script/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	title SCRIPTS ARE UP-TO-DATE - Grabbing build now...
)

if "%SYNC_DISKIMAGES%" == "1" (
	p4 sync %DEPOT_ROOT%build/disk_images/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
)

p4 sync %DEPOT_ROOT%build/dev_ng/common/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
p4 sync %DEPOT_ROOT%build/dev_ng/update/common/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
p4 sync %DEPOT_ROOT%titleupdate/dev_ng/common/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
if not "%UPDATE_DEPOT_ROOT%" == "" (
	p4 sync %UPDATE_DEPOT_ROOT%build/dev_ng/common/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
)
for %%A IN (%SYNC_PLATFORM%) DO (
	echo >>%LOG_FILE% %DATE% %TIME% Syncing %%A, CL %LAST_CL%
	p4 sync %DEPOT_ROOT%build/dev_ng/%%A/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	p4 sync %DEPOT_ROOT%build/dev_ng/update/%%A/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	p4 sync %DEPOT_ROOT%titleupdate/dev_ng/%%A/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	if not "%UPDATE_DEPOT_ROOT%" == "" (
		p4 sync %UPDATE_DEPOT_ROOT%build/dev_ng/%%A/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	)

	if %%A == "ps4" (
		p4 sync %DEPOT_ROOT%build/dev_ng/sce_*/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		p4 sync %DEPOT_ROOT%titleupdate/dev_ng/sce_*/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	)

	if %%A == "xboxone" (
		p4 sync %DEPOT_ROOT%build/dev_ng/xbo_loose/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		p4 sync %DEPOT_ROOT%build/dev_ng/xbo_scripts/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		p4 sync %DEPOT_ROOT%titleupdate/dev_ng/xbo_loose/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		p4 sync %DEPOT_ROOT%titleupdate/dev_ng/xbo_scripts/...%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
	)
)

:AssetsDone

if "%SYNC_EXECUTABLES%" == "1" (
	title ASSETS COMPLETE - SYNCING EXECUTABLES NOW
	for %%A IN (%SYNC_PLATFORM%) DO (
		if %%A == "xboxone" (
			p4 sync %DEPOT_ROOT%build/dev_ng/game_durango*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
			p4 sync %DEPOT_ROOT%titleupdate/dev_ng/game_durango*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		)
		
		if %%A == "ps4" (
			p4 sync %DEPOT_ROOT%build/dev_ng/game_orbis*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
			p4 sync %DEPOT_ROOT%titleupdate/dev_ng/game_orbis*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		)
		
		if %%A == "x64" (
			p4 sync %DEPOT_ROOT%build/dev_ng/game_win64*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
			p4 sync %DEPOT_ROOT%titleupdate/dev_ng/game_win64*%LABEL_SUFFIX% 2>>"%LOCAL_ERROR_LOG%"
		)
	)
)

p4 sync //3rdparty/... 2>>"%LOCAL_ERROR_LOG%"

echo >>%LOG_FILE% %DATE% %TIME% SYNC COMPLETE.
echo ALL DONE!

echo >>x:\LastSync.txt Last successful %SYNC_TITLE% sync (%SYNC_PLATFORM%) to CL %LAST_CL% on %DATE% %TIME%.

x:\%SYNC_TITLE%\tools_ng\bin\tail x:\LastSync.txt >x:\LastSync_.txt
del x:\LastSync.txt
ren x:\LastSync_.txt LastSync.txt

echo *****************************
echo MESSAGE LOG
echo *****************************
type "%LOCAL_ERROR_LOG%"

title CODE AND ASSETS UP-TO-DATE.
p4 describe -s %LAST_CL% > TEMP_CL_INFO.TXT
set CL_TIME="from Unknown Time"
FOR /F "tokens=5,6,7" %%A in (TEMP_CL_INFO.TXT) do IF "%%A" == "on" ( set CL_TIME= from %%B %%C )

msg %USERNAME% You now have latest code and assets as of CL %LAST_CL% %CL_TIME%. Congratulations. Please see the TTY for error messages that came up during the sync.
pause

popd


:End
