@echo off

set USAGE=usage: p4SyncToRevAfterPurge.bat [any number of valid P4 paths] [OPTIONAL: -noprompt]

if "%*" equ "" (
	echo.
	echo %USAGE%
	goto done
)

if /I "%*" equ "-noprompt" (
	echo.
	echo %USAGE%
	goto done
)

p4SyncToRevAfterPurge.rb %*

:done
echo.
pause