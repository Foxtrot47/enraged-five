#! /usr/bin/perl -w

use strict;

my @files;
my @revert;

my $maxFiles = 100;

if (@ARGV < 1 || $ARGV[0] eq 'default')
{
	print("Reading default changelist...\n");
    open CHANGELIST, "p4 change -o |";
}
else
{
	print("Reading changelist $ARGV[0]...\n");
    open CHANGELIST, "p4 change -o $ARGV[0] |";
}

while (<CHANGELIST>)
{
	last if (/^Files:/);
}

while (<CHANGELIST>)
{
	push(@files, $1) if (/^\t(.*)\t# (edit|add|delete|branch|integrat)/);
}

close CHANGELIST;



while (@files)
{
	my @someFiles;

	if (@files > $maxFiles)
	{
		@someFiles = @files[0..$maxFiles-1];
		@files = @files[$maxFiles..$#files];
	}
	else
	{
		@someFiles = @files;
		@files = ();
	}



	my @unresolved;

	print("Auto-resolving all files scheduled for resolve (this may take awhile)...\n");

	open RESOLVE, 'p4 resolve -am ' . join(' ', map { "\"$_\"" } @someFiles) . '|';

	while (<RESOLVE>)
	{
		push(@unresolved, $1) if (/(\/\/.*) - resolve skip/);
	}

	close RESOLVE;



	print("Looking up depot path for all unresolved files...\n");

	my %skip;

	open LOOKUP, 'p4 files ' . join(' ', map { "\"$_\"" } @unresolved) . '|';

	while (<LOOKUP>)
	{
		chomp;
		s/\#.*//;
		$skip{$_} = 1;
	}

	close LOOKUP;



	print("Diffing all open files...\n");

	open DIFF, 'p4 diff ' . join(' ', map { "\"$_\"" } @someFiles) . '|';

	my $file;

	while (<DIFF>)
	{
		if (/==== (\/\/.*)#[0-9]/)
		{
			if ($file)
			{
				if ($skip{$file})
				{
					print("$file needs resolving, not reverting\n");
				}
				else
				{
					print("$file has no changes, will revert\n");
					push(@revert, $file);
				}
			}
			$file = $1;
		}
		else
		{
			print("$file has changes, not reverting\n") if ($file);
			undef $file;
		}
	}

	if ($file)
	{
		if ($skip{$file})
		{
			print("$file needs resolving, not reverting\n");
		}
		else
		{
			print("$file has no changes, will revert\n");
			push(@revert, $file);
		}
	}

	close DIFF;
}



if (@revert == 0)
{
	print("No files will be reverted because all files have been changed or deleted or are new (ADD or BRANCH) files.\n");
}
else
{
	print("Reverting all unchanged files not needing to be resolved...\n");
}

while (@revert > $maxFiles)
{
	system('p4', 'revert', @revert[0..$maxFiles-1]);
	@revert = @revert[$maxFiles..$#revert];
}

system('p4', 'revert', @revert) if @revert;
