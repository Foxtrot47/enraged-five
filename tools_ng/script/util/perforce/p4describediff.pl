#!/usr/bin/perl

use strict;

my $DiffTool = "C:\\Program Files (x86)\\Nikeware\\DiffCmd Bundle\\viscmp.exe";
my $TmpFile = ".p4describe.tmp";

if ($ENV{"DIFFTOOL"} ne "") {
	$DiffTool = $ENV{"DiffTool"};
}

my $Shelved = 0;

# Handle the variables
foreach (@ARGV) {
	# Something number-only will be considered the description.
	if (/^(\d+)$/) {
		my $Changeset = $1;
		
		# Is this a shelved changeset?
		open(SHELVETEST, "p4 describe -s $1 |") or die "Cannot run p4 describe -s";
		while (<SHELVETEST>) {
			if (/^Change\s*\d+/) {
				if (/\*pending\*/) {
					$Shelved = 1;
					print "This is an uncommitted changeset. I will compare the shelved code.\n";
					last;
				}
			}
		}
		close SHELVETEST;
		
		my $ShelveOption = ($Shelved) ? "-S" : "";
		
		system("p4 describe $ShelveOption $1 >$TmpFile");
		open(STDIN, "<$TmpFile") or die "Cannot redirect STDIN";
#		open(STDIN, "|p4 describe $1") or die "Cannot run p4 describe";
	}
	
	if (/-shelved/i) {
		$Shelved = 1;
	}
}

unless (-e $DiffTool) {
	print "I can't find the diff tool (expected at $DiffTool).\n";
	print "Set the environment variable DIFFTOOL to the full path of the diff executable.\n";
	exit 10;
}

my $OutputFileRoot = ".diff";
my $OutputFile1 = "${OutputFileRoot}1.txt";
my $OutputFile2 = "${OutputFileRoot}2.txt";

open(OUT1, ">$OutputFile1") or die "Cannot open $OutputFile1";
open(OUT2, ">$OutputFile2") or die "Cannot open $OutputFile2";

my $DiffCount = 0;

while (<STDIN>) {
	# Write to both streams until we hit a diff.
	while (1) {
		if (/^(\d+)(,\d+)?([acd])(\d+)/) {
			# Beginning of a diff.
			print OUT1 "\nLine $1 ($4)\n\n";
			print OUT2 "\nLine $1 ($4)\n\n";
			
			$DiffCount++;
			
			while (<STDIN>) {
				# Ignore the terminator.
				next if (/---/);
				
				if (/^\</) {
					print OUT1 substr($_, 1);
				} else {
					if (/^\>/) {
						print OUT2 substr($_, 1);
					} else {
						last;
					}
				}
			}
		} else {
			last;
		}
	}
	
	# This is text outside a diff - that goes to both streams.
	print OUT1 $_;
	print OUT2 $_;
}

if ($DiffCount == 0 && $Shelved) {
	my @Err = ("", "**************", "No diffs detected. This is an uncommitted changeset.",
							"You will need to shelve the changeset to be use this tool.", "**************");
	
	foreach (@Err) {
		my $ErrLine = "$_\n";
		print OUT1 $ErrLine;
		print OUT2 $ErrLine;
		print $ErrLine;
	}
}	

close OUT2;
close OUT1;

system("\"$DiffTool\" $OutputFile1 $OutputFile2");
unlink($OutputFile1);
unlink($OutputFile2);
unlink($TmpFile);
