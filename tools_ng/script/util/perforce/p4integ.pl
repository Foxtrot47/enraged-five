#! /usr/bin/perl -w

use strict;

if (@ARGV < 2 or $ARGV[0] eq 'default')
{
    print "usage: p4integ <branchspec> <changelist> [<changelist>]...\n";
    exit;
}


my $branchspec = shift @ARGV;


open CHANGELIST, '>changelist.tmp';
print CHANGELIST "Change: new\nDescription:\n";
foreach my $to (sort @ARGV)
{
    print CHANGELIST " $to\n\n";
}
close CHANGELIST;

my $changelist = `p4 change -i < changelist.tmp`;

    unlink 'changelist.tmp';

if ($changelist =~ /Change ([0-9]+) created./)
{
    $changelist = $1;
    print "\nCreated new changelist: $changelist\n";
}
else
{
    print "Unable to create new changelist: $changelist\n";
    exit;
}


foreach my $to (sort @ARGV)
{
    my (@integrated);

    print "\nIntegrating files and scheduling for resolution...\n";
    system("p4", "integ", "-o", "-c", $changelist, "-d", "-i", "-b", $branchspec, "//...\@$to,\@$to");
    system("p4", "integ", "-o", "-c", $changelist, "-r", "-d", "-i", "-b", $branchspec, "//...\@$to,\@$to");

    print "\nReading changelist description...\n";
    open CHANGELIST, "p4 describe -s $changelist |";

    while (<CHANGELIST>)
    {
        last if (/Affected files/);
    }

    while (<CHANGELIST>)
    {
        my $line = Strip($_);

        if ($line =~ /^\.\.\. (.*)#[0-9]+ (edit|add|delete|branch|integrat)/)
        {
            print "$1 was $2ed\n";

            if ($2 eq "integrat")
            {
                push @integrated, $1;
            }
        }
    }

    close CHANGELIST;

    if (@integrated)
    {
        print "\nAuto resolving integrated files...\n";
        system("p4", "resolve", "-am", @integrated);
    }
}


system("p4", "changelist", "-d", $changelist);


sub Strip
{
    my $in = shift;
    $in =~ s/[^ -~]//go;
    return $in;
}
