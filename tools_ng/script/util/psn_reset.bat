@echo off
echo -----------------------------------------------------------------------------
echo PS3 RESET RESET RESET RESET
echo -----------------------------------------------------------------------------
echo Launched at %TIME%
call %RS_TOOLSROOT%\bin\setenv.bat
set COMMANDLINE=-f %RS_BUILDBRANCH% -h %RS_BUILDBRANCH% -r
echo.
echo ps3run %COMMANDLINE%
echo.
echo -----------------------------------------------------------------------------
echo PS3 RESET RESET RESET RESET
echo -----------------------------------------------------------------------------
ps3run %COMMANDLINE% > NUL