@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...
ECHO %RS_PROJECT% Title Update: Getting %RS_PROJECT% Title Update build...

set syncerror=0
::#set %PERFORCE_DLC_ROOT% = //gta5_dlc/;
set PERFORCE_ROOT=//depot/gta5

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


p4 sync %PERFORCE_ROOT%/titleupdate/dev/*.*@GTA5_TU_Prebuild 2> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/common/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/dlc_patch/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/ps3/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/TROPDIR/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev/xbox360/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
REM - 25/11/2015 - Ross McK - comment out Agent Trevor grab
::p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)

p4 sync //gta5_dlc/mpPacks/mpHeist/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpArmy/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)




:: Grab dev_Live also
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/*.*@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/common/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/dlc_patch/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/ps3/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/TROPDIR/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/titleupdate/dev_Live/xbox360/...@gta5_TU_prebuild 2>> %RS_TOOLSROOT%/logs/tu_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)



if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/tu_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause

:END
