rem @echo off
TITLE Importing stats into DB

IF [%1]==[] GOTO USAGE
IF [%2]==[] GOTO USAGE

echo Importing %1 (%2) to webserver DB...
call setenv.bat

rem =============================================
rem To be maintained by web guys.
rem =============================================
set JAR_FILE=statisticsLoader-0.0.1-SNAPSHOT.one-jar.jar
set DB_URL=jdbc:mysql://rsgedilin1:3306/stats2
set DB_USER=system
set DB_PWD=rdsgokuhero8e5rg

rem =============================================
rem Other settings.
rem =============================================
set FOLDER=K:\stream%RS_PROJECT%\stats\%2\%RS_PROJECT%\PS3\noon\%1
set JAR_PATH=%RS_TOOLSROOT%\script\util\stats\%JAR_FILE%

rem =============================================
rem Check folder.
rem =============================================
echo Folder being imported is %FOLDER%
IF NOT EXIST %FOLDER% GOTO NO_FOLDER

rem =============================================
rem The meat in the sandwich
rem =============================================
set COMMAND=java -jar %JAR_PATH% --db-url %DB_URL% --db-user %DB_USER% --db-pass %DB_PWD% --no-raw --title %RS_PROJECT% --map %RS_PROJECT% %FOLDER%
echo %COMMAND%
%COMMAND%

rem =============================================
rem The meat in the sandwich
rem =============================================
IF ERRORLEVEL 0 GOTO OK
GOTO FAILED

:NO_FOLDER
echo Error: Folder does not exist : %FOLDER%
pause
GOTO END

:USAGE
echo Usage: importDB.bat <folderdate>
pause
GOTO END

:FAILED
echo Error: Something went wrong with db import.
pause
GOTO END


:OK
EXIT ERRORLEVEL
:END