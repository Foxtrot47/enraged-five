#!/usr/bin/env ruby
# Usage: restlink.rb sender receiver URL

 require "net/http"
 require "optparse"

# Turn on NODELAY for HTTP connections, to speed up ps3
# https://gist.github.com/steveyen/1201110  
class MyHTTP < Net::HTTP
  def on_connect()
    @socket.io.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)
  end
end
 
options = {
	:delay => 100
}
optparse = OptionParser.new("Usage:  restlink.rb sender receiver URL [outputURL] [options]") do |opts|
	opts.on("-c", "--count TIMES", Integer, "Sync the machines TIMES times") do |arg|
		options[:count] = arg
	end
	
	opts.on("-d", "--delay MSEC", Integer, "Wait MSEC milliseconds between messages") do |arg|
		options[:delay] = arg
	end
	
	opts.on("-k", "--key", "Wait for a keypress between messages") do |arg|
		options[:key] = true
	end
	
	opts.on("--post", "Use POST instead of PUT") do |arg|
		options[:post] = true
	end
	
	opts.on("-v", "--verbose", "Verbose output") do |arg|
		options[:verbose] = true
	end
end

optparse.parse!

if (ARGV.length < 3)
	puts optparse
	exit(1)
end

sender = ARGV[0]
receiver = ARGV[1]
inURL = ARGV[2]
outURL = inURL
if (ARGV.length > 3)
	outURL = ARGV[3]
end
 
senderHttp = MyHTTP.new(sender)
receiverHttp = (receiver == "-") ? nil : MyHTTP.new(receiver)

counter = 0

if options[:key]
	puts "Enter to continue, q to quit"
end

while true
	if options[:verbose]:
		puts "<<< #{sender}#{inURL}"
	end
	senderHttp.request_get(inURL) do |getResp|
		if getResp.is_a?(Net::HTTPSuccess)
			if options[:verbose]:
				puts getResp.read_body
				puts ">>> #{receiver}#{outURL}"
			end
			if receiverHttp
				if options[:post]
					putResp = receiverHttp.request_post(outURL, getResp.read_body)
				else
					putResp = receiverHttp.request_put(outURL, getResp.read_body)
				end
				if !putResp.is_a?(Net::HTTPSuccess)
					puts "Receiver said: #{putResp.status}"
					exit(0)
				end
				putResp.read_body
			end
		else
			puts "Sender said: #{getResp}"
			exit(0)
		end
	end

	if options[:count]
		counter += 1
		if counter >= options[:count]
			break
		end
	end
	
	if options[:key]
		cmd = $stdin.gets.chomp
		if cmd == "q"
			break
		end
	end
	
	if options[:delay]
		sleep(options[:delay] / 1000.0)
	end
end


