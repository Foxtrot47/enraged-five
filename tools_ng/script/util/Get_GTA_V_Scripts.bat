@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE GTAV - Getting latest scripts, dialogue, strings, and design docs...
ECHO GTAV - Getting latest scripts, dialogue, strings, and design docs...

p4 sync %PERFORCE_ROOT%/script/dev/singleplayer/GTA5_SP.scproj...#head %PERFORCE_ROOT%/script/dev/...#head %PERFORCE_ROOT%/build/dev/common/data/script/xml...#head %PERFORCE_ROOT%/assets/Dialogue...#head %PERFORCE_ROOT%/assets/GameText/American/...#head %PERFORCE_ROOT%/docs/Design...#head 2> %RS_TOOLSROOT%/logs/Get_GTA_V_Scripts.log

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab Successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps3_sync.txt
	pause
)
