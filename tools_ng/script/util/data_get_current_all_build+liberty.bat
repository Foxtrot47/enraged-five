@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call data_get_project_info.bat

::-- Batch file title bar
TITLE Getting GTA5_NG_Current label for all...
ECHO Getting GTA5_NG_Current label for all...

REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	::-- Set studio sync paths
	::for /f %%i in ('%RS_TOOLSIRONLIB%\bin\RSG.Pipeline.WhatIsMyStudio.exe') do set studio=%%i
	::echo Studio is %studio%

	:: Old
		::for /f "tokens=*" %%a in ( 
		::'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
		::) do ( 
		::set studio=%%a
		::) 
		::echo/%%studio%%=%studio% 

	::if %studio%==north set networkbuildpath="N:\RSGEDI\Build Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==toronto set networkbuildpath="N:\RSGTOR\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==sandiego set networkbuildpath="N:\RSGSAN\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==newengland set networkbuildpath="N:\RSGNWE\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==leeds set networkbuildpath="N:\RSGLDS\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==london set networkbuildpath="N:\RSGLDN\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==newyork set networkbuildpath="N:\RSGNYC\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%==glencove set networkbuildpath="N:\RSGLIC\Build_Data\GTA5NG\Latest\build\dev_ng"
	::if %studio%=="" (
	::	echo Error: No studio set!!
	::	pause
	::	exit
	::)

	::echo NETWORK BUILD PATH SET: %networkbuildpath%
	::--------

::--- extra setup
set localbuildpath=X:\gta5\build\dev_ng
set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
	:: - Sync local network stored platform data -
	:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
	:: ROBOCOPY info. /S copies non-empty subfolders, /DCOPY:T keeps source timestamps
	::robocopy /S /DCOPY:T %networkbuildpath%\ps4\ %localbuildpath%\ps4\ *.* 2> %RS_TOOLSROOT%/logs/all_sync.txt
	::robocopy /S /DCOPY:T %networkbuildpath%\xboxone\ %localbuildpath%\xboxone\ *.* 2>> %RS_TOOLSROOT%/logs/all_sync.txt

	
REM 25/11/2015 - Ross McK - swap back to p4 base game grabs
:: grab all
p4 sync //depot/gta5/build/dev_ng/...@GTA5_NG_Current 2> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab DLC compat packs

::Beach
p4 sync //gta5_dlc/mpPacks/mpBeach/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Christmas
p4 sync //gta5_dlc/mpPacks/mpChristmas/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Valentine's
p4 sync //gta5_dlc/mpPacks/mpValentines/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business
p4 sync //gta5_dlc/mpPacks/mpBusiness/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business2
p4 sync //gta5_dlc/mpPacks/mpBusiness2/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Hipster
p4 sync //gta5_dlc/mpPacks/mpHipster/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpIndependence
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpHeist
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPilot
p4 sync //gta5_dlc/mpPacks/mpPilot/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLTS
p4 sync //gta5_dlc/mpPacks/mpLTS/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe2
p4 sync //gta5_dlc/mpPacks/mpLuxe2/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe2/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPatchesNG
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

REM 25/11/2015 - Ross McK - comment out clifford sync
::CLIFFORD
::p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)
::p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)

::spUpgrade
p4 sync //gta5_dlc/spPacks/spUpgrade/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/spUpgrade/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay1NG
p4 sync //gta5_dlc/patchPacks/patchDay1NG/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay1NG/build/dev_ng/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2NG
p4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2bNG
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay3NG
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/dev_ng/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Christmas2
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: GRAB LIBERTY NG 
p4 sync //gta5_liberty/build/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab NG TU

::TitleUpdate - DEV_NG
p4 sync //depot/gta5/titleupdate/dev_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

REM 25/11/2015 - Ross McK - comment out dev_Temp sync
::TitleUpdate - DEV_TEMP
::p4 sync //depot/gta5/titleupdate/dev_Temp/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/all_sync.txt
::IF %ERRORLEVEL% EQU 1 (
::	set syncerror=1
::)



if "%BUILD_MACHINE%"=="1" (
	goto:END
)


::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/all_sync.txt
)



)
::---------------------------------------------------------------

pause

:END
