#
# File:: data_get_latest_all.rb
# Description:: Project-specific get all latest data from Perforce.
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 7 August 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/path'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
PROJECT_KEY = 'jimmy'
CONFIG = Pipeline::Config::instance()
SCRIPT_PATH = Pipeline::OS::Path.combine( CONFIG.toolsbin, 'util', 'data_get_latest_all.rb' )

#-----------------------------------------------------------------------------
# Entry
#-----------------------------------------------------------------------------

if ( __FILE__ == $0 ) then

    command = "ruby #{SCRIPT_PATH} --project=#{PROJECT_KEY}"
    exec( command )
end

# data_get_latest_all.rb
