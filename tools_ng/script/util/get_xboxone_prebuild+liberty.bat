@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA5_NG_PreBuild label for Xbox One...
ECHO Getting GTA5_NG_PreBuild label for Xbox One...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab Main Build
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_Prebuild 2> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xboxone/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xbo_loose/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/xbo_scripts/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_durango_*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab NG TU
::TitleUpdate - DEV_NG
p4 sync //depot/gta5/titleupdate/dev_ng/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xbo_loose/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xbo_scripts/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/xboxone/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/game_durango_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::TitleUpdate - DEV_TEMP
p4 sync //depot/gta5/titleupdate/dev_Temp/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/xbo_loose/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/xbo_scripts/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/xboxone/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/game_durango_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_Temp/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab DLC compat packs

::Beach
p4 sync //gta5_dlc/mpPacks/mpBeach/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBeach/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Christmas
p4 sync //gta5_dlc/mpPacks/mpChristmas/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Christmas2
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Valentine's
p4 sync //gta5_dlc/mpPacks/mpValentines/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpValentines/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business
p4 sync //gta5_dlc/mpPacks/mpBusiness/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Business2
p4 sync //gta5_dlc/mpPacks/mpBusiness2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpBusiness2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::Hipster
p4 sync //gta5_dlc/mpPacks/mpHipster/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHipster/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpIndependence
p4 sync //gta5_dlc/mpPacks/mpIndependence/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpIndependence/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpHeist
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPilot
p4 sync //gta5_dlc/mpPacks/mpPilot/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPilot/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLTS
p4 sync //gta5_dlc/mpPacks/mpLTS/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLTS/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpLuxe2
p4 sync //gta5_dlc/mpPacks/mpLuxe2/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpLuxe2/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::mpPatchesNG
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


::CLIFFORD
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/dlc_agentTrevor/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::spUpgrade
p4 sync //gta5_dlc/spPacks/spUpgrade/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/spPacks/spUpgrade/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay1NG
p4 sync //gta5_dlc/patchPacks/patchDay1NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay1NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2NG
p4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay2bNG
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::patchDay3NG
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

:: GRAB LIBERTY NG DATA
p4 sync //gta5_liberty/build/dev_ng/common/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_liberty/build/dev_ng/xboxone/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_liberty/build/dev_ng/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/xbone_prebuild_sync.txt
)

)
::---------------------------------------------------------------

pause

:END
