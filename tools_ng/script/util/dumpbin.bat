@ECHO OFF
REM File:: %RS_TOOLSROOT%\script\util\dumpbin.bat
REM Description:: Run the Visual Studio COFF executable analysis tool dumpbin
REM eventually this will support VS2010 also.
REM
REM Author:: Derek Ward <derek.ward@rockstarnorth.com>
REM Date:: 13 June 2012
REM

CALL setenv.bat

call "C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\vcvarsall.bat" x86
"C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\dumpbin.exe" %1 %2

REM Done.
