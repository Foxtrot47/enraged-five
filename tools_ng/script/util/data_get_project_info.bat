@ECHO OFF

call ../../install.bat /switch
call setenv.bat

set PERFORCE_ROOT=%RS_PROJROOT%

set LABEL_NAME=current
CALL :getlabel
set PERFORCE_CURRENT_LABEL_NAME=%RETURN_VALUE%

set LABEL_NAME=network
CALL :getlabel
set PERFORCE_CURRENT_NETWORK_LABEL_NAME=%RETURN_VALUE%

set LABEL_NAME=networkdemo
CALL :getlabel
set PERFORCE_NETWORK_DEMO_LABEL_NAME=%RETURN_VALUE%

set LABEL_NAME=prebuild
CALL :getlabel
set PERFORCE_PREBUILD_LABEL_NAME=%RETURN_VALUE%

set LABEL_NAME=milestone
CALL :getlabel
set PERFORCE_MILESTONE_LABEL_NAME=%RETURN_VALUE%

REM Current Tools Label
SET LABEL_NAME=tools_current
CALL :getlabel
SET PERFORCE_CURRENT_TOOLS_LABEL_NAME=%RETURN_VALUE%

REM Testing Tools Label
SET LABEL_NAME=tools_testing
CALL :getlabel
SET PERFORCE_TESTING_TOOLS_LABEL_NAME=%RETURN_VALUE%

REM Testing Patch Tools Label
SET LABEL_NAME=tools_testing_patch
CALL :getlabel
SET PERFORCE_TESTING_PATCH_TOOLS_LABEL_NAME=%RETURN_VALUE%

goto :eof

:getlabel

set XMLSTARLET=%RS_TOOLSBIN%\xmlstarlet.exe

set XMLSTARLET_COMMAND=%XMLSTARLET% sel -t -v "//project/labels/label[@type='%LABEL_NAME%']/@name" %RS_TOOLSCONFIG%\project.xml
FOR /F "tokens=*" %%i IN ('%XMLSTARLET_COMMAND%') do (
	set RETURN_VALUE=%%i
)

:eof