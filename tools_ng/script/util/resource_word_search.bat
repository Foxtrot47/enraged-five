@ECHO OFF
echo. resource_word_search
echo.   search specified directories for a given list of words
echo.   unpacks and searches any RPFs it finds to a temporary folder
echo. 
echo. 
echo Usage:
echo.  resource_word_search c:/files.txt c:/keywords.txt c:/output.txt
echo.  1st argument is file containing a list of directories
echo.  separated by new lines
echo.    e.g.
echo.    X:\gta5\build\disk_images\orbis\gta5-orbis\game\*.* (all files)
echo.    X:\gta5\build\disk_images\orbis\gta5-orbis\game\*.rpf (all RPFs)
echo.    X:\gta5\build\disk_images\orbis\gta5-orbis\game\ (all files and subfolders)
echo.  2nd argument is file containing a list of keywords
echo.  separated by new lines
echo.  3rd argument is where results of the search are put
echo.
echo.
echo Optional Arguments
echo.  -t [directory] specify the temporary directory (default: d:/temp_search)
echo.  -v adds extra data to the output
echo.  -w length of reported match in output (bytes)
echo.  -i include file path names in the search
echo. 
echo. 
echo. All Files/Folders in temp file will be deleted at the start of each search
echo. 
echo. 

call %~dp0%..\..\bin\setenv.bat
python %~dp0rpfsearch.py %*

pause