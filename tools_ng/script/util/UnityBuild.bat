@ECHO OFF

REM ****************************************************************************************************************
REM *** UnityBuild.bat                                                                                           
REM ***                                                                                                          
REM *** Derek Ward <derek.ward@rockstarnorth.com> Jan 2011                                                       
REM ***                                                                                                          
REM *** - Control script to 'unitise' various projects.
REM ***                                                                                                         
REM *** A unity build aims to reduce the number of compilation units a compiler processes, not neccessarily just one
REM *** as its name suggests.
REM ***                                                                                                          
REM *** In doing so, tests indicate the resultant # of .obj files and the # of hdr files included yields a 
REM *** 6x compiler speed and 2x linker speed increase.                                                                                     
REM ***                                                                                                          
REM *** A unity build works by including source files inside new source files that are added to the project, 
REM *** thereafter existing source files are excluded from the build.
REM ***                                                                                                         
REM *** various symbol collisions inevitably occur and must be resolved. 
REM *** Alters the compilers ability to optmise ( inlining etc. ), yet to be determined if this has a 
REM *** positive or negative outcome.                                                                                                          
REM ***                                                                                                          
REM ***************************************************************************************************************

REM Unitybuild REVERT
REM Unitybuild CONVERT "Win32" 		"SN PS3 SNC Beta" 
REM Unitybuild CONVERT "Xbox 360" 	"BankRelease" 

SET CONVERT=1
IF "%1"=="REVERT" SET CONVERT=0 

SET PLATFORM=%2
SET BUILDCONFIG=%3

CALL setenv.bat
TITLE %RS_PROJECT% UnityBuild

PUSHD %RS_TOOLSSRC%\ui\UnityTool

REM ***** GLOBAL SETTINGS *****

SET RAGE_P4PORT=rsgedip4p1:1667
SET P4_USER=buildernorth
SET P4_CLIENT=RSGEDICC1

REM ***** JOBS *****

CALL :UNITYBUILD_SCALEFORM
CALL :UNITYBUILD_RAGESUITECREATURE
CALL :UNITYBUILD_RAGESCRIPT
CALL :UNITYBUILD_RAGEPHYSICS
CALL :UNITYBUILD_RAGENET
CALL :UNITYBUILD_RAGEMISC
CALL :UNITYBUILD_RAGEGRAPHICS
CALL :UNITYBUILD_RAGECREATURE
CALL :UNITYBUILD_RAGECORE
CALL :UNITYBUILD_RAGEAUDIO
CALL :UNITYBUILD_GAME1_LIB
CALL :UNITYBUILD_GAME2_LIB
CALL :UNITYBUILD_GAME3_LIB
CALL :UNITYBUILD_GAME4_LIB
CALL :UNITYBUILD_NATURALMOTION
CALL :UNITYBUILD_NETWORK
CALL :UNITYBUILD_RAGEFRAMEWORK

GOTO:END




REM ****************************************************************************************************************
REM *** JOBS SUBROUTINES
REM ****************************************************************************************************************

:UNITYBUILD_SCALEFORM
ECHO -----UNITYBUILD_SCALEFORM-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\scaleform\Src\vcproj\ScaleformGfx\ScaleformGfx_2008.vcproj
SET REV=head
SET STYLE=-minor
SET EXCLUDE=
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGESUITECREATURE
ECHO -----UNITYBUILD_RAGESUITECREATURE-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\suite\src\vcproj\RageSuiteCreature\RageSuiteCreature_2008.vcproj
SET REV=head
SET STYLE=-minor
SET EXCLUDE=-exclude styletests.cpp -exclude stylegenerator.cpp -exclude correspondencegenerator.cpp
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGESCRIPT
ECHO -----UNITYBUILD_RAGESCRIPT-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\script\src\vcproj\RageScript\RageScript_2008.vcproj
SET REV=head
SET STYLE=-master
SET EXCLUDE=
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGEPHYSICS
ECHO -----UNITYBUILD_RAGEPHYSICS-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RagePhysics\RagePhysics_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude btGjkEpa.cpp -exclude shapetest.cpp -exclude SpuCollisionTaskProcess.cpp
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGENET
ECHO -----UNITYBUILD_RAGENET-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RageNet\RageNet_2008.vcproj
SET REV=head
SET STYLE=-minor
REM really hard to understand the issues with this code.
SET EXCLUDE=-exclude rl -exclude net
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGEMISC
ECHO -----UNITYBUILD_RAGEMISC-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\VS_Project\RageMisc\ragemisc_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude move_ -exclude ptxspufuncs.cpp -exclude ptxu_collision_common.cpp -exclude skyKeyframed.cpp -exclude typexml.cpp
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_RAGEGRAPHICS
ECHO -----UNITYBUILD_RAGEGRAPHICS-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RageGraphics\RageGraphics_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude device_d3d.cpp -exclude texturexenon.cpp -exclude texturexenonproxy.cpp -exclude edge_callbacks_gta4.cpp -exclude edge_callbacks.cpp -exclude primitive.cpp -exclude xbstrip.cpp -exclude indexbuffer_d3d.cpp -exclude texture_d3d -exclude texturereference.cpp -exclude d3d -exclude image.cpp -exclude grcore
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGECREATURE
ECHO -----UNITYBUILD_RAGECREATURE-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RageCreature\RageCreature_2008.vcproj
SET REV=head
SET STYLE=-minor
SET EXCLUDE=
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGECORE
ECHO -----UNITYBUILD_RAGECORE-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RageCore\RageCore_2008.vcproj
SET REV=head
SET STYLE=-minor
SET EXCLUDE=-exclude hull.cpp -exclude memberstring.cpp -exclude memberarray.cpp -exclude new.cpp -exclude streambuffers.cpp -exclude dummy_titleid.cpp -exclude tracker_file.cpp -exclude vec2v.cpp -exclude vec3v.cpp -exclude vec4v.cpp -exclude vectormath.cpp -exclude quatv.cpp -exclude scalarv.cpp -exclude vec2f.cpp -exclude vec3f.cpp -exclude vec4f.cpp -exclude quatf.cpp -exclude pix_pc.cpp -exclude geometry.cpp -exclude namedpipe.cpp -exclude winsock.cpp
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_RAGEAUDIO
ECHO -----UNITYBUILD_RAGEAUDIO-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\base\src\vcproj\RageAudio\RageAudio_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude nulleffect.cpp - exclude soundupdatejob.cpp -exclude submixjob.cpp -exclude waveplayerjob.cpp -exclude audioengine -exclude audiohardware -exclude audiosynth
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_GAME1_LIB
ECHO -----UNITYBUILD_GAME1_LIB-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\vs_project1_lib\game1_lib_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude EventDecisionMakerResponse.cpp -exclude EventData.cpp -exclude VectorMap.cpp -exclude scriptaudioentity.cpp -exclude Frontend.cpp -exclude ScaleFormMgr.cpp -exclude MenuSystem.cpp
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_GAME2_LIB	
ECHO -----UNITYBUILD_GAME2_LIB-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\vs_project2_lib\game2_lib_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_GAME3_LIB
ECHO -----UNITYBUILD_GAME3_LIB-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\vs_project3_lib\game3_lib_2008.vcproj
SET REV=head
SET STYLE=-minor
SET EXCLUDE=-exclude game.cpp
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_GAME4_LIB
ECHO -----UNITYBUILD_GAME4_LIB-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\vs_project4_lib\game4_lib_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude VectorMap.cpp -exclude cargen.cpp
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_NATURALMOTION
ECHO -----UNITYBUILD_NATURALMOTION-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\naturalmotion\vcproj\NaturalMotion\naturalmotion_2008.vcproj
SET REV=head
SET STYLE=-master
SET EXCLUDE=
CALL:SUBRAGE
GOTO:EOF

:UNITYBUILD_NETWORK
ECHO -----UNITYBUILD_NETWORK-----
SET VCPROJFILE=%RS_CODEBRANCH%\game\VS_Project_network\network_2008.vcproj
SET REV=head
SET STYLE=-master -nummajor 4
SET EXCLUDE=
CALL:SUBLOCAL
GOTO:EOF

:UNITYBUILD_RAGEFRAMEWORK
ECHO -----UNITYBUILD_RAGEFRAMEWORK-----
SET VCPROJFILE=%RS_CODEBRANCH%\rage\framework\src\vcproj\RageFramework\RageFramework_2008.vcproj
SET REV=head
SET STYLE=
SET EXCLUDE=-exclude ptxu_decalpool.cpp -exclude ptxu_decal.cpp -exclude vectormap.cpp
CALL:SUBRAGE
GOTO:EOF

REM ****************************************************************************************************************
REM *** CONVERT SUBROUTINES 
REM ****************************************************************************************************************

:SUBRAGE
echo Unityising %VCPROJFILE%#%REV% %BUILDCONFIG% %PLATFORM%
p4 -p %RAGE_P4PORT% -u %P4_USER% -c %P4_CLIENT% sync -f %VCPROJFILE%#%REV%
IF %CONVERT%==0 echo *** REVERTED TO %VCPROJFILE%#%REV% *** 
IF %CONVERT%==1 echo %RS_TOOLSBIN%\UnityTool.exe %VCPROJFILE% -unity %STYLE% -buildconfig %BUILDCONFIG% -platform %PLATFORM% %EXCLUDE%
IF %CONVERT%==1 %RS_TOOLSBIN%\UnityTool.exe %VCPROJFILE% -unity %STYLE% -buildconfig %BUILDCONFIG% -platform %PLATFORM% %EXCLUDE%
ECHO -
GOTO:EOF

:SUBLOCAL
echo Unityising %VCPROJFILE%#%REV% %BUILDCONFIG% %PLATFORM%
p4 sync -f %VCPROJFILE%#%REV%
IF %CONVERT%==0 echo *** REVERTED TO %VCPROJFILE%#%REV% *** 
IF %CONVERT%==1 echo %RS_TOOLSBIN%\UnityTool.exe %VCPROJFILE% -unity %STYLE% -buildconfig %BUILDCONFIG% -platform %PLATFORM% %EXCLUDE%
IF %CONVERT%==1 %RS_TOOLSBIN%\UnityTool.exe %VCPROJFILE% -unity %STYLE% -buildconfig %BUILDCONFIG% -platform %PLATFORM% %EXCLUDE%
ECHO -
GOTO:EOF


REM ****************************************************************************************************************
REM *** END
REM ****************************************************************************************************************
:END

POPD
ECHO ====== UnityBuild.Bat has completed. ======
PAUSE

