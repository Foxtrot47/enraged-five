@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting GTA_NG_PreBuild label for PS4...
ECHO Getting GTA_NG_PreBuild label for PS4...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: Grab main build.
p4 sync //depot/gta5/build/dev_ng/common/...@GTA5_NG_PreBuild 2> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_companion_httpd/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/sce_module/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/build/dev_ng/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

:: Grab NG TU

::TitleUpdate - dev_ng_Live
p4 sync //depot/gta5/titleupdate/dev_ng_Live/common/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/dlc_patch/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/ps4/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/sce_sys/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/*.xml@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/*.prx@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/game_orbis_*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //depot/gta5/titleupdate/dev_ng_Live/_launchgame.bat@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


:: Grab DLC compat packs

::mpChristmas2018
p4 sync //gta5_dlc/mpPacks/mpChristmas2018/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/mpPacks/mpChristmas2018/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::patchDay20NG
p4 sync //gta5_dlc/patchPacks/patchDay20NG/build/dev_ng/...@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync //gta5_dlc/patchPacks/patchDay20NG/*.*@GTA5_NG_PreBuild 2>> %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps4_Live_prebuild_sync.txt
)


)
::---------------------------------------------------------------

pause

:END
