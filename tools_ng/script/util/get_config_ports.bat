@ECHO OFF

call setenv.bat > nul

set XMLSTARLET=%RS_TOOLSBIN%\xmlstarlet.exe
set PORT_CONFIG_FILE=%RS_TOOLSCONFIG%\ports.xml
set LOCAL_PORT_CONFIG_FILE=%RS_TOOLSCONFIG%\ports.local.xml

if not exist "%PORT_CONFIG_FILE%" (
	echo Port config file doesn't exist: %PORT_CONFIG_FILE%
)
if not exist "%LOCAL_PORT_CONFIG_FILE%" (
	echo Local port config file doesn't exist: %LOCAL_PORT_CONFIG_FILE%
)

set CONFIG_PORT_ID=SystrayRFS
CALL :getport
set PORT_SYSTRAYRFS=%RETURN_VALUE%

set CONFIG_PORT_ID=RAGProxyHubConnectionService
CALL :getport
set PORT_RAGPROXYHUBCONNECTIONSERVICE=%RETURN_VALUE%

goto :eof

:getport

set XPATH_EXPRESSION=/Ports/Port[@id='%CONFIG_PORT_ID%']/Value
set RETURN_VALUE=
set USING_DEFAULT=0
set USING_LOCAL=0

if  exist "%PORT_CONFIG_FILE%" (
	FOR /F "tokens=*" %%i IN ('%XMLSTARLET% sel -t -v "%XPATH_EXPRESSION%" "%PORT_CONFIG_FILE%"') do (
		set RETURN_VALUE=%%i
		set USING_DEFAULT=1
	)
)

if  exist "%LOCAL_PORT_CONFIG_FILE%" (
	FOR /F "tokens=*" %%i IN ('%XMLSTARLET% sel -t -v "%XPATH_EXPRESSION%" "%LOCAL_PORT_CONFIG_FILE%"') do (
		set RETURN_VALUE=%%i
		set USING_LOCAL=1
	)
)

if "%USING_LOCAL%"=="1" (
	echo Using local value '%RETURN_VALUE%' for '%CONFIG_PORT_ID%'.
) else if %USING_DEFAULT%==1 (
	echo Using default value '%RETURN_VALUE%' for '%CONFIG_PORT_ID%'.
) else (
	echo Unable to retrieve the port for '%CONFIG_PORT_ID%'.
)

:eof