@ECHO OFF

cd /d %~dp0
call data_get_project_info.bat

TITLE Getting JPN GTA5_NG_Current label for PS4...
ECHO Getting JPN GTA5_NG_Current label for PS4...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync //depot/gta5/build/Japanese_ng/common/...@GTA5_NG_Current 2> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/Japanese_ng/ps4/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/Japanese_ng/sce_companion_httpd/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/Japanese_ng/sce_sys/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/Japanese_ng/sce_module/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/build/Japanese_ng/game_orbis_*.*@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

p4 sync //depot/gta5/titleupdate/Japanese_ng/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/ps4_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/ps4_sync.txt
)




)
::---------------------------------------------------------------

pause

:END
