@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% Title Update: Getting %RS_PROJECT% dev_ng_Live build...
ECHO %RS_PROJECT% Title Update: Getting %RS_PROJECT% dev_ng_Live build...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

:: Grab NG_Live TitleUpdate
p4 sync //depot/gta5/titleupdate/dev_ng_Live/...@GTA5_NG_Prebuild 2> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)


if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay1NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay1NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay2NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay2NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay2bNG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay3NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay3NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay4NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay4NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay5NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay5NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay6NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay6NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay7NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay7NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay8NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay8NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay9NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay9NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay10NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay10NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay11NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay11NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay12NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay12NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay13NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay13NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay14NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay14NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay15NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay15NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay16NG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\patchpacks (
p4 sync //gta5_dlc/patchPacks/patchDay16NG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpHeist (
p4 sync //gta5_dlc/mpPacks/mpHeist/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpHeist (
p4 sync //gta5_dlc/mpPacks/mpHeist/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpLuxe (
p4 sync //gta5_dlc/mpPacks/mpLuxe/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpLuxe (
p4 sync //gta5_dlc/mpPacks/mpLuxe/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpLuxe2 (
p4 sync //gta5_dlc/mpPacks/mpLuxe2/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpLuxe2 (
p4 sync //gta5_dlc/mpPacks/mpLuxe2/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpReplay (
p4 sync //gta5_dlc/mpPacks/mpReplay/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpReplay (
p4 sync //gta5_dlc/mpPacks/mpReplay/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpLowrider (
p4 sync //gta5_dlc/mpPacks/mpLowrider/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpLowrider (
p4 sync //gta5_dlc/mpPacks/mpLowrider/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpHalloween (
p4 sync //gta5_dlc/mpPacks/mpHalloween/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpHalloween (
p4 sync //gta5_dlc/mpPacks/mpHalloween/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpApartment (
p4 sync //gta5_dlc/mpPacks/mpApartment/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpApartment (
p4 sync //gta5_dlc/mpPacks/mpApartment/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpLowrider2 (
p4 sync //gta5_dlc/mpPacks/mpLowrider2/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpLowrider2(
p4 sync //gta5_dlc/mpPacks/mpLowrider2/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpPatchesNG (
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpPatchesNG (
p4 sync //gta5_dlc/mpPacks/mpPatchesNG/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpChristmas2 (
p4 sync //gta5_dlc/mpPacks/mpChristmas2/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpChristmas2 (
p4 sync //gta5_dlc/mpPacks/mpChristmas2/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpxmas_604490 (
p4 sync //gta5_dlc/mpPacks/mpxmas_604490/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpxmas_604490 (
p4 sync //gta5_dlc/mpPacks/mpxmas_604490/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpJanuary2016 (
p4 sync //gta5_dlc/mpPacks/mpJanuary2016/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpJanuary2016 (
p4 sync //gta5_dlc/mpPacks/mpJanuary2016/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpValentines2 (
p4 sync //gta5_dlc/mpPacks/mpValentines2/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpValentines2 (
p4 sync //gta5_dlc/mpPacks/mpValentines2/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpExecutive (
p4 sync //gta5_dlc/mpPacks/mpExecutive/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpExecutive (
p4 sync //gta5_dlc/mpPacks/mpExecutive/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpstunt (
p4 sync //gta5_dlc/mpPacks/mpstunt/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpstunt (
p4 sync //gta5_dlc/mpPacks/mpstunt/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpBiker (
p4 sync //gta5_dlc/mpPacks/mpBiker/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpBiker (
p4 sync //gta5_dlc/mpPacks/mpBiker/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpImportExport (
p4 sync //gta5_dlc/mpPacks/mpImportExport/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpImportExport (
p4 sync //gta5_dlc/mpPacks/mpImportExport/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if exist x:\gta5_dlc\mpPacks\mpSpecialRaces (
p4 sync //gta5_dlc/mpPacks/mpSpecialRaces/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpSpecialRaces (
p4 sync //gta5_dlc/mpPacks/mpSpecialRaces/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpGunRunning (
p4 sync //gta5_dlc/mpPacks/mpGunRunning/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpGunRunning (
p4 sync //gta5_dlc/mpPacks/mpGunRunning/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_LIVE_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpAirRaces(
p4 sync //gta5_dlc/mpPacks/mpAirRaces/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpAirRaces(
p4 sync //gta5_dlc/mpPacks/mpAirRaces/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpSmuggler(
p4 sync //gta5_dlc/mpPacks/mpSmuggler/build/dev_ng/...@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)
if exist x:\gta5_dlc\mpPacks\mpSmuggler(
p4 sync //gta5_dlc/mpPacks/mpSmuggler/*.*@GTA5_NG_Prebuild 2>> %RS_TOOLSROOT%/logs/NG_TU_sync.txt
if errorlevel 1 set syncerror=1
)

if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/tu_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause

:END
