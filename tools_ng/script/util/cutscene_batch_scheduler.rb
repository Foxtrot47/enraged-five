require 'pipeline/log/log'
require 'pipeline/util/environment'
require 'pipeline/os/getopt'

include Pipeline
include Util

MOTIONBUILDER_PATH = "C:/Program Files/Autodesk/MotionBuilder 2012 (64-bit)/bin/x64/motionbuilder.exe" 
LOG_PATH = "X:/gta5/tools/logs/cutscene/workbench_batch.log"

logFile = File.new(LOG_PATH, "w").close

OPTIONS = [[ '--scriptDir', '-sD', OS::Getopt::REQUIRED, 'Script directory.' ]]

opts, trailing = OS::Getopt::getopts( OPTIONS )
directory = opts['scriptDir'].nil? ? "" : opts['scriptDir']

Dir.glob(directory + '*.py') do |py_file|
  puts ("processing - " + py_file)
  commandline = MOTIONBUILDER_PATH + " " + py_file
  status, sout, serr = OS::start( commandline )
end