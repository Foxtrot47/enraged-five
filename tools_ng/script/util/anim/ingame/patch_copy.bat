@ECHO OFF
REM
REM File:: patch_copy.bat
REM Description:: Copy all rpf's from each platforms /anim/ingame folder in mp_patches to specified dlc project
REM Arguments:: dlc project to copy into, example: mp_pilot
REM				optional: specify the specific path (mpPacks or spPacks) if unsupplied, tries to guess from name. 
REM
REM Author:: Fraser Tomison <fraser.tomison@rockstarnorth.com>
REM Date:: 18 July 2014
REM

TITLE MPPatches RPF Copy

SET TARGET=%~1
REM Ensure Target is Valid: If second argument supplied, use that. else try to decide from name
SET PREFIX=%TARGET:~0,2%
Set PACK_TYPE=
IF NOT "%~2"=="" (
	SET PACK_TYPE=%~2
) ELSE IF "%PREFIX%"=="mp" (
	SET PACK_TYPE="mpPacks"
) ELSE IF "%PREFIX%"=="sp" (
	SET PACK_TYPE="spPacks"
) ELSE (
	ECHO Invalid target %TARGET%
	PAUSE
	EXIT /B
)
p4 dirs //gta5_dlc/%PACK_TYPE%/%TARGET%/build | findstr /C:"//gta5_dlc/%PACK_TYPE%/%TARGET%/build"
IF not %errorlevel% == 0 (
	ECHO Invalid target %TARGET%
	PAUSE
	EXIT /B
)

ECHO Syncing rpf's from mpPatches...

p4 sync //gta5_dlc/mpPacks/mpPatches/build/*/*/anim/ingame/*
p4 sync //gta5_dlc/%PACK_TYPE%/%TARGET%/build/*/*/anim/ingame/*

ECHO Performing copy...

p4 change -o | findstr /v /C:"<enter description here>" | findstr /v /C:"Description:" | findstr /v /C:"Files:" | findstr /v /C:"//" > changelist.txt
ECHO Description: Latest overlay patch data >> changelist.txt

for /F "usebackq tokens=2" %%A in (`p4 change -i ^< changelist.txt`) DO SET CL_NUM=%%A

p4 copy -c %CL_NUM% //gta5_dlc/mpPacks/mpPatches/build/*/*/anim/ingame/* //gta5_dlc/%PACK_TYPE%/%TARGET%/build/*/*/anim/ingame/*

for /F "usebackq tokens=2 delims=# " %%A in (`p4 describe %CL_NUM% ^| findstr delete`) DO p4 revert %%A

DEL changelist.txt

PAUSE

REM patch_copy.bat
