
CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\cutscene_changes.rb --src_dir "$(assets)\cuts" --out_file %RS_PROJROOT%\build_changelists\changes_%TIME:~0,2%_%TIME:~3,2%_%TIME:~6,2%.txt
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\zip_animations.rb
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\process_animations.rb --src_dir "$(export)/anim/cutscene/"
echo Press any key to continue...
pause