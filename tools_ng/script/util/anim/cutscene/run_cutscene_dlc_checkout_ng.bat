
CALL setenv.bat >NUL
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\cutscene_changes_dlc.rb
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\zip_animations_dlc.rb --checkout --branch dev_ng
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\anim\cutscene\process_animations_dlc.rb --branch dev_ng
echo Press any key to continue...
timeout 10