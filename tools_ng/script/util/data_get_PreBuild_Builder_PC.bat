@ECHO OFF
cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% PreBuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for PC x64...
ECHO %RS_PROJECT% PreBuild dev: Getting %PERFORCE_PREBUILD_LABEL_NAME% labelled build for PC x64...

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe

p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_PREBUILD_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/pc_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/x64/...@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/pc_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/game_win64_beta_dx11.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/pc_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/binkw32.dll.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/pc_sync.txt
p4 sync %PERFORCE_ROOT%/build/dev/binkw64.dll.*@%PERFORCE_PREBUILD_LABEL_NAME% 2>> %RS_TOOLSROOT%/logs/pc_sync.txt

IF %ERRORLEVEL% EQU 0 (
	echo .
	echo Grab successful.
	pause
)
IF %ERRORLEVEL% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/pc_sync.txt
	pause
	exit
)