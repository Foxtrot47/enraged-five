@ECHO OFF

:: SWITCH ON REMOTE SYMLINKS
fsutil behavior set SymlinkEvaluation R2L:1
fsutil behavior set SymlinkEvaluation R2R:1

if "%1"=="buildmachine" (
	set BUILD_MACHINE=1
)

cd /d %~dp0
call data_get_project_info.bat

TITLE %RS_PROJECT% GTA5: Getting Current mpPacks DLC...
ECHO %RS_PROJECT% GTA5: Getting Current mpPacks DLC...

set syncerror=0

echo Killing SystrayRfs and RAG
taskkill /IM SysTrayRfs.exe
taskkill /IM rag.exe
taskkill /IM ragApp.exe


:: ================ SYNC ====================::
p4 sync -f //gta5_dlc/mpPacks/mpBeach/build/dev/...@GTA5_NG_Current 2> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpChristmas/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpValentines/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpBusiness/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpBusiness2/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpHipster/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpIndependence/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpPilot/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLTS/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpHeist/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpArmy/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpChristmas2/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLuxe/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpLuxe2/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpxmas_604490/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpJanuary2016/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpExecutive/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpstunt/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpImportExport/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpBiker/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpSpecialRaces/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpGunRunning/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpAirraces/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/mpPacks/mpAirraces/build/dev/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay1NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay2NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay2bNG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay3NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay4NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay5NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay6NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay7NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay8NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay9NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay10NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay11NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay12NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay13NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay14NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay15NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync -f //gta5_dlc/patchPacks/patchDay16NG/build/...@GTA5_NG_Current 2>> %RS_TOOLSROOT%/logs/dlc_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
::==========================================::


if "%BUILD_MACHINE%"=="1" (
	goto:END
)

::--- check for errors --- 
IF %syncerror% EQU 0 (
	echo .
	echo Grab successful.
)
IF %syncerror% EQU 1 (
	echo .
	echo WARNING: Errors were reported during the grab.
	notepad %RS_TOOLSROOT%/logs/dlc_sync.txt
)


::----- restart rag and systray -----------
tasklist /FI "IMAGENAME eq rag.exe" 2>NUL | find /I "rag.exe" > NUL
IF !ERRORLEVEL! EQU 1 (
 	echo Starting Rag proxy.
 	start /d %RS_TOOLSBIN%\rag\ %RS_TOOLSBIN%\rag\rag.exe 
) ELSE (
 	echo Rag proxy already started.
)
tasklist | find /I "SysTrayRfs.exe"
IF %ERRORLEVEL% EQU 1 (

start %RS_TOOLSBIN%\SysTrayRfs.exe -trusted -nofocus

)
::---------------------------------------------------------------

pause


