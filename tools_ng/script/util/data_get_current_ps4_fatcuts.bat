@ECHO OFF
REM
REM File:: data_get_tools_and_install.bat
REM Description::
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 12 July 2011
REM

CALL setenv.bat
TITLE Syncing latest %RS_PROJECT% ps4 fatscuts...

p4 sync //depot/gta5/assets_ng/non_final/build/dev_ng/ps4/anim/cutscene/...
p4 sync //depot/gta5/build/dev_ng/common/data/common_cutscene_fat.meta
p4 sync //depot/gta5/build/dev_ng/common/data/common_cutscene.meta


PAUSE

REM data_get_tools_and_install.bat