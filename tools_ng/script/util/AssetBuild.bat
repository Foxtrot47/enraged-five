@ECHO OFF
REM 
REM Starts the AssetBuilder (default branch)
REM David Muir <david.muir@rockstarnorth.com>
REM
REM Now includes a keep-alive loop.
REM

CALL setenv.bat
TITLE %RS_PROJECT% Asset Builder - dev
SET ASSETBUILDER=\util\AssetBuilder

PUSHD %RS_TOOLSLIB%\%ASSETBUILDER%
:START
%RS_TOOLSRUBY% AssetBuilder.rb --project=%RS_PROJECT% --branch=dev

# Print our restart time to file
TIME /T >> restart_dev.txt
DATE /T >> restart_dev.txt

GOTO START
POPD

ECHO Asset builder has quit.
PAUSE

REM AssetBuild.bat
