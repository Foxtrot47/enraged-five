@ECHO OFF
REM
REM File:: %RS_TOOLSROOT%/script/util/check_p4_login.bat
REM Description:: Verify the user has a P4 Login.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 23 April 2012
REM

CALL setenv.bat >NUL
SET XMLSTARLET=%RS_TOOLSBIN%\xmlstarlet.exe

PUSHD %RS_PROJROOT%

SET P4_SERVER=
SET P4_USERNAME=
SET XMLSTARLET_COMMAND=%XMLSTARLET% sel -t -v "//local/scm/build/@server" %RS_TOOLSROOT%\local.xml
FOR /F "tokens=*" %%i IN ('%XMLSTARLET_COMMAND%') do (
	SET P4_SERVER=%%i
)
SET XMLSTARLET_COMMAND=%XMLSTARLET% sel -t -v "//local/scm/build/@username" %RS_TOOLSROOT%\local.xml
FOR /F "tokens=*" %%i IN ('%XMLSTARLET_COMMAND%') do (
	SET P4_USERNAME=%%i
)

ECHO P4 Server: %P4_SERVER%
ECHO P4 User:   %P4_USERNAME%

p4 login -s
IF %ERRORLEVEL%==0 GOTO EOF

:LOGIN
%RS_TOOLSBIN%\dialog\dialog.exe --special p4login %P4_SERVER% %P4_USERNAME%

p4 login -s
IF %ERRORLEVEL% NEQ 0 GOTO LOGIN

:EOF

POPD

REM Done.