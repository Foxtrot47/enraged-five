@ECHO OFF
CALL setenv.bat >NUL
CALL data_get_project_info.bat >NUL

IF "%PERFORCE_CURRENT_TOOLS_LABEL_NAME%"=="" (
	ECHO Run this bat file from explorer, not perforce!
	PAUSE
	goto:eof
)

setlocal EnableDelayedExpansion

set ADDITIONAL_P4_ARGS=%1

TITLE %RS_PROJECT% : Syncing current labelled tools...
PUSHD %RS_PROJROOT%

ECHO Unregistering Shell Extension
PUSHD %RS_TOOLSBIN%\install
CALL %RS_TOOLSBIN%\install\unregister_shell_extension.bat
POPD

set UPDATE_ABORTED=0
set SYNC_ERROR=0

if not exist %RS_TOOLSROOT%/logs/ (
	mkdir %RS_TOOLSROOT%/logs/
)
del "%RS_TOOLSROOT%\logs\tools_sync.txt" 2> NUL

:: Check whether 3ds max, motion builder or the workbench are running.
set WAIT_PROCESS=3dsmax.exe
call:WaitForAppClose
if "%UPDATE_ABORTED%"=="1" goto:eos
set WAIT_PROCESS=motionbuilder.exe
call:WaitForAppClose
if "%UPDATE_ABORTED%"=="1" goto:eos
set WAIT_PROCESS=Workbench.exe
call:WaitForAppClose
if "%UPDATE_ABORTED%"=="1" goto:eos

:: Kill systray, rag app and rag before continuing.
set KILL_PROCESS=SysTrayRfs.exe
call:KillProcess
if "%UPDATE_ABORTED%"=="1" goto:eos
set KILL_PROCESS=ragApp.exe
call:KillProcess
if "%UPDATE_ABORTED%"=="1" goto:eos
set KILL_PROCESS=rag.exe
call:KillProcess
if "%UPDATE_ABORTED%"=="1" goto:eos

:: Make sure the user is logged into perforce and sync the various things we need.
CALL %RS_TOOLSROOT%\script\util\check_p4_login.bat

set LABEL_SYNC_RESULT=0

echo Syncing ironlib
p4 sync %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/ironlib/...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% 2>> "%RS_TOOLSROOT%/logs/tools_sync.txt"
IF %ERRORLEVEL% EQU 1 (
	start notepad %RS_TOOLSROOT%/logs/tools_sync.txt
	set LABEL_SYNC_RESULT=1
)

echo Syncing tools and metadata defitions
CALL %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\perforce\p4_sync.rb %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% %RS_ASSETS%/metadata/definitions/...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% %RS_ASSETS%/metadata/content/...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% //gta5_dlc/mpPacks/*/*.xml //gta5_dlc/spPacks/*/*.xml
IF %ERRORLEVEL% EQU 1 (
	set LABEL_SYNC_RESULT=1
)

echo Syncing wildwest and techart
p4 sync %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/techart/...
p4 sync %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/wildwest/...
p4 sync %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/dcc/current/max2012/ui/macroscripts/rstecharttools.mcr
p4 sync %ADDITIONAL_P4_ARGS% %RS_TOOLSROOT%/dcc/current/max2012/scripts/pipeline/rstecharttools.ms

IF %LABEL_SYNC_RESULT% EQU 0 (
	ECHO .
	ECHO Grab successful.
)
IF %LABEL_SYNC_RESULT% EQU 1 (
	ECHO .
	ECHO WARNING: Errors were reported during the grab.
)

ECHO Registering Shell Extension
PUSHD %RS_TOOLSBIN%\install
CALL %RS_TOOLSBIN%\install\register_shell_extension.bat
POPD

:: Check whether we need to run the tools installer.
set XMLSTARLET=%RS_TOOLSBIN%\xmlstarlet.exe
set LATEST_VERSION=0
set INSTALLED_VERSION=0
set TOOLS_LABEL=""

for /F "tokens=*" %%i in ('%XMLSTARLET% sel -t -v "/config/@version" %RS_TOOLSROOT%\config.xml') do (
	set LATEST_VERSION=%%i
)
for /F "tokens=*" %%i in ('%XMLSTARLET% sel -t -v "/local/@version" %RS_TOOLSROOT%\local.xml') do (
	set INSTALLED_VERSION=%%i
)
for /F "tokens=*" %%s in ('%XMLSTARLET% sel -t -v "/version/@label" %RS_TOOLSROOT%\etc\version.xml') do (
	set TOOLS_LABEL=%%s
)

echo Latest Tools Config Version: %LATEST_VERSION%
echo Installed Tools Config Version: %INSTALLED_VERSION%
echo Tools Label Version: %TOOLS_LABEL%

if %LATEST_VERSION% NEQ %INSTALLED_VERSION% (
	echo .
	echo Tools are out of date.  Running the installer.
	call %RS_TOOLSROOT%\install.bat
)

:: End of Script
:eos
if "%UPDATE_ABORTED%"=="1" (
	echo .
	echo Update was aborted.
)

PAUSE
goto:eof


:: Method for checking if max is running and waiting until max has been closed by the user.
:WaitForAppClose
echo Checking if %WAIT_PROCESS% is running.

tasklist /FI "IMAGENAME eq %WAIT_PROCESS%" 2>NUL | find /I /N "%WAIT_PROCESS%">NUL
if "%ERRORLEVEL%"=="0" (
	echo %WAIT_PROCESS% is running, requesting user to close.
	
	%RS_TOOLSBIN%\dialog\dialog.exe --b OKCancel --i exclamation "The update script has detected that %WAIT_PROCESS% is running.||Please close it before pressing 'Ok' to continue.|'Cancel' will abort the tools update." "%WAIT_PROCESS% is running"
	set RESULT=!ERRORLEVEL!
	
	if "!RESULT!"=="0" (
		goto:WaitForAppClose
	) else (
		set UPDATE_ABORTED=1
	)
)

goto:eof


:: Method for attempting to kill a particular process
:KillProcess
echo Killing %KILL_PROCESS%.

taskkill /IM %KILL_PROCESS%
PING 1.1.1.1 -n 1 -w 1000 >NUL
tasklist /FI "IMAGENAME eq %KILL_PROCESS%" 2>NUL | find /I /N "%KILL_PROCESS%">NUL
if "%ERRORLEVEL%"=="0" (
	taskkill /F /IM %KILL_PROCESS%
	PING 1.1.1.1 -n 1 -w 1000 >NUL
	tasklist /FI "IMAGENAME eq %KILL_PROCESS%" 2>NUL | find /I /N "%KILL_PROCESS%">NUL
	
	if "!ERRORLEVEL!"=="0" (
		call:LoopUntilKilled
	)
)

goto:eof


:: Loops until the KILL_PROCESS has been killed
:LoopUntilKilled
echo Unable to kill %KILL_PROCESS%, requesting user to kill it manually.
%RS_TOOLSBIN%\dialog\dialog.exe --b OKCancel --i exclamation "The update script was unable to automatically stop the %KILL_PROCESS% process.||Please stop it via the task manager before pressing 'Ok' to continue.|'Cancel' will abort the tools update." "%KILL_PROCESS% is running"
set RESULT=%ERRORLEVEL%

if "%RESULT%"=="0" (
	tasklist /FI "IMAGENAME eq %KILL_PROCESS%" 2>NUL | find /I /N "%KILL_PROCESS%">NUL
	if "!ERRORLEVEL!"=="0" goto:LoopUntilKilled
) else (
	set UPDATE_ABORTED=1
)

goto:eof
