@ECHO OFF
REM copy_diffs.bat
REM Description : Copy files that differ from between 2 folders to a new folder
REM Author : Derek Ward <derek@rockstarnorth.com>
REM Date : 02 September 2013

call setenv>NUL

SET CALLER=%0 
SET PLATFORM=%1
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/script/copy_diffs.rb
REM SET SRC=C:\temp\ScoFilesForDerek_DevNetwork
REM SET COMPARE=C:\temp\ScoFileForDerek
REM SET SRC=%RS_PROJROOT%\script\dev_network\singleplayer\sco\RELEASE\%PLATFORM%
REM SET COMPARE=%RS_PROJROOT%\script\FinalSubmissionCompiledScripts\RELEASE\%PLATFORM%
REM SET WILDCARD=*.sco
REM SET EMAIL=false
REM SET DEST=%SRC%\diff_with_base

if not "%2"=="" SET SRC=%2 
if not "%3"=="" SET COMPARE=%3
if not "%4"=="" SET DEST=%4
if not "%5"=="" SET WILDCARD=%5
if not "%6"=="" SET EMAIL=%6
SET ARGS=--source_folder %SRC% --compare_folder %COMPARE% --dest_folder %DEST% --wildcard %WILDCARD% --email %EMAIL%

:START
ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO SOURCE FOLDER    = %SRC% 
ECHO COMPARE FOLDER   = %COMPARE% 
ECHO DEST FOLDER = %DEST%
ECHO COMPARE WILDCARD = %WILDCARD%
ECHO EMAIL = %EMAIL%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

:START_PROCESS
call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

:EXIT
ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%