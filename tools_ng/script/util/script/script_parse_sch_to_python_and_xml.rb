#
# Creates a formated list of commands that can be used to parse GTA script files using the old command format, swapping for the new commands 
#
# Author:: Thomas French
# Date:: 1 Oct 2008
#

#-----------------------------------------------------------------------------
# Function
#-----------------------------------------------------------------------------
#old command  - command previous to any change
#updated commands - commands that have char -> ped, car -> vehicle and vector change
#final commands -. had names standardised as well as updated

# changes from the old script commands to new: char -> ped, car -> vehicle, commmands return by value, format of commands has changed to be come standardied
#This uses 3 files to create the list, the GTA header file, the latest up to date header file and the relevamt user created commands audit file 
#Two sets of classes are created gta commands and the latest header files these are parsed form the relevant header files
#the old list then has all its char -ped substituions etc 
#this list is then run against the audited list (the audited list conatins of updated to final commands) to convert updated commands to final commands 
#The lis is chekced against the final command list (generated from our finla header file) any commands that dont mtach the final are maeked as being removed

#REGEXP_UNDERLINE = (/_/)
REGEXP_COMMENT_LINES_AND_ALL = (/[ \t]*[\/]{2,}(.*)/)
REGEXP_COMMENT_LINES = (/[ \t]*[\/]{2,}/)
REGEXP_INFO = (/[ \t]*[\/]{2,}[\s]*INFO[\s]*:/) 
REGEXP_PURPOSE = (/[ \t]*[\/]{2,}[\s]*PURPOSE[\s]*:/)
REGEXP_PARAM_NOTES = (/[ \t]*[\/]{2,}[\s]*PARAM NOTES[\s]*:/)
REGEXP_PARAM_RETURNS = (/[ \t]*[\/]{2,}[\s]*RETURNS[\s]*:/)
REGEXP_NATIVE_FUNC = (/[ \t]*NATIVE FUNC/)
REGEXP_NATIVE_PROC = (/[ \t]*NATIVE PROC/) 
REGEXP_FUNC = (/^[\s]*FUNC[\s]/)
REGEXP_PROC = (/^[\s]*PROC[\s]/)
REGEXP_QUOTES = (/"/)
REGEXP_NO_WIKI = (/[ \t]*[\/]{2,}[\s]*#NO_WIKI/)
REGEXP_END_NO_WIKI = (/[ \t]*[\/]{2,}[\s]*#END_NO_WIKI/) 
#Regular expressions for parsing xml file 
REGEXP_ROOT_PATH = (/NativePath=(".*")/i)
REGEXP_NATIVE_FILE = (/native=(".*")/i)

#include REXML
'pipeline/util/rexml_write_fix'
require 'rexml/document'
include REXML

#r* tools 
require 'pipeline/config/projects'
require 'pipeline/os/file'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/rage.rb'
require 'pipeline/util/rexml_write_fix'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	[ '--input', '-i', OS::Getopt::REQUIRED, 'input image filename.' ],
    [ '--output', '-o', OS::Getopt::REQUIRED, 'output filename.' ],
	[ '--native', '-n', OS::Getopt::BOOLEAN, 'create native cmds.' ],
	[ '--script', '-n', OS::Getopt::BOOLEAN, 'create script cmds.' ],
	[ '--xml', '-n', OS::Getopt::BOOLEAN, 'output xml format.' ],
	[ '--python', '-n', OS::Getopt::BOOLEAN, 'output python format.' ],
]

	
	
#class to descibe a script commad used to create an insatnce of each command
class ScriptCommand
	attr_accessor :funcname, :paramnotes, :purpose, :info, :returns, :filename
	
	def initialize()
		@funcname = nil
		@purpose = []  
		@paramnotes = [] 
		@info = []
		@filename = nil
		@returns = []
	end
	
end

# A clsss to give debug info about the files parsed
class Fileinfo
	attr_accessor :files
	
	def initialize()
		@files = []
	end
end

#reads an xml file at the moment X:\jimmy\script\dev\native\wikiUpdate to tells whch header files to compile
def ParseXMLForScriptHeadersUsingREXML(sourcefile, parsenative, parsescript) 
data =[]	
	root_path = nil
	public_root_path = nil
	doc = Document.new File.new(sourcefile)			# creates an xml trees
	
		#searches through the NATIVE HEADERS
		puts "parsenative" 
		doc.elements.each("WikiIncludeFiles/WikiHeading") do |element|
			root_path = element.attributes["path"]	# finds the root path of the native functiona
			element.elements.each('child::node()') do |newelement|
				if (parsenative == true)
					cmd = newelement.attributes["native"]
				else
					cmd = newelement.attributes["script"]
				end	
					if not (cmd == nil)
						data << root_path + cmd
						puts root_path + cmd
						cmd = ''
					end
			end
			
		end
	return data
end

#fills the command objects with a parased file
def ParseScriptHeadersForCommands (source_files)
data = []   #defines a hash object
newfile = Fileinfo.new()
		begin
			source_files.each do |inc_path|		#look thorugh all our data files and parse them
				File.open( inc_path ) do |fp|	#open each file at a time  parse
					currentcommand = nil
					extracommentlines = nil
					bcancreatenewcommand = true
					bcheckformultiplelines = false
					bFoundRexp = false
					setflags = false
					bcanparse = true
					newfile.files << inc_path
					puts inc_path
					data[0] = newfile				#reserves the first element for a list of th files that it has parsed
					
					fp.each do |line|
						# Skip blank lines
						if (line =~ REGEXP_NO_WIKI)
							bcanparse = false		#checks for a special token to see if the command can be added to the wiki
						end
						
						if (line =~ REGEXP_END_NO_WIKI)
							bcanparse = true
							next
						end
						
						if (bcanparse == true)
							line.chomp!
							bFoundRexp = false
							
							#skip empty lines
							if ( 0 == line.size )
								bcheckformultiplelines = false
								next
							end
							
							if (bcancreatenewcommand == true)			# this flag is set once a FUNCTION or PROC key word has been found
								currentcommand = ScriptCommand.new()	#create a tempoary object as we dont know the order in which the text will appaer
								bcancreatenewcommand = false
							end

							if( line =~ REGEXP_PARAM_NOTES) 			#check for the param notes key word
								extracommentlines = "Paramnotes"
								currentcommand.paramnotes << line
								bFoundRexp = true
								bcheckformultiplelines = true
							end
							
							if( line =~ REGEXP_PURPOSE) 				#check for the PURPOSE: key word
								extracommentlines = "Purpose"
								currentcommand.purpose << line
								bFoundRexp = true
								bcheckformultiplelines = true
							end
							if( line =~ REGEXP_INFO) 					#Check for the 
								#puts line
								extracommentlines = "Info"
								currentcommand.info << line
								bFoundRexp = true
								bcheckformultiplelines = true
							end
							if( line =~ REGEXP_PARAM_RETURNS)
								xtracommentlines = "Returns"
								currentcommand.returns << line
								bFoundRexp = true
								bcheckformultiplelines = true
							end
							#our functions are only one one line so reset the parse count
							if( (line =~ REGEXP_NATIVE_FUNC) || (line =~ REGEXP_NATIVE_PROC) || (line =~ REGEXP_PROC) || (line =~ REGEXP_FUNC)) 
								currentcommand.funcname = line		#copies the function name
								bFoundRexp = true
								bcheckformultiplelines = false		
								bcancreatenewcommand = true
								if not (line =~ REGEXP_COMMENT_LINES)	#checks to see that this line is not commented out 
									currentcommand.filename = File.basename( inc_path , ".sch")
									data << currentcommand	#copies the contetnts of the temp object into the array of object to be returned for the function 
								else
									currentcommand = nil	
								end
							end
							
							# deals with commenst over multilpe lines 
							if not bFoundRexp	#No key word was found this line 
								if (bcheckformultiplelines == true)	#one must have been found the line before somewhere keep checking until we find another key word of a function
									if ( line =~ REGEXP_COMMENT_LINES_AND_ALL) #grabs the comment line and evrything after it does not support /* as comments		
										tempdata = (REGEXP_COMMENT_LINES_AND_ALL.match(line))
										if not tempdata.nil?	#checks that there is text after the comment
											newline = tempdata[1]
											newline = newline.strip
											case extracommentlines	#fills out the the relevavnt object structure
												when "Info" then currentcommand.info << newline
												when "Purpose" then currentcommand.purpose << newline
												when "Paramnotes" then currentcommand.paramnotes <<  newline
												when "Returns" then currentcommand.returns <<  newline
												else puts "failed to add a command"
											end		
										end
									end
								end
							end
						end	
					end					
				end	
			
			end

		rescue Exception => ex
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		end
		
	return data
end

#Formats the output for simons python proprietory system
def formatoutputforpython (input_data, output_file)
	begin 
		currentfilename = nil
		File.open( output_file , "w+" ) do |fp|
			fp.puts("[[sanScript(JAVACODE)]]")
			fp.puts("")
			fp.puts("[[TOC]]")
			fp.puts("")
			input_data.each_with_index do |line, command_index|
				
				#print the file info to the output window
				if (command_index == 0)	
#					puts "Parsed the following files:\n"
#					input_data[0].files.each_with_index do |line, extra_info_index|
#						fileinfo = input_data[0].files[extra_info_index]
#						puts fileinfo
#					end
				else
				
				#-------------------------- Need to print the file name when we are on to a new file lets check our file name and print when files a diff----------
				
				if (input_data[command_index].filename != currentfilename)
					currentfilename = input_data[command_index].filename 
					fp.puts("== " + currentfilename + " ==")
					fp.puts("")
				end
				
				#----------------  COMMAND NAME ----------------------------------------
					tempfunction = input_data[command_index].funcname
					tempdata = ( /([a-zA-Z_0-9]+([\s]*))\(/.match(input_data[command_index].funcname ) )	# gets the command name 
					
					if not tempdata.nil?
						cmdsummary = tempdata[1]
						cmdsummary = cmdsummary.chomp
						cmdsummary = cmdsummary.strip
						cmdsummary = "#"+cmdsummary+"#"	#adds the #'s after them
						tempfunction = tempfunction.gsub(tempdata[1],cmdsummary)	#copies the #ed version back into the original
						tempfunction = tempfunction.gsub(')', '') 					# removes any brackets	
						tempfunction = tempfunction.gsub('(', '') 
						#puts input_data[command_index].funcname 
					end
					pythonline = "[[sanScript(" + tempfunction + "#"		#ends up with a double ## m,ay need fixed
				#	fp.puts(pythonline)	
					
				#--------------------------------------- COMMENT LINES ----------------------------	
					
					purposeline = ""
					infoline = ""
					paramsline = ""
					
				#-> PURPOSE
					input_data[command_index].purpose.each_with_index do |line, extra_purpose_index|
						newpurposeformat = input_data[command_index].purpose[extra_purpose_index]
						newpurposeformat = newpurposeformat.gsub(REGEXP_QUOTES, "")				#remove qoutes 
						newpurposeformat = newpurposeformat.gsub(REGEXP_COMMENT_LINES, "")		#remove comments
						if (extra_purpose_index == 0 )
							purposeline = newpurposeformat + "<br/>" #add this at each line end so python knows its a line end			
						else
							purposeline = purposeline + newpurposeformat  + "<br/>"
						end
					end
				
				#-> INFO
					input_data[command_index].info.each_with_index do |line, extra_info_index|
						newinfoformat = input_data[command_index].info[extra_info_index]
						newinfoformat = input_data[command_index].info[extra_info_index].gsub(REGEXP_QUOTES, "")
						newinfoformat = newinfoformat.gsub(REGEXP_COMMENT_LINES, "")
						if (extra_info_index == 0 )
							infoline = newinfoformat  + "<br/>" #add this at each line end so python knows its a line end			
						else
							infoline = infoline + newinfoformat  + "<br/>"
						end
					end	
				
				#-> PARAM_NOTES 				
					input_data[command_index].paramnotes.each_with_index do |line, extra_params_index|
						newparamformat = input_data[command_index].paramnotes[extra_params_index]
						newparamformat = newparamformat.gsub(REGEXP_QUOTES, "")
						newparamformat = newparamformat.gsub(REGEXP_COMMENT_LINES, "")
						if (extra_params_index == 0 )
							paramsline = newparamformat  + "<br/>"  #add this at each line end so python knows its a line end			
						else
							paramsline = paramsline + newparamformat  + "<br/>"
						end
					#	puts paramsline
					end
				
				#-> RETURNS
					input_data[command_index].returns.each_with_index do |line, extra_returns_index|
						newparamformat = input_data[command_index].returns[extra_returns_index]
						newparamformat = newparamformat.gsub(REGEXP_QUOTES, "")
						newparamformat = newparamformat.gsub(REGEXP_COMMENT_LINES, "")
						if (extra_returns_index == 0 )
							paramsline = newparamformat  + "<br/>"  #add this at each line end so python knows its a line end			
						else
							paramsline = paramsline + newparamformat  + "<br/>"
						end
					#	puts paramsline
					end
					
					
				#-> CLOSING THE PYTHON FORMAT	
					
					fp.puts(pythonline + purposeline +infoline+ paramsline +")]]")
					fp.puts("")
					
				end
			end	
			
			
			puts "Number of commands parsed: " + (input_data.length - 1).to_s		#the first element is reseverd for file info
			
		end
	end
end 

def formatoutputforXML (input_data, output_file)
	begin
		doc = Document.new			#create our REXML tree structure
		root = doc.add_element "ScriptCommands.doc "	#add our root element
		
		input_data.each_with_index do |line, command_index|
			if (command_index > 0)		# the first element of our structure has been reserved for file info so 
				
				#-> COMMAND NAME add as an element all comment lines are children of this element
				
				funcname = input_data[command_index].funcname
				funcname = funcname.gsub('&', "&amp;")
				funcname = funcname.gsub('<', "&lt;")
				funcname = funcname.gsub('>', "&gt;")
				command_el = root.add_element "command" , {"name"=>funcname}		#add the element assign it a value
				
				tempdata = ( /([A-Z_0-9]+([\s]*))\(/.match(input_data[command_index].funcname ) )
				if not tempdata.nil?
					cmdsummary = tempdata[1]
					cmdsummary = cmdsummary.strip
					command_el.add_attribute("cmdsummary",cmdsummary)	# give a summary of the command just its name 
				end
				
				#-> PURPOSE COMMENT
				purpose_e1 =  command_el.add_element "purpose"
	
				input_data[command_index].purpose.each_with_index do |line, extra_info_index|
					newpurposeformat = input_data[command_index].purpose[extra_info_index]
					newpurposeformat = newpurposeformat.gsub(REGEXP_QUOTES, "")
					newpurposeformat = newpurposeformat.gsub(REGEXP_COMMENT_LINES, "")
					newpurposeformat = newpurposeformat.gsub('&', "&amp;")
					newpurposeformat = newpurposeformat.gsub('<', "&lt;")
					newpurposeformat = newpurposeformat.gsub('>', "&gt;")
					purposebody = purpose_e1.add_element "purposebody" , {"purposetext"=>newpurposeformat}	#Add each line of text as an element to retain the formatting and get round text node issuess todo with formatting and underlined text
				end
				
				#-> PARAM_NOTES  COMMENT
				paramnotes_e1 =  command_el.add_element "paramnotes" 	
				
				input_data[command_index].paramnotes.each_with_index do |line, extra_info_index|
					newparamformat = input_data[command_index].paramnotes[extra_info_index]
					newparamformat = newparamformat.gsub(REGEXP_QUOTES, "")
					newparamformat = newparamformat.gsub(REGEXP_COMMENT_LINES, "")
					newparamformat = newparamformat.gsub('&', "&amp;")
					newparamformat = newparamformat.gsub('<', "&lt;")
					newparamformat = newparamformat.gsub('>', "&gt;")
					parmnotesbody = paramnotes_e1.add_element "parambody" , {"paramtext"=>newparamformat}
				end
				
				#-> INFO COMMENT
				info_e1 =  command_el.add_element "info"
				
				input_data[command_index].info.each_with_index do |line, extra_info_index|
					newinfoformat = input_data[command_index].info[extra_info_index].gsub(REGEXP_QUOTES, "")
					newinfoformat = newinfoformat.gsub(REGEXP_COMMENT_LINES, "")
					newinfoformat = newinfoformat.gsub('&', "&amp;")
					newinfoformat = newinfoformat.gsub('<', "&lt;")
					newinfoformat = newinfoformat.gsub('>', "&gt;")
					infobody = info_e1.add_element "infobody" , {"infotext"=>newinfoformat}
				end
				
				#-> INFO COMMENT
				returns_e1 =  command_el.add_element "returns"
				
				input_data[command_index].returns.each_with_index do |line, extra_info_index|
					newinfoformat = input_data[command_index].returns[extra_info_index].gsub(REGEXP_QUOTES, "")
					newinfoformat = newinfoformat.gsub(REGEXP_COMMENT_LINES, "")
					newinfoformat = newinfoformat.gsub('&', "&amp;")
					newinfoformat = newinfoformat.gsub('<', "&lt;")
					newinfoformat = newinfoformat.gsub('>', "&gt;")
					infobody = returns_e1.add_element "returnbody" , {"returntext"=>newinfoformat}
				end
				
				
			end
		end
		
		
		
		#-> WRITE OUT THE XML TREE TO PASSED IN FILE NAME 
		
		File.open( output_file, 'w+' ) do |file|
			outputXML = REXML::Document.new()
			outputXML << XMLDecl.new
			outputXML << root
			#outputXML.write( file, 2 )
			
			fmt = REXML::Formatters::Pretty.new()
			fmt.write( outputXML, file ) 
		end

		
	rescue Exception => ex
		puts "Exception: #{ex.message}"
		puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
	end	
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
	
	#---------------------------------------------------------------------
    # Parse Command Line
	#---------------------------------------------------------------------
	g_Input = ''
	g_Output = ''
	g_Native = false
	g_Script = false
	gXML = false
	g_Python = false
	#the files are created from a bat file. The bat file specifies the location of the xml files it should parse and the destination of the output file.
    opts, trailing = OS::Getopt.getopts( OPTIONS )
	
	g_Output = ( nil == opts['output'] ) ? nil : ::File::expand_path( opts['output'] )
	if ( g_Output.nil? ) then
		puts 'No output itd file specified.'
		puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
		exit( 2 )
	end
	
	g_Input = ( nil == opts['input'] ) ? nil : ::File::expand_path( opts['input'] )
	if ( g_Input.nil? ) then
		puts 'No input file specified.'
		puts OS::Getopt.usage( OPTIONS, TRAILING_DESC )
		exit( 2 )
	end
	
	gXML = opts['xml']				#tell the output to be xml	
	g_Python = opts['python']		#tell the output to be python
	
	g_Script = opts['script']
	g_Native = opts['native']
	
	#Array of the command objects
	final_commands = []
	
	#header files to parse
	script_header_files = []
	
	script_header_files = ParseXMLForScriptHeadersUsingREXML(g_Input, g_Native, g_Script) 
	
	#Generates an array of all the command objects 
	final_commands = ParseScriptHeadersForCommands(script_header_files)
	
	#Generates the python ouput
	if (g_Python == true)
		formatoutputforpython(final_commands, g_Output)
	end
	
	#Generates the XML output
	if (gXML == true)
		formatoutputforXML(final_commands, OS::Path::replace_ext(g_Output, 'xml')) #if xml is selected then print this out
	end
	#let the user see what files have been parsed 
	#puts "Press enter to exit"
	#puts ""
		
	#$stdin.gets.chomp 
	
rescue Exception => ex
	puts "Exception: #{ex.message}"
	puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
end


