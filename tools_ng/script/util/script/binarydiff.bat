@ECHO OFF
REM Binarydiff.bat
REM Description : Binary diff script folders
REM Author : Derek Ward <derek@rockstarnorth.com>
REM Date : 28 August 2013

call setenv

SET CALLER=%0 
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/script/binarydiff.rb
SET SRC=%RS_PROJROOT%\script\BaselineScriptsForTitleUpdateComparisons\RELEASE\sco
SET COMPARE=%RS_PROJROOT%\script\dev_network\singleplayer\sco\RELEASE
SET WILDCARD=*.sco
SET EMAIL=false
if not "%1"=="" SET SRC=%1 
if not "%2"=="" SET COMPARE=%2
if not "%3"=="" SET WILDCARD=%3
if not "%4"=="" SET EMAIL=%4
SET ARGS=--source_folder %SRC% --compare_folder %COMPARE% --wildcard %WILDCARD% --email %EMAIL%

:START
ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO SOURCE FOLDER    = %SRC% 
ECHO COMPARE FOLDER   = %COMPARE% 
ECHO COMPARE WILDCARD = %WILDCARD%
ECHO EMAIL = %EMAIL%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

:START_PROCESS
call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSROOT%\bin\ironruby\bin\ir.exe %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

:EXIT
ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%