@ECHO OFF
REM global_sig_checker.bat
REM Description : Checks hash consistency for files output from scriptbuidler
REM Author : Derek Ward <derek@rockstarnorth.com>
REM Date : 29 August 2013

call setenv

SET CALLER=%0 
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/script/global_sig_checker.rb
SET SRC=%RS_PROJROOT%\script\dev_network\singleplayer\sco\RELEASE
SET WILDCARD=*.global_block.txt
SET EMAIL=false
if not "%1"=="" SET SRC=%1 
if not "%2"=="" SET WILDCARD=%2
if not "%3"=="" SET EMAIL=%3
SET ARGS=--source_folder %SRC% --wildcard %WILDCARD% --email %EMAIL%

:START
ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO SOURCE FOLDER    = %SRC% 
ECHO WILDCARD = %WILDCARD%
ECHO EMAIL = %EMAIL%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

:START_PROCESS
call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSROOT%\bin\ironruby\bin\ir.exe %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

:EXIT
ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%