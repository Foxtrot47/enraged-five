@echo off

set renders_path="x:\gta5\titleupdate\dev_ng\Renders\"

set story_path="x:\gta5_dlc\art\dev\peds\PedPackGallery\Peds_Story\"
set world_path="x:\gta5_dlc\art\dev\peds\PedPackGallery\Peds_World\"
set animal_path="x:\gta5_dlc\art\dev\peds\PedPackGallery\Animals\"

TITLE Move Ped Renders

md %story_path%
md %world_path%

for /f "eol=: delims=" %%F in ('dir /b /a-d %renders_path%*_CS_*') do (move /y %renders_path%%%F %story_path%%%F)
for /f "eol=: delims=" %%F in ('dir /b /a-d %renders_path%*_CSB_*') do (move /y %renders_path%%%F %story_path%%%F)
for /f "eol=: delims=" %%F in ('dir /b /a-d %renders_path%*_IG_*') do (move /y %renders_path%%%F %story_path%%%F)

for /f "eol=: delims=" %%F in ('dir /b /a-d %renders_path%*_a_c_*') do (move /y %renders_path%%%F %animal_path%%%F)

for /f "eol=: delims=" %%F in ('dir /b /a-d %renders_path%*') do (move /y %renders_path%%%F %world_path%%%F)

