@ECHO OFF
REM dlc_global_sig_checker.bat
REM Description : Checks that DLC scripts only use certain global blocks.
REM Checks all .global_block.txt files against a text file of hashes for global blocks
REM If the text file specifies a hash value for a global block then all .global_block.txt files that list that global block must have that exact hash value
REM If the text file has a hash of 0 for a global block then the .global_block.txt files don't have to have a specific hash value. The hash should still be the same across all .global_block.txt files, 
REM    but that value doesn't have to match a value specified in the text file.
REM If the text file doesn't list a global block index then the .global_block.txt files can't contain that global block
REM Author : Graeme Williamson - copied from Derek Ward's global_sig_checker.bat
REM Date : 15 January 2014

call setenv

SET CALLER=%0 
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/script/dlc_global_sig_checker.rb
REM SET SRC=%RS_PROJROOT%\script\dev_network\singleplayer\sco\RELEASE
SET WILDCARD=*.global_block.txt
SET EMAIL=false


if "%1" == "" (
    echo The first parameter should be the folder containing the .global_block.txt files
    EXIT /B -1
) ELSE (
    SET SRC=%1
)

if "%2" == "" (
    echo The second parameter should be the name of the text file containing the hashes of the global blocks
    EXIT /B -1
) ELSE (
    SET TEXT_FILE=%2
)


SET ARGS=--source_folder %SRC% --text_file %TEXT_FILE% --wildcard %WILDCARD% --email %EMAIL%


:START
ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO SOURCE FOLDER    = %SRC% 
ECHO TEXT FILE = %TEXT_FILE%
ECHO WILDCARD = %WILDCARD%
ECHO EMAIL = %EMAIL%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

:START_PROCESS
call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSROOT%\bin\ironruby\bin\ir.exe %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

:EXIT
ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%