@ECHO OFF
REM copy_matching_files.bat
REM Description : Compare two folders of files. Any files that match are copied to a third folder - used for scripts in TU. Variation on Derek's copy_diffs.bat
REM Author : Derek Ward <derek@rockstarnorth.com>, modified by Graeme
REM Date : 02 September 2013

call setenv

SET CALLER=%0 
REM SET PLATFORM=%1
SET PROCESS=%RS_TOOLSROOT%/ironlib/util/script/copy_matching_files.rb
SET SRC=%RS_PROJROOT%\script\dev\singleplayer\sco\JAPANESE_RELEASE\
SET COMPARE=%RS_PROJROOT%\script\dev_network\singleplayer\sco\JAPANESE_RELEASE\
SET WILDCARD=*.sco
SET EMAIL=false
SET DEST=%RS_PROJROOT%\script\BaselineScriptsForTitleUpdateComparisons\JAPANESE_RELEASE\sco

REM if not "%2"=="" SET SRC=%2 
REM if not "%3"=="" SET COMPARE=%3
REM if not "%4"=="" SET WILDCARD=%4
REM if not "%5"=="" SET DEST=%5
REM if not "%6"=="" SET EMAIL=%6
SET ARGS=--source_folder %SRC% --compare_folder %COMPARE% --dest_folder %DEST% --wildcard %WILDCARD% --email %EMAIL%

:START
ECHO *************************************************************************************
ECHO %CALLER% 
ECHO %PROCESS% %ARGS%
ECHO SOURCE FOLDER    = %SRC% 
ECHO COMPARE FOLDER   = %COMPARE% 
ECHO DEST FOLDER = %DEST%
ECHO COMPARE WILDCARD = %WILDCARD%
ECHO EMAIL = %EMAIL%
ECHO start %date% %time%
ECHO *************************************************************************************
ECHO.

:START_PROCESS
call %RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %PROCESS% %ARGS%
SET RETCODE=%ERRORLEVEL%
ECHO %PROCESS% RETCODE: %RETCODE%

:EXIT
ECHO.
ECHO completed %date% %time%
EXIT /B %RETCODE%