#
# mk_actual_assetlist.rb
# Make Actual Asset Lists
#
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 29 February 2008
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------


REGEXP_UNDERLINE = (/_/)
REGEXP_COMMENT_LINES = /^[ \t]*\/\/.*/i

REGEXP_EXTRA_INFO = (/~XP/i) 
EXTRA_PARAM_TEXT = " @_No_of_params "

REGEXP_PARAM_SWAP = (/~SP/i)
PARAM_CHANGE_TEXT = " @_bool_param_neg_to_pos "

REGEXP_REMOVE_CMD = (/~TR/i)
REMOVE_CMD_TEXT = " @_remove_cmd "

REGEXP_RETURN_BY_VALUE_CMD = (/~rbv/i)
RETURN_BY_VALUE_CMD_TEXT = " @_return_by_value"

#DATA_FILES = 'X:\docs\code\Script\Commands Audit\Ped  Commands Audit.txt'
#DATA_FILES = 'X:\docs\code\Script\OutputTestSCFile.txt'

require 'pipeline/config/projects'
require 'pipeline/os/path'
include Pipeline

c = Pipeline::Config::instance


=begin DATA_FILES = [OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsAudio.txt' ),
            OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsBrains.txt'), 
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsClock.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsCutscene.txt'), 
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsDebug.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsGang.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsGraphics.txt'), 
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsHud.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsMisc.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsObject.txt'), 
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsPad.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsPed.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsPlayer.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsScript.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsTask.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsVehicle.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsWeapon.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsZone.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsStreaming.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsFire.txt') ,
			OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsCamera.txt'),  
			OS::Path::combine( c.toolsconfig, 'script', 'parsedEntityCommands.txt' )
			 ]	
=end
	
	DATA_FILES = [OS::Path::combine( c.toolsconfig, 'script', 'parsedEntityCommands.txt' ), 
					OS::Path::combine( c.toolsconfig, 'script', 'parsedCommandsCutSceneNew.txt' )]
	
def open_and_read_input_file ()
command_names = {}   #defines a hash object

		begin
			DATA_FILES.each do |inc_path|		#look thorugh all our data files and parse them
				File.open( inc_path ) do |fp|	#open each file at a time  parse
					fp.each do |line|
						# Skip blank lines
						next if ( 0 == line.size )
						
						#Skip comment lines
						next if ( line =~ REGEXP_COMMENT_LINES)
							
						#throw Exception.new( 'testing' )
						#puts fp.lineno	
						if (line =~ REGEXP_UNDERLINE)
							command = line.split('->')		#splits the command on the symbol to create our regular expression 
							
							#r = Regexp.new( command[0].strip.upcase + "([ \t]*)\("
							r = Regexp.new( "#{command[0].strip.upcase}([ \t]*)\\(", true )  #inlcudes white space
							#r = Regexp.new(command[0].strip.upcase, true)		#use the regular expression to identify old commands
							
							command_names[r] = command[1].strip.upcase	#fill the  object using the regular expression as the key 
							
							#puts r.source
							#puts command[1]
						
						end	
					end
				end
			end
		rescue Exception => ex
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		ensure			
			#fp.close()
		end
	
	command_names.each_pair do |k,v|
		#puts "#{k} #{v}"
		end
		
	return command_names
end

def open_and_write_to_target_script (command_dict, input_file)
	
	#input_file = "X:/docs/code/Script/TestSCFile.txt" 
	#output_file = "X:/docs/code/Script/OutputTestSCFile.txt" 
	
	
	local_script_copy = []
		
	begin	
		File.open( input_file , "r" ) do |fp|
			local_script_copy = fp.readlines
		end
			
		begin 
			local_script_copy.each_with_index do |line, index|
				# Skip blank lines
				next if ( 0 == line.size )
				if (line =~ REGEXP_UNDERLINE)
					command_dict.each_pair do |k,v|
						regexp = k				# this is our old command set as a regular exprsion used  to comapre old strings in the int the text fil
						replace = ( v + "(")	# this is a new command 
						line.chomp! #remove the end of line rerturns so we can add our extra info
						
						if (line =~ regexp)
							#write our new array line
							# still remmoving  a line  from 
						
							$stdout.print("Swap ", index+1 , ": ", local_script_copy[index], "\n"  )
							local_script_copy[index] = line.gsub!(regexp, replace) 	# add in info about the file
							
							if (line =~ REGEXP_RETURN_BY_VALUE_CMD ) 	# should only run on  converting the old commands to new when parsing commands old headers
								local_script_copy[index] = local_script_copy[index].gsub!(REGEXP_RETURN_BY_VALUE_CMD , RETURN_BY_VALUE_CMD_TEXT)  
							end
							
							if (v =~ REGEXP_PARAM_SWAP ) 	# check that the line we are going to replace requires some user in put 
								local_script_copy[index] = local_script_copy[index].gsub!(REGEXP_PARAM_SWAP , PARAM_CHANGE_TEXT)  # remove the token info token from the replace string	
							end
							
							if (v =~ REGEXP_EXTRA_INFO )			
								local_script_copy[index] = local_script_copy[index].gsub!(REGEXP_EXTRA_INFO , EXTRA_PARAM_TEXT) # remove the token
							end
							
							if (v =~ REGEXP_REMOVE_CMD)
								local_script_copy[index] = local_script_copy[index].gsub!(REGEXP_REMOVE_CMD , REMOVE_CMD_TEXT) 
							end
							$stdout.print( index+1 , ": ", local_script_copy[index], "\n" ,"\n" )		# prints the line change to the console window
						end
					
					end
				end	
			end
		
			begin 
				File.open( input_file , "w" ) do |fp|
					local_script_copy.each do |line|
						#puts line
						fp.puts(line)			#use puts because the carrige returns have been removed by the chomp functions when formulating strings
					end
				end	
			rescue Exception => ex
				puts "Exception: #{ex.message}"
				puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
			ensure

			end
	
		rescue Exception => ex
			puts "Exception: #{ex.message}"
			puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
		end
				
	rescue Exception => ex
		puts "Exception: #{ex.message}"
		puts "\tStacktrace: #{ex.backtrace.join('\n\r')}"
	ensure
	
	puts "Parsing #{input_file} complete"
	puts ""
	
	end
	
		
end

#-----------------------------------------------------------------------------
# Entry Point
#-----------------------------------------------------------------------------

begin
	command_dictionary = open_and_read_input_file() 
	run = 1
	
	while (run == 1)
		puts "Enter the file path to parse (You can drag the file into the ruby window from windows explorer): Press q to quit"
		puts ""
		
		input = $stdin.gets.chomp 
		input = input.gsub('"', ""  )
		puts ""
		
		if(input == "q")
			run = 0
		end
		open_and_write_to_target_script( command_dictionary, input )
	end	
	
end