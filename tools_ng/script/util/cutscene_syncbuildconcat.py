import os
import sys

PROJECT = os.environ['RS_PROJECT']
PROJECT_ROOT = os.environ['RS_PROJROOT']
TOOLSROOT = os.environ['RS_TOOLSROOT']
CONCATLIST_DIR = PROJECT_ROOT + "\\assets\\cuts\\!!Cutlists\\"
CUTSCENE_DIR = PROJECT_ROOT + "\\assets\\cuts\\"

#os.system("p4 sync " + CONCATLIST_DIR + "...")
#os.system("p4 sync " + CUTSCENE_DIR + "...")

LOG_FILE = open(TOOLSROOT + "\\logs\\cutscene\\loggyloglog.txt", "w")

for file in os.listdir(CONCATLIST_DIR):
	if file.endswith(".concatlist"):
	
		SCENE_NAME = os.path.splitext(os.path.basename(file))[0]
		SCENE_PATH = PROJECT_ROOT + "\\assets\\cuts\\" + SCENE_NAME
	
		ANIMCONCATDRIVER_PATH = TOOLSROOT + "\\bin\\anim\\animconcatdriver.exe " + CONCATLIST_DIR + file + " -perforce"
		print ANIMCONCATDRIVER_PATH
		rtn = os.system(ANIMCONCATDRIVER_PATH)
		if rtn != 0:
			LOG_FILE.write("FAILED: " + file + " : " + SCENE_PATH + " : " + ANIMCONCATDRIVER_PATH + "\n\n")
			continue
		
		CUTSCENE_BUILD_PATH = TOOLSROOT + "\\lib\\util\\motionbuilder\\cutscene.rb " + '--build --inputDir=' + SCENE_PATH + '  --sectionMethod="0"  --sceneName=' + SCENE_NAME + ' --perforce'
		print CUTSCENE_BUILD_PATH
		rtn = os.system(CUTSCENE_BUILD_PATH)
		if rtn != 0:
			LOG_FILE.write("FAILED: " + file + " : " + SCENE_PATH + " : " + CUTSCENE_BUILD_PATH + "\n\n")
			continue
			
		PACK_FILE_PATH = TOOLSROOT + "\\lib\\pipeline\\resourcing\\gateways\\pack_file.rb " + "--project=" + PROJECT + " --rootsrcdir=" + SCENE_PATH + " --animsrcdir=" + SCENE_PATH + "\\obj" + " --scenename=" + SCENE_NAME + " --destdir=x:\gta5\art\anim\export_mb_compressed --outputdir=cuts/ --sectioned"
		print PACK_FILE_PATH
		rtn = os.system(PACK_FILE_PATH)
		if rtn != 0:
			LOG_FILE.write("FAILED: " + file + " : " + SCENE_PATH + " : " + PACK_FILE_PATH + "\n\n")
			continue
			
		LOG_FILE.write("SUCCESS: " + file)

LOG_FILE.close
		
IMAGE_FILE = TOOLSROOT + "\\lib\\pipeline\\resourcing\\gateways\\image_file.rb " + "--project=" + PROJECT + " --srcdir=cuts --imagename=cuts --exportsrcdir=" + PROJECT_ROOT + "\\assets\\cuts" + " --cutscene"
os.system(IMAGE_FILE)