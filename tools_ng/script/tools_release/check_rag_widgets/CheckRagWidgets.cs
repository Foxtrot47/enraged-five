//css_ref RSG.Rag.Clients.dll;
//css_ref RSG.Rag.Contracts.dll;
//css_ref RSG.Base.dll;
//css_ref RSG.Platform.dll;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using RSG.Base.Logging;
using RSG.Rag.Clients;
using RSG.Rag.Contracts.Data;

class Script
{
	private static readonly String _ulogViewer = "%RS_TOOLSROOT%/bin/UniversalLogViewer/UniversalLogViewer.exe";

	private static String[] _bugstarWidgets = new String[]
		{
			"_LiveEdit_/Bugstar",
			"_LiveEdit_/Bugstar/Warp Player x y z h vx vy vz",
			"_LiveEdit_/Bugstar/Warp now"
		};
		
	private static String[] _lightCreateWidgets = new String[]
		{
			"TimeCycle/Create Timecycle Widgets",
			"Cut Scene Debug/Toggle Cut Scene Debug bank"
		};
		
	private static String[] _lightWidgets = new String[]
		{
			"_LiveEdit_/Lights Edit",
			"_LiveEdit_/Lights Edit/Enable light editing",
			"_LiveEdit_/Lights Edit/Debug/Focus on current light",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Position X",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Position Y",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Position Z",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Direction X",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Direction Y",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Direction Z",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Tangent X",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Tangent Y",
			"_LiveEdit_/Lights Edit/Light Source/Position - Direction - Tangent/Tangent Z",
			"_LiveEdit_/Lights Edit/Light Source/Light type",
			"_LiveEdit_/Lights Edit/Light Source/Colour",
			"_LiveEdit_/Lights Edit/Light Source/Intensity",
			"_LiveEdit_/Lights Edit/Light Source/Falloff distance",
			"_LiveEdit_/Lights Edit/Light Source/Falloff exponent",
			"_LiveEdit_/Lights Edit/Light Source/Spot/Cone inner",
			"_LiveEdit_/Lights Edit/Light Source/Spot/Cone outer",
			"_LiveEdit_/Lights Edit/Light Source/Capsule/Capsule extent",
			"_LiveEdit_/Lights Edit/Light Source/Shadows/Shadow near clip",
			"_LiveEdit_/Lights Edit/Light Source/Culling plane/Enable",
			"_LiveEdit_/Lights Edit/Light Source/Culling plane/X",
			"_LiveEdit_/Lights Edit/Light Source/Culling plane/Y",
			"_LiveEdit_/Lights Edit/Light Source/Culling plane/Z",
			"_LiveEdit_/Lights Edit/Light Source/Culling plane/Dist",
			"_LiveEdit_/Lights Edit/Light Source/Volume/Volume outer colour",
			"_LiveEdit_/Lights Edit/Light Source/Volume/Volume outer intensity",
			"_LiveEdit_/Lights Edit/Light Source/Volume/Volume outer exponent",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Use volume outer colour",
			"_LiveEdit_/Lights Edit/Mesh display options/Show light mesh",
			"_LiveEdit_/Lights Edit/Mesh display options/Use hi-res light mesh",
			"_LiveEdit_/Lights Edit/Mesh display options/Wireframe",
			"_LiveEdit_/Lights Edit/Light Cost/Show light cost",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 00",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 01",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 02",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 03",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 04",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 05",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 06",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 07",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 08",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 09",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 10",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 11",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 12",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 13",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 14",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 15",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 16",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 17",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 18",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 19",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 20",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 21",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 22",
			"_LiveEdit_/Lights Edit/Light Source/Hours light is on/Hour 23",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Cast Static Shadows",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Cast Dynamic Shadows",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Calc From sun",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Draw volume",
			"_LiveEdit_/Lights Edit/Light Source/Flags/No specular",
			"_LiveEdit_/Lights Edit/Light Source/Effects/Buzzing",
			"_LiveEdit_/Lights Edit/Light Source/Effects/Forced Buzzing",
			"_LiveEdit_/Lights Edit/Light Source/Volume/Volume intensity",
			"_LiveEdit_/Lights Edit/Light Source/Volume/Volume size scale",
			"_LiveEdit_/Lights Edit/Light Source/Cutoff distances/Light",
			"_LiveEdit_/Lights Edit/Light Source/Cutoff distances/Shadow",
			"_LiveEdit_/Lights Edit/Light Source/Cutoff distances/Specular",
			"_LiveEdit_/Lights Edit/Light Source/Cutoff distances/Volumetric",
			"_LiveEdit_/Lights Edit/Light Source/Corona/Corona size",
			"_LiveEdit_/Lights Edit/Light Source/Corona/Corona intensity",
			"_LiveEdit_/Lights Edit/Light Source/Flags/Corona only",
			"_LiveEdit_/Lights Edit/Light Source/Corona/Corona z bias",
			
			"TimeCycle/Game/Weather and Time Overrides/Override Time",
			"TimeCycle/Game/Weather and Time Overrides/Curr Time"
		};

	private static ILog _log;
		
    public static int Main(string[] args)
    {
		LogFactory.Initialize();
		LogFactory.CreateApplicationConsoleLogTarget();
		_log = LogFactory.ApplicationLog;
		
		_log.Message("Checking RAG Widgets");
		
		try
		{
			IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();
			
			GameConnection connection = connections.FirstOrDefault(item => item.DefaultConnection);
			if (connection == null)
			{
				_log.Error("No game connections detected.");
			}
			else
			{
				WidgetClient client = RagClientFactory.CreateWidgetClient(connection);
				CheckBugstarWidgets(client);
				CheckLightWidgets(client);
			}
		}
		catch (Exception e)
		{
			_log.ToolException(e, "Unhandled exception");
		}
		
		// Shutdown the log factory displaying the universal log viewer if we detected any errors.
		LogFactory.ApplicationShutdown();
		if (_log.HasErrors)
		{
			Process.Start(Environment.ExpandEnvironmentVariables(_ulogViewer), LogFactory.ApplicationLogFilename);
		}
		return _log.HasErrors ? 1 : 0;
    }
	
	private static void CheckBugstarWidgets(WidgetClient client)
	{
		foreach (String widgetPath in _bugstarWidgets)
		{
			VerifyWidgetExists(client, widgetPath, "Bugstar");
		}
	}
	
	private static void CheckLightWidgets(WidgetClient client)
	{
		foreach (String widgetPath in _lightCreateWidgets)
		{
			CreateWidget(client, widgetPath, "Light Editing");
		}
		
		foreach (String widgetPath in _lightWidgets)
		{
			VerifyWidgetExists(client, widgetPath, "Light Editing");
		}
	}
	
	private static void VerifyWidgetExists(WidgetClient client, String widgetPath, String category)
	{
		if (!client.WidgetExists(widgetPath))
			_log.Error("{0} widget doesn't exist: {1}", category, widgetPath);
		else
			_log.Message("{0} widget exists: {1}", category, widgetPath);
	}
	
	private static void CreateWidget(WidgetClient client, String widgetPath, String category)
	{
		if (!client.WidgetExists(widgetPath))
		{
			_log.Warning( "{0} widget doesn't exist: {1}.  The widgets might already have been created.", category, widgetPath);
		}
		else
		{
			client.PressButton(widgetPath);
			_log.Message("{0} - Successfully pressed the {1} widget.", category, widgetPath);
			client.SendSyncCommand();
		}
	}
}