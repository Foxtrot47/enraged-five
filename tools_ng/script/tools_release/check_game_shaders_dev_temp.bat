@ECHO OFF
REM
REM File:: check_shaders.bat
REM Description::
REM		Ensures that ragebuilder can load the game's shaders.
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 16 April 2014
REM

CALL setenv.bat >NUL
SET BRANCH=dev_temp

TITLE %RS_PROJECT% : Check Game Shaders (%BRANCH%)
PUSHD %RS_PROJROOT%

SET TEST_ASSET=%RS_PROJROOT%\assets_ng\titleupdate\dev_temp\models\cdimages\streamedpeds\strm_peds_mpShare.itd.zip
p4 sync %TEST_ASSET%

REM Update the testing label to the current state of the Workspace.
%RS_TOOLSCONVERT% --platforms Win64,XboxOne,PS4 --branch %BRANCH% --rebuild %TEST_ASSET%
IF "%ERRORLEVEL%"=="0" (
	ECHO Conversion completed successfully.  This indicates that the shaders are fine.
) ELSE (
	ECHO An error occurred while trying to convert the graphics.itd.zip file.  This may indicate that there is a problem with the shaders.  Please check the universal log output for details.
)

POPD
PAUSE
REM Done.