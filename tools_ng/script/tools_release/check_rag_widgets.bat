@ECHO OFF
REM
REM File:: check_rag_widgets.bat
REM Description::
REM		Ensures that widgets required for live-editing exist in the game.
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1 April 2013
REM

CALL setenv.bat >NUL

TITLE %RS_PROJECT% : Check RAG Widgets

%RS_TOOLSROOT%\bin\cs-script\cscs.exe %cd%\check_rag_widgets\CheckRagWidgets.cs
ECHO.
IF "%ERRORLEVEL%"=="0" (
	ECHO All widgets required for live-editing were found in RAG.
) ELSE (
	ECHO Some widgets required for live-editing were missing.  Please double check with the developer in charge of the live-editing functionality to make sure that this is OK.
)

POPD
PAUSE
REM Done.