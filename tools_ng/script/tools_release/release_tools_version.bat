@ECHO OFF
REM
REM File:: release_tools_version.bat
REM Description:: Prompts the user as to which tools version he wishes to release.
REM
REM Author:: Michael Täschler <michael.taschler@rockstarnorth.com>
REM Date:: 08/03/2013
REM

CALL setenv.bat >NUL
CALL ../util/data_get_project_info.bat >NUL

set /P VERSION="Enter the tools version you wish to release (e.g. 24 or 32.1): "

TITLE %RS_PROJECT% : Setting current tools to version %VERSION%...
PUSHD %RS_PROJROOT%

REM Update the testing patch label to the current state of the Workspace.
%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\tools_release\release_tools_label.rb --version %VERSION%

PAUSE
REM Done.
