REM @ECHO OFF
REM
REM File:: ragebuilder_change_process.bat
REM Description:: Rebuild project data associated with Ragebuilder Change Process.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 1 July 2014
REM

CALL setenv.bat >NUL

TITLE %RS_PROJECT% : Ragebuilder Change Process Asset Rebuilds
PUSHD %RS_PROJROOT%

SET CONVERT_OPTIONS=-rebuild
SET EXPORT=%RS_EXPORT%

SET ASSET_LIST=%EXPORT%\levels\tools_test\tools_test.zip %EXPORT%\levels\gta5\vehicles_packed\adder.veh.zip %EXPORT%\data\effects\ptfx.zip %EXPORT%\models\cdimages\componentpeds\a_f_m_beach_01.ped.zip %EXPORT%\models\streamedpeds\player_zero.ped.zip

%RS_TOOLSCONVERT% %CONVERT_OPTIONS% %ASSET_LIST% 

POPD
REM Done.
PAUSE
