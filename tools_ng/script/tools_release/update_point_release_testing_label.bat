@ECHO OFF
REM
REM File:: update_testing_patch_label.bat
REM Description:: Update tools testing patch label to current workspace.
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 08/03/2013
REM

CALL setenv.bat >NUL
CALL ../util/data_get_project_info.bat >NUL

SET LABEL=%PERFORCE_TESTING_PATCH_TOOLS_LABEL_NAME%

TITLE %RS_PROJECT% : Updating %LABEL%...
PUSHD %RS_PROJROOT%

REM Update the testing patch label to the current state of the Workspace.
%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\tools_release\update_testing_label.rb --point-release

PAUSE
REM Done.
