@ECHO OFF
REM
REM File:: create_new_testing_label.bat
REM Description::
REM		Creates a new full release label.
REM		Updates tools testing label and new full release label to current workspace.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 19 December 2011
REM

CALL setenv.bat >NUL
CALL ../util/data_get_project_info.bat >NUL

SET LABEL=%PERFORCE_TESTING_TOOLS_LABEL_NAME%

TITLE %RS_PROJECT% : Updating %LABEL%...
PUSHD %RS_PROJROOT%

REM Update the testing label to the current state of the Workspace.
%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\tools_release\create_testing_label.rb

PAUSE
REM Done.