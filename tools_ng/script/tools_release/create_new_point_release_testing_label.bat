@ECHO OFF
REM
REM File:: create_new_testing_patch_label.bat
REM Description::
REM		Creates a new patch label.
REM		Updates tools testing label and new patch label to current workspace.
REM
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 08/03/2013
REM

CALL setenv.bat >NUL
CALL ../util/data_get_project_info.bat >NUL
SET LABEL=%PERFORCE_TESTING_PATCH_TOOLS_LABEL_NAME%

TITLE %RS_PROJECT% : Updating %LABEL%...
PUSHD %RS_PROJROOT%

:: Do a diff of the user's workspace against the currently released tools to show them what has changed.
echo Checking for differences between your local workspace and the currently released tools.
echo Please be patient as this may take a while.

del "%RS_TOOLSROOT%\logs\point_release_diff.txt" 2> NUL
p4 diff2 -q %RS_TOOLSROOT%\...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% %RS_TOOLSROOT%\...#have | grep -v identical > "%RS_TOOLSROOT%\logs\point_release_diff.txt"
::p4 sync -n %RS_TOOLSROOT%\...@%PERFORCE_CURRENT_TOOLS_LABEL_NAME% > "%RS_TOOLSROOT%\logs\point_release_diff.txt"
start /B notepad "%RS_TOOLSROOT%\logs\point_release_diff.txt"

%RS_TOOLSBIN%\dialog\dialog.exe --b OKCancel --i question "Notepad should have opened containing the list of files that are different between your local workspace and the current tools release.|If notepad didn't open it means that no differences exist, otherwise please check that the files listed are the ones that you wish to include in the new point release.||Select 'OK' to continue with creating the new point release tools label.|Select 'Cancel' to abort."

if "%ERRORLEVEL%"=="0" (
	:: User select OK so create a new point release testing label.
	%RS_TOOLSROOT%\ironlib\prompt.bat %RS_TOOLSIR% %RS_TOOLSROOT%\ironlib\util\tools_release\create_testing_label.rb --point-release
)

PAUSE
REM Done.
