@ECHO OFF
CALL setenv.bat >NUL
SET AUTOMATION_WORKER=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ClientWorker.exe
SET ARGS=-role assetbuilder,test -automation net.tcp://LOCALHOST:7010/automation.svc -filetransfer net.tcp://LOCALHOST:7010/FileTransfer.svc

CALL %RS_TOOLSROOT%/ironlib/prompt.bat %AUTOMATION_WORKER% %ARGS%

PAUSE