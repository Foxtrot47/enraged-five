# Powershell Script to automate generation of CSR.
#Requires -Version 2.0  
Param(
[switch]$Install
)

###### Define and Populate Variables ######
$Computer = hostname
$CRequestFileName = "C:\Windows\Temp\$Computer-CertRequest.req"
$CRRequestINFFileName = "C:\Windows\Temp\$Computer-CertRequest.inf"
$ReturnedCert = "C:\Windows\Temp\$Computer-IssuedCert"
$CA = "Certificates\Take-Two Interactive Issuing CA1"
##########################################################

function Install {
	$ErrorActionPreference = "Stop"
	# Get the Certificate Request Number from the Registry
	$ReqNo =  (get-ItemProperty HKLM:\Software\RockstarNorth\SSL -Name "ReqNo").reqno
	# Retrieve the cert
	Write-Output "Retrieving Cert Request: $ReqNo"
	certreq -retrieve -config "$CA" $reqNo $ReturnedCert
	# Import the Cert into Windows Store
	Write-Output "Installing the Cert into the windows store"
	certreq -accept $ReturnedCert
	Write-Output "Now Processing Cert and Obtaining Thumbprint to install into Tools"
	#Filter out the cert and get thumbprint
	$cert = Get-ChildItem Cert:\LocalMachine\My | Where {$_.Subject -like "CN=$Computer, O=Rockstar North, C=UK"}
	$Thumbprint = $cert.thumbprint
	Write-Output "Found the Thumbprint: $thumbprint"
	# Importing the cert with netsh: netsh http add sslcert ipport=0.0.0.0:8081 certhash=$thumbprint appid={63f4a5a4-5d7e-450d-8d23-1995bec283ac}
	CMD.EXE /C "netsh http add sslcert ipport=0.0.0.0:8081 certhash=$thumbprint appid={63f4a5a4-5d7e-450d-8d23-1995bec283ac}"
	Write-Output "Cert installed into the Tools - All Done"
	#Break out after
	exit
}
# end function Install

if ($install){
	Install
}

# Check to ensure the running user is an admin
$isadmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
if (!$isadmin) {
	Write-Output "The current running user ($runninguser) is not part of the local administrator group on this machine"
	Write-Output "As such would be unable to request the certificate - Please add to local admin group and run the script again)"
	exit}

# If the above have passed we are ready to go.....
# Set the registry values in preparation to store the request number
if (!(Get-ItemProperty HKLM:\Software\RockstarNorth\SSL -Name ReqNo -EA SilentlyContinue)){
	# this is the first time the script has run - let's create the registry key and value for future runs
	New-Item -path HKLM:\Software\RockstarNorth -EA SilentlyContinue | Out-Null
	New-Item -path HKLM:\Software\RockstarNorth\SSL | Out-Null
	New-ItemProperty -path HKLM:\Software\RockstarNorth\SSL -Name "ReqNo" 
	write-host "Initial configuration completed." -ForegroundColor green
}

# Test to see if there is an existing registry value which would hint that the request is made and may need to be run with -install to install the cert
$CheckRegistry = (get-ItemProperty HKLM:\Software\RockstarNorth\SSL).ReqNo
if ($CheckRegistry) {
Write-host "There appears to be a request already with Certificate Request Number $CheckRegistry" -foregroundcolor "red"
Write-host "If this has been issued you will want to run this script again with the -install switch to import the certificate" -foregroundcolor "red"
Write-host "If this is an error please delete the value ReqNo from the registry at HKLM:\Software\RockstarNorth\SSL and run the Script again" -foregroundcolor "green"
# Quit since we don't want to proceed
exit}

# ***** Create Server Certificate Request File *****
Write-Verbose "Create Server Certificate Request File (CertReq.inf) for $Computer `r"

$CRRequestINF =
@"
;----------------- request.inf ----------------- `r
 `r
[Version] `r
 `r
Signature="$Windows NT$ `r
 `r
[NewRequest] `r
 `r
Subject="C=UK, O=Rockstar North, CN=$Computer" `r
Exportable = TRUE `r
KeyLength = 2048 `r
KeySpec = 1 `r
KeyUsage = 0xf0 `r
MachineKeySet = TRUE `r
ProviderName = "Microsoft RSA SChannel Cryptographic Provider" `r
[RequestAttributes] `r
CertificateTemplate="WebServer-T2" `r
[EnhancedKeyUsageExtension] `r
OID = 1.3.6.1.5.5.7.3.1 `r
OID = 1.3.6.1.5.5.7.3.2 `r
[Extensions] `r
;Any extra extensions here `r
 `r
;----------------------------------------------- `r
"@

write-output "Generating Certificate Request file... `r "
$CRRequestINF | out-file -filepath $CRRequestINFFileName -force

# ***** Use INF to create request *****
certreq -new $CRRequestINFFileName $CRequestFileName
write-output "Certificate Request File Created."

# Submitting Certificate to CA for Signing
$ErrorActionPreference = "Stop"
write-output "Submitting the Request to the Certificate Authority."
#certreq -submit -config "$CA" $CRequestFileName
$casubmit = certreq -submit -config "$CA" $CRequestFileName 2>&1 -ErrorAction Stop
$reqno = ($casubmit | Select-Object -first 1).split()[-1]
# update registry with the request number 
Write-output "Storing the request number in the registry"
Set-ItemProperty HKLM:\Software\RockstarNorth\SSL -Name "ReqNo" -Value $reqno
Write-Output "Submitted to CA with Request Number $reqno - Please wait for Certificate to be approved then rerun this script with the -install option"

