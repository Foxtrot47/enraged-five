@ECHO OFF
CALL setenv.bat >NUL
SET BOOTSTRAP_NAME=MAP_AUTO_EXPORT_CLIENT
SET BOOTSTRAP_BAT=%RS_TOOLSROOT%/script/util/bootstrap.bat
SET BOOTSTRAP_SCRIPT=%RS_TOOLSROOT%/ironlib/util/bootstrap.rb 
SET AAUTOMATION_PROCESS=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ClientWorker.exe
SET ARGS=-role mapexport -automation net.tcp://LOCALHOST:7010/automation.svc -filetransfer net.tcp://LOCALHOST:7010/FileTransfer.svc --nopopups

CALL %BOOTSTRAP_BAT% %BOOTSTRAP_NAME% %BOOTSTRAP_SCRIPT% %AUTOMATION_PROCESS% %ARGS%