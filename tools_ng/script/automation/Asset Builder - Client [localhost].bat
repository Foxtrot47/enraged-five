@ECHO OFF
CALL setenv.bat >NUL
SET BOOTSTRAP_NAME=ASSETBUILDER_CLIENT
SET BOOTSTRAP_BAT=%RS_TOOLSROOT%/script/util/bootstrap.bat%
SET BOOTSTRAP_SCRIPT=%RS_TOOLSROOT%/ironlib/util/bootstrap.rb 
SET AUTOMATION_PROCESS=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ClientWorker.exe
SET ARGS=--role assetbuilder --automation net.tcp://localhost:7010/automation.svc --filetransfer net.tcp://localhost:7010/filetransfer.svc --nopopups

CALL %BOOTSTRAP_BAT% %BOOTSTRAP_NAME% %BOOTSTRAP_SCRIPT% %AUTOMATION_PROCESS% %ARGS%