@ECHO OFF
CALL setenv.bat >NUL
SET BUILD=%1
SET AUTOMATED=%2

TITLE Automation %BUILD% Asset Rebuild
SET AUTOMATION_SERVERHOST=rsgediabld5
REM SET AUTOMATION_SERVERHOST=localhost
SET AUTOMATION_SCRIPT=%CD%\%BUILD%.console

SET ARGS=--host %AUTOMATION_SERVERHOST% --script %AUTOMATION_SCRIPT%

ECHO ****************************************************************************
ECHO *
ECHO * Asset Rebuild: %BUILD%
ECHO *
ECHO * This is an expensive operation; only run this script if you know what
ECHO * you are doing.  
ECHO * 
ECHO * Please abort this by closing this window if you ran it accidentally.
ECHO *
ECHO ****************************************************************************

IF NOT "%AUTOMATED%"=="AUTOMATED" (
ECHO.
call :PrintWhiteOnRed REGARDLESS OF WHICH BRANCH YOU ARE ON ENSURE THIS IS THE SERVER YOU WISH TO INSTRUCT TO ISSUE REBUILDS
ECHO.
call :PrintBlackOnYellow   "----       %AUTOMATION_SERVERHOST%      ----"
ECHO.
call :PrintWhiteOnRed Check this server! ^^^^ Is it the assetbuilder server for %RS_TOOLSROOT%? 
call :PrintWhiteOnRed CTRL-C NOW if you are in doubt.
ECHO.
PAUSE
)

PUSHD %RS_TOOLSIRONLIB%\lib
RSG.Pipeline.Automation.Console.exe %ARGS%
POPD

IF NOT "%AUTOMATED%"=="AUTOMATED" PAUSE
goto :eof

:PrintBlackOnYellow
powershell -Command Write-Host "%*" -foreground "black" -background "yellow"
goto:eof

:PrintWhiteOnRed
powershell -Command Write-Host "%*" -foreground "white" -background "red"
goto:eof