@ECHO OFF
CALL setenv.bat >NUL
SET DELAY=60
SET AUTOMATED=%1

TITLE Automation Full Asset Rebuild

ECHO ****************************************************************************
ECHO *
ECHO * Asset Rebuild: ALL 2 %AUTOMATED%
ECHO *
ECHO * This is a very expensive operation; only run this script if you know what
ECHO * you are doing.  
ECHO * 
ECHO * Please abort this by closing this window if you ran it accidentally.
ECHO * 
ECHO ****************************************************************************

IF "%AUTOMATED%"=="AUTOMATED" ( 
	ECHO * In %DELAY% seconds it will begin.
	ping -n %DELAY% 127.0.0.1 > nul	
) ELSE (
	PAUSE 
)
 
IF "%AUTOMATED%"=="AUTOMATED" (
	ECHO *** 
	ECHO *** NO VERIFICATION WILL TAKE PLACE AS THIS IS AUTOMATED ***
	ECHO *** 
	@CALL automation_rebuild.bat automation_rebuild_all_unsafe %AUTOMATED%
) ELSE (
	ECHO *** 
	ECHO *** VERIFICATION WILL TAKE PLACE AS THIS IS NOT AUTOMATED ***
	ECHO *** 
	@CALL automation_rebuild.bat automation_rebuild_all %AUTOMATED%
)
  
ECHO ****************************************************************************
ECHO * Asset Rebuild: ALL has Completed.
ECHO ****************************************************************************

IF "%AUTOMATED%"=="AUTOMATED" ( 
	ECHO * In %DELAY% seconds it will end.
	ping -n %DELAY% 127.0.0.1 > nul	
) ELSE (
	PAUSE 
)

exit