__author__ = 'mharrison-ball'

from ctypes import *
from pyfbsdk import *
from pyfbsdk_additions import *

cdll.rexMBRage.CutsceneExporterInitialize_Py()
# Get our scene coordinates
cdll.rexMBRage.GetCutsceneOffsetsX_Py.restype = c_float
cdll.rexMBRage.GetCutsceneOffsetsY_Py.restype = c_float
cdll.rexMBRage.GetCutsceneOffsetsZ_Py.restype = c_float
cdll.rexMBRage.GetCutsceneOrientation_Py.restype = c_float
x = cdll.rexMBRage.GetCutsceneOffsetsX_Py()
y = cdll.rexMBRage.GetCutsceneOffsetsY_Py()
z = cdll.rexMBRage.GetCutsceneOffsetsZ_Py()
r = cdll.rexMBRage.GetCutsceneOrientation_Py()

cdll.rexMBRage.ClearCutsceneShots_Py()
cdll.rexMBRage.AddCutsceneShot_Py("Shot_1")
# Set our scene position
cdll.rexMBRage.SetCutsceneOffsets_Py(c_float(x), c_float(y), c_float(z), c_float(r))

end_range = int(FBSystem().CurrentTake.LocalTimeSpan.GetStop().GetTimeString().replace('*', ''))

cdll.rexMBRage.SetCutsceneShotStartRange_Py(0, 0)
cdll.rexMBRage.SetCutsceneShotEndRange_Py(0, end_range)

cdll.rexMBRage.SetCutsceneShotEnableDOFFirstCut_Py( 0, False )
cdll.rexMBRage.SetCutsceneShotEnableDOFSecondCut_Py( 0, False)
cdll.rexMBRage.SetCutsceneShotDisableDOF_Py( 0, True )
cdll.rexMBRage.SetCutsceneStoryMode_Py( False )

cdll.rexMBRage.SetCutsceneShotCatchupCamera_Py( 0, False )
cdll.rexMBRage.SetCutsceneShotFadeInCutscene_Py( 0, False, c_float(0) )
cdll.rexMBRage.SetCutsceneShotFadeOutCutscene_Py( 0, False, c_float(0) )
cdll.rexMBRage.SetCutsceneShotBlendOut_Py( 0, False, 0, 0 )
cdll.rexMBRage.SetCutsceneShotSectioning_Py( 0, 1 )
cdll.rexMBRage.SetCutsceneShotSectioningDuration_Py( 0, 4 )

cdll.rexMBRage.SetCutsceneConcatMode_Py( False )

cdll.rexMBRage.ExportActiveCutscene_Py( False, True )




