@ECHO OFF
CALL setenv.bat >NUL
SET BOOTSTRAP_NAME=SCRIPTBUILDER_CLIENT
SET BOOTSTRAP_BAT=%RS_TOOLSROOT%/script/util/bootstrap.bat%
SET BOOTSTRAP_SCRIPT=%RS_TOOLSROOT%/ironlib/util/bootstrap.rb 
SET AUTOMATION_PROCESS=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ClientWorker.exe
SET ARGS=--role scriptbuilder --automation net.tcp://ediw-cc-9:7010/automation.svc --filetransfer net.tcp://sanb-vm21:7010/filetransfer.svc --nopopups

CALL %BOOTSTRAP_BAT% %BOOTSTRAP_NAME% %BOOTSTRAP_SCRIPT% %AUTOMATION_PROCESS% %ARGS%