@ECHO OFF
CALL setenv.bat >NUL
SET BOOTSTRAP_NAME="CODE_ANALYSER_SERVER"
SET BOOTSTRAP_BAT=%RS_TOOLSROOT%/script/util/bootstrap.bat
SET BOOTSTRAP_SCRIPT=%RS_TOOLSROOT%/ironlib/util/bootstrap.rb 
SET AUTOMATION_PROCESS=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ServerHost.exe
SET ARGS=-role "framecapture,cutscenescripter,cutscenerpfbuilder"

CALL %BOOTSTRAP_BAT% %BOOTSTRAP_NAME% %BOOTSTRAP_SCRIPT% %AUTOMATION_PROCESS% %ARGS%