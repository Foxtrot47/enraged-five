@ECHO OFF
CALL setenv.bat >NUL
SET AUTOMATION_SERVER=%RS_TOOLSROOT%/ironlib/lib/RSG.Pipeline.Automation.ServerHost.exe
SET ARGS=--role test

CALL %RS_TOOLSROOT%/ironlib/prompt.bat %AUTOMATION_SERVER% %ARGS%

PAUSE