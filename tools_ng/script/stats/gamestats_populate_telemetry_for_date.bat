@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_generate_processed.bat
REM Description:: Wrapper around the Generate Processed Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

set /P STARTDATE="Enter start date you wish to populate (e.g. 2013-01-01 00:00:00Z): "
set /P ENDDATE="Enter start date you wish to populate (e.g. 2013-01-01 00:00:00Z): "
set /P PLATFORM="Enter the platform (e.g. ps3 or leave blank for all): "
set /P SYNCASSETS="Sync assets? (e.g. y/n): "

CALL setenv.bat

:: Construct the arguments to pass to the app
SET ARGS=--start %STARTDATE% --end %ENDDATE%

if NOT "%PLATFORM%"=="" (
	set ARGS=%ARGS% --platform %PLATFORM%
)
if NOT "%SYNCASSETS%"=="y" (
	set ARGS=%ARGS% --nosync
)

echo Running following command:
echo %RS_TOOLSBIN%\Statistics\RSG.Statistics.PopulateTelemetryStats.exe %ARGS%

%RS_TOOLSBIN%\Statistics\RSG.Statistics.PopulateTelemetryStats.exe %ARGS%

echo Population complete. Press any key to exit...
pause