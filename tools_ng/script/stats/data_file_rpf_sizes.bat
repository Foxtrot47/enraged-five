@ECHO OFF
REM
REM File:: %RS_TOOLSROOT%/script/stats/data_file_rpf_sizes.bat
REM Description:: Wrapper around the data_file_rpf_sizes Ruby Script
REM 
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 21 June 2013
REM

SET OUTPUT_FILENAME=x:\rpf_sizes.csv
CALL setenv.bat >NUL

%RS_TOOLSIRONLIB%\prompt.bat %RS_TOOLSIR% %RS_TOOLSIRONLIB%/util/stats/data_file_rpf_sizes.rb --output %OUTPUT_FILENAME% %RS_BUILDBRANCH%\ps3\levels\gta5

NOTEPAD %OUTPUT_FILENAME%
