@ECHO OFF
CALL setenv.bat >NUL
SET AUTOMATED=%1

TITLE Statistics Sync Assets
SET AUTOMATION_SCRIPT=%CD%\generate_weekly_preset.console

SET ARGS=--script %AUTOMATION_SCRIPT%

PUSHD %RS_TOOLSBIN%\Statistics
RSG.Statistics.Console.exe %ARGS%
POPD

IF NOT "%AUTOMATED%"=="AUTOMATED" PAUSE
