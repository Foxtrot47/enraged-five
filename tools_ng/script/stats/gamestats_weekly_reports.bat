@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_weekly_reports.bat
REM Description:: Runs list of reports that should run weekly.
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

%RS_TOOLSBIN%\ReportGenerator\ReportGenerator.exe --level %RS_PROJECT% --reports WeeklyExportStatsReport --nopopups --outputdir %RS_ASSETS%\reports\exports --p4edit --p4submit --cldesc "Weekly map export stats" --skiplevelload