@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_generate_reports.bat
REM Description:: Wrapper around the Generate Processed Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

%RS_TOOLSBIN%\ReportGenerator\generate_all_reports.bat %RS_PROJECT% %RS_ASSETS%\reports nopause