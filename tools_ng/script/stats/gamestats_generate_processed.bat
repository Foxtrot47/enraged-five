@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_generate_processed.bat
REM Description:: Wrapper around the Generate Processed Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

echo Generating Automated Stats from Yesterday (MapOnly)
%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe --automatedonly --automated 1 --resolution 100

echo Generating Automated Stats from Yesterday (Everything)
%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe --automatedonly --automated 2 --resolution 100

echo Generating Memory Shortfall Stats from Yesterday
%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe --memshortfall --resolution 100

echo Generating User Processed Stats
%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe --buildcount 5
