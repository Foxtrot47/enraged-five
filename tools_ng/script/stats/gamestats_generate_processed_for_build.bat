@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_generate_processed.bat
REM Description:: Wrapper around the Generate Processed Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

set /P USERBUILD="Enter the build (e.g. 236.2): "
set /P PLATFORM="Enter the platform (e.g. PS3 or leave blank for all): "
set /P LEVEL="Enter the level (e.g. gta5 or leave blank for all): "
set /P GAMETYPE="Enter the game type (e.g. singleplayer or leave blank for all): "
set /P RESOLUTION="Enter the resolution (e.g. 100 or leave blank for all): "
set /P BUILDCONFIG="Enter the build config (e.g. BankRelease or leave blank for all): "
set /P AUTOMATED="Run for automated tests? (blank for non automated, 1 for map only, 2 for everything): "
set /P DRAWLISTS="Generate drawlist results? (y/n or x for only drawlists): "

CALL setenv.bat

:: Construct the arguments to pass to the app
SET ARGS=--build %USERBUILD%

if NOT "%PLATFORM%"=="" (
	set ARGS=%ARGS% --platform %PLATFORM%
)
if NOT "%LEVEL%"=="" (
	set ARGS=%ARGS% --level %LEVEL%
)
if NOT "%GAMETYPE%"=="" (
	set ARGS=%ARGS% --gametype %GAMETYPE%
)
if NOT "%RESOLUTION%"=="" (
	set ARGS=%ARGS% --resolution %RESOLUTION%
)
if NOT "%BUILDCONFIG%"=="" (
	set ARGS=%ARGS% --buildconfig %BUILDCONFIG%
)
if NOT "%AUTOMATED%"=="" (
	set ARGS=%ARGS% --automated %AUTOMATED%
)
if "%DRAWLISTS%"=="n" (
	set ARGS=%ARGS% --drawlists x
) else if "%DRAWLISTS%"=="x" (
	set ARGS=%ARGS% --drawlists +
)

echo Running following command:
echo %RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe %ARGS%

%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateProcessedStats.exe %ARGS%

echo Generation complete. Press any key to exit...
pause
