@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_populate_assetstats.bat
REM Description:: Wrapper around the Generate Asset Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

p4 sync //depot/gta5/assets/cuts/....cutxml
p4 sync //depot/gta5/assets/cuts/!!Cutlists/...

%RS_TOOLSBIN%\Statistics\RSG.Statistics.GenerateAssetStats.exe
