@ECHO OFF
CALL setenv.bat >NUL
SET AUTOMATED=%1

TITLE Statistics Sync Assets
SET AUTOMATION_SCRIPT=%CD%\sync_assets.console

SET ARGS=--script %AUTOMATION_SCRIPT%

:: Sync the required assets
p4 sync //depot/gta5/assets/characters/....xml
p4 sync //depot/gta5/assets/export/anim/...
p4 sync //depot/gta5/assets/reports/...
p4 sync //depot/gta5/assets/export/....xml
p4 sync //depot/gta5/assets/vehicles/....xml
p4 sync //depot/gta5/assets/weapons/....xml
p4 sync //depot/gta5/build/dev/common/data/....xml
p4 sync //depot/gta5/build/dev/common/data/....meta

:: Animation files
p4 sync //depot/gta5/assets/export/....icd.zip
p4 sync //depot/gta5/assets/export/....zip
p4 sync //depot/gta5/assets/cuts/....cutxml
p4 sync //depot/gta5/assets/cuts/!!Cutlists/...

:: Game text
p4 sync //depot/gta5/assets/GameText/American/...

:: Radio stations
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/Audio/GameObjects/Radio/Stations.xml

:: DLC
p4 sync //gta5_dlc/.../assets/characters/....xml
p4 sync //gta5_dlc/.../assets/vehicles/....xml
p4 sync //gta5_dlc/.../assets/weapons/....xml


PUSHD %RS_TOOLSBIN%\Statistics
RSG.Statistics.Console.exe %ARGS%
POPD

IF NOT "%AUTOMATED%"=="AUTOMATED" PAUSE
