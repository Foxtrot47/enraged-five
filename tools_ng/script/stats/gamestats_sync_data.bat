@ECHO OFF

if "%1"=="nopause" (
	set SKIP_PAUSE=1
)

cd /d %~dp0
call %RS_TOOLSROOT%/script/util/data_get_project_info.bat

TITLE %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...
ECHO %RS_PROJECT% dev: Getting %PERFORCE_CURRENT_LABEL_NAME% labelled build...


for /f "tokens=*" %%a in ( 
'xmlstarlet sel -t -v "/local/studio/@name" %RS_TOOLSROOT%/local.xml' 
) do ( 
set studio=%%a
) 
echo/%%studio%%=%studio% 


if %studio%==north set networkbuildpath="N:\RSGEDI\Build Data\Latest\gta5\build\dev"
if %studio%==toronto set networkbuildpath="N:\RSGTOR\Build_Data\Latest\gta5\build\dev"
if %studio%==sandiego set networkbuildpath="N:\RSGSAN\Build_Data\Latest\gta5\build\dev"
if %studio%==newengland set networkbuildpath="N:\RSGNWE\Build_Data\Latest\gta5\build\dev"
if %studio%==leeds set networkbuildpath="N:\RSGLDS\Build_Data\Latest\gta5\build\dev"
if %studio%==london set networkbuildpath="N:\RSGLDN\Build_Data\Latest\gta5\build\dev"
if %studio%==newyork set networkbuildpath="N:\RSGNYC\Build_Data\Latest\gta5\build\dev"

echo NETWORK BUILD PATH SET: %networkbuildpath%

set localbuildpath=X:\gta5\build\dev

if not exist %RS_TOOLSROOT%/logs/ (
	mkdir %RS_TOOLSROOT%/logs/
)


:: - Sync local network stored platform data -
:: **** E.G. robocopy SOURCE\ DESTINATION\ files     ******
:: ROBOCOPY info. /S copies non-empty subfolders, /DCOPY:T keeps source timestamps
robocopy /S /DCOPY:T %networkbuildpath%\pc\ %localbuildpath%\pc\ *.* 2> %RS_TOOLSROOT%/logs/build_DATA_sync.txt
robocopy /S /DCOPY:T %networkbuildpath%\ps3\ %localbuildpath%\ps3\ *.* 2>> %RS_TOOLSROOT%/logs/build_DATA_sync.txt
robocopy /S /DCOPY:T %networkbuildpath%\xbox360\ %localbuildpath%\xbox360\ *.* 2>> %RS_TOOLSROOT%/logs/build_DATA_sync.txt

p4 sync %PERFORCE_ROOT%/xlast/...@%PERFORCE_CURRENT_LABEL_NAME% 2> %RS_TOOLSROOT%/logs/build_sync.txt
IF %ERRORLEVEL% EQU 1 (
	set syncerror=1
)
p4 sync %PERFORCE_ROOT%/build/dev/common/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/build/dev/x64/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/build/dev/TROPDIR/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/build/dev/*.*@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/script/dev/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/export/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/processed/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/characters/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/cuts/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/reports/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/vehicles/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/weapons/...@%PERFORCE_CURRENT_LABEL_NAME%
p4 sync %PERFORCE_ROOT%/assets/maps/PropSwap.txt@%PERFORCE_CURRENT_LABEL_NAME%
