@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_populate_profilestats.bat
REM Description:: Wrapper around the Populate Profile Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

%RS_TOOLSBIN%\Statistics\RSG.Statistics.PopulateProfileStats.exe
