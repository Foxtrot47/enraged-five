@echo off
REM
REM File:: %RS_TOOLSROOT%/script/stats/gamestats_populate_telemetry.bat
REM Description:: Wrapper around the Populate Telemetry Stats client app
REM 
REM Author:: Michael T�schler <michael.taschler@rockstarnorth.com>
REM Date:: 1st June 2012
REM

CALL setenv.bat

:: Sync the required assets
p4 sync //depot/gta5/assets/characters/....xml
p4 sync //depot/gta5/assets/export/anim/...
p4 sync //depot/gta5/assets/reports/...
p4 sync //depot/gta5/assets/export/....xml
p4 sync //depot/gta5/assets/vehicles/....xml
p4 sync //depot/gta5/assets/weapons/....xml
p4 sync //depot/gta5/build/dev/common/data/....xml

:: Animation files
p4 sync //depot/gta5/assets/export/....icd.zip
p4 sync //depot/gta5/assets/export/....zip
p4 sync //depot/gta5/assets/cuts/....cutxml
p4 sync //depot/gta5/assets/cuts/!!Cutlists/...

:: Game text
p4 sync //depot/gta5/assets/GameText/American/...

:: Radio stations
p4 sync //depot/gta5/audio/dev/assets/Objects/Core/Audio/GameObjects/Radio/Stations.xml

:: Check whether an hours argument was supplied
SET ARGS=
if NOT "%1"=="" (
	SET ARGS=%ARGS% --hours %1
)

echo Running following command:
echo %RS_TOOLSBIN%\Statistics\RSG.Statistics.PopulateTelemetryStats.exe %ARGS%

%RS_TOOLSBIN%\Statistics\RSG.Statistics.PopulateTelemetryStats.exe %ARGS%
