@ECHO OFF
REM
REM File:: data_build_componentpeds.bat
REM Description::
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 15 April 2013
REM

CALL setenv.bat >NUL
TITLE Building NM Test ComponentPeds...

SET PED_ZIP_EXPORT_DIR=%RS_EXPORT%\models\cdimages\componentpeds
SET AUTHORISED_PEDS=(S_M_Y_Cop_01 s_m_y_swat_01 s_f_m_sweatshop_01 s_m_m_chemsec_01 S_m_m_dockwork_01 A_C_Rottweiler slod_human slod_large_quadped slod_small_quadped)
SET AUTHORISED_TXDS=(comp_peds_generic)
SET NM_PED_EXPORT_DIR=%RS_EXPORT%\models\cdimages\componentpeds_nmtest

IF NOT EXIST %NM_PED_EXPORT_DIR%\NUL MKDIR %NM_PED_EXPORT_DIR%

FOR %%I IN %AUTHORISED_PEDS% DO (

  ECHO Processing Ped: %PED_ZIP_EXPORT_DIR%\%%I.ped.zip
  P4 sync %PED_ZIP_EXPORT_DIR%\%%I.ped.zip
  COPY /Y %PED_ZIP_EXPORT_DIR%\%%I.ped.zip %NM_PED_EXPORT_DIR%\%%I.ped.zip
)

FOR %%I IN %AUTHORISED_TXDS% DO (

  ECHO Processing Texture Dictionary: %PED_ZIP_EXPORT_DIR%\%%I.itd.zip
  P4 sync %PED_ZIP_EXPORT_DIR%\%%I.itd.zip
  COPY /Y %PED_ZIP_EXPORT_DIR%\%%I.itd.zip %NM_PED_EXPORT_DIR%\%%I.itd.zip
)

%RS_TOOLSCONVERT% %NM_PED_EXPORT_DIR%

PAUSE
REM Done.
