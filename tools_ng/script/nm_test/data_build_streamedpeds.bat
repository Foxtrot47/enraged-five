@ECHO OFF
REM
REM File:: data_build_streamedpeds.bat
REM Description::
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 17 May 2013
REM

CALL setenv.bat >NUL
TITLE Building NM Test StreamedPeds...

SET PED_ZIP_EXPORT_DIR=%RS_EXPORT%\models\cdimages\streamedpeds
SET AUTHORISED_PEDS=(player_zero)
SET AUTHORISED_TXDS=()
SET NM_PED_EXPORT_DIR=%RS_EXPORT%\models\cdimages\streamedpeds_nmtest

ECHO %RS_TOOLSIR%
ECHO %RS_TOOLSCONVERT% %NM_PED_EXPORT_DIR%
%RS_TOOLSCONVERT% %NM_PED_EXPORT_DIR%

PAUSE
REM Done.
