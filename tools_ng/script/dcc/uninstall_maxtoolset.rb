#
# File:: /tools/script/dcc/uninstall_maxtoolset.rb
# Description:: Uninstall all tools from max to make it easier to find problems
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 1 December 2009
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'

config = Pipeline::Config::instance()
config.copy_core_max_plugins(true,true)