# script to generate an outsource package
# Greg Smith
# Luke Openshaw

require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'pipeline/util/autodesk3dsmax'
require 'util/max/maxscript'
require 'find'
require 'fileutils'

module Pipeline

	#-----------------------------------------------------------------------------
	# Constants
	#-----------------------------------------------------------------------------
	OPTIONS = [
		[ '--help', '-h', OS::Getopt::BOOLEAN, 'display usage information' ]
	]
	
	#---------------------------------------------------------------------
	# Parse Command Line
	#---------------------------------------------------------------------
	g_Opts, g_Trailing = OS::Getopt::getopts( OPTIONS )

	if ( g_Opts['help'] ) then
		puts OS::Getopt::usage( OPTIONS )
		exit( 1 )
	end
	
	max_version = "14.0"
	max_version_str = "max2012"

	#dcc...	
	dcc_dir = "#{Globals::instance.toolsroot}/dcc/outsource_raw/#{max_version_str}"
	target_folder = "#{Globals::instance.toolsroot}/dcc/outsource/#{max_version_str}"

	#FileUtils.rm_r(target_folder, :force => true)
	
	#game files...
	p = Config::instance().project
	p.load_config
	
	#replace all filein and include calls to have .mse instead of .mse extensions
	mxsfiles = OS::FindEx.find_files_recurse( dcc_dir + "/scripts/*.ms" )
	mxsfiles = mxsfiles + OS::FindEx.find_files_recurse( dcc_dir + "/UI/macroscripts/*.mcr" )
	
	mxsfiles.each do | mxsfilepath |

		ms = Maxscript::File.new( mxsfilepath )
		ms.load()
		ms.modify_include_strings( ".ms", ".mse" )

		outfile = mxsfilepath.sub!( dcc_dir, "#{target_folder}" )
		puts(outfile)
		OS::FileUtilsEx.make_sure_path_exists( outfile )
		ms.save( outfile )
	end
	
	#encrypt max files
	max_install_dirs  = Autodesk3dsmax::instance( ).installdirs( )
	latest_version = Autodesk3dsmax::instance( ).latest_version

	max_install_dir = nil
	if ( ( :x64 == OS::WinSystemInfo::SysType( ) ) and max_install_dirs.has_key?( :x64 ) ) then
		max_install_dir = max_install_dirs[:x64][max_version]
	elsif ( installdirs.has_key?( :x86 ) ) then
		max_install_dir = max_install_dirs[:x86][max_version]
	end

	os_execute_str = "#{OS::Path::combine( max_install_dir, '3dsmax' )} -mxs \"global RsOutSourceDir=\"#{target_folder}/\"\" -U MAXScript #{dcc_dir}/scripts/encrypt_toolset.ms"
	puts os_execute_str

	system( os_execute_str )
	
end #module Pipeline