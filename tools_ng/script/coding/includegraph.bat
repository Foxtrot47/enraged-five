
setlocal

set TEMPFILE=%TMP%\includegraph_out.dot
set OUTFILE=%2
if "%OUTFILE%" == "" (
	set %OUTFILE%=%~dpn1.pdf
)

%~dp0\cinclude2dot.py -I X:\gta5\src\dev\rage\base\src -I X:\gta5\src\dev\rage\suite\src -I X:\gta5\src\dev\rage\script\src -I X:\gta5\src\dev\rage\naturalmotion\src -I X:\gta5\src\dev\rage\framework\src -I X:\gta5\src\dev\rage\scaleform\src -I X:\gta5\src\dev\rage\scaleform\Include -I "%VS80COMNTOOLS%\..\..\vc\platformsdk\include"  -I "%VS80COMNTOOLS%\..\..\vc\include" -I X:\gta5\src\dev\game --nodeScore=1 --edgeScore=0 --branchMult=1 --branchPower=1.5 %1 %TEMPFILE%

set PFX86=%ProgramFiles(x86)%
if "%PFX86%" == "" ( 
	set PFX86=%ProgramFiles% 
)

dot.exe -v -Tpdf -o%OUTFILE% %TEMPFILE%

start %2

endlocal
cp C: