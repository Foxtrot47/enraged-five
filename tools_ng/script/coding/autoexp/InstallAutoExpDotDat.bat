@ECHO OFF
SETLOCAL
cd /d %~dp0

SET EEADDIN="x:\gta5\tools_ng\bin\eeaddin.dll"
if NOT EXIST %EEADDIN% (
	ECHO Couldn't find %EEADDIN%, it has to be at that exact path.
	ECHO Make sure it exists and try again
	EXIT /b 0
)

SETLOCAL ENABLEEXTENSIONS

SET HasSDKs=Has SDKs:

SET HASVS05=0
SET DESTPC8=""
SET BACKUPPC8=""
if DEFINED VS80COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2005
	SET HASVS05=1
	SET DESTPC8="%VS80COMNTOOLS%..\Packages\Debugger\autoexp.dat"
	SET BACKUPPC8="%VS80COMNTOOLS%..\Packages\Debugger\autoexp.bak"
)

set HASVS08=0
SET DESTPC9=""
SET BACKUPPC9=""
if DEFINED VS90COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2008
	SET HASVS08=1
	SET DESTPC9="%VS90COMNTOOLS%..\Packages\Debugger\autoexp.dat"
	SET BACKUPPC9="%VS90COMNTOOLS%..\Packages\Debugger\autoexp.bak"
)

set HASVS10=0
SET DESTPC10=""
SET BACKUPPC10=""
if DEFINED VS100COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2010
	SET HASVS10=1
	SET DESTPC10="%VS100COMNTOOLS%..\Packages\Debugger\autoexp.dat"
	SET BACKUPPC10="%VS100COMNTOOLS%..\Packages\Debugger\autoexp.bak"
)

SET HASVS12=0
SET DESTVS2012=""
if DEFINED VS110COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2012
	SET HASVS12=1
	SET DESTVS2012="%USERPROFILE%\Documents\Visual Studio 2012\Visualizers\rage.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2012\Visualizers\RageCore.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2012\Visualizers\RageFramework.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2012\Visualizers\game.natvis"	
)

SET HASVS13=0
SET DESTVS2013=""
if DEFINED VS120COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2013
	SET HASVS13=1
	SET DESTVS2013="%USERPROFILE%\Documents\Visual Studio 2013\Visualizers\rage.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2013\Visualizers\RageCore.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2013\Visualizers\RageFramework.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2013\Visualizers\game.natvis"		
)

SET HASVS15=0
SET DESTVS2015=""
if DEFINED VS140COMNTOOLS (
	SET HasSDKs=%HasSDKS% VS2015
	SET HASVS15=1
	SET DESTVS2015="%USERPROFILE%\Documents\Visual Studio 2015\Visualizers\rage.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2015\Visualizers\RageCore.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2015\Visualizers\RageFramework.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2015\Visualizers\game.natvis"	
)

REM Cannot rely on VS150COMNTOOLS or higher as they are no longer set by default on installation. Check for target folder instead.

SET HASVS17=0
SET DESTVS2017=""
if EXIST "%USERPROFILE%\Documents\Visual Studio 2017\" (
	SET HasSDKs=%HasSDKS% VS2017
	SET HASVS17=1
	SET DESTVS2017="%USERPROFILE%\Documents\Visual Studio 2017\Visualizers\rage.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2017\Visualizers\RageCore.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2017\Visualizers\RageFramework.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2017\Visualizers\game.natvis"		
)

SET HASVS19=0
SET DESTVS2019=""
if EXIST "%USERPROFILE%\Documents\Visual Studio 2019\" (
	SET HasSDKs=%HasSDKS% VS2019
	SET HASVS19=1
	SET DESTVS2019="%USERPROFILE%\Documents\Visual Studio 2019\Visualizers\rage.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2019\Visualizers\RageCore.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2019\Visualizers\RageFramework.natvis"
	del "%USERPROFILE%\Documents\Visual Studio 2019\Visualizers\game.natvis"		
)

SET HASXENON=0
SET DESTXENON=""
SET BACKUPXENON=""
if DEFINED XEDK (
	SET HasSDKs=%HasSDKS% Xenon
	SET HASXENON=1
	SET DESTXENON="%XEDK%\bin\win32\autoexp.dat"
	SET BACKUPXENON="%XEDK%\bin\win32\autoexp.bak"
)

SET HASDURANGO=0
if DEFINED DurangoXDK (
	SET HASDURANGO=1
	SET HasSDKs=%HasSDKS% XboxOne
)

SET HASPS3=0
SET DESTPS3=""
SET DESTPS3_AUTOEXEC=""
if DEFINED SN_PS3_PATH (
	SET HasSDKs=%HasSDKS% PS3
	SET HASPS3=1
	SET DESTPS3="%SN_PS3_PATH%\bin\autoexp.dat"
	SET DESTPS3_AUTOEXEC="%SN_PS3_PATH%\bin\autoexec.eic"
)

set HASPS4=0
if EXIST "%USERPROFILE%\Documents\SCE\orbis-debugger\" (
	SET HasSDKs=%HasSDKS% PS4
	SET HASPS4=1
	SET DESTPS4="%USERPROFILE%\Documents\SCE\orbis-debugger\autoexp.dat"
)

ECHO --------------------------------------------------------
ECHO %HasSDKs%
ECHO --------------------------------------------------------

if "%1"=="-remove" (
	echo Restoring original autoexp.dat from backup.
	if %HASVS05%==1 (
		copy %BACKUPPC8% %DESTPC8%
		del %BACKUPPC8%
	)
	if %HASVS08%==1 (
		copy %BACKUPPC9% %DESTPC9%
		del %BACKUPPC9%
	)
	if %HASVS10%==1 (
		copy %BACKUPPC10% %DESTPC10%
		del %BACKUPPC10%
	)
	if %HASVS12%==1 (
		del %DESTVS2012%
	)
	if %HASVS13%==1 (
		del %DESTVS2013%
	)
	if %HASVS15%==1 (
		del %DESTVS2015%
	)
	if %HASVS17%==1 (
		del %DESTVS2017%
	)
	if %HASVS19%==1 (
		del %DESTVS2019%
	)
	if %HASXENON%==1 (
		copy %BACKUPXENON% %DESTXENON%
		del %BACKUPXENON%
	)
	if %HASPS3%==1 (
		del %DESTPS3%
		del %DESTPS3_AUTOEXEC%
	)
	if %HASPS4%==1 (
		del %DESTPS4%
	)
	ENDLOCAL
	exit /b 0
)

if %HASVS05%==1 if not exist %BACKUPPC8% (
	echo VS05 Backing up PC autoexp.dat file to autoexp.bak in the same directory.
	copy %DESTPC8% %BACKUPPC8%
)

if %HASVS08%==1 if not exist %BACKUPPC9% (
	echo VS08: Backing up PC autoexp.dat file to autoexp.bak in the same directory.
	copy %DESTPC9% %BACKUPPC9%
)

if %HASVS10%==1 if not exist %BACKUPPC10% (
	echo VS10: Backing up PC autoexp.dat file to autoexp.bak in the same directory.
	copy %DESTPC10% %BACKUPPC10%
)

if %HASXENON%==1 if not exist %BACKUPXENON% (
	echo Xenon: Backing up Xenon autoexp.dat file to autoexp.bak in the same directory.
	copy %DESTXENON% %BACKUPXENON%
)

echo Copying autoexp files to where the debuggers will look for them.
echo To restore the backup, run this script with -remove.
if %HASVS05%==1 (
	call :CopyFile autoexp_pc_vs8.dat %DESTPC8%
)
if %HASVS08%==1 (
	call :CopyFile autoexp_pc_vs9.dat %DESTPC9%
)
if %HASVS10%==1 (
	call :CopyFile autoexp_pc_vs10.dat %DESTPC10%
)

if %HASXENON%==1 (
	call :CopyFile autoexp_xenon.dat %DESTXENON%
)

if %HASPS3%==1 (
	call :CopyFile autoexp_ps3.dat %DESTPS3%
	call :CopyFile autoexec_ps3.eic %DESTPS3_AUTOEXEC%
)

if %HASPS4%==1 (
	call :CopyFile autoexp_ps4.dat %DESTPS4%
)

if %HASVS12%==1 (
	call :CopyFile rage.natvis %DESTVS2012%
)

if %HASVS13%==1 (
	call :CopyFile rage.natvis %DESTVS2013%
)

if %HASVS15%==1 (
	call :CopyFile rage.natvis %DESTVS2015%
)

if %HASVS17%==1 (
	call :CopyFile rage.natvis %DESTVS2017%
)

if %HASVS19%==1 (
	call :CopyFile rage.natvis %DESTVS2019%
)

GOTO :END

:CopyFile
	echo    %~f2
	echo f | xcopy %1 %2 >NUL

:END
ENDLOCAL