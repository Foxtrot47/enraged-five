@echo off
setlocal enabledelayedexpansion

if "%1"=="/unattended" goto doit

if not exist "clean_soft_tree".bat goto NotSameDir
echo *************
echo NOTE: You're calling this script from within its own directory. I'm pretty
echo That this is not what you want. You want to execute it from within a source
echo tree, like x:\MyProject\src\dev. You can go ahead if you want to, but I'm
echo telling you right now that I think you're not doing what you think you're
echo doing. Press CTRL+C to bail.

pause

:NotSameDir

echo This script deletes all intermediate files in the current directory downward.
echo Read-only files won't be touched.
echo DO NOT RUN THIS FROM YOUR ROOT DIRECTORY, SINCE IT DELETS EXE's TOO.

pause

echo No, seriously, I'm not screwing around here.

echo (Run with /unattended switch in the future if you don't want confirmation)

pause

:doit
REM Delete intermediate files
echo Cleaning old SPU executables . . .
rd/s/q c:\spu_debug > NUL
mkdir c:\spu_debug > NUL

echo Cleaning CGC capture directory . . .
if NOT "%SCECGC_CAPTUREDIR%"=="" rd/s/q "%SCECGC_CAPTUREDIR%" > NUL
if NOT "%SCECGC_CAPTUREDIR%"=="" mkdir "%SCECGC_CAPTUREDIR%" > NUL

echo Cleaning intermediate files . . .
del/s *.obj *.obj.debug *.obj.nodebug *.o *.a *_parser.xsd?* *.lib *.pdb *.idb *.ncb *.od *.d *.mak *.ib_pdb_index *.ilk *.map BuildLog.htm *.cmp *.exp *.xdb *.manifest *.manifest.res *.pch *.ib_tag *.i *.s *.dspfx.bat *.job.bat *.cpp.bat *.psc.bat *.fx.bat *.frag.bat *.task.bat *_lib.txt mt.dep *.tlog *.lastbuildstate > NUL 2>&1

if /i not "%1"=="/unattended" (
set /p remempties=Do you wish to remove empty directories, too? [y/n] 
if /i not "%remempties%"=="y" goto:eof
)

echo Removing empty directories . . .
for /d %%d in (*) do (
pushd %%d
for /r %%f in (.) do if /i not "%%f"=="!cd!\." rd %%f > NUL 2>&1
popd
)
