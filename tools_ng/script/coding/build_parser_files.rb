# regenerates all _parser.h files in a source tree

startTime = Time.now
doRebuild = ARGV.include?("--rebuild") or ARGV.include?("-r")
justGame = ARGV.include?("--gameproj")
allProj = ARGV.include?("--allproj")

pargenArgs = ""
if doRebuild
	pargenArgs += " --rebuild"
end

pscFiles = {}

# Scan projects? Or scan directories?
dirScan = !justGame && !allProj

if dirScan
	puts "Scanning for .psc files. This could take a while if you haven't run it recently."
	puts "If it takes too long, re-run the script with --gameproj to only build PSC files that are listed in the game .vcxproj files"
	Dir.glob("**/*.psc") do |x| 
		pscFiles[File.expand_path(x)] = 0;
		if pscFiles.length % 50 == 0 then
			puts "...#{pscFiles.length} found"
		end
	end
else
	vcprojects = []
	if justGame
		code = ENV["RS_CODEBRANCH"]
		vcprojects = [
		"#{code}\\game\\vs_project_lib_psc\\game_lib_psc_2012_unity.vcxproj",
		"#{code}\\rage\\base\\src\\rage_lib_psc\\rage_lib_psc_2012_unity.vcxproj",
		"#{code}\\rage\\framework\\src\\framework_lib_psc\\framework_lib_psc_2012_unity.vcxproj",
		"#{code}\\rage\\suite\\src\\suite_lib_psc\\suite_lib_psc_2012_unity.vcxproj"
		]
	else
		puts "Looking for project files"
		# for each directory
		vcprojects = Dir.glob("**/*_2012.vcxproj")
	end

	puts "Scanned #{vcprojects.length} projects"

	# extract all the "CustomBuild" strings from them
	vcprojects.each do |proj|
		puts proj
		projdir = File.dirname(proj)
		file = File.new(proj)

		file.each_line do |line|
			if line =~ /CustomBuild.*Include=(['"])([^'"]*\.psc)\1/
				fileName = File.expand_path($2, projdir)
				pscFiles[fileName] = 0
			end
		end
	end
end

puts "Found #{pscFiles.length} PSC files"

def newer?(fname1, fname2)
	if !File.exists?(fname1)
		return false
	end
	if !File.exists?(fname2)
		return true
	end
	return File.mtime(fname1) > File.mtime(fname2)
end


oldDir = Dir.getwd()
pscFiles.keys().group_by{|x| File.dirname(x)}.sort().each() do |dir, filesInDir|
	Dir.chdir(dir)
	files = []
	filesInDir.each() do |fileName|
		baseName = File.basename(fileName, ".psc")
		if doRebuild or not newer?(baseName+"_parser.h", baseName+".psc")
			files.push(baseName + ".psc")
		end
	end

	if !files.empty?
		cmdLine = "#{ENV['RS_TOOLSROOT']}\\bin\\coding\\python\\parCodeGen.exe #{pargenArgs} #{files.join(' ')}"
	end
	
	begin
		result = system(cmdLine)
	rescue
	end
end
Dir.chdir(oldDir)

endTime = Time.now

puts "Runtime was #{endTime - startTime}"


