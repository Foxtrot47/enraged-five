@IF "%RS_PROJROOT%"=="" EXIT/B
@IF "%RS_TOOLSROOT%"=="" EXIT/B
@ECHO Will delete %RS_PROJROOT%\cache\convert and %RS_TOOLSROOT%\tmp.
@PAUSE
rd /s/q %RS_PROJROOT%\cache\convert
rd /s/q %RS_TOOLSROOT%\tmp
