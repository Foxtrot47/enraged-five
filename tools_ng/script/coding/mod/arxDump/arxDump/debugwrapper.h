
#ifndef DEBUG_WRAPPER_H
#define DEBUG_WRAPPER_H
#include <dbgeng.h>
#include <DbgHelp.h>
#include <dia2.h>
#include <windows.h>


#include "logging.h"

class DebugWrapper
{
private:
	static unsigned long m_numThreads;
	static IDebugClient * m_debugClient;
	static IDebugControl5 * m_dbgControl5;
	static IDebugControl4 * m_dbgControl4;
	static IDebugControl * m_dbgControl;
	static IDebugSymbols3 * m_dbgSymbols;
	static IDebugSystemObjects * m_dbgSysObj;
	static IDebugDataSpaces * m_dbgDataSpaces;
	static IDebugRegisters *m_dbgRegisters;
	static IDebugAdvanced2 * m_dbgAdv2;
	static IDebugAdvanced * m_dbgAdv;
	static IDebugRegisters2 * m_dbgRegisters2;

public:

	struct DumpInfo
	{
		char ArxmapPath[MAX_PATH];
		char TargetPath[MAX_PATH];
		char DumpPath[MAX_PATH];
		char SymbolPath[MAX_PATH];
	};

	DebugWrapper();
	~DebugWrapper()
	{
		if (m_debugClient != NULL)
		{
			m_debugClient->Release();
			m_debugClient = NULL;
		}
		if (m_dbgControl != NULL)
		{
			m_dbgControl->Release();
			m_dbgControl = NULL;
		}
		if (m_dbgControl5 != NULL)
		{
			m_dbgControl5->Release();
			m_dbgControl5 = NULL;
		}
		if (m_dbgSymbols != NULL)
		{
			m_dbgSymbols->Release();
			m_dbgSymbols = NULL;
		}
		if (m_dbgSysObj != NULL)
		{
			m_dbgSysObj->Release();
			m_dbgSysObj = NULL;
		}
		if (m_dbgDataSpaces != NULL)
		{
			m_dbgDataSpaces->Release();
			m_dbgDataSpaces = NULL;
		}
		if (m_dbgRegisters != NULL)
		{
			m_dbgRegisters->Release();
			m_dbgRegisters = NULL;
		}
	}
	static DWORD WINAPI LoadDebugInformation(LPVOID param);
	static IDebugClient * DebugClient() { return m_debugClient;}
	static IDebugControl5 * DebugControl5() {return m_dbgControl5;}
	static IDebugControl4 * DebugControl4() {return m_dbgControl4;}
	static IDebugControl * DebugControl() { return m_dbgControl;}
	static IDebugSymbols3 * DebugSymbols3() { return m_dbgSymbols;}
	static IDebugSystemObjects * DebugSystemObjects() { return m_dbgSysObj;}
	static IDebugDataSpaces * DebugDataSpaces() { return m_dbgDataSpaces;}
	static IDebugRegisters * DebugRegisters() { return m_dbgRegisters;}
	static IDebugRegisters2 * DebugRegisters2() { return m_dbgRegisters2;}
	static IDebugAdvanced2 * DebugAdvanced2() { return m_dbgAdv2;}
	static IDebugAdvanced * DebugAdvanced() { return m_dbgAdv;}
	static void GetModuleBaseAddress(ULONG64 testAddr, ULONG64 *baseAddress, ULONG64 *isProtectedModule);
	static unsigned int GetNumThreads() { return m_numThreads;}
	static void RequeryInterfaces(ULONG i);
	static void VerifyStatus(HRESULT hr, PCSTR name)
	{
		if ( hr != S_OK)
		{
			GetLogger()->Info("%s failed with %d",name, hr);
			exit(-1);
		}
		return;
	}
	static void SampleData()
	{
		
	}

};


class OutputCallbacks : public IDebugOutputCallbacks
{
public:
	OutputCallbacks() : m_refCount(1) {}
	virtual ~OutputCallbacks() {}

	// IUnknown.
	STDMETHOD(QueryInterface)(REFIID iid, PVOID* ppInterface)
	{
		if(ppInterface == NULL)
			return E_POINTER;
		*ppInterface = NULL;
		if(iid == __uuidof(IUnknown) || iid == __uuidof(IDebugOutputCallbacks))
		{
			*ppInterface = this;
			AddRef();
			return S_OK;
		}
		return E_NOINTERFACE;
	}

	STDMETHOD_(ULONG, AddRef)()
	{
		LONG ret = InterlockedIncrement(&m_refCount);
		return (ULONG)ret;
	}

	STDMETHOD_(ULONG, Release)()
	{
		if(m_refCount == 0)
		{
			// Umm....
			return 0;
		}
		LONG ret = InterlockedDecrement(&m_refCount);
		return (ULONG)ret;
	}

	// IDebugOutputCallbacks.
	STDMETHOD(Output)(ULONG Mask, PCSTR Text)
	{
		WORD attr = 0;
		if(Mask & DEBUG_OUTPUT_ERROR)
		{
			attr = FOREGROUND_INTENSITY | FOREGROUND_RED;
		}
		else if(Mask & DEBUG_OUTPUT_WARNING)
		{
			attr = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN;
		}
		else if(Mask & DEBUG_OUTPUT_NORMAL)
		{
			attr = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
		}
		else
		{
			attr = FOREGROUND_GREEN;
		}
		if(attr != 0)
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), attr);

		GetLogger()->Info("%s", Text);

		if(attr != 0)
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		return S_OK;
	}

private:
	LONG m_refCount;
};


#endif 