#ifndef FILEPARSING_H
#define FILEPARSING_H
#include <windows.h>
#include <string.h>
DWORD WINAPI parseArxMap(LPVOID param);
DWORD WINAPI parseArxInstMap(LPVOID param);
DWORD WINAPI parseMapFile(LPVOID param);
string GetShortName(string longstr);
void deleteLists();
#endif