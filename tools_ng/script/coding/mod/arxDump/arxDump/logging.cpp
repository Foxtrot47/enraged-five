#include "Logging.h"
#include <windows.h>

#define CHARS_FOR_BUFF 4096
#define CHARS_FOR_PARAMS 3500
char m_logFile[MAX_PATH];

static Logger * sLogger;

HMODULE GetCurrentModule()
{ // NB: XP+ solution!
	HMODULE hModule = NULL;
	GetModuleHandleEx(
		GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
		(LPCTSTR)GetCurrentModule,
		&hModule);

	return hModule;
}
Logger * GetLogger()
{
	if(sLogger == NULL)
	{
		sLogger = new Logger();
	}
	return sLogger;
}
Logger::Logger()
{
	//m_outfile = std::ofstream(m_logFile,std::ios::out);
	memset(m_logFile, 0, sizeof(m_logFile));

	HMODULE hModule = GetCurrentModule();
	if (GetModuleFileNameA(hModule, m_logFile, MAX_PATH) != 0)  
	{
		size_t slash = -1;

		for (size_t i = 0; i < strlen(m_logFile); i++) {
			if (m_logFile[i] == '/' || m_logFile[i] == '\\') {
				slash = i;
			}
		}

		if (slash != -1) {
			m_logFile[slash + 1] = '\0';
			strcat(m_logFile, "arxDumpLogging.log");
		}
	}

}
void Logger::Info(const char * fmt, ...)
{
	va_list va_alist;
	char chLogBuff[CHARS_FOR_BUFF];
	char chParameters[CHARS_FOR_PARAMS];
	char szTimestamp[30];
	struct tm current_tm;
	time_t current_time = time(NULL);
	FILE* file;

	localtime_s(&current_tm, &current_time);
	sprintf_s(szTimestamp, "[%02d:%02d:%02d][MSG] %%s\n", current_tm.tm_hour, current_tm.tm_min, current_tm.tm_sec);

	va_start(va_alist, fmt);
	_vsnprintf_s(chParameters, sizeof(chParameters), fmt, va_alist);
	va_end(va_alist);
	sprintf_s(chLogBuff, szTimestamp, chParameters);
	if ((fopen_s(&file, m_logFile, "a")) == 0)
	{
		printf("%s", chLogBuff);
		fprintf_s(file, "%s", chLogBuff);
		fclose(file);
	}

}


void Logger::Debug(const char * fmt, ...)
{
	va_list va_alist;
	char chLogBuff[CHARS_FOR_BUFF];
	char chParameters[CHARS_FOR_PARAMS];
	char szTimestamp[30];
	struct tm current_tm;
	time_t current_time = time(NULL);
	FILE* file;

	localtime_s(&current_tm, &current_time);
	sprintf_s(szTimestamp, "[%02d:%02d:%02d][DBG] %%s\n", current_tm.tm_hour, current_tm.tm_min, current_tm.tm_sec);

	va_start(va_alist, fmt);
	_vsnprintf_s(chParameters, sizeof(chParameters), fmt, va_alist);
	va_end(va_alist);
	sprintf_s(chLogBuff, szTimestamp, chParameters);
	if ((fopen_s(&file, m_logFile, "a")) == 0)
	{
		fprintf_s(file, "%s", chLogBuff);
		fclose(file);
	}

}
