#include "arxdump.h"
#include "fileparsing.h"
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <string.h>
#pragma optimize("", off)
using namespace std;





string GetShortName(string longstr)
{
	size_t removePath = longstr.find_last_of("\\");
	string shortFileName = longstr.substr(removePath+1, longstr.length());
	size_t firstidx = shortFileName.find_first_of("."); 
	string shortName  = shortFileName.substr(0, firstidx); 
	return shortName;
}

DWORD WINAPI parseMapFile(LPVOID param)		//Runs as it's own thread to save some time
{	
	char * mapFile = (char*)(param);
	int i = 0;

	mapNode * rootNode = GetMapNodeRoot();
	
	do
	{
		if(mapFile== NULL || strlen(mapFile) == 0)
		{
			mapFile+=MAX_PATH;
			continue;
		}

		FILE *fp = fopen(mapFile, "r");
		

		if(fp == NULL)
		{
			mapFile = NULL;
			return 1;
		}


		GetLogger()->Info("Parsing Map file: %s", mapFile);
		bool cont = true;

		bool foundPreferred = false;
		bool foundProcessing = false;
		bool doneProcessing	 = false;
		std::string shortName = GetShortName(mapFile);
		// Parse the first line
		char buffer[4096] = {0};
		if(!feof(fp))
			fgets(buffer, sizeof(buffer), fp);
		while(!feof(fp))
		{	
			memset(buffer, 0, sizeof(buffer));
			//std::getline(file, line);	//Most expensive call in program.... Might be a better way to parse CSV
			fgets(buffer, sizeof(buffer), fp);
			// First look for our preferred load address
			if(!foundPreferred)
			{
				// Lets not grind on a find if we don't need to.
				char *s = strstr(buffer, "Preferred load address is");
				if(s!=NULL)
				{
					mapNode * newNode = new mapNode();

					newNode->base = atoui64(buffer+28);
					newNode->name =  shortName;

					if(rootNode ==NULL)
						SetMapNodeRoot(newNode);
					else
					{
						mapNode *curr = rootNode;
						while(curr->next!=NULL)
							curr=curr->next;
						curr->next = newNode;
						newNode->prev = curr;
					}
					foundPreferred = true;
				}
			}
			else if(!foundProcessing)
			{
				char *s = strstr(buffer, "Rva+Base");
				if(s!=NULL)
				{
					foundProcessing = true;
					// Now consume the empty line
					fgets(buffer, sizeof(buffer), fp);
				}
			}
			else if(!doneProcessing)
			{
				// Now we're grinding, not that it saves us a lot.

				// Lets only carea bout the contigous set of blocks given to us.
				if(strlen(Trim(buffer)) == NULL)
				{
					doneProcessing = true;
					break;
				}

				// The meat of our parsing.
				// First we need to transalte the first weirdly delimited address into a number
				//std::string hexAddr = /*line.substr(1,4) + */line.substr(6,8);
				//std::string section = line.substr(4,1);
				char * sectionPtr = buffer+4;
				char * hexAddtrPtr = buffer+6;
				// We only care about executable code
				//if(section.compare("1") != 0)
				if(*sectionPtr !='1')
					continue;

				ULONG64 hexAddrTyped = atoui64(hexAddtrPtr);
				std::string symbol = buffer+21;
				//symbol = symbol.substr(21, symbol.length());
				//GetLogger()->LogMessage("[0x%I64X]\t[%s]", hexAddrTyped, symbol.c_str());
				//GetMapFileSkipList(i)->insert()
				//GetMapFileSkipList(i)->insert(hexAddrTyped, symbol);
				GetMapFileAddresses(i)->push_back(hexAddrTyped);
				std::pair<ULONG64, string> newPair = std::pair<ULONG64, string>(hexAddrTyped, symbol);
				GetMapFileEntryPairs(i)->insert(newPair);
			}


		}
		fclose(fp);
		std::sort(GetMapFileAddresses(i)->begin(), GetMapFileAddresses(i)->end());
		// Increment our step in the file array
		mapFile+=MAX_PATH;
	}
	while(++i<MAX_ARXMAP_TARGETS);
	GetLogger()->Info("Finished parsing map files");

	return 0;
}


DWORD WINAPI parseArxInstMap(LPVOID param)		//Runs as it's own thread to save some time
{	
	char * arxInstMapFile = (char*)(param);
	int i = 0;

	do
	{
		if(arxInstMapFile == NULL || strlen(arxInstMapFile) == 0)
		{
			arxInstMapFile+=MAX_PATH;
			continue;
		}

		FILE *fp = fopen(arxInstMapFile, "r");


		if(fp == NULL)
		{
			arxInstMapFile = NULL;
			return 1;
		}


		GetLogger()->Info("Parsing ArxInstMap file: %s", arxInstMapFile);


		std::string line;
		// Parse the first line
		char buffer[4096] = {0};
		if(!feof(fp))
			fgets(buffer, sizeof(buffer), fp);
		while(!feof(fp))
		{	
			memset(buffer, 0, sizeof(buffer));
			arxInstMapNode * newNode = new arxInstMapNode();
			if(fscanf(fp, "0x%llX, 0x%llX, 0x%llX\r", &newNode->current, &newNode->original, &newNode->stack)!=3)
			{
				//GetLogger()->Debug("Guard Inst maybe?");
				fscanf(fp, "Guard Inst, \r");
				newNode->original = 0;
				newNode->stack = 0;
			}
			//std::getline(file, line);	//Most expensive call in program.... Might be a better way to parse CSV
			//fgets(buffer, sizeof(buffer), fp);
			//
			//std::istringstream ss(buffer);		
			//char c1,c2;
			//arxInstMapNode * newNode = new arxInstMapNode();
			//
			//ss >> std::hex >> newNode->current >> c1 >>				
			//	std::hex >> newNode->original	>> c2 >>
			//	std::hex >> newNode->stack;				
			//
			//
			// If we're root, initialize the list
			//cout << *newNode << endl;
			GetArxInstMapNodes(i)->push_back(newNode);
		}
		fclose(fp);
		// Increment our Map File Index
		arxInstMapFile+=MAX_PATH;
	}
	while(++i<MAX_ARXMAP_TARGETS);
	GetLogger()->Info("Finished parsing ArxInstMap files.");
	return 0;
}


void deleteLists()
{
   ///* deref head_ref to get the real head */
   //arxMapNode** current = GetArxMapNodeRoot();
   //arxMapNode** next;
 
   //while (current != NULL) 
   //{
   //    next = *current->next;
   //    delete current;
   //    current = next;
   //}
   //
   ///* deref head_ref to affect the real head back
   //   in the caller. */
   //arxMapNode* arxMapRoot = GetArxMapNodeRoot();
   //arxMapRoot = NULL;

   //   /* deref head_ref to get the real head */
   //arxInstMapNode** arxInstMapRoot = GetArxInstMapNodeRoot();
   //arxInstMapNode** currentInst = GetArxInstMapNodeRoot();
   //arxInstMapNode** nextInst;
 
   //while (currentInst != NULL) 
   //{
   //    nextInst = currentInst->next;
   //    delete currentInst;
   //    currentInst = nextInst;
   //}
   //
   ///* deref head_ref to affect the real head back
   //   in the caller. */
   //arxInstMapRoot = NULL;
}