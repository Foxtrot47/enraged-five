#ifndef ARXDUMP_H
#define ARXDUMP_H

#include <atlbase.h>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <list>
#include <map>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <vector>

#include "common.h"
#include "debugwrapper.h"
#include "logging.h"


using namespace std;


#define USAGE_INSTRUCTIONS "arxmap.exe <file path to dump> <target binary name, no file ext> <arxmap file> <directory if symbol files>"
#define ONEATATIME 1
unsigned __int64 atoui64(const char *szUnsignedInt);
class mapNode
{
public:
	mapNode()
	{
		next = NULL;
		prev = NULL;
	}
	~mapNode()
	{
		base = 0;
		name.clear();
	}
	friend ostream& operator<<(ostream& os, const mapNode& amn)
	{
		os	<< setw(WIDTH) << std::hex  << amn.base
			<< setw(WIDTH) << amn.name;
		return os;
	}
	ULONG64 base;
	std::string name;
	mapNode * next;
	mapNode * prev;
};
class arxMapNode
{
public:
	arxMapNode()
	{
		next = NULL;
		prev = NULL;
	}
	~arxMapNode()
	{
		starts = 0;
		ends = 0;
		original.clear();
	}
	friend ostream& operator<<(ostream& os, const arxMapNode& amn)
	{
		os	<< setw(WIDTH) << std::hex  << amn.starts
			<< setw(WIDTH) << std::hex  << amn.ends 
			<< setw(WIDTH) << amn.original;
		return os;
	}
	ULONG64 starts;
	ULONG64 ends;
	std::string original;
	arxMapNode * next;
	arxMapNode * prev;
};
class arxInstMapNode
{
public:
	arxInstMapNode()
	{
		/*next = NULL;
		prev = NULL;*/
	}
	~arxInstMapNode()
	{
		current = 0;
		original = 0;
		stack = 0;
	}
	friend ostream& operator<<(ostream& os, const arxInstMapNode& amn)
	{
		os	<< setw(WIDTH) << std::hex  << amn.current
			<< setw(WIDTH) << std::hex  << amn.original
			<< setw(WIDTH) << std::hex  << amn.stack;
		return os;
	}
	ULONG64 current;
	ULONG64 original;
	ULONG64 stack;
	//arxInstMapNode * next;
	//arxInstMapNode * prev;
};

mapNode			*GetMapNodeRoot();
arxMapNode		*GetArxMapNodeRoot();
arxInstMapNode	*GetArxInstMapNodeRoot();

vector<ULONG64>			* GetMapFileAddresses(ULONG64);
map<ULONG64, string>	* GetMapFileEntryPairs(ULONG64);
list<arxInstMapNode*>	* GetArxInstMapNodes(ULONG64);

void SetMapNodeRoot(mapNode *rt);

DebugWrapper::DumpInfo * readCommandLine(int argc, char* argv[]);
DebugWrapper::DumpInfo * findInputFiles(LPCSTR dir);
ULONG64 ResolveCurrentBinary(char* filename);
void Exit(int exitCode, const char * msg);

#endif

