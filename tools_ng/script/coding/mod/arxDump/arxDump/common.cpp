#include "common.h"
using namespace std;
ULONG64 ResolveCurrentBinary(char* filename)
{
	std::string fname = filename;

	if( strstr(filename,"GTA5") != NULL)
		return 0;
	else if( strstr(filename,"game_win64") != NULL)
		return 0;
	else if( strstr(filename,"gta5") != NULL)
		return 0;
	else if( strstr(filename,"GTAVLauncher") != NULL)
		return 1;
	else if( strstr(filename,"socialclub") != NULL)
		return 2;
	else if( strstr(filename,"PlayGTAV") != NULL)
		return 3;
	else if( strstr(filename,"quickTesting") != NULL)
		return 4;
	else
		return -1;
}
char* LTrim(char* szX)
{
	if(' '==szX[0]) while(' '==(++szX)[0]);
	return szX;
}

char* RTrim(char* szX)
{
	int i = strlen(szX);
	while(' '==szX[--i]) szX[i] = 0;
	return szX;
}

char* Trim(char* szX)
{
	szX = LTrim(szX);
	szX = RTrim(szX);
	return szX;
}


unsigned __int64 atoui64(const char *szUnsignedInt) {
	return _strtoui64(szUnsignedInt, NULL, SIZE_WORD*2);
}

std::vector<std::string> split(const std::string &text, char sep) {
	std::vector<std::string> tokens;
	std::size_t start = 0, end = 0;
	while ((end = text.find(sep, start)) != std::string::npos) {
		tokens.push_back(text.substr(start, end - start));
		start = end + 1;
	}
	tokens.push_back(text.substr(start));
	return tokens;
}

