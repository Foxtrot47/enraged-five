#include <string.h>

#include "arxdump.h"
#include "fileparsing.h"
#include "logging.h"
#include "stackparsing.h"
// Set our optimizations
#pragma optimize("", off)

// Declare our static variables
unsigned long			DebugWrapper::m_numThreads;
IDebugClient*			DebugWrapper::m_debugClient;
IDebugControl5 *		DebugWrapper::m_dbgControl5;
IDebugControl4 *		DebugWrapper::m_dbgControl4;
IDebugControl *			DebugWrapper::m_dbgControl;
IDebugSymbols3 *		DebugWrapper::m_dbgSymbols;
IDebugSystemObjects *	DebugWrapper::m_dbgSysObj;
IDebugDataSpaces *		DebugWrapper::m_dbgDataSpaces;
IDebugRegisters *		DebugWrapper::m_dbgRegisters;
IDebugRegisters2 *		DebugWrapper::m_dbgRegisters2;
IDebugAdvanced2 *		DebugWrapper::m_dbgAdv2;
IDebugAdvanced *		DebugWrapper::m_dbgAdv;

static arxInstMapNode	* arxInstMapRoot; 
static arxMapNode		* arxMapRoot; 
static mapNode			* mapRoot;

static char arxMapFiles[MAX_ARXMAP_TARGETS][MAX_PATH];
static char arxInstMapFiles[MAX_ARXMAP_TARGETS][MAX_PATH];
static char mapFiles[MAX_ARXMAP_TARGETS][MAX_PATH];

// Static variables for map file entries
static vector<ULONG64> mapFileAddresses[MAX_ARXMAP_TARGETS];
static map<ULONG64, string> mapFileEntries[MAX_ARXMAP_TARGETS];

// Static variables for arxInstMapNodes
static list<arxInstMapNode*> arxInstMapNodes[MAX_ARXMAP_TARGETS];

int main(int argc,char* argv[])
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Initialize components
	clock_t start = clock();	
	DebugWrapper::DumpInfo * di = readCommandLine(argc,argv);
	//New thread to parse arxmap file, it takes a while...
#if !ONEATATIME
	HANDLE hArxMapThread;
	HANDLE hArxInstMapThread;
	HANDLE hLoadDebugFile;
	HANDLE hMapFileThread;
	DWORD arxMapThreadId;
	DWORD arxInstMapThreadId;
	DWORD loadDebugFileThreadId;
	DWORD hMapFileThreadId;
#endif
	// Create our threads
	// Create our debug wrapper and initialize our debug routines from the OS
	DebugWrapper *wrap = new DebugWrapper();

	// Create our threads
#if ONEATATIME
	DebugWrapper::LoadDebugInformation((LPVOID)di);
	parseMapFile((LPVOID)&mapFiles);
	parseArxInstMap((LPVOID)&arxInstMapFiles);
#else
	hMapFileThread = CreateThread(NULL,0,parseMapFile,(LPVOID)&mapFiles,0,&hMapFileThreadId);
	hArxMapThread = CreateThread(NULL,0,parseArxMap,(LPVOID)&arxMapFiles,0,&arxMapThreadId);
	hArxInstMapThread = CreateThread(NULL,0,parseArxInstMap,(LPVOID)&arxInstMapFiles,0,&arxInstMapThreadId);
	hLoadDebugFile = CreateThread(NULL,0,DebugWrapper::LoadDebugInformation,(LPVOID)di,0,&loadDebugFileThreadId);

	// Wait for the threads to stop parsing
	WaitForSingleObject(hMapFileThread ,INFINITE);
	WaitForSingleObject(hArxMapThread,INFINITE);
	WaitForSingleObject(hArxInstMapThread,INFINITE);
	WaitForSingleObject(hLoadDebugFile,INFINITE);

#endif

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Walk the stack
	GetLogger()->Info("**********************");
	GetLogger()->Info("Stack Output          ");
	GetLogger()->Info("**********************\n");

	parseStack(wrap);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Cleanup	

	void Cleanup();
	clock_t stop = clock();
	int time = (stop-start)/CLOCKS_PER_SEC;
	GetLogger()->Info("arxDumpComplete...",time);
	GetLogger()->Info("Took %d seconds...",time);
	Exit(1,"Finished Translating Dump, press Enter to exit.");
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Functions


DebugWrapper::DumpInfo *readCommandLine(int argc, char* argv[])
{
	char inputDirectory[MAX_PATH];
	memset(inputDirectory,0,MAX_PATH);
	if(argc > 3)
	{
		std::cout<< USAGE_INSTRUCTIONS << std::endl;
		GetLogger()->Info("Incorrect arguments");
	}
	else if(argc == 2)
		strcpy_s(inputDirectory,MAX_PATH,argv[1]);
	else
	{
		while(strlen(inputDirectory) < 1)
		{
			GetLogger()->Info("No directory provided - Please enter a target directory.");
			std::cin>>inputDirectory;
		}
	}

	DebugWrapper::DumpInfo * di = findInputFiles(inputDirectory);
	if(di == NULL)
	{
		Exit(0,"findInputFiles");
		return NULL;
	}
	else
		return di;
	//time_t timeInfo;
	//time(&timeInfo);
	//sprintf(logFile,"%s.arxDump.%d.log.txt",dumpInfo->DumpPath,timeInfo);
}

DebugWrapper::DumpInfo * findInputFiles(LPCSTR inputDirectories)
{
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LARGE_INTEGER filesize;
	char szDir[MAX_PATH] = {0};
	char* ext = NULL;

	DebugWrapper::DumpInfo *info = new DebugWrapper::DumpInfo();

	for(int i=0;i<MAX_ARXMAP_TARGETS;i++)
	{
		memset(mapFiles[i], 0, MAX_PATH);
		memset(arxMapFiles[i], 0, MAX_PATH);
		memset(arxInstMapFiles[i], 0, MAX_PATH);
	}

	std::string inDir = inputDirectories;
	std::vector<std::string> splitDirs = split(inDir, ';');
	std::vector<std::string>::iterator itr = splitDirs.begin();
	for(itr; itr!=splitDirs.end();itr++)
	{
		std::string currentDirectory = *itr;
		GetLogger()->Info("Searching the directory %s for map, arxmap, dmp, and symbol files.", currentDirectory.c_str());

		strncpy(szDir,currentDirectory.c_str(),MAX_PATH);
		strncat(szDir,"\\*",MAX_PATH);

		hFind = FindFirstFile(szDir,&ffd);

		if(INVALID_HANDLE_VALUE == hFind)
		{
			continue;
		}

		
		strncpy((char *)&info->SymbolPath,currentDirectory.c_str(),MAX_PATH);

		do
		{
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				//Directory
			}
			else
			{
				char* file;
				char* tmp;
				char delim[2];
				delim[0] = '.';
				delim[1] = 0;
				ext = "";

				filesize.LowPart = ffd.nFileSizeLow;
				filesize.HighPart = ffd.nFileSizeHigh;

				file = new char[MAX_PATH];
				strcpy_s(file,MAX_PATH,ffd.cFileName);

				tmp = strtok(file,delim);
				while(tmp!=NULL)
				{
					ext = tmp;
					tmp = strtok(0,delim);
				}

				if(!strcmp(ext,"arxmap"))
				{
					strcpy_s(file,MAX_PATH,currentDirectory.c_str());
					strcat_s(file,MAX_PATH,"\\");
					strcat_s(file,MAX_PATH+1,ffd.cFileName);
					ULONG64 idx = ResolveCurrentBinary(file);
					if(idx < 0)
						Exit(-1,"Arxmap filename does not match a known target");
					if (arxMapFiles[idx][0] == NULL)
					{
					
						strcpy_s(arxMapFiles[idx],MAX_PATH,file);
					}
					else
						std::cout<<"ERROR: Duplicate file: "<<file<<std::endl<<"Proceeding with file: "<<arxMapFiles[idx]<<std::endl;
				}

				if(!strcmp(ext,"arxinstmap"))
				{
					strcpy_s(file,MAX_PATH,currentDirectory.c_str());
					strcat_s(file,MAX_PATH,"\\");
					strcat_s(file,MAX_PATH+1,ffd.cFileName);
					ULONG64 idx = ResolveCurrentBinary(file);
					if(idx < 0)
						Exit(-1,"ArxInstMap filename does not match a known target");
					if (arxInstMapFiles[idx][0] == NULL)
					{
						strcpy_s(arxInstMapFiles[idx],MAX_PATH,file);
					}
					else
						std::cout<<"ERROR: Duplicate file: "<<file<<std::endl<<"Proceeding with file: "<<arxInstMapFiles[idx]<<std::endl;
				}

				if(!strcmp(ext,"map"))
				{
					strcpy_s(file,MAX_PATH,currentDirectory.c_str());
					strcat_s(file,MAX_PATH,"\\");
					strcat_s(file,MAX_PATH+1,ffd.cFileName);
					ULONG64 idx = ResolveCurrentBinary(file);
					if(idx < 0)
						Exit(-1,"Map filename does not match a known target");
					if (mapFiles[idx][0] == NULL)
					{
						strcpy_s(mapFiles[idx],MAX_PATH,file);
					}
					else
						std::cout<<"ERROR: Duplicate file: "<<file<<std::endl<<"Proceeding with file: "<<mapFiles[idx]<<std::endl;
				}


				if(StrStrI(ext,"dmp"))
				{
					strcpy_s(file,MAX_PATH,currentDirectory.c_str());
					strcat_s(file,MAX_PATH,"\\");
					strcat_s(file,MAX_PATH,ffd.cFileName);
					strcpy_s((char *)(&info->DumpPath),MAX_PATH,file);
				}
			}
		}
		while(FindNextFile(hFind, &ffd) !=0);
		FindClose(hFind);
	}

	if(info->DumpPath == NULL)
	{
		Exit(1,"No Dump file found... Exiting");
	}
	return info;
}


mapNode *GetMapNodeRoot()
{
	return mapRoot;
}

void SetMapNodeRoot(mapNode *rt)
{
	mapRoot = rt;
}

vector<ULONG64> * GetMapFileAddresses(ULONG64 i)
{
	return &mapFileAddresses[i];
}

map<ULONG64, string>  * GetMapFileEntryPairs(ULONG64 i)
{
	return &mapFileEntries[i];
}



list<arxInstMapNode*> * GetArxInstMapNodes(ULONG64 i)
{
	return &arxInstMapNodes[i];
}

void Exit(int exitCode, const char * msg)
{
	GetLogger()->Info("%s", msg);
	exit(exitCode);
}

static bool deleteArxInstMap( arxInstMapNode * theElement )
{
	delete theElement; return true; 
}

void Cleanup()
{

	for(ULONG i = 0; i < MAX_ARXMAP_TARGETS; i++)
	{
		GetArxInstMapNodes(i)->remove_if(deleteArxInstMap);
	}
	return;
}

