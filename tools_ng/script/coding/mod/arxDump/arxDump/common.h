#ifndef COMMON_H
#define COMMON_H

#include <string>
#include <windows.h>
#include <vector>



#define MAX_ARXMAP_TARGETS		5
#define NUMBER_CONTEXTS			8
#define NUMBER_FRAMES			32
#define BUFFER_LENGTH_FRAME		4096
#define BUFFER_LENGTH_STRING	1024
#define BUFFER_LENGTH_MODULE	256
#define SIZE_WORD				8
#define SIZE_TEXT_SECTION		0x1000
#define STACK_DEPTH_ATTEMPT		48
#define STACK_TRACE_FRAMES		200
#define WIDTH					16

#define IsProtectedModule(x)	(x >=0 && x < MAX_ARXMAP_TARGETS) 

unsigned __int64 atoui64(const char *szUnsignedInt);
ULONG64 ResolveCurrentBinary(char* filename);

// Trimming Functions
char* LTrim(char* szX);
char* RTrim(char* szX);
char* Trim(char* szX);

// STL enhancements
std::vector<std::string> split(const std::string &text, char sep);
template<class ForwardIt, class T>
ForwardIt lower_bound(ForwardIt first, ForwardIt last, const T& value)
{
	ForwardIt it;
	typename std::iterator_traits<ForwardIt>::difference_type count, step;
	count = std::distance(first, last);

	while (count > 0) {
		it = first; 
		step = count / 2; 
		std::advance(it, step);
		if (*it < value) {
			first = ++it; 
			count -= step + 1; 
		}
		else
			count = step;
	}
	return first;
}
#endif