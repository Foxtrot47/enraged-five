#include "arxdump.h"
#include "fileparsing.h"
#include "stackparsing.h"

#include <string.h>
#include <vector>
#include <iterator>
#include <windows.h>
#include "logging.h"
#include "common.h"
using namespace std;
// Pragmas to make my life easier

#pragma optimize("", off)
#pragma warning(disable:4996)


vector<DEBUG_MODULE_PARAMETERS> allModuleParameters;


void parseStack(DebugWrapper *dw, ULONG64 moduleIndicator, ULONG64 baseImageAddress, ULONG64 rsp, ULONG64 rip)
{
	GetLogger()->Debug("********************");
	GetLogger()->Debug("RSP        : 0x%I64X", rsp);
	GetLogger()->Debug("RIP        : 0x%I64X", rip);
	GetLogger()->Debug("BASE       : 0x%I64X", baseImageAddress);
	GetLogger()->Debug("ISPRTCTED  : 0x%I64X", moduleIndicator);

	ULONG64 selectedNodeStack = 0; 
	arxInstMapNode *node;
	arxInstMapNode *prevNode;
	// Lets get the associated arxInstMapNode
	findCorrespondingArxInstMapNodes(moduleIndicator, rip - baseImageAddress, node, prevNode);

	if(node == NULL)
	{
		if(rip == 0)
			return;
		
		if(IsProtectedModule(moduleIndicator))
		{
			const size_t buflen = BUFFER_LENGTH_STRING;
			CHAR wszBuf[buflen] = {0};
			// Attempt second chance
			if(prevNode == NULL)
			{
				HRESULT hres = dw->DebugSymbols3()->GetNameByOffset(rip - baseImageAddress, wszBuf, buflen, 0, 0);
				GetLogger()->Info("> %s",wszBuf);
			}
			else
			{
				// Lets get this function's name
				char functionName[BUFFER_LENGTH_MODULE];
				memset(functionName, 0, BUFFER_LENGTH_MODULE);
				ULONG sizeOfBuffer = 0;
				ULONG64 displacement = 0;
				//GetLogger()->LogMessage("SBLZING    : 0x%I64X", node->original+preferredBaseAddress);

				vector<ULONG64>::iterator itr = lower_bound(GetMapFileAddresses(moduleIndicator)->begin(), GetMapFileAddresses(moduleIndicator)->end(), prevNode->original-0x1000);
				itr = std::prev(itr);

				if(*itr)
				{
					ULONG64 deltaAddr = (prevNode->original-SIZE_TEXT_SECTION)-*itr;
					map<ULONG64, string>  * entryPairs = GetMapFileEntryPairs(moduleIndicator);
					map<ULONG64, string>::iterator entryItr = entryPairs->begin();
					// Need a for loop here, as optimziations may yield multiple symbols 
					// resolving to a single address
					for(entryItr; entryItr!=entryPairs->end();entryItr++)
					{
						ULONG64 funcaddr = entryItr->first;
						if(funcaddr == *itr)
						{
							string mapFileLine = entryItr->second;
							size_t idxEndOfMapFileFxnName = mapFileLine.find_first_of(" ");
							GetLogger()->Info("> %-64s", mapFileLine.substr(0,idxEndOfMapFileFxnName).c_str());

							const size_t bufLen = BUFFER_LENGTH_STRING;
							CHAR tryName[bufLen] = {0};
						}
					}

				}

				selectedNodeStack = prevNode->stack; 	
			}
			
		}
		else
		{
			GetLogger()->Info("*UNRESOLVED* [0x%I64X]",rip - baseImageAddress);
		}
	}
	else
	{
		// Lets get this function's name
		char functionName[BUFFER_LENGTH_MODULE];
		memset(functionName, 0, BUFFER_LENGTH_MODULE);
		ULONG sizeOfBuffer = 0;
		ULONG64 displacement = 0;
		//GetLogger()->LogMessage("SBLZING    : 0x%I64X", node->original+preferredBaseAddress);

		vector<ULONG64>::iterator itr = lower_bound(GetMapFileAddresses(moduleIndicator)->begin(), GetMapFileAddresses(moduleIndicator)->end(), node->original-0x1000);
		itr = std::prev(itr);

		if(*itr)
		{
			ULONG64 deltaAddr = (node->original-SIZE_TEXT_SECTION)-*itr;
			map<ULONG64, string>  * entryPairs = GetMapFileEntryPairs(moduleIndicator);
			map<ULONG64, string>::iterator entryItr = entryPairs->begin();
			// Need a for loop here, as optimziations may yield multiple symbols 
			// resolving to a single address
			for(entryItr; entryItr!=entryPairs->end();entryItr++)
			{
				ULONG64 funcaddr = entryItr->first;
				if(funcaddr == *itr)
				{
					string mapFileLine = entryItr->second;
					size_t idxEndOfMapFileFxnName = mapFileLine.find_first_of(" ");
					GetLogger()->Info("> %-64s", mapFileLine.substr(0,idxEndOfMapFileFxnName).c_str());

					const size_t bufLen = BUFFER_LENGTH_STRING;
					CHAR tryName[bufLen] = {0};

					//string t = mapFileLine.substr(idxEndOfMapFileFxnName+1, mapFileLine.length());
					//size_t idxEndOfMapFileAddr = t.find_first_of(" ");
					//DWORD64 mapFileAddr = atoui64(mapFileLine.substr(idxEndOfMapFileFxnName,idxEndOfMapFileAddr).c_str());
					//HRESULT hres = dw->DebugSymbols3()->GetNameByOffset(node->original-0x1000, tryName, bufLen, 0, 0);
					//GetLogger()->Info("<<<<<< %-64s", tryName);
				}
			}
		}
		selectedNodeStack = node->stack;
		
	}

	ULONG64 rspValue;
	ULONG read = 0;
	DebugWrapper::VerifyStatus(dw->DebugDataSpaces()->ReadVirtual(rsp,&rspValue,SIZE_WORD,&read), "ReadVirtual");
	
	ULONG64 rspPlusStackAddress = 0;
	ULONG64 rspPlusStackValue = 0;
	

	// In the case that we found a node, we'll have a value, otherwise it'll be 0
	rspPlusStackAddress = rsp + selectedNodeStack;
	

	GetLogger()->Debug("RSP+Stack  : 0x%I64X", rspPlusStackAddress);
	read = 0;
	DebugWrapper::VerifyStatus(dw->DebugDataSpaces()->ReadVirtual(rspPlusStackAddress,&rspPlusStackValue,SIZE_WORD,&read), "ReadVirtual");
	GetLogger()->Debug("[RSP+Stack]: 0x%I64X",rspPlusStackValue);
	
	ULONG64 newModuleInidcator = 0;
	ULONG64 newBaseAddress = 0;
	DebugWrapper::GetModuleBaseAddress(rspPlusStackValue, &newBaseAddress, &newModuleInidcator);
	return parseStack(dw, newModuleInidcator, newBaseAddress, rspPlusStackAddress + SIZE_WORD ,rspPlusStackValue);
}


bool AttemptStackWalk(DebugWrapper *dw)
{
	bool retVal = true;
	//const ULONG MAX_FRAMES	   = 1024;
	const ULONG numberContexts = NUMBER_CONTEXTS;
	const ULONG numberFrames   = NUMBER_FRAMES;
	const ULONG	bufferSize	   = BUFFER_LENGTH_FRAME;
	HRESULT hr = S_OK;
	DEBUG_STACK_FRAME exceptionStackFrames[numberFrames] = {0};
	ULONG filledNumFrames = 0;

	CONTEXT exceptionContext[numberContexts] = {0};
	ULONG exceptionType = 0;
	ULONG processId = 0;
	ULONG threadID = 0;
	ULONG filledContextSize = 0;
	char *filledFrameContexts = NULL;
	char extraData[bufferSize] = {0};
	ULONG filledExtraSize = 0;
	try
	{
		// look for an embedded event
		hr = dw->DebugControl4()->GetStoredEventInformation(
			&exceptionType, 
			&processId, 
			&threadID,
			exceptionContext, 
			sizeof(exceptionContext), 
			&filledContextSize, 
			extraData,
			sizeof(extraData), 
			&filledExtraSize);

		DWORD64* extraDataVal = (DWORD64*)extraData;
		if((*extraDataVal == 0xC0000005) || (*extraDataVal = 0x0000000080000003))
			retVal = false;
		// get the stack trace
		else if(SUCCEEDED(hr))
		{
			filledFrameContexts = new char[numberFrames*filledContextSize];
			hr = dw->DebugControl4()->GetContextStackTrace(
				exceptionContext, 
				filledContextSize,
				exceptionStackFrames, 
				ARRAYSIZE(exceptionStackFrames), 
				filledFrameContexts,
				numberFrames*filledContextSize, 
				filledContextSize, 
				&filledNumFrames);

			if(FAILED(hr))
			{
				GetLogger()->Info("!!11DANGER WIL ROBINSON111! - GetContextStackTrace() failed. Create a bug for Amir Soofi.");
			}

			for(ULONG i = 0 ; i < filledNumFrames; i++)
			{
			
				ULONG64 moduleIndicator = 0;
				ULONG64 baseImageAddress = 0;
				const size_t buflen = BUFFER_LENGTH_STRING;
				CHAR funcName[buflen];
				CHAR funcLine[buflen];
				ULONG lineNo = 0;
				HRESULT hres = dw->DebugSymbols3()->GetNameByOffset(exceptionStackFrames[i].InstructionOffset, funcName, buflen, 0, 0);
				//DebugWrapper::GetModuleBaseAddress(ctxFilledContexts[i].Rip, &baseImageAddress, &moduleIndicator);
				
			
				hres = dw->DebugSymbols3()->GetLineByOffset(exceptionStackFrames[i].InstructionOffset,
					&lineNo, funcLine, ARRAYSIZE(funcLine)-1, NULL, NULL);
				GetLogger()->Info("> %-64s%s:%u", funcName, funcLine, lineNo);
			}
		}
		
	}
	catch (exception e)
	{
		GetLogger()->Info("Failed at Exception Stack Walking.");
		retVal = false;
	}

	if(!retVal)
	{
		ULONG64 moduleIndicator = 0;
		ULONG64 baseImageAddress = 0;
		DebugWrapper::GetModuleBaseAddress(exceptionContext[0].Rip, &baseImageAddress, &moduleIndicator);

		parseStack(dw, (ULONG64)moduleIndicator, baseImageAddress, exceptionContext[0].Rsp, exceptionContext[0].Rip);
		retVal = true;
	}
	return retVal;
}

void parseStack(DebugWrapper *dw) 
{
	if(AttemptStackWalk(dw))
	{
		// This succeeded, so lets return
		return;
	}
	else
		GetLogger()->Info("No form of stack walking succeded. Make a bug against Amir Soofi.");
	return;
}

void findCorrespondingArxInstMapNodes(ULONG64 moduleIndicator, DWORD64 offset, arxInstMapNode *& result, arxInstMapNode *& resultPrev)
{
	// Only do linear scans on modules that we actually know about.
	if(moduleIndicator >=0 && moduleIndicator <= MAX_ARXMAP_TARGETS)
	{
		// Linear scans suck, but I'm willing wait for functionality
		list<arxInstMapNode*>::iterator itr = GetArxInstMapNodes(moduleIndicator)->begin();
		list<arxInstMapNode*>::iterator beginItr = GetArxInstMapNodes(moduleIndicator)->begin();
		for(itr; itr!=GetArxInstMapNodes(moduleIndicator)->end(); itr++)
		{
			arxInstMapNode* node = (arxInstMapNode*)(*itr);
			if(offset == node->current)
			{
				result = node;
				resultPrev = NULL;
				return;
			}
			else if(offset < node->current )
			{
				result = NULL;
				if(itr!=beginItr)
				{
					itr = std::prev(itr);
					resultPrev = *itr;
				}
				else
				{
					result = NULL;
					resultPrev = NULL;
				}
				return;
			}
		
		}

	}
	result = NULL;
	resultPrev = NULL;
}


// Deprecated code
#if 0

bool AttemptArxInstMapStackWalk(DebugWrapper *dw)
{
	// Before we do anything, lets determine if there's an exception to worry about.
	const ULONG numFrames			= 32;
	const ULONG numExceptionFrames	= 32;

	HRESULT hr = S_OK;

	DEBUG_STACK_FRAME exceptionStackFrames[numExceptionFrames] = {0};


	// We actually want to iterate over every thread, and then select the information of the stack frame based on that thread.
	PULONG threadIds		= new ULONG[dw->GetNumThreads()];
	PULONG threadSystemIds	= new ULONG[dw->GetNumThreads()];
	ULONG threadSystemId;
	ULONG threadId;
	ULONG processId;

	// Get all of our thread IDS
	dw->DebugSystemObjects()->GetThreadIdsByIndex(0, dw->GetNumThreads(), threadIds, threadSystemIds);

	// Iterate over every thread
	for (ULONG64 i = 0; i< dw->GetNumThreads() ;i++)
	{
		DebugWrapper::VerifyStatus(dw->DebugSystemObjects()->SetCurrentThreadId(threadIds[i]),"SetCurrentThread");			//Debug engine stores threads numbered 0->n, which is the same order as seen in VS debugger
		DebugWrapper::VerifyStatus(dw->DebugSystemObjects()->GetCurrentProcessId(&processId), "GetCurrentProcessID");
		DebugWrapper::VerifyStatus(dw->DebugSystemObjects()->GetCurrentThreadId(&threadId), "GetCurrentThreadId");
		DebugWrapper::VerifyStatus(dw->DebugSystemObjects()->GetCurrentThreadSystemId(&threadSystemId),"GetCurrentThreadSystemId");	//Grabs actual PID

		GetLogger()->LogMessage("\nThread ID  : 0x%03X	Thread #: %d",threadSystemIds[i], i);
		GetLogger()->LogMessage("Displaying Windows First Chance Stack Unrolling");

		const size_t buflen = 1024;
		CHAR wszBuf[buflen];

		ULONG numFramesFilled	= 0;
		LONG brokenIndex		= -1;
		DEBUG_STACK_FRAME frames[numFrames] = {0};
		if(threadSystemIds[i] != 0x24CC)
			continue;

		//HRESULT hr = dw->DebugControl()->GetStackTrace(0, 0, 0, frames, numFrames, &numFramesFilled);
		//if (SUCCEEDED(hr))
		//{
		//
		//	for (ULONG i=0; i < numFramesFilled; ++i) 
		//	{
		//		HRESULT hres = dw->DebugSymbols3()->GetNameByOffset(frames[i].InstructionOffset, wszBuf, buflen, 0, 0);
		//
		//
		//		ULONG64 tmIndic = 0;
		//		ULONG64 tbAddr  = 0;
		//		DebugWrapper::GetModuleBaseAddress(frames[i].InstructionOffset, &tbAddr, &tmIndic);
		//
		//		if(FAILED(hres))
		//		{
		//			GetLogger()->LogMessage("> Broken frame!");
		//			brokenIndex = i;
		//			break;
		//		}
		//		// In this scenario, we want to mark the rip and rsp
		//		else if(tmIndic >= 0 && tmIndic < MAX_ARXMAP_TARGETS)
		//		{
		//			// Blamo, we've reached our code
		//			GetLogger()->LogMessage("> Huzzah!");
		//			brokenIndex = i;
		//			break;
		//		}
		//		else
		//		{
		//			// Display the generic stack trace, if it isn't one of our functions	
		//			GetLogger()->LogMessage("> %s", wszBuf);
		//		}
		//	}
		//}

		if(brokenIndex == -1)
		{

			ULONG64 rip =  frames[brokenIndex].InstructionOffset;
			ULONG64 rsp =  frames[brokenIndex].StackOffset;

			CONTEXT * currentThreadContext = new CONTEXT;
			dw->DebugAdvanced()->GetThreadContext(currentThreadContext, sizeof(CONTEXT));
			rip =  currentThreadContext->Rip;
			rsp =  currentThreadContext->Rsp;

			ULONG64 moduleIndicator = 0;
			ULONG64 baseImageAddress = 0;
			DebugWrapper::GetModuleBaseAddress(rip, &baseImageAddress, &moduleIndicator);


			GetLogger()->LogMessage("********************");
			GetLogger()->LogMessage("RSP        : 0x%I64X", rsp);
			GetLogger()->LogMessage("RIP        : 0x%I64X", rip);
			GetLogger()->LogMessage("BASE       : 0x%I64X", baseImageAddress);
			GetLogger()->LogMessage("DELTA      : 0x%I64X", rip-baseImageAddress);
			GetLogger()->LogMessage("ISPRTCTED  : 0x%I64X", moduleIndicator);

			parseStack(dw, (ULONG64)moduleIndicator, baseImageAddress, rsp, rip);


		}
	}
	return true;
}

int arxInstMapize(DWORD64 currentIP, DWORD64 currentRsp, DWORD64 baseImageAddress, DebugWrapper *dw)
{

	DWORD64 relCrashAddress = currentIP - baseImageAddress;
	arxInstMapNode *node = findCorrespondingArxInstMapNode(relCrashAddress);
	if(node)
	{
		char symbolName[BUFFER_LENGTH] = {0};
		char buffer[BUFFER_LENGTH] = {0};
		ULONG nameSize = 0;
		ULONG64 displace = 0;
		DebugWrapper::VerifyStatus(dw->DebugSymbols3()->GetNameByOffset(node->current+baseImageAddress,symbolName,BUFFER_LENGTH,&nameSize,&displace),"GetNameByOffset");
		memset(buffer,0,BUFFER_LENGTH);
		sprintf(buffer,"%s+%x",symbolName,displace);		//Write function name to log
		GetLogger()->LogMessage(buffer);

		DWORD64 originalRva = node->original;
		DWORD64 memAddrReturnAddress = currentRsp + node->stack;
		DWORD64 memAddrReturnAddressVal = 0;
		ULONG read = 0;
		dw->DebugDataSpaces()->ReadVirtual(memAddrReturnAddress,&memAddrReturnAddressVal,8,&read);
		return arxInstMapize(memAddrReturnAddressVal, memAddrReturnAddress+8, baseImageAddress, dw);
	}
	else
	{
		cout << "Offset " << std::hex << relCrashAddress << " not found in ArxInstMap" << endl;
		return 0;
	}
}

bool translateArxOffset(char ** arxMapFiles, ULONG64 off,int idx,char* out)
{
	if(arxMapFiles[idx][0] == NULL)
		return false;

	arxMapNode * nodePtr = GetArxMapNodeRoot();
	arxMapNode * oldNode = NULL;
	while(nodePtr!=NULL && nodePtr->next!=NULL)
	{
		if(off >= nodePtr->starts && off < nodePtr->ends)				//The arxmap file is ordered low to high addresses.
		{
			std::string s = nodePtr->original;		//All original addresses are stored as strings		
			PCSTR a = s.c_str();					//Any memory ranges under an Arxan guard have a string defining their purpose
			strncpy_s(out,MAX_PATH,a,s.length());
			return true;
		}
		oldNode = nodePtr;
		nodePtr = nodePtr->next;
	}
	return false;
}

bool IsInMemorySpace(ULONG64 test, ULONG64 base, ULONG64 size)
{
	if((test < (base + size)) && (test >= base))
		return true;
	return false;
}

ULONG64 GetPreferredBaseAddress(string moduleName)
{

	mapNode *rootPtr = GetMapNodeRoot();
	while(rootPtr!=NULL)
	{
		if(moduleName.find(rootPtr->name.c_str())!=string::npos)
		{
			return rootPtr->base;
		}
		rootPtr = rootPtr->next;
	}

	return 0;
}
#endif