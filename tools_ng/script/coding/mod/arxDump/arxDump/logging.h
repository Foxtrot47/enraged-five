#ifndef LOGGING_H
#define LOGGING_H

#include <sstream>
#include <iomanip>
#include <iostream>
#include <fstream>

class Logger
{
private:
	static std::ofstream m_outfile;

	
public:
	Logger();
	static void Info(const char * format, ...);
	static void Debug(const char * format, ...);
	
};
Logger *GetLogger();
#endif