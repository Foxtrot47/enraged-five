#include "debugwrapper.h"
#include "common.h"

DWORD WINAPI DebugWrapper::LoadDebugInformation(LPVOID param)
{
	DumpInfo * dumpInfo = (DumpInfo *)param;
	// Check symsrv.dll is in the right place (Alongside the loaded dbghelp.dll)
	char filename[MAX_PATH];
	if(GetModuleFileName(GetModuleHandle("dbghelp.dll"), filename, sizeof(filename)) != 0)
	{
		char* p = strrchr(filename, '\\');
		if(p)
			p[1] = '\0';
		strcat(filename, "symsrv.dll");
		if(GetFileAttributes(filename) == INVALID_FILE_ATTRIBUTES)
		{
			GetLogger()->Info(filename);
			GetLogger()->Info(" does not exist. Add it to get symbols from Microsoft Symbol Servers.");
		}
	}

	m_dbgSymbols->AppendSymbolPath(dumpInfo->SymbolPath);
	m_dbgSymbols->AppendSymbolPath("srv*http://msdl.microsoft.com/download/symbols");

	VerifyStatus(m_debugClient->OpenDumpFile(dumpInfo->DumpPath),"OpenDumpFile");
	VerifyStatus(m_dbgControl->WaitForEvent(0,10000),"Wait expired on Dump file load");

	// Set the output callbacks appropriately
	OutputCallbacks g_OutputCb;
	m_debugClient->SetOutputCallbacks(&g_OutputCb);
	// Get the number of threads and assign it to our member variable
	VerifyStatus( m_dbgSysObj->GetNumberThreads(&m_numThreads),"GetNumThreads");

	return 0;
}

DebugWrapper::DebugWrapper()
{
	//Create DebugClient, and query interfaces
	VerifyStatus(DebugCreate(__uuidof(IDebugClient), (void **)&m_debugClient),"DebugCreate");
	// Why is thethread ID being set to 0?
	//m_dbgSysObj>SetCurrentThreadId(0);
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugSymbols3),(void **)&m_dbgSymbols),"QueryInterface DebugSymbols" );
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugDataSpaces),(void**)&m_dbgDataSpaces), "QueryInterface DebugDataSpaces");
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugRegisters),(void**)&m_dbgRegisters), "QueryInterface Debug Registers");
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugAdvanced2), (void **)&m_dbgAdv2),"Query DebugAdvanced2");
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugAdvanced), (void **)&m_dbgAdv), "Query DebugAdvanced");
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugRegisters2), (void **)&m_dbgRegisters2), "Query Debug Registers2");

	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugControl),(void **)&m_dbgControl),"QueryInterface DebugControl" );
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugControl4),(void **)&m_dbgControl4),"QueryInterface DebugControl 4" );
	VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugSystemObjects),(void **)&m_dbgSysObj),"QueryInterface SystemObjects" );

}

void DebugWrapper::RequeryInterfaces(ULONG i)
{
	//m_dbgSysObj->SetCurrentThreadId(i);

	//VerifyStatus( m_debugClient->QueryInterface(__uuidof(IDebugSymbols3),(void **)&m_dbgSymbols),"QueryInterface DebugSymbols" );
	//VerifyStatus( m_debugClient->QueryInterface(__uuidof(IDebugDataSpaces),(void**)&m_dbgDataSpaces), "QueryInterface DebugDataSpaces");
	//VerifyStatus( m_debugClient->QueryInterface(__uuidof(IDebugRegisters),(void**)&m_dbgRegisters), "QueryInterface Debug Registers");
	//VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugAdvanced2), (void **)&m_dbgAdv2),"Query DebugAdvanced2");
	//VerifyStatus(m_debugClient->QueryInterface(__uuidof(IDebugAdvanced), (void **)&m_dbgAdv), "Query DebugAdvanced");
}

void DebugWrapper::GetModuleBaseAddress(ULONG64 testAddr, ULONG64 *baseAddress, ULONG64 *moduleIndicator)
{

	ULONG numLoadedModules = 0;
	ULONG numUnloadedModules = 0;
	// Get the number of modules
	DebugSymbols3()->GetNumberModules(&numLoadedModules, &numUnloadedModules);

	for(ULONG k = 0; k < numLoadedModules;k++)
	{

		ULONG nameSize = 0;
		char moduleName[BUFFER_LENGTH_MODULE];
		DEBUG_MODULE_PARAMETERS moduleParameters;
		ULONG64 moduleBase;
		DebugSymbols3()->GetModuleByIndex(k, &moduleBase);

		DebugWrapper::VerifyStatus(DebugSymbols3()->GetModuleNameString(DEBUG_MODNAME_MODULE,k,NULL,moduleName,BUFFER_LENGTH_MODULE,&nameSize),"GetModuleNameString");
		DebugWrapper::VerifyStatus(DebugSymbols3()->GetModuleParameters(1,&moduleBase, NULL, &moduleParameters), "GetModuleParameters");

		// Let the caller know if it's a protected binary
		if(testAddr >= moduleParameters.Base && testAddr < (moduleParameters.Base+moduleParameters.Size))
		{
			*moduleIndicator = ResolveCurrentBinary(moduleName);
			*baseAddress = moduleBase;
			return;
		}
	}

	*baseAddress = 0;
	*moduleIndicator = -1;
	return;
}