#ifndef STACKPARSE_H
#define STACKPARSE_H

#include "arxdump.h"
#include "debugwrapper.h"

void parseStack(DebugWrapper *dw);
void parseStack(DebugWrapper *dw, ULONG64, ULONG64,ULONG64, ULONG64);
void findCorrespondingArxInstMapNodes(ULONG64, DWORD64, arxInstMapNode *&, arxInstMapNode *&);

#if 0
bool translateArxOffset(char ** arxMapFiles, ULONG64 off,int idx,char* out);
int arxInstMapize(DWORD64 currentIP, DWORD64 currentRsp, DWORD64 baseImageAddress, DebugWrapper *dw);
#endif

#endif STACKPARSE_H