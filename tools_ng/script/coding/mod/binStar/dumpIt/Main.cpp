#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "VictimProcess.h"
#include <iostream>

int main(int argc, char* argv[])
{
	if( argc < 3 )
	{
		std::cout<<"Not enough argsssss..."<<std::endl;
		getchar();
		exit(0);
	}
	LPSTR sDumpPath = "C:\\temp\\";

	if(argc == 5)
		sDumpPath = argv[4];
	VictimProcess vp = VictimProcess(argv[1],argv[2],sDumpPath);


	vp.RunProcess();
	Sleep(20000);
	vp.findProcess(vp.__sProcess);
	vp.dumpModule(argv[3]);
	vp.killPProcess();
}
