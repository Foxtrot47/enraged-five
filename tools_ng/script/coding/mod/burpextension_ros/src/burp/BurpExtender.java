package burp;

import java.awt.Component;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BurpExtender implements IBurpExtender, IMessageEditorTabFactory {

    private IBurpExtenderCallbacks callbacks;
    private IExtensionHelpers helpers;
    private IHttpRequestResponse currentRequest;
    String m_RosTitleName = "";
    String m_RosPlatformName = "";
    String m_RosVersion = "";
    String m_RosKey = "";
    String flag;
    String titleKey = "";
    String titleSalt = "";
    String myKey = "MTExMTExMTExMTExMTExMQ==";

    //
    // implement IBurpExtender
    //
    @Override
    public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {
        // keep a reference to our callbacks object
        this.callbacks = callbacks;

        // obtain an extension helpers object
        helpers = callbacks.getHelpers();

        // set our extension name
        callbacks.setExtensionName("R* SCS editor");

        // register ourselves as a message editor tab factory
        callbacks.registerMessageEditorTabFactory(this);
    }

    //
    // implement IMessageEditorTabFactory
    //
    @Override
    public IMessageEditorTab createNewInstance(IMessageEditorController controller, boolean editable) {
        // create a new instance of our custom editor tab
        return new SCSInputTab(controller, editable);
    }

    //
    // class implementing IMessageEditorTab
    //
    class SCSInputTab implements IMessageEditorTab {

        private boolean editable;
        private ITextEditor txtInput;
        private byte[] currentMessage;

        public SCSInputTab(IMessageEditorController controller, boolean editable) {
            this.editable = editable;

            // create an instance of Burp's text editor, to display our deserialized data
            txtInput = callbacks.createTextEditor();
            txtInput.setEditable(editable);
        }

        //
        // implement IMessageEditorTab
        //
        @Override
        public String getTabCaption() {
            return "R*";
        }

        @Override
        public Component getUiComponent() {
            return txtInput.getComponent();
        }

        @Override
        public boolean isEnabled(byte[] content, boolean isRequest) {
            // enable this tab for requests containing a data parameter
            //return isRequest && null != helpers.getRequestParameter(content, "data");
            return true;
        }

        @Override
        public void setMessage(byte[] content, boolean isRequest) {
           
            if (content == null) {
                // clear our display
                txtInput.setText(null);
                txtInput.setEditable(false);
            } else {
                if (isRequest = true) {
                    try {
                        IRequestInfo messageInfo = helpers.analyzeRequest(content);

                        
                        for (String header : messageInfo.getHeaders()) {
                            //System.out.println(header);
                            if (header.startsWith("User-Agent: ros")|| header.startsWith("USER-AGENT")) {
                                String ros = header.split("ros\\s")[1];
                                //System.out.println("Got ros header: "+ros);
                                byte[] decryptedRos = RosDecrypt(ros.getBytes());
                                byte[] titleKey = helpers.base64Decode(GetTitleKeyBase64());
                                //System.out.println("GotKey: "+ titleKey);
                                byte[] titleSalt = helpers.base64Decode(GetTitleSaltBase64());
                                //System.out.println("GotSalt: "+ titleSalt);

                                //Get the request body to decrypt
                                int bodyOffset = messageInfo.getBodyOffset();
                                byte[] data = Arrays.copyOfRange(content, bodyOffset, content.length);

                                byte[] crypt = decrypt(data, titleKey);
                                byte[] dataToEdit = Arrays.copyOfRange(crypt, 0, crypt.length - 20);

                                System.out.println("Decode: " + new String(crypt));
                                txtInput.setText(dataToEdit);
                                txtInput.setEditable(true);

                            }
                            
                        }
                    } catch (Exception exception) {
                        System.out.println(exception);
                    }
                    
                    //////////Response Junk
                     IResponseInfo responseInfo = helpers.analyzeResponse(content);
            
                    //System.out.println(rheader);
                    byte[] titleKey = helpers.base64Decode(GetTitleKeyBase64());
                    int bodyOffset = responseInfo.getBodyOffset();
                    byte[] data = Arrays.copyOfRange(content, bodyOffset, content.length);
                    byte[] crypt = decrypt(data, titleKey);
                    byte[] dataToEdit = Arrays.copyOfRange(crypt, 0, crypt.length - 20);
                    System.out.println("Decode: " + new String(crypt));
                    txtInput.setText(dataToEdit);
                
            //////Response Junk
                } else {
                    System.out.println("Not a request!");
                    IResponseInfo messageInfo = helpers.analyzeResponse(content);
                    int bodyOffset = messageInfo.getBodyOffset();
                    byte[] data = Arrays.copyOfRange(content, bodyOffset, content.length);


                    //RSCryptor cryptor = new RSCryptor();
                    //byte[] crypt = cryptor(titleKey.getBytes(), titleSalt.getBytes(), data, false);
                    byte[] titleKey = helpers.base64Decode(GetTitleKeyBase64());
                    byte[] crypt = decrypt(data, titleKey);
                    System.out.println("Decode: " + new String(crypt));
                    txtInput.setText(crypt);
                    txtInput.setEditable(true);
                }

            }

            // remember the displayed content
            currentMessage = content;
            System.out.println("Content " + new String(content));
        }

        @Override
        public byte[] getMessage() {
            // determine whether the user modified the deserialized data
            if (txtInput.isTextModified()) {
                // reserialize the data

                //Get the input text
                byte[] text = txtInput.getText();
                //String input = helpers.urlEncode(helpers.base64Encode(text));

                //Get the key and salt for re-encrypting
                byte[] titleKey = helpers.base64Decode(GetTitleKeyBase64());
                byte[] titleSalt = helpers.base64Decode(GetTitleSaltBase64());
                byte[] Key = helpers.base64Decode(myKey);
                System.out.println("titleKey: " + new String(titleKey));
                
                // Encrypt the new message
                byte[] edited = encrypt(titleKey,Key,text,titleSalt);
                // update the request with the new parameter value
                //
                //return helpers.updateParameter(currentMessage, helpers.buildParameter("data", input, IParameter.PARAM_BODY));
                System.out.println("CURRENTMESSAGE: " + currentMessage);
                return helpers.buildHttpMessage(helpers.analyzeRequest(currentMessage).getHeaders(), edited);
                
            } else {
                return currentMessage;
            }
        }

        @Override
        public boolean isModified() {
            return txtInput.isTextModified();
        }

        @Override
        public byte[] getSelectedData() {
            return txtInput.getSelectedText();
        }
    }

    //RosDecrypt
    byte[] RosDecrypt(byte[] header) {
        byte[] b_rosb64Decoded = helpers.base64Decode(header);

        //Get Key from b_rosb64Decoded (this is the first 4 bytes of the base64 "encrypted" message)
        byte[] b_rosKey = Arrays.copyOfRange(b_rosb64Decoded, 0, 4);
        //System.out.println("b_rosKey: "+new String(b_rosKey, "UTF-8")+"\r\n");

        //Get ros values from b_rosb64Decoded
        byte[] b_rosValue = Arrays.copyOfRange(b_rosb64Decoded, 4, b_rosb64Decoded.length);
        //System.out.println("b_rosValue: "+new String(b_rosValue, "UTF-8")+"\r\n");

        //"Decrypt" the Ros Value
        for (int i = 0; i < b_rosValue.length; i++) {
            byte[] cp_rosValue = b_rosValue;
            int iterator = i;
            cp_rosValue[iterator] ^= b_rosKey[i % b_rosKey.length];
        }
        //System.out.println("RosHeader.decode: "+b_rosValue.toString());
        String rosString = new String(b_rosValue);

        //Parse the decoded ros header to determine platform, title, etc.
        String[] walk = rosString.split(",");

        for (int j = 0; j < walk.length; j++) {
            String text = walk[j];

            if (text.startsWith("e=")) {
                this.flag = text.substring(2);
            } else {
                if (text.startsWith("t=")) {
                    this.m_RosTitleName = text.substring(2);
                } else {
                    if (text.startsWith("p=")) {
                        this.m_RosPlatformName = text.substring(2);
                    } else {
                        if (text.startsWith("v=")) {
                            this.m_RosVersion = text.substring(2);
                        }
                        else {
                            if (text.startsWith("k=")) {
                                this.m_RosKey = text.substring(2);
                            }
                        }
                    }
                }
            }
        }
        return b_rosValue;
    }

    //Get TitleKey
    private String GetTitleKeyBase64() {
        String result = "";
        if (this.m_RosTitleName.equals("samplesession")) {
            result = "FRV/HJVrvH4DRX8gfYc5rA==";
        } else {
            if (this.m_RosTitleName.equals("mp3")) {
                if (this.m_RosPlatformName.equals("pcros")) {
                    result = "EsX7vohcAaAj+pOGtS08ZA==";
                    return result;
                }
            } else {
                if (this.m_RosTitleName.equals("gta5")) {
                    if (this.m_RosPlatformName.equals("pcros")) {
                        result = "Q3sNY+YCLtRFpjdAsxcxTQ==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("xbox360")) {
                        result = "IJMn/L+OTdIRkYST0+LOCw==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("ps3")) {
                        result = "uhBv3xKE/fzCEZKDAmXijw==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("durango")) {
                        result = "097NOSxQuf/Tis6dLdLgKw==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("ps4")) {
                        result = "mAu+0JyV1it5cNiSGEMJRA==";
                        return result;
                    }
                }
            }
            //System.out.println("result1: "+result);
            result = "";
        }
        //System.out.println("returned result: "+result);
        this.titleKey = result;
        return result;

    }

    //Get titleSalt
    private String GetTitleSaltBase64() {
        String result;
        if (this.m_RosTitleName.equals("samplesession")) {
            result = "DtYSu9H7KfhrwIAgRGMNKA==";
        } else {
            if (this.m_RosTitleName.equals("mp3")) {
                if (this.m_RosPlatformName.equals("pcros")) {
                    result = "2e8BISHP5RWm6ayeHni7Jw==";
                    return result;
                }
            } else {
                if (this.m_RosTitleName.equals("gta5")) {
                    if (this.m_RosPlatformName.equals("pcros")) {
                        result = "zt+Z4Yd8SVQWhf4Bq2PVPw==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("xbox360")) {
                        result = "hKpQcWAPHoT+ub20mJ4dLA==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("ps3")) {
                        result = "hgsP+yJ+otIgnXKvSrGOjw==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("durango")) {
                        result = "kWUTy/HjyOicZU0jFjdEfA==";
                        return result;
                    }
                    if (this.m_RosPlatformName.equals("ps4")) {
                        result = "x0OaQC3GQ6EB1l//9IXWVw==";
                        return result;
                    }
                }
            }
            result = "";
        }
        this.titleSalt = result;
        return result;
    }

    public static byte[] getHash(byte[] message, byte[] salt){
        try {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update(message);
        digest.update(salt);
        return digest.digest();
        } catch (Exception e){
            System.out.println("getHash broke!");
            return null;
        }
        
    }
  
    public byte[] encrypt(byte[] titleKey, byte[] myKey, byte[] data, byte[] salt){
        //Encrypt
        byte[] e_Message = data;
        
		//blocksize seems to be a constant of 0x00 0x00 0x04 0x00
	//	byte[] e_blockSize;
        //e_blockSize = helpers.base64Decode("AAAEAA==");
                
		
		//Encrypt the message
        RC4 eRC = new RC4(myKey);
        byte[] encrypted = eRC.rc4(e_Message);
        
		//Xor the key used to encrypt the message with the title key
        if (m_RosKey != "") {
            byte[] rosKey = helpers.base64Decode(m_RosKey);
            //XOR the key

          for (int i = 0; i < 16; i++) {
                byte[] b_pK = myKey;
             int iterator = i;
             b_pK[iterator] ^= rosKey[i % rosKey.length];
          }
        }
        
	for (int i = 0; i < 16; i++) {
            byte[] b_pK = myKey;
            int iterator = i;
            b_pK[iterator] ^= titleKey[i % titleKey.length];
        }
		
		//Get the SHA-1 hash of the message and the salt
		byte[] toHash = new byte[myKey.length+encrypted.length];
		System.arraycopy(myKey, 0 , toHash, 0, myKey.length);
		System.arraycopy(encrypted, 0, toHash, myKey.length, encrypted.length);
		
		byte[] e_messageHash = getHash(toHash, salt);
                //System.out.println("Hash: "+new String(Base64.encodeBase64(e_messageHash)));
		
		//Prepend the XORed key to the encrypted message and return.
                byte[] finalMessage = new byte[myKey.length + encrypted.length + e_messageHash.length];
                System.arraycopy(myKey, 0, finalMessage, 0, myKey.length);
                System.arraycopy(encrypted, 0, finalMessage, myKey.length, encrypted.length);
		System.arraycopy(e_messageHash, 0, finalMessage, encrypted.length + myKey.length, e_messageHash.length);

		
        return finalMessage;
        
    }
    
    public byte[] decrypt(byte[] data, byte[] inKey) {
        //System.out.println("===================================\r\nDECRYPT\r\n===================================");
        byte[] messageKey = Arrays.copyOfRange(data,0,16);
        
        if (m_RosKey != "") {
            byte[] rosKey = helpers.base64Decode(m_RosKey);
            //XOR the key
        
            for (int i = 0; i < 16; i++) {
                byte[] b_pK = messageKey;
                int iterator = i;
                b_pK[iterator] ^= rosKey[i % rosKey.length];
            }
        }
        for (int i = 0; i < 16; i++) {
            byte[] b_pK = messageKey;
            int iterator = i;
            b_pK[iterator] ^= inKey[i % inKey.length];
        }
        //System.out.println("preambleKey2: "+new String(Base64.encodeBase64(preambleKey)));
        
        RC4 rC = new RC4(messageKey);
        byte[] todecrypt = Arrays.copyOfRange(data,16,data.length);
        byte[] decrypted_block = rC.rc4(todecrypt);
        return decrypted_block;
        
    }
}
