import subprocess, os
import sys, getopt

log = open('stringsReport.txt')
files = open('files.txt')
words = open('words.txt')

lnbrk = "\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n"
split = "\n\n-----------------------------------------------------------------\n\n"
header = "\n###########################################################\n"

#Consume cli args, initialize variable values
def init(argv):
	global files
	global words
	global log
	usage = 'checkBuildStrings.py -w <wordListFile> -o <outFile> [-f <binaryFile> or -i <inputListFile>]\n\t\t\t-h for Help dialogue'
	try:
		opts,args = getopt.getopt(argv,"hw:o:f:i:",["wordList=","fileList=","file=","outFile="])
	except getop.GetoptError:
		print usage
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print "\n*Requires two input files. A bad word list, and target binary\n"\
					"*Additionally, you could supply a text file of files to be scanned.\n"\
					"*Output is a report of any 'watch' words found, along with surronding lines\n"\
					"*All input list files must be return deliminated (one string per line).\n"\
					"*Report defaults to <filename>.report.txt, and log to stdout."+split
			print usage+'\n'
			sys.exit()
		elif opt in("-w","--wordList"):
			words = processInputFile(arg)
		elif opt in ("-i","--inputListFile"):
			files = processInputFile(arg)
		elif opt in ("-f","--file"):
			files = [arg]
		elif opt in ("-o","--outFile"):
			log = open(arg,'w')
		else:
			print usage
	if len(sys.argv)<2:
		print usage

	if log == '':
		log = open('stringsReport.txt','w')

#Consumes file name and outputs a list of strings
def processInputFile(f):
	fh = open(f,'r')
	outList = fh.read()
	if not outList:
		print "No files in input file, exiting"
		sys.exit(1)
	outList = outList.split('\n')
	fh.close()
	return outList

#Walks word list for given file, extracting any bad sections and logging them
def searchFile(fileName):
	fileName = fileName.rstrip()
	log.write(lnbrk+fileName+lnbrk)
	if strings(fileName) == None:
		print "Strings broke"
		return
	for word in words:
		word = word.rstrip('\n')
		output = grep(word)
		if output == None:
			continue
		log.write(header+"Search string: \t"+word+header)
		parseOutput(output)
	os.remove("x")

#Runs strings on given file, returns output 
def strings(fileName):
	cmd = "strings -n 4 "+fileName
	try:
		out = subprocess.check_output(cmd)
	except subprocess.CalledProcessError:
		return None
	fh = open("x",'w')
	fh.write(out)
	fh.close()
	return True

#Greps strings output, 
def grep(word):
	cmd = "grep -A 5 -B 5 -i -H --label=X '"+word+"' x"
	try:
		output = subprocess.check_output(cmd)
	except subprocess.CalledProcessError:
		return None
	return output

def parseOutput(output):
	strings = output.split("\n--\n")
	for row in strings:
		for line in row.split("\n"):
			if len(line)==0:
				continue
			elif line[1]=="-":
				line="   "+line[2:]
			elif line[1]==":":
				line="-> "+line[2:]

			log.write(line+'\n')
		log.write(split)

if __name__ == "__main__":
	init(sys.argv[1:])
	for f in files:
		searchFile(f)
	log.close()