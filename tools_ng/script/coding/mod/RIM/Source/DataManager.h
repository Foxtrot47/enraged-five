#ifndef DATAMANAGER_H
#define DATAMANGAGER_H

class DataManager
{
public:
	static DataManager& GetInstance()
	{
		static DataManager instance;
		return instance;
	}
	void Initialize();
private:
	DataManager() {};

};
#endif
