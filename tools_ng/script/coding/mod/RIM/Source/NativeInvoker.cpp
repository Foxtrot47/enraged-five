//NativeInvoker.cpp
#include "stdafx.h"

static NativeManagerContext g_context;
static UINT64 g_hash;
static std::mutex g_native_mutex;


void(*scrNativeCallContext::SetVectorResults)(scrNativeCallContext*) = nullptr;

void nativeInit(UINT64 hash) {
	g_native_mutex.lock();
	g_context.Reset();
	g_hash = hash;

}

void nativePush64(UINT64 value) {
	g_context.Push(value);
}

unsigned long long * nativeCall() {
	auto fn = Hooking::GetNativeHandler(g_hash);

	if (fn != 0) {

		fn(&g_context);
		scrNativeCallContext::SetVectorResults(&g_context);
		
	}
	else
	{
		DEBUGMSG("[RIM] Unable to find a registered native function for hash %x!!!", g_hash);
	}

	void* pReturn = g_context.GetResultPointer();
	g_native_mutex.unlock();
	return reinterpret_cast<unsigned long long*>(pReturn);
}

