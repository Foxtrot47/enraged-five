#pragma once

#define GAME_WINDOW_NAME L"Game"
BOOL CALLBACK ProcessWindow(HWND hWnd, LPARAM lParam);
void ProcessWindows(LPARAM lParam);

class Hooking
{
public:
	static void Start(HMODULE hmoduleDLL);
	static void Cleanup();
	static eGameState GetGameState();
	static BlipList* GetBlipList();
	static uint64_t getWorldPtr();
	static void onTickInit();
	static bool HookNatives();
	static HWND getWindow() { return hWindow; }

	struct RageSecObject
	{
		int type;
		unsigned int id;
		unsigned int version;
		unsigned int address;
		unsigned int data;
	};
	typedef char(__fastcall * RageSecPush)(void * _this, RageSecObject * reaction);

	// Native function handler type
	typedef void(__cdecl * NativeHandler)(scrNativeCallContext * context);
	struct NativeRegistration
	{
		NativeRegistration * nextRegistration;
		Hooking::NativeHandler handlers[7];
		uint32_t numEntries;
		uint64_t hashes[7];
	};	
	static NativeHandler GetNativeHandler(uint64_t origHash);
	static bool FindGameWindow();
private:
	static BOOL InitializeHooks();
	static void FindPatterns();
	static void FailPatterns(const char* name, Memory::pattern ptn);
	static HWND hWindow;
};	void WAIT(DWORD ms);

