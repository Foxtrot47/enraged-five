#ifndef __DX11_H__
#define __DX11_H__

#pragma once

class DX11Hook
{
private:

	ID3D11Device*			 DX11Device = NULL;
	ID3D11DeviceContext*	 DX11DeviceContext = NULL;

	IDXGISwapChain*			 DX11SwapChain = NULL;
	DWORD_PTR*				 SwapChainVtable = NULL;
	ID3D11RenderTargetView*  DX11RenderTargetView = NULL;

	BOOLEAN					 Initialized = FALSE;

public:

	DX11Hook() {}

	static float ScrX, ScrY;
	static float ScrWidth, ScrHeight;

	DWORD InitDevice(HWND hWindow);

	BOOLEAN InitDX11RenderStuff(IDXGISwapChain* _SwapChain);

	void CreateRenderTarget();

	void DX11RenderScene();

	void ResizeBuffers(UINT width, UINT height);

	void CleanupRenderTarget();

	void CleanupDeviceD3D();

};
using namespace std;
extern std::unique_ptr<DX11Hook> DX11;

#endif // __DX11_H__