#ifndef __CROSS_MAPPING_H__
#define __CROSS_MAPPING_H__

#pragma once
#include "stdafx.h"

typedef std::unordered_map<unsigned long long, unsigned long long> nMap;
static std::vector<unsigned long long> nativeFailedVec;

struct CrossMappingEntry
{
	unsigned long long first;
	unsigned long long second;
};

class CrossMapping
{
public:
	static void initNativeMap();
	static unsigned long long MapNative(unsigned long long inNative);
	static void dumpNativeMappingCache();
	static bool searchMap(nMap map, unsigned long long inNative, unsigned long long *outNative);
};

#endif // __CROSS_MAPPING_H__


