#include "stdafx.h"

using namespace ImGui;

/* COLORS */
ImVec4 clear_col = ImColor(114, 144, 154);

/* BOOLEANS */
static bool is_menu_open = false;
static bool show_test_window = false;
static bool show_another_window = false;

static int selected_player = -1;

BOOLEAN Menu::Init()
{
	return TRUE;
}

void Menu::Render()
{
	// start draw
	ImGui_ImplDX11_NewFrame();

	static bool no_titlebar = false;
	static bool no_border = true;
	static bool no_resize = false;
	static bool auto_resize = false;
	static bool no_move = false;
	static bool no_scrollbar = false;
	static bool no_collapse = false;
	static bool no_menu = true;
	static bool start_pos_set = false;

	static float teleportPos[3] = { 1000.00, 1000.00, 1000.00 };

	// various window flags. Typically you would just use the default.
	ImGuiWindowFlags	window_flags = 0;
	if (no_titlebar)	window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (!no_border)		window_flags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)		window_flags |= ImGuiWindowFlags_NoResize;
	if (auto_resize)	window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
	if (no_move)		window_flags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar)	window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)	window_flags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)		window_flags |= ImGuiWindowFlags_MenuBar;
	ImGui::SetNextWindowSize(ImVec2(350, 380), ImGuiSetCond_FirstUseEver);
	if (!start_pos_set) { ImGui::SetNextWindowPos(ImVec2(10, 10)); start_pos_set = true; }

	if (trainer_switch_pressed()) is_menu_open = !is_menu_open;
	if (demo_switch_pressed()) show_test_window = !show_test_window;

	ImGui::GetIO().MouseDrawCursor = true;
	
	if (is_menu_open)
	{
		ImGui::Begin("RIM DRD11", &is_menu_open, window_flags);

		//ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.65f);    // 2/3 of the space for widget and 1/3 for labels
		ImGui::PushItemWidth(-140);                                 // Right align, keep 140 pixels for labels

		ImGui::TextColored(ImColor(0, 128, 255), "Rockstar Internal Menu - RDR3"); 

		if (CollapsingHeader("Player Options"))
		{
			Checkbox("Godmode", &GodmodeEnabled);
			Checkbox("Superjump", &SuperjumpEnabled);
			Separator();
			Checkbox("XP Loop (Headshot, Award)", &Rankloop);
			Checkbox("Money Loop (Cents, Loot)", &Moneyloop);
		}

		if (CollapsingHeader("Weapon Options"))
		{
			if (Button("Give all weapons"))
			{
				Ped playerPed = PLAYER::PLAYER_PED_ID();
				for (int i = 0; i < sizeof(weaponNames) / sizeof(weaponNames[0]); i++)
				{
					WEAPON::GIVE_DELAYED_WEAPON_TO_PED(playerPed, $((char *)weaponNames[i]), 100, false); // Missing addReason
				}
				OpenPopup("added");
				if (BeginPopup("added"))
				{
					Text("All weapons have been given to you.");
					EndPopup();
				}
			}
		}

		if (CollapsingHeader("World Options"))
		{
			Text("Time Options");
			Separator();
			if (SmallButton("Day"))
				NETWORK::NETWORK_OVERRIDE_CLOCK_TIME(12, 0, 0);
			SameLine();
			if (SmallButton("Night"))
				NETWORK::NETWORK_OVERRIDE_CLOCK_TIME(0, 0, 0);
			Checkbox("Always Day", &AlwaysDay);
			Checkbox("Always Night", &AlwaysNight);
		}

		if (CollapsingHeader("Teleport to..."))
		{
			InputFloat3("Position", teleportPos);
			if (Button("Teleport!"))
			{
				teleportTo[0] = teleportPos[0];
				teleportTo[1] = teleportPos[1];
				teleportTo[2] = teleportPos[2];
				doTeleport = true;
			}
		}

		bool selection[32];
		if (CollapsingHeader("Online Players"))
		{
			
			Text("Online Players: %i", NETWORK::NETWORK_GET_NUM_CONNECTED_PLAYERS());
			ImGui::BeginChild("Sub1", ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, 200), false);
			PushItemWidth(-1);
			ListBoxHeader("##empty");
			for (int i = 0; i < NETWORK::NETWORK_GET_NUM_CONNECTED_PLAYERS(); i++)
			{
				if (Selectable(PLAYER::GET_PLAYER_NAME(i) ? PLAYER::GET_PLAYER_NAME(i) : "*** Invalid ***", selection[i]))
					selected_player = i;
			}
			ListBoxFooter();
			EndChild(); SameLine();
			ImGui::BeginChild("Sub2", ImVec2(0, 200), false);
			Text("Actions:");
			if (selected_player != -1)
			{
				Player playerIndex = PLAYER::INT_TO_PLAYERINDEX(selected_player);
				const char* playerName = PLAYER::GET_PLAYER_NAME(playerIndex) ? PLAYER::GET_PLAYER_NAME(playerIndex) : "*** INVALID ***";
				Entity playerEnt = PLAYER::GET_PLAYER_PED(playerIndex);
				Vector3 entCoords = ENTITY::GET_ENTITY_COORDS(playerEnt, true);
				if (Button("Teleport to player"))
				{
					teleportTo[0] = entCoords.x;
					teleportTo[1] = entCoords.y;
					teleportTo[2] = entCoords.z;
					doTeleport = true;
				}

				// Possible remote features go here...
			}
			EndChild();

			if (selected_player != -1)
				ShowPlayerInfo(selected_player);

		}

		if (CollapsingHeader("Nearby Ped Actions..."))
		{
			if (Button("Kill nearby peds"))
				kill_pedestrians();
			if (Button("Clear nearby peds"))
				clear_pedestrians();
			if (Button("Clear cops"))
				clear_cop_pedestrians();
			if (Button("Clear vehicles"))
				clear_pedestrians_vehicles();
		}

		//Text("Player ped id: %i", PLAYER::PLAYER_PED_ID());
		ImGui::End();
	}
	
	// Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (show_test_window)
	{
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
		ImGui::ShowTestWindow(&show_test_window);
	}

	ImGui::Render();


	IDXGIAdapter * pAdapter;
	std::vector <IDXGIAdapter*> vAdapters;
	IDXGIFactory* pFactory = NULL;

	// Create a DXGIFactory object.
	if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory)))
	{

	}

	static bool printOnce = false;

	if(!printOnce)
	{
		for (UINT i = 0;
			pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND;
			++i)
		{
			vAdapters.push_back(pAdapter);
			Log::Msg("Adapter %d at 0x%I64X", i, pAdapter);
			Log::Msg("Adapter %d at 0x%I64X", i, &pAdapter);
		}
		if (pFactory)
		{
			pFactory->Release();
		}
		printOnce = true;
	}
}

void Menu::ShowPlayerInfo(int index)
{
	ImVec2 parentPos = ImGui::GetWindowPos();
	ImVec2 parentSize = ImGui::GetWindowSize();
	ImVec2 newPos = ImVec2(parentPos.x + parentSize.x + 20, parentPos.y);
	ImGui::SetNextWindowPos(newPos);
	if (!ImGui::Begin("PlayerInfo", false, ImVec2(0, 0), 0.3f, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings))
	{
		ImGui::End();
		return;
	}
	Player playerIndex = PLAYER::INT_TO_PLAYERINDEX(index);
	const char* playerName = PLAYER::GET_PLAYER_NAME(playerIndex) ? PLAYER::GET_PLAYER_NAME(playerIndex) : "*** INVALID ***";
	Entity playerEnt = PLAYER::GET_PLAYER_PED(playerIndex);
	Vector3 entCoords = ENTITY::GET_ENTITY_COORDS(playerEnt, true);
	ImGui::Text("Player Info: %s", playerName);
	ImGui::Separator();
	ImGui::Text("Position: %f,%f,%f", entCoords.x, entCoords.y, entCoords.z);
	ImGui::Text("Health: %i Godmode: %d", ENTITY::GET_ENTITY_HEALTH(playerEnt), PLAYER::GET_PLAYER_INVINCIBLE(index));
	ImGui::End();

}

bool Menu::IsOpen()
{
	return is_menu_open;
}
