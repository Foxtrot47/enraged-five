//InputHook.cpp
#include "stdafx.h"

#define GXLPARAM(lp) ((short)LOWORD(lp))
#define GYLPARAM(lp) ((short)HIWORD(lp))

InputHook iHook;
WNDPROC	oWndProc;

//using namespace Utility;

static std::set<InputHook::TKeyboardFn> g_keyboardFunctions;

bool InputHook::FindGameWindow()
{
	ProcessWindows((LPARAM)hWindow);
	if (hWindow != NULL)
		return true;
	else
		return false;
}
void InputHook::keyboardHandlerRegister(TKeyboardFn function) {

	g_keyboardFunctions.insert(function);
}

void InputHook::keyboardHandlerUnregister(TKeyboardFn function) {

	g_keyboardFunctions.erase(function);
}

bool InputHook::Initialize() {

	hWindow = NULL;
	if (!FindGameWindow())
		return false;
	oWndProc = (WNDPROC)SetWindowLongPtr(hWindow, GWLP_WNDPROC, (__int3264)(LONG_PTR)WndProc);
	if (oWndProc == NULL) {

		Log::Error("Failed to attach input hook");
		return false;
	}
	else {

		keyboardHandlerRegister(OnKeyboardMessage);
		DEBUGMSG("Input hook attached:  WndProc 0x%p", (DWORD_PTR)oWndProc);
		return true;
	}
}

void InputHook::Remove() {

	SetWindowLongPtr(hWindow, GWLP_WNDPROC, (LONG_PTR)oWndProc);
	DEBUGMSG("Removed input hook");
}

using namespace ImGui;
LRESULT APIENTRY WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_LBUTTONDOWN:
		if (Menu::IsOpen()) { GetIO().MouseDown[0] = true; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_LBUTTONUP:
		if (Menu::IsOpen()) { GetIO().MouseDown[0] = false; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_RBUTTONDOWN:
		if (Menu::IsOpen()) { GetIO().MouseDown[1] = true; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_RBUTTONUP:
		if (Menu::IsOpen()) { GetIO().MouseDown[1] = false; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_MBUTTONDOWN:
		if (Menu::IsOpen()) { GetIO().MouseDown[2] = true; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_MBUTTONUP:
		if (Menu::IsOpen()) { GetIO().MouseDown[2] = false; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_MOUSEWHEEL:
		if (Menu::IsOpen()) { GetIO().MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f; return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_MOUSEMOVE:
		if (Menu::IsOpen()) { GetIO().MousePos.x = (signed short)(lParam); GetIO().MousePos.y = (signed short)(lParam >> 16); return DefWindowProc(hwnd, uMsg, wParam, lParam); }
		break;
	case WM_SIZE:
		break;
	case WM_KEYDOWN: case WM_KEYUP: case WM_SYSKEYDOWN: case WM_SYSKEYUP:
	{
		auto functions = g_keyboardFunctions; for (auto & function : functions) function((DWORD)wParam, lParam & 0xFFFF, (lParam >> 16) & 0xFF, (lParam >> 24) & 1, (uMsg == WM_SYSKEYDOWN || uMsg == WM_SYSKEYUP), (lParam >> 30) & 1, (uMsg == WM_SYSKEYUP || uMsg == WM_KEYUP));
		if (Menu::IsOpen() && uMsg == WM_KEYUP) { GetIO().KeysDown[wParam] = 0; return true; }
		if (Menu::IsOpen() && uMsg == WM_KEYDOWN) { GetIO().KeysDown[wParam] = 1; return true; }
	}
		break;
	case WM_CHAR:
		if (Menu::IsOpen()) { if (wParam > 0 && wParam < 0x10000) GetIO().AddInputCharacter((unsigned short)wParam); return true; }
		break;
	}

	return CallWindowProc(oWndProc, hwnd, uMsg, wParam, lParam);

}
