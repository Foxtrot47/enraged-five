#pragma once

namespace Menu
{
	BOOLEAN Init();
	void Render();
	void ShowPlayerInfo(Player plr);
	bool IsOpen();
};


