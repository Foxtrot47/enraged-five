#include "stdafx.h"
#include "ScriptEvents.h"

int ScriptEvents::Get_Args_Size_From_Data(int sizeOfType)
{
	return sizeOfType / 8;
}

void ScriptEvents::MakePlayerDoAmbientScript(Player player, MPMissionID missionId)
{
	int bits = 0;
	GAMEPLAY::SET_BIT(&bits, player);
	Event_Ambient_Script args = {};
	args.missionId = missionId;
	args.common.iFromPlayer = player; // this is still wrong
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&args, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Ambient_Script)), bits);
}

void ScriptEvents::GivePlayerRP(Player player)
{
	int targetBits = 0;
	GAMEPLAY::SET_BIT(&targetBits, player);
	Event_Give_XP args = { { SCRIPT_EVENT_GIVE_PLAYER_XP, player }, 3075600, 0x22138701, 0x01 };
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&args, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Give_XP)), targetBits);
}

void ScriptEvents::GivePlayerMoneyMethod2(Player player, int money)
{
	int targetBits = 0;
	GAMEPLAY::SET_BIT(&targetBits, player);
	Event_BossVBoss_DM_Reply args = {};
	args.common = { SCRIPT_EVENT_BOSSVBOSS_DM_REPLY, 0xFFFFFFFF };
	args.cashAmount = money;
	args.unk1 = 0;
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&args, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_BossVBoss_DM_Reply)), targetBits);
}

void ScriptEvents::SetPlayerRP(Player player, int rpValue)
{
	int targetBits = 0;
	GAMEPLAY::SET_BIT(&targetBits, player);
	Event_Alter_Non_Local_Stat args = {};
	args.common = { SCRIPT_EVENT_ALTER_NON_LOCAL_PLAYER_STAT, player };
	args.playerIndex = 0;
	//args.iMpIntReward = 0x59;
	//args.iMpBoolReward = 0x24;
	//args.iMpFloatReward = 0x01;
	args.iMpIntStats = 0x756;
	//args.iMpBoolStats = 0x12E;
	//args.iMpFloatStats = 0xE5;
	args.iNewIntValue = rpValue;
	//args.iNewFloatValue = 7;
	//args.iNewBoolValue = 0;
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&args, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Alter_Non_Local_Stat)), targetBits);
}

char* ScriptEvents::PromptPlayerFor(char* thing)
{
	Ped playerPed = PLAYER::PLAYER_PED_ID();
	GAMEPLAY::DISPLAY_ONSCREEN_KEYBOARD(true, thing, "", "1", "", "", "", 1000);

	// Wait for submissions...
	while (GAMEPLAY::UPDATE_ONSCREEN_KEYBOARD() == 0)
	{
		WAIT(0);
	}
	if (!GAMEPLAY::GET_ONSCREEN_KEYBOARD_RESULT()) return "";

	return GAMEPLAY::GET_ONSCREEN_KEYBOARD_RESULT();
}

void ScriptEvents::GivePlayerMoney(Player player, int money)
{
	int targetBits = 0;
	GAMEPLAY::SET_BIT(&targetBits, player);
	Event_Ticker_Event tEvent = {};
	tEvent.common = { SCRIPT_EVENT_TICKER_MESSAGE, player };
	tEvent.TickerEvent = TICKER_EVENT_FINDERSKEEPERS_COLLECTED_ALL_PACKAGES;
	tEvent.TickerData = money;
	tEvent.iEntityNum = player;

	// Hardcoded bitset
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&tEvent, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Ticker_Event)), targetBits);
}

void ScriptEvents::GivePlayerMoneyMethod3(Player plrIndex, int money)
{
	int bits = 0;
	GAMEPLAY::SET_BIT(&bits, plrIndex);
	Event_Paid_Goon_Cash sEvent = {};
	sEvent.moneyAmount = money;

	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&sEvent, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Paid_Goon_Cash)), bits);
}

void ScriptEvents::SendPlayerTo(Player plrIndex, int place)
{
	int bits = 0;
	GAMEPLAY::SET_BIT(&bits, plrIndex);
	Event_Async_Grid_Warp warp = {};
	warp.locationFromEvent = place;
	warp.common.iFromPlayer = plrIndex; // Force does this

	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&warp, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Async_Grid_Warp)), bits);
}

void ScriptEvents::SendPlayerHospitalBill(Player plrIndex, int iBillAmount)
{
	int targetBits = 0; // I really should make this a function...
	GAMEPLAY::SET_BIT(&targetBits, plrIndex);
	Event_Pay_Hospital_Bills tEvent = {};
	tEvent.common = { SCRIPT_EVENT_PAY_PLAYERS_HOSPITAL_BILLS, 0xFFFFFFFF }; // Force Elite sets sender to FFFFFFFF...
	tEvent.iHospitalBill = iBillAmount;
	tEvent.bPlayerInPassiveMode = false;

	//Send the thing
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&tEvent, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Pay_Hospital_Bills)), targetBits);
}

void ScriptEvents::SetBountyOnPlayer(Player plrIndex, int bounty)
{
	// Force elite iterates through the player list and calls this 1 times for each player. (We're doing this)
	// Technically, they should of just set all the bits and made one call but /WHATEVER/ 
	// (They're doing it wrong)

	Event_Set_Bounty bountyEvt = {};
	bountyEvt.common = { SCRIPT_EVENT_SET_BOUNTY_ON_PLAYER, plrIndex };
	bountyEvt.playerId = plrIndex;
	bountyEvt.iResultBS = 1;
	bountyEvt.iBountyValue = bounty;

	// There's likely a better way of doing this. Just sayin'
	for (int i = 0; i < 32; i++)
	{
		if (ENTITY::DOES_ENTITY_EXIST(PLAYER::GET_PLAYER_PED(i)))
		{
			int targetBits = 0;
			GAMEPLAY::SET_BIT(&targetBits, i);
			SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&bountyEvt, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Set_Bounty)), targetBits);
		}
	}
}

void ScriptEvents::DoPlayerContraband(Player plrIndex)
{
	int targetBits = 0;
	GAMEPLAY::SET_BIT(&targetBits, plrIndex);

	Event_Contraband_Destroyed evt = {};
	evt.playerOwner = plrIndex;
	evt.playerDestroyer = plrIndex;

	// Padding all zeros
	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any *)&evt, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Contraband_Destroyed)), targetBits);

}

void ScriptEvents::DoCEOCash(Player plrIndex, int amount)
{
	int bits = 0;
	GAMEPLAY::SET_BIT(&bits, plrIndex);

	Event_Deliver_Vehicle veh = {};
	veh.playerDoingDelivery = plrIndex;
	veh.playerOriginalOwner = plrIndex;
	veh.vehicleModel = 0x9CFFFC56; // Not sure which model this is...
	veh.iDeliveryRewardValue = amount;
	veh.eDropoffID = 0x15;
	veh.eIEVehicle = 0x20;

	SCRIPT::TRIGGER_SCRIPT_EVENT(1, (Any*)&veh, ScriptEvents::Get_Args_Size_From_Data(sizeof(Event_Deliver_Vehicle)), bits);
}
