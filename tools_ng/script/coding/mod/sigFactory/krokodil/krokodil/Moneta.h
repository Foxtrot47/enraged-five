#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>


//Helper functions

//Print DWORD in MsgBox
void msgbxAddr(DWORD64 addy)
{
	char buff[1024];
	sprintf(buff, "Addy %02p", addy);
	MessageBox(NULL, buff, "wat...", MB_OK);
}

//Write to log file
void writeLog(char* szString, char * logFile, char* concat=NULL)
{
	char out[1024];
	sprintf(out,szString,concat);
	FILE* pFile = fopen(logFile,"a");
	fprintf(pFile, "%s\n", out);
	fclose(pFile);
}



//RTM functions

//Walks pages of process space and dumps memory to given binary file
DWORD64 memDump(LPVOID addr, MEMORY_BASIC_INFORMATION mbi, FILE* fPtr)
{
		DWORD64 memoryCount = 0;

		while(VirtualQuery(addr, &mbi, sizeof(MEMORY_BASIC_INFORMATION)) == sizeof(MEMORY_BASIC_INFORMATION))
		{
			BYTE* p = (BYTE *)mbi.BaseAddress;

			if (mbi.State == MEM_COMMIT )
			{
				DWORD oldProt;
				memoryCount += (DWORD64)mbi.RegionSize;

				VirtualProtect(addr, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &oldProt);
				fwrite(p, mbi.RegionSize, 1, fPtr);
				VirtualProtect(addr, mbi.RegionSize, oldProt, 0);
			}
			addr = p + mbi.RegionSize;
		}

		return memoryCount;
}


//Places a jump at 'Address' that jumps to address'jumpTo'
void jmpIt(BYTE *Address, DWORD64 jumpTo, DWORD64 length=9)
{
	DWORD dwOldProt, dwBkup;
	DWORD64 dwRelAddr;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	dwRelAddr = (DWORD64)(jumpTo - (DWORD64)Address) -9;

	*Address = 0xE9;

	*((DWORD64 *)(Address + 0x1)) = dwRelAddr;

	for(DWORD64 x=0x9; x<length; x++)
		*(Address + x) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}

//Writes a nop slide of 'length' starting at 'Address'
void nopSlide(BYTE *Address, DWORD64 length=2)
{
	DWORD dwOldProt, dwBkup;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	for(DWORD64 i=0; i<length; i++)
		*(Address + i) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}


//Get's MODULEINFO construct for a given Module name 'szModule'
MODULEINFO GetModInfo(char * szModule)
{
	MODULEINFO modInfo = {0};
	HMODULE hModule = GetModuleHandle(szModule);

	if(hModule==0)
		return modInfo;

	GetModuleInformation(GetCurrentProcess(), hModule, &modInfo, sizeof(MODULEINFO));

	return modInfo;
}

//Scans memory for a pattern specified by 'pattern'. 'mask' length is the same as 'pattern', of arbitrary characters. Any '?' in 'mask' identifies a wildcard at the same position in 'pattern'.
DWORD64 findPattern(char *module, char *pattern, char *mask)
{
	MODULEINFO mInfo = GetModInfo(module);

	DWORD64 base = (DWORD64)mInfo.lpBaseOfDll;
	DWORD64 size = (DWORD64)mInfo.SizeOfImage;

	DWORD64 patternLength = (DWORD64)strlen(mask);

	for(DWORD64 i=0; i < size-patternLength; i++)
	{
		bool found = true;

		for (DWORD64 j=0; j < patternLength; j++)
		{
			found &= mask[j] == '?' || pattern[j] == *(char*)(base+i+j);
		}

		if(found)
			return base+i;
	}
	return 0;
	
}