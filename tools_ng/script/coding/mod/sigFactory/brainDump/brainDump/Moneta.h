#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <queue>
#include "time.h"

#define DUMP_DIR "C:\\dumps"

//Struct containing MBI as well as int for order
struct memPage
{
	int order;
	MEMORY_BASIC_INFORMATION mbi;
};



template <class T>
class threadSafeQueue: public std::queue<T>
{
public:
	memPage getPage();
	threadSafeQueue() : std::queue<T>()	{mutex = false;}
	void tsPush(T);
	void flip();
	T tsPop();
protected:
	bool mutex;
};


class Moneta
{
	char dumpFile[MAX_PATH];
	char logFile[MAX_PATH];
	
	DWORD64 threadAction(void*,DWORD64);
	DWORD64 pauseThreads(HANDLE*);
	
	bool resumeThreads(HANDLE*,DWORD64);
	bool writeDump(BYTE*,SIZE_T);

	void msgbxAddr(DWORD64);
	void initLog(time_t);
	void initDump(time_t);
	void dumpFolder();	

protected:
	DWORD64 findPattern();
	DWORD64 dumpWalk(LPVOID);
	DWORD64 findPattern(char*, char*, char*);
	
	void jmpIt(BYTE*, DWORD64, DWORD64);
	void nopSlide(BYTE*, DWORD64);
	
	MODULEINFO GetModInfo(char*);	
	
public:
	//Write to log file
	void writeLog(char* szString, char* concat=NULL)
	{
		char out[1024];
		sprintf(out,szString,concat);
		FILE* pFile = fopen(logFile,"a");
		fprintf(pFile, "%s\n", out);
		fclose(pFile);
	}
	//void writeLog(char* szString, DWORD64 concat)
	//{
	//	char out[1024];
	//	sprintf(out,szString,concat);
	//	FILE* pFile = fopen(logFile,"a");
	//	fprintf(pFile, "%s\n", out);
	//	fclose(pFile);
	//}
	DWORD WINAPI initThread(LPVOID);
	DWORD64 Start(LPVOID);
};


///////////////Moneta Methods

////RTM functions

//Walks pages of process space and dumps memory
DWORD64 Moneta::dumpWalk(LPVOID addr)
{
		DWORD64 memoryCount = 0;
		DWORD64 pageCount = 0;
		DWORD64 failedPages = 0;

		MEMORY_BASIC_INFORMATION mbi;
		memset(&mbi, 0, sizeof(MEMORY_BASIC_INFORMATION));
		BYTE* p;
		
		writeLog("Beginning to dump process memory...");
		while(VirtualQuery(addr, &mbi, sizeof(MEMORY_BASIC_INFORMATION)) == sizeof(MEMORY_BASIC_INFORMATION))
		{
			p = (BYTE *)mbi.BaseAddress;
			pageCount++;

			if (mbi.State == MEM_COMMIT )//&& (mbi.Type == MEM_IMAGE || mbi.Type == MEM_MAPPED)
			{
				//DWORD oldProt;

				/*if(!VirtualProtect(addr, mbi.RegionSize, PAGE_READONLY, &oldProt))
				{
					char m[100];
					sprintf(m,"%x | Error: %d",addr, GetLastError());
					writeLog("Could not set RWX perms at address: %s",m);
					addr = p + mbi.RegionSize;
					continue;
				}*/

				if(!writeDump(p,mbi.RegionSize))
				{
					failedPages++;
					//return -1;
				}
				memoryCount += (DWORD64)mbi.RegionSize;

				//if(!VirtualProtect(addr, mbi.RegionSize, oldProt, 0))
				//{
				//	char m[100];
				//	sprintf(m,"%x | Error: %d",addr, GetLastError());
				//	writeLog("Could not set old protections at address: %s",m);					
				//}
			}
			addr = p + mbi.RegionSize;
		}
		char m[100];
		sprintf(m,"%d KB written to disk",(memoryCount/1000));
		writeLog("Completed dump, %s",m);
		memset(m,0,100);
		sprintf(m,"Total Number of Memory Pages: %d | Number of Failed Page Dumps: %d",pageCount,failedPages);
		writeLog(m);
		return memoryCount;
}

//Places a jump at 'Address' that jumps to address'jumpTo'
void Moneta::jmpIt(BYTE *Address, DWORD64 jumpTo, DWORD64 length=9)
{
	DWORD dwOldProt, dwBkup;
	DWORD64 dwRelAddr;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	dwRelAddr = (DWORD64)(jumpTo - (DWORD64)Address) -9;

	*Address = 0xE9;

	*((DWORD64 *)(Address + 0x1)) = dwRelAddr;

	for(DWORD64 x=0x9; x<length; x++)
		*(Address + x) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}

//Writes a nop slide of 'length' starting at 'Address'
void Moneta::nopSlide(BYTE *Address, DWORD64 length=2)
{
	DWORD dwOldProt, dwBkup;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	for(DWORD64 i=0; i<length; i++)
		*(Address + i) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}

//Get's MODULEINFO construct for a given Module name 'szModule'
MODULEINFO Moneta::GetModInfo(char * szModule)
{
	MODULEINFO modInfo = {0};
	HMODULE hModule = GetModuleHandle(szModule);

	if(hModule==0)
		return modInfo;

	GetModuleInformation(GetCurrentProcess(), hModule, &modInfo, sizeof(MODULEINFO));

	return modInfo;
}

//Scans memory for a pattern specified by 'pattern'. 'mask' length is the same as 'pattern', of arbitrary characters. Any '?' in 'mask' identifies a wildcard at the same position in 'pattern'.
DWORD64 Moneta::findPattern(char *module, char *pattern, char *mask)
{
	MODULEINFO mInfo = GetModInfo(module);

	DWORD64 base = (DWORD64)mInfo.lpBaseOfDll;
	DWORD64 size = (DWORD64)mInfo.SizeOfImage;

	DWORD64 patternLength = (DWORD64)strlen(mask);

	for(DWORD64 i=0; i < size-patternLength; i++)
	{
		bool found = true;

		for (DWORD64 j=0; j < patternLength; j++)
		{
			found &= mask[j] == '?' || pattern[j] == *(char*)(base+i+j);
		}

		if(found)
			return base+i;
	}
	return 0;
	
}


////Memory dump 

//Print DWORD in MsgBox
void Moneta::msgbxAddr(DWORD64 addy)
{
	char buff[1024];
	sprintf(buff, "Addy %02p", addy);
	MessageBox(NULL, buff, "wat...", MB_OK);
}

//Checks for dump folder, and creates if doesn't exist
void Moneta::dumpFolder()
{
	DWORD dwAttrib = GetFileAttributes(DUMP_DIR);

	if( dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY))
		return;
	else
	{
		CreateDirectory(DUMP_DIR,NULL);
	}

	return;
}

//Init logFile
void Moneta::initLog(time_t ti)
{
	char path[MAX_PATH];
	char file[_MAX_FNAME];
	
	dumpFolder();
	GetModuleFileName(NULL,path,MAX_PATH);
	_splitpath(path,NULL,NULL,file,NULL);
	sprintf(logFile,"%s\\%d.%s.brainDump.log.txt",DUMP_DIR,ti,file);
	remove(logFile);
	FILE * fPtr = fopen(logFile,"w");
	rewind(fPtr);
	fclose(fPtr);
	return;
}

//Initiates file to be dumped to
void Moneta::initDump(time_t ti)
{
	char path[MAX_PATH];
	char file[_MAX_FNAME];
	
	dumpFolder();
	GetModuleFileName(NULL,path,MAX_PATH);
	_splitpath(path,NULL,NULL,file,NULL);
	sprintf(dumpFile,"%s\\%d.%s.brainDump.bin",DUMP_DIR,ti,file);
	remove(dumpFile);
	FILE* fPtr = fopen(dumpFile, "wb");
	rewind(fPtr);
	fclose(fPtr);
	return;
}

//Appends memory chunk to file
bool Moneta::writeDump(BYTE* p,SIZE_T sz)
{
	FILE* fPtr = fopen(dumpFile, "ab");
	DWORD64 ret = fwrite(p,sz,1,fPtr);
	fclose(fPtr);

	if(ret < 1)
		return false;
	else
		return true;
}

////Threading Methods

//Pauses all other threads of host process, performs ACTIONS, resumes all threads, unloads the DLL, and exits
DWORD WINAPI Moneta::initThread(LPVOID args)
{
	//HANDLE hands[20];

	time_t ti;
	time(&ti);
	initDump(ti);
	initLog(ti);
	writeLog("Thread kicked off");
	writeLog("Writing data to dump file: %s",dumpFile);


	HINSTANCE hinstDLL = (HINSTANCE)args;
	MessageBoxA(NULL, "Houston, we're in", "Dump-o-Matic", 0);
	//DWORD64 threadCount = pauseThreads(hands);

	//single threaded action
	if(threadAction(NULL,0) < 0)
		writeLog("ERROR: threadAction screwed up...");
	else
		writeLog("threadAction completed");

	//if(!resumeThreads(hands,threadCount))
	//{
	//	writeLog("resumeThread failed");
	//	//FreeLibraryAndExitThread(hinstDLL,-1);
	//}

	MessageBoxA(NULL, "All done", "Dump-o-Matic", 0);
	writeLog("Thread completed, exiting");
	FreeLibraryAndExitThread(hinstDLL,0);
	writeLog("You shouldn't be seeing this...");
	return 0;
}

//Loops through processes threads, pausing all but itself, and saving handles in an array
DWORD64 Moneta::pauseThreads(HANDLE* hands)
{
	HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
	THREADENTRY32 te32;
	DWORD64 it=0;

	DWORD64 dwOwnerPID =  GetCurrentProcessId();
	hThreadSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, 0);
	
	if(hThreadSnap == INVALID_HANDLE_VALUE)
	{
		writeLog("ERROR: Failed CreateToolhelp32Snapshot\n");
		return 0;
	}
	
	te32.dwSize = sizeof(THREADENTRY32);

	if( !Thread32First(hThreadSnap, &te32))
	{
		writeLog("ERROR: Fuck Thread32First");
		CloseHandle(hThreadSnap);
		return 0;
	}

	while(Thread32Next(hThreadSnap,&te32))
	{
		if(te32.th32OwnerProcessID == dwOwnerPID)
		{
			if(te32.th32ThreadID != GetCurrentThreadId())
			{
				HANDLE th = OpenThread(THREAD_SUSPEND_RESUME,NULL,te32.th32ThreadID);
				SuspendThread(th);
				hands[it]=th;
				it++;
			}
		}
	}
	writeLog("All threads suspended, numbered: %d",(char*)it);
	return it;
}
	
//Loops through the array of handles and resumes each thread
bool Moneta::resumeThreads(HANDLE* hands,DWORD64 it)
{
	for(int i=0;i<it;i++)
	{
		DWORD x=2;
		while(x>1)
		{
			x = ResumeThread(hands[i]);
		}
		if(x<0)
		{
			writeLog("Resuming thread failed for: %X",(char*)hands[i]);
			return false;
		}
		CloseHandle(hands[i]);
	}
	writeLog("All threads resumed, numbered: %d",(char*)it);
	return true;
}

//Kicks off the DLL function
DWORD64 Moneta::Start(LPVOID args)
{
	initThread(args);
	return 0;
}

//WINAPI to initiate the thread
static DWORD WINAPI spinUpThread(LPVOID args)
{
	Moneta mon;
	mon.Start(args);
	return 0;
}


///////////////threadSafeQueue Methods

//Push instance of template class onto queue
template <class T>
void threadSafeQueue<T>::tsPush(T t)
{
	while(true)
	{
		if(!mutex)
		{
			mutex = true;
			queue<T>::push(t);
			mutex = false;
			return;
		}
	}
}

//Pop instance of template class off queue
template <class T>
T threadSafeQueue<T>::tsPop()
{
	while(true)
	{
		if(!mutex)
		{
			mutex = true;
			T t = queue<T>::pop();
			mutex = false;
			return t;
		}
	}
}

//Cycle the write-safe mutex
template <class T>
void threadSafeQueue<T>::flip()
{
	mutex = !mutex;
}