This is a tool that auto-generates a tunables file for testing the memory analysis engine(name TBD)

An X: drive is required for using this tool.

The archive will auto extract to X:\Chiliad.
The above directory is required to be in this location for the tool to work. DO NOT MOVE IT!!!

Usage:
	- Just double-click the file X:\Chiliad\tunableGenerator.bat
	- A window will pop up. If working correctly, it will display "Injecting into Thread: #" 
	- Otherwise, an error message will display, and quit. 
		- If it sounds like something you didn't do, submit a bug as detailed below.
	- A pop-up should appear for the game, displaying "Houston, we're in"
	- After a few moments, another popup will appear displaying "All done". This means the tool has completed.
	- After some time, the window will display "Press enter to exit...". Do so.

Config file settings
	There is a file included with the package, config.json, that you can use for managing the operation of the tool. The following format is expected:


	{
		"memory_hasher_binary": This is the path to MemoryHasher.exe,
		"build_version": Build version of the game you are targeting, current is 372,
		"page_length": Page size, default is 1024,
		"default_tunables_file": The path to the "stock" tunables.json file you wish to use,
		"output_tunables_file": File path to the output tunables.json file, with the bonus array inserted,
		"memory_hasher_commands":[
			{
				"cmd_type":Integer to denote type of command, range of 0-3,
				"signature": String to be used for a binary signature. Can be ASCII or Hex, depending on the next field,
				"is_hex_string":Boolean that tells the tool whether to expect ASCII or Hex. (i.e. The string "Signature" would have a value of 'false' here, no quotes) ,
				"dll_name": Name of DLL to generate a signature for, if cmd_type 1
				"process_name": Name of remote process to generate signature for, if cmd_type 2
				"action_flags":Integer representing what actions the game will take, if signature has been detected. See table below
			},
			...
		]
	}

Command Types
	There are four types of signatures MemoryHasher.exe can generate:
	- Memory Byte Signature
		- The tool expects a string of space deliminated hex bytes and a byte range, which it generates a signature for
			-MemoryHasher.exe -exe=game_win64_master.protected.exe -addrL=0x10001000 -addrH 0x00000001 -length 1024 -crc -stringcheck "ff 25 ae b6 05 00 00 00 00 00 00 00 00 00 00 00 ff 25 a6 b6 05 00 00 00 00 00 00 00 00 00 00 00" -versionnumber 364 -actionflags 2 -dumpscanmem
	- DLL Name
		- The tool takes a string denoting the name of a DLL. If the string is found in the list of loaded modules, the game performs the denoted action
			- MemoryHasher.exe -exe=game_win64_master.protected.exe -length 1024 -dllname -dll=vehdebug-x86_64.dll -versionnumber 364 -actionflags 2 -dumpscanmem
	- Remote Process Name
		- The tool takes a string denoting the name of a remote process. If the process is found in the system process name list, game takes action.
			- MemoryHasher.exe -exe=game_win64_master.protected.exe -length 1024 -processName -processName=cheatengine-x86_64.exe -versionnumber 364 -actionflags 1 -dumpscanmem
	- Process Memory Hash
		- The tool takes a name of a remote process, and if found on the system list, takes a hash of the process' memory snapshot, which the games uses to fingerprint the target process
			- MemoryHasher.exe -exe=game_win64_master.protected.exe -length 1024 -processCrc -processName=cheatengine-x86_64.exe -versionnumber 364 -actionflags 2 -dumpscanmem


Signature Actions
	Part of the signature generation takes an integer denoting "action flags", which represent reactions the game can have when detecting a signature.
	The action flags can be a combination of any of these actions, by adding their respective values together:
        Telemetry = 1
        	The game reports its user via telemetry.
        Report = 2
        	The game report its user to any one peer in session at that point in time.
        KickRandomly = 8
        	After an random amount of time, kick the user from online.
        NoMatchmake = 16
        	Prevents the user from matchmaking with other players



Bugs
	- Firstly, this is doing some funky stuff with running memory, crashes are not unexpected
		- That being said, I still have yet to have the latest syringe crash
	- If the game client crashes after running this tool, try it again one more time before submitting a bug
	- If it crashes again at the same point, do the following:
		- Double click the file X:\Chiliad\help\GTA_MINIDUMP.reg
			- This sets registry values that force windows to auto-create dump files during a crash
		- A window will appear with text "Adding information can unintentionally...Are you sure you want to continue?"
			- You are sure, click Yes
			- Once it's done, click OK
	- Launch the game, and then run tunableGenerator.bat
	- If it crashed the game, then you should find a minidump file in help/dumps/
		- Will be named something like: game_win64_bankrelease.exe.11968.dmp
		- If there's more than one, just grab the latest!
	- Now open a bug under the Social Club project, detail what happened, and assign it to Conor Walsh

	- Afterwards, you may run X:\Chiliad\help\MINIDUMP_CLEANUP.reg to stop dumps from being auto-generated.


If you have any issues or questions, feel free to contact conor.walsh@rockstargames.com