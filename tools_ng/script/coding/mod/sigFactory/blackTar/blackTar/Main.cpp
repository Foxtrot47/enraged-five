#define _CRT_SECURE_NO_WARNINGS
#include "Moneta.h"

void InitiateHooks()
{	
	DWORD64 wantedAddr = findPattern("game_win64_bankrelease.exe", 
		"\xc7\x83\x94\x00\x00\x00\x01\x00\x00\x00\xc6\x43\x28\x01\xc6\x83\xdc\x03\x00\x00\x01\xe9\x8d\x00\x00\x00\x85\xff\x74\x66\x4c\x89\xbb\xc4", 
		"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

	if(wantedAddr == 0)
		return;

	char off[] = {0x0, 0x2C, 0x3A, 0x2C, 0x30};

	for(int i = 0; i < sizeof(off); i++)
	{
		DWORD64 offset = off[i];
		wantedAddr -= offset;
		nopSlide((BYTE*)wantedAddr, 0xA);
		//printAddr(wantedAddr);
	}
}

DWORD64 WINAPI OverwriteMem()
{
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		MessageBoxA(NULL, "Xttached Successfully", "", 0);
		InitiateHooks();
		//CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)OverwriteMem, NULL, NULL, NULL);
		break;
	}
	return TRUE;
}

