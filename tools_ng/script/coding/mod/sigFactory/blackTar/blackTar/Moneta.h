#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>

void printAddr(DWORD64 addy)
{
	char buff[1024];
	sprintf(buff, "Addy %02p", addy);
	MessageBox(NULL, buff, "wat...", MB_OK);
}




void jmpIt(BYTE *Address, DWORD64 jumpTo, DWORD64 length=9)
{
	DWORD dwOldProt, dwBkup;
	DWORD64 dwRelAddr;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	dwRelAddr = (DWORD64)(jumpTo - (DWORD64)Address) -9;

	*Address = 0xE9;

	*((DWORD64 *)(Address + 0x1)) = dwRelAddr;

	for(DWORD64 x=0x9; x<length; x++)
		*(Address + x) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}


void nopSlide(BYTE *Address, DWORD64 length=2)
{
	DWORD dwOldProt, dwBkup;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	for(DWORD64 i=0; i<length; i++)
		*(Address + i) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}







MODULEINFO GetModInfo(char * szModule)
{
	MODULEINFO modInfo = {0};
	HMODULE hModule = GetModuleHandle(szModule);

	if(hModule==0)
		return modInfo;

	GetModuleInformation(GetCurrentProcess(), hModule, &modInfo, sizeof(MODULEINFO));

	return modInfo;
}

DWORD64 findPattern(char *module, char *pattern, char *mask)
{
	MODULEINFO mInfo = GetModInfo(module);

	DWORD64 base = (DWORD64)mInfo.lpBaseOfDll;
	DWORD64 size = (DWORD64)mInfo.SizeOfImage;

	DWORD64 patternLength = (DWORD64)strlen(mask);

	for(DWORD64 i=0; i < size-patternLength; i++)
	{
		bool found = true;

		for (DWORD64 j=0; j < patternLength; j++)
		{
			found &= mask[j] == '?' || pattern[j] == *(char*)(base+i+j);
		}

		if(found)
			return base+i;
	}
	return 0;
	
}