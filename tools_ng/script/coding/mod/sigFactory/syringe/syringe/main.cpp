#include <windows.h>
#include <tlhelp32.h>
#include <iostream>
#include <shlwapi.h>

extern "C" int __stdcall loadDll();
extern "C" void* begin;
extern "C" void* finish;

//#define targetProc "notepad.exe"
//#define targetProc "GTA5.exe"
//#define targetProc "game_win64_bankrelease.exe"
//#define targetDll "fuckworld.dll"
//#define targetDll "C:\\Users\\cwalsh\\work\\dev\\sigFactory\\brainDump\\x64\\Release\\brainDump.dll"

DWORD FindProcessName(const char *__ProcessName, HANDLE *pEntry);
DWORD GetThreadByProcess(DWORD __DwordProcess);


int main(int argc, char* argv[])
{
	char* targetProc;
	char* targetDll;

	void *dllString = NULL;
	void *stub = NULL;
	DWORD wowID, threadID, oldprot;
	DWORD64 stubLen, loadLibAddy, oldIP;
	HANDLE hProcess = NULL, hThread = NULL;
	CONTEXT ctx;
	SIZE_T numWritten = 0;

	DWORD64 addr = (DWORD64)(&begin);
	DWORD64 addr2 = (DWORD64)(&finish);
	stubLen = addr2-addr; 

	if(argc != 3)
	{
		std::cout<<"Invalid number of arguments\nUsage: syringe.exe <process name> <path to DLL>"<<std::endl;
		return -1;
	}
	else
	{
		targetProc = argv[1];
		targetDll = argv[2];
	}


   //Navigate extern "C" jump table for ACTUAL function pointer. Credit: Amir Soofi
   //Debug uses jump tables for functions. Will need to be fixed if compiled in Release

   //unsigned char * loadDllReloc = (unsigned char *)&(loadDll);
   //loadDllReloc++;
   //unsigned long * loadDllRelocIntPtr = (unsigned long*)loadDllReloc;
   //unsigned long loadDllRelocVal = *loadDllRelocIntPtr;
   //unsigned long long actLoadDllAddr = (unsigned long long)&loadDll+loadDllRelocVal;
   //actLoadDllAddr+=5;
   //unsigned long long *actLoadDll = (unsigned long long *)actLoadDllAddr;



   //Grab address for LoadLibraryA in kernel32.dll
   loadLibAddy = (DWORD64)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
   if(!loadLibAddy){std::cout<<"Error:  Couldn't get Kernel32.dll::LoadLibraryA proc"<<std::endl; goto cleanup;}

   //Get PID from proc name
   ////Grab procID
   //wowID    = FindProcessName(targetProc,&hProcess);
   //if (wowID == 0x0)
   //{
	  // std::cout<<"ERROR: Process '"<<targetProc<<"' is not running. Exiting..."<<std::endl;
	  // Sleep(2000);
	  // exit(1);
   //}

   //Get PID from args
   wowID = atoi(targetProc);

   hProcess = OpenProcess((PROCESS_VM_WRITE | PROCESS_VM_OPERATION), false, wowID);
   if(!hProcess) {std::cout<<"Error: Could not open handle to remote process"<<std::endl; goto cleanup;}

   //Allocate memory for DLL name and cave to sperlunk into
   dllString = VirtualAllocEx(hProcess, NULL, (strlen(targetDll) + 1), MEM_COMMIT, PAGE_READWRITE);
   stub      = VirtualAllocEx(hProcess, NULL, stubLen, (MEM_COMMIT | MEM_RESERVE), PAGE_EXECUTE_READWRITE);
   if(dllString == NULL || stub == NULL) {std::cout<<"Error:  Virtual Alloc Failed"<<std::endl; goto cleanup;}

   //Write name of DLL to inject into remote process space
   WriteProcessMemory(hProcess, dllString, targetDll, strlen(targetDll), &numWritten);
   if(numWritten != strlen(targetDll)) {std::cout<<"Error:  Write DLL Name Failed"<<std::endl; goto cleanup;}
    
   //Grab ThreadID for main thread
   threadID = GetThreadByProcess(wowID);
   std::cout<<"Injecting into Thread: "<<threadID<<std::endl;

   hThread   = OpenThread((THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME ), false, threadID);
   if(!hThread) {std::cout<<"Error: Failed to open remote thread"<<std::endl; goto cleanup;}

   SuspendThread(hThread);

   //Grab thread context
   ctx.ContextFlags = CONTEXT_CONTROL;
   if(!GetThreadContext(hThread, &ctx)){std::cout<<"Error: Failed to get context of remote thread..."<<std::endl; goto cleanup;}

   //Store old RIP, and set RIP to code cave entry point
   oldIP   = ctx.Rip;
   ctx.Rip = (DWORD64)stub;
   ctx.ContextFlags = CONTEXT_CONTROL;


   //USE THESE IF COMPILING DEBUG////////////////////////////////

   //int virtProtectReturn = VirtualProtect((void *)actLoadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

   ////Copy relevant addresses into the place holders in sperlunk code
   //memcpy((void *)((DWORD64)actLoadDll + 0x5), &oldIP, sizeof(oldIP));
   //memcpy((void *)((DWORD64)actLoadDll + 0x2A), &dllString, sizeof(dllString));
   //memcpy((void *)((DWORD64)actLoadDll + 0x34), &loadLibAddy, sizeof(loadLibAddy));    

   ///////////////////////////////////////////////////////////////


   //USE THESE IF COMPILING RELEASE//////////////////////////////

   int virtProtectReturn = VirtualProtect((void *)loadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

   //Copy relevant addresses into the place holders in sperlunk code
   memcpy((void *)((DWORD64)loadDll + 0x5), &oldIP, sizeof(oldIP));
   memcpy((void *)((DWORD64)loadDll + 0x2A), &dllString, sizeof(dllString));
   memcpy((void *)((DWORD64)loadDll + 0x34), &loadLibAddy, sizeof(loadLibAddy));    

   ///////////////////////////////////////////////////////////////


   //Set necessary protections on code cave, and write code into memory cave
   numWritten = 0;
   VirtualProtectEx(hProcess, stub, stubLen, PAGE_EXECUTE_READWRITE, NULL);
   WriteProcessMemory(hProcess, stub, loadDll, stubLen, &numWritten);
   VirtualProtectEx(hProcess, stub, stubLen, PAGE_EXECUTE, NULL); //Unsure if needed, but remote process seemed to crash much less after adding this line...
   if(numWritten != stubLen)std::cout<<"Error: Couldn't write to process memory"<<std::endl;

   //Set thread context and flush the cache in case commands were queued up
   SetThreadContext(hThread, &ctx);
   //FlushInstructionCache(hProcess, NULL, NULL);



   ResumeThread(hThread);
   Sleep(15000);

cleanup:
   std::cout<<"Press any key to exit..."<<std::endl;
   getchar();
   ResumeThread(hThread);
   if(dllString)
	   VirtualFreeEx(hProcess, dllString, strlen(targetDll)+1, MEM_DECOMMIT);
   if(stub)
	   VirtualFreeEx(hProcess, stub, stubLen, (MEM_DECOMMIT | MEM_RELEASE));

   CloseHandle(hProcess);
   CloseHandle(hThread);
   return 0;

}


DWORD FindProcessName(const char *__ProcessName, HANDLE *pEntry)
{   
    PROCESSENTRY32 __ProcessEntry;
    __ProcessEntry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot == INVALID_HANDLE_VALUE) return 0;        if (!Process32First(hSnapshot, &__ProcessEntry))
    {
        CloseHandle(hSnapshot);
        return 0;
    }
    do{if (!_strcmpi(__ProcessEntry.szExeFile, __ProcessName))
    {
        memcpy((void *)pEntry, (void *)&__ProcessEntry, sizeof(PROCESSENTRY32));
        CloseHandle(hSnapshot);
        return __ProcessEntry.th32ProcessID;
    }} while (Process32Next(hSnapshot, &__ProcessEntry));
    CloseHandle(hSnapshot);
    return 0;
}
 
DWORD GetThreadByProcess(DWORD __DwordProcess)
{   
	THREADENTRY32 __ThreadEntry;
	__ThreadEntry.dwSize = sizeof(THREADENTRY32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE) return 0;
 
	if (!Thread32First(hSnapshot, &__ThreadEntry)) {CloseHandle(hSnapshot); return 0; }
 
	do {if (__ThreadEntry.th32OwnerProcessID == __DwordProcess)
	{
		CloseHandle(hSnapshot);
		return __ThreadEntry.th32ThreadID;
	}} while (Thread32Next(hSnapshot, &__ThreadEntry));
	CloseHandle(hSnapshot);      
	return 0;
}