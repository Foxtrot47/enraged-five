#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <queue>
#include "time.h"
#include "json\json.h"
#include <fstream>

#define WORK_DIR "X:\\Chiliad\\"

//Struct containing MBI as well as int for order
struct memPage
{
	int order;
	MEMORY_BASIC_INFORMATION mbi;
};

template <class T>
class threadSafeQueue: public std::queue<T>
{
public:
	memPage getPage();
	threadSafeQueue() : std::queue<T>()	{mutex = false;}
	void tsPush(T);
	void flip();
	T tsPop();
protected:
	bool mutex;
};

class Moneta
{
	char dumpFile[MAX_PATH];
	char logFile[MAX_PATH];

	DWORD64 pauseThreads(HANDLE*);

	LPVOID setExecProtect();
	
	bool resumeThreads(HANDLE*,DWORD64);
	bool writeDump(BYTE*,SIZE_T);

	void initLog(time_t);
	void initDump(time_t);
	void workFolder();

	std::string exec(const char*);
	std::string exec(std::string);

protected:
	DWORD64 FindPattern();
	DWORD64 DumpWalk(LPVOID,DWORD64);
	DWORD64 FindPattern(char*, char*, char*);
	
	void JmpIt(BYTE*, DWORD64, DWORD64);
	void NopSlide(BYTE*, DWORD64);
	
	MODULEINFO GetModInfo(char*);	
	
public:
	void WriteLog(char* format, ...);
	void MsgBox(const char*);
	void GetProcessName(char*,int);
	
	DWORD InitThread();
	DWORD64 Start(LPVOID);
	DWORD64 Quit();
	DWORD64 DllBaseAddress();

	std::string RunCmd(char* cmdFormatStr,...);
	std::string HexString(std::string);

	bool WriteJSON();

	Json::Value ReadJSON(const char*);

	~Moneta()
	{
		MsgBox("All done");
		WriteLog("Thread completed, exiting");
	}
};

///////////////Moneta Methods

////Utility Functions

//Essentially just a wrapper for vfprintf into the logFile
void Moneta::WriteLog(char* format, ...)
{
	va_list argptr;
	va_start(argptr, format);
	FILE* pFile = fopen(logFile,"a");
	vfprintf(pFile,format,argptr);
	va_end(argptr);
	fprintf(pFile,"\n");
	fclose(pFile);
}

//Print DWORD in MsgBox
void Moneta::MsgBox(const char* msg)
{
	char buff[1024];
	sprintf(buff, "Alert: %s", msg);
	MessageBox(NULL, buff, "Dll Dude", MB_OK);
}

//Executes a command, returns output in a string
std::string Moneta::exec(const char* cmd) {
	WriteLog("Run command:\n %s",cmd);
    FILE* pipe = _popen(cmd, "r");
    if (!pipe)
	{
		WriteLog("ERROR: could not open new process");
		return "";
	}
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
    	if(fgets(buffer, 128, pipe) != NULL)
    		result += buffer;
    }
    _pclose(pipe);
    return result;
}

std::string Moneta::exec(std::string cmd) {exec(cmd.c_str());}

//Sprintf's an arbitrary number of arguments, and runs the command, returns output
std::string Moneta::RunCmd(char* cmdFormatStr,...)
{
	const int cmdMax = 3000;
	va_list argptr;
	std::string cmdd;
	
	char cmd[cmdMax];
	std::string out;

	va_start(argptr, cmdFormatStr);	
	
	if(vsnprintf(cmd,cmdMax,cmdFormatStr,argptr)<0)
	{
		WriteLog("Command longer than 3000 chars: (Truncated)\n(%s",cmd);
		return "";
	}
	out = exec(cmd);
	if(out.empty()){WriteLog("ERROR: Application failed to start");return "";}
	WriteLog("Command Line Output:\n%s",out.c_str());

	va_end(argptr);

	return out;
}

//Checks for dump folder, and creates if doesn't exist
void Moneta::workFolder()
{
	DWORD dwAttrib = GetFileAttributes(WORK_DIR);
	if( dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY))return;
	else CreateDirectory(WORK_DIR,NULL);
	return;
}

//Init logFile
void Moneta::initLog(time_t ti)
{
	char file[_MAX_FNAME];
	
	workFolder();
	GetProcessName(file,_MAX_FNAME);
	sprintf(logFile,"%s\\logs\\%d.%s.redHerring.log.txt",WORK_DIR,ti,file);
	remove(logFile);
	FILE * fPtr = fopen(logFile,"w");
	fprintf(fPtr,"Moneta: Injected Memory Library\n-------------------------------\nStarted: %d\n-------------------------------\n\n",ti);
	fclose(fPtr);
	return;
}

//Convert ASCII string to ASCII Hex string; Space delim'd bytes
std::string Moneta::HexString(std::string in)
{
	std::string hString;
	WriteLog("Generating hex string from ASCII string: \"%s\"",in.c_str());
	for(int i=0;i<in.length();i++)
	{
		char x[5];
		sprintf(x,"%02X ",in[i]);
		hString.append(x);
	}
	WriteLog("Generated hex string: \"%s\"",hString.c_str());
	return hString;
}

//Read in JSON file to Json::Value object
Json::Value Moneta::ReadJSON(const char* file)
{
	bool run = true;
	while(run)
	{
		Json::Value root;
		Json::Reader reader;
		std::ifstream test(file, std::ifstream::binary);

		if(!reader.parse(test,root,false))
		{
			std::cout<< "Error\n";
		}

		return root;
	}
	return NULL;
}

//Returns base address of this injected DLL
DWORD64 Moneta::DllBaseAddress()
{
	MODULEINFO modInfo;
	memset(&modInfo,0,sizeof(modInfo));
	MEMORY_BASIC_INFORMATION mbi;
	memset(&mbi,0,sizeof(MEMORY_BASIC_INFORMATION));
	HMODULE hDll = GetModuleHandle("redHerring.dll");

	//Get information for current module
	if(!GetModuleInformation(GetCurrentProcess(),hDll, &modInfo,sizeof(MODULEINFO)))
	{
		WriteLog("GetModInfo error: 0x%x",GetLastError());
	}

	return (DWORD64)modInfo.lpBaseOfDll;
}

//Passes name of victim process
void Moneta::GetProcessName(char* target,int length)
{
	char file[MAX_PATH+1];
	char ext[_MAX_EXT];
	GetModuleFileName(NULL,file,length);
	_splitpath(file,NULL,NULL,target,ext);
	strncat(target,ext,length);
}

////Threading Methods

//Pauses all other threads of host process, performs ACTIONS, resumes all threads, unloads the DLL, and exits
DWORD Moneta::InitThread()
{
	//HANDLE hands[20];
	//DWORD64 threadCount = pauseThreads(hands);

	//if(!resumeThreads(hands,threadCount))
	//{
	//	WriteLog("resumeThread failed");
	//	//FreeLibraryAndExitThread(hinstDLL,-1);
	//}

	time_t ti;
	time(&ti);
	initLog(ti);
	WriteLog("Thread kicked off");
	
	MsgBox("Houston, we're in");

	setExecProtect();

	return 0;
}

//Loops through processes threads, pausing all but itself, and saving handles in an array
DWORD64 Moneta::pauseThreads(HANDLE* hands)
{
	HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
	THREADENTRY32 te32;
	DWORD64 it=0;

	DWORD64 dwOwnerPID =  GetCurrentProcessId();
	hThreadSnap = CreateToolhelp32Snapshot( TH32CS_SNAPTHREAD, 0);
	
	if(hThreadSnap == INVALID_HANDLE_VALUE)
	{
		WriteLog("ERROR: Failed CreateToolhelp32Snapshot\n");
		return 0;
	}
	
	te32.dwSize = sizeof(THREADENTRY32);

	if( !Thread32First(hThreadSnap, &te32))
	{
		WriteLog("ERROR: Fuck Thread32First");
		CloseHandle(hThreadSnap);
		return 0;
	}

	while(Thread32Next(hThreadSnap,&te32))
	{
		if(te32.th32OwnerProcessID == dwOwnerPID)
		{
			if(te32.th32ThreadID != GetCurrentThreadId())
			{
				HANDLE th = OpenThread(THREAD_SUSPEND_RESUME,NULL,te32.th32ThreadID);
				SuspendThread(th);
				hands[it]=th;
				it++;
			}
		}
	}
	WriteLog("All threads suspended, numbered: %d",(char*)it);
	return it;
}
	
//Loops through the array of handles and resumes each thread
bool Moneta::resumeThreads(HANDLE* hands,DWORD64 it)
{
	for(int i=0;i<it;i++)
	{
		DWORD x=2;
		while(x>1)
		{
			x = ResumeThread(hands[i]);
		}
		if(x<0)
		{
			WriteLog("Resuming thread failed for: %X",(char*)hands[i]);
			return false;
		}
		CloseHandle(hands[i]);
	}
	WriteLog("All threads resumed, numbered: %d",(char*)it);
	return true;
}


////RTM functions

//Walks pages of process space and dumps memory
DWORD64 Moneta::DumpWalk(LPVOID start, DWORD64 end)
{
		DWORD64 memoryCount = 0;
		DWORD64 pageCount = 0;
		DWORD64 failedPages = 0;

		MEMORY_BASIC_INFORMATION mbi;
		memset(&mbi, 0, sizeof(MEMORY_BASIC_INFORMATION));
		BYTE* p;
		
		WriteLog("Beginning to dump process memory...");
		while(VirtualQuery(start, &mbi, sizeof(MEMORY_BASIC_INFORMATION)) == sizeof(MEMORY_BASIC_INFORMATION))
		{
			p = (BYTE *)mbi.BaseAddress;
			pageCount++;

			if (mbi.State == MEM_COMMIT )
			{
				if(!writeDump(p,mbi.RegionSize))
				{
					failedPages++;
					//return -1;
				}
				memoryCount += (DWORD64)mbi.RegionSize;
			}
			start = p + mbi.RegionSize;
		}
		char m[100];
		sprintf(m,"%d KB written to disk",(memoryCount/1000));
		WriteLog("Completed dump, %s",m);
		memset(m,0,100);
		sprintf(m,"Total Number of Memory Pages: %d | Number of Failed Page Dumps: %d",pageCount,failedPages);
		WriteLog(m);
		return memoryCount;
}

//Places a jump at 'Address' that jumps to address'jumpTo'
void Moneta::JmpIt(BYTE *Address, DWORD64 jumpTo, DWORD64 length=9)
{
	DWORD dwOldProt, dwBkup;
	DWORD64 dwRelAddr;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	dwRelAddr = (DWORD64)(jumpTo - (DWORD64)Address) -9;

	*Address = 0xE9;

	*((DWORD64 *)(Address + 0x1)) = dwRelAddr;

	for(DWORD64 x=0x9; x<length; x++)
		*(Address + x) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}

//Writes a nop slide of 'length' starting at 'Address'
void Moneta::NopSlide(BYTE *Address, DWORD64 length=2)
{
	DWORD dwOldProt, dwBkup;

	VirtualProtect(Address, (SIZE_T)length, PAGE_EXECUTE_READWRITE, &dwOldProt);

	for(DWORD64 i=0; i<length; i++)
		*(Address + i) = 0x90;

	VirtualProtect(Address, (SIZE_T)length, dwOldProt, &dwBkup);
}

//Get's MODULEINFO construct for a given Module name 'szModule'
MODULEINFO Moneta::GetModInfo(char * szModule)
{
	MODULEINFO modInfo = {0};
	HMODULE hModule = GetModuleHandle(szModule);

	if(hModule==0)
		return modInfo;

	GetModuleInformation(GetCurrentProcess(), hModule, &modInfo, sizeof(MODULEINFO));

	return modInfo;
}

//Scans memory for a pattern specified by 'pattern'. 'mask' length is the same as 'pattern', of arbitrary characters. Any '?' in 'mask' identifies a wildcard at the same position in 'pattern'.
DWORD64 Moneta::FindPattern(char *module, char *pattern, char *mask)
{
	MODULEINFO mInfo = GetModInfo(module);

	DWORD64 base = (DWORD64)mInfo.lpBaseOfDll;
	DWORD64 size = (DWORD64)mInfo.SizeOfImage;

	DWORD64 patternLength = (DWORD64)strlen(mask);

	for(DWORD64 i=0; i < size-patternLength; i++)
	{
		bool found = true;

		for (DWORD64 j=0; j < patternLength; j++)
		{
			found &= mask[j] == '?' || pattern[j] == *(char*)(base+i+j);
		}

		if(found)
			return base+i;
	}
	return 0;
	
}

LPVOID Moneta::setExecProtect()
{
	char buf[100];

	MODULEINFO modInfo;
	memset(&modInfo,0,sizeof(modInfo));
	MEMORY_BASIC_INFORMATION mbi;
	memset(&mbi,0,sizeof(MEMORY_BASIC_INFORMATION));
	HMODULE hDll = GetModuleHandle("redHerring.dll");

	//Get information for current module
	if(!GetModuleInformation(GetCurrentProcess(),hDll, &modInfo,sizeof(MODULEINFO)))
	{
		WriteLog("GetModInfo error: 0x%x",GetLastError());
	}
	LPVOID addr = modInfo.lpBaseOfDll;
	
	memset(buf,0,100);
	sprintf(buf,"0x%x, ending at 0x%x",(DWORD64)addr,(DWORD64)((DWORD)modInfo.lpBaseOfDll + modInfo.SizeOfImage));
	WriteLog("Starting at %s",buf);
	
	WriteLog("Looping through memory pages...");
	//Walk through owned memory pages, and add executable permissions
	while((DWORD64)addr<((DWORD64)modInfo.lpBaseOfDll + modInfo.SizeOfImage))
	{
		
		if(VirtualQuery(addr,&mbi,sizeof(MEMORY_BASIC_INFORMATION)) == sizeof(MEMORY_BASIC_INFORMATION))
		{
			memset(buf,0,100);
			sprintf(buf,"0x%x",(DWORD64)addr);
			WriteLog("Checking protections on page at %s",buf);

			if(mbi.Protect<0x10) //All non-exe protections are <16
			{
				DWORD oldProt;
				if(!VirtualProtect(addr,mbi.RegionSize,mbi.Protect*0x10,&oldProt)) //"Adding" exec protections is really mulitplying by 0x10
				{
					memset(buf,0,100);
					sprintf(buf,"0x%x with Error: 0x%x",addr,GetLastError());		//Hopefully this isn't a memory page containing the signature...
					WriteLog("Error writing prots to %s",buf);
				}
				else
				{
					memset(buf,0,100);
					sprintf(buf,"0x%x to 0x%x",mbi.Protect*0x10,addr);
					WriteLog("Wrote protection value %s",buf); 
				}
			}
			addr = (LPVOID)((DWORD64)addr + mbi.RegionSize);
		}
	}
	WriteLog("Done writing protections\n");

	return modInfo.lpBaseOfDll;
}


////Memory dump 

//Initiates file to be dumped to
void Moneta::initDump(time_t ti)
{
	char path[MAX_PATH];
	char file[_MAX_FNAME];
	
	workFolder();
	GetModuleFileName(NULL,path,MAX_PATH);
	_splitpath(path,NULL,NULL,file,NULL);
	sprintf(dumpFile,"%s\\%d.%s.brainDump.bin",WORK_DIR,ti,file);
	remove(dumpFile);	
	WriteLog("Writing data to dump file: %s",dumpFile);
	FILE* fPtr = fopen(dumpFile, "wb");
	rewind(fPtr);
	fclose(fPtr);
	return;
}

//Appends memory chunk to file
bool Moneta::writeDump(BYTE* p,SIZE_T sz)
{
	FILE* fPtr = fopen(dumpFile, "ab");
	DWORD64 ret = fwrite(p,sz,1,fPtr);
	fclose(fPtr);

	if(ret < 1)
		return false;
	else
		return true;
}

///////////////threadSafeQueue Methods

//Push instance of template class onto queue
template <class T>
void threadSafeQueue<T>::tsPush(T t)
{
	while(true)
	{
		if(!mutex)
		{
			mutex = true;
			queue<T>::push(t);
			mutex = false;
			return;
		}
	}
}

//Pop instance of template class off queue
template <class T>
T threadSafeQueue<T>::tsPop()
{
	while(true)
	{
		if(!mutex)
		{
			mutex = true;
			T t = queue<T>::pop();
			mutex = false;
			return t;
		}
	}
}

//Cycle the write-safe mutex
template <class T>
void threadSafeQueue<T>::flip()
{
	mutex = !mutex;
}