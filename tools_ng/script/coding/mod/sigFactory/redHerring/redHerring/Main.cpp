#include "Moneta.h"

Json::Value parseBonusRow(std::string row);
std::string parseSignatures(std::string out);
DWORD64 threadAction(Moneta&);
bool runMemHashCmd(const Json::Value&,Moneta&,Json::Value&);


//Signature string
std::string sig = "ThisisAstringNotInTheGame";
//std::string sig2 = "here's a new vulnerability in town...   \"The new bug, dubbed LogJam, is a cousin of Freak. But it�s in the basic design of TLS itself";
//std::string sig3 = "Just this last week the percentage of cheaters I've encountered has dropped with 95%";

std::string bonus;
std::string hSig;

DWORD64 version;
DWORD64 actionFlags;
DWORD64 length;
DWORD64 low;
DWORD64 high;

std::ofstream tunFile;

Json::Value config;
Json::Value tunables;
Json::Value bonusArray(Json::arrayValue);

char target[MAX_PATH+1];
char file[MAX_PATH+1];
const char* hashBin;
const char* defTun;

//WINAPI to initiate the thread
static DWORD WINAPI spinUpThread(LPVOID args)
{
	Moneta mon;
	mon.InitThread();
	threadAction(mon);
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		HANDLE h = CreateThread(NULL, 0,spinUpThread, &hinstDLL, 0, NULL);
		return TRUE;
	}
}

DWORD64 threadAction(Moneta &mon)
{
	sprintf_s(file,MAX_PATH,"%sconfig.json",WORK_DIR);
	config = mon.ReadJSON(file);
	if(!config){mon.WriteLog("ERROR: No config file found here: %s",file);return -1;}

	mon.GetProcessName(target,_MAX_FNAME);

	try{
		//Get default tunables file
		tunables = mon.ReadJSON(config["default_tunables_file"].asCString());	if(!tunables){mon.WriteLog("ERROR: Invalid default tunables file found here: %s",config["default_tunables_file"].asCString());return -1;}
		//Memory hasher cli arguments
		hashBin = config["memory_hasher_binary"].asCString();		if(!hashBin){mon.WriteLog("ERROR: Invalid MemoryHasher path: %s",config["memory_hasher_binary"].asCString());return -1;}
		version = config["build_version"].asInt64();				if(!version){mon.WriteLog("ERROR: Invalid build version: %s",config["build_version"].asCString());return -1;}
		length = config["page_length"].asInt64();					if(!length){mon.WriteLog("ERROR: Invalid page length: %s",config["page_length"].asCString());return -1;}
		defTun = config["output_tunables_file"].asCString();	if(!tunFile){mon.WriteLog("ERROR: Could not open output tunables file",config["output_tunables_file"].asCString());return -1;}
		remove(defTun);
	}
	catch(...)
	{
		mon.WriteLog("Invalid config file...");
		return -1;
	}


	mon.WriteLog("Setting Exectuable protections on the DLL memory space");
	DWORD64 dllBaseAddr = mon.DllBaseAddress();

	//Memory Signature check
	low =  dllBaseAddr % 0x100000000;
	high = (dllBaseAddr - low)/0x100000000;
	Json::Value memCmds;

	try{
		if(config["memory_hasher_commands"].size() == 0)	{mon.WriteLog("ERROR: No MemoryHasher commands found");return -1;}
		memCmds = config["memory_hasher_commands"];
	}
	catch(...)
	{
		mon.WriteLog("No memory_hasher_commands entry in config file");
		return -1;
	}

	//Runs MemoryHasher.exe
	if(!runMemHashCmd(memCmds,mon,bonusArray)){mon.WriteLog("ERROR: runMemHashCmd failed");return -1;}

	//Check for empty bonus array
	if(bonusArray.empty()){mon.WriteLog("ERROR: Empty Bonus Array");return -1;}
	mon.WriteLog("BonusArray: %s",bonusArray.toStyledString().c_str());

	//Write data to tunables.json
	mon.WriteLog("Writing data to tunable file: %s",defTun);
	tunFile.open(defTun);
	tunables["bonus"] = bonusArray;
	mon.WriteLog("Writing tunables to disk: \n%s",tunables.toStyledString().c_str());
	Json::StyledWriter sw;
	tunFile << sw.write(tunables);
	tunFile.close();
	mon.WriteLog("Tunables file written");

	return 0;
}

bool runMemHashCmd( const Json::Value &root, Moneta &mon, Json::Value &bonusArray)
{
	if(root.size() > 0){
		std::string out;
		const char* name;
		int type;
		int key;
		Json::Value cmd;

		mon.WriteLog("Number of MemoryHasher commands to process: %d\n",root.size());
		for(Json::ValueIterator itr = root.begin(); itr !=root.end();	itr++){
			cmd = *itr;

			try
			{
				std::string sig1;
				bool hex;

				type = cmd["cmd_type"].asInt();
				key = itr.key().asInt();

				switch(type){
				case 0:	//Memory Signature
					mon.WriteLog("Processing Memory Signature Command");
					sig1 = cmd["signature"].asString();			if(sig1 == ""){mon.WriteLog("ERROR: Invalid signature string: %s",cmd["signature"].asCString());return false;}
					actionFlags = cmd["action_flags"].asInt64();				if(!actionFlags){mon.WriteLog("ERROR: Invalid action flags: %s",cmd["action_flags"].asCString());return false;}	
					hex = cmd["is_hex_string"].asBool();

					if(!hex)hSig = mon.HexString(sig1);								

					out = parseSignatures(mon.RunCmd("%s -exe=%s -addrL 0x%x -addrH 0x%x -length %d -crc -stringcheck \"%s\" -versionnumber %d -actionflags %d",
												hashBin,target,low,high,length,hSig.c_str(),version,actionFlags));
					if(out == "error"){mon.WriteLog("ERROR: MemoryHasher command failed. Continuing with other commands...");continue;}
					if(out == "fail" ){mon.WriteLog("ERROR: Could not connect to target process");continue;}

					bonusArray.append(parseBonusRow(out));
					continue;

				case 1: //DLL Name
					mon.WriteLog("Processing DLL Name Command");
					name = cmd["dll_name"].asCString();			if(!name){mon.WriteLog("ERROR: Invalid dll name: %s",cmd["name"].asCString());return false;}
					actionFlags = cmd["action_flags"].asInt64();				if(!actionFlags){mon.WriteLog("ERROR: Invalid action flags: %s",cmd["action_flags"].asCString());return false;}

					out = parseSignatures(mon.RunCmd("%s -exe=%s -length %d -dllname -dll=%s -versionnumber %d -actionflags %d",
												hashBin,target,length,name,version,actionFlags));		//No need to check for fail, process independent string hash

					bonusArray.append(parseBonusRow(out));
					continue;

				case 2: //Process Name
					mon.WriteLog("Processing Remote Process Name Command");
					name = cmd["process_name"].asCString();		if(!name){mon.WriteLog("ERROR: Invalid process name: %s",cmd["name"].asCString());return false;}
					actionFlags = cmd["action_flags"].asInt64();				if(!actionFlags){mon.WriteLog("ERROR: Invalid action flags: %s",cmd["action_flags"].asCString());return false;}

					out = parseSignatures(mon.RunCmd("%s -exe=%s -length %d -processName -processName=%s -versionnumber %d -actionflags %d",
												hashBin,target,length,name,version,actionFlags));	//No need to check for fail, process independent string hash

					bonusArray.append(parseBonusRow(out));
					continue;

				case 3: //Memory Hash
					mon.WriteLog("Processing Process Memory Hash Command");
					name = cmd["process_name"].asCString();		if(!name){mon.WriteLog("ERROR: Invalid process name: %s",cmd["name"].asCString());return false;}
					actionFlags = cmd["action_flags"].asInt64();				if(!actionFlags){mon.WriteLog("ERROR: Invalid action flags: %s",cmd["action_flags"].asCString());return false;}

					out = parseSignatures(mon.RunCmd("%s -exe=%s -length %d -processCrc -processName=%s -versionnumber %d -actionflags %d",
												hashBin,target,length,name,version,actionFlags));
					if(out == "fail" ){mon.WriteLog("ERROR: Could not connect to target process");continue;}

					bonusArray.append(parseBonusRow(out));
					continue;

				default:
					mon.WriteLog("Invalid command type: %d",type);return false;
				}
			}
			catch(...)
			{
				mon.WriteLog("ERROR: Issue reading JSON MemoryHasher command #%d",key+1);
				return false;
			}
		}
		return true;
	}
	else{
		mon.WriteLog("ERROR: Empty command list");
		return false;
	}
	return true;
}

std::string parseSignatures(std::string out)
{
	//Tool still outputs Encoded array with an error, so need to check for string
	if(out.substr(out.find_first_of('\n')+1,5) == "Error")
		return "error";
	size_t beg = out.find_last_of("[")+1;		//Last array has values for tunables
	size_t end = out.find_last_of("]");
	if(beg == std::string::npos || end == std::string::npos)	//No array output if can't connect to process
		return "fail";
	std::string bonus = out.substr(beg,end-beg);
	return bonus;
}

Json::Value parseBonusRow(std::string row)
{
	std::stringstream ss(row);
	int i;
	Json::Value bonusRow(Json::arrayValue);
	bool neg = false;
	
	while(ss >> i){
		if(neg){
			i = -i;
			neg = false;
		}
		bonusRow.append(i);
		if(ss.peek() == ',')
			ss.ignore();
	}
	return bonusRow;
}