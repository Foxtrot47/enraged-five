﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace guardPoke
{
    public partial class Form1 : Form
    {
        bool dbgAttach;
        int procPid;
        DebuggerWatcher dbg = new DebuggerWatcher();

        public Form1()
        {
            InitializeComponent();
            rvaInput.Text = "Relative Virtual addresses;Semicolon deliminated;      e.g.:    0x100000;0x120000;0x130000";
            dbgAttach = false;
            setTargetPid(0);
        }

        public void logMessage(String msg)
        {
            log.Items.Add(msg);
            if (log.Items.Count > 1000)
                log.Items.RemoveAt(0);
            log.SelectedIndex = log.Items.Count - 1;
            log.ClearSelected();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            logMessage("Guard Poker Initiated");
            Application.Idle += new EventHandler(Form1_Idle);
        }

        private void Form1_Idle(object sender, System.EventArgs e)
        {
            updateGameProcs();
        }

        private void dbgBtn_Click(object sender, EventArgs e)
        {     
            if (!dbgAttach)
            {
                int pid = getPid();
                
                if (pid < 1)                    
                    return;

                logMessage("Starting to attach debugger to target process...");
                int ret = dbg.debugTarget(pid);
                if (ret == 0)
                    logMessage("Debugger attached successfully!");
                else
                    logMessage(String.Format("ERROR: Debugger returned {0} attaching to Process {1}", ret, procPid));
                dbgBtn.Text = "Detach Debugger";
                dbgAttach = true;
            }
            else
            {
                dbg.stopDebug();
                logMessage("Debugger detached from target.");
                dbgBtn.Text = "Attach Debugger";
                dbgAttach = false;
            }

        }

        private void hookBtn_Click(object sender, EventArgs e)
        {
            logMessage("Starting DLL hooking... Oh wait, this doesn't work yet...");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MemoryTickler mt = new MemoryTickler();
            ulong addr = 0;
            int pid = getPid();
            String sProc = getProcName();
            String inAddr = readAddress.Text;
            byte[] buffer = new byte[2];

            if(pid < 1)
                return;           

            try
            {
                addr = Convert.ToUInt64(inAddr, 16);
            }
            catch
            {
                logMessage("Invalid input, please fix. Only hex addresses, with or without '0x'");
                return;
            }


            int ret = mt.readVA(pid, sProc, addr, out buffer,buffer.Length, absolute.Checked);
            if (ret > 0)
                logMessage(String.Format("ERROR: Address poking did not work with Windows System Error {0}", ret));
            else if (ret == -1)
                logMessage("ERROR: Did not read a complete INT from memory address");
            else if (ret == -2)
                logMessage("ERROR: Could not find module to get base address, is the target still there?");

            string hex = BitConverter.ToString(buffer);
            hex = hex.Replace("-", "");
            readData.Text = hex;
            logMessage(String.Format("Read {0} bytes at address {1}", buffer.Length, inAddr));
        }

        private void rvaBtn_Click(object sender, EventArgs e)
        {            
            MemoryTickler mt = new MemoryTickler();
            int pid = getPid();
            if (pid < 1)
                return;

            String sProc = getProcName();
            String input = rvaInput.Text;
            List<ulong> VAs = parseVAs(input);
            if (VAs == null)
            {
                logMessage("No addresses parsed, please paste valid virtual addresses");
                return;
            }

            logMessage("Starting to blast memory at RVA's...");
            foreach (ulong u in VAs)
            {
                logMessage(String.Format("Writing 0xBEEF to address {0:X}", u));
                int ret = mt.tickleVA(pid,sProc,u, absolute.Checked);
                if (ret > 0)
                    logMessage(String.Format("ERROR: Address poking did not work with Windows System Error {0}", ret));
                else if (ret == -1)
                    logMessage("ERROR: Did not write a complete INT to memory address");
                else if (ret == -2)
                    logMessage("ERROR: Could not find module to get base address, is the target still there?");
                else
                    logMessage("Success!");
            }
        }

        private void findProcs_Click(object sender, EventArgs e)
        {
            processes.Items.Clear();
            getGameProcs();
        }       

        private void processes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (processes.SelectedItems.Count < 1)
                return;
            Process p = (Process)processes.SelectedItems[0].Tag;
            setTargetPid(p.Id);
        }


        //Class Methods////////////////////////////////

        public int getPid()
        {
            if (procPid < 1)
                logMessage("No target selected. Please choose a process in the list to select a target");
            else
                return procPid;
            return -1;
        }

        public String getProcName()
        {
            if (processes.SelectedItems.Count < 1)
                return null;
            return processes.SelectedItems[0].Name;
        }

        private List<ulong> parseVAs(String input)
        {
            logMessage("Parsing Virtual Address input");
            
            String[] sVAs = input.Split(';');

            if(sVAs.Length < 1)
            {
                logMessage("Invalid input, please fix. Only hex addresses, with or without '0x', separated by semicolons: 0x1000;0x2000;");
                return null;
            }
            List<ulong> VAs = new List<ulong>();
            foreach(string s in sVAs)
            {
                try
                {
                    ulong i = Convert.ToUInt64(s, 16);
                    VAs.Add(i);
                }
                catch
                {
                    logMessage("Invalid input, please fix. Only hex addresses, with or without '0x', separated by semicolons: 0x1000;0x2000;");
                    return null;
                }                
            }
            return VAs;
        }

        private void getGameProcs()
        {
            String[] targets = {
                "GTA5",
                "game_win64_final",             
                "game_win64_bankrelease",
                "game_win64_release",                
                "game_win64_beta",   
                "GTAVLauncher",
                "PlayGTAV"
            };

            foreach (string t in targets)
            {
                Process[] procs = Process.GetProcessesByName(t);
                if (procs.Count() < 1)
                    continue;
                if (procs.Count() > 1)  //More than one, which one to pick!?
                    logMessage(String.Format("Too many processes with name '{0}', please close one if you wish to use as target", t));
                ListViewItem processList = new ListViewItem();
                processList.Text = procs[0].ProcessName;
                processList.Tag = procs[0];
                processes.Items.Add(processList);
            }
            if (processes.Items.Count == 0)
                logMessage("No targets found, try starting the game...");
        }

        private void updateGameProcs()
        {
            foreach (ListViewItem item in processes.Items)
            {
                Process[] procs = Process.GetProcessesByName(item.Text);
                if (procs.Count() != 1)
                    continue;
                item.Tag = procs[0];
                if (item.Selected)
                    setTargetPid(procs[0].Id);
            }
        }

        private void setTargetPid(int pid)
        {
            procPid = pid;
            if (pid > 1)
                pidBox.Text = procPid.ToString();
            else
                pidBox.Text = "No target";
        }



    }
}
