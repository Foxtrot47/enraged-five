﻿namespace guardPoke
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dbgBtn = new System.Windows.Forms.Button();
            this.hookBtn = new System.Windows.Forms.Button();
            this.rvaBtn = new System.Windows.Forms.Button();
            this.rvaInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.log = new System.Windows.Forms.ListBox();
            this.absolute = new System.Windows.Forms.RadioButton();
            this.relative = new System.Windows.Forms.RadioButton();
            this.findProcs = new System.Windows.Forms.Button();
            this.processes = new System.Windows.Forms.ListView();
            this.GameProcs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pidBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.readAddress = new System.Windows.Forms.TextBox();
            this.readData = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dbgBtn
            // 
            this.dbgBtn.Location = new System.Drawing.Point(29, 12);
            this.dbgBtn.Name = "dbgBtn";
            this.dbgBtn.Size = new System.Drawing.Size(148, 30);
            this.dbgBtn.TabIndex = 0;
            this.dbgBtn.Text = "Attach Debugger";
            this.dbgBtn.UseVisualStyleBackColor = true;
            this.dbgBtn.Click += new System.EventHandler(this.dbgBtn_Click);
            // 
            // hookBtn
            // 
            this.hookBtn.Location = new System.Drawing.Point(29, 50);
            this.hookBtn.Name = "hookBtn";
            this.hookBtn.Size = new System.Drawing.Size(148, 30);
            this.hookBtn.TabIndex = 1;
            this.hookBtn.Text = "HookDLL";
            this.hookBtn.UseVisualStyleBackColor = true;
            this.hookBtn.Click += new System.EventHandler(this.hookBtn_Click);
            // 
            // rvaBtn
            // 
            this.rvaBtn.Location = new System.Drawing.Point(29, 88);
            this.rvaBtn.Name = "rvaBtn";
            this.rvaBtn.Size = new System.Drawing.Size(148, 30);
            this.rvaBtn.TabIndex = 2;
            this.rvaBtn.Text = "Blast Memory";
            this.rvaBtn.UseVisualStyleBackColor = true;
            this.rvaBtn.Click += new System.EventHandler(this.rvaBtn_Click);
            // 
            // rvaInput
            // 
            this.rvaInput.Location = new System.Drawing.Point(15, 160);
            this.rvaInput.Multiline = true;
            this.rvaInput.Name = "rvaInput";
            this.rvaInput.Size = new System.Drawing.Size(260, 126);
            this.rvaInput.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Output";
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.Location = new System.Drawing.Point(15, 305);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(507, 134);
            this.log.TabIndex = 6;
            // 
            // absolute
            // 
            this.absolute.AutoSize = true;
            this.absolute.Location = new System.Drawing.Point(29, 124);
            this.absolute.Name = "absolute";
            this.absolute.Size = new System.Drawing.Size(66, 17);
            this.absolute.TabIndex = 7;
            this.absolute.TabStop = true;
            this.absolute.Text = "Absolute";
            this.absolute.UseVisualStyleBackColor = true;
            // 
            // relative
            // 
            this.relative.AutoSize = true;
            this.relative.Location = new System.Drawing.Point(120, 124);
            this.relative.Name = "relative";
            this.relative.Size = new System.Drawing.Size(64, 17);
            this.relative.TabIndex = 8;
            this.relative.TabStop = true;
            this.relative.Text = "Relative";
            this.relative.UseVisualStyleBackColor = true;
            // 
            // findProcs
            // 
            this.findProcs.Location = new System.Drawing.Point(360, 12);
            this.findProcs.Name = "findProcs";
            this.findProcs.Size = new System.Drawing.Size(120, 23);
            this.findProcs.TabIndex = 10;
            this.findProcs.Text = "Find Game Processes";
            this.findProcs.UseVisualStyleBackColor = true;
            this.findProcs.Click += new System.EventHandler(this.findProcs_Click);
            // 
            // processes
            // 
            this.processes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.GameProcs});
            this.processes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.processes.Location = new System.Drawing.Point(312, 50);
            this.processes.Name = "processes";
            this.processes.Size = new System.Drawing.Size(218, 97);
            this.processes.TabIndex = 11;
            this.processes.UseCompatibleStateImageBehavior = false;
            this.processes.View = System.Windows.Forms.View.Details;
            this.processes.SelectedIndexChanged += new System.EventHandler(this.processes_SelectedIndexChanged);
            // 
            // GameProcs
            // 
            this.GameProcs.Width = 201;
            // 
            // pidBox
            // 
            this.pidBox.Enabled = false;
            this.pidBox.Location = new System.Drawing.Point(222, 22);
            this.pidBox.Name = "pidBox";
            this.pidBox.Size = new System.Drawing.Size(100, 20);
            this.pidBox.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Addresses";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Log Output";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Targets";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(222, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Target Process ID";
            // 
            // readAddress
            // 
            this.readAddress.Location = new System.Drawing.Point(430, 224);
            this.readAddress.Name = "readAddress";
            this.readAddress.Size = new System.Drawing.Size(100, 20);
            this.readAddress.TabIndex = 17;
            // 
            // readData
            // 
            this.readData.Enabled = false;
            this.readData.Location = new System.Drawing.Point(430, 251);
            this.readData.Name = "readData";
            this.readData.Size = new System.Drawing.Size(100, 20);
            this.readData.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(312, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Target Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(312, 251);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Memory (4 bytes)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(360, 180);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Read Memory";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 477);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.readData);
            this.Controls.Add(this.readAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pidBox);
            this.Controls.Add(this.processes);
            this.Controls.Add(this.findProcs);
            this.Controls.Add(this.relative);
            this.Controls.Add(this.absolute);
            this.Controls.Add(this.log);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rvaInput);
            this.Controls.Add(this.rvaBtn);
            this.Controls.Add(this.hookBtn);
            this.Controls.Add(this.dbgBtn);
            this.MaximumSize = new System.Drawing.Size(545, 515);
            this.MinimumSize = new System.Drawing.Size(545, 515);
            this.Name = "Form1";
            this.Text = "Poke the Guards";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button dbgBtn;
        private System.Windows.Forms.Button hookBtn;
        private System.Windows.Forms.Button rvaBtn;
        private System.Windows.Forms.TextBox rvaInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox log;
        private System.Windows.Forms.RadioButton absolute;
        private System.Windows.Forms.RadioButton relative;
        private System.Windows.Forms.Button findProcs;
        private System.Windows.Forms.ListView processes;
        private System.Windows.Forms.TextBox pidBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ColumnHeader GameProcs;
        private System.Windows.Forms.TextBox readAddress;
        private System.Windows.Forms.TextBox readData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}

