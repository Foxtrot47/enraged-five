var React = require('react');
// var MessageList = require('./MessageList.js');
var stuff = "shit"
// const LOG_FILE = "./websocket.log"
var RulesControl = require('./RulesControl');

const LOG_FILE = "./websocket.log"

module.exports = LogReader = React.createClass({

  checkWindowScroll : function(){

  },

  getLogs : function(){
    $.ajax({
      url : "/messages",
      dataType: 'json',
      cache: false,
      success: function(data){
        this.loadMessages(data);
      }.bind(this),
      error: function(xhr, status, err){
        console.error("/messages", status, err.toString());
      }.bind(this)
    });
  },

  loadMessages : function(data){
    // var updates = JSON.parse(data);
    var messages = this.state.messages.concat(data);

    this.setState({messages : messages});
  },

  // Set the initial component state
  getInitialState: function(props){

    props = props || this.props;

    if(!props.messages)
      props.messages = [];

    // Set initial application state using props
    return {
    messages: props.messages,
    msg: "{}",
    data : "",
    target : "",
    count: 0,
    page: 0,
    paging: false,
    skip: 0,
    done: false,
    rules : { 
      "server": { id : 5555,
            wack : 1234 },
      "client": { id : 6666,
            wack : 1234 } 
    }
    };

  },

  componentWillReceiveProps: function(newProps, oldProps){
    this.setState(this.getInitialState(newProps));
  },

  // Called directly after component rendering, only on client
  componentDidMount: function(){

    this.getLogs();
    this.checkConnection();

    // Attach scroll event to the window for infinity paging
    window.addEventListener('scroll', this.checkWindowScroll);

  },

  checkConnection : function() {
    var self = this;
    if(!self.socket || self.socket.readyState !== 1){
      self.socket = new WebSocket('ws://localhost:4444')
      self.socket.onmessage = (evt)=>{ 
        console.log(evt); 
        var msg = JSON.parse(evt.data)
        var msgs = this.state.messages.concat(msg);
        self.setState({messages : msgs});
      }
      self.socket.onclose = (evt)=>{
        setTimeout(self.checkConnection,3000)
      }
      self.socket.onerror = (evt)=>{
        console.error(evt.data)
        setTimeout(self.checkConnection,3000)
      }
    }
  },

  handleTextInput : function(event) {
    this.setState({target: event.target.value})
  },

  handleTextAreaInput : function(event) {
    this.setState({data: event.target.value})
  },

  handleClick : function() {
    
    try{
      var msgObj = {target : JSON.parse(this.state.target), data : JSON.parse(this.state.data)}
      var msg = JSON.stringify(msgObj);
    }catch(err){
      console.error(err)
      return false;
    }
    var msgs = this.state.messages.concat(msgObj);
    this.setState({messages : msgs});
    this.socket.send(msg)
  },

  loadMessage : function(msg) {
    var txt = JSON.stringify(msg.data, undefined, 4)
    var target = JSON.stringify(msg.target);
    this.setState({
      data : txt,
      target : target,
      msg : JSON.stringify(msg)
    })
  },

  // Render the component
  render: function(){
    var msgs = this.state.messages;
    if(!msgs)
      msgs = [];
    return (
      <div className="container col-lg-12">
        <div className="row col-lg-12">
          <div className="container-fluid col-lg-6" >
            <h3> Packet Repeater </h3>
            <div className="form-group-row">
              <div className="container col-md-4">
                <label className="control-label">Target</label>
              <input className="form-control" type="text" value={this.state.target} onChange={this.handleTextInput} />
            </div>

                
            <div className="col-md-6" >
              <label className="control-label">Payload</label>
              <textarea className="form-control span8" rows="5" value={this.state.data} onChange={this.handleTextAreaInput} />
            </div>
          </div>
          <div className="form-group-row">
            <button type="button" className="btn-primary btn-lg" onClick={this.handleClick} >
              Send
            </button>
          </div>
        </div>
        <RulesControl />
      </div>
      <div className="container-fluid messages-list">
        <MessageList messages={msgs} load={this.loadMessage} />
      </div>
    </div>
    
    )

  }

});



var MessageList = React.createClass({


  // Render the component
  render: function(){
    var messages = this.props.messages.slice(0);
    messages.reverse();
    var content = messages.map((message,i)=>{
      return <Message key={i} num={messages.length - i} message={message} load={this.props.load} />
    });

    return (
      <ul className="container-fluid messages">
      <h2> Websocket Log </h2>
      {content}
      </ul>
    )

  }

});


var Message = React.createClass({
  loadMessage : function(msg) {
    this.props.load(msg)
  },

  render: function(){
    var message = this.props.message;
    var name = "container-fluid col-md-4 col-sm-4 col-lg-2";
    var source = ""
    if(message.target.to === 'client'){
      name = name + " clientMessageInfo"
      source = "/images/arrow_green_left.png"
    }
    else{
      name = name + " serverMessageInfo"
      source = "/images/arrow_red_right.png"
    }


    return(
      <div className="row-fluid col-md-12 col-sm-12 message" onClick={this.loadMessage.bind(this,message)} >
        <div className={name}>
          <label className="control-label">#{this.props.num}</label>
          <br/>
          <label className="control-label"> UserID: </label> {message.target.token.UserId}<br/>
          <label className="control-label"> Destination: </label> {message.target.to}<br/>
          <label className="control-label"> Channel: </label> {message.target.channel}<br/>
        </div>
        <div className="col-sm-1 col-md-1">
            <img src={source} className="img-responsive"/>
        </div>
        <div className="container col-md-6 col-sm-6 col-lg-8">
          <div className="span8 content"> {JSON.stringify(message, undefined, 4)} </div>
        </div>

      </div>
    )
  }
})