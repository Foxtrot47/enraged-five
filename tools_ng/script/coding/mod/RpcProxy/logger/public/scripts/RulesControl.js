var React = require('react');
var fs = require('fs');

var RULES_FILE = './rules.json'

module.exports = RulesControl = React.createClass({

	componentDidMount: function(){
		this.grabRules();
	},


	postRules : function(rules) {
		$.post('/rules', rules, ()=>{alert("Rules updated")})
	},
	
	grabRules : function(){
		$.ajax({
	      url : "/rules",
	      dataType: 'json',
	      cache: false,
	      success: function(data){
	        this.loadRules(data)
	      }.bind(this),
	      error: function(xhr, status, err){
	        console.error("/messages", status, err.toString());
	      }.bind(this)
	    });
	},

	loadRules : function(rulesStr) {
		try{
			var rules = JSON.parse(rulesStr)
		}catch(err){
			console.error(err)
		}

		console.log(rules)
		var clientRules = []
		for(var rule in rules.client){
			clientRules.push({key : rule, value : rules.client[rule]})
		}

		var serverRules = []
		for(var rule in rules.server){
			serverRules.push({key : rule, value : rules.server[rule]})
		}

		this.setState({
			rules : rules,
			clientRules : clientRules,
			serverRules : serverRules
		})
	},

	getInitialState: function(props){

	    props = props || this.props;

	    // Set initial application state using props
	    return {
			loaded : false,
			rules : { server : {}, client : {}},
			clientRules : [],
			serverRules : [],
		}
	},

	handleClick : function(str){
		//save
		var rules = {client : {}, server : {}}
		this.state.clientRules.forEach((rule,i)=>{
			rules.client[rule.key] = rule.value;
		})

		this.state.serverRules.forEach((rule,i)=>{
			rules.server[rule.key] = rule.value;
		})

		try{
			var str = JSON.stringify(rules, undefined, 4)
		}catch(err){
			console.error("Rules were not proper JSON")
			console.error(err)
			return false;
		}

		this.postRules(rules);
	},

	addClientRow : function(){
		this.setState({
			clientRules : this.state.clientRules.concat({key: "", value: ""})
		})
	},

	addServerRow : function(){
		this.setState({
			serverRules : this.state.serverRules.concat({key: "", value: ""})	
		})
		
	},

	render : function() {
		var clientRules = this.state.clientRules;
		var serverRules = this.state.serverRules;
		return(
			<div className="row col-lg-6">
				<div className="container-fluid col-lg-6">
					<h3>Client Replace Rules</h3>
					<div className="row">
						<div className="col-sm-4">
						<label className="control-label">Key to Target</label>
						</div>
						<div className="col-sm-4">
						<label className="control-label">Value to Replace</label>
						</div>
					</div>
					{(()=>{
						var rows = []
						clientRules.forEach((rule, i)=>{
							rows.push(
								<div className="row" key={i}>

									<div className="col-md-4 col-sm-4 form-group-row">
									<input className="form-control" type="text" value={this.state.clientRules[i].key} onChange={(evt)=>{
										var clientRules = this.state.clientRules;
										console.log(clientRules)
										console.log(evt.target.value)
										clientRules[i].key = evt.target.value
										console.log(clientRules)
										this.setState({
											clientRules : clientRules
										})}} />
										</div>
									<div className="col-md-4 col-sm-4 form-group-row">
									<input className="form-control" type="text" value={this.state.clientRules[i].value} onChange={(evt)=>{
										var clientRules = this.state.clientRules;
										clientRules[i]['value'] = evt.target.value
										this.setState({
											clientRules : clientRules
										})}} />
									</div>
								</div>)
						})
						return <div>{rows}<button type="button" className="btn-primary center btn-md" onClick={this.addClientRow} > +Row </button></div>;
					})()}
				</div>

				<div className="container-fluid col-lg-6">
					<h3>Server Replace Rules</h3>
					<div className="row">
						<div className="col-sm-4">
						<label className="control-label">Key to Target</label>
						</div>
						<div className="col-sm-4">
						<label className="control-label">Value to Replace</label>
						</div>
					</div>
					{(()=>{
						var rows = [];
						serverRules.forEach((rule, i)=>{
							rows.push(
								<div className="row" key={i}>

									<div className="col-md-4 col-sm-4 form-group-row">
									<input className="form-control" type="text" value={this.state.serverRules[i].key} onChange={(evt)=>{
										var serverRules = this.state.serverRules;
										console.log(serverRules)
										console.log(evt.target.value)
										serverRules[i].key = evt.target.value
										console.log(serverRules)
										this.setState({
											serverRules : serverRules
										})}} />
										</div>
									<div className="col-md-4 col-sm-4 form-group-row">
									<input className="form-control" type="text" value={this.state.serverRules[i].value} onChange={(evt)=>{
										var serverRules = this.state.serverRules;
										serverRules[i]['value'] = evt.target.value
										this.setState({
											serverRules : serverRules
										})}} />
									</div>
								</div>)
						})
						return <div>{rows}<button type="button" className="btn-primary btn-md" onClick={this.addServerRow} > +Row </button></div>;
					})()}
					<br/>
				</div>
				<br/>
				<button type="button" className="btn-primary center btn-lg" onClick={this.handleClick.bind(this,'server')} >
						Save Rules
				</button>
			</div>
		)
	}
})