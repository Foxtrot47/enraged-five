var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var RULES_FILE = './rules.json'

var LOG_FILE = path.join(__dirname, '..\\websocket.log');

module.exports = init = function() {
  app.set('port', (process.env.PORT || 3000));

  app.use('/', express.static(path.join(__dirname, 'public')));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  // Additional middleware which will set headers that we need on each request.
  app.use(function(req, res, next) {
      // Set permissive CORS header - this allows this server to be used only as
      // an API server in conjunction with something like webpack-dev-server.
      res.setHeader('Access-Control-Allow-Origin', '*');

      // Disable caching so we'll always get the latest comments.
      res.setHeader('Cache-Control', 'no-cache');
      next();
  });

  app.get('/messages', function(req, res) {
      try{        
        fs.readFile(LOG_FILE, { encoding : 'utf-8' } ,function(err, data) {
          if (err){
            console.error("Error loading file")
            console.error(err);
            return
          }

          var array = data.split("\n");
          console.log(array)
          var messages = [];
          // console.log(array)
          array.forEach((msg)=>{
            try{
              var msgObj = JSON.parse(msg);
            }catch(err){
              console.error(err)
              return
            }
            messages.push(msgObj);
          })
          res.json(messages);
        })
      }
      catch(err){
        console.error(err)
        res.end()
      }
  });

  app.get('/rules', function(req, res){
    var rules = fs.readFileSync(RULES_FILE,{encoding : 'utf8'})
    res.json(rules);
  })

  app.post('/rules', function(req, res) {  
    try{
      var rules = req.body;
      var rulesStr = JSON.stringify(rules)
      console.log("Saving rules",rulesStr)
      fs.writeFileSync(RULES_FILE, rulesStr, {encoding : 'utf8'})
    }catch(err){
      console.error(err)
      res.send(400)
    }
    res.send(200)
  })

  app.listen(app.get('port'), function() {
    console.log('LogServer accessed at http://localhost:' + app.get('port') + '/');
  });

}


function parseLog(res){
  

  // throw new Error("Read File failed")
}









// function streamLog(){
//     var stream = fs.createReadStream(LOG_FILE, {flags : 'r', encoding : 'utf-8'});

//     stream.on('data', function(d) {
//         buf += d.toString(); // when data is read, stash it in a string buffer
//         pump(); // then process the buffer
//     });


// }

// function pumpMessage() {
//     var pos;

//     while ((pos = buf.indexOf('\n')) >= 0) { // keep going while there's a newline somewhere in the buffer
//         if (pos == 0) { // if there's more than one newline in a row, the buffer will now start with a newline
//             buf = buf.slice(1); // discard it
//             continue; // so that the next iteration will start with data
//         }
//         processLine(buf.slice(0,pos)); // hand off the line
//         buf = buf.slice(pos+1); // and slice the processed data off the buffer
//     }
// }

// function processMessage(line) { // here's where we do something with a line

//     if (line[line.length-1] == '\r') line=line.substr(0,line.length-1); // discard CR (0x0D)

//     if (line.length > 0) { // ignore empty lines
//         var obj = JSON.parse(line); // parse the JSON
//         console.log(obj); // do something with the data here!
//     }
// }