function ScsAccessToken(userId, accountId, rockstarId, platformId) {
	this.UserId = userId;
	this.AccountId = accountId;
	this.RockstarId = rockstarId;
	this.PlatformId = platformId;
	this.Roles = 1; //UserRole.Player
}

//encodes this accessToken to a base64 encoded string.
ScsAccessToken.prototype.encode = function () {
	var tokenStr = JSON.stringify(this);
	var utf8 = unescape(encodeURIComponent(tokenStr));
	var buff = new Buffer(utf8);
	return buff.toString('base64')
}

ScsAccessToken.prototype.decode = function(str) {
	try{
		var tokenObj = JSON.parse(new Buffer(str, 'base64').toString())
		if(!tokenObj.UserId){
			console.error('No UserId field in SCS Token')
			return false
		}if(!tokenObj.AccountId){
			console.error('No AccountId field in SCS Token')
			return false
		}if(!tokenObj.RockstarId){
			console.error('No RockstarId field in SCS Token')
			return false
		}if(!tokenObj.PlatformId){
			console.error('No PlatformId field in SCS Token')
			return false
		}

		this.UserId = tokenObj.UserId;
		this.AccountId = tokenObj.AccountId;
		this.RockstarId = tokenObj.RockstarId;
		this.PlatformId = tokenObj.PlatformId;
		this.Roles = 1;
		return true;

	}catch(err){
		console.error(err);
		return false;
	}
}

module.exports = ScsAccessToken;
	
