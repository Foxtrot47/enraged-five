const WebSocket = require('ws')

var gameserver_uri = 'ws://localhost:8182/'

module.exports = RpcProxyClient = function(target, scsToken, modifyMessage, index, server)
{
	this.websocket = {};
	this.isMux = true;
	this.IsConnected = false;
	this.locked = false;
	this.nextSock = {};
	this.server = server;
	this.index = index;
	this.scsToken = scsToken;
	// if(!target)
	// 	target = gameserver_uri;
	this.target = target;
	if(typeof(modifyMessage) == 'function')
		this.modifyMessage = modifyMessage
}

RpcProxyClient.prototype.connect = function(path, socket) {
	var self = this;

	self.nextSock = socket;
	self.websocket = new WebSocket(self.target + path, ["mux.json-rpc.rockstargames.com","json-rpc.rockstargames.com"]);
	self.init();
}

//Loads helper functions into the class websocket
RpcProxyClient.prototype.init = function(){
	var self = this;

	self.websocket.onopen=function(evt) {
		self.onOpen(evt);
		self.IsConnected = true;
		console.log("Connected to "+self.target);
		console.log("---------------------------------------------\n")
	};
	
	self.websocket.onclose=function(evt) {
		self.IsConnected = false;
		console.log('Disconnected from '+self.target+'\n');
		self.onClose(evt);
	};

	self.websocket.onmessage=function(evt) {
		//Intended for user defined function run on the raw (mux encoded) payload
		self.onMessage(evt);
		self.processMessage(evt);
	};
	
	self.websocket.onerror=function(evt) {
		self.IsConnected = false;
		self.onError(evt);
	};
}

//Init function to be used by the Websocket Server. 
//The callback on 'connection' event returns a websocket, which we load into the class and run the standard init()
RpcProxyClient.prototype.load = function(websocket, server){
	var self = this;

	self.websocket = websocket;
	self.nextSock = server;
	self.IsConnected = (websocket.readyState == 1);

	self.init();
}

RpcProxyClient.prototype.onOpen = function(evt) {}

RpcProxyClient.prototype.onClose = function(evt) {}

RpcProxyClient.prototype.onMessage = function(evt) {}

RpcProxyClient.prototype.onError = function(evt) {
	console.error(evt.message)
}

RpcProxyClient.prototype.processMessage = function(evt) {
	var message = "";
	var self = this;

	console.log(self.scsToken)
	console.log('\nRaw:')
	console.log(evt.data)
	this.blobToAsciiString(evt.data, function(result, channel) {
		console.log("\nChannel: Hex: ",channel.toString('hex'), "\t Int: ",parseInt(channel.toString('hex'), 16))
		console.log('\nASCII: ')
		console.log(result);
		console.log('')

		//User defined function run on the decoded payload
		var msg = self.modifyMessage(result);

		if(msg != result){
			console.log("\nModified Message: ")
			console.log(msg)
			result = msg;
		}

		var message = self.encodeChannelMessage(msg, channel);
		
		if(self.nextSock){
			if(self.nextSock.IsConnected){
				var isServer = (self.target ? true : false);
				
				self.server.log(msg, self.index, isServer, channel, self.scsToken)
				self.nextSock.send(message)
			}
			else{
				console.log("Server not connected...")
			}
		}
		else
			console.log("No server in client")

		console.log("---------------------------------------------\n")
	});
}

RpcProxyClient.prototype.encodeChannelMessage = function (str, channelId) {
	if(!channelId)
	{
		console.log("\n\nNo Channel\n\n")
		var buf = new ArrayBuffer(str.length);
		var view = new Uint8Array(buf);
		for (var i = 0, strLen = str.length; i<strLen; i++)
		{
			view[i] = str.charCodeAt(i);
		}

		return buf;
	}
	else
	{
		var channel = channelId.readInt32BE();
		var buf = new ArrayBuffer(str.length + 4);
		var view = new Uint8Array(buf);
		for (var i = 0, strLen = str.length; i<strLen; i++)
		{
			view[i+4] = str.charCodeAt(i);
		}

		view[0] = (channel >> 24) & 0xFF;
		view[1] = (channel >> 16) & 0xFF;
		view[2] = (channel >> 8) & 0xFF;
		view[3] = (channel >> 0) & 0xFF;

		return buf;
	}
}

RpcProxyClient.prototype.blobToAsciiString = function(buf, callback) {
	var blob = buf.slice(4);
	var channel = buf.slice(0,4);
	if(typeof(channel) == 'string'){
		var buffer = new ArrayBuffer(channel.length)
		var view = new Uint8Array(channel);
		for (var i = 0, strLen = channel.length; i<strLen; i++)
		{
			view[i] = channel.charCodeAt(i);
		}
		channel = new Buffer(channel, 'ascii')
	}
	var text = blob.toString('utf-8');
	callback(text, channel)
}
	
RpcProxyClient.prototype.send = function(message, options) {
	var self = this;
	console.log("Sending:")
	console.log(new Buffer(message, 'ascii'))
	// self.locked = true;
	self.websocket.send(message, options);
}

RpcProxyClient.prototype.unlock = function() {
	this.locked = false;
}

RpcProxyClient.prototype.modifyMessage = function(message) {
	return message;		
}

function generateUri() {    
    var hostIpAddr = gameserver_uri;
    var platformId = 11;
    var accessToken = new ScsAccessToken("trik", 12345, 12345, platformId);
    var base64 = accessToken.encode();
    var wsUri = "ws://" + hostIpAddr + "?auth=" + base64;

    return wsUri;
}