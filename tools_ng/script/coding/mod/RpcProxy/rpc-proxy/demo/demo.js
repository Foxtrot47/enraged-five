var RpcProxy = require('./rpc-proxy')

proxy = new RpcProxy('ws://localhost:8182',8181, modifyMessageToClient, modifyMessageToServer);

function modifyMessageToServer(message){
	var msgObj = JSON.parse(message)

	return JSON.stringify(msgObj);
}

function modifyMessageToClient(message){
	var msgObj = JSON.parse(message)

	return JSON.stringify(msgObj);
}

proxy.init();