var RpcProxyServer = require('./lib/RpcProxyServer.js');

module.exports = RpcProxy = function(target, port, modifyClient, modifyServer, logger)
{
	this.Server = new RpcProxyServer(target, port, modifyClient, modifyServer, logger);
}

RpcProxy.prototype.init = function(){
	var self = this;
	self.Server.start()
}