var RpcProxy = require('./rpc-proxy')
var fs = require('fs')
var logServer = require('./logger/logServer')


proxy = new RpcProxy('ws://localhost:8182',8181, modifyMessageToClient, modifyMessageToServer, 4444);
logServer()

function modifyMessageToServer(message){
	return readRulesFromFile('./rules.json','server',message)
	// return message
}

function modifyMessageToClient(message){
	return readRulesFromFile('./rules.json','client',message)
	// return message
}


function readRulesFromFile(file,path,message){
	try{
		var msgObj = JSON.parse(message)
	}
	catch(e){
		console.error("Message is not valid JSON\n"+e.message)
		return message
	}

	var rulesText = fs.readFileSync(file,'utf8');
	try{
		var allRules = JSON.parse(rulesText)
	}
	catch(e){
		console.error("Rules file not valid JSON\n"+e.message)
		return message;
	}

	if(allRules[path]){
		var rules = allRules[path];
		for(rule in rules){
			if(rules.hasOwnProperty(rule)){
				msgObj[rule] = rules[rule];
			}
		}
	}

	try{
		return JSON.stringify(msgObj);
	}
	catch(e){
		console.error("Modified message is not valid JSON\n"+e.message)
		return message;
	}
}

proxy.init();