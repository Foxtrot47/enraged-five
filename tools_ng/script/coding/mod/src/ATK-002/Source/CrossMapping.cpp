//CrossMapping.cpp
#include "stdafx.h"

/*##################################################################################################*/
::std::unordered_map<uint64_t, uint64_t> nativeHashMap;

uint64_t __HASHMAPDATA[] = {
	0xfc8202efc642e6f2, 0xaf48077651290fac
};

void CrossMapping::initNativeMap() {
	static int init = 0;
	struct twoQwords {
		uint64_t first;
		uint64_t second;
	} *p2q;

	if (init) {
		DEBUGMSG("Already init, nativeHashMap has %lli members", nativeHashMap.size());
		return;
	}

	p2q = reinterpret_cast<twoQwords *>(__HASHMAPDATA);
	//DEBUG_OUT("p2q: %p", p2q);
	//DEBUG_OUT("p2q->first: %llx", p2q->first);
	//DEBUG_OUT("p2q->second: %llx", p2q->second);
	while (p2q->first) {
		//DEBUG_OUT("initNHM: %llx, %llx", p2q->first, p2q->second);
		nativeHashMap.emplace(p2q->first, p2q->second);
		//DEBUG_OUT("nativeHashMap now has %lli members", nativeHashMap.size());
		++p2q;
	}
	init = 1;
	DEBUGMSG("nativeHashMap has %lli members", nativeHashMap.size());
}
/*##################################################################################################*/

static nMap nativeCache;

bool CrossMapping::searchMap(nMap map, uint64_t inNative, uint64_t *outNative)
{
	bool found = false;
	//LOG_DEBUG("inNative 0x%016llx", inNative);
	for (nMap::const_iterator it = map.begin(); it != map.end(); ++it)
	{
		found = (inNative == it->first);
		if (found) {
			*outNative = it->second;
			//LOG_DEBUG("outNative 0x%016llx", outNative);
			break;
		}	
	}

	return found;

}

uint64_t CrossMapping::MapNative(uint64_t inNative)
{
	uint64_t currentNative, outNative;
	bool found = false;

	currentNative = inNative;
	found = searchMap(nativeCache, currentNative, &outNative);
	if (found) return outNative;
	found = searchMap(nativeHashMap, currentNative, &outNative);
	if (found) {
		nativeCache[inNative] = outNative;
		return outNative;
	}

	// Fail safe to prevent LOG_ERROR spam due to ontick run failed natives
	found = std::find(nativeFailedVec.begin(), nativeFailedVec.end(), inNative) != nativeFailedVec.end();
	if (found) {
		return NULL;
	}
	else nativeFailedVec.push_back(inNative);
	Log::Error("Failed to find new hash for 0x%p", inNative);
	return NULL;
}
