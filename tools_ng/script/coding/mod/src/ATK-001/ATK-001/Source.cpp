#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <tlhelp32.h>
#include <Psapi.h>
#include "Log.h"
using namespace std;
#pragma optimize("", off)

HMODULE GetCurrentModule()
{ // NB: XP+ solution!
	HMODULE hModule = NULL;
	GetModuleHandleEx(
		GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
		(LPCTSTR)GetCurrentModule,
		&hModule);

	return hModule;
}

MODULEINFO GetModuleInfo()
{
	MODULEINFO modinfo = {0};
	HMODULE hModule = GetCurrentModule();
	GetModuleInformation(GetCurrentProcess(), hModule, &modinfo, sizeof(MODULEINFO));
	Log::Msg("Returning Module Information");
	return modinfo;
}

DWORD64 FindPattern(char *pattern, char *mask)
{
	bool allfound = false;
	Log::Msg("Getting ModuleInfo");
	MODULEINFO mInfo = GetModuleInfo();
	
	DWORD64 base = (DWORD64)mInfo.lpBaseOfDll;
	DWORD64 size =  (DWORD64)mInfo.SizeOfImage;

	DWORD64 patternLength = (DWORD64)strlen(mask);
	Log::Msg("Base [0x%I64X] Size [0x%I64X]", base, size);

	Log::Msg("Iterating len: %d", patternLength);
	for(DWORD64 i = 0; i < size - patternLength; i++)
	{
		bool found = true;
		for(DWORD64 j = 0; j < patternLength; j++)
		{
			found &= mask[j] == '?' || pattern[j] == *(char*)(base + i + j);
		}

		if(found) 
		{
			allfound = found;
			Log::Msg("Found at [0x%I64X]", base+i);
			return base + i;
		}
	}
	if(allfound)
		Log::Msg("Nothing Found");
	Log::Msg("Returning Null");
	return NULL;
} 

static int char2int(char input)
{
	if(input >= '0' && input <= '9')
		return input - '0';
	if(input >= 'A' && input <= 'F')
		return input - 'A' + 10;
	if(input >= 'a' && input <= 'f')
		return input - 'a' + 10;
	return -1;
}

static void hex2bin(const char* src, char* target)
{
	while(*src && src[1])
	{
		*(target++) = char2int(*src)*16 + char2int(src[1]);
		src += 2;
	}
}

static void AllocateBlock(DWORD protect, const char * strbytes)
{
	
	const unsigned int sizeOfPage = 1024*4;
	const unsigned int sizeOfAlloc = sizeOfPage * 4;
	const unsigned int sizeOfWrite = 16;
	// Start by making it writable
	void * ptr = VirtualAlloc(NULL, sizeOfAlloc,  MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
	DWORD oldProtect = 0;
	memset(ptr, 0, sizeOfAlloc);
	// Write bytes out.
	hex2bin(strbytes, (char*)ptr);
	
	BOOL succ = VirtualProtect(ptr, sizeOfAlloc, protect, &oldProtect);
	Log::Msg("0x%I64X - %d", ptr, protect);
}

static void WINAPI AllocateBlocks()
{
	AllocateBlock(PAGE_EXECUTE			, "DEADBEEF");
	AllocateBlock(PAGE_READONLY			, "BEEFDEAD");
	AllocateBlock(PAGE_READWRITE		, "BEDEADEF");
	AllocateBlock(PAGE_EXECUTE_READ		, "F00DBAAD");
	AllocateBlock(PAGE_EXECUTE_READWRITE, "F0BAAD0D");

}
//WINAPI to initiate the thread

static DWORD WINAPI doWork(LPVOID args)
{
	Log::Msg("======= Stage 1");
	Log::Msg("Finding Pattern");
	DWORD64 rtAddr = FindPattern("\x45\x33\xDB\x44\x8B\xD2\x66\x44\x39\x59\x10\x74\x4B\x44\x0F\xB7\x49\x10\x33\xD2\x41\x8B\xC2\x41\xF7\xF1\x48\x8B\x41\x08\x48\x8B\x0C\xD0\xEB\x09", 
		"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
	//Okay now we need to turn the address into a pointer
	Log::Msg("Assigning Pointers");

	//DWORD64 rtAddr = FindPattern(L"testATK-001Injection.exe", 
	//	"\x44\x89\x44\x24\x18\x89\x54\x24\x10\x48\x89\x4C\x24\x08\x57", 
	//	"xxxxxxxxxxxxxxx");
	unsigned char * fxnPtr;
	fxnPtr = (unsigned char*)rtAddr;
	fxnPtr+=0xB;
	
	HANDLE hProc = GetCurrentProcess();
	DWORD dwOldProtectSystemFunction = 0;
	Log::Msg("Modifying permissions at [0x%I64X]", fxnPtr);
	VirtualProtectEx(hProc, fxnPtr, 2, PAGE_EXECUTE_READWRITE, &dwOldProtectSystemFunction);
	Log::Msg("Setting to nop's", fxnPtr);
	memset(fxnPtr, 0x90, 2);
	Log::Msg("======= Stage 2");
	Log::Msg("Allocating Blocks");
	AllocateBlocks();
	Log::Msg("Exiting");
	
	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD64 fdwReason, LPVOID lpReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		{
			Log::Init(hinstDLL);
			Log::Msg("Creating Thread");
			HANDLE h = CreateThread(NULL,0,doWork, &hinstDLL, 0, NULL);
			return TRUE;
		}
		break;
	case DLL_PROCESS_DETACH:
		{
			return TRUE;
			break;
		}
	}
	return TRUE;
}
