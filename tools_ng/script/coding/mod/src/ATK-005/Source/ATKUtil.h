#pragma once
enum BuildConfig
{
	CONFIG_INVALID,
	CONFIG_GTAV_MASTER,
	CONFIG_GTAV_MASTER_WL,
	CONFIG_GTAV_BETA,
};

namespace ATK005
{
	inline BuildConfig CurrentBuildconfig()
	{
	#if NORMAL
		return CONFIG_GTAV_MASTER;
	#elif WLTWEAKS
		return CONFIG_GTAV_MASTER_WL;
	#elif VBETA
		return CONFIG_GTAV_BETA;
	#else
		return CONFIG_INVALID;
	#endif
	};
}
