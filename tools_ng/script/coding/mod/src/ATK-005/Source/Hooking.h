#pragma once
extern bool doNetshopRepl;
extern bool doNetshopRepl2;
extern bool doNetshopRepl3;

class Hooking
{
public:
	static void Start(HMODULE hmoduleDLL);
	static void Cleanup();
	static eGameState GetGameState();
	static BlipList* GetBlipList();
	static uint64_t getWorldPtr();
	static uint64_t getGlobalPtr();
	static uint64_t getTunablePtr();
	static void onTickInit();
	static bool HookNatives();

	struct RageSecObject
	{
		int type;
		unsigned int id;
		unsigned int version;
		unsigned int address;
		unsigned int data;
	};
	typedef char(__fastcall * RageSecPush)(void * _this, RageSecObject * reaction);
	typedef bool(__fastcall * IsSessionActive)();

	// Native function handler type
	typedef void(__cdecl * NativeHandler)(scrNativeCallContext * context);

	struct NativeRegistration
	{
		NativeRegistration * nextRegistration;
		Hooking::NativeHandler handlers[7];
		uint32_t numEntries;
		uint64_t hashes[7];
	};	
	static NativeHandler GetNativeHandler(uint64_t origHash);

	// rage::scrCommandHash::Lookup()
	typedef NativeHandler(__fastcall * HashLookupFn)(NativeRegistration** nTable, uint64_t hashcode);

	// CNetworkIncrementStatEvent::Trigger()
	// 48 89 5C 24 08 48 89 74  24 10 57 48 83 EC 20 8B F1 48 8B 0D 88 69 6B 01 Lazy
	typedef void(__fastcall * NetworkStatFn)(unsigned int uStatHash, int iStrength, __int64 *pPlayer);

	static void IncrementNetworkStat(unsigned int uStatHash, int iStrength, __int64 *pPlayer);

	// CTheScripts::FindNetworkPlayer()
#ifndef VBETA
	typedef __int64*(__fastcall * FindNetworkPlayerFn)(int playerIndex);
#else
	typedef __int64*(__fastcall * FindNetworkPlayerFn)(int playerIndex, bool bAssert);
#endif

	

	static __int64 * FindNetworkPlayer(int playerIndex);


private:
	static BOOL InitializeHooks();
	static void FindPatterns();
	static void FailPatterns(const char* name, Memory::pattern ptn);
};	void WAIT(DWORD ms);
