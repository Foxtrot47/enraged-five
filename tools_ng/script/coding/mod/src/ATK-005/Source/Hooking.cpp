//Hooking.cpp
#include "stdafx.h"

using namespace Memory;
HMODULE _hmoduleDLL;
HANDLE mainFiber;
DWORD wakeAt;

static eGameState* 											m_gameState;
static uint64_t												m_worldPtr;
static BlipList*											m_blipList;
static Hooking::NativeRegistration**						m_registrationTable;
static std::unordered_map<uint64_t,Hooking::NativeHandler>	m_handlerCache;
static uint64_t												m_globalTable;
static uint64_t												m_tunables;

static Hooking::HashLookupFn								fn_RagLookup;
static Hooking::NetworkStatFn								fn_IncrementStat;
static Hooking::FindNetworkPlayerFn							fn_FindNetworkPlayer;


/* Start Hooking */

void Hooking::Start(HMODULE hmoduleDLL)
{
	_hmoduleDLL = hmoduleDLL;
	Log::Init(hmoduleDLL);
	DEBUGMSG("Hooking Started...");
	FindPatterns();
	if (!InitializeHooks())
	{
		DEBUGMSG("Hook init failed!!!");
		Cleanup();
	}
}

/* Hooks */

void Hooking::IncrementNetworkStat(unsigned int uStatHash, int iStrength, __int64 * pPlayer)
{
	return fn_IncrementStat(uStatHash, iStrength, pPlayer);
}

__int64 * Hooking::FindNetworkPlayer(int playerIndex)
{
#ifndef VBETA
	return fn_FindNetworkPlayer(playerIndex);
#else
	return fn_FindNetworkPlayer(playerIndex, false);
#endif
	
}

// Initialization
BOOL Hooking::InitializeHooks()
{
	BOOL returnVal = TRUE;

	// Input hook
	if (!iHook.Initialize()) {

		Log::Error("Failed to initialize InputHook");
		returnVal = FALSE;
	}

	// init minhook
	if (MH_Initialize() != MH_OK) {
		Log::Error("MinHook failed to initialize");
		returnVal = FALSE;
	}

	// init native hook
	if (!HookNatives()) {

		Log::Error("Failed to initialize NativeHooks");
		returnVal = FALSE;
	}

	return returnVal;
}

/* Native Hook Function  */
template <typename T>
bool Native(DWORD64 hash, LPVOID hookFunction, T** trampoline)
{
	if (*reinterpret_cast<LPVOID*>(trampoline) != NULL)
		return true;
	auto originalFunction = Hooking::GetNativeHandler(hash);
	if (originalFunction != 0) {
		MH_STATUS createHookStatus = MH_CreateHook(originalFunction, hookFunction, reinterpret_cast<LPVOID*>(trampoline));
		if (((createHookStatus == MH_OK) || (createHookStatus == MH_ERROR_ALREADY_CREATED)) && (MH_EnableHook(originalFunction) == MH_OK))
		{
			DEBUGMSG("Hooked Native 0x%#p", hash);
			return true;
		}
		else
		{
			Log::Error("Failed to hook native! MH_STATUS: %i", createHookStatus);
		}
	}
	else
	{
		Log::Error("Original / Handler function is ZERO!!!");
	}

	return false;
}


/* Native Hook Function  */
template <typename T>
bool GenericFunctionHook(LPVOID pPushFunc, LPVOID hookFunction, T** trampoline)
{
	if (*reinterpret_cast<LPVOID*>(trampoline) != NULL)
		return true;
	auto originalFunction = pPushFunc; // This might not work
	if (originalFunction != 0) {
		MH_STATUS createHookStatus = MH_CreateHook(originalFunction, hookFunction, reinterpret_cast<LPVOID*>(trampoline));
		if (((createHookStatus == MH_OK) || (createHookStatus == MH_ERROR_ALREADY_CREATED)) && (MH_EnableHook(originalFunction) == MH_OK))
		{
			DEBUGMSG("Hooked Native 0x%#p", pPushFunc);
			return true;
		}
		else
		{
			Log::Error("Failed to hook native! MH_STATUS: %i", createHookStatus);
		}
	}
	else
	{
		Log::Error("Original function is ZERO!!!");
	}

	return false;
}

Hooking::NativeHandler ORIG_GET_FRAME_COUNT = NULL;
void* __cdecl MY_GET_FRAME_COUNT(NativeContext *cxt)
{
	static int lastFrame = 0;
	int thisFrame = cxt->GetArgument<int>(0);
	if (thisFrame != lastFrame) {
		lastFrame = thisFrame;
		Hooking::onTickInit();
	}	ORIG_GET_FRAME_COUNT(cxt);

	return cxt;
}

Hooking::NativeHandler ORIG_NETSHOP = NULL;
bool doNetshopRepl = false;
bool doNetshopRepl2 = false;
bool doNetshopRepl3 = false;
void* __cdecl MY_NETSHOP(NativeContext *cxt)
{
	if (doNetshopRepl)
	{
		cxt->SetArgument(2, 0xA174F633);
		cxt->SetArgument(4, 15000000);
	}

	if (doNetshopRepl2)
	{
		cxt->SetArgument(2, 0x2A6B291E);
		cxt->SetArgument(4, 25000);
	}
	if (doNetshopRepl3)
	{
		//cxt->SetArgument(1, 0x57DE404E);
		cxt->SetArgument(2, 0x21ECDA63);
		//cxt->SetArgument(3, 0x562592BB);
		cxt->SetArgument(4, 1);
		cxt->SetArgument(5, 1);

		#if NORMAL
		auto pNetshopQuantity = pattern("89 83 E8 00 00 00 C7 83 F0 00 00 00 01 00 00 00 EB 02");
		#elif WLTWEAKS
		auto pNetshopQuantity = pattern("89 87 F0 00 00 00 C7 87 F8 00 00 00 01 00 00 00 EB 02");
		#elif VBETA
		auto pNetshopQuantity = pattern("84 24 80 00 00 00 C7 87 F8 00 00 00 01 00 00 00 89 87");
		#endif
		Memory::putVP(pNetshopQuantity.count(1).get(0).get<void>(12), (DWORD32)0x7FFFFFFF);
		ORIG_NETSHOP(cxt);	
		Memory::putVP(pNetshopQuantity.count(1).get(0).get<void>(12), (DWORD32)0x1);
		
		return cxt;

	}
	ORIG_NETSHOP(cxt);

	return cxt;
}

Hooking::RageSecPush ORIG_RAGE_PUSH = NULL;
char __fastcall MY_RAGE_PUSH(void * _this, Hooking::RageSecObject * o)
{
	Log::Msg("RageHook: Attempted RagSec action %08x... From plugin ID %08x... Blocking security action...", o->type, o->id);
	o->type = 0xD79AD02D; // REACT_INVALID
	return ORIG_RAGE_PUSH(_this, o);
}

Hooking::IsSessionActive ORIG_SESSION_ACTIVE = NULL;
bool __fastcall MY_SESSION_ACTIVE()
{
	return false;
}

bool Hooking::HookNatives()
{
	bool success = true;
	if (ATK005::CurrentBuildconfig() == CONFIG_GTAV_MASTER || ATK005::CurrentBuildconfig() == CONFIG_GTAV_MASTER_WL)
	{
		auto pRagePush = pattern("44 8B 99 08 E0 00 00 4C 8B C9 B9 00 04 00 00 4C 8B D2 44 3B D9 0F 8D AB 00 00 00 45 8B 81 00 E0 00 00 33 C0 41 FF C0 44 3B C1 44 0F 44 C0 41 8B 02");
		success = success && GenericFunctionHook(pRagePush.count(1).get(0).getAddress(), &MY_RAGE_PUSH, &ORIG_RAGE_PUSH);
		if (!success)
			Log::Error("Failed to initialize Ragesec Hook!");
	}
	// session active hook
	auto pSessionActive = pattern("48 8B 0D ? ? ? ? 33 C0 48 85 C9 74 25 39 81");
	success = success && GenericFunctionHook(pSessionActive.count(1).get(0).getAddress(), &MY_SESSION_ACTIVE, &ORIG_SESSION_ACTIVE);
	if(!success)
		Log::Error("Failed to initialize Session Active Hook!");
	// native hooks	
	success = success && Native(0xFC8202EFC642E6F2, &MY_GET_FRAME_COUNT, &ORIG_GET_FRAME_COUNT);
	if (!success)
		Log::Error("Failed to initialize Frame count hook!");
	
	success = success && Native(0x3C5FD37B5499582E, &MY_NETSHOP, &ORIG_NETSHOP);
	if (!success)
		Log::Error("Failed to initialize Netshop Hook!");

	return success;

}

void __stdcall ScriptFunction(LPVOID lpParameter)
{
	try
	{
		ScriptMain();
	}
	catch (...)
	{
		Log::Fatal("Failed scriptFiber");
	}
}

void Hooking::onTickInit()
{
	if (mainFiber == nullptr)
		mainFiber = ConvertThreadToFiber(nullptr);

	if (timeGetTime() < wakeAt)
		return;

	static HANDLE scriptFiber;
	if (scriptFiber)
		SwitchToFiber(scriptFiber);
	else
		scriptFiber = CreateFiber(NULL, ScriptFunction, nullptr);
}

/* Pattern Scanning */

void Hooking::FailPatterns(const char* name, pattern ptn)
{
	Log::Error("finding %s", name);
	Cleanup();
}

void Hooking::FindPatterns()
{
	#ifdef WLTWEAKS
	auto p_fixVector3Result = pattern("83 79 18 00 48 8B D1 74 4A FF 4A 18"); // PASS
	auto p_gameState = pattern("83 3D ? ? ? ? ? 74 0A B9 01 00 00 00");
	auto p_worldPtr = pattern("48 8B 05 ? ? ? ? 45 ? ? ? ? 48 8B 48 08 48 85 C9 74 07"); // PASS
	auto p_blipList = pattern("4C 8D 05 ? ? ? ? 0F B7 C1"); // PASS
	auto p_nativeTable = pattern("76 4D 48 8B 53 40 48 8D 0D"); // FIXED
	auto p_modelCheck = pattern("48 85 C0 0F 84 ? ? ? ? 8B 48 50"); // PASS
	auto p_modelSpawn = pattern("48 8B C8 FF 52 30 84 C0 74 05 48"); // PASS
	auto p_scriptGlobals = pattern("0D ? ? ? ? 48 8B F9 75 53 8B F3 4C 8D 35 ? ? ? ?");
	auto p_ragLookup = pattern("48 89 5C 24 18 48 89 7C 24 20 0F B6 C2 4C 8B D2 4C 8B 04 C1 4D 85 C0 0F 84 89 00 00 00 33 C9 4D"); // rage::scrCommandHash<void (*)(rage::scrThread::Info &)>::Lookup()
	auto p_tunableList = pattern("40 53 48 83 EC 20 48 8B 1D ? ? ? ? BA 64 41 F2 6C B9 BD C5 AF E3");
	auto p_whyPleaseStop = pattern("84 C0 EB 20");
	auto p_networkIncr = pattern("48 89 5C 24 08 48 89 6C 24 10 48 89 74 24 18 57 48 83 EC 20 33 DB 49 8B F8 8B F2 38 1D A5 9E 99 01"); // Lazy please redo
	auto p_networkPlayer = pattern("48 83 EC 28 33 C0 38 05 ? ? ? ? 74 0A 83 F9 1F");
	#elif defined VBETA
	auto p_fixVector3Result = pattern("83 79 18 00 48 8B D1 74 4A FF 4A 18"); // PASS
	auto p_gameState = pattern("83 3D ? ? ? ? ? B3 01 74 61 38 05 ? ? ? ?"); // Updated to CMousePointer::AutomaticallyTurnOn()
	auto p_worldPtr = pattern("48 8B 05 ? ? ? ? 45 ? ? ? 41 0F 28 FB 48 8B 48 08"); // Updated
	auto p_blipList = pattern("48 8D 05 ? ? ? ? 48 8B 04 F8 48 85 C0"); // Updated
	auto p_nativeTable = pattern("76 4D 48 8B 53 40 48 8D 0D"); // Updated (Note to self, redo this patten on all builds to be more generic)
	auto p_scriptGlobals = pattern("01 CC FF 0D ? ? ? ? 75 6B 8B EF 4C 8D 35 ? ? ? ?"); // (Assuming fixed)
	auto p_ragLookup = pattern("48 89 5C 24 18 48 89 7C 24 20 0F B6 C2 4C 8B D2 4C 8B 04 C1 4D 85 C0 0F 84 89 00 00 00 33 C9 4D"); // rage::scrCommandHash<void (*)(rage::scrThread::Info &)>::Lookup()
	auto p_tunableList = pattern("01 85 C0 75 01 CC 48 8B 35 ? ? ? ? 40 8A 3D ? ? ? ? 4C 8D 05 7D");
	//auto p_whyPleaseStop = pattern("84 C0 EB 20");
	auto p_networkIncr = pattern("48 89 5C 24 08 48 89 6C 24 10 48 89 74 24 18 57 48 83 EC 30 49 8B F0 8B EA 8B F9 E8 C4 9E FF FF"); // Lazy please redo
	auto p_networkPlayer = pattern("48 89 5C 24 08 48 89 74 24 10 57 48 83 EC 40 33 DB 40 8A F2 8B F9 38 1D ? ? ? ? 75 ? E8 ? ? ? ?"); // Lazy please redo
	#else
	auto p_fixVector3Result = pattern("83 79 18 00 48 8B D1 74 4A FF 4A 18");
	auto p_gameState = pattern("83 3D ? ? ? ? ? 8A D9 74 0A");
	auto p_worldPtr = pattern("48 8B 05 ? ? ? ? 45 ? ? ? ? 48 8B 48 08 48 85 C9 74 07");
	auto p_blipList = pattern("4C 8D 05 ? ? ? ? 0F B7 C1");
	auto p_nativeTable = pattern("76 32 48 8B 53 40 48 8D 0D");
	auto p_modelCheck = pattern("48 85 C0 0F 84 ? ? ? ? 8B 48 50");
	auto p_modelSpawn = pattern("48 8B C8 FF 52 30 84 C0 74 05 48");
	auto p_scriptGlobals = pattern("0D ?  ?  ?  ? 48 8B F9 75 40 48 8D 35 ?  ?  ?  ?"); // Moved to more generic pattern for smugglers                           
	auto p_ragLookup = pattern("48 89 5C 24 18 48 89 7C 24 20 0F B6 C2 4C 8B D2 4C 8B 04 C1 4D 85 C0 0F 84 89 00 00 00 33 C9 4D"); // rage::scrCommandHash<void (*)(rage::scrThread::Info &)>::Lookup()
	auto p_tunableList = pattern("40 53 48 83 EC 20 48 8B 1D ? ? ? ? BA 64 41 F2 6C B9 BD C5 AF E3");
	auto p_whyPleaseStop = pattern("84 C0 EB 20");
	auto p_networkIncr = pattern("48 89 5C 24 08 48 89 74 24 10 57 48 83 EC 20 8B F1 48 8B 0D 88 69 6B 01"); // Lazy please redo
	auto p_networkPlayer = pattern("48 83 EC 28 33 C0 38 05 ? ? ? ? 74 0A 83 F9 1F"); // Lazy please redo
	#endif
	char * c_location = nullptr;

	// Executable Base Address
	DEBUGMSG("baseAddr\t\t 0x%p", get_base());

	// Executable End Address
	DEBUGMSG("endAddr\t\t 0x%p", get_base() + get_size());	

	// Get game state
	c_location = p_gameState.count(1).get(0).get<char>(2);
	DEBUGMSG("Finding Game State...");
	while (c_location == nullptr) Sleep(60);
	c_location == nullptr ? FailPatterns("gameState", p_gameState) : m_gameState = reinterpret_cast<decltype(m_gameState)>(c_location + *(int32_t*)c_location + 5);

	// Get vector3 result fixer function
	auto void_location = p_fixVector3Result.count(1).get(0).get<void>(0);
	if (void_location != nullptr) scrNativeCallContext::SetVectorResults = (void(*)(scrNativeCallContext*))(void_location);


	// Get native registration table
	c_location = p_nativeTable.count(1).get(0).get<char>(9);
	c_location == nullptr ? FailPatterns("native registration Table", p_nativeTable) : m_registrationTable = reinterpret_cast<decltype(m_registrationTable)>(c_location + *(int32_t*)c_location + 4);

	// Get native registration table
	c_location = p_ragLookup.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("native lookup function", p_ragLookup) : fn_RagLookup = reinterpret_cast<decltype(fn_RagLookup)>(c_location);


#ifdef VBETA
	// Get network increment
	c_location = p_networkIncr.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("network stat increment function", p_networkIncr) : fn_IncrementStat = reinterpret_cast<decltype(fn_IncrementStat)>(c_location);

	// Get network player
	c_location = p_networkPlayer.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("network player find function", p_networkPlayer) : fn_FindNetworkPlayer = reinterpret_cast<decltype(fn_FindNetworkPlayer)>(c_location);
#endif // VBETA

#ifndef VBETA
	// Get network increment
	c_location = p_networkIncr.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("network stat increment function", p_networkIncr) : fn_IncrementStat = reinterpret_cast<decltype(fn_IncrementStat)>(c_location);

	// Get network player
	c_location = p_networkPlayer.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("network player find function", p_networkPlayer) : fn_FindNetworkPlayer = reinterpret_cast<decltype(fn_FindNetworkPlayer)>(c_location);
#endif // !VBETA

	// Get world pointer
	c_location = p_worldPtr.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("world Pointer", p_worldPtr) : m_worldPtr = reinterpret_cast<uint64_t>(c_location) + *reinterpret_cast<int*>(reinterpret_cast<uint64_t>(c_location) + 3) + 7;
	
	DEBUGMSG("World pointer at %p", m_worldPtr);
#ifndef VBETA
	c_location = p_tunableList.count(1).get(0).get<char>(9);
	c_location == nullptr ? FailPatterns("tunables Pointer", p_worldPtr) : m_tunables = reinterpret_cast<uint64_t>(c_location + *(uint32_t*)c_location + 4);
#endif
	

	DEBUGMSG("Tunables pointer at %p", m_tunables);


	// Get blip list
	c_location = p_blipList.count(1).get(0).get<char>(0);
	c_location == nullptr ? FailPatterns("blip List", p_blipList) : m_blipList = (BlipList*)(c_location + *reinterpret_cast<int*>(c_location + 3) + 7);

	// Script globals
	#if WLTWEAKS || VBETA
	c_location = p_scriptGlobals.count(1).get(0).get<char>(15); // Moved up by one byte
	c_location == nullptr ? FailPatterns("global script table", p_scriptGlobals) : m_globalTable = reinterpret_cast<uint64_t>(c_location + *(uint32_t*)c_location + 4);
	#else
	c_location = p_scriptGlobals.count(1).get(0).get<char>(13);
	c_location == nullptr ? FailPatterns("global script table", p_scriptGlobals) : m_globalTable = reinterpret_cast<uint64_t>(c_location + *(uint32_t*)c_location + 4);
	#endif

	DEBUGMSG("Script Global Table at %p", m_globalTable);

	//c_location = p_whyPleaseStop.count(1).get(0).get<char>(0);
	//*c_location = (char)0xB0;
	//*(c_location + 1) = (char)0x00;
	
	// Disable fixups on logging (are these even needed?)
	#if NORMAL
	// Bypass online model requests block
	Memory::nop(p_modelCheck.count(1).get(0).get<void>(0), 24);

	// Bypass is player model allowed to spawn checks
	Memory::nop(p_modelSpawn.count(1).get(0).get<void>(8), 2);
	#endif

	DEBUGMSG("Initializing natives");
	CrossMapping::initNativeMap();


	// Check if game is ready
	Log::Msg("Checking if game is ready...");
	while (!*m_gameState == GameStatePlaying) {
		Sleep(100);
	}
	
	Log::Msg("Game ready");
}

static Hooking::NativeHandler _Handler(uint64_t origHash) {

	uint64_t newHash = CrossMapping::MapNative(origHash);
	if (newHash == 0) {
		return nullptr;
	}

	return fn_RagLookup(m_registrationTable, newHash);

}

Hooking::NativeHandler Hooking::GetNativeHandler(uint64_t origHash)
{
	auto& handler = m_handlerCache[origHash];

	if (handler == nullptr)
	{
		handler = _Handler(origHash);
	}

	if (!handler)
	{
		Log::Error("Unable to find handler for %08X!!!", origHash);
	}

	return handler;
}

eGameState Hooking::GetGameState()
{
	return *m_gameState;
}

BlipList* Hooking::GetBlipList()
{
	return m_blipList;
}
uint64_t Hooking::getWorldPtr()
{
	return m_worldPtr;
}

uint64_t Hooking::getTunablePtr()
{
	return m_tunables;
}

uint64_t Hooking::getGlobalPtr()
{
	return m_globalTable;
}
void WAIT(DWORD ms)
{
	wakeAt = timeGetTime() + ms;
	SwitchToFiber(mainFiber);
}

/* Clean Up */
void Hooking::Cleanup()
{
	Log::Msg("CleanUp: RIM");

	iHook.keyboardHandlerUnregister(OnKeyboardMessage);
	iHook.Remove();
	MH_Uninitialize();
	FreeLibraryAndExitThread(_hmoduleDLL, 0);
}