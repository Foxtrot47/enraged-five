// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#define IM_ARRAYSIZE(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))		// ImGui Arrays

// Windows Library Files:
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "winmm.lib")

// Windows Header Files:
#include <windows.h>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <bcrypt.h>
#include <tchar.h>
#include <memory>
#include <Psapi.h>
#include <MinHook.h>
#include <timeapi.h>
#include <time.h>

// D3d Related Files
#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <DXGI.h>
#include <d3d11.h>
#include "D3d11Hook.h"
//#define DIRECTINPUT_VERSION 0x0800
//#include <dinput.h>

// Additional Header Files:
#include "Log.h"
#include "Memory.h"
#include "types.h"
#include "enums.h"
#include "InputHook.h"
#include "keyboard.h"
#include "CrossMapping.h"
#include "NativeInvoker.h"
#include "nativeCaller.h"
#include "natives.h"
#include "Hooking.h"
#include "Functions.h"
#include "script.h"
#include "menu.h"