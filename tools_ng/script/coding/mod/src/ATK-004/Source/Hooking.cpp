//Hooking.cpp
#include "stdafx.h"

using namespace Memory;
HANDLE mainFiber;
DWORD wakeAt;

static eGameState* 											m_gameState;
static uint64_t												m_worldPtr;
static BlipList*											m_blipList;
static Hooking::NativeRegistration**						m_registrationTable;
HMODULE 													Hooking::_hmoduleDLL;
HWND 														Hooking::hWindow;
static std::unordered_map<uint64_t,Hooking::NativeHandler>	m_handlerCache;

/* Start Hooking */

void Hooking::Start(HMODULE hmoduleDLL)
{
	_hmoduleDLL = hmoduleDLL;

	// init logfile
	Log::Init(hmoduleDLL);
	MH_Initialize();
	hWindow = NULL;
	while (hWindow == NULL)
	{
		hWindow = FindWindow(L"grcWindow", NULL);
		Sleep(100);
	}

	//FindPatterns();

	if (!InitializeHooks()) Cleanup();
}

/* Hooks */

// Function Pointers
NtQueryVirtualMemoryFnPtr NtQueryVirtualMemory;
NtQuerySystemInformationFnPtr NtQuerySystemInformation;
NtQueryInformationProcessFnPtr NtQueryInformationProcess;
NtOpenProcessFnPtr NtOpenProcess;

VirtualQueryExFnPtr VirtualQueryEx_Trampoline;
NtQueryVirtualMemoryFnPtr NtQueryVirtualMemory_Trampoline;
NtQuerySystemInformationFnPtr NtQuerySystemInformation_Trampoline;
OpenProcessFnPtr OpenProcess_Trampoline;
NtQueryInformationProcessFnPtr NtQueryInformationProcess_Trampoline;
NtOpenProcessFnPtr NtOpenProcess_Trampoline;
GetModuleHandleAFnPtr GetModuleHandleA_Trampoline;
GetModuleHandleExAFnPtr GetModuleHandleExA_Trampoline;
GetModuleFileNameAFnPtr GetModuleFileNameA_Trampoline;
GetModuleHandleWFnPtr GetModuleFileNameW_Trampoline;
ReadProcessMemoryFnPtr ReadProcessMemory_Trampoline;
SIZE_T VirtualQueryEx_Detour(
	__in     HANDLE hProcess,
	__in_opt LPCVOID lpAddress,
	__out_bcount_part(dwLength, return) PMEMORY_BASIC_INFORMATION lpBuffer,
	__in     SIZE_T dwLength)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("VirtualQueryEx Detour");
		ran = true;
	}
	return VirtualQueryEx(hProcess, lpAddress, lpBuffer, dwLength);
}

NTSTATUS NtQueryVirtualMemory_Detour(IN HANDLE               ProcessHandle,
	_In_ PVOID                BaseAddress,
	_In_ MEMORY_INFORMATION_CLASS MemoryInformationClass,
	_Out_ PVOID               Buffer,
	_In_ size_t                Length,
	_Out_ size_t*	              ResultLength)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("NtQueryVirtualMemory Detour");
		ran = true;
	}
	return NtQueryVirtualMemory_Trampoline(ProcessHandle, BaseAddress, MemoryInformationClass, Buffer, Length, ResultLength);
}

NTSTATUS NtQuerySystemInformation_Detour(
	_In_       SYSTEM_INFORMATION_CLASS SystemInformationClass,
	_Inout_    PVOID SystemInformation,
	_In_       size_t SystemInformationLength,
	_Out_opt_  size_t* ReturnLength)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("NtQuerySystemInformation Detour");
		ran = true;
	}
	return NtQuerySystemInformation_Trampoline(SystemInformationClass, SystemInformation, SystemInformationLength, ReturnLength);
}

HANDLE OpenProcess_Detour(
	__in DWORD dwDesiredAccess,
	__in BOOL bInheritHandle,
	__in DWORD dwProcessId)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("NtQuerySystemInformation Detour");
		ran = true;
	}
	return OpenProcess_Trampoline(dwDesiredAccess, bInheritHandle, dwProcessId);
}


NTSTATUS NtOpenProcess_Detour(
	_Out_     PHANDLE            ProcessHandle,
	_In_      ACCESS_MASK        DesiredAccess,
	_In_      POBJECT_ATTRIBUTES ObjectAttributes,
	_In_opt_  PVOID				ClientId)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("NtOpenProcess Detour");
		ran = true;
	}
	return NtOpenProcess_Trampoline(ProcessHandle, DesiredAccess, ObjectAttributes, ClientId);
}

HMODULE GetModuleHandleA_Detour(
	_In_opt_ LPCSTR lpModuleName)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("GetModuleHandleA Detour");
		ran = true;
	}
	return GetModuleHandleA_Trampoline(lpModuleName);
}

BOOL GetModuleHandleExA_Detour(
	_In_ DWORD dwFlags,
	_In_opt_ LPCSTR lpModuleName,
	_Out_ HMODULE * phModule
)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("GetModuleHandleExA Detour");
		ran = true;
	}
	return GetModuleHandleExA_Trampoline(dwFlags, lpModuleName, phModule);
}

DWORD  GetModuleFileNameA_Detour(
	_In_opt_ HMODULE hModule,
	_Out_writes_to_(nSize, ((return < nSize) ? (return +1) : nSize)) LPSTR lpFilename,
	_In_ DWORD nSize
)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("GetModuleFileNameA Detour");
		ran = true;
	}
	return GetModuleFileNameA_Trampoline(hModule, lpFilename, nSize);
}

HMODULE  GetModuleFileNameW_Detour(
	_In_opt_ HMODULE hModule,
	_Out_writes_to_(nSize, ((return < nSize) ? (return +1) : nSize)) LPWSTR lpFilename,
	_In_ DWORD nSize
)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("GetModuleFileNameW Detour");
		ran = true;
	}
	return GetModuleFileNameW_Trampoline(hModule, lpFilename, nSize);
}

BOOL ReadProcessMemory_Detour(
	__in      HANDLE hProcess,
	__in      LPCVOID lpBaseAddress,
	__out_bcount_part(nSize, *lpNumberOfBytesRead) LPVOID lpBuffer,
	__in      SIZE_T nSize,
	__out_opt SIZE_T * lpNumberOfBytesRead
)
{
	static bool ran = false;
	if (!ran)
	{
		Log::Msg("ReadProcessMemory Detour");
		ran = true;
	}
	return ReadProcessMemory_Trampoline(hProcess, lpBaseAddress, lpBuffer, nSize, lpNumberOfBytesRead);
}

BOOL Hooking::InitializeHooks()
{
	HMODULE ntHandle = GetModuleHandle(L"ntdll.dll");
	HMODULE kHandle = GetModuleHandle(L"kernel32.dll");

	NtQueryVirtualMemory = (NtQueryVirtualMemoryFnPtr)GetProcAddress(ntHandle, "NtQueryVirtualMemory");
	NtQuerySystemInformation = (NtQuerySystemInformationFnPtr)GetProcAddress(ntHandle, "NtQuerySystemInformation");
	NtQueryInformationProcess = (NtQueryInformationProcessFnPtr)GetProcAddress(ntHandle, "NtQueryInformationProcess");
	
	// NtQuerySystemInformation
	if (MH_CreateHook(VirtualQueryEx, VirtualQueryEx_Detour, reinterpret_cast<void**>(&VirtualQueryEx_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for VirtualQueryEx"); return 1; }
	if (MH_EnableHook(VirtualQueryEx) != MH_OK) { Log::Error("Failed to Enable Hook for VirtualQueryEx"); return 1; }

	// NtQueryVirtualMemory
	if (MH_CreateHook(NtQueryVirtualMemory, NtQueryVirtualMemory_Detour, reinterpret_cast<void**>(&NtQueryVirtualMemory_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for NtQueryVirtualMemory"); return 1; }
	if (MH_EnableHook(NtQueryVirtualMemory) != MH_OK) { Log::Error("Failed to Enable Hook for NtQueryVirtualMemory"); return 1; }

	// NtQuerySystemInformation
	if (MH_CreateHook(NtQuerySystemInformation, NtQuerySystemInformation_Detour, reinterpret_cast<void**>(&NtQuerySystemInformation_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for NtQuerySystemInformation"); return 1; }
	if (MH_EnableHook(NtQuerySystemInformation) != MH_OK) { Log::Error("Failed to Enable Hook for NtQuerySystemInformation"); return 1; }

	// OpenProcess
	if (MH_CreateHook(OpenProcess, OpenProcess_Detour, reinterpret_cast<void**>(&OpenProcess_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for OpenProcess"); return 1; }
	if (MH_EnableHook(OpenProcess) != MH_OK) { Log::Error("Failed to Enable Hook for OpenProcess"); return 1; }

	// NtOpenProcess
	//if (MH_CreateHook(NtOpenProcess, NtOpenProcess_Detour, reinterpret_cast<void**>(&NtOpenProcess_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for NtOpenProcess"); return 1; }
	//if (MH_EnableHook(NtOpenProcess) != MH_OK) { Log::Error("Failed to Enable Hook for NtOpenProcess"); return 1; }

	// GetModuleHandleA
	if (MH_CreateHook(GetModuleHandleA, GetModuleHandleA_Detour, reinterpret_cast<void**>(&GetModuleHandleA_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for GetModuleHandleA"); return 1; }
	if (MH_EnableHook(GetModuleHandleA) != MH_OK) { Log::Error("Failed to Enable Hook for GetModuleHandleA"); return 1; }

	// GetModuleHandleExA
	if (MH_CreateHook(GetModuleHandleExA, GetModuleHandleExA_Detour, reinterpret_cast<void**>(&GetModuleHandleExA_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for GetModuleHandleExA"); return 1; }
	if (MH_EnableHook(GetModuleHandleExA) != MH_OK) { Log::Error("Failed to Enable Hook for GetModuleHandleExA"); return 1; }

	// GetModuleHandleExA
	if (MH_CreateHook(GetModuleFileNameA, GetModuleFileNameA_Detour, reinterpret_cast<void**>(&GetModuleFileNameA_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for GetModuleFileNameA"); return 1; }
	if (MH_EnableHook(GetModuleFileNameA) != MH_OK) { Log::Error("Failed to Enable Hook for GetModuleFileNameA"); return 1; }

	// GetModuleHandleExA
	if (MH_CreateHook(GetModuleFileNameW, GetModuleFileNameW_Detour, reinterpret_cast<void**>(&GetModuleFileNameW_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for GetModuleFileNameW"); return 1; }
	if (MH_EnableHook(GetModuleFileNameW) != MH_OK) { Log::Error("Failed to Enable Hook for GetModuleFileNameW"); return 1; }

	// GetModuleHandleExA
	if (MH_CreateHook(ReadProcessMemory, ReadProcessMemory_Detour, reinterpret_cast<void**>(&ReadProcessMemory_Trampoline)) != MH_OK) { Log::Error("Failed to Create Hook for ReadProcessMemory"); return 1; }
	if (MH_EnableHook(ReadProcessMemory) != MH_OK) { Log::Error("Failed to Enable Hook for GetModuleFileNameW"); return 1; }

	// D3D hooks
	DX11.reset(new DX11Hook());
	DX11->InitDevice(hWindow);

	return TRUE;
}

/* Clean Up */
void Hooking::Cleanup()
{
	DX11->CleanupDeviceD3D();
	MH_Uninitialize();
	FreeLibraryAndExitThread(_hmoduleDLL, 0);
}