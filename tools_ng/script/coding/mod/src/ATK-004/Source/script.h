#pragma once

class Script
{
public:
	static void isInit();
	static void onTickNative();
}; extern uint RequestedModel;
enum RequestState
{
	loaded,
	requesting
}; 
extern RequestState model_state;
extern void(*modelFunction)();

// Booleans
extern bool playerControl;
extern bool mobileRadio;
extern bool mobileRadioUpdated;