#include "stdafx.h"

using namespace ImGui;

/* COLORS */
ImVec4 clear_col = ImColor(114, 144, 154);

/* BOOLEANS */
static bool is_menu_open = true;
static bool show_test_window = false;
static bool show_another_window = false;

BOOLEAN Menu::Init()
{
	return TRUE;
}

void Menu::Render()
{
	// start draw
	ImGui_ImplDX11_NewFrame();

	static bool no_titlebar = false;
	static bool no_border = true;
	static bool no_resize = false;
	static bool auto_resize = false;
	static bool no_move = false;
	static bool no_scrollbar = false;
	static bool no_collapse = false;
	static bool no_menu = true;
	static bool start_pos_set = false;

	// various window flags. Typically you would just use the default.
	ImGuiWindowFlags	window_flags = 0;
	if (no_titlebar)	window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (!no_border)		window_flags |= ImGuiWindowFlags_ShowBorders;
	if (no_resize)		window_flags |= ImGuiWindowFlags_NoResize;
	if (auto_resize)	window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
	if (no_move)		window_flags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar)	window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)	window_flags |= ImGuiWindowFlags_NoCollapse;
	if (!no_menu)		window_flags |= ImGuiWindowFlags_MenuBar;
	ImGui::SetNextWindowSize(ImVec2(550, 680), ImGuiSetCond_FirstUseEver);
	if (!start_pos_set) { ImGui::SetNextWindowPos(ImVec2(10, 10)); start_pos_set = true; }

	//if (trainer_switch_pressed()) is_menu_open = !is_menu_open;

	ImGui::GetIO().MouseDrawCursor = is_menu_open;
	
	if (is_menu_open)
	{
		ImGui::Begin("ATK-004", &is_menu_open, window_flags);

		//ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.65f);    // 2/3 of the space for widget and 1/3 for labels
		ImGui::PushItemWidth(-140);                                 // Right align, keep 140 pixels for labels

		ImGui::TextColored(ImColor(0, 128, 255), "ATK-004"); 

		ImGui::End();
	}
	
	// Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (show_test_window)
	{
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
		ImGui::ShowTestWindow(&show_test_window);
	}

	ImGui::Render();


	IDXGIAdapter * pAdapter;
	std::vector <IDXGIAdapter*> vAdapters;
	IDXGIFactory* pFactory = NULL;

	// Create a DXGIFactory object.
	if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory)))
	{

	}

	static bool printOnce = false;

	if(!printOnce)
	{
		for (UINT i = 0;
			pFactory->EnumAdapters(i, &pAdapter) != DXGI_ERROR_NOT_FOUND;
			++i)
		{
			vAdapters.push_back(pAdapter);
			Log::Msg("Adapter %d at 0x%I64X", i, pAdapter);
			Log::Msg("Adapter %d at 0x%I64X", i, &pAdapter);
		}
		if (pFactory)
		{
			pFactory->Release();
		}
		printOnce = true;
	}
}

bool Menu::IsOpen()
{
	return is_menu_open;
}
