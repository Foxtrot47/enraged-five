#pragma once

#include <Ntsecapi.h>
#include <psapi.h>
typedef enum _MEMORY_INFORMATION_CLASS {
	MemoryBasicInformation,
	MemoryWorkingSetList,
	MemorySectionName,
	MemoryBasicVlmInformation,
	Last_MEMORY_INFORMATION_CLASS = 0xffffffffffffffff	// Need to force this enum to be 64bit
} MEMORY_INFORMATION_CLASS;


typedef enum _SYSTEM_INFORMATION_CLASS {
	SystemBasicInformation = 0,
	SystemProcessInformation = 5,
	Last_SYSTEM_INFORMATION_CLASS = 0xffffffffffffffff	// Need to force this enum to be 64bit
} SYSTEM_INFORMATION_CLASS;

typedef struct _SYSTEM_PROCESS_INFORMATION // Size=200
{
	ULONG NextEntryOffset; // Size=4 Offset=0
	ULONG NumberOfThreads; // Size=4 Offset=4
	LARGE_INTEGER WorkingSetPrivateSize; // Size=8 Offset=8
	ULONG HardFaultCount; // Size=4 Offset=16
	ULONG NumberOfThreadsHighWatermark; // Size=4 Offset=20
	ULONGLONG CycleTime; // Size=8 Offset=24
	LARGE_INTEGER CreateTime; // Size=8 Offset=32
	LARGE_INTEGER UserTime; // Size=8 Offset=40
	LARGE_INTEGER KernelTime; // Size=8 Offset=48
	UNICODE_STRING ImageName; // Size=8 Offset=56
	LONG BasePriority; // Size=4 Offset=64
	SIZE_T UniqueProcessId; // Size=4 Offset=68
	SIZE_T InheritedFromUniqueProcessId; // Size=4 Offset=76
	ULONG HandleCount; // Size=4 Offset=84
	ULONG SessionId; // Size=4 Offset=88
	ULONG UniqueProcessKey; // Size=4 Offset=92
	SIZE_T PeakVirtualSize; // Size=4 Offset=96
	SIZE_T VirtualSize; // Size=4 Offset=104
	ULONG PageFaultCount; // Size=4 Offset=112
	ULONG PeakWorkingSetSize; // Size=4 Offset=116
	ULONG WorkingSetSize; // Size=4 Offset=120
	ULONG QuotaPeakPagedPoolUsage; // Size=4 Offset=124
	ULONG QuotaPagedPoolUsage; // Size=4 Offset=128
	ULONG QuotaPeakNonPagedPoolUsage; // Size=4 Offset=132
	ULONG QuotaNonPagedPoolUsage; // Size=4 Offset=136
	ULONG PagefileUsage; // Size=4 Offset=140
	ULONG PeakPagefileUsage; // Size=4 Offset=144
	ULONG PrivatePageCount; // Size=4 Offset=148
	LARGE_INTEGER ReadOperationCount; // Size=8 Offset=152
	LARGE_INTEGER WriteOperationCount; // Size=8 Offset=160
	LARGE_INTEGER OtherOperationCount; // Size=8 Offset=168
	LARGE_INTEGER ReadTransferCount; // Size=8 Offset=176
	LARGE_INTEGER WriteTransferCount; // Size=8 Offset=184
	LARGE_INTEGER OtherTransferCount; // Size=8 Offset=192
} SYSTEM_PROCESS_INFORMATION;

typedef struct _OBJECT_ATTRIBUTES {
	ULONG           Length;
	HANDLE          RootDirectory;
	PUNICODE_STRING ObjectName;
	ULONG           Attributes;
	PVOID           SecurityDescriptor;
	PVOID           SecurityQualityOfService;
}  OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

typedef NTSTATUS(WINAPI *NtQueryVirtualMemoryFnPtr)(IN HANDLE               ProcessHandle,
	_In_ PVOID                BaseAddress,
	_In_ MEMORY_INFORMATION_CLASS MemoryInformationClass,
	_Out_ PVOID               Buffer,
	_In_ size_t                Length,
	_Out_ size_t*	              ResultLength);

typedef NTSTATUS(WINAPI *NtQuerySystemInformationFnPtr)(
	_In_       SYSTEM_INFORMATION_CLASS SystemInformationClass,
	_Inout_    PVOID SystemInformation,
	_In_       size_t SystemInformationLength,
	_Out_opt_  size_t* ReturnLength
	);

typedef NTSTATUS(WINAPI *NtQueryInformationProcessFnPtr)(
	_In_       HANDLE ProcessHandle,
	_In_       PROCESS_INFORMATION_CLASS ProcessInformationClass,
	_Out_      PVOID ProcessInformation,
	_In_       size_t ProcessInformationLength,
	_Out_opt_  size_t* ReturnLength
	);

typedef NTSTATUS(*NtOpenProcessFnPtr)(
	_Out_     PHANDLE            ProcessHandle,
	_In_      ACCESS_MASK        DesiredAccess,
	_In_      POBJECT_ATTRIBUTES ObjectAttributes,
	_In_opt_  PVOID				ClientId
	);

typedef SIZE_T (*VirtualQueryExFnPtr)(
	_In_ HANDLE hProcess,
	_In_opt_ LPCVOID lpAddress,
	_Out_writes_bytes_to_(dwLength, return) PMEMORY_BASIC_INFORMATION lpBuffer,
	_In_ SIZE_T dwLength
);

typedef HANDLE (*OpenProcessFnPtr)(
	__in DWORD dwDesiredAccess,
	__in BOOL bInheritHandle,
	__in DWORD dwProcessId
);

typedef HMODULE (*GetModuleHandleAFnPtr)(
	_In_opt_ LPCSTR lpModuleName
);

typedef BOOL (*GetModuleHandleExAFnPtr)(
	__in        DWORD    dwFlags,
	__in_opt    LPCSTR lpModuleName,
	__out HMODULE* phModule
);

typedef DWORD (*GetModuleFileNameAFnPtr)(
	__in_opt HMODULE hModule,
	__out_ecount_part(nSize, return +1) LPSTR lpFilename,
	__in     DWORD nSize
);

typedef HMODULE (*GetModuleHandleWFnPtr)(
	_In_opt_ HMODULE hModule,
	_Out_writes_to_(nSize, ((return < nSize) ? (return +1) : nSize)) LPWSTR lpFilename,
	_In_ DWORD nSize
);

typedef BOOL (*ReadProcessMemoryFnPtr)(
	__in      HANDLE hProcess,
	__in      LPCVOID lpBaseAddress,
	__out_bcount_part(nSize, *lpNumberOfBytesRead) LPVOID lpBuffer,
	__in      SIZE_T nSize,
	__out_opt SIZE_T * lpNumberOfBytesRead
);

class Hooking
{
public:
	static void Start(HMODULE hmoduleDLL);
	static void Cleanup();
	static HWND getWindow() { return hWindow; }
	static HMODULE getModule() { return _hmoduleDLL; }

	// Native function handler type
	typedef void(__cdecl * NativeHandler)(scrNativeCallContext * context);
	struct NativeRegistration
	{
		NativeRegistration * nextRegistration;
		Hooking::NativeHandler handlers[7];
		uint32_t numEntries;
		uint64_t hashes[7];
	};	
private:
	static BOOL InitializeHooks();
	static HMODULE _hmoduleDLL;
	static HWND hWindow;
};	
