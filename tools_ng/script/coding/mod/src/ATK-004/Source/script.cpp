#include "stdafx.h"

using namespace ImGui;

//static
int		frame				= 0;
bool	playerControl		= false;
bool	mobileRadio			= false;
bool	mobileRadioUpdated	= false;

/* Models */
uint RequestedModel = 0ul;
RequestState model_state = loaded;
void(*modelFunction)() = nullptr;
void modelCheck()
{
	switch (model_state)
	{
	case loaded:
		break;
	case requesting:
		if (STREAMING::HAS_MODEL_LOADED(RequestedModel) == TRUE)
		{
			model_state = loaded;

			modelFunction();

			break;
		}
		else
		{
			STREAMING::REQUEST_MODEL(RequestedModel);
			break;
		}
	}
}

void Script::isInit()
{
	srand(GetTickCount());
	while (true)
	{
		//onTickNative();
		WAIT(0);
	}

}

void Script::onTickNative()
{
	frame++;
	if (frame >= 100000)
	{	// Reset frame count
		frame = 0;
	}

	if (frame % 50 == 0)
	{	// Frame limited

	}

	if (Menu::IsOpen())
	{
		// disable controls
		disableControls(ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantTextInput || ImGui::GetIO().WantCaptureKeyboard || playerControl);
	}

	// Per game frame
	Player player = PLAYER::PLAYER_ID();
	Ped playerPed = PLAYER::PLAYER_PED_ID();

	if (!PLAYER::IS_PLAYER_PLAYING(player)) return;

	Vehicle playerVeh = NULL;
	if (PED::IS_PED_IN_ANY_VEHICLE(playerPed, FALSE))
		playerVeh = PED::GET_VEHICLE_PED_IS_USING(playerPed);

	// Model needed?
	modelCheck();

	if(mobileRadio) {
		AUDIO::SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(true);
		mobileRadioUpdated = true;
	}
	else if (!mobileRadio && mobileRadioUpdated) {
		AUDIO::SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(false);
		mobileRadioUpdated = false;
	}

	//Increase wanted level.
	if (KeyJustUp(VK_MULTIPLY))
	{
		if (PLAYER::GET_PLAYER_WANTED_LEVEL(player) < 5)
		{
			PLAYER::SET_PLAYER_WANTED_LEVEL(player, PLAYER::GET_PLAYER_WANTED_LEVEL(player) + 1, FALSE);
			PLAYER::SET_PLAYER_WANTED_LEVEL_NOW(player, FALSE);
		}
	}

	//Decrease wanted level.
	if (KeyJustUp(VK_SUBTRACT))
	{
		if (PLAYER::GET_PLAYER_WANTED_LEVEL(player) != 0)
		{
			PLAYER::SET_PLAYER_WANTED_LEVEL(player, PLAYER::GET_PLAYER_WANTED_LEVEL(player) - 1, FALSE);
			PLAYER::SET_PLAYER_WANTED_LEVEL_NOW(player, FALSE);
		}
	}

	//Teleport to waypoint.
	if (KeyJustUp(VK_NUMPAD0))
	{
		teleport_to_marker();
	}

	//Teleport to Mission Objective
	if (KeyJustUp(VK_NUMPAD2))
	{
		teleport_to_objective();
	}

	//Spawn Vehicle
	if (KeyJustUp(VK_DECIMAL))
	{
		request_vehicle();
	}

	if (KeyDown(VK_CONTROL))
		if (KeyJustUp(VK_KEY_D)) {
			CrossMapping::dumpNativeMappingCache();
		}
	
}


















