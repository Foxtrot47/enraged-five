#include "stdafx.h"
#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>
MODULEINFO g_MainModuleInfo = { 0 };

#define ISDLL 1


#if ISDLL
BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
#else

HANDLE GetBrHandle()
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE hRet = NULL;
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (wcscmp(entry.szExeFile, L"game_win64_bankrelease.exe") == 0)
			{
				hRet  = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
			}
		}
	}
	CloseHandle(snapshot);

	return hRet;
}

HMODULE GetCurrentModuleHandle()
{
	HMODULE hMod = NULL;
	GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		reinterpret_cast<LPCWSTR>(&GetCurrentModuleHandle), &hMod);
	return hMod;
}

HMODULE GetBrModule(HANDLE processHandle)
{
	HMODULE hMods[1024];
	HMODULE retMod = NULL;
	DWORD cbNeeded;
	unsigned int i;

	// Get a list of all the modules in this process.

	if (EnumProcessModules(processHandle, hMods, sizeof(hMods), &cbNeeded))
	{
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
		{
			TCHAR szModName[MAX_PATH];

			// Get the full path to the module's file.

			if (GetModuleFileNameEx(processHandle, hMods[i], szModName,
				sizeof(szModName) / sizeof(TCHAR)))
			{
				// Print the module name and handle value.
				std::wstring str = szModName;
				if (str.find(L"bankrelease") != std::string::npos)
				{
					retMod = hMods[i];
				}
				
			}
		}
	}

	// Release the handle to the process.
	return retMod;
}

int main(int argc, char ** argv)
#endif
{
#if ISDLL
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
#else
		HMODULE hModule = GetCurrentModuleHandle();
		HANDLE gtaProc = GetBrHandle();
		HMODULE gtaModule = GetBrModule(gtaProc);
#endif
		Log::Init(hModule);

		DEBUGOUT("ATK-003 loaded");
#if ISDLL
		if (!GetModuleInformation(GetCurrentProcess(), GetModuleHandle(0), &g_MainModuleInfo, sizeof(g_MainModuleInfo))) {
			Log::Fatal("Unable to get MODULEINFO from GTA5.exe");
		}
#else
		if (!GetModuleInformation(gtaProc, gtaModule, &g_MainModuleInfo, sizeof(g_MainModuleInfo))) {
			Log::Fatal("Unable to get MODULEINFO from GTA5.exe");
		}
#endif


		DEBUGOUT("GTA5 [0x%I64X][0x%X]", g_MainModuleInfo.lpBaseOfDll, g_MainModuleInfo.SizeOfImage);

		NoIntro();
		SpawnScriptHook(); //Hook that shit.
		//PLAYER::GET_PLAYER_PED(0);
		//BypassOnlineModelRequestBlock(); //This allows us to spawn models on the LE XD INTERNET
#if ISDLL
	}
#endif

#if ISDLL
	return TRUE;
#endif

	while (true)
	{
		Sleep(33);
	}
}
