#include "stdafx.h"

//https://s-media-cache-ak0.pinimg.com/236x/a5/32/43/a5324394baa368ef5273ef2e95a2976c.jpg
Player moneyPlayer = -1;

BlipList* pBlipList;
GtaThread_VTable gGtaThreadOriginal;
GtaThread_VTable gGtaThreadNew;

HANDLE mainFiber;
DWORD wakeAt;

void printVTable(GtaThread_VTable vt)
{
	DEBUGOUT("Run:0x%016llX", vt.Run);
	DEBUGOUT("Dcn:0x%016llX", vt.Deconstructor);
	DEBUGOUT("Kll:0x%016llX", vt.Kill);
	DEBUGOUT("Rst:0x%016llX", vt.Reset);
	DEBUGOUT("Tck:0x%016llX", vt.Tick);
}
void WAIT(DWORD ms)
{
#ifndef __DEBUG
	wakeAt = timeGetTime() + ms;
	SwitchToFiber(mainFiber);
#endif
}

eThreadState Trampoline(GtaThread* This)
{
	//	rage::scrThread* runningThread = GetActiveThread();
	//	SetActiveThread(This);
	//#ifdef  __DEBUG
	//	Run(); //We don't want to also call RunUnlireable, since it's expecting WAIT() to work, which it doesn't in debug mode. #depechemode
	//#else
	//	Tick();
	//#endif
	//	SetActiveThread(runningThread);
	return gGtaThreadOriginal.Run(This);
}

void __stdcall ReliableScriptFunction(LPVOID lpParameter)
{
	try
	{
		while (1)
		{
			Run();
			SwitchToFiber(mainFiber);
		}
	}
	catch (...)
	{
		Log::Fatal("Failed scriptFiber");
	}
}

void __stdcall HeavyWaitFunction(LPVOID lpParameter)
{
	try
	{
		while (1)
		{
			RunUnreliable();
			SwitchToFiber(mainFiber);
		}
	}
	catch (...)
	{
		Log::Fatal("Failed scriptFiber");
	}
}

void Tick()
{
	if (mainFiber == nullptr)
		mainFiber = ConvertThreadToFiber(nullptr);

	static HANDLE reliableFiber;
	if (reliableFiber)
		SwitchToFiber(reliableFiber);
	else
		reliableFiber = CreateFiber(NULL, ReliableScriptFunction, nullptr);

	if (timeGetTime() < wakeAt)
		return;

	static HANDLE scriptFiber;
	if (scriptFiber)
		SwitchToFiber(scriptFiber);
	else
		scriptFiber = CreateFiber(NULL, HeavyWaitFunction, nullptr);
}

int cint(float number)
{
	int result = (number + 0.5);
	return result;
}
void RunUnreliable() //Put functions that don't really need to be run every frame that can cause heavy wait times for the function here.
{

}

void Run() //Put functions that don't really need to be run every frame that can cause heavy wait times for the function here.
 //Only call WAIT(0) here. The Tick() function will ignore wakeAt and call this again regardless of the specified wakeAt time.
{
	return;
}
bool AttemptScriptHook()
{
	rage::pgPtrCollection<GtaThread>* threadCollection =  GetGtaThreadCollection(pBlipList);

	if (!threadCollection) {
		DEBUGOUT("Thread Collection Failed");
		return false;
	}
	for (UINT16 i = 0; i < threadCollection->count(); i++) {
		GtaThread* pThread = threadCollection->at(i);
		DEBUGOUT("Checking thread %d at address 0x%I64X, pointing to 0x%I64X", i, pThread, *pThread);
		if (!pThread)
		{
			DEBUGOUT("PThread at %d/%d is null", i, threadCollection->count());
			GtaThread_VTable ct;
			memcpy(&ct, (DWORD64*)((DWORD64*)pThread)[0], sizeof(ct)); //Create a backup of the original table so we can call the original functions from our hook.
			DEBUGOUT("Inapplicable:");
			printVTable(ct);
			continue;
		}

		//s0biet originally had some junk thread that was called for like 2 seconds then died. This thread is better.
		if (pThread->GetContext()->ScriptHash != 0x5700179C)
		{ //MAIN_PERSISTANT?
			DEBUGOUT("Context Address: 0x%I64X", pThread->GetContext());
			DEBUGOUT("Script Hash %d of thread %d / %d is not the one I care about", pThread->GetContext()->ScriptHash, i, threadCollection->count());
			GtaThread_VTable ct;
			memcpy(&ct, (DWORD64*)((DWORD64*)pThread)[0], sizeof(ct)); //Create a backup of the original table so we can call the original functions from our hook.
			DEBUGOUT("Inapplicable:");
			printVTable(ct);
			continue;
		}

		DEBUGOUT("Hooking thread [%i] (0x%X)", pThread->GetContext()->ThreadId, pThread->GetContext()->ScriptHash);

		// Now what? We need to find a target thread and hook it's "Tick" function
		if (gGtaThreadOriginal.Deconstructor == NULL) {
			
			memcpy(&gGtaThreadOriginal, (DWORD64*)((DWORD64*)pThread)[0], sizeof(gGtaThreadOriginal)); //Create a backup of the original table so we can call the original functions from our hook.
			DEBUGOUT("Old");
			printVTable(gGtaThreadOriginal);
			memcpy(&gGtaThreadNew, &gGtaThreadOriginal, sizeof(GtaThread_VTable)); //Construct our VMT replacement table.
			
			gGtaThreadNew.Run = Trampoline; //Replace the .Run method in the new table with our method.
			DEBUGOUT("New");
			printVTable(gGtaThreadNew);
			
		}

		if (((DWORD64*)pThread)[0] != (DWORD64)&gGtaThreadNew) { //If the table is not VMT Hooked.
			DEBUGOUT("Hooking thread [%i] (0x%X)", pThread->GetContext()->ThreadId, pThread->GetContext()->ScriptHash);
			((DWORD64*)pThread)[0] = (DWORD64)&gGtaThreadNew; //Replace the VMT pointer with a pointer to our new VMT.
			DEBUGOUT("Hooked thread [%i] (0x%X)", pThread->GetContext()->ThreadId, pThread->GetContext()->ScriptHash);

			DEBUGOUT("Displaying all the rest:");

			for (UINT16 j = 0; j < threadCollection->count(); j++)
			{
				GtaThread* qThread = threadCollection->at(j);
				DEBUGOUT("%d", j);

				GtaThread_VTable ct;
				memcpy(&ct, (DWORD64*)((DWORD64*)qThread)[0], sizeof(ct)); //Create a backup of the original table so we can call the original functions from our hook.
				printVTable(ct);
			}
			
			return true;
		}
	}
	return false;
}

DWORD WINAPI lpHookScript(LPVOID lpParam) {
	while (!AttemptScriptHook()) {
		Sleep(33);
	}

	return 0; //We no longer need the lpHookScript thread because our Trampoline function will now be the hip and or hop hang out spot for the KewlKidzKlub®.
}

void SpawnScriptHook() {
	CreateThread(0, 0, lpHookScript, 0, 0, 0);
}