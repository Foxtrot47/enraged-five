#include "Syringe.h"



int main(int argc,char* argv[])
{
	DWORD mode,wowID;
	//target dll
	//injection mode
	//target process id
	//target process name

	char* usage = "Usage: syringe.exe -[n|p] <process> <injection mode> <DLL path>\n"
				"\t-n <process_name>\n"
				"\t\tRequires full file name of target process (i.e. notepad.exe)\n"
				"\t-p <process_id>\n"
				"\t\tRequires target process PID\n"
				"\t<injection mode>\n"
				"\t\t1: Code Cave	(sneaky)\n"
				"\t\t2: ?????????	(???)\n"
				"\t\t3: ?????????	(???)\n\n";



	if(argc != 5)
	{
		printf("ERROR: Invalid number of arguments");
		printf(usage);
		exit(1);
	}
	
	mode = atoi(argv[3]);
	if(!(0 < mode < 4))
	{
		printf("ERROR: Invalid value for injection mode");
		printf(usage);
		exit(1);
	}
	if(!strncmp(argv[1],"-n",2))
	{
		Syringe::InjectIntoNamedProcess(argv[2],mode,argv[4]);
	}
	else if(!strncmp(argv[1],"-p",2))
	{
		wowID = atoi(argv[2]);
		if(wowID == 0)
		{
			printf("ERROR: Invalid integer for process ID");
			printf(usage);
			exit(1);
		}
		Syringe::InjectIntoProcess(wowID,mode,argv[4]);
	}
	else
	{
		printf("ERROR: Invalid process parameter");
		printf(usage);
	}

	printf("All Done\nPress a key to exit...");
	getchar();
}



