

extern "C" int __stdcall loadDll();
extern "C" void* begin;
extern "C" void* finish;

DWORD64 addr = (DWORD64)(&begin);
DWORD64 addr2 = (DWORD64)(&finish);



void CodeCave(HANDLE hProcess,DWORD wowID, DWORD threadID,LPCSTR targetDll)
{
	DWORD64 stubLen, loadLibAddy, oldIP;
	DWORD oldprot;	
	void *dllString = NULL;
	void *stub = NULL;
	SIZE_T numWritten = 0;
	CONTEXT ctx;
	HANDLE hThread = 0;
	char msg[200];

	stubLen = addr2-addr;

#if _DEBUG
	//Navigate extern "C" jump table for ACTUAL function pointer. Credit: Amir Soofi
	//Debug uses jump tables for functions. Will need to be fixed if compiled in Release
	Log("Running debug shit");
	Log("Debug does not work right now....\nGoodbye");
	return 0;
	unsigned char * loadDllReloc = (unsigned char *)&(loadDll);
	loadDllReloc++;
	unsigned long * loadDllRelocIntPtr = (unsigned long*)loadDllReloc;
	unsigned long loadDllRelocVal = *loadDllRelocIntPtr;
	unsigned long long actLoadDllAddr = (unsigned long long)&loadDll+loadDllRelocVal;
	actLoadDllAddr+=5;
	unsigned long long *actLoadDll = (unsigned long long *)actLoadDllAddr;
#endif


	//Grab address for LoadLibraryA in kernel32.dll
	loadLibAddy = (DWORD64)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	if(!loadLibAddy){Log("ERROR:  Couldn't get Kernel32.dll::LoadLibraryA proc"); goto cleanup;}

	//Allocate memory for DLL name and cave to sperlunk into
	dllString = VirtualAllocEx(hProcess, NULL, (strlen(targetDll) + 1), MEM_COMMIT, PAGE_READWRITE);
	stub      = VirtualAllocEx(hProcess, NULL, stubLen, (MEM_COMMIT | MEM_RESERVE), PAGE_EXECUTE_READWRITE);
	if(dllString == NULL || stub == NULL) {Log("ERROR:  Virtual Alloc Failed"); goto cleanup;}

	//Write name of DLL to inject into remote process space
	WriteProcessMemory(hProcess, dllString, targetDll, strlen(targetDll), &numWritten);
	if(numWritten != strlen(targetDll)) {Log("ERROR:  Write DLL Name Failed"); goto cleanup;}
    
	//Grab ThreadID for main thread
	threadID = GetThreadByProcess(wowID);
	memset(msg,0,200);
	sprintf_s(msg,200,"Injecting into Thread: %X",threadID);
	Log(msg);
	
	hThread = GetThread(threadID);
	if(!hThread)
	{
		Log("ERROR: Could not get handle to main thread, exiting"); goto cleanup;

	}

	SuspendThread(hThread);

	//Grab thread context
	ctx.ContextFlags = CONTEXT_CONTROL;
	if(!GetThreadContext(hThread, &ctx)){Log("ERROR: Failed to get context of remote thread..."); goto cleanup;}

	//Store old RIP, and set RIP to code cave entry point
	oldIP   = ctx.Rip;
	ctx.Rip = (DWORD64)stub;
	ctx.ContextFlags = CONTEXT_CONTROL;


#if _DEBUG
	int virtProtectReturn = VirtualProtect((void *)actLoadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

	////Copy relevant addresses into the place holders in sperlunk code
	
	memcpy((void *)((DWORD64)actLoadDll + 0x5), &oldIP, sizeof(oldIP));
	memcpy((void *)((DWORD64)actLoadDll + 0x2A), &dllString, sizeof(dllString));
	memcpy((void *)((DWORD64)actLoadDll + 0x34), &loadLibAddy, sizeof(loadLibAddy));
#else
	int virtProtectReturn = VirtualProtect((void *)loadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

	//Copy relevant addresses into the place holders in sperlunk code
	memcpy((void *)((DWORD64)loadDll + 0x5), &oldIP, sizeof(oldIP));
	memcpy((void *)((DWORD64)loadDll + 0x2A), &dllString, sizeof(dllString));
	memcpy((void *)((DWORD64)loadDll + 0x34), &loadLibAddy, sizeof(loadLibAddy));  
#endif


	//Set necessary protections on code cave, and write code into memory cave
	numWritten = 0;
	VirtualProtectEx(hProcess, stub, stubLen, PAGE_EXECUTE_READWRITE, NULL);
	WriteProcessMemory(hProcess, stub, loadDll, stubLen, &numWritten);
	VirtualProtectEx(hProcess, stub, stubLen, PAGE_EXECUTE, NULL); //Unsure if needed, but remote process seemed to crash much less after adding this line...
	if(numWritten != stubLen)Log("ERROR: Couldn't write to process memory");

	//Set thread context and flush the cache in case commands were queued up
	SetThreadContext(hThread, &ctx);
	//FlushInstructionCache(hProcess, NULL, NULL);



	ResumeThread(hThread);
	Sleep(15000);

cleanup:
	Sleep(2000);
	ResumeThread(hThread);
	if(dllString)
		VirtualFreeEx(hProcess, dllString, strlen(targetDll)+1, MEM_DECOMMIT);
	if(stub)
		VirtualFreeEx(hProcess, stub, stubLen, (MEM_DECOMMIT | MEM_RELEASE));

}