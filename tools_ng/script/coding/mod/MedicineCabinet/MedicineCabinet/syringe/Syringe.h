#include <windows.h>
#include <tlhelp32.h>
#include <iostream>
#include <shlwapi.h>

#define LOG_FILE "syringe.log.txt"

bool Log(char* msg)
{
//#if _DEBUG
	printf("%s\n",msg);
//#else
//	FILE fpOut;
//	fpOut = fopen(LOG_FILE,"a");
//	time_t ltime;
//	ltime=time(NULL);
//	fprintf("%lld: %s\n",ltime,msg);
//	fclose(fpOut);
//#endif
	return true;
}

HANDLE GetProcess(DWORD wowID)
{
	HANDLE hProcess = OpenProcess((PROCESS_VM_WRITE | PROCESS_VM_OPERATION), false, wowID);
	if(!hProcess) 
	{
		Log("ERROR: Could not open handle to remote process");
		return 0;
	}
	return hProcess;
}

DWORD FindProcessByName(const char *__ProcessName, HANDLE *pEntry)
{   
    PROCESSENTRY32 __ProcessEntry;
    __ProcessEntry.dwSize = sizeof(PROCESSENTRY32);
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot == INVALID_HANDLE_VALUE) return 0;        if (!Process32First(hSnapshot, &__ProcessEntry))
    {
        CloseHandle(hSnapshot);
        return 0;
    }
    do{if (!_strcmpi(__ProcessEntry.szExeFile, __ProcessName))
    {
		if(pEntry != NULL)
		{
			memcpy((void *)pEntry, (void *)&__ProcessEntry, sizeof(PROCESSENTRY32));
		}
		CloseHandle(hSnapshot);
        return __ProcessEntry.th32ProcessID;
    }} while (Process32Next(hSnapshot, &__ProcessEntry));
    CloseHandle(hSnapshot);
    return 0;
}

HANDLE GetThread(DWORD threadID)
{
	HANDLE hThread   = OpenThread((THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME ), false, threadID);
	if(!hThread) 
	{
		Log("ERROR: Failed to open remote thread");
	}
	return hThread;
}
 
DWORD GetThreadByProcess(DWORD __DwordProcess)
{   
	THREADENTRY32 __ThreadEntry;
	__ThreadEntry.dwSize = sizeof(THREADENTRY32);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE) return 0;
 
	if (!Thread32First(hSnapshot, &__ThreadEntry)) {CloseHandle(hSnapshot); return 0; }
 
	do {if (__ThreadEntry.th32OwnerProcessID == __DwordProcess)
	{
		CloseHandle(hSnapshot);
		return __ThreadEntry.th32ThreadID;
	}} while (Thread32Next(hSnapshot, &__ThreadEntry));
	CloseHandle(hSnapshot);      
	return 0;
}

