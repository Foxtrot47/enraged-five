#include "Syringe.h"
#include "CodeCave.h"

//#define targetProc "notepad.exe"
//#define targetProc "GTA5.exe"
//#define targetProc "game_win64_bankrelease.exe"
//#define targetDll "fuckworld.dll"
//#define targetDll "C:\\Users\\cwalsh\\work\\dev\\sigFactory\\brainDump\\x64\\Release\\brainDump.dll"

extern "C" __declspec( dllimport ) bool InjectIntoProcess(DWORD,DWORD,LPCSTR);

bool InjectIntoProcess(DWORD targetProcess,DWORD injectionMethod,LPCSTR targetDLL)
{
	HANDLE hProcess = NULL, hThread = NULL;
	DWORD threadID;

   hProcess = GetProcess(targetProcess);
   threadID = GetThreadByProcess(targetProcess);
   if(!hProcess || !threadID)
	   exit(2);

	switch(injectionMethod)
	{
	case 1:			//CodeCave
		Log("Using Code Cave injection method...");
		CodeCave(hProcess,targetProcess,threadID,targetDLL);
		break;
	case 2:

		break;
	case 3:

		break;
	default:
		break;
	}

   CloseHandle(hProcess);
   CloseHandle(hThread);
   return 0;
}

bool InjectIntoNamedProcess(char* targetProcess,DWORD injectionMethod,char *targetDLL)
{
	DWORD wowID;

	wowID = FindProcessByName(targetProcess,NULL);
	return InjectIntoProcess(wowID,injectionMethod,targetDLL);
}

int main(int argc,char* argv[])
{
	DWORD mode,wowID;
	//target dll
	//injection mode
	//target process id
	//target process name

	char* usage = "Usage: syringe.exe -[n|p] <process> <injection mode> <DLL path>\n"
				"\t-n <process_name>\n"
				"\t\tRequires full file name of target process (i.e. notepad.exe)\n"
				"\t-p <process_id>\n"
				"\t\tRequires target process PID\n"
				"\t<injection mode>\n"
				"\t\t1: Code Cave	(sneaky)\n"
				"\t\t2: ?????????	(???)\n"
				"\t\t3: ?????????	(???)\n\n";

	if(argc != 5)
	{
		Log("ERROR: Invalid number of arguments");
		Log(usage);
		exit(1);
	}
	
	mode = atoi(argv[3]);
	if(!(0 < mode < 4))
	{
		Log("ERROR: Invalid value for injection mode");
		Log(usage);
		exit(1);
	}
	if(!strncmp(argv[1],"-n",2))
	{
		InjectIntoNamedProcess(argv[2],mode,argv[4]);
	}
	else if(!strncmp(argv[1],"-p",2))
	{
		wowID = atoi(argv[2]);
		if(wowID == 0)
		{
			Log("ERROR: Invalid integer for process ID");
			Log(usage);
			exit(1);
		}
		InjectIntoProcess(wowID,mode,argv[4]);
	}
	else
	{
		Log("ERROR: Invalid process parameter");
		Log(usage);
	}
}



