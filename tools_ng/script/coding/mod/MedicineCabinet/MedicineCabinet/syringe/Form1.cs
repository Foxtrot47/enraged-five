﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;



namespace syringe
{
    public partial class Form1 : Form
    {
        int procPid = 0;
        int injectMethod = 2;
        string dll = "";

        String[] GtaTargets = {
                "GTA5",
                "game_win64_final",             
                "game_win64_bankrelease",
                "game_win64_release",                
                "game_win64_beta",   
                "GTAVLauncher",
                "PlayGTAV"
            };
        String[] BobTargets = {
                "RDR3",
                "game_win64_final",             
                "game_win64_bankrelease",
                "game_win64_release",                
                "game_win64_beta",
            };

        public Form1()
        {
            InitializeComponent();
        }

        private void proc_button_Click(object sender, EventArgs e)
        {
            listProcs();
            proc_button.Width = 115;
            proc_button.Text = "Refresh Processess";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Application.Idle += new EventHandler(Form1_Idle);
            this.dllBox.GotFocus += new EventHandler(dllBox_Focus);
        }

        private void Form1_Idle(object sender, System.EventArgs e)
        {
            updateProcs();
        }

        private void Form1_Closed(object sender, System.EventArgs e)
        {
            Console.WriteLine("Closing...");
            Program.exitClean();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 0)
            {
                Process p = (Process)listView1.SelectedItems[0].Tag;
                setTargetPid(p.Id);
                logMessage("Injecting with the following parameters: PID: " + procPid + ", Injection Method: " + injectMethod + ",  DLL: " + dll);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            dllBox.ForeColor = Color.Black;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            getGameProcs(GtaTargets);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            getGameProcs(BobTargets);
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void dllBox_TextChanged(object sender, EventArgs e)
        {
            dll = dllBox.Text;
            dllBox.ForeColor = System.Drawing.SystemColors.InfoText;
            logMessage("Set to inject with the following parameters: PID: " + procPid + ", Injection Method: " + injectMethod + ",  DLL: " + dll);
        }

        protected void dllBox_Focus(Object sender, EventArgs e)
        {
            dllBox.Text = "";
        }

        private void fileBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                this.dllBox.Text = openFileDialog1.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int pid = procPid;
            logMessage("Injecting with the following parameters: PID: " + procPid + ", Injection Method: " + injectMethod + ",  DLL: '" + dll + "'");
            Syringe.InjectIntoProcess(pid, injectMethod, dll);
        }


        //////////////////////////////Non Gui Methods

        

        private void listProcs()
        {
            logMessage("Listing all running processes");
            listView1.Clear();
            Process[] procList = Process.GetProcesses();
            if (procList.Count() < 1)
                return; //print error
            foreach (Process proc in procList)
            {
                ListViewItem procItem = new ListViewItem();
                procItem.Text = proc.ProcessName;
                procItem.Tag = proc;
                listView1.Items.Add(procItem);
            }
                
        }

        private void getGameProcs(String[] targets)
        {

            string s = "Searching for game clients: ";
            listView1.Clear();

            foreach (string t in targets)
            {
                s += t+".exe ";
                Process[] procs = Process.GetProcessesByName(t);
                if (procs.Count() < 1)
                    continue;
                if (procs.Count() > 1)  //More than one, which one to pick!?
                    continue;
                ListViewItem processList = new ListViewItem();
                processList.Text = procs[0].ProcessName;
                processList.Tag = procs[0];
                listView1.Items.Add(processList);
            }
            logMessage(s);
            if (listView1.Items.Count == 0)
                errorMessage("No targets found, try starting the game...");
            
        }

        private void updateProcs()
        {
            foreach(ListViewItem item in listView1.Items)
            {
                int pid = 0;
                Process proc = (Process)item.Tag;
                try
                {
                    proc = Process.GetProcessById(proc.Id);
                    pid = proc.Id;
                }
                catch (System.ArgumentException)
                {
                    item.Remove();
                    pid = 0;
                }
                if (item.Selected)
                     setTargetPid(pid);
                    
            }
        }

        private void setTargetPid(int pid)
        {
            procPid = pid;
            if (pid > 1)
            {
                pidBox.ForeColor = Color.Black;
                pidBox.Text = procPid.ToString();
            }
            else
            {
                pidBox.ForeColor = Color.LightGray;
                errorMessage("Seems like your target quit...");
                pidBox.Text = "No target";
            }
        }

        protected void pidBox_Focus(Object sender, EventArgs e)
        {
            pidBox.Text = "";
            pidBox.ForeColor = System.Drawing.SystemColors.InfoText;
        }

        void errorMessage(string s)
        {
            textBox2.ForeColor = Color.Red;
            textBox2.Text = s + '\n';
        }

        void logMessage(string s)
        {
            textBox2.ForeColor = Color.Black;
            textBox2.Text = s + '\n';
        }




    }
}
