﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace syringe
{
    static class Program
    {

        static string usage = "\n\n\nusage: syringe.exe -[n|p|l] process dll_path [injection_mode]\n" +
                       "\t-n process_name\n" +
                "\t\trequires full file name of target process (i.e. notepad.exe)\n" +
                "\t-p process_id\n" +
                "\t\trequires target process pid\n" +
                "\t-l process_list\n" +
                "\t\t Takes CSV list of proc names (i.e. notepad,calc,GTA5)\n\n" +
                "\tDLL_path\n" +
                "\t\tDll path takes single or CSV list of filepaths \n\n" +
                "\t[injection_mode](Optional)\n" +
                "\t\t1: CreateRemoteThread \n" +
                "\t\t2: Code Cave \n\n" +
                "\nAuto-targeting for game titles: (-bob | -gta)\n" +
                "\tsyringe.exe -gta C:\\Windows\\twain.dll\n\n" + 
                "Examples: \n" +
                "\tsyringe.exe -bob  X:\\hackme.dll,X:\\hackyou.dll\n" + 
                "\tsyringe.exe bankapp,notepad 2 X:\\hackyou.dll\n";


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            int pid = 0;
            int injectMethod = 1;

            string dllPath = "";
            string dllPathArg = "";

            bool procListFlag = false;
            bool pathListFlag = false;

            List<Process[]> procsList = new List<Process[]>();
            List<string> dllPathList = new List<string>();



            System.Console.ForegroundColor = ConsoleColor.DarkGray;

            System.Console.WriteLine("\n\nWelcome to the Medicine Cabinet...\n\t lets inject some weird stuff and see what happens.\n");
            System.Threading.Thread.Sleep(1000);

            if (args.Contains("-gui") || args.Count() == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
                
                return;
            }


process:
            if (args.Contains("-gta"))
            {
                pid = getGTAProcs();
                injectMethod = 2;
                dllPathArg = args[1];
                goto dll;
            }

            if (args.Contains("-bob"))
            {
                pid = getBOBProcs();
                injectMethod = 2;
                dllPathArg = args[1];
                goto dll;
            }

            if(args.Count() < 3)
                errorOut();
     

            if (args[0] == "-n")
            {
                string procName = args[1];
                Process[] processes = Process.GetProcessesByName(procName);

                if (processes.Count() == 0)
                    ErrorMessage("No processes found by that name");
                else if (processes.Count() > 1)
                    ErrorMessage("Too many processes found by that name");

                pid = processes[0].Id;
            }
            else if (args[0] == "-l")
            {
                procListFlag = true;
                string procListArg = args[1];
                procsList = procListArg.Split(',').Select(Process.GetProcessesByName).ToList();
                if (procsList.Count() == 0)
                    ErrorMessage("No processes found for list");

                pid = grabProcFromList(procsList);
            }
            else if (args[0] == "-p")
            {
                if (!Int32.TryParse(args[1], out pid))
                    ErrorMessage("-p arg not an integer, try again...");
            }
            else
                errorOut();

            dllPathArg = args[2];

            injectMethod = 2;
            if(args.Count() > 3)
                if (!Int32.TryParse(args[3], out injectMethod))
                    ErrorMessage("Argument 4 not an integer, try again...");


dll:
            if(Convert.ToBoolean(dllPathArg.Length))
                dllPathList = dllPathArg.Split(',').ToList();                
            else
                ErrorMessage("No DLL Path string");

            if (dllPathList.Count() == 0)
                ErrorMessage("DLL file does not exist");

            if (dllPathList.Count() > 1)
                pathListFlag = true;

            dllPath = grabDllFromList(dllPathList);




inject:
            Syringe.InjectIntoProcess(pid, injectMethod, dllPath);
            System.Console.WriteLine("Sleeping for 5 seconds...");
            System.Threading.Thread.Sleep(5000);



//List Looping

            if(pathListFlag)
            {
                if (dllPathList.Count() != 0)
                {
                    dllPath = grabDllFromList(dllPathList);
                    goto inject;
                }
            }

            if (procListFlag)
            {
                if (procsList.Count() != 0)
                {
                    pid = grabProcFromList(procsList);
                    dllPathList = dllPathArg.Split(',').ToList();
                    goto inject;
                }                
            }

            System.Console.WriteLine("All Done\nPress a key to exit...");
            System.Console.ReadKey();
            exitClean();
        }

        static string grabDllFromList(List<string> dllPathList)
        {
            string dllPath = dllPathList[0];
            dllPathList.RemoveAt(0);

            if (!File.Exists(dllPath))
                ErrorMessage("DLL file does not exist");

            return dllPath;
        }

        static int grabProcFromList(List<Process[]> procsList)
        {
            Process[] processes = procsList[0];
            procsList.RemoveAt(0);

            if (processes.Count() == 0)
                ErrorMessage("No processes found");
            else if (processes.Count() > 1)
                ErrorMessage("Too many processes found by name" + processes[0].ProcessName);

            System.Console.Write("Proceeding with process: ");
            System.Console.ForegroundColor = ConsoleColor.Green;
            System.Console.Write(processes[0].ProcessName+".exe");
            System.Console.ForegroundColor = ConsoleColor.DarkGray;
            System.Console.Write(" PID: ");
            System.Console.ForegroundColor = ConsoleColor.Green;
            System.Console.WriteLine(processes[0].Id);
            System.Console.ForegroundColor = ConsoleColor.DarkGray;

            return processes[0].Id;            
        }

        static void ErrorMessage(string s)
        {
            System.Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine("Error: "+s);
            System.Console.ForegroundColor = ConsoleColor.DarkGray;
            errorOut();
        }

        public static void exitClean()
        {
            System.Console.ResetColor();
            Environment.Exit(1);
        }

        static void errorOut()
        {
            System.Console.WriteLine(usage);
            System.Console.ResetColor();
            Environment.Exit(1);
        }

        static int getGTAProcs()
        {
            String[] targets = {
                "GTA5",
                "game_win64_final",             
                "game_win64_bankrelease",
                "game_win64_release",                
                "game_win64_beta",   
                "GTAVLauncher",
                "PlayGTAV"
            };
                

            foreach (string t in targets)
            {
                Process[] procs = Process.GetProcessesByName(t);
                if (procs.Count() < 1)
                    continue;
                if (procs.Count() > 1)  //More than one, which one to pick!?
                    ErrorMessage(String.Format("Too many processes with name '{0}', please close one if you wish to use as target", t));
                return procs[0].Id;
            }

            ErrorMessage("No GTA targets found, try starting the game...");
            return 0;
        }

        static int getBOBProcs()
        {
            String[] targets = {
                "rdr3",
                "game_win64_final",             
                "game_win64_bankrelease",
                "game_win64_release",                
                "game_win64_beta",
            };

            foreach (string t in targets)
            {
                Process[] procs = Process.GetProcessesByName(t);
                if (procs.Count() < 1)
                    continue;
                if (procs.Count() > 1)  //More than one, which one to pick!?
                    ErrorMessage(String.Format("Too many processes with name '{0}', please close one if you wish to use as target", t));
                return procs[0].Id;
            }

            ErrorMessage("No BOB targets found, try starting the game...");
            return 0;
        }
    }

    public class Syringe
    {
        [DllImport("syringeLib.dll", EntryPoint = "InjectIntoProcess", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool InjectIntoProcess(int pid, int injectionMethod, string dllPath);

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern int SetStdHandle(int device, IntPtr handle); 
    }
}
