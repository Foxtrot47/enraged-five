#include "Util.h"
#include "CodeCave.h"
#include "CRThread.h"


#ifdef SYRINGEDLL_EXPORTS
#define SYRINGEDLL_API __declspec(dllexport) 
#else
#define SYRINGEDLL_API __declspec(dllimport) 
#endif


namespace Syringe
{
	extern "C" SYRINGEDLL_API bool InjectIntoProcess(DWORD targetProcess,DWORD injectionMethod,char* targetDLL);
	extern "C" SYRINGEDLL_API bool InjectIntoNamedProcess(char* targetProcess,DWORD injectionMethod,char *targetDLL);	
}

