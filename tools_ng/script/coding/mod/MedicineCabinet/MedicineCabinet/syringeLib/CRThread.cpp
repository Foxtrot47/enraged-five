#include "CRThread.h"

int RunCreateRemoteThread(PVOID processPtr, _TCHAR *dllFile)
{

	// _TCHAR *dllFile;
	char dllBaseName[_MAX_FNAME];
	DWORD test = NULL; //pid
	HMODULE dllHandle = NULL;
	FARPROC loadLibraryAddress;
	PVOID baseAddress, threadPtr = NULL;
	BOOL writeSuccess = false;
	SIZE_T numBytesWritten = 0;	

	byte *buffer = (byte*)dllFile;

	//Grab a handle to Kernel32 and get the base address of the LoadLibraryW function
	loadLibraryAddress = Syringe::GetLibFuncAddress("Kernel32","LoadLibraryA");
	if(!loadLibraryAddress)
		Syringe::Log("Failed to find LoadLibraryA",true);
	Syringe::Log("LoadLibraryA Address:  \t\t\t\t 0x%X", false,(DWORD64)loadLibraryAddress);

	//TODO:  VALIDATE TARGET PROCESS ARCHITECTURE MATCHES DLL AND INJECTOR

	// allocate memory in the target process
	baseAddress = Syringe::AllocWriteRemoteMemory(processPtr, NULL, dllFile, strlen(dllFile), MEM_COMMIT, PAGE_READWRITE);
	if(!baseAddress)
	{
		Syringe::Log("Failed to write DLL Name to remote process",true);
		return 0;
	}
	Syringe::Log("DLL path written!");
	
	// create remote thread in the target process
	threadPtr = CreateRemoteThread(processPtr, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibraryAddress, baseAddress, 0, NULL);
	Syringe::Log("Thread handle: \t\t\t\t\t 0x%X",false, (DWORD64)threadPtr);
	if (threadPtr == NULL)
	{
		Syringe::Log("Failed to create remote thread!\nError: 0x%X",true,GetLastError());
		return 0;
	}
						
	// wait for thread to finish TODO
	test = WaitForSingleObject(threadPtr, INFINITE);

	// close handle to thread
	if (!CloseHandle(threadPtr))
	{
		Syringe::Log("Could not close handle",true);
		return 1;
	}

	// release allocated memory
	if (!VirtualFreeEx(processPtr, baseAddress, strlen(dllFile), MEM_DECOMMIT))
	{
		Syringe::Log("Could not free memory",true);
		return 1;
	}

	return 0;
}



