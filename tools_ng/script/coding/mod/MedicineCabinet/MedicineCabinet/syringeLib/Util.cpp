#include "Util.h"

namespace Syringe {
	HANDLE hConsole;
	HWND victimWindowHandle;
	//Logging
	bool Log(LPCSTR msg, bool error, LPCSTR data)
	{
		WORD m_currentConsoleAttr;
		CONSOLE_SCREEN_BUFFER_INFO consoleInfo;

		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);	
		GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
		m_currentConsoleAttr = consoleInfo.wAttributes;

		//#if _DEBUG
		if(error)
			SetConsoleTextAttribute(hConsole, 12);
	
		char buff[1024];
		sprintf_s(buff,msg,data);
		printf("%s\n",buff);
		SetConsoleTextAttribute(hConsole, 8);
		//#else
		//	FILE fpOut;
		//	fpOut = fopen(LOG_FILE,"a");
		//	time_t ltime;
		//	ltime=time(NULL);
		//	fprintf("%lld: %s\n",ltime,msg);
		//	fclose(fpOut);
		//#endif
		return true;
	}

	bool Log(LPCSTR msg,bool error, DWORD64 data)
	{
		char buff[1024];
		sprintf_s(buff,msg,data);

		return Log(buff,error,"");
	}

	bool Log(LPCSTR msg, bool error)
	{
		return Log(msg,error,"");
	}

	bool Log(LPCSTR msg)
	{
		return Log(msg,false,"");
	}



	//Remote Process Actions
	HANDLE GetProcess(DWORD wowID, DWORD access)
	{
		//(PROCESS_VM_WRITE | PROCESS_VM_OPERATION)
		HANDLE hProcess = OpenProcess(access, false, wowID);
		if(!hProcess) 
		{
			Log("Could not open handle to remote process",true);
			return 0;
		}
		Log("Process handle:  \t\t\t\t 0x%d",false,(DWORD64)hProcess);
		return hProcess;
	}

	DWORD FindProcessByName(const LPCSTR __ProcessName, HANDLE *pEntry)
	{   
		PROCESSENTRY32 __ProcessEntry;
		__ProcessEntry.dwSize = sizeof(PROCESSENTRY32);
		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hSnapshot == INVALID_HANDLE_VALUE) 
		{
			Log("Could not grab handle to TH32CS_SNAPPROCESS",true);
			return 0;        
		}
		if (!Process32First(hSnapshot, &__ProcessEntry))
		{
			Log("Could not grab Process32First from Snapshot",true);
			CloseHandle(hSnapshot);
			return 0;
		}
		do{if (!_strcmpi(__ProcessEntry.szExeFile, __ProcessName))
		{
			if(pEntry != NULL)
			{
				memcpy((void *)pEntry, (void *)&__ProcessEntry, sizeof(PROCESSENTRY32));
			}
			CloseHandle(hSnapshot);
			return __ProcessEntry.th32ProcessID;
		}} while (Process32Next(hSnapshot, &__ProcessEntry));

		CloseHandle(hSnapshot);
		Log("Could not find Process name",true);
		return 0;

	}


	HANDLE GetThread(DWORD threadID)
	{
		HANDLE hThread   = OpenThread((THREAD_GET_CONTEXT | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME ), false, threadID);
		if(!hThread) 
		{
			Log("Failed to open remote thread",true);
			return 0;
		}
		Log("Thread handle:  \t\t\t\t 0x%d",false,(DWORD64)hThread);
		return hThread;
	}
 
	DWORD GetThreadByProcess(DWORD __DwordProcess)
	{   
		THREADENTRY32 __ThreadEntry;
		__ThreadEntry.dwSize = sizeof(THREADENTRY32);
		HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
		if (hSnapshot == INVALID_HANDLE_VALUE) 
		{
			Log("Could not grab handle to TH32CS_SNAPTHREAD",true);
			return 0;
		}
 
		if (!Thread32First(hSnapshot, &__ThreadEntry)) 
		{
			Log("Could not grab Thread32First from Snapshot",true);
			CloseHandle(hSnapshot); 
			return 0; 
		}
 
		do {if (__ThreadEntry.th32OwnerProcessID == __DwordProcess)
		{
			CloseHandle(hSnapshot);
			return __ThreadEntry.th32ThreadID;
		}} while (Thread32Next(hSnapshot, &__ThreadEntry));

		CloseHandle(hSnapshot);      
		Log("Could not find Process name",true);
		return 0;
	}

	bool IsModuleLoaded(PVOID processPtr,LPCSTR dllBaseName, bool print)
	{
		HMODULE hMods[1024];
		DWORD cbNeeded;
		bool foundMod = false;
		unsigned int i;
		char buff[1024];

		if (EnumProcessModules(processPtr, hMods, sizeof(hMods), &cbNeeded))
		{
			for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
			{
				TCHAR szModName[MAX_PATH];
				if (GetModuleBaseName(processPtr, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
				{
					if (((std::string)szModName).find(dllBaseName) != std::string::npos) 
					{
						foundMod = true;
						if(print)
						{
							memset(buff,0,strlen(buff));
							sprintf_s(buff,">\t%s (0x%08p)", szModName, hMods[i]);
							Log("---------------------\n");
							SetConsoleTextAttribute(hConsole, 10);
							Log(buff);
							Log("\n---------------------");
						}
					}
					else if(print) {
						memset(buff,0,strlen(buff));
						sprintf_s(buff,"\t%s (0x%08p)", szModName, hMods[i]);
						Log(buff);
					}
				}
			}
		}
		return foundMod;
	}


	LPVOID AllocWriteRemoteMemory(HANDLE hProcess, LPVOID destAddress,  LPCSTR bytesToBeWritten, DWORD64 size, DWORD alloc, DWORD protect)
	{
		void* baseAddr;
		SIZE_T bytes;
		char buff[1024];

		char * remoteMemory = new char[size];

		baseAddr = VirtualAllocEx(hProcess, destAddress, size, alloc, protect);
		if(!baseAddr)
		{
			Log("Failed to allocate remote memory",true);
			return false;
		}
		if(destAddress && (baseAddr != destAddress) )
		{
			Log("Was unable to allocate at the specified memory location.\n If this is not an issue, try using a NULL pointer",true);
			return false;
		}
		Log("Base address to allocated memory:  \t\t 0x%X",false, (DWORD64)baseAddr);

		if(!WriteProcessMemory(hProcess, baseAddr, bytesToBeWritten, size, &bytes))
		{
			Log("Failed to write to process memory at: 0x%X",true,(DWORD64)baseAddr);
			return false;
		}
		
		if(bytes != size)
		{
			memset(buff,0,strlen(buff));
			sprintf_s(buff,"%d out of actual %d bytes",bytes,size);
			Log("Wrote incorrect amount of bytes to remote process: %s",true,buff);
			return false;
		}
		
		memset(remoteMemory,0,strlen(remoteMemory));
		if(!ReadProcessMemory(hProcess, baseAddr, remoteMemory, size, &bytes))
		{
			Log("Failed to read process memory at: 0x%X",true,(DWORD64)baseAddr);
			return false;
		}

		if(strncmp(bytesToBeWritten,remoteMemory,size))
		{
			Log("Remote memory read does not match the expected bytes that were written...\nLocal bytes:\n%s",true,bytesToBeWritten);
			Log("Remote Bytes:\n%s",true,remoteMemory);
			return false;
		}

		SetConsoleTextAttribute(hConsole, 10);
		Log("Remote memory matches provided byte string: \t %s",false,remoteMemory);
		return baseAddr;
	}


	BOOL CALLBACK FindWindowByPID(HWND hwnd, LPARAM pid)
	{
		DWORD proc, vpid;
		vpid = (DWORD)pid;
		GetWindowThreadProcessId(hwnd,&proc);
		
		if(proc == vpid)
		{
			Sleep(5000);
			//SetParent(HWND(), NULL);
			ShowWindow(hwnd,NULL);
			ShowWindow(hwnd,SW_HIDE);
			//if(SetActiveWindow(hwnd) == NULL)
			ShowWindow(hwnd,SW_RESTORE);
			Sleep(5000);
			if(!ShowWindow(hwnd,SW_RESTORE))
			{
				Log("Could not bring victim to forefront, may not detect DLL!\n\nPlease select the target process!",true);
				Sleep(5000);
			}
			return false;
		}
		else
			return true;
	}


	HWND GetWindowHandle(DWORD pid)
	{
		EnumWindows(FindWindowByPID, (LPARAM)pid);
		return victimWindowHandle;
	}


	FARPROC GetLibFuncAddress(LPCSTR dll, LPCSTR function)
	{
		HMODULE dllHandle = GetModuleHandle(dll);
		FARPROC funcAddr = GetProcAddress(dllHandle, function);
		if(!funcAddr)
		{
			char buff[1024];
			sprintf_s(buff,"%s@%s",function,dll);
			Log("Could not find address: ",true,buff);
			return NULL;
		}
		return funcAddr;
	}
}