;
_text SEGMENT
public begin, finish
begin:
loadDll PROC FRAME
   ;mov rax, 0AAAAAAAAAAAAAAAAh
   ;push rax
   ;pushfq
.endprolog

   push rax		;Dummy value for return address
   pushfq		;Save the flags
   push rax		;Save Registers
   nop
   mov rax, 0AAAAAAAAAAAAAAAAh	;Placeholder for oldIP
   mov qword ptr [rsp+16],rax	;Stores address below oldRax and flags, 8 bytes each
   push rcx
   push rdx
   push rbx
   push rbp
   push rsi
   push rdi
   push r8
   push r9
   push r10
   push r11
   push r12
   push r13
   push r14
   push r15

   mov rcx, 0BBBBBBBBBBBBBBBBh	;Placeholder for string address
   mov rax, 0CCCCCCCCCCCCCCCCh	;Placeholder for LoadLibrary address

   call rax		;Call LoadLibrary

   pop r15		;Restore registers
   pop r14
   pop r13
   pop r12
   pop r11
   pop r10
   pop r9
   pop r8
   pop rdi
   pop rsi
   pop rbp
   pop rbx
   pop rdx
   pop rcx
   pop rax
   popfq

   ret

loadDll ENDP
finish:
_text ENDS
END