#include "CodeCave.h"


extern "C" int __stdcall loadDll();
extern "C" void* begin;
extern "C" void* finish;

DWORD64 addr = (DWORD64)(&begin);
DWORD64 addr2 = (DWORD64)(&finish);

void CodeCave(HANDLE hProcess, LPCSTR targetDll)
{
	DWORD64 stubLen, loadLibAddy, oldIP;
	DWORD oldprot,threadID;	
	void *dllString = NULL;
	void *stub = NULL;
	SIZE_T numWritten = 0;
	CONTEXT ctx;
	HANDLE hThread = 0;
	char msg[1024];

	stubLen = addr2-addr;

#if _DEBUG
	//Navigate extern "C" jump table for ACTUAL function pointer. Credit: Amir Soofi
	//Debug uses jump tables for functions. Will need to be fixed if compiled in Release
	Syringe::Log("Running debug shit");
	Syringe::Log("Debug does not work right now....\nGoodbye");
	return;
	unsigned char * loadDllReloc = (unsigned char *)&(loadDll);
	loadDllReloc++;
	unsigned long * loadDllRelocIntPtr = (unsigned long*)loadDllReloc;
	unsigned long loadDllRelocVal = *loadDllRelocIntPtr;
	unsigned long long actLoadDllAddr = (unsigned long long)&loadDll+loadDllRelocVal;
	actLoadDllAddr+=5;
	unsigned long long *actLoadDll = (unsigned long long *)actLoadDllAddr;
#endif

	//Grab address for LoadLibraryA in kernel32.dll
	loadLibAddy = (DWORD64)Syringe::GetLibFuncAddress("Kernel32","LoadLibraryA");
	if(!loadLibAddy){Syringe::Log("Failed to find LoadLibraryA",true); goto cleanup; }
	Syringe::Log("LoadLibraryA Address:  \t\t\t\t 0x%X", false,loadLibAddy);
	
	
	//dllString = VirtualAllocEx(hProcess, NULL, (strlen(targetDll) + 1), MEM_COMMIT, PAGE_READWRITE);
	stub = VirtualAllocEx(hProcess, NULL, stubLen, MEM_RESERVE, PAGE_READWRITE);
	if(stub == NULL) {Syringe::Log("ERROR:  Virtual Alloc Failed"); goto cleanup; }
	Syringe::Log("Cave Address:  \t\t\t\t\t 0x%x", false, (DWORD64)stub);

	//Allocate memory for DLL name and cave to sperlunk into
	dllString = Syringe::AllocWriteRemoteMemory(hProcess, NULL ,targetDll, strlen(targetDll), MEM_COMMIT, PAGE_READWRITE);
	if(dllString == NULL)	{ Syringe::Log("AllocWriteRemoteMemory Failed", true); goto cleanup; }
	Syringe::Log("DLL String has been written to remote process: \t 0x%X",false,(DWORD64)dllString);
	
    
	//Grab ThreadID and Handle for main thread
	threadID = Syringe::GetThreadByProcess(GetProcessId(hProcess));
	Syringe::Log("Grabbing Thread:  \t\t\t\t 0x%X", false, threadID);
	
	hThread = Syringe::GetThread(threadID);
	if(!hThread)	{ Syringe::Log("Could not get handle to main thread", true); goto cleanup; }

	//Suspend remote thread
	if(SuspendThread(hThread) < 0)	{ Syringe::Log("Could not suspend remote thread", true); goto cleanup; }

	//Grab thread context
	ctx.ContextFlags = CONTEXT_CONTROL;
	if(!GetThreadContext(hThread, &ctx))	{ Syringe::Log("Failed to get context of remote thread", true); goto cleanup; }

	//Store old RIP, and set RIP to code cave entry point
	oldIP   = ctx.Rip;
	ctx.Rip = (DWORD64)stub;
	ctx.ContextFlags = CONTEXT_CONTROL;


#if _DEBUG
	int virtProtectReturn = VirtualProtect((void *)actLoadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

	////Copy relevant addresses into the place holders in sperlunk code
	
	memcpy((void *)((DWORD64)actLoadDll + 0x5), &oldIP, sizeof(oldIP));
	memcpy((void *)((DWORD64)actLoadDll + 0x2A), &dllString, sizeof(dllString));
	memcpy((void *)((DWORD64)actLoadDll + 0x34), &loadLibAddy, sizeof(loadLibAddy));
#else
	int virtProtectReturn = VirtualProtect((void *)loadDll, stubLen, PAGE_EXECUTE_READWRITE, &oldprot);

	//Copy relevant addresses into the place holders in sperlunk code
	memcpy((void *)((DWORD64)loadDll + 0x6), &oldIP, sizeof(oldIP));
	memcpy((void *)((DWORD64)loadDll + 0x2B), &dllString, sizeof(dllString));
	memcpy((void *)((DWORD64)loadDll + 0x35), &loadLibAddy, sizeof(loadLibAddy));  
#endif	

	//Set necessary protections on code cave, and write code into memory cave
	stub = Syringe::AllocWriteRemoteMemory(hProcess, stub,(char*)loadDll, stubLen, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if(!stub)	{ Syringe::Log("Couldn't write code cave to process memory", true); goto cleanup; }
	VirtualProtectEx(hProcess, stub, stubLen, PAGE_EXECUTE, NULL);
		

	//Set thread context and flush the cache in case commands were queued up
	if( !SetThreadContext(hThread, &ctx) )	Syringe::Log("Couldn't set original thread context, target process may crash...", true);

	if( !FlushInstructionCache(hProcess, NULL, NULL) )	Syringe::Log("Couldn't flush target instruction cache, target process may crash...", true);


	Syringe::Log("Brothers hold, resuming thread execution...\nMake sure to click the target process!");
	ResumeThread(hThread);
	Syringe::Log("Pausing for 7 seconds... Don't ask why");
	Sleep(7000);

cleanup:
	Sleep(2000);
	ResumeThread(hThread);
	/*if(dllString)
		VirtualFreeEx(hProcess, dllString, strlen(targetDll)+1, MEM_DECOMMIT);
	if(stub)
		VirtualFreeEx(hProcess, stub, stubLen, (MEM_DECOMMIT | MEM_RELEASE));
	*/

}


