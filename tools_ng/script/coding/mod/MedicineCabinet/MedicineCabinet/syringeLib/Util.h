#include <windows.h>
#include <tlhelp32.h>
#include <iostream>
#include <shlwapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <string>
#include <Psapi.h>

namespace Syringe {
	

	bool Log(LPCSTR msg, bool error, LPCSTR data);
	bool Log(LPCSTR msg, bool error, DWORD64 data);
	bool Log(LPCSTR msg, bool error);
	bool Log(LPCSTR msg);

	bool IsModuleLoaded(PVOID processPtr, LPCSTR dllBaseName, bool print);

	HANDLE GetThread(DWORD threadID);
	HANDLE GetProcess(DWORD wowID, DWORD access);
	extern HANDLE hConsole;

	HWND GetWindowHandle(DWORD pid);
	extern HWND victimWindowHandle;
	
	DWORD GetThreadByProcess(DWORD __DwordProcess);
	DWORD FindProcessByName(const LPCSTR __ProcessName, HANDLE *pEntry);

	LPVOID AllocWriteRemoteMemory(HANDLE hProcess, LPVOID destAddress, LPCSTR bytesToBeWritten, DWORD64 size, DWORD alloc, DWORD protect);

	FARPROC GetLibFuncAddress(LPCSTR dll, LPCSTR function);

	
}