#include "Syringe.h"

namespace Syringe
{
	bool InjectIntoProcess(DWORD targetProcess,DWORD injectionMethod,char* targetDLL)
	{
		PVOID processPtr;
		char dllBaseName[_MAX_FNAME];

		DWORD procAccess[] = {			
			(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ),
			(PROCESS_VM_WRITE | PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_QUERY_INFORMATION)
		};

		_splitpath_s(targetDLL, NULL, 0, NULL, 0, dllBaseName, _countof(dllBaseName), NULL, 0);

		Log("Attempting DLL injection...hold on to your butts!\n");
		Log("Target PID: \t\t\t\t\t 0x%X",false, targetProcess);



		// open the target process
		processPtr = Syringe::GetProcess(targetProcess,procAccess[injectionMethod-1]);
		if (processPtr == NULL)
		{
			Syringe::Log("Failed to open the remote process: %d!\n",true, targetProcess);
			return 0;
		}

		//Validate whether module is already loaded in target process
		if (Syringe::IsModuleLoaded(processPtr,dllBaseName,false))
		{
			Syringe::Log("Your DLL is already loaded in the process as a module!\n",true);
			return 0;
		}



		//Run Injection Function
		switch(injectionMethod)
		{
		case 1:
			Log("\nUsing CreateRemoteThread method...\n\n");
			RunCreateRemoteThread(processPtr, targetDLL);
			break;
		case 2:	
			Log("\nUsing Code Cave injection method...\n\n");
			CodeCave(processPtr, targetDLL);
			break;
		case 3:

			break;
		default:
			break;
		}




		// verify DLL injection by analyzing loaded modules
		Log("Verifying DLL injection...");
		HWND targetWindow = GetWindowHandle(targetProcess);
		bool foundMod = Syringe::IsModuleLoaded(processPtr,dllBaseName,true);

		if (!foundMod)
			Log("Injection failed! :(\n",true);
		else
		{
			SetConsoleTextAttribute(hConsole, 10);
			Log("Injection successful! >:)\n");
		}

		// close handle to process
		if (!CloseHandle(processPtr)) return 1;

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 8);
		return 0;
	}

	bool InjectIntoNamedProcess(char* targetProcess,DWORD injectionMethod,char *targetDLL)
	{
		DWORD wowID;

		wowID = FindProcessByName(targetProcess,NULL);
		return InjectIntoProcess(wowID,injectionMethod,targetDLL);
	}


	

}

