
#include "InjectorCLI.h"

int RunCreateRemoteThread(int argc, _TCHAR* argv[])
{

	_TCHAR *dllFile;
	char dllBaseName[_MAX_FNAME];
	DWORD pid, test = NULL;
	HMODULE dllHandle = NULL;
	FARPROC loadLibraryAddress;
	PVOID processPtr, baseAddress, threadPtr = NULL;
	BOOL writeSuccess = false;
	SIZE_T numBytesWritten = 0;

	HANDLE hConsole;
	WORD m_currentConsoleAttr;
	CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
	m_currentConsoleAttr = consoleInfo.wAttributes;
	SetConsoleTextAttribute(hConsole, 8);

	//Validate the existance of the necessary input parameters
	if (argv[1] == NULL || argv[2] == NULL)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("You must provide a valid DLL filename and PID!\n");
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}
	
	//Validate the DLL input path/file
	dllFile = argv[1];
	pid = (DWORD)strtod(argv[2], NULL);
	_splitpath_s(dllFile, NULL, 0, NULL, 0, dllBaseName, _countof(dllBaseName), NULL, 0);
	//TODO:  VALIDATE FILE EXISTS

	printf("Attempting DLL injection...hold on to your butts!\n");
	printf("Target PID:  %d\n", pid);
	printf("Injection DLL:  %s\n", dllFile);
	printf("Method:  Create Remote Thread\n\n");

	byte *buffer = (byte*)dllFile;

	//Grab a handle to Kernel32 and get the base address of the LoadLibraryW function
	dllHandle = GetModuleHandle("Kernel32");
	loadLibraryAddress = GetProcAddress(dllHandle, "LoadLibraryA");
	printf("LoadLibraryA address:  0x%08p\n", loadLibraryAddress);

	// open the target process
	processPtr = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ, false, pid);
	printf("Process handle:  0x%08p\n", processPtr);
	if (processPtr == NULL)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Failed to open the remote process: %d!\n", pid);
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}

	//TODO:  VALIDATE TARGET PROCESS ARCHITECTURE MATCHES DLL AND INJECTOR

	//Validate whether module is already loaded in target process
	if (IsModuleLoaded(processPtr,dllBaseName))
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Your DLL is already loaded in the process as a module!\n");
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}

	// allocate memory in the target process
	baseAddress = VirtualAllocEx(processPtr, NULL, strlen(dllFile), MEM_COMMIT, PAGE_READWRITE);
	printf("Base address to write DLL path:  0x%08p\n", baseAddress);
	if (baseAddress == NULL)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Failed to allocate memory in the remote process!\n");
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}

	// write the name of the injection DLL to the target process memory
	writeSuccess = WriteProcessMemory(processPtr, baseAddress, buffer, strlen(dllFile), &numBytesWritten);
	if (!writeSuccess)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Failed to write memory to the remote process!\n");
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}
	printf("DLL path written!\n");
	
	// create remote thread in the target process
	threadPtr = CreateRemoteThread(processPtr, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibraryAddress, baseAddress, 0, NULL);
	printf("Thread handle:  0x%08p\n", threadPtr);
	if (threadPtr == NULL)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Failed to create remote thread!\n");
		SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
		return 0;
	}
						
	// wait for thread to finish TODO
	test = WaitForSingleObject(threadPtr, INFINITE);

	// close handle to thread
	if (!CloseHandle(threadPtr)) return 1;

	// release allocated memory
	if (!VirtualFreeEx(processPtr, baseAddress, strlen(dllFile), MEM_DECOMMIT)) return 1;

	
	// verify DLL injection by analyzing loaded modules
	printf("Verifying DLL injection...\n");
	HMODULE hMods[1024];
	DWORD cbNeeded;
	bool foundMod = false;
	unsigned int i;

	if (EnumProcessModules(processPtr, hMods, sizeof(hMods), &cbNeeded))
	{
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
		{
			TCHAR szModName[MAX_PATH];
			if (GetModuleBaseName(processPtr, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
			{
				if (((std::string)szModName).find(dllBaseName) != std::string::npos)
				{
					foundMod = true;
					SetConsoleTextAttribute(hConsole, 10);
					_tprintf("\t%s (0x%08p)\n", szModName, hMods[i]);
					SetConsoleTextAttribute(hConsole, 8);
				}
				else {
					_tprintf("\t%s (0x%08p)\n", szModName, hMods[i]);
				}
			}
		}
	}
	if (!foundMod)
	{
		SetConsoleTextAttribute(hConsole, 12);
		printf("Injection failed! :(\n");
		SetConsoleTextAttribute(hConsole, 8);
	}
	else {
		SetConsoleTextAttribute(hConsole, 10);
		printf("Injection successful! >:)\n");
		SetConsoleTextAttribute(hConsole, 8);
	}

	// close handle to process
	if (!CloseHandle(processPtr)) return 1;

	printf("Press enter to exit...\n");
	getchar();

	SetConsoleTextAttribute(hConsole, m_currentConsoleAttr);
	return 0;
}

bool IsModuleLoaded(PVOID processPtr, char *dllBaseName)
{
	HMODULE hMods[1024];
	DWORD cbNeeded;
	bool foundMod = false;
	unsigned int i;

	if (EnumProcessModules(processPtr, hMods, sizeof(hMods), &cbNeeded))
	{
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
		{
			TCHAR szModName[MAX_PATH];
			if (GetModuleBaseName(processPtr, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
			{
				if (((std::string)szModName).find(dllBaseName) != std::string::npos) return true;
			}
		}
	}
	return false;
}

