#Conor Walsh
#Rockstar Games, 2014
#Script to extract and diff two different 


##############	Configuration	##############

param([switch]$out, [int]$v=0, [string]$r)

$cleanRPF = "..\rpfs\clean\common.rpf"
$cleanDir = "clean"
$dirtyDir = "dirty"
$outFile = "diff.txt"
$archDir = "archive"

##############################################






Function Main
{
	cleanup
	cd .\clean
	unpack $cleanRPF
	cd ..\dirty
	unpack $r
	cd ..
	write "Performing Recursive Diff..."
	recurTrav $dirtyDir
	archive
	write "Done"
	exit
}






##############	Functions		##############

Function cleanup
{
	if (Test-Path $outFile)
	{
		Remove-Item $outFile
	}
	if (Test-Path $dirtyDir)
	{
		Remove-Item $dirtyDir -recurse
	}
	New-Item -ItemType directory -Path $dirtyDir | Out-Null

	if (Test-Path $cleanDir)
	{
		Remove-Item $cleanDir -recurse
	}
	New-Item -ItemType directory -Path $cleanDir | Out-Null
}

Function archive
{
	if (Test-Path $outfile)
	{	
		$stamp = Get-Date -Format "MM-dd-y-hh;mm;ss"
		$arch = "$stamp.$outFile"
		cp $outFile $archDir\$arch
	}
}

Function output($s)
{
	if ($out)
	{
		Add-Content $outFile "$s"
	}
	else
	{
		write "$s"
	}
}

Function unpack($rpf)
{
	if ($rpf -eq "" -or $rpf -eq $null)
	{
		write "Please enter the path to the RPF archive you wish to diff:"
		$rpf = Read-Host
		$rpf = $rpf.Replace("`"","")		#Powershell surronds any file/folders with a space within quotes, which breaks the input
		$rpf = $rpf.Replace("'","")
	}

	if (Test-Path $rpf)
	{
		write "Unpacking RPF archive: $rpf"
		..\bin\unrpf.exe -x $rpf
		write "RPF file unpacked"
	}
	else
	{
		write $rpf
		write "Error: File does not exsist"
		Exit
	}
}


Function runDiff($a,$b,$c)
{
	if ($v -gt 1)
	{
		$x = bin\diff.exe --unchanged-group-format="" --changed-group-format="`tChanged lines: [%dF-%dL]`n`t`t%>`n" --old-group-format="`tRemoved lines: %dF-%dL`n" --new-group-format="`tNew lines: [%dF-%dL]`n`t`t%>`n" $a $b		
	}
	else
	{		
		$x = bin\diff.exe --unchanged-group-format="" --changed-group-format="`tChanged lines: [%dF-%dL]`n" --old-group-format="`tRemoved lines: [%dF-%dL]`n" --new-group-format="`tNew lines: [%dF-%dL]`n" $a $b
	}


	if ($x -eq $null)
	{
		Continue
	}

	output "File has been changed: $c"
	if ($v -gt 0)
	{
		$x = Out-String -InputObject $x
		output $x
	}
}


Function recurTrav($path)
{
	foreach ($dirtyItem in Get-ChildItem $path)# -recurse)
	{
		$relDir = Resolve-Path -Relative $dirtyItem.FullName
		$dirty = $relDir
		$relDir = $relDir.Replace($dirtyDir,"")
		$clean = $cleanDir + $relDir
		if (Test-Path $dirtyItem.FullName -PathType Container)
		{
			if ( -Not (Test-Path $clean) )
			{
				output "Directory has been added: $relDir"
				Continue
			}

			recurTrav $dirtyItem.FullName
		}
		else
		{
			if (Test-Path $clean)
			{
				runDiff $clean $dirty $relDir
			}
			else
			{
				output "New file has been added: $clean"
			}
		}
	}
}

##############################################

Main