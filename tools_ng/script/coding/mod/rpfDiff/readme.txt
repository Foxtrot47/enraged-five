RPF Archive Diff'ing tool
------------------------------------------------------------------------------

This tool takes two different RPF archives, extracts, then diff's the two resulting archives, printing the output to either the command line, or file of choosing.

The targeted purpose of the tool was for analyzing differences between stock game archives, and the modified versions of them available on the internet for use in modded consoles. Verbosity of the tool, as well as the output location, can be set to the user's liking.


Using RPFdiff.ps1
------------------------------------------------------------------------------

Default Operation:

	Unpacks default 'clean' RPF archive.
	Requests a 'dirty' RPF archive to compare against.
		When prompted, you can merely drag and drop the targeted file onto the powershell window.
	Unpacks targeted RPF.
	Traverses and diff's contents of the two extracted archives.
	Prints names of all the modified files and newly created directories to the command line



Usage:
	.\RPFdiff.ps1 [-o] [-v (0-2)] [-r file.rpf]
		-o 				Output is written to a file, designated in the "Configuration" portion of the script
		-v integer 		Verbosity level, either 0, 1, or 2. For any value higher than 0, '-o' flag is advised for larger archives.
							0: Simply reports new directories, and modified files
							1: Prints the line blocks that have been changed, added, or removed within 	each file
							2: Each set of added or changed blocks is followed by the modified lines of text.
		-r RPF file		'Dirty' RPF archive to be extracted and compared against retail archive



WARNINGS
------------------------------------------------------------------------------

The script MUST be run from the same directory structure from the RPFdiff.zip archive. 
	All the file paths in the script are relative, so moving any of the files or directories will break it. Feel free to move the files, just make sure to change the script accordingly.

The output file, as well as the extracted files are deleted at the beginning of script execution. 
	Currently, the default extraction directories are 'clean\' and 'dirty\', and the output file 'diff.txt'
	Make sure to move or rename files if you would like to preserve them over consecutive runs of the script.