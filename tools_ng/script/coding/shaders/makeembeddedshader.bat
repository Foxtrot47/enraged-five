@echo off
REM *************************** Obsolete I think ************************************

setlocal enableextensions enabledelayedexpansion

set MAKESHADERS_SCRIPT_DIR=%~dp0

set HAS_DX11=0
set HAS_DX11_1=0
set HAS_DX12=0

REM set WindowsSDK_ExecutablePath=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x86
REM set WindowsSDK_ExecutablePath_x64=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x64

set ARCH=%PROCESSOR_ARCHITECTURE%
set DX12_DIR=%WindowsSDK_ExecutablePath%

if "%ARCH%"=="AMD64" set DX12_DIR=%WindowsSDK_ExecutablePath_x64%

if exist %windir%\system32\D3DCompiler_43.dll set HAS_DX11=1
if exist %windir%\system32\D3DCompiler_46.dll set HAS_DX11_1=1

if exist %DX12_DIR%\dxcompiler.dll   set HAS_DX12=1
if not exist %DX12_DIR%\dxil.dll 	 set HAS_DX12=0

if "%1"=="-C" (
	chdir %2
	shift
	shift
)

if "%2"=="" echo Rebuild your vcproj with a newer version of ProjBuilder.
if "%2"=="" goto good

call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 %2 %3
if errorlevel 1 goto fail

REM if "%2"=="win32_30" call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 %2_atidx9 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

REM if "%2"=="win32_30" call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 %2_atidx10 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

REM if "%2"=="win32_30" call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 %2_nvdx9 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

REM if "%2"=="win32_30" call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 %2_nvdx10 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

REM if "%2"=="win32_30" if %HAS_DX11%==1 call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 win32_40_atidx10 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

REM if "%2"=="win32_30" if %HAS_DX11%==1 call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 win32_40_nvdx10 -noPerformanceDump -quiet %3
REM if errorlevel 1 goto fail

if "%2"=="win32_30" if %HAS_DX11%==1 call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %1 win32_40 -noPerformanceDump -quiet %3
if errorlevel 1 goto fail

if "%2"=="win32_30" echo Generating embedded_%~n3_win32_30.h

if "%2"=="win32_30" %RS_TOOLSROOT%\bin\coding\embedfile win32_30/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_30 __WIN32PC
REM if "%2"=="win32_30" %RS_TOOLSROOT%\bin\coding\embedfile win32_30_atidx9/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_30_atidx9 __WIN32PC -append
REM if "%2"=="win32_30" %RS_TOOLSROOT%\bin\coding\embedfile win32_30_atidx10/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_30_atidx10 __WIN32PC -append
REM if "%2"=="win32_30" %RS_TOOLSROOT%\bin\coding\embedfile win32_30_nvdx9/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_30_nvdx9 __WIN32PC -append
REM if "%2"=="win32_30" %RS_TOOLSROOT%\bin\coding\embedfile win32_30_nvdx10/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_30_nvdx10 __WIN32PC -append
REM if "%2"=="win32_30" if %HAS_DX11%==1 %RS_TOOLSROOT%\bin\coding\embedfile win32_40_atidx10/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_40_atidx10 __WIN32PC -append
REM if "%2"=="win32_30" if %HAS_DX11%==1 %RS_TOOLSROOT%\bin\coding\embedfile win32_40_nvdx10/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_40_nvdx10 __WIN32PC -append
if "%2"=="win32_30" if %HAS_DX11%==1 %RS_TOOLSROOT%\bin\coding\embedfile win32_40/%~n3.fxc embedded_%~n3_win32_30.h embedded_%~n3_win32_40 __WIN32PC -append
if "%2"=="fxl_final" %RS_TOOLSROOT%\bin\coding\embedfile fxl_final/%~n3.fxc embedded_%~n3_fxl_final.h embedded_%~n3_fxl_final __XENON
if "%2"=="psn" %RS_TOOLSROOT%\bin\coding\embedfile psn/%~n3.cgx embedded_%~n3_psn.h embedded_%~n3_psn __PPU
if "%2"=="psp2" %RS_TOOLSROOT%\bin\coding\embedfile psp2/%~n3.cgx embedded_%~n3_psp2.h embedded_%~n3_psp2 __PSP2

goto good
:fail
echo Embedded shader creation failed.
if "%2"=="win32_30" del embedded_%~n3_win32_30.h
if "%2"=="fxl_final" del embedded_%~n3_fxl_final.h
if "%2"=="psn" del embedded_%~n3_psn.h
if "%2"=="psp2" del embedded_%~n3_psp2.h
:good
