REM @echo off
echo Don't use this any more, just build shaders from the IDE.
exit/b

set HAS_DX12=0

REM set WindowsSDK_ExecutablePath=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x86
REM set WindowsSDK_ExecutablePath_x64=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x64

set ARCH=%PROCESSOR_ARCHITECTURE%
set DX12_DIR=%WindowsSDK_ExecutablePath%

if "%ARCH%"=="AMD64" set DX12_DIR=%WindowsSDK_ExecutablePath_x64%

if exist %DX12_DIR%\dxcompiler.dll   set HAS_DX12=1
if not exist %DX12_DIR%\dxil.dll 	 set HAS_DX12=0

if "%HAS_DX12%"=="1" echo *** DX12 supported, binaries found ***

setlocal enableextensions enabledelayedexpansion

set MAKESHADERS_SCRIPT_DIR=%~dp0

if exist shaderpath.bat call shaderpath.bat

if EXIST embedded.list (
	ECHO Please convert your project over to use EMBEDDED line
	ECHO in makefile.bat.
	EXIT/B
)

if not "%1" == "/embedded" (

	REM Make sure AB assets never contain embedded files.
	REM Clean them all out before building the shaders normally.
	if exist shaderpath.bat (
		del /f %SHADERPATH%fx_max\*.dcl
		del /f %SHADERPATH%win32_20\*.fxc
		del /f %SHADERPATH%win32_30\*.fxc
		del /f %SHADERPATH%win32_40\*.fxc
		del /f %SHADERPATH%win32_50\*.fxc
		del /f %SHADERPATH%win32_60\*.fxc
		del /f %SHADERPATH%win32_60\*.dxbc
		del /f %SHADERPATH%fxl_final\*.fxc
		del /f %SHADERPATH%psn\*.cgx
		del /f %SHADERPATH%dcl\*.dcl
		del /f %SHADERPATH%orbis\*.fxc
		del /f %SHADERPATH%durango\*.fxc	
	)

	for /f %%I in (preload.list) do (
		if %%~xI==.fx (
			echo ==== %%I ====
			call %MAKESHADERS_SCRIPT_DIR%makeshader.bat %%I %*
			if errorlevel 1 goto fail
		)
	)
)


:success

echo *** SUCCESSFUL ***

GOTO done

:fail

ECHO *** FAILED ***

GOTO done

:done

endlocal
