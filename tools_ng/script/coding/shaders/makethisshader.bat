@echo off
REM echo %1 %2
REM USAGE:
REM Set this up as an external tool in devstudio
REM Title: Compile This Shader
REM Command: x:\gta5\tools\script\coding\shaders\makethisshader.bat
REM Arguments: $(ItemFileName)$(ItemExt) $(TargetName)
REM Initial Directory: $(ItemDir)
REM [X] Use Output Window          [ ] Prompt for arguments
REM [ ] Treat output as Unicode    [X] Close on exit
REM You may need to edit the Command: path appropriately.
REM Figure out which slot it was used in (9 or 10 most likely), then
REM go to Tools / Options / Keybard, Tools.ExternalCommand10 (or whatever)
REM and assign it to Shift-F7

if NOT "%~2"=="" (
	echo Compiling target dir '%~2'...
	goto %~2
	echo ...if you see this message, you may be trying to build an embedded shader, try Ctrl-F7 for that...
	exit/b 1
)

:all
:fxl_final
:psn
:win32_30
:win32_40
:win32_50
:win32_60
:durango
:orbis

set filename=%1
set platform="all"
if not "%~2"=="" set platform="%~2"
set platform=%platform:~1,-1%
set ext=%filename:~-4,-1%
if "%ext%" == ".fx" call %~dp0makeshader.bat -platform %platform% %1
