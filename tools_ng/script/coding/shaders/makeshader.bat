@echo off

set WARNING_AS_ERROR=0
set HAS_DX10=0
set HAS_DX11=0
set HAS_DX12=0

REM set Win10SDK_ExecutablePath=C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x86
REM set Win10SDK_ExecutablePath_x64=#C:\Program Files (x86)\Windows Kits\10\bin\10.0.18362.0\x64

set BUILDARCH=x86
set ARCH=%PROCESSOR_ARCHITECTURE%
set DX12_DIR=%WindowsSDK_ExecutablePath%

if "%ARCH%"=="AMD64" set BUILDARCH=x64
if "%ARCH%"=="AMD64" set DX12_DIR=%WindowsSDK_ExecutablePath_x64%

if exist %windir%\system32\d3d10.dll set HAS_DX10=1
if exist %windir%\system32\d3d11.dll set HAS_DX11=1
if exist "%DX12_DIR%\dxcompiler.dll" set HAS_DX12=1
if not exist "%DX12_DIR%\dxil.dll" 	 set HAS_DX12=0

echo %ARCH%
echo %BUILDARCH%
echo %HAS_DX12%
echo %DX12_DIR%

if "%MAKESHADERS_SCRIPT_DIR%"=="" setlocal
if exist "%DXSDK_DIR%\utilities\fxc.exe" set DX_FXC=utilities\fxc.exe
if exist "%DXSDK_DIR%\utilities\bin\x86\fxc.exe" set DX_FXC=utilities\bin\x86\fxc.exe

REM If RAGE_COMPILE_ALL_SHADER_PLATFORMS is set to anything, ignore the
REM -platform switch.
set PLATFORM=all
if "%1"=="-platform" if "%RAGE_COMPILE_ALL_SHADER_PLATFORMS%"=="" (
    set PLATFORM=%2
    shift /1
    shift /1
    REM echo compiling only for %2...
)

set PLATFORMCONFIG=
if "%1"=="-platformConfig" (
    set PLATFORMCONFIG=%2
    shift /1
    shift /1
    REM echo platform config %2...
)
set COMPILE_PSN_RANDOMIZE=
if "%1"=="-useRandomizer" ( 
    set COMPILE_PSN_RANDOMIZE="-useRandomizer"
    shift /1
)   

set NO_PERFORMANCE_DUMP=""
if "%1"=="-noPerformanceDump" ( 
    set NO_PERFORMANCE_DUMP="-noPerformanceDump"
    shift /1
)
set QUIET=
if "%1"=="-quiet" ( 
    set QUIET="-quiet"
    shift /1
)   

set COMPILE_MAX_NOLINES=0
if "%1"=="-noWaveLines" ( 
    set COMPILE_MAX_NOLINES=1
    shift /1
)   

set COMPILE_WIN32_DEBUG_INFO=0
set DEBUG_INFO=
if %COMPILE_WIN32_DEBUG_INFO%==1 (
    set DEBUG_INFO="-debugInfo"
    set COMPILE_WIN32_DEBUG=1
) else ( 
    set COMPILE_WIN32_DEBUG=0 
)

goto %PLATFORM%
:all
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=1
set COMPILE_WIN32_60=1
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
if NOT "%DurangoXDK%"=="" set COMPILE_DURANGO=1
set COMPILE_ORBIS=0
if NOT "%SCE_ORBIS_SDK_DIR%"=="" set COMPILE_ORBIS=1
set COMPILE_MAX=1
goto common

:fxl_final
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=1
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:psn
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=1
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:psp2
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=1
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:win32_30
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=1
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:win32_40
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=1
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:win32_50
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=1
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:win32_60
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=1
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:durango
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=1
set COMPILE_ORBIS=0
set COMPILE_MAX=0
goto common

:orbis
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=1
set COMPILE_MAX=0
goto common

:fx_max
set COMPILE_WIN32_20=0
set COMPILE_WIN32_30=0
set COMPILE_WIN32_40=0
set COMPILE_WIN32_50=0
set COMPILE_WIN32_60=0
set COMPILE_XENON=0
set COMPILE_PSN=0
set COMPILE_PSP2=0
set COMPILE_DURANGO=0
set COMPILE_ORBIS=0
set COMPILE_MAX=1
goto common

:win32_30_atidx9
:win32_30_nvdx9
:win32_30_atidx10
:win32_30_nvdx10
:win32_40_atidx10
:win32_40_nvdx10
set SHADERPATH=%~p1
if EXIST shaderpath.bat call shaderpath.bat
REM echo Creating dummy file for %SHADERPATH%%PLATFORM%\%~n1.fxc . . .
echo Dummy > %SHADERPATH%%PLATFORM%\%~n1.fxc
exit/b 0

:common
set SHADERPATH=%~p1
set TEMPLATEPATH=%~p2
set USERDATAPATH=%~p3
if EXIST shaderpath.bat call shaderpath.bat
set SHADERNAME=%~n1
set SHADERFILE=%1

if "%SHADERPATH%"=="" (
    echo %SHADERFILE% : error X1000: SHADERPATH is empty
    goto common_fail
)

set SHADER_FOLDER_SUFFIX=
set WAVE_PLATFORMCONFIG_DEFINE=
set FXC_PLATFORMCONFIG_DEFINE=
if /I "%PLATFORMCONFIG%"=="final" (
	set SHADER_FOLDER_SUFFIX=_final
    set WAVE_PLATFORMCONFIG_DEFINE=-DSHADER_FINAL=1
    set FXC_PLATFORMCONFIG_DEFINE=/DSHADER_FINAL=1
)
rem command line option defaults
rem NOTE: any calling batch file can also set the environment variables
rem directly rather than use the command line options

rem Removing these unless we need them but the makefile build won't
rem pick up the dependencies properly.
rem if "%COMPILE_WIN32_20%"=="" set COMPILE_WIN32_20=0
rem if "%COMPILE_WIN32_30%"=="" set COMPILE_WIN32_30=1
rem if "%COMPILE_WIN32_DEBUG%"=="" set COMPILE_WIN32_DEBUG=1
rem if "%COMPILE_XENON%"=="" set COMPILE_XENON=1
if "%WARNING_AS_ERROR%"=="" set WARNING_AS_ERROR=1
if "%CONTINUE_ON_ERROR%"=="" set CONTINUE_ON_ERROR=0
if "%COMPILE_PSN%"=="" set COMPILE_PSN=1
if "%COMPILE_MAX%"=="" set COMPILE_MAX=1

REM echo %COMPILE_WIN32_30% %COMPILE_XENON% %COMPILE_PSN% %COMPILE_MAX%

if %COMPILE_PSN%==1 if "%SCE_PS3_ROOT%"=="" (
    echo *** PSN shader compiler requires Sony Cg install -- SCE_PS3_ROOT not set
    if "%ROCKSTAR_STUDIO%"=="rockstarsandiego.com" echo *** See \\zena\setup\PSN-Tools\readme-win32.txt for instructions.
    goto fail
)

if %COMPILE_PSN%==1 if not exist "%SCE_PS3_ROOT%\host-win32\Cg\bin\sce-cgc.exe" (
    echo *** PSN shader compiler requires Sony Cg install  -- sce-cgc.exe not found!
    echo *** SC3_PS3_ROOT is set to: %SCE_PS3_ROOT%.
    goto fail
)

rem parse command line parameters

for %%P in (%*) do (
    if /i "%%P"=="/20" (
        set COMPILE_WIN32_20=1
    ) else if /i "%%P"=="/debug" (
        set COMPILE_WIN32_DEBUG=1       
    ) else if /i "%%P"=="/continue" (
        set CONTINUE_ON_ERROR=1
    ) else if /i "%%P"=="/nopsn" (
        set COMPILE_PSN=0
    ) else if /i "%%P"=="/nomax" (
        set COMPILE_MAX=0
    )

)

if "%COMPILE_XENON%"=="1" echo Building shader %SHADERFILE% for Xbox 360
if "%COMPILE_PSN%"=="1" echo Building shader %SHADERFILE% for PlayStation 3
if "%COMPILE_WIN32_30%"=="1" echo Building shader %SHADERFILE% for shadermodel 3.0
if "%COMPILE_WIN32_40%"=="1" echo Building shader %SHADERFILE% for shadermodel 4.0
if "%COMPILE_WIN32_50%"=="1" echo Building shader %SHADERFILE% for shadermodel 5.0
if "%COMPILE_WIN32_60%"=="1" echo Building shader %SHADERFILE% for shadermodel 6.0
if "%COMPILE_WIN32_DEBUG%"=="1" echo Building shader %SHADERFILE% for debug
if "%COMPILE_DURANGO%"=="1" echo Building shader %SHADERFILE% for Durango
if "%COMPILE_ORBIS%"=="1" echo Building shader %SHADERFILE% for Orbis



REM ====================================================================================

if "%COMPILE_WIN32_20%"=="0" goto post_WIN32_20

REM echo **   win32_20:

set SHADER_FOLDER=win32_20%SHADER_FOLDER_SUFFIX%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
"%DXSDK_DIR%\%DX_FXC%" /Tfx_2_0 /Fo%SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc /nologo /D__RAGE=1 %FXC_PLATFORMCONFIG_DEFINE% /D__XENON=0 /D__WIN32PC=1 /D__SHADERMODEL=20 /D__FXL=1 /D__PSN=0 /D__PS3=0 -D__MAX=0  -DRSG_PC=1 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE%
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto fail
if exist shaderpath.bat if exist %SHADER_FOLDER%\* copy %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc %SHADER_FOLDER%\  >nul

:post_WIN32_20

REM ====================================================================================

if "%COMPILE_WIN32_30%"=="0" goto post_WIN32_30

REM ====================================================================================

REM echo **   win32_30:

set SHADER_FOLDER=win32_30%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=1 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=1 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc -platform=pc -SHADERMODEL=30 %QUIET% %DEBUG_INFO% > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_win32_30
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_win32_30
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc

:post_WIN32_30

REM ====================================================================================

if "%COMPILE_WIN32_40%"=="0" goto post_WIN32_40

REM ====================================================================================

REM echo **   win32_40:

set SHADER_FOLDER=win32_40%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=1 -D__SHADERMODEL=40 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=1 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc -platform=pc -SHADERMODEL=40 %QUIET% %DEBUG_INFO% > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_win32_40
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_win32_40
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc

:post_WIN32_40

REM ====================================================================================

if "%COMPILE_WIN32_50%"=="0" goto post_WIN32_50

REM ====================================================================================

REM echo **   win32_50:

set SHADER_FOLDER=win32_50%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=1 -D__SHADERMODEL=50 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=1 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc -platform=pc -SHADERMODEL=50 %QUIET% %DEBUG_INFO% > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_win32_50
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_win32_50
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc

:post_WIN32_50

REM ====================================================================================

REM ====================================================================================

if "%HAS_DX12%"=="0" goto error_win32_60
if "%COMPILE_WIN32_60%"=="0" goto post_WIN32_60

REM ====================================================================================

REM echo **   win32_60:

set SHADER_FOLDER=win32_60%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=1 -D__SHADERMODEL=60 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=1 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\hlsldxc_%BUILDARCH%_Debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.dxbc -platform=pc -SHADERMODEL=60 %QUIET% %DEBUG_INFO% > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_win32_60
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_win32_60
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.dxbc

:error_win32_60
echo compilation failed; DX12 unsupported, see %DX12_DIR% for dxcompiler and dxil dll's

:post_WIN32_60

REM ====================================================================================

if "%COMPILE_XENON%"=="0" goto post_XENON

REM echo **   fxl:

set SHADER_FOLDER=fxl_final%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=1 -D__WIN32PC=0 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=1 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc %NO_PERFORMANCE_DUMP% %QUIET% -platform=xenon > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_XENON
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_XENON
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc

:post_XENON

REM ====================================================================================

if "%COMPILE_PSN%"=="0" goto post_PSN

REM echo **   psn:

set SHADER_FOLDER=psn%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist psn\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=0 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=1 -D__PS3=1 -D__PSP2=0 -D__MAX=0  -DRSG_PC=0 -DRSG_PS3=1 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.cgx %NO_PERFORMANCE_DUMP% %QUIET% %COMPILE_PSN_RANDOMIZE% -platform=ps3 > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_PSN
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_PSN
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.cgx
goto post_PSN

:post_PSN

REM ====================================================================================

if "%COMPILE_PSP2%"=="0" goto post_PSP2

REM echo **   psp2:

set SHADER_FOLDER=psp2%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=0 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=1 -D__MAX=0  -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.cgx %NO_PERFORMANCE_DUMP% %QUIET% -platform=psp2 > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_PSP2
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_PSP2
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.cgx
goto post_PSP2

:post_PSP2

REM ====================================================================================

if "%COMPILE_DURANGO%"=="0" goto post_DURANGO

REM echo **   durango:

set SHADER_FOLDER=durango%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=1 -D__SHADERMODEL=50 -D__PSSL=0 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=1 -DRSG_ORBIS=0 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc %NO_PERFORMANCE_DUMP% %QUIET% -shadermodel=50 -platform=durango > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_DURANGO
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_DURANGO
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc
goto post_DURANGO

:post_DURANGO

REM ====================================================================================

if "%COMPILE_ORBIS%"=="0" goto post_ORBIS

REM echo **   orbis:

set SHADER_FOLDER=orbis%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADER_FOLDER%\* mkdir %SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* %RS_TOOLSROOT%\bin\coding\nmkdir %SHADERPATH%%SHADER_FOLDER%
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=0 -D__SHADERMODEL=50 -D__PSSL=1 -D__FXL=1 -D__PS3=0 -D__PSP2=0 -D__MAX=0  -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=1 %SHADERFILE% > %SHADER_FOLDER%\%SHADERNAME%.i
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto wave_fail
%RS_TOOLSROOT%\bin\coding\fx2cg_win32_debug.exe %SHADER_FOLDER%\%SHADERNAME%.i %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc %NO_PERFORMANCE_DUMP% %QUIET% -shadermodel=50 -platform=orbis > %SHADER_FOLDER%\%SHADERNAME%.txt
if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
type %SHADER_FOLDER%\%SHADERNAME%.txt | %RS_TOOLSROOT%\bin\coding\findstr /v succeeded
IF "%WARNING_AS_ERROR%"=="0" goto noWarning_ORBIS
%RS_TOOLSROOT%\bin\coding\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
IF NOT ERRORLEVEL 1 (
    echo %SHADERNAME% : error X1000: warnings treated as errors by script.
    if "%CONTINUE_ON_ERROR%"=="0" goto fail
)
:noWarning_ORBIS
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fxc
goto post_ORBIS

:post_ORBIS

REM ====================================================================================

if "%COMPILE_MAX%"=="0" goto post_MAX

REM echo **   max:

REM Don't allow folder suffic for Max since we don't do final builds here
set SHADER_FOLDER_SUFFIX=
set SHADER_FOLDER=fx_max%SHADER_FOLDER_SUFFIX%
set PDIR=%SHADER_FOLDER%
if not exist %SHADERPATH%%SHADER_FOLDER%\* mkdir %SHADERPATH%%SHADER_FOLDER%
echo %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fx
if "%COMPILE_MAX_NOLINES%"=="1" goto noLinesMax
%RS_TOOLSROOT%\bin\coding\wave.exe -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=0 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=0 -D__PS3=0 -D__PSP2=0 -D__MAX=1 -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fx
goto noWarning_MAX
:noLinesMax
%RS_TOOLSROOT%\bin\coding\wave.exe -L0 -I. -D__RAGE=1 %WAVE_PLATFORMCONFIG_DEFINE% -D__XENON=0 -D__WIN32PC=0 -D__SHADERMODEL=30 -D__PSSL=0 -D__FXL=0 -D__PS3=0 -D__PSP2=0 -D__MAX=1 -DRSG_PC=0 -DRSG_PS3=0 -DRSG_XENON=0 -DRSG_DURANGO=0 -DRSG_ORBIS=0 %SHADERFILE% > %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fx
REM if "%CONTINUE_ON_ERROR%"=="0" if errorlevel 1 goto common_fail
REM type %SHADER_FOLDER%\%SHADERNAME%.txt | %RAGE_DIR%\base\bin\findstr /v succeeded
REM IF "%WARNING_AS_ERROR%"=="0" goto noWarning_MAX
REM %RAGE_DIR%\base\bin\findstr warning %SHADER_FOLDER%\%SHADERNAME%.txt
REM IF NOT ERRORLEVEL 1 (
REM echo %SHADERNAME% : error X1000: warnings treated as errors by script.
REM if "%CONTINUE_ON_ERROR%"=="0" goto fail
REM )
:noWarning_MAX
echo compilation succeeded; see %SHADERPATH%%SHADER_FOLDER%\%SHADERNAME%.fx
goto post_MAX

:common_fail
type %PDIR%\%SHADERNAME%.txt
echo %SHADERFILE% : error X1000: shader compilation failed
if "%CONTINUE_ON_ERROR%"=="0" goto fail

:post_MAX

REM ====================================================================================

goto end

:wave_fail
echo %SHADERFILE% : error X1000: shader preprocessing failed

:fail
if "%MAKESHADERS_SCRIPT_DIR%"=="" endlocal
exit /b 1

:end

if "%MAKESHADERS_SCRIPT_DIR%"=="" endlocal
exit /b 0
