@echo off

pushd %RS_CODEBRANCH%\game\script_headers

p4 edit x:\gta5\script\dev\core\common\native\*.sch

copy *.sch x:\gta5\script\dev\core\common\native

p4 revert -a x:\gta5\script\dev\core\common\native\*.sch

:: These files are in a different place.
p4 revert \\depot\gta5\script\dev\core\common\native\stack_sizes.sch
p4 revert \\depot\gta5\script\dev\core\common\native\weapon_enums.sch

p4 edit x:\gta5\script\dev\core\game\data\*.sch

copy stack_sizes.sch  x:\gta5\script\dev\core\game\data
copy weapon_enums.sch x:\gta5\script\dev\core\game\data 
p4 revert -a x:\gta5\script\dev\core\game\data\*.sch


