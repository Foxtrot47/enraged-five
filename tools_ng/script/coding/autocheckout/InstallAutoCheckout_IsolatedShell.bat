@ECHO OFF
SETLOCAL
ECHO %ALLUSERSPROFILE%

IF "%ALLUSERSPROFILE%"=="C:\ProgramData" (
	ECHO Installing to Vista/Win7-compatible location.
	SET DEST=%ALLUSERSPROFILE%\Application Data\Microsoft\MSEnvShared\Addins
) ELSE (
	ECHO Installing to XP-compatible location.
	SET DEST=%USERPROFILE%\Documents\Visual Studio 2010\Addins
)
ECHO This will install the AutoCheckout addin for .Net 2010.
ECHO To remove it, run this script with the -remove switch.
PAUSE

ECHO Copying to %DEST%

IF NOT EXIST "%DEST%\*" MKDIR "%DEST%"
IF NOT "%1"=="-remove" COPY %~dp0\AutoCheckoutIsolatedShell\AutoCheckout2010.* "%DEST%"
IF "%1"=="-remove" DEL "%DEST%\AutoCheckout2010.*"
PAUSE