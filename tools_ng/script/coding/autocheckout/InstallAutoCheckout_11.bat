@ECHO OFF
SETLOCAL
IF "%ALLUSERSPROFILE%"=="C:\ProgramData" (
	ECHO Installing to Vista/Win7-compatible location.
	SET DEST=%USERPROFILE%\Documents\Visual Studio 11\Addins
) ELSE (
	ECHO Installing to XP-compatible location.
	SET DEST=%ALLUSERSPROFILE%\Application Data\Microsoft\MSEnvShared\Addins
)
ECHO This will install the AutoCheckout addin for DevStudio 11
ECHO into %DEST%
ECHO To remove it, run this script with the -remove switch.
PAUSE
IF NOT EXIST "%DEST%\*" MKDIR "%DEST%"
IF NOT "%1"=="-remove" COPY %~dp0\AutoCheckout11\AutoCheckout2010.* "%DEST%"
IF "%1"=="-remove" DEL "%DEST%\AutoCheckout.*"
