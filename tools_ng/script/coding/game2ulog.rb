require "pipeline/os/start"
require "pipeline/os/path"
require "pipeline/os/getopt"
require "rexml/document"
include REXML  # so that we don't have to prefix everything with REXML::...

#in_filename = "C:\\Users\\greg\\Desktop\\console.log"
#out_filename = "C:\\Users\\greg\\Desktop\\console.ulog"

OPTIONS = [
	[ '--input', '-i', OS::Getopt::REQUIRED, 'input' ],
	[ '--output', '-m', OS::Getopt::OPTIONAL, 'output' ]
]

opts, trailing = OS::Getopt.getopts( OPTIONS )

in_filename = ( nil == opts['input'] ) ? nil : opts['input']
out_filename = ( nil == opts['output'] ) ? nil : opts['output']

if in_filename == nil then

	exit(0)
end

if out_filename == nil then

	out_filename = OS::Path.remove_extension(in_filename) + "_out.ulog"
end

lines = File.readlines(in_filename)

prefixes = [	"Fatal Error",
				"Error",
				"Error",
				"Warning",
				"Debug1",
				"Debug2",
				"Debug3"]

contexts = Hash.new()

class Info

	attr_accessor :context, :message, :time, :error_level
end

begin

lines.each { |line|

	if /^\[(\d{8})\] /.match(line) != nil then
	
		time = $1
		line = line[11..-1]
		context = "default"
		error_level = "Debug1"

		prefixes.each { |prefix|
		
			if line[0,prefix.size] == prefix then
			
				error_level = prefix
				line = line[(prefix.size + 2)..-1]
				break
			end
		}
		
		if /^\[(\S+)\] /.match(line) != nil then
		
			context = $1
			line = line[(context.size + 3)..-1]
		end
		
		if contexts[context] == nil then
		
			contexts[context] = Array.new()
		end
	
		info = Info.new()
		info.context = context
		info.message = line.strip
		info.error_level = error_level

		contexts[context].push(info)
		
		#puts(info)		
	end
}
end
doc = Document.new("<ulog></ulog>")

contexts.keys.each { |key|
	
	elem = Element.new("context")
	elem.add_attribute("name",key)
	
	doc.root.add_element(elem)
	
	contexts[key].each { |item|
	
		msg_elem = Element.new(item.error_level)
		msg_elem.add_text(item.message)
	
		elem.add_element(msg_elem)
	}
}

File.open(out_filename,"w+") { |file|

	doc.write(file)
}