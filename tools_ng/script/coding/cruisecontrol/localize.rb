#
# File:: convert.rb
#			Command line wrapper for the projbuild system 
#
# Pipeline::
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Date:: 5 February 2010
#
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/config/projects'
require 'pipeline/os/getopt'
require 'pipeline/os/path'
require 'REXML/document'
include REXML
#-----------------------------------------------------------------------------
# Helper Functions
#-----------------------------------------------------------------------------

class CCTrayProject

	attr_reader :server_name, :port, :project_name, :show
	attr_writer :server_name, :port, :project_name, :show

	def initialize(s_name, port, proj_name, show)
		@server_name = s_name
		@port = port
		@project_name = proj_name
		@show = show
	end
	
	def get_cctray_output()
	
		output = "<Project serverUrl=\"tcp://#{server_name}:#{port}/CruiseManager.rem\" projectName=\"#{project_name}\" showProject=\"#{show}\"/>"
	
		return output
	end
end

class Project

	attr_reader :name, :show
	attr_writer :name, :show
	
	def initialize( project_node )

		@name = project_node.attributes['Name']
		@show = project_node.attributes['Show']
		@show = 'true' if @show.eql?("") or @show.nil?
	end
end

class Server

	attr_reader :projects, :name, :port
	attr_writer :projects, :name, :port
	
	def initialize( server_node )

		@name = server_node.attributes['Name']
		@port = server_node.attributes['Port'] 
		@projects = []
		
		server_node.elements.each('Project') do |project_node|
		
			new_project = Project.new( project_node )
			
			@projects << new_project
		end
	end
end

class Locale

	attr_reader :servers
	attr_writer :servers
	
	def initialize( name ) 
	
		@name = name
		@servers = []
	end
	
	def initialize( locale_node )
		
		@name = locale_node.attributes['Name']
		@servers = []
		
		locale_node.elements.each('Server') do |server_node|
		
			new_server = Server.new(server_node)
			@servers << new_server
		end
	end
	
	def acquire_cctray_projects( keywords )
		
		split_keywords = []
		
		results = []
		@servers.each do |server|
			
			server.projects.each do |project|
			
				keywords.each do |keyword|
				
					next if keyword.nil?
					
					match = true
					
					split_keywords = keyword.split(" ")
					split_keywords.each do |split_keyword|
					
						if project.name.index(split_keyword) == nil
							match = false
							break
						end
					end
					
					if match == true
					
						server_project = CCTrayProject.new(server.name, server.port, project.name, project.show)
							
						results << server_project
						break
					end
				end
			end
		end
		
		results
	end
end

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

COMMAND_OPTIONS = [ ["--help", "-h", Getopt::BOOLEAN, "Display help text." ],
					["--file", "-f", Getopt::OPTIONAL, "Global servers list" ],
					["--keyword", "-k", Getopt::OPTIONAL, "Keyword to categorize servers" ],
					["--keyword2", "-k2", Getopt::OPTIONAL, "Additional to categorize servers" ],
					["--keyword3", "-k3", Getopt::OPTIONAL, "Additional to categorize servers" ],
					["--locale", "-l", Getopt::OPTIONAL, "Current location" ],
					["--output", "-o", Getopt::OPTIONAL, "Output file" ],
					]
													
TRAILING_DESC = { }

opts, trailing = Pipeline::OS::Getopt.getopts( COMMAND_OPTIONS )

if ( opts["help"] ) then
	puts OS::Getopt.usage( COMMAND_OPTIONS, TRAILING_DESC )
	exit( 1 )
end

options = Hash.new

trailing.each { |item|

	tokens = item.split("=")
	
	key = tokens[0].intern
	
	if key == :type then
	
		options[key] = tokens[1].intern
	else
	
		options[key] = tokens[1]
	end
}

options = nil if options.keys.size == 0

input_file = opts['file']
if File.exist?(input_file) == false
	print "Input file does not exist!  Aborting...\n"
	exit
end

locale = opts['locale']
if locale.nil?
	print "Locale not specified!  Aborting...\n"
	exit
end

keyword = opts['keyword']
if keyword.nil?
	print "Keyword not specified.  Aborting...\n"
	exit
end

keyword2 = opts['keyword2']
keyword3 = opts['keyword3']
keywords = [ keyword, keyword2, keyword3 ]

output_file = opts['output']
if output_file.nil?
	print "Output file not specified.  Aborting...\n"
	exit
end

xml = File.read(input_file)
doc = REXML::Document.new(xml)

stripped_locale = locale.gsub(" ", "")
stripped_locale.downcase!

template_file = ENV['RS_TOOLSROOT'] + "\\etc\\CruiseControl\\cctray\\cctray-settings-template-#{stripped_locale}.xml"
if File.exist?(template_file) == false
	print "Template file #{template_file} does not exist!  Aborting...\n"
	exit
end

doc.elements.each('Locales/Locale') do |locale_node|

	current_locale_name = locale_node.attributes['Name']

	if locale.eql?(current_locale_name) == true
		
		locale = Locale.new(locale_node)

		#Search for the keyword in this locale's servers' projects.
		cctray_projects = locale.acquire_cctray_projects( keywords )
		
		local_servers_output = "  <Projects>"
		cctray_projects.each do |cctray|
			local_servers_output += "\n   "
			local_servers_output += cctray.get_cctray_output()
		end
		
		local_servers_output += "\n  </Projects>"
		
		local_servers_replacement_token = "&localServers"
		lines = File.readlines(template_file)
		
		file = File.new(output_file, "w")
		lines.each do |line|
			line.gsub!(local_servers_replacement_token, local_servers_output)
			line.gsub!("$(RS_TOOLSROOT)", ENV['RS_TOOLSROOT'])
			
			file.write(line)
		end
		file.close
		
		print "Successfully generated localized CCTray file: #{output_file}."
		exit
	end
end
