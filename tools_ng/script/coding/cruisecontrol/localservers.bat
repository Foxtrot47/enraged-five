@echo off

localize.rb --locale="San Diego" --file=localservers.xml --keyword="Rage" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-rage.xml

localize.rb --locale="San Diego" --file=localservers.xml --keyword="Rage PS3" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-rage-ps3.xml

localize.rb --locale="San Diego" --file=localservers.xml --keyword="Rage Samples" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-rage-samples.xml

localize.rb --locale="San Diego" --file=localservers.xml --keyword="Rage Tools" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-rage-tools.xml

localize.rb --locale="North" --file=localservers.xml --keyword="ADMIN_ONLY" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-admin.xml

localize.rb --locale="North" --file=localservers.xml --keyword="smoketester_gta5_dev_ps3_gta5_pedPics" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-art.xml

localize.rb --locale="North" --file=localservers.xml --keyword="assetbuilder_gta5_dev" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-assetbuild.xml

localize.rb --locale="North" --file=localservers.xml --keyword="toolstester_gta5_dev_ci" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-nm.xml

localize.rb --locale="North" --file=localservers.xml --keyword="smoketester_gta5_dev" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-smoketest.xml

localize.rb --locale="North" --file=localservers.xml --keyword="codebuilder_gta5_dev_win32" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-toolsbuild.xml

localize.rb --locale="North" --file=localservers.xml --keyword="toolstester_gta5_dev_diff" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-toolstest.xml

localize.rb --locale="North" --file=localservers.xml --keyword="assettester_gta5_dev" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-assettest.xml

localize.rb --locale="North" --file=localservers.xml --keyword="codebuilder_gta5_dev_ps3" --keyword2="codebuilder_gta5_dev_xbox360" --keyword3="codebuilder_gta5_dev_win32_ragebuilder" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-code.xml

localize.rb --locale="North" --file=localservers.xml --keyword="codebuilder _rebuild" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-code-full-rebuilds.xml

localize.rb --locale="North" --file=localservers.xml --keyword="codebuilder_gta5_dev_win32_ragebuilder" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-code-ragebuilder.xml

localize.rb --locale="North" --file=localservers.xml --keyword="codebuilder _build" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-code-regular.xml

localize.rb --locale="North" --file=localservers.xml --keyword="scriptbuilder_gta5" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-script.xml

localize.rb --locale="North" --file=localservers.xml --keyword="toolstester_gta5_dev" --keyword2="toolstester_gta5_dev_ci_test" --output=%RS_TOOLSROOT%\etc\CruiseControl\cctray\cctray-settings-toolsunittest.xml

