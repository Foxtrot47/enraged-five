@ECHO OFF
REM usage: %0 project-name (usually same as base part of %CD%)

REM This crap is to get the basename of %CD%
if "%1"=="" %0 %CD%
if "%2"=="" %0 %~n1 %1

setlocal
call %RS_TOOLSROOT%\etc\projbuild\environment_libs.bat
call makefile.bat
if "%XPROJ%"=="" set XPROJ=%RAGE_DIR%\base\src
if "%LIBS%"=="" set LIBS=RageCore
set XPROJ=.. %XPROJ%

p4 edit *.slndef
p4 edit *.makefile
p4 edit *.makefiles

echo Solution %1 { > %1.slndef
echo Include %%RS_TOOLSROOT%%\etc\projgen\sample.slnTargets >> %1.slndef
echo Include %RS_TOOLSROOT%\etc\projgen\sample.slnRules >> %1.slndef
echo Projects { >> %1.slndef

echo build %%RS_TOOLSROOT%%\etc\projgen\sample.build > %1.makefiles
echo Processing %TESTERS% . . .

for %%f in (%TESTERS%) do (

	echo %%f %%f { >> %1.slndef
	echo Dependencies { >> %1.slndef
	for %%g in (%LIBS%) do (
		if EXIST %%~png.txt (
			echo  %%~ng %%~png >> %1.slndef
		) else (
			for %%h in (%XPROJ%) do if EXIST %%h\%%g\makefile.txt (
				echo  %%g %%h\%%g\%%g >> %1.slndef
			)
		)
	)
	
	echo  stlport %RAGE_DIR%\stlport\STLport-5.0RC5\src\stlport >> %1.slndef
	echo } >> %1.slndef
	echo } >> %1.slndef

	echo Project %%f > %%f.makefile
	echo ConfigurationType exe >> %%f.makefile
	echo RootDirectory . >> %%f.makefile
	echo Files { >> %%f.makefile
	echo %%f.cpp >> %%f.makefile
	echo Folder Resources { >> %%f.makefile
	echo 	../durango_files/Package.appxmanifest >> %%f.makefile
	echo 	../durango_files/Logo.png >> %%f.makefile
	echo 	../durango_files/SmallLogo.png >> %%f.makefile
	echo 	../durango_files/SplashScreen.png >> %%f.makefile
	echo 	../durango_files/StoreLogo.png >> %%f.makefile
	echo } >> %%f.makefile
	echo } >> %%f.makefile

	echo %CD%\%%f.makefile >> %1.makefiles

	REM TODO - check suite and base to locate project
	echo Libraries { >> %%f.makefile
	for %%g in (%LIBS%) do (
		if EXIST %%~png.txt (
			echo  %%~png >> %%f.makefile
		) else (
			for %%h in (%XPROJ%) do IF EXIST %%h\%%g\makefile.txt (
				echo  %%h\%%g\%%g >> %%f.makefile
			)
		)
	)
	echo  %RAGE_DIR%\stlport\STLport-5.0RC5\src\stlport >> %%f.makefile
	echo } >> %%f.makefile
)

echo } >> %1.slndef
echo } >> %1.slndef

REM This processes the vcxproj's
%RS_TOOLSROOT%\bin\ProjectGenerator\ProjectGenerator %1.makefiles
REM And this updates the solutions
%RS_TOOLSROOT%\script\coding\projbuild\projGen.bat
