@echo off
SETLOCAL

call setenv.bat
 
%RS_TOOLSRUBY% %RS_TOOLSROOT%\script\coding\projbuild\helper.rb --project=%1 --in_format=rageprojbuilder --unity_build_enabled type=project
IF %ERRORLEVEL% NEQ 0 (
    ECHO *********************************************************************
    ECHO *********************************************************************
    ECHO **** See alert popup.
    ECHO **** 
    ECHO **** IMPORTANT %1 failed to convert with ret code %ERRORLEVEL%
    ECHO ****     
    ECHO **** 1. Take note of this project name : %1 
    ECHO **** 2. Once all further conversions are complete rerun rebuild_vcproj.bat in the folder of this project file. 
    ECHO **** 3. If thereafter the same library continues to fail please contact your tools dept. 
    ECHO **** .
    ECHO **** NB.This is a non-fixable Ruby gem LiBXML2 issue.
    ECHO *********************************************************************
    ECHO *********************************************************************
    ruby -e "str=ARGV[0].dup;msgbox = Win32API.new( 'user32', 'MessageBox', ['i', 'p', 'p', 'i'],'i' );msgbox.call(0, str, 'PROJECT BUILDER ERROR', 48 )" '**** IMPORTANT %1 failed to convert with ret code %ERRORLEVEL% **** 1. Take note of this project name : %1 2. Once all further conversions are complete rerun rebuild_vcproj.bat in the folder of this project file. 3. If thereafter the same library continues to fail please contact your tools dept. NB.This is a non-fixable Ruby gem LiBXML2 issue.'
    pause     
)

ENDLOCAL
