#
# File:: genragevcproj.rb
# Description:: Replacement for the old rage batch file genragevcproj.bat that 
#               rebuilt all rage projects.
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 12 February 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------
require 'pipeline/coding/projbuild/helper'
require 'pipeline/gui/exception_dialog'
require 'pipeline/os/getopt'
include Pipeline

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------
OPTIONS = [
	[ '--out_format', '-o', Getopt::REQUIRED, 'Output format (vs2008, vs2010).' ],
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

begin
	options, trailing = OS::Getopt::getopts( OPTIONS )
	config = Pipeline::Config.instance()
	config.project.load_config()
	builder = ProjBuild::ProjBuilderHelper.new()

	# Override builder output format.
	if ( not options['out_format'].nil? ) then
		builder.out_format = options['out_format'].to_sym
	end
	
	builder.set_root(config.project.ragecode)
	builder.build("naturalmotion/vcproj/naturalmotion")
	builder.build_in("base/src/vcproj")
	builder.build_in("suite/src/vcproj")
	builder.build_in("contrib/src/vcproj")
	builder.build_in("script/src/vcproj")
	builder.build_in("framework/src/vcproj")
	builder.build("speedtree/src/vcproj/speedtree")
	builder.build("scaleform/src/vcproj/scaleformgfx")
	builder.build("stlport/stlport-5.0rc5/src/","stlport")
	
rescue Exception => ex
	
	GUI::ExceptionDialog::show_dialog( ex, ex.message )
end
	