#
# File:: helper.rb
# Description::	Command line wrapper for the helper script for projbuild project generation
#
# Author:: Greg Smith <greg@rockstarnorth.com>
# Author:: David Muir <david.muir@rockstarnorth.com>
# Date:: 5 February 2010
#

#-----------------------------------------------------------------------------
# Uses
#-----------------------------------------------------------------------------

require 'pipeline/coding/projbuild/helper.rb'
require 'pipeline/os/getopt'

#-----------------------------------------------------------------------------
# Constants
#-----------------------------------------------------------------------------

OPTIONS = [
	['--path', '-f', Getopt::OPTIONAL, 'project path to build' ],
	['--project', '-f', Getopt::OPTIONAL, 'optional project file to look for' ],
	['--is_folder', '-f', Getopt::BOOLEAN, 'whether this is a folder to look in, or a single project' ],
	['--in_format', '-f', Getopt::OPTIONAL, 'file format to import (in case of conflicting extensions)' ],
	['--out_type', '-f', Getopt::OPTIONAL, 'file type to export (project or solution)' ],
	['--out_format', '-f', Getopt::OPTIONAL, 'file format to export (in case of conflicting extensions)' ],
	['--unity_build_enabled', '-u', Getopt::BOOLEAN, 'unity build enabled' ],
	
]

#-----------------------------------------------------------------------------
# Implementation
#-----------------------------------------------------------------------------

begin

	# DW: Enforce log level info due to existing tools install issue with log level debug being set for new installs.
	g_Config = Pipeline::Config.instance( ) 
	g_Config::log_level = 2 if g_Config::log_level < 2

	opts, trailing = Pipeline::OS::Getopt.getopts( OPTIONS )
	options = Hash.new
													
	trailing.each { |item|

		tokens = item.split("=")
	
		key = tokens[0].intern
	
		if key == :type then
	
			options[key] = tokens[1].intern
		else
	
			options[key] = tokens[1]
		end
	}

	options = nil if options.keys.size == 0

	in_format = nil
	in_format = opts["in_format"].intern if opts["in_format"] != nil

	out_format = nil
	out_format = opts["out_format"].intern if opts["out_format"] != nil

	unity_build_enabled = opts["unity_build_enabled"]

	out_type = nil
	out_type = opts["out_type"].intern if opts["out_type"] != nil
	
	log = Pipeline::LogSystem.instance().rootlog
	log.debug("path: #{opts['path']}")
	log.debug("project: #{opts['project']}")
	log.debug("is_folder: #{opts['is_folder']}")
	log.debug("input format: #{opts['in_format']}")
	log.debug("output format: #{opts['out_format']}")
	log.debug("output type: #{opts['out_type']}")

	builder = Pipeline::ProjBuild::ProjBuilderHelper.new()
	builder.in_options = options if options != nil
	builder.in_format = in_format if in_format != nil
	builder.out_options = options if options != nil
	builder.out_format = out_format if out_format != nil
	builder.unity_build_enabled = unity_build_enabled if unity_build_enabled != nil
	builder.out_type = out_type if out_type != nil

	path = Dir.getwd
	path = opts['path'] if opts['path'] != nil

	if opts["is_folder"] then

		builder.build_in(path)
	else

		builder.build(path,opts['project'])
	end

rescue Exception => ex

	puts "Unhandled exception: #{ex.message}!"
	puts ex.backtrace.join( "\n" )
end
