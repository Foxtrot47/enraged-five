@echo off

REM This is not done as a block because turning on delayed expansion also keeps cd from working.
if "%1"=="back" set BACKBACK=%BACK%
if "%1"=="back" set BACK=%CD%
if "%1"=="back"	cd /d %BACKBACK%
if "%1"=="back" set BACKBACK=
if "%1"=="back" exit/b


set BACK=%CD%

if "%1"=="ragebuilder" (
	cd /d %RS_CODEBRANCH%\rage\framework\tools\src\cli\ragebuilder
	exit/b
) 

if "%1"=="scriptcompiler" (
	cd /d %RS_CODEBRANCH%\rage\script\tools\scriptcompiler
	exit/b
)

for %%p in (%RS_BUILDBRANCH% %RS_BUILDBRANCH%\common %RS_TOOLSROOT% %RS_CODEBRANCH%\game %RS_CODEBRANCH%\rage\base\src %RS_CODEBRANCH%\rage\base\samples %RS_CODEBRANCH%\rage\base\tools\libs %RS_CODEBRANCH%\rage\base\tools\cli %RS_CODEBRANCH%\rage\suite\src %RS_CODEBRANCH%\rage\suite\samples %RS_CODEBRANCH%\rage\framework\src) do (
	if EXIST %%p\%1 if NOT "%CD%"=="%%p\%1" (
		cd /d %%p\%1
		exit/b
	)
)
