@ECHO OFF
REM
REM File:: %RS_TOOLSROOT%/script/coding/tools/generate_all_doxygen.bat
REM Description:: Doxygen documentation generation script for all tools libraries.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 16 April 2012
REM

CALL setenv.bat

SETLOCAL
SET GENERATE=%RS_TOOLSROOT%\script\coding\tools\generate.bat
SET PATH=%PATH%;%RS_TOOLSBIN%\doxygen;%RS_TOOLSBIN%\doxygen\htmlhelp
SET OUTPUT_PATH=%RAGE_DIR%\framework\tools\doc
PUSHD %RAGE_DIR%

IF EXIST %OUTPUT_PATH% GOTO TREEPROCESS
MKDIR %OUTPUT_PATH%

:TREEPROCESS
rem Do whatever you want here over the files of this subdir, for example:
REM CHO Current Directory: %CD%
IF EXIST %CD%\Doxyfile %GENERATE% %CD%
for /D %%d in (*) do (
    PUSHD %%d
    CALL :TREEPROCESS
    POPD
)

EXIT /B
