@ECHO OFF
REM
REM File:: %RS_TOOLSBIN%/doxygen/generate.bat
REM Description:: Doxygen documentation generation script.
REM
REM Author:: David Muir <david.muir@rockstarnorth.com>
REM Date:: 16 April 2012
REM

CALL setenv.bat > NUL

SETLOCAL
SET PROCESSOR=doxygen.exe
SET PATH=%PATH%;%RS_TOOLSBIN%\doxygen;%RS_TOOLSBIN%\doxygen\htmlhelp
SET OUTPUT_PATH=%RAGE_DIR%\framework\tools\doc
SET INPUT_PATH=%1

IF EXIST %OUTPUT_PATH% GOTO GENERATE
MKDIR %OUTPUT_PATH%

:GENERATE
ECHO Output Path: %OUTPUT_PATH%
ECHO Output File: %~nx1.chm

%PROCESSOR% %INPUT_PATH%\Doxyfile
COPY /Y %INPUT_PATH%\doc\html\index.chm %OUTPUT_PATH%\%~nx1.chm

EXIT /B
