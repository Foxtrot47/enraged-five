require 'getoptlong'
require 'csv'
require 'pp'
# Setting up
RAGE_DIR = ENV['RAGE_DIR'].gsub(/\\/, "/")
RS_CODEBRANCH = ENV['RS_CODEBRANCH'].gsub(/\\/, "/")
RS_BUILDBRANCH = ENV['RS_BUILDBRANCH'].gsub(/\\/, "/")
RS_TOOLSROOT = ENV['RS_TOOLSROOT'].gsub(/\\/, "/")
RS_TITLE = ENV['RS_TITLE']
USER_PROFILE = ENV['USERPROFILE']
USER_DESKTOP = USER_PROFILE + "/Desktop"
USERNAME = ENV['USERNAME']
PROTECTION_PATH = RS_TOOLSROOT + "/script/coding/protection"
COMMON_RUBY_PATH = RS_TOOLSROOT + "/script/coding/protection/common/ruby"
TOOL_PATH = PROTECTION_PATH + "/guardSweeper"
GENERATED_PATH = PROTECTION_PATH + "/generated"
CURR_TIME = Time.now.strftime("%Y%m%d.%H.%M.%S")   
GENERATED_TIME_PATH =  GENERATED_PATH +  "/" + CURR_TIME 
$LOAD_PATH << COMMON_RUBY_PATH
DOT_PATH = RAGE_DIR + "/3rdparty/Arxan/GuardIT/8.4.1/plugins/com.arxan.guardit_8.4.1/graphviz/dot.exe"
# Creating our logger
require 'RsgLog'
LOG_SEVERITY_LEVEL = Logger::DEBUG
log = RsgLog::RsgLog.instance(GENERATED_PATH,CURR_TIME, LOG_SEVERITY_LEVEL)

# Alright, lets start the fun

# Do the work for our parameters

class Parameters
    attr_reader :guardlog
    attr_reader :guardscript
    def initialize
        
        @guardlog = ""
        @guardscript = ""
        @opts = GetoptLong.new(
          [ '--guardlog', '-l',      GetoptLong::REQUIRED_ARGUMENT ],
          [ '--guardscript', '-g',      GetoptLong::REQUIRED_ARGUMENT ]
        )
    end
    def PrintHelp()
        RsgLog::RsgLog.instance().info("DNE Yet");
    end           
    def ParseArguments()
        @opts.each do |opt, arg|
            case opt
            when '--help'
                PrintHelp()
                exit
            when '--guardlog'
                @guardlog = arg
            when '--guardscript'
                @guardscript = arg
            end
        end
    end
end


# Define a class for our results
class GuardItem
    attr_reader   :invoked
    attr_reader   :ran
    attr_reader   :exited
    attr_reader   :fired
    
    def initialize
        @ran = 0
        @invoked = 0
        @exited = 0
        @fired = 0
    end
    
    def invoked
        @invoked = @invoked +1
    end
    
    def ran
        @ran = @ran +1
    end
    
    def fired
        @fired = @fired +1
    end
    
    def exited
        @exited = @exited +1
    end
    
    def to_s
        "\n\tFired:\t#{@fired}\n\tInvoked:#{@invoked}\n\tRan:\t#{@ran}\n\tExited:\t#{@exited}"
    end
    
    def to_array
        {"FIRED"=> @fired, "INVOKED"=>@invoked, "RAN"=>@ran, "EXITED"=>@exited}
    end
    
end

# Create a sub-dir

begin
    Dir.mkdir(GENERATED_TIME_PATH)
rescue Exception =>e
    puts e.message
end
    
log.info("Starting the magic...")
log.info("Parsing arguments...")
parameters = Parameters.new
parameters.ParseArguments()

if parameters.guardlog == "" || (File::file?(parameters.guardlog) == 0)
    parameters.PrintHelp()
    log.error("Invalid path [#{parameters.guardlog}] specified")
    exit
elsif parameters.guardscript == "" || (File::file?(parameters.guardscript) == 0)
    parameters.PrintHelp()
    log.error("Invalid path [#{parameters.guardscript}] specified")
    exit
end

# Open and read the file 
gsoutput = File.read(parameters.guardscript)

# Create our arrays
canonicalGuardNames = Array.new
guards = Hash.new


guardNameRegex = Regexp.new('.*<guard_cmd name="(.*)".*')
guardRanRegex  = Regexp.new('\[[0-9][0-9]+\] (.*) ran.*')
guardInvokedRegex  = Regexp.new('\[[0-9][0-9]+\] (.*) invoked.*')
guardFiredRegex  = Regexp.new('\[[0-9][0-9]+\] (.*) fired.*')
guardExitedRegex  = Regexp.new('\[[0-9][0-9]+\] (.*) exited.*')

gsoutput.each_line do |line|
    matchData = guardNameRegex.match(line.strip)
    if matchData == nil
        #log.debug("No bueno RegExp for #{line}")
    else
        guardName = matchData[1]
        canonicalGuardNames.push(guardName)
    end
end

gloutput = File.read(parameters.guardlog)
gloutput.each_line do |line|
    if line =~ /algorithm/i
        next
    end
    
    matchData = guardRanRegex.match(line.strip)
    if matchData != nil
        guardName = matchData[1]
        if guardName =~/.*\[[0-9]\]/
            guardName = guardName[0..-4]
        end
        
        if guards.has_key?(guardName) == false
            guards[guardName]=GuardItem.new
        end
        guards[guardName].ran()
    end
    
    
    matchData = guardInvokedRegex.match(line.strip)
    if matchData != nil
        guardName = matchData[1]
        if guardName =~/.*\[[0-9]\]/
            guardName = guardName[0..-4]
        end
        
        if guards.has_key?(guardName) == false
            guards[guardName]=GuardItem.new
        end
        guards[guardName].invoked()
    end
    
    matchData = guardFiredRegex.match(line.strip)
    if matchData != nil
        guardName = matchData[1]
        if guardName =~/.*\[[0-9]\]/
            guardName = guardName[0..-4]
        end
        if guards.has_key?(guardName) == false
            guards[guardName]=GuardItem.new
        end
        guards[guardName].fired()
    end
    
    
    matchData = guardExitedRegex.match(line.strip)
    if matchData != nil
        guardName = matchData[1]
        if guardName =~/.*\[[0-9]\]/
            guardName = guardName[0..-4]
        end
        
        if guards.has_key?(guardName) == false
            guards[guardName]=GuardItem.new
        end
        guards[guardName].exited()
    end
    
end

guards.each do |key, array|
  log.info("\n#{key}#{array}")
 
end


canonicalGuardNames = canonicalGuardNames.uniq{|x| x.user_id}
# And we should be done!
log.info("Exiting!")