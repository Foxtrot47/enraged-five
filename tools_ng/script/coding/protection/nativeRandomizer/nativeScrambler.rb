require 'pp'
require 'open3'
# Get environment Variable
RS_CODEBRANCH = ENV['RS_CODEBRANCH']
RS_SCRIPTBRANCH = ENV['RS_SCRIPTBRANCH']
# Pull the directory we need
CODE_DIR   = RS_CODEBRANCH.gsub("\\", "/" ) + "/game/script/"

# Now lets do some work

# Get our glob pattern
codeHeaderGlob  = CODE_DIR + "commands_*.cpp"
# Run the glob
codeFiles = Dir.glob(codeHeaderGlob)



# Create our Perforce Changelist, with all the edits we need
# Read from the gta5files.txt file
# gtafilesContents = File.read(GTAVFILES)
# Build our string, from both of the data sources we have
filesForEdit = []
codeFiles.each { |x|
#    filesForEdit = filesForEdit + "\t" + x.strip.gsub("/", "\\" ) + "\n"
	filesForEdit << x.strip.gsub("/", "\\" )
}

# Iterate over each file
filesForEdit.each { |file|

    # Display
    puts "Processing #{file}..."
    # Read the file
    fileContentLines = IO.readlines(file)
    # Iterate over the ocntents, such that we can replace each
    # UUID separately
    applicableLines = Array.new
    applicableLinesIndicies = Array.new
    okayProcess = false
    preprocessorCount = 0
    fileContentLines.each_with_index { |line, index| 
        # Only start processing once we've gotten to our function, just in case
        if line =~ /void SetupScriptCommands/
            okayProcess = true
        # And finish once we've reached the exit of our function
        elsif line =~ /\}/
            okayProcess = false
        elsif okayProcess == true
            # Handle any preprocessor directives in a very lazy way
            if line =~ /#if/
                preprocessorCount = preprocessorCount + 1
            elsif line =~ /#endif/
                preprocessorCount = preprocessorCount - 1
            end 
            # If we're not in a preprocessor directive, go ahead and add it to
            # our candidates of potential shuffles
            if preprocessorCount == 0
                if line =~ /.*SCR_REGISTER.*/
                    # Do the replacement
                    # newContents += line.gsub(/\s*= \"0x\w+\"/, "")
                    #puts "Found #{line} with index #{index}"
                    applicableLinesIndicies.push(index)
                    applicableLines.push(line)
                end
            end
        end
    }
    # Shuffle our indices three times, just for good luck
    applicableLinesIndicies.shuffle!.shuffle!.shuffle!
    
    # Now lets write out our file, all over again
    applicableLinesIndicies.each_with_index { |referralIndex, index| 
        puts "Setting\n\t#{fileContentLines[referralIndex].strip}\n\tto\n\t#{applicableLines[index].strip}"
        fileContentLines[referralIndex] = applicableLines[index]
    }
    # Write the file back out to disk
    File.open(file, 'w') { |writeFile|
        writeFile.write(fileContentLines)
    }   
}

