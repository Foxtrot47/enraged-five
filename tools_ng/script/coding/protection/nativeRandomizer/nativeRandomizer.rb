require 'pp'
require 'open3'
# Get environment Variable
RS_CODEBRANCH = ENV['RS_CODEBRANCH']
RS_SCRIPTBRANCH = ENV['RS_SCRIPTBRANCH']
#GTAVFILES = ENV['RS_SCRIPTBRANCH'] + "/gta5_files.txt"
# Pull the directory we need
CODE_HEADER_DIR   = RS_CODEBRANCH.gsub("\\", "/" ) + "/game/script_headers/"
SCRIPT_HEADER_DIR   = RS_SCRIPTBRANCH.gsub("\\", "/" ) + "/core/common/native/"

# Now lets do some work

# Get our glob pattern
codeHeaderGlob  = CODE_HEADER_DIR + "*.sch"
# Run the glob
codeFiles = Dir.glob(codeHeaderGlob)

# Get our glob pattern
scriptHeaderGlob  = SCRIPT_HEADER_DIR + "*.sch"
# Run the glob
scriptFiles = Dir.glob(scriptHeaderGlob)

# Create our Perforce Changelist, with all the edits we need
# Read from the gta5files.txt file
# gtafilesContents = File.read(GTAVFILES)
# Build our string, from both of the data sources we have
filesForEdit = []
scriptFiles.each { |x|
#    filesForEdit = filesForEdit + "\t" + x.strip.gsub("/", "\\" ) + "\n"
	filesForEdit << x.strip.gsub("/", "\\" )
}
codeFiles.each { |x|
#    filesForEdit = filesForEdit + "\t" + x.strip.gsub("/", "\\" ) + "\n"
	filesForEdit << x.strip.gsub("/", "\\" )
}

# Iterate over each file
filesForEdit.each { |file|
    # Only worry about files that do commands_ work
    if file =~ /commands_/
        # Display
        puts "Processing #{file}..."
        # Read the file
        fileContents = File.read(file)
        newContents = ""
        # Iterate over the ocntents, such that we can replace each
        # UUID separately
        fileContents.each_line { |line| 
            if line =~ /NATIVE.*0x/
                # Do the replacement
                newContents += line.gsub(/\s*= \"0x\w+\"/, "")
            else
                newContents += line
            end
        }
        # Write the file back out to disk
        File.open(file, 'w') { |writeFile|
            writeFile.write(newContents)
        }    
    end
}

