
require 'getoptlong'
require 'fileutils'
class Parameters
    attr_reader :storeDir
    def initialize
        @storeDir = ""
        
        @opts = GetoptLong.new(
          [ '--storageDir', '-s',      GetoptLong::REQUIRED_ARGUMENT ]
        )
    end
    
    def PrintHelp()
        puts "usage:"
        puts "ruby artifactCollector.rb -s <storageDirectory>"
        puts "                          This is the directory to write the artifacts out to"
    end           
    def ParseArguments()
        @opts.each do |opt, arg|
            case opt
            when '--help'
                PrintHelp()
                exit
            when '--storageDir'
                @storeDir = arg
            end
        end
    end
end

def safecreatepath(path)
    begin
    FileUtils.mkdir_p(path) unless File.exists?(path)
    rescue Exception =>e
        puts e.message
    end
end

# Parsing parameters
parameters = Parameters.new
parameters.ParseArguments()

if parameters.storeDir == "" 
    parameters.PrintHelp()
    exit
end
# Setting up
RS_TOOLSROOT = ENV['RS_TOOLSROOT'].gsub(/\\/, "/")
PROTECTION_PATH = RS_TOOLSROOT + "/script/coding/protection"
GENERATED_PATH = PROTECTION_PATH + "/generated"
GENERATED_GLOB = GENERATED_PATH + "/*"


# Get the files that I generate
latestFile = Dir.glob(GENERATED_GLOB).max_by {|f| File.mtime(f)}
timestampRegex = Regexp.new('([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)')
matchData = timestampRegex.match(latestFile)

# Create our destination directory
destinationPath = parameters.storeDir + "/#{matchData[1]}"

safecreatepath(destinationPath)

globOfLatestFiles = GENERATED_PATH + "/*#{matchData[1]}*"
latestFiles = Dir.glob(globOfLatestFiles)

# Copy all the files that I care about.
latestFiles.each { |file|
    begin
        FileUtils.cp(file, destinationPath)
    rescue Exception =>e
        puts "FILECOPY/WARNING: " + e.message
    end
}