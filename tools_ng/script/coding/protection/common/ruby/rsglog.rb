module  RsgLog
    require 'Logger'
    require 'singleton'
    require 'fileutils'


    class MultiDelegator
      def initialize(*targets)
        @targets = targets
      end

      def self.delegate(*methods)
        methods.each do |m|
          define_method(m) do |*args|
            @targets.map { |t| t.send(m, *args) }
          end
        end
        self
      end

      class <<self
        alias to new
      end
    end

    class RsgLog
        include Singleton
        attr_accessor :log
        attr_accessor :logFile
        attr_accessor :isInitialized
        def SetParameters(generatedPath, currTime, severity)
            logFileHandle = File.open(generatedPath + "/" + currTime + ".log", "w")
            @log = Logger.new MultiDelegator.delegate(:write, :close).to(STDOUT)
            @logFile = Logger.new MultiDelegator.delegate(:write, :close).to(logFileHandle)
            puts "#{generatedPath}"
            logFileHandle.write("woot")
            @log.datetime_format = "%Y%m%d-%H:%M:%S"
            if(defined?(LOG_FILE_SEVERITY_LEVEL)).nil?
                @log.level = Logger::INFO
            else
                @log.level = LOG_SEVERITY_LEVEL
            end
            
            @log.formatter = proc do |serverity, time, progname, msg|
              "[#{time.strftime(@log.datetime_format)}]\t[#{serverity}]\t#{msg}"
            end
              
            @logFile.datetime_format = "%Y%m%d-%H:%M:%S"
            if (defined?(LOG_FILE_SEVERITY_LEVEL)).nil?
                @logFile.level = Logger::DEBUG
            else
                @logFile.level = LOG_FILE_SEVERITY_LEVEL
            end
            @logFile.formatter = proc do |serverity, time, progname, msg|
              "[#{time.strftime(@logFile.datetime_format)}]\t[#{serverity}]\t#{msg}"
            end
            @isInitialized = true
        end
        def checkInitialized
            if(!isInitialized)
                raise "RsgLog attempting to be used without being initialized. Please call the SetParameters() function in the accessing script. Exiting"
            end
        end
        def info(msg, newline=true)
            checkInitialized()
            if newline == true
                msg = msg + "\n"
            end
            @log.info(msg)
            @logFile.info(msg)
        end
        def debug(msg, newline=true)
            checkInitialized()
            if newline == true
                msg = msg + "\n"
            end
            @log.debug(msg)
            @logFile.debug(msg)
        end
        def warn(msg, newline=true)
            checkInitialized()
            if newline == true
                msg = msg + "\n"
            end
            @log.warn(msg)
            @logFile.warn(msg)
        end
        def error(msg, newline=true)
            checkInitialized()
            if newline == true
                msg = msg + "\n"
            end
            @log.error(msg)
            @logFile.error(msg)
        end
        
        def SetLevel(level)
            @log.level = level
        end

    end

end