require 'Win32API'
require 'fileutils'


def message_box(message, title, type)
   # Create a Windows MessageBox.
   # 0 = 'OK' Button
   # 1 = 'OK' 'Cancel' Buttons
   # 2 = 'Abort' 'Retry' 'Ignore' Buttons
   # 3 = 'Yes' 'No' 'Cancel' Buttons
   # 4 = 'Yes' 'No' Buttons
   # 5 = 'Retry' 'Cancel' Buttons
   # 6 = 'Cancel' 'Try Again' 'Continue'
   #######################
   # 16 = 'OK' Button with 'Error' Symbol
   # ... SEE ABOVE EXAMPLES
   # 22 = 'Cancel' 'Try Again' 'Continue' Buttons with 'Error'
   #######################################
   # 32 = 'OK' Button with 'Question' Symbol
   # ... SEE ABOVE EXAMPLES
   # 38 = 'Cancel' 'Try Again' 'Continue' Buttons with 'Question'
   #########################################
   # 48 = 'OK' Button with 'Warning' Symbol
   # ... SEE ABOVE EXAMPLES
   # 54 = 'Cancel' 'Try Again' 'Continue' Buttons with 'Warning'
   ########################################
   # 64 = 'OK' Button with 'Info' Symbol
   # ... SEE ABOVE EXAMPLES
   # 70 = 'Cancel' 'Try Again' 'Continue' Buttons with 'Info'
   ######################################
   mb = Win32API.new("user32", "MessageBox", ['i','p','p','i'], 'i')
   mb.call(0, message, title, type)
end

# Lets define up our globals
TOOLS_PATH = ENV['RS_TOOLSROOT']
SRC_PATH = ENV['RS_CODEBRANCH']

RAGE_PATH = ENV['RAGE_DIR']
GUARDIT_GUI_PATH = RAGE_PATH + "/3rdparty/Arxan/GuardIT/8.3.0"
GUARDIT_GUI_BIN = GUARDIT_GUI_PATH + "/eclipse.exe"

PROTECTION_PATH = TOOLS_PATH + "/script/coding/protection"
GENERATED_PATH = PROTECTION_PATH + "/generated"
GUARDIT_TOOLS_PATH = PROTECTION_PATH + "/guardit"
WORKSPACE_PATH = GUARDIT_TOOLS_PATH + "/workspace"
CONFIGURATION_PATH = GUARDIT_TOOLS_PATH + "/configuration"

# Launch GuardIT
system(GUARDIT_GUI_BIN + " -data " + WORKSPACE_PATH)
