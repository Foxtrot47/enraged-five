require 'Win32API'
require 'fileutils'

# Lets define up our globals
TOOLS_PATH = ENV['RS_TOOLSROOT']
SRC_PATH = ENV['RS_CODEBRANCH']

RAGE_PATH = ENV['RAGE_DIR']
GUARDIT_GUI_PATH = RAGE_PATH + "/3rdparty/Arxan/GuardIT/8.3.0"
GUARDIT_GUI_BIN = GUARDIT_GUI_PATH + "/eclipse.exe"

PROTECTION_PATH = TOOLS_PATH + "/script/coding/protection"
GENERATED_PATH = PROTECTION_PATH + "/generated"
GUARDIT_TOOLS_PATH = PROTECTION_PATH + "/guardit"
WORKSPACE_PATH = GUARDIT_TOOLS_PATH + "/workspace"
CONFIGURATION_PATH = GUARDIT_TOOLS_PATH + "/configuration"

# Register DIA SDK
system('X:\gta5\src\dev_ng\rage\3rdparty\Arxan\GuardIT\8.3.0\plugins\com.arxan.guardit_8.3.0\diareg64.exe')
# Now lets checkout our workspace so that it's writable
# system('p4 edit ' + WORKSPACE_PATH + "/...")
system('p4 edit ' + CONFIGURATION_PATH + "/...")
system('p4 edit ' + PROTECTION_PATH + "/gtav_pc.gsml")
# Okay, in order for this to work I have to copy over some settings to your local path.
begin
    FileUtils.rm_f(SRC_PATH + "/.settings")
    FileUtils.rm_f(SRC_PATH + "/.guarditproject")
    FileUtils.rm_f(SRC_PATH + "/.project")
    FileUtils.rm_f(SRC_PATH + "/gtav_pc.gsml")
    FileUtils.rm_f(SRC_PATH + "/guardit_project_config.xml")
rescue Exception =>e
end

begin
    FileUtils.cp_r(CONFIGURATION_PATH + "/.", SRC_PATH + "/..")
    FileUtils.cp_r(PROTECTION_PATH + "/gtav_pc.gsml", SRC_PATH + "/..")
rescue Exception =>e
    puts "There was an error when copying over the configurational files: " 
    puts e.message
end


# Create our generated directory
begin
    Dir.mkdir(GENERATED_PATH)
rescue Exception =>e
    puts e.message
end
