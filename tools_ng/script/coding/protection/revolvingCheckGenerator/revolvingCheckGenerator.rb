require 'pp'
require 'open3'

REVOLVING_ITEMS = 100 
def GetRandomOperator()

	op = rand(5)
	if op == 0
		return "+"
	elsif op == 1
		return "^"
	elsif op == 2
		return "*"
	elsif op == 3
		return "&"
	else
		return "|"
	end
end

def GetRandomMember()

	op = rand(4)
	if op == 0
		return "a"
	elsif op == 1
		return "b"
	elsif op == 2
		return "c"
	elsif op == 3
		return "d"
	end
end

def GenerateRandomExpression()
	newExpr = GetRandomMember()
	newExpr.concat("=")
	newExpr.concat(GetRandomMember())
	newExpr.concat(GetRandomOperator())
	newExpr.concat(GetRandomMember())
	return newExpr
end

def GenerateRevolvingCode()
	newText = "\tint selector = d % #{REVOLVING_ITEMS};\n"
	
	for i in (0..REVOLVING_ITEMS-1) do
	  formattedNumHex = sprintf("0x%03x", i)
	  formattedNum = sprintf("%03d", i)
	  newText.concat("\tif(selector == #{formattedNumHex}) {\n")
	  newText.concat("\t\t//@@: location REVOLVING_LOCATION_#{formattedNum}\n\t\t")
	  newText.concat(GenerateRandomExpression() + ";")
	  newText.concat(GenerateRandomExpression() + ";")
	  newText.concat(GenerateRandomExpression() + ";")
	  newText.concat(GenerateRandomExpression() + ";")
	  newText.concat(GenerateRandomExpression() + ";\n")
	  newText.concat("\t}\n")
	end
	
	return newText
end
# Get environment Variable
RS_CODEBRANCH = ENV['RS_CODEBRANCH']
RS_SCRIPTBRANCH = ENV['RS_SCRIPTBRANCH']

#X:\gta5\src\dev_ng\game\security\plugins\revolvingcheckerplugin.cpp
CODE_HEADER_DIR   = RS_CODEBRANCH.gsub("\\", "/" ) + "/game/security/plugins/"

# Now lets do some work

# Get our glob pattern
codeHeaderGlob  = CODE_HEADER_DIR + "*cpp"
# Run the glob
codeFiles = Dir.glob(codeHeaderGlob)

# Create our Perforce Changelist, with all the edits we need
# Read from the gta5files.txt file
# gtafilesContents = File.read(GTAVFILES)
# Build our string, from both of the data sources we have
filesForEdit = []

codeFiles.each { |x|
#    filesForEdit = filesForEdit + "\t" + x.strip.gsub("/", "\\" ) + "\n"
	filesForEdit << x.strip.gsub("/", "\\" )
}

# Iterate over each file
filesForEdit.each { |file|
    # Only worry about files that do commands_ work
    if file =~ /revolvingcheckerplugin/
        # Display
        puts "Processing #{file}..."
        # Read the file
        fileContents = File.read(file)
        newContents = ""
        # Iterate over the contents, such that we can replace each
        # UUID separately
		replaceToggle = false
        fileContents.each_line { |line| 

			if line =~ /REVOLVINGCHECKERPLUGIN_WORK_NESTED_BODY/
				if replaceToggle
					newContents.concat("$GENERATE_REVOLVING\n")
				end
                replaceToggle = replaceToggle ^ true
				newContents.concat(line)
			elsif replaceToggle == false
				newContents.concat(line)				
            end
        }
		
		newContents = newContents.gsub("$GENERATE_REVOLVING", GenerateRevolvingCode())
        # Write the file back out to disk
        File.open(file, 'w') { |writeFile|
            writeFile.write(newContents)
        }    
    end
}

