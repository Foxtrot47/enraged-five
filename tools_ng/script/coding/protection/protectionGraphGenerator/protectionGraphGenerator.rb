require 'getoptlong'
require 'csv'
require 'pp'
# Setting up
RAGE_DIR = ENV['RAGE_DIR'].gsub(/\\/, "/")
RS_CODEBRANCH = ENV['RS_CODEBRANCH'].gsub(/\\/, "/")
RS_BUILDBRANCH = ENV['RS_BUILDBRANCH'].gsub(/\\/, "/")
RS_TOOLSROOT = ENV['RS_TOOLSROOT'].gsub(/\\/, "/")
RS_TITLE = ENV['RS_TITLE']
USER_PROFILE = ENV['USERPROFILE']
USER_DESKTOP = USER_PROFILE + "/Desktop"
USERNAME = ENV['USERNAME']
PROTECTION_PATH = RS_TOOLSROOT + "/script/coding/protection"
COMMON_RUBY_PATH = RS_TOOLSROOT + "/script/coding/protection/common/ruby"
TOOL_PATH = PROTECTION_PATH + "/protectionGraphGenerator"
GENERATED_PATH = PROTECTION_PATH + "/generated"
CURR_TIME = Time.now.strftime("%Y%m%d.%H.%M.%S")   
GENERATED_TIME_PATH =  GENERATED_PATH +  "/" + CURR_TIME 
$LOAD_PATH << COMMON_RUBY_PATH
DOT_PATH = RAGE_DIR + "/3rdparty/Arxan/GuardIT/8.5.1/plugins/com.arxan.guardit_8.5.1/graphviz/dot.exe"
# Creating our logger
require 'RsgLog'
LOG_SEVERITY_LEVEL = Logger::INFO
log = RsgLog::RsgLog.instance()
log.SetParameters(GENERATED_PATH,CURR_TIME, LOG_SEVERITY_LEVEL)

# Alright, lets start the fun

# Do the work for our parameters

class Parameters
    attr_reader :path
    attr_reader :numnetworks
    def initialize
        @numnetworks = 0
        @path = ""
        @opts = GetoptLong.new(
          [ '--path', '-p',      GetoptLong::REQUIRED_ARGUMENT ],
          [ '--numnetworks', '-n',      GetoptLong::REQUIRED_ARGUMENT ]
        )
    end
    def PrintHelp()
        RsgLog::RsgLog.instance().info("DNE Yet");
    end           
    def ParseArguments()
        @opts.each do |opt, arg|
            case opt
            when '--help'
                PrintHelp()
                exit
            when '--path'
                @path = arg
            when '--numnetworks'
                @numnetworks = arg.to_i
            end
        end
    end
end

# Create a sub-dir

begin
    Dir.mkdir(GENERATED_TIME_PATH)
rescue Exception =>e
    puts e.message
end
    
log.info("Starting the magic...")
log.info("Parsing arguments...")
parameters = Parameters.new
parameters.ParseArguments()


if parameters.path == "" || (File::file?(parameters.path) == 0)
    parameters.PrintHelp()
    log.error("Invalid path [#{parameters.path}] specified")
    exit
elsif parameters.numnetworks == 0
    parameters.PrintHelp()
    log.error("Invalid number of networks [#{parameters.numnetworks}] specified")
    exit  
end

# Open and read the file 
output = File.read(parameters.path)

prologue = Array.new
definitions = Array.new
mappings = Array.new
epilogue = Array.new

definitionRegex = Regexp.new('.*\[label.*')
mappingsRegex   = Regexp.new('.*-.*\[color.*')
isPrologue = true
output.each_line do |line|
    if line.strip.empty?
    elsif isPrologue == true
        definitionMatch = definitionRegex.match(line.strip)
        mappingsMatch = mappingsRegex.match(line.strip)
        if definitionMatch != nil
            definitions.push(line)
        elsif mappingsMatch != nil
            mappings.push(line)
        elsif mappings.length > 0
            isPrologue = false
        else
            if definitions.length <= 0 
                prologue.push(line)
            end
        end 
    else
        epilogue.push(line)
    end
end

log.info("Prologue is #{prologue.length} long")
log.info("Definits is #{definitions.length} long")
log.info("Mappings is #{mappings.length} long")
log.info("Epilogue is #{epilogue.length} long")

## Lets start by building the set of things I want to generate a graph for
for i in 1..parameters.numnetworks
    # First create our formatting str
    formattedGuardNumber = "%04d" % i
    
    applicableMatchees = Array.new
    applicableMatchers = Array.new
    # Grepo our epilogue for the iteration that we care about, and fetch the results
    mappings.each do |mappingLine|
        regExStr = "\".*\\.(.*#{formattedGuardNumber}.*)\".*\".*\\.(.*)\""
        buildingRegex   = Regexp.new(regExStr)
        mappingMatch = buildingRegex.match(mappingLine.strip)
        # If we have results, print them
        if mappingMatch != nil
            #log.info(mappingMatch[1] + "=>" + mappingMatch[2])
            applicableMatchers.push(mappingMatch[1])
            applicableMatchees.push(mappingMatch[2])
        end
    end
    applicableMatchees = applicableMatchees.uniq
    applicableMatchers = applicableMatchers.uniq
    
    # Good. Now lets build our temporary output file.
    outputFileName = GENERATED_TIME_PATH + "/" + formattedGuardNumber + ".dot"
    log.info("Creating dot file for #{formattedGuardNumber}")
    File.open(outputFileName ,'w') do |outputFile|
        
        prologue.each do |line|
            outputFile.puts line
        end
        
        definitions.each do |line|
            
            applicableMatchers.each do |matchLine|
                if line.include?(matchLine)
                    outputFile.puts line
                    break
                end
            end
            applicableMatchees.each do |matchLine|
                if line.include?(matchLine)
                    outputFile.puts line
                    break
                end
            end
        end
        
        mappings.each do |line|
            applicableMatchers.each do |matchLine|
                if line.include?(matchLine)
                    outputFile.puts line
                    break
                end
            end
        end
        
        
        epilogue.each do |line|
            outputFile.puts line
        end
    end
    # Now lets call dot to create the picture
    log.info("Creating PNG file for #{formattedGuardNumber}")
    outputPngFilePath = GENERATED_TIME_PATH + "/" + formattedGuardNumber + ".png"
    cmd = DOT_PATH + " -Tpng #{outputFileName} -o #{outputPngFilePath}"
    dotOutput = `#{cmd}`
    
    log.info(dotOutput)
    
    # Go on to the next round...
end
# And we should be done!
log.info("Exiting!")