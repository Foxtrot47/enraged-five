require 'win32ole'


class EmailItem
    attr_accessor :username
    attr_accessor :path
    attr_accessor :selectorHash
    attr_accessor :strategy
    attr_accessor :machineHash
    
    def initialize(u,p,s,st,mh)
        @username = u
        @path = p
        @selectorHash = s
        @strategy = st
        @machineHash = mh
    end
end

emailItemHash = Hash.new

myApp = WIN32OLE::new("outlook.Application")

# load Outlook OLE constants

class OutlookConst
end
WIN32OLE.const_load(myApp, OutlookConst)


ns = myApp.GetNameSpace("MAPI")
#ns.Logon # uncomment for online usage
folders = ns.Folders

new_messages = 0

folders.each { 
    | folder |
    #puts "+" + folder.Name
    begin
        folder.Folders.each {
            | folder |
            #puts " " + folder.Name
            if ["Inbox"].member? folder.Name
                folder.Folders.each {
                    |folder|
                    #puts " " + folder.Name
                    if ["Machine Hash Data"].member? folder.Name
                    
                        
                        folder.Items.each {
                            |msg|
                            body = msg['Body']
                            bodySplit = body.split("\r\n")
                            # I guess I'm sending the PATH twice?
                            if bodySplit.size == 6
                                next
                            end
                            
                            newEmailItem = EmailItem.new(bodySplit[0], bodySplit[1], bodySplit[2],bodySplit[3],bodySplit[4])
                            if emailItemHash.has_key?(bodySplit[0])
                                if emailItemHash[bodySplit[0]].strategy != bodySplit[3]
                                    puts "DISTURBANCE IN THE FORCE"
                                    puts "    #{emailItemHash[bodySplit[0]].username} (#{emailItemHash[bodySplit[0]].strategy}|#{emailItemHash[bodySplit[0]].machineHash}) : (#{bodySplit[3]})|#{bodySplit[4]})"
                                end
                            else
                            emailItemHash[bodySplit[0]] = newEmailItem
                            end
                        }
                    end
                }
            end
        }
    rescue
         puts " Unable to open"
    end
}

wmiCount = 0
winCount = 0
smbCount = 0
emailItemHash.each do |key, array|
    if array.strategy == "SMBIOS"
        smbCount +=1
    elsif array.strategy == "WIN32"
        winCount +=1
    elsif array.strategy == "WMI"
        wmiCount +=1
    end
end

puts "Reporting on #{emailItemHash.size} users:"
puts "WMI   :\t\t#{wmiCount}"
puts "WIN32 :\t\t#{winCount}"
puts "SMBIOS:\t\t#{smbCount}"
