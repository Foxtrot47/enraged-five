Protection Script
=================

You'll get a prompt telling you that you can't run the guardscript in the IDE. This is becuase the guardscript is tokenized to support the different releases. In order to run the guardscript, run the ProtectIT.rb script with appropriate arguments. Here's a quick copy/pasta for those that don't want to think too much :)

ruby ProtectIT.rb -c -v -r bankrelease