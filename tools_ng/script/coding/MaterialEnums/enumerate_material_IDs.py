from __future__ import with_statement

import sys, re

try:
    material_names = []
    with open(r'x:/gta5/build/dev/common/data/materials/materials.dat', 'r') as materialFile:
        for line in materialFile.readlines():
            if not re.match('^#', line) and not re.match('^[0-9]', line) and not line.strip()=='':
                material_names.append(re.match('(\S*)', line).group(0))

    for (i, j) in enumerate(material_names):
        print i, j

except:
    print sys.exc_info()
finally:
    print "Press enter to exit."
    raw_input()
