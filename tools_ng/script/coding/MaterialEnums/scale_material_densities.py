# Python script to scale the density value for every material in the game by the same amount.

from __future__ import with_statement

import sys, re, os
import traceback

try:
    # Prompt the user for the multiplier (plus a bit of protection from user error).
    print "Please enter the density scaling value:"
    scale_str=""
    while scale_str=="":
        input=""
        while input=="":
            input = raw_input('>')
        fr = re.findall(r'[-+]?[0-9]*\.?[0-9]+', input)
        if len(fr)>0:
            scale_str = fr[0]
    scale = float(scale_str)
    print "Scaling all density values by %f..." % scale

    # Read each line of the original materials.dat file and, if it contains a material
    # entry, replace it with the scaled value.
    materials_dat_fname = r'x:/gta5/build/dev/common/data/materials/materials.dat'
    temp_fname = r'temp_materials.dat'
    with open(materials_dat_fname, 'r') as materialFile:
        with open(temp_fname, 'w') as newMaterialFile:
            for line in materialFile.readlines():
                if not re.match('^#', line) and not re.match('^[0-9]', line) and not line.strip()=='':
                    three_way_split = re.match(r'(.*?\d+\.\d+\s+\d+\.\d+\s+)(\d+.\d+)(.*?$)', line)
                    density = float(three_way_split.group(2))
                    material_name = re.match('(\S*)', line).group(0)
                    print 'Density of {0:30s}: {1:.1f} --> {2:.1f}'.format(material_name, density, density*scale)
                    # Split out the other stuff around the density value on this line so that we can
                    # write it to our new file, tabs and all.
                    leftSideOfDensity = three_way_split.group(1) #re.match(r'.*?\d+\.\d+\s+\d+\.\d+\s+', line).group()
                    rightSideOfDensity = three_way_split.group(3)
                    newMaterialFile.write('{0:s}{1:.1f}{2:s}\n'.format(leftSideOfDensity, density*scale, rightSideOfDensity))
                else:
                    newMaterialFile.write(line)

    # If we got here without throwing an exception, we can now attempt to check out
    # materials.dat from Perforce and overwrite it with our new temporary file.
    print "Attempting to check out %s..." % materials_dat_fname
    os.system('p4 edit //depot%s'%materials_dat_fname[2:])

    # Now move our temporary file over the old materials.dat file.
    os.remove(materials_dat_fname)
    os.rename(temp_fname, materials_dat_fname)

except Exception, err:
    traceback.print_exc(file=sys.stdout)
finally:
    print "Press enter to exit."
    raw_input()
