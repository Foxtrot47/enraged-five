# regenerates all _parser.h files in a source tree

# helpful tips from http://devver.wordpress.com/2009/06/30/a-dozen-or-so-ways-to-start-sub-processes-in-ruby-part-1/
require 'rbconfig'
THIS_FILE = File.expand_path(__FILE__)

RUBY = File.join(Config::CONFIG['bindir'], Config::CONFIG['ruby_install_name']) + ".exe"

exec(RUBY, File.dirname(THIS_FILE) + "\\build_parser_files.rb", "--rebuild", *ARGV)
