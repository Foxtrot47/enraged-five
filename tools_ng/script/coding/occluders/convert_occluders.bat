@echo off

if "%1" == "" goto BadArgs
if "%2" == "" goto BadArgs

if not exist "%1" goto End

%RS_TOOLSROOT%\bin\ragebuilder_0378 %~dp0\convert_occluders.rbs -input %1 -output %2

REM Create the resulting ZIP file

del %2.zip
zip -0j %2.zip %2.imap

pushd %~dp2
p4 add %2.zip
popd

REM Local conversion
%RS_TOOLSROOT%\bin\ruby\bin\ruby.exe %RS_TOOLSROOT%\lib\util\data_convert_file.rb %2.zip


goto End

:BadArgs
echo Usage: convert_occluders [source.zip] [output without extension]

:End
