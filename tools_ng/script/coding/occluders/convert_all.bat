@echo off

set INPUT_PREFIX=x:\gta5\assets\export\levels\gta5
set OUTPUT_PREFIX=x:\gta5\assets\export\levels\gta5

set INPUT_SUFFIX=_meshes.zip
set OUTPUT_SUFFIX=_occl


set       SRC=_hills\Country_01\cs1
set SRC=%SRC% _hills\Country_02\cs2
set SRC=%SRC% _hills\Country_03\cs3
set SRC=%SRC% _hills\Country_04\cs4
set SRC=%SRC% _hills\Country_05\cs5
set SRC=%SRC% _hills\Country_06\cs6
set SRC=%SRC% _hills\cityhills_01\ch1
set SRC=%SRC% _hills\cityhills_02\ch2
set SRC=%SRC% _hills\cityhills_03\ch3
set SRC=%SRC% _citye\downtown_01\dt1
set SRC=%SRC% _citye\hollywood_01\hw1
set SRC=%SRC% _citye\indust_01\id1
set SRC=%SRC% _citye\indust_02\id2
set SRC=%SRC% _citye\port_01\po1
set SRC=%SRC% _citye\scentral_01\sc1
set SRC=%SRC% _citye\sunset\ss1
set SRC=%SRC% _cityw\airport_01\ap1
set SRC=%SRC% _cityw\beverly_01\bh1
set SRC=%SRC% _cityw\koreatown_01\kt1
set SRC=%SRC% _cityw\santamon_01\sm
set SRC=%SRC% _cityw\sanpedro_01\sp1
set SRC=%SRC% _cityw\venice_01\vb




for %%A IN (%SRC%) DO call convert_occluders %INPUT_PREFIX%\%%A%INPUT_SUFFIX% %OUTPUT_PREFIX%\%%A%OUTPUT_SUFFIX%
