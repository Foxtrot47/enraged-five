@echo off
setlocal
pushd .

if exist rage\framework\tools\install.bat (
	cd rage\framework\tools
) else if exist ..\rage\framework\tools\install.bat (
	cd ..\rage\framework\tools
) else if exist ..\..\rage\framework\tools\install.bat (
	cd ..\..\rage\framework\tools
) else if exist ..\..\..\rage\framework\tools\install.bat (
	cd ..\..\..\rage\framework\tools
) else if exist ..\..\..\..\rage\framework\tools\install.bat (
	cd ..\..\..\..\rage\framework\tools
) else if exist ..\..\..\..\..\rage\framework\tools\install.bat (
	cd ..\..\..\..\..\rage\framework\tools
) else if exist ..\..\..\..\..\..\rage\framework\tools\install.bat (
	cd ..\..\..\..\..\..\rage\framework\tools
) else (
	echo Cannot find appropriate tools directory.
	goto :EOF
)
set RS_TOOLSROOT=%CD%
cd ..\..
set RAGE_DIR=%CD%

popd
echo RS_TOOLSROOT=%RS_TOOLSROOT%
echo RAGE_DIR=%RAGE_DIR%

if not "%1"=="" (
	if exist "%1" (
		echo Loading %1 . . .
		%1
	) else if exist "%1.sln" (
		echo Loading %1.sln . . .
		%1.sln
	) else if exist "%1_2008.sln" (
		echo Loading %1_2008.sln . . .
		%1_2008.sln
	) else if exist "%1_2005.sln" (
		echo Loading %1_2005.sln . . .
		%1_2005.sln
	)
) else (
	for %%i in (*_2008.sln) do (
		echo Loading %%i . . .
		%%i
		goto :EOF
	)
)
