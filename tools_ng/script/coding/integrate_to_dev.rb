require 'pipeline/config/projects'
require 'util/perforce/p4_integrate'

config = Pipeline::Config.instance

g_Source = {}	# Source Perforce server settings
g_Target = {}	# Target Perforce server settings

#not sure where to get the depot root from at this point... will see if dave already has something setup for this
#GRS

g_Source[:port] = config.sc_server
g_Source[:client] = config.sc_workspace
g_Source[:user] = config.sc_username
g_Source[:path] = "//depot/gta5"

g_Target[:port] = config.sc_rage_server
g_Target[:client] = config.sc_rage_workspace
g_Target[:user] = config.sc_rage_username
g_Target[:path] =  "//rage/gta5/dev/rage/framework"

g_Options, g_Trailing = OS::Getopt.getopts( OPTIONS )

initialize_source_options(g_Source, g_Options)
initialize_target_options(g_Target, g_Options)

integrate_source_to_target(g_Source,g_Target,g_Trailing)