# Takes on stdin a bunch of data that looks like this, and calls canonicalize_psc on just the psc files
#//depot/gta5/src/dev/game/frontend/Credits.cpp#14 edit
#//depot/gta5/src/dev/game/frontend/Credits.psc#1 add
#//depot/gta5/src/dev/game/frontend/PauseMenu.cpp#219 edit
#//depot/gta5/src/dev/game/frontend/PauseMenu.psc#1 add
#//depot/gta5/src/dev/game/VS_Project1_lib/game1_lib_2008.vcproj#207 edit
#//depot/gta5/src/dev/game/vs_project1_lib/game1_lib_2008_unity.vcproj#180 edit

ARGF.each do |line|
	if line =~ /\.psc/ then
		filename = line.split("#")[0].strip()
		filename = filename.sub("//depot", "X:")
		filename = filename.sub("//rage/gta5/dev", "X:\\gta5\\src\\dev")
		filename = filename.gsub("/", "\\")
		puts "Processing #{filename}"
		`p4 edit #{filename}`
		puts `C:\\Python25\\python.exe X:\\gta5\\tools\\script\\coding\\canonicalize_psc.py #{filename} #{filename}`
	end
end
