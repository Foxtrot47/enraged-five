
import os
import locale
import time
from time import localtime, strftime
import datetime

# Constants 
bgCol = '#eeeeee'
headerBgCol = '#ffffff'
goodTextCol = '#000000'
badTextCol = '#d00000'
discardableTextCol= '#cc0000'
queuedTextCol= '#003399'
wtfTextCol= '#000000'

# Functions
def CN(number):
	return locale.format('%d', number, True)

def GetTime():
	return strftime('%H:%M:%S', localtime())
	
def GetDate():
	return datetime.date.fromtimestamp(time.time()).strftime('%Y/%m/%d')

# -------------------------------------------------------------------
class HTMLWrapper:
	
	def __init__(self):
		self.indent = ''
	
	def Open(self, fileName):
		self.file = open(fileName, 'w')
		self.STag('HTML')

	def Close(self):
		self.ETag('HTML')
		self.file.close()

	def Indent(self):
		self.indent = self.indent + '  '
		
	def UnIndent(self):
		self.indent = self.indent[2:]

	def Write(self, text):
		self.file.write(self.indent+text+'\n')

	def Wrap(self, tag):
		justTag = tag.split()[0]
		return '<%s>%s</%s>'%(tag, '%s', justTag)

	def WrapO(self, tag):
		return '<%s>%s'%(tag, '%s')

	def Tag(self, tag):
		self.Write('<%s>'%tag)
		
	def STag(self, tag):
		self.Write('<%s>'%tag)
		self.Indent()

	def ETag(self, tag=None):
		self.UnIndent()
		if tag:
			self.Write('</%s>'%tag)
			
	def MakeLink(self, url, content, title=None, name=None, target=None, extra=None):
		tag = 'A HREF="%s"'%url
		if title:
			tag += ' TITLE="%s"'%title
		if name:
			tag += ' NAME="%s"'%name
		if target:
			tag += ' TARGET="%s"'%target
		if extra:
			tag += extra
		return self.Wrap(tag)%content

	def Link(self, url, content, title=None, name=None, target=None, extra=None):
		self.Write(self.MakeLink(url, content, title, name, target, extra))

	def Image(self, path, width=0, height=0, alt="Picture", extra=None):
		self.Write(self.ImageTag(path, width, height, alt, extra))

	def ImageTag(self, path, width=0, height=0, alt="Picture", extra=None):
		tag = 'IMG SRC="%s" ALT="%s"'%(path, alt)
		if width!=0:
			tag += ' WIDTH="%d"'%(width)
		if height!=0:
			tag += ' HEIGHT="%d"'%(height)
		if extra:
			tag += ' '+extra
		return('<%s>'%tag)
		self.Write('<%s>'%tag)
		
	def WriteTimeStamp(self):
		self.Write(self.Wrap('SMALL')%("%s @ %s"%(GetDate(), GetTime())))

# -------------------------------------------------------------------
class ImageMap:
	
	def __init__(self):
		self.entries = []

	def AddHotspot(self, type, coords, href, title, alt):
		newEntry = {'COORDS':coords, 'HREF':href, 'TITLE':title, 'ALT':alt}
		if type == 'circle' and len(coords) == 3:
			newEntry.update({'SHAPE':'circle'})
		elif type == 'rect' and len(coords) == 4:
			newEntry.update({'SHAPE':'rect'})
		elif type == 'poly' and len(coords) >= 6:
			newEntry.update({'SHAPE':'poly'})
		else:
			return False
		
		self.entries.append(newEntry)
		return True
	
	def AddToHTML(self, hf, name):
		hf.STag('MAP NAME="%s" id="%s"'%(name, name))
		for e in self.entries:
			coordsString = ''
			for c in e['COORDS']:
				coordsString += ','+str(c)
			coordsString = coordsString[1:]

			tag = 'AREA SHAPE="%s" COORDS="%s" HREF="%s" TITLE="%s" ALT="%s"'\
				%(e['SHAPE'], coordsString, e['HREF'], e['TITLE'], e['ALT'])
			hf.Tag(tag)
		hf.ETag('MAP')
		
	def DrawOnImage(self, image, drawOptions=None):
		draw = ImageDraw.Draw(image)
		fillCol = '#008000'
		lineCol = '#000000'
		
		goodOptions = (drawOptions != None) and (len(drawOptions) == len(self.entries))
		
		doi = 0
		for e in self.entries:
			if (goodOptions):
				do = drawOptions[doi]
				fillCol = do['col']
				doi += 1
			
			if e['SHAPE'] == 'circle':
				x, y = e['COORDS'][0], e['COORDS'][1]
				r = e['COORDS'][2]
				r += 1
				coords = (x-r, y-r, x+r, y+r)
				draw.ellipse(coords, fill=fillCol, outline=lineCol)
				
			elif e['SHAPE'] == 'rect':
				x0, y0 = e['COORDS'][0], e['COORDS'][1]
				x1, y1 = e['COORDS'][2], e['COORDS'][3]
				coords = (x0, y0, x1, y1)
				draw.rectangle(coords, fill=fillCol, outline=lineCol)
				
			elif e['SHAPE'] == 'poly':
				coords = e['COORDS']
				coords += [coords[0], coords[1]]
				draw.polygon(coords, fill=fillCol, outline=lineCol)
				
		del draw
				
# -------------------------------------------------------------------
class HTMLLegend:
	
	def __init__(self):
		self.height = 15
		self.spanSteps = 20
		self.divCol = '#000000'
		self.divWidth = 2
		self.columns = []
		
	def Span(self, a, b, width):
		def ColString(col):
			return "#%02x%02x%02x"%(col[0],col[1],col[2])
		
		def LCol(t, a, b):
			return ColString(InterpolateCol(t, legend[a], legend[b]))
			
		cw = int(float(width)/self.spanSteps)
		for i in range(0, self.spanSteps+1):
			t = float(i)/self.spanSteps
			self.columns.append({ 'col':LCol(t, a, b), 'width':cw })

	def Div(self, text):
		self.columns.append({ 'col':self.divCol, 'width':self.divWidth, 'text':text })

	def Write(self, hf, caption):
		hf.STag('TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0')
		hf.Write(hf.Wrap('CAPTION')%hf.Wrap('H4')%caption)
		# Size text
		hf.Tag('TR')
		row = ''
		for column in self.columns:
			if 'text' in column:
				text = column['text']
				row += hf.WrapO('TD WIDTH="%d" STYLE="background-color: %s;"'%(self.divWidth, self.divWidth))%''
				row += hf.WrapO('TD COLSPAN="%d" '%(self.spanSteps+1))%text
		hf.Write(row)
		# Colour bars
		hf.Tag('TR')
		row = ''
		for column in self.columns:
			col = column['col']
			width = column['width']
			row += hf.WrapO('TD HEIGHT="%d" WIDTH="%d" STYLE="background-color: %s;"'%(self.height, width, col))%''
		hf.Write(row)
		
		hf.ETag('TABLE')
		hf.Tag('BR')
