'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Utility
def mysplit(s, delim=None):
    return [x for x in s.split(delim) if x]

def GetInput(pos):
	if len(sys.argv) < 2:
		print 'USAGE: python compare_orbis.py <file>'
		exit(-1)

	return sys.argv[pos]
	
def Read(path):
	file = open(path, 'r')

	lines = []
	
	for line in file.readlines():
		lines.append(line.strip())
		
	file.close();
	
	#for line in lines:
	#	print line
	
	i = 0
	total = 0
	count = 0
	print len(lines)
	
	while i < len(lines):			
		if not lines[i].startswith('- Name:'):
			i += 1
			continue
		
		name = lines[i][lines[i].find(': ') + 2:]
		
		i += 1
		start_str = lines[i]

		i += 1		
		end_str = lines[i]
		
		i += 1
		protection = lines[i]
		
		i += 1
		flags = lines[i]
		
		if 'MEM_FLAG_FLEXIBLE' in flags:
			count += 1
			
			i += 1
			type = lines[i]
			
			i += 1
			offset = lines[i]
			
			start_hex = start_str[start_str.find(': ') + 2:]
			end_hex = end_str[end_str.find(': ') + 2:]
			size = int(end_hex, 16) - int(start_hex, 16)
			total += size
			
			print name + "," + start_hex + "," + end_hex + "," + str(size)

	print ""
	print "TOTAL = ", total
	print "COUNT = ", count
	
def Main():
	path = GetInput(1);
	source = Read(path)

# Run
if __name__ == "__main__":
	Main()