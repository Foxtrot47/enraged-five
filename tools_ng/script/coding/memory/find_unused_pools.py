'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Globals
g_pools = {}

# Object definition
class CPool:
	def __init__(self, name, storage, total, peak = 0):
		self.name = name
		self.samples = 0
		self.storage = int(storage)
		self.total = int(total)		
		self.peak = int(peak)

	def __str__(self):		
		return self.name + ", " + str(self.samples) + ", " + str(self.storage) + ", " + str(self.total) + ", " + str(self.peak)

	def GetName(self):
		return self.name

	def GetSamples(self):
		return int(self.samples)

	def AddSamples(self, samples):
		self.samples += samples

	def GetStorage(self):
		return int(self.storage)

	def GetTotal(self):
		return int(self.total)

	def GetPeak(self):
		return int(self.peak)

# Utility
def MySplit(s, delim=None):
    
	items = []
	for item in s.split(delim):
		if item and ',,,' not in item and '"' not in item:
			items.append(item)
	
	return items

def GetLegend():
	if len(sys.argv) < 2:
		print 'USAGE: python find_unused_pools.py <legend> <directory>'
		exit(-1)
	
	if not os.path.isfile(sys.argv[1]):
		print sys.argv[1], "is not a file!"
		exit(-1)

	return sys.argv[1]

def GetDirectory():
	if len(sys.argv) < 3:
		print 'USAGE: python find_unused_pools.py <legend> <directory>'
		exit(-1)
	
	if not os.path.isdir(sys.argv[2]):
		print sys.argv[2], "is not a directory!"
		exit(-1)

	directory = sys.argv[2]
	if not (directory.endswith('/') or directory.endswith('\\')):
		 directory += '/'

	return directory

def NameCompare(x, y):	
	return cmp(x.name.lower(), y.name.lower())

def GetName(items, pos):
	i = 0
	name = ""
	while i < pos:
		if len(name) > 0:
			name += ' '
		name += items[i]
		i += 1
	
	return name

def ParseLegend():
	path = GetLegend()
	file = open(path, 'r')
	file.readline()

	for line in file.readlines():
		items = line.split()

		pos = len(items) - 6
		name = GetName(items, pos)
		storage = int(items[pos + 1])
		total = int(items[pos + 2])
		g_pools[name] = CPool(name, storage, total)

	file.close()

def ParseData():
	directory = GetDirectory()
	for path in os.listdir(directory):
		# Skip first line
		file = open(directory + path, 'r')
		line = file.readline()

		# Old or new verison?
		if line.startswith("Pool Name"):
			ParseNew(file)
		else:
			ParseOld(file)

		file.close()

def ParseOld(file):
	pool = None

	for line in file.readlines():
		items = MySplit(line, ',')

		name = items[1]
		pool = g_pools[name]
		pool.AddSamples(int(items[2]))

		peak = int(items[4])
		pool = g_pools[name]
		if peak > pool.GetPeak():
			pool.peak = peak

def ParseNew(file):
	start = True
	pool = None

	for line in file.readlines():
		line = line.strip()
		items = MySplit(line, ',')
		if len(items) < 2:
			continue

		if start:
			name = items[0]
			pool = g_pools[name]
			pool.AddSamples(int(items[1]))				
			start = (len(items) > 4)

			if start:
				peak = int(items[5])				
				if peak > pool.GetPeak():
					pool.peak = peak
		else:
			start = True				
			peak = int(items[3])
			if peak > pool.GetPeak():
				pool.peak = peak

# Main
def Main():
	ParseLegend()
	ParseData()

	pools = []
	for key in g_pools:
		pools.append(g_pools[key])
	pools.sort(cmp = NameCompare)

	totalUnused = 0
	totalBytes = 0
	print "Name, Samples, Storage, Total, Peak, Unused, Unused Size, 75% Reduction, 50% Reduction, 25% Reduction"
	for pool in pools:
		if pool.GetPeak() < pool.GetTotal():
			unused = pool.GetTotal() - pool.GetPeak()
			bytes = unused * pool.GetStorage()
			n75 = int((pool.GetTotal() - pool.GetPeak()) * 0.75)
			n50 = int((pool.GetTotal() - pool.GetPeak()) * 0.5)
			n25 = int((pool.GetTotal() - pool.GetPeak()) * 0.25)

			print str(pool) + ", " + str(unused) + ", " + str(bytes) + ", " + str(n75) + ", " + str(n50) + ", " + str(n25)
			totalBytes += bytes
			totalUnused += 1

	print
	print "Total Pools = ", len(pools)
	print "Total Pools with Unused Nodes = ", totalUnused
	print "Total Unused Pool Node Size = ", totalBytes, "(" + str(float(totalBytes / 1024)), "KB)"
	print "Total 75% Reduction Savings = ", int(totalBytes * 0.75), "(" + str(float(int(totalBytes * 0.75) / 1024)), "KB)"
	print "Total 50% Reduction Savings = ", int(totalBytes * 0.5), "(" + str(float(int(totalBytes * 0.5) / 1024)), "KB)"
	print "Total 25% Reduction Savings = ", int(totalBytes * 0.25), "(" + str(float(int(totalBytes * 0.25) / 1024)), "KB)"

# Run
if __name__ == "__main__":
	Main()