
import os
import sys
from html import *

class ObjectEntry:
	def __init__(self, img, name, state, physical, virtual):
		self.img = img.lower()
		self.name = name
		self.state = state.lower()
		self.physical = int(physical)
		self.virtual = int(virtual)
		self.total = int(physical) + int(virtual)

	def __str__(self):
		return str(self.img) + ', ' + str(self.name) + ', ' + str(self.state) + ', ' + str(self.physical) + ', ' + str(self.virtual)

def get_param(pos):
	if len(sys.argv) < (pos + 1):
		print 'USAGE: python ResourceVisualize.py <filename>'
		exit(-1)
	
	return sys.argv[pos]

def Header(html, input_path):
	title = input_path
	html.Write(html.Wrap('HEAD')%html.Wrap('TITLE')%title)
	html.STag('BODY BGCOLOR="%s"'%bgCol)
	html.Write(html.Wrap('H3')%title)
	html.WriteTimeStamp()

def Body(html, objects):
	if objects == []:
		return

	hc = (' STYLE="background-color: %s;"'%headerBgCol)
	#html.Write(html.Wrap('H3 ALIGN=LEFT')%)
	html.Tag('HR')

	# Create images set
	images = {}
	for entry in objects:
		list = images.get(entry.img)
		if list == None:
			list = []

		list.append(entry)
		images[entry.img] = list

	sorted_images = sorted(images)
	imgIndex = 0	

	for key1 in sorted_images:
		value1 = images[key1]
		 
		html.STag('TABLE BORDER=1 CELLSPACING=0 CELLPADDING=1 WIDTH=800')
		linkAnchorTemplate = html.Wrap('A NAME="img_%d"'%imgIndex)
		html.Write(html.Wrap('CAPTION')%html.Wrap('H4')%(linkAnchorTemplate%key1))

		start = key1.rfind('/') + 1
		end = key1.rfind('.')
		if end == -1:
			end = len(key1)

		# Types
		types = {}
		for entry in value1:
			pos = entry.name.rfind('.')
			name = entry.name[0:pos]
			type = entry.name[pos + 1:len(entry.name)]

			list = types.get(type)
			if list == None:
				list = []

			list.append(entry)
			types[type] = list

		sorted_types = sorted(types)
		virtualTotal = 0
		physicalTotal = 0

		for type in sorted_types:

			#dict = key1[start:end]
			objectsHeaderTemplate = html.WrapO('TR')%(html.WrapO('TH WIDTH=500'+hc) + html.WrapO('TH WIDTH=150 ALIGN=RIGHT'+hc) + html.WrapO('TH WIDTH=150 ALIGN=RIGHT'+hc) + html.WrapO('TH WIDTH=150 ALIGN=RIGHT'+hc))
			linkAnchorTemplate = html.Wrap('A NAME="img_%d_%s"'%(imgIndex, type))
			html.Write(objectsHeaderTemplate%(type, 'Virtual KB', 'Physical KB', 'Total KB'))

			list = types[type]

			for entry in list:
				# Colors
				memTemplate = html.WrapO('TD ALIGN=RIGHT')%html.Wrap('TT')
				entryTemplate = html.WrapO('TD WIDTH=800')

				if 'queued' in entry.state:
					memTemplate = memTemplate%html.Wrap('B')%html.Wrap('FONT COLOR="%s"'%queuedTextCol)
					entryTemplate = entryTemplate%html.Wrap('B')%html.Wrap('FONT COLOR="%s"'%queuedTextCol)
				elif 'discardable' in entry.state:
					memTemplate = memTemplate%html.Wrap('B')%html.Wrap('FONT COLOR="%s"'%discardableTextCol)
					entryTemplate = entryTemplate%html.Wrap('B')%html.Wrap('FONT COLOR="%s"'%discardableTextCol)

				pos = entry.name.rfind('.')
				name = entry.name[0:pos].lower()
				rowTemplate = html.WrapO('TR')%(entryTemplate)
				html.Write(rowTemplate%(name))

				html.Write(memTemplate%CN(entry.virtual / 1024))
				html.Write(memTemplate%CN(entry.physical / 1024))                                     
				html.Write(memTemplate%CN(entry.total / 1024))
				
				# Total
				virtualTotal += entry.virtual
				physicalTotal += entry.physical			

		row = html.WrapO('TD')%html.Wrap('B')%('Total')		
		row += html.WrapO('TD ALIGN=RIGHT')%html.Wrap('B')%html.Wrap('TT')%("%s"%CN(virtualTotal / 1024))
		row += html.WrapO('TD ALIGN=RIGHT')%html.Wrap('B')%html.Wrap('TT')%("%s"%CN(physicalTotal / 1024))
		row += html.WrapO('TD ALIGN=RIGHT')%html.Wrap('B')%html.Wrap('TT')%("%s"%CN((virtualTotal + physicalTotal) / 1024))
		html.Write(html.WrapO('TR')%row)

		html.ETag('TABLE')
		html.Tag('BR')
		imgIndex += 1

def Footer(html):
	html.Write(html.Wrap('DIV STYLE="height: 1200px;"')%'')
	html.Write('Thank you Mario! But our princess is in another castle!')
	html.ETag('BODY')
	html.Close()

def Main():	
	input_path = get_param(1);	
	file = open(input_path, 'r')
	objects = []

	for line in file.readlines():
		
		# Ignore comments
		if line.startswith("#"):
			continue;

		# Strip whitespace
		line = line.strip()

		# Parse
		data = line.split(',');
		if len(data) < 5:
			continue

		entry = ObjectEntry(data[0], data[1], data[2], data[3], data[4])
		objects.append(entry)

	file.close()

	# Header
	html = HTMLWrapper()
	output_path = input_path[0:input_path.rfind('.')] + '.html'	
	html.Open(output_path)

	Header(html, input_path)

	# Body	
	Body(html, objects)

	# Footer
	Footer(html);

if __name__ == "__main__":
	Main()
	
