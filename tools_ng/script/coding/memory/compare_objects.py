'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Globals
g_legend = {}

# Classes
class Allocation:
	def __init__(self):		
		self.Clear()

	def __str__(self):		
		return str(self.id) + "," + self.name + "," + self.ext + "," + str(self.vsize) + "," + str(self.psize)

	def __eq__(self, other):
		return (other != None) and (self.id == other.id)

	def  __hash__(self):
		return self.name.__hash__()

	def Empty(self):
		return self.vsize == 0 and self.psize == 0

	def Clear(self):
		self.id = 0
		self.vsize = 0
		self.psize = 0
		self.name = ""
		self.ext = ""

	def Parse(self, data):
		self.Clear()
		items = mysplit(data, ' \t')

		self.id = int(items[1][1:len(items[1])-1])
		self.name = items[2].strip()
		self.vsize = int(items[3][:len(items[3])-1])
		self.psize = int(items[4][:len(items[4])-1])

		pos = self.name.find('.')
		self.ext = self.name[pos+1:len(self.name)].strip()

def RemoveParam(value):
	return value[1:len(value)-1]

# Utility
def mysplit(string, delimiters, maxsplit=0):
	regexPattern = '|'.join(map(re.escape, delimiters))
	return [x for x in re.split(regexPattern, string, maxsplit) if x]

def GetInput(pos):
	if len(sys.argv) < 3:
		print 'USAGE: python compare_objects.py <source> <target>'
		exit(-1)

	return sys.argv[pos]

def GetSource():
	return GetInput(1)

def GetTarget():
	return GetInput(2)

def ReadLegend():
	if not os.path.isfile('legend.csv'):
		return

	file = open('legend.csv', 'r')
	legend = {}

	for line in file.readlines():
		items = mysplit(line, ',')
		legend[items[0].strip()] = items[1].strip()

	file.close();

	return legend

def Read(path):
	file = open(path, 'r')
	file.readline()

	allocs = []
	for line in file.readlines():
		if line[0] == '#':
			continue

		alloc = Allocation()
		alloc.Parse(line)

		if not alloc.Empty():
			allocs.append(alloc)

	file.close();

	return allocs

def SortByExtension(x, y):
	return cmp(x.ext, y.ext)

def CreateExtMap(allocs):
	map = {}

	for alloc in allocs:		
		key = alloc.ext
		if map.get(key) == None:
			map[key] = []

		list = map[key]
		list.append(alloc)

	return map

def SortByExtension(x, y):
	return cmp(x.ext, y.ext)

def CreateTallyMap(source):
	allocs = []

	for key,data in source.items():
		vsize = 0
		psize = 0		
		for alloc in data:
			vsize += alloc.vsize
			psize += alloc.psize
		
		alloc = Allocation()
		alloc.ext = key
		alloc.vsize = vsize
		alloc.psize = psize
		allocs.append(alloc)

	return sorted(allocs, SortByExtension)

def GetDetail(ext):
	if g_legend.get(ext):
		return g_legend[ext]

	return "UNKNOWN"

def Print(name, allocs):
	print
	print name
	print "{0:<8}".format("Type"), "{0:>24}".format("Detail"), "{0:>8}".format("VSize"), "{0:>8}".format("PSize")
	print "-------------------------------------------------------"
	
	for alloc in allocs:
		print "{0:<8}".format(alloc.ext), "{0:>24}".format(GetDetail(alloc.ext)), "{0:>8}".format(alloc.vsize), "{0:>8}".format(alloc.psize), "{0:>3}".format("KB")

def PrintTotals(allocs):	
	vsize = 0
	psize = 0

	for alloc in allocs:
		vsize += alloc.vsize
		psize += alloc.psize
	
	print "-------------------------------------------------------"	
	print "{0:<8}".format("Total"), "{0:>8}".format(vsize), "{0:>8}".format(psize), "{0:>3}".format("KB")

def PrintDeltas(target_tally, target_map):
	print
	print "[TARGET_VSIZE_INCREASE_DETAIL]"

	for tally in target_tally:
		if tally.vsize > 0:
			ext = tally.ext
			allocs = target_map.get(ext)
			
			print "{0:<72}".format(ext + " (" + GetDetail(ext) + ")"), "{0:>8}".format("VSize")
			print "-------------------------------------------------------------------------------------"

			for alloc in allocs:
				if (alloc.vsize > 0):
					print "{0:<72}".format(alloc.name), "{0:>8}".format(alloc.vsize), "{0:>3}".format("KB")
			print

	print
	print "[TARGET_PSIZE_INCREASE_DETAIL]"

	for tally in target_tally:
		if tally.psize > 0:
			ext = tally.ext
			allocs = target_map.get(ext)
			
			print "{0:<72}".format(ext + " (" + GetDetail(ext) + ")"), "{0:>8}".format("PSize")
			print "-------------------------------------------------------------------------------------"

			for alloc in allocs:
				if (alloc.psize > 0):
					print "{0:<72}".format(alloc.name), "{0:>8}".format(alloc.psize), "{0:>3}".format("KB")
			print

def IsExtInList(ext, list):
	for item in list:
		if ext == item.ext:
			return True;

	return False

def GetAlloc(ext, list):
	for item in list:
		if ext == item.ext:
			return item;

	return None

def Delta(source_allocs, target_allocs):
	delta = []
	for target_alloc in target_allocs:		
		ext = target_alloc.ext
		if not IsExtInList(ext, source_allocs):
			delta.append(target_alloc)
			continue

		source_alloc = GetAlloc(ext, source_allocs)

		alloc = Allocation()		
		alloc.vsize = target_alloc.vsize - source_alloc.vsize
		alloc.psize = target_alloc.psize - source_alloc.psize
		alloc.ext = ext

		delta.append(alloc)

	return delta

def Main():
	global g_legend
	g_legend = ReadLegend()

	path = GetInput(1);
	source = Read(path)
	source_map = CreateExtMap(source)
	source_tally = CreateTallyMap(source_map)
	Print("SOURCE: " + path, source_tally)

	path = GetInput(2);
	target = Read(path)
	target_map = CreateExtMap(target)
	target_tally = CreateTallyMap(target_map)
	Print("TARGET: " + path, target_tally)

	delta_tally = Delta(source_tally, target_tally)
	Print("TARGET DELTAS: ", delta_tally)
	PrintTotals(delta_tally)

	PrintDeltas(delta_tally, target_map)

# Run
if __name__ == "__main__":
	Main()
