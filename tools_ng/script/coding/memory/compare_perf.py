'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Classes
class Perf:
	def __init__(self):		
		self.Clear()

	def __str__(self):		
		return "{0}".format(self.name) + ", " + str(self.frame) + ", " + str(self.update)

	def Clear(self):
		self.name = 0
		self.frame = 0
		self.update = 0

	def IsValid(self):
		return self.frame > 0 and self.update > 0

def GetInput(pos):
	if len(sys.argv) < 3:
		print 'USAGE: python comapre_perf.py <source_folder> <target_folder>'
		exit(-1)

	return sys.argv[pos]

def GetSource():
	return GetInput(1)

def GetTarget():
	return GetInput(2)

def Read(folderPath):
	filePaths = [ f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f)) and f.endswith(".xml") ]
	data = []

	for filePath in filePaths:
		perf = Perf();
		perf.name = filePath
		file = open(folderPath + "\\" + filePath, 'r')				
		
		while 1:
			line = file.readline()
			if not line:
				break
			
			if "msPerFrameResult" in line:
				file.readline()
				file.readline()
				line = file.readline()
				value = line[line.find("\"") + 1 : line.rfind("\"")]
				perf.frame = float(value)

			if "Update Thread" in line:
				file.readline()
				file.readline()
				line = file.readline()
				value = line[line.find("\"") + 1 : line.rfind("\"")]
				perf.update = float(value)

			if perf.IsValid():
				data.append(perf)
				break

		file.close();
	
	return data

def Compare(source, target):
	print

def Main():
	# SOURCE
	sourcePath = GetInput(1);
	print "SOURCE: " + sourcePath

	sourceData = Read(sourcePath)
	for data in sourceData:
		print data

	# TARGET
	targetPath = GetInput(2);
	print "\nTARGET: " + targetPath

	targetData = Read(targetPath)
	for data in targetData:
		print data

	# DELTA
	print "\nDELTA"
	i = 0
	while i < len(sourceData):
		name = frame = sourceData[i].name		
		frame = targetData[i].frame - sourceData[i].frame
		update = targetData[i].update - sourceData[i].update
		print('%(name)s, %(frame)+f, %(update)+f' % {'name' : name, 'frame': frame, 'update' : update})
		i += 1

# Run
if __name__ == "__main__":
	Main()
