'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Classes
class Allocation:
	def __init__(self):		
		self.Clear()

	def __str__(self):		
		return "{0}".format(self.size) + " Bytes" + "\r\n" + self.callstack

	#def __eq__(self, other):
	#	return (self.size == other.size) and (self.callstack == other.callstack)

	def  __hash__(self):
		#value = str(self.id) + str(self.address)
		return self.callstack.__hash__()

	def Clear(self):
		self.id = 0
		self.bucket = 0
		self.address = 0
		self.size = 0
		self.callstack = ""
		self.functions = []

	def Parse(self, data):
		self.Clear()
		items = mysplit(data, ',')

		self.id = int(items[0])
		self.bucket = items[2]
		self.frame = int(items[3])
		self.address = int(items[4])
		self.size = int(items[5])
		
		i = 6
		str = ""
		while i < len(items):
			str += items[i]
			i += 1

		str = str.strip()
		if str.startswith("\"") and str.endswith("\""):
			str = str[1:len(str) - 1]
		elif str.endswith("'") and str.endswith("'"):
			str = str[1:len(str) - 1]

		# Callstack		
		items = str.split("|")
		for item in items:
			if len(item) > 0:
				self.callstack += item + "\r\n"
				self.functions.append(item)

# Utility
def mysplit(s, delim=None):
    return [x for x in s.split(delim) if x]

def GetInput(pos):
	if len(sys.argv) < 3:
		print 'USAGE: python comapre_memvis.py <source> <target>'
		exit(-1)

	return sys.argv[pos]

def GetSource():
	return GetInput(1)

def GetTarget():
	return GetInput(2)

def Read(path):
	file = open(path, 'r')
	file.readline()

	allocs = {}
	for line in file.readlines():
		if line[0] == '#':
			continue

		alloc = Allocation()
		alloc.Parse(line)
		if allocs.get(alloc.callstack) == None:
			allocs[alloc.callstack] = alloc
		else:
			allocs[alloc.callstack].size += alloc.size

	file.close();

	return allocs

def SortByCallstack(x, y):
	return cmp(x.callstack, y.callstack)

def Compare(source, target):
	# Added
	added = []
	increase = []
	decrease = []

	for key,data in source.items():
		if target.get(key) == None:
			added.append(data)
			continue

		temp = target[key]
		if data.size != temp.size:
			alloc = Allocation()
			alloc.callstack = data.callstack
			
			if temp.size > data.size:
				alloc.size = temp.size - data.size
				increase.append(alloc)
			else:
				alloc.size = data.size - temp.size
				decrease.append(alloc)

	if len(added) > 0:
		print "UNIQUE SOURCE"
		print "----------------------------------------"
		for item in added:
			print item

	# Removed
	removed = []
	for key,data in target.items():
		if source.get(key) == None:
			removed.append(data)
			continue

	if len(removed) > 0:
		print "UNIQUE TARGET"
		print "----------------------------------------"
		for item in removed:
			print item

	if len(increase) > 0:
		print "INCREASED TARGET"
		print "----------------------------------------"
		for item in increase:
			print item

	if len(decrease) > 0:
		print "DECREASED TARGET"
		print "----------------------------------------"
		for item in decrease:
			print item

def Main():
	path = GetInput(1);
	source = Read(path)

	path = GetInput(2);
	target = Read(path)

	Compare(source, target)

# Run
if __name__ == "__main__":
	Main()
