'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'	TODO
'	Eric J Anderson
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import os
import os.path
import sys
import re

# Globals
g_pools = []
g_objects = []

# Object definition
class CObject:
	def __init__(self, name, size, padding):
		self.name = name
		self.size = int(size)
		self.padding = int(padding)

	def __str__(self):		
		return self.name + ',' + str(self.size) + ',' + str(self.padding)

	def GetName(self):
		return self.name

	def GetSize(self):
		return self.size

	def GetPadding(self):
		return self.padding

class CPool:
	def __init__(self, name, storage, size):
		self.name = name
		self.storage = int(storage)
		self.size = int(size)

	def __str__(self):		
		return self.name + ',' + str(self.storage) + ',' + str(self.size)

	def GetName(self):
		return self.name

	def GetStorage(self):
		return self.storage

	def GetSize(self):
		return self.size

class CWastedMemory:
	def __init__(self, pool, object):
		self.pool = pool
		self.object = object

	def __str__(self):		
		return self.GetName() + ',' + str(self.object.GetPadding()) + ',' + str(self.pool.GetSize()) + ',' + str(self.GetSize())

	def GetName(self):
		return self.pool.GetName()

	def GetPool(self):
		return self.pool

	def GetObject(self):
		return self.object

	def GetSize(self):
		return self.object.GetPadding() * self.pool.GetSize()

def GetParam(pos):
	if len(sys.argv) < (pos + 1):
		print 'USAGE: python find_pools.py <pool input> <class input>'
		exit(-1)
	
	return sys.argv[pos]

def NameCompare(x, y):
	return cmp(x.GetName(), y.GetName())

def GetObject(pool):
	for object in g_objects:
		if pool.GetName() == object.GetName():
			return object
	return None

def GetWastedMemory():
	results = []
	for pool in g_pools:
		object = GetObject(pool)
		if object != None:
			entry = CWastedMemory(pool, object)
			results.append(entry)
	return results

def Main():
	# Pools
	path = GetParam(1)
	file = open(path, 'r')
	global g_pools

	for line in file.readlines():
		line = line.strip()
		if line.startswith('#'):
			continue

		data = line.split(',')
		entry = CPool(data[0], data[2], data[3])		
		g_pools.append(entry)	

	file.close()
	g_pools.sort(cmp = NameCompare)

	# Classes
	path = GetParam(2)
	file = open(path, 'r')
	global g_objects

	for line in file.readlines():
		line = line.strip()
		if line.startswith('#'):
			continue

		data = line.split(',')
		data[0] = data[0][6:]
		entry = CObject(data[0], data[1], data[2])		
		g_objects.append(entry)

	file.close()
	g_objects.sort(cmp = NameCompare)

	# Search
	file = open("pools.csv", 'w')
	file.write("Name,Padding,Pool_Size,Bytes_Wasted\n")
	results = GetWastedMemory()
	for item in results:
		print str(item)
		file.write(str(item) + '\n')
	file.close()

# Run
if __name__ == "__main__":
	Main()