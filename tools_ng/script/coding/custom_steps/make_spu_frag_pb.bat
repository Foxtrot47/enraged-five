@ECHO OFF

IF "%1"=="" (
	echo usage: %0 jobdir intdir fragname configname
	echo First parameter names the output .frag.o file and the symbols.
	echo Second parameter is the intermediate file directory.
	echo Third parameter is the frag name [sans .frag extension]
	echo Fourth parameter is the quoted configuration name
	echo Fifth parameter is any additional compile flags [include paths]
	exit/b 0
)

SETLOCAL
ECHO make_spu_frag_pb: %1 %2 %3 %4 %~5

set DIR=%2
IF %4=="SN PS3 Debug" set DIR=psn_debug
IF %4=="SN PS3 SNC Debug" set DIR=psn_debug
IF %4=="GTA4 PS3 Debug" set DIR=psn_debug
IF %4=="SN PS3 Beta" set DIR=psn_beta
IF %4=="SN PS3 SNC Beta" set DIR=psn_beta
IF %4=="GTA4 PS3 Beta" set DIR=psn_beta
IF %4=="SN PS3 PreRelease" set DIR=psn_prerelease
IF %4=="SN PS3 SNC PreRelease" set DIR=psn_prerelease
IF %4=="GTA4 PS3 PreRelease" set DIR=psn_prerelease
IF %4=="SN PS3 Profile" set DIR=psn_profile
IF %4=="SN PS3 SNC Profile" set DIR=psn_profile
IF %4=="GTA4 PS3 Profile" set DIR=psn_profile
IF %4=="SN PS3 Release" set DIR=psn_release
IF %4=="SN PS3 SNC Release" set DIR=psn_release
IF %4=="GTA4 PS3 Release" set DIR=psn_release
IF %4=="SN PS3 BankRelease" set DIR=psn_bankrelease
IF %4=="SN PS3 SNC BankRelease" set DIR=psn_bankrelease
IF %4=="SN PS3 Final" set DIR=psn_final
IF %4=="SN PS3 SNC Final" set DIR=psn_final
IF %4=="GTA4 PS3 Final" set DIR=psn_final
IF %4=="SN PS3 Final" set DIR=psn_final

REM DW - handle VS2010 build config names
IF %4=="Debug" set DIR=psn_debug
IF %4=="Beta" set DIR=psn_beta
IF %4=="PreRelease" set DIR=psn_prerelease
IF %4=="Profile" set DIR=psn_profile
IF %4=="Release" set DIR=psn_release
IF %4=="BankRelease" set DIR=psn_bankrelease
IF %4=="Final" set DIR=psn_final


PATH %SCE_PS3_ROOT:/=\%\host-win32\ppu\bin;%SCE_PS3_ROOT:/=\%\host-win32\spu\bin;%PATH%

IF "%SN_COMMON_PATH%"=="" (
	echo SN_COMMON_PATH not set, using default.
	set SN_COMMON_PATH=C:\Program Files\SN Systems\Common
)

set GCC2VS=gcc2vs.exe
REM Compile and link the elf.
SET FLAGS=-O2 -g -Wl,-q -ffunction-sections -fdata-sections -Wl,--gc-sections
IF %DIR%==psn_debug       SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_beta        SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_bankrelease SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_prerelease  SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_release     SET FLAGS=-fstack-check %FLAGS%

SET SRCDIR=%1
SET SRCDIR=%SRCDIR:\=/%

SET WARN=-Werror

pushd %1

REM echo cd is %CD%, RAGE_DIR is %RAGE_DIR%
cd /D %RAGE_DIR%
set ABS_RAGE_DIR=%CD%

popd

pushd %1

REM       Call the task/job.bat file to override settings.
IF exist %3.bat CALL %3.bat
IF exist spu_compile_flags.bat echo Warning: spu_compile_flags.bat is no longer used.  Use %3.bat instead.

popd

SET DFILE=%CD%\%2\%3.frag.d
REM ECHO Writing dependency info for %1%3.frag to %DFILE% (RAGE_DIR=%ABS_RAGE_DIR%)
REM only reason -mspurs-job is defined below is so that __SPURS_JOB__ is defined consistently
%GCC2VS% spu-lv2-gcc -MM %CPPFLAGS% %~5 -D__SPUFRAG=1 -include %ABS_RAGE_DIR:\=/%/base/src/forceinclude/spu_%DIR%.h %FORCE_INC% %INC_DIR% -fpic -mspurs-job -I %ABS_RAGE_DIR:\=/%/base/src -I %ABS_RAGE_DIR:\=/%/suite/src -I %ABS_RAGE_DIR:\=/%/framework/src -x c++ %1%3.frag > %DFILE%

pushd %1

ECHO Configuration is '%2' (%DIR%) compiling with '%FLAGS%', dependencies in %DFILE%

REM Set JOBTYPE to -mspurs-job or -mspurs-job-initialize in spu_compile_flags.bat.  The latter allows global ctors and
REM C++ virtuals to work correctly, at the cost of 1.5k of startup code and a blocking DMA at job startup.
REM (Note that none of this applied to code fragments!)
REM IF "%JOBTYPE%"=="" SET JOBTYPE=-mspurs-job -- leave it off by default, causes spurious warning under new tool chains

REM The following line is one of the differences between make_spurs_job_pb.bat and this file.
REM -nostartfiles		: Do not use the standard system startup files when linking
REM -e SpuShaderMain	: Defines out entry point symbol, SpuShaderMain
REM -Ttext=0x0			: Bring the .text section as close as possible to the beginning of the executable. There are two sections that
REM							must come before it though: .SpuGUID, followed by .before_text. We use the macro SPUFRAG_DECL(...) to name
REM							the entry point SpuShaderMain() and additionally give it an __attribute__((section(".before_text"))), so
REM							that it always comes right after .SpuGUID.
ECHO ON
%GCC2VS% spu-lv2-gcc %FLAGS% -D__SPUFRAG=1 -DDEBUG_SPU_JOB_NAME=\"%3\" -fno-strict-aliasing -Wall -Werror -Wundef -Wno-reorder %WARN% -o %3_%DIR%.frag.elf -include %RAGE_DIR:\=/%\base\src\forceinclude\spu_%DIR%.h %FORCE_INC% %INC_DIR% -fpic %JOBTYPE% %~5 %OBJ_FILES% -I %RAGE_DIR:\=/%/base/src -I %RAGE_DIR:\=/%/suite/src -I %RAGE_DIR:\=/%/framework/src -x c++ %SRCDIR%%3.frag %LIBS% -L %RAGE_DIR:\=/%/lib -ldma -lgcm_spu -latomic -nostartfiles -e SpuShaderMain -Ttext=0x0 2> %3_%DIR%.log
@ECHO OFF
if errorlevel 1 (
	%RAGE_DIR%\qa\qa-sed s,/,\\,g %3_%DIR%.log
	echo %1%3.frag: error: warning or error detected during compile.
	exit/b 1
)
%RAGE_DIR%\qa\qa-sed s,/,\\,g %3_%DIR%.log

REM This is not a problem with Job 2.0 - Hurrah!
REM Verify that the main symbol really is at offset zero as required.
REM ECHO -Offset- --Size--   ----Name----
REM spu-lv2-nm --demangle -S --size-sort %3_%DIR%.job.elf
REM spu-lv2-nm %3_%DIR%.job.elf | %RAGE_DIR%\qa\qa-egrep.exe "000000[01]0 T cellSpursJobMain2" > NUL
REM REM if errorlevel 1 (
REM 	echo %3: cellSpursJobMain2 not found, or not at address zero
REM 	exit/b 1
REM )

REM ECHO ON
REM spu-lv2-objdump -dCr %3_%DIR%.frag.elf > %3_%DIR%.frag.txt
REM @ECHO OFF

popd

REM Convert to a binary file, preserving bss. Expected symbol is _binary_patcherspu_frag_bin_start.
REM The "--format-binary" option is one of the differences between make_spurs_job_pb.bat and this file. It removes the .ELF header when
REM converting the SPU .elf to a PPU obj. This is b/c we don't need that header if we're never going to load the .elf in the traditional
REM manner.
if %DIR%==psn_final spu-lv2-strip %1\%3_%DIR%.frag.elf
spu_elf-to-ppu_obj --section-align=4 --format=binary --symbol-name=spu_%3_frag_bin %1\%3_%DIR%.frag.elf %2/%3.frag.obj
if errorlevel 1 exit/b 1

REM Put the original elf file somewhere easy to find in the debugger.
if not exist c:\spu_debug mkdir c:\spu_debug
move %1\%3_%DIR%.frag.elf c:\spu_debug

REM (Note that the below may not work, because of the "--format=binary" a few lines above)
cd %2
REM echo Created embedded frag with symbols:
REM ppu-lv2-nm %3.frag.obj
