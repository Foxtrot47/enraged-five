setlocal enabledelayedexpansion

REM hacks for 2010 compatibility (it adds a \ to output dir)
if %1==psn_debug\ %0 psn_debug %2
if %1==psn_beta\ %0 psn_beta %2
if %1==psn_bankrelease\ %0 psn_bankrelease %2
if %1==psn_release\ %0 psn_release %2
if %1==psn_profile\ %0 psn_profile %2
if %1==psn_final\ %0 psn_final %2
if %1==psn_mastereuropean\ %0 psn_mastereuropean %2
if %1==psn_masteramerican\ %0 psn_masteramerican %2
if %1==psn_masterjapanese\ %0 psn_masterjapanese %2

if not exist c:\spu_debug\* mkdir c:\spu_debug
echo Processing SPURS %2s for config %1 . . .

for %%f in (%1\*.elf) do (
	REM need to rename %1\Whatever_%2.elf to Whatever_%1.%2.elf
	set N=%%~nf
	set D=c:\spu_debug\!N:_%2=!_%1.%2
	copy %%f !D!.elf > nul
	REM echo %%f . . . !D!.elf
	%SCE_PS3_ROOT%\host-win32\spu\bin\spu-lv2-objcopy -j.SpuGUID -O binary !D!.elf !D!.guid
	%SCE_PS3_ROOT%\host-win32\sn\bin\ps3bin -dsy -c !D!.elf > !D!.sym
	%RS_TOOLSROOT%\bin\coding\compressmap !D!.sym
)

for %%f in (%1\*.bin) do (
	copy %%f c:\spu_debug > nul
)

