platform = ARGV[0]
platform = platform.chop() if platform.end_with?('/', '\\')

buildType = ARGV[1]

Dir.mkdir("C:\\spu_debug") if not File.directory?("C:\\spu_debug")


puts "checking " + platform + "/*.elf"
Dir.glob("#{platform.gsub('\\', '/')}/*.elf").each do |sourceFile|
	newFilename = File.basename(sourceFile.gsub("\\", "/"), ".elf")
	# rename %1\Whatever_%2.elf to Whatever_%1.%2.elf
	newFilename = newFilename.gsub(/_#{buildType}/,  "")
	newFilename = newFilename + "_#{platform}.#{buildType}"
	dest = "C:\\spu_debug\\#{newFilename}"
	
	if File.exist?("#{dest}.elf") and File.mtime(sourceFile) <= File.mtime("#{dest}.elf") then
		next
	end
	puts "Updating #{sourceFile}"
	
	system("copy #{sourceFile.gsub('/', '\\')} #{dest.gsub('/', '\\')}.elf > nul")
	system("%SCE_PS3_ROOT%\\host-win32\\spu\\bin\\spu-lv2-objcopy -j.SpuGUID -O binary #{dest}.elf #{dest}.guid")
	system("%SCE_PS3_ROOT%\\host-win32\\sn\\bin\\ps3bin -dsy -c #{dest}.elf > #{dest}.sym")
	system("%RS_TOOLSROOT%\bin\coding\compressmap #{dest}.sym")
end

