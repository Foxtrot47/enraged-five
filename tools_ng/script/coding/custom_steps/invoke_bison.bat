@echo off
if not exist \usr\local\bin\bison.exe echo Missing bison install.  See scriptcompiler directory.
echo Translating %1.y to %1.tab.cpp . . .
if not exist \tmp mkdir \tmp
echo /usr/local/bin/bison.exe -d -p %1_ -dv %1.y
/usr/local/bin/bison.exe -d -p %1_ -dv %1.y
echo move %1.tab.c %1.tab.cpp
move %1.tab.c %1.tab.cpp
