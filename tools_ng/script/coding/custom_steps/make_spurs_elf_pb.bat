@ECHO OFF

IF "%1"=="" (
	echo usage: %0 %5dir intdir %5name configname
	echo First parameter names the input directory.
	echo Second parameter is the intermediate file directory.
	echo Third parameter is the %5 name [sans .%5 extension]
	echo Fourth parameter is the quoted configuration name
	echo Fifth parameter is the type of elf being created task/job.
	echo Sixth parameter is either mspurs-job or mspurs-task
	echo Seventh parameter is either job_bin or task_elf, the symbol suffix to use
	echo Eighth parameter is any additional compile flags [include paths]
	exit/b 0
)

SETLOCAL
SETLOCAL ENABLEDELAYEDEXPANSION
REM ECHO make_spurs_%5_pb: %1 %2 %3 %4 %5 %~6

SET DIR=%2

REM === START VS2010 ===
IF %4=="SN SNC Debug" set DIR=psn_debug
IF %4=="SN SNC Beta" set DIR=psn_beta
IF %4=="SN SNC PreRelease" set DIR=psn_prerelease
IF %4=="SN SNC Profile" set DIR=psn_profile
IF %4=="SN SNC Release" set DIR=psn_release
IF %4=="SN SNC BankRelease" set DIR=psn_bankrelease
IF %4=="SN SNC Final" set DIR=psn_final

IF %4=="Debug" set DIR=psn_debug
IF %4=="Beta" set DIR=psn_beta
IF %4=="PreRelease" set DIR=psn_prerelease
IF %4=="Profile" set DIR=psn_profile
IF %4=="Release" set DIR=psn_release
IF %4=="BankRelease" set DIR=psn_bankrelease
IF %4=="Final" set DIR=psn_final

REM === END VS2010 ===

IF %4=="SN PS3 Debug" set DIR=psn_debug
IF %4=="SN PS3 SNC Debug" set DIR=psn_debug
IF %4=="GTA4 PS3 Debug" set DIR=psn_debug
IF %4=="SN PS3 Beta" set DIR=psn_beta
IF %4=="SN PS3 SNC Beta" set DIR=psn_beta
IF %4=="GTA4 PS3 Beta" set DIR=psn_beta
IF %4=="SN PS3 PreRelease" set DIR=psn_prerelease
IF %4=="SN PS3 SNC PreRelease" set DIR=psn_prerelease
IF %4=="GTA4 PS3 PreRelease" set DIR=psn_prerelease
IF %4=="SN PS3 Profile" set DIR=psn_profile
IF %4=="SN PS3 SNC Profile" set DIR=psn_profile
IF %4=="GTA4 PS3 Profile" set DIR=psn_profile
IF %4=="SN PS3 Release" set DIR=psn_release
IF %4=="SN PS3 SNC Release" set DIR=psn_release
IF %4=="GTA4 PS3 Release" set DIR=psn_release
IF %4=="SN PS3 BankRelease" set DIR=psn_bankrelease
IF %4=="SN PS3 SNC BankRelease" set DIR=psn_bankrelease
IF %4=="GTA4 PS3 BankRelease" set DIR=psn_bankrelease
IF %4=="SN PS3 Final" set DIR=psn_final
IF %4=="SN PS3 SNC Final" set DIR=psn_final
IF %4=="GTA4 PS3 Final" set DIR=psn_final

PATH %SCE_PS3_ROOT:/=\%\host-win32\ppu\bin;%SCE_PS3_ROOT:/=\%\host-win32\spu\bin;%SCE_PS3_ROOT:/=\%\host-win32\bin;%PATH%

IF "%SN_COMMON_PATH%"=="" (
	echo SN_COMMON_PATH not set, using default.
	set SN_COMMON_PATH=C:\Program Files\SN Systems\Common
)

REM      Set up any default values here so they can be used or 
REM      overriden by the task/job.bat file called below.
SET FLAGS=-ffunction-sections -fdata-sections -g -fno-strict-aliasing
IF %DIR%==psn_debug       SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_beta        SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_bankrelease SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_prerelease  SET FLAGS=-fstack-check %FLAGS%
IF %DIR%==psn_release     SET FLAGS=-fstack-check %FLAGS%
SET DEF_OPT_LVL=-O2
SET COMPILE_SET_0_FILES=%3.%5
SET DEF_WARN=-Wall -Werror -Wundef -Wno-reorder -Wno-multichar 
SET LIBS=-lspurs -ldma -latomic -lsync2
SET ELF_TYPE_FLAG=%6
SET OUTDIR=%CD%\%2
SET OBJ_FILES=
SET SIXTH_PARAM=%~8

pushd %1
cd /D %RAGE_DIR%
SET ABS_RAGE_DIR=%CD%
popd

REM      Save input parameters in env variables so the subroutines below can access them.

SET DONE_FLAG=false
SET INC_DIR=-I %ABS_RAGE_DIR:\=/%/base/src -I %ABS_RAGE_DIR:\=/%/suite/src -I %ABS_RAGE_DIR:\=/%/framework/src
SET FORCE_INC=-include %ABS_RAGE_DIR:\=/%\base\src\forceinclude\spu_%DIR%.h
SET LINK_FLAGS=-Wl,--gc-sections -Wl,-q
SET ELF_NAME=%3
SET ELF_TYPE=%5
SET DEP_FILE_LIST=
SET ADD_DEP_LIST=
SET SPU_ELF_TO_PPU_OBJ_EXTRA_FLAGS=

REM       Call the task/job.bat file to override settings.
pushd %1
IF exist %3.bat CALL %3.bat
IF exist spu_compile_flags.bat echo Warning: spu_compile_flags.bat is no longer used.  Use %3.bat instead.
popd

REM Define a macro called DEBUG_SPU_JOB_NAME that contains a human-readable name for this job. 
REM This can be used in error messages to make things more obvious which job is having problems
SET CPPFLAGS=%CPPFLAGS%	-DDEBUG_SPU_JOB_NAME=\"%ELF_NAME%\"


pushd %1

REM
REM      For each compile set call CompileOrBuild once with the list of 
REM      files in the set, the flags, and the optimization level to be used.
REM      Also pass the file list for the next compile set so that CompileOrBuild
REM      will know if this is the last set to be compiled.  If more compile 
REM      sets are needed, just add more calls to CompileOrBuild at the end 
REM      of this list, and increment the numbers.
REM
CALL :CompileOrBuild "%COMPILE_SET_0_FILES%" "%COMPILE_SET_0_FLAGS%" "%COMPILE_SET_0_OPT_LVL%" "%COMPILE_SET_1_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

CALL :CompileOrBuild "%COMPILE_SET_1_FILES%" "%COMPILE_SET_1_FLAGS%" "%COMPILE_SET_1_OPT_LVL%" "%COMPILE_SET_2_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

CALL :CompileOrBuild "%COMPILE_SET_2_FILES%" "%COMPILE_SET_2_FLAGS%" "%COMPILE_SET_2_OPT_LVL%" "%COMPILE_SET_3_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

CALL :CompileOrBuild "%COMPILE_SET_3_FILES%" "%COMPILE_SET_3_FLAGS%" "%COMPILE_SET_3_OPT_LVL%" "%COMPILE_SET_4_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

CALL :CompileOrBuild "%COMPILE_SET_4_FILES%" "%COMPILE_SET_4_FLAGS%" "%COMPILE_SET_4_OPT_LVL%" "%COMPILE_SET_5_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

CALL :CompileOrBuild "%COMPILE_SET_5_FILES%" "%COMPILE_SET_5_FLAGS%" "%COMPILE_SET_5_OPT_LVL%" "%COMPILE_SET_6_FILES%"
IF errorlevel 1 exit/b 1
IF "%DONE_FLAG%"=="true" goto PostBuildStep

ECHO error: Too may compilation units defined. Add more if needed.
exit/b 1


:PostBuildStep
REM ECHO Post build step...

%ABS_RAGE_DIR%\qa\qa-sed s,/,\\,g %ELF_NAME%_%DIR%.log

REM spu-lv2-objdump -dCr %ELF_NAME%_%DIR%.%5.elf > %ELF_NAME%_%DIR%.%5.txt

popd

REM Convert to a binary file, preserving bss. 
REM Expected symbol is _binary_job_jobname_job_bin_start or _binary_task_taskname_task_bin_start
spu_elf-to-ppu_obj %SPU_ELF_TO_PPU_OBJ_EXTRA_FLAGS% --symbol-name=%5_%ELF_NAME%_%7 %1\%ELF_NAME%_%DIR%.%5.elf %2\%ELF_NAME%.%5.obj > NUL

if errorlevel 1 exit/b 1

REM Put the original elf file somewhere easy to find in the debugger.
IF not exist c:\spu_debug mkdir c:\spu_debug
move %1\%ELF_NAME%_%DIR%.%5.elf c:\spu_debug
move %1\%ELF_NAME%_%DIR%.%5.cmp c:\spu_debug
move %1\%ELF_NAME%_%DIR%.%5.guid c:\spu_debug

REM      This is the end...    of the script
goto:eof


REM **********************************************************************
REM
REM      This subroutine checks to see if various flags have been set, and 
REM      if they have not been it then sets the default flags.  It also puts 
REM      all the command line options to gcc together into one variable, and 
REM      then calls either CompileAndLink or Compile depending on whehter this
REM      is the last compile set.
REM
REM      param 1:  List of files to be compiled.
REM      param 2:  Compiler flags to be used.
REM      param 3:  Optimization level to be used.
REM      param 4:  The next compilation unit's list of files to be compiled.
REM
REM **********************************************************************
:CompileOrBuild
	
	REM If no flags set for this comp unit use default.
	IF %2=="" (
		SET COB_FLAGS=%FLAGS%
	) else (
		SET COB_FLAGS=%~2
	)
	
	REM If no optimization level set for this comp unit use default.
	IF %3=="" (
		SET COB_FLAGS=%COB_FLAGS% %DEF_OPT_LVL%
	) else (
		SET COB_FLAGS=%COB_FLAGS% %~3
	)
	
	IF "%ELF_TYPE%"=="job" (
		SET COB_FLAGS=%COB_FLAGS% -fpic	
	)
	
	SET COB_FLAGS=%CPPFLAGS% %COB_FLAGS% %SIXTH_PARAM% %FORCE_INC% %INC_DIR% %ELF_TYPE_FLAG% %DEF_WARN% %WARN%
	
	REM If this is the last compilation unit compile and link rather than just compile.
	IF %4=="" (
		CALL :CompileAndLink %1 "%COB_FLAGS%"
		SET DONE_FLAG=true
	) else (
		CALL :Compile %1 "%COB_FLAGS%"
	)
	
goto:eof


REM **********************************************************************
REM
REM      This subroutine first calls CreateDepFiles to create a dependency 
REM      file for each input source file.  It then compiles each source file,
REM      and saves the name of the object file in OBJ_FILES.  Additionally, 
REM      here we expand the source file names to a full path so that Visual 
REM      Studio will jump to the code when an error message is double 
REM      clicked in the output window.
REM
REM      param 1:  List of files to be compiled.
REM      param 2:  Compiler flags to be used.
REM      
REM **********************************************************************
:Compile

	CALL :CreateDepFiles %1 %2

	ECHO Following files use: %~2 -c
	
	FOR %%I IN (%~1) DO (
	
		ECHO compiling: %%I
	
		IF "%%~xI"==".%ELF_TYPE%" ( 
			set CP_LANG_FLAG=-x c++ 
		) else ( 
			set CP_LANG_FLAG= 
		) 	
	
		set CP_EXT=%%~xI
		set CP_EXT=!CP_EXT:~1!
		set CP_OUT_FILE=%OUTDIR%\%ELF_NAME%_%%~nI_!CDF_EXT!.spuobj	
	
		gcc2vs spu-lv2-gcc %~2 -c -o !CP_OUT_FILE! !CP_LANG_FLAG! %%~fI 2> %ELF_NAME%_%DIR%.log
		
		IF ERRORLEVEL 1 (	
			%ABS_RAGE_DIR%\qa\qa-sed s,/,\\,g %ELF_NAME%_%DIR%.log
			echo %ELF_NAME%.%ELF_TYPE%: error: warning or error detected during compile.
			exit/b 1
		)
					
		SET OBJ_FILES=!OBJ_FILES! !CP_OUT_FILE!
	)

goto:eof


REM **********************************************************************
REM
REM      This subroutine first calls CreateDepFiles() to create a dependency 
REM      file for each input source file.  It then compiles and links the 
REM      input files with a single call to gcc, and the output file is task/job 
REM      elf.  Then it calls MergeDepFiles to merge the dependency files 
REM      into a single file.
REM
REM      param 1:  List of files to be compiled.
REM      param 2:  Compiler flags to be used.
REM    
REM **********************************************************************
:CompileAndLink

	CALL :CreateDepFiles %1 %2
	
	ECHO Compiling and linking: %~1
	
	call :AddLangFlagToFileList %1
	
	SET BUILD_FILES=%ALFTFL_RETURN_VALUE%
	
	REM The following compiles, as necessary, and links all input files: object, source, assmebly.
	REM ECHO command line  = %~2 %OBJ_FILES% -o %ELF_NAME%_%DIR%.%ELF_TYPE%.elf %LINK_FLAGS% %BUILD_FILES% %LIBS%
	gcc2vs spu-lv2-gcc %~2 %OBJ_FILES% -o %ELF_NAME%_%DIR%.%ELF_TYPE%.elf %LINK_FLAGS% %BUILD_FILES% %LIBS% 2> %ELF_NAME%_%DIR%.log	
	
	IF ERRORLEVEL 1 (	
		%ABS_RAGE_DIR%\qa\qa-sed s,/,\\,g %ELF_NAME%_%DIR%.log
		echo %ELF_NAME%.%ELF_TYPE%: error: warning or error detected during build.
		exit/b 1
	)
	
	spu-lv2-objcopy -j.SpuGUID -O binary %ELF_NAME%_%DIR%.%ELF_TYPE%.elf %ELF_NAME%_%DIR%.%ELF_TYPE%.guid
	%SCE_PS3_ROOT%\host-win32\sn\bin\ps3bin -dsy -c %ELF_NAME%_%DIR%.%ELF_TYPE%.elf > %ELF_NAME%_%DIR%.%ELF_TYPE%.sym
	%RS_TOOLSROOT%\bin\coding\compressmap %ELF_NAME%_%DIR%.%ELF_TYPE%.sym

	CALL :MergeDepFiles "%DEP_FILE_LIST%" %ELF_NAME%.%ELF_TYPE%.d %ELF_NAME%.o		

goto:eof


REM **********************************************************************
REM
REM      This subroutine takes as input a list of source files and searches for 
REM      one with a .job, or .task extension based on the fifth parameter to 
REM      this script.  If it finds such a file it puts it at the end of the list, 
REM      and puts a -x c++ in front of it.  This file needs to be at the end of 
REM      the list since the -x flag effects all source files that follow, until 
REM      another -x option is encountered.  As output this subroutine stores 
REM      the resulting list in: ALFTFL_RETURN_VALUE to be used by the caller.
REM      Additionally, here we expand the file names to a full path so that 
REM      Visual Studio will jump to the code when an error message is double 
REM      clicked in the output window.
REM
REM      param 1:  List of files to search, and add language compiler flag to.
REM      return:   Upon return the modified list will be stored in ALFTFL_RETURN_VALUE.
REM
REM **********************************************************************
:AddLangFlagToFileList

	set ALFTFL_FILE_TO_ADD_FLAG_TO=
	set ALFTFL_RETURN_VALUE=
	
	FOR %%I IN (%~1) DO (
		
		IF "%%~xI"==".%ELF_TYPE%" (
			set ALFTFL_FILE_TO_ADD_FLAG_TO=%%~fI
		) else (
			set ALFTFL_RETURN_VALUE=!ALFTFL_RETURN_VALUE! %%~fI
		)
	)
	
	IF NOT "!ALFTFL_FILE_TO_ADD_FLAG_TO!"=="" (
		set ALFTFL_RETURN_VALUE=%ALFTFL_RETURN_VALUE% -x c++ !ALFTFL_FILE_TO_ADD_FLAG_TO!
	)

goto:eof



REM **********************************************************************
REM
REM      This subroutine takes as input a list of files, and creates one dependency
REM      file (.d) for each of them.  It also adds the name of the dependency file
REM      created to a list of files (stored in DEP_FILE_LIST) to be merged later.
REM      If this subroutine encounters a .s file it saves the name of the file in
REM      ADD_DEP_LIST to be put into the final dependency file later.  Input file
REM      names need to be expanded to a full path to get gcc to put full paths 
REM      in the .d file.
REM
REM      param 1:  List of files to create dependency files for.
REM      param 2:  Compiler flags to be used.
REM
REM **********************************************************************
:CreateDepFiles

	FOR %%I IN (%~1) DO (
	
		IF NOT "%%~xI"==".s" (
		
			IF "%%~xI"==".%ELF_TYPE%" ( 
				set CDF_LANG_FLAG=-x c++ 
			) else ( 
				set CDF_LANG_FLAG= 
			) 
			
			set CDF_EXT=%%~xI
			set CDF_EXT=!CDF_EXT:~1!
		
			spu-lv2-gcc -MM %~2 -MF %OUTDIR%\%ELF_NAME%_%%~nI_!CDF_EXT!.d !CDF_LANG_FLAG! %%~fI
			
			set DEP_FILE_LIST=!DEP_FILE_LIST! %ELF_NAME%_%%~nI_!CDF_EXT!.d	
						
		) else (
		
			SET ADD_DEP_LIST=!ADD_DEP_LIST! %%~fI
		) 
	)

goto:eof



REM **********************************************************************
REM
REM      This subroutine takes a list of dependency files as input and creates a
REM      single dependency file by merging them together.  The input files and 
REM      the output file are read/written from OUTDIR.
REM   
REM      param 1:  List of files to be merged.
REM      param 2:  Name of output dependency file to be created.
REM      param 3:  Name of the object file token to be written to the top of the 
REM                file. (Probably does not matter what name is used since I think
REM                ProDG VSI is just looking for a file with the same name as the
REM                embedable elf obj file, but with a .d extension)
REM
REM **********************************************************************
:MergeDepFiles

	pushd %OUTDIR%

	echo %3: \> %2
	
	FOR %%I IN (%ADD_DEP_LIST%) DO echo   %%I \>> %2	

	set /a NUM_FILES=0
	set /a FILE_IDX=0

	FOR %%I IN (%~1) DO set /a NUM_FILES+=1

	FOR %%I IN (%~1) DO (
		
		set IS_FIRST_LINE=true
		set /a NUM_LINES=0
		set /a FILE_IDX+=1
		set /a LINE_IDX=0
			
		FOR /f "tokens=1*" %%A IN (%%I) DO set /a NUM_LINES+=1
		
		FOR /f "tokens=1*" %%A IN (%%I) DO (
		
			set /a LINE_IDX+=1
		
			if "!IS_FIRST_LINE!"=="true" (
				if NOT "%%B"=="\" (
					echo   %%B>> %2
				)
				set IS_FIRST_LINE=false
			) else (
				if "!LINE_IDX!"=="!NUM_LINES!" (
				
					if "!FILE_IDX!"=="!NUM_FILES!" (
						echo   %%A %%B>> %2
					) else (
						echo   %%A %%B\>> %2
					)
					
				) else (
					echo   %%A %%B>> %2
				)
			)
		)
	)
	
	popd

goto:eof
