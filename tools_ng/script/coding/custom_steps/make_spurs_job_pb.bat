
REM        Param 5 that is passed in needs to be last since 
REM        it may be empty, so here it becomes param 8. 

REM Fix directory - VS2010 appends a trailing slash, done in ruby just out of interest for now...
REM ruby -e 'puts ARGV[0].gsub(/\\/,"")' %2 > tmpfile  
REM set /p INTDIR=<tmpFile  
REM del tmpFile 
REM echo "INTDIR %2 => %INTDIR%"

REM change -mspurs-job to -mspurs-job-initialize for libsync2
REM call %~dp0make_spurs_elf_pb.bat %1 %INTDIR% %3 %4 job -mspurs-job job_bin %5
call %~dp0make_spurs_elf_pb.bat %1 %2 %3 %4 job -mspurs-job job_bin %5
