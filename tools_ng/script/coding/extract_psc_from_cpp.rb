# regenerates all _parser.h files in a source tree

require "rexml/Document"
include REXML

startTime = Time.now

# for each directory
Dir.glob("**/.") do |filename|
	# get any vcproj files in the directory
	vcprojects = Dir.glob(File.join(File.dirname(filename), "*.vcproj"))

	# extract all the "CommandLine=" strings from them
	commandLines = {}
	vcprojects.each do |proj|
		puts proj
		file = File.new(proj)
		# begin
			# xml = Document.new(file)
			# XPath.each(xml, "//@CommandLine") do |cmd|
				# cmdStr = Text.unnormalize(cmd.to_s())
				# if cmdStr =~ /parCodeGen/
					# commandLines[cmdStr] = nil # add cmdStr to the hash
				# end
			# end
		# rescue ParseException
			# $stderr.print("Couldn't parse file #{proj}\n")
			# $stderr.print $!
		# end
		ENV["InputDir"] = File.expand_path(File.dirname(filename))
		ENV["InputPath"] = File.expand_path(File.dirname(filename))

		file.each_line do |line|
			if line =~ /RelativePath\s*=\s*(['"])(.*?)\1/
				ENV["InputFileName"] = $2
			end
			if line =~ /CommandLine\s*=\s*(['"])(.*?)\1/
				# replace $(VAR) with the value of VAR
				origCmd = $2
				cmd = $2.gsub(/\$\((\w+)\)/) {ENV[$1]}
				
				commandLines[cmd] = origCmd # add cmdstr to the hash
			end
		end
	end

	oldDir = Dir.getwd()
	commandLines.each() do |key, value|
		keyCmds = Text.unnormalize(key.dup).split(/$|&&/)
		Dir.chdir(File.dirname(filename))
		dirStack = []
#		begin
			keyCmds.each do |cmd|
				case cmd
				when /pushd\s+(.*)/
					dirStack.push(Dir.getwd())
					newDir = $1.strip()
					newDir.gsub!(/\\/, '/') # \ to /
					newDir.sub!(/^\.\//, '') # remove leading ./
					newDir = File.expand_path(newDir)
					Dir.chdir(newDir)
				when /parCodeGen/
					# Add a 'rebuild' option 
#					newCmd = cmd.strip().gsub(/parCodeGen\.\w+/, '\& --rebuild')
				
#					begin
#						system(newCmd)
#					rescue 
#					end
					cmd.split()[1..-1].each do |f|
						if File::exist?(f) and f =~ /\.psc$/ then
							puts "Skipping .psc file #{f}"
						elsif File::exist?(f + ".cpp") and not File::exist?(f + ".psc") then
							puts "Found one. Extracting structdefs from #{f}.cpp"
							`X:\\gta5\\tools\\bin\\coding\\python\\parCodeGen.exe --noschema --nocode --rebuild --savemetadata=. #{f}`
							`p4 add #{f}.psc`
						end
					end

				when /popd/
					Dir.chdir(dirStack.pop())
				end
			end
#		rescue
#		end
		Dir.chdir(oldDir)
	end
	
end

endTime = Time.now

puts "Runtime was #{endTime - startTime}"


