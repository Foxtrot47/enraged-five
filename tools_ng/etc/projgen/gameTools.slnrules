#   
# Project to Solution config transformations/mapping for the Game.
#

Properties {
	SolutionItems += '%RS_CODEBRANCH%\projgen\cpp.props'
	SolutionItems += '%RS_CODEBRANCH%\projgen\projgen.props'
	SolutionItems += '%RS_CODEBRANCH%\projgen\unity.targets'
	SolutionItems += '%RS_CODEBRANCH%\Directory.Build.props'
	SolutionItems += '%RS_TOOLSSCRIPT%\coding\autoexp\rage.natvis'
}

Project_Config_Rules { 

    #
    # Regex(proj_id:sln cfg:sln platform)  = project cfg|project platform
    #

    ^[^:]*:([^:]*):([^:]*)$                   = '(1)|(2)'                 # Default All projects map to a default eg.     *[Debug|Win32] = Debug|Win32
    [^:]*:([^:]*):Win32 DX11                = '(1) DX11|Win32'          # Projects in Win32 DX11 solution config, map to the project config DX11|Win32                              
    shaders:([^:]*):Win32 DX11              = '(1)|Win32'               # Shaders project has no DX11 platform or configs

    # The shaders_rc and shaders_dependency projects can be built in Debug or Release configurations. Debug is the usual development build for shaders. 
    # Release is a "final" shader build that strips out anything that we don't want to ship.
    
    shaders_rc*:([^:]*):([^:]*)             = 'Debug|(2)'               # Always build debug config of shaders_rc except for Final builds   
    shaders_rc*:Final:([^:]*)               = 'Release|(1)'             # Build release config of shaders_rc for Final builds
    shaders_rc*:([^:]*):Win32 DX11          = 'Debug Win32 4.0|Win32'   # Remap target/platform to shader ones.
    shaders_rc*:Final:Win32 DX11            = 'Release Win32 4.0|Win32' # Remap target/platform to shader ones.
    shaders_rc*:([^:]*):x64                 = 'Debug Win32 4.0|Win32'   # Remap target/platform to shader ones.
    shaders_rc*:Final:x64                   = 'Release Win32 4.0|Win32' # Remap target/platform to shader ones.
    
    shaders_dependency*:([^:]*):([^:]*)     = 'Debug|Win32'             # Always build Debug|Win32 of shaders_dependency except for Final builds 
    shaders_dependency*:Final:([^:]*)       = 'Release|Win32'           # Build Release|Win32 of shaders_dependency for Final builds 
    
    PS3_[^:]*:[^:]*:(?!PS3)...              = 'Empty|PS3'               # Non PS3 platform configs map to Empty|PS3 for PS3 projects ( VS2010 quirk! )
	^[^:]*_psc:[^:]*:[^:]*$           = 'Release|x64'
}

# -------------------------------
# --- Solution Exporter Rules ---
# -------------------------------

Project_Config_Rules { 
    
    #
    # Regex(proj_id:sln cfg:sln platform:exporter)  = project cfg|project platform
    #

    [^:]*:[^:]*:Durango:VS2010.*        = ''        # Exporter rule : No Durango in VS2010
#   [^:]*:[^:]*:ORBIS:VS2010.*		= ''        # Exporter rule : No Orbis in VS2010

    [^:]*:[^:]*:[^:]*:VS2012.*		= ''        # Start with a clean slate for VS2012
    [^:]*:([^:]*):(Win32 DX11):VS2012   = '(1) DX11|Win32'          # Projects in Win32 DX11 solution config, map to the project config DX11|Win32                              
    [^:]*:([^:]*):(Durango):VS2012.*    = '(1)|(2)' # Exporter rule : Durango in VS2012
    [^:]*:([^:]*):(x64):VS2012.*        = '(1)|(2)' # Exporter rule : x64 in VS2012
    [^:]*:([^:]*):(Win32):VS2012.*  	= '(1)|(2)' # Exporter rule : win32 in VS2012
    PS3_[^:]*:[^:]*:[^:]*:VS2012.*      = ''        # Exporter rule : No PS3_* libs in VS2012
	^[^:]*_psc:[^:]*:[^:]*:[^:]*$           = 'Release|x64'
}


Build_Rules {                   # Projects to build.
    ^[^:]*:[^:]*:[^:]*$       		= 'true'    # Default; ALL projects build by default.
    shaders:[^:]*               = 'false'   # Shaders projects do not build.
    shaders_rc:[^:]*            = 'false'   # shaders_rc projects do not build.
	shaders_dependency:[^:]*    = 'false'   # shaders_dependency projects do not build.
    PS3_[^:]*:[^:]*:(?!PS3)...  = 'false'   # Non PS3 platform configs map to Empty|PS3 don't build
}

Deploy_Rules {                  # Projects to Deploy.
    [^:]*:([^:]*):([^:]*)       = 'false'   # Default; ALL projects do not deploy by default.
    shaders.*                   = 'false'   # No shaders will deploy
    game:[^:]*:Xbox 360         = 'true'    # The game will deploy on Xbox 360
    game:[^:]*:Durango          = 'true'    # The game will deploy on Durango
}