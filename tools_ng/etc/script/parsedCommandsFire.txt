EXTINGUISH_FIRE_AT_POINT 	-> 	STOP_FIRE_IN_RANGE	~xp
START_CHAR_FIRE 	-> 	START_PED_FIRE
EXTINGUISH_PED_FIRE 	-> 	STOP_PED_FIRE
EXTINGUISH_CHAR_FIRE 	-> 	STOP_PED_FIRE
IS_CHAR_ON_FIRE 	-> 	IS_PED_ON_FIRE
IS_SCRIPT_FIRE_EXTINGUISHED -> IS_SCRIPT_FIRE_EXTINGUISHED	~TR
REMOVE_ALL_SCRIPT_FIRES -> REMOVE_ALL_SCRIPT_FIRES	~TR
START_OBJECT_FIRE -> START_OBJECT_FIRE	~TR
EXTINGUISH_OBJECT_FIRE -> EXTINGUISH_OBJECT_FIRE	~TR
SET_SCRIPT_FIRE_AUDIO -> SET_SCRIPT_FIRE_AUDIO	~TR
GET_NUMBER_OF_FIRES_IN_AREA -> GET_NUMBER_OF_FIRES_IN_AREA	~TR
GET_SCRIPT_FIRE_COORDS -> GET_SCRIPT_FIRE_COORDS	~TR
SET_MAX_FIRE_GENERATIONS -> SET_MAX_FIRE_GENERATIONS	~TR
DOES_SCRIPT_FIRE_EXIST -> DOES_SCRIPT_FIRE_EXIST	~TR
CLEAR_ALL_SCRIPT_FIRE_FLAGS -> CLEAR_ALL_SCRIPT_FIRE_FLAGS	~TR
