SET RAGE_CORE_LIBS=%RAGE_DIR%\base\src\vcproj\RageCore\RageCore.vcproj stlport
SET RAGE_IM_LIBS=grcore shaderlib input jpeg
SET RAGE_GFX_LIBS=%RAGE_DIR%\base\src\vcproj\RageGraphics\RageGraphics.vcproj
SET RAGE_OLD_AUD_LIBS=%RAGE_DIR%\base\src\vcproj\RageOldAudio\RageOldAudio.vcproj
SET RAGE_NEW_AUD_LIBS=%RAGE_DIR%\base\src\\vcproj\RageAudio\RageAudio.vcproj
SET RAGE_AUD_LIBS=%RAGE_NEW_AUD_LIBS%
SET RAGE_PH_LIBS=%RAGE_DIR%\base\src\vcproj\RagePhysics\RagePhysics.vcproj
SET RAGE_NET_LIBS=%RAGE_DIR%\base\src\vcproj\RageNet\RageNet.vcproj
SET RAGE_AI_LIBS=%RAGE_DIR%\suite\src\vcproj\RageSuiteAi\RageSuiteAi.vcproj
SET RAGE_FRAGMENT_LIBS=%RAGE_DIR%\suite\src\vcproj\RageSuiteMisc\RageSuiteMisc.vcproj
SET RAGE_CR_LIBS=%RAGE_DIR%\base\src\vcproj\RageCreature\RageCreature.vcproj
SET RAGE_SUITE_CR_LIBS=%RAGE_DIR%\suite\src\vcproj\RageSuiteCreature\RageSuiteCreature.vcproj
SET RAGE_SCRIPT_LIBS=%RAGE_DIR%\script\src\vcproj\RageScript\RageScript.vcproj
SET RAGE_SCALEFORM_LIBS=%RAGE_DIR%\scaleform\src\vcproj\ScaleformGfx\ScaleformGfx.vcproj scaleform
set RAGE_SAMPLE_GRCORE_LIBS=%RAGE_DIR%\base\samples\vcproj\RageBaseSample\RageBaseSample.vcproj
set RAGE_SAMPLE_LIBS=%RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore shaders

set RAGE_NM_LIBS=%RAGE_DIR%\naturalmotion\vcproj\NaturalMotion\NaturalMotion.vcproj

set RAGE_ST_LIBS=%RAGE_DIR%\speedtree\src\vcproj\SpeedTree\SpeedTree.vcproj
