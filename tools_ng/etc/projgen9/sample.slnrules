#
# Sample.slnrules : General rules that apply to all samples
#

Properties {
	SolutionItems += '%RS_CODEBRANCH%\projgen\cpp.props'
	SolutionItems += '%RS_CODEBRANCH%\projgen\projgen.props'
	SolutionItems += '%RS_CODEBRANCH%\Directory.Build.props'
	SolutionItems += '%RS_TOOLSSCRIPT%\coding\autoexp\rage.natvis'
}

Project_Config_Rules { 

	#
    # Regex(proj_id:sln cfg:sln platform)  = "project cfg|project platform"
	#

    ^[^:]*:([^:]*):([^:]*)$                   = '(1)|(2)'                 # Default All projects map to a default eg.     *[Debug|Win32] = Debug|Win32
    ^[^:]*:([^:]*):Win32 DX11$                = '(1) DX11|Win32'          # Projects in Win32 DX11 solution config, map to the project config DX11|Win32                              
    ^shaders:([^:]*):Win32 DX11$              = '(1)|Win32'               # Shaders project has no DX11 platform or configs
    ^[^:]*:([^:]*):ORBIS 7.0$                = '(1) 7.0|ORBIS'          # Projects in ORBIS 7.0 solution config, map to the project config DX11|Win32                              
    ^shaders:([^:]*):ORBIS 7.0$              = '(1)|ORBIS'               # Shaders project has no 7.0 platform or configs

    # The shaders_rc and shaders_dependency projects can be built in Debug or Release configurations. Debug is the usual development build for shaders. 
    # Release is a "final" shader build that strips out anything that we don't want to ship.
    
    ^shaders_rc*:[^:]*:([^:]*)$               = 'Debug|(1)'               # Always build debug config of shaders_rc except for Final builds   
    ^shaders_rc*:Final:([^:]*)$               = 'Release|(1)'             # Build release config of shaders_rc for Final builds
    ^shaders_rc*:Master[A-Za-z]*:([^:]*)$     = 'Release|(1)'             # Build release config of shaders_rc for Master builds
    ^shaders_rc*:[^:]*:Win32 DX11$            = 'Debug Win32 4.0|Win32'   # Remap target/platform to shader ones.
    ^shaders_rc*:Final:Win32 DX11$            = 'Release Win32 4.0|Win32' # Remap target/platform to shader ones.
    ^shaders_rc*:[^:]*:ORBIS 7.0$            = 'Debug|ORBIS'   # Remap target/platform to shader ones.
    ^shaders_rc*:Final:ORBIS 7.0$            = 'Release|ORBIS' # Remap target/platform to shader ones.
    ^shaders_rc*:[^:]*:x64$                   = 'Debug Win32 4.0|Win32'   # Remap target/platform to shader ones.
    ^shaders_rc*:Final:x64$                   = 'Release Win32 4.0|Win32' # Remap target/platform to shader ones.
    
    ^shaders_dependency*:[^:]*:[^:]*$         = 'Debug|Win32'             # Always build Debug|Win32 of shaders_dependency except for Final builds 
    ^shaders_dependency*:Final:[^:]*$         = 'Release|Win32'           # Build Release|Win32 of shaders_dependency for Final builds 
	^shaders_dependency*:Master[A-Za-z]*:[^:]*$ = 'Release|Win32'		  # Build Release|Win32 of shaders_dependency for Master builds 
	
	^PS3_[^:]*:Master[A-Za-z]*:PS3$           = 'Final|PS3' 			  # Use final version of ps3 specific projects
	
    ^PS3_[^:]*:[^:]*:(?!PS3)...               = 'Empty|PS3'               # Non PS3 platform configs map to Empty|PS3 for PS3 projects ( VS2010 quirk! )
	^[^:]*_psc:[^:]*:[^:]*$           = 'Release|x64'
}

# -------------------------------
# --- Solution Exporter Rules ---
# -------------------------------

Project_Config_Rules { 
	
	#
    # Regex(proj_id:sln cfg:sln platform:exporter)  = "project cfg|project platform"
	#

    ^[^:]*:[^:]*:Durango:VS2010.*$      = ''        # Exporter rule : No Durango in VS2010
    ^[^:]*:[^:]*:Durango:VS2019.*$      = ''        # Exporter rule : No Durango in VS2019
    ^[^:]*:[^:]*:Win32:VS2012.*$		= ''        # Exporter rule : No Win32 in VS2012
    ^[^:]*:[^:]*:Win32 DX11:VS2012.*$	= ''        # Exporter rule : No Win32 in VS2012
    ^[^:]*:[^:]*:Rsc:VS2012.*$			= ''        # Exporter rule : No Rsc in VS2012
    ^[^:]*:[^:]*:Tool:VS2012.*$			= ''        # Exporter rule : No Tool in VS2012 
    ^[^:]*:[^:]*:Tool64:VS2012.*$		= ''        # Exporter rule : No Tool64 in VS2012        
    ^[^:]*:[^:]*:Yeti:VS2012.*$         = ''        # Exporter rule : No Yeti in VS2012
    ^[^:]*:[^:]*:Yeti:VS2017.*$         = ''        # Exporter rule : No Yeti in VS2017
    ^[^:]*:[^:]*:Yeti:VS2019.*$         = ''        # Exporter rule : No Yeti in VS2019    
    ^[^:]*:[^:]*:GGP:VS2012.*$          = ''        # Exporter rule : No GGP in VS2012
    ^[^:]*:[^:]*:GGP:VS2015.*$          = ''        # Exporter rule : No GGP in VS2015
    ^[^:]*:[^:]*:NX64:VS2012.*$          = ''        # Exporter rule : No NX64 in VS2012
    ^[^:]*:[^:]*:NX64:VS2015.*$          = ''        # Exporter rule : No NX64 in VS2015
    ^[^:]*:[^:]*:Gaming.Xbox.x64:VS2012.*$          = ''        # Exporter rule : No Gaming.Xbox.x64 in VS2012
    ^[^:]*:[^:]*:Gaming.Xbox.x64:VS2015.*$          = ''        # Exporter rule : No Gaming.Xbox.x64 in VS2015
    ^[^:]*:[^:]*:Prospero:VS2012.*$          = ''        # Exporter rule : No Prospero in VS2012
    ^[^:]*:[^:]*:Prospero:VS2015.*$          = ''        # Exporter rule : No Prospero in VS2015
}


Build_Rules {					# Projects to build.
    ^[^:]*:[^:]*:[^:]*$       		= 'true'    # Default; ALL projects build by default.
    ^PS3_[^:]*:[^:]*:(?!PS3)...  	= 'false'   # Non PS3 platform configs map to Empty|PS3 don't build
}

Deploy_Rules { # Projects to Deploy.
    ^[^:]*:[^:]*:[^:]*$             = 'false'   # Default; ALL projects do not deploy by default.
    ^sample_[^:]*:[^:]*:Durango$    = 'true'    # The game will deploy on Durango
    ^sample_[^:]*:[^:]*:Xbox 360$   = 'true'    # The game will deploy on Xbox 360
    ^sample_[^:]*:[^:]*:Gaming.Desktop.x64$        	= 'true'    # The game will deploy on Gaming.Desktop.x64
    ^sample_[^:]*:[^:]*:Gaming.Xbox.XboxOne.x64$    = 'true'    # The game will deploy on Gaming.Xbox.XboxOne.x64
    ^sample_[^:]*:[^:]*:Gaming.Xbox.Scarlett.x64$   = 'true'    # The game will deploy on Gaming.Xbox.Scarlett.x64
}






