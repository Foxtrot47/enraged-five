##########################################################################################################################
# RULES FILE 
# Usage & Details : https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3#game.rules
##########################################################################################################################

# Rules : independent of exporter.
Rules {	
	#-------------------------------------------------------------------------------------------
	# Setup Scratch ( make your own vars here and reuse with <var> syntax. )
	#-------------------------------------------------------------------------------------------
	Scratch.BuildDir			= '$(RS_TITLE_UPDATE)'
	Scratch.CompressMap			= '&quot;$(RS_TOOLSROOT)\bin\coding\compressmap&quot;'
	Scratch.Ps3Bin				= '$(SCE_PS3_ROOT)\host-win32\sn\bin\ps3bin'
	Scratch.DumpBin				= '$(RS_TOOLSROOT)\script\util\dumpbin.rb'
	Scratch.StripObjects			= '$(RS_TOOLSROOT)/bin/coding/python/stripobjects.exe'
	Scratch.XbeCopy				= '&quot;$(XEDK)\bin\win32\xbecopy&quot;'
	
	# 'Architecture'
	Scratch.Arch[Platform=='PS3']		= 'psn'
	Scratch.Arch[Platform=='Xbox 360']	= 'xenon'
	Scratch.Arch[Platform=='Win32']		= 'win32'
	Scratch.Arch[Platform=='x64']		= 'win64'
	Scratch.Arch[Platform=='Orbis']		= 'orbis'
	Scratch.Arch[Platform=='Durango']	= 'durango'
	
	# DX11 name mangling : pain in the t*ts
	Scratch.ForceInclude				= '_<Config>.h'
	Scratch.ForceInclude[Config=='Debug DX11']	= '_dx11_debug.h'
	Scratch.ForceInclude[Config=='Beta DX11']	= '_dx11_beta.h'
	Scratch.ForceInclude[Config=='BankRelease DX11']= '_dx11_bankrelease.h'
	Scratch.ForceInclude[Config=='Release DX11']	= '_dx11_release.h'
	Scratch.ForceInclude[Config=='Profile DX11']	= '_dx11_profile.h'
	Scratch.ForceInclude[Config=='Final DX11']	= '_dx11_final.h'
	
	#-------------------------------------------------------------------------------------------
	# General 
	#-------------------------------------------------------------------------------------------	
	Settings.General.Rtti		= 'true'

	Condition [Platform=='Win32'] {
		#Settings.General.IncludePath += '$(WindowsSdk_Dir)\Include\shared'
		#Settings.General.IncludePath += '$(WindowsSdk_Dir)\Include\um'
		#Settings.General.IncludePath += '$(DXSDK_DIR)\Include'
		#Settings.General.LibraryPath += '$(WindowsSdk_Dir)\lib\win8\um\x86'
		#Settings.General.LibraryPath += '$(DXSDK_DIR)\Lib\x86'
	}
	
	Condition [Platform=='x64'] {
		#Settings.General.IncludePath += '$(WindowsSdk_Dir)\Include\shared'
		#Settings.General.IncludePath += '$(WindowsSdk_Dir)\Include\um'
		#Settings.General.IncludePath += '$(DXSDK_DIR)\Include'
		#Settings.General.LibraryPath += '$(WindowsSdk_Dir)\lib\win8\um\x64'
		#Settings.General.LibraryPath += '$(DXSDK_DIR)\Lib\x64'
	}
	
	Condition [Platform=='Durango'] {
		Settings.General.IncludePath += '$(DurangoXDK)/xdk/include/winrt'
		Settings.General.IncludePath += '$(DurangoXDK)/xdk/include/um'
		Settings.General.IncludePath += '$(DurangoXDK)/xdk/include/shared'
		Settings.General.LibraryPath +=	'$(RAGE_DIR)/lib'
		Settings.General.LibraryPath +=	'$(DurangoXDK)/xdk/lib'
	}

	#
	# Note the suffix for the exporter eg. _vs_2010 is not set here, this is responsibility of the exporter rules
	#
	
	Action Settings.General.OutputDirectoryName {		
		[ProjectType!='Exe'&&Platform!='Durango']		= '<Scratch.Arch>_<Config>'		
		[ProjectType=='Exe'&&Platform!='Durango'] 		= '<Scratch.BuildDir>/'		# exe written directly to build directory.
	} 												# NOTE: there ARE exporter rules that augment this setting further ( if the case of unity ) 
	
	Action Settings.General.IntermediateDirectoryName {  							
		# For exe do not pollute the build folder with intermediate files.
		[ProjectType=='Exe'&&Platform!='Durango']		= '<Scratch.Arch>_<Config>'		
		[ProjectType!='Exe'&&Platform!='Durango'] 		= '$(OutDir)' 			# In general ( for libs ) we want to cut down on superfluous directory bloat; so intermediate this is the same as output.
	} 												# NOTE: there ARE exporter rules that augment this setting further ( if unity ) 
	
	Action Settings.General.TargetName {
		[Platform!='Durango'] 					= '$(ProjectName)'		# All targets start out with a name.		
		[ProjectType=='Exe'&&Platform!='Durango']		<< '_<Scratch.Arch>_<Config>'	# for EXE though we mangle with its architecture, since it will reside alongside other 'exes', for everything else a name is good enough.
		[ProjectType=='Exe'&&Platform=='PS3']			<< '_snc'			# PS3 quirk - is this necessary?
	}
		
	#-------------------------------------------------------------------------------------------
	# Compiler 
	#-------------------------------------------------------------------------------------------	
	Action Settings.Compiler.FavourSize {
		[Name=='Beta|Xbox 360'] = 'true'
		[Name!='Beta|Xbox 360'] = 'false'
	}
	Action Settings.Compiler.FavourSpeed {
		[Name=='Beta|Xbox 360'] = 'false'
		[Name!='Beta|Xbox 360'] = 'true'
	}
	Settings.Compiler.GenerateDebugInformation		= 'true'
	Settings.Compiler.TreatWarningsAsErrors[Platform!='Durango']= 'true'	
	
	Action Settings.Compiler.StringPooling {
		[ProjectType!='Exe']	 = 'true'
		[ProjectType=='Exe']	 = 'false'	
		[Name=='ToolBeta|Win32'] = 'false'
	}	
	
	Action Settings.Compiler.AdditionalOptions {
		[Name=='Profile|Xbox 360'||Name=='Release|Xbox 360'||Name=='BankRelease|Xbox 360'||Name=='Final|Xbox 360'] += '/QVMX128'	
		[Name=='Profile|Xbox 360']	+=	'/callcap' 
		[Platform=='Orbis'] += '-frtti'
	}	
		
	Action Settings.Compiler.PreprocessorDefinitions {
		[ProjectType!='Exe'] 			  += '_LIB'
		[ProjectType=='Exe'] 			  += '_WINDOWS'		# some settings are as I found them in the old project generator, I suspect such exe / lib variations might not actually have been intentional ( somebody please review )
		[Platform=='Xbox 360'||Platform=='Win32'] += 'WIN32'
		[Platform=='x64'||Platform=='Durango']	  += 'WIN64'
		[Platform=='PS3']			  += '__SNC__'
		[Platform=='PS3']			  += 'SN_TARGET_PS3'
		#[Platform=='x64'||Platform=='Win32']	  += '__DXSDK=$(DXSDK_DIR)'

		[Config=~'Debug'||Config=='Beta DX11'||Config=='RscBeta'||Config=='ToolBeta']	+= '_DEBUG'			
		[Config=='Beta'||Config=~'Release'||Config=~'Final'||Config=~'Profile']		+= 'NDEBUG'
	}
	
	Action Settings.Compiler.ForceIncludes {
		+='$(RAGE_DIR)\base\src\forceinclude\<Scratch.Arch><Scratch.ForceInclude>'	
	}	
	
	Action Settings.Compiler.AdditionalIncludeDirectories {
		+='$(RAGE_DIR)\base\src'
		+='$(RAGE_DIR)\suite\src'
		+='$(ProjectDir)..'
		+='$(RAGE_DIR)\naturalmotion\include'
		+='$(RAGE_DIR)\naturalmotion\src'
		+='$(RAGE_DIR)\script\src'
		+='$(RAGE_DIR)\framework\src'
	}	
		
	Action Settings.Compiler.AdditionalLibraryDirectories {
		[Platform=='Durango'] += '$(RAGE_DIR)\lib'
	}
	
	#-------------------------------------------------------------------------------------------
	# Librarian 
	#-------------------------------------------------------------------------------------------
	Condition [ProjectType!='Exe'] {
		Settings.Librarian.AdditionalOptions[Platform!='PS3' && Platform!='Orbis']	+= '/ignore:4221'		
	}
	
	#-------------------------------------------------------------------------------------------
	# Exe Settings : Linker, Image & Deploy
	#-------------------------------------------------------------------------------------------			
	Condition [ProjectType=='Exe'&&Platform=='Orbis'] {
		Settings.Linker.AdditionalLibraryDirectories += '$(SCE_ORBIS_SDK_DIR)\target\lib;'
	}

	Condition [ProjectType=='Exe'] {	
		Settings.Linker.AdditionalLibraryDirectories[Platform=='x64'] += '$(RAGE_DIR)\3rdparty\Oodle\oodle2sdkwin\lib'	
		Settings.Linker.AdditionalLibraryDirectories 		+= '$(RAGE_DIR)\lib;$(RAGE_DIR)\3rdparty\bink2\lib'	
		Settings.Linker.AdditionalLibraryDirectories 		+= '$(RAGE_DIR)\lib'
		Settings.Linker.AdditionalOptions[Platform!='PS3' && Platform!='Orbis'] 	+= '/FIXED:NO' 
		Settings.Linker.AdditionalOptions[Platform!='PS3' && Platform!='Orbis'] 	+= '/IGNORE:4089'
		Settings.Linker.GenerateDebugInfo 			= 'true'
		Settings.Linker.GenerateMapFile 			= 'true'
		Settings.Linker.LinkIncremental 			= 'false'
		Settings.Linker.LinkIncremental[Config=='Debug']	= 'true'	# not sure what this was set in previous project generator - verify this is what you want.
	}

	Condition [ProjectType=='Exe'&&Platform=='PS3'] {	
		Settings.Linker.DataStripping				= 'StripFuncsAndData'
		Settings.Linker.AdditionalLibraryDirectories		+= '&quot;$(SCE_PS3_ROOT)\target\ppu\lib&quot;'	
		Settings.Linker.AdditionalOptions 			+= '--notocrestore'
		Settings.Linker.AdditionalOptions 			+= '-Wl,--disable-warning=134'
		Settings.Linker.AdditionalOptions 			+= '-Wl,--oformat=fself'		
		Settings.Linker.AdditionalOptions 			+= '-Wl,--strip-duplicates'		
		Settings.Linker.AdditionalOptions[Config=='Profile'] 	+= '--callprof'
	}	
	
	Condition [ProjectType=='Exe'&&Platform=='Xbox 360'] {	
		Settings.Linker.AdditionalDependencies  += 'xboxkrnl.lib'
		Settings.Linker.IgnoreLibraries[Config!='Release' && Config!='Final'] 	+= 'xapilib.lib'	
	
		#-------------------------------------------------------------------------------------------
		# Image
		#-------------------------------------------------------------------------------------------
		Settings.Image.OutputFilename 		= '<Scratch.BuildDir>/$(ProjectName)_<Scratch.Arch>_<Config>.xex'

		#-------------------------------------------------------------------------------------------
		# Deploy
		#-------------------------------------------------------------------------------------------
		Settings.Deploy.DeployFiles 	+= '$(RemoteRoot)=$(TEMP)\rfs.dat'
		Settings.Deploy.DeployFiles 	+= '$(RemoteRoot)=$(ImagePath)'
		Settings.Deploy.DeployType 	= 'CopyToHardDrive'	
		Settings.Deploy.ExecutionBucket = '85313600'
		Settings.Deploy.RemoteRoot 	= 'xe:\$(ProjectName)' 
	}	
} # end rules







# Exporter Rules must always be run after normal rules.
# Residing here are rules that are exporter conditional.
# Also we have 'bucket' rules that are yet to be abstracted into an independent format.
ExporterRules[Exporter=~'VS201'] { # The family of VS201X exporters ( VCXPROJ ) 

	#-------------------------------------------------------------------------------------------
	# Scratch ( ExporterRules ) 
	#-------------------------------------------------------------------------------------------
	Scratch.Unity[Exporter=~'_Unity'] 	= '_unity'
	Scratch.Unity[Exporter!~'_Unity'] 	= ''
	Scratch.VS[Exporter=~'VS2010'] 		= '2010'
	Scratch.VS[Exporter=~'VS2012'] 		= '2012'

	#-------------------------------------------------------------------------------------------
	# Deactivate specific targets at export if required
	#-------------------------------------------------------------------------------------------	
	Condition [Exporter=~'VS2010'] {
#		Active[Platform=='Orbis'] = 'false'
		Active[Platform=='Durango'] = 'false'
	}

	Condition [Exporter=~'VS2012'] {
		Active[Platform!='Win32'&&Platform!='Win32 DX11'&&Platform!='x64'&&Platform!='Durango'] = 'false'
	}

	#-------------------------------------------------------------------------------------------
	# General ( ExporterRules )
	#-------------------------------------------------------------------------------------------
	Settings.General.CharacterSet = 'MultiByte'
	Settings.General.OutputDirectoryName[ProjectType!='Exe'&&Platform!='Durango'] 		<< '<Scratch.Unity>_<Scratch.VS>/'	# for libs the output directory needs to be unique for unity and exporter variants
	Settings.General.IntermediateDirectoryName[ProjectType=='Exe'&&Platform!='Durango'] 	<< '<Scratch.Unity>_<Scratch.VS>/'	# for exe only the intermediate directory only is required to be elsewhere, don't worry libs will use $(OutDir) - this uses the above.

	Action Settings.General.Exceptions { # False, Sync, SyncCThrow, Async
		= 'False'  	
		#[Name=='ToolDebug|Win32'] = 'Sync'
	}

	#-------------------------------------------------------------------------------------------
	# Compiler ( ExporterRules )
	#-------------------------------------------------------------------------------------------
	Settings.Compiler.CompileAsWinRT[Platform=='Durango'] = 'false'

	Action Settings.Compiler.FunctionLevelLink {
		[ProjectType=='Exe'] = 'false'		# some settings are as I found them in the old project generator, I suspect such exe / lib variations might not actually have been intentional ( somebody please review )
		[ProjectType!='Exe'] = 'true'		
	}

	# Disabled, Full, MaxSpeed, MinSpace
	Action Settings.Compiler.Optimization { 
		[Platform!='PS3']					= 'MaxSpeed'			
		[Config=~'Debug'&&Platform!='PS3']			= 'Disabled'		
		[Config=='Final'&&Platform!='PS3']			= 'Full'
		[Name=='ToolDebug|Win32']				= 'Disabled'	
		[Name=='Beta|Xbox 360']					= 'MinSpace'
	}						
	
	Action Settings.Compiler.RunTimeLibrary	{ # MultiThreaded, MultiThreadedDebugDLL, MultiThreadedDebug, MultiThreadedDLL
		= 'MultiThreaded'
		[Config=~'Debug'&&Platform!='PS3'] 		= 'MultiThreadedDebug'
		[Config=='ToolDebug'] 				= 'MultiThreadedDebugDLL'
		[Config=='ToolBeta'||Config=='ToolRelease'] 	= 'MultiThreadedDLL'
		[Config=='Debug'&&Platform=='Durango']		= 'MultiThreadedDebugDLL'
		[Config!='Debug'&&Platform=='Durango']		= 'MultiThreadedDLL'
	}
	
	Action Settings.Compiler.InlineFunctionExpansion { # Disabled, AnySuitable, OnlyExplicitInline
		= 'OnlyExplicitInline'
		[ProjectType!='Exe'&&Platform=='Xbox 360']	= 'AnySuitable'
		[Name=='ToolDebug|Win32']			= 'Default' 	# not compatible with DebugInformationFormat=EditAndContinue
	}
	
	Action Settings.Compiler.DisableSpecificWarnings {
		[Platform=='Win32'] 	+= '4100'	# eg. warning C4100: 'hasFocus' : unreferenced formal parameter
		[Platform=='PS3'] 	+= '552'
		[Platform=='PS3'] 	+= '178'		
	}	
				
	Settings.Compiler.BasicRuntimeChecks								= 'default'
	Settings.Compiler.BasicRuntimeChecks[Name=='ToolDebug|Win32']					= 'EnableFastChecks'
	Settings.Compiler.BufferSecurityCheck								= 'false'
	Settings.Compiler.BufferSecurityCheck[Name=='ToolBeta|Win32'] 					= 'true'
	Settings.Compiler.CompileAs 									= 'default'
	Settings.Compiler.DebugInformationFormat							= 'ProgramDatabase'	
	#Settings.Compiler.DebugInformationFormat[Config='ToolDebug'&&Platform=='Win32'] 			= 'EditAndContinue'	
	Settings.Compiler.ForceConformanceInForLoopScope 						= 'true'
	Settings.Compiler.IntrinsicFunctions[ProjectType!='Exe']					= 'true'			# some settings are as I found them in the old project generator, I suspect such exe / lib variations might not actually have been intentional ( somebody please review )
	Settings.Compiler.IntrinsicFunctions[ProjectType=='Exe']					= 'false'	
	Settings.Compiler.MinimalRebuild[Name=='ToolDebug|Win32']					= 'true'	
	Settings.Compiler.OmitFramePointers[Name=='ToolBeta|Win32'] 					= 'false'
	Settings.Compiler.OptimizationLevel[Platform=='PS3']						= 'Levels'
	Settings.Compiler.OptimizationLevel[Name=='Debug|PS3'] 						= 'Leveld'			# note name here OptimizationLevel NOT Optimization
	Settings.Compiler.OptimizationLevel[Platform=='ORBIS'] 						= 'Level3'			# note name here OptimizationLevel NOT Optimization
	Settings.Compiler.OptimizationLevel[Name=='Beta|ORBIS'||Name=='BankRelease|ORBIS'] 		= 'Level2'			# note name here OptimizationLevel NOT Optimization
	Settings.Compiler.OptimizationLevel[Name=='Debug|ORBIS'] 					= 'Level0'			# note name here OptimizationLevel NOT Optimization
	Settings.Compiler.PrecompiledHeader								= 'NotUsing'
	Settings.Compiler.PrecompiledHeaderFile								= ''
	Settings.Compiler.ProgramDataBaseFileName							= '$(IntDir)$(TargetName).pdb'
	Settings.Compiler.TrapIntegerDividesOptimization[ProjectType!='Exe'&&Platform=='Xbox 360'] 	= 'true'			# some settings are as I found them in the old project generator, I suspect such exe / lib variations might not actually have been intentional ( somebody please review )
	Settings.Compiler.TrapIntegerDividesOptimization[ProjectType=='Exe'&&Platform=='Xbox 360'] 	= 'false'
	Settings.Compiler.FunctionLevelLink[ProjectType=='Exe'&&Config=='Debug']			= 'true'			# copied from VS2008 settings - is this what is desired?
	Settings.Compiler.FunctionLevelLink[ProjectType!='Exe'&&Config!='Debug']			= 'true'
	Settings.Compiler.WarningLevel														= 'Level4'			# Off, Level1, Level2, Level3, Level4
	Settings.Compiler.OpenMPSupport	[Platform=='Win32'||Platform=='x64']				= 'true'

	# Overrides for everything except debug win32 builds.
	Condition [Config!='Debug'&&Config!='Debug DX11'&&Config!='RscDebug'&&Config!='ToolDebug'&&Platform!='ORBIS'&&Platform!='Xbox 360'&&Platform!='PS3'&&Platform!='Durango'] {
		Settings.Compiler.OmitFramePointers 				= 'true'
		Settings.Compiler.EnableFiberSafeOptimizations		= 'true'
		Settings.Compiler.FloatingPointModel				= 'Fast'
		Settings.Compiler.IntrinsicFunctions				= 'true'
		Settings.Compiler.Optimization						= 'MaxSpeed'
		Settings.Compiler.FavourSpeed						= 'true'
	}
	Condition [Platform=='Win32'] {
		Settings.Compiler.EnableEnhancedInstructionSet	    = 'StreamingSIMDExtensions2'
	}
	Settings.Compiler.EnableEnhancedInstructionSet[Platform=='Durango'] = 'AdvancedVectorExtensions'

	#-------------------------------------------------------------------------------------------
	# Librarian ( ExporterRules )
	#-------------------------------------------------------------------------------------------
	Settings.Librarian.ModuleDefinitionFile[Platform=='PS3'&&ProjectType!='Exe'] = '3'

	Condition [ProjectType=='Exe'] {		
		Settings.Linker.MapFilename 				= '$(TargetDir)$(TargetName).map'
		Settings.Linker.OutputFilename 				= '$(TargetPath)'
		Settings.Linker.EnableCOMDATFolding			= 'true'
		Settings.Linker.OptimizeReferences			= 'true'
		Settings.Linker.LargeAddressAware[Platform=='Win32'] 	= 'true'
	}
		
	#-------------------------------------------------------------------------------------------
	# Build Events ( ExporterRules )
	#-------------------------------------------------------------------------------------------
	Condition [ProjectType=='Exe'&&Platform=='PS3'] {		

		Settings.BuildEvents.PostBuild.Description 	= 'Create Symbol table, compress.'
		Settings.BuildEvents.PostBuild.Command 		<< '<Scratch.Ps3Bin> -dsy -c "$(TargetPath)" &gt; "$(TargetDir)$(TargetName).sym" &amp;&amp;'
		Settings.BuildEvents.PostBuild.Command 		<< '<Scratch.CompressMap> "$(TargetDir)$(TargetName).sym"'					
	}
		
	Condition [ProjectType=='Exe'&&Platform=='Xbox 360'] {			
		Settings.BuildEvents.PostBuild.Description	= 'Check 32Mb Text segment. Compress map. Deploy .cmp.'
		Settings.BuildEvents.PostBuild.Command 		<< 'ruby <Scratch.DumpBin> "dumpbin.exe $(TargetPath)" _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< 'if errorlevel == 0 goto BuildEventOK _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< 'echo dumpbin for $(ProjectName) FAILED _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< 'exit 1 _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< ':BuildEventOK _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< 'echo dumpbin for $(ProjectName) OK _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< '<Scratch.CompressMap> "$(TargetDir)$(TargetName).map" _NEWLINE_'
		Settings.BuildEvents.PostBuild.Command 		<< '<Scratch.XbeCopy> /nologo "$(TargetDir)$(TargetName).cmp" $(RemoteRoot)\$(TargetName).cmp'
	}

	Condition [ProjectType=='Exe'&&Platform=='Durango'] {			
		Settings.BuildEvents.PostBuild.Description	= 'Building Symbol file.'
		Settings.BuildEvents.PostBuild.Command		<< 'copy /Y $(TargetPath) $(RS_BUILDBRANCH)\$(ProjectName)_<Scratch.Arch>_<Config>.exe _NEWLINE_' 
 		Settings.BuildEvents.PostBuild.Command		<< 'copy /Y "$(TargetDir)$(TargetName).map" "$(RS_BUILDBRANCH)\$(ProjectName)_<Scratch.Arch>_<Config>.map" _NEWLINE_' 
 		Settings.BuildEvents.PostBuild.Command		<< 'REM $(RS_TOOLSROOT)\bin\coding\compressmap "$(RS_BUILDBRANCH)\$(ProjectName)_<Scratch.Arch>_<Config>.map"'
	}
	
	Condition [ProjectType=='Exe'&&Platform=='Win32'] {		
		Settings.BuildEvents.PostBuild.Description 	= 'Compress map.'
		Settings.BuildEvents.PostBuild.Command 		<< '<Scratch.CompressMap> "$(TargetDir)$(TargetName).map"' 
		Settings.BuildEvents.PreLink.Description 	  = 'Compile curl library...'
		Settings.BuildEvents.PreLink.Command 		<< '"$(WINDIR)\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe" "$(RAGE_DIR)\3rdparty\curl-7.28.0\curl-7.28.0\curl-7.28.0.sln" /p:PlatformToolset=v90 /p:Configuration=Release /p:Platform="Win32"' 
	}
}
