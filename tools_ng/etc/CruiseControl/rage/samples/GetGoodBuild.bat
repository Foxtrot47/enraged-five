REM Get latest ps3 sdk
p4 sync //ps3sdk/dev/usr/local/...#head

REM Kill rag and systrayrfs so you can get the latest
powershell  -NonInteractive -NoLogo  -command .\killSys.ps1

REM Log all changes since last update

powershell  -NonInteractive -NoLogo -command .\CreateChangeList.ps1 > updateChanges.txt
REM Get the latest good version of code
p4 sync //rage/dev/rage/...@RageGoodBuild > updateFiles.txt

REM Resolve and merge automatically any code
p4 resolve -am

REM If any conflicts run p4v
if %ERRORLEVEL% NEQ 0 p4v.exe

REM now get the assets
p4 sync //rageassets/...#head


REM run rag and systrayrfs again
powershell  -NonInteractive -NoLogo  -command .\setupSys.ps1