<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cb="urn:ccnet.config.builder">
  <xsl:output method="xml" encoding="ISO-8859-1" omit-xml-declaration="yes" indent="yes"/> 
     
  <xsl:template match="/">

    <xsl:comment> Base CruiseControl configuration file for a studio as built by the tools installer </xsl:comment>
    <xsl:comment> This file has been auto built by a tools install, there are hand made versions kicking aorund and this is not one of those.</xsl:comment>
    <xsl:comment> DO NOT SUBMIT </xsl:comment>

      <xsl:element name="cb:config-template" xmlns:cb="urn:ccnet.config.builder">
        
        <!-- Rage -->
        <xsl:element name="cb:define">
            <xsl:attribute name="ragep4server" >
              <xsl:value-of select="substring-before(local/scm/rage/@server,':')"/>
            </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="ragep4port" >
            <xsl:value-of select="local/scm/rage/@server"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="ragep4client" >
            <xsl:value-of select="local/scm/rage/@workspace"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="ragep4user" >
            <xsl:value-of select="local/scm/rage/@username"/>
          </xsl:attribute>
        </xsl:element>

        <!-- game -->
        <xsl:element name="cb:define">
          <xsl:attribute name="gamep4server" >
            <xsl:value-of select="substring-before(local/scm/build/@server,':')"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="gamep4port" >
            <xsl:value-of select="local/scm/build/@server"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="gamep4client" >
            <xsl:value-of select="local/scm/build/@workspace"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="gamep4user" >
            <xsl:value-of select="local/scm/build/@username"/>
          </xsl:attribute>
        </xsl:element>

        <!-- tools -->
        <xsl:element name="cb:define">
          <xsl:attribute name="toolp4server" >
            <xsl:value-of select="substring-before(local/scm/build/@server,':')"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="toolp4port" >
            <xsl:value-of select="local/scm/build/@server"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="toolp4client" >
            <xsl:value-of select="local/scm/build/@workspace"/>
          </xsl:attribute>
        </xsl:element>

        <xsl:element name="cb:define">
          <xsl:attribute name="toolp4user" >
            <xsl:value-of select="local/scm/build/@username"/>
          </xsl:attribute>
        </xsl:element>

        <!-- mail -->
        <xsl:element name="cb:define">
          <xsl:attribute name="mail_hostUsername" >
            <xsl:value-of select="local/user/@username"/>
          </xsl:attribute>
        </xsl:element>
		
	<xsl:element name="cb:define">
          <xsl:attribute name="studio_networkdrive" >
            <xsl:value-of select="local/studio/@networkdrive"/>
          </xsl:attribute>
        </xsl:element>
		
	<xsl:element name="cb:define">
          <xsl:attribute name="studio_timezoneoffset" >
            <xsl:value-of select="local/studio/@timezoneoffset"/>
          </xsl:attribute>
        </xsl:element>

      </xsl:element>
   
  </xsl:template>

</xsl:stylesheet>

<!--
Eg. source 

<local version='17'>
  <user flags='65535' username='derek' emailaddress='derek.ward@rockstarnorth.com'/>
  <scm>
    <build workspace='EDIW-DWARD-001' username='derek' server='rsgedip4d1:1666'/>
    <rage workspace='EDIW-DWARD-001' username='derek' server='rsgedip4proxy01:1667'/>
  </scm>
  <projects>
    <project name='gta5' enabled='true'/>
  </projects>
  <logging stdout='false' level='2' trace='false' generate_html='false'>
    <logmails enabled='true'/>
  </logging>
  <applications>
    <app name='xge' enabled='true'/>
  </applications>
</local>

Eg. dest

<cb:config-template xmlns:cb="urn:ccnet.config.builder">

	<cb:define ragep4server  	="rsgedip4proxy01"/>
	<cb:define ragep4port    	="$(ragep4server):1667"/>
	<cb:define ragep4client      	="$(COMPUTERNAME)"/>
	<cb:define ragep4user		="buildernorth"/>

	<cb:define gamep4server		="rsgedip4d1"/>
	<cb:define gamep4port      	="$(gamep4server):1666"/>
	<cb:define gamep4client      	="$(COMPUTERNAME)"/>
	<cb:define gamep4user		="buildernorth"/>

	<cb:define toolp4server		="rsgedip4d1"/>
	<cb:define toolp4port        	="$(toolp4server):1666"/>
	<cb:define toolp4client		="$(COMPUTERNAME)"/>
	<cb:define toolp4user		="buildernorth"/>

	<cb:define mail_hostUsername 	="buildernorth"/>

</cb:config-template>
-->
