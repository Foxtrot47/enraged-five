<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cb="urn:ccnet.config.builder">
  <xsl:output method="text" encoding="ISO-8859-1" omit-xml-declaration="yes" indent="yes"/>
  <xsl:template match="/ArrayOfModification/Modification">        [colourise=black]INFO_MSG: XSL Transform: <xsl:value-of select="Type"/>: <xsl:value-of select="FolderName"/><xsl:value-of select="FileName"/>
  </xsl:template>
</xsl:stylesheet>
