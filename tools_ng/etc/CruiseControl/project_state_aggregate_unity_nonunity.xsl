<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- derek.ward@rockstarnorth.com 15-03-2011 -->
<!-- Transforms project state aggregate into an html doc of sorted changelists -->

<xsl:output method="html" encoding="UTF-8"/>


 <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




<!-- entry -->
<xsl:template match="/project_state_aggregate">

<xsl:variable name="run_time" select="@run_time"/>
<xsl:variable name="common" select="@common"/>
<xsl:variable name="poll_interval" select="@poll_interval"/>
<xsl:variable name="auto_refresh" select="@auto_refresh"/>
<xsl:variable name="title" select="@title"/>
<xsl:variable name="webserver" select="@webserver"/>
<xsl:variable name="prev_report" select="@prev_report"/>
<xsl:variable name="next_report" select="@next_report"/>

<html>

<!-- inline css for convenience -->
<STYLE TYPE="text/css">
	TD{font-family: Arial; font-size: 9pt; vertical-align: center; }
	TD.comment{font-family: Arial; font-size: 7pt;}
	
 	.PlainLink { TEXT-DECORATION: none; }
	.PlainLink:hover { TEXT-DECORATION: underline; }
	
	TR.top_header{background-color:#FFFFFF; font-size: 24pt;}	
	TR.code_test_pipeline{background-color:#7FFFD2; font-size: 15pt;}
	TR.codebuilder_gta5_dev{background-color:#BBBBFF; font-size: 15pt;}
	TR.scriptbuilder_gta5_dev{background-color:#FFDD00; font-size: 15pt;}
	TR.assetbuilder_gta5_dev{background-color:#F8B8FF; font-size: 15pt;}
	TR.toolbuilder_gta5_dev{background-color:#58FA58; font-size: 15pt;}
	TR.codebuilder_gta5_dev_ng{background-color:#BBBBFF; font-size: 15pt;}
	TR.scriptbuilder_gta5_dev_ng{background-color:#FFDD00; font-size: 15pt;}
	TR.assetbuilder_gta5_dev_ng{background-color:#F8B8FF; font-size: 15pt;}
	TR.toolbuilder_gta5_dev_ng{background-color:#58FA58; font-size: 15pt;}
	TR.header{background-color:#BBBBBB; }	
	TH.header2010{background-color:#99CCFF; }	
	TH.header2012{background-color:#FFCC99; }	
	TH.nonunityheader{background-color:#C2D6D6; }	
	TH.header{background-color:#BBBBBB; }	
	
	
	TH.unitydivide { border-style:none; background-color:#BBBBBB;}	
	
	TH.Failure{background-color:#F6CECE;}
	TH.Success{background-color:#A9F5A9;}		
	
	TR.Failure{background-color:#F6CECE;}
	TR.Exception{background-color:#F6CECE;}
	TR.Success{background-color:#A9F5A9;}
	TR.SeeLog{background-color:#FF9933;}
	TR.seelog{background-color:#FF9933;}
	
	TR.Pending{background-color:#AAFFFF; color: #BBBBBB;}
	TR.pending{background-color:#FFAAFF; color: #BBBBBB;}
	TR.running{background-color:#FFFFAA;}
	TR.pending{background-color:#FFFFFF;}

	TR.no_new_exe{background-color:#E0E0E0;}
	TR.no_smoke_start{background-color:#E0E0E0;}
	TR.no_smoke_finish{background-color:#E0E0E0;}
	TR.no_new_capture{background-color:#E0E0E0;}

	TD.no_new_exe0{background-color:#E0E0E0;}
	TD.no_smoke_start0{background-color:#E0E0E0;}
	TD.no_smoke_finish0{background-color:#E0E0E0;}
	TD.no_new_capture0{background-color:#E0E0E0;}

	TD.no_new_exe1{background-color:#EEEEEE;}
	TD.no_smoke_start1{background-color:#EEEEEE;}
	TD.no_smoke_finish1{background-color:#EEEEEE;}
	TD.no_new_capture1{background-color:#EEEEEE;}

	TD.Failure{background-color:#F6CECE;}
	TD.Exception{background-color:#F6CECE;}
	TD.Success{background-color:#A9F5A9;}
	TD.SeeLog{background-color:#FF9933;}
	
	TD.running{background-color:#FFFFAA;}
	TD.pending{background-color:#FFFFFF;}	

	TD.Failure0{background-color:#F6CECE;}
	TD.Exception0{background-color:#F6CECE;}
	TD.Success0{background-color:#A9F5A9;}
	TD.SeeLog0{background-color:#FF9933;}
	
	TD.running0{background-color:#FFFFAA;}
	TD.pending0{background-color:#FFFFFF;}	



	TD.Failure1{background-color:#F6DEDE;}
	TD.Exception1{background-color:#F6DEDE;}
	TD.Success1{background-color:#C9F5C9;}
	TD.SeeLog1{background-color:#FFCC99;}
	
	TD.running1{background-color:#FFFFAA;}
	TD.pending1{background-color:#FFFFFF;}	

	a img { position: absolute; visibility: hidden }
	a.PlainLink:hover img { visibility: visible; align: bottom; COLOR: #000000;}
	a.PlainLink:link {COLOR: #000000;}
	a.PlainLink:visited {COLOR: #000000;}
	a.PlainLink:active {COLOR: #000000;}	
</STYLE>

<head>
<xsl:if test="$auto_refresh&gt;0">
	<meta http-equiv="refresh">
		<xsl:attribute name="content"><xsl:value-of select="$auto_refresh"/></xsl:attribute>
	</meta>
	
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>	
</xsl:if>
<title><xsl:value-of select="$title"/> : <xsl:value-of select="$common"/> - Cruise Control - Custom Changelist Report - Auto Refresh <xsl:value-of select="$auto_refresh"/> - Regen <xsl:value-of select="$poll_interval"/></title>
</head>
<body>

<xsl:variable name="projects_unity" select="projects/project[contains(@category,'unity')]"/>
<xsl:variable name="projects_nonunity" select="projects/project[not(contains(@category,'unity'))]"/>

<xsl:variable name="num_projects" select="count(projects/project)"/>
<xsl:variable name="num_projects_unity" select="count($projects_unity)"/>
<xsl:variable name="num_projects_nonunity" select="count($projects_nonunity)"/>

<table width="100%" border="0">
  <THEAD>
  	<TR class="top_header">
  		<TH align="left"><img src="Rockstar_North_Logo.jpg" alt="RSN logo" width="59" height="72"/></TH>
  		

		<TH align="left">  		
			<a class="PlainLink">
				<xsl:attribute name="title">Wiki</xsl:attribute>
				<xsl:attribute name="href">
					https://devstar.rockstargames.com/wiki/index.php/Custom_Changelist_Reports
				</xsl:attribute>  		  				
  				<xsl:value-of select="$title"/> : Cruise Control - Custom Changelist Report				
  			</a>
  		</TH>
  	</TR>
  </THEAD>
 </table>


<table width="100%" border="0">
 	<TR class="top_header">
		<td>
		<FORM>
			<xsl:attribute name="action">http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$prev_report"/>.html</xsl:attribute> 
			<xsl:attribute name="style">display:inline;</xsl:attribute>
			<INPUT> 
				<xsl:attribute name="type">submit</xsl:attribute>
				<xsl:attribute name="value">Previous Report</xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="$prev_report"/></xsl:attribute>
			</INPUT>
			<a> 
				<xsl:attribute name="href">
					http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$prev_report"/>.html
				</xsl:attribute>   	
			</a>
		 </FORM>

		 <FORM>
			<xsl:attribute name="action">http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$next_report"/>.html</xsl:attribute> 
			<xsl:attribute name="style">display:inline;</xsl:attribute>
			<INPUT> 
				<xsl:attribute name="type">submit</xsl:attribute>
				<xsl:attribute name="value">Next Report</xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="$next_report"/></xsl:attribute>
			</INPUT>
			<a> 
				<xsl:attribute name="href">
					http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$next_report"/>.html
				</xsl:attribute>   	
			</a>
		 </FORM>
		</td>
	</TR>
 </table>

 
 <table width="100%" border="1">
   <THEAD>
	<TR>
		<xsl:attribute name="class"><xsl:value-of select="$common"/></xsl:attribute>
		<!--<xsl:for-each select="projects/project"><h2><xsl:value-of select="@short_name"/></h2></xsl:for-each>-->
		<TH><xsl:value-of select="$common"/></TH>
		<TH>Updated @ <xsl:value-of select="$run_time"/></TH>
		<TH>Auto Refresh <xsl:value-of select="$auto_refresh"/>s / Update Rate <xsl:value-of select="$poll_interval"/>s</TH>
	</TR>
  </THEAD>
</table>

<table width="100%" border="1">
  <THEAD>	
	   <xsl:choose>
	   	<xsl:when test="$num_projects&gt;1">
			<TR class="header">	   
			   <TH rowspan="3" width="5%"><B>Change</B></TH>
			   <TH rowspan="3" width="10%"><B>User</B></TH>
			   <TH rowspan="3" width="10%"><B>Buddy</B></TH>
			   		   	   
			   <xsl:if test="$num_projects_unity&gt;0 and $num_projects_nonunity&gt;0 ">				   				
				
				<xsl:variable name="unity_failures" select="count($projects_unity[@status='Failure'])"/>
				<xsl:variable name="nonunity_failures" select="count($projects_nonunity[@status='Failure'])"/>
				<xsl:variable name="unity_ok" select="($num_projects_unity - $unity_failures)"/>
				<xsl:variable name="nonunity_ok" select="($num_projects_nonunity - $nonunity_failures)"/>
								
				<xsl:if test="$unity_failures&gt;0">
					<TH class="Failure" width="5%">
						<xsl:attribute name="colspan"><xsl:value-of select="$num_projects_unity"/></xsl:attribute>
						<B>Unity : <xsl:value-of select="$unity_failures"/> Broken</B>							
					</TH>
				</xsl:if>
					
				<xsl:if test="$unity_failures=0">
					<TH class="Success" width="5%">
						<xsl:attribute name="colspan"><xsl:value-of select="$num_projects_unity"/></xsl:attribute>
						<B>Unity : <xsl:value-of select="$unity_ok"/> OK</B>	
					</TH>
				</xsl:if>					
				   
				<TH class="unitydivide" width="3%"/>
				
				
				<xsl:if test="$nonunity_failures &gt; 0">
					<TH class="Failure" width="5%">
						<xsl:attribute name="colspan"><xsl:value-of select="$num_projects_nonunity"/></xsl:attribute>
						<B>Non Unity : <xsl:value-of select="$nonunity_failures"/> Broken</B>							 
					</TH>
				</xsl:if>

				<xsl:if test="$nonunity_failures=0">
					<TH class="Success" width="5%">
						<xsl:attribute name="colspan"><xsl:value-of select="$num_projects_nonunity"/></xsl:attribute>
						<B>Non Unity : <xsl:value-of select="$nonunity_ok"/> OK</B>							
					</TH>
				</xsl:if>
							   
			   </xsl:if>			   			   		   			   
			   <TH rowspan="3" width="10%"><B>Modified</B></TH>
			   <TH rowspan="3" width="10%"><B>Last Build Completed</B></TH>			   
			   <TH rowspan="3" width="15%"><B>Comment</B></TH>
			</TR>
			<TR class="header">
				<xsl:for-each select="$projects_unity">
					<xsl:variable name="prev_category" select="preceding::*[1]/@category"/>
					<xsl:variable name="next_category" select="following::*[1]/@category"/>
					<xsl:variable name="this_category" select="@category"/>
					
					<xsl:if test="not($this_category=$prev_category)">

						<xsl:if test="contains($this_category, '2012')">
							<TH class="header2012" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>
					
						<xsl:if test="contains($this_category, '2010')">
							<TH class="header2010" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>

						<xsl:if test="not(contains($this_category, '2010')) and not(contains($this_category, '2012'))">
							<TH class="header" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>					
										
					</xsl:if>					
				</xsl:for-each>
				
				<TH class="unitydivide" width="3%"/>
				
				<xsl:for-each select="$projects_nonunity">
					<xsl:variable name="prev_category" select="preceding::*[1]/@category"/>
					<xsl:variable name="next_category" select="following::*[1]/@category"/>
					<xsl:variable name="this_category" select="@category"/>
					
					<xsl:if test="not($this_category=$prev_category)">

						<xsl:if test="contains($this_category, '2012')">
							<TH class="header2012" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>
					
						<xsl:if test="contains($this_category, '2010')">
							<TH class="header2010" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>

						<xsl:if test="not(contains($this_category, '2010')) and not(contains($this_category, '2012'))">
							<TH class="header" width="5%">
								<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
								<B><xsl:value-of select="$this_category"/></B>												
							</TH>
						</xsl:if>					
										
					</xsl:if>
				</xsl:for-each>				
			</TR>	 
			<TR class="header">
				<xsl:for-each select="$projects_unity">
					<xsl:variable name="project_name" select="@project_name"/>
					<xsl:variable name="project_url" select="@project_url"/>							
					<xsl:variable name="this_category" select="@category"/>
					
					<xsl:if test="contains($this_category, '2010')">
						<TH class="header2010" width="5%">
							<a class="PlainLink">
								<xsl:attribute name="href">
									<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
								</xsl:attribute>
								<B><xsl:value-of select="@sub_category"/></B>
							</a>							
						</TH>
					</xsl:if>
					
					<xsl:if test="not(contains($this_category, '2010'))">
						<TH class="header" width="5%">
							<a class="PlainLink">
								<xsl:attribute name="href">
									<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
								</xsl:attribute>
								<B><xsl:value-of select="@sub_category"/></B>
							</a>							
						</TH>
					</xsl:if>	
					
				</xsl:for-each>
				
				<TH class="unitydivide" width="3%"/>
				
				<xsl:for-each select="$projects_nonunity">
					<xsl:variable name="project_name" select="@project_name"/>
					<xsl:variable name="project_url" select="@project_url"/>							
					<xsl:variable name="this_category" select="@category"/>

					<xsl:if test="contains($this_category, '2012')">
						<TH class="header2010" width="5%">
							<a class="PlainLink">
								<xsl:attribute name="href">
									<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
								</xsl:attribute>
								<B><xsl:value-of select="@sub_category"/></B>
							</a>							
						</TH>
					</xsl:if>
					
					<xsl:if test="contains($this_category, '2010')">
						<TH class="header2010" width="5%">
							<a class="PlainLink">
								<xsl:attribute name="href">
									<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
								</xsl:attribute>
								<B><xsl:value-of select="@sub_category"/></B>
							</a>							
						</TH>
					</xsl:if>
					
					<xsl:if test="not(contains($this_category, '2010')) and not(contains($this_category, '2012'))">
						<TH class="header" width="5%">
							<a class="PlainLink">
								<xsl:attribute name="href">
									<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
								</xsl:attribute>
								<B><xsl:value-of select="@sub_category"/></B>
							</a>							
						</TH>
					</xsl:if>	
					
				</xsl:for-each>				
				
			</TR>	
		</xsl:when>
		<xsl:otherwise>			
			<xsl:variable name="project_url" select="projects/project/@project_url"/>							
		
			<TR class="header">	   
			   <TH rowspan="1" width="5%"><B>Change</B></TH>
			   <TH rowspan="1" width="10%"><B>User</B></TH>
			   <TH rowspan="1" width="10%"><B>Buddy</B></TH>
			   <TH rowspan="1" width="5%">
				<a class="PlainLink">
					<xsl:attribute name="href">
						<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
					</xsl:attribute>			   
			   		<B>Status</B>	   	   
			   	</a>
			   </TH>
			   <TH rowspan="1" width="10%"><B>Modified</B></TH>
			   <TH rowspan="1" width="10%"><B>Last Build Completed</B></TH>
			   <TH rowspan="1" width="15%"><B>Comment</B></TH>
			</TR>
		</xsl:otherwise>			
	</xsl:choose>
  </THEAD> 
  <TBODY>	

	<xsl:variable name="limit_rows">300</xsl:variable>
  
	<xsl:for-each select="changes/change">			
		<xsl:sort select="@sort_date" order="descending"/>
		<xsl:if test="not(position() > $limit_rows)">
			<TR>	
				<xsl:variable name="change_number" select="@change_number"/>			
				<xsl:variable name="full_comment" select="@comment"/>
				<xsl:variable name="comment" select="substring(@comment, 1, 100)"/>
				<xsl:variable name="user" select="@user"/>
				<xsl:variable name="buddy" select="@buddy"/>
				<xsl:variable name="url" select="@url"/>
				<xsl:variable name="modified_date" select="@modified_date"/>
				<xsl:variable name="last_build_date" select="@last_build_date"/>
				<xsl:variable name="turnaround" select="@turnaround"/>
				
				<xsl:variable name="user_no_dots">
				    <xsl:call-template name="string-replace-all">
				      <xsl:with-param name="text" select="$user" />
				      <xsl:with-param name="replace" select="'.'" />
				      <xsl:with-param name="by" select="'_'" />
				    </xsl:call-template>
				</xsl:variable>

				<TD align="right">
				<a>
					<xsl:attribute name="title">p4Web</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="$url"/>
					</xsl:attribute>
					<xsl:value-of select="$change_number"/>
				</a>			
				</TD>
				<TD align="left">
					<a class="PlainLink" onmouseover="">
						<!--<xsl:attribute name="href">mailto:<xsl:value-of select="$user"/>@rockstarnorth.com?subject=Re. Changelist <xsl:value-of select="$change_number"/></xsl:attribute>-->
						<xsl:attribute name="href">#</xsl:attribute>
						<img width="120">
							<xsl:attribute name="src">users/<xsl:value-of select="$user"/>.jpg</xsl:attribute>			
						</img>
						
						<!--<img>
							<xsl:attribute name="src">http://intranet.rockstarnorth.com/images/people/<xsl:value-of select="$user_no_dots"/>.jpg</xsl:attribute>			
						</img>
						-->

						<xsl:value-of select="$user"/>
					</a>
				</TD>

				<TD align="left"><xsl:value-of select="$buddy"/></TD>
				<xsl:for-each select="project">
					<xsl:sort select="@order" data-type="number" order="ascending"/>
					
					<xsl:if test="contains(@project_name, 'unity')">
						<xsl:variable name="log_name" select="@log_name"/>				
						<xsl:variable name="status" select="@status"/>
						<xsl:variable name="details" select="@details"/>
						<xsl:variable name="swizzler" select="@swizzler"/>				
						<xsl:variable name="project_name" select="@project_name"/>
						<xsl:variable name="build_duration" select="@build_duration"/>				
						<xsl:variable name="build_date" select="@build_date"/>					
						<xsl:variable name="project_url" select="//project_state_aggregate/projects/project[@name=$project_name]/@project_url"/>

						<TD align="center">

							<xsl:choose>
								<xsl:when test="not($details='') and (string-length($details) &gt; 1)">
									<!--<xsl:attribute name="class"><xsl:value-of select="$details"/><xsl:value-of select="$swizzler"/></xsl:attribute>-->
									<xsl:attribute name="class">SeeLog<xsl:value-of select="$swizzler"/></xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class"><xsl:value-of select="$status"/><xsl:value-of select="$swizzler"/></xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="$status='unknown' or $status='pending' or $status='Pending' or $status='running'">										
									<xsl:value-of select="$status"/>
								</xsl:when>
								<xsl:otherwise>						



										<a>
											<xsl:attribute name="title">Completed: <xsl:value-of select="$build_date"/>&#10;&#13;Duration: <xsl:value-of select="$build_duration"/>
												<!--<xsl:if test="not($details='')">&#10;Details:&#10;<xsl:value-of select="$details"/></xsl:if>-->
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="$project_url"/>build/<xsl:value-of select="$log_name"/>/ViewBuildReport.aspx
											</xsl:attribute>

											<xsl:choose>
												<xsl:when test="not($details='')">SeeLog</xsl:when>
												<!-- <xsl:value-of select="$details"/> -->												
												<xsl:otherwise>
													<xsl:value-of select="$status"/>
												</xsl:otherwise>
											</xsl:choose>
										</a>

								</xsl:otherwise>
							</xsl:choose>
						</TD>
					</xsl:if>
												
					<xsl:if test="position()=$num_projects_unity">
						<TH class="unitydivide" width="3%"/>
					</xsl:if>							
												
					<xsl:if test="not(contains(@project_name, 'unity'))">
												
						<xsl:variable name="log_name" select="@log_name"/>				
						<xsl:variable name="status" select="@status"/>
						<xsl:variable name="details" select="@details"/>
						<xsl:variable name="swizzler" select="@swizzler"/>				
						<xsl:variable name="project_name" select="@project_name"/>
						<xsl:variable name="build_duration" select="@build_duration"/>				
						<xsl:variable name="build_date" select="@build_date"/>					
						<xsl:variable name="project_url" select="//project_state_aggregate/projects/project[@name=$project_name]/@project_url"/>

						<TD align="center">

							<xsl:choose>
								<xsl:when test="not($details='') and (string-length($details) &gt; 1)">
									<!--<xsl:attribute name="class"><xsl:value-of select="$details"/><xsl:value-of select="$swizzler"/></xsl:attribute>-->
									<xsl:attribute name="class">SeeLog<xsl:value-of select="$swizzler"/></xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class"><xsl:value-of select="$status"/><xsl:value-of select="$swizzler"/></xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="$status='unknown' or $status='pending' or $status='Pending' or $status='running'">										
									<xsl:value-of select="$status"/>
								</xsl:when>
								<xsl:otherwise>						



										<a>
											<xsl:attribute name="title">Completed: <xsl:value-of select="$build_date"/>&#10;&#13;Duration: <xsl:value-of select="$build_duration"/>
												<!--<xsl:if test="not($details='')">&#10;Details:&#10;<xsl:value-of select="$details"/></xsl:if>-->
											</xsl:attribute>
											<xsl:attribute name="href">
												<xsl:value-of select="$project_url"/>build/<xsl:value-of select="$log_name"/>/ViewBuildReport.aspx
											</xsl:attribute>

											<xsl:choose>
												<xsl:when test="not($details='')">SeeLog</xsl:when>
												<!-- <xsl:value-of select="$details"/> -->												
												<xsl:otherwise>
													<xsl:value-of select="$status"/>
												</xsl:otherwise>
											</xsl:choose>
										</a>

								</xsl:otherwise>
							</xsl:choose>
						</TD>
					</xsl:if>					
							
				</xsl:for-each>	
											
				<TD align="center"><xsl:value-of select="$modified_date"/></TD>
				<TD align="center">
					<a>
						<xsl:attribute name="title">Turnaround: <xsl:value-of select="$turnaround"/></xsl:attribute>					
						<xsl:value-of select="$last_build_date"/>
					</a>					
				</TD>
				<TD align="left">
					<a>
						<xsl:attribute name="title"><xsl:value-of select="$full_comment"/></xsl:attribute>					
						<xsl:value-of select="$comment"/>...
					</a>				
				</TD>		
			</TR>
		</xsl:if>
	</xsl:for-each>
  </TBODY>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

