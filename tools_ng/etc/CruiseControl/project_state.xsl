<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- derek.ward@rockstarnorth.com 15-03-2011 -->
<!-- Transforms project state into an html doc of sorted changelists -->

<!-- for refernce - was going to do comment with javascript - some problems with this - later.
        <a href="#" onclick="alert('$projectGridRow.Name\r\n\r\n $projectGridRow.CurrentMessage\r\n'); return false;">
          <img style="border:0; padding:0; margin:0;" src="${localBreakerJpg}" onerror="this.onerror=null;this.src='${breakerJpg}';" width="$imagewidth" height="$imageheight" alt="${premsg} : ${msg}"/>
        </a>

      <a href="http://rsgedip4d2:8080/@md=d&cd=//&c=0Td@/${projectGridRow.LastChangelist}?ac=10">
        $projectGridRow.LastChangelist
      </a>
      #else
      <a href="http://rsgsanp4web.rockstar.t2.corp:8080/@md=d&cd=//&c=7FM@/${projectGridRow.LastChangelist}?ac=10">
        $projectGridRow.LastChangelist
      </a>
-->

<xsl:output method="html" encoding="UTF-8"/>

<!-- change template - a row in the table -->
<xsl:template name="change">
	<xsl:param name="build_id"/>
	<xsl:param name="number"/>
	<xsl:param name="status"/>
	<xsl:param name="build_date"/>
	<xsl:param name="build_duration"/>
	<xsl:param name="comment"/>
	<xsl:param name="user"/>
	<xsl:param name="buddy"/>
	<xsl:param name="url"/>

	<TR>
		<xsl:attribute name="class"><xsl:value-of select="$status"/></xsl:attribute>
		<TD width="2%"><xsl:value-of select="$build_id" /></TD>  	
		<TD width="5%">
			<a>
				<xsl:attribute name="href">
					<xsl:value-of select="$url"/>
				</xsl:attribute>
				<xsl:value-of select="$number"/>
			</a>
		</TD>  			   
		<TD width="10%"><xsl:value-of select="$user" /></TD>
		<TD width="10%"><xsl:value-of select="$buddy" /></TD>
		<TD width="10%"><xsl:value-of select="$status" /></TD> 
		<TD class="comment" width="10%"><xsl:value-of select="$comment"/></TD>
		<xsl:choose>
			<xsl:when test="$status='pending'">
				<xsl:if  test="not($build_date='-')">
					<TD width="20%"><xsl:value-of select="$build_date"/> (modified)</TD>
				</xsl:if>
				<xsl:if  test="$build_date='-'">
					<TD width="20%"><xsl:value-of select="$build_date"/></TD>
				</xsl:if>
			</xsl:when>
			<xsl:when test="$status='running'">
				<xsl:if  test="not($build_date='-')">
					<TD width="20%"><xsl:value-of select="$build_date"/> (started)</TD>
				</xsl:if>
				<xsl:if  test="$build_date='-'">
					<TD width="20%"><xsl:value-of select="$build_date"/></TD>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<TD width="20%"><xsl:value-of select="$build_date"/> (built)</TD>
			</xsl:otherwise>
		</xsl:choose>
		<TD width="10%"><xsl:value-of select="$build_duration" /></TD>
	</TR>		
</xsl:template>


<!-- entry -->
<xsl:template match="/project_state">

<xsl:variable name="project_name" select="@project_name"/>
<xsl:variable name="run_time" select="@run_time"/>

<html>

<!-- inline css for convenience -->
<STYLE TYPE="text/css">
	TD{font-family: Arial; font-size: 8pt;}
	TD.comment{font-family: Arial; font-size: 7pt;}
	
	TR.header{background-color:#BBBBBB;}
	
	TR.Failure{background-color:#F6CECE;}
	TR.Exception{background-color:#F6CECE;}
	TR.Success{background-color:#A9F5A9;}
	
	TR.running{background-color:#FFFFAA;">
}
	TR.pending{background-color:#FFFFFF;}
</STYLE>

<head><title>Cruise Control Project Change Report : <xsl:value-of select="$project_name"/></title>
</head>
<body>
<h1 align="center"><u>Cruise Control Project Change Report</u></h1>
<h2><xsl:value-of select="$project_name"/></h2>
<h3>Updated @ <xsl:value-of select="$run_time"/></h3>
<table width="100%" border="1">
  <THEAD>
	<TR class="header">
	   <TD width="2%"><B>Build#</B></TD>
	   <TD width="5%"><B>Change</B></TD>
	   <TD width="10%"><B>User</B></TD>
	   <TD width="10%"><B>Buddy</B></TD>
 	   <TD width="10%"><B>Status</B></TD>
 	   <TD width="10%"><B>Comment</B></TD>
	   <TD width="20%"><B>Build/Modified Time</B></TD>
	   <TD width="10%"><B>Duration(hh:mm:ss)</B></TD>
	</TR>
  </THEAD> 
  <TBODY>
	<xsl:for-each select="work_units/work_unit">
		<xsl:sort select="@build_date" order="descending"/>
		
		<xsl:variable name="build_id" 		select="position()"/>
		<xsl:variable name="status" 		select="@status"/>
		<xsl:variable name="build_date" 	select="@build_date"/>		
		<xsl:variable name="build_duration" 	select="@build_duration"/>

		<xsl:if test="not(changes/change)">
			<!-- blank out fields -->
			<xsl:variable name="number">-</xsl:variable>
			<xsl:variable name="comment">-</xsl:variable>
			<xsl:variable name="user">-</xsl:variable>
			<xsl:variable name="buddy">-</xsl:variable>
			<xsl:variable name="url">-</xsl:variable>
			<xsl:variable name="no_build_date">-</xsl:variable>			
			<xsl:variable name="no_build_duration">-</xsl:variable>

			<xsl:call-template name="change">				
				<xsl:with-param name="build_id" select="$build_id"/>
				<xsl:with-param name="number" select="$number"/>
				<xsl:with-param name="status" select="$status"/>
				<xsl:with-param name="build_date" select="$no_build_date"/>
				<xsl:with-param name="build_duration" select="$no_build_duration"/>
				<xsl:with-param name="comment" select="$comment"/>
				<xsl:with-param name="user" select="$user"/>
				<xsl:with-param name="buddy" select="$buddy"/>
				<xsl:with-param name="url" select="$url"/>
			</xsl:call-template>							
		</xsl:if> 
		
		<xsl:for-each select="changes/change">	
			<xsl:sort select="@number" order="descending"/>

			<xsl:variable name="number" select="@number"/>
			<xsl:variable name="comment" select="substring(@comment, 1, 80)"/>
			<xsl:variable name="user" select="@user"/>
			<xsl:variable name="buddy" select="@buddy"/>
			<xsl:variable name="url" select="@url"/>
			<xsl:call-template name="change">				
				<xsl:with-param name="build_id" select="$build_id"/>
				<xsl:with-param name="number" select="$number"/>
				<xsl:with-param name="status" select="$status"/>
				<xsl:with-param name="build_date" select="$build_date"/>
				<xsl:with-param name="build_duration" select="$build_duration"/>
				<xsl:with-param name="comment" select="$comment"/>
				<xsl:with-param name="user" select="$user"/>
				<xsl:with-param name="buddy" select="$buddy"/>
				<xsl:with-param name="url" select="$url"/>
			</xsl:call-template>				
		</xsl:for-each>
	</xsl:for-each>
  </TBODY>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

