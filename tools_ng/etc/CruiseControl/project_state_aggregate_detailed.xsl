<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- derek.ward@rockstarnorth.com 15-03-2011 -->
<!-- Transforms project state aggregate into an html doc of sorted changelists -->

<xsl:output method="html" encoding="UTF-8"/>


 <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>




<!-- entry -->
<xsl:template match="/project_state_aggregate">

<xsl:variable name="run_time" select="@run_time"/>
<xsl:variable name="common" select="@common"/>
<xsl:variable name="poll_interval" select="@poll_interval"/>
<xsl:variable name="auto_refresh" select="@auto_refresh"/>
<xsl:variable name="title" select="@title"/>
<xsl:variable name="webserver" select="@webserver"/>
<xsl:variable name="prev_report" select="@prev_report"/>
<xsl:variable name="next_report" select="@next_report"/>

<html>

<!-- inline css for convenience -->
<STYLE TYPE="text/css">
	TD{font-family: Arial; font-size: 9pt; vertical-align: center; }
	TD.comment{font-family: Arial; font-size: 7pt;}
	
 	.PlainLink { TEXT-DECORATION: none; }
	.PlainLink:hover { TEXT-DECORATION: underline; }
	
	TR.top_header{background-color:#FFFFFF; font-size: 24pt;}	
	TR.code_test_pipeline{background-color:#7FFFD2; font-size: 15pt;}
	TR.codebuilder_gta5_dev{background-color:#BBBBFF; font-size: 15pt;}
	TR.scriptbuilder_gta5_dev{background-color:#FFDD00; font-size: 15pt;}
	TR.assetbuilder_gta5_dev{background-color:#F8B8FF; font-size: 15pt;}
	TR.toolbuilder_gta5_dev{background-color:#58FA58; font-size: 15pt;}
	
	TR.codebuilder_gta5_dev_ng{background-color:#BBBBFF; font-size: 15pt;}
	TR.scriptbuilder_gta5_dev_ng{background-color:#FFDD00; font-size: 15pt;}
	TR.assetbuilder_gta5_dev_ng{background-color:#F8B8FF; font-size: 15pt;}
	TR.toolbuilder_gta5_dev_ng{background-color:#58FA58; font-size: 15pt;}	
	
	TR.header{background-color:#BBBBBB; }	
	
	TR.Failure{background-color:#F6CECE;}
	TR.Exception{background-color:#F6CECE;}
	TR.Success{background-color:#A9F5A9;}
	
	TR.Pending{background-color:#AAFFFF; color: #BBBBBB;}
	TR.pending{background-color:#FFAAFF; color: #BBBBBB;}
	TR.running{background-color:#FFFFAA;}
	TR.pending{background-color:#FFFFFF;}

	TR.no_new_exe{background-color:#E0E0E0;}
	TR.no_smoke_start{background-color:#E0E0E0;}
	TR.no_smoke_finish{background-color:#E0E0E0;}
	TR.no_new_capture{background-color:#E0E0E0;}
	TR.skipped{background-color:#E0E0E0;}

	TR.eventtable{}	

	TD.no_new_exe0{background-color:#E0E0E0;}
	TD.no_smoke_start0{background-color:#E0E0E0;}
	TD.no_smoke_finish0{background-color:#E0E0E0;}
	TD.no_new_capture0{background-color:#E0E0E0;}
	TD.skipped0{background-color:#E0E0E0;}

	TD.no_new_exe1{background-color:#EEEEEE;}
	TD.no_smoke_start1{background-color:#EEEEEE;}
	TD.no_smoke_finish1{background-color:#EEEEEE;}
	TD.no_new_capture1{background-color:#EEEEEE;}
	TD.skipped1{background-color:#EEEEEE;}

	TD.Failure{background-color:#F6CECE;}
	TD.Exception{background-color:#F6CECE;}
	TD.Success{background-color:#A9F5A9;}
	
	TD.running{background-color:#FFFFAA;}
	TD.pending{background-color:#FFFFFF;}	

	TD.Failure0{background-color:#F6CECE;}
	TD.Exception0{background-color:#F6CECE;}
	TD.Success0{background-color:#A9F5A9;}
	
	TD.running0{background-color:#FFFFAA;}
	TD.pending0{background-color:#FFFFFF;}	



	TD.Failure1{background-color:#F6DEDE;}
	TD.Exception1{background-color:#F6DEDE;}
	TD.Success1{background-color:#C9F5C9;}
	
	TD.running1{background-color:#FFFFAA;}
	TD.pending1{background-color:#FFFFFF;}	

	a img { position: absolute; visibility: hidden }
	a.PlainLink:hover img { visibility: visible; align: bottom; COLOR: #000000;}
	a.PlainLink:link {COLOR: #000000;}
	a.PlainLink:visited {COLOR: #000000;}
	a.PlainLink:active {COLOR: #000000;}	
	
	a.BigLink:hover img { visibility: visible; align: bottom; COLOR: #FF0000;}
	a.BigLink:link {COLOR: #FF0000;}
	a.BigLink:font-size {2.1em;}
	
</STYLE>

<head>


<script type="text/javascript">
	function showHide()
	{   
		var elements = document.getElementsByTagName("tr");
		var display;
		for(var i=0;i&lt;elements.length;i++)
		{
      			if(elements[i].className == "eventtable")
			{
				elements[i].style.display = (elements[i].style.display == "none")?"table-row":"none";  
				display = elements[i].style.display;
			}
		}
 		document.getElementById("btnPlus").value = (display == "none")?"+":"-";		
	}
</script>


<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>

<xsl:if test="$auto_refresh&gt;0">
	<meta http-equiv="refresh">
		<xsl:attribute name="content"><xsl:value-of select="$auto_refresh"/></xsl:attribute>
	</meta>			
</xsl:if>
<title><xsl:value-of select="$title"/> : <xsl:value-of select="$common"/> - Cruise Control - Custom Changelist Report - Auto Refresh <xsl:value-of select="$auto_refresh"/> - Regen <xsl:value-of select="$poll_interval"/></title>


<xsl:if test="$title='Asset Builder'">
	<a>	
		<xsl:attribute name="title">Asset Pipeline 3 (AP3) Assetbuilder</xsl:attribute>
		<xsl:attribute name="href">
			http://rsgediabld7:8080/automation/index.html
		</xsl:attribute>  		  				
		Go to ASSET PIPELINE 3 (AP3) ASSETBUILDER
	</a>
</xsl:if>


</head>
<body>

<xsl:variable name="num_projects" select="count(projects/project)"/>

<table width="100%" border="0">
  <THEAD>
  	<TR class="top_header">
  		<TH align="left"><img src="Rockstar_North_Logo.jpg" alt="RSN logo" width="59" height="72"/></TH>
  		

		<TH align="left">  		
			<a class="PlainLink">
				<xsl:attribute name="title">Wiki</xsl:attribute>
				<xsl:attribute name="href">
					https://devstar.rockstargames.com/wiki/index.php/Custom_Changelist_Reports
				</xsl:attribute>  		  				
  				<xsl:value-of select="$title"/> : Cruise Control - Custom Changelist Report
  			</a>
  		</TH>
  	</TR>
	
  </THEAD>
 </table>
 
 <table width="100%" border="0">
 	<TR class="top_header">
		<td>
		<FORM>
			<xsl:attribute name="action">http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$prev_report"/>.html</xsl:attribute> 
			<xsl:attribute name="style">display:inline;</xsl:attribute>
			<INPUT> 
				<xsl:attribute name="type">submit</xsl:attribute>
				<xsl:attribute name="value">Previous Report</xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="$prev_report"/></xsl:attribute>
			</INPUT>
			<a> 
				<xsl:attribute name="href">
					http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$prev_report"/>.html
				</xsl:attribute>   	
			</a>
		 </FORM>

		 <FORM>
			<xsl:attribute name="action">http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$next_report"/>.html</xsl:attribute> 
			<xsl:attribute name="style">display:inline;</xsl:attribute>
			<INPUT> 
				<xsl:attribute name="type">submit</xsl:attribute>
				<xsl:attribute name="value">Next Report</xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="$next_report"/></xsl:attribute>
			</INPUT>
			<a> 
				<xsl:attribute name="href">
					http://<xsl:value-of select="$webserver"/>/ccnet/custom_reports/<xsl:value-of select="$next_report"/>.html
				</xsl:attribute>   	
			</a>
		 </FORM>
		</td>
	</TR>
 </table>

 <table width="100%" border="1">
   <THEAD>
	<TR>
		<xsl:attribute name="class"><xsl:value-of select="$common"/></xsl:attribute>
		<!--<xsl:for-each select="projects/project"><h2><xsl:value-of select="@short_name"/></h2></xsl:for-each>-->
		<TH><xsl:value-of select="$common"/></TH>
		<TH>Updated @ <xsl:value-of select="$run_time"/></TH>
		<TH>Auto Refresh <xsl:value-of select="$auto_refresh"/>s / Update Rate <xsl:value-of select="$poll_interval"/>s</TH>
	</TR>
  </THEAD>
</table>








<table width="100%" border="1">
  	<THEAD>	
		<TR class="header">
			<TH align="centre"><input id="btnPlus" type="button" onclick="showHide()" value="+"/></TH>	   			
			<TH><B>Events</B></TH>
			<TH><B>CC Project</B></TH>			
			<TH><B>Description</B></TH>
			<TH><B>Status</B></TH>
			<TH><B>Build Start</B></TH>			
			<TH><B>Build End</B></TH>
			<TH><B>Build Duration</B></TH>
		</TR>
 	</THEAD>
 	
	
	<xsl:variable name="limit_event_rows">100</xsl:variable>


	<TBODY>

		<xsl:for-each select="events/event">			
			<xsl:if test="(position() = 1)">
				<TR>						
					<xsl:variable name="project" select="@project"/>			
					<xsl:variable name="description" select="@description"/>
					<xsl:variable name="status" select="@status"/>
					<xsl:variable name="build_date" select="@build_date"/>
					<xsl:variable name="build_completed" select="@build_completed"/>
					<xsl:variable name="build_duration" select="@build_duration"/>
					<xsl:variable name="cc_project_name" select="@cc_project_name"/>

					<TD></TD>
					<TD align="left"><xsl:value-of select="$project"/></TD>
					<TD align="left"><xsl:value-of select="$cc_project_name"/></TD>
					<TD align="left"><xsl:value-of select="$description"/></TD>
					<TD align="center">
						<xsl:attribute name="class">
							<xsl:value-of select="$status"/>
						</xsl:attribute>
						<xsl:value-of select="$status"/>
					</TD>
					<TD align="center"><xsl:value-of select="$build_date"/></TD>
					<TD align="center"><xsl:value-of select="$build_completed"/></TD>
					<TD align="center"><xsl:value-of select="$build_duration"/></TD>
				</TR>
			</xsl:if>
		</xsl:for-each>

		<xsl:for-each select="events/event">			
			<xsl:if test="not(position() > $limit_event_rows) and position() &gt; 1">
				<TR class="eventtable" style='display:none'>	
					<xsl:variable name="project" select="@project"/>			
					<xsl:variable name="description" select="@description"/>
					<xsl:variable name="status" select="@status"/>
					<xsl:variable name="build_date" select="@build_date"/>
					<xsl:variable name="build_completed" select="@build_completed"/>
					<xsl:variable name="build_duration" select="@build_duration"/>
					<xsl:variable name="cc_project_name" select="@cc_project_name"/>
					
					<TD></TD>
					<TD align="left"><xsl:value-of select="$project"/></TD>
					<TD align="left"><xsl:value-of select="$cc_project_name"/></TD>
					<TD align="left"><xsl:value-of select="$description"/></TD>
					<TD align="center">
						<xsl:attribute name="class">
							<xsl:value-of select="$status"/>
						</xsl:attribute>
						<xsl:value-of select="$status"/>
					</TD>
					<TD align="center"><xsl:value-of select="$build_date"/></TD>
					<TD align="center"><xsl:value-of select="$build_completed"/></TD>
					<TD align="center"><xsl:value-of select="$build_duration"/></TD>
				</TR>
			</xsl:if>
		</xsl:for-each>

	</TBODY> 
</table>












<table width="100%" border="1">
  <THEAD>	
	   <xsl:choose>
	   	<xsl:when test="$num_projects&gt;1">
			<TR class="header">	   
			   <TH rowspan="3" width="5%"><B>Change</B></TH>
			   <TH rowspan="3" width="10%"><B>User</B></TH>
			   <TH rowspan="3" width="10%"><B>Buddy</B></TH>
			   <TH width="10%">
				<xsl:attribute name="colspan"><xsl:value-of select="$num_projects"/></xsl:attribute>
				<B>Status</B>
			   </TH>	   	   
			   <TH rowspan="3" width="10%"><B>Modified</B></TH>
			   <TH rowspan="3" width="10%"><B>Last Build Completed</B></TH>			   
			   <TH rowspan="3" width="10%"><B>Comment</B></TH>
			</TR>
			<TR class="header">
				<xsl:for-each select="projects/project">
					<xsl:variable name="prev_category" select="preceding::*[1]/@category"/>
					<xsl:variable name="next_category" select="following::*[1]/@category"/>
					<xsl:variable name="this_category" select="@category"/>
					
					<xsl:if test="not($this_category=$prev_category)">
						<TH width="10%">
							<xsl:attribute name="colspan"><xsl:value-of select="count(//project_state_aggregate/projects/project[@category=$this_category])"/></xsl:attribute>
							<B><xsl:value-of select="$this_category"/></B>												
						</TH>
					</xsl:if>
				</xsl:for-each>
			</TR>	 
			<TR class="header">
				<xsl:for-each select="projects/project">
					<xsl:variable name="project_name" select="@project_name"/>
					<xsl:variable name="project_url" select="@project_url"/>							
					
					<TH width="10%">
						<a class="PlainLink">
							<xsl:attribute name="href">
								<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
							</xsl:attribute>
							<B><xsl:value-of select="@sub_category"/></B>
						</a>							
					</TH>
				</xsl:for-each>
			</TR>	
		</xsl:when>
		<xsl:otherwise>			
			<xsl:variable name="project_url" select="projects/project/@project_url"/>							
		
			<TR class="header">	   
			   <TH rowspan="1" width="5%"><B>Change</B></TH>
			   <TH rowspan="1" width="10%"><B>User</B></TH>
			   <TH rowspan="1" width="10%"><B>Buddy</B></TH>
			   <TH rowspan="1" width="10%">
				<a class="PlainLink">
					<xsl:attribute name="href">
						<xsl:value-of select="$project_url"/>/ViewProjectReport.aspx
					</xsl:attribute>			   
			   		<B>Status</B>	   	   
			   	</a>
			   </TH>
			   <TH rowspan="1" width="10%"><B>Modified</B></TH>
			   <TH rowspan="1" width="10%"><B>Last Build Completed</B></TH>
			   <TH rowspan="1" width="10%"><B>Comment</B></TH>
			</TR>
		</xsl:otherwise>			
	</xsl:choose>
  </THEAD> 
  <TBODY>	

	<xsl:variable name="limit_rows">300</xsl:variable>
 
	<xsl:for-each select="changes/change">			
		<xsl:sort select="@sort_date" order="descending"/>
		<xsl:if test="not(position() > $limit_rows)">
			<TR>	
				<xsl:variable name="change_number" select="@change_number"/>			
				<xsl:variable name="full_comment" select="@comment"/>
				<xsl:variable name="comment" select="substring(@comment, 1, 60)"/>
				<xsl:variable name="user" select="@user"/>
				<xsl:variable name="buddy" select="@buddy"/>
				<xsl:variable name="url" select="@url"/>
				<xsl:variable name="modified_date" select="@modified_date"/>
				<xsl:variable name="last_build_date" select="@last_build_date"/>
				<xsl:variable name="turnaround" select="@turnaround"/>
				
				<xsl:variable name="user_no_dots">
				    <xsl:call-template name="string-replace-all">
				      <xsl:with-param name="text" select="$user" />
				      <xsl:with-param name="replace" select="'.'" />
				      <xsl:with-param name="by" select="'_'" />
				    </xsl:call-template>
				</xsl:variable>

				<TD align="right">
				<a>
					<xsl:attribute name="title">p4Web</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:value-of select="$url"/>
					</xsl:attribute>
					<xsl:value-of select="$change_number"/>
				</a>			
				</TD>
				<TD align="left">
					<a class="PlainLink" onmouseover="">
						<!--<xsl:attribute name="href">mailto:<xsl:value-of select="$user"/>@rockstarnorth.com?subject=Re. Changelist <xsl:value-of select="$change_number"/></xsl:attribute>-->
						<xsl:attribute name="href">#</xsl:attribute>
						<img width="120">
							<xsl:attribute name="src">users/<xsl:value-of select="$user"/>.jpg</xsl:attribute>			
						</img>

						<xsl:value-of select="$user"/>
					</a>
				</TD>

				<TD align="left"><xsl:value-of select="$buddy"/></TD>
				<xsl:for-each select="project">
					<xsl:sort select="@order" data-type="number" order="ascending"/>

					<xsl:variable name="log_name" select="@log_name"/>				
					<xsl:variable name="status" select="@status"/>
					<xsl:variable name="details" select="@details"/>
					<xsl:variable name="swizzler" select="@swizzler"/>				
					<xsl:variable name="project_name" select="@project_name"/>
					<xsl:variable name="build_duration" select="@build_duration"/>				
					<xsl:variable name="build_date" select="@build_date"/>					
					<xsl:variable name="project_url" select="//project_state_aggregate/projects/project[@name=$project_name]/@project_url"/>

					<TD align="center">

						<xsl:choose>
							<xsl:when test="not($details='') and (string-length($details) &gt; 1)">
								<xsl:attribute name="class"><xsl:value-of select="$details"/><xsl:value-of select="$swizzler"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="class"><xsl:value-of select="$status"/><xsl:value-of select="$swizzler"/></xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						
						<xsl:choose>
							<xsl:when test="$status='unknown' or $status='pending' or $status='Pending' or $status='running'">										
								<xsl:value-of select="$status"/>
							</xsl:when>
							<xsl:otherwise>						


								
									<a>
										<xsl:attribute name="title">Completed: <xsl:value-of select="$build_date"/>&#10;&#13;Duration: <xsl:value-of select="$build_duration"/>
											<!--<xsl:if test="not($details='')">&#10;Details:&#10;<xsl:value-of select="$details"/></xsl:if>-->
										</xsl:attribute>
										<xsl:attribute name="href">
											<xsl:value-of select="$project_url"/>build/<xsl:value-of select="$log_name"/>/ViewBuildReport.aspx
										</xsl:attribute>
										
										<xsl:choose>
											<xsl:when test="not($details='')">
												<xsl:value-of select="$details"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$status"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
																		
							</xsl:otherwise>
						</xsl:choose>
					</TD>							
				</xsl:for-each>			
				<TD align="center"><xsl:value-of select="$modified_date"/></TD>
				<TD align="center">
					<a>
						<xsl:attribute name="title">Turnaround: <xsl:value-of select="$turnaround"/></xsl:attribute>					
						<xsl:value-of select="$last_build_date"/>
					</a>					
				</TD>
				<TD align="left">
					<a>
						<xsl:attribute name="title"><xsl:value-of select="$full_comment"/></xsl:attribute>					
						<xsl:value-of select="$comment"/>...
					</a>				
				</TD>		
			</TR>
		</xsl:if>
	</xsl:for-each>
  </TBODY>
</table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>

