#ifdef __SNC__
#pragma control notocrestore=2
#pragma control fastmath=1
#pragma control assumecorrectalignment=1
#pragma control assumecorrectsign=1
#pragma control relaxalias=0
#pragma control checknew=0
#pragma control c+=rtti
#pragma control c-=exceptions
#define _CPPRTTI 
#endif

// Basic types
BEGIN_NS_RAGE
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef vector unsigned int u128;

typedef signed char s8;
typedef signed short s16;
typedef signed int s32;

#ifdef __LP64__
typedef unsigned long u64;
typedef signed long s64;
#else
typedef unsigned long long u64;
typedef signed long long s64;
#endif

typedef float f32;
typedef u32 uptr;

END_NS_RAGE

// Platform definitions
#define __BE		1
#define __CONSOLE	1
#define __PS2		0
#define __PS3		1
#define __PPU		1
#define __SPU		0
#define __WIN32		0
#define __OSX		0
#define __POSIX		0
#define __XBOX		0
#define __XENON		0
#define __GCUBE		0
#define __PSP		0
#define __LINUX		0
#define __WIN32PC	0
#define __PAGING	1
#define __RESOURCECOMPILER	0

#define CONSOLE_ONLY(x) x
#define PS2_ONLY(x)
#define PS3_ONLY(x) x
#define PPU_ONLY(x) x
#define SPU_ONLY(x)
#define WIN32_ONLY(x)
#define XBOX_ONLY(x)
#define XENON_ONLY(x)
#define GCUBE_ONLY(x)
#define PSP_ONLY(x)	
#define LINUX_ONLY(x)
#define WIN32PC_ONLY(x)

#define VUDATA(type,x)	extern type x __attribute__((section(".vudata")))
#define IN_SCRATCHPAD
#define BEGIN_ALIGNED(x)
#define ALIGNED(x)	__attribute__((aligned(x)))
#define UNCACHED(x) 	((unsigned)(x) | 0x20000000)
#define CACHED(x) 	((unsigned)(x) & ~0x30000000)
#define DEPRECATED	__attribute__((__deprecated__))
#define RESTRICT	__restrict
#define MAY_ALIAS	__attribute__((__may_alias__))
#define __forceinline	__attribute__((__always_inline__)) inline

// VC.Net-specific keyword, meaningless to us
#define __w64

#ifdef __SNC__
#define __debugbreak() __builtin_snpause()
#else
static inline void __debugbreak() { __asm__ volatile("tw 31,1,1"); }
#endif

#ifdef __SNC__
#pragma diag_error 194		// zero used for undefined preprocessing identifier "xxxx"
#pragma diag_error 828		// entity-kind "entity" was never referenced
#pragma diag_suppress 613	// overloaded virtual function "blah" is only partially overridden in class "blahChild"
#pragma diag_suppress 739	//  using-declaration ignored -- it refers to the current namespace
#pragma diag_suppress 1421	//  support for trigraphs is disabled
#pragma diag_suppress 1437	//  multicharacter character literal (potential portability problem)
#pragma diag_suppress 1669	//  Potential uninitialized reference to "blah" in function "bar" (nice but spurious)
#if __SN_VER__ >= 33001
#pragma diag_suppress 1787	//  nnn is not a valid value for the enumeration type "blah"
#endif
#endif


