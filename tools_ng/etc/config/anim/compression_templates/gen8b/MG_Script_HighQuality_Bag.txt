movertranslation,*,0.0001,maxdecompressioncost=60;
moverrotation,*,0.0001,maxdecompressioncost=60;
bonetranslation,#0,0.001,maxdecompressioncost=60;
bonerotation,#0,0.0001,maxdecompressioncost=60;
bonerotation,*clav*,0.0005;
bonerotation,*arm*,0.0005;
bonerotation,*Arm*,0.0005;
bonerotation,*Finger*,0.002,15;
bonerotation,*Neck*,0.001;
bonerotation,*Head*,0.001;
bonerotation,*Spine*,0.00005;
bonerotation,*Bag*,0.00005;
bonetranslation,*Bag*,0.00005;
bonerotation,*Thigh*,0.0005;
bonerotation,*Calf*,0.0005;
bonerotation,*Foot*,0.001;
bonerotation,*Toe*,0.001,15;
bonetranslation,*,0.001;
bonerotation,*,0.001;
cameratranslation,*,0.00001;
camerarotation,*,0.00001;
cameraFOV,*,0.00001;
cameraDOF,*,0.00001;
facialtranslation,*,0.00001;
facialrotation,*,0.00001;
facialControl,*,0.00001;
shaderSlideU,*,0.00001;
shaderSlideV,*,0.00001;
*,*,channellinearbias=1.5,maxdecompressioncost=80;