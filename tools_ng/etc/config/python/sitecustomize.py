"""
This sitecustomize.py is used by the launcher to perform site customization based on the chosen launch profile.
"""
import os
import sys
import site


# Add any additional "site" directories passed through from launcher config.
# This will add each dir to sys.path and evaluate any .pth files they contain.
siteDirsStr = os.getenv("RSTA_PYTHON_SITE_DIRS")
if siteDirsStr:
    for siteDir in siteDirsStr.split(";"):
        print ("RSTA Add site: {}".format(siteDir))
        site.addsitedir(siteDir.strip())

# Remove this directory and this module from sys.path and sys.modules
# and then import sitecustomize again so that the launcher doesn't block
# the standard sitecustomize if there is another one in use
currDir = os.path.dirname(__file__)
sys.path.remove(currDir)

sitecustomizeModule = None
if "sitecustomize" in sys.modules:
    sitecustomizeModule = sys.modules["sitecustomize"]
    del sys.modules["sitecustomize"]

try:
    import sitecustomize
except ImportError:
    pass

# Restore this module into sys.modules if there wasn't another one
if "sitecustomize" not in sys.modules and sitecustomizeModule is not None:
    sys.modules["sitecustomize"] = sitecustomizeModule