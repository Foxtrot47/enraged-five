@echo off
setlocal
set cmdlinefile=%1\commandline.txt
if not exist "%cmdlinefile%" set cmdlinefile=%1\args.txt
if not exist "%cmdlinefile%" set cmdlinefile=%1\commandline.txt
if not exist "%cmdlinefile%" (
echo # Add commandline parameters below, one per line.>"%cmdlinefile%"
echo # Examples: "-rag" or "-multisample 8">>"%cmdlinefile%"
echo # Starting a line with '#' causes it to be ignored.>>"%cmdlinefile%"
echo # See the commandline parameters documentation for complete details.>>"%cmdlinefile%"
echo.>>"%cmdlinefile%"
)
start "" /d "%1" "%cmdlinefile%"
