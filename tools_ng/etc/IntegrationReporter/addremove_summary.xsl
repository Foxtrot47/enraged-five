<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>
	
	<xsl:variable name="allRemovedFiles" select="/IntegrationReport/ChangelistRecords/ChangelistRecord/Files/File[@Action='add']"/>
	<xsl:variable name="allAddedFiles" select="/IntegrationReport/ChangelistRecords/ChangelistRecord/Files/File[@Action='remove']"/>
		
	<xsl:template match="/">
	
		<head>
			<title>Integration Report</title>
			<link type="text/css" rel="stylesheet" href="/integration_reporter.css" />
		</head>
		<body class="wholepage" bgcolor="darkgrey" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

		<h2 align="center">Integration Report</h2>

		<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="50%" align="center">
		
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Branch Specification:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@BranchName"/></b>
				</td>
			</tr>	

			<tr bgcolor="ivory">
				<td align="Center">
					<b>Last Submitted Changelist:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@LastChangelist"/></b>
				</td>
			</tr>	

			<tr bgcolor="ivory">
				<td align="Center">
					<b>Time:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Time"/></b>
				</td>
			</tr>	
			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>User:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@User"/></b>
				</td>
			</tr>	

			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Workspace:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Workspace"/></b>
				</td>
			</tr>	
			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Server:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Port"/></b>
				</td>
			</tr>	
		</table>	

		<p>
			<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="50%" align="center">
				<tr bgcolor="ivory">
					<td align="center">
						<b>Report Summaries</b>
					</td>
				</tr>	
				
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="General_Summary.html#GeneralSummary">General Summary</a></b>
					</td>
				</tr>	
								
				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Conflict_Summary.html#ConflictSummary">Conflict Summary</a></b>
					</td>
				</tr>	
				
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="Change_Summary.html#ChangelistSummary">Changelist Summary</a></b>
					</td>
				</tr>
				
				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Change_Summary.html#BugSummary">Bug Summary</a></b>
					</td>
				</tr>	
								
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="AddRemove_Summary.html#AddSummary">Add/Remove Summary</a></b>
					</td>
				</tr>	
				
				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Diff_Summary.html#DiffSummary">Diff Summary</a></b>
					</td>
				</tr>		
			</table>
		</p>		
				
		<a name="AddRemoveSummary"></a>
		<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			<tr bgcolor="Ivory" color="ProjectGridHeader">
                <td align="Center">
					<b>Add/Remove Summary - List of all added and removed files from the integration.</b>
				</td>
			</tr>
		
			<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			
				<xsl:choose>
					<xsl:when test="count($allAddedFiles) + count($allRemovedFiles) = 0 ">
						<tr bgcolor="Ivory" class="ProjectGridHeader">
							<td align="Center">
								<b>No files were found.</b>
							</td>
						</tr>					
					</xsl:when>
					<xsl:otherwise>
							
						<tr bgcolor="#3399FF" class="ProjectGridHeader">
							<td align="Center">
								<b>File</b>
							</td>
							<td align="Center">
								<b>Changelist</b>
							</td>
							<td align="Center">
								<b>Action</b>
							</td>
						</tr>
						
						<xsl:call-template name="FileByAction"/>
					</xsl:otherwise>
				</xsl:choose>
			
			</table>
		</table>
		
		</body>
	</xsl:template>
	
	<xsl:template name="FileByAction">
		<xsl:for-each select="$allAddedFiles">
			
			<xsl:sort select="@Name" order="ascending" case-order="lower-first"/> 
			<tr>
				<xsl:choose>
					<xsl:when test="position() mod 2=0">
					  <xsl:attribute name="class">section-oddrow</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
					  <xsl:attribute name="class">section-evenrow</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>

				<td class="norm"><xsl:value-of select="@Name"/></td>
	
				<td class="norm" align="center">
					<xsl:variable name="changelistNumber" select="../../@ChangelistNumber"/>
					<xsl:element name="a"> 
					<xsl:attribute name="href">
						<xsl:text>http://10.11.16.17:8080/</xsl:text>
						<xsl:value-of select="$changelistNumber" />
						<xsl:text>?ac=10</xsl:text>
					</xsl:attribute> 
					<xsl:value-of select="$changelistNumber" />
					</xsl:element> 
					<br/>
				</td>
				
				<td class="norm" align="Center"><xsl:value-of select="@Action"/></td>

			</tr>
		</xsl:for-each>
		
		<xsl:for-each select="$allRemovedFiles">
			
			<xsl:sort select="@Name" order="ascending" case-order="lower-first"/> 
			<tr>
				<xsl:choose>
					<xsl:when test="position() mod 2=0">
					  <xsl:attribute name="class">section-oddrow</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
					  <xsl:attribute name="class">section-evenrow</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>

				<td class="norm"><xsl:value-of select="@Name"/></td>
	
				<td class="norm" align="center">
					<xsl:variable name="changelistNumber" select="../../@ChangelistNumber"/>
					<xsl:element name="a"> 
					<xsl:attribute name="href">
						<xsl:text>http://10.11.16.17:8080/</xsl:text>
						<xsl:value-of select="$changelistNumber" />
						<xsl:text>?ac=10</xsl:text>
					</xsl:attribute> 
					<xsl:value-of select="$changelistNumber" />
					</xsl:element> 
					<br/>
				</td>
				
				<td class="norm" align="Center"><xsl:value-of select="@Action"/></td>

			</tr>
		</xsl:for-each>
	</xsl:template>
	
		

</xsl:stylesheet>
