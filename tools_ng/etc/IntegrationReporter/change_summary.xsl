<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>
	
	<xsl:variable name="allFiles" select="/IntegrationReport/ResolveRecords/ResolveRecord"/>
	<xsl:variable name="conflictedFiles" select="/IntegrationReport/ResolveRecords/ResolveRecord[@ResolveStatus='HAS_CONFLICTS']"/>
	<xsl:variable name="allChangelists" select="/IntegrationReport/ChangelistRecords/ChangelistRecord"/>
	<xsl:variable name="allBugs" select="/IntegrationReport/BugRecords/BugRecord"/>
		
	<xsl:template match="/">
	
		<head>
			<title>Integration Report</title>
			<link type="text/css" rel="stylesheet" href="/integration_reporter.css" />
		</head>
		<body class="wholepage" bgcolor="darkgrey" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

		<h2 align="center">Integration Report</h2>

		<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="50%" align="center">
		
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Branch Specification:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@BranchName"/></b>
				</td>
			</tr>	

			<tr bgcolor="ivory">
				<td align="Center">
					<b>Last Submitted Changelist:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@LastChangelist"/></b>
				</td>
			</tr>	

			<tr bgcolor="ivory">
				<td align="Center">
					<b>Time:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Time"/></b>
				</td>
			</tr>	
			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>User:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@User"/></b>
				</td>
			</tr>	

			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Workspace:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Workspace"/></b>
				</td>
			</tr>	
			
			<tr bgcolor="ivory">
				<td align="Center">
					<b>Server:</b>
				</td>
				<td align="Center">
					<b><xsl:value-of select="/IntegrationReport/@Port"/></b>
				</td>
			</tr>	
		</table>	

		<p>
			<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="50%" align="center">
				<tr bgcolor="ivory">
					<td align="center">
						<b>Report Summaries</b>
					</td>
				</tr>	
				
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="General_Summary.html#GeneralSummary">General Summary</a></b>
					</td>
				</tr>	
								
				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Conflict_Summary.html#ConflictSummary">Conflict Summary</a></b>
					</td>
				</tr>	
				
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="Change_Summary.html#ChangelistSummary">Changelist Summary</a></b>
					</td>
				</tr>	

				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Change_Summary.html#BugSummary">Bug Summary</a></b>
					</td>
				</tr>	
				
				<tr bgcolor="ivory">
					<td align="center">
						<b><a href="AddRemove_Summary.html#AddSummary">Add/Remove Summary</a></b>
					</td>
				</tr>	
				
				<tr bgcolor="lavender">
					<td align="center">
						<b><a href="Diff_Summary.html#DiffSummary">Diff Summary</a></b>
					</td>
				</tr>	
			</table>
		</p>		
				
		<a name="ChangelistSummary"></a>
		<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			<tr bgcolor="Ivory" color="ProjectGridHeader">
                <td align="Center">
					<b>Changelist Summary - List of all changes brought over from the integration.</b>
				</td>
			</tr>
		
			<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			
				<xsl:choose>
					<xsl:when test="count($allChangelists) = 0 ">
						<tr bgcolor="Ivory" class="ProjectGridHeader">
							<td align="Center">
								<b>No files were found.</b>
							</td>
						</tr>					
					</xsl:when>
					<xsl:otherwise>
							
						<tr bgcolor="#3399FF" class="ProjectGridHeader">
							<td align="Center">
								<b>Changelist Number</b>
							</td>
							<td align="Center">
								<b>Bug Number</b>
							</td>
							<td align="Center">
								<b>Description</b>
							</td>
							<td align="Center">
								<b>User</b>
							</td>
						</tr>
						
						<xsl:call-template name="ChangelistByUser"/>
					</xsl:otherwise>
				</xsl:choose>
			
			</table>
		</table>
		
		<p>
		
		</p>
		
		<a name="BugSummary"></a>
		<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			<tr bgcolor="Ivory" color="ProjectGridHeader">
                <td align="Center">
					<b>Bug Summary - List of declared Bugstar fixes from the changelist description.  This may not be a complete list.</b>
				</td>
			</tr>
		
			<table cellpadding="0" cellspacing="3" rules="rows" bordercolor="#333399" border="1" id="StatusGrid" bgcolor="White" width="98%" align="center">
			
				<xsl:choose>
					<xsl:when test="count($allBugs) = 0 ">
						<tr bgcolor="Ivory" class="ProjectGridHeader">
							<td align="Center">
								<b>No files were found.</b>
							</td>
						</tr>					
					</xsl:when>
					<xsl:otherwise>
							
						<tr bgcolor="#3399FF" class="ProjectGridHeader">
							<td align="Center">
								<b>Bug Number</b>
							</td>
							<td align="Center">
								<b>Changelists</b>
							</td>
						</tr>
						
						<xsl:call-template name="ChangelistByBug"/>
					</xsl:otherwise>
				</xsl:choose>
			
			</table>
		</table>
		
		</body>
	</xsl:template>
	
	<xsl:template match="ResolveRecord">
		<tr>
			<xsl:choose>
				<xsl:when test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			
			<td class="norm"><xsl:value-of select="@TargetFile"/></td>
			<td class="norm" align="center">
				<xsl:choose>
					<xsl:when test="./ResolveChanges/@Conflict &gt; 0">
						<b><xsl:value-of select="./ResolveChanges/@Conflict"/></b>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="./ResolveChanges/@Conflict"/>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			
			<xsl:choose>
				<xsl:when test="@EndRevision != 0">
					<td class="norm" align="center"><xsl:value-of select="@StartRevision"/>-<xsl:value-of select="@EndRevision"/></td>
				</xsl:when>
				<xsl:otherwise>
					<td class="norm" align="center"><xsl:value-of select="@StartRevision"/></td>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	</xsl:template>
	
	<xsl:template match="ChangelistRecord">
		<tr>
			<xsl:choose>
				<xsl:when test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			
			<td class="norm" align="Center"><xsl:value-of select="@ChangelistNumber"/></td>
			<td class="norm" align="Center"><xsl:value-of select="@BugNumber"/></td>
			<td class="norm"><xsl:value-of select="@Description"/></td>
			<td class="norm" align="Center"><xsl:value-of select="@User"/></td>

		</tr>
	</xsl:template>
	
	<xsl:template match="File">
		<tr>
			<xsl:choose>
				<xsl:when test="position() mod 2=0">
				  <xsl:attribute name="class">section-oddrow</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
				  <xsl:attribute name="class">section-evenrow</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			
			<td class="norm"><xsl:value-of select="@Name"/></td>
			<td class="norm"><xsl:value-of select="@Revision"/></td>
			<td class="norm"><xsl:value-of select="@Integrated"/></td>
		</tr>
	</xsl:template>
	
	<xsl:template name="ConflictedFiles">
		<xsl:for-each select="$conflictedFiles">
			<tr>
				<xsl:choose>
					<xsl:when test="position() mod 2=0">
					  <xsl:attribute name="class">section-oddrow</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
					  <xsl:attribute name="class">section-evenrow</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				<td class="norm"><xsl:value-of select="@TargetFile"/></td>
				
				<td class="norm" align="center">
					<xsl:value-of select="./ResolveChanges/@Conflict"/>
				</td>

				<td class="norm" align="center">
				<xsl:for-each select="./Changelists/Changelist">
					<xsl:variable name="changelistNumber" select="."/>
						<xsl:element name="a"> 
						<xsl:attribute name="href">
							<xsl:text>http://10.11.16.17:8080/</xsl:text>
							<xsl:value-of select="$changelistNumber" />
							<xsl:text>?ac=10</xsl:text>
						</xsl:attribute> 
						<xsl:value-of select="$changelistNumber" />
						</xsl:element> 
						<br/>
					</xsl:for-each>
				</td>
				
				<td class="norm" align="center">
				<xsl:for-each select="./Changelists/Changelist">	
					<xsl:variable name="changelistNumber" select="."/>
					<xsl:value-of select="/IntegrationReport/ChangelistRecords/ChangelistRecord[@ChangelistNumber=$changelistNumber]/@User"/>
					<br/>
				</xsl:for-each>
				</td>
			</tr>
		</xsl:for-each>
		
	</xsl:template>
	
	<xsl:template name="ChangelistByUser">
		<xsl:for-each select="$allChangelists">
			
			<xsl:sort select="@User" order="ascending" case-order="lower-first"/> 
			<tr>
				<xsl:choose>
					<xsl:when test="position() mod 2=0">
					  <xsl:attribute name="class">section-oddrow</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
					  <xsl:attribute name="class">section-evenrow</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				<td class="norm" align="Center">
					<xsl:element name="a"> 
					<xsl:attribute name="href">
					<xsl:text>http://10.11.16.17:8080/</xsl:text>
					<xsl:value-of select="@ChangelistNumber" />
					<xsl:text>?ac=10</xsl:text>
					</xsl:attribute> 
					<xsl:value-of select="@ChangelistNumber" />
					</xsl:element> 
				</td>
				<td class="norm" align="Center">			
					<xsl:element name="a"> 
					<xsl:attribute name="href">
					<xsl:text>bugstar:</xsl:text>
					<xsl:value-of select="@BugNumber" />
					</xsl:attribute> 
					<xsl:value-of select="@BugNumber" />
					</xsl:element> 
				</td>
				<td class="norm"><xsl:value-of select="@Description"/></td>
				<td class="norm" align="Center"><xsl:value-of select="@User"/></td>

			</tr>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="ChangelistByBug">
	
		<xsl:for-each select="$allBugs">
			
			<xsl:sort select="@BugNumber" data-type="number" order="ascending" case-order="lower-first"/> 
			<tr>
				<xsl:choose>
					<xsl:when test="position() mod 2=0">
					  <xsl:attribute name="class">section-oddrow</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
					  <xsl:attribute name="class">section-evenrow</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				
				<td class="norm" align="Center">			
					<xsl:element name="a"> 
					<xsl:attribute name="href">
					<xsl:text>bugstar:</xsl:text>
					<xsl:value-of select="@BugNumber" />
					</xsl:attribute> 
					<xsl:value-of select="@BugNumber" />
					</xsl:element> 
				</td>
				
				<td class="norm" align="left">
					<xsl:variable name="currentBug" select="@BugNumber"/>
					<xsl:for-each select="/IntegrationReport/BugRecords/BugRecord[@BugNumber=$currentBug]/ChangelistNumbers/Changelist">
						<xsl:variable name="currentChangelist" select="."/>
						<xsl:element name="a"> 
						<xsl:attribute name="href">
							<xsl:text>http://10.11.16.17:8080/</xsl:text>
							<xsl:value-of select="$currentChangelist" />
							<xsl:text>?ac=10</xsl:text>
						</xsl:attribute> 
						<xsl:value-of select="$currentChangelist" />
						</xsl:element> 
						- <xsl:value-of select="/IntegrationReport/ChangelistRecords/ChangelistRecord[@ChangelistNumber=$currentChangelist]/@Description"/>
						<br/>
					</xsl:for-each>
				</td>
			</tr>
		</xsl:for-each>
		
	</xsl:template>

</xsl:stylesheet>
