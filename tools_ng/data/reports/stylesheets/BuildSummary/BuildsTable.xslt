<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rep="http://schemas.datacontract.org/2004/07/RSG.Statistics.Common.Dto.Reports">

<xsl:template match="/">
	<table>
		<tr>
			<th colspan="3" class="title">Builds Summary</th>
		</tr>
		<tr>
			<th>HasAssetStats</th>
			<th>HasProcessedStats</th>
			<th>Identifier</th>
      	</tr>
      
      	<xsl:for-each select="rep:ArrayOfBuildSummaryData/rep:BuildSummaryData">
        <tr>
          <td><xsl:value-of select="rep:HasAssetStats" /></td>
          <td><xsl:value-of select="rep:HasProcessedStats" /></td>
		  <td><xsl:value-of select="rep:Identifier" /></td>
        </tr>
      </xsl:for-each>

    </table>
  
</xsl:template>

</xsl:stylesheet> 