<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rep="http://schemas.datacontract.org/2004/07/RSG.Model.Common.Playthrough">

<xsl:template match="/">
	<table>
		<tr>
			<th colspan="5" class="title">Playthrough Summary</th>
		</tr>
		<tr>
			<th>Username</th>
			<th>Total Sessions</th>
			<th>Total Time</th>
			<th>Total Mission Attempts</th>
			<th>Total Unique Mission Attempts</th>
      	</tr>
      
      	<xsl:for-each select="rep:ArrayOfUserSessionSummary/rep:UserSessionSummary">
        <tr>
          <td><xsl:value-of select="rep:UserName" /></td>
          <td><xsl:value-of select="rep:SessionCount" /></td>
		  <td><xsl:value-of select="rep:TimeSpentPlaying" /></td>
		  <td><xsl:value-of select="rep:TotalMissionAttempts" /></td>
		  <td><xsl:value-of select="rep:UniqueMissionAttempts" /></td>
        </tr>
      </xsl:for-each>

    </table>
  
</xsl:template>

</xsl:stylesheet> 