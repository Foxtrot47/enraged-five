<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE template>
<template>
	<item name="entity" type="Entity">
		<property name="OutputPath" value="c:/output/"/>
		<property name="SkipBoundSectionOfTypeFile" value="false"/>
		<item name="mesh" type="Mesh">
			<property name="OutputPath" value="c:/output/"/>
			<property name="TextureOutputPath" value="c:/output/"/>
			<property name="ExportTextures" value="true"/>
			<property name="MeshAsAscii" value="false"/>
			<property name="MeshForSkeleton" value="false"/>
		</item>
	</item>
</template>
