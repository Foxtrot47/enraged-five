<?xml version="1.0" ?>
<!--
	scenexml.xml
	
	3dsmax SceneXml Exporter Configuration File
	David Muir <david.muir@rockstarnorth.com>
	
	10 December 2007
	
	This configuration file determines which 3dsmax scene nodes are exported,
	and how they are exported to XML.
	
	The properties defined attempt to read the same name property from the node
	by default.  The following are special properties which are handled uniquely
	in the SceneXml exporter:
		- "name"
			Object's name.
		
		- "attributes"
			Defined to export non-default value MaxDataStore attributes to the 
			XML output file.
		
		- "transform"
			Defined to export non-default position and rotation to the XML 
			output file (world-space).

		- "materials"
			Defined to export materials meta-data to the XML output file.

		- "paramblocks"
			3dsmax ParamBlock2 definitions (some custom objects still use these)
		
		- "boundingbox"
			3dsmax bounding box (world-space).
		
		- "user"
			3dsmax user properties.

    - "subobjects"
      Subobject properties (be careful about usage).

    - "lodhierarchy"
      Export Rockstar North LOD hierarchy for object (parent and children)
      if applicable.

    - "

	All node's class and superclass properties should be in lowercase with
	spaces converted to "_" characters.  The SceneXml plugin automatically
	handles converting class names to this format so comparisons will work.
	
	Generic type strings must be one of:
		string, int, bool, float
-->
<scenexml>

	<!--
		Object node name in exported XML file.  Having all nodes exported as
		the same top-level XML node name makes writing parses easier and
		more maintainable, as they can just ignore classes they don't 
		understand.
	-->
	<object export_as="object" />
	
	<!--
		Material table configuration.
	-->
	<material export="true" />

	<!--
		Superclass Definitions
		
		Specify superclass friendly names and their ID export_as defined in the 3dsmax
		SDK headers.  The friendly names are matched against any superclass
		nodes defined below.
		
		Identifiers should be in decimal notation.
	-->

	<superclasses>
		<superclass id="2" name="gen_derivob" />
		<superclass id="3" name="derivob" />
		<superclass id="16" name="geomobject" />
		<superclass id="32" name="camera" />
		<superclass id="48" name="light" />
		<superclass id="64" name="shape" />
		<superclass id="80" name="helper" />
	</superclasses>

	<!--
		Class Nodes
	
		Specify class nodes below this comment.  These are used to catch 
		specific object classes in 3dsmax.
	-->

	<!--
		Export editable_mesh nodes
	-->
	<node class="editable_mesh">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<!-- <property name="paramblocks" /> -->
			<property name="materials" />
			<property name="lodhierarchy" />
      <property name="polycount" />
		</properties>
	</node>
	
	<!--
		Export editable_poly nodes
	-->
	<node class="editable_poly">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<!-- <property name="paramblocks" /> -->
			<property name="materials" />
			<property name="lodhierarchy" />
      <property name="polycount" />
		</properties>
	</node>

	<!--
		3dsmax 2010 Container nodes
	-->
	<node class="container">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
		</properties>
	</node>

	<!--
		Export Gta ProcObj nodes
	-->
	<node class="gta_procobj">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
		</properties>
	</node>

	<!--
	3dsmax 2011 Stated Anim proxy nodes
	-->
	<node class="statedanimproxy">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
		</properties>
	</node>

	<!--
		Export XRef nodes
		
		From ixrefobj.h:
		  #define IID_XREF_OBJECT8 Interface_ID(0x470145ad, 0x3daa64e1)
		Needs to be converted to decimal for TinyXML... even has problems with
		Hex-strings.  Erk.
		
		Note: This is the Interface_ID rather than the Class_ID!  Can't find
		      a way to fetch this at run-time.
	-->
	<node class="xref_object" ida="1191265709" idb="1034577121">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="activeFileName" export_as="reffile" type="filename" />
			<property name="activeObjName" export_as="refname" type="string" />
			<property name="lodhierarchy" />
		</properties>
	</node>

	<!--
		Export InternalRef nodes
	-->
	<node class="internalref" ida="0x62a94c64" idb="0x3635185">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="materials" />
			<property name="subobjects" />
			<property name="lodhierarchy" />
		</properties>	
	</node>

	<!--
		Export GtaBlock nodes
	-->
	<node class="gta_block" ida="571614280" idb="1621251351">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
		</properties>	
	</node>

	<!--
		Export GtaTimeCycle nodes
	-->
	<node class="gta_timecycle" ida="943292105" idb="1509126041">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
		</properties>	
	</node>

	<!--
		Export RSLedge nodes
	-->
	<node class="rs_ledge" ida="510208733" idb="1676355765">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="subobjects" />
			<property name="references" />
			<property name="lodhierarchy" />
			
			<!-- 
				HACK to export ledges in IDE file ok.
				Shouldn't need to do this from the SceneXml info but
				the stars aren't aligned this week.
			-->
			<property name="localtransform" custom="true" />
			<property name="ledge_left" custom="true" />
			<property name="ledge_right" custom="true" />
			<property name="ledge_normal" custom="true" />
		</properties>
	</node>

	<!-- 
		Collision nodes
	-->
	<node class="col_box">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
      <property name="polycount" />
		</properties>
	</node>
	<node class="col_sphere">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
      <property name="polycount" />
		</properties>
	</node>
	<node class="col_mesh">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
      <property name="polycount" />
		</properties>
	</node>
	<node class="col_plane">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
      <property name="polycount" />
		</properties>
	</node>

	<!--
		Superclass Nodes
	
		Specify superclass nodes below this comment.  These are used export_as catch
		all cases so we don't have to micro-manage this file when new custom
		objects (e.g. Helpers) are added into our maps.
	-->

	<!--
		Export helper nodes
	-->
	<node superclass="helper">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="subobjects" />
			<property name="references" />
			<property name="lodhierarchy" />
		</properties>	
	</node>

	<!--
		Export light nodes
		
		We have some additional custom light properties that are required for 
		the 2DFX light IDE file generation and we can't seem to fetch them
		using the user property.
	-->
	<node superclass="light">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="materials" />
			<property name="light_attributes" />
		</properties>	
	</node>
	
	<!--
		Export Photometric Light nodes
		
		We don't export the parameter blocks because these crash the plugin.
	-->
	<node class="free_light">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="attributes" />
		</properties>
	</node>
	
	<!--
		Export shape nodes
	-->
	<node superclass="shape">
		<properties>
			<property name="name" />
			<property name="transform" />
			<property name="boundingbox" />
			<property name="attributes" />
			<property name="paramblocks" />
			<property name="materials" />
			<property name="lodhierarchy" />
		</properties>
	</node>
</scenexml>
<!-- End of scenexml.xml -->
