filein "statedAnim.ms"

plugin SimpleObject statedAnimProxy
name:"StatedAnimProxy"
--extends:Editable_Mesh
--replaceUI:true
classID:#(0x21417f9b, 0x7d5e06d0)
category:"RS Utils"
( 
	parameters main rollout:params
	(
		nodeTab type:#maxObjectTab tabSizeVariable:true-- subAnim:true
		showBegin type:#boolean ui:showBegin default:true
		showEnd type:#boolean ui:showEnd default:true
	)
	fn updateMyMesh = 
	(
		if undefined==nodeTab then 
		(
			print "no nodeTab is set for proxy!"
			return false 
		)
			
		try
		(
			local proxyObj = Editable_mesh()
			local mats = #()
			local numMats = 0
			local theNode = (refs.dependents this)[1]
			if showBegin then
			(
				local startProxy = copy nodeTab[1]
				if undefined!=startProxy then
				(
					numMats = numMats+1
					for f=1 to startProxy.mesh.faces.count do
						setFaceMatID startProxy.mesh f numMats
					attach proxyObj startProxy

					local mat  = standard()
					--startProxy.mesh.wirecolor = 
					mat.diffuse = green
					append mats mat
				)
			)
			if showEnd then
			(
 				local endProxy = copy nodeTab[3]
				if undefined!=endProxy then
				(
					numMats = numMats+1
					for f=1 to endProxy.mesh.faces.count do
						setFaceMatID endProxy.mesh f numMats
					attach proxyObj endProxy

					local mat  = standard()
					--startProxy.mesh.wirecolor = 
					mat.diffuse = red
					append mats mat
				)
			)
			setMesh mesh proxyObj.mesh
			delete proxyObj
			
			if mats.count>0 then
			(
				if statedAnimProxy==(classof theNode) then
				(
					theNode.material = multimaterial numsubs:mats.count
					for mi=1 to mats.count do
					(
						theNode.material.materialList[mi] = mats[mi]
					)
				)
			)
			
		)catch ( print (getCurrentException()))
	)
	
	fn testNodeForAnimation targ= 
	(
		local formerSelection = selection
		select targ
		local foundAnim = gRsStatedAnimTool.selectionChanged()
		if foundAnim then
		(
			local theNode = (refs.dependents this)[1]
			local referenceState = gRsStatedAnimTool.currAnim.objectArray[1]
			nodeTab = gRsStatedAnimTool.currAnim.objectArray
			updateMyMesh()
			local idxGroupName = getAttrIndex "Gta Object" "groupName"
			theNode.name = (getattr referenceState idxGroupName)+"_anim"
			theNode.pos = referenceState.pos
			select theNode
			return true
		)
		select formerSelection
		return false
	)
	
	rollout params "StatedAnim Parameters"
	(
		checkbox showbegin "Show begin state" checked:true
		checkbox showEnd "Show end state" checked:true
		pickbutton pickAnimation "pick an animation state"
		
		on showbegin changed val do 
		(
			includeBegin = val
			myMesh = undefined
			updateMyMesh()
		)
		on showEnd changed val do 
		(
			includeEnd = val
			myMesh = undefined
			updateMyMesh()
		)
		on pickAnimation picked targ do
		(
			testNodeForAnimation targ
		)
	)
	on buildMesh do
	(
		updateMyMesh()
	)
	
-- 	on statedAnimProxy close do
-- 	(
-- 		for r in statedAnimProxy.rollouts do removeRollout r
-- 	)
	
	tool create
	(
		on mousePoint click do coordsys screen
		(
			local worldRay = mapScreenToWorldRay viewPoint
			local alltargets = intersectRayScene worldRay
			if undefined!=alltargets and alltargets.count>0 then
			(
				local targ = alltargets[1][1] -- first hit, node part of result
				if testNodeForAnimation targ then 
					return #stop
			)
		)
	)
)