--
-- File:: startup/RsLightShaftDirectionManipulator.ms
-- Description:: Light Shaft heading attributes manipulator gizmo.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 June 2010
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- None

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsLightShaftDirectionManipulator
-- desc: Light Shaft direction attribute manipulator.
--
plugin SimpleManipulator RsLightShaftDirectionManipulator
	name:"LightShaftDirectionManipulator"
	invisible:true
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local directionXidx = ( GetAttrIndex "Gta LightShaft" "Direction X" )
	local directionYidx = ( GetAttrIndex "Gta LightShaft" "Direction Y" )
	local directionZidx = ( GetAttrIndex "Gta LightShaft" "Direction Z" )
	local vectorScale = ( 10.0 )
	
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulateNode node do
	(
		return ( GtaLightShaft == (classOf node.baseObject) )
	)
		
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(	
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		local col = [1,0,0]
		local colsel = [1,1,1]
		
		-- Refresh heading vector representation from the manipulated target.
		local directionX = ( GetAttr target directionXidx )
		local directionY = ( GetAttr target directionYidx )
		local directionZ = ( GetAttr target directionZidx )
		local direction = [ directionX, directionY, directionZ ] * vectorScale
		format "DIRECTION: %\n" direction
		local directionDir = manip.makeGizmoShape()
		directionDir.addPoint( [0,0,0] )
		directionDir.addPoint( direction )
			
		this.addGizmoShape directionDir flags col colsel
		this.addGizmoMarker #diamond direction flags col colsel
		
		local tip = stringStream ""
		format "Direction [%, %, %]" directionX directionY directionZ to:tip
		(tip as String)
	)
	
	--
	-- event: mouseMove
	-- desc: Invoked when mouse is moved when dragging the manipulator.
	--
	on mouseMove m which do
	(
		local pl = manip.makePlaneFromNormal z_axis [0,0,0]
		local projectedPoint = [0,0,0]
		local viewRay = this.getLocalViewRay m
		
		local res = pl.intersect viewRay &projectedPoint
		if ( res ) then
		(
			projectedPoint = ( normalize projectedPoint )
			--format "z_axis Point: %\n" projectedPoint
			SetAttr target directionXidx projectedPoint[1]
			SetAttr target directionYidx projectedPoint[2]
			
			-- Force redraw and updateGizmos call.
			target.transform = target.transform
		)
		else
		(
			pl = manip.makePlaneFromNormal x_axis [0,0,0]
			projectedPoint = [0,0,0]
			res = pl.intersect viewRay &projectedPoint
			if ( res ) then
			(
				projectedPoint = ( normalize projectedPoint )
				--format "x_axis Point: %\n" projectedPoint
				SetAttr target directionZidx projectedPoint[3]
				
				-- Force redraw and updateGizmos call.
				target.transform = target.transform
			)
		)
	)
	
	--
	-- event: mouseUp
	-- desc:
	--
	on mouseUp m which do
	(
		-- Normalise the heading vector.
		local directionX = ( GetAttr target directionXidx )
		local directionY = ( GetAttr target directionYidx )
		local directionZ = ( GetAttr target directionZidx )
		local direction = [ directionX, directionY, directionZ ]
		direction = ( normalize direction )
		format "Normalised: %\n" direction
		
		SetAttr target directionXidx (direction[1] as float)
		SetAttr target directionYidx (direction[2] as float)
		SetAttr target directionZidx (direction[3] as float)
	)
)

-- startup/RsLightShaftDirectionManipulator.ms
