plugin Helper gtaSpawnPoint
name:"Gta SpawnPoint" 
classID:#(0x5ae41793, 0x575c0170)
category:"Gta" 
extends:dummy 
( 
	local meshObj,lastType

	parameters pblock rollout:params
	(
		spawnType type:#string animatable:false default:"Seat_Bench"
	)

	rollout params "Gta SpawnPoint Parameters"
	(
		dropdownlist lstSpawnType "Type:" items:#("Seat_Bench","Seat_Chair","Seat_Netbrowser","HangOut_Street","Scenario_Leaning","NewspaperVendingMachine","Scenario_SecurityGuard","Scenario_BuildingWorkers","Scenario_SmokingOutsideOffice")
		
		on lstSpawnType selected var do (
		
			spawnType = lstSpawnType.items[lstSpawnType.selection]
		)
		
		on params open do (
		
			setSel = 0
		
			for i = 1 to lstSpawnType.items.count do (
			
				itemName = lstSpawnType.items[i]
			
				if itemName == spawnType then (
				
					setSel = i
				)
			)
		
			lstSpawnType.selection = setSel
		)
	)

	on getDisplayMesh do (

		if meshobj == undefined then (
		
			meshobj = trimesh()
		)
			
		if lastType != spawnType do (
				
			if findstring spawnType "Seat_" != undefined then (
						
				vp = #()
				fl = #()
	
				-- Vertex data ---------------------------
				append vp [-0.174465,-0.133207,0.129882]
				append vp [-0.174465,0.164664,0.142701]
				append vp [-0.174465,-0.204731,0.629332]
				append vp [-0.174465,-0.0189303,0.636782]
				append vp [0.18468,-0.133207,0.129882]
				append vp [0.184681,0.164664,0.142701]
				append vp [0.18468,-0.204732,0.629332]
				append vp [0.18468,-0.018931,0.636781]
				append vp [0.0771526,-0.204329,0.686059]
				append vp [0.077153,-0.0126054,0.686059]
				append vp [0.0771528,-0.204329,0.906925]
				append vp [0.077153,-0.0126055,0.906925]
				append vp [-0.0610686,-0.204329,0.686059]
				append vp [-0.0610682,-0.012605,0.686058]
				append vp [-0.0610685,-0.204329,0.906925]
				append vp [-0.0610683,-0.0126052,0.906925]
				append vp [0.0185184,-0.0765724,-0.0328432]
				append vp [0.0668501,0.547562,0.0440606]
				append vp [0.0185183,-0.112996,0.12133]
				append vp [0.0668501,0.449277,0.134316]
				append vp [0.205466,-0.0765728,-0.0328433]
				append vp [0.205466,0.547562,0.0440606]
				append vp [0.205466,-0.112996,0.12133]
				append vp [0.205466,0.449276,0.134316]
				append vp [-0.193549,-0.076572,-0.0328433]
				append vp [-0.193549,0.547563,0.0440607]
				append vp [-0.193549,-0.112995,0.12133]
				append vp [-0.193549,0.449277,0.134316]
				append vp [-0.00660148,-0.0765724,-0.0328433]
				append vp [-0.0549324,0.547562,0.0440607]
				append vp [-0.00660153,-0.112996,0.12133]
				append vp [-0.0549324,0.449277,0.134316]
				append vp [-0.167821,0.406362,-0.446937]
				append vp [-0.167821,0.528606,-0.439709]
				append vp [-0.190089,0.372347,0.0179565]
				append vp [-0.190089,0.517672,0.0285395]
				append vp [-0.0719225,0.406361,-0.446937]
				append vp [-0.0719223,0.528606,-0.439709]
				append vp [-0.0496546,0.372346,0.0179563]
				append vp [-0.0496543,0.517672,0.0285393]
				append vp [0.0881477,0.405358,-0.44701]
				append vp [0.088148,0.529609,-0.439636]
				append vp [0.0668499,0.372346,0.0179564]
				append vp [0.0668501,0.517672,0.0285393]
				append vp [0.185986,0.405357,-0.44701]
				append vp [0.185986,0.529609,-0.439636]
				append vp [0.207284,0.372346,0.0179563]
				append vp [0.207285,0.517671,0.0285392]
				append vp [0.0668499,0.387584,-0.497445]
				append vp [0.0668502,0.684117,-0.497445]
				append vp [0.06685,0.387583,-0.385814]
				append vp [0.0668503,0.684117,-0.434348]
				append vp [0.203469,0.387583,-0.497445]
				append vp [0.20347,0.684116,-0.497445]
				append vp [0.20347,0.387583,-0.385814]
				append vp [0.20347,0.684116,-0.434348]
				append vp [-0.190081,0.387584,-0.497445]
				append vp [-0.190081,0.684118,-0.497445]
				append vp [-0.190081,0.387584,-0.385814]
				append vp [-0.190081,0.684118,-0.434348]
				append vp [-0.0534616,0.387583,-0.497445]
				append vp [-0.0534613,0.684117,-0.497445]
				append vp [-0.0534616,0.387583,-0.385814]
				append vp [-0.0534613,0.684117,-0.434348]
				append vp [0.205466,0.073245,-0.0736879]
				append vp [0.03012,0.0732455,-0.0736878]
				append vp [-0.0184344,0.0732454,-0.0736878]
				append vp [-0.193549,0.0732457,-0.0736878]
	
				-- Face data ---------------------------
				append fl [1,3,4]
				append fl [4,2,1]
				append fl [5,6,8]
				append fl [8,7,5]
				append fl [1,2,6]
				append fl [6,5,1]
				append fl [2,4,8]
				append fl [8,6,2]
				append fl [4,3,7]
				append fl [7,8,4]
				append fl [3,1,5]
				append fl [5,7,3]
				append fl [11,9,12]
				append fl [10,12,9]
				append fl [14,13,16]
				append fl [15,16,13]
				append fl [10,9,14]
				append fl [13,14,9]
				append fl [12,10,16]
				append fl [14,16,10]
				append fl [11,12,15]
				append fl [16,15,12]
				append fl [9,11,13]
				append fl [15,13,11]
				append fl [17,19,20]
				append fl [20,18,66]
				append fl [23,65,24]
				append fl [65,23,21]
				append fl [66,18,22]
				append fl [65,21,17]
				append fl [18,20,24]
				append fl [24,22,18]
				append fl [20,19,23]
				append fl [23,24,20]
				append fl [19,17,21]
				append fl [21,23,19]
				append fl [25,27,28]
				append fl [28,26,68]
				append fl [29,67,32]
				append fl [32,31,29]
				append fl [68,26,30]
				append fl [67,29,25]
				append fl [26,28,32]
				append fl [32,30,26]
				append fl [28,27,31]
				append fl [31,32,28]
				append fl [27,25,29]
				append fl [29,31,27]
				append fl [33,35,36]
				append fl [36,34,33]
				append fl [37,38,40]
				append fl [40,39,37]
				append fl [33,34,38]
				append fl [38,37,33]
				append fl [34,36,40]
				append fl [40,38,34]
				append fl [36,35,39]
				append fl [39,40,36]
				append fl [35,33,37]
				append fl [37,39,35]
				append fl [41,43,44]
				append fl [44,42,41]
				append fl [45,46,48]
				append fl [48,47,45]
				append fl [41,42,46]
				append fl [46,45,41]
				append fl [42,44,48]
				append fl [48,46,42]
				append fl [44,43,47]
				append fl [47,48,44]
				append fl [43,41,45]
				append fl [45,47,43]
				append fl [49,51,52]
				append fl [52,50,49]
				append fl [53,54,56]
				append fl [56,55,53]
				append fl [49,50,54]
				append fl [54,53,49]
				append fl [50,52,56]
				append fl [56,54,50]
				append fl [52,51,55]
				append fl [55,56,52]
				append fl [51,49,53]
				append fl [53,55,51]
				append fl [57,59,60]
				append fl [60,58,57]
				append fl [61,62,64]
				append fl [64,63,61]
				append fl [57,58,62]
				append fl [62,61,57]
				append fl [58,60,64]
				append fl [64,62,58]
				append fl [60,59,63]
				append fl [63,64,60]
				append fl [59,57,61]
				append fl [61,63,59]
				append fl [66,65,17]
				append fl [65,22,24]
				append fl [65,66,22]
				append fl [20,66,17]
				append fl [68,67,25]
				append fl [67,30,32]
				append fl [67,68,30]
				append fl [28,68,25]
	
				setmesh meshObj vertices:vp faces:fl

			) else (
			
				vp = #()
				fl = #()
				
				-- Vertex data ---------------------------
				append vp [-0.174061,-0.10603,0.0423626]
				append vp [-0.174061,0.191421,0.0220025]
				append vp [-0.174061,-0.121613,0.546667]
				append vp [-0.174061,0.0638656,0.533425]
				append vp [0.185085,-0.10603,0.0423626]
				append vp [0.185084,0.191421,0.0220025]
				append vp [0.185084,-0.121613,0.546667]
				append vp [0.185085,0.0638656,0.533425]
				append vp [0.0775571,-0.124758,0.606859]
				append vp [0.077557,0.0592572,0.553039]
				append vp [0.077557,-0.0627575,0.818844]
				append vp [0.077557,0.121257,0.765024]
				append vp [-0.0606643,-0.124758,0.606859]
				append vp [-0.0606643,0.0592572,0.553039]
				append vp [-0.0606643,-0.0627575,0.818844]
				append vp [-0.0606643,0.121257,0.765024]
				append vp [0.0189226,-0.0686093,0.0159526]
				append vp [0.0672537,0.18905,-0.557693]
				append vp [0.0189225,0.0679494,0.0962498]
				append vp [0.0672537,0.246282,-0.437151]
				append vp [0.20587,-0.0686094,0.0159525]
				append vp [0.20587,0.18905,-0.557694]
				append vp [0.20587,0.0679493,0.0962498]
				append vp [0.20587,0.246282,-0.437151]
				append vp [-0.193145,-0.0686094,0.0159525]
				append vp [-0.193145,0.18905,-0.557693]
				append vp [-0.193145,0.0679493,0.0962498]
				append vp [-0.193145,0.246282,-0.437151]
				append vp [-0.0061973,-0.0686093,0.0159526]
				append vp [-0.0545289,0.18905,-0.557693]
				append vp [-0.00619741,0.0679494,0.0962498]
				append vp [-0.0545289,0.246282,-0.437151]
				append vp [-0.167418,0.102366,-0.927612]
				append vp [-0.167418,0.224656,-0.934013]
				append vp [-0.189685,0.120221,-0.461818]
				append vp [-0.189685,0.265822,-0.467449]
				append vp [-0.0715188,0.102366,-0.927612]
				append vp [-0.0715188,0.224656,-0.934013]
				append vp [-0.0492508,0.120221,-0.461818]
				append vp [-0.0492509,0.265822,-0.467449]
				append vp [0.0885516,0.101361,-0.927573]
				append vp [0.0885515,0.225662,-0.934052]
				append vp [0.0672536,0.120221,-0.461818]
				append vp [0.0672535,0.265822,-0.467449]
				append vp [0.18639,0.101361,-0.927573]
				append vp [0.18639,0.225662,-0.934052]
				append vp [0.207688,0.120221,-0.461818]
				append vp [0.207688,0.265822,-0.467449]
				append vp [0.0672537,0.0807779,-0.988396]
				append vp [0.0672535,0.377201,-0.996488]
				append vp [0.0672537,0.0838242,-0.876807]
				append vp [0.0672535,0.378923,-0.933415]
				append vp [0.203873,0.0807777,-0.988396]
				append vp [0.203873,0.377201,-0.996488]
				append vp [0.203873,0.083824,-0.876807]
				append vp [0.203873,0.378923,-0.933415]
				append vp [-0.189678,0.0807779,-0.988396]
				append vp [-0.189678,0.377201,-0.996488]
				append vp [-0.189678,0.0838242,-0.876807]
				append vp [-0.189678,0.378923,-0.933415]
				append vp [-0.0530579,0.0807777,-0.988396]
				append vp [-0.0530581,0.377201,-0.996488]
				append vp [-0.0530579,0.083824,-0.876807]
				append vp [-0.0530581,0.378923,-0.933415]
				append vp [0.20587,-0.0634243,-0.139247]
				append vp [0.0305241,-0.0634242,-0.139247]
				append vp [-0.0180303,-0.0634242,-0.139247]
				append vp [-0.193145,-0.0634242,-0.139247]
				
				-- Face data ---------------------------
				append fl [1,3,4]
				append fl [4,2,1]
				append fl [5,6,8]
				append fl [8,7,5]
				append fl [1,2,6]
				append fl [6,5,1]
				append fl [2,4,8]
				append fl [8,6,2]
				append fl [4,3,7]
				append fl [7,8,4]
				append fl [3,1,5]
				append fl [5,7,3]
				append fl [11,9,12]
				append fl [10,12,9]
				append fl [14,13,16]
				append fl [15,16,13]
				append fl [10,9,14]
				append fl [13,14,9]
				append fl [12,10,16]
				append fl [14,16,10]
				append fl [11,12,15]
				append fl [16,15,12]
				append fl [9,11,13]
				append fl [15,13,11]
				append fl [17,19,20]
				append fl [20,18,66]
				append fl [23,65,24]
				append fl [65,23,21]
				append fl [66,18,22]
				append fl [65,21,17]
				append fl [18,20,24]
				append fl [24,22,18]
				append fl [20,19,23]
				append fl [23,24,20]
				append fl [19,17,21]
				append fl [21,23,19]
				append fl [25,27,28]
				append fl [28,26,68]
				append fl [29,67,32]
				append fl [32,31,29]
				append fl [68,26,30]
				append fl [67,29,25]
				append fl [26,28,32]
				append fl [32,30,26]
				append fl [28,27,31]
				append fl [31,32,28]
				append fl [27,25,29]
				append fl [29,31,27]
				append fl [33,35,36]
				append fl [36,34,33]
				append fl [37,38,40]
				append fl [40,39,37]
				append fl [33,34,38]
				append fl [38,37,33]
				append fl [34,36,40]
				append fl [40,38,34]
				append fl [36,35,39]
				append fl [39,40,36]
				append fl [35,33,37]
				append fl [37,39,35]
				append fl [41,43,44]
				append fl [44,42,41]
				append fl [45,46,48]
				append fl [48,47,45]
				append fl [41,42,46]
				append fl [46,45,41]
				append fl [42,44,48]
				append fl [48,46,42]
				append fl [44,43,47]
				append fl [47,48,44]
				append fl [43,41,45]
				append fl [45,47,43]
				append fl [49,51,52]
				append fl [52,50,49]
				append fl [53,54,56]
				append fl [56,55,53]
				append fl [49,50,54]
				append fl [54,53,49]
				append fl [50,52,56]
				append fl [56,54,50]
				append fl [52,51,55]
				append fl [55,56,52]
				append fl [51,49,53]
				append fl [53,55,51]
				append fl [57,59,60]
				append fl [60,58,57]
				append fl [61,62,64]
				append fl [64,63,61]
				append fl [57,58,62]
				append fl [62,61,57]
				append fl [58,60,64]
				append fl [64,62,58]
				append fl [60,59,63]
				append fl [63,64,60]
				append fl [59,57,61]
				append fl [61,63,59]
				append fl [66,65,17]
				append fl [65,22,24]
				append fl [65,66,22]
				append fl [20,66,17]
				append fl [68,67,25]
				append fl [67,30,32]
				append fl [67,68,30]
				append fl [28,68,25]
			
				setmesh meshObj vertices:vp faces:fl
	
			)
				
			lastType = spawnType
		)
		
		meshObj
	)

	tool create ( 
	
		on mousePoint click do (
				
			nodeTM.translation = gridPoint
			#stop 
		)
	) 
)
