-- Rockstar Startup
-- Rockstar North
-- 2/2/2005
-- by Greg Smith

-- create the rockstar export and utility menu if it needs to be created

filein "rockstar/export/settings.ms"
filein "rockstar/util/menu.ms"
filein "pipeline/util/string.ms"
filein "pipeline/util/global_callbacks.ms"

-- Only load the menu if settings have initialised properly
format "RsToolsInstalled %\n" RsToolsInstalled

if RsToolsInstalled == true or gRsIsOutSource == true then (
	fn LoadCollisionEntriesFromFile filename = (

		if (getfiles filename).count < 1 then return 0

		if RexAddCollisionOption == undefined then return 0

		intFile = openfile filename mode:"rb"

		while eof intFile == false do (

			intLine = RsRemoveSpacesFromStartOfString(readline intFile)

			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"

				found = false

				pedDensity = 0

	--			if intTokens[1] == "PAVING_SLABS" then (
	--				
	--				pedDensity = 3
	--			)

				RexAddCollisionOption intTokens[1] pedDensity
			)
		)

		close intFile		
	)

	fn LoadProceduralEntriesFromFile filename = (

		if (getfiles filename).count < 1 then return 0

		if RexAddProceduralOption == undefined then return 0

		intFile = openfile filename mode:"rb"

		nameList = #()

		while eof intFile == false do (

			intLine = RsRemoveSpacesFromStartOfString(readline intFile)

			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"

				found = false

				nameToken = RsUppercase intTokens[1]

				if findstring nameToken "_DATA_START" == undefined and findstring nameToken "_DATA_END" == undefined then (

					if finditem nameList nameToken == 0 then (

						append nameList nameToken
						RexAddProceduralOption nameToken
					)
				)
			)
		)

		close intFile		
	)





	-- We'll need a new solution to this but I dont have time at the moment
	LoadCollisionEntriesFromFile ( RsConfigGetCommonDir() + "data/materials/materials.dat" )
	LoadProceduralEntriesFromFile ( RsConfigGetCommonDir() + "data/materials/procedural.dat" )

	RstSetDefaultShader "default"
	RstSetDefaultAlphaShader "alpha"

	try
	(
		-- Initialise GtaParticle plugin for current project.
		rageparticle_setentityfxdat ( RsConfigGetCommonDir() + "data/effects/entityfx.dat" )
		-- DHM this sucks, we should add the 'art' directory in the configuration data.
		if ( "jimmy" == RsConfigGetProjectName() ) then
			rageparticle_seteffectroot ( RsConfigGetProjRootDir() + "jimmy_art/misc/rmptfx" )
		else if ( "gta" == RsConfigGetProjectName() ) then
			rageparticle_seteffectroot ( RsConfigGetProjRootDir() + "gta_assets/rmptfx" )
		else
			rageparticle_seteffectroot ( RsConfigGetArtDir() + "vfx/rmptfx" )
		rageparticle_rebuild()
	)
	catch
	(
		format "Exception rebuilding GtaParticle entity descriptions: *** % ***\n" (getCurrentException())
	)

	------------------------------------------------------------------------------------
	-- beginning of execution
	------------------------------------------------------------------------------------

	minHeapSize = 122880000

	if heapsize < minHeapSize then (

		heapsize = minHeapSize
	)

	RstSetNormalMapFlip true
	rexSetTextureLimit 1024
	
	-- We only regenerate the 3dsmax menus if we have either
	--  1. Switched project configurations
	--  2. This magic number has changed (use 3dsmax class ID generator) 
	if ( ( true == ( menuman.registerMenuContext 0x29182d56 ) ) or gRsIsOutSource == true or
	     ( RsGetMenuRebuild() ) ) then 
	(
		-- Clear rebuild flag.
		if gRsIsOutSource == false then RsClearMenuRebuild()

		RsSetMenu "RS Export" #(
					"RsMapExport",
					"RsPedExport",
					"RsVehicleExport",
					"RsModelExport",
					"RsSingleIPLExport",
					"RsPlayerCoords",
					"RsWaterExport",
					"RsExportImportZones",
					"RsExportAmbientScripts",
					"RsExportCutLoc",
					"RsExportViseme",
					"RsExportVectorMap")

		RsSetMenu "RS Utils" #( "CustomLauncher", "MapBrowser", "ShowSelector", "IgnoreMe","DontExport","DontApplyRadiosity","BatchUpdate" )

		RsSetMenu "Rs Organisational" #( "Bugstar", "VisibilitySet", "CreateSelectionSets", "ProjMan", "Containers", "PropGroups" ) menuParentName:"RS Utils"

		RsSetMenu "RS Placement" #( "PlacementToolkit", "ObjPosChecker" ) menuParentName:"RS Utils"

		RsSetMenu "Rs Lighting" #( "RadiosityLighting" ) menuParentName:"RS Utils"

		RsSetMenu "Rs Characters" #( "RsAudioTagger", "RsPedSyncTool", "RsPedVarManager", "RsMirroringPerVertTool" ) menuParentName:"RS Utils"

		RsSetMenu "RS Interiors" #( "PropsMilos", "MiloOperations" ) menuParentName:"RS Utils"

		RsSetMenu "Rs Collision" #( 	"CollToolkit",
						"AudioMatCheck", 
						"CollRenamer",
						"ProceduralTypes",
						"RsCollisionSet", 
						"OptimisePlanar",
						"SetBoundMaterials" ) menuParentName:"RS Utils"

		RsSetMenu "Rs Xrefs" #( "XrefToolkit", "PropViewer" ) menuParentName:"RS Utils"

		RsSetMenu "Rs InternalRefs" #( "IrefToolkit" ) menuParentName:"RS Utils"

		RsSetMenu "Rs Modelling" #( "ModelToolkit" ) menuParentName:"RS Utils"

		RsSetMenu "RS Material" #( 	"MaterialToolkit",
						"ChangeShaders",
						"ReplaceShaders",
						"BadTex",
						"RsTerrainSetup", 
						"TexturePathRemap", 
						"VertexColourTools", 
						"EnvironmentMap",
						"ShowMapsInViewport",
						"RsTerrainCbPaint",
						"VertAnglePainter",
						"ProxyTextureSwap",
						"GrassPainter" ) menuParentName:"RS Utils"

		RsSetMenu "RS Info" #( 	"InfoToolkit", 
					"DoShowSceneStats", 
					"DoShowSceneImage", 
					"DoShowTextureStats",
					"DoShowTXDStats",
					"ImportDebugNodes"			) menuParentName:"RS Utils"

		RsSetMenu "RS Lod" #( 	
					"LodEditor", 
					"LodToolkit",
					"LodModUtil",
					"LodContainerTool" ) menuParentName:"RS Utils"

		RsSetMenu "RS VFX" #( 	"VFXToolkit",
								"StatedAnimationSetup") menuParentName:"RS Utils"

		RsSetMenu "RS Vehicle" #( 	"VehicleToolkit", 
									"RsImportVehicleRec") menuParentName:"RS Utils"

		RsSetMenu "RS Cloth" #( "RsClothEditor" )	menuParentName:"RS Utils"

		RsSetMenu "RS Debug" #( "RsImportIndBoundsFromFile", 
					"RsImportIndBoundsFromDirectory",
					"SetCameraFromGame", 
					"RsFragmentTuner",
					"ScriptDump",
					"ERotFinder",
					"SetObjectAttr" ) menuParentName:"RS Utils"

		quadmenu = (menuMan.getViewportRightClickMenu #shiftpressed).getmenu 2
		quadmenu.setTitle("Rockstar")
		RsEmptyMenu quadmenu

		RsAddMenuItems quadmenu "RS Utils" #(	"ShowSuperLodDetail", "ShowLodDetail", "ShowHiDetail","ShowMapsInViewportQuad",
							"TextureTools","BatchSetAttr","CreateShadMesh","CreateCollMeshFromExist","CreateCollMesh","CreateCollBox",
							"CreateCollSphere","CreateCollCapsule","CreateCollCylinder","SelectLODChildren","SelectLODParent","SelectLODAllSet",
							"SelectXRefAllSet","SelectNormalAllSet", "SelectHorizonAllSet", "SwitchSceneDXMats", "SwitchSceneRageMats", 
							"SwitchStdMatsToRage")

		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Object Selection) Toggle Light" #( "LightObjOn", "LightObjOff" )
		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Face Selection) Toggle Light" #( "LightFaceOn", "LightFaceOff" )
		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Entire Scene) Toggle Light" #( "LightSceneOn", "LightSceneOff" )
		
		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Object Selection) Change Texture Scale" #( "To100pc", "To75pc", "To50pc", "To25pc", "To12pc" )
		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Face Selection) Change Texture Scale" #( "To100pcFace", "To75pcFace", "To50pcFace", "To25pcFace", "To12pcFace" )
		RsAddQuadMenuSubItems quadmenu "RS Utils" "(Entire Scene) Change Texture Scale" #( "To100pcScene", "To75pcScene", "To50pcScene", "To25pcScene", "To12pcScene" )

	)

	-- Include the auto-generated RS Wildwest menu
	filein "startup/rswildwest.ms"
	menuMan.updateMenuBar()
	
	
	-- Force changes to autosave for everyone: GTA5 #5307
	autosave.NumberOfFiles = 10
	autosave.Interval = 15.0


	-----------------------------------------------------------------------------
	-- Install Callbacks
	-----------------------------------------------------------------------------
	callbacks.removeScripts #filePreOpenProcess id:#RsGlobalCallbackProjectCheck
	callbacks.addScript #filePreOpenProcess "RsGlobalCallbackProjectCheck()" id:#RsGlobalCallbackProjectCheck
	
)

-- startup/rsutil.ms
