--
-- File:: startup/RsRotationConstraint.ms
-- Description:: Single axis rotation constraint helper.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 November 2009
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/constraints.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsRotationConstraint
-- desc: Rotational constraint helper object
--
plugin SimpleManipulator RsRotationConstraint
	name:"RotationConstraint"
	classID:#(0x2ea24371, 0x65810cc5)
	category:"RAGE Physics"
(
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	
	--
	-- pblock: paramLimits
	-- desc: Limits parameter block.
	--
	parameters paramLimits rollout:rolloutConstraint
	(
		Axis type:#integer ui:lstAxis default:1
		LimitMin type:#angle ui:spnMin default:-90.0
		LimitMax type:#angle ui:spnMax default:90.0

		-- Handler to ensure LimitMin does not exceed LimitMax
		on LimitMin set arg do
		(
			if ( arg > LimitMax ) then
				LimitMin = LimitMax			
		)
		
		-- Handler to ensure LimitMax does not exceed LimitMin
		on LimitMax set arg do
		(
			if ( arg < LimitMin ) then
				LimitMax = LimitMin
		)
	)
	
	--
	-- pblock: paramSize
	-- desc: Helper size parameter block.
	--
	parameters paramSize rollout:rolloutConstraint
	(
		Size type:#worldUnits ui:spnSize default:0.25
	)
	
	-------------------------------------------------------------------------
	-- Rollouts
	-------------------------------------------------------------------------
	
	--
	-- rollout: rolloutConstraint
	-- desc: Rollout for all parameter blocks.
	--
	rollout rolloutConstraint "Parameters"
	(
		dropdownlist lstAxis "Axis:" items:#("X-Axis", "Y-Axis", "Z-Axis")
		spinner spnMin "LimitMin:" range:[-360.0,360.0,-90.0]
		spinner spnMax "LimitMax:" range:[-360.0,360.0,90.0]
		spinner spnSize "Size:"
	)
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulate target return (classOf target) == RsRotationConstraint
	
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(		
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		local segsize = 9.0
		local segs = ( LimitMax - LimitMin ) / segsize
		
		local axisstart
		local axisend
		local axisarc = manip.makeGizmoShape()
		local axiscolour
		
		case Axis of
		(
			1: -- X-Axis Constraint
			(
				axiscolour = [1,0,0]
				axisstart = [0,cos(LimitMin),sin(LimitMin)] * size
				axisend = [0,cos(LimitMax),sin(LimitMax)] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [0,cos(angle_from),sin(angle_from)] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)
			2: -- Y-Axis Constraint
			(				
				axiscolour = [0,1,0]
				axisstart = [cos(LimitMin),0,sin(LimitMin)] * size
				axisend = [cos(LimitMax),0,sin(LimitMax)] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [cos(angle_from),0,sin(angle_from)] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)			
			3: -- Z-Axis Constraint
			(
				axiscolour = [0,0,1]
				axisstart = [cos(LimitMin),sin(LimitMin),0] * size
				axisend = [cos(LimitMax),sin(LimitMax),0] * size
				axisarc.addPoint( [0,0,0] )
				axisarc.addPoint( axisstart )
				for seg = 0 to segs do
				(
					local angle_from = LimitMin + (seg*segsize)
					local xp = [cos(angle_from),sin(angle_from),0] * size
					axisarc.addPoint( xp )
				)
				axisarc.addPoint( axisend )
				axisarc.addPoint( [0,0,0] )
			)				
		)
		
		this.addGizmoShape axisarc flags axiscolour axiscolour
		this.addGizmoMarker #diamond axisstart flags axiscolour axiscolour
		this.addGizmoMarker #diamond axisend flags axiscolour axiscolour
		
		"RotationConstraint"
	)
	
	on mouseDown m which do
    (
      -- Toggle the value of the "Hide" state
      if (which == 1) then 
		target.hide = not hide 
    )
	
	--
	-- event: create
	-- desc: Event handler called on manipulator creation.
	--
	tool create
	(
        on mousePoint click do
           case click of
           (
              1:
				(
					#stop 
				)
           )
	)
)

-- startup/RsRotationConstraint.ms
