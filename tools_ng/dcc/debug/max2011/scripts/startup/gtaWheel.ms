-- Rockstar gtaWheel Object
-- Rockstar North
-- 26/07/2007
-- by Greg Smith

-- helper object for milo portal setup

plugin helper GtaWheel
	name:"Gta Wheel"
	classID:#(0x575a5bdb, 0x4c09135f)
	category:"Gta"
	extends:grid
( 

	local lastWidth,lastLength, meshObj

	on getDisplayMesh do 
	(
		if (meshObj == undefined) do 
		(
			lastWidth = this.grid.width
			lastLength = this.grid.length
		
			meshObj = createInstance box length:lastLength width:lastWidth height:lastLength mapCoords:false
		)
		
		if this.grid.width != lastWidth or this.grid.length != lastLength do 
		(
			lastWidth = this.grid.width
			lastLength = this.grid.length		
		
			meshObj.length = lastLength
			meshObj.width = lastWidth
			meshObj.height = lastLength
		)
		
		meshObj.mesh
	)
)