--
-- File:: startup/RsTextNode.ms
-- Description:: sphere with text
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Date:: 4 June 2010
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/constraints.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsTextNode
-- desc: Translation constraint helper object
--
plugin SimpleManipulator RsTextNode
	name:"RsTextNode"
	classID:#(0x51a1807, 0x303163b6)
	category:"Rs Util"
(
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	
	--
	-- pblock: paramLimits
	-- desc: Limits parameter block.
	--
	parameters main rollout:rolloutConstraint
	(
		radius type:#worldUnits ui:spnRadius default:5.0
		description type:#string ui:edtDescription default:"node"
	)
		
	-------------------------------------------------------------------------
	-- Rollouts
	-------------------------------------------------------------------------
	
	--
	-- rollout: rolloutConstraint
	-- desc: Rollout for all parameter blocks.
	--
	rollout rolloutConstraint "Parameters"
	(
		spinner spnRadius "Radius:" range:[-1000.0, 1000.0, -0.5] scale:0.01
		edittext edtDescription "Radius:"
	)
	
	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulate target return (classOf target) == RsTextNode
	
	local g = [0, 1, 0], r = [1, 0, 0]
	 
	 
	-- Create the manipulator gizmo.
	-- This is called initially and whenever the manipulator target changes
	on updateGizmos do
	(
		-- Clear the current gizmo cache
		this.clearGizmos()
		-- Set the radius of circle gizmo a little bigger than the target radius
		 
		giz = manip.makeCircle [0,0,0] (this.radius * 1.01) 28
		 
		-- Add the circle to the manipulator
		this.addGizmoShape giz 0 g r 
		this.addGizmoText this.description [0,0,0] 0 g g
		 
		-- return the ToolTip string
		return "radius = " + this.radius as string 
	)
	 
	-- mouseMove is called on every mouse move when dragging the manip
	-- It needs to convert the mouse position 'm' into a new value for the 

	on mouseMove m which do
	(
		-- Create the XY plane. 
		 
		-- manip.makePlaneFromNormal takes a normal vector and a point
		-- and creates a plane passing through the point with the given normal
		local pl = manip.makePlaneFromNormal z_axis [0, 0, 0],
		projectedPoint = [0,0,0]
		 
		-- Compute the hit-ray in local coordinates
		viewRay = this.getLocalViewRay m
		 
		-- Intersect the plane with the view ray
		res = pl.intersect viewRay &projectedPoint
		-- If the intersection worked, set the radius
		 
		if (res) then target.radius = (length projectedPoint) / 1.01
	)

	tool create
	(
		on mousePoint click do
		(
			coordsys grid (nodeTM.translation = gridPoint)
			#stop
		)
	)--end create
)