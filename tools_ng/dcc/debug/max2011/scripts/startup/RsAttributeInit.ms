-- Rockstar Attribute callback initialisation
-- Rockstar North
-- 15/5/2006
-- by Gunnar Droege

-- Create and set the function to populate north attribute editor controls

-- enum in x:\gta5\src\dev\rage\base\tools\dcc\max\rageMaxDataStore\script.cpp
--enum eAttrFunctorType
--{
	eFunctorInit = 0
	eFunctorChange = 1
--	eFunctorNum
-- };

fn getControlEntries attributeName = 
(
	case attributeName of
	(
		"Glass type":
		(
			local glasspath = "X:/gta5/build/dev/common/data/tune/Glass/*"
			local glassTypes = getdirectories glasspath
			if glassTypes.count==0 then
			(
				print ("you don't appear to have the path "+glasspath+" on your machine")
				return ""
			)
			local returnString = ""
			for typePath in glassTypes do 
			(
				local folders = filterstring typePath "\\"
				returnString += (folders[folders.count]+",")
			)
			print returnString
			return returnString
		)
		default: return false
	)
)

fn printChange objectName attrname = print (objectName+", "+attrName as string)

if SetAttrFunc!=undefined then
(
	SetAttrFunc "Gta Object" eFunctorInit "Glass type" "getControlEntries"
	
	SetAttrFunc "Gta Object" eFunctorChange "looped" "printChange"
	SetAttrFunc "Gta Object" eFunctorChange "animState" "printChange"
	SetAttrFunc "Gta Object" eFunctorChange "groupName" "printChange"
)