--
-- File:: startup/RsTranslationConstraint.ms
-- Description:: Single axis translation constraint helper.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 11 November 2009
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/constraints.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsTranslationConstraint
-- desc: Translation constraint helper object
--
plugin SimpleManipulator RsTranslationConstraint
	name:"TranslationConstraint"
	classID:#(0x5a11807, 0x303613b6)
	category:"RAGE Physics"
(
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	
	--
	-- pblock: paramLimits
	-- desc: Limits parameter block.
	--
	parameters paramLimits rollout:rolloutConstraint
	(
		Axis type:#integer ui:lstAxis default:1
		LimitMin type:#worldUnits ui:spnMin default:-0.5
		LimitMax type:#worldUnits ui:spnMax default:0.5
		
		-- Handler to ensure LimitMin does not exceed LimitMax
		on LimitMin set arg do
		(
			if ( arg > LimitMax ) then
				LimitMin = LimitMax			
		)
		
		-- Handler to ensure LimitMax does not exceed LimitMin
		on LimitMax set arg do
		(
			if ( arg < LimitMin ) then
				LimitMax = LimitMin
		)
	)
		
	-------------------------------------------------------------------------
	-- Rollouts
	-------------------------------------------------------------------------
	
	--
	-- rollout: rolloutConstraint
	-- desc: Rollout for all parameter blocks.
	--
	rollout rolloutConstraint "Parameters"
	(
		dropdownlist lstAxis "Axis:" items:#("X-Axis", "Y-Axis", "Z-Axis")
		spinner spnMin "LimitMin:" range:[-1000.0, 1000.0, -0.5] scale:0.01
		spinner spnMax "LimitMax:" range:[-1000.0, 1000.0, 0.5] scale:0.01
	)
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulate target return (classOf target) == RsTranslationConstraint
	
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(		
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		
		local axisstart
		local axisend
		local axisshape = manip.makeGizmoShape()
		local axiscolour
		
		case Axis of
		(
			1: -- X-Axis Constraint
			(
				axiscolour = [1,0,0]
				axisstart = [LimitMin, 0, 0]
				axisend = [LimitMax, 0, 0]
				axisshape.addPoint axisstart
				axisshape.addPoint axisend
			)
			2: -- Y-Axis Constraint
			(
				axiscolour = [0,1,0]
				axisstart = [0,LimitMin, 0]
				axisend = [0,LimitMax, 0]
				axisshape.addPoint axisstart
				axisshape.addPoint axisend
			)			
			3: -- Z-Axis Constraint
			(
				axiscolour = [0,0,1]
				axisstart = [0,0,LimitMin]
				axisend = [0,0,LimitMax]
				axisshape.addPoint axisstart
				axisshape.addPoint axisend
			)				
		)
		
		this.addGizmoShape axisshape flags axiscolour axiscolour
		this.addGizmoMarker #dot [0,0,0] flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisstart flags axiscolour axiscolour
		this.addGizmoMarker #bigBox axisend flags axiscolour axiscolour
		
		"TranslationConstraint"
	)
	
	on mouseDown m which do
    (
      -- Toggle the value of the "Hide" state
      if (which == 1) then 
		target.hide = not hide 
    )
	
	--
	-- event: create
	-- desc: Event handler called on manipulator creation.
	--
	tool create
	(
        on mousePoint click do
           case click of
           (
              1:
				(
					#stop 
				)
           )
	)
)

-- startup/RsRotationConstraint.ms
