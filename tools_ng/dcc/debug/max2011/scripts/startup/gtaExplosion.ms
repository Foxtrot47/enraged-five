-- Rockstar Gta Explosion Object
-- Rockstar North
-- 8/2/2007
-- by Greg Smith

-- 2007/05/15 - DHM
--						Version 2: handle conversion of Explosion Tag attribute index (7->10)


plugin helper GtaExplosion
	name:"Gta Explosion"
	classID:#(0x7f2d077a, 0x1de6659c)
	category:"Gta"
	extends:Gta_Pickup
	version:2
( 

	-- Handle the Gta Explosion object upgrade by updating Explosion Tag index
	-- 7 to 10.
	on update do
	(

		-- DHM -- THIS UPGRADE CODE SHOULD BE REMOVED IN THE NEAR FUTURE IN CASE
		       -- INDEX 7 IS REUSED.
		if ( 1 == version ) then
		(
			
			print ( refs.dependents this )
			
			pNode = ( (refs.dependents this)[1] )
			format "Node: %\n" pNode
				
			tagIndex = GetAttrIndex "Gta Explosion" "Explosion Tag"
			tagVal = GetAttr pNode tagIndex

			-- Determine whether we need to upgrade...
			if ( 7 == tagVal ) then
			(
				SetAttr pNode tagIndex 10

				format "Node: %\n" pNode
				format "Gta Explosion: % upgraded Explosion Tag from % to %\n" pNode.name 7 10
			)		
		)
	
	) -- End of update

)