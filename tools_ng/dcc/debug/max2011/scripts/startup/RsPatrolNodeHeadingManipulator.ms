--
-- File:: startup/RsPatrolNodeHeadingManipulator.ms
-- Description:: PatrolNode heading attributes manipulator gizmo.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 15 December 2009
--
-- References::
--  http://forums.cgsociety.org/showthread.php?f=98&t=822103&highlight=scripted+helper
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/constraints.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsRotationConstraint
-- desc: Rotational constraint helper object
--
plugin SimpleManipulator RsPatrolNodeHeadingManipulator
	name:"PatrolNodeHeadingManipulator"
	invisible:true
(
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	local headingXidx = ( GetAttrIndex "PatrolNode" "Heading X" )
	local headingYidx = ( GetAttrIndex "PatrolNode" "Heading Y" )
	local headingZidx = ( GetAttrIndex "PatrolNode" "Heading Z" )
	local vectorScale = ( 10.0 )
	
	-------------------------------------------------------------------------
	-- Parameter Blocks
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------
    --on mouseDown m which do
    --(
      -- Toggle the value of the "Hide" state
    --  if (which == 1) then target.hide = not hide 
   -- )
	--
	-- event: canManipulate
	-- desc:
	--
	on canManipulateNode node do
	(
		return ( PatrolNode == (classOf node.baseObject) )
	)
		
	--
	-- event: updateGizmos
	-- desc: Refresh gizmo representation.
	--
	on updateGizmos do
	(	
		this.clearGizmos()
		local flags = (gizmoActiveViewportOnly)
		local col = [1,0,0]
		local colsel = [1,1,1]
		
		-- Refresh heading vector representation from the manipulated target.
		local headingX = ( GetAttr target headingXidx )
		local headingY = ( GetAttr target headingYidx )
		local headingZ = ( GetAttr target headingZidx )
		local heading = [ headingX, headingY, headingZ ] * vectorScale
		format "HEADING: %\n" heading
		local headingDir = manip.makeGizmoShape()
		headingDir.addPoint( [0,0,0] )
		headingDir.addPoint( heading )
			
		this.addGizmoShape headingDir flags col colsel
		this.addGizmoMarker #diamond heading flags col colsel
		
		local tip = stringStream ""
		format "Heading [%, %, %]" headingX headingY headingZ to:tip
		(tip as String)
	)
	
	--
	-- event: mouseMove
	-- desc: Invoked when mouse is moved when dragging the manipulator.
	--
	on mouseMove m which do
	(
		local pl = manip.makePlaneFromNormal z_axis [0,0,0]
		local projectedPoint = [0,0,0]
		local viewRay = this.getLocalViewRay m
		
		local res = pl.intersect viewRay &projectedPoint
		if ( res ) then
		(
			projectedPoint = ( normalize projectedPoint )
			--format "z_axis Point: %\n" projectedPoint
			SetAttr target headingXidx projectedPoint[1]
			SetAttr target headingYidx projectedPoint[2]
			
			-- Force redraw and updateGizmos call.
			target.transform = target.transform
		)
		else
		(
			pl = manip.makePlaneFromNormal x_axis [0,0,0]
			projectedPoint = [0,0,0]
			res = pl.intersect viewRay &projectedPoint
			if ( res ) then
			(
				projectedPoint = ( normalize projectedPoint )
				--format "x_axis Point: %\n" projectedPoint
				SetAttr target headingZidx projectedPoint[3]
				
				-- Force redraw and updateGizmos call.
				target.transform = target.transform
			)
		)
	)
	
	--
	-- event: mouseUp
	-- desc:
	--
	on mouseUp m which do
	(
		-- Normalise the heading vector.
		local headingX = ( GetAttr target headingXidx )
		local headingY = ( GetAttr target headingYidx )
		local headingZ = ( GetAttr target headingZidx )
		local heading = [ headingX, headingY, headingZ ]
		heading = ( normalize heading )
		format "Normalised: %\n" heading
		
		SetAttr target headingXidx (heading[1] as float)
		SetAttr target headingYidx (heading[2] as float)
		SetAttr target headingZidx (heading[3] as float)
	)
)

-- startup/RsPatrolNodeHeadingManipulator.ms
