utility liveTextureUpdate "live texture update" width:162 height:125
(
	local selectedObjects = #{}
	local formerSelectedObjects = #{}
	local wasEditorOpen = false
	fn SetMaterialupdate mtl refresh = 
	(
		if Rage_Shader == (classof mtl) then
			RstSetTextureRefresh mtl refresh
		else if Multimaterial == (classof mtl) then
			for m in mtl.materiallist do SetMaterialupdate m refresh
	)
	fn CleanUpScene = 
	(
		for o in objects do
			SetMaterialupdate o.material false
	)
	
	label msg "This script slows down the scene. Always deactivate and use on limited amount of objects.\nOpen Multimaterials in the Material Editor judder on update, so select rage shader in question." pos:[3,5] width:150 height:120
	checkbutton buttActivate "activate" width:150 height:30 highlightColor:(color 255 0 0)
	button buttCleanup "cleanup - deactivate all scene" width:150 height:21
	
	on buttActivate changed val do
	(
		if val then
		(
			formerSelectedObjects = for o in objects where not o.isHidden collect o
			selectedObjects = selection
			hide objects
			unhide selectedObjects
			for o in selectedObjects do
				SetMaterialupdate o.material val
			buttActivate.text = "DEACTIVATE (important)"
			wasEditorOpen = MatEditor.isOpen()
			MatEditor.Open()
		)
		else
		(
			for o in selectedObjects do
				SetMaterialupdate o.material val
			unhide formerSelectedObjects
			selectedObjects = #{}
			buttActivate.text = "activate"
			if not wasEditorOpen then
				MatEditor.Close()
		)
	)
	on buttCleanup pressed do
	(
		CleanUpScene()
	)
)
