-- Rockstar Rex Report
-- Rockstar North
-- 17/4/2007
-- by Greg Smith

RsRexReport = #()
RsCountVal = 10
RsRetVal = true

fn RsRexExportWithReport = (

	RsRetVal = true

	RsRexReport = RexExport()

	if RsRexReport != true then (

		RsCountVal = 10
		
		rollout RsRexExportReport "rexExport Errors"
		(
			listbox lstMissing items:RsRexReport height:20
			checkbox chkFreeze "Freeze" pos:[50,280]
--			button btnOK "OK" width:100 pos:[150,280]
			button btnCancel "Close" width:100 pos:[250,280]
			timer tmrCount

--			on btnOK pressed do (
--
--				RsRetVal = true
--				DestroyDialog RsRexExportReport
--			)	
			
			on btnCancel pressed do (

				RsRetVal = false
				DestroyDialog RsRexExportReport
			)		
						
			on tmrCount tick do (
/*
				if chkFreeze.checked == false then (

					if RsCountVal == 0 then (

						RsRetVal = false
						DestroyDialog RsRexExportReport 
					) else (

						RsCountVal = RsCountVal - 1
						btnCancel.text = "Close (" + (RsCountVal as string) + ")"
					)
				)*/
			)
			
			on RsRexExportWithReport open do (

				btnCancel.text = "Close (" + (RsCountVal as string) + ")"
			)
		)

		CreateDialog RsRexExportReport width:600 modal:true
	)
	
	RsRetVal
)