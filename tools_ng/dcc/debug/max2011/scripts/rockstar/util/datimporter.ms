-- Rockstar DAT Importer
-- Rockstar North
-- 14/8/2006
-- by Luke Openshaw

-- Creates splines from dat files.

	---------------------------------------------------------------
	-- Import data and build a spline
	---------------------------------------------------------------
	fn BuildLine file = (
		
		datFile = openFile file
		
		if datFile == undefined then (
			messagebox "Could not open file"
			return false
		)
		
		splineObj = Line()
		addNewSpline splineObj 
		leftstations = #()
		rightstations = #()
		leftstationnames = #()
		rightstationnames = #()
		i = 0
		while eof(datFile) == false do (
			currentLine = readLine datFile
			
			arrLine = filterString currentLine " "
			splinePoint = [0,0,0]
			if arrLine.count > 2 then (
				splinePoint.x = arrLine[1] as float
				splinePoint.y = arrLine[2] as float
				splinePoint.z = arrLine[3] as float
				if arrLine[4] == "1" then (
					append leftstations (i as string)
					if arrLine.count == 5 then append leftstationnames arrLine[5]
				)
				else if arrLine[4] == "2" then (
					append rightstations (i as string)
					if arrLine.count == 5 then append rightstationnames arrLine[5]
				)
				addKnot splineObj 1 #smooth #curve splinePoint 
			)
		i=i+1
		)
		
		--stationStr = "stations="
		leftstationStr=""
		for station in leftstations do (
			leftstationStr = leftstationStr+station+","	
		)
		
		rightstationStr=""
		for station in rightstations do (
			rightstationStr = rightstationStr+station+","	
		)
		
		rightNameStr=""
		for stnname in rightstationnames do rightNameStr = rightNameStr+stnname+","
		
		leftNameStr=""
		for stnname in leftstationnames do leftNameStr = leftNameStr+stnname+","
		
		setuserprop splineObj "leftstations" leftstationStr
		setuserprop splineObj "rightstations" rightstationStr
		setuserprop splineObj "leftstationnames" leftNameStr
		setuserprop splineObj "rightstationnames" rightNameStr
		updateShape splineObj
			
	)
	
	fn ExportSpline file = (
		splineObj = selection[1]
		
		leftstationstring = getuserprop splineObj "leftstations"
		rightstationstring = getuserprop splineObj "rightstations"
		oldstylestationstring = getuserprop splineObj "stations"
		
		leftstationnamesstring = getuserprop splineObj "leftstationnames"
		rightstationnamesstring = getuserprop splineObj "rightstationnames"
		
		leftstations = #()
		rightstations = #()
		
		leftstationnames = #()
		rightstationnames = #()
		
		if leftstationstring != undefined then (
			stationtokens = filterstring leftstationstring ","
			
			for stationtok in stationtokens do (
				append leftstations (stationtok as number)
			)
		)
		
		-- Append the old style as left stations by default
		if oldstylestationstring != undefined then (
			stationtokens = filterstring oldstylestationstring ","

			for stationtok in stationtokens do (
				
				append leftstations (stationtok as number)
			)
		)
		
		if rightstationstring != undefined then (
			stationtokens = filterstring rightstationstring ","

			for stationtok in stationtokens do (
				append rightstations (stationtok as number)
			)
		)
		
		if leftstationnamesstring != undefined then (
			stationtokens = filterstring leftstationnamesstring ","

			for stationtok in stationtokens do (
	
				append leftstationnames stationtok 
			)
		)

		if rightstationnamesstring != undefined then (
			stationtokens = filterstring rightstationnamesstring ","

			for stationtok in stationtokens do (
				append rightstationnames stationtok
			)
		)
		
		if (rightstationnames.count != rightstations.count) or (leftstationnames.count != leftstations.count) then (
			messagebox "Not all stations have station names"
			--return 0
		)
		
		if classof(splineObj) != line then (
			messagebox "Select a line object"
			return 0
		)
		
		datFile = createFile file
		
		if datFile == undefined then (
			messagebox "Could not create file"
			return false
		)
		
		knotCount = numKnots splineObj
		
		currLine = (knotCount as string) + "\n"
		format currLine to: datFile
		
		for i = 1 to knotCount do (
			knotPoint = getKnotPoint $ 1 i
			stationIdx = 0
			stationName = ""
			
			leftIdx = findItem leftstations i
			rightIdx = findItem rightstations i
			
			if leftIdx  !=0 then (
				stationIdx = 1
				stationName = leftstationnames[leftIdx]
				
			)
			else if rightIdx !=0 then (
				stationIdx = 2
				stationName = rightstationnames[rightIdx]
			)
			
			
			if stationName != undefined then currLine = knotPoint.x as string + " " + knotPoint.y as string + " " + knotPoint.z as string + " " + stationIdx as string  + " " + stationName + "\n"
			else currLine = knotPoint.x as string + " " + knotPoint.y as string + " " + knotPoint.z as string + " " + stationIdx as string  + "\n"
			format currLine to:datFile
		)
		
		close datFile
		
		
	)

	