--
-- IconMaker256Single.ms: Creates an icon for the selected object
-- Must only be 1 selected object
-- Creates a 256 x 256 JPEG
-- Written by Shaun
-- Based on code Written by Aaron Garbut, Adam Fowler, Greg Smith
-- Added Not Exported watermark

backgroundcolor = [128,128,128]

tempDir = sysInfo.tempdir
if "" == tempDir then tempDir = "C:\\"
--tempDir = "C:\\test\\"
idxDontExport = getattrindex "Gta Object" "Dont Export"

sel = selection as array

print "tempdir"
print tempDir

if sel.count == 1 then
(
	obj = sel[1]
	max hide inv
			
	savepos = obj.pos
	obj.pos = [0,0,0]
	
	if getattr obj idxDontExport == true then (
				
		textObj = text pos:[0,0,7]
		textObj.name = obj.name
		theRot = eulerangles 90 0 -27.5
		rotate textObj theRot
		textObj.render_renderable = true
		textObj.render_thickness = 0.1
		textObj.text = "Not Exported"
		textObj.size = 3.0
		textObj.wireColor = [255,0,0] as color
		
	)
				max tool zoomextents
	filenameJpg = tempDir + obj.name + ".jpg" 
	bitmapsave = render outputwidth:512 outputheight:512 vfb:false	bitmapsave.filename = filenameJpg
	save bitmapsave
	
	max unhide all

	obj.pos = savepos
	
	print textObj
	if textObj != undefined then delete textObj
)
