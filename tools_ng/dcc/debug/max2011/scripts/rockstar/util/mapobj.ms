idxExportGeometry = getattrindex "Gta Object" "Export Geometry"

--------------------------------------------------------------
-- returns true if the passed in object is a valid map export object
--------------------------------------------------------------
fn IsMapObject obj = (

	retval = true

	if isDeleted obj then (
	
		retval = false
	)
	else if classof obj == XRefObject then (
	
		retval = false
	)
	else if classof obj == InternalRef then (
	
		retval = false
	)
	else if obj.parent != undefined then (
	
		if ( classof obj.parent != Gta_MILO ) and 
			( classof obj.parent != GtaMloRoom ) and
			( classof obj.parent != Container ) then (
		
			retval = false
		)
	)
	else if classof obj == Gta_MILO then (
	
		retval = true
	)
	else if classof obj == SpeedTree_4 then (
	
		retval = true
	)
	else if classof obj == GtaMloRoom then (
	
		retval = true
	)
	else if classof obj == Container then (
		
		retval = true
	)
	else if getattrclass obj != "Gta Object" then (
	
		retval = false
	)
	else if retval == true then (
	
		idxFragProxy = getattrindex "Gta Object" "Is FragProxy"
		valFragProxy = getattr obj idxFragProxy 
		
		idxAnimProxy = getattrindex "Gta Object" "Is AnimProxy"
		isAnimProxy = getattr obj idxAnimProxy 
		
		if valFragProxy == true or isAnimProxy == true then (
			
			retval = false
		) else (
		
			idxDontExport = getattrindex "Gta Object" "Dont Export"
			idxDontAddToIpl = getattrindex "Gta Object" "Dont Add To IPL"
			valDontExport = getattr obj idxDontExport

			if valDontExport == true then (

				setattr obj idxDontAddToIpl true
				retval = false
			)
		)
	)	
	
	retval
)

--------------------------------------------------------------
-- returns true if the passed in object is a valid map export object
--------------------------------------------------------------
fn IsMapObjectWithXref obj = (

	if isDeleted obj then return false
	isXref = (classof obj == XRefObject)
	
	if obj.parent != undefined then (
	
		if classof obj.parent != Gta_MILO and classof obj.parent != GtaMloRoom then (
		
			return false
		)
	)
	
	if classof obj == Gta_MILO then return true
	if classof obj == GtaMloRoom then return true
	if getattrclass obj != "Gta Object" and getattrclass obj != "Gta MILOTri" then return false
	
	if isXref == false and getattrclass obj == "Gta Object" then (
	
		idxFragProxy = getattrindex "Gta Object" "Is FragProxy"
		valFragProxy = getattr obj idxFragProxy 
		
		if valFragProxy == true then return false

		idxAnimProxy = getattrindex "Gta Object" "Is AnimProxy"
		isAnimProxy = getattr obj idxAnimProxy 
		
		if isAnimProxy == true then return false
		
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		valDontExport = getattr obj idxDontExport
	
		if valDontExport == true then return false
	)
	
	return true
)


fn RsGetMapObjectsFromRoom source target childobj = (
 
	for actualobj in childobj.children do (

		if IsMapObject actualobj then (

			append target actualobj
		)
	)
)

RsMapObjectSourceCache = #()
RsMapObjectCache = #()

--------------------------------------------------------------
-- Turn all objects in the input list into valid export objects
--------------------------------------------------------------
fn RsGetMapObjects source target exportGeometryCheck:false isCutsceneObject:false miloHelpers:#() = (
	
	if source.count == RsMapObjectSourceCache.count and isCutsceneObject == false then (
		
		for item in RsMapObjectCache do (
		
			if isdeleted item == false then (
		
				append target item
			)
		)
	) else (

		for obj in source do (

			if IsMapObject obj then (

				if classof obj == Gta_MILO then (
					append miloHelpers obj
					lodobj = GetLodAttrParent obj
					
					if lodobj != undefined then (
						append target lodobj 
						lod2obj = GetLodAttrParent lodobj
						if lod2obj != undefined then append target lod2obj
					)
					
					for childobj in obj.children do (

						if classof childobj == GtaMloRoom then (

							RsGetMapObjectsFromRoom source target childobj						
						) else (

							if IsMapObject childobj then (

								append target childobj 
							)
						)
					)

				) else if classof obj == GtaMloRoom then (

					RsGetMapObjectsFromRoom source target obj
				) else if classof obj == Container then (
				
					append target obj
					
					-- Recurse into the Container.
					for childobj in obj.children do (
						
						if IsMapObject childobj then (
						
							append target childobj
						)
					)
					
				) else if isdeleted obj == false then (

					addObject = true

					if getattrclass obj == "Gta Object" then (

						addObject = getattr obj idxExportGeometry
					)

					if addObject then (
					
						append target obj
					)
				)
			)
		)		
		
		RsMapObjectSourceCache = #() + source
		RsMapObjectCache = #() + target
	)
)

fn RsGetMapObjectsFromRoomWithXref source target childobj = (
 
	for actualobj in childobj.children do (

		if IsMapObjectWithXref actualobj then (

			append target actualobj
		)
	)
)

--------------------------------------------------------------
-- Turn all objects in the input list into valid export objects
--------------------------------------------------------------
fn RsGetMapObjectsWithXrefs source target = (

	for obj in source do (

		if IsMapObjectWithXref obj then (

			if classof obj == Gta_MILO then (

				for childobj in obj.children do (

					if classof childobj == GtaMloRoom then (

						RsGetMapObjectsFromRoomWithXref source target childobj						
					) else (
					
						if IsMapObject childobj then (
	
							append target childobj 
						)
					)
				)

			) else if classof obj == GtaMloRoom then (
			
				RsGetMapObjectsFromRoomWithXref source target obj
			) else (

				append target obj
			)
		)
	)		
)
