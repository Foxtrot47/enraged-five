-- Rockstar Rage Report
-- Rockstar North
-- 26/1/2007
-- by Greg Smith

RsReportFilename = RsConfigGetBinDir() + "report.txt"
RsRagebuilderReport = #()
RsCountVal = 10
RsRetVal = true

fn RsRunRageScripWithReport scriptFilename = (

	RsRetVal = true

	cmdline = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + scriptFilename + " -buildlog " + RsReportFilename
		
	if doscommand cmdline != 0 then (

		messagebox ("failed to run script: " + scriptFilename)
		return false
	)

	reportFile = openfile RsReportFilename mode:"r"

	if reportFile != undefined then (

		RsRagebuilderReport = #()
		RsCountVal = 10
		
		while eof reportFile == false do (
		
			reportFileLine = readline reportFile
			append RsRagebuilderReport reportFileLine
		)

		close reportFile

		rollout RsRageScriptReport "Ragebuilder Output"
		(
			listbox lstMissing items:RsRagebuilderReport height:20
			checkbox chkFreeze "Freeze" pos:[50,280]
			button btnOK "OK" width:100 pos:[150,280]
			button btnCancel "Cancel" width:100 pos:[250,280]
			button btnSave "Save" width:100 pos:[350,280]
			timer tmrCount

			on btnOK pressed do (

				RsRetVal = true
				DestroyDialog RsRageScriptReport
			)	
			
			on btnCancel pressed do (

				RsRetVal = false
				DestroyDialog RsRageScriptReport
			)		
			
			on btnSave pressed do (
			
				chkFreeze.checked = true
				saveFilename = getsavefilename caption:"choose save location" filename:"output.log" types:"log file (*.log)|*.log"
				
				if saveFilename != undefined then (
				
					copyfile RsReportFilename saveFilename
				)
			)
			
			on tmrCount tick do (

				if chkFreeze.checked == false then (

					if RsCountVal == 0 then (

						RsRetVal = true
						DestroyDialog RsRageScriptReport 
					) else (

						RsCountVal = RsCountVal - 1
						btnOK.text = "OK (" + (RsCountVal as string) + ")"
					)
				)
			)
			
			on RsRageScriptReport open do (

				btnOK.text = "OK (" + (RsCountVal as string) + ")"
			)
		)

		CreateDialog RsRageScriptReport width:600 modal:true
	)
	
	RsRetVal
)

fn RsRunRageScripWithoutReport scriptFilename = (

	RsRetVal = true

	cmdline = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + scriptFilename
		
	if doscommand cmdline != 0 then (

		messagebox ("failed to run script: " + scriptFilename)
		RsRetVal = false
	)

	RsRetVal
)