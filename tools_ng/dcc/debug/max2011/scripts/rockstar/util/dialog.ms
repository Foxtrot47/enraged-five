-- Rockstar Dialog
-- Rockstar North
-- 1/3/2005
-- by Greg Smith

-- Some useful dialogs

fn RsQueryBox message edittext:"" = (

	global returnVal = ""
	global inVal = edittext

	rollout RsTheQueryBox message
	(
		edittext edtItem "" text:inVal
		button btnOK "OK" width:100 pos:[90,40]
		button btnCancel "Cancel" width:100 pos:[210,40]

		on btnOK pressed do (

			returnVal = edtItem.text
			DestroyDialog RsTheQueryBox 
		)	

		on btnCancel pressed do (

			DestroyDialog RsTheQueryBox 
		)	
	)

	CreateDialog RsTheQueryBox width:300 modal:true
	
	return returnVal
)

fn RsQueryBoxTimeOut message timeout:60 = (

	returnval = true
	global countVal = timeout
	global lblVal = message

	rollout RsTheQueryBox "Query"
	(
		label lblMsg "test"
		button btnOK "Yes" width:100 pos:[50,40]
		button btnCancel "No" width:100 pos:[160,40]
		timer tmrCount

		on btnOK pressed do (

			DestroyDialog RsTheQueryBox 
		)	

		on btnCancel pressed do (

			returnval = false
			DestroyDialog RsTheQueryBox 
		)	
		
		on tmrCount tick do (
		
			if countVal == 0 then (
				
				DestroyDialog RsTheQueryBox 
			) else (
		
				countVal = countVal - 1
				btnOK.text = "Yes (" + (countVal as string) + ")"
			)
		)
		
		on RsTheQueryBox open do (
			
			lblMsg.text = lblVal
			btnOK.text = "Yes (" + (countVal as string) + ")"
		)
	)

	CreateDialog RsTheQueryBox width:300 modal:true
	
	return returnVal
)

fn RsQueryBoxAll message = (

	returnval = #yes
	global countVal = timeout
	global lblVal = message

	rollout RsTheQueryBox "Query"
	(
		label lblMsg "test"
		button btnYes "Yes" width:100 pos:[285,40]
		button btnYesAll "Yes To All" width:100 pos:[395,40]
		button btnNo "No" width:100 pos:[505,40]
		button btnNoAll "No To All" width:100 pos:[615,40]

		on btnYes pressed do (

			DestroyDialog RsTheQueryBox 
		)	

		on btnYesAll pressed do (

			returnval = #yesall
			DestroyDialog RsTheQueryBox 
		)	
		
		on btnNo pressed do (

			returnval = #no
			DestroyDialog RsTheQueryBox 
		)	

		on btnNoAll pressed do (

			returnval = #noall
			DestroyDialog RsTheQueryBox 
		)			
		
		on RsTheQueryBox open do (
			
			lblMsg.text = lblVal
		)
	)

	CreateDialog RsTheQueryBox width:1000 modal:true
	
	return returnVal
)
