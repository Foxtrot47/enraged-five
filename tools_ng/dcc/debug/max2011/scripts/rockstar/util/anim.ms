-- Rockstar Anim Utility
-- Rockstar North
-- 26/7/2005
-- by Greg Smith

-- Set of utility functions for dealing with animations

--------------------------------------------------------------
-- add all the nodes of a hierarchy into a list
--------------------------------------------------------------
fn RsAddHierarchyToList obj addList = (
	
	append addList obj
	
	for childobj in obj.children do (
	
		RsAddHierarchyToList childobj addList
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------	
fn RsCreateSpecFileRec boneval specfile blendShapeOnly foranim formap = (

	if (formap == true) or boneval.isSelected then (

		format "		<Item>\n" to:specfile
		format "			<nameExpr>%</nameExpr>\n" (RsRemoveSpaces boneval.name) to:specfile
		format "			<bIncludeChildren value=\"false\"/>\n" to:specfile
		format "			<Tracks>\n" to:specfile

		transVal = getuserprop boneval "exportTrans"
		bonetag = getuserprop boneval "tag"
		
		--toleranceTransVal = getuserprop boneval "exportTransTolerance"
		--toleranceRotVal = getuserprop boneval "exportRotTolerance"

		if blendShapeOnly == false and bonetag != "DO_NOT_EXPORT" then (
		--if true then (

			if transVal == true then (

				format "				<Item>\n" to:specfile
				format "					<inputName>translate</inputName>\n" to:specfile
				format "					<outputName>trans</outputName>\n" to:specfile
				format "					<components>X,Y,Z</components>\n" to:specfile
				
				/*
				if toleranceTransVal != undefined then (
				
					format "					<compressionTol value=\"%\" />\n" toleranceTransVal to:specfile
				)
				*/
								
				format "				</Item>\n" to:specfile
			)

			format "				<Item>\n" to:specfile
			format "					<inputName>rotate</inputName>\n" to:specfile
			format "					<outputName>rot</outputName>\n" to:specfile
			format "					<components>X,Y,Z</components>\n" to:specfile
			
			/*
			if toleranceRotVal != undefined then (

				format "					<compressionTol value=\"%\" />\n" toleranceRotVal to:specfile
			)
			*/
			
			format "				</Item>\n" to:specfile
			
			if foranim then (
			
				for i = 0 to 7 do (
	
					outputName = "projectData" + (i as string)	
	
					format "				<Item>\n" to:specfile
					format "					<inputName>%</inputName>\n" outputName to:specfile
					format "					<outputName>%</outputName>\n" outputName to:specfile
					format "					<components>X,Y,Z</components>\n" to:specfile
					format "				</Item>\n" to:specfile
				)
			)
		)
		
		format "			</Tracks>\n" to:specfile
		format "		</Item>\n" to:specfile			
	)

	for childobj in boneval.children do (

		RsCreateSpecFileRec childobj specfile blendShapeOnly foranim formap
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------	
fn RsCreateSpecFileNew boneroot filename blendShapeOnly foranim formap = (

	specfile = openfile filename mode:"w+"

	format "<rage__AnimExportCtrlSpec>\n" to:specfile
	format "	<TrackSpecs>\n" to:specfile

	if boneroot.isSelected then (

		format "		<Item>\n" to:specfile
		format "			<nameExpr>root</nameExpr>\n" to:specfile
		format "			<bIncludeChildren value=\"false\"/>\n" to:specfile
		format "			<Tracks>\n" to:specfile

		if blendShapeOnly == false then (
		--if true then (

			format "				<Item>\n" to:specfile
			format "					<inputName>translate</inputName>\n" to:specfile
			format "					<outputName>trans</outputName>\n" to:specfile
			format "					<components>X,Y,Z</components>\n" to:specfile
			format "				</Item>\n" to:specfile
			format "				<Item>\n" to:specfile
			format "					<inputName>rotate</inputName>\n" to:specfile
			format "					<outputName>rot</outputName>\n" to:specfile
			format "					<components>X,Y,Z</components>\n" to:specfile
			format "				</Item>\n" to:specfile
		)

		for i = 0 to 7 do (

			outputName = "projectData" + (i as string)	

			format "				<Item>\n" to:specfile
			format "					<inputName>%</inputName>\n" outputName to:specfile
			format "					<outputName>%</outputName>\n" outputName to:specfile
			format "					<components>X,Y,Z</components>\n" to:specfile
			format "				</Item>\n" to:specfile
		)

		format "			</Tracks>\n" to:specfile
		format "		</Item>\n" to:specfile
	)

	if blendShapeOnly == false then (
	--if true then (

		for childobj in boneroot.children do (

			RsCreateSpecFileRec childobj specfile blendShapeOnly foranim formap
		)
	)

	format "		<Item>\n" to:specfile
	format "			<nameExpr>*</nameExpr>\n" to:specfile
	format "			<bIncludeChildren value=\"false\"/>\n" to:specfile
	format "			<Tracks>\n" to:specfile
	
	if blendShapeOnly == false then (
	--if true then (
	
		format "				<Item>\n" to:specfile
		format "					<inputName>rotate</inputName>\n" to:specfile
		format "					<outputName>rot</outputName>\n" to:specfile
		format "					<components>X,Y,Z</components>\n" to:specfile
		format "				</Item>\n" to:specfile
	)
	
	format "			</Tracks>\n" to:specfile
	format "		</Item>\n" to:specfile

	format "	</TrackSpecs>\n" to:specfile
	format "</rage__AnimExportCtrlSpec>\n" to:specfile

	close specfile
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------	
fn RsCreateSpecFile boneroot filename foranim:false formap:false = (

	(RsCreateSpecFileNew boneroot filename false foranim formap)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------		
fn RsGetBoneListInner objnode boneList = (

	if objnode == undefined then (
	
		return 0
	)

	if classof objnode != Col_Capsule and classof objnode != Col_Mesh and classof objnode != Col_Box then (
	
		nameCheck = RsLowercase objnode.name
	
		subnameCheck = ""
		
		if nameCheck.count >= 3 then (
	
			subnameCheck = (substring nameCheck (nameCheck.count - 2) 3)
		)	
	
		if (findstring namecheck "footsteps" == undefined) and (subnameCheck != "nub") then (
		
			append boneList objnode
		)
	)

	for childobj in objnode.children do (
	
		RsGetBoneListInner childobj boneList
	)
)


--------------------------------------------------------------
-- 
--------------------------------------------------------------		
fn RsGetBoneList objnode boneList = (

	RsGetBoneListInner objnode boneList
)

fn RsCreateDummyAndMover charbone =
(
		--Get selected Biped part
		object = charbone

		--get COM name
		bipname = biped.getNode object #horizontal

		--Delete any existing Dummy or Mover Nodes
		moverNode = getnodebyname "mover" exact:true
		if moverNode != undefined then delete moverNode
		dummyNode = getnodebyname "Dummy01" exact:true
		if dummyNode != undefined then delete dummyNode
		axisNode = getnodebyname "axis_helper" exact:true
		if axisNode != undefined then delete axisNode
		layer = layermanager.getlayerfromname "Mover Node"
		if layer != undefined then layermanager.deleteLayerByName "Mover Node"

		--move slider to start
		slidertime = animationrange.start

		--get root bone z height
		bipname.transform.controller.figureMode = true
		z_height = bipname.transform[4].z
		bipname.transform.controller.figureMode = false

		--create Dummy node
		newDummyNode = Dummy()
		newDummyNode.name = "Dummy01"
		newDummyNode.boxsize = [.2,.2,.2]
		newDummyNode.pos = [0.0,0.0,z_height]

		--parent biped to it
		bipname.parent = newDummyNode

		--create Mover Node
		newMoverNode = Dummy()
		newMoverNode.name = "mover"
		newMoverNode.boxsize = [.3,.3,.3]
		newMoverNode.pos = [0.0,0.0,z_height]

		--create axis helper node
		axis_helper = Point pos:[0,0,1] isSelected:off cross:off axistripod:on size:0.25 name:"axis_helper"
		setTransformLockFlags axis_helper #all

		--parent nodes correctly
		newMoverNode.parent = newDummyNode
		axis_helper.parent = newMoverNode

		--create a new layer for Dummy and Mover nodes 
		layer = layermanager.newlayerfromname "Mover Node"
		layer.addnode axis_helper
		layer.addnode newMoverNode 
		layer.addnode newDummyNode
)

fn RsUpdateMover xp yp zp yaw pitch roll = 
(
	--check selection
	if (($mover != undefined) and ($mover.name == "mover")) then
	(	
		select $mover

		dummyNode = selection[1].parent

		biproot=$char.controller

		for child in dummyNode.children do
		(
			if classof child == biped_object then biproot = child
		)

		biproot.transform.controller.figuremode = true
		figureModeBipRot = biproot.transform as eulerAngles
		figureModeBipPos = biproot.transform[4]
		biproot.transform.controller.figuremode = false

		--delete all the keys on the mover track
		deleteKeys $.controller #allKeys

		dividorLimit = (ceil (((animationrange.end.frame - animationrange.start.frame) as float) / 50)) as integer

		with animate on
		(
			dividorCount = 0
			for i = animationrange.start to animationrange.end do
			(

				if dividorCount == 0 then (

					slidertime = i

					--get position and rotation
					bipPos = biped.getTransform biproot #pos
					bipRot = (biped.getTransform biproot #rotation) as eulerAngles

					--translate mover node
					if (xp ==true) then
					(		
						$mover.pos.x = bipPos.x
					)
					else
					(
						$mover.pos.x =0.0
					)

					if (yp == true) then
					(		
						$mover.pos.y = bipPos.y
					)
					else
					(
						$mover.pos.y =0.0
					)

					if (zp == true) then
					(		
						$mover.pos.z = bipPos.z
					)
					else
					(
						$mover.pos.z = figureModeBipPos.z
					)

					-- rotate mover node
					if (pitch == true) then
					(		
						$mover.rotation.x_rotation = 0.0
					)
					else
					(
						$mover.rotation.x_rotation=0.0
					)

					if (roll == true) then
					(		
						$mover.rotation.y_rotation = 0.0
					)
					else 
					(
						$mover.rotation.y_rotation = 0.0
					)

					if (yaw == true) then
					(		
						$mover.rotation.z_rotation = bipRot.z + figureModeBipRot.z
					)
					else
					(
						$mover.rotation.z_rotation = 0.0
					)
				)
				dividorCount = dividorCount + 1
				if dividorCount == dividorLimit then dividorCount = 0
			)
		)
	)
	else
	(
		messagebox "Invalid mover"
	)
)