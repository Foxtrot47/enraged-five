tool foo
(
	local sel
	local newCylinder
	local startMousePos = [0,0]
	local startCylHeight
	local startCylWidth
	local startCylSides
	on mousePoint clickno do
	(
		if clickno==1 and $!=undefined and $.mesh!=undefined then
		(
			sel=$ 
			if $.mesh.selectedFaces.count<=1 then
			(
				print ("select a face on "+sel.name)
				return false
			)
			if subObjectLevel==4 then
			(
 				local normals =
					for faceindex in $.mesh.selectedFaces collect (getFaceNormal $.mesh faceindex.index)
				local middle = [0,0,0]
				for faceindex in $.mesh.selectedFaces do 
				(
					local verts = getFace $.mesh faceindex.index
					middle += getVert $.mesh verts[1]
					middle += getVert $.mesh verts[2]
					middle += getVert $.mesh verts[3]
--					print (getVert $.mesh verts[1])
				)
				local normal = [0,0,0]
				for n in normals do normal += n
				normal = normalize normal
				middle = middle/($.mesh.selectedFaces.count * 3)
--				print normal
				newCylinder = cylinder()
				move newCylinder ($.pos+middle)
				rotate newCylinder (((MatrixFromNormal normal)*$.transform) as eulerAngles)
				
				startMousePos = mouse.screenpos
				startCylHeight = newCylinder.height
				startCylWidth = newCylinder.radius
				startCylSides = newCylinder.sides
			)
			else print ("select a face on "+sel.name)
		)
		else if clickno<4 then
		(
			if clickno==3 then 
			startMousePos = mouse.screenpos
		)
		else if clickno>4 then #stop
	)
	
	on mouseMove clickno do 
	(
		if newCylinder!=undefined then
		(
--			print clickno
			if clickno==2 then
			(
				newCylinder.height  = startCylHeight + (startMousePos.y - mouse.screenpos.y)
			)
			else if clickno==3 then
			(
				newCylinder.radius  = startCylWidth + (startMousePos.y - mouse.screenpos.y)
			)
			else if clickno==4 then
			(
				newCylinder.sides  = startCylSides + (startMousePos.y - mouse.screenpos.y)/2
			)
			else #stop
		)
		else #stop
	)
	on mouseAbort arg do
	(
		delete newCylinder
		#stop
	)

)

startTool foo
