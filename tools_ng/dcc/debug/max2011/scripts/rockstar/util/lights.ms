-- Rage light functions
-- Rockstar North
-- 16/5/2005
-- by Greg Smith

-- various light utility functions

--------------------------------------------------------------
-- returns all the lights in the scene
--------------------------------------------------------------
fn RsGetAllLights = (

	alllights = #()
	
	for obj in rootnode.children do (

		if superclassof obj == light then (
		
			append alllights obj
		)
	)
	
	return alllights
)

--------------------------------------------------------------
-- qsort function that sort by the distance from QsortObject
--------------------------------------------------------------
global QsortObject = undefined
fn RsQsortLightsByDistance v1 v2 = (

	length1 = length (QsortObject.pos - v1.pos)
	length2 = length (QsortObject.pos - v2.pos)
	
	if length1 < length2 then (
	
		return -1
	)
	
	if length1 > length2 then (
	
		return 1
	)
	
	return 0
)

--------------------------------------------------------------
-- returns the specified number of lights that are nearest the specified obj
--------------------------------------------------------------
fn RsGetNearestLights number obj = (

	retLights = #()
	alllights = RsGetAllLights()
	
	QsortObject = obj
	
	qsort alllights RsQsortLightsByDistance

	for i = 1 to number do (
	
		if alllights.count < i then (
		
			exit
		)
		
		append retLights alllights[i]
	)
	
	return retLights
)