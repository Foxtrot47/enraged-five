--
-- assetsizes.ms
-- Asset Size Utilities
-- David Muir <david.muir@rockstarnorth.com>
--

include "pipeline/util/IdeUtils.ms"

-- Load IDE data (required by these functions)
global ideFileList = #()
global ideObjectList = #()
GtaLoadIDEFiles GTA_MapsRoot &ideFileList &ideObjectList

-------------------------------------------------------------------------------------------
-- name:		RsAssetDrawableMemSize
-- description: Fetch information regarding Drawable (IDR) asset sizes for Max objects
--				
--
-- [in]	 objs		 	List of Max objects to fetch information about
-- [in]  visOnly		Only process visible objects (boolean)
-- [in]	 inclXRefs		Include XRefs (boolean)
-- [in[  progress		Progress bar to update (can be undefined)
-- [out] idrNameList	List of IDR names
-- [out] idrSizeList	List of IDR file sizes
-- [out] idrFileList	List of IDR files
-------------------------------------------------------------------------------------------
fn RsAssetDrawableMemSize objs visOnly inclXRefs progress &idrNameList &idrSizeList &idrFileList = (

	idrNameList = #()
	idrSizeList = #()
	idrFileList = #()
	local i = 1

	for o in objs do
	(
		if ( undefined != progress ) then
			progress.value = 100.0 * i / objs.count
		i += 1

		-- Skip this object if its hidden and we are only processing
		-- visible objects
		if ( visOnly and o.isHidden ) then
			continue			

		if ( ( XRefObject == classof o ) and ( "Gta Object" == GetAttrClass o ) and ( inclXRefs ) ) then
		(
			local idrPathName = ( RsRemovePathAndExtension( o.filename ) )
			local idrName = o.ObjectName

			if ( 0 == findItem idrNameList idrName ) then
			(
				-- Store IDR name in list
				append idrNameList idrName

				-- Store IDR size in list
				idrFile = stringStream ""
				format "N:/streamGTA/maps/%/%.idr" idrPathName idrName to:idrFile
				idrFileSize = ( getfilesize ( idrFile as string ) )

				-- Maybe IDR is local only
				if ( 0 == idrFileSize ) then
				(
					idrFile = stringStream ""
					format "X:/streamGTA/maps/%/%.idr" idrPathName idrName to:idrFile
					idrFileSize = ( getfilesize ( idrFile as string ) )

					-- Have IDR filesize to add it
					append idrSizeList ( getfilesize ( idrFile as string ) )

					-- Store IDR filename
					append idrFileList ( idrFile as string )
				)
				else
				(
					-- Have an IDR so add it
					append idrSizeList idrFileSize

					-- Store IDR filename
					append idrFileList ( idrFile as string )
				)
			)
		)
		else if ( ( XRefObject != classof o ) and "Gta Object" == ( GetAttrClass o ) ) then
		(
			local idrName = o.name
			if ( 0 == findItem idrNameList idrName ) then
			(
				-- Store IDR name in list
				append idrNameList idrName

				-- Store IDR size in list
				idrFile = stringStream ""
				format "N:/streamGTA/maps/%/%.idr" ( RsRemovePathAndExtension(maxFilename) ) idrName to:idrFile
				idrFileSize = ( getfilesize ( idrFile as string ) )

				-- Try IDD
				if ( 0 == idrFileSize ) then
				(
					local iddGroupAttrIndex = GetAttrIndex "Gta Object" "Model Group"
					iddModelGroup = GetAttr o iddGroupAttrIndex

					idrFile = stringStream ""
					format "N:/streamGTA/maps/%/%.idd" ( RsRemovePathAndExtension(maxFilename) ) iddModelGroup to:idrFile
					idrFileSize = ( getfilesize ( idrFile as string ) )

					-- Have an IDD so add it
					append idrSizeList idrFileSize

					-- Store IDD filename
					append idrFileList ( idrFile as string )
				)
				else
				(
					-- Have an IDR so add it
					append idrSizeList idrFileSize

					-- Store IDR filename
					append idrFileList ( idrFile as string )
				)
			)		
		) -- End of Gta Objects			

	) -- End of for each object
) -- End of RsAssetDrawableMemSize function


-------------------------------------------------------------------------------------------
-- name:		RsAssetTXDMemSize
-- description: Fetch information regarding Texture Dictionary (TXD) asset sizes for Max objects
--				(includes XRefs)
--
-- [in]	 objs		 	List of Max objects to fetch information about
-- [in]  visOnly		Only process visible objects (boolean)
-- [in]	 inclXRefs		Include XRefs (boolean)
-- [in[  progress		Progress bar to update (can be undefined)
-- [out] txdNameList	List of IDR names
-- [out] txdSizeList	List of IDR file sizes
-- [out] txdFileList	List of IDR files
-- [out] txdErrorList	List of errors found whilst collecting this information
-------------------------------------------------------------------------------------------
fn RsAssetTXDMemSize objs visOnly inclXRefs progress &txdNameList &txdSizeList &txdFileList &txdErrorList = (

	txdNameList = #()
	txdSizeList = #()
	txdFileList = #()
	txdErrorList = #()
	local txdAttrIndex = GetAttrIndex "Gta Object" "TXD"
	local i = 1

	for o in objs do
	(
		if ( undefined != progress ) then
			progress.value = 100.0 * i / objs.count
		i += 1		

		-- Skip this object if its hidden and we are only processing
		-- visible objects
		if ( visOnly and o.isHidden ) then
			continue	

		if ( ( XRefObject == classof o ) and ( "Gta Object" == GetAttrClass o ) and ( inclXRefs ) ) then
		(
			-- Get TXD name attribute, comes from IDE file instead of using GetAttr
			local ideObject = GtaFindIDEObject ideObjectList o.objectName
			if ( undefined == ideObject ) then
			(
				local error = stringStream ""
				format "Error: % XRef IDE Object not found for TXD lookup." o.objectName to:error
				if ( 0 == findItem txdErrorList ( error as string ) ) then
					append txdErrorList ( error as string )
				continue
			)
			else if ( "null" == ideObject.txd ) then
			(
				local error = stringStream ""
				format "Error: % XRef IDE Object has null TXD name." o.objectName to:error
				if ( 0 == findItem txdErrorList ( error as string ) ) then
					append txdErrorList ( error as string )
				continue
			)


			local txdName = ideObject.txd
			local txdPathname = ( RsRemovePathAndExtension( o.filename ) )

			if ( ( "CHANGEME" != txdName ) and 0 == ( findItem txdNameList txdName ) ) then
			(
				-- Store TXD name in list
				append txdNameList txdName

				-- Store TXD size in list
				txdFile = stringStream ""
				format "N:/streamGTA/maps/%/%.itd" txdPathName txdName to:txdFile
				txdFileSize = ( getfilesize ( txdFile as string ) )

				-- Maybe ITD is local only
				if ( 0 == txdFileSize ) then
				(
					txdFile = stringStream ""
					format "X:/streamGTA/maps/%/%.itd" txdPathName txdName to:txdFile
					txdFileSize = ( getFileSize ( txdFile as string ) ) 

					-- Have ITD so add it
					append txdSizeList ( getfilesize ( txdFile as string ) )

					-- Store ITD filename
					append txdFileList ( txdFile as string )

				)
				else
				(
					-- Have ITD so add it
					append txdSizeList ( getfilesize ( txdFile as string ) )

					-- Store IDR filename
					append txdFileList ( txdFile as string )
				)
			)

		)
		else if ( ( XRefObject != classof o ) and ( "Gta Object" == GetAttrClass o ) ) then
		(

			-- Get TXD name attribute
			local txdName = ( GetAttr o txdAttrIndex )

			if ( ( "CHANGEME" != txdName ) and 0 == ( findItem txdNameList txdName ) ) then
			(
				-- Store TXD name in list
				append txdNameList txdName

				-- Store TXD size in list
				txdFile = stringStream ""
				format "N:/streamGTA/maps/%/%.itd" ( RsRemovePathAndExtension(maxFilename) ) txdName to:txdFile
				append txdSizeList ( getfilesize ( txdFile as string ) )

				-- Store TXD file in list
				append txdFileList ( txdFile as string )
			)
		)

	) -- End of for each object
)

-- End of script
