--
-- File:: rockstar/util/matmapset.ms
-- Description:: Rockstar Material Map Set
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 16/9/2005
--
-----------------------------------------------------------------------------
-- -1 unset
-- 0 set all off
-- 1 set all on
-- 2 set both

RsMapEnables = #(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1)

rollout RsMaterialMapSetRoll "Material Map Set"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Change_Shaders#Material_Map_Set" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	

	checkbox chkAmbient "Ambient" across:3
	checkbox chkDiffuse "Diffuse"
	checkbox chkSpecColour "Specular Colour"
	checkbox chkSpecLevel "Specular Level" across:3
	checkbox chkGlossiness "Glossiness"
	checkbox chkSelfIllum "Self Illumination"
	checkbox chkOpacity "Opacity" across:3
	checkbox chkFilterColour "Filter Colour"
	checkbox chkBump "Bump"
	checkbox chkReflection "Reflection" across:3
	checkbox chkRefraction "Refraction"
	checkbox chkDisplacement "Displacement"
	button butGet "Get" across:2
	button butSet "Set"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	fn GetMapSettingsFromMaterial mat = (
	
		if classof mat == Standardmaterial then (
		
			for i = 1 to RsMapEnables.count do (
			
				if mat.mapEnables[i] then (
				
					if RsMapEnables[i] == -1 then (
					
						RsMapEnables[i] = 1
					) else (
					
						if RsMapEnables[i] == 0 then (
						
							RsMapEnables[i] = 2
						)
					)
				) else (

					if RsMapEnables[i] == -1 then (
					
						RsMapEnables[i] = 0
					) else (
					
						if RsMapEnables[i] == 1 then (
						
							RsMapEnables[i] = 2
						)
					)			
				)
			)
			
			return 0
		)
		
		if classof mat == Multimaterial then (
		
			for submat in mat.materialList do (
			
				GetMapSettingsFromMaterial submat
			)
			
			return 0
		)
	)
	
	fn GetMapSettings selset = (
	
		for obj in selset do (
		
			GetMapSettingsFromMaterial obj.material
			
			GetMapSettings obj.children
		)
	)

	fn SetMapSettingsForMaterial mat = (
	
		if classof mat == Standardmaterial then (
		
			for i = 1 to RsMapEnables.count do (
			
				if RsMapEnables[i] != 2 then (
				
					if RsMapEnables[i] == 0 then (
					
						mat.mapEnables[i] = false
					)
					
					if RsMapEnables[i] == 1 then (

						mat.mapEnables[i] = true					
					)					
				)
			)
			
			return 0
		)
		
		if classof mat == Multimaterial then (
		
			for submat in obj.material.materialList do (
			
				SetMapSettingsForMaterial submat
			)
			
			return 0
		)
	)

	fn SetMapSettings selset = (
	
		for obj in selset do (
		
			SetMapSettingsForMaterial obj.material
			
			SetMapSettings obj.children
		)
	)
	
	fn ClampEnable val = (
	
		if val < 0 then (
		
			return 0
		)
		
		if val > 2 then (
		
			return 2
		)
		
		return val
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on butGet pressed do (
	
		for i = 1 to RsMapEnables.count do (
		
			RsMapEnables[i] = -1
		)
		
		GetMapSettings selection
		
		chkAmbient.triState = ClampEnable(RsMapEnables[1])
		chkDiffuse.triState = ClampEnable(RsMapEnables[2])
		chkSpecColour.triState = ClampEnable(RsMapEnables[3])
		chkSpecLevel.triState = ClampEnable(RsMapEnables[4])
		chkGlossiness.triState = ClampEnable(RsMapEnables[5])
		chkSelfIllum.triState = ClampEnable(RsMapEnables[6])
		chkOpacity.triState = ClampEnable(RsMapEnables[7])
		chkFilterColour.triState = ClampEnable(RsMapEnables[8])
		chkBump.triState = ClampEnable(RsMapEnables[9])
		chkReflection.triState = ClampEnable(RsMapEnables[10])
		chkRefraction.triState = ClampEnable(RsMapEnables[11])
		chkDisplacement.triState = ClampEnable(RsMapEnables[12])
	)
	
	on butSet pressed do (
	
		RsMapEnables[1] = chkAmbient.triState
		RsMapEnables[2] = chkDiffuse.triState
		RsMapEnables[3] = chkSpecColour.triState
		RsMapEnables[4] = chkSpecLevel.triState
		RsMapEnables[5] = chkGlossiness.triState
		RsMapEnables[6] = chkSelfIllum.triState
		RsMapEnables[7] = chkOpacity.triState
		RsMapEnables[8] = chkFilterColour.triState
		RsMapEnables[9] = chkBump.triState
		RsMapEnables[10] = chkReflection.triState
		RsMapEnables[11] = chkRefraction.triState
		RsMapEnables[12] = chkDisplacement.triState

		SetMapSettings selection
	)
)
