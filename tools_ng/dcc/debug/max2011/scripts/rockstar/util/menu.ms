-- Rockstar Util Menu
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- by Luke Openshaw

-- utility functions for creating menus

-- Remove menu from main menu
fn RsEmptyMenu menu = (
	for i = 1 to menu.numItems() do (
		menu.removeItemByPosition 1
	)	
)

fn RsAddMenuItems menu category items = (
	for item in items do (
	
		if item == "" then (
		
			menui = menuman.createSeparatorItem()
		) else (
			
			menui = menuman.createActionItem item category
		)	
		
		if menui != undefined then (
		
			menu.additem menui -1
		)
	)
)

fn RsAddQuadMenuSubItems quadmenu category submenuname items = (
	
	retMenu = menuman.createmenu submenuname
	retMenuItem = menuman.createsubmenuitem submenuname retMenu
	
	for item in items do (
		menui = menuman.createActionItem item category
		retMenu.additem menui -1
	)
	
	quadmenu.additem retMenuItem -1
)

------------------------------------------------------------------------------------
-- try's to find a root menu with the specified name. if it isnt found it creates it
------------------------------------------------------------------------------------
fn RsGetMenu menuName menuParentName = (

	retMenu = menuman.findmenu menuName
	
	if retMenu != undefined do (
	
		menuman.unRegisterMenu retMenu
	)	

	retMenu = menuman.createmenu menuName
	retMenuItem = menuman.createsubmenuitem menuName retMenu

	parentMenu = menuman.getmainmenubar()

	if classof menuParentName == String then (
		parentMenu = menuman.findmenu menuParentName
	)

	parentMenu.additem retMenuItem -1
	
	return retMenu
)

------------------------------------------------------------------------------------
-- add the passed in items to the menu, the item list should be a list of macro names
-- whose categories are the same as the menu name
------------------------------------------------------------------------------------
fn RsSetMenu menuAddTo items menuParentName:undefined = (
	
	if classof menuAddTo == String then (
		menuAddTo = RsGetMenu menuAddTo menuParentName
	)
	
	
	-- make sure the menu is clear
	for i = 1 to menuAddTo.numItems() do (
	
		menuAddTo.removeItemByPosition 1
	)		
	
	RsAddMenuItems menuAddTo (menuAddTo.getTitle()) items
)