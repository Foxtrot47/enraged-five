-- Rockstar Map Export
-- Rockstar North
-- 30/10/2008
-- by Luke Openshaw
-- Utilities for manipulating the terrain shader

filein "rockstar/util/shader_config_loader.ms"

global TexColList = #( color 0 0 0, color 0 0 255, color 0 255 0, color 0 255 255, color 255 0 0, color 255 0 255, color 255 255 0, color 255 255 255 )

fn RsCopyDxMatParamsToRageMatParams ragemat dxmat shader = (
	
	global gmat = dxmat
	textureList = #()
	errorlist = #()

	i = 1
	for shadervar in shader.shadervars do (
		
		try (
			matvar = execute ("gmat." + shadervar.name)
		) catch (
			append errorlist ((getCurrentException()) + " " + shadervar.name )
		)
		if matvar != undefined then (
			-- Check if the variable has changed rather than wholesale copy and gc()
			-- since it seems to leak
			if shadervar.type == "difftex" or shadervar.type == "bumptex" then (
				if matvar.filename != RstGetVariable ragemat (shadervar.id as integer) then
				(
					RstSetVariable ragemat (shadervar.id as integer) matvar.filename
					gc()
				)
			)
			else (
				if matvar != RstGetVariable ragemat (shadervar.id as integer) then
					RstSetVariable ragemat (shadervar.id as integer) matvar
			)
		)
	)
	
)

fn RsDxRageSwitcherooInner mat = (
	dxshadername = RsRemovePathAndExtension mat.effectfile
	
	shader = RsLoadShaderFromXml "terrain" dxshadername	
	RsCopyDxMatParamsToRageMatParams mat.rendermaterial mat shader

	--return mat.rendermaterial
	
)



fn RsDxRageSwitcheroo obj = (
	print ("RsDxRageSwitcheroo called on " + obj.name)
	dxmat = undefined
	if classof obj.mat == MultiMaterial then (
		
		for submat in obj.mat.materiallist do (
			
			if classof submat == DirectX_9_Shader then (
				--dxmat = submat
				--submat = RsDxRageSwitcherooInner submat
				--RsDxRageSwitcherooInner submat
				
			)
		)
	)
	else if classof obj.mat == DirectX_9_Shader then (
		--dxmat = obj.mat
		--obj.mat = RsDxRageSwitcherooInner obj.mat
		RsDxRageSwitcherooInner obj.mat
	)
	--return dxmat
)

fn RsIsTerrainMaterial mat = (
	-- Doing a string based test sucks but Im not sure of another way to determine if
	-- it is a terrain shader
	if classof mat == Rage_Shader then (
		matname = RstGetShadername mat
		if findString matname "terrain_cb" != undefined then return true
	)

	false
)