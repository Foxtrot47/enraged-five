--
-- File:: collutil.ms
-- Description:: Collision utility functions
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- 29/10/09
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Combined in functions from boundsexport.ms
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/material.ms"
filein "rockstar/util/attribute.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsFragBoundList = #()
RsTotalSurfProperty = ""

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

fn RsGetCollMaterialList obj mat collmatlist idx:-1 = (

	if classof mat == Multimaterial then (
		
		matlist = #()
		RsMatGetMapIdsUsedOnObject obj matlist
		
		for i = 1 to matlist.count do (

			matidx = finditem mat.materialIDList matlist[i]

			if matidx != 0 then (

				RsGetCollMaterialList obj mat.materiallist[matidx] collmatlist idx:i
			)
		)		
	) else if classof mat == RexBoundMtl then (
	
		append collmatlist mat
	)
)

fn RsGetCollMatList obj mat collmatlist idx:-1 = (

	if classof mat == Multimaterial then (
		
		matlist = #()
		RsMatGetMapIdsUsedOnObject obj matlist
		
		for i = 1 to matlist.count do (

			matidx = finditem mat.materialIDList matlist[i]

			if matidx != 0 then (

				RsGetCollMatList obj mat.materiallist[matidx] collmatlist idx:i
			)
		)		
	) else if classof mat == RexBoundMtl then (
	
		append collmatlist idx
	)
)

fn RsGetCollMatListUsingFlags obj mat collmatlist flag idx:-1 = (

	if classof mat == Multimaterial then (
		
		matlist = #()
		RsMatGetMapIdsUsedOnObject obj matlist
		
		for i = 1 to matlist.count do (

			matidx = finditem mat.materialIDList matlist[i]

			if matidx != 0 then (

				RsGetCollMatListUsingFlags obj mat.materiallist[matidx] collmatlist flag idx:i
			)
		)		
	) else if classof mat == RexBoundMtl then (
	
		if bit.and (RexGetCollisionFlags mat) flag == flag then (
	
			append collmatlist idx
		)
	)
)

fn RsSetFacesUsingMatByID obj idx = (

	if classof obj == Col_Mesh then (
	
		col2mesh obj
	)
	
	select obj

	facesel = #()

	numFaces = getnumfaces obj

	for j = 1 to numFaces do (

		if classof obj == Editable_Mesh then (

			matID = getfacematid obj j
		) else if classof obj == Editable_Poly or classof obj == PolyMeshObject then (

			matID = polyop.getfacematid obj j
		)

		if matID == idx then (

			append facesel j
		)
	)
	
	setfaceselection obj facesel
	setCommandPanelTaskMode(#modify) 
	subobjectlevel = 3
)

fn RsSetFacesUsingMatByIDList obj idxlist = (

	if classof obj == Col_Mesh then (
	
		col2mesh obj
	)
	
	select obj

	facesel = #()

	numFaces = getnumfaces obj

	for j = 1 to numFaces do (

		if classof obj == Editable_Mesh then (

			matID = getfacematid obj j
		) else if classof obj == Editable_Poly or classof obj == PolyMeshObject then (

			matID = polyop.getfacematid obj j
		)

		if finditem idxlist matID != 0 then (

			append facesel j
		)
	)
	
	setfaceselection obj facesel
	setCommandPanelTaskMode(#modify) 
	subobjectlevel = 3
)

fn RsSetFacesByFlag obj flag = (

	idxlist = #()
	RsGetCollMatListUsingFlags obj obj.material idxlist flag
	RsSetFacesUsingMatByIDList obj idxlist
)

fn RsDoesObjectUseFlag obj flag = (

	collist = #()

	RsGetCollMatList obj obj.material collist
	
	for i in collist do (
	
		matidx = finditem obj.material.materialIDList collist[i]
	
		if bit.and (RexGetCollisionFlags obj.material.materiallist[1]) flag == flag then (
		
			return true
		)
	)
	
	false
)

fn RsDoesCollMatHaveRooms obj = (

	collmatlist = #()
	RsGetCollMaterialList obj obj.material collmatlist
	
	retval = false
	
	for item in collmatlist do (
	
		cobj = RexGetRoomNode item
	
		if retval == false and cobj != undefined and (isdeleted cobj == false) then (
		
			retval = true
		)
	)
	
	retval
)

fn RsDoesObjHaveCollRooms obj = (

	for childobj in obj.children do (
	
		if classof childobj == Col_Mesh then (
		
			if RsDoesCollMatHaveRooms childobj then return true
		)
	)
	
	false
)

-----------------------------------------------------------------------------
-- MWW: Moved over from boundsexport.ms

--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBoundsInner objnode rexroot pedDir = (

	numBones = 0

	if objnode == undefined then (
	
		return 0
	)

	if classof objnode == Col_Capsule or classof objnode == Col_Mesh or classof objnode == Col_Box or classof objnode == Col_Sphere then (

		surfaceType = RsGetCollSurfaceTypeString objnode
		RsTotalSurfProperty = RsTotalSurfProperty + objnode.name + "=" + surfaceType + ":"
		
		append RsFragBoundList objnode
	) else (
	
		nameCheck = RsLowercase objnode.name
	
		subnameCheck = ""
		
		if nameCheck.count >= 3 then (
	
			subnameCheck = (substring nameCheck (nameCheck.count - 2) 3)
		)
	
		if (findstring namecheck "footsteps" == undefined) and (subnameCheck != "nub") then (
		
			numBones += 1
		)
		
--		print numBones
	)

	for childobj in objnode.children do (
	
		numBones += RsSetupFragBoundsInner childobj rexroot pedDir
	)
	
	numBones
)


--------------------------------------------------------------
-- setup the ragdoll version of the ped model's bounds elements
--------------------------------------------------------------		
fn RsSetupFragBounds objnode rexroot pedDir = (

	RsFragBoundList = #()

	ret = RsSetupFragBoundsInner objnode rexroot pedDir
	clearSelection()
	select RsFragBoundList
	
	if selection.count > 0 then (
	
		bound = rexAddChild rexroot selection[1].name "Bound"
		rexSetPropertyValue bound "OutputPath" pedDir
		rexSetPropertyValue bound "ForceExportAsComposite" "false"
		rexSetPropertyValue bound "ExportAsComposite" "false"
		rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
		rexSetPropertyValue bound "BoundExportWorldSpace" "false"
		rexSetPropertyValue bound "SurfaceType" RsTotalSurfProperty
		rexSetNodes bound	
	)
	
	ret
)