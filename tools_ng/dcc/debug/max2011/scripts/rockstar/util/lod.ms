-- Rockstar Lod Utils
-- Rockstar North
-- 12/10/2005
-- by Greg Smith

fn RsGetObjLodDistance obj = (

	if getattrclass obj != "Gta Object" then return 0.0

	valLodDistance = getattr obj (getattrindex "Gta Object" "LOD distance")
	valLodDistanceInst = getattr obj (getattrindex "Gta Object" "Instance LOD distance")
	
	if classof obj == InternalRef and valLodDistanceInst == false then (
	
		linkobj = ixref_gettarget obj
	
		if getattrclass obj == "Gta Object" then (
	
			valLodDistance = getattr obj (getattrindex "Gta Object" "LOD distance")
		)
	)	
	
	valLodDistance
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetRootMostLodObject lobj = (

	parentobj = getlodattrparent lobj
	
	while parentobj != undefined do (
	
		lobj = parentobj
		parentobj = getlodattrparent lobj
	)
	
	lobj
)

--------------------------------------------------------------
-- get all the lod children of a passed in object
--------------------------------------------------------------
fn RsGetLODChildren lodparent = (

	childObjs = #()
	
	lobj = lodparent

	refobjs = refs.dependents lobj

	for refObj in refobjs do (
	
		if isdeleted refObj == false then (
	
			if classof(refObj) == LodAttributes then (			
				
				refobjs2 = refs.dependents refObj
			
				if refobjs2[1] != undefined and isdeleted refobjs2[1] == false then (
			
					append childObjs refobjs2[1]
				)
			)
		)
	)
	
	return childObjs
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetAllLODChildren lodobj output = (

	lodchildren = RsGetLODChildren lodobj
	
	for lobj in lodchildren do (
	
		append output lobj
	)
	
	for lobj in lodchildren do (
	
		RsGetAllLODChildren lobj output
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsAnyLOD lobj = (

	retval = false

	if getattrclass lobj == "Gta Object" then (

		lodChildren = RsGetLODChildren lobj

		if (lodChildren.count > 0) then (

			retval = true
		) 
	)
	
	retval
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsLOD lobj = (

	idxIsMiloLod = getattrindex "Gta Object" "Is MLO LOD"

	retval = false

	if getattrclass lobj == "Gta Object" then (

		lodChildren = RsGetLODChildren lobj
		
		if ((lodChildren.count > 0) and (getattr lobj idxIsMiloLod) == false) then (
		
			retval = true
			
			for childobj in lodChildren do (
			
				childlodChildren = RsGetLODChildren childobj
				
				if childlodChildren.count > 0 then (
				
					retval = false
				)
			)
		)
	)
	
	retval
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsSuperLOD lobj = (

	idxIsMiloLod = getattrindex "Gta Object" "Is MLO LOD"

	retval = false

	if getattrclass lobj == "Gta Object" then (

		lodChildren = RsGetLODChildren lobj
		lodParent = getlodattrparent lobj

		if ((lodChildren.count > 0) and (lodParent == undefined) and (getattr lobj idxIsMiloLod) == false) then (

			isSuperLod = false

			for childobj in lodChildren do (

				lodChildrenChildren = RsGetLODChildren childobj

				if lodChildrenChildren.count > 0 then (

					isSuperLod = true
				)
			)

			if isSuperLod == true then retval = true
		) 
	)
	
	retval	
)