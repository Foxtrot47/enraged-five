-- Settings
-- Rockstar North
-- 2/2/2005

-- allows settings to be saved in a generic way

-------------------------------------------------------------
-- returns the name of the file to save the setting information
-------------------------------------------------------------
fn RsSettingGetIniFile = (
	return "rs.ini"
)

-------------------------------------------------------------
-- write a setting to the ini file
-------------------------------------------------------------
fn RsSettingWrite group variableName val = (
	setINISetting (RsSettingGetIniFile()) group variableName (val as string)
)

-------------------------------------------------------------
-- read a setting from the ini file. if no value is found
-- the default value is returned
-------------------------------------------------------------
fn RsSettingRead group variableName defaultVal = (

	retval = getINISetting (RsSettingGetIniFile()) group variableName
	
	if retval == "" or retval == "undefined" then (
		retval = defaultVal
	)
	
	return retval
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to a boolean
-------------------------------------------------------------
fn RsSettingsReadBoolean group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal) as BooleanClass
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to a string
-------------------------------------------------------------
fn RsSettingsReadString group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal)
)

-------------------------------------------------------------
-- read a setting from the ini file and force it to an integer
-------------------------------------------------------------
fn RsSettingsReadInteger group variableName defaultVal = (
	return (RSSettingRead group variableName defaultVal) as integer
)