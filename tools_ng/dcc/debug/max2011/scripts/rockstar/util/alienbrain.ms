-- Rockstar Alienbrain Utilities
-- Rockstar North
-- 24 May 2007
-- by David Muir <david.muir@rockstarnorth.com>
--

---------------------------------------------------------------------------------
-- Login to Alienbrain server
--
-- [return] Status of ab command
---------------------------------------------------------------------------------
function AB_login server project user pass = (

	local cmd = stringStream ""
	format "ab logon -s % -d % -u % -p %" server project user pass to:cmd
	return ( DOScommand ( cmd as string ) )
)

---------------------------------------------------------------------------------
-- Logoff from Alienbrain server
--
-- [return] Status of ab command
---------------------------------------------------------------------------------
function AB_logoff = (

	return ( DOScommand "ab logoff" )
)

---------------------------------------------------------------------------------
-- Get latest of a file from Alienbrain server
--
-- [in]     path - full local path of file to get latest version of
-- [in]     recurse - boolean indicating whether to recursively get latest
-- [in]     overwriteWritable - boolean indicating whether to overwrite writeable files
-- [return] Status of ab command
---------------------------------------------------------------------------------
function AB_getlatest path recurse:false overwriteWritable:false = (

	local cmd = stringStream ""
	format "ab getlatest " to:cmd
	
	-- Handle recursive flag
	if ( not recurse ) then
		format " -norecursive " path to:cmd
	
	-- Handle overwriteWritable flag
	if ( overwriteWritable ) then
		format " -overwritewritable replace " to:cmd
	else
		format " -overwritewritable skip " to:cmd
	
	-- Path
	format " % " path to:cmd
	
	return ( DOScommand ( cmd as string ) )
)

-- End of script
