-- Rockstar Map Export
-- Rockstar North
-- 34/12/2008
-- by Luke Openshaw
-- Load a shaders attributes

filein "pipeline/util/xml.ms"

struct dxshadervar ( name, type, id )
struct dxshader ( name, shadervars=#() )
RsShaderDataCache = #()

fn RsLoadShaderVars commonelem shader = (
	
	
	attrelems = commonelem.childnodes
	for i = 0 to ( attrelems.Count - 1 ) do (
		shadervar = dxshadervar
		attrelem = attrelems.itemof(i)
		
		if attrelem != undefined then (
			nameattr = attrelem.Attributes.ItemOf( "name" )
			typeattr = attrelem.Attributes.ItemOf( "type" )
			idattr = attrelem.Attributes.ItemOf( "id" )
			
			if nameattr != undefined and typeattr != undefined and idattr != undefined then (
				shadervar = dxshadervar name:nameattr.Value type:typeattr.Value id:idattr.Value
				
				append shader.shadervars shadervar
			)
		)
		
		
		
		
	)
)


fn RsLoadShaderFromXml shadertype shadername = (
	-- Check the cache first so we dont parse too much shit
	for cachedshader in RsShaderDataCache do (
		if cachedshader.name == shadername then return cachedshader
		print "Shader Cache Hit"
	)
	
	shader = dxshader name:shadername
	
	
	xmlDocPath = ((getdir #plugcfg) + "rs_dx_shaders.xml")
	
	
	xmlDoc = XmlDocument()	
	xmlDoc.init()
	xmlDoc.load xmlDocPath
	
	xmlRoot = xmlDoc.document.DocumentElement
	if xmlRoot != undefined then (
		
		shadertypeelem = RsGetXmlElement xmlRoot shadertype
		if shadertypeelem != undefined then (
			commonelem = RsGetXmlElement shadertypeelem "common"
			if commonelem != undefined then RsLoadShaderVars commonelem shader
			
			shaderelem = RsGetXmlElement shadertypeelem shadername
			if shaderelem != undefined then RsLoadShaderVars shaderelem shader
			
			--print shader	
		)
	)
	
	append RsShaderDataCache shader
	gc()
	return shader
)
