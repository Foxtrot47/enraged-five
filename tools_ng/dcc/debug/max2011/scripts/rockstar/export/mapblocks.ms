-- Rockstar Map Block Setup
-- Rockstar North
-- 3/11/2005
-- Export a map section into the game


rollout RsBlockRoll "Block Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnColourByBlock "Colour By Blocks"
	dropdownlist lstOwner "Owner:" items:#( "R* North", "R* San Diego" )
	edittext edtArtist "Artist:"
	listbox lstBlocks "Blocks" width:145 height:5
	button btnRefresh "R" offset:[78,-70]
	button btnRemove "-" offset:[77,0] width:22
	edittext edtGroupID "Name:" offset:[0,50] 
	checkbox chkFirstPass "1st Pass"
	checkbox chkSecondPassStart "2nd Pass Started"
	checkbox chkSecondPassOut "2nd Pass Outsourced"
	checkbox chkSecondPassComplete "2nd Pass Complete"
	checkbox chkThirdPassComplete "3rd Pass Complete"
	checkbox chkComplete "Complete"
	checkbox chkFreeze "Freeze"
	button btnSelectItems "Select Owned Items"
	groupbox grpBlock "Block Details" offset:[-8,-220] width:175 height:225
	button btnRefreshInfo "Refresh Block Info"
	edittext edtMemModel "Model Mem:" readOnly:true offset:[0,30]
	edittext edtMemCollision "Coll Mem:" readOnly:true
	edittext edtMemTextureMem "Tex Mem:" readOnly:true
	edittext edtNumModel "Num Models:" readOnly:true
	edittext edtNumXrefs "Num Xrefs:" readOnly:true
	groupbox grpBlockInfo "Block Info" offset:[-8,-135] width:175 height:140

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	fn SelectItemsFromCurrentBlock = (
	
		selectset = #()
		allobj = #()
		
		idxBlockID = getattrindex "Gta Object" "BlockID"
		idxBlockIDMilo = getattrindex "Gta MILOTri" "BlockID"

		RsGetMapObjectsWithXrefs rootnode.children allobj
		
		for obj in allobj do (
		
			if getattrclass obj == "Gta Object" then (
			
				if RsLowercase(getattr obj idxBlockID) == RsLowercase(edtGroupID.text) then (
				
					append selectset obj
				)
			) else if getattrclass obj == "Gta MILOTri" then (

				if RsLowercase(getattr obj idxBlockIDMilo) == RsLowercase(edtGroupID.text) then (
				
					append selectset obj
				)
			)
		)

		select selectset
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	fn GetSelectedBlock = (

		retobj = undefined

		idxNumModels = getattrindex "Gta Block" "NumModels"

		if lstBlocks.selection > 0 then (
	
			objList = getnodebyname lstBlocks.items[lstBlocks.selection] exact:true all:true

			for obj in objList do (
			
				if getattrclass obj == "Gta Block" then (
				
--					if getattr obj idxNumModels > 0 then (					
					
						retobj = obj	
--					)
				)
			)
		)	
		
		retobj
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	fn GetAllSelectedBlocks = (

		objRetList = #()

		if lstBlocks.selection > 0 then (
	
			objList = getnodebyname lstBlocks.items[lstBlocks.selection] exact:true all:true

			for obj in objList do (

				if obj != undefined then (
	
					if getattrclass obj == "Gta Block" then (
	
						append objRetList obj		
					)
				)
			)
		)	
		
		objRetList
	)
	
	--------------------------------------------------------------
	-- update the block details
	--------------------------------------------------------------	
	fn UpdateBlockDetails = (
		
		obj = GetSelectedBlock()	
		
		if obj != undefined then (

			edtGroupID.text = obj.name
			lstOwner.selection = findItem lstOwner.items (getattr obj (getattrindex "Gta Block" "Owner"))
			edtArtist.text = getattr obj (getattrindex "Gta Block" "Artist")
			chkFirstPass.checked = getattr obj (getattrindex "Gta Block" "1st Pass")
			chkSecondPassStart.checked = getattr obj (getattrindex "Gta Block" "2nd Pass Started")
			chkSecondPassOut.checked = getattr obj (getattrindex "Gta Block" "2nd Pass Outsourced")
			chkSecondPassComplete.checked = getattr obj (getattrindex "Gta Block" "2nd Pass Complete")
			chkThirdPassComplete.checked = getattr obj (getattrindex "Gta Block" "3rd Pass Complete")
			chkComplete.checked = getattr obj (getattrindex "Gta Block" "Complete")
			chkFreeze.checked = getattr obj (getattrindex "Gta Block" "Freeze")
			edtMemModel.text = ((getattr obj (getattrindex "Gta Block" "MemModel") / 1024) as string) + "k"
			edtMemCollision.text = ((getattr obj (getattrindex "Gta Block" "MemCollision")) as string) + " polys"
			edtMemTextureMem.text = ((getattr obj (getattrindex "Gta Block" "MemTexture") / 1024) as string) + "k"
			edtNumModel.text = (getattr obj (getattrindex "Gta Block" "NumModels")) as string
			edtNumXrefs.text = (getattr obj (getattrindex "Gta Block" "NumXrefs")) as string
		) else (
		
			edtGroupID.text = ""
			lstOwner.selection = 1
			edtArtist.text = "unknown"
			chkFirstPass.checked = false
			chkSecondPassStart.checked = false
			chkSecondPassOut.checked = false
			chkSecondPassComplete.checked = false
			chkThirdPassComplete.checked = false
			chkComplete.checked = false
			chkFreeze.checked = false
			edtMemModel.text = ""
			edtMemCollision.text = ""
			edtMemTextureMem.text = ""
			edtNumModel.text = ""
			edtNumXrefs.text = ""
		)
	)
	
	--------------------------------------------------------------
	-- update the list of blocks in this scene
	--------------------------------------------------------------	
	fn UpdateBlockList = (
	
		objNames = #()
	
		for obj in rootnode.children do (
		
			if getattrclass obj == "Gta Block" then (
			
				if finditem objNames obj.name == 0 then (
			
					append objNames obj.name	
					--hide obj
				)
			)
		)
		
		sort objNames
		
		lstBlocks.items = objNames
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateBlocks = (
	
		UpdateBlockList()
		UpdateBlockDetails()		
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on btnColourByBlock pressed do (
	
		blockColours = #()
	
		for item in lstBlocks.items do (
		
			append blockColours (color (random 0 255) (random 0 255) (random 0 255))
		)
		
		allobj = #()
		
		idxBlockID = getattrindex "Gta Object" "BlockID"
		idxBlockIDMilo = getattrindex "Gta MILOTri" "BlockID"

		RsGetMapObjectsWithXrefs rootnode.children allobj		
		
		for obj in allobj do (
		
			blockID = undefined
		
			if getattrclass obj == "Gta Object" then (
			
				blockID = RsLowercase(getattr obj idxBlockID)
			) 
			else if getattrclass obj == "Gta MILOTri" then (
				
				blockID = RsLowercase(getattr obj idxBlockIDMilo)
			)	
			
			if blockID != undefined then (
			
				for i = 1 to lstBlocks.items.count do (
				
					item = RsLowercase(lstBlocks.items[i])
					
					if item == blockID then (
					
						obj.wirecolor = blockColours[i]
					)
				)
			) else (
			
				obj.wirecolor = (colour 0 0 0)
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on lstOwner selected index do (
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "Owner") lstOwner.items[index]
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on edtArtist entered text do (
	
		data = custattributes.get rootnode blockOwnerDataCADef
		
		if data == undefined then (
		
			custattributes.add rootnode blockOwnerDataCADef
			data = custattributes.get rootnode blockOwnerDataCADef
		)
		
		if data != undefined then (
		
			data.blockOwner = edtArtist.text
		)				
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on edtGroupID entered text do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				obj.name = text
				
				selitem = lstBlocks.selection
				UpdateBlockList()
				lstBlocks.selection = selitem
			)		
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFirstPass changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "1st Pass") chkFirstPass.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkSecondPassStart changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "2nd Pass Started") chkSecondPassStart.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkSecondPassOut changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "2nd Pass Outsourced") chkSecondPassOut.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkSecondPassComplete changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "2nd Pass Complete") chkSecondPassComplete.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)	
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkThirdPassComplete changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "3rd Pass Complete") chkThirdPassComplete.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)	
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on chkComplete changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "Complete") chkComplete.checked
				setattr obj (getattrindex "Gta Block" "Date Edited") localtime
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on chkFreeze changed val do (
	
		objList = GetAllSelectedBlocks()	
		
		for obj in objList do (
		
			if obj != undefined then (
			
				setattr obj (getattrindex "Gta Block" "Freeze") chkFreeze.checked
			)		
		)
	)

	--------------------------------------------------------------
	-- clicking on an item
	--------------------------------------------------------------	
	on lstBlocks selected item do (
	
		objList = GetAllSelectedBlocks()	
		
		select objList
	
		UpdateBlockDetails()
	)

	--------------------------------------------------------------
	-- clicking on an item
	--------------------------------------------------------------	
	on lstBlocks doubleClicked item do (
	
		objList = GetAllSelectedBlocks()	
		
		select objList
		
		UpdateBlockDetails()
		
		max tool zoomextents
	)

	--------------------------------------------------------------
	-- refresh the block list
	--------------------------------------------------------------	
	on btnRefresh pressed do (
	
		UpdateBlocks()
		RsSetupBlocks true
	)

	--------------------------------------------------------------
	-- deleting an existing block
	--------------------------------------------------------------	
	on btnRemove pressed do (
	
		if lstBlocks.selection > 0 then (
	
			objList = getnodebyname lstBlocks.items[lstBlocks.selection] exact:true all:true

			for obj in objList do (
			
				delete obj
			)
			
			UpdateBlocks()
		)		
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSelectItems pressed do (
	
		SelectItemsFromCurrentBlock()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSetSelected pressed do (
	
		idxBlockID = getattrindex "Gta Object" "BlockID"
		idxBlockIDMilo = getattrindex "Gta MILOTri" "BlockID"
		
		for obj in selection do (
		
			if getattrclass obj == "Gta Object" then (
			
				setattr obj idxBlockID edtGroupID.text
			) else if getattrclass obj == "Gta MILOTri" then (
			
				setattr obj idxBlockIDMilo edtGroupID.text
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnRefreshInfo pressed do (
	
		objListWithXref = #()
		RsGetMapObjectsWithXrefs rootnode.children objListWithXref
		RsSetupBlockInfo objListWithXref
	)

	--------------------------------------------------------------
	-- reload from source file
	--------------------------------------------------------------
	on btnLoad pressed do (
	
		RsMapSetupInfo()
		
		iplFilename = RsMapDir + RsMapName + ".ipl"
		
		iplFile = openfile iplFilename mode:"r"
		
		if iplFile != undefined then (
		
			mode = #none
		
			while eof iplFile == false do (
			
				iplLine = readline iplFile
				
				if mode == #none then (

					if iplLine == "blok" then (
					
						mode = #reading
					)
				
				) else if mode == #reading then (
				
					if iplLine == "end" then (
					
						mode = #none
					) else (
										
						tokens = filterstring iplLine ", "
						
						foundobj = getnodebyname tokens[1] exact:true
						
						if foundobj == undefined then (
						
							newobj = GtaBlock()
							newobj.name = tokens[1]
							
							flagId = tokens[4] as number
						
							if (bit.and flagId 1) > 0 then (
								setattr newobj (getattrindex "Gta Block" "In Progress") true
							)
							
							if (bit.and flagId 2) > 0 then (
								setattr newobj (getattrindex "Gta Block" "1st Pass") true
							)
							
							if (bit.and flagId 4) > 0 then (
								setattr newobj (getattrindex "Gta Block" "2nd Pass Started") true
							)
							
							if (bit.and flagId 8) > 0 then (
								setattr newobj (getattrindex "Gta Block" "2nd Pass Outsourced") true
							)							

							if (bit.and flagId 16) > 0 then (
								setattr newobj (getattrindex "Gta Block" "2nd Pass Complete") true
							)
							
							if (bit.and flagId 32) > 0 then (
								setattr newobj (getattrindex "Gta Block" "3rd Pass Complete") true
							)
							
							if (bit.and flagId 64) > 0 then (
								setattr newobj (getattrindex "Gta Block" "Complete") true
							)
						)
					)
				)
			)
		
			close iplFile
		)
		
		UpdateBlocks()
	)

	--------------------------------------------------------------
	-- initialisation of rollout
	--------------------------------------------------------------
	on RsBlockRoll open do (
	
		callbacks.addScript #filePostOpen "RsBlockRoll.UpdateBlocks()" id:#RsBlockPostOpen
	
		data = custattributes.get rootnode blockOwnerDataCADef
		
		if data == undefined then (
		
			custattributes.add rootnode blockOwnerDataCADef
			data = custattributes.get rootnode blockOwnerDataCADef
			data.blockOwner = "unknown"
		)
		
		if data != undefined then (
		
			edtArtist.text = data.blockOwner
		)
	
		UpdateBlocks()
	)
	
	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsBlockRoll close do (
	
		callbacks.removeScripts #filePostOpen id:#RsBlockPostOpen
	
		RsSettingWrite "rsmapblocks" "rollup" (not RsBlockRoll.open)
	)
)

