--
-- File:: rockstar/export/ambientscript.ms
-- Description:: Rockstar Script Export
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 6/8/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn RsUpdateSelectionSets = (
	-- make a selection set so that the ide export (gta3.dle) doesnt export specified objects

	SelDontExport = #()
	SelDontAddToIPL = #()

	for obj in rootnode.children do (
		idxDontExport = -1
		idxDontAddToIPL = -1

		if GetAttrClass obj == "Gta Object" then (
			idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
			idxDontAddToIPL = GetAttrIndex "Gta Object" "Dont Add To IPL"
		)

		if GetAttrClass obj == "GtaAnimHierarchy" then (
			idxDontExport = GetAttrIndex "GtaAnimHierarchy" "Dont Export"
			idxDontAddToIPL = GetAttrIndex "GtaAnimHierarchy" "Dont Add To IPL"
		)

		if idxDontExport != -1 then (
			if GetAttr obj idxDontExport == true then (
				append SelDontExport obj
			)			
		)

		if idxDontAddToIPL != -1 then (
			if GetAttr obj idxDontAddToIPL == true then (
				append SelDontAddToIPL obj
			)			
		)
	)

	selectionsets["GtaDontExport"] = SelDontExport
	selectionsets["GtaIgnore"] = SelDontAddToIPL
)

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsScriptRoll "Ambient Script Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Export_Ambient_Scripts" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExport "Export"

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnExport pressed do (
	
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		idxDontAddToIpl = getattrindex "Gta Object" "Dont Add To IPL"
		idxName = getattrindex "Gta Script" "Name"
	
		for obj in rootnode.children do (
		
			if getattrclass obj == "Gta Object" then (
			
				setattr obj idxDontExport true
				setattr obj idxDontAddToIpl true
			) 
			else if getattrclass obj == "Gta Script" then (
			
				obj.name = getattr obj idxName
			)
			else (
			
				delete obj
			)
		)
	
		RsUpdateSelectionSets()
	
		exportFilename = RsConfigGetProjRootDir() + "/build/common/data/maps/ambient.ipl"
	
		print exportFilename 
	
		setsingleiplfile(true)
		--setiplfilegroup(RsConfigGetProjectName() + "_pc")
	
		if (getfiles exportFilename).count > 0 then (
		
			if deletefile exportFilename == false then (

				messagebox ("problem deleting target ipl file: " + exportFilename)
				return false
			)
		)
	
		ret = exportFile exportFilename #noprompt
		
		if ret == false then (

			messagebox "problem exporting ipl file"
			return false
		)
		
		print exportFilename
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsScriptRoll close do (
	
		RsSettingWrite "rsscript" "rollup" (not RsScriptRoll.open)
	)
)

try CloseRolloutFloater RsAnimUtil catch()
RsScriptUtil = newRolloutFloater "Rockstar Anim Exporter" 200 105 50 126
addRollout RsSettingsRoll RsScriptUtil rolledup:(RsSettingsReadBoolean "rssettings" "rollup" false)
addRollout RsScriptRoll RsScriptUtil rolledup:(RsSettingsReadBoolean "rsscript" "rollup" false)
