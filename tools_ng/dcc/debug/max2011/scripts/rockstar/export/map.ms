--
-- Rockstar Map Export
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
--
-- Export a map section into the game
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"

filein "rockstar/util/lod.ms"
filein "pipeline/util/file.ms"
filein "rockstar/util/mapobj.ms"
filein "rockstar/util/dialog.ms"
filein "pipeline/util/string.ms"
filein "rockstar/util/material.ms"
filein "rockstar/util/ragereport.ms"
filein "rockstar/util/rexreport.ms"
filein "rockstar/util/collutil.ms"

filein "rockstar/export/settings.ms"
filein "rockstar/export/maputil.ms"
filein "pipeline/export/maps/maptest.ms"
filein "pipeline/export/maps/mapsetup.ms"
filein "rockstar/export/ragebuilder.ms"
filein "rockstar/export/mapbounds.ms"
filein "rockstar/export/mapblocks.ms"
filein "rockstar/export/mapanim.ms"

-- Additional export logic
filein "pipeline/export/maps/mapgeom.ms"
filein "pipeline/export/maps/maptxd.ms"
filein "pipeline/export/maps/objects.ms"
filein "pipeline/export/maps/preview.ms"

-- Additional UI rollouts
filein "pipeline/export/ui/map_geometry_ui.ms"
filein "pipeline/export/ui/map_namespace_ui.ms"
filein "pipeline/export/ui/map_preview_ui.ms"
filein "pipeline/export/ui/map_scenexml_ui.ms"
filein "pipeline/export/ui/map_scripts_ui.ms"
filein "pipeline/export/ui/map_txd_ui.ms"


-----------------------------------------------------------------------------
-- Global Initialisation
-----------------------------------------------------------------------------
RsMapExportTexturePathCheck = true
RsParentFileList = #() -- DHM FIX 2010/02/10.

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------
-- None - see "pipeline/export/maps/globals.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsMapRoll "Map Export"
(
	---------------------------------------------------------------------------
	-- UI Widgets
	---------------------------------------------------------------------------
	hyperlink lnkHelp					"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportAll 			"Export Everything" width:100 tooltip:"Export all map sections and prop groups within this file."
	button btnExportSel 			"Export Selected" width:100 tooltip:"Export all selected objects into correct section and prop groups."
	button btnExportContainer "Export Container" width:100 tooltip:"Export selected map section Container."
	button btnExportPropGroup "Export PropGroup" width:100 tooltip:"Export selected object's prop group."
	button btnBuild 					"Build Image(s)" width:100 tooltip:"Build all map section and prop group RPF files within this file."
	button btnRebuild 				"Rebuild Image(s)" width:100 tooltip:"Rebuild all map section and prop group RPF files within this file."
	button btnTestMap 				"Test Maps" width:100 tooltip:"Run map tests for all map sections and prop groups within this file."
	button btnDeleteStream 		"Delete Stream(s)" width:100 tooltip:"Clean out all map section and prop group stream files."
	button btnLoadLights 			"Load Lights" width:100 tooltip:"Load lights information saved from RAG."
	
	group "IDE/IPL Export"
	(
		button btnExportIDEIPL "Export IDE/IPL" width:100 tooltip:"Export IDE/IPL file(s) for all map sections and prop groups within this file."
		button btnExportIDE "Export IDE" width:100 tooltip:"Export IDE file(s) for all map sections and prop groups within this file."
	)
		
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------

	--
	-- name: DoExportSelected
	-- desc: Export selected geometry items.
	--
	fn DoExportSelected = (
		
		local exportselobjs = RsMapGetMapContainers()

		-- Loop through our array of RsMapContainer objects
		for exportsel in exportselobjs do
		(
			if ( not exportsel.is_exportable() ) then
				continue
		
			if ( not exportsel.is_propgroup() ) then
			(
				if ( false == ( RsPreExport exportsel docheck:false ) ) then
					return false

				format "Exporting selected from map: %...\n" RsMapName

				-- Check what's in the selection
				if ( false == ( RsCheckMapContents exportsel.selobjects ) ) then 
					return false

				local mapgeom = RsMapGeometry()
				if ( false == ( mapgeom.CheckMissing exportsel.selobjects ) ) then
					return false

				if (RsQueryBoxTimeOut "collision export?") then (

					if RsBoundsRoll.ExportBounds exportsel.selobjects == false then return false
				)

				if RsMapHasAnim then (
					if (RsQueryBoxTimeOut "anim export?") then (

						if RsMapAnimation.ExportAnims exportsel.selobjects == false then return false
					)
				)

				if RsMapHasGeom then (
					if (RsQueryBoxTimeOut "geometry export?") then (

						if ( false == ( mapgeom.ExportMap exportsel.selobjects ) ) then 
							return false
					)
				)

				if (RsQueryBoxTimeOut "txd export?") then (

					local maptxd = RsMapTXD()
					if ( false == maptxd.ExportSelected() ) then
						return false

					if (RsQueryBoxTimeOut "parent txd export?") then (

						local mapparenttxd = RsMapParentTXD()
						if ( false == ( mapparenttxd.WriteSceneTxdList() ) ) then 
							return false
					)
				)

				RsMapHasMetaInfo = gRsStatedAnimTool.HasSceneAnyAnimations()
				if (RsQueryBoxTimeOut "Stated anim export?") then (
					-- rayfire XML
					if ( false ==  gRsStatedAnimTool.ExportXML()) then
						return false
				)

				if (RsQueryBoxTimeOut "ide/ipl export?") then (
					if ( false == RsMapExportIDEIPL() ) then
						return false
				)					

				if ( false == ( RsPostExport exportsel ) ) then
					return false
			)
			else if ( exportsel.is_propgroup() ) then
			(
				-- PropGroups are just XRefs so we only need to re-export the
				-- IPL files for this map section; then build the image.
				format "Exporting selected from propgroup %...\n" exportsel.propgroup

				if ( false == ( RsPreExport exportsel docheck:true ) ) then
					return false

				if (RsQueryBoxTimeOut "collision export?") then (

					if RsBoundsRoll.ExportBounds exportsel.selobjects == false then return false
				)

				RsMapHasMetaInfo = gRsStatedAnimTool.HasSceneAnyAnimations()
				if (RsQueryBoxTimeOut "Stated anim export?") then (
					-- rayfire XML
					if ( false ==  gRsStatedAnimTool.ExportXML()) then
						return false
				)

				if (RsQueryBoxTimeOut "ide/ipl export?") then (
					if ( false == RsMapExportIDEIPL() ) then
						return false
				)

				if ( false == ( RsPostExport exportsel ) ) then
					return false					
			)

			-- Build image/rpf
			local ss = stringStream ""
			format "do you want to build the % map?" RsMapName to:ss
			if ( RsQueryBoxTimeOut (ss as string) ) then (
				RsMapBuildImage()
			)
		)
		
		true
	)

	--
	-- name: DoExportMapSection
	-- desc: Export a single map section.
	--
	fn DoExportMapSection map doCheck:false = (
	
		RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	
		-- Preexport pass; don't test map as its all done up front.
		if ( ( RsPreExport map doCheck:doCheck ) == false ) then 
			return false		

		-- Check the contents of the map
		if ( RsCheckMapContents map.objects == false ) then 
			return false

		-- Static Bounds pass
		if ( RsBoundsRoll.ExportBounds map.objects == false ) then 
			return false
		
		if ( not map.is_propgroup() ) then
		(
			-- Animation pass
			if ( RsMapAnimation.ExportAnims map.objects == false ) then 
				return false

			-- Geometry pass
			local mapgeom = RsMapGeometry()
			if ( false == ( mapgeom.ExportMap map.objects ) ) then 
				return false

			-- TXD pass
			local maptxd = RsMapTXD()
			if ( false == ( maptxd.ExportAllTxds map.objects ) ) then 
				return false

			-- Parent TXD pass
			local mapparenttxd = RsMapParentTXD()
			if ( false == ( mapparenttxd.WriteSceneTxdList() ) ) then 
				return false	
		)
		else if ( map.is_propgroup() ) then
		(
			-- Nothing special for prop groups (yet).
		)
		
		-- rayfire XML
		RsMapHasMetaInfo = gRsStatedAnimTool.HasSceneAnyAnimations()
		if ( false == gRsStatedAnimTool.ExportXML() ) then
			return false

		-- IDE/IPL pass
		if ( false == RsMapExportIDEIPL() ) then
			return false

		-- Post pass
		if ( false == ( RsPostExport map ) ) then
			return false

		-- Build image/rpf
		local ss = stringStream ""
		format "do you want to build the % map?" RsMapName to:ss
		if ( RsQueryBoxTimeOut (ss as string) ) then (
			RsMapBuildImage()
		)
	)

	--
	-- name: DoExportAll
	-- desc: Export all map sections.
	--
	fn DoExportAll = (
		
		-- Loop through all RsMapContainers for the scene.
		local maps = RsMapGetMapContainers()
		if ( false == ( RsCheckMap true ) ) then
			return false

		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
				
			DoExportMapSection map
		)
				
		true
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- Load lights information saved from RAG (c:/lights.txt)
	--------------------------------------------------------------
	on btnLoadLights pressed do (

		local objectnames = #()
		local objectlights = #()
		local lightfilename = "c:\\lights.txt"
		local lightfile = undefined
		
		-- More user-friendly to ask for lights file with old default however.
		lightfilename = ( getOpenFileName caption:"Open Light Defs File" filename:lightfilename types:"Text Files (*.txt)|*.txt|All Files (*.*)|*.*" )
		if ( undefined == lightfilename ) then
			return ( false )
		
		lightfile = ( openfile lightfilename mode:"r" )		

		if ( undefined == lightfile ) then 
		(
			MessageBox "Lights load failed (c:/lights.txt does not exist)."
			return ( false )
		)

		while eof lightfile == false do (

			lightfileline = readline lightfile
			lightfiletokens = filterstring lightfileline ", "

			if ( lightfiletokens.count == 42 ) then 
			(
				idxFound = finditem objectnames lightfiletokens[1]

				if idxFound == 0 then (

					append objectnames lightfiletokens[1]
					append objectlights (#(lightfiletokens))
				) else (

					append objectlights[idxFound] lightfiletokens
				)
			)
		)
		close lightfile

		local idxStatic = getattrindex "Gta Light" "Cast Static Object Shadow"
		local idxDynamic = getattrindex "Gta Light" "Cast Dynamic Object Shadow"
		local idxCalcFromSun = getattrindex "Gta Light" "Calc From Sunlight"
		local idxDrawVolume = getattrindex "Gta Light" "Volume Drawing"
		local idxNoSpecular = getattrindex "Gta Light" "No Specular"
		local idxFlickerFlags = getattrindex "Gta Light" "Flashiness"

		for i = 1 to objectnames.count do (

			objectname = objectnames[i]
			objectlightlist = objectlights[i]

			obj = getNodeByName objectname exact:true ignoreCase:true

			if obj != undefined and classof obj != XRefObject then (

				print (objectname + " " + (objectlightlist.count as string))

				lightindex = 1

				for childobj in obj.children do (

					if superclassof childobj == light and lightindex <= objectlightlist.count then (

						objectlight = objectlightlist[lightindex]

						name = objectlight[1]
						posx = objectlight[2]
						posy = objectlight[3]
						posz = objectlight[4]
						lightType = objectlight[10]
						colR = objectlight[11]
						colG = objectlight[12]
						colB = objectlight[13]
						colA = objectlight[14]
						lodDistance = objectlight[17]
						coronaSize = objectlight[19]
						flashiness = objectlight[22]
						reflectionType = objectlight[23]
						lensFlareType = objectlight[24]
						flags = objectlight[25] as integer
						coneInner = objectlight[26]
						coneOuter = objectlight[27]
						multiplier = objectlight[28]
						volScale = objectlight[29]
						volSize = objectlight[30]
						sizeMax = objectlight[32] as float
						dirX = objectlight[33]
						dirY = objectlight[34]
						dirZ = objectlight[35]
						tanX = objectlight[36]
						tanY = objectlight[37]
						tanZ = objectlight[38]
						boneTag = objectlight[39]
						lightFadeDist = objectlight[40]
						volFadeDist = objectlight[41]
						coronaHDR = objectlight[42]								

						print "light"

						newCol = (color (colR as integer) (colG as integer) (colB as integer))

						print ("multiplier was " + (childobj.multiplier as string) + " now " + (multiplier as string))
						print ("colour was " + (childobj.rgb as string) + " now " + (newCol as string))
						print ("falloff was " + (childobj.farAttenEnd as string) + " now " + (sizeMax as string))

						childobj.multiplier = multiplier as float
						childobj.rgb = newCol
						childobj.farattenstart = 0.0
						childobj.farAttenEnd = sizeMax 

						isSpot = classof(childobj) == #freeSpot or classof(childobj) == #targetSpot

						if isSpot then (
							childobj.hotspot = 2 * (coneInner as float)
							childobj.falloff = 2 * (coneOuter as float)
						)

						if bit.and flags (bit.shift 1 14) > 0 then (

							setattr childobj idxStatic true

						) else (

							setattr childobj idxStatic false
						)

						if bit.and flags (bit.shift 1 15) > 0 then (

							setattr childobj idxDynamic true
						) else (

							setattr childobj idxDynamic false
						)							

						if bit.and flags (bit.shift 1 16) > 0 then (

							setattr childobj idxCalcFromSun true
						) else (

							setattr childobj idxCalcFromSun false
						)							

						if bit.and flags (bit.shift 1 20) > 0 then (

							setattr childobj idxNoSpecular true
						) else (

							setattr childobj idxNoSpecular false
						)							

						setattr childobj idxFlickerFlags (flashiness as integer)

						setpos = [(posx as float),(posy as float),(posz as float)]
						in coordsys parent (						
							print ("pos was " + (childobj.pos as string) + " now " + (setpos as string))
							childobj.pos = setpos
						)

						if isSpot then (
							setdir = [-(dirx as float),-(diry as float),-(dirz as float)]					
							in coordsys world (
								print ("dir was " + (childobj.dir as string) + " now " + (setdir as string))
								childobj.dir = setdir
							)
						)
						lightindex = lightindex + 1
					)
				)
			)
		)
	)

	--
	-- event: btnExportAll pressed
	-- desc: Export all available map sections in this file (containers, prop groups etc).
	--
	on btnExportAll pressed do (
		
		if querybox "are you sure you want to export all?" title:"sanity check" == false then return false

		try 
		(
			DoExportAll()
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)
	)

	--
	-- event: btnExportSel pressed
	-- desc: Export Selected objects to map sections in this file (container, prop groups etc).
	--
	on btnExportSel pressed do (
	
		try 
		(
			DoExportSelected()
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)

	--
	-- event: btnExportContainer pressed 
	-- desc: Export currently selected map section (container).
	--
	on btnExportContainer pressed do (
	
		try		
		(
			if ( Container != classof $ ) then
			(
				messageBox "No map section container selected."
			)
			else if ( ( Container == classof $ ) and ( not $.IsOpen() ) ) then
			(
				messageBox "Selected map section container is not open."
			)
			else
			(	
				local maps = RsMapGetMapContainers()
				for map in maps do
				(
					if ( not map.is_exportable() ) then
						continue	
				
					if ( not map.is_container() ) then
						continue
						
					if ( map.cont == $ ) then
					(
						DoExportMapSection map doCheck:true
					)
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)
	)

	--
	-- event: btnExportPropGroup pressed
	-- desc: Export currently selected object's prop group.
	--
	on btnExportPropGroup pressed do (
	
		try
		(
			if selection.count == 0 then (
				
				return 0
			)
		
			local attrObjectPropGroupIndex = ( GetAttrIndex "Gta Object" "Prop Group" )
			local attrMiloPropGroupIndex = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
			
			if ( ( "Gta Object" != GetAttrClass $ ) and ( "Gta MILOTri" != GetAttrClass $ ) ) then
			(
				messageBox "No object or MILOTri selected."
			)
			else if ( ( "Gta Object" == GetAttrClass $ ) and 
							  ( "undefined" == GetAttr $ attrObjectPropGroupIndex ) ) then
			(
				messageBox "Selected object does not have a valid Prop Group set."
			)
			else if ( ( "Gta MILOTri" == GetAttrClass $ ) and 
							  ( "undefined" == GetAttr $ attrMiloPropGroupIndex ) ) then
			(
				messageBox "Selected MILOTri does not have a valid Prop Group set."
			)
			else if ( "Gta Object" == GetAttrClass $ ) then
			(
				local propGroupName = ( GetAttr $ attrObjectPropGroupIndex )
				local propGroupObjects = ( RsMapObjectGetGroupObjects propGroupName )
				local maps = RsMapGetMapContainers()
				
				for map in maps do
				(
					if ( not map.is_exportable() ) then
						continue	
					if ( not map.is_propgroup() ) then
						continue
						
					if ( map.name == propGroupName ) then
					(
						DoExportMapSection map docheck:true
					)
				)				
			)
			else if ( "Gta MILOTri" == GetAttrClass $ ) then
			(
				local propGroupName = ( GetAttr $ attrMiloPropGroupIndex )
				local propGroupObjects = ( RsMapObjectGetGroupObjects propGroupName )
				local maps = RsMapGetMapContainers()
				
				for map in maps do
				(
					if ( not map.is_exportable() ) then
						continue	
					if ( not map.is_propgroup() ) then
						continue
						
					if ( map.name == propGroupName ) then
					(
						DoExportMapSection map docheck:true
					)
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)

	--------------------------------------------------------------
	-- export IDE/IPL button pressed
	--------------------------------------------------------------
	on btnExportIDEIPL pressed do (
	
		try
		(
			local maps = RsMapGetMapContainers()
			for map in maps do
			(
				if ( not map.is_exportable() ) then
					continue	
				
				if ( false == RsPreExport map ) then 
					return false

				RsMapExportIDEIPL()
				RsPostExport map
				
				-- Build image/rpf
				local ss = stringStream ""
				format "do you want to build the % map?" RsMapName to:ss
				if ( RsQueryBoxTimeOut (ss as string) ) then (
					RsMapBuildImage()
				)
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnExportIDE pressed do (
	
		try
		(
			local maps = RsMapGetMapContainers()
			for map in maps do
			(
				if ( not map.is_exportable() ) then
					continue	
			
				if ( false == RsPreExport map ) then 
					return false

				RsMapExportIDEOnly()
				RsPostExport map
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)

	--------------------------------------------------------------
	-- Build Image button pressed
	--------------------------------------------------------------
	on btnBuild pressed do (

		try
		(	
			local maps = RsMapGetMapContainers()
			for map in maps do
			(
				if ( not map.is_exportable() ) then
					continue	
					
				if ( false == RsPreExport map ) then 
					return false

				RsPostExport map
				RsMapBuildImage()
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)
	
	--------------------------------------------------------------
	-- Rebuild Image button pressed
	--------------------------------------------------------------	
	on btnRebuild pressed do (
	
		try
		(
			local maps = RsMapGetMapContainers()
			for map in maps do
			(
				if ( not map.is_exportable() ) then
					continue	
				
				if ( false == RsPreExport map ) then 
					return false

				RsPostExport map
				RsMapRebuildImage()
			)		
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)			
	)

	--------------------------------------------------------------
	-- Test Map button pressed
	--------------------------------------------------------------
	on btnTestMap pressed do (

		try
		(
			local maps = RsMapGetMapContainers()
			for map in maps do
			(
				if ( not map.is_exportable() ) then
					continue	
				
				if ( false == ( RsPreExport map docheck:false ) ) then
					return false
				if ( true == ( RsCheckMap false ) ) then
					messageBox "No problems!"
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				RsAssert( false )
			)
		)
	)

	--
	-- event: btnDeleteStream pressed
	-- desc: Delete currently selected map section's stream
	--
	on btnDeleteStream pressed do (
	
		local maps = RsMapGetMapContainers()
		
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue			
		
			RsMapSetupGlobals map

			local msg = stringStream ""
			format "Are you sure you want to delete the % map's stream?\n\nYou will need to do an Export Container/PropGroup or Export Everything afterwards." RsMapName to:msg

			if ( querybox (msg as string) ) == false then 
				return false

			RsMapDeleteStream()
		)
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsMapRoll open do (
		rexSetTextureShift 1 -- Need to leave this hack in until i get the word
		
		/*
		-- This dirty little hack should only apply to gta
		-- It halves the texture size by shifting a texture shift bit for width and height that is used when scaling the texture (LoadDevilImage)
		if RsConfigGetProjectName() == "gta" or RsConfigGetProjectName() == "gta_e1" or RsConfigGetProjectName() == "gta_e2" then (
			rexSetTextureShift 1 
		)
		*/
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsMapRoll close do (
	
		rexSetTextureShift 0
		RsSettingWrite "rsmap" "rollup" (not RsMapRoll.open)
	)
)

rollout RsOcclusionRoll "Map Occlusion"
(
	button btnExport "Export Occlusion" width:100
	button btnImport "Import Occlusion" width:100
	
	on btnExport pressed do (
	
		filein "pipeline/export/maps/ExportOcclusion.ms"
	)

	on btnImport pressed do (
	
		filein "pipeline/export/maps/ImportOcclusion.ms"
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsOcclusionRoll close do (
	
		RsSettingWrite "rsmapocclusion" "rollup" (not RsOcclusionRoll.open)
	)
)

--------------------------------------------------------------
-- beginning of execution
--------------------------------------------------------------
try CloseRolloutFloater RsMapUtil catch()
RsMapUtil = newRolloutFloater "Rockstar Map Exporter" 200 520 50 126
addRollout RsSettingsRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rssettings" "rollup" false)
addRollout RsMapRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmap" "rollup" false)
addRollout RsMapPreviewRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmappreview" "rollup" false)
addRollout RsSceneXmlRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsscenexml" "rollup" false)
addRollout RsGeomRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapgeom" "rollup" false)
addRollout RsMapTxdRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmaptxd" "rollup" false)
addRollout RsParentTxdRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsparenttxd" "rollup" false)
addRollout RsBoundsRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapbounds" "rollup" false)
addRollout RsMapAnimation RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapanimation" "rollup" false)
addRollout RsBlockRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapblocks" "rollup" false)
addRollout RsOcclusionRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapocclusion" "rollup" false)
addRollout RsMapNamespaceRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapnamespace" "rollup" false)
addRollout RsScriptsRoll RsMapUtil rolledup:(RsSettingsReadBoolean "rsmapchecks" "rollup" false)

-- map.ms
