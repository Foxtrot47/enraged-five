-- Rockstar Map Export
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- Export a map sections animations into the game

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/mapgeom.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------

struct RsExportAnim (model, uvindex, texmap)

idxHasAnim = getattrindex "Gta Object" "Has Anim"
idxHasUvAnim = getattrindex "Gta Object" "Has UvAnim"
idxAnimLoops = getattrindex "Gta Object" "looped"

fn RsGetAnimatedBones obj sel = (

	--if classof obj == Editable_Mesh or classof obj == Editable_Poly then (
	--if classof obj == Editable_Mesh or classof obj == Point or classof obj == Biped_Object or classof obj == BoneGeometry then (
	if RsIsValidBoneForMap obj then (
	
		append sel obj
	)
	
	for childobj in obj.children do (
	
		RsGetAnimatedBones childobj sel
	)
)

rollout RsMapAnimation "Animation Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////	

	button btnExport "Export" width:100

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn SaveClipFile filename model = (
		
		--eBoneMask eBoneMask_Parse(const char *str, const eBoneMask defaultVal);
		-- enum eAnimPlayerFlag
		-- {
		APF_UNUSED_1 = 0
		APF_UNUSED_2 = 1
		APF_UNUSED_3 = 2
		APF_UNUSED_4 = 3
		APF_ISPLAYING = 4
		APF_ISLOOPED = 5
		APF_LOOP_1 = 6
		APF_LOOP_2 = 7
		APF_ISSYNCRONISED = 8
		APF_UNUSED_5 = 9
		APF_UNUSED_6 =10
		APF_UNUSED_7 = 11
		APF_UNUSED_8 = 12
		APF_IGNOREROTATION = 13
		APF_ISBLENDAUTOREMOVE = 14
		APF_ISFINISHAUTOREMOVE = 15
		APF_IGNORE_BLEND_VELOCITY_EXTRACTION = 16
		APF_ADDITIVE = 17
		APF_FACIAL = 18
		APF_GESTURE = 19
		APF_CUTSCENE = 20
		APF_UPPERBODYONLY = 21
		APF_SKIP_NEXT_UPDATE_BLEND = 22
		APF_INCAR = 23
		APF_FOOTFALL = 24
		APF_COPY = 25
		APF_BLOCK_IK = 26
		APF_BLOCK_LEG_IK = 27
		APF_BLOCK_HEAD_IK = 28
		APF_FOLLOW_PED_ROOT_BONE = 29
		APF_ADD_TO_POINTCLOUD = 30
		APF_USE_DEFAULT_RCB = 31
		-- };

		if RsIsFileReadOnly filename then (

			messagebox (filename + " is read only")
			return false
		)

		clipEditInst = clipEditor filename
		local flags = 0
		if (getattr model idxAnimLoops) then
		(
--			print "Object has looped animation."
			flags = bit.or flags (bit.shift 1 APF_ISLOOPED)
		)
--		print ("AnimPlayerFlags:"+flags as string)
		clipSetProperty clipEditInst flags 2 ("AnimPlayerFlags")
		
		clipsave filename model clipEditInst
		true
			
	)
	
	fn CreateAnimList animList rootfolder = (
		-- create the ragebuilder script to construct an iad file
				
		xmlFileName = RsConfigGetScriptDir() + RsMapName + "/anim.xml"
		RsMakeSurePathExists xmlFileName

		
		-- init xml doc
		animXml = XmlDocument()
		animXml.init()


		animXmlRoot = animXml.createelement("mapanims")
		animXml.document.AppendChild animXmlRoot
		

		for exportItem in animList do (
			
			animFile = exportItem.model.name
			if exportItem.uvindex != -1 then (
				animFile = exportItem.model.name + "_UV_" + (exportItem.uvindex as string)	
			)
			
			animElem = animXml.createelement(animFile)
			animXmlRoot.AppendChild(animElem)
		)
		
		result = animXml.save xmlFileName
		rubyString = "anim_content = Pipeline::Content::MapAnim.new(\"" + xmlFileName + "\", project, map_name, generic )\n"
		rubyString = rubyString + "component_group.add_child(anim_content)\n\n"
		append RsContentScriptQueue rubyString
		result
	)


	fn ExportAnims models = (
		
		currAnimRange = animationRange
		rootfolder = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/anim"
		currsel = #()
		currsel = currsel + selection
	
		exportList = #()
		modelsin = #()
		lodmodelsin = #()
		modelsin = modelsin + models

		RsDeleteFiles (rootfolder + "/*.anim")
		RsDeleteFiles (rootfolder + "/*.clip")

		-- find the animated objects		
		local mapgeom = RsMapGeometry()
		if (modelsin = mapgeom.RsFilterExportModels modelsin lodmodelsin) == false then return false
	
		for lodmodlist in lodmodelsin do (
		
			for lodmod in lodmodlist do (
			
				append modelsin lodmod
			)
		)
	
		progressStart "setting up animation"
	
		for i = 1 to modelsin.count do (
		
			model = modelsin[i]

			progressUpdate (100.0*i/modelsin.count)
			if getProgressCancel() then throw("progress_cancel")

			-- model animation

			if getattrclass model == "Gta Object" and getattr model idxHasAnim then (
	
				if RsIsObjectAnimated model then (

					append exportList (RsExportAnim model -1 undefined)
				)
			)

			if getattrclass model == "Gta Object" and getattr model idxHasUvAnim then (

				-- uv animation
				texmaplist = #()
				RsGetMainTexMapsFromMaterial model model.material texmaplist

				for i = 1 to texmaplist.count do (

					texmap = texmaplist[i]

					if classof texmap == Bitmaptexture and rexIsTexMapAnimated texmap then (

						append exportList (RsExportAnim model (i-1) texmaplist[i])	
					)
				)
			)
		)

		progressEnd()

		-- export the anim files
		
		progressStart "exporting animation"
		
		for i = 1 to exportList.count do (
		
			exportItem = exportList[i]
			
			try progressUpdate (100.0*i/exportList.count) catch()
			if getProgressCancel() then throw("progress_cancel")
	
			if exportItem.uvindex == -1 then (
	
				model = exportItem.model
				outname = model.name
				rootbone = rexGetSkinRootBone model
				
				if rootbone != undefined then (
				
					model = rootbone
				)
				
				select model
	
				--animationRange = (interval model.pos.controller.keys[1].time model.pos.controller.keys[model.pos.controller.keys.count].time)
				animationRange = RsGetAnimRange model
	
				rexReset()
				graphRoot = rexGetRoot()
	
				animation = rexAddChild graphRoot outname "Animation" 
				rexSetPropertyValue animation "OutputPath" rootfolder
				rexSetPropertyValue animation "ChannelsToExport" "(translateX;translateY;translateZ;)(rotateX;rotateY;rotateZ;)(projectData;projectData0;projectData1;projectData2;projectData3;projectData4;projectData5;projectData6;projectData7;)"
				rexSetPropertyValue animation "AnimMoverTrack" "false"
				rexSetPropertyValue animation "AnimAuthoredOrient" "true"
				rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
				rexSetPropertyValue animation "AnimExtraMoverTrack" "mover"
				rexSetPropertyValue animation "AnimLocalSpace" "true"
				rexSetPropertyValue animation "AnimExportSelected" "true"
				rexSetNodes animation
	
				selset = #()
				RsGetAnimatedBones model selset
				select selset
				
				rexExport()
				
				clearSelection()
				
				SaveClipFile ( rootfolder + "/" + outname ) model
			) else (
			
				model = exportItem.model
		
				select model
				
				animationRange = RsGetTexMapUVAnimRange exportItem.texmap
		
				rexReset()
				graphRoot = rexGetRoot()
	
				uvName = (model.name + "_UV_" + (exportItem.uvindex as string))
	
				animation = rexAddChild graphRoot uvName "Animation" 
				rexSetPropertyValue animation "OutputPath" rootfolder
				rexSetPropertyValue animation "ChannelsToExport" "(projectData19;projectData20;)"
				rexSetPropertyValue animation "AnimMoverTrack" "false"
				rexSetPropertyValue animation "AnimAuthoredOrient" "false"
				rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
				rexSetPropertyValue animation "AnimExtraMoverTrack" "mover"
				rexSetPropertyValue animation "AnimLocalSpace" "true"
				rexSetPropertyValue animation "AnimUvAnimIndex" (exportItem.uvindex as string)
				rexSetNodes animation
	
				clearSelection()
				
				
	
				if RsRexExportWithReport() == false then (
		
					progressEnd()
					return false
				)
				SaveClipFile ( rootfolder + "/" + uvName ) model
			)
		)		
		
		progressEnd()

		CreateAnimList exportList rootfolder
		
		selection = currsel
		animationRange = currAnimRange
			
		true
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	fn DoExportAnims = (
		
		if RsMapIscontainer then (
			containers = RsMapObjectGetTopLevelOpenContainers()
			for c in containers do (
				if ExportAnims c.children == false then return false
			)
		)
		else
			if ExportAnims rootnode.children == false then return false	
		
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnExport pressed do (

		-- Loop through all RsMapContainers for the scene.
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	

			if ( false == ( RsPreExport map ) ) then 
				return false
			DoExportAnims()
			RsPostExport map
				if (querybox "do you wish to build the map?") == true then (
				RsMapBuildImage()
			)
		)
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsMapAnimation close do (
	
		RsSettingWrite "rsmapanimation" "rollup" (not RsMapAnimation.open)
	)
)