-- Rockstar Map Bounds Export
-- Rockstar North
-- 11/07/2005
-- by Shaun

-- Export a map bounds section into the game

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "rockstar/util/attribute.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------

idxDynamic = getattrindex "Gta Object" "Is Dynamic"
idxInstanceCollision = getattrindex "Gta Object" "Instance Collision"
idxFixed = getattrindex "Gta Object" "Is Fixed"
idxFragment = getattrindex "Gta Object" "Is Fragment"
idxAttribute = getattrindex "Gta Object" "Attribute"
	
	
fn RsMiloObjectNeedsOwnCollision obj = (


	doesItNeedToHaveCollision = false

	for childobj in obj.children do (

		test = classof childobj

		if test == Gta_Particle or test == GtaExplosion or test == GtaProcObj or test == GtaSwayable then (

			doesItNeedToHaveCollision = true
			exit
		)
	)	
	
	doesItNeedToHaveCollision
)

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------
rollout RsBoundsRoll "Bounds Export"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnExportBounds "Export Bounds" width:100
	button btnTestBounds "Test Bounds" width:100
	button btnCreateXrefBounds "Create Xref Bounds" width:100

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	--
	-------------------------------------------------------------
	fn CreateStaticBoundsForInternalRefs modelsin = (
	
		idxPieceType = getattrindex "Gta Collision" "Piece Type"
		idxDayBrightness = getattrindex "Gta Collision" "Day Brightness"
		idxNightBrightness = getattrindex "Gta Collision" "Night Brightness"
		idxCollType = getattrindex "Gta Collision" "Coll Type"
	
		totalCount = 0
		
		irefObjList = #()

		for i = 1 to modelsin.count do (
		
			progressUpdate (100.0 * i / modelsin.count)
			if getProgressCancel() then throw("progress_cancel")
		
			obj = modelsin[i]
		
			if classof obj == InternalRef then (
				append irefObjList obj
				totalCount = totalCount + 1
			)
			else if classof obj == Gta_MILO then (
				for child in obj.children do (
					if classof child == GtaMloRoom then (
						for roomChild in child.children do (
							if classof roomChild == InternalRef then (
								append irefObjList roomChild
								totalCount = totalCount + 1
							)
						)
					)
				)
			)
		)

		currentObj = 1
	
		deleteset = #()
	
		progressStart "deleting ixref collision"
	
		for i = 1 to irefObjList.count do (
		
			progressUpdate (100.0 * currentObj / totalCount)
			if getProgressCancel() then throw("progress_cancel")
		
			obj = irefObjList[i]
		
			if classof obj == InternalRef then (
			
				currentObj = currentObj + 1
			
				for collobj in obj.children do (
				
					if classof collobj == Col_Mesh or classof collobj == Col_Box or classof collobj == Col_Sphere or classof collobj == Col_Capsule then (
					
						if isDeleted collobj == false then (
					
							append deleteset collobj
							--delete collobj
						)
					)
				)
			)
		)
	
		progressEnd()
		
		delete deleteset
	
		currentObj = 1
	
		progressStart "creating ixref collision"
	
		for i = 1 to irefObjList.count do (
		
			try (
		
				progressUpdate (100.0 * currentObj / totalCount)				
			) catch (
				
			)
		
			if getProgressCancel() then throw("progress_cancel")
		
			obj = irefObjList[i]
		
			if classof obj == InternalRef then (
			
				currentObj = currentObj + 1
					
				srcObj = ixref_gettarget obj
				
				copyCollision = false
				
				if srcObj != undefined then (
				
					if getattrclass srcObj == "Gta Object" then (
					
						if getattr srcObj idxDynamic == false then (
						
							copyCollision = true
						)
					)
				)				
				
				if copyCollision then (
				
					for collobj in srcObj.children do (
					
						if classof collobj == Col_Mesh or classof collobj == Col_Box or classof collobj == Col_Sphere or classof collobj == Col_Capsule then (
							
							newcollobj = copy collobj
							
							col2mesh collobj
							col2mesh newcollobj
							newcollobj.material = collobj.material
							mesh2col collobj
							mesh2col newcollobj
							
							valPieceType = getattr collobj idxPieceType
							valDayBrightness = getattr collobj idxDayBrightness
							valNightBrightness = getattr collobj idxNightBrightness
							valCollType = getattr collobj idxCollType
							
							setattr newcollobj idxPieceType valPieceType
							setattr newcollobj idxDayBrightness valDayBrightness
							setattr newcollobj idxNightBrightness valNightBrightness
							setattr newcollobj idxCollType valCollType
							
--							select collobj
--							copyattrs()
--							select newcollobj
--							pasteattrs()
							
							newcollobj.parent = obj
							newcollobj.transform = obj.transform * (collobj.transform * (inverse (collobj.parent.transform)))
							
							-- reset the transform on the new collision object
							newcollobj.parent = undefined
							resettransform newcollobj
							newcollobj.parent = obj
						)
					)				
				)
			)	
		)
		
		progressEnd()
		
		true
	)

	--------------------------------------------------------------
	-- create a set of static bounds representing the xref objects
	--------------------------------------------------------------
	fn CreateStaticBoundsForXrefs modelsin = (
	
		if RsMapIsGeneric then return true
	
		boundObjList = #()
		boundObjNameList = #()
	
		currentObj = 1
	
		deleteset = #()
	
		progressStart "deleting xref collision"
	
		for i = 1 to modelsin.count do (
		
			progressUpdate (100.0 * i / modelsin.count)
			if getProgressCancel() then throw("progress_cancel")
	
			obj = modelsin[i]
		
			if classof obj != XRefObject then (
			
				continue
			)	

			for childobj in obj.children do (
			
				if classof childobj == Col_Mesh and isdeleted childobj == false then (
				
					append deleteset childobj
				)
			)
		)
		
		progressEnd()
			
		delete deleteset
	
		i = 0
	
		progressStart "creating xref collision"
	
		for objid = 1 to modelsin.count do (
			
			progressUpdate (100.0 * objid / modelsin.count)
			if getProgressCancel() then throw("progress_cancel")
		
			obj = modelsin[objid]
		
			if classof obj != XRefObject then (
			
				continue
			)				

			objectName = (obj.objectname + "_bmesh")
			
			boundObjIdx = finditem boundObjNameList objectName
			newobj = undefined
			
			if boundObjIdx == 0 then (
	
				filename = RsConfigGetNetworkStreamDir() + "maps/" + (RsRemovePathAndExtension obj.filename) + "/bound_import/" + obj.objectname + ".bnd"
				
				maplist = #()
				format "Creating bounds from BND file: % for object %\n" filename objectName
				newobj = CreateBoundsFromBndFile filename objectName maplist
				
				if newobj != undefined then (
				
					submatlist = #()
					
					for map in maplist do (
					
						local mat = trimLeft( map )
						mat = trimRight( mat )
						if ( "" == mat ) then
							continue -- skip
					
						mapTokens = filterstring mat "|"
	
						proc = ""
						roomID = 0
						popMult = 0
						flags = 0
					
						mtl = mapTokens[1]
						
						if mapTokens.count >= 6 then (
							proc = mapTokens[2]
							roomID = mapTokens[3] as integer
							popMult = mapTokens[4] as integer
							flags = mapTokens[5] as integer
														
							if proc == "0" then proc = ""
						)
						else if mapTokens.count == 5 then (
	
							roomID = mapTokens[2] as integer
							popMult = mapTokens[3] as integer
							flags = mapTokens[4] as integer
						)
						else (
							messagebox "Invalid material " + mat + " in bound file " + filename 
						)
					
						newsubmat = RexBoundMtl()		
						RexSetCollisionName newsubmat mtl
						RexSetProceduralName newsubmat proc
						RexSetCollisionFlags newsubmat flags
						
						append submatlist newsubmat
					)
					
					newmat = Multimaterial()
					newmat.numsubs = submatlist.count
					newmat.materialList = submatlist				
				
					col2mesh newobj
					newobj.material = newmat
					mesh2col newobj
				
					append boundObjList newobj
					append boundObjNameList objectName
				)
			
			) else (
				
				newobj = copy boundObjList[boundObjIdx]	
			)
			
			if newobj != undefined then (
	
				newobj.transform = obj.transform
				newobj.parent = obj
				
				i = i + 1
			)
		)
		
		progressEnd()
		
		return true
	)	

	--------------------------------------------------------------
	-- create the xml hierarchy
	--------------------------------------------------------------
	fn CreateStaticCollisionList modelsin = (
		xmlFileName = RsConfigGetScriptDir() + RsMapName + "/bnd_static.xml"
		RsMakeSurePathExists xmlFileName
				
		-- init xml doc
		bndStaticXml = XmlDocument()
		bndStaticXml.init()
		
		bndStaticXmlRoot = bndStaticXml.createelement("bndstatic")
		bndStaticXml.document.AppendChild bndStaticXmlRoot
		
		for i = 1 to modelsin.count do (
			model = modelsin[i]
			
			-- Each dictionary has only one bnd but organising it like
			-- this should mean dynamic and static collison content can
			-- be created in the same way
			bndDictElem = bndStaticXml.createelement(model)
			bndStaticXmlRoot.AppendChild(bndDictElem)
			
			bndElem = bndStaticXml.createelement(model)
			bndDictElem.AppendChild(bndElem)
		)
		
		
		result = bndStaticXml.save xmlFileName
		rubyString = "static_content = Pipeline::Content::MapBounds.new(\"" + xmlFileName + "\", project, map_name, \"bound_static\", \"ibn\", generic, true )\n"
		rubyString = rubystring + "component_group.add_child(static_content)\n\n"
		append RsContentScriptQueue rubyString
		result
	)
	
	
	--------------------------------------------------------------
	-- resourcify the collision for the passed in model set and destroy the source files
	--------------------------------------------------------------
	fn ResourcifyStaticCollision = (

		-- get the source bnd files

		modelsin = #()

		exportFiles = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_static/*.bnd"
		files = getfiles exportFiles

		if files.count == 0 then return true

		for i = 1 to files.count do (
			
			file = RsRemovePathAndExtension files[i]
			append modelsin file
		)

		-- create the xml hierarchy
		result = CreateStaticCollisionList modelsin
		
		return result
		
	)

	--------------------------------------------------------------
	-- create the xml hierarchy
	--------------------------------------------------------------	
	fn CreateDynamicCollisionList dirs = (
		xmlFileName = RsConfigGetScriptDir() + RsMapName + "/bnd_dynamic.xml"
		RsMakeSurePathExists xmlFileName

		-- init xml doc
		bndDynamicXml = XmlDocument()
		bndDynamicXml.init()

		bndDynamicXmlRoot = bndDynamicXml.createelement("bnddynamic")
		bndDynamicXml.document.AppendChild bndDynamicXmlRoot

		for dir in dirs do (
			
			dir = substring dir 1 (dir.count - 1)
			
			-- get the source bnd files

			outName = RsRemovePathAndExtension dir

			modelsin = #()

			exportFiles = dir + "/*.bnd"
			files = getfiles exportFiles

			if files.count == 0 then continue
			bndDictElem = bndDynamicXml.createelement(outName)
			bndDynamicXmlRoot.AppendChild(bndDictElem)
			for i = 1 to files.count do (

				file = RsRemovePathAndExtension files[i]

				bndElem = bndDynamicXml.createelement(file)
				bndDictElem.AppendChild(bndElem)
			)
			
		)
 
		result = bndDynamicXml.save xmlFileName
		rubyString = "dynamic_content = Pipeline::Content::MapBounds.new(\"" + xmlFileName + "\", project, map_name, Pipeline::OS::Path.combine(\"bound_dynamic\", map_name) , \"ibd\", generic, false )\n"
		rubyString = rubyString + "component_group.add_child(dynamic_content)\n\n"
		append RsContentScriptQueue rubyString
		result
	)


	

	--------------------------------------------------------------
	-- resourcify the collision for the passed in model set and destroy the source files
	--------------------------------------------------------------
	fn ResourcifyDynamicCollision = (

		rootDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_dynamic"
		dirs = getdirectories (rootDir + "/*")
		print "dirs"
		print dirs
		result = CreateDynamicCollisionList dirs
		
		return result
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	fn AddDynamicBoundObject allObj obj = (
	
		idxDontExport = getattrindex "Gta Object" "Dont Export"
	
		if getattrclass obj == "Gta Object" and classof obj != XrefObject then (
				
			if getattr obj idxFragment == true then (
			
				return 0
			)
				
			if RsMapIsGeneric == false then (
		
				if getattr obj idxDynamic == false then (
				
					return 0
				)
			)
			
			if getattr obj idxDontExport == true then (
			
				return 0
			)
			
			append allObj obj
		)		
	)

	fn AddMiloCollisionRec obj selectobjs miloParents parentObj &surfaceProperty &roomProperty allObj isInMilo intoOctree = (
	
		if ( classof obj == Col_Mesh ) or 
			 ( classof obj == Col_Box ) or 
			 ( classof obj == Col_Sphere ) or 
			 ( classof obj == Col_Cylinder ) or 
			 ( classof obj == Col_Capsule ) then 
		(

			append miloParents obj.pivot
			append selectobjs obj
			obj.pivot = parentObj.pivot

			surfaceType = RsGetCollSurfaceTypeString obj 
			surfaceProperty = surfaceProperty + obj.name + "="
			surfaceProperty = surfaceProperty + surfaceType + ":"
			
			roomID = RsGetCollRoomIDString obj 
			roomProperty = roomProperty + obj.name + "="
			roomProperty = roomProperty + roomID + ":"
		)
		
		if intoOctree == true and getattrclass obj == "Gta Object" then (
	
			intoOctree = false
	
			if (((getattr obj idxDynamic == false) or (getattr obj idxFixed == true)) and (getattr obj idxAttribute != 2)) then (
			
				if RsMiloObjectNeedsOwnCollision obj then (
			
					append allObj obj
					append isInMilo true
				) else (
			
					intoOctree = true			
				)
			) else (
			
				append allObj obj
				append isInMilo true
			)
		)
	
		if intoOctree then (
	
			for childobj in obj.children do (
			
				AddMiloCollisionRec childobj selectobjs miloParents parentObj &surfaceProperty &roomProperty allObj isInMilo intoOctree
			)		
		)
	)

	--------------------------------------------------------------
	-- exports the collision for the whole map section
	--------------------------------------------------------------
	fn ExportDynamicCollision modelsin = (
		
		exportForImportDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_import"
		exportRootDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_dynamic"
		
		RsDeleteFiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/*.?bd")
		RsDeleteDirectory exportRootDir
		
		RsMakeSurePathExists exportForImportDir
		RsDeleteFiles (exportForImportDir + "/*.bnd")
		
		cursel = #()
		cursel = cursel + selection
		
		allCollisionGroup = #()		
		allMiloObj = #()
		allObj = #()
		isInMilo = #()
		
		idxCollGroup = getattrindex "Gta Object" "Collision Group"
		
		for obj in modelsin do (
				
			isMilo = false
			findName = RsMapName
			
			if getattrclass obj == "Gta MILO" then (
				
				isMilo = true
				findName = obj.name
				
			) else if getattrclass obj == "Gta Object" then (

				findName = getattr obj idxCollGroup
				
				if findName == "" or findName == "undefined" then (
							
					findName = RsMapName
				)
			)
			
			findName = RsRemoveSpaces findName
			
			idxFound = finditem allCollisionGroup findName
			
			if idxFound == 0 then (
		
				append allCollisionGroup findName
				idxFound = finditem allCollisionGroup findName
				
				append allMiloObj #()
				append allObj #()
				append isInMilo #()
			)
			
			if isMilo then (
				
				append allMiloObj[idxFound] obj
			) else (
			
				AddDynamicBoundObject allObj[idxFound] obj
				append isInMilo[idxFound] false
			)
		)

		for j = 1 to allCollisionGroup.count do (
				
			exportDir = exportRootDir + "/" + allCollisionGroup[j]

			RsMakeSurePathExists exportDir
				
			surfaceProperty = ""
			roomProperty = ""

			maxCombinePolys = 300000

			progressStart "exporting milo collision"

			for i = 1 to allMiloObj[j].count do (

				obj = allMiloObj[j][i]
				
				progressUpdate (100.0*i/allMiloObj[j].count)
				if getProgressCancel() then throw("progress_cancel")

				exportAsOctree = true

				selectobjs = #()
				pivotParents = #()

				AddMiloCollisionRec obj selectobjs pivotParents obj &surfaceProperty &roomProperty allObj[j] isInMilo[j] true

				select selectobjs

				rexReset()

				if selectobjs.count == 0 then continue

				collpolys = getcollpolycount obj

				if exportAsOctree and (collpolys > maxCombinePolys) then (

					messagebox (obj.name + " has more than " + (maxCombinePolys as string) + " collision polys, can't export")
					continue
				) 

				graphRoot = rexGetRoot()
				bound = rexAddChild graphRoot obj.name "Bound"

				rexSetPropertyValue bound "ForceExportAsComposite" "false"

				rexSetPropertyValue bound "OutputPath" exportDir
				rexSetPropertyValue bound "SurfaceType" surfaceProperty
				rexSetPropertyValue bound "BoundRoomID" roomProperty
				rexSetPropertyValue bound "BoundExportWorldSpace" "false"
				rexSetPropertyValue bound "ApplyBulletShrinkMargin" "false"							
				rexSetPropertyValue bound "ExportAsComposite" "false"					
				rexSetPropertyValue bound "ExportAsOctree" "false"
				rexSetPropertyValue bound "ExportAsBvhGeom" "true"
				rexSetPropertyValue bound "BoundExportObjectSpace" "true"
				rexSetPropertyValue bound "MaxPolyCountToCombine" (maxCombinePolys as string)
				rexSetPropertyValue bound "MaxPolyCountOfCombined" (maxCombinePolys as string)
				rexSetPropertyValue bound "MaxDistanceToCombine" "10000000.0"
				rexSetPropertyValue bound "BoundOctreeMaxHeight" (8 as string)
				rexSetPropertyValue bound "BoundOctreeMaxPerCell" (22 as string)

				rexSetNodes bound

				try (

					rexExport()
				)
				catch ()

				if pivotParents.count > 0 then (

					for i = 1 to selectobjs.count do (

						selectobjs[i].pivot = pivotParents[i]
					)
				)
			)

			progressEnd()

			progressStart "exporting dynamic collision"

			for i = 1 to allObj[j].count do (

				obj = allObj[j][i]
				inMilo = isInMilo[j][i]

				progressUpdate (100.0*i/allObj[j].count)
				if getProgressCancel() then throw("progress_cancel")

				exportForImport = false
				exportAsOctree = false

				if(getattr obj idxAttribute == 2) then (

					if (getattr obj idxFragment == false) then (

						setattr obj idxFixed true
						exportAsOctree = true
					)

				) else if (getattr obj idxDynamic == false) or (getattr obj idxFixed == true) then (
		
					exportAsOctree = true
					exportForImport = true

					for childobj in obj.children do (

						if ( classof childobj == Col_Sphere ) or 
						   ( classof childobj == Col_Cylinder ) or
						   ( classof childobj == Col_Capsule ) then (
		
							exportAsOctree = false
							exportForImport = false
							exit
						)
					)					
				)

				if (getattr obj idxDynamic == true) or ( RsMapIsGeneric == false ) or ( inMilo == true ) then (

					exportForImport = false
				)

				selectobjs = #()
				pivotParents = #()

				for childobj in obj.children do (

					if ( classof childobj == Col_Mesh ) or 
						 ( classof childobj == Col_Box ) or 
						 ( classof childobj == Col_Sphere ) or 
						 ( classof childobj == Col_Cylinder ) or 
						 ( classof childobj == Col_Capsule ) then
					(

						if classof childobj == Col_Mesh and (hasproperty childobj "pivot" == false) then (

							col2mesh childobj
							mesh2col childobj
						)

						if exportAsOctree then (

							append pivotParents childobj.pivot
							append selectobjs childobj

							childobj.pivot = obj.pivot				
						) else (

							append selectobjs childobj
						)

						surfaceType = RsGetCollSurfaceTypeString childobj 
						surfaceProperty = surfaceProperty + childobj.name + "="
						surfaceProperty = surfaceProperty + surfaceType + ":"
					)
				)

				select selectobjs

				rexReset()

				if selectobjs.count == 0 then continue

				collpolys = getcollpolycount obj

				if exportAsOctree and (collpolys > maxCombinePolys) then (

					messagebox (obj.name + " has more than " + (maxCombinePolys as string) + " collision polys, can't export")
					continue
				) 

				graphRoot = rexGetRoot()
				bound = rexAddChild graphRoot obj.name "Bound"

				if ((getattr obj idxAttribute != 2) or (getattr obj idxFragment == false)) then (

					rexSetPropertyValue bound "ForceExportAsComposite" "false"
				) else (

					rexSetPropertyValue bound "ForceExportAsComposite" "true"
				)

				if exportForImport and RsMapIsGeneric then (
				
					rexSetPropertyValue bound "OutputPath" exportForImportDir
				) else (

					rexSetPropertyValue bound "OutputPath" exportDir
				)
				
				rexSetPropertyValue bound "SurfaceType" surfaceProperty
				rexSetPropertyValue bound "BoundRoomID" roomProperty
				rexSetPropertyValue bound "BoundExportWorldSpace" "false"
				rexSetPropertyValue bound "ApplyBulletShrinkMargin" "false"

				if exportAsOctree then (

					rexSetPropertyValue bound "ExportAsComposite" "false"					
					rexSetPropertyValue bound "ExportAsOctree" "false"
					rexSetPropertyValue bound "ExportAsBvhGeom" "true"
					rexSetPropertyValue bound "MaxPolyCountToCombine" (maxCombinePolys as string)
					rexSetPropertyValue bound "MaxPolyCountOfCombined" (maxCombinePolys as string)
					rexSetPropertyValue bound "MaxDistanceToCombine" "10000000.0"
					rexSetPropertyValue bound "BoundOctreeMaxHeight" (8 as string)
					rexSetPropertyValue bound "BoundOctreeMaxPerCell" (22 as string)
				) else (

					rexSetPropertyValue bound "ExportAsOctree" "false"
					rexSetPropertyValue bound "ExportAsBvhGeom" "false"
					rexSetPropertyValue bound "ExportAsComposite" "true"
					
					if selection.count > 1 then (
					
						rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "true"
					) else (
					
						rexSetPropertyValue bound "BoundZeroPrimitiveCentroids" "false"
					)
				)

				rexSetNodes bound

				try (

					rexExport()
				)
				catch ()

				if pivotParents.count > 0 then (

					for i = 1 to selectobjs.count do (

						selectobjs[i].pivot = pivotParents[i]
					)
				)
			)

			progressEnd()
		)
			
		select cursel
		
		return true
	)

	--------------------------------------------------------------
	-- used to get the sets of objects for octree generation
	--------------------------------------------------------------
	fn ExportStaticCollisionCreateSets modelsin octreeSize = (
	
		allObj = #()
	
		idxDontExport = getattrindex "Gta Object" "Dont Export"
		idxDontAddIpl = getattrindex "Gta Object" "Dont Add To IPL"
		idxForceCollision = getattrindex "Gta Object" "Force Collision Export"
		
		for obj in modelsin do (
		
			addObject = true
		
			if ( Container == classof obj ) then (
				
				addObject = false -- Don't add the Container helper itself
				
				for o in obj.children do (
					
					if ( "Gta Object" == GetAttrClass o ) then
					(
						if ( ( false == GetAttr o idxDynamic ) and ( false == GetAttr o idxFragment ) and
							( false == GetAttr o idxDontExport ) and ( false == GetAttr o idxDontAddIpl ) ) then
							append allObj o
					)
					else if ( "Gta Collision" == GetAttrClass o ) then
					(
						append allObj o
					)
				)
			)
			else if getattrclass obj == "Gta Object" then (
				
				if getattr obj idxDynamic == true then (
				
					addObject = false
				)
	
				if getattr obj idxFragment == true then (
				
					addObject = false
				)

				if RsMapIsPatch == false then (

					if getattr obj idxDontExport == true then (

						addObject = false
					)

					if getattr obj idxDontAddIpl == true then (

						addObject = false
					)			
				)
			) else (

				if classof obj != Col_Mesh and classof obj != Col_Box and classof obj != InternalRef then (
				
					addObject = false
				)
			)
	
			if addObject then (
	
				append allObj obj
			)
		)
				
		sections = CreateBinaryTree allObj octreeSize

		return sections		
	)

	--
	-- name: ExportStaticCollisionInner
	-- desc: Export static map collision for the specified models.
	--
	fn ExportStaticCollisionInner modelsin = (
		
		with redraw off 
		(
			exportDir = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/bound_static"
			
			RsDeleteDirectory exportDir
			RsMakeSurePathExists exportDir
			RsDeleteFiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/*.?bn")
		
			if RsDeleteFiles (exportDir + "/*.bnd") == false then (
			
				messagebox "couldnt delete bounds files for static collision export"
				return false
			)
			
			if RsMapIsGeneric == true then (
			
				return true
			)
			
			octreeSize = 250000
			cursel = #()
			cursel = cursel + selection
			
			sections = ( ExportStaticCollisionCreateSets modelsin octreeSize )
			
			octreeSize = 32000
			
			if sections == false then return true
	
			progressStart "exporting static collision"
			
			actualCount = 0
			
			for i = 1 to sections.count do (
	
				selectobjs = #()
	
				section = sections[i]
				foundCol = false
	
				progressUpdate (100.0*i/sections.count)
				if getProgressCancel() then throw("progress_cancel")
	
				for obj in section do (
	
					if getattrclass obj == "Gta Object" or classof obj == InternalRef then (
	
						for childobj in obj.children do (
		
							if classof childobj == Col_Mesh or classof childobj == Col_Box then (
											
								if(foundCol == false) then (
	
									foundCol = true
									actualCount += 1
								)
								
								append selectobjs childobj
							)
						)
					)
					
					if classof obj == Col_Mesh or classof obj == Col_Box then (
					
						if(foundCol == false) then (
	
							foundCol = true
							actualCount += 1
						)
						
						append selectobjs obj
					)				
				)
				
				select selectobjs
		
				if selectobjs.count > 0 then (
		
					rexReset()
		
					graphRoot = rexGetRoot()
					surfaceProperty = ""
		
					for selobj in selectobjs do (
		
						surfaceType = RsGetCollSurfaceTypeString selobj
		
						surfaceProperty = surfaceProperty + selobj.name + "="
						surfaceProperty = surfaceProperty + surfaceType + ":"
					)
		
					bound = rexAddChild graphRoot (RsMapName + "_" + (actualCount as string)) "Bound"
		
					rexSetPropertyValue bound "OutputPath" exportDir
					rexSetPropertyValue bound "ExportAsOctree" "false"
					rexSetPropertyValue bound "ExportAsBvhGeom" "true"
					rexSetPropertyValue bound "ApplyBulletShrinkMargin" "false"
					rexSetPropertyValue bound "ExportAsComposite" "true"
					rexSetPropertyValue bound "ForceExportAsComposite" "false"	
					rexSetPropertyValue bound "BoundExportWorldSpace" "true"
					rexSetPropertyValue bound "BoundSetToNodeName" "false"
					rexSetPropertyValue bound "BoundMaxPolyCountToCombine" (octreeSize as string)
		--			rexSetPropertyValue bound "MaxPolyCountToCombine" (octreeSize as string)
		--			rexSetPropertyValue bound "MaxPolyCountOfCombined" (octreeSize as string)
					rexSetPropertyValue bound "SurfaceType" surfaceProperty
					rexSetPropertyValue bound "BoundOctreeMaxHeight" (8 as string)
					rexSetPropertyValue bound "BoundOctreeMaxPerCell" (22 as string)
					rexSetPropertyValue bound "BoundHasSecondSurface" "true"
					rexSetPropertyValue bound "BoundSecondSurfaceMaxHeight" (1.0 as string)
					rexSetPropertyValue bound "BoundColourTemplateFile" (RsConfigGetProjBinConfigDir() + "/maps/" + RsMapLevel + "/ground_palette.xml")
					
					rexSetNodes bound
			
					rexExport()
				)
				
				files = getfiles (exportDir + "/*.bnd")
				
				for j = 1 to files.count do (
				
					renamefile files[j] (exportDir + "/" + RsMapName + "_" + (i as string) + "_" + (j as string) + ".bnd")
				)
			)
			
			progressEnd()
			
			files = getfiles (exportDir + "/*.bnd")
	
			for i = 1 to files.count do (
	
				renamefile files[i] (exportDir + "/" + RsMapName + "_" + (i as string) + ".bnd")
			)		
			
			select cursel
		)
		return true
	)

	--------------------------------------------------------------
	-- exports the collision for the whole map section, with xref bounds creation and deletion
	--------------------------------------------------------------
	fn ExportStaticCollision modelsin = (
	
		-- Check whether we need to ask this question
		if RsMapHasXRef then
		(
			if (RsQueryBoxTimeOut "rebuild xref collision?") then 
			(
				if CreateStaticBoundsForXrefs modelsin == false then return false
			)
		)
		else if RsMapHasIRef then
		(
			if (RsQueryBoxTimeOut "rebuild iref collision?") then 
			(
				if CreateStaticBoundsForInternalRefs modelsin == false then return false
			)
		) 
		else 
		(
			format "NONE OF THOSE.............."
		)
		
		-- Return result of ExportStaticCollisionInner
		( ExportStaticCollisionInner modelsin )
	)

	--------------------------------------------------------------
	-- export the passed in models Bounds
	--------------------------------------------------------------
	fn ExportBounds modelsin = 
	(		
		gc()
		local retVal = True
		undo off 
		(
			if ExportStaticCollision modelsin == false do retVal = false
			if retVal and ResourcifyStaticCollision() == false do retVal = false
			if retVal and ExportDynamicCollision modelsin == false do retVal = false
			if retVal and ResourcifyDynamicCollision() == false do retVal = false
		)
		true
	)

	fn DoCreateXrefBounds modelsin = (
	
		if CreateStaticBoundsForXrefs modelsin == false then return false
		if CreateStaticBoundsForInternalRefs modelsin == false then return false		
	)

	--------------------------------------------------------------
	-- export Bounds button pressed
	--------------------------------------------------------------
	fn DoExportBounds modelsin = (
	
		if ( false == ExportBounds modelsin ) then 
			return false
			
		true
	)
	
	fn RecDoTestBounds parobj = (
	
		for obj in parobj.children do (
		
			if classof obj == Col_Mesh then (
			
				col2mesh obj
				
				numverts = getnumverts obj
				
				for i = 1 to numverts do (
				
					checkvert = getvert obj i
					
					badVert = false
					
					for j = 1 to 3 do (
					
						if (bit.isNan checkvert[j]) then (
						
							badVert = true
						)
					)
					
					if badVert then (
					
						print ("vert " + (i as string) + " is bad in " + obj.name)
					)
				)
				
				mesh2col obj
			)
			
			RecDoTestBounds obj
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	fn DoTestBounds modelsin = (
	
		for m in modelsin do
			RecDoTestBounds m
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	on btnCreateXrefBounds pressed do (
	
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
			if ( false == RsPreExport map ) then
				return false
				
			DoCreateXrefBounds map.objects
			RsPostExport map
			
			-- Build image/rpf
			local ss = stringStream ""
			format "do you want to build the % map?" RsMapName to:ss
			if ( RsQueryBoxTimeOut (ss as string) ) then (
				RsMapBuildImage()
			)
		)
	)

	--------------------------------------------------------------
	-- export Bounds button pressed
	--------------------------------------------------------------
	on btnExportBounds pressed do (
	
		local maps = RsMapGetMapContainers()
		for map in maps do
		(
			if ( not map.is_exportable() ) then
				continue	
			if ( false == RsPreExport map ) then 
				return false
			
			DoExportBounds map.objects
			RsPostExport map
			
			-- Build image/rpf
			local ss = stringStream ""
			format "do you want to build the % map?" RsMapName to:ss
			if ( RsQueryBoxTimeOut (ss as string) ) then (
				RsMapBuildImage()
			)
		)
	)
	
	on btnTestBounds pressed do (
	
		local maps = RsMapGetMapContainers()
		
		for map in maps do
		(	
			if ( not map.is_exportable() ) then
				continue	
			
			if ( false == RsPreExport map docheck:false ) then
				return false
				
			DoTestBounds map.objects
		)
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsBoundsRoll close do (
	
		RsSettingWrite "rsmapbounds" "rollup" (not RsBoundsRoll.open)
	)
)
