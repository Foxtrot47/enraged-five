idxHasAnim = getattrindex "Gta Object" "Has Anim"
idxHasUvAnim = getattrindex "Gta Object" "Has UvAnim"

fn RsSetTransFlagsRec obj = (

	if getattrclass obj == "Gta Object" then (
		
		if obj.pos.controller.keys.count > 0 or obj.rotation.controller.keys.count > 0 then (
			
			if obj.pos.controller.keys.count > 0 then (
			
				setuserprop obj "exportTrans" true
			)	
		)	

		for childobj in obj.children do (

			RsSetTransFlagsRec childobj
		)
	)	
)


--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimatedRec obj = (

	retval = false

	if getattrclass obj == "Gta Object" then (
		
		if obj.pos.controller.keys.count > 0 or obj.rotation.controller.keys.count > 0 then (
			
			if obj.pos.controller.keys.count > 0 then (
			
				setuserprop obj "exportTrans" true
			)
			
			retval = true
		)
		
		if retval == false then (
		
			for childobj in obj.children do (

				if RsIsObjectAnimatedRec childobj then (

					retval = true
				)
			)
		)
	)
	
	retval
	
)

fn RsIsValidBoneForMap rootbone = (

	retval = true

	if (classof rootbone != Editable_mesh) and (classof rootbone != Editable_poly) and (classof rootbone != Point) and (classof rootbone != Biped_Object) and (classof rootbone != BoneGeometry) then (
		
		retval = false
	)
	
	retval
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimated obj = (

	retval = false

	if getattrclass obj == "Gta Object" then (
		
		if getattr obj idxHasAnim then (
		
			rootbone = rexGetSkinRootBone obj
		
			if rootbone == undefined then (
		
				-- dont look at root

				for childobj in obj.children do (

					if RsIsObjectAnimatedRec childobj then (

						retval = true
					)
				)

				if retval == false then (

					setattr obj idxHasAnim false
				)
			) else (

				if RsIsValidBoneForMap rootbone == false then (			
--				if classof rootbone != Editable_Mesh and classof rootbone != Point and classof rootbone != Biped_Object and classof obj != BoneGeometry then (
				
					messagebox (obj.name + " has skinned animation but invalid root bone, disabling")
					setattr obj idxHasAnim false
					retval = false
				) else (
			
					RsSetTransFlagsRec rootbone
			
					retval = true
				)
			)
		)
	)
	
	retval
	
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetAnimRangeRec model animRange = (
	
	if RsIsValidBoneForMap model then (
	
		if model.pos.controller.keys.count > 1 then (

			newAnimRange = (interval model.pos.controller.keys[1].time model.pos.controller.keys[model.pos.controller.keys.count].time)

			if animRange.start == -1 or animRange.start > newAnimRange.start then (

				animRange.start = newAnimRange.start
			)

			if animRange.end == -1 or animRange.end < newAnimRange.end then (

				animRange.end = newAnimRange.end
			)
		)

		if model.rotation.controller.keys.count > 1 then (

			newAnimRange = (interval model.rotation.controller.keys[1].time model.rotation.controller.keys[model.rotation.controller.keys.count].time)

			if animRange.start == -1 or animRange.start > newAnimRange.start then (

				animRange.start = newAnimRange.start
			)

			if animRange.end == -1 or animRange.end < newAnimRange.end then (

				animRange.end = newAnimRange.end
			)
		)
	)
	
	for childobj in model.children do (
	
		RsGetAnimRangeRec childobj animRange
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetAnimRange model = (

	rootbone = rexGetSkinRootBone model
	
	if rootbone != undefined then model = rootbone

	retAnimRange = (interval -1 -1)
	
	for childobj in model.children do (
	
		RsGetAnimRangeRec childobj retAnimRange
	)
	
	if retAnimRange.start > retAnimRange.end then (
	
		tempVal = retAnimRange.start
		retAnimRange.start = retAnimRange.end
		retAnimRange.end = tempVal
	)
	
	retAnimRange
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsGetTexMapUVAnimRange texmap = (
	
	animationRange = (interval 0f 3000f)
	currentAnimRange = animationRange
	
	beginUV = undefined
	endUV = undefined
	
	ctrlList = #()
	
	append ctrlList texmap.coords.u_offset.controller
	append ctrlList texmap.coords.v_offset.controller
	append ctrlList texmap.coords.W_Angle.controller
	
	for currentCtl in ctrlList do (
		
		if (currentCtl != undefined) and (currentCtl.keys.count > 1) then (
			
			if beginUV == undefined or beginUV > currentCtl.keys[1].time then (
			
				beginUV = currentCtl.keys[1].time
			)
			
			if endUV == undefined or endUV < currentCtl.keys[currentCtl.keys.count].time then (
			
				endUV = currentCtl.keys[currentCtl.keys.count].time
			)
		)	
	)
	
	if beginUV != undefined and beginUV > currentAnimRange.start then (
	
		currentAnimRange.start = beginUV
	)
	
	if endUV != undefined and endUV < currentAnimRange.end then (
	
		currentAnimRange.end = endUV 
	)
	
	if currentAnimRange.start > currentAnimRange.end then (

		tempVal = currentAnimRange.start
		currentAnimRange.start = currentAnimRange.end
		currentAnimRange.end = tempVal
	)
	
	currentAnimRange
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectUVAnimatedRec model = (

	local foundAnim = false

	if getattrclass model == "Gta Object" then (
	
		foundAnim = false

		texmaplist = #()
		RsGetMainTexMapsFromMaterial model model.material texmaplist

		for texmap in texmaplist do (

			if foundAnim == false and classof texmap == Bitmaptexture and rexIsTexMapAnimated texmap then (

				foundAnim = true
			)
		)
		
		for childobj in model.children do (
		
			local foundRet = RsIsObjectUVAnimatedRec childobj
			
			if foundRet == true then (
			
				foundAnim = foundRet
			)
		)
	)
	
	foundAnim
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectUVAnimated model = (

	foundAnim = false

	if getattrclass model == "Gta Object" then (

		if getattr model idxHasUvAnim then (

			foundAnim = RsIsObjectUVAnimatedRec model
			
			if foundAnim == false then (
			
				setattr model idxHasUvAnim false
			)
		)
	)
	
	foundAnim 
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsIsObjectAnimatedAtAll model = (

	-- model animation
	foundAnim = false

	if RsIsObjectAnimated model then (

		foundAnim = true
	)

	-- uv animation
	if RsIsObjectUVAnimated model then (
	
		foundAnim = true
	)
	
	foundAnim
)