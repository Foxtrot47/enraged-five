-- 
-- File:: rockstar/export/settings.ms
-- Description:: Configuration data functions.
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 March 2005
--
-- DHM 25 November 2009
-- With the tools framework we should really stop duplicating the project 
-- data into the RsConfigInfo struct and use the new Project struct directly
-- altering the functions in here to return data based on that.
--

-- setting.ms is included in many scripts.  This ensures the global is only 
-- declared if it doesn't already exist.  Declaring it as a global in config.ms 
-- results in it seen as undefined during project creation.
if RsConfig == undefined then 
	global RsConfig = undefined

-----------------------------------------------------------------------------
-- Includes
-----------------------------------------------------------------------------
include "rockstar/util/settingsave.ms"
include "pipeline/util/xml.ms"
include "pipeline/util/file.ms"
include "pipeline/export/config.ms"
include "pipeline/export/project.ms"

-- This is only used to get the path for the configswitch require.  It should only be used for this
fn RsGetRootDirForConfigSwitch = (
	RsMakeSafePath( RsConfig.toolsroot + "\\dcc" )
)

RsToolsInstalled = true

-- First things first, check pipeline.xml exists 
pipelinexmlfile = RsGetRootDirForConfigSwitch() + "/current/max2011/plugcfg/pipeline.xml"
foundfile = getfiles pipelinexmlfile
if foundfile.count == 0 then (
	
	messagebox "No pipeline.xml file found your tools will not load. Have you run the tools installer (x:/tools/install.bat)?"
	RsToolsInstalled = false
)


leadingPath = RsGetRootDirForConfigSwitch()
if leadingPath != undefined then (
	filein (leadingPath + "/common/configswitch_clean.ms")
)

-----------------------------------------------------------------------------
-- Structure Definitions
-----------------------------------------------------------------------------

--
-- struct: RsConfigInfo
-- desc: Project configuration data.  This is the old struct and reads most 
--       of its data from pipeline/export/project.ms: Project struct.
--
struct RsConfigInfo 
(
	name, 
	projrootdir, 
	bindir, 
	contentdir, 
	texdir, 
	streamdir, 
	genstreamdir, 
	networkdir, 
	speedtexdir, 
	ispc, 
	isps3, 
	isxenon, 
	rbbin, 
	showexportlog, 
	forcetextureexport, 
	shadersdir, 
	isepisodic, 
	haslevels,
	artdir,
	projbinconfigdir,
	projconfigdir,
	independentdir,
	commondir
)

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsTargets = #("pc","xenon","ps3", "common")
RsRageTargets = #("win32","xenon","ps3")
RsConfigInfos = #()
configNames = #()

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

rexSetForceTextureExport false

for i = 1 to RsConfig.projects.count do (

	project = RsConfig.projects[i]
	name = project.name	
	projrootdir = RsMakeSafePath( project.root )
	bindir = RsMakeSafePath( RsConfig.toolsbin )
	contentdir = RsMakeSafePath( RsConfig.toolsconfig + "\\content" ) 
	projconfigdir = RsMakeSafePath( RsConfig.toolsconfig )
	projbinconfigdir = RsMakeSafePath( RsConfig.toolsconfig + "\\config" ) 
	texdir = RsMakeSafePath( project.nettexture )
	streamdir = RsMakeSafePath( project.netstream )
	genstreamdir = RsMakeSafePath( project.netgenstream )
	networkdir = RsMakeSafePath( project.network )
	speedtexdir = RsMakeSafePath( project.netsttexture )
	shadersdir = RsMakeSafePath( project.shaders )
	independentdir = RsMakeSafePath( project.independent )
	commondir = RsMakeSafePath( project.common )
	if project.isepisodic == "true" then isepisodic = true
	else isepisodic = false
	if project.haslevels == "true" then haslevels = true
	else haslevels = false
	artdir = RsMakeSafePath( project.art )
	
	ispc = false
	isps3 = false
	isxenon = false
	rbbin = "ragebuilder_0190.exe"  -- default for the time being
	forcetextureexport = project.forcetextureexport as BooleanClass
	showexportlog = project.showexportlog as BooleanClass
	for i = 1 to project.targets.count do
	(
		if ( "win32" == project.targets[i].platform ) then (
			ispc = project.targets[i].enabled as BooleanClass
			rbbin = RsRemovePath project.targets[i].ragebuilder -- We only ever use one platform for resourcing independent assets within max
		)
		else if ( "ps3" == project.targets[i].platform ) then
			isps3 = project.targets[i].enabled as BooleanClass
		else if ( "xbox360" == project.targets[i].platform ) then
			isxenon = project.targets[i].enabled as BooleanClass
	)

	-- We need a quick fix for this, laods of artists are being caught out
	if name != "gta_e2" then (
		append configNames name
		configInfo = (RsConfigInfo name projrootdir bindir contentdir texdir streamdir genstreamdir networkdir speedtexdir ispc isps3 isxenon rbbin showexportlog forcetextureexport shadersdir isepisodic haslevels artdir projbinconfigdir projconfigdir independentdir commondir)
		append RsConfigInfos configInfo
	)
)

RsCurrentConfig = RsConfig.currconfig -- Have had to put thi back in since it seems it's referenced in the exporters.  TODO:  remove all references

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigIsEpisodic = (

	RsConfigInfos[RsConfig.currconfig].isepisodic
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigHasLevels = (

	RsConfigInfos[RsConfig.currconfig].haslevels
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetProjDir platformID = (

	if platformID > RsTargets.count then (

		messagebox "error with platform id in RsConfigGetProjDir"
		platformID = RsTargets.count
	)

	RsConfigInfos[RsConfig.currconfig].projrootdir + "build/dev/" + RsTargets[platformID] + "/"	
)

fn RsConfigGetIndependentDir = 
(
	RsConfigInfos[RsConfig.currconfig].projrootdir + "build/dev/independent/"
)

-- Return project name key (e.g. "jimmy" )
fn RsConfigGetProjectName = (

	RsConfigInfos[RsConfig.currconfig].name
)

-- Return absolute path to project root directory.
fn RsConfigGetProjRootDir = (

	RsConfigInfos[RsConfig.currconfig].projrootdir
)

-- Return absolute path to project configuration data ($(toolsconfig))
fn RsConfigGetProjConfigDir = (
	RsConfigInfos[RsConfig.currconfig].projconfigdir
)

-- Return absolute path to export configuration data ($(toolsconfig)\config) 
fn RsConfigGetProjBinConfigDir = (
	RsConfigInfos[RsConfig.currconfig].projbinconfigdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetBinDir = (

	RsConfigInfos[RsConfig.currconfig].bindir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetContentDir = (

	RsConfigInfos[RsConfig.currconfig].contentdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetTexDir = (

	RsConfigInfos[RsConfig.currconfig].texdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetLodTexDir = (

	texdir = RsConfigInfos[RsConfig.currconfig].texdir

	(substring texdir 1 (texdir.count - 1) + "lod/")
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetStreamDir = (

	RsConfigInfos[RsConfig.currconfig].streamdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetPatchDir = (
	RsConfigInfos[RsConfig.currconfig].projrootdir + "build/dev/preview/" 
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsClearPatchDir = (

	projDir = RsConfigGetPatchDir()

	RsDeleteDirectory projDir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetNetworkStreamDir = (

	RsConfigInfos[RsConfig.currconfig].genstreamdir
)


--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetMapStreamDir = (

	if RsMapIsGeneric == true then (

		RsConfigInfos[RsConfig.currconfig].genstreamdir
	) else (

		RsConfigInfos[RsConfig.currconfig].streamdir
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetNetworkDir = (

	RsConfigInfos[RsConfig.currconfig].networkdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetSpeedTexDir = (

	RsConfigInfos[RsConfig.currconfig].speedtexdir
)

-- Return absolute path to project's independent directory.
fn RsConfigGetIndDir = (

	RsConfigInfos[RsConfig.currconfig].independentdir
)

fn RsConfigGetIndMapDir = (
	-- Need a generic solution for this.  We could store the equi levels value in the content system
	RsConfigGetIndDir() + "levels/"
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetScriptDir = (

	if RsMapIsGeneric == true then (

		RsConfigInfos[RsConfig.currconfig].genstreamdir + "mapscript/"
	) else (

		RsConfigInfos[RsConfig.currconfig].streamdir + "mapscript/"
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetAnimDir = (

	RsConfigInfos[RsConfig.currconfig].projrootdir + RsConfigInfos[RsConfig.currconfig].name + "_art/anim/"
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetProjBinDir = (

	RsConfigInfos[RsConfig.currconfig].projrootdir + "bin/" + RsConfigInfos[RsConfig.currconfig].name + "_general/"
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetProjContentDir = (

	-- DHM -- Nasty Hack of 10 February 2009.  We should really 
	-- have a configuration option to specify whether we are in
	-- our development branch rather than comparing the 
	-- toolsroot against a known value.
	if ( "x:/" == RsConfig.toolsroot ) then
	(
		-- This should be the default art branch.
		return ( RsConfigInfos[RsConfig.currconfig].projrootdir + "bin/content/" )
	)
	else
	(
		-- This is our pipedev development branch.
		return ( RsConfig.toolsroot + "/" + RsConfigInfos[RsConfig.currconfig].name + "/bin/content/" )
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetCutsceneDir = (
	RsConfigInfos[RsConfig.currconfig].projrootdir + "gta_assets/cuts/"
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetCommonDir = (
	RsConfigInfos[RsConfig.currconfig].commondir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetShadersDir = (
	RsConfigInfos[RsCurrentConfig].shadersdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetTextureSourceDir = (
	RsConfigInfos[RsConfig.currconfig].artdir + "textures/"
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetArtDir = (
	RsConfigInfos[RsConfig.currconfig].artdir
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsConfigGetProxyTextureSourceDir = (
	RsConfigInfos[RsConfig.currconfig].artdir + "textures_PROXY/"
)

--
-- name: RsConfigGetWildWestDir
-- desc: Return root directory of wildwest.
--
fn RsConfigGetWildWestDir = (
	RsMakeSafeSlashes( RsConfig.toolsroot + "/wildwest/" )
)

--------------------------------------------------------------
-- !!!OBSOLETE!!!
--------------------------------------------------------------
fn RsConfigGetRagebuilder = (

	RsConfigInfos[RsConfig.currconfig].rbbin
)

--------------------------------------------------------------
-- !!!OBSOLETE!!!
--------------------------------------------------------------
fn RsConfigGetToolsScriptDir = (
	"x:/tools_release/gta_bin/script/"
)

--------------------------------------------------------------
-- !!!OBSOLETE!!!
--------------------------------------------------------------
fn RsConfigGetPcExport = (

	RsConfigInfos[RsConfig.currconfig].ispc
)

--------------------------------------------------------------
-- !!!OBSOLETE!!!
--------------------------------------------------------------
fn RsConfigGetPS3Export = (

	RsConfigInfos[RsConfig.currconfig].isps3
)

--------------------------------------------------------------
-- !!!OBSOLETE!!!
--------------------------------------------------------------
fn RsConfigGetXenonExport = (

	RsConfigInfos[RsConfig.currconfig].isxenon
)


--------------------------------------------------------------
-- Substitute an env variable
--------------------------------------------------------------
fn RsEnvSub path = (
	resolvedpath = RsConfig.projects[RsConfig.currconfig].SubEnv path
	resolvedpath
)

--------------------------------------------------------------
-- figures out the platform enum from a platform string
--------------------------------------------------------------
fn RsTargetNameToID target = (

	retval = 0

	for i = 1 to RsTargets.count do (

		if RsTargets[i] == target then (

			retval = i
		)
	)

	retval
)

--------------------------------------------------------------
-- returns the platform string for the platform enum
--------------------------------------------------------------
fn RsTargetIDToName target = (

	retval = ""

	if target <= RsTargets.count then (

		retval = RsTargets[target]
	)

	retval
)

--------------------------------------------------------------
-- returns the platform string for the platform enum
--------------------------------------------------------------
fn RsTargetIDToRageName target = (

	retval = ""

	if target <= RsRageTargets.count then (

		retval = RsRageTargets[target]
	)

	retval
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsResetConfigs = (


	for project in RsConfig.projects do (

		project.__load false
		project.UpdateLocalConfig undefined
		RsSettingsRoll.SetCurrentConfig RsConfig.currConfig

	)

)

--currversion = RsSettingsReadInteger "rssettings" "currentversion" 0


if RsConfig.settingsversion < RsSettingsVersion then (
	RsResetConfigs()
)



rollout RsSettingsRoll "Settings"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	checkbox chkShowLog "Show Export Log" --checked:RsShowExportLog
	checkbox chkForceTexture "Force Texture Export" --checked:RsForceTextureExport
	dropdownlist lstConfig "Configs:"
	button btnReset "Reset to Defaults" align:#right
	groupbox grpTargets "Target" width:180 height:60 offset:[-10,0]
	checkbox chkPC "PC"  offset:[0,-38]
	checkbox chkPS3 "PS3"  offset:[50,-20]
	checkbox chkXenon "Xenon"  offset:[100,-20]
	groupbox grpDirectories "Directories" width:180  height:180 offset:[-10,20]
	edittext edtProjectRootFolder "Project Root:" offset:[0,-150]
	edittext edtBinFolder "Bin:"
	edittext edtTextureFolder "Texture:"
	edittext edtStreamFolder "Stream:"	
	edittext edtGenStreamFolder "Gen Stream:"
	edittext edtSpeedTreeFolder "ST Tex Dir:"

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- display all the info for the selected config
	--------------------------------------------------------------
	fn SetCurrentConfig index = (

		if index == 0 then return 0
		if index > RsConfigInfos.count then return 0

		if RsConfig.currconfig != index then (

			messagebox "You need to restart max for the shaders to be loaded correctly"
		)

		--RsCurrentConfig = index

		RsConfig.currconfig = index
		RsConfig.UpdateLocalConfig()
		--RsSettingWrite "rssettings" "currentconfig" RsCurrentConfig

		edtProjectRootFolder.text = RsConfigInfos[index].projrootdir		
		edtBinFolder.text = RsConfigInfos[index].bindir
		edtTextureFolder.text = RsConfigInfos[index].texdir
		edtStreamFolder.text = RsConfigInfos[index].streamdir
		edtGenStreamFolder.text = RsConfigInfos[index].genstreamdir
		edtSpeedTreeFolder.text = RsConfigInfos[index].speedtexdir
		chkPC.checked = RsConfigInfos[index].ispc
		chkPS3.checked = RsConfigInfos[index].isps3
		chkXenon.checked = RsConfigInfos[index].isxenon
		chkShowLog.checked = RsConfigInfos[index].showexportlog
		chkForceTexture.checked = RsConfigInfos[index].forcetextureexport

		-- set REX shaderlib path
		--RstSetShaderPath("x:/gta/gta_pc/shaders")

		if rslowercase(RsConfigInfos[index].projrootdir) == "x:/gta_e1/" or rslowercase(RsConfigInfos[index].projrootdir) == "x:/gta_e2/" then (

			RstSetShaderPath("x:/gta/build/common/shaders")
		) else (

			RstSetShaderPath (RsConfigInfos[index].projrootdir  + "build/dev/common/shaders") false
		)
	)

	--------------------------------------------------------------
	-- resets all configs to that in rsproj.xml
	--------------------------------------------------------------
	fn ResetConfigs = (

		RsResetConfigs()		

		-- setup the ui

		lstConfig.items = configNames
		lstConfig.selection = 1
		SetCurrentConfig 1
	)



	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- initialisation of rollout
	--------------------------------------------------------------
	on RsSettingsRoll open do (		

		lstConfig.items = configNames
		lstConfig.selection = RsConfig.currconfig

		SetCurrentConfig RsConfig.currconfig
	)

	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsSettingsRoll close do (
		RsSettingWrite "rssettings" "rollup" (not RsSettingsRoll.open)	
	)


	on btnSwitchConfig pressed do (
		RsUpdateMaxSystemPaths "gta_e2"	
		RstSetShaderPath "x:/gta/build/common/shaders" false
	)
	--------------------------------------------------------------
	-- when the user selects a different config from the drop down list
	--------------------------------------------------------------
	on lstConfig selected idx do (

		SetCurrentConfig idx

	)

	--------------------------------------------------------------
	-- reset button pressed
	--------------------------------------------------------------
	on btnReset pressed do (

		ResetConfigs()
	)

	--------------------------------------------------------------
	-- changed value of show export log
	--------------------------------------------------------------	
	on chkShowLog changed newVal do (

		--RsShowExportLog = newVal
		--RsSettingWrite "rssettings" "showexportlog" RsShowExportLog
		RsConfig.projects[RsConfig.currconfig].showexportlog = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkForceTexture changed newVal do (

		--RsForceTextureExport = newVal
		--rexSetForceTextureExport RsForceTextureExport
		--RsSettingWrite "rssettings" "forcetextureexport" RsForceTextureExport
		RsConfig.projects[RsConfig.currconfig].forcetextureexport = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)

	--------------------------------------------------------------
	-- changed value of pc config checkbox
	--------------------------------------------------------------	
	on chkPC changed newVal do (

		RsConfigInfos[RsConfig.currconfig].ispc = newVal
		pcTgt = Target "win32" "" newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig pcTgt
	)

	--------------------------------------------------------------
	-- changed value of ps3 config checkbox
	--------------------------------------------------------------	
	on chkPS3 changed newVal do (

		RsConfigInfos[RsConfig.currconfig].isps3 = newVal
		ps3Tgt = Target "ps3" "" newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig ps3Tgt
	)

	--------------------------------------------------------------
	-- changed value of xenon config checkbox
	--------------------------------------------------------------	
	on chkXenon changed newVal do (

		RsConfigInfos[RsConfig.currconfig].isxenon = newVal
		xenonTgt = Target "xbox360" "" newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig xenonTgt
	)

	--------------------------------------------------------------
	-- changed value of project folder
	-- do we want this to be user editable?
	--------------------------------------------------------------
	on edtProjectRootFolder entered newVal do (

		newVal = RsMakeSafePath newVal
		RsConfigInfos[RsConfig.currconfig].projrootdir = newVal
		edtProjectRootFolder.text = newVal
		RsConfig.projects[RsConfig.currconfig].root = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
		--RsSettingWrite "rssettings" ("config" + (lstConfig.selection as string) + "projrootdir") newVal
	)

	--------------------------------------------------------------
	-- changed value of bin folder
	-- do we want this to be user editable?
	--------------------------------------------------------------

	on edtBinFolder entered newVal do (

		newVal = RsMakeSafePath newVal
		RsConfigInfos[RsConfig.currconfig].bindir = newVal
		edtBinFolder.text = newVal
		--RsSettingWrite "rssettings" ("config" + (lstConfig.selection as string) + "bindir") newVal
	)

	--------------------------------------------------------------
	-- changed value of texture folder
	--------------------------------------------------------------
	on edtTextureFolder entered newVal do (

		newVal = RsMakeSafePath newVal

		edtTextureFolder.text = newVal
		RsConfig.projects[RsConfig.currconfig].nettexture = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)

	--------------------------------------------------------------
	-- changed value of texture folder
	--------------------------------------------------------------
	on edtStreamFolder entered newVal do (

		newVal = RsMakeSafePath newVal

		edtStreamFolder.text = newVal
		RsConfig.projects[RsConfig.currconfig].netstream = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)

	--------------------------------------------------------------
	-- changed value of texture folder
	--------------------------------------------------------------
	on edtGenStreamFolder entered newVal do (

		newVal = RsMakeSafePath newVal

		edtGenStreamFolder.text = newVal
		RsConfig.projects[RsConfig.currconfig].netgenstream = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)

	--------------------------------------------------------------
	-- changed value of texture folder
	--------------------------------------------------------------
	on edtSpeedTreeFolder entered newVal do (

		newVal = RsMakeSafePath newVal

		edtSpeedTreeFolder.text = newVal
		RsConfig.projects[RsConfig.currconfig].netsttexture = newVal
		RsConfig.projects[RsConfig.currconfig].UpdateLocalConfig undefined 
	)
)
