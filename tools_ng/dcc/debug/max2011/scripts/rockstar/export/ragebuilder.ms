-- Rockstar Ped Export
-- Rockstar North
-- 1/7/2005
-- by Greg Smith

filein "rockstar/export/settings.ms"

fn RsCreateSetupScriptWithStream streamFolder = (

	if (getfiles (RsConfigGetNetworkDir() + "disable.txt")).count != 0 then (

		messagebox "Export is disabled"
		return false
	)

	versionString = ""
	versionCurrString = ""

	versionFilename = RsConfigGetNetworkDir() + "rage_version.txt"
	versionCurrFilename = RsConfigGetBinDir() + "rage_version_curr.txt"

	if (getfiles versionCurrFilename).count != 0 then (
	
		if deletefile versionCurrFilename == false then (
		
			messagebox ("Error: couldn't delete " + versionCurrFilename)
			return false
		)
	)

	doscommand (RsConfigGetBinDir() + RsConfigGetRagebuilder() + " -version >> " + versionCurrFilename)

	versionFile = openFile versionFilename mode:"r"

	if versionFile != undefined then (

		versionString = readline versionFile
		close versionFile

		versionCurrFile = openFile versionCurrFilename mode:"r"
	
		if versionCurrFile != undefined then (
	
			versionCurrString = readline versionCurrFile
			close versionCurrFile
		) else (
		
				messagebox ("Error: couldn't find " + versionCurrFilename)
				return false		
		)		
		
--		if versionCurrString != versionString then (
--		
--			messagebox ("Wrong version of ragebuilder. Should be " + versionString + " but is " + versionCurrString + ". Get latest tools.")
--			return false
--		)
	)

	luaFileName = RsConfigGetProjBinDir() + "setup.rbs"
	RsMakeSurePathExists luaFileName
	
	luaFile = openfile luaFileName mode:"w+"

	if RsConfigGetPcExport() then (
		
		format "enabled_win32 = true\n" to:luaFile
	) else (
		format "enabled_win32 = false\n" to:luaFile
	)

	if RsConfigGetPS3Export() then (
		
		format "enabled_ps3 = true\n" to:luaFile
	) else (
		format "enabled_ps3 = false\n" to:luaFile
	)
	
	if RsConfigGetXenonExport() then (
		
		format "enabled_xenon = true\n" to:luaFile
	) else (
		format "enabled_xenon = false\n" to:luaFile
	)

	format "projectName = \"%\"\n" (RsConfigGetProjectName()) to:luaFile
	format "dirStream = \"%\"\n" (streamFolder) to:luaFile
	format "dirTexture = \"%\"\n" (RsConfigGetTexDir()) to:luaFile
	format "dirLodTexture = \"%\"\n" (RsConfigGetLodTexDir()) to:luaFile
	format "dirBin = \"%\"\n" (RsConfigGetBinDir()) to:luaFile
	format "set_platform(\"independent\")\n" to:luaFile

	close luaFile
	
	true
)

fn RsCreateSetupScript = (

	RsCreateSetupScriptWithStream (RsConfigGetStreamDir())
)

fn RsCreateMapSetupScript = (

	RsCreateSetupScriptWithStream (RsConfigGetMapStreamDir())
)