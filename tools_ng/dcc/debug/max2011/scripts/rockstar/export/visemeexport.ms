-- Rockstar Viseme Export
-- Rockstar North
-- by Luke Openshaw
-- Exports modifier info form Voice-O-Matic plugin. 

filein "rockstar/export/settings.ms"

filein "pipeline/util/string.ms"
filein "pipeline/util/file.ms"

if $head_000_r == undefined then (
	messagebox "Cannot find head_000_r object required for viseme export."
) else (
	

	select $head_000_r

	sel = selection[1]
	morphObj = undefined
	vFound = false

	for mod in sel.modifiers do (
		if mod.name == "VisemeMorph" then (
			morphObj = mod
			vFound = true
		)
	)

	if vFound == true then (

		print "adsadsadasd"

		temp = filterstring WAVSound.filename "\\"
		animName = temp[temp.count]
		animName = RsLowercase animName
		animName = RsRemoveExtension animName

		animPath = getsavefilename filename:(RsConfigGetAnimDir() + "export/AUDIO/viseme.anim") types:"rage anim file (*.anim)|*.anim"
		animPath = RsRemoveFilename filepath



		if mod == undefined then return false
		else (
			rexReset()
			graphRoot = rexGetRoot()

			animation = rexAddChild graphRoot animName "Animation"
			rexSetPropertyValue animation "OutputPath" animPath
			rexSetPropertyValue animation "ChannelsToExport" "(viseme;)"
			rexSetPropertyValue animation "AnimExportSelected" "false"


			rexSetNodes animation

			rexExport()
		)

		gc()
	)
	else messagebox "No Viseme Morpher found.  Check you have renamed the morpher to 'VisemeMorph'."
)