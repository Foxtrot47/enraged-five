filein "pipeline/util/string.ms"

RsSourceMaterial = #()
RsTargetMaterial = #()
RsTargetProcedural = #()

fn RsLoadConvertFile filename = (

	RsSourceMaterial = #()
	RsTargetMaterial = #()
	RsTargetProcedural = #()

	datfile = openfile filename
	
	if datfile == undefined then return false
	
	while eof datfile == false do (
	
		datline = readline datfile
		
		if datline.count > 0 and datline[1] != "#" then (
		
			lineTokens = filterstring datline " \t"
			
			if lineTokens.count > 3 then (
			
				if (substring lineTokens[3] 1 2) == "##" or lineTokens[3] == "-" then (
				
					lineTokens[3] = ""
				)
				
				lineTokens[1] = RsUppercase lineTokens[1]
				lineTokens[2] = RsUppercase lineTokens[2]
				lineTokens[3] = RsUppercase lineTokens[3]
				
				append RsSourceMaterial lineTokens[1]
				append RsTargetMaterial lineTokens[2]
				append RsTargetProcedural lineTokens[3]
			)
		)
	)
	
	close datfile
)

RsLoadConvertFile "X:\\GTA\\build\\common\\data\\materials\\mtl_convert.txt"

fn RsConvertCollisionOnMaterial mat = (

	if classof mat == RexBoundMtl then (
	
		collName = RexGetCollisionName mat 
		
		idFound = finditem RsSourceMaterial collName
		
		if idFound != 0 then (
		
			RexSetCollisionName mat RsTargetMaterial[idFound]
			RexSetProceduralName mat RsTargetProcedural[idFound]			
		)
	)
)

fn RsConvertCollisionOnObjectCollision childobj = (
	
	if classof childobj == Col_Mesh then (
	
		idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
		valSurfaceType = RsUppercase(getattr childobj idxSurfaceType)

		idFound = finditem RsSourceMaterial valSurfaceType 

		if idFound != 0 then (

			savetrans = childobj.transform
			col2mesh childobj

			if classof childobj.material == Multimaterial then (

				for childmat in childobj.material.materiallist do (

					if classof childmat == RexBoundMtl then (

						RsConvertCollisionOnMaterial childmat		
					)
				)
			)
			else if classof childobj.material == RexBoundMtl then (

				RsConvertCollisionOnMaterial childobj.material
			)
			else (

				boundmat = RexBoundMtl()

				RexSetCollisionName boundmat RsTargetMaterial[idFound]
				RexSetProceduralName boundmat RsTargetProcedural[idFound]

				childobj.material = Multimaterial()
				childobj.material.materiallist[1] = boundmat

				numFaces = getnumfaces childobj

				for i = 1 to numFaces do (

					SetfaceMatID childobj i 1
				)
			) 

			mesh2col childobj
			childobj.transform = savetrans
		)	
	) else if getattrclass childobj == "Gta Collision" then (
	
		idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
		valSurfaceType = RsUppercase(getattr childobj idxSurfaceType)
		
		idFound = finditem RsSourceMaterial valSurfaceType 
		
		if idFound != 0 then (
		
			setattr childobj idxSurfaceType RsTargetMaterial[idFound]
		)
	)
)

fn RsConvertCollisionOnObject obj = (

	if getattrclass obj == "Gta Object" then (
	
		for childobj in obj.children do (
					
			RsConvertCollisionOnObjectCollision childobj
		)
	) else (
	
		RsConvertCollisionOnObjectCollision obj
	)
)


rollout MatConvertRoll "Convert"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnConvert "Convert Selected"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnConvert pressed do (
	
		for obj in selection do (

			RsConvertCollisionOnObject obj
		)		
	)
)

try CloseRolloutFloater MatConvertUtil catch()
MatConvertUtil = newRolloutFloater "Collion Convert" 200 70 50 126
addRollout MatConvertRoll MatConvertUtil rolledup:false