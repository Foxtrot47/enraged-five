-- terrain_helperfunctions.ms
-- created by Gunnar Droege
-- written by Luke Openshaw
-- ripped out of the collision set tool for general use
-- 10/06/2010

filein "rockstar/util/terrain.ms"

global TERRAIN_PAINT_CHANNEL = 9

fn NormaliseColourFormat colour = (
	newcolour = color (colour.r/255) (colour.g/255) (colour.b/255)
)

-- Nicked wholesale from Alex H's terrain shader
fn GetAlpha rgb mask  = (
	
	alphaR = mask.r*rgb.x + (1.0-mask.r)*(1.0-rgb.x)
	alphaG = mask.g*rgb.y + (1.0-mask.g)*(1.0-rgb.y)
	alphaB = mask.b*rgb.z + (1.0-mask.b)*(1.0-rgb.z)
	
	local res = (alphaR*alphaG*alphaB)
-- 	print "+++++++++++"
-- 	print rgb
-- 	print mask
-- 	format ("% % %\n") alphaR alphaG alphaB
-- 	print res
-- 	print "+++++++++++"

	return res
)

fn GetTextureElementFromVertex obj faceId vertId normcolour maskTextureColour:undefined = (
	local currAlpha
	if classof obj == Editable_Mesh then (
		if undefined==maskTextureColour then 
		(
			local currFace = meshop.getMapFace obj TERRAIN_PAINT_CHANNEL faceId
			maskTextureColour=( meshop.getMapVert obj TERRAIN_PAINT_CHANNEL currFace[vertId] )
		)
	)
	else if classof obj == Editable_Poly then (
		if undefined==maskTextureColour then 
		(
			local currFace = polyop.getMapFace obj TERRAIN_PAINT_CHANNEL faceId
			maskTextureColour=( polyop.getMapVert obj TERRAIN_PAINT_CHANNEL currFace[vertId] )
		)
	)
	currAlpha = GetAlpha maskTextureColour normcolour
	return currAlpha
)

fn GetDominantTextureFromTerrainFace obj mat matID faceId = (
	LocalTexMapNames = #()
	LocalTexMapsBmps = #()
	collMaterial = "default"

	-- all textures
	RsSetMapsFlags true true false false
	RsGetTexMapsFromObjWithMaps obj LocalTexMapNames LocalTexMapsBmps id:matID
	RsSetMapsFlags true true true true
	avgColour = color 0 0 0
	-- diffuse textures
	numvars = RstGetVariableCount mat
	texlist = #()
	for i = 1 to numvars do (
		if RstGetVariableType mat i == "texmap" and (matchpattern (RstGetVariableName mat i) pattern:"Diffuse ?" == true) then (
			append texlist (RsRemovePath (RsRemoveExtension(RsLowercase(RstGetVariable mat i))))
		)
	)
-- 	print LocalTexMapNames
-- 	print texlist
	
	highestAlpaIdx = 1
	highestAlpha = 0.0
	
	local shadername = rstgetshadername mat
	local isMasked = (matchPattern shadername pattern:"*_cm*")
	local myUvFace = (meshop.getMapFace obj 1 faceId)
				
	if texlist.count > 0 then (
		for i = 1 to TexColList.count do (
			if undefined!=texlist[i] then
			(
				local currAlpha = 0.0
				local normColour = (NormaliseColourFormat TexColList[i])
				local lastBitmaptex = LocalTexMapsBmps[LocalTexMapsBmps.count]
				local theBmp = lastBitmaptex.bitmap
					
				for j = 1 to 3 do (
					local vertAlpha = undefined
					local colour = undefined
					if isMasked then
					(
						-- get colour from the mask texture
						local myUv = meshop.getMapVert obj 2 myUvFace[j]
						local moduloedUvs = [(mod myUv[1] 1.0), (mod myUv[2] 1.0)]
						if moduloedUvs[1] < 0 then moduloedUvs[1] = 1.0 + moduloedUvs[1]
						if moduloedUvs[2] < 0 then moduloedUvs[2] = 1.0 + moduloedUvs[2]
						moduloedUvs = moduloedUvs * (point2 theBmp.width theBmp.height)
						--print lastBitmaptex.bitmap.filename
						--try(
							colour = (getPixels theBmp moduloedUvs 1)[1]
							colour = (NormaliseColourFormat (getPixels theBmp moduloedUvs 1)[1]) as point3
						--)catch(print (getCurrentException()))
					)
					vertAlpha = GetTextureElementFromVertex obj faceId j normColour maskTextureColour:colour
					currAlpha = currAlpha + vertAlpha
				)
				currAlpha =  currAlpha/3
				if currAlpha > highestAlpha then (
					highestAlpaIdx = i
					highestAlpha = currAlpha
				)
			)
		)
	)
	
	print ("highestAlpaIdx:"+highestAlpaIdx as string)
	local textureIndex = findItem LocalTexMapNames texlist[highestAlpaIdx]
	if 0!=textureIndex then
	(
		return LocalTexMapsBmps[textureIndex]
	)
	
	return undefined
)

