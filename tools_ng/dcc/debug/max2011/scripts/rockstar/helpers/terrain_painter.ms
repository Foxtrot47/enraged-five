--
-- File:: rockstar/helpers/terrain_painter.ms
-- Description:: Data driven utilities for manipulating the terrain shader
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
--
-----------------------------------------------------------------------------
-- HISTORY
-- 11/11/2009
-- by Stuart Macdonald
-- Added undo to vert and face colouring
-- Fixed bitmap resize load ( line 399 )
--
-- 9/11/2009
-- by Marissa Warner-Wu
-- Incorporated terrain texture palette tool from Aaron/Stu.
--
-- 4/12/2008
-- by Luke Openshaw
-- Update: Hack in Multisub support
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------

filein "rockstar/util/shader_config_loader.ms"
filein "pipeline/util/devil.ms"
filein "rockstar/util/terrain.ms"
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
--global TexColList = #( color 0 0 0, color 0 0 255, color 0 255 0, color 0 255 255, color 255 0 0, color 255 0 255, color 255 255 0, color 255 255 255 )
global TexColNameList = #( "black", "blue", "green", "turquoise", "red", "purple", "yellow", "white" )
global TerrainObj
global TerrainPaintChannel = 9
global RageShaderName
global vPainttool
global testSrcBitmap

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--------------------------------------------------------------
-- Terrain texture palette (main rollout)
--------------------------------------------------------------
rollout RsTerrainPalette "Terrain Palette"
(
		
	--////////////////////////////////////////////////////////////
	-- variables
	--////////////////////////////////////////////////////////////
	local TexturePaths = #()
	local TerrainMaterials = #()
	local TextureList = #()
	
	-- Images displayed on the texture buttons, defaults to the layer colour
	local bmpBtnTex1
	local bmpBtnTex2
	local bmpBtnTex3
	local bmpBtnTex4
	local bmpBtnTex5
	local bmpBtnTex6
	local bmpBtnTex7
	local bmpBtnTex8
	
	local bmpDefault
	
	-- UI control groups
	local TexLabels = #()
	local TexButtons = #()
	local TexBmps = #()
	
	
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Terrain_Paint" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	

	button btnTexture1 "tex1" pos:[8,56] width:64 height:64 tooltip:"Texture 1"
	bitmap bmpTexture1 pos:[8,120] bitmap:(bitmap 64 24 color:TexColList[1])
	label lblTexture1 "Safe" pos:[8,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture2 "tex2" pos:[72,56] width:64 height:64 tooltip:"Texture 2"
	bitmap bmpTexture2 pos:[72,120] bitmap:(bitmap 64 24 color:TexColList[2])
	label lblTexture2 "Safe" pos:[72,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture3 "tex3" pos:[136,56] width:64 height:64 tooltip:"Texture 3"
	bitmap bmpTexture3 pos:[136,120] bitmap:(bitmap 64 24 color:TexColList[3])
	label lblTexture3 "Safe" pos:[136,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture4 "tex4" pos:[200,56] width:64 height:64 tooltip:"Texture 4"
	bitmap bmpTexture4 pos:[200,120] bitmap:(bitmap 64 24 color:TexColList[4])
	label lblTexture4 "Safe" pos:[200,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture5 "tex5" pos:[264,56] width:64 height:64 tooltip:"Texture 5"
	bitmap bmpTexture5 pos:[264,120] bitmap:(bitmap 64 24 color:TexColList[5])
	label lblTexture5 "Safe" pos:[264,150] style_sunkenedge:true width:64 height:16
		
	button btnTexture6 "tex6" pos:[328,56] width:64 height:64 tooltip:"Texture 6"
	bitmap bmpTexture6 pos:[328,120] bitmap:(bitmap 64 24 color:TexColList[6])
	label lblTexture6 "Safe" pos:[328,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture7 "tex7" pos:[392,56] width:64 height:64 tooltip:"Texture 7"
	bitmap bmpTexture7 pos:[392,120] bitmap:(bitmap 64 24 color:TexColList[7])
	label lblTexture7 "Safe" pos:[392,150] style_sunkenedge:true width:64 height:16
	
	button btnTexture8 "tex8" pos:[456,56] width:64 height:64 tooltip:"Texture 8"
	bitmap bmpTexture8 pos:[456,120] bitmap:(bitmap 64 24 color:TexColList[8])
	label lblTexture8 "Safe" pos:[456,150] style_sunkenedge:true width:64 height:16
	
	checkbutton chkVertPaint "Start Vertex Paint" pos:[552,50] width:96 height:32  tooltip:"Toggle vertex painting"
	button btnMore "More..." pos:[424,16] width:96 height:32  tooltip:"Open more terrain painting utilities"
	button btnRefresh "Refresh" pos:[312,16] width:96 height:32 tooltip:"Refresh the material list"
	dropDownList cboMaterials "Select a material" pos:[8,8] width:288 height:40
	
	spinner spnFillStrength "" pos:[568,110] width:64 height:16 range:[0,100,100] scale:1
	label lbFillStrength "Fill strength" pos:[568,90] width:72 height:16
	
	label lblClickHelp "Right click on a texture to view colour relationships" pos:[8,170]
	
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- Functions for painting by selection
	--------------------------------------------------------------
	fn PaintSelection index = (
		$.showVertexColors = true
		$.vertexColorType = #alpha
		
		if subobjectlevel == 4 then (
			faces = getfaceselection $
			if classof TerrainObj == Editable_mesh then (
				undo "fill faces" on meshop.setFaceColor $ 9 faces ( TexColList[index] )
				meshop.setFaceAlpha $ 9 faces ( spnFillStrength.value/100 )
				meshop.setFaceColor $ 9 faces ( TexColList[index] )
			) 
			else (
				if classof TerrainObj == Editable_Poly then (
					undo "fill faces" on polyop.setFaceColor $ 9 faces ( TexColList[index] )
					greyscale = ( [255, 255, 255] * ( spnFillStrength.value/100 ) ) as color
					polyop.setMapSupport $ -2 true
					polyop.setFaceColor $ -2 faces greyscale
					polyop.setFaceColor $ 9 faces ( TexColList[index] )
				)
				else messagebox "Please select an editable poly or editable mesh."
			)
		)
		else (	
			if subobjectlevel == 1 then (
				vertices = getvertselection $
				if classof TerrainObj == Editable_mesh then (
					undo "fill vertices" on meshop.setvertColor $ 9 vertices ( TexColList[index] )
					meshop.setVertAlpha $ 9 vertices ( spnFillStrength.value/100 )
					meshop.setVertColor $ 9 vertices ( TexColList[index] )
				)
				else (
					if classof TerrainObj == Editable_Poly then (
					undo "fill vertices" on polyop.setvertColor $ 9 vertices ( TexColList[index] )
					greyscale = ( [255, 255, 255] * ( spnFillStrength.value/100 ) ) as color
					polyop.setMapSupport $ -2 true
					polyop.setVertColor $ -2 vertices greyscale
					polyop.setVertColor $ 9 vertices ( TexColList[index] )
					)
					else messagebox "Please select an editable poly or editable mesh."
				)
			)
			else (
				messagebox "Selection must be at the vertex or polygon sub-object level."
			)
		)
		
		update $
	)
	
	--------------------------------------------------------------
	-- Functions for vertex painting
	--------------------------------------------------------------
	fn SetupModifier = (
		p = VertexPaint()
		p.mapchannel = TerrainPaintChannel
		addmodifier TerrainObj p
		
		vPainttool.paintColor = TexColList[1]
		vPainttool.mapDisplayChannel = TerrainPaintChannel
		vPainttool.curPaintMode = 1
		
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
	)
	
	fn ValidateTerrainObj = (
		local modNotFound = true
		for modv in TerrainObj.modifiers do (
			if classof modv == VertexPaint then (
				modNotFound = false
			)
		)
		return modNotFound
	)
	
	fn SetVertPaintColour index = (
		vPainttool.paintcolor = TexColList[index]
		vPainttool.curPaintMode = 1
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
	)
	
	--------------------------------------------------------------
	-- Functions for getting materials
	--------------------------------------------------------------
	fn PopulateTextureListForRageInner mat = (
			TexturePaths = #()
			texlist = #()
			
			numvars = RstGetVariableCount mat
			
			for i = 1 to numvars do (
				
				print (RstGetVariableName mat i)
				print (RstGetVariable mat i)
				if RstGetVariableType mat i == "texmap" and (matchpattern (RstGetVariableName mat i) pattern:"Diffuse ?" == true) and ""!=(RstGetVariable mat i) then 
				(
					append TexturePaths (RstGetVariable mat i)
					append texlist (RsRemovePath (RstGetVariable mat i))
				)
			)
			TextureList = texlist
			
			return true
	)
	
	fn GetDxTextures mat shader getpath:false = (
		-- has to be global to use in the execute command as it executes in the global scope
		global gmat = mat
		textureList = #()
		
		i = 1
		for shadervar in shader.shadervars do (
			if shadervar.type == "difftex" then (
				
				tex = execute ("gmat." + shadervar.name)
				
				if tex != undefined then (
					if getpath == true then insertItem tex.filename textureList i
					else insertItem (RsRemovePath tex.filename) textureList i
				)
				else insertItem "undefined" textureList i
				i = i + 1
			)
			
		)
		
		return textureList
	)
	
	fn PopulateTextureListForDxInner mat = (
		TexturePaths = #()
		
		dxshadername = RsRemovePathAndExtension mat.effectfile
		
		shaderExists = false
		
		shader = RsLoadShaderFromXml "terrain" dxshadername
		if shader != undefined then (
			
			TextureList = GetDxTextures mat shader
			
			TexturePaths = GetDxTextures mat shader getpath:true
			rageShader = Rage_Shader()
			RstSetShaderName rageShader (dxshadername + ".sps")
			mat.rendermaterial  = rageShader
		)
		else (
			messagebox ("Invalid shader selected.  Make sure it exists in " + ((getdir #plugcfg) + "rs_dx_shaders.xml"))
			return false
		)
		
		return true
	)
	
	fn PopulateTextureList mat = (

		dxShaderFound = false
		
		if classof mat == MultiMaterial then (	
			for submat in TerrainObj.mat.materiallist do (
				if classof submat == Rage_Shader then (
					dxShaderFound = PopulateTextureListForRageInner submat
				)
			)
		)
		else if classof mat == DirectX_9_Shader then (
			dxShaderFound = PopulateTextureListForDxInner mat
		)
		else if classof mat == Rage_Shader then (
			dxShaderFound = PopulateTextureListForRageInner mat
		)
		
		dxShaderFound
	)
	
	fn PopulateMaterialList = (
		TerrainMaterials = #()
		dxShaderFound = false

		if classof TerrainObj.mat == MultiMaterial then (	
			for submat in TerrainObj.mat.materiallist do (
				if classof submat == Rage_Shader then append TerrainMaterials submat
			)
		)
		else if classof TerrainObj.mat == DirectX_9_Shader then append TerrainMaterials TerrainObj.mat
		else if classof TerrainObj.mat == Rage_Shader then append TerrainMaterials TerrainObj.mat
		

		tempmatnames = #()
		
		for mat in TerrainMaterials do append tempmatnames mat.name
		cboMaterials.items = tempmatnames
				
		dxShaderFound
	)
	
	--------------------------------------------------------------
	-- Functions to scale a bitmap
	--------------------------------------------------------------
	fn scaleLine  SrcArray SrcWidth TarWidth  = (
		tempTarArray = #()
				
		local int NumPixels = TarWidth
		local int IntPart = SrcWidth / TarWidth
		local int FractPart = mod SrcWidth TarWidth
		local int SourcePixel = 1
		local int myE = 0
			
		for n = 1 to NumPixels do
		(
			tempTarArray[n] = SrcArray[SourcePixel]
			SourcePixel += IntPart
			
			myE += FractPart
			if myE >= TarWidth then
			(
					
				myE -= TarWidth
				SourcePixel += IntPart
			)
		)
				
		return tempTarArray
	)
					
	fn scaleBitmap tarBitmap srcBitmap = (
		local int tarHeight = tarBitmap.height
		local int tarWidth = tarBitmap.width
		local int srcHeight = srcBitmap.height
		local int srcWidth = srcBitmap.width
					
		local int NumPixels = tarHeight
		local int IntPart = srcHeight / tarHeight
		local int FractPart = mod srcHeight tarHeight
		local int myE = 0
					
		local int tempPixelArray = #()
		local int srcLine = 1
					
		for n = 1 to NumPixels do
		(
			srcPixelLine = getPixels srcBitmap [0, (srcLine-1)] srcWidth
			tempPixelArray = scaleLine srcPixelLine srcWidth tarWidth
							
			try setPixels tarBitmap [0,(n-1)] tempPixelArray catch ()
							
			srcLine += IntPart
						
			myE += FractPart
			if myE >= tarHeight then
			(
				myE -= tarHeight
				srcline += 1
			)
						
		)
					
		return tarBitmap
	)
	
	--------------------------------------------------------------
	-- Utility functions
	--------------------------------------------------------------
	fn ResetBmps = (
	-- Reset all the bmps to their default values
		plugcfgdir = getdir #plugcfg
		
		bmpDefault = openBitMap (plugcfgdir + "\\colours\\black.bmp")
		
		bmpBtnTex1 = openBitMap (plugcfgdir + "\\colours\\black.bmp")
		bmpBtnTex2 = openBitMap (plugcfgdir + "\\colours\\blue.bmp")
		bmpBtnTex3 = openBitMap (plugcfgdir + "\\colours\\green.bmp")
		bmpBtnTex4 = openBitMap (plugcfgdir + "\\colours\\turquoise.bmp")
		bmpBtnTex5 = openBitMap (plugcfgdir + "\\colours\\red.bmp")
		bmpBtnTex6 = openBitMap (plugcfgdir + "\\colours\\purple.bmp")
		bmpBtnTex7 = openBitMap (plugcfgdir + "\\colours\\yellow.bmp")
		bmpBtnTex8 = openBitMap (plugcfgdir + "\\colours\\white.bmp")
		
		TexButtons[1].images = #(bmpBtnTex1, undefined, 1,1,1,1,1 )
		TexButtons[2].images = #(bmpBtnTex2, undefined, 1,1,1,1,1 )	
		TexButtons[3].images = #(bmpBtnTex3, undefined, 1,1,1,1,1 )	
		TexButtons[4].images = #(bmpBtnTex4, undefined, 1,1,1,1,1 )	
		TexButtons[5].images = #(bmpBtnTex5, undefined, 1,1,1,1,1 )	
		TexButtons[6].images = #(bmpBtnTex6, undefined, 1,1,1,1,1 )	
		TexButtons[7].images = #(bmpBtnTex7, undefined, 1,1,1,1,1 )	
		TexButtons[8].images = #(bmpBtnTex8, undefined, 1,1,1,1,1 )
	)
	
	fn ResetLabels = (
		-- Reset all the label names
		for thisLabel in TexLabels do (
			thisLabel.text = "Artifact"
			thisLabel.enabled = false
		)
	)
	
	fn ResetButtons = (
		-- Reset all the buttons
		ResetBmps()
		for i=1 to TexButtons.count do (
			if TexturePaths[i] == undefined then (
				TexButtons[i].enabled = false
				TexButtons[i].tooltip = ( "Texture " + ( i as string ) )
			)
			else (
				-- Allow handling for textures which may not be on this machine
				if RsFileExist TexturePaths[i] then (
					bmp = openBitMap TexturePaths[i]
					TexBmps[i] = ( scaleBitmap bmpDefault bmp )
					TexButtons[i].images = #( TexBmps[i], undefined, 1,1,1,1,1 )
				)
				TexButtons[i].enabled = true
				TexButtons[i].tooltip = TextureList[i]
			)
		)
	)
	
	fn MakeLabelSafe index = (
		TexLabels[index].text = "Safe"
		TexLabels[index].enabled = true
	)
	
	fn Refresh = (
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one object"
			return false

		)
		else (
			TerrainObj = sel[1]
			PopulateTextureList TerrainMaterials[cboMaterials.selection]
			ResetButtons()
		)
		return true
	)
	
	--------------------------------------------------------------
	-- Colour relationships
	--------------------------------------------------------------
	fn ShowRelation index = (
		ResetLabels()
		MakeLabelSafe index
		
		if index == 1 then (
			MakeLabelSafe 2
			MakeLabelSafe 3
			MakeLabelSafe 5
		)
		
		if index == 2 then (
			MakeLabelSafe 1
			MakeLabelSafe 4
			MakeLabelSafe 6
		)
		
		if index == 3 then (
			MakeLabelSafe 1
			MakeLabelSafe 4
			MakeLabelSafe 7
		)
		
		if index == 4 then (
			MakeLabelSafe 2
			MakeLabelSafe 3
			MakeLabelSafe 8
		)
		
		if index == 5 then (
			MakeLabelSafe 1
			MakeLabelSafe 6
			MakeLabelSafe 7
		)
		
		if index == 6 then (
			MakeLabelSafe 2
			MakeLabelSafe 5
			MakeLabelSafe 8
		)
		
		if index == 7 then (
			MakeLabelSafe 3
			MakeLabelSafe 5
			MakeLabelSafe 8
		)
		
		if index == 8 then (
			MakeLabelSafe 4
			MakeLabelSafe 6
			MakeLabelSafe 7
		)
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- Texture buttons pressed
	--------------------------------------------------------------
	on btnTexture1 pressed do (
		ShowRelation 1
		if chkVertPaint.checked == true then (
			SetVertPaintColour 1
		)
		else PaintSelection 1
	)
			
	on btnTexture2 pressed do (
		ShowRelation 2
		if chkVertPaint.checked == true then (
			SetVertPaintColour 2
		)
		else PaintSelection 2
	)
			
	on btnTexture3 pressed do (
		ShowRelation 3
		if chkVertPaint.checked == true then (
			SetVertPaintColour 3
		)
		else PaintSelection 3
	)
			
	on btnTexture4 pressed do (
		ShowRelation 4
		if chkVertPaint.checked == true then (
			SetVertPaintColour 4
		)
		else PaintSelection 4
	)
			
	on btnTexture5 pressed do (
		ShowRelation 5
		if chkVertPaint.checked == true then (
			SetVertPaintColour 5
		)
		else PaintSelection 5
	)
			
	on btnTexture6 pressed do (
		ShowRelation 6
		if chkVertPaint.checked == true then (
			SetVertPaintColour 6
		)
		else PaintSelection 6
	)
			
	on btnTexture7 pressed do (
		ShowRelation 7
		if chkVertPaint.checked == true then (
			SetVertPaintColour 7
		)
		else PaintSelection 7
	)
			
	on btnTexture8 pressed do (
		ShowRelation 8
		if chkVertPaint.checked == true then (
			SetVertPaintColour 8
		)
		else PaintSelection 8
	)
	
	--------------------------------------------------------------
	-- Show colour relationships on right click
	--------------------------------------------------------------
	on btnTexture1 rightclick do (
		ShowRelation 1
	)
			
	on btnTexture2 rightclick do (
		ShowRelation 2
	)
			
	on btnTexture3 rightclick do (
		ShowRelation 3
	)
			
	on btnTexture4 rightclick do (
		ShowRelation 4
	)
			
	on btnTexture5 rightclick do (
		ShowRelation 5
	)
			
	on btnTexture6 rightclick do (
		ShowRelation 6
	)
			
	on btnTexture7 rightclick do (
		ShowRelation 7
	)
			
	on btnTexture8 rightclick do (
		ShowRelation 8
	)
	
	--------------------------------------------------------------
	-- UI control events
	--------------------------------------------------------------				
	on btnRefresh pressed do (
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one object"

		)
		else (
			
			TerrainObj = sel[1]
			PopulateMaterialList()
			PopulateTextureList TerrainMaterials[1]
			ResetButtons()
		)
	)
			
	on btnMore pressed do (
		-- Open additional utilities in a separate window
		filein "rockstar/helpers/terrain_painter_ui.ms"
	
	)
			
	on chkVertPaint changed checkState do (	
		-- If vertex painting has been switched on
		if checkState == true then (
			chkVertPaint.text = "Stop Vertex Paint"
			vPainttool = VertexPaintTool()
			vPainttool.brushOpacity = spnFillStrength.value
			
			-- Get the object
			sel = selection as array
			if sel.count != 1 then (
				messagebox "Please select one object"
			)
			else (
				TerrainObj = sel[1]
				
				-- Set up the modifier if we haven't already done so
				PopulateMaterialList()
				if PopulateTextureList TerrainMaterials[1] == false then (
					messagebox "Object is not setup with a DX terrain shader."
				)
				else (
					if ValidateTerrainObj() == true then SetupModifier()
				)
			)
		)
		-- If vertex painting has been switched off
		else (
			chkVertPaint.text = "Start Vertex Paint"
		)
	)
	
	on cboMaterials selected idx do (
		-- Get the list of textures for this material
		PopulateTextureList TerrainMaterials[cboMaterials.selection]
		
		-- Change the button images to match the textures
		ResetButtons()
	)
	
	on spnFillStrength changed value do (
		-- If we are vertex painting, we need to reset the brush opacity
		if chkVertPaint.checked == true then (
			vPainttool.brushOpacity = value
		)
	)
	
	--------------------------------------------------------------
	-- Rollout events
	--------------------------------------------------------------
	on RsTerrainPalette open do (	
		-- Create groups of controls to allow us to manipulate them easily
		TexButtons = #( btnTexture1, btnTexture2, btnTexture3, btnTexture4, btnTexture5, btnTexture6, btnTexture7, btnTexture8 )
		TexLabels = #( lblTexture1, lblTexture2, lblTexture3, lblTexture4, lblTexture5, lblTexture6, lblTexture7, lblTexture8 )
		TexBmps = #( bmpBtnTex1, bmpBtnTex2, bmpBtnTex3, bmpBtnTex4, bmpBtnTex5, bmpBtnTex6, bmpBtnTex7, bmpBtnTex8 )
		
		-- Attempt to set the selection
		sel = selection as array
		if sel.count == 1 then TerrainObj = sel[1]
		
		-- Set the button images
		ResetBmps()	
	)
	
)

-- rollout and floater generation
try CloseRolloutFloater RsTerrainPainterRoll catch()
RsTerrainPainterRoll = newRolloutFloater "Terrain Painter" 680 220 100 100
addrollout RsTerrainPalette RsTerrainPainterRoll

