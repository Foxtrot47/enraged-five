-- List Texmaps
-- Greg Smith
-- Rockstar North
-- 18/3/2005

-- print out all the texmaps used by the selected objects

include "rockstar/util/material.ms"

texmaplist = #()
maxsizelist = #()
isbumplist = #()

for obj in selection do (

	RsGetTexMapsFromObj obj texmaplist maxsizelist isbumplist
)

sort texmaplist

print "**************************"
print "Texmaps used by selected objects:"

for texmap in texmaplist do (
	print texmap
)