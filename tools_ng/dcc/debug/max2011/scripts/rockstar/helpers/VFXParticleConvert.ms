--
-- File:: rockstar/helpers/VFXParticleConvert.ms
-- Description:: VFX Conversion
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 15 May 2007
--
-----------------------------------------------------------------------------

-- VFX Conversion Wizard Rollout Definition
rollout RsVFXConversionWizRoll "VFX Particle Conversion Wizard"
(

	-------------------------------------------------------------------------------
	-- Local data
	-------------------------------------------------------------------------------
	local 			conversionParticleFile = "X:\\tools_release\\gta_bin\\rmptfx\\ENTITY_CONVERSION.txt"
	
	local				conversionParticleMappingOld = #()
	local				conversionParticleMappingNew = #()

	-------------------------------------------------------------------------------
	-- Interface Controls
	-------------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Particle_Conversion_Wizard" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button 			btnConvert "Convert" width:100
	progressbar barProgress

	-------------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------------
	
	--
	-- Load particle conversion mapping data from conversionFiles
	-- Adds mappings to conversionParticleMapping arrays
	--
	fn loadParticleVFXMapping = (
	
		print ""
		print ""
		print "--------------------------------------------------------------------------------"
		print "-- Particle Conversion Table"
		print "--------------------------------------------------------------------------------"
		print ""	
	
		-- For each of our conversion files we construct our conversionMapping
		-- data, mapping old object classes to new object classes.
		try
		(

			fs = openFile conversionParticleFile mode:"rt"

			while ( not ( eof fs ) ) do
			(

				sLine = readLine fs

				-- Skip comments
				if ( ( sLine[1] == "#" ) or ( 0 == sLine.count ) ) then
					continue

				sLine = trimLeft sLine
				sLine = trimRight sLine

				ssLine = stringStream sLine

				sOld = readToken ssLine
				sNew = readToken ssLine

				-- Ignore conversions that won't do anything :)
				if ( sOld == sNew ) then
					continue

				append conversionParticleMappingOld sOld

				-- Conversions with new set to "-" (to be removed)
				if ( sNew == "-" ) then
				(
					append conversionParticleMappingNew undefined
					format "Remove: %\n" sOld
				)
				else
				(
					append conversionParticleMappingNew sNew
					format "Convert: % to %\n" sOld sNew
				)

			)

			close fs

		)
		catch 
		(
			MessageBox "Exception processing particle conversion file.\n\nContact tools programmers."
		)
	
	)
	
	--
	-- Convert particle helper objects
	--
	fn convertParticleObjects = (
	
		print ""
		print ""
		print "--------------------------------------------------------------------------------"
		print "-- Particle Conversion Details"
		print "--------------------------------------------------------------------------------"
		print ""

		if ( conversionParticleMappingOld.count != conversionParticleMappingNew.count ) then
		(
			MessageBox "Error: conversion particle mapping data inconsistent.\n\nContact tools programmers."
			return false
		)

		nameIndex = GetAttrIndex "RAGE Particle" "Name"
		if ( undefined == nameIndex ) then
		(
			MessageBox "Error: RAGE Particle's \"Name\" attribute undefined index.\n\nContact tools programmers."
			return false
		)

		-- Convert all objects
		numObject = 0
		for obj in $objects do
		(

			numObject += 1
			barProgress.value = ( 100.0 * numObject ) / $objects.count

			if ( RAGE_Particle != classof( obj ) ) then
				continue

			-- Fetch current name attribute
			sCurrentName = GetAttr obj nameIndex		

			conversionIndex = ( findItem conversionParticleMappingOld sCurrentName )
			if ( 0 != conversionIndex ) then
			(
				-- Two paths:
				-- 1. Conversion has removed the effect tag
				-- 2. Valid conversion exists
			
				if ( undefined == conversionParticleMappingNew[conversionIndex] ) then
				(
					-- 1. Conversion has removed the effect tag
				
					format "Converting: object % using now removed particle tag, setting to null\n" obj.name
					format "Renaming: object % to %\n" obj.name "particle_null"
				
					-- Set name attribute
					SetAttr obj nameIndex ""
					-- Set object name
					obj.name = "particle_null"
				
				)
				else
				(
					-- 2. Valid conversion exists
					
					local conversionName = conversionParticleMappingNew[conversionIndex]
					local newName = stringStream ""
					format "particle_%" conversionName to:newName
					
					format "Converting: object % from % to %\n" obj.name sCurrentName conversionName
					format "Renaming: object % to %\n" obj.name (newName as string)
				
					-- Set name attribute
					SetAttr obj nameIndex conversionName
					-- Set object name
					obj.name = ( newName as string )
					
				)			

			)	-- if valid conversion index
			else
			(

				format "Converting: object % no conversion available\n" obj.name

			)

		)	
	
	)
	
	-------------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------------

	on btnConvert pressed do
	(
		loadParticleVFXMapping()

		convertParticleObjects()

		-- redraw viewports to ensure our RAGE Particle helper objects
		-- are refreshed to reflect any changes we have made
		redrawViews()

		-- Reset progress
		barProgress.value = 0

		setSaveRequired true

	) -- btnConvert pressed handler

) -- End of rollout definition

-- End of script
