--
-- File:: rockstar/helpers/bugstar.ms
-- Description:: Bugstar
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com>
-- Date:: 11/5/2004
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
GtaNumBugsA = 0
GtaNumBugsB = 0
GtaNumBugsC = 0
GtaNumBugsD = 0
GtaNumBugsWish = 0
GtaNumBugsTODO = 0
GtaNumBugsFixed = 0

GtaUserList = #()
GtaUserIDList = #()
GtaUserPrintList = #()

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn ResetBugStarList = (

	GtaNumBugsA = 0
	GtaNumBugsB = 0
	GtaNumBugsC = 0
	GtaNumBugsD = 0
	GtaNumBugsWish = 0
	GtaNumBugsTODO = 0
	GtaNumBugsFixed = 0

	for obj in rootnode.children do (
	
		if classof obj == Sphere and isdeleted obj == false then (
		
			delete obj
		)
	)
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout GtaBuglistRoll "Bugstar Viewer"
(
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Bugstar" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	dropdownlist listUsers "User"
	button buttonLoad "Get From Database" width:110 offset:[-60,0]
	button buttonReset "Reset" width:60 offset:[30,-26]
	checkbox checkShowA "A" checked:true offset:[0,-0]
	checkbox checkShowB "B" checked:true offset:[35,-20]
	checkbox checkShowC "C" checked:true offset:[70,-20]
	checkbox checkShowD "D" checked:true offset:[105,-20]
	checkbox checkShowWISH "WISH" checked:true offset:[140,-20]
	checkbox checkShowTODO "TODO" checked:true offset:[195,-20]
	edittext currentItem "Current:" enabled:false
	edittext currentOwner "Owner:" enabled:false
	edittext currentClass "Class:" enabled:false
	checkbox checkFixed "Fix" checked:false 
	button buttonSendFixes "Send Fixes to Database" width:140 offset:[20,-20]
	button buttonPrevious "Previous" width:70 offset:[-80,0]
	button buttonNext "Next" width:70 offset:[0,-26]
	button buttonSavePos "Save Pos" width:70 offset:[80,-26]
	checkbox checkHideFixed "Freeze Fixed" checked:false
	edittext bugsTypeA "A class bugs" enabled:false
	edittext bugsTypeB "B class bugs" enabled:false
	edittext bugsTypeC "C class bugs" enabled:false
	edittext bugsTypeD "D class bugs" enabled:false
	edittext bugsTypeWISH "WISH class bugs" enabled:false
	edittext bugsTypeTODO "TODO class bugs" enabled:false
	edittext bugsTypeFIXED "Fixed bugs" enabled:false
	
	fn updateWhatToShow = (
	
		for obj in rootnode.children do (
		
			if classof obj == Sphere then (
			
				objClass = getuserprop obj "class"
				found = false
				
				if objClass == "A" and checkShowA.checked == false then (
					hide obj
					found = true
				)

				if objClass == "B" and checkShowB.checked == false then (
					hide obj
					found = true
				)
				
				if objClass == "C" and checkShowC.checked == false then (
					hide obj
					found = true
				)
				
				if objClass == "D" and checkShowD.checked == false then (
					hide obj
					found = true
				)
								
				if objClass == "WISH" and checkShowWISH.checked == false then (
					hide obj
					found = true
				)

				if objClass == "TODO" and checkShowTODO.checked == false then (
					hide obj
					found = true
				)
				
				if found == false then (
					unhide obj
				)		
			)
		)
	)

	fn updateStats = (
	
		bugsTypeA.text = (GtaNumBugsA as string)
		bugsTypeB.text = (GtaNumBugsB as string)
		bugsTypeC.text = (GtaNumBugsC as string)
		bugsTypeD.text = (GtaNumBugsD as string)
		bugsTypeWISH.text = (GtaNumBugsWish as string)
		bugsTypeTODO.text = (GtaNumBugsTODO as string)
		bugsTypeFIXED.text = (GtaNumBugsFixed as string)
	)

	fn computeStats = (
	
		GtaNumBugsA = 0
		GtaNumBugsB = 0
		GtaNumBugsC = 0
		GtaNumBugsD = 0
		GtaNumBugsWish = 0
		GtaNumBugsTODO = 0
		GtaNumBugsFixed = 0
	
		for obj in rootnode.children do (
			if classof obj == Sphere then (
			
				objClass = getuserprop obj "class"
				objFixed = getuserprop obj "fixed"
							
				if objFixed == "yes" then (
					GtaNumBugsFixed = GtaNumBugsFixed + 1
				) else (
					if objClass == "A" then (
						GtaNumBugsA = GtaNumBugsA + 1
					)
	
					if objClass == "B" then (
						GtaNumBugsB = GtaNumBugsB + 1
					)
					
					if objClass == "C" then (
						GtaNumBugsC = GtaNumBugsC + 1
					)
					
					if objClass == "D" then (
						GtaNumBugsD = GtaNumBugsD + 1
					)
								
					if objClass == "WISH" then (
						GtaNumBugsWish = GtaNumBugsWish + 1
					)
					
					if objClass == "TODO" then (
						GtaNumBugsTODO = GtaNumBugsTODO + 1
					)
				)
			)
		)
		
		updateStats()
	)
	
	fn loadBugList = (
	
		ResetBugStarList()
		
		if listUsers.selection == 0 then return 0
		
		idFound = finditem GtaUserList listUsers.items[listUsers.selection]
		
		bugs = bugstar_getbugs (GtaUserIDList[idFound])
		
		for bugItem in bugs do (
		
			if bugItem[4] == 0 then (
		
				newSphere = Sphere()
				newSphere.pos = [bugItem[5] as float,bugItem[6] as float,bugItem[7] as float]
				newSphere.radius = 2.0
				newSphere.segs = 4
						
				if bugItem[3] == 0 then (
					bugItem[3] = "A"
					GtaNumBugsA = GtaNumBugsA + 1
					newSphere.wirecolor = (color 255 0 0)
				) else if bugItem[3] == 1 then (
					bugItem[3] = "B"
					GtaNumBugsB = GtaNumBugsB + 1
					newSphere.wirecolor = (color 0 255 0)
				) else if bugItem[3] == 2 then (
					bugItem[3] = "C"
					GtaNumBugsC = GtaNumBugsC + 1
					newSphere.wirecolor = (color 0 0 255)
				) else if bugItem[3] == 3 then (
					bugItem[3] = "D"
					GtaNumBugsD = GtaNumBugsD + 1
					newSphere.wirecolor = (color 255 255 0)
				) else if bugItem[3] == 4 then (
					bugItem[3] = "WISH"
					GtaNumBugsWish = GtaNumBugsWish + 1
					newSphere.wirecolor = (color 255 0 255)
				) else (
					bugItem[3] = "TODO"
					GtaNumBugsTODO = GtaNumBugsTODO + 1
					newSphere.wirecolor = (color 255 0 255)
				)
				
				setuserprop newSphere "summary" bugItem[2]
				setuserprop newSphere "id" (bugItem[1] as string)
				setuserprop newSphere "owner" listUsers.items[listUsers.selection]
				setuserprop newSphere "fixed" "no"
				setuserprop newSphere "class" bugItem[3]
				
				newSphere.name = ("BUG" + (bugItem[8] as string) + "_" + bugItem[2])
			)
		)
		
		updateStats()
		updateWhatToShow()
	)
	
	fn updateSelection = (
	
		currentPickSelection = #()
	
		for obj in selection do (
		
			if classof obj == Sphere then (
			
				append currentPickSelection obj
			)
		)	
		
		if currentPickSelection.count > 1 then (
		
			currentItem.text = "Multiple"
			currentOwner.text = ""
			currentClass.text = ""
		) else (
		
			if currentPickSelection.count == 1 then (
				currentItem.text = currentPickSelection[1].name
				
				ownerText = getuserprop currentPickSelection[1] "owner"
				
				if ownerText == undefined then (
					currentOwner.text = ""
				) else (
					currentOwner.text = ownerText
				)
				
				classText = getuserprop currentPickSelection[1] "class"
				
				if classText == undefined then (
					currentClass.text = ""
				) else (
					currentClass.text = classText
				)
				
				isFixed = getuserprop selection[1] "fixed"
				
				if isFixed == "yes" then (
					checkFixed.checked = true
				) else (
					checkFixed.checked = false
				)
				
			) else (
				currentItem.text = ""
				currentOwner.text = ""
				currentClass.text = ""
			)
		)
	)
		
	on buttonSavePos pressed do (
	
		if selection.count < 1 then (
			return 0
		)
	
		if classof selection[1] != Sphere then (
			return 0
		)
		
		filename = "x:\\gta\\build\\playercoords.txt"
		bugstarfile = openfile filename mode:"w+"
	
		if bugstarfile == undefined then (
			return 0
		)
	
		format "% % %\n" selection[1].pos[1] selection[1].pos[2] selection[1].pos[3] to:bugstarfile
		
		close bugstarfile
	)
	
	on buttonPrevious pressed do (
		currentSel = undefined
		previous = undefined
	
		-- get the currently selected object
	
		for obj in selection do (
			if classof obj == Sphere and isdeleted obj == false then (
				currentSel = obj
				exit
			)
		)	
	
		clearSelection()	
		
		-- look for the next object
		
		for obj in rootnode.children do (
			if classof obj == Sphere and isdeleted obj == false and obj.ishidden == false then (

				if obj == currentSel then (
					if previous == undefined then (
						for obj in rootnode.children do (
							if classof obj == Sphere and isdeleted obj == false and obj.ishidden == false then (			
								previous = obj
							)
						)
						
						if previous != undefined then (
							select previous
						)
	
						return 0
						
					) else (
						select previous
						return 0
					)
				)
				
				previous = obj
			)
		)
	)
	
	on buttonNext pressed do (
		currentSel = undefined
		previous = undefined
	
		-- get the currently selected object
	
		for obj in selection do (
			if classof obj == Sphere and isdeleted obj == false then (
				currentSel = obj
				exit
			)
		)	
	
		clearSelection()	
		
		-- look for the next object
	
		found = false
		first = undefined		

		for obj in rootnode.children do (
		
			if classof obj == Sphere and isdeleted obj == false and obj.ishidden == false then (			

				if first == undefined then first = obj

				if found == true then (
				
					select obj
					return 0
				)

				if obj == currentSel then (
				
					found = true
				)
			)
		)
		
		if first != undefined then select first
	)

	
	on buttonUpdate pressed do (
	
		return 0
	
		itemFixed = #()
	
		for obj in rootnode.children do (
			if classof obj == Sphere then (
				if getuserprop obj "fixed" == "yes" then (
					append itemFixed (getuserprop obj "id" as integer)
				)
			)
		)
		
		if itemFixed.count > 0 then (
		
		--	setbugfixed userName.text itemFixed
		
			loadBugList()
		)
	)
	
	on checkShowA changed state do (
		updateWhatToShow()
	)
	
	on checkShowB changed state do (
		updateWhatToShow()
	)
	
	on checkShowC changed state do (
		updateWhatToShow()
	)
	
	on checkShowD changed state do (
		updateWhatToShow()
	)
		
	on checkShowWISH changed state do (
		updateWhatToShow()
	)

	on checkShowTODO changed state do (
		updateWhatToShow()
	)
	
	on checkHideFixed changed state do (
	
		for obj in rootnode.children do (
		
			if classof obj == Sphere then (
			
				if state == true and (getuserprop obj "fixed" == "yes") then (
				
					freeze obj
				) else (
				
					unfreeze obj
				)
			)
		)
	)
	
	on buttonSendFixes pressed do (
	
		retval = querybox "are you sure? you can only undo this through bugstar"
	
		if retval then (
	
			for obj in rootnode.children do (
			
				if classof obj == Sphere and isdeleted obj == false then (		
				
					isFixed = getuserprop obj "fixed"	
					
					if isFixed == "yes" then (
					
						bugid = getuserprop obj "id"		
						username = getuserprop obj "owner"		
						
						idFound = finditem GtaUserList username
	
						if idFound != 0 then (
	
							userid = GtaUserIDList[idFound]
	
							bugstar_resolve userid bugid "resolved from max"
					
							case getuserprop obj "class" of (
								"A": GtaNumBugsA = GtaNumBugsA - 1
								"B": GtaNumBugsB = GtaNumBugsB - 1
								"C": GtaNumBugsC = GtaNumBugsC - 1
								"D": GtaNumBugsD = GtaNumBugsD - 1
								"WISH": GtaNumBugsWish = GtaNumBugsWish - 1
								"TODO": GtaNumBugsTODO = GtaNumBugsTODO - 1
							)
							
							GtaNumBugsFixed = GtaNumBugsFixed + 1
						
							delete obj
						)					
					)
				)
			)
		) else (
			
			messagebox "this can only be undone through bugstar"
		)
	)
	
	on checkFixed changed state do (
	
		if selection.count == 1 and classof selection[1] == Sphere then (		
		
			obj = selection[1]
		
			if state == true then (
			
				bugid = getuserprop obj "id"		
				username = getuserprop obj "owner"		
				
				idFound = finditem GtaUserList username

				if idFound != 0 then (
			
					setuserprop obj "fixed" "yes"
					obj.xray = true
					
					if checkHideFixed.checked == true and getuserprop obj "fixed" == "yes" then (
						freeze obj
					) else (
						unfreeze obj
					)
				)
			) else (
			
				setuserprop obj "fixed" "no"
			)
		)
		
		updateStats()
	)
	
	on buttonReset pressed do (
		ResetBugStarList()
	)
	
	on buttonLoad pressed do (
		loadBugList()
	)
	
	on GtaBuglistRoll open do (
	
		userlist = bugstar_getusers()

		if userlist == false then (
		
			messagebox "bugstar server is down. have you enabled max integration in bugstar?"
			return 0
		)

		GtaUserList = #()
		GtaUserIDList = #()
		GtaUserPrintList = #()

		for item in userlist do (
		
			append GtaUserPrintList item[2]
			append GtaUserList item[2]
			append GtaUserIDList item[1]
		)
		
		sort GtaUserPrintList
		
		listUsers.items = GtaUserPrintList
	
		computeStats()
		updateSelection()
		callbacks.addscript #selectionSetChanged "GtaBuglistUtil.rollouts[1].updateSelection()" id:#pickBugselect
	)
	
	on GtaBuglistRoll close do (
	
		callbacks.removescripts id:#pickBugselect	)
)

rollout GtaBugReassignRoll "Bugstar Reassign"
(
	dropdownlist listUsers "Reassign to:"
	button butReassign "Reassign Selected"
	
	on butReassign pressed do (
	
		if listUsers.selected == 0 then (
			messagebox "please select a user to reassign to"
			return 0
		)
	
		retval = queryBox "Are you sure you want to reassign?"
		
		if retval == false then (
			return 0
		)
		
		bugsID = #()
		bugsDelete = #()
		
		for bugItem in selection do (
			if classof bugItem == Sphere then (
				idVal = getuserprop bugItem "id"
				idVal = idVal as integer
				
				if idVal != undefined then (
					append bugsID idVal
					append bugsDelete bugItem
				)
			)
		)
		
		if bugsID.count < 1 then (
			messagebox "please select some bugs to reassign"
			return 0
		)
		
		retval = reassignbugs listUsers.selected bugsID
		
		if retval == true then (
			messagebox ((bugsID.count as string) + " bugs reassigned to " + listUsers.selected)
			delete bugsDelete
		) else (
			messagebox "error in reassign"
		)
	)
	
	on GtaBugReassignRoll open do (
		userNames = getbugusers()
--		userNames = #()
		
		listUsers.items = userNames
	)
)

try CloseRolloutFloater GtaBuglistUtil catch()
GtaBuglistUtil = newRolloutFloater "Bugstar" 300 530 1 136
addRollout GtaBuglistRoll GtaBuglistUtil
