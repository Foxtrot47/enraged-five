--
-- File:: rockstar/helpers/stdmatreplace.ms
-- Description:: Standard Material Replacer
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 3 July 2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "Rockstar\\util\\material.ms"
filein "pipeline\\util\\ragematerial.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout StdMatReplacerRoll "Standard Material to Rage Shader"
(
	-------------------------------------------------------------------------------
	-- Data
	-------------------------------------------------------------------------------
	local mapParentMats = #()
	local mapMats	= #()
	
	-- Blinn StdMaterial Model (see MaxScript docs)
	local idxStdMatDiffuseMap = 2
	local idxStdMatOpacityMap = 7
	
	local idxStdMatSpecMap = 3
	local idxStdMatBumpMap = 9

	-------------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Replace_Shaders#Standard_Material_to_Rage_Shader" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	group "All used materials on selection, fast, auto typed"
	(
		button btnAutoReplace "Replace" width:80 align:#left
	)
	
	group "Controlled type replace, slower"
	(
		dropdownlist 	lstAvailableMats "Selection Standard Materials:" items:#() width:150 across:2
		button 			btnRefresh "Refresh" width:80 align:#right
		dropdownlist 	lstReplaceShader "Replace with:" items:#()
		button 			btnReplace "Replace" width:100 across:2
		button			btnReplaceAll "Replace All" width:100
	)
	-------------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------------
	fn GetAvailMats objs = (
	
		-- store the list of object names locally since we don't use it here
		objList = #()

		RsGetStdMatListForObjects objs &objList &mapMats &mapParentMats
		local mapMatNames = #()
		for i = 1 to mapMats.count do
			append mapMatNames mapMats[i].name
		lstAvailableMats.items = mapMatNames	
	)

	-- Return index of subMat in parentMat.materialList
	fn GetParentIndex parentMat subMat = (
	
		for i = 1 to parentMat.materialList.count do
		(
			if ( parentMat.materialList[i] == subMat ) then
				return i
		)
		
		return undefined
	)
	
	-- Return index of tex variable in Rage Shader
	fn GetTexureIndex mat varname = (

		if ( Rage_Shader != classof mat ) then
			return undefined

		local numVars = ( RstGetVariableCount mat )
		for i = 1 to numVars do
		(
			print (RstGetVariableName mat i)
			if ( varname == ( RstGetVariableName mat i ) ) then
				return i
		)

		return undefined
	)

	-------------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------------
	
	on btnAutoReplace pressed do
	(
		if selection.count<1 then messagebox "select at least one object"
		RsConvertStdMaterialsOnSelected (selection as array)
	)
	
	on StdMatReplacerRoll open do
	(
		-- Refresh available Standard Materials
		lstAvailableMats.items = GetAvailMats selection
		
		-- Get list of shaders for listbox
		local shaders = RsGetShaderList()
		-- Post process shaders list
		for i = 1 to shaders.count do
		(
			if ( undefined == findString shaders[i] ".sps" ) then
				shaders[i] += ".sps"
		)
		
		lstReplaceShader.items = shaders
	)

	-- Refresh scene shaders list
	on btnRefresh pressed do
	(	

		lstAvailableMats.items = GetAvailMats selection
	)

	-- Replace shaders in selected objects
	on btnReplace pressed do
	(

		parentMat = mapParentMats[lstAvailableMats.selection]
		stdMat = mapMats[lstAvailableMats.selection]
		stdMatDiffuse = stdMat.Maps[idxStdMatDiffuseMap]
		stdMatOpacity = stdMat.Maps[idxStdMatOpacityMap]
		stdMatSpec = stdMat.Maps[idxStdMatSpecMap]
		stdMatBump = stdMat.Maps[idxStdMatBumpMap]

		rsShader = Rage_Shader()
		RstSetShaderName rsShader (lstReplaceShader.selected)

		-- Determine if user wants to set diffuse map
		if ( undefined != stdMatDiffuse ) then
		(
			msg = stringStream ""
			format "The Standard Material % has a Diffuse Map (%: %) set.  Copy this to the new Rage Shader?" stdMat.name stdMatDiffuse.filename stdMatDiffuse to:msg
			if ( QueryBox (msg as string) ) then
			(
				local idx = GetTexureIndex rsShader "Diffuse Texture"
				RstSetVariable rsShader idx stdMatDiffuse.filename
			)
		)
		
		-- Determine if user wants to set spec map
		if ( undefined != stdMatSpec ) then
		(
			msg = stringStream ""
			format "The Standard Material % has a Spec Map (%: %) set.  Copy this to the new Rage Shader?" stdMat.name stdMatSpec.filename stdMatSpec to:msg
			if ( QueryBox (msg as string) ) then
			(
				local idx = GetTexureIndex rsShader "Specular Texture"
				if idx != undefined then RstSetVariable rsShader idx stdMatSpec.filename
			)
		)
		
		-- Determine if user wants to set normal map
		if ( undefined != stdMatBump ) then
		(
			msg = stringStream ""
			local filename = undefined
			if (classof stdMatBump)==Bitmaptexture then filename = stdMatBump.filename
			else if (classof stdMatBump)==Normal_Bump then 
			(
				if stdMatBump.normal_map != undefined then 
					filename = stdMatBump.normal_map.filename
			)
			if undefined!=filename then 
			(
				format "The Standard Material % has a Bump Map (%: %) set.  Copy this to the new Rage Shader?" stdMat.name filename stdMatBump to:msg
				if ( QueryBox (msg as string) ) then
				(
					local idx = GetTexureIndex rsShader "Bump Texture"
					if idx != undefined then RstSetVariable rsShader idx filename
				)
			)
		)

		-- If user has set an alpha Rage Shader, then attempt to copy the 
		-- Standard Materials Opacity Map across
		if ( undefined != stdMatOpacity and RstGetIsAlphaShader( rsShader ) ) then
		(
			msg = stringStream ""
			format "The Standard Material % has an Opacity Map (%: %) set.  Copy this to the new Rage Shader?" stdMat.name stdMatOpacity.filename stdMatOpacity to:msg
			if ( QueryBox ( msg as string ) ) then
			(
				setSubTexmap rsShader 2 stdMatOpacity
			)
		)

		-- Update the materials
		if ( undefined != parentMat ) then
		(

			idxParent = ( GetParentIndex parentMat stdMat )		
			if ( undefined == idxParent ) then
			(
				MessageBox "Error reading parent material.  Contact tools."
				return false
			)

			-- Replace material in parent material
			parentMat.materialList[idxParent] = rsShader
		)
		else
		(
		
			-- We do not have a parent material, so we must manually replace all
			-- objects that reference the old material with the new one.
		
			for obj in objects do
			(
				if ( obj.material != stdMat ) then
					continue
					
				-- Otherwise this object references it so we update the reference
				format "Replacing object % material to %\n" obj.name rsShader.name
				obj.material = rsShader
			)		
		)	
		
	) -- on btnReplace pressed
	
	
	-- Replace all shaders in selected objects
	on btnReplaceAll pressed do
	(
	
		for i = 1 to mapMats.count do
		(
			parentMat = mapParentMats[i]
			stdMat = mapMats[i]
			stdMatDiffuse = stdMat.Maps[idxStdMatDiffuseMap]
			stdMatDiffuse = stdMat.Maps[idxStdMatDiffuseMap]
			stdMatOpacity = stdMat.Maps[idxStdMatOpacityMap]
			stdMatSpec = stdMat.Maps[idxStdMatSpecMap]
			stdMatBump = stdMat.Maps[idxStdMatBumpMap]
			
			idxParent = ( GetParentIndex parentMat stdMat )
			if ( undefined == idxParent ) then
			(
				MessageBox "Error reading parent material.  Contact tools."
				return false
			)

			rsShader = Rage_Shader()
			RstSetShaderName rsShader (lstReplaceShader.selected)

			-- Determine if user wants to set diffuse map
			if ( undefined != stdMatDiffuse ) then
			(
				local idx = GetTexureIndex rsShader "Diffuse Texture"
				RstSetVariable rsShader idx stdMatDiffuse.filename
			)
			
			-- Determine if user wants to set spec map
			if ( undefined != stdMatSpec ) then
			(
				local idx = GetTexureIndex rsShader "Specular Texture"
				if idx != undefined then RstSetVariable rsShader idx stdMatSpec.filename
			)
			
			-- Determine if user wants to set normal map
			if ( undefined != stdMatBump ) then
			(
				local filename = undefined
				if (classof stdMatBump)==Bitmaptexture then filename = stdMatBump.filename
				else if (classof stdMatBump)==Normal_Bump then 
				(
					if stdMatBump.normal_map != undefined then 
						filename = stdMatBump.normal_map.filename
				)
				if undefined!=filename then 
				(
					local idx = GetTexureIndex rsShader "Bump Texture"
					if idx != undefined then RstSetVariable rsShader idx filename
				)
			)

			-- If user has set an alpha Rage Shader, then attempt to copy the 
			-- Standard Materials Opacity Map across
			if ( undefined != stdMatOpacity and RstGetIsAlphaShader( rsShader ) ) then
			(
				setSubTexmap rsShader 2 stdMatOpacity
			)

			parentMat.materialList[idxParent] = rsShader			
		)		
	) -- on btnReplaceAll pressed
)

-- End of script
