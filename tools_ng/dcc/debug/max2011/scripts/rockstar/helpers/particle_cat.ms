--
-- File:: rockstar/helpers/particle_cat.ms
-- Description:: Particle setup utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
--
-----------------------------------------------------------------------------
-- HISTORY
--
-- DHM - 30/04/2007
-- GtaParticle plugin changes to support emitter domain visualisation
--  o Added VFX Database rollout (to rebuild VFX in-memory database)
--  o Added addition Set handler code to mark object dirty
--
-- LPXO: 20/7/2009
-- Switch to using RAGE Particle
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsCollCats = #()
RsCollItems = #()
RsIdxPartName = getattrindex "RAGE Particle" "Name"
RsIdxTriggerName = getattrindex "RAGE Particle" "Trigger"

struct RsCollParticle (name, type)

RsCollParts = #()
RsCollCurrPartNames = #()

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
rollout RsPartSetupRoll "Particle Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Particle_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	checkbox chkAmbient "Ambient" checked:true
	checkbox chkBreak "Break" checked:true
	checkbox chkShot "Shot" checked:true
	checkbox chkCollision "Collision" checked:true
	checkbox chkDestruction "Destruction" checked:true
	checkbox chkAnim "Anim" checked:true
	dropdownlist cboParticle "Name:"
	button btnSet "Set" width:100
	button btnClone "Clone" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
	
		filename = ( RsConfigGetCommonDir() + "/data/effects/entityFx.dat" )

		intFile = openfile filename mode:"rb"

		newType = #none
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			
			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"
				
				print intTokens[1]
								
				if intTokens[1] == "ENTITYFX_AMBIENT_START" then (

					newType = #ambient
				) else if intTokens[1] == "ENTITYFX_COLLISION_START" then (

					newType = #collision
				) else if intTokens[1] == "ENTITYFX_SHOT_START" then (

					newType = #shot
				) else if intTokens[1] == "FRAGMENTFX_BREAK_START" then (

					newType = #break
				) else if intTokens[1] == "FRAGMENTFX_DESTROY_START" then (

					newType = #destruction
				) else if intTokens[1] == "ENTITYFX_ANIM_START" then (

					newType = #anim
				) 
				else if intTokens[1] == "ENTITYFX_SHOT_END" or  intTokens[1] == "ENTITYFX_AMBIENT_END" or  intTokens[1] == "ENTITYFX_COLLISION_END" or intTokens[1] == "FRAGMENTFX_BREAK_END" or intTokens[1] == "FRAGMENTFX_DESTROY_END" or intTokens[1] == "FRAGMENTFX_ANIM_END" then (

					newType = #none
				) else  if intTokens.count > 1 then (

					newItem = RsCollParticle intTokens[1] newType
					append RsCollParts newItem
				)
			)
		)
		
		close intFile
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetType = (
		
		for item in selection do (
		
			if getattrclass item == "RAGE Particle" then (
			
				for i = 1 to RsCollParts.count do (
				
					if RsCollParts[i].name == cboParticle.items[cboParticle.selection] then (
					
						setattr item RsIdxPartName RsCollParts[i].name
						
						item.name = "particle_" + RsCollParts[i].name
						
						setval = 0
						
						if RsCollParts[i].type == #ambient then (
						
							setval = 0
						) else if RsCollParts[i].type == #collision then (
						
							setval = 1
						) else if RsCollParts[i].type == #shot then (
						
							setval = 2
						) else if RsCollParts[i].type == #break then (
						
							setval = 3
						) else if RsCollParts[i].type == #destruction then (
						
							setval = 4
						) else if RsCollParts[i].type == #anim then (
						
							setval = 5
						)						
						
						setattr item RsIdxTriggerName setval
						
						-- Mark item as dirty so representation is rebuild
						rageparticle_fxrefresh item
					)
				)
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateTypes = (
	
		print "updating"
	
		RsCollCurrPartNames = #()
		
		for item in RsCollParts do (
				
			show = false

			if chkAmbient.checked and item.type == #ambient then show = true
			if chkBreak.checked and item.type == #break then show = true
			if chkShot.checked and item.type == #shot then show = true
			if chkCollision.checked and item.type == #collision then show = true
			if chkDestruction.checked and item.type == #destruction then show = true
			if chkAnim.checked and item.type == #anim then show = true

			if show then (

				append RsCollCurrPartNames item.name
			)
		)
	
		sort RsCollCurrPartNames
	
		cboParticle.items = RsCollCurrPartNames
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkAmbient changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkBreak changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkCollision changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkShot changed arg do (
	
		UpdateTypes()
	)	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on chkDestruction changed arg do (
	
		UpdateTypes()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSet pressed do (
	
		SetType()
	)
	
	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (
	
		if selection.count > 0 then (
		
			if ( "RAGE Particle" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()
				
				nodeCopy = copy nodeToCopy
			
				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning RAGE Particle node.  Contact tools."
					return false
				)

				clearSelection()
				selectMore nodeCopy
				PasteAttrs()
			)
		)
	)
	
	--------------------------------------------------------------
	-- Rollup Startup
	--------------------------------------------------------------
	on RsPartSetupRoll open do (
	
		LoadEntries()
		UpdateTypes()
	)
	
)

-- VFX Database Rollout
rollout RsPartDBRoll "VFX Database"
(

	-- Interface
	button btnRebuild "Rebuild VFX DB" width:100
	label lblInfo2 "Use this rebuild button if you get\nlatest from Alienbrain to refresh\nthe loaded VFX. This saves\nyou having to restart 3DS Max." align:#left height:60

	-- Event handlers
	on btnRebuild pressed do (
	
		rageparticle_rebuild()
	
	)
)
