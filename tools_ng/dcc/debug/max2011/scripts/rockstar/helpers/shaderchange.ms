--
-- File:: rockstar/helpers/shaderchange.ms
-- Description:: Shader Variable Parameter Changer
--
-----------------------------------------------------------------------------

rollout ShaderChangeRoll "Shader Attribute Changer"
(
	-----------------------------------------------------------------------------------------
	-- Interface
	-----------------------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Change_Shaders#Shader_Attribute_Changer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)	
	
	dropdownlist lstShader "Shader:" items:#( "All" )
	dropdownlist lstName "Name:" items:#("Specular Falloff", "Specular Intensity", "Bumpiness")
	spinner spnValue "Value to Change:"range:[0,5000,0.01]
	spinner spnTolerance "Tolerance:" range:[0,5000,0.01]
	spinner spnNewValue "New Value:" range:[0,5000,100]
	button btnSet "Change" across:2
	button btnSetAbove "Change If Above"
	
	-----------------------------------------------------------------------------------------
	-- Methods
	-----------------------------------------------------------------------------------------
	
	-- Populate combo box from our material list
	fn populateShaderList = (

		local materials = #()
		
		for obj in $selection do
		(
			if ( ( Editable_Mesh != classof( obj ) ) and
					 ( Editable_Poly != classof( obj ) ) ) then
				continue

			if ( Multimaterial != classof( obj.material ) ) then
				continue
		
			if ( 0 == ( getNumSubMtls obj.material ) ) then
				continue

			for submat in obj.material.materialList do
			(
			
				if ( Rage_Shader != classof( submat ) ) then
					continue
			
				if ( 0 != ( findItem materials submat ) ) then
					continue
			
				append materials submat
			
			)
		)

		lstShader.items = #()
		items = #( "All" )

		for mat in materials do
		(

			if ( Rage_Shader != classof( mat ) ) then
				continue

			shaderName = RstGetShaderName mat
			if ( 0 != ( findItem items shaderName ) ) then
				continue

			append items shaderName
			
		)
		lstShader.items = items
	)

	fn modifyShaderValue material valname checkvalue tolerance newvalue = (
		
		if classof material == Multimaterial then (
		
			for submat in material.materiallist do (
			
				modifyShaderValue submat valname checkvalue tolerance newvalue
			)
		) 
		else if classof material == Rage_Shader then (
		
			-- DHM -- 2007-05-22 -- Check the shader name has been selected or "All"
			local shaderName = RstGetShaderName material
			local selectedShaderName = lstShader.selected
			
			if ( ( 1 != lstShader.selection ) and ( shaderName != selectedShaderName ) ) then
			(
				return false -- Ignore submaterial
			)
			-- End
		
			varcount = RstGetVariableCount material
		
			for i = 1 to varcount do  (
			
				varName = RstGetVariableName material i
				varType = RstGetVariableType material i
				
				if varName == valname and varType == "float" then (
									
					currvalue = RstGetVariable material i
										
					if (currvalue >= (checkvalue - tolerance)) and (currvalue <= (checkvalue + tolerance)) then (
										
						RstSetVariable material i newvalue
					)
				)
			)
		)
	)

	fn modifyShaderValueIfAbove material valname checkvalue newvalue = (
		
		if classof material == Multimaterial then (
		
			for submat in material.materiallist do (
			
				modifyShaderValueIfAbove submat valname checkvalue newvalue
			)
		) 
		else if classof material == Rage_Shader then (

			-- DHM -- 2007-05-22 -- Check the shader name has been selected or "All"
			local shaderName = RstGetShaderName material
			local selectedShaderName = lstShader.selected
			
			if ( ( 1 != lstShader.selection ) and ( shaderName != selectedShaderName) ) then
			(
				return false -- Ignore submaterial
			)
			-- End
		
			varcount = RstGetVariableCount material
			
			for i = 1 to varcount do  (
			
				varName = RstGetVariableName material i
				varType = RstGetVariableType material i

				if varName == valname and varType == "float" then (
									
					currvalue = RstGetVariable material i
										
					if (currvalue >= checkvalue) then (
						
						RstSetVariable material i newvalue
					)
				)
			)
		)
	)

	fn setSelected = (

		for obj in selection do (
		
			if classof obj == Editable_mesh or classof obj == Editable_poly do (
			
				modifyShaderValue obj.material lstName.selected spnValue.value spnTolerance.value spnNewValue.value	
			)
		)
	)
	
	fn setSelectedIfAbove =(
	
		for obj in selection do (
		
			if classof obj == Editable_mesh or classof obj == Editable_poly do (
			
				modifyShaderValueIfAbove obj.material lstName.selected spnValue.value spnNewValue.value	
			)
		)		
	)
	
	-----------------------------------------------------------------------------------------
	-- Events
	-----------------------------------------------------------------------------------------
	
	--
	-- 
	--
	on btnSet pressed do (
	
		setSelected()	
	)
	
	--
	--
	--
	on btnSetAbove pressed do (
	
		setSelectedIfAbove()
	)
	
	--
	-- Rollout open event, initialise dropdown shader list
	--
	on ShaderChangeRoll open do (
	
		populateShaderList()
	
		-- Install handler for selection updates
		callbacks.addScript #selectionSetChanged "ShaderChangeRoll.populateShaderList()" id:#shaderChangeSelection
	)
	
	on ShaderChangeRoll close do (
	
		-- Remove handler for selection updates
		callbacks.removeScripts id:#shaderChangeSelection
	)
)
-- End of script
