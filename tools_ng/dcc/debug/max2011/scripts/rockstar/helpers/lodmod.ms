--
-- File:: rockstar/helpers/lodmod.ms
-- Description:: lod modifier helper utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 1/8/2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsCurrentLod = undefined
RSStoreWire = undefined

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
fn RsAllowLodMod o = (

	(classof o == GtaLodModifier)
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsLodModifierRoll ""
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Lod_Modifer" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnSelectLOD "Select LodMod" pos:[5,20] width:133 height:26
	button btnSelectLODMod "Use Selected LodMod" pos:[142,20] width:133 height:26
	editText edtCurrentLOD "" readonly:true pos:[3,52] width:271 height:18
	
	button btnAddSelected "Add Selected" pos:[5,84] width:133 height:26
	button btnRemoveSelected "Remove Selected" pos:[142,84] width:133 height:26
	button btnToggleVis "Toggle Visibility" pos:[5,113] width:133 height:26
	button btnToggleTrans "Toggle Transparency" pos:[142,113] width:133 height:26
	button btnCreateBoxes "Create Visibility Boxes" pos:[6,143] width:133 height:26
	button btnRemoveBoxes "Remove Visibility Boxes" pos:[143,143] width:133 height:26
	button btnSelectLodModFromGeom "Select LM with Geom" pos:[6,173] width:133 height:26
	button btnSelectLodModCullWater "Select Cull Water LMs" pos:[143,173] width:133 height:26
	button btnAddSelectedToGroup "Add Selected Geo to Selected LodMods" pos:[34,203] width:217 height:26

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnRemoveBoxes pressed do (
	
		for obj in rootnode.children do (
		
			if classof obj == GtaLodModifier then (
			
				for childobj in obj.children do (
				
					if isdeleted childobj == false then (
					
						delete childobj
					)
				)
			)
		)
	)

	on btnCreateBoxes pressed do (
	
		for obj in rootnode.children do (
		
			if classof obj == GtaLodModifier then (
			
				for childobj in obj.children do (
				
					if isdeleted childobj == false then (
					
						delete childobj
					)
				)
			
				newobj = Box()
				newobj.transform = obj.transform
				newobj.parent = obj
				newobj.width = obj.boxgizmo.width
				newobj.length = obj.boxgizmo.length
				newobj.height = obj.boxgizmo.height
			)
		)
	)

	on btnAddSelectedToGroup pressed do (
	
		sellods = #()
		objlist = #()
	
		for obj in selection do (
		
			if classof obj == GtaLodModifier then (
			
				append objlist obj
			)
			
			if getattrclass obj == "Gta Object" then (
			
				append sellods obj
			)
		)	
		
		for obj in objlist do (
		
			for item in sellods do (
		
				if getattrclass item == "Gta Object" then (

					if finditem obj.LodNodeList item == 0 then (

						append obj.LodNodeList item
					)
				)
			)
		)
	)

	on btnAddSelected pressed do (
	
		if RsCurrentLod == undefined then (
		
			messagebox "no lod modifier selected"
			return 0
		)
		
		for item in selection do (
		
			if getattrclass item == "Gta Object" then (
		
				if finditem RsCurrentLod.LodNodeList item == 0 then (
				
					append RsCurrentLod.LodNodeList item
				)
			)
		)
	)
	
	on btnRemoveSelected pressed do (

		if RsCurrentLod == undefined then (
		
			messagebox "no lod modifier selected"
			return 0
		)	
		
		for item in selection do (
		
			if getattrclass item == "Gta Object" then (
		
				idx = finditem RsCurrentLod.LodNodeList item
		
				if idx != 0 then (
				
					deleteitem RsCurrentLod.LodNodeList idx
				)
			)
		)
	)
	
	on btnToggleVis pressed do (
	
		if RsCurrentLod == undefined then (
		
			messagebox "no lod modifier selected"
			return 0
		)	
		
		for obj in rootnode.children do (
		
			if getattrclass obj == "Gta Object" then (
		
				if finditem RsCurrentLod.LodNodeList obj == 0 then (
				
					if obj.ishidden then (
						
						obj.ishidden = false
					) else (
					
						obj.ishidden = true
					)					
				)
			)
		)
	)
	
	on btnToggleTrans pressed do (
	
		if RsCurrentLod == undefined then (
		
			messagebox "no lod modifier selected"
			return 0
		)	
		
		for item in RsCurrentLod.LodNodeList do (
	
			if item.xray then (
				
				item.xray = false
			) else (
			
				item.xray = true
			)		
		)
	)
	
	on btnSelectLODMod pressed do (
	
		if RsCurrentLod != undefined then (
		
			RsCurrentLod.wirecolor = RSStoreWire
		)	
		
		RsCurrentLod = selection[1]
		
		if RsCurrentLod == undefined then (
		
			edtCurrentLOD.text = ""
		) else (
		
			RSStoreWire = RsCurrentLod.wirecolor
			RsCurrentLod.wirecolor = (color 255 199 199)
		
			edtCurrentLOD.text = RsCurrentLod.name
		)		
	)
	
	on btnSelectLOD pressed do (
	
		if RsCurrentLod != undefined then (
		
			RsCurrentLod.wirecolor = RSStoreWire
		)
	
		RsCurrentLod = pickobject count:1 filter:RsAllowLodMod
		
		if RsCurrentLod == undefined then (
		
			edtCurrentLOD.text = ""
		) else (
		
			RSStoreWire = RsCurrentLod.wirecolor
			RsCurrentLod.wirecolor = (color 255 199 199)
		
			edtCurrentLOD.text = RsCurrentLod.name
		)
	)
	
	on btnSelectLodModCullWater pressed do (

		idxCullWater = getattrindex "Gta LodModifier" "Cull Water"
	
		tosel = #()
	
		for obj in rootnode.children do (
		
			if classof obj == GtaLodModifier then (

				if getattr obj idxCullWater then (

					append tosel obj
				)
			)
		)	
		
		clearselection()
		select tosel		
	)
	
	on btnSelectLodModFromGeom pressed do (

		tosel = #()
	
		for obj in rootnode.children do (
		
			if classof obj == GtaLodModifier then (

				for checkobj in selection do (

					if finditem obj.LodNodeList checkobj != 0 then (
					
						append tosel obj
					)					
				)
			)
		)	
		
		clearselection()
		select tosel
	)
	
	on RsLodModifierRoll close do (
	
		if RsCurrentLod != undefined then (
		
			RsCurrentLod.wirecolor = RSStoreWire
		)
	)	
)

try CloseRolloutFloater RsLodModifierUtil catch()
RsLodModifierUtil = newRolloutFloater "Lod Modifier Utilites" 300 265 1 136
addRollout RsLodModifierRoll RsLodModifierUtil