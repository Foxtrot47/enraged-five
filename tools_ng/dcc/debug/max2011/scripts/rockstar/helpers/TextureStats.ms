--
-- File:: rockstar/helpers/TextureStats.ms
-- Description:: 3dsMax Rage Shader Texture Map Statistics
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 22 June 2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/material.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsRageTextureMapStatsRoll "Texture Map Statistics"
(
	-----------------------------------------------------------------------------------------
	-- Script-scope Variables
	-----------------------------------------------------------------------------------------
	local			appTitle	= "Texture Map Statistics"
	local			headers     = #( "ID", "Texture Map", "Object Usage (count)", "Objects",
									 "TXD Usage (count)", "TXDs", "TXD Usage (ratio)" )
	local			widths		= #( 800, 3200, 2600, 3200, 2600, 3200, 2600 )
	
	-----------------------------------------------------------------------------------------
	-- UI Widgets and Layout
	-----------------------------------------------------------------------------------------
	button			btnUpdate 		"Update" align:#left width:100 across:3
	checkbox		chkSelection	"Selection Only" align:#center
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Texture_Stats" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	dotNetControl lstView "System.Windows.Forms.ListView" height:(500-65) align:#center 
	progressbar		barProgress		across:2
	button			btnClose		"Close" align:#right width:100

	-----------------------------------------------------------------------------------------
	-- Functions
	-----------------------------------------------------------------------------------------

	-- None

	-----------------------------------------------------------------------------------------
	-- UI Event Handlers
	-----------------------------------------------------------------------------------------
	
	on btnUpdate pressed do
	(

		lstView.Items.Clear()
		
		local textureList = #()
		local textureCounts = #()
		local objectsList = #()
		local txdList = #()
		local i = 1
		local objCount = 0
		local objs
		local idxTXD = GetAttrIndex "Gta Object" "TXD"
		local uniqueTXDs = #()
		
		if ( chkSelection.checked ) then
			objs = $selection
		else
			objs = $objects
			
		objCount = objs.count
		
		for o in objs do
		(
			
			i += 1
			barProgress.value = 100.0 * i / objCount
			
			if ( "Gta Object" != GetAttrClass o ) then
				continue
		
			local objTextureList = #()
			local objBitmapList = #()
			RsGetTexMapsFromObjWithMaps o objTextureList objBitmapList
		
			-- Process texture list
			for t in objTextureList do
			(
				id = findItem textureList t
				
				if ( 0 == id ) then
				(
					append textureList t
					append textureCounts 0
					append objectsList #()
					append txdList #()
					id = textureList.count
				)
				textureCounts[id] += 1
				if ( 0 == findItem objectsList[id] o ) then
					append objectsList[id] o
				
				local txdname = ( GetAttr o idxTXD )
				if ( 0 == findItem txdList[id] txdname ) then				
					append txdList[id] txdname
			)
		)
		
		-- Find number of unique TXDs for percentage count
		for txdl in txdList do
		(
			for t in txdl do
			(
				if ( 0 == findItem uniqueTXDs t ) then
				(
					append uniqueTXDs t
				)
			)
		)
		
		-- Populate listview
		theRange = #() --array to collect the list items
		for i = 1 to textureList.count do
		(
			local objs = stringStream ""
			local txds = stringStream ""
			
			for j = 1 to objectsList[i].count do
			(
				if ( j == objectsList[i].count ) then
					format "%" objectsList[i][j].name to:objs
				else				
					format "%, " objectsList[i][j].name to:objs
			)
			for j = 1 to txdList[i].count do
			(
				if ( j == txdList[i].count ) then
					format "%" txdList[i][j] to:txds
				else
					format "%, " txdList[i][j] to:txds
			)
		
			-- ID, Map, Object Count, Object List, TXD Count, TXD List
			/* ActiveX Version:
			newItem = lstView.ListItems.Add Text:( i as string )
			newItem.listSubItems.Add Text:textureList[i]
			newItem.listSubItems.Add Text:( textureCounts[i] as string )
			newItem.listSubItems.Add Text:( objs as string )
			newItem.listSubItems.Add Text:( txdList[i].count as string )
			newItem.listSubItems.Add Text:( txds as string )
			newItem.listSubItems.Add Text:( ( txdList[i].count as string )  + " / " + ( uniqueTXDs.count as string ) )
			*/
			 --First we create a ListViewItem object with the object's name:
			newItem = dotNetObject "System.Windows.Forms.ListViewItem" ( i as string )
			--Then we add all the sub-items with the desired string values:
			sub_li = newItem.SubItems.add (textureList[i] as string)
			sub_li = newItem.SubItems.add (textureCounts[i] as string)
			sub_li = newItem.SubItems.add (objs as string)
			sub_li = newItem.SubItems.add (txdList[i].count as string)
			sub_li = newItem.SubItems.add (txds as string)
			sub_li = newItem.SubItems.add ((txdList[i].count as string) + " / " + ( uniqueTXDs.count as string ) )
			append theRange newItem --we add the list item to the array
			
		) -- End of Populate ListView		
		lstView.Items.AddRange theRange --when done, we populate the ListView
	)
	
	on btnClose pressed do
	(
		DestroyDialog RsRageTextureMapStatsRoll
	)
	
	on lstView ColumnClick ColumnHeader do
	(

	)

	/* ActiveX Version:
	on RsRageTextureMapStatsRoll open do
	(

		lstView.FullRowSelect = true
		lstView.View = #lvwReport
	
		for h in headers do
			lstView.ColumnHeaders.Add Text:h
		for i = 1 to widths.count do
			lstView.ColumnHeaders[i].width = widths[i]
		
		lstView.MultiSelect = true
		lstView.Sorted = false	
	)
	*/
	on RsRageTextureMapStatsRoll open do
	(

		lstView.fullRowSelect = true
		lstView.View = (dotNetClass "System.Windows.Forms.View").Details
	
		i = 1;
		for h in headers do
		(
			lstView.Columns.add h (widths[i] * 0.04)
			i = i +1
		)
		
		lstView.MultiSelect = true
		-- lstView.Sorted = false	
	)
	
) -- End of rollout definition

DestroyDialog RsRageTextureMapStatsRoll
CreateDialog RsRageTextureMapStatsRoll modal:false width:800 height:500
				

-- End of script
