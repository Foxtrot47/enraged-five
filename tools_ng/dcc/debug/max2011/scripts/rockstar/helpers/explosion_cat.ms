--
-- File:: rockstar/helpers/explosion_cat.ms
-- Description:: Particle setup utility
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsCollCats = #()
RsCollItems = #()
RsIdxPartName = getattrindex "Gta Explosion" "Explosion Tag"
RsIdxTriggerName = getattrindex "Gta Explosion" "Trigger"

struct RsCollParticle (name)

RsCollParts = #()
RsCollCurrPartNames = #()

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsExpSetupRoll "Explosion Setup"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/VFX_Toolkit#Explosion_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	dropdownlist cboParticle "Name:"
	dropdownlist cboTrigger "Trigger:" items:#("Shot","Break","Destroy")
	button btnSet "Set" width:100
	button btnClone "Clone" width:100
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn LoadEntries = (
		filename = RsConfigGetCommonDir() + "data/effects/explosionFx.dat"
		if ( 0 == ( getFileSize filename ) ) then
		(
			local ss = stringStream ""
			format "Error: cannot find explosion FX data file (%)." filename to:ss
			MessageBox (ss as string) title:"Error"
			return false
		)
		
		intFile = openfile filename mode:"rb"

		newType = #none
		
		while eof intFile == false do (
		
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
			
			if intLine.count > 0 and intLine[1] != "#" then (

				intTokens = filterstring intLine " \t"
				
				if intTokens.count > 1 then (

					newItem = RsCollParticle intTokens[1]
					append RsCollParts newItem
				)
			)
		)
		
		close intFile
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetType = (
		
		for item in selection do (
		
			if getattrclass item == "Gta Explosion" then (
											
				setattr item RsIdxPartName (cboParticle.selection - 1)
				setattr item RsIdxTriggerName (cboTrigger.selection - 1)
			)
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateSelection = (
	
		if selection.count > 0 then (
				
			if getattrclass selection[1] == "Gta Explosion" then (

				val = (getattr selection[1] RsIdxPartName) + 1
				cboParticle.selection = val
				
				val = (getattr selection[1] RsIdxTriggerName) + 1
				cboTrigger.selection = val
			)
		)		
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn UpdateTypes = (
	
		RsCollCurrPartNames = #()
		
		for item in RsCollParts do (
				
			append RsCollCurrPartNames item.name
		)
	
		cboParticle.items = RsCollCurrPartNames
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSet pressed do (
	
		SetType()
	)
	
	--------------------------------------------------------------
	-- Clone
	--------------------------------------------------------------	
	on btnClone pressed do (

		if selection.count > 0 then (

			if ( "Gta Explosion" == GetAttrClass selection[1] ) then
			(
				nodeToCopy = selection[1]
				CopyAttrs()

				nodeCopy = copy nodeToCopy

				if ( undefined == nodeCopy ) then
				(
					MessageBox "Error cloning Gta Explosion node.  Contact tools."
					return false
				)

				clearSelection()
				selectMore nodeCopy
				PasteAttrs()
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on RsExpSetupRoll open do (
	
		LoadEntries()
		UpdateTypes()
		callbacks.addscript #selectionSetChanged "VFXToolkit.rollouts[1].UpdateSelection()" id:#rsexpobjselect
		UpdateSelection()
	)
	
	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsExpSetupRoll close do (
	
		callbacks.removescripts id:#rsexpobjselect
		RsSettingWrite "rspartsetup" "rollup" (not RsExpSetupRoll.open)
	)
)