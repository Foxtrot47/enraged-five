--
-- File:: rockstar/helpers/sceneimage.ms
-- Description:: Gta Scene Image
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 12/2/2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/util/file.ms"
filein "pipeline/export/maps/mapsetup.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsTotalIplFilename = "c:\\cache.ipl"

RsObjectNameList = #()
RsObjectPosList = #()
RsObjectMapList = #()

struct blockObject (posx, posy, rotz, width, height)
struct blockObject3 (posx1, posy1, posx2, posy2, posx3, posy3, posx4, posy4)
struct blockInfo (name, map, blockobjs, objects, txds, polys, collpolys, modelsize, txdsize, totalsize, lodsize, xrefsize, propsize, interiorsize, volume, blockver)

RsBlockList = #()
RsBlockNameList = #()
RsBlockMaximums = undefined
RsBlockMaximumsDensity = undefined
RsBlockMinimums = undefined
RsBlockMinimumsDensity = undefined

global RsSceneImageRoll

RsForceMinimum = 5.0

RsShowPos = false
RsXPos = 0.0
RsYPos = 0.0

fn RsDrawTriangle screenColList pos1 pos2 pos3 colVal = (

	local minpos = copy pos1
	local maxpos = copy pos1
	
	if minpos.x > pos2.x then minpos.x = pos2.x
	if minpos.x > pos3.x then minpos.x = pos3.x
	if maxpos.x < pos2.x then maxpos.x = pos2.x
	if maxpos.x < pos3.x then maxpos.x = pos3.x

	if minpos.y > pos2.y then minpos.y = pos2.y
	if minpos.y > pos3.y then minpos.y = pos3.y
	if maxpos.y < pos2.y then maxpos.y = pos2.y
	if maxpos.y < pos3.y then maxpos.y = pos3.y
	
	for x = minpos.x to maxpos.x do (
	
		for y = minpos.y to maxpos.y do (
		
			screenColList[x * 512 + y] = colVal
		)
	)
)

fn RsSphereBoxCollCheck spherePos sphereRadius blockObj = (

	sPos = [spherePos.x,spherePos.y,0.0]

--	print ((spherePos as string) + " " + (sphereRadius as string) + " " + (blockObj as string))

	minx = (-blockObj.height / 2.0)
	maxx = (blockObj.height / 2.0)
	miny = (-blockObj.width / 2.0)
	maxy = (blockObj.width / 2.0)
	
	matRot = rotateZMatrix -blockObj.rotz
	
	sphereRelPos = (sPos - [blockObj.posx,blockObj.posy,0.0]) * matRot
	
	minx = minx - sphereRelPos.x
	miny = miny - sphereRelPos.y
	maxx = maxx - sphereRelPos.x
	maxy = maxy - sphereRelPos.y
	
	Rad2 = sphereRadius * sphereRadius
	
	if (maxx < 0) then (
	
	   	if (maxy < 0) then return ((maxx * maxx + maxy * maxy) < Rad2)
	   	else if (miny > 0) then return ((maxx * maxx + miny * miny) < Rad2)
	   	else return((abs maxx) < sphereRadius)
	) else if (minx > 0) then (
	
   		if (maxy < 0) then return ((minx * minx + maxy * maxy) < Rad2)
	   	else if (miny > 0) then return ((minx * minx + miny * miny) < Rad2)
	   	else return (minx < sphereRadius)
 	) else (
	
   		if (maxy < 0) then return ((abs maxy) < sphereRadius)
	   	else if (miny > 0) then return (miny < sphereRadius)
	   	else return true
	)

	return false
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsSceneImageItemRoll "Items" width:260
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnShowSelected "Show Selected" align:#left
	listbox lstSelected width:250
	checkbox chkSelectedOnly "Display Selected Only"
	label lblInstances "0 instances" align:#left
	listbox lstMaps width:250
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	fn UpdateMaps = (
	
		idxFound = finditem RsObjectNameList lstSelected.selected
		
		if idxFound != 0 then (
		
			lstMaps.items = RsObjectMapList[idxFound]
		) else (
			
			lstMaps.items = #()
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	on chkSelectedOnly changed arg do (
	
		RsSceneImageRoll.UpdateImage()
	)
	
	on lstSelected selected arg do (
	
		if chkSelectedOnly.checked then (
		
			RsSceneImageRoll.UpdateImage()	
		)
		
		UpdateMaps()
	)

	on btnShowSelected pressed do (
	
		if (getFiles RsTotalIplFilename).count == 0 then (
		
			if querybox "no cache file. do you want to build it?" == false then return 0
			
			RsSceneImageRoll.CreateCacheFile()	
		)
	
		objNameList = #()
	
		for obj in selection do (
		
			if obj.parent == undefined then (
			
				objName = RsGetNonFragmentName obj.name
				
				append objNameList objName
			)
		)
	
		lstSelected.items = objNameList
	
		RsSceneImageRoll.FindObjectData objNameList
		RsSceneImageRoll.UpdateImage()
		UpdateMaps()
	)
)

rollout RsSceneImageBlockRoll "Blocks" width:260
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	button btnShowBlocks "Show Blocks" align:#left
	listbox lstMaps width:250
	listbox lstBlocks width:250
	checkbox chkSelectedMapOnly "Display Map Only"
	checkbox chkSelectedBlockOnly "Display Block Only"
	listbox lstColourise width:250 items:#("White","By Polys/m2","By Coll Polys/m2","By Model/m2","By TXD/m2","By Total/m2","By Lod/m2","By Xref/m2","By Prop/m2","By Interior/m2","By Polys","By Coll Polys","By Model","By TXD","By Total","By Lod","By Xref","By Prop","By Interior")
	spinner spnMinPolyDensity "Min:" align:#left
	label lblMaxPolyDensity "Max:" align:#left
	label lblCurrPolyDensity "Current:" align:#left
	
	checkbox chkShowPosition "Show Position"
	spinner spnXPos "XPos" range:[-3000.0,3000.0,0.0] align:#left
	spinner spnyPos "YPos" range:[-3000.0,3000.0,0.0] align:#left
	spinner spnRadius "Radius" range:[0.0,3000.0,0.0]
	label lblTotalMem "Mem Usage Inside Radius: 0"
	button btnCalcMem "Calc Memory Usage"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
		
	on spnMinPolyDensity entered argA argB do (
	
		if argB == false then (
		
			RsSceneImageRoll.UpdateBlockImage()
		)		
	)
	
	on chkSelectedMapOnly changed arg do (

		RsSceneImageRoll.CreateBlockList()	
		RsSceneImageRoll.UpdateBlockImage()
	)
	
	on chkSelectedBlockOnly changed arg do (
	
		RsSceneImageRoll.UpdateBlockImage()
	)

	on lstMaps selected arg do (
	
		if chkSelectedMapOnly.checked then (

			RsSceneImageRoll.CreateBlockList()		
			RsSceneImageRoll.UpdateBlockImage()
		)
	)
	
	on lstBlocks selected arg do (

		blockItem = undefined

		for i = 1 to RsBlockList.count do (
		
			if RsBlockList[i].name == lstBlocks.selected then (
			
				blockItem = RsBlockList[i]
				exit
			)
		)
	
		if blockItem != undefined then (

			case lstColourise.selection of (
			1: lblCurrPolyDensity.text = ""
			2: lblCurrPolyDensity.text = "Curr: " + ((blockItem.polys / blockItem.volume) as string)
			3: lblCurrPolyDensity.text = "Curr: " + ((blockItem.collpolys / blockItem.volume) as string)
			4: lblCurrPolyDensity.text = "Curr: " + ((blockItem.modelsize / blockItem.volume) as string)
			5: lblCurrPolyDensity.text = "Curr: " + ((blockItem.txdsize / blockItem.volume) as string)
			6: lblCurrPolyDensity.text = "Curr: " + ((blockItem.totalsize / blockItem.volume) as string)
			7: lblCurrPolyDensity.text = "Curr: " + ((blockItem.lodsize / blockItem.volume) as string)
			8: lblCurrPolyDensity.text = "Curr: " + ((blockItem.xrefsize / blockItem.volume) as string)
			9: lblCurrPolyDensity.text = "Curr: " + ((blockItem.propsize / blockItem.volume) as string)
			10: lblCurrPolyDensity.text = "Curr: " + ((blockItem.interiorsize / blockItem.volume) as string)
			11: lblCurrPolyDensity.text = "Curr: " + ((blockItem.polys) as string)
			12: lblCurrPolyDensity.text = "Curr: " + ((blockItem.collpolys) as string)
			13: lblCurrPolyDensity.text = "Curr: " + ((blockItem.modelsize) as string)
			14: lblCurrPolyDensity.text = "Curr: " + ((blockItem.txdsize) as string)
			15: lblCurrPolyDensity.text = "Curr: " + ((blockItem.totalsize) as string)
			16: lblCurrPolyDensity.text = "Curr: " + ((blockItem.lodsize) as string)
			17: lblCurrPolyDensity.text = "Curr: " + ((blockItem.xrefsize) as string)
			18: lblCurrPolyDensity.text = "Curr: " + ((blockItem.propsize) as string)
			19: lblCurrPolyDensity.text = "Curr: " + ((blockItem.interiorsize) as string)
		) else (
			
			lblCurrPolyDensity.text = ""
		)
	
		if chkSelectedBlockOnly.checked then (
		
			RsSceneImageRoll.UpdateBlockImage()
		)
	)
	
	on lstColourise selected arg do (
	
		case lstColourise.selection of (
		1: lblMaxPolyDensity.text = ""
		2: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.polys as string)
		3: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.collpolys as string)
		4: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.modelsize as string)
		5: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.txdsize as string)
		6: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.totalsize as string)
		7: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.lodsize as string)
		8: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.xrefsize as string)
		9: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.propsize as string)
		10: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximumsDensity.interiorsize as string)
		11: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.polys as string)
		12: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.collpolys as string)
		13: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.modelsize as string)
		14: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.txdsize as string)
		15: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.totalsize as string)
		16: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.lodsize as string)
		17: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.xrefsize as string)
		18: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.propsize as string)
		19: lblMaxPolyDensity.text = "Max: " + (RsBlockMaximums.interiorsize as string)
		)
		
		--RsSceneImageBlockRoll.lblCurrPolyDensity.text = "Current Poly Density:"			
	
		RsSceneImageRoll.UpdateBlockImage()
	)
	
	on chkShowPosition changed newval do (
	
		RsShowPos = newval
		RsSceneImageRoll.UpdateBlockImage()
	)
	
	on spnXPos entered inSpin inCancel do (
	
		if RsShowPos then (
	
			RsXPos = spnXPos.value
			RsSceneImageRoll.UpdateBlockImage()	
		)
	)
	
	on spnYPos entered inSpin inCancel do (
	
		if RsShowPos then (
	
			RsYPos = spnYPos.value
			RsSceneImageRoll.UpdateBlockImage()	
		)
	)
	
	on btnShowBlocks pressed do (
	
		RsSceneImageRoll.LoadBlockData()
	)
	
	on btnCalcMem pressed do (
	
		totalMemUsage = 0
	
		for item in RsBlockList do (
		
			doesCollide = false

			if item.blockver != 3 then (
			
				for blockSquare in item.blockobjs do (

					if RsSphereBoxCollCheck [spnXPos.value,spnYPos.value] spnRadius.value blockSquare then (

						doesCollide = true
						exit
					)
				)
			)
			
			if doesCollide then (
			
				totalMemUsage = totalMemUsage + item.totalsize
--				totalMemUsage = totalMemUsage + 1
			)
		)
		
		lblTotalMem.text = "Mem Usage Inside Radius: " + (totalMemUsage as string)
	)
)

rollout RsSceneImageRoll "Scene Image" width:512 height:512
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Scene_Image" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	progressbar barLoad width:300 align:#left
	button btnCache "Create Cache" align:#left offset:[429,-20]
	bitmap bmpImage height:512 width:512 align:#left
	
	subRollout subInfo offset:[516,-520] width:300 height:560
		
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	fn TransformWorldPos2ScreenPos inPos = (
	
		PosX = ((inPos[1] + 3000.0) * 512.0) / 6000.0
		PosY = ((6000.0 - (inPos[2] + 3000.0)) * 512.0) / 6000.0
		
		[PosX as integer,PosY as integer]
	)

	fn CreateCacheFile = (

		utilityFolder = RsConfigGetProjRootDir() + RsConfigGetProjectName() + "_bin/" + RsConfigGetProjectName() + "_general/utility/"
		commandLine = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + utilityFolder + "generateTotalIpl.rbs -output " + RsTotalIplFilename
		DOSCommand commandLine
	)

	fn UpdateBlockImage = (

		bmpFilename = (getdir #scripts) + "\\rockstar\\data\\wholemap.bmp"
		bmpSource = openBitMap bmpFilename
	
		done = false

		screenColList = #()
		screenColList.count = 512 * 512
	
		for i = 1 to RsBlockList.count do (
		
			blockItem = RsBlockList[i]
		
			barLoad.value = 100.0 * i / RsBlockList.count
		
			if RsSceneImageBlockRoll.chkSelectedMapOnly.checked and RsSceneImageBlockRoll.lstMaps.selected != blockItem.map then continue
			if RsSceneImageBlockRoll.chkSelectedBlockOnly.checked and RsSceneImageBlockRoll.lstBlocks.selected != blockItem.name then continue

			maxColVal = 255.0
			colVal = 28

			spnValue = RsSceneImageBlockRoll.spnMinPolyDensity.value

			case RsSceneImageBlockRoll.lstColourise.selection of (

			1: colVal = 128.0 
			2: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.polys / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.polys - spnValue))
			3: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.collpolys / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.collpolys - spnValue))
			4: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.modelsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.modelsize - spnValue))
			5: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.txdsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.txdsize - spnValue))
			6: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.totalsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.totalsize - spnValue))
			7: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.lodsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.lodsize - spnValue))
			8: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.xrefsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.xrefsize - spnValue))
			9: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.propsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.propsize - spnValue))
			10: if(blockItem.volume != 0.0) then colVal = maxColVal * (((blockItem.interiorsize / blockItem.volume) - spnValue) / (RsBlockMaximumsDensity.interiorsize - spnValue))
			11: colVal = maxColVal * (((blockItem.polys as float) - spnValue) / (RsBlockMaximums.polys - spnValue))
			12: colVal = maxColVal * (((blockItem.collpolys as float) - spnValue) / (RsBlockMaximums.collpolys - spnValue))
			13: colVal = maxColVal * (((blockItem.modelsize as float) - spnValue) / (RsBlockMaximums.modelsize - spnValue))
			14: colVal = maxColVal * (((blockItem.txdsize as float) - spnValue) / (RsBlockMaximums.txdsize - spnValue))
			15: colVal = maxColVal * (((blockItem.totalsize as float) - spnValue) / (RsBlockMaximums.totalsize - spnValue))
			16: colVal = maxColVal * (((blockItem.lodsize as float) - spnValue) / (RsBlockMaximums.lodsize - spnValue))
			17: colVal = maxColVal * (((blockItem.xrefsize as float) - spnValue) / (RsBlockMaximums.xrefsize - spnValue))
			18: colVal = maxColVal * (((blockItem.propsize as float) - spnValue) / (RsBlockMaximums.propsize - spnValue))
			19: colVal = maxColVal * (((blockItem.interiorsize as float) - spnValue) / (RsBlockMaximums.interiorsize - spnValue))
			)
					
			for blockItemObject in blockItem.blockobjs do (
		
				if blockItem.blockver == 3 then (
				
					pos1 = TransformWorldPos2ScreenPos [blockItemObject.posx1,blockItemObject.posy1]
					pos2 = TransformWorldPos2ScreenPos [blockItemObject.posx2,blockItemObject.posy2]
					pos3 = TransformWorldPos2ScreenPos [blockItemObject.posx3,blockItemObject.posy3]
					pos4 = TransformWorldPos2ScreenPos [blockItemObject.posx4,blockItemObject.posy4]
								
					RsDrawTriangle screenColList pos1 pos2 pos3 colVal
					RsDrawTriangle screenColList pos2 pos3 pos4 colVal
				) else (
		
					matRot = rotateZMatrix -blockItemObject.rotz

					if blockItemObject.rotz == 0.0 then (

						minx = blockItemObject.posx - (blockItemObject.height / 2.0)
						maxx = blockItemObject.posx + (blockItemObject.height / 2.0)
						miny = blockItemObject.posy - (blockItemObject.width / 2.0)
						maxy = blockItemObject.posy + (blockItemObject.width / 2.0)

						screenMin = TransformWorldPos2ScreenPos [minx,miny]
						screenMax = TransformWorldPos2ScreenPos [maxx,maxy]

						for x = screenMin[1] to screenMax[1] do (

							for y = screenMax[2] to screenMin[2] do (					

								newitem = [x,y]

								if screenColList[newitem.x * 512 + newitem.y] == undefined then (

									screenColList[newitem.x * 512 + newitem.y] = colVal
								)
							)
						)							
					) else (

						minx = (-blockItemObject.height / 2.0) as integer
						maxx = (blockItemObject.height / 2.0) as integer
						miny = (-blockItemObject.width / 2.0) as integer
						maxy = (blockItemObject.width / 2.0) as integer

						for x = minx to maxx do (

							for y = miny to maxy do (

								actualPos = [x,y,0.0] * matRot							

								newitem = (TransformWorldPos2ScreenPos [blockItemObject.posx + actualPos.x,blockItemObject.posy + actualPos.y])

								if screenColList[newitem.x * 512 + newitem.y] == undefined then (

									screenColList[newitem.x * 512 + newitem.y] = colVal
								)							
							)
						)
					)
				)
			)
		)
				
		for x = 1 to 512 do (
			
			barLoad.value = 100.0 * x / 512
			
			for y = 1 to 512 do (

				colVal = screenColList[x * 512 + y]

				if colVal != undefined then (
			
					currPixels = getPixels bmpSource [x,y] 1

					if RsSceneImageBlockRoll.lstColourise.selected == "White" then (

						setColor = composite (color 128 128 128 128) currPixels[1]
					) else (

						setColor = composite (color colVal (255 - colVal) 0 128) currPixels[1]
					)

					setPixels bmpSource [x,y] #(setColor)
				)
			)
		)
		
		if RsShowPos then (
		
			newitem = TransformWorldPos2ScreenPos [RsXPos,RsYPos]
			
			setPixels bmpSource [newitem.x,newitem.y] #((color 255 255 255 255))
		)
	
		bmpImage.bitmap = bmpSource
	)
	
	fn CreateBlockList = (
	
		RsBlockNameList = #()
	
		for blockItem in RsBlockList do (
		
			if RsSceneImageBlockRoll.chkSelectedMapOnly.checked and RsSceneImageBlockRoll.lstMaps.selected != blockItem.map then continue
			
			append RsBlockNameList blockItem.name
		)
		
		sort RsBlockNameList
		RsSceneImageBlockRoll.lstBlocks.items = RsBlockNameList
	)
	
	fn LoadBlockData = (
	
		RsBlockList = #()	
		RsBlockMapList = #()
		RsBlockMinimums = (blockInfo "Total" "Total" #() 0 0 0 0 0 0 0 0 0 0 0 0 0)
		RsBlockMaximums = (blockInfo "Total" "Total" #() 0 0 0 0 0 0 0 0 0 0 0 0 0)
		RsBlockMinimumsDensity = (blockInfo "Total" "Total" #() 0 0 0 0 0 0 0 0 0 0 0 0 0)
		RsBlockMaximumsDensity = (blockInfo "Total" "Total" #() 0 0 0 0 0 0 0 0 0 0 0 0 0)
	
		filenameList = getfiles (RsConfigInfos[RsCurrentConfig].genstreamdir + "mapstats/blockmap/*.csv")
		
		firstGo = true
		
		for mapFilename in filenameList do (
		
			mapName = RsRemovePathAndExtension mapFilename
			
			mapFile = openfile mapFilename mode:"r"
			
			if mapFile != undefined then (
			
				mapFileLine = readline mapFile
				
				statVersion = 1
			
				if mapFileLine == "VER2" then (
				
					statVersion = 2
				) else if mapFileLine == "VER3" then (
				
					statVersion = 3
				)
			
				while eof mapFile == false do (

					mapFileLine = readline mapFile

					mapFileLineTokens = filterstring mapFileLine ","

					if mapFileLineTokens.count > 1 then (

						afterIndex = 0
						blockname = mapFileLineTokens[1]
						blockList = #()	

						if statVersion == 1 then (
							
							minx = mapFileLineTokens[2] as number
							miny = mapFileLineTokens[3] as number
							maxx = mapFileLineTokens[4] as number
							maxy = mapFileLineTokens[5] as number
							
							afterIndex = 5
														
							width = maxx - minx
							height = maxy - miny

							posx = minx + (width / 2.0)
							posy = miny + (height / 2.0)

							blockObj = (blockObject posx posy 0.0 height width)
							append blockList blockObj														
							
						) else if statVersion == 2 then (
						
							blockCount = mapFileLineTokens[2] as number
							afterIndex = 2 + (5 * blockCount)
							
							for i = 1 to blockCount do (
							
								posx = mapFileLineTokens[2 + ((i - 1) * 5) + 1] as number
								posy = mapFileLineTokens[2 + ((i - 1) * 5) + 2] as number
								rotz = mapFileLineTokens[2 + ((i - 1) * 5) + 3] as number
								width = mapFileLineTokens[2 + ((i - 1) * 5) + 4] as number
								height = mapFileLineTokens[2 + ((i - 1) * 5) + 5] as number
							
								blockObj = (blockObject posx posy rotz width height)
								append blockList blockObj
							)
						) else (
						
							blockCount = mapFileLineTokens[2] as number
							afterIndex = 2 + (9 * blockCount)
							
							for i = 1 to blockCount do (
							
								numPoints = mapFileLineTokens[2 + ((i - 1) * 9) + 1] as number
								posx1 = mapFileLineTokens[2 + ((i - 1) * 9) + 2] as number
								posy1 = mapFileLineTokens[2 + ((i - 1) * 9) + 3] as number
								posx2 = mapFileLineTokens[2 + ((i - 1) * 9) + 4] as number
								posy2 = mapFileLineTokens[2 + ((i - 1) * 9) + 5] as number
								posx3 = mapFileLineTokens[2 + ((i - 1) * 9) + 6] as number
								posy3 = mapFileLineTokens[2 + ((i - 1) * 9) + 7] as number
								posx4 = mapFileLineTokens[2 + ((i - 1) * 9) + 8] as number
								posy4 = mapFileLineTokens[2 + ((i - 1) * 9) + 9] as number				
							
								blockObj = (blockObject3 posx1 posy1 posx2 posy2 posx3 posy3 posx4 posy4)
								append blockList blockObj
							)							
						)

						objects = mapFileLineTokens[(afterIndex + 1)] as number
						txds = mapFileLineTokens[(afterIndex + 2)] as number
						polys = mapFileLineTokens[(afterIndex + 3)] as number
						collpolys = mapFileLineTokens[(afterIndex + 4)] as number
						modelsize = mapFileLineTokens[(afterIndex + 5)] as number
						txdsize = mapFileLineTokens[(afterIndex + 6)] as number
						totalsize = mapFileLineTokens[(afterIndex + 7)] as number
						lodsize = mapFileLineTokens[(afterIndex + 8)] as number
						xrefsize = mapFileLineTokens[(afterIndex + 9)] as number
						propsize = mapFileLineTokens[(afterIndex + 10)] as number
						interiorsize = mapFileLineTokens[(afterIndex + 11)] as number
						volume = mapFileLineTokens[(afterIndex + 12)] as number							
						
						print volume

						objects_den = 0
						txds_den = 0
						polys_den = 0
						collpolys_den = 0
						modelsize_den = 0
						txdsize_den = 0
						totalsize_den = 0
						lodsize_den = 0
						xrefsize_den = 0
						propsize_den = 0
						interiorsize_den = 0

						if (volume != 0) then (

							objects_den = objects / volume
							txds_den = txds / volume
							polys_den = polys / volume
							collpolys_den = collpolys / volume
							modelsize_den = modelsize / volume
							txdsize_den = txdsize / volume
							totalsize_den = totalsize / volume
							lodsize_den = lodsize / volume
							xrefsize_den = xrefsize / volume
							propsize_den = propsize / volume
							interiorsize_den = interiorsize / volume
						)

						if firstGo then (
						
							firstGo = false

							RsBlockMinimums.objects = RsBlockMaximums.objects = objects
							RsBlockMinimums.txds = RsBlockMaximums.txds = txds
							RsBlockMinimums.polys = RsBlockMaximums.polys = polys
							RsBlockMinimums.collpolys = RsBlockMaximums.collpolys = collpolys
							RsBlockMinimums.modelsize = RsBlockMaximums.modelsize = modelsize
							RsBlockMinimums.txdsize = RsBlockMaximums.txdsize = txdsize
							RsBlockMinimums.totalsize = RsBlockMaximums.totalsize = totalsize
							RsBlockMinimums.lodsize = RsBlockMaximums.lodsize = lodsize
							RsBlockMinimums.xrefsize = RsBlockMaximums.xrefsize = xrefsize
							RsBlockMinimums.propsize = RsBlockMaximums.propsize = propsize
							RsBlockMinimums.interiorsize = RsBlockMaximums.interiorsize = interiorsize

							RsBlockMinimumsDensity.objects = RsBlockMaximumsDensity.objects = objects_den
							RsBlockMinimumsDensity.txds = RsBlockMaximumsDensity.txds = txds_den
							RsBlockMinimumsDensity.polys = RsBlockMaximumsDensity.polys = polys_den
							RsBlockMinimumsDensity.collpolys = RsBlockMaximumsDensity.collpolys = collpolys_den
							RsBlockMinimumsDensity.modelsize = RsBlockMaximumsDensity.modelsize = modelsize_den
							RsBlockMinimumsDensity.txdsize = RsBlockMaximumsDensity.txdsize = txdsize_den
							RsBlockMinimumsDensity.totalsize = RsBlockMaximumsDensity.totalsize = totalsize_den
							RsBlockMinimumsDensity.lodsize = RsBlockMaximumsDensity.lodsize = lodsize_den
							RsBlockMinimumsDensity.xrefsize = RsBlockMaximumsDensity.xrefsize = xrefsize_den
							RsBlockMinimumsDensity.propsize = RsBlockMaximumsDensity.propsize = propsize_den
							RsBlockMinimumsDensity.interiorsize = RsBlockMaximumsDensity.interiorsize = interiorsize_den
						
						) else (

							if objects > RsBlockMaximums.objects then RsBlockMaximums.objects = objects
							if txds > RsBlockMaximums.txds then RsBlockMaximums.txds = txds
							if polys > RsBlockMaximums.polys then RsBlockMaximums.polys = polys
							if collpolys > RsBlockMaximums.collpolys then RsBlockMaximums.collpolys = collpolys
							if modelsize > RsBlockMaximums.modelsize then RsBlockMaximums.modelsize = modelsize
							if txdsize > RsBlockMaximums.txdsize then RsBlockMaximums.txdsize = txdsize
							if totalsize > RsBlockMaximums.totalsize then RsBlockMaximums.totalsize = totalsize
							if lodsize > RsBlockMaximums.lodsize then RsBlockMaximums.lodsize = lodsize
							if xrefsize > RsBlockMaximums.xrefsize then RsBlockMaximums.xrefsize = xrefsize
							if propsize > RsBlockMaximums.propsize then RsBlockMaximums.propsize = propsize
							if interiorsize > RsBlockMaximums.interiorsize then RsBlockMaximums.interiorsize = interiorsize

							if objects_den > RsBlockMaximumsDensity.objects then RsBlockMaximumsDensity.objects = objects_den
							if txds_den > RsBlockMaximumsDensity.txds then RsBlockMaximumsDensity.txds = txds_den
							if polys_den > RsBlockMaximumsDensity.polys then RsBlockMaximumsDensity.polys = polys_den
							if collpolys_den > RsBlockMaximumsDensity.collpolys then RsBlockMaximumsDensity.collpolys = collpolys_den
							if modelsize_den > RsBlockMaximumsDensity.modelsize then RsBlockMaximumsDensity.modelsize = modelsize_den
							if txdsize_den > RsBlockMaximumsDensity.txdsize then RsBlockMaximumsDensity.txdsize = txdsize_den
							if totalsize_den > RsBlockMaximumsDensity.totalsize then RsBlockMaximumsDensity.totalsize = totalsize_den
							if lodsize_den > RsBlockMaximumsDensity.lodsize then RsBlockMaximumsDensity.lodsize = lodsize_den
							if xrefsize_den > RsBlockMaximumsDensity.xrefsize then RsBlockMaximumsDensity.xrefsize = xrefsize_den
							if propsize_den > RsBlockMaximumsDensity.propsize then RsBlockMaximumsDensity.propsize = propsize_den
							if interiorsize_den > RsBlockMaximumsDensity.interiorsize then RsBlockMaximumsDensity.interiorsize = interiorsize_den
							
							if objects < RsBlockMinimums.objects then RsBlockMinimums.objects = objects
							if txds < RsBlockMinimums.txds then RsBlockMinimums.txds = txds
							if polys < RsBlockMinimums.polys then RsBlockMinimums.polys = polys
							if collpolys < RsBlockMinimums.collpolys then RsBlockMinimums.collpolys = collpolys
							if modelsize < RsBlockMinimums.modelsize then RsBlockMinimums.modelsize = modelsize
							if txdsize < RsBlockMinimums.txdsize then RsBlockMinimums.txdsize = txdsize
							if totalsize < RsBlockMinimums.totalsize then RsBlockMinimums.totalsize = totalsize
							if lodsize < RsBlockMinimums.lodsize then RsBlockMinimums.lodsize = lodsize
							if xrefsize < RsBlockMinimums.xrefsize then RsBlockMinimums.xrefsize = xrefsize
							if propsize < RsBlockMinimums.propsize then RsBlockMinimums.propsize = propsize
							if interiorsize > RsBlockMaximums.interiorsize then RsBlockMaximums.interiorsize = interiorsize

							if objects_den < RsBlockMinimumsDensity.objects then RsBlockMinimumsDensity.objects = objects_den
							if txds_den < RsBlockMinimumsDensity.txds then RsBlockMinimumsDensity.txds = txds_den
							if polys_den < RsBlockMinimumsDensity.polys then RsBlockMinimumsDensity.polys = polys_den
							if collpolys_den < RsBlockMinimumsDensity.collpolys then RsBlockMinimumsDensity.collpolys = collpolys_den
							if modelsize_den < RsBlockMinimumsDensity.modelsize then RsBlockMinimumsDensity.modelsize = modelsize_den
							if txdsize_den < RsBlockMinimumsDensity.txdsize then RsBlockMinimumsDensity.txdsize = txdsize_den
							if totalsize_den < RsBlockMinimumsDensity.totalsize then RsBlockMinimumsDensity.totalsize = totalsize_den
							if lodsize_den < RsBlockMinimumsDensity.lodsize then RsBlockMinimumsDensity.lodsize = lodsize_den
							if xrefsize_den < RsBlockMinimumsDensity.xrefsize then RsBlockMinimumsDensity.xrefsize = xrefsize_den
							if propsize_den < RsBlockMinimumsDensity.propsize then RsBlockMinimumsDensity.propsize = propsize_den
							if interiorsize_den < RsBlockMinimumsDensity.interiorsize then RsBlockMinimumsDensity.interiorsize = interiorsize_den
						)

						append RsBlockList (blockInfo blockname mapName blockList objects txds polys collpolys modelsize txdsize totalsize lodsize xrefsize propsize interiorsize volume statVersion)
					)
				)
			
				close mapFile
				
				append RsBlockMapList mapName
			)
		)
		
		sort RsBlockMapList
		
		RsSceneImageBlockRoll.lstMaps.items = RsBlockMapList
		
		CreateBlockList()
		UpdateBlockImage()
	)

	fn UpdateImage = (
	
		bmpFilename = (getdir #scripts) + "\\rockstar\\data\\wholemap.bmp"
		bmpSource = openBitMap bmpFilename

		colourArray = #(color 255 0 0 255,color 0 255 0 255,color 0 0 255 255,color 255 255 0 255,color 255 0 255 255,color 0 255 255 255)

		totalInstances = 0

		for i = 1 to RsObjectPosList.count do (
		
			objectItemList = RsObjectPosList[i]

			if RsSceneImageItemRoll.chkSelectedOnly.checked == false or (finditem RsObjectNameList RsSceneImageItemRoll.lstSelected.selected) == i then (

				totalInstances = totalInstances + objectItemList.count

				for item in objectItemList do (
					
					screenPos = TransformWorldPos2ScreenPos [item.x,item.y]
					setCol = colourArray[((mod (i - 1) 6) + 1)]
				
					setPixels bmpSource screenPos #(setCol)
				)
			)
		)
		
		RsSceneImageItemRoll.lblInstances.text = (totalInstances as string) + " instances"
		bmpImage.bitmap = bmpSource
	)

	fn FindObjectData objectNameList = (
		
		RsTotalIplFile = openfile RsTotalIplFilename mode:"rb"
		RsObjectNameList = #()
		RsObjectPosList = #()
		RsObjectMapList = #()
		
		if RsTotalIplFile != undefined then (
	
			seek RsTotalIplFile #eof
			maxlen = filepos RsTotalIplFile
			current = 0
			seek RsTotalIplFile 0
	
			while eof RsTotalIplFile == false do (
			
				barLoad.value = 100.0 * current / maxlen
			
				RsTotalIplLine = readline RsTotalIplFile
				
				current = current + RsTotalIplLine.count
							
				RsTotalIplTokens = filterstring RsTotalIplLine ","
				
				if RsTotalIplTokens.count > 1 then (
				
					objNameTokens = filterstring RsTotalIplTokens[1] "-"
					objName = ""
					instName = ""
					miloName = ""
				
					if objNameTokens.count > 1 then (
					
						if objNameTokens[1] == "milo" then (
						
							objName = objNameTokens[4]	
							instName = objNameTokens[3]								
							miloName = objNameTokens[2]
						) else (
						
							objName = objNameTokens[1]	
							instName = objNameTokens[2]
						)
					) else (
					
						objName = objNameTokens[1]
						instName = "unknown"
					)
					
					instName = RsRemovePathAndExtension(instName)
				
					if finditem objectNameList objName != 0 then (
							
						itemPos = [RsTotalIplTokens[3] as number,RsTotalIplTokens[4] as number]

						idxFound = finditem RsObjectNameList objName

						if idxFound == 0 then (

							append RsObjectNameList objName
							append RsObjectPosList #(itemPos)
							append RsObjectMapList #(instName)
						) else (

							append RsObjectPosList[idxFound] itemPos
							
							idxInst = finditem RsObjectMapList[idxFound] instName
							
							if idxInst == 0 then (
							
								append RsObjectMapList[idxFound] instName
							)
						)
					)
				)
			)
			
			barLoad.value = 0
		)
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////	

	on btnCache pressed do (
	
		CreateCacheFile()
	)

	on RsSceneImageRoll open do (
		
		AddSubRollout subInfo RsSceneImageItemRoll
		AddSubRollout subInfo RsSceneImageBlockRoll
	
		UpdateImage()
	)
)

CreateDialog RsSceneImageRoll modal:false width:850 height:620