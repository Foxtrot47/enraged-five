-- Rockstar MILO Custom Tools
-- Rockstar North
-- 14/11/2005
-- by Luke Openshaw

-- A helper containing a series of tools for MILO objects

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/helpers/props/proprenderer.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsMiloRoll "MILO Tools"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Props_and_MILOs#MILO_Tools" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnMiloPortal "MILO Portal"  width:110 height:30
	button btnMultiAssign "Multi Assign Portals" width:110 height:30
	button btnMultiAssignNone "Multi Assign None" width:110 height:30
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
		
	--------------------------------------------------------------
	-- Create a MILO portal 2.5m by 1.5m at the point selected
	--------------------------------------------------------------
	fn CreatePortal = (
		
		creationPos = pickPoint message:"Please select a location for portal creation."
		GtaMloPortal pos:creationPos isSelected:on 
		$.wirecolor = (color 0 255 255)
		$.grid.length = 2.5
		$.grid.width = 1.5
		portalRot = eulerangles 90 0 0 
		rotate $ portalRot
		
		return true
	)
	
		
	--------------------------------------------------------------
	-- Assign a link from the selected MILO portals to the MILO
	-- room selected by the user.
	--------------------------------------------------------------
	fn MultiAssign = (
		sel = selection as array
		
		if sel.count == 0 then (
		
			messagebox "Please select at least one MILO Portal"
			return false
		)
		
		obj = pickObject message:"Please select a room MILO"
		print obj
		
		for i = 1 to sel.count do (
			if sel[i].LinkFirst == undefined then (
				
				sel[i].LinkFirst = obj
			)
			else if sel[i].LinkSecond == undefined then (
				
				sel[i].LinkSecond = obj
			)
			else (
				messagebox ("MILO Portal " + sel[i].name + " already had two room links.  The first link has now been replaced.")
				sel[i].LinkFirst = obj
				
			)
		
				
		)
		return true
	)
	
	fn MultiAssignNone = (
		sel = selection as array
		
		if sel.count == 0 then (
		
			messagebox "Please select at least one MILO Portal"
			return false
		)
		
		for i = 1 to sel.count do (
			sel[i].LinkFirst = undefined
			sel[i].LinkSecond = undefined
		)
		return true
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	on btnMiloPortal pressed do (
		if CreatePortal() == false then return false
	)
	
	on btnMultiAssign pressed do (
		if MultiAssign() == false then return false
	)
	
	on btnMultiAssignNone pressed do (
		if MultiAssignNone() == false then return false
	)
	
)

--------------------------------------------------------------
-- beginning of execution
--------------------------------------------------------------
try CloseRolloutFloater RsMilo catch()
RsMilo = newRolloutFloater "Props and MILOs" 260 335 100 206
addRollout RsMiloRoll RsMilo  
addRollout RsRenderPropsRoll RsMilo 