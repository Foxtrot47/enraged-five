--
-- File:: rockstar/helpers/badtex.ms
-- Description:: Helper to find textures that are not in the standard texture path
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 17/10/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/mapobj.ms"
filein "pipeline/util/string.ms"
filein "pipeline/util/file.ms"
filein "rockstar/util/material.ms"

-----------------------------------------------------------------------------
-- Global
-----------------------------------------------------------------------------
errorObjs = #()

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout BadTexRoll "Bad Texture Finder"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Bad_Texture_Finder" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext edtCorrectRoot "Correct Root:"
	multilistbox lstErrors width:800 height:15
	edittext edtFixRoot "Fix Root:"
	button btnFind "Find"
	button btnFix "Fix Selected"

	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////

	----------------------------------------------------------------------------------------
	-- 
	----------------------------------------------------------------------------------------
	fn RsFixTexMap subTexMap safepath fixpath = (
		
		if classof subTexMap == Bitmaptexture then (	
						
			if subTexMap.filename != undefined and subTexMap.filename != "" then (
			
				currentfile = RsLowercase(RsMakeSafeSlashes(subTexMap.filename))
				
				print ("trying to fix " + currentfile)
				
				if substring currentfile 1 (safepath.count) != safepath then (
	
					if (getfiles currentfile).count > 0 then (

						safefile = fixpath + RsRemovePath currentfile

						if (getfiles safefile).count < 1 then (

							copyfile currentfile safefile
						)
						
						setfile = RsMakeBackSlashes(safefile)

						subTexMap.filename = setfile
						subTexMap.reload()
						
						print ("replacing " + currentfile + " with " + setfile)
					) else (
					
						print (currentfile + " doesnt exist")
					)
				)				
			)
			
		) 
		else if classof subTexMap == CompositeTexturemap then (

			numMaps = getnumsubtexmaps subTexMap

			for j = 1 to numMaps do (

				subsubTexMap = getSubTexmap subTexMap j
				
				RsFixTexMap subsubTexMap safepath fixpath
			)
		)						
	)

	----------------------------------------------------------------------------------------
	-- 
	----------------------------------------------------------------------------------------
	fn RsFixTexMapsOnObj obj mat safepath fixpath = (
			
		print ("checking " + obj.name)
			
		if classof mat == MultiMaterial then (

			matlist = #()

			RsMatGetMapIdsUsedOnObject obj matlist

			for i = 1 to matlist.count do (

				matidx = finditem mat.materialIDList matlist[i]

				if matidx != 0 then (

					RsFixTexMapsOnObj obj mat.materiallist[matidx] safepath fixpath
				)
			)
		) 
		else if classof mat == XRef_Material then (

			RsFixTexMapsOnObj obj (mat.getsrcitem()) safepath fixpath
		)
		else if classof mat == Rage_Shader then (

			numTexMap = getNumSubTexmaps mat

			if RstGetIsAlphaShader mat then (

				numTexMap = numTexMap / 2
			)

			for i = 1 to numTexMap do (

				texmapname = ""

				subTexMap = getSubTexmap mat i

				RsFixTexMap subTexMap safepath fixpath		
			)
		) else (

			numTexMap = getNumSubTexmaps mat

			for i = 1 to numTexMap do (

				subTexMap = getSubTexmap mat i

				RsFixTexMap subTexMap safepath fixpath
			)
		)
	)


	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn GetSelectedObjects selItems = (
	
		for item in lstErrors.selection do (

			if (errorObjs[item] != undefined) then (

				if isdeleted errorObjs[item] == false then (

					if finditem selItems errorObjs[item] == 0 then (

						append selItems errorObjs[item]
					)
				)
			)				
		)		
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn RefreshList = (
	
		safepath = RsLowercase(RsMakeSafeSlashes(edtCorrectRoot.text))
	
		sourceAll = #()
		sourceAll = sourceAll + rootnode.children
		modelsAll = #()

		RsGetMapObjects sourceAll modelsAll
		
		errorLines = #()
		errorObjs = #()
		
		for obj in modelsAll do (
		
			if getattrclass obj == "Gta Object" then (
			
				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()
			
				RsGetTexMapsFromObjNoStrip obj texmaplist maxsizelist isbumplist
				
				for path in texmaplist do (
				
					if substring path 1 (safepath.count) != safepath then (
					
						errorLine = obj.name + " has " + path
						
						append errorLines errorLine
						append errorObjs obj
					)
				)
				
				lstErrors.items = errorLines
			)
		)		
	)

	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnFind pressed do (

		RefreshList()	
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnFix pressed do (
	
		if lstErrors.selection.count > 0 then (
	
			safepath = RsLowercase(RsMakeSafePath(edtCorrectRoot.text))
			fixpath = RsLowercase(RsMakeSafePath(edtFixRoot.text))

			if substring fixpath 1 (safepath.count) != safepath then (

				messagebox (safepath + " is not a subfolder of " + fixpath)
				return 0
			)
			
			selItems = #()
			GetSelectedObjects selItems			
			RsMakeSurePathExists fixpath
			
			for obj in selItems do (
			
				RsFixTexMapsOnObj obj obj.material safepath fixpath
			)

			RefreshList()
			
			messagebox "Remember to add the textures to alienbrain!"
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on lstErrors selectionEnd do (
	
		if lstErrors.selection.count > 0 then (
	
			selItems = #()
			GetSelectedObjects selItems		
			select selItems
			max zoomext sel
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on BadTexRoll open do (
	
		edtCorrectRoot.text = "X:\\GTA\\gta_art\\textures\\"
		
		if maxfilename != "" then (
		
			edtFixRoot.text = "X:\\GTA\\gta_art\\textures\\" + RsRemoveExtension(maxfilename)
		)
	)
)

try CloseRolloutFloater BadTexUtil catch()
BadTexUtil = newRolloutFloater "Texture Util" 850 360 50 126
addRollout BadTexRoll BadTexUtil rolledup:false
