--
-- File:: rockstar/helpers/terrain.ms
-- Description:: Helper for editing objects with the terrain shaders
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 10/8/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
numShaderMaps = undefined
selObj = undefined
uvChannel = undefined
alphaChannel = undefined
selMaterial = undefined
materialNames = undefined
materialIDs = undefined
isUVA = false

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsMultiRoll "Multipass Materials"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Terrain_Setup" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	button btnRefresh "Refresh"
	label stcObject "Object: None selected"
	button btnSetup "Setup Modifiers"
	dropdownlist lstMaterial "Materials:"
	label stcMatID "Material ID: 0"
	label stcName "Shader Name:"
	label stcLayers "Number of Layers: 0"
	dropdownlist lstLayer "Layer:"
	label stcUVChannel "UV Channel: 0"
	imgTag imgTexMap "Texture Map" width:50 height:50 align:#center
	label stcAlphaChannel "Alpha Channel: 0"
	button btnAlphaEdit "Edit Alpha"
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	fn GetNumShaderMaps mat = (
	
		matName = RsRemoveExtension(RstGetShaderName mat)
		numMaps = 0
		isUVA = false

		if matName == "terrain_va_1lyr" then (
		
			numMaps = 1
		) else if matName == "terrain_va_2lyr" then (
		
			numMaps = 2
		) else if matName == "terrain_va_3lyr" then (
		
			numMaps = 3
		) else if matName == "terrain_va_4lyr" then (
		
			numMaps = 4
		)
		
		if matName == "terrain_va_a1lyr" then (
		
			isUVA = true
			numMaps = 1
		) else if matName == "terrain_va_a2lyr" then (
		
			isUVA = true
			numMaps = 2
		) else if matName == "terrain_va_a3lyr" then (
		
			isUVA = true
			numMaps = 3
		) else if matName == "terrain_va_a4lyr" then (
		
			isUVA = true
			numMaps = 4
		)
		
		numMaps
	)

	--------------------------------------------------------------
	--
	--------------------------------------------------------------	
	fn RefreshLayer = (
		
		if numShaderMaps == 0 then (
		
			uvChannel = 0
			alphaChannel = 0
		)
		else if numShaderMaps == 1 then (
			
			if lstLayer.selection == 1 then (
			
				uvChannel = 1
				alphaChannel = 2
			)
		)
		else if numShaderMaps == 2 then (
		
			if lstLayer.selection == 1 then (
			
				uvChannel = 1
				alphaChannel = 3
			) else if lstLayer.selection == 2 then (
			
				uvChannel = 2
				alphaChannel = 4
			)
		)
		else if numShaderMaps == 3 then (
		
			if lstLayer.selection == 1 then (
			
				uvChannel = 1
				alphaChannel = 4
			) else if lstLayer.selection == 2 then (
			
				uvChannel = 2
				alphaChannel = 5
			) else if lstLayer.selection == 3 then (
			
				uvChannel = 3
				alphaChannel = 6
			)		
		)
		else if numShaderMaps == 4 then (
		
			if lstLayer.selection == 1 then (
			
				uvChannel = 1
				alphaChannel = 5
			) else if lstLayer.selection == 2 then (
			
				uvChannel = 2
				alphaChannel = 6
			) else if lstLayer.selection == 3 then (
			
				uvChannel = 3
				alphaChannel = 7
			) else if lstLayer.selection == 4 then (
			
				uvChannel = 4
				alphaChannel = 8
			)		
		)
		
		stcUVChannel.text = "UV Channel: " + (uvChannel as string)
		stcAlphaChannel.text = "Alpha Channel: " + (alphaChannel as string)
		
		nummaps = getnumsubtexmaps selMaterial
		
		if uvChannel > 0 and uvChannel <= nummaps then (
		
			texmap = getsubtexmap selMaterial uvChannel
		
			if texmap == undefined then (
				imgTexMap.bitmap = undefined 
			) else (
				imgTexMap.bitmap = texmap.bitmap
			)
		) else (
		
			imgTexMap.bitmap = undefined
		)
	)
	
	--------------------------------------------------------------
	--
	--------------------------------------------------------------		
	fn RefreshMaterial = (
	
		layers = #()
	
		if lstMaterial.selection != 0 then (
			
			if numShaderMaps > 0 then append layers "1st layer"
			if numShaderMaps > 1 then append layers "2nd layer"
			if numShaderMaps > 2 then append layers "3rd layer"
			if numShaderMaps > 3 then append layers "4th layer"
		)
			
		selMaterial = undefined
		selID = 0
			
		if classof selObj.material == Rage_Shader then (
		
			numShaderMaps = GetNumShaderMaps selObj.material
		
			if numShaderMaps > 0 then (
			
				selMaterial = selObj.material
			)
		) else if classof selObj.material == Multimaterial then (
		
			for i = 1 to selObj.material.materiallist.count do (
			
				submat = selObj.material.materiallist[i]
				subid = selObj.material.materialIDList[i]
				
				if 0==lstMaterial.selection then lstMaterial.selection = 1
				
				print lstMaterial.selection
				
				if classof submat == Rage_Shader and  submat.name == lstMaterial.items[lstMaterial.selection] then (
				
					selMaterial = submat
					selID = subid
				)
			)
		)
	
		lstLayer.items = layers
		stcMatID.text =  "Material ID: " + (selID as string)
		
		if selMaterial == undefined then (
			
			stcName.text = "Shader Name: "
		) else (
		
			stcName.text = "Shader Name: " + (RstGetShaderName selMaterial)
		)
		stcLayers.text = "Number of Layers: " + (numShaderMaps as string)
		
		RefreshLayer()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn RefreshObject = (
	
		if selection.count == 0 then (
		
			stcObject.text = "Object: None selected"		
			return 0
		)

		selObj = selection[1]		
		stcObject.text = "Object: " + selObj.name
		
		materialNames = #()
		materialIDs = #()
		
		if classof selObj.material == Rage_Shader then (
		
			numShaderMaps = GetNumShaderMaps selObj.material
		
			if numShaderMaps > 0 then (
			
				append materialNames selObj.material.name
				append materialIDs 0
			)
		) else if classof selObj.material == Multimaterial then (
		
			for i = 1 to selObj.material.materiallist.count do (
			
				submat = selObj.material.materiallist[i]
				subid = selObj.material.materialIDList[i]
				
				if classof submat == Rage_Shader and undefined!=(findString (RsRemoveExtension(RstGetShaderName submat)) "terrain") then 
				(
					numShaderMaps = GetNumShaderMaps submat
					
					if numShaderMaps > 0 then (
					
						append materialNames submat.name
						append materialIDs subid
					)
				)
				else
				(
					messagebox ("material has an unsupported material in its list:"+submat.name)
					return false
				)
			)
		)
		
		lstMaterial.items = materialNames
		lstMaterial.selection = 1
		
		RefreshMaterial()
	)	

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn SetupModifiers = (
	
		selObj = selection[1]
		mat = undefined
	
		if classof selObj.material == Rage_Shader then (
		
			mat = selObj.material
		) else if classof selObj.material == Multimaterial then (
		
			matID = materialIDs[lstMaterial.selection]
			idxFound = selObj.material.materialIDList[matID]
			
			if idxFound > 0 then (
			
				mat = selObj.material.materiallist[idxFound]
			)
		)
	
		if mat != undefined and classof mat != Rage_Shader then (
		
			messagebox "object doesnt have a rage shader"
			return 0			
		)
		
		numMaps = GetNumShaderMaps mat
		
		if numMaps == 0 then (
		
			messagebox "unsupported shader"
			return 0			
		)
		
--		maxops.collapsenode obj off
		
		for i = 1 to numMaps do (
		
			tmap = getsubtexmap mat i
			
			if classof tmap == Bitmaptexture then (
			
				tmap.coords.mapchannel = i
			)
			
--			newUV = Uvwmap()
--			newUV.mapchannel = numMaps - i + 1
--			addmodifier obj newUV
			
			setChan = (numMaps * 2) - i + 1
			if isUVA then setChan = setChan + 1
			
			newPaint = VertexPaint()
			newPaint.mapchannel = setChan
			addmodifier selObj newPaint
		)
		
		if isUVA then (
		
			newUV = Uvwmap()
			newUV.mapchannel = numMaps + 1
			addmodifier selObj newUV
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on SetupUVAChannel pressed do (
	
		obj = selection[1]

		if classof obj.material != Rage_Shader then (
		
			messagebox "object doesnt have a rage shader"
			return 0			
		)
		
		numMaps = GetNumShaderMaps obj.material
		
		if numMaps == 0 then (
		
			messagebox "unsupported shader"
			return 0			
		)
		

		newUV = Uvwmap()
		newUV.mapchannel = numMaps + 1
		addmodifier obj newUV\
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------
	on btnRefresh pressed do (
	
		RefreshObject()
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnSetup pressed do (
		if selection.count != 1 then (
			messagebox "Select an object first and press Refresh"
		)
		else (
			SetupModifiers()
		)
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnUV_A pressed do (
	
		SetupUVAChannel()
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on lstMaterial selected arg do (
	
		RefreshMaterial()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	on lstLayer selected arg do (
	
		RefreshLayer()
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------			
	on btnAlphaEdit pressed do (
		if selection.count != 1 then (
			messagebox "Select an object first and press Refresh"
		)
		else (
			foundID = -1
			tries = 2
			
			while tries > 0 and foundID == -1 do (
		
				numModifiers = selObj.modifiers.count
		
				for i = 1 to numModifiers do (
				
					if classof selObj.modifiers[i] == VertexPaint and selObj.modifiers[i].mapChannel == alphaChannel then (
									
						foundID = i
						exit
					)
				)
			
				if foundID == -1 then SetupModifiers()
				
				tries = tries - 1
			)
			
			if foundID != -1 then (

				max modify mode
				modPanel.setCurrentObject selObj.modifiers[foundID]
			)
		)
	)
)

try CloseRolloutFloater RsMultiUtil catch()
RsMultiUtil = newRolloutFloater "Terrain Setup" 400 400 50 126
addRollout RsMultiRoll RsMultiUtil