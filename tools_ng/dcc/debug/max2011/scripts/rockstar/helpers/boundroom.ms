--
-- File:: rockstar/helpers/collrenamer.ms
-- Description:: Bound material cloner
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 25/4/2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
RsCurrentRoom = undefined

-----------------------------------------------------------------------------
-- Function
-----------------------------------------------------------------------------
fn miloRoomFilter obj = (

	(classof obj == GtaMloRoom)
)

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout BoundMtlRoomRoll "Room Utils"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Props_and_MILOs#RexBound/Room_Utils" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnRoom "Room to Set" 
	button btnSet "Set Room on Selected Faces"
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////

	on btnRoom pressed do (
	
		gotObj = pickobject message:"pick a milo room" filter:miloRoomFilter
		
		if gotObj != undefined then (
		
			RsCurrentRoom = gotObj
			btnRoom.text = gotObj.name
		)
	)
	
	on btnSet pressed do (
	
		-- check we can do the set
	
		if selection.count != 1 then (
		
			messagebox "please select a single object"
			return 0
		)
		
		selObj = selection[1]
		
		facesellist = getfaceselection selObj
		
		if facesellist.count < 1 then (
		
			messagebox "no faces selected"
			return 0
		)
	
		-- check that we are using a multimaterial
	
		if classof selObj.material != MultiMaterial then (
		
			newmat = MultiMaterial()
					
			setMat = undefined
			
			if classof selObj.material == RexBoundMtl then (
			
				setMat = selObj.material
			) else (
			
				setMat = RexBoundMtl()
			)
					
			newmat.materiallist.count = 1
			newmat.materiallist[1] = selObj.material
			
			numfaces = getnumfaces selobj
			
			for i = 1 to numfaces do (
			
				setfacematid selobj i 1
			)
			
			selObj.material = newmat
		)
			
		-- go through every selected face working out what materials are used
		
		usedMatIDs = #()
		newMatID = #()
		
		for selFace in facesellist do (
		
			matID = getfacematid selobj selFace
			
			if matID != undefined then (

				idxFound = finditem usedMatIDs matID

				if idxFound == 0 then (

					append usedMatIDs matID
					
					matidx = finditem selobj.material.materialIDList matID
					
					newmat = undefined
					
					if classof selobj.material.materialList[matidx] == RexBoundMtl then (
					
						newmat = copy selobj.material.materialList[matidx]
					) else (
						
						newmat = RexBoundMtl()
					)
					
					RexSetRoomNode newmat RsCurrentRoom
					
					foundItem = 0
					
					for i = 1 to selobj.material.materialList.count do (
					
						checkmat = selobj.material.materialList[i]
						
						if RexIsCollisionEqual checkmat newmat then (
						
							foundItem = i
							exit
						)
					)
					
					if foundItem != 0 then (
					
						newID = selobj.material.materialIDList[foundItem]
					) else (
					
						selobj.material.materialList.count = selobj.material.materialList.count + 1
						selobj.material.materialList[selobj.material.materialList.count] = newmat
						
						newID = selobj.material.materialIDList[selobj.material.materialList.count]
					)
					
					append newMatID newID
					setfacematid selobj selFace newID
				) else (
				
					setfacematid selobj selFace newMatID[idxFound]
				)
			)			
		)
	)
)