camfile = openfile "x:\\gta\\build\\temp_debug.txt" mode:"r"

postoken = "SET_CAM_POS"
rottoken = "SET_CAM_ROT"

posval = undefined
rotval = undefined

while eof camfile == false do (

	camfileline = readline camfile
	
	if substring camfileline 1 postoken.count == postoken and posval == undefined then (
	
		camfiletokens = filterstring camfileline ",)"
		posval = [camfiletokens[2] as number,camfiletokens[3] as number,camfiletokens[4] as number]
	)
	
	if substring camfileline 1 rottoken.count == rottoken and rotval == undefined then (
	
		camfiletokens = filterstring camfileline ",)"
		rotval = [camfiletokens[2] as number,camfiletokens[3] as number,camfiletokens[4] as number]
	)
)

close camfile

setmat = (eulerangles -90 0 0) as matrix3

inmatx = inverse ( (eulerangles 44.995380 0.0 0.0) as matrix3 )
inmaty = inverse ( (eulerangles 0.0 0.0 0.0) as matrix3 )
inmatz = inverse ( (eulerangles 0.0 0.0 45.696262) as matrix3 )

if superclassof $ == camera then (

	$.rotation = (inmatz * inmaty * inmatx * setmat) as quat
)
