--
-- File:: rockstar/helpers/TerrainLodder.ms
-- Description:: Terrain Lodder
--
-- Author:: Stuart Macdonald <stuart.macdonald@rockstarnorth.com
--
-----------------------------------------------------------------------------
-- HISTORY
--
--v1.1	(SM)	Altered default vert percentage to 60 from 30
--		    (SM)	Added prefix or postfix option for LOD tag
	
--v1.2	(SM)	Added checkbox for Maintain Base Vertices option of MultiRes modifier
--
-----------------------------------------------------------------------------		
	
-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------
	rollout TerrainLodder_roll "Terrain Lodder"
	(
		--variables
		global LODmat
		
		--ui
		hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/LOD_Toolkit#Terrain_Lodder" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

		button lodit "LOD IT" pos:[19,125] width:243 height:24
		radioButtons rbt_lod "Name Position:" pos:[17,27] width:118 height:30 labels:#("LOD_", "_LOD") default:2 columns:2
		materialbutton picklodmtl "Pick LOD Mat" pos:[149,36] width:100 height:23
		spinner optrange "Vert" range:[1,100,60] type:#integer pos:[27,81] width:104 height:16
		spinner pushrange "Push" range:[-10.0,10.0,-0.2] pos:[27,101] width:104 height:16 range:[0,100,0]
		label lblVert "Percentage of verts left:" pos:[17,64] width:128 height:16
		checkbox chbx_mverts "Keep Base Verts" checked:true tooltip:"keep this on for terrain" pos:[150,85] width:103 height:17
		
		--select inner verts of polymesh
		fn selInnerVerts n =
		(
			--Get exterior vertices
			OpenEdges = polyOp.getOpenEdges n
			SelVerts = polyOp.getVertsUsingEdge n OpenEdges
			n.selectedVerts = -SelVerts			
		
		)
		
		--select outer verts of polymesh
		fn selOuterVerts n =
		(
			--Get exterior vertices
			OpenEdges = polyOp.getOpenEdges n
			SelVerts = polyOp.getVertsUsingEdge n OpenEdges
			n.selectedVerts = SelVerts			
		
		)
		
		--optimize function
		fn optlod =
		(					
			
			modPanel.addModToSelection (Multires())
			$.modifiers[#MultiRes].reqGenerate = true
			$.modifiers[#MultiRes].vertexPercent = optrange.value
			$.modifiers[#MultiRes].baseVertices = chbx_mverts.state
			--$.modifiers[#MultiRes].reqGenerate = true
		)
		
		--push function
		fn pushlod = 
		(
			convertTo $ Editable_Poly
			selInnerVerts $	
			subObjectLevel = 1
			--Add the Push mod
			pushmod = push()
			pushmod.push_value = pushrange.value
			modPanel.addModtoSelection( pushmod )			
		)
		
		on picklodmtl picked mtl do
		(
			
			LODmat = mtl
			picklodmtl.caption = mtl.name
			
		)		
			
		on lodit pressed do
		(	
		
			messagebox "Warning: The Multires() modifier has been disabled due to a MAX crash that occurs when LODing many meshes"
			gc()
			selset = getCurrentSelection()
			lodset = #()
			
			-- If it's not an editable mesh or poly, exit here rather than possibly after changing some
			-- selected objects in the next loop.
			for obj in selset do (
				
				if (( classOf obj != editable_Mesh) and ( classOf obj != editable_Poly)) then (
					
					messagebox "One of the selected objects is not an editable mesh or poly"
					return false
				)
			)
			
			clearselection()
			
			for n = 1 to selset.count do
			(
				--check it is editable mesh, convert
				if classOf selset[n] == editable_Mesh then convertTo selset[n] Editable_Poly
				--check obj is editable poly
				if classOf selset[n] == editable_Poly then
				(
					selOuterVerts selset[n]					
					newlod = copy selset[n]
					
					--check whether prefix LOD_ or postfix _LOD
					if rbt_lod.state == 1 then
					(
						newlod.name = "LOD_" + selset[n].name
					) else newlod.name =  selset[n].name + "_LOD"
					
					append lodset newlod					
				)
			)
			
			max modify mode

			--run push on each object
			for n = 1 to lodset.count do
			(
				select lodset[n]

				--optlod()
				pushlod()
				convertTo lodset[n] Editable_Mesh
				convertTo selset[n] Editable_Mesh

				--Apply standard material if none picked
				if LODmat == undefined then
				(
					lodset[n].material = standardMaterial()
					lodset[n].material.name = "CHANGE_THIS"
				) else lodset[n].material = LODmat

			)

			--select the lods
			select lodset
			selectMore selset

			macros.run "RS Lod" "SetLODByNameAndPos"

			clearselection()
			select lodset					
		)	
	)
