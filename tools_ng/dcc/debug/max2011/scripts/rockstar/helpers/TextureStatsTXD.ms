--
-- File:: rockstar/helpers/TextureStatsTXD.ms
-- Description:: 3dsMax Rage Shader Texture Map TXD Statistics
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 22 June 2007
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/material.ms"

-- .Net ListView Control
filein ( (getdir #maxroot) + "stdplugs/stdscripts/NET_ListViewWrapper.ms" )

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsRageTextureMapTXDStatsRoll "Texture Map TXD Statistics"
(
	-----------------------------------------------------------------------------------------
	-- Script-scope Variables
	-----------------------------------------------------------------------------------------
	local			appTitle	= "Texture Map TXD Statistics"
	
	-----------------------------------------------------------------------------------------
	-- Column Sorting
	-----------------------------------------------------------------------------------------
	local			ass 		= dotNet.loadAssembly ( ( pluginPaths.get 2 ) + "GenericEditor.dll" ) 
	local 			sorter 		= dotNetClass "GenericEditor.cListViewColumnIntSorter"

	-----------------------------------------------------------------------------------------
	-- UI Widgets and Layout
	-----------------------------------------------------------------------------------------
	button			btnUpdate 		"Update" align:#left width:100 across:3
	checkbox		chkSelection	"Selection Only" align:#center
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Texture_TXD_Stats" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	dotNetControl	lstView			"System.Windows.Forms.ListView" height:(500-65)
	progressbar		barProgress		across:2
	button			btnClose		"Close" align:#right width:100

	-----------------------------------------------------------------------------------------
	-- UI Event Handlers
	-----------------------------------------------------------------------------------------
	
	on btnUpdate pressed do
	(
	
		lstView.ListViewItemSorter = dotNetObject "GenericEditor.cListViewColumnNullSorter"
		lvops.ClearLvItems lstView
		
		local txdList = #()
		local txdObjList = #()
		local i = 1
		local objs
		
		if ( chkSelection.checked ) then
			objs = $selection
		else
			objs = $objects
			
		objCount = objs.count
		
		RsGetTxdList objs txdList txdObjList
		
		-- For each TXD object list find the maps that make up that TXD
		local txdMaps = #()
		for objList in txdObjList do
		(
			local objTextureList = #()
			local objBitmapList = #()
		
			barProgress.value = 100.0 * i /  txdObjList.count
		
			for o in objList do
			(				
				RsGetTexMapsFromObjWithMaps o objTextureList objBitmapList
			)
			
			append txdMaps objTextureList
			i += 1
		)

		-- Post process our lists
		
		for i = 1 to txdMaps.count do
		(
			-- Find shared-maps (maps used in >1 TXD)
			local sharedMaps = #()
			for j = 1 to txdMaps.count do
			(				
				if ( i != j ) then
				(
					if ( 0 != findItem txdMaps[j] txdMaps[i][j] ) then
					(
						-- Only add shared maps not already added
						--if ( 0 == findItem sharedMaps txdMaps[i][j] ) then
						append sharedMaps ( txdList[j] + ":" + txdMaps[i][j] )
					)
				)
			)
		
			format "%\t%\t%\t%\t%\n" (i as string) txdList[i] (txdMaps[i].count as string) (sharedMaps.count as string) (sharedMaps as string)
			lvops.AddLvItem lstView pTextItems:#( i as string, txdList[i], txdMaps[i].count as string, sharedMaps.count as string, sharedMaps as string )
		)
	)
	
	on btnClose pressed do
	(
		DestroyDialog RsRageTextureMapTXDStatsRoll
	)
	
	on lstView ColumnClick args do
	(
		if ( 1 != args.Column ) then
		(

			sorting = dotNetClass "System.Windows.Forms.SortOrder"

			lstView.ListViewItemSorter = dotNetObject "GenericEditor.cListViewColumnIntSorter" args.Column
			lstView.Sorting = sorting.Ascending
			lstView.Sort()
		)
	)
	
	on RsRageTextureMapTXDStatsRoll open do
	(

		lvops.InitListView lstView
		lvops.AddLvColumnHeader lstView pCaption:"ID" pWidth:30
		lvops.AddLvColumnHeader lstView pCaption:"TXD Name" pWidth:150
		lvops.AddLvColumnHeader lstView pCaption:"Texture Map Usage (count)" pWidth:150
		lvops.AddLvColumnHeader lstView pCaption:"Texture Map Shared (count)" pWidth:150
		lvops.AddLvColumnHeader lstView pCaption:"Texture Map Shared" pWidth:350
		
			
		sorting = dotNetClass "System.Windows.Forms.SortOrder"

		lstView.ListViewItemSorter = dotNetObject "GenericEditor.cListViewColumnIntSorter" 2
		local sortOrder = dotNetClass "System.Windows.Forms.SortOrder"
		lstView.Sorting = sortOrder.Descending;

	)
	
) -- End of rollout definition

DestroyDialog RsRageTextureMapTXDStatsRoll
CreateDialog RsRageTextureMapTXDStatsRoll modal:false width:800 height:500 \
				

-- End of script
