--
-- File:: rockstar/helpers/proc_types.ms
-- Description:: Procedural Type Viewer
--							Displays screenshots of the different procedural types
--
-- Date:: 1/12/2006
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsProceduralTypesRoll "View"
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Procedural_Types" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)

	edittext edtScreenSource "Bmp Source:" text:"X:\\GTA\\gta_bin\\gta_general\\collision\\proc_shots\\"
	dropdownlist lstProcedural "Procedural Type:"
	bitmap bmpProc width:450 height:450
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn RefreshProcedurals = (
	
		filename = "X:\\GTA\\build\\common\\data\\materials\\procedural.dat"
		
		if (getfiles filename).count < 1 then return 0
		
		intFile = openfile filename mode:"rb"
	
		nameList = #()
	
		while eof intFile == false do (
	
			intLine = RsRemoveSpacesFromStartOfString(readline intFile)
	
			if intLine.count > 0 and intLine[1] != "#" then (
	
				intTokens = filterstring intLine " \t"
	
				found = false
	
				nameToken = RsUppercase intTokens[1]
				
				if findstring nameToken "_DATA_START" == undefined and findstring nameToken "_DATA_END" == undefined then (
				
					if finditem nameList nameToken == 0 then (
	
						append nameList nameToken
					)
				)
			)
		)
	
		close intFile		
		
		lstProcedural.items = nameList
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	fn RefreshTexture = (
	
		local bmpset = bitmaptexture()
		
		loadfilename = edtScreenSource.text + lstProcedural.items[lstProcedural.selection] + ".bmp"
		
		bmpset.filename = loadfilename
		bmpset.reload()
	
		img = bitmap 450 450
		rm = rendermap bmpset into:img size:[450,450]
		bmpProc.bitmap = img
		close rm		
	)
	
	--////////////////////////////////////////////////////////////
	-- events
	--////////////////////////////////////////////////////////////
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on RsProceduralTypesRoll open do (
	
		RefreshProcedurals()
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on lstProcedural selected arg do (
	
		RefreshTexture()
	)
)

--------------------------------------------------------------
-- beginning of execution
--------------------------------------------------------------
try CloseRolloutFloater RsProceduralTypesUtil catch()
RsProceduralTypesUtil = newRolloutFloater "Procedural Types" 600 600 50 126
addRollout RsProceduralTypesRoll RsProceduralTypesUtil rolledup:false