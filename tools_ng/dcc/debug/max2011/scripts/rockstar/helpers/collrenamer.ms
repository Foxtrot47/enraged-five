--
-- File:: rockstar/helpers/collrenamer.ms
-- Description:: Renames collision
--
-- Author:: Greg Smith <greg.smith@rockstarnorth.com
-- Date:: 25/4/2007
--
-----------------------------------------------------------------------------

fn RsRecRename parentobj isroot:false = (

	for childobj in parentobj.children do (
	
		if getattrclass childobj == "Gta Collision" then (
		
			if isroot == false then (
			
				childobj.name = parentobj.name + "_collision"
			)
		) else (
		
			RsRecRename childobj
		)
	)
)

RsRecRename rootnode isroot:true