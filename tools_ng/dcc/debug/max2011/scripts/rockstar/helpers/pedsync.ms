--
-- File:: rockstar/helpers/pedsync.ms
-- Description:: Rockstar Ped Sync Tool: Ensure ped files are in sync
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 04/04/2007
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/util/string.ms"

-----------------------------------------------------------------------------
-- Rollout
-----------------------------------------------------------------------------
rollout RsPedSyncRoll "Ped Sync" width:250 height:300
(
	--////////////////////////////////////////////////////////////
	-- interface
	--////////////////////////////////////////////////////////////
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Sync_Tool" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	
	button btnImgToIde "Image To IDE" width:150
	button btnIdeToImg "IDE To Image" width:150
	button btnDatToIde "pedGrp.dat to IDE" width:150
	button btnIdeToDat "IDE to pedGrp.dat " width:150
	button btnIdeToPedVar "IDE to pedVariations.dat" width:150
	
	--////////////////////////////////////////////////////////////
	-- methods
	--////////////////////////////////////////////////////////////
	
	

	local SyncWarnings = #()
	
	--------------------------------------------------------------
	-- Generic file open with warning on undefined
	--------------------------------------------------------------	
	fn OpenFileForRead filename = (
		fiStream = openfile filename

		currentLine = ""

		if fiStream == undefined then (
			messagebox "Failed to open " + filename + ".  Check that is exists on your machine"
		)
		return fiStream 
	)
	--------------------------------------------------------------
	-- Throws the differences between ped lists.  Shamelessly nicked
	-- from Greg's pedtest.ms script
	--------------------------------------------------------------	
	fn ThrowSyncWarnings pedFile1 pedFile2 = (
	
		if SyncWarnings.count > 0 then (
			
			
			global warningHeader = "These peds were found in " + pedFile1 + " but not " + pedFile2 + "."
			global tempArr = #()
			for item in SyncWarnings do (
				append tempArr item
	
			)
			
			rollout SyncWarningsRoll "Sync Warnings"
			(
				label lblWarn ""
				listbox lstMissing1 items:tempArr height:30
				button btnOK "OK" width:100 
				
				on SyncWarningsRoll open do (
					
					lblWarn.text = warningHeader
				)
				
				
				on btnOK pressed do (
	
					DestroyDialog SyncWarningsRoll 
					
				)
				
			)
	
			CreateDialog SyncWarningsRoll width:300 modal:true			
	
	
		)
		SyncWarnings = #()
	
	)


	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn GetPedsFromPedIde pedsIde = (
		fiStream = OpenFileForRead (RsConfigGetCommonDir() + "data/peds.ide") 
		if fiStream != undefined then (

			currentLine = ""

			if fiStream == undefined then (
				messagebox "Failed to open peds.ide.  Check that is exists on your machine"
				return false
			)

			while eof fiStream == false do (
				currentLine = readline fiStream

				if currentLine[1] != "#" then (
					tokens = filterstring currentLine ","
					if tokens.count > 0 and tokens.count != 1 then (
						pedEntry = RsLowercase tokens[1]
						append pedsIde pedEntry
					)

				)

			)
		)
	)
	
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn GetUniquePedsFromPedGrpDat pedsGrpDat = (
		fiStream  = OpenFileForRead (RsConfigGetCommonDir() + "data/pedgrp.dat") 
		if fiStream != undefined then (

			while eof fiStream == false do (
				currentLine = readline fiStream

				if currentLine[1] != "#" then (
					tokens = filterstring currentLine ","
					for i=1 to i=tokens.count do (
						tabTokens = filterstring tokens[i] "\t"
						if tabTokens[1] != undefined then (

							pedEntry = RsRemoveSpacesFromString tabTokens[1]
							pedEntry = RsLowercase pedEntry
							if pedEntry != "" and pedEntry[1] != "#" then (
								append pedsGrpDat pedEntry
							)
						)
					)
				)
			)
		)
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------		
	fn GetPedsFromPedVarDat pedsVarDat = (
		fiStream = OpenFileForRead (RsConfigGetCommonDir() + "data/pedVariations.dat") 
		if fiStream != undefined then (
			while eof fiStream == false do (
				currentLine = readline fiStream
			
				if currentLine[1] != "#" then (
					tokens = filterstring currentLine ","
					if tokens.count == 2 then append pedsVarDat (RsLowercase tokens[1])
						
				)
			)
		
		)
		print pedsVarDat 
		
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn GetPedsFromPedCompImage pedsCompImg = (
		-- create temp ped list here
		luaFileName = RsConfigGetToolsScriptDir() + "generatecompedlist.rbs"
		
		cmdline = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + luaFileName
		if doscommand cmdline != 0 then (

			messagebox "Couldn't get ped list form componentped image.  Check the image exists."
			return false
		)
		
		compPedListFile = RsConfigGetIndDir() + "models\cdimages\componentpedlist.txt"
		
		fiStream = openfile compPedListFile
		
		currentLine = ""
		
		if fiStream == undefined then (
			messagebox "Failed to open pedcomponent list."
			return false
		)
		
		while eof fiStream == false do (
			currentLine = readline fiStream
			tokens = filterstring currentLine "."
			if tokens.count > 0 then (
				pedEntry = tokens[1]
				pedEntry = RsLowercase pedEntry
				append pedsCompImg pedEntry
				
			)
		)
		close fiStream
		deleteFile compPedListFile
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	fn ComparePedArrays peds1 peds2 = (
		unMatchedPeds = #()
		for ped1 in peds1 do (
			found = false
			for ped2 in peds2 while found == false do (
				if ped1 == ped2 then (
					found = true
				)
			)
			if found == false then append SyncWarnings ped1
		)
		--print "Unmatched"
		--print unMatchedPeds
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnDatToIde pressed do (
		pedsIde = #()
		pedsGrpDat = #()
		
		GetPedsFromPedIde pedsIde
		GetUniquePedsFromPedGrpDat pedsGrpDat 
		
		ComparePedArrays pedsGrpDat pedsIde
		
		ThrowSyncWarnings "pedsGrp.dat" "peds.ide"
	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnImgToIde pressed do (
		pedsIde = #()
		pedsImg = #()

		GetPedsFromPedIde pedsIde
		GetPedsFromPedCompImage  pedsImg 
		ComparePedArrays pedsImg pedsIde
		
		ThrowSyncWarnings "componentpeds.img" "peds.ide"

	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnIdeToDat pressed do (
		pedsIde = #()
		pedsGrpDat = #()

		GetPedsFromPedIde pedsIde
		GetUniquePedsFromPedGrpDat pedsGrpDat 

		ComparePedArrays pedsIde pedsGrpDat 

		ThrowSyncWarnings "peds.ide" "pedsGrp.dat" 
	)

	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnIdeToImg pressed do (
		pedsIde = #()
		pedsImg = #()

		GetPedsFromPedIde pedsIde
		GetPedsFromPedCompImage  pedsImg 
		ComparePedArrays pedsIde pedsImg

		ThrowSyncWarnings "peds.ide" "componentpeds.img"

	)
	
	--------------------------------------------------------------
	-- 
	--------------------------------------------------------------	
	on btnIdeToPedVar pressed do (
		pedsIde = #()
		pedsVarDat = #()
		
		GetPedsFromPedIde pedsIde
		GetPedsFromPedVarDat pedsVarDat
		
		print pedVarsDat
		
		ComparePedArrays pedsIde pedsVarDat
		ThrowSyncWarnings "peds.ide" "pedVariation.dat"
		
	)
)


try CloseRolloutFloater RsPedSync  catch()
RsPedSync  = newRollOutFloater "Sync Tool" 230 190
addRollout RsPedSyncRoll RsPedSync 