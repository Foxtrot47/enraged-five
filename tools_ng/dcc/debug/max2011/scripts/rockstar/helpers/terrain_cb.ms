-- Rockstar Terrain Painter
-- Rockstar North
-- 29/10/2008
-- by Luke Openshaw

-- terrain shader paint utility


filein "pipeline/util/file.ms"

rollout RsTerrainCbRoll "Terrain Painting"
(
	dropdownlist cboShader "Pick a shader" items:#("4-some", "7-some", "8-some")
	dropdownlist cboTextures "Select a texture to paint with:"
	button btnInit "Initilise"
	button btnRefresh "Refresh"
	
	--button btnPaint "Paint"
	
	
	global TerrainObj
	global TerrainPaintChannel = 9
	global RageShaderName
	global vPainttool
	
	local TexColList = #( color 0 0 0, color 0 0 255, color 0 255 0, color 0 255 255, color 255 0 0, color 255 0 255, color 255 255 0, color 255 255 255 )
	
	fn SetupModifier = (
		p = VertexPaint()
		p.mapchannel = TerrainPaintChannel
		addmodifier TerrainObj p
		vPainttool = VertexPaintTool()
		
		vPainttool.paintColor = TexColList[1]
		vPainttool.mapDisplayChannel = TerrainPaintChannel
		vPainttool.curPaintMode = 1
		
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
		
		
	
		
	)
	
	fn GetDxTextures7 mat textureList = (
			
		if mat.diffuseTexture_layer4 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer4.filename)
		if mat.diffuseTexture_layer5 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer5.filename)
		if mat.diffuseTexture_layer6 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer6.filename)
								
	)
		
	fn GetDxTextures8 mat textureList = (

		if mat.diffuseTexture_layer7 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer7.filename)
									
	)
	
	fn GetDxTextures mat textureList layers = (
		textureList = #()
		
		if mat.diffuseTexture_layer0 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer0.filename)
		if mat.diffuseTexture_layer1 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer1.filename)
		if mat.diffuseTexture_layer2 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer2.filename)
		if mat.diffuseTexture_layer3 != undefined then append textureList (RsRemovePath mat.diffuseTexture_layer3.filename)
		
		if layers >= 7 then GetDxTextures7 mat textureList
		if layers == 8 then GetDxTextures8 mat textureList
		
		return textureList
	)
	
	fn PopulateTextureListInner mat = (
		dxshadername = RsRemovePath mat.effectfile
		shaderExists = false
		if dxshadername == "terrain_cb_4lyr.fx" then (
			if classof mat.rendermaterial == Rage_Shader then (
				if RstGetShaderName mat.rendermaterial == "terrain_cb_4lyr.sps" then (
					shaderExists = true
				)
			)
			
			if shaderExists == false then (
				rageShader = Rage_Shader()
				RstSetShaderName rageShader "terrain_cb_4lyr.sps"
				mat.rendermaterial  = rageShader
			)
			
			cboTextures.items = GetDxTextures mat textureList 4
		)
		else if dxshadername == "terrain_cb_7lyr.fx" then (
			if classof mat.rendermaterial == Rage_Shader then (
				if RstGetShaderName mat.rendermaterial == "terrain_cb_7lyr.sps" then (
					shaderExists = true
				)
			)
						
			if shaderExists == false then (
				rageShader = Rage_Shader()
				RstSetShaderName rageShader "terrain_cb_7lyr.sps"
				mat.rendermaterial  = rageShader
			)
			
			cboTextures.items = GetDxTextures mat textureList 7
		)
		else if dxshadername == "terrain_cb_8lyr.fx" then (
			if classof mat.rendermaterial == Rage_Shader then (
				if RstGetShaderName mat.rendermaterial == "terrain_cb_8lyr.sps" then (
					shaderExists = true
				)
			)
						
			if shaderExists == false then (
				rageShader = Rage_Shader()
				RstSetShaderName rageShader "terrain_cb_8lyr.sps"
				mat.rendermaterial  = rageShader
			)
			
			cboTextures.items = GetDxTextures mat textureList 8
		)
		else (
			--messagebox "Invalid shader selected.  Make sure it is of type terrain_cb"
			return false
		)
		
		/*
		if shaderExists == false then (
			numMapVerts = meshop.getNumVerts TerrainObj.mesh 
			print 	numMapVerts
			for i = 1 to (numMapVerts) do (
				print "FUCK"
				meshop.setMapVert TerrainObj.mesh TerrainPaintChannel i (color 1 0 0)
			)
		)
		*/
		return true
	)
	
	
	
	fn PopulateTextureList = (
		textureList = #()
		dxShaderFound = false
		if classof TerrainObj.mat == MultiMaterial then (	
			for submat in TerrainObj.mat.materiallist do (
				if classof submat == DirectX_9_Shader then (
					dxShaderFound = PopulateTextureListInner submat
					/*
					dxmat = submat
					if classof submat.rendermaterial == Rage_Shader then (
						shadername = RstGetShaderName submat.rendermaterial
						if shadername == "terrain_cb_4lyr.sps" then (
							cboTextures.items = GetDxTextures submat textureList 4
							dxShaderFound = true
						)
						else if shadername == "terrain_cb_7lyr.sps" then (
							cboTextures.items = GetDxTextures submat textureList 7
							dxShaderFound = true
						)
						else if shadername == "terrain_cb_8lyr.sps" then (
							cboTextures.items = GetDxTextures submat textureList 8
							dxShaderFound = true
						)
						else messagebox "Invalid sps.  Check you have selected a 4, 7 or 8 layer terrain_cb shader as your software render material"

						
					)
					*/
				)
			)
		)
		else if classof TerrainObj.mat == DirectX_9_Shader then (
			dxShaderFound = PopulateTextureListInner TerrainObj.mat
			/*
			dxmat = submat
			if classof TerrainObj.mat.rendermaterial == Rage_Shader then (
				shadername = RstGetShaderName TerrainObj.mat.rendermaterial
				if shadername == "terrain_cb_4lyr.sps" then (
					cboTextures.items = GetDxTextures TerrainObj.mat textureList 4
					dxShaderFound = true
				)
				else if shadername == "terrain_cb_7lyr.sps" then (
					cboTextures.items = GetDxTextures TerrainObj.mat textureList 7
					dxShaderFound = true
				)
				else if shadername == "terrain_cb_8lyr.sps" then (
					cboTextures.items = GetDxTextures TerrainObj.mat textureList 8
					dxShaderFound = true
				)
				else messagebox "Invalid sps.  Check you have selected a 4, 7 or 8 layer terrain_cb shader as your software render material"
	
			)
			*/
		)
		dxShaderFound
	)
	
	fn ValidateTerrainObj = (
		local modNotFound = true
		for modv in TerrainObj.modifiers while modNotFound do (
			if classof modv == VertexPaint then (
				messagebox "This object already has a paint modifier."
				modNotFound = false
			)
		)
		return modNotFound
	)
	
	on cboTextures selected idx do (
		vPainttool.paintcolor = TexColList[idx]
		vPainttool.curPaintMode = 1
		aaa = vPainttool.keepToolboxOpen	
		vPainttool.keepToolboxOpen = aaa
		
		
	)
	
	on btnInit pressed do (
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one object"

		)
		else (
			
			TerrainObj = sel[1]
			if ValidateTerrainObj() == true then (
				if PopulateTextureList() == false then messagebox "Object is not setup with a DX terrain shader."
				else SetupModifier()
			)
		)
	)
	
	on btnRefresh pressed do (
		sel = selection as array
		if sel.count != 1 then (
			messagebox "Please select one object"

		)
		else (
			TerrainObj = sel[1]
			PopulateTextureList()
		)
	)
	

)

try CloseRolloutFloater RsTerrainUtil catch()
RsTerrainUtil = newRolloutFloater "Texture Slut" 200 170 50 126
addRollout RsTerrainCbRoll RsTerrainUtil rolledup:false


