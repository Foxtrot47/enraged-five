	--
-- File:: rockstar/helpers/terrain_painter_ui.ms
-- Description:: Extra UIs for the terrain painter
--
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
--
-----------------------------------------------------------------------------

	rollout RsTerrainCbRoll "Terrain Painting"
	(
		button btnDumpMap "Dump Colour Map"
		
		on btnDumpMap pressed do (
			texpaths = RsTerrainPalette.TexturePaths
			
			if RsTerrainPalette.Refresh() == true then (
				width = 512
				height = 1024
				horizoffsettex = 50
				horizoffsetcol = 250
				texturelist = #()

				DevilManager.init()
				outputfilename = (getdir #plugcfg) + "colour_map.bmp"

				DevilManager.newbitmap outputfilename width height
				vertoffset = (height - 50) / texpaths.count
				for i=1 to texpaths.count do (
					
					vertoffsetcurrent = ((vertoffset + 10) * (i - 1))
					colourbitmapfilename = (getdir #plugcfg + "colours/" + TexColNameList[i] + ".bmp")
					
					DevilManager.blit texpaths[i] outputfilename horizoffsettex vertoffsetcurrent 0 0 0 0 128 128 1
					DevilManager.blit colourbitmapfilename outputfilename horizoffsetcol vertoffsetcurrent 0 0 0 0 128 128 1	
				)
				
				messagebox ( "Texture/colour map saved to " + outputfilename )
			)
		)
	)


	rollout RsColourSwitcherRoll "Texture Channel Swap"
	(
		dropdownlist cboMaterials "Select a material"
		dropdownlist cboTexture1 "Texture 1:"
		dropdownlist cboTexture2 "Texture 2:"
		button btnSwap "Swap"
		button btnRefresh "Refresh"
		
		local CurrMapVertList = #()

		local CurrVertexAlphas = #() --( black=#(), blue=#(), green=#(), turquoise=#(), red=#(), purple=#(), yellow=#(), white=#() )
		local NewVertexAlphas = #()
		
		local TerrainMaterials = #()
		
		fn NormaliseColourFormat colour = (
			newcolour = color (colour.r/255) (colour.g/255) (colour.b/255)
		)
		
		fn CalcNewColour alphas = (
			
			i = 1
			vertColour = color 0 0 0 1
			for texcol in RsTerrainPalette.TexColList do (
				vertexAlpha = alphas[i]*(NormaliseColourFormat texcol)
				vertColour = vertColour + vertexAlpha
				
				i = i + 1
			)
			vertColour.a = 1
			vertColour
		)
		
		-- Nicked wholesale from Alex H's terrain shader
		fn GetAlpha rgb mask  = (
			alphaR = mask.r*rgb.x + (1.0-mask.r)*(1.0-rgb.x)
			alphaG = mask.g*rgb.y + (1.0-mask.g)*(1.0-rgb.y)
			alphaB = mask.b*rgb.z + (1.0-mask.b)*(1.0-rgb.z)
			
			return (alphaR*alphaG*alphaB)
		)
		
		fn RefreshDx mat = (
			dxshadername = RsRemovePathAndExtension mat.effectfile
			shaderExists = false
			shader = RsLoadShaderFromXml "terrain" dxshadername

			if shader != undefined then (
				cboTexture1.items = RsTerrainPalette.GetDxTextures mat shader
				cboTexture2.items = cboTexture1.items
			)
				
		)
		
		fn RefreshRage mat = (
			--mat = obj.mat
			numvars = RstGetVariableCount mat
			texlist = #()
			
			for i = 1 to numvars do (
							
				if RstGetVariableType mat i == "texmap" and (matchpattern (RstGetVariableName mat i) pattern:"Diffuse ?" == true) then (
					append texlist (RsRemovePath (RstGetVariable mat i))
				)
			)
			cboTexture1.items = texlist
			cboTexture2.items = texlist
		)
		
		fn DeconstructVertexColours obj = (
			
			if classof obj == Editable_mesh then (
				numverts = meshop.getnummapverts obj TerrainPaintChannel
				for i=1 to numverts do (
					append CurrMapVertList (meshop.getmapvert obj TerrainPaintChannel i)
				)
			)
			else if classof obj == Editable_Poly then (
				numverts = polyop.getnummapverts obj TerrainPaintChannel
				for i=1 to numverts do (
					append CurrMapVertList (polyop.getmapvert obj TerrainPaintChannel i)
				)
			)
			
			j = 1	
			for mapvert in CurrMapVertList do (
				
				append CurrVertexAlphas #()
				append NewVertexAlphas #()
				
				for col in RsTerrainPalette.TexColList do (
					append CurrVertexAlphas[j] ( GetAlpha mapvert ( NormaliseColourFormat col ) )
				)
				j = j + 1
			)
			
			return true
			--print CurrVertexAlphas
		)
		
		fn SwapAlphaChannels = (
			
			colour1Idx = cboTexture1.selection
			colour2Idx = cboTexture2.selection

			i = 1
			for vert in CurrVertexAlphas do (
				j = 1
				
				for alphaChannel in vert do (
					if j == colour1Idx then NewVertexAlphas[i][j] = CurrVertexAlphas[i][colour2Idx]
					else if j == colour2Idx then NewVertexAlphas[i][j] = CurrVertexAlphas[i][colour1Idx]
					else NewVertexAlphas[i][j] = CurrVertexAlphas[i][j]
					
					j = j + 1
				)
				
				i = i + 1
			)
			
			
			
			return true
		)
		
		fn ConstructAnSetVertexColours obj = (
			numverts = 0
			if classof obj == Editable_mesh then numverts = meshop.getnummapverts obj TerrainPaintChannel
			else if classof obj == Editable_Poly then numverts = polyop.getnummapverts obj TerrainPaintChannel
			
			for i=1 to numverts do (
				
				newVertColour = ( CalcNewColour NewVertexAlphas[i] )
				newVertColour = [newVertColour.r, newVertColour.g, newVertColour.b]
				
				meshop.setMapSupport obj TerrainPaintChannel true
				if classof obj == Editable_mesh then meshop.setmapvert obj TerrainPaintChannel i newVertColour
				else if classof obj == Editable_Poly then (

					polyop.setmapvert obj TerrainPaintChannel i newVertColour
				)
				
			)
			
			
			return true	
		)
		
		fn SwapTextureChannelsDx mat = (
			global gmat2 = mat -- has to be a global to use in the eexecute statement since it is execute in the global scope
			
			dxshadername = RsRemovePathAndExtension gmat2.effectfile
						
			shader = RsLoadShaderFromXml "terrain" dxshadername
			
			global textureChannel1 = undefined
			global bumpChannel1 = undefined
			global textureChannel2 = undefined 
			global bumpChannel2 = undefined
			
			textureChannelName1 = undefined
			bumpChannelName1 = undefined
			textureChannelName2 = undefined
			bumpChannelName2 = undefined
			
			for i=1 to shader.shadervars.count do (
				shadervar = shader.shadervars[i]
				try (
					matvar = execute ("gmat2." + shadervar.name)
				) catch (
					append errorlist ((getCurrentException()) + " " + shadervar.name )
				)
				if matvar != undefined then (
							
					if shadervar.type == "difftex" then (
						if RsRemovePath matvar.filename == cboTexture1.selected then (
							textureChannel1 = matvar
							textureChannelName1 = shadervar.name
							-- bump is always in the next slot
							bumpChannel1 = execute ("gmat2." + shader.shadervars[i+1].name)
							bumpChannelName1 = shader.shadervars[i+1].name
						)
						else if RsRemovePath matvar.filename == cboTexture2.selected then (
							textureChannel2 = matvar
							textureChannelName2 = shadervar.name
							-- bump is always in the next slot
							bumpChannel2 = execute ("gmat2." + shader.shadervars[i+1].name)
							bumpChannelName2 = shader.shadervars[i+1].name
						)
					)
				)
				
				
				
			)
		
			
			if textureChannel1 != undefined and textureChannel2 != undefined then (
							
				matvar = execute ("gmat2." + textureChannelName1 + " = textureChannel2 ")
				matvar = execute ("gmat2." + textureChannelName2 + " = textureChannel1 ")
				
				if bumpChannel1 != undefined and bumpChannel2 != undefined then (
					matvar = execute ("gmat2." + bumpChannelName1 + " = bumpChannel2 ")
					matvar = execute ("gmat2." + bumpChannelName2 + " = bumpChannel1 ")
				)
			)
			else (
				messagebox "Tried to switch on an undefined channel.  Bailing out."
				return false
			)
			
			-- Hack to fix redraw issue with Editable_Polys.  Sceneredraw does not solve the issue
			if classof obj == Editable_Poly then (
				dxmat = obj.mat
				obj.mat = undefined
				obj.mat = dxmat
			)
			
			return true
			
		)
		
		fn SwapTextureChannelsRage mat = (
			
			--mat = obj.mat
			numvars = RstGetVariableCount mat
			
			
			diffTexmap1 = undefined
			bumpTexmap1 = undefined
			idx1 = undefined
			diffTexmap2 = undefined
			bumpTexmap2 = undefined 
			idx2 = undefined
			
			
			for i = 1 to numvars do (

				if RstGetVariableType mat i == "texmap" and (matchpattern (RstGetVariableName mat i) pattern:"Diffuse ?" == true) then (
					
					if RsRemovePath (RstGetVariable mat i) == cboTexture1.selected then (
						diffTexmap1 = RstGetVariable mat i
						bumpTexmap1 = RstGetVariable mat (i+1)
						idx1 = i
					) 
					else if RsRemovePath (RstGetVariable mat i) == cboTexture2.selected then (
						diffTexmap2 = RstGetVariable mat i
						bumpTexmap2 = RstGetVariable mat (i+1)
						idx2 = i
					)
				)
			)
			
			if diffTexmap1 != undefined and diffTexmap2 != undefined then (
				RstSetVariable mat idx1 diffTexmap2
				RstSetVariable mat (idx1+1) bumpTexmap2
				RstSetVariable mat idx2 diffTexmap1
				RstSetVariable mat (idx2+1) bumpTexmap1
			)
			else (
				messagebox "Tried to switch on an undefined channel.  Bailing out."
				return false
			)
			
			return true
			
		)
		
		
		on btnSwap pressed do (
			CurrMapVertList = #()
				
			CurrVertexAlphas = #()
			NewVertexAlphas = #()
			
			sel = selection as array
			
			if sel.count == 1  and classof sel[1].mat == DirectX_9_Shader then (
			
				if SwapTextureChannelsDx sel[1].mat == false then return false
				if DeconstructVertexColours sel[1] == false then return false
				if SwapAlphaChannels() == false then return false
				if ConstructAnSetVertexColours sel[1] == false then return false
				
				RsTerrainPalette.Refresh()
				RefreshDx sel[1].mat
			)
			else if  sel.count == 1  and classof sel[1].mat == Rage_Shader then (
				if SwapTextureChannelsRage sel[1].mat == false then return false
				if DeconstructVertexColours sel[1] == false then return false
				if SwapAlphaChannels() == false then return false
				if ConstructAnSetVertexColours sel[1] == false then return false

				RsTerrainPalette.Refresh()
				RefreshRage sel[1].mat
			)
			else if sel.count == 1  and classof sel[1].mat == MultiMaterial then (
				for submat in sel[1].mat.materiallist do (
					if classof submat == Rage_Shader then (
						if submat.name == cboMaterials.selected then (
							if SwapTextureChannelsRage submat == false then return false
						)

						
					)
					
				)
				if DeconstructVertexColours sel[1] == false then return false
				if SwapAlphaChannels() == false then return false
				if ConstructAnSetVertexColours sel[1] == false then return false
				
				for submat in sel[1].mat.materiallist do (
					if classof submat == Rage_Shader then (
						if submat.name == cboMaterials.selected then RefreshRage submat
					)
				)
				RsTerrainPalette.Refresh()
			)
			else (
				messagebox "Please select only one object. Ensure it is an object with a terrain shader applied"
			) 
		)
		
		on btnRefresh pressed do (
			RsTerrainPalette.PopulateMaterialList()
			TerrainMaterials = #()
			sel = selection as array
			if sel.count == 1  and classof sel[1].mat == DirectX_9_Shader then (
				
				RefreshDx sel[1].mat
				append TerrainMaterials sel[1].mat
			)
			else if sel.count == 1  and classof sel[1].mat == Rage_Shader then (
				RefreshRage sel[1].mat
				append TerrainMaterials sel[1].mat
			)
			else if sel.count == 1  and classof sel[1].mat == MultiMaterial then (
				for submat in sel[1].mat.materiallist do (
					if sel.count == 1  and classof submat == Rage_Shader then (
						append TerrainMaterials submat
					)
				)
				if TerrainMaterials.count > 0 then RefreshRage TerrainMaterials[1]
			)
			else (
				messagebox "Please select only one object. Ensure it is an object with a terrain shader applied"
				return 0
			) 
			
			tempmatlist = #()
			for mat in TerrainMaterials do append tempmatlist mat.name
			
			cboMaterials.items = tempmatlist
		)
		
		on cboMaterials selected idx do (
				
			RefreshRage TerrainMaterials[cboMaterials.selection]
		)
		
	)


	rollout RsTerrainDxRageSwap "DX RAGE Swap"
	(
		button btnDxToRage "Switch DX To RAGE"
		
		
		fn SwitchDxToRage = (
			if querybox "Are you sure you want to switch all materials in your scene to Rage Materials?" == false then return false
			dxMatCount = 0
			for anode in rootnode.children do (
				RsDxRageSwitcheroo anode
				mat = anode.mat


				if classof mat == Multimaterial then (
					for j=1 to mat.materiallist.count do (
						submat = mat.materiallist[j]

						if classof submat == DirectX_Shader or classof submat == DirectX_9_Shader then (

							if classof submat.rendermaterial == Rage_Shader then (
								mat.materiallist[j] = submat.rendermaterial
								dxMatCount = dxMatCount + 1

							)
						)
					)
				)
				else if classof mat == DirectX_Shader or classof mat == DirectX_9_Shader then (

					if classof mat.rendermaterial == Rage_Shader then (
						anode.material = mat.rendermaterial
						dxMatCount = dxMatCount + 1
					)
				)
			)
			
			messagebox ("Converted " + (dxMatCount as string) + " terrain material(s).")
		)
		
		on btnDxToRage pressed do (
			SwitchDxToRage()
		)
	)
	
	try CloseRolloutFloater RsTerrainUtil catch()
	RsTerrainUtil = newRolloutFloater "Terrain Utils" 200 400 50 126
	addRollout RsTerrainCbRoll RsTerrainUtil rolledup:false
	addRollout RsColourSwitcherRoll RsTerrainUtil rolledup:false
	addRollout RsTerrainDxRageSwap RsTerrainUtil rolledup:false