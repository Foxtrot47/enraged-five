--
-- File:: pipeline/export/model.ms
-- Description:: Simple model mesh and material exporter.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 November 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/cloth.ms"
filein "pipeline/util/drawablelod.ms"
filein "pipeline/export/models/globals.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- name: RsModelExportMesh
-- desc: Export a simple model mesh, supports fragments and cloth.
--
fn RsModelExportMesh obj asciiMesh:false = (

	local modelFile = ( RsModelOutputDirectory + "/" + obj.name )
	local modelDir = ( RsModelOutputDirectory + "/" + obj.name )

	RsDeleteFiles (modelFile + ".*")
	RsDeleteFiles (modelDir + "*.*")

	select obj
	rexReset()
	graphRoot = rexGetRoot()

	selectobjs = #()
	for childobj in obj.children do (

		if ( classof childobj == Col_Mesh or 
			classof childobj == Col_Box or 
			classof childobj == Col_Sphere or 
			classof childobj == Col_Cylinder or
			classof childobj == Col_Capsule ) then (

			append selectobjs childobj
		)
	)

	-------------------------------------------------------------------------
	-- Geometry Export
	-------------------------------------------------------------------------

	local idxFragment = ( GetAttrIndex "Gta Object" "Is Fragment" )
	if ( getattr obj idxFragment == true ) then (

		entity = rexAddChild graphRoot "entity" "Entity"
		rexSetPropertyValue entity "OutputPath" modelDir		
		rexSetPropertyValue entity "SkipBoundSectionOfTypeFile" "true"
		rexSetPropertyValue entity "EntityWriteBoundSituations" "true"
		rexSetPropertyValue entity "EntityFragmentWriteMeshes" "true"

		RsTotalSurfProperty = ""
		RsSetupFragBounds obj entity modelDir
		rexSetPropertyValue entity "SurfaceType" RsTotalSurfProperty		

		select obj

		-- RAGE Drawable LOD Support
		local drawobjs = RsLodDrawable_SetUserProps obj
		selectMore drawobjs

		mesh = rexAddChild entity "mesh" "Mesh"
		rexSetPropertyValue mesh "OutputPath" modelDir
		rexSetPropertyValue mesh "ExportTextures" "true"
		rexSetPropertyValue mesh "TextureOutputPath" modelDir
		rexSetPropertyValue mesh "MeshForSkeleton" "true"
		rexSetPropertyValue mesh "MeshSkinOffset" "false"
		rexSetPropertyValue mesh "MeshAsAscii" (asciiMesh as String)
		rexSetNodes mesh

		if ( undefined != obj.modifiers[#Skin] ) then
		(
			skel = rexAddChild entity "skeleton" "Skeleton"
			rexSetPropertyValue skel "OutputPath" modelDir
			rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
			rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
			rexSetNodes skel
		)

		fragment = rexAddChild entity "fragment" "Fragment"
		rexSetPropertyValue fragment "OutputPath" modelDir
		rexSetNodes fragment

		-- Cloth Support (if flagged)
		if ( RsCloth.IsCloth obj ) then
			RsCloth.SetupEnvClothForExport obj entity mesh
	) 
	else 
	(

		entity = rexAddChild graphRoot "entity" "Entity"
		rexSetPropertyValue entity "OutputPath" modelDir

		-- RAGE Drawable LOD Support
		local drawobjs = RsLodDrawable_SetUserProps obj
		selectMore drawobjs
		
		mesh = rexAddChild entity "mesh" "Mesh"
		rexSetPropertyValue mesh "OutputPath" modelDir
		rexSetPropertyValue mesh "ExportTextures" "true"
		rexSetPropertyValue mesh "TextureOutputPath" modelDir
		rexSetPropertyValue mesh "MeshAsAscii" (asciiMesh as String)
		rexSetNodes mesh

		if ( undefined != obj.modifiers[#Skin] ) then
		(
			skel = rexAddChild entity "skeleton" "Skeleton"
			rexSetPropertyValue skel "OutputPath" modelDir
			rexSetPropertyValue skel "SkeletonUseBoneIDs" "true"
			rexSetPropertyValue skel "SkeletonRootAtOrigin" "true"
			rexSetNodes skel
		)
	)

	select obj

	rexExport()

	return modelDir	
)


--
-- name: RsModelExportGroup
-- desc: Export array of objects.
--
fn RsModelExportGroup objs asciiMesh:false = (
		
		local dirs = #()
		for o in objs do
		(
			if ( "Gta Object" != ( GetAttrClass o ) ) then
				continue
			
			format "Export Model: %\n" o.name
			dir = ( RsModelExportMesh o asciiMesh:asciiMesh )
			append dirs dir
		)
		
		dirs
)

-- pipeline/export/model.ms
