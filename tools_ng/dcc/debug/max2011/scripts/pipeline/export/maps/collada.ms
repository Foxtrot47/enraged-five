--
-- File:: pipeline/export/maps/collada.ms
-- Description:: Map Collada Export and Utility Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 1 May 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "pipeline/export/maps/objects.ms"
filein "pipeline/util/collada.ms"
filein "pipeline/util/file.ms"
filein "pipeline/util/hierarchy.ms"
filein "pipeline/util/scripttimer.ms"
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsMapCollision_OctreeSize = 250000

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- struct: RsMapColladaStruct
-- desc: Map COLLADA export functions.
--
-- GLOBAL INSTANCE: RsMapCollada
--
-- COLLADA files are exported with the DAE extension.  Zipped COLLADA files
-- are exported with the DAE.ZIP.
--
struct RsMapColladaStruct
(	
	-- Member Data
	ext = ".DAE",
	ext_zip = ".ZIP",

	--
	-- name: GetStreamDir
	-- desc: Return stream directory for storing collada files.
	--
	fn GetStreamDir = (
		( RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/collada/" )
	),
	
	--
	-- name: GetLogFile
	-- desc: Return log filename for writing listener output during export.
	--
	fn GetLogFile = (
		( RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/collada_export.log" )
	),
	
	--
	-- name: CleanFiles
	-- desc: Clean out all collada (.DAE) files from export directory.
	--
	fn CleanFiles = 
	(
		local mapStreamDir = ( GetStreamDir() )
		RsDeleteFiles ( mapStreamDir + "drawables/*.*" )
		RsDeleteFiles ( mapStreamDir + "fragments/*.*" )
		RsDeleteFiles ( mapStreamDir + "static_collision/*.*" )
	),

	--
	-- name: SerialiseObjectsArrays
	-- desc: Write out an arrays of arrays of Objects to an XML file.
	--
	fn SerialiseObjectsArrays rootname objects filename =
	(
		local xmldoc = XmlDocument()
		xmldoc.init()
		
		-- Initialise our root node with a count attribute.
		local rootelem = xmldoc.createelement rootname
		local countattr = xmldoc.createattribute "count"
		countattr.value = ( objects.count as string )
		rootelem.Attributes.append countattr
		xmldoc.document.AppendChild rootelem
		
		-- Loop through objects lists
		for list in objects do
		(
			local listelem = xmldoc.createelement "objects"
			local listcountattr = xmldoc.createattribute "count"
			listcountattr.value = ( list.count as string )
			listelem.Attributes.append listcountattr
		
			for o in list do
			(
				local objelem = xmldoc.createelement "object"
				local nameattr = xmldoc.createattribute "name"
				nameattr.value = o.name
				objelem.Attributes.append nameattr
				
				listelem.AppendChild objelem
			)
			
			rootelem.AppendChild listelem
		)
		
		-- Write XML
		xmldoc.save filename
	),

	--
	-- name: ExportGeometry
	-- desc: Export all geometry objects (Gta Object) in object list.
	--
	-- Dynamic collision will be included with the necessary geometry.
	-- Static collision is exported in a single separate Collada file.
	--
	fn ExportGeometry objs zipped:false =
	(
		local gtaObjects = #()
		format "\n\nStarting Geometry Export...\n"

		for o in objs do
		(
			if ( RsMapObjectIsDontExport o ) then
				continue
				
			if ( not ( RsMapObjectIsGeometry o ) ) then
			(
				format "Skipping % as its not a geometry object.\n" o.name
				continue
			)
			if ( DrawableLodHierarchy.HasHigherDetailNode o ) then
			(
				format "Skipping % as its part of a Drawable LOD hierarchy but not the highest detail mesh.\n" o.name
				continue
			)
			if ( RsMapObjectIsFragment o ) then
			(
				-- Temporarily skip fragment objects, these are picked up in
				-- the ExportFragments function below.
				continue
			)

			local subObjects = #( o )

			-- Objects that are in a drawable LOD hierarchy will have their
			-- lower detail objects appended here.
			local getLowerDetailFunc = DrawableLodHierarchy.GetLowerDetailNode
			local lowerDetail = ( getLowerDetailFunc o )
			while ( lowerDetail != undefined ) do
			(
				append subObjects lowerDetail
				lowerDetail = ( getLowerDetailFunc lowerDetail )
			)

			-- Dynamic objects will get their collision appended here.
			-- And then exported in the RsCollada.ExportObjectsSeparately call.
			if ( RsMapObjectIsDynamic obj ) then
			(
				for child in o.children do
				(
					if ( not ( IsCollisionObject child ) ) then
						continue

					append subObjects child
				)
			)

			append gtaObjects subObjects
		)

		local colladaStream = ( GetStreamDir() + "drawables/" )
		RsCollada.ExportObjectsSeparately gtaObjects colladaStream zipped:zipped
		format "Geometry Export Done.\n"
	),

	--
	-- name: ExportFragments
	-- desc: Export all fragment objects object list.
	--
	fn ExportFragments objs zipped:false = (
	
		local gtaObjects = #()
		format "\n\nStarting Fragment Export...\n"
	
		for o in objs do
		(
			if ( not ( RsMapObjectIsFragment o ) ) then
				continue
				
			format "Exporting fragment: %\n" o.name
			
			local subObjects = ( RsHierarchy_AllRootChildren o )
			append gtaObjects subObjects
		)
		
		local colladaStream = ( GetStreamDir() + "fragments/" )
		RsCollada.ExportObjectsSeparately gtaObjects colladaStream zipped:zipped
		
		format "Fragment Export Done.\n"
	),

	--
	-- name: ExportStaticCollision
	-- desc: Export all static map collision (Gta Collision) in map scene.
	--
	fn ExportStaticCollision zipped:false =
	(
		if ( not RsMapIsGeneric ) then
		(
			local collObjects = #()
			for o in $objects do
			(
				if ( RsMapObjectIsDontExport o ) then
					continue
				if ( not ( RsMapObjectIsGeometry o ) ) then
					continue

				if ( RsMapObjectIsDynamic o ) then
					continue
				if ( RsMapObjectIsFragment o ) then
					continue

				for child in o.children do
				(
					if ( not ( RsMapObjectIsCollision child ) ) then
						continue
					append collObjects child
				)
			)

			local collFile = ( GetStreamDir() + "static_collision/static_collision" )
			if ( zipped ) then
				collFile = collFile + ext_zip
			else
				collFile = collFile + ext
			
			format "Exporting static collision to Collada file: %\n" collFile
			RsCollada.ExportObjectsTogether collObjects collFile
			
			-- Now we export our collision binary tree.  At some point this
			-- may be moved out into Ruby script, but lets just utilise the
			-- pretty well tested GTA3 "CreateBinaryTree" function, and 
			-- serialise its return sets into XML.
			local sections = CreateBinaryTree collObjects RsMapCollision_OctreeSize
			local sectionsFilename = ( GetStreamDir() + "static_collision/static_collision.xml" )
			SerialiseObjectsArrays "static_collision_sections" sections sectionsFilename zipped:zipped
		)
		else
		(
			format "Not exporting static collision because map is generic.\n"
		)
	),

	--
	-- name: ExportScene
	-- desc: Export current scene as a single Collada (DAE) file.
	--
	fn ExportScene zipped:false =
	(	
		local logFilename = ( GetLogFile() )
		openLog logFilename "w"
		
		RsMapSetupGlobals()
		local filename = ( GetStreamDir() + RsMapName )
		if ( zipped ) then
			filename = filename + ext_zip
		else
			filename = filename + ext

		local timer = ScriptTimer()
		timer.start()

		RsMakeSurePathExists filename
		exportFile filename #noPrompt using:ColladaExporter

		timer.end()
		format "Collada Map Scene Export: % ms\n" ( timer.interval() )
		
		closeLog()
	),

	--
	-- name: ExportSelected
	-- desc: Export selected map geometry, static/dynamic collision.
	--
	fn ExportSelected zipped:false =
	(
		local logFilename = ( GetLogFile() )
		openLog logFilename "w"
		
		RsMapSetupGlobals()
		-- No clean here.

		local timer = ScriptTimer()
		timer.start()

		ExportGeometry selection zipped:zipped
		ExportFragments selection zipped:zipped
		ExportStaticCollision zipped:zipped

		timer.end()
		format "Collada Map Export (selected): % ms\n" ( timer.interval() )
		
		closeLog()
	),

	--
	-- name: Export
	-- desc: Export complete map geometry, static/dynamic collision.
	--
	fn Export zipped:false =
	(
		local logFilename = ( GetLogFile() )
		openLog logFilename "w"
		
		RsMapSetupGlobals()
		CleanFiles()

		local timer = ScriptTimer()
		timer.start()

		ExportGeometry $objects zipped:zipped
		ExportFragments $objects zipped:zipped
		ExportStaticCollision zipped:zipped

		timer.end()
		format "Collada Map Export: % ms\n" ( timer.interval() )
		
		closeLog()
	)
	
) -- RsMapColladaStruct struct

-----------------------------------------------------------------------------
-- Struct Global Instances
-----------------------------------------------------------------------------
global RsMapCollada = RsMapColladaStruct()

-- pipeline/export/maps/collada.ms
