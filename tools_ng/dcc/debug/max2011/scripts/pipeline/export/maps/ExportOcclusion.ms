--
-- File:: pipeline/export/maps/ExportOcclusion.ms
-- Description::  Export zones 
--
-----------------------------------------------------------------------------

fn ExportOcclusionFile filename = (
	local file
	
	if filename == undefined do return false

	file = createfile filename
	if file == undefined do (
		messagebox "Cannot open file" title:"Occlusion file export"
		return false
	)	
	format "occl\n" to:file
	for obj in rootNode.children do (
		if classof obj == Box do (

			flagVal = 0
			
			if getattr obj (getattrindex "GtaCullzone" "Cull Dont Stream") then (
				flagVal = flagVal + 1
			)
			
			format "%, %, %, " obj.pos.x obj.pos.y obj.pos.z to:file
			format "%, %, %, " (obj.width*obj.scale.x) (obj.length*obj.scale.y) (obj.height*obj.scale.z) to:file
			format "%, %, %, " obj.rotation.z_rotation obj.rotation.y_rotation obj.rotation.x_rotation to:file
			format "%\n" flagVal to:file
		)
	)
	format "end\n" to:file
	
	close file
)

-- get filename
ExportOcclusionFile (getSaveFilename caption:"Occlusion file" types:"Occlusion file (*.ipl)|*.ipl")

