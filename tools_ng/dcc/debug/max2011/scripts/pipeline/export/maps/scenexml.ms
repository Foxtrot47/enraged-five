--
-- File: pipeline/export/maps/scenexml.ms
-- Desc: SceneXml exporting and IDE/IPL file creation functions.
--
-- Author: David Muir <david.muir@rockstarnorth.com>
-- Date: 19 January 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "rockstar/export/settings.ms"
filein "pipeline/util/file.ms"

--
-- name: RsSceneXmlDefaultFilename
-- desc: Return default SceneXml filename for the current max file.
--
fn RsSceneXmlDefaultFilename mapname = (	
	return (RsConfigGetMapStreamDir() + "maps/" + mapname + "/" + mapname + ".xml")
)

--
-- name: RsSceneXmlDefaultLogFile
-- desc: Return default SceneXml processing log filename.
--
fn RsSceneXmlDefaultLogFile mapname suffix = (	
	return (RsConfigGetMapStreamDir() + "maps/" + mapname + "/" + mapname + "_" + suffix + ".log" )
)

--
-- name: RsSceneXmlExport
-- desc: Export map container SceneXml XML file.
--
fn RsSceneXmlExport map = (

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."

	RsMapSetupGlobals map
	local filename = ( RsSceneXmlDefaultFilename RsMapName )
	
	RsMakeSurePathExists filename
	SceneXmlReloadCfg()
	if ( map.is_container() ) then
		SceneXmlExportFrom #( map.cont ) filename
	else 
		SceneXmlExportFrom map.objects filename
	
	format "SceneXml Exported to %\n" filename
)


--
-- name: RsSceneXmlExportAs
-- desc: Export entire scene (all containers) to a specified SceneXml XML file.
--
-- param: filename - Export file
--
fn RsSceneXmlExportAs filename = (

	RsMakeSurePathExists filename
	SceneXmlReloadCfg()
	SceneXmlExport filename
	format "SceneXml Exported to %\n" filename
)


--
-- name: RsSceneXmlExportIDE
-- desc: Export map IDE from SceneXml file.
--
-- param: filename - SceneXml XML filename (optional).
-- param: log_file - Log file (optional).
-- param: debug    - add --debug switch allowing debugger attach.
--
fn RsSceneXmlExportIDE filename:unsupplied log_file:unsupplied debug:false = (

	local bindir = RsConfig.toolsbin
	local builddir = RsConfigGetIndDir()
	if ( unsupplied == filename ) then
		filename = ( RsSceneXmlDefaultFilename RsMapName )
	if ( unsupplied == log_file ) then
		log_file = ( RsSceneXmlDefaultLogFile RsMapName "export" )
	local debugstr = ""
	if ( debug ) then
		debugstr = "--debug"	
	
	local ss = stringStream ""
	if ( unsupplied != log_file ) then
		format "%/MapExportIDEIPL.exe --build % --ide % --log % % %" bindir builddir RsMapIdeFilename log_file debugstr filename to:ss
	else
		format "%/MapExportIDEIPL.exe --build % --ide % % %" bindir builddir RsMapIdeFilename debugstr filename to:ss

	format "Command: %\n" (ss as string)

	RsMakeSurePathExists RsMapIdeFilename
	RsMakeSurePathExists log_file
	format "%\n" (ss as String)
	HiddenDOSCommand (ss as String)

	RsMapExportIDEAppendParentTXD RsMapIdeFilename
)


--
-- name: RsSceneXmlExportIDEForObject
-- desc: Export IDE from SceneXml file for a single object.
--
-- param: idefile - IDE file to write
-- param: filename - SceneXml XML filename (optional)
--
fn RsSceneXmlExportIDEForObject idefile objectName filename:unsupplied = (

	local bindir = RsConfig.toolsbin
	local builddir = RsConfigGetIndDir()
	if ( unsupplied == filename ) then
		filename = ( RsSceneXmlDefaultFilename RsMapName )
	
	local ss = stringStream ""
	format "%/MapExportIDEIPL.exe --build % --ide % --ideobject % %" bindir builddir idefile objectName filename to:ss

	format "Command: %\n" (ss as string)

	RsMakeSurePathExists RsMapIdeFilename
	format "%\n" (ss as String)
	HiddenDOSCommand (ss as String)
)

--
-- name: RsSceneXmlExportIDEIPL
-- desc: Export map IDE and IPL files from SceneXml file.
--
-- param: filename - SceneXml XML filename (optional).
-- param: log_file - Log file (optional).
-- param: debug    - add --debug switch allowing debugger attach
--
fn RsSceneXmlExportIDEIPL filename:unsupplied iplver:6 log_file:unsupplied debug:false = (

	local bindir = RsConfig.toolsbin
	local builddir = RsConfigGetIndDir()
	if ( unsupplied == filename ) then
		filename = ( RsSceneXmlDefaultFilename RsMapName )
	if ( unsupplied == log_file ) then
		log_file = ( RsSceneXmlDefaultLogFile RsMapName "export" )
	local streamdir = ( RsConfigGetMapStreamDir() + "maps/" + RsMapName )
	local debugstr = ""
	if ( debug ) then
		debugstr = "--debug"
	
	RsDeleteFiles ( streamdir + "/*.ipl" )
	
	local ss = stringStream ""
	if ( ( "" != RsMapIdeFilename ) and ( "" != RsMapIplFilename ) ) then
		format "%/MapExportIDEIPL.exe --build % --ide % --ipl% % --iplstream % --log % % %" bindir builddir RsMapIdeFilename iplver RsMapIplFilename streamdir log_file debugstr filename to:ss
	else if ( "" != RsMapIdeFilename ) then
		format "%/MapExportIDEIPL.exe --build % --ide % --log % % %" bindir builddir RsMapIdeFilename log_file debugstr filename to:ss
	else if ( "" != RsMapIplFilename ) then
		format "%/MapExportIDEIPL.exe --build % --ipl% % --iplstream % --log % % %" bindir builddir iplver RsMapIplFilename streamdir log_file debugstr filename to:ss
	
	RsMakeSurePathExists RsMapIdeFilename
	RsMakeSurePathExists RsMapIplFilename
	RsMakeSurePathExists log_file
	format "%\n" (ss as String)
	HiddenDOSCommand (ss as String)

	RsMapExportIDEAppendParentTXD RsMapIdeFilename
)


--
-- name: RsSceneXmlExportIPL
-- desc: Export map single IPL file from SceneXml file.
--
fn RsSceneXmlExportIPL iplver:6 = (

	-- Export SceneXml File
	local bindir = RsConfig.toolsbin
	local builddir = RsConfigGetIndDir()
	local scenexml_filename = ( getOpenFileName caption:"Open SceneXml..." types:"SceneXml File (*.xml)|*.xml|All Files (*.*)|*.*" )
		
	-- Export IPL File from SceneXml File
	local ipl_filename = ( getSaveFileName caption:"Save IPL As..." types:"IPL File (*.ipl)|*.ipl|All Files (*.*)|*.*" )
	local ss = stringStream ""
	
	format "%/MapExportIDEIPL.exe --build % --ipl% % --single %"  bindir builddir iplver ipl_filename scenexml_filename to:ss 

	format "%\n" (ss as String)
	RsMakeSurePathExists ipl_filename
	HiddenDOSCommand (ss as String)
)


--
-- name: RsSceneXmlValidate
-- desc: Validate a map file from SceneXml file.
--
-- param: filename - SceneXml XML filename (optional)
-- param: log_file - Log file (optional)
-- param: debug    - add --debug switch allowing debugger attach
--
fn RsSceneXmlValidate filename:unsupplied log_file:unsupplied debug:false = (

	local bindir = RsConfig.toolsbin
	local builddir = RsConfigGetIndDir()
	if ( unsupplied == filename ) then
		filename = ( RsSceneXmlDefaultFilename RsMapName )
	if ( unsupplied == log_file ) then
		log_file = ( RsSceneXmlDefaultLogFile RsMapName "validation" )
	local debugstr = ""
	if ( debug ) then
		debugstr = "--debug"
	
	local ss = stringStream ""
	if ( unsupplied != log_file ) then
		format "%/MapExportValidation.exe --build % --log % % %" bindir builddir log_file debugstr filename to:ss
	else
		format "%/MapExportValidation.exe --build % % %" bindir builddir debugstr filename to:ss

	format "%\n" (ss as String)
	RsMakeSurePathExists log_file
	HiddenDOSCommand (ss as String)
)

-- pipeline/export/maps/scenexml.ms
