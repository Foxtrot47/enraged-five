--
-- File:: pipeline/export/maps/ImportOcclusion.ms
-- Description::  Import zones 
--
-----------------------------------------------------------------------------

fn ImportOcclusionFile filename = (
	local file
	local line
	local str
	local elements
	local minV, maxV
	local zone
	
	local bReading = false

	if filename == undefined do return false
	
	file = openfile filename mode:"r"
	
	while (not eof file) do (
		line = readline file
		if line == "occl" do ( 
			bReading = true
			continue
		)	
		if line == "end" do bReading = false

		if bReading == true do (
			elements = filterString line ", "
			zone = Box()
			zone.pos = (Point3 (elements[1] as float) (elements[2] as float) (elements[3] as float))
			zone.width = elements[4] as float
			zone.length = elements[5] as float
			zone.height = elements[6] as float
			zone.rotation.z_rotation = elements[7] as float
		)
	)
	
	close file
	
	return true
)

-- get filename
ImportOcclusionFile (getOpenFilename caption:"Occlusion file" types:"Occlusion file (*.ipl)|*.ipl")


