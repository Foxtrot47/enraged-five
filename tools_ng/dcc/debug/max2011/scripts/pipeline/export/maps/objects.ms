--
-- File:: pipeline/export/maps/objects.ms
-- Description:: Map object test functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 23 July 2009
--

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global idxExportGeometry = ( GetAttrIndex "Gta Object" "Export Geometry" )
global idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
global idxIsFragment = ( GetAttrIndex "Gta Object" "Is Fragment" )
global idxIsFragProxy = ( GetAttrIndex "Gta Object" "Is FragProxy" )
global idxIsAnimProxy = ( GetAttrIndex "Gta Object" "Is AnimProxy" )
global idxIsDynamic = ( GetAttrIndex "Gta Object" "Is Dynamic" )

-----------------------------------------------------------------------------
-- Structures
-----------------------------------------------------------------------------

--
-- struct: RsMapContainer
-- desc: Hybrid container structure.  See RsMapGetMapContainers.
--
struct RsMapContainer (
	name = "",							-- Name of container.
	cont = undefined,				-- Container object (if applicable).
	propgroup = false,			-- Is Prop Group?
	objects = #(),					-- Array of all objects in container.
	selobjects = #(),				-- Array of selected objects in container.
		
	-- Return true iff this is a Container.
	fn is_container = (
		( ( cont != undefined ) and ( Container == classof cont ) )
	),
	
	-- Return true iff this is a Prop Group.
	fn is_propgroup = (
		( propgroup )
	),
	
	-- Return true iff this map container is exportable (i.e. Container is open)
	fn is_exportable = (
		if ( is_container() ) then
			( cont.isopen() and ( objects.count > 0 ) ) -- Containers only exportable if open.
		else
			true -- Prop Groups, props and interiors etc are always exportable.
	),
	
	-- Return number of objects in container.
	fn count = (
		( objects.count )
	),
	
	-- Return number of selected objects to export.
	fn selcount = (
		( selobjects.count )
	)
)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- name: RsMapObjectGetTopLevelOpenContainers
-- desc: Return array of open top-level scene Container objects.
--
-- OBSOLETE 2010/03/24 DHM
--
fn RsMapObjectGetTopLevelOpenContainers = (

	containers = #()
	for o in rootNode.children do
	(
		if ( undefined != o.parent ) then
			continue
		if ( Container != classof o ) then
			continue
		if ( not o.IsOpen() ) then
			continue
	
		append containers o
	)
	
	containers
)


--
-- name: RsMapObjectGetTopLevelContainerFor
-- desc: Return the top-level container object for the specified object
--       or undefined if there isn't one.
--
fn RsMapObjectGetTopLevelContainerFor o = (
	
	obj = o.parent
	while ( ( undefined != obj ) and ( undefined != obj.parent ) ) do
		obj = obj.parent
		
	if ( Container == classof obj ) then
		return ( obj )
		
	return ( undefined )
)


--
-- name: RsMapObjectGetPropGroups
-- desc: Return array of unique prop group names ("Gta Object", "Prop Group" attribute).
--
fn RsMapObjectGetPropGroups = (

	local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
	local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
	local attrPropGroupDefault = ( GetAttrDefault "Gta Object" attrObjectPropGroupIdx )

	groups = #()
	for o in rootNode.children do
	(	
		local attrClass = ( GetAttrClass o )
		
		if ( ( "Gta Object" != attrClass ) and 
		     ( "Gta MILOTri" != attrClass ) ) then
			continue
		
		local propGroup = attrPropGroupDefault
		if ( "Gta Object" == attrClass ) then
		(
			propGroup = RsLowercase( GetAttr o attrObjectPropGroupIdx )
		)
		else if ( "Gta MILOTri" == attrClass ) then
		(
			propGroup = RsLowercase( GetAttr o attrMiloPropGroupIdx )
		)
		
		if ( attrPropGroupDefault == propGroup ) then
			continue
		
		if ( 0 == findItem groups propGroup ) then
			append groups propGroup
	)
	
	groups
)


--
-- name: RsMapObjectGetPropGroupObjects
-- desc: Return array of object nodes within a specific group.
--
fn RsMapObjectGetGroupObjects grpname = (

	local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
	local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
	
	grpname = RsLowercase( grpname )
	group_objects = #()
	for o in rootNode.children do
	(
		local attrClass = ( GetAttrclass o )
		
		if ( "Gta Object" == attrClass ) then
		(		
			if ( grpname == RsLowercase( GetAttr o attrObjectPropGroupIdx ) ) then
				append group_objects o
		)
		else if ( "Gta MILOTri" == attrClass ) then
		(
			if ( grpname == RsLowercase( GetAttr o attrMiloPropGroupIdx ) ) then
				append group_objects o
		)
	)
	
	group_objects
)


--
-- name: RsMapGetMapContainers
-- desc: Return an array of RsMapContainer objects to that each object in
--       selset is within a RsMapContainer object (if applicable).
--
-- This supports Container helpers, Prop Groups and also "old-style" maps
-- for interiors and props, and the odd map section which hasn't been
-- brought across yet.
--
fn RsMapGetMapContainers = (

	local attrObjectPropGroupIdx = ( GetAttrIndex "Gta Object" "Prop Group" )
	local attrMiloPropGroupIdx = ( GetAttrIndex "Gta MILOTri" "Prop Group" )
	local attrAnimPropGroupIdx = ( GetAttrIndex "RsStatedAnim" "Prop Group" )
	local containers_map = #() -- Array of RsMapContainer objects
	local propgroups_map = #() -- Array of RsMapContainer objects

	local containers = #()
	local propgroups = #()
	local gcontainer = undefined
	
	-- Do a pass on all rootNode children to find all available containers.
	-- Populating their entire objects array.
	for o in rootNode.children do
	(
		local cont = undefined 
		if ( Container == classof o ) then
		(
			cont = o
		)
		else
		(
			-- Otherwise try and find parent container.
			cont = ( RsMapObjectGetTopLevelContainerFor o )
		)
		
		if ( undefined != cont ) then
		(
			-- Object is within a 3dsmax 2010 Container Helper
			local idxContainer = ( findItem containers cont )
			if ( 0 == idxContainer ) then
			(
				append containers cont
				if ( Container == classof o ) then
					append containers_map ( RsMapContainer cont.name cont false cont.children )
				else
					append containers_map ( RsMapContainer cont.name cont false #( o ) )
			)
			else
			(
				if ( Container != classof o ) then
					append containers_map[idxContainer].objects o
			)
		)
		else
		(
			-- Determine if the object is within a RSG Prop Group
			local propGroupName = "undefined"
			
			if ( "Gta Object" == GetAttrClass o ) then
				propGroupName = ( GetAttr o attrObjectPropGroupIdx )
			else if ( "Gta MILOTri" == GetAttrClass o ) then
				propGroupName = ( GetAttr o attrMiloPropGroupIdx )
			else if ( "RsStatedAnim" == GetAttrClass o ) then
				propGroupName = ( GetAttr o attrAnimPropGroupIdx )
			
			local idxPropGroup = ( findItem propgroups propGroupName )
			if ( ( "undefined" != propGroupName ) and
			     ( 0 == idxPropGroup ) ) then
			(
				append propgroups propGroupName
				append propgroups_map ( RsMapContainer propGroupName undefined true #( o ) )
			)
			else if ( idxPropGroup > 0 ) then
			(
				append propgroups_map[idxPropGroup].objects o
			)
			else
			(
				-- Otherwise we add it to the global container (e.g. non-Container maps, props, interiors).
				if ( undefined == gcontainer ) then
				(
					gcontainer = ( RsMapContainer (RsRemoveExtension maxFileName) undefined false #( o )  )
				)
				else
				(
					append gcontainer.objects o
				)
			)
		)
	)
	
	-- Do a pass of our selection to populate our selobjects arrays.
	for o in $selection do
	(
		local cont = undefined 
		if ( Container == classof o ) then
		(
			cont = o
		)
		else
		(
			-- Otherwise try and find parent container.
			cont = ( RsMapObjectGetTopLevelContainerFor o )
		)
		
		if ( undefined != cont ) then
		(
			-- Object is within a 3dsmax 2010 Container Helper
			local idxContainer = ( findItem containers cont )
			if ( 0 == idxContainer ) then
			(
				append containers cont
				if ( Container == classof o ) then
					append containers_map ( RsMapContainer cont.name cont false cont.children )
				else
					append containers_map ( RsMapContainer cont.name cont false #( o ) )
			)
			else
			(
				if ( Container != classof o ) then
					append containers_map[idxContainer].selobjects o
			)
		)
		else
		(
			-- Determine if the object is within a RSG Prop Group
			local propGroupName = "undefined"
			
			if ( "Gta Object" == GetAttrClass o ) then
				propGroupName = ( GetAttr o attrObjectPropGroupIdx )
			else if ( "Gta MILOTri" == GetAttrClass o ) then
				propGroupName = ( GetAttr o attrMiloPropGroupIdx )
			
			local idxPropGroup = ( findItem propgroups propGroupName )
			if ( ( "undefined" != propGroupName ) and
			     ( 0 == idxPropGroup ) ) then
			(
				append propgroups propGroupName
				append propgroups_map ( RsMapContainer propGroupName undefined true #( o ) )
			)
			else if ( idxPropGroup > 0 ) then
			(
				append propgroups_map[idxPropGroup].selobjects o
			)
			else
			(
				-- Otherwise we add it to the global container (e.g. non-Container maps, props, interiors).
				if ( undefined == gcontainer ) then
				(
					gcontainer = ( RsMapContainer (RsRemoveExtension maxFileName) undefined false #( o )  )
				)
				else
				(
					append gcontainer.selobjects o
				)
			)
		)
	)	
	
	-- we only append the global container if we have no prop or container groups already.
	maps = ( containers_map + propgroups_map )
	if ( ( 0 == maps.count ) and ( undefined != gcontainer ) ) then
		append maps gcontainer
	
	maps
)


-----------------------------------------------------------------------------
-- Map Object Test Functions
-----------------------------------------------------------------------------


--
-- name: RsMapObjectIsDontExport
-- desc: Determine whether a node should be exported or not.
--
fn RsMapObjectIsDontExport obj = 
(
	local dontExport = false

	if ( "Gta Object" == ( GetAttrClass obj ) ) then
		dontExport = ( GetAttr obj idxDontExport )

	dontExport
)

--
-- name: RsMapObjectIsGeometry
-- desc: Determine whether a node is a valid geometry object for export.
--
-- note: checks for XRefs, and our "Dont Export" attribute.
--
fn RsMapObjectIsGeometry obj =
(
	if ( undefined == obj ) then
		false
	else if ( XRefObject == ( classof obj ) ) then
		false
	else if ( "Gta Object" != ( GetAttrClass obj ) ) then
		false
	else if ( true == ( GetAttr obj idxExportGeometry ) ) then
		true
	else
		false
)

--
-- name: RsMapObjectIsFragment
-- desc: Determine whether a node is a valid fragment object for export.
--
fn RsMapObjectIsFragment obj =
(
	local isfrag = false

	if ( ( IsGeometry obj ) and ( true == ( GetAttr obj idxIsFragment ) ) ) then
		isfrag = true

	isfrag
)

--
-- name: RsMapObjectIsFragmentProxy
-- desc: Determine whether a node is a valid fragment proxy object.
--
fn RsMapObjectIsFragmentProxy obj =
(
	-- Cannot use IsGeometry because fragment proxies
	-- are set to Dont Export.
	if ( XRefObject == ( classof obj ) ) then
		return ( false )
	else if ( "Gta Object" != ( GetAttrClass obj ) ) then
		return ( false )

	-- There is a hidden "Is FragProxy" attribute, which I don't
	-- see getting set by anything but we better check it in case.
	if ( true == ( GetAttr obj idxIsFragProxy ) ) then
		return ( true )

	-- Otherwise we see if there is an object with the same name
	-- and with a "_frag_" suffix.
	local fragmentName = ( obj.name + "_frag_" )
	local fragmentObject = ( getNodeByName fragmentName ignoreCase:true )
	if ( undefined != fragmentObject ) then
		return ( true )

	false
)

--
-- name: RsMapObjectIsDynamic
-- desc: Determine whether an object is a dynamic object.
--
fn RsMapObjectIsDynamic obj = 
(
	local isdyn = false

	if ( ( IsGeometry obj ) and ( true == ( GetAttr obj idxIsDynamic ) ) ) then
		isdyn = true

	isdyn
)

--
-- name: RsMapObjectIsCollision
-- desc: Determine whether a node is a valid collision object for export.
--
fn RsMapObjectIsCollision obj = 
(
	( "Gta Collision" == ( GetAttrClass obj ) )
)

-- pipeline/export/maps/objects.ms
