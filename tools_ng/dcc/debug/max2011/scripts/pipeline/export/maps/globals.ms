--
-- File:: pipeline/export/maps/globals.ms
-- Description:: Map Export Global Variables
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 22 April 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/export/maps/ruby_script_generator.ms"
filein "pipeline/util/assert.ms"
filein "pipeline/util/scripttimer.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------

global RsMapName
global RsMapObjectPrefix
global RsMapPrefixes
global RsMapDir
global RsMapIdeFilename
global RsMapIplFilename
global RsMapRpfFilename
global RsMapImgFilename
global RsStatedAnimFilename
global RsMapIsGeneric
global RsMapType
global RsMapIsPatch
global RsMapIsContainer
global RsMapIsPropGroup
global RsContentScriptQueue
global RsMapPedantic		-- Pedantic checks during export?  Defined in maps.xml
global RsMapLevel			-- Which level the current map belongs to
global RsMapLevelDir		-- Level path

-- What the map contains
global RsMapHasIRef 
global RsMapHasXRef
global RsMapHasGeom
global RsMapHasBounds
global RsMapHasAnim
global RsMapHasMetaInfo

-- Compatibility global for disabling source texture path checks during
-- export (default: true in map.ms).
global RsMapExportTexturePathCheck

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsMapResetGlobals
-- desc: Reset map exporter global variables.
--
fn RsMapResetGlobals = (

	RsMapDir = ""
	RsMapName = ""
	RsMapObjectPrefix = ""
	RsMapPrefixes = #()
	RsMapIdeFilename = ""
	RsMapIplFilename = ""
	RsMapRpfFilename = ""
	RsMapImgFilename = ""	
	RsStatedAnimFilename = ""
	RsMapIsGeneric = false
	RsMapType = #none
	RsMapIsPatch = false
	RsMapIsContainer = false
	RsMapIsPropGroup = false
	RsMapPedantic = false
	RsMapLevel = ""
	RsMapLevelDir = ""
	RsContentScriptQueue = #()
	
	RsMapHasIRef = false
	RsMapHasXRef = false
	RsMapHasGeom = false
	RsMapHasBounds = false
	RsMapHasAnim = false
	RsMapHasMetaInfo = false

	OK
)

fn RsObjectNamespacePass o = 
(
	local suspectMergedObjects = #()
	local idxDontExport = ( GetAttrIndex "Gta Object" "Dont Export" )
	local idxTXD = ( GetAttrIndex "Gta Object" "TXD" )
	local prefixPattern = RsMapObjectPrefix + "*"

	if ( ( undefined != o.parent ) and ( Gta_MILO != classof o.parent ) and ( GtaMloRoom != classof o.parent ) ) then
	(
		format "Skipping: %\n" o.name
		return false
	)
	if ( Dummy == ( classof o ) ) then
		return false
	if ( XRefObject == ( classof o ) ) then
		return false
	if ( "Gta Object" != ( GetAttrClass o ) ) then
		return false
	if ( true == ( GetAttr o idxDontExport ) ) then
		return false

	-- Merged object check
	-- Currently disabled as its slow.  DHM.
	--for prefix in RsMapPrefixes do
	--(
	--	local prefixPattern = prefix + "*"
	--	format "Check: %\n" prefixPattern
	--	if ( matchPattern o.name pattern:prefixPattern ignoreCase:true ) then
	--		append suspectMergedObjects o
	--)
		
	-- Otherwise lets check the object's name.
	if ( not ( matchPattern o.name pattern:prefixPattern ignoreCase:true ) ) then
	(
		-- Set new object name.
		format "RsMapNamespacePass: Set object % name to %\n" o.name ( RsMapObjectPrefix + o.name )
		o.name = ( RsMapObjectPrefix + o.name )
	)
	
	local txdname = ( GetAttr o idxTXD )
	if ( ( not ( matchPattern txdname pattern:prefixPattern ignoreCase:true ) ) and
		 ( "CHANGEME" != txdname ) ) then
	(
		-- Set object TXD name
		format "RsMapNamespacePass: Set object % TXD to %\n" o.name ( RsMapObjectPrefix + txdname )
		SetAttr o idxTXD ( RsMapObjectPrefix + txdname )
	)
)

fn recurseChildren obj =
(
	RsObjectNamespacePass obj
	if ( Gta_MILO == classof obj ) or ( GtaMloRoom == classof obj ) then
	(
		for o in obj.children do recurseChildren o
	)
)

--
-- name: RsMapNamespacePass
-- desc: Enforce max object namespace criteria.  This function needs to
--       be called VERY VERY early in the export process.
--
fn RsMapNamespacePass map = (

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."

	local timer = ScriptTimer()
	timer.start()

	-- Process our object list.  The logic in this loop will need
	-- to change should we want to enforce non-Gta Object naming
	-- conventions.
	for o in map.objects do
	(
		recurseChildren o
	)
	
	-- Put this in a dialog/rollout.
	--format "Suspected Merged Objects:\n"
	--for o in suspectMergedObjects do
	--	format "\t%\n" o.name
	--print suspectMergedObjects
	
	timer.end()
	format "Map namespace verify took: %ms\n" (timer.interval() as Integer)
	true
)


--
-- name: RsMapFindLevelForMapRpf
-- desc: Find a level content node for a maprpf node (by walking up content tree)
--
fn RsMapFindLevelForMapRpf maprpf = (

	RsAssert (ContentMapRpf == classof maprpf) message:"Invalid ContentMapRpf node!"
	
	local levelObj = undefined
	local tempParent = maprpf.parent
	
	if ( "ContentLevel" != (classof tempParent as string) ) then
	(
		while ( ( tempParent != undefined ) and
				"ContentLevel" != (classof tempParent as string) ) do
		(
			print (classof tempParent)
			tempParent = tempParent.parent
		)
	)
	if ( "ContentLevel" == (classof tempParent as string) ) then
	(
		levelObj = tempParent
	)
	levelObj
)


--
-- name: RsMapLoadGlobalsFromContentTree
-- desc: Load map exporter global variables from rageMaxConfigParser content
--       functions.
--
fn RsMapLoadGlobalsFromContentTree = (

	RsProjectContentReload()
	local mapNode = ( RsProjectContentFind RsMapName type:"map" )
	if ( undefined == mapNode ) then
		return ( false )
		
	local mapRpfNode = ( RsProjectContentFind RsMapName type:"maprpf" )
	if ( undefined == mapRpfNode ) then
		return ( false )

	RsMapIsGeneric = ( not mapNode.instances )
	RsMapObjectPrefix = mapNode.prefix
	RsMapPedantic = mapNode.pedantic
	RsMapLevelObj = ( RsMapFindLevelForMapRpf mapRpfNode )
	RsMapLevel = RsMapLevelObj.name
	
	children = mapRpfNode.children
	RsMapDir = mapRpfNode.path
	RsMapLevelDir = RsMapLevelObj.path
	
	RsMapIplFilename = ""
	RsMapRpfFilename = ""
	RsMapImgFilename = ""
	for c in mapRpfNode.children do
	(
		if ( undefined != ( findString c.filename ".ide" ) ) then
			RsMapIdeFilename = mapRpfNode.path + c.filename
		else if ( undefined != ( findString c.filename ".ipl" ) ) then
			RsMapIplFilename = mapRpfNode.path + c.filename
		else if ( undefined != ( findString c.filename ".rpf" ) ) then
			RsMapRpfFilename = mapRpfNode.path + c.filename
		else if ( undefined != ( findString c.filename ".img" ) ) then
			RsMapImgFilename = mapRpfNode.path + c.filename
	)
		
	-- Initialise RsMapType constant.
	if ( RsMapIsGeneric ) then
	(
		if ( undefined != ( findString RsMapIdeFilename "prop" ) ) then
			RsMapType = #prop
		else if ( undefined != ( findString RsMapIdeFilename "interior" ) ) then
			RsMapType = #interior
		else
			RsMapType = #normal
	)
	else
	(
		RsMapType = #normal
	)
		
	-- stated anim xml
	RsStatedAnimFilename = mapRpfNode.path + "/" + mapRpfNode.name + ".meta"

	true
)


--
-- name: RsMapSetupGlobals
-- desc: Setup map exporter global variables.  If a Container is specified
--       then that will be exported as a block (RsMapIsContainer set to true), 
--       otherwise the entire file is exported (RsMapIsContainer set to false).
--
fn RsMapSetupGlobals map = (

	RsAssert (RsMapContainer == classof map) message:"Invalid RsMapContainer object supplied."
	RsMapResetGlobals()

	if ( RsCurrentConfig > RsConfigInfos.count ) then 
	(
		MessageBox "Invalid config index (out of bounds)." title:"Map Configuration Error"
		return false
	)

	if ( map.is_container() ) then
	(
		RsMapName = RsLowercase( map.name )
		RsMapIsContainer = true
	)
	else if ( map.is_propgroup() ) then
	(
		RsMapName = RsLowercase( map.name )
		RsMapIsPropGroup = true
	)
	else
	(
		RsMapName = RsLowercase( map.name )
		RsMapIsContainer = false
		RsMapIsPropGroup = false
	)

	if ( not RsMapLoadGlobalsFromContentTree() ) then 
	(
		local mapxmlpath = RsConfigGetContentDir() + "maps.xml"
		local outputxmlpath = RsConfigGetContentDir() + "output.xml"
		local msg = stringStream ""
		
		if ( RsMapIsContainer ) then
			format "The % Container does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%" RsMapName mapxmlpath outputxmlpath to:msg
		else if ( RsMapIsPropGroup ) then
			format "The % group does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%" RsMapName mapxmlpath outputxmlpath to:msg
		else 
			format "The % map does not exist or has an incorrect entry in the content tree.\n\nHave you added it to the following files?\n\n\t%\n\t%" RsMapName mapxmlpath outputxmlpath to:msg

		messagebox (msg as string) title:"Map Configuration Error"
		return false
	)
	
	true
)

-- pipeline/export/maps/globals.ms
