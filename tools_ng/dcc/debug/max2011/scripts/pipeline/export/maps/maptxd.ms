--
-- File:: pipeline/export/maps/maptxd.ms
-- Description:: Map exporter TXD export logic functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--
-----------------------------------------------------------------------------
-- HISTORY
-----------------------------------------------------------------------------
-- Rockstar Map Txd Export
-- Rockstar North
-- 1/3/2005
-- by Greg Smith
-- Export a map sections texture dictionaries into the game
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/globals.ms"
filein "rockstar/helpers/parentiser.ms"

-----------------------------------------------------------------------------
-- Script Globals
-----------------------------------------------------------------------------
RsMisplacedTexMaps = #()

--
-- struct: RsMapTXD
-- desc: Map TXD export logic functions.
--
struct RsMapTXD
(
	--
	-- name: ReportMisplacedTextures
	-- desc: Report misplaced textures
	--
	fn ReportMisplacedTextures = (
		
		-- Displays an error box with a list of all misplaced textures, times out after 60 seconds
		rollout RsMapMisplacedTextures "The following textures have not be saved under the projects texture directory and have NOT been exported"
		(
			local countVal = 60
			
			listbox lstMisplaced items:RsMisplacedTexMaps
			button btnOK "OK" width:100 pos:[150,150]
			timer tmrCount
			
			on tmrCount tick do (
		
			if countVal == 0 then (
				
				DestroyDialog RsMapMisplacedTextures 
			) else (
		
				countVal = countVal - 1
				btnOK.text = "OK (" + (countVal as string) + ")"
			)
		)

			on btnOK pressed do (

				DestroyDialog RsMapMisplacedTextures
			)

			on RsMapMisplacedTextures open do (
				btnOK.text = "OK (" + (countVal as string) + ")"
			)
		)
		
		-- If our path check is enabled (default) then display our dialog
		if ( RsMapExportTexturePathCheck and RsMapPedantic ) then
			CreateDialog RsMapMisplacedTextures width:800 modal:true
	),

	--
	-- name: GetAllSceneTXDs
	-- desc: Return array of all scene TXDs
	--
	fn GetAllSceneTXDs = (
	
		txdlist = #()
		
		modelsin = #()
		RsGetMapObjects $objects modelsin exportGeometryCheck:true						
		
		RsGetSortedTxdList modelsin txdlist		
		txdlist
	),

	--
	-- name: GetObjectWithTxd
	-- desc: Append object to objList if it uses txdName
	--
	fn GetObjectWithTxd objList obj txdName = (
	
		local idxTxd = getattrindex "Gta Object" "TXD"
		local idxDontExport = GetAttrIndex "Gta Object" "Dont Export"
	
		if classof obj != XrefObject then (
	
			if (getattrclass obj) == "Gta Object" do (

				valTxd = RsLowercase(getattr obj idxTxd)
				valDontExport = getattr obj idxDontExport

				if valDontExport == false then (

					if valTxd == txdName then (

						append objList obj
					)
				)
			)
		)
	),

	--
	-- name: DeleteTextures
	-- desc:
	--
	fn DeleteTextures txdlist txdobjlist normalonly:false = (

		inputobjlist = #()
		RsGetInputObjectList rootnode.children inputobjlist

		for i = 1 to txdlist.count do (	

			txdname = txdlist[i]

			progressStart ("deleting textures for " + txdname)			
			
			txdobjs = txdobjlist[i]
		
			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()
		
			RsGetTexMapsByTxdNameNoStrip texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist
			
			for i = 1 to texmaplist.count do (
			
				if getProgressCancel() then throw("progress_cancel")
				progressUpdate (100.0*i/texmaplist.count)
			
				if isbumplist[i] == false and normalonly == true then continue
			
				texmap = texmaplist[i]
				maxsize = maxsizelist[i]
			
				stripName = RsStripTexturePath(texmap)
				singleName = (filterstring texmap "+")[1]
				
				foundFiles = getfiles singleName
				
				if foundFiles.count > 0 then (
					
					outputFile = (RsConfigGetTexDir() + stripName + ".dds")
			
					RsDeleteFiles outputFile
				)
			)
			
			progressEnd()
		)
	),

	--
	-- name: ExportTextures
	-- desc: export the textures require for specific txd's
	--
	fn ExportTextures txdlist txdobjlist normalonly:false = (
	
		RsMakeSurePathExists (RsConfigGetTexDir())
		RsMakeSurePathExists (RsConfigGetLodTexDir())

		texmaplist = #()
		maxsizelist = #()
		isbumplist = #()
		usagelist = #()
		exemptionlist = #()
		
		RsMisplacedTexMaps = #()
		
		lod_texmaplist = #()
		lod_maxsizelist = #()
		lod_isbumplist = #()
		lod_usagelist = #()
		lod_exemptionlist = #()
		
		inputobjlist = #()
		RsGetInputObjectList rootnode.children inputobjlist
		
		progressStart ("compiling texture list")
		
		for i = 1 to txdlist.count do (
			
			if getProgressCancel() then throw("progress_cancel")
			progressUpdate (100.0*i/txdlist.count)

			txdname = txdlist[i]
			txdobjs = txdobjlist[i]
			
			if RsIsTXDUsedOnLod txdname then (
			
				RsGetTexMapsByTxdNameNoStrip lod_texmaplist lod_maxsizelist lod_isbumplist lod_usagelist txdname srcobjlist:inputobjlist
			) else (	
			
				RsGetTexMapsByTxdNameNoStrip texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist exemptionlist:exemptionlist
			)
		)
		
		progressEnd()
				
		progressStart ("exporting textures")
		
		for i = 1 to texmaplist.count do (

			progressUpdate (100.0*i/texmaplist.count)
			if getProgressCancel() then throw("progress_cancel")
			
			texmap = texmaplist[i]
			maxsize = maxsizelist[i]
			isbump = isbumplist[i]
			usage = usagelist[i]
			isresizeexempt = exemptionlist[i]
			
			-- Ensure textures live under the project root dir
			rootTexmapPath = ""
			tokens = filterstring texmap "/"
			tokensAlt = filterstring texmap "\\" -- Projected light maps use the old style slashes
			correctTexpath = RsConfigGetTextureSourceDir()
			correctTexpathTokens = filterstring correctTexpath "/"
			
			if tokens.count > (correctTexpathTokens.count - 1) then (
				for i = 1 to correctTexpathTokens.count do (
					rootTexmapPath = rootTexmapPath + tokens[i] + "/"
				)
			)
			else if tokensAlt.count > (correctTexpathTokens.count - 1)then (
				for i = 1 to correctTexpathTokens.count do (
					rootTexmapPath = rootTexmapPath + tokensAlt[i] + "/"
				)
			)
			
			-- Hack: if we find a proxy texture, re-point it to the high-res location
			if (( RsLowercase (RsConfigGetProxyTextureSourceDir())) == (RsLowercase rootTexmapPath)) or (RsMakeSafeSlashes (RsLowercase (RsConfigGetProxyTextureSourceDir()))) == (RsMakeSafeSlashes (RsLowercase rootTexmapPath)) then (
				texmap = correctTexpath
				for i = correctTexpathTokens.count + 1 to tokens.count do (
					texmap = texmap + "/" + tokens[i]
				)
				print ("Fixed up proxy path to: " + texmap)
			)
			-- If a texture doesnt reside under the correct texture root then add to misplaced texture list
			else if (RsLowercase correctTexpath) != (RsLowercase rootTexmapPath) then (
				append RsMisplacedTexMaps texMap
				
				-- If our path check is enabled (default) then skip this 
				-- texture export.
				if ( RsMapExportTexturePathCheck and RsMapPedantic ) then
					continue
			)
			
			stripName = RsStripTexturePath(texmap)
			singleName = (filterstring texmap "+>")[1]

			outputFile = (RsConfigGetTexDir() + RsRemoveSpacesFromString(stripName) + ".dds")

			if isbump == false and normalonly == true then continue
			
			print ("exported texture: "+texmap+" outputFile:"+outputFile)
			
			if isbump then (
				rexExportBumpTexture texmap outputFile maxsize
			) else if isresizeexempt then (
				rexSetTextureShift 0
				rexExportTexture texmap outputFile maxsize
				rexSetTextureShift 1
			) else (
				
				rexExportTexture texmap outputFile maxsize
			)
		)
		
		progressEnd()
		
		progressStart ("exporting lod textures")
		
		for i = 1 to lod_texmaplist.count do (

			progressUpdate (100.0*i/lod_texmaplist.count)
			if getProgressCancel() then throw("progress_cancel")

			texmap = lod_texmaplist[i]
			maxsize = lod_maxsizelist[i]
			isbump = lod_isbumplist[i]
			usage = lod_usagelist[i]
			
			-- Ensure textures live under the project root dir
			rootTexmapPath = ""
			tokens = filterstring texmap "/"
			tokensAlt = filterstring texmap "\\" -- Projected light maps use the old style slashes
			correctTexpath = RsConfigGetTextureSourceDir()
			correctTexpathTokens = filterstring correctTexpath "/"

			if tokens.count > (correctTexpathTokens.count - 1) then (
				for i = 1 to correctTexpathTokens.count do (
					rootTexmapPath = rootTexmapPath + tokens[i] + "/"
				)
			)
			else if tokensAlt.count > (correctTexpathTokens.count - 1)then (
				for i = 1 to correctTexpathTokens.count do (
					rootTexmapPath = rootTexmapPath + tokensAlt[i] + "/"
				)
			)

			-- Hack: if we find a proxy texture, re-point it to the high-res location
			if ((RsLowercase (RsConfigGetProxyTextureSourceDir())) == (RsLowercase rootTexmapPath)) or (RsMakeSafeSlashes (RsLowercase (RsConfigGetProxyTextureSourceDir()))) == (RsMakeSafeSlashes (RsLowercase rootTexmapPath)) then (
				texmap = correctTexpath
				for i = correctTexpathTokens.count + 1 to tokens.count do (
					texmap = texmap + "/" + tokens[i]
				)
				print ("Fixed up proxy path to: " + texmap)
			)
			-- If a texture doesnt reside under the correct texture root then add to misplaced texture list
			else if (RsLowercase correctTexpath) != (RsLowercase rootTexmapPath) then (
				append RsMisplacedTexMaps texMap
				
				-- If our path check is enabled (default) then skip this 
				-- texture export.
				if ( RsMapExportTexturePathCheck and RsMapPedantic ) then
					continue
			)

			stripName = RsStripTexturePath(texmap)
			singleName = (filterstring texmap "+>")[1]

			outputFile = (RsConfigGetLodTexDir() + RsRemoveSpacesFromString(stripName) + ".dds")

			if isbump == false and normalonly == true then continue

			if isbump then (

				rexExportBumpTexture texmap outputFile maxsize
			) else (

				rexExportTexture texmap outputFile maxsize
			)
		)
		
		-- Throw warning about textures that do not reside under texture root
		if ( RsMisplacedTexMaps.count > 0 ) then 
			ReportMisplacedTextures()
		progressEnd()
	),

	--
	-- name: RemovedUnused 
	-- desc: delete any unused texture dictionaries from disk
	--
	fn RemovedUnused = (
		
		local sceneTxdList = GetAllSceneTXDs()
		local sceneTxds = #()
		RsGetFileTxdList sceneTxds
		
		txdFiles = getfiles (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/*.?td")
		
		for i = 1 to txdFiles.count do (
			
			txdFile = RsLowercase(RsRemovePathAndExtension(txdFiles[i]))

			if (finditem sceneTxdList txdFile == 0) and (finditem sceneTxds txdFile == 0) then (

				RsDeleteFiles txdFiles[i]
			)
		)
	),
	
	------------------------------------------------------------------
	-- Create an xml hierarchy of textures
	--
	-- in: txdlist: a list of all unique txds in the scene
	-- in: txdobjlist: a list of all unique objects in the scene
	-- in: outfilename: path and filename of output file
	------------------------------------------------------------------
	fn CreateListExportTxds txdlist txdobjlist outFileName = (
		
		--dirTarget = ("dirStream .. \"maps/" + RsMapName + "/\"")
		dirTargetFolder = (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/")
		dirBumpTargetFolder = dirTargetFolder + "bumplist" + "/"
		
		RsMakeSurePathExists dirBumpTargetFolder

		infoFileName = RsConfigGetBinDir() + "/info.rbs"
		infoFile = openfile infoFileName mode:"w+"
		
		format "isLOD = true\n" to:infoFile
		close infoFile
		
		-- init xml doc
		txdXml = XmlDocument()
		txdXml.init()
		
		txdXmlRoot = txdXml.createelement("txds")
		txdXml.document.AppendChild txdXmlRoot
		
		inputobjlist = #()
		
		-- Get scene objects without packed textures
		RsGetInputObjectList rootnode.children inputobjlist
		
		for i = 1 to txdlist.count do (
			bumpFileName = dirBumpTargetFolder + txdlist[i] + ".textures"
			bumpFile = openfile bumpFileName mode:"w+"

			progressUpdate (100.0*i/txdlist.count)
			if getProgressCancel() then throw("progress_cancel")

			txdname = txdlist[i]
			txdobjs = txdobjlist[i]

			texnamelist = #()

			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()

			RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist

			parenttexmaplist = #()
			RsGetTxdParentTextureList txdname parenttexmaplist

			for j = 1 to texmaplist.count do (

				if (usagelist[j] > 1) or (usagelist[j] < 0) then (

					texmapname = texmaplist[j]
					texmapname = RsRemoveSpacesFromString texmapname

					if finditem parenttexmaplist texmapname == 0 then (

						append texnamelist texmapname

						texname = RsRemoveSpacesFromString ((filterstring texmapname ".")[1])

						if isbumplist[j] then (

							format "% { Flags 1 }\n" texname to:bumpFile
						) else (

							format "%\n" texname to:bumpFile
						)
					)
				)
			)
						
			close bumpFile
			
			print("txdname: " + txdname)
			
			txdElem = txdXml.createelement(txdname)
			txdXmlRoot.AppendChild txdElem

			isLod = RsIsTXDUsedOnLod txdname 
			
			for item in texnamelist do (
				texElem = txdXml.createelement(item + ".dds")
				txdElem.AppendChild texElem
				
				if isLod == true then (
					attr = txdXml.createattribute "path"
					attr.value = RsRemoveFile(RsConfigGetLodTexDir())
					texElem.Attributes.append(attr)
				)
				
				
			)
			
			
			
			if isLod == true then (
				print (txdname + " is used on LODs")
				
				
				
				infoElem = txdXml.createelement(RsRemovePath(infoFileName))
				attr = txdXml.createattribute "path"
				attr.value = RsRemoveFile(infoFileName)
				infoElem.Attributes.append(attr)
				txdElem.AppendChild infoElem
			)
			
			
			bumpElem = txdXml.createElement(RsRemovePath(bumpfilename))
			attr = txdXml.createattribute "path"
			attr.value = RsRemoveFile(bumpfilename)
			bumpElem.Attributes.append(attr)
			txdElem.AppendChild bumpElem
			
			
		)

		progressEnd()

		
		result = txdXml.save outFileName
		rubyString = "txd_content = Pipeline::Content::MapTxd.new(\"" + outFileName+ "\", project, map_name, generic )\n"
		rubyString = rubyString + "component_group.add_child(txd_content)\n\n"
		append RsContentScriptQueue rubyString
		result
		
	),
	
	--
	-- name: CreateScriptExportTxds
	-- desc: create a script to generate the passed in texture dictionaries
	--
	fn CreateScriptExportTxds txdlist txdobjlist luaFileName = (

		dirTarget = ("dirStream .. \"maps/" + RsMapName + "/\"")
		dirTargetFolder = (RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/")
		dirBumpTargetFolder = dirTargetFolder + "bumplist" + "/"
		
		RsMakeSurePathExists dirBumpTargetFolder

		infoFileName = RsConfigGetBinDir() + "/info.rbs"
		infoFile = openfile infoFileName mode:"w+"
		
		format "isLOD = true\n" to:infoFile
		
		close infoFile

		luaFile = openfile luaFileName mode:"w+"

		format "require(\"%config.rbs\")\n" (RsConfigGetProjBinDir()) to:luaFile
		format "dirTarget = %\n" dirTarget to:luaFile

		inputobjlist = #()
		RsGetInputObjectList rootnode.children inputobjlist

		progressStart "creating txd script"

		for i = 1 to txdlist.count do (

			bumpFileName = dirBumpTargetFolder + txdlist[i] + ".textures"
			bumpFile = openfile bumpFileName mode:"w+"
				
			progressUpdate (100.0*i/txdlist.count)
			if getProgressCancel() then throw("progress_cancel")

			txdname = txdlist[i]
			txdobjs = txdobjlist[i]

			texnamelist = #()

			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()
			
			RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist

			parenttexmaplist = #()
			RsGetTxdParentTextureList txdname parenttexmaplist

			for j = 1 to texmaplist.count do (
			
				if (RsPackingForSingleTextures == false) or (usagelist[j] > 1) or (usagelist[j] < 0) then (
			
					texmapname = texmaplist[j]
					texmapname = RsRemoveSpacesFromString texmapname

					if finditem parenttexmaplist texmapname == 0 then (

						append texnamelist texmapname
						
						texname = RsRemoveSpacesFromString ((filterstring texmapname ".")[1])
						 
						if isbumplist[j] then (
						
							format "% { Flags 1 }\n" texname to:bumpFile
						) else (
						
							format "%\n" texname to:bumpFile
						)
					)
				)
			)
			
			close bumpFile

--			if texnamelist.count > 0 then (

				if RsIsTXDUsedOnLod txdname then (

					format "set_root_dir(dirLodTexture)\n" to:luaFile
				) else (

					format "set_root_dir(dirTexture)\n" to:luaFile
				)

				format "start_build()\n" to:luaFile

				for item in texnamelist do (

					format "load_texture(\"%.dds\")\n" item to:luaFile
				)

				if RsIsTXDUsedOnLod txdname then (

					format "load_texture(\"%\")\n" infoFileName to:luaFile
				)
				
				format "load_texture(\"%\")\n" bumpFileName to:luaFile

				tdline = "save_texture_dictionary(dirTarget .. \"" + txdname + "\")\n"
				
				format tdline to:luaFile
--			) else (
--				
--				delf = (dirTargetFolder + txdname + ".?td")
--				
--				RsDeleteFiles delf
--			)
		)

		progressEnd()

		close luaFile				
	),

	--
	-- name: SetupBlockTxdInfo
	-- desc:
	--
	fn SetupBlockTxdInfo = (
	
		modelsin = #()
		RsGetMapObjects rootnode.children modelsin
	
		objNames = #()
		objList = #()
		blockTxdList = #()

		idxBlockID = getattrindex "Gta Object" "BlockID"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxMemTexture = getattrindex "Gta Block" "MemTexture"

		-- clear all of the blocks details
		for obj in rootnode.children do (

			if classof obj == GtaBlock then (

				append objNames obj.name
				append objList obj
				append blockTxdList #()

				setattr obj idxMemTexture 0
			)
		)

		txdNameList = GetAllSceneTXDs()
		txdSizeList = #()

		for txd in txdNameList do (
		
			txdName = RsConfigGetMapStreamDir() + "maps/" + RsMapName + "/" + txd + ".itd"
			txdSize = getfilesize txdName
			
			append txdSizeList txdSize
		)

		-- go through the objects and add up the block info
		for model in modelsin do (

			if getattrclass model == "Gta Object" then (	

				blockId = getattr model idxBlockID
								
				id = finditem objNames blockId

				if id != 0 then (

					obj = objList[id]

					if classof model != XRefObject then (

						txd = RsLowercase(getattr model idxTxd)
						
						if finditem blockTxdList[id] txd == 0 then (
						
							append blockTxdList[id] txd
						)
					)
				)
			)
		)
		
		for i = 1 to objList.count do (
		
			block in objList[i]
			totalSize = 0
		
			for txd in blockTxdList[i] do (
			
				txdIdx = finditem txdNameList txd 
				
				if txdIdx != 0 then (
				
					totalSize = totalSize + txdSizeList[txdIdx]
				)
			)
			
			setattr objList[i] idxMemTexture totalSize
		)
	),
	
	--
	-- name: ExportTxds
	-- desc: Export out the passed in set of texture dictionaries
	--
	fn ExportTxds txdlist txdobjlist = (

		-- set up map export globals
		RemovedUnused()

		-- export the required textures to the output path (55 secs for all textures in manhat07 when there are none to export)
		
		ExportTextures txdlist txdobjlist

		-- create an export script (5 secs for manhat07)
		xmlFileName = RsConfigGetScriptDir() + RsMapName + "/txd.xml"
		RsMakeSurePathExists xmlFileName
		CreateListExportTxds txdlist txdobjlist xmlFileName
				
		SetupBlockTxdInfo()
		
		return true
	),

	--
	-- name: ExportAllTxds
	-- desc: export all the txds
	--
	fn ExportAllTxds children = (

		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects children modelsin exportGeometryCheck:true
		
		RsGetTxdList modelsin txdlist txdobjlist
		if ExportTxds txdlist txdobjlist == false then return false
	),

	--
	-- name: ExportSelectedTxds
	-- desc: Export selected geometry's texture dictionaries
	--
	fn ExportSelectedTxds = (

		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects selection modelsin exportGeometryCheck:true		
		
		RsGetTxdList modelsin txdlist txdobjlist
		if ExportTxds txdlist txdobjlist == false then return false
	),
	
	--
	-- name: ExportSelectedTxdsPreview
	-- desc: Export selected geometry's texture dictionaries to the preview folder
	--
	fn ExportSelectedTxdsPreview = (
	
		txdlist = #()
		txdobjlist = #()
		
		modelsin = #()
		RsGetMapObjects selection modelsin exportGeometryCheck:true				
		
		RsGetTxdList modelsin txdlist txdobjlist
		if ExportTxdsPreview txdlist txdobjlist == false then return false
	),

	--
	-- name: ExportSingleTXD
	-- desc: Export single texture dictionary from selection
	--
	fn ExportSingleTXD txdname = (
	
		txdlist = #()
		txdobjlist = #()
				
		append txdlist txdname
		append txdobjlist rootnode.children
	
		( ExportTxds txdlist txdobjlist )		
	),

	--
	-- name: ExportAll
	-- desc: Export all object's texture dictionaries
	--
	fn ExportAll = (

		for o in $objects do
		(
			if ( undefined != o.parent ) then
				continue
			if ( Container != classof o ) then
				continue
			if ( not o.IsOpen() ) then
				continue
		
			( ExportAllTxds o.children )
		)
	),
	
	--
	-- name: ExportSelected
	-- desc: Export selected object's texure dictionaries
	--
	fn ExportSelected = (
		
		( ExportSelectedTxds() )
	)

) -- RsMapTXD struct

--
-- struct: RsMapParentTXD
-- desc: Map Parent TXD export logic functions.
--
struct RsMapParentTXD
(

	--
	-- name: WriteSceneTxdList
	-- desc:
	--
	fn WriteSceneTxdList = (
		
		if RsMapName != "" then (

			sceneTxds = #()
			RsGetFileTxdList sceneTxds

			xmlFileName = RsConfigGetScriptDir() + RsMapName + "/ptxd.xml"
			
			-- init xml doc
			ptxdXml = XmlDocument()
			ptxdXml.init()


			ptxdXmlRoot = ptxdXml.createelement("ptxds")
			ptxdXml.document.AppendChild ptxdXmlRoot

			for txdname in sceneTxds do (

				txdElem = ptxdXml.createelement(txdname)
				ptxdXmlRoot.AppendChild(txdElem)

				texlist = #()
				RsGetFileTxdTextures txdname texlist

				for tex in texlist do (

					texElem = ptxdXml.createelement(tex + ".dds")
					txdElem.AppendChild(texElem)
					
				)

				for tex in texlist do (
					texElem = ptxdXml.createelement(tex + ".dds")
					attr = ptxdXml.createattribute "path"
					attr.value = RsConfigGetLodTexDir()
					texElem.Attributes.append(attr)
					txdElem.AppendChild(texElem)
					
					
				)

			)

			
			result = ptxdXml.save xmlFileName
			rubyString = "ptxd_content = Pipeline::Content::MapTxd.new(\"" + xmlFileName+ "\", project, map_name, generic )\n"
			rubyString = rubyString + "component_group.add_child(ptxd_content)\n\n"
			append RsContentScriptQueue rubyString
			
			
			
			
		)		
		
		
		
	),
	
	--
	-- name: DoSceneTxdExport
	-- desc:
	--
	fn DoSceneTxdExport = (
		WriteSceneTxdList()
	),
	
	--
	-- name: BuildGtxdImage
	-- desc:
	--
	fn BuildGtxdImage = (
		contenttype = bit.or RS_CONTENT_OUTPUT RS_CONTENT_PLATFORM
		rbscript= ""

		rubyFileName = RsConfigGetScriptDir() + "/gtxd/build_rpf.rb"
		RsMakeSurePathExists rubyFileName
		deletefile rubyFileName
		rubyFile = createfile rubyFileName


		RBGenDynamicRPFResourcingScript rubyFile (RsConfigGetProjectName()) "gtxd" "maps/gtxd"

		close rubyFile

		cmdline = "ruby" + " "	
		cmdline = cmdline + rubyFileName

		if doscommand cmdline != 0 then (

			messagebox ("failed to make gtxd RPF: " + rubyFileName)
			return false
		)
		true	
	),

	
	--
	-- name: WriteGlobalTxdList
	-- desc:
	--
	fn WriteGlobalTxdList = (
		RsLoadParentFileList "global"
		
		RsMakeSurePathExists(RsConfigGetStreamDir() + "gtxd")
		RsMakeSurePathExists(RsConfigGetStreamDir() + "maps/gtxd")

		RsDeleteFiles (RsConfigGetStreamDir() + "maps/gtxd/*.*")

		xmlFileName = RsConfigGetScriptDir() + "/gtxd.xml"

		sceneTxds = #()
		RsGetFileTxdList sceneTxds
		
		-- init xml doc
		gtxdXml = XmlDocument()
		gtxdXml.init()

		gtxdXmlRoot = gtxdXml.createelement("gtxds")
		gtxdXml.document.AppendChild gtxdXmlRoot

		for txdname in sceneTxds do (
			txdElem = gtxdXml.createelement(txdname)
			gtxdXmlRoot.AppendChild(txdElem)

			texlist = #()
			RsGetFileTxdTextures txdname texlist

			for tex in texlist do (

				texElem = gtxdXml.createelement(tex + ".dds")
				txdElem.AppendChild(texElem)

			)

			for tex in texlist do (
				texElem = gtxdXml.createelement(tex + ".dds")
				attr = gtxdXml.createattribute "path"
				attr.value = RsConfigGetLodTexDir()
				texElem.Attributes.append(attr)
				txdElem.AppendChild(texElem)


			)


		)
		gtxdXml.save xmlFileName
		rubyString = "gtxd_content = Pipeline::Content::MapTxd.new(\"" + xmlFileName+ "\", project, map_name, false )\n"
		rubyString = rubyString + "component_group.add_child(gtxd_content)\n\n"
		
		rubyFileName = (RsConfigGetScriptDir()  + "gtxd_resourcing.rb")
		rubyMapResourceFile = createfile rubyFileName
		RBGenGtxdResourcingScriptPre rubyMapResourceFile
		format rubyString to:rubyMapResourceFile
		RBGenResourcingScriptPost rubyMapResourceFile
		
		close rubyMapResourceFile
		cmdline = "ruby " + rubyFileName

		if doscommand cmdline != 0 then (

			messagebox "Failed to build gtxd resource"
			return false
		)

		idefilename = RsConfigGetProjRootDir() + "build/dev/common/data/gtxd.ide"
		writefile = openfile idefilename mode:"w"

		if writefile != undefined then (

			format "txdp\n" to:writefile

			for item in RsParentFileList do (
				
				if item.type == 4 and item.parentname != "root" then (
					
					format "%,%\n" item.name item.parentname to:writefile
				) 
			)		

			format "end\n" to:writefile

			close writefile
		) else (

			messagebox (idefilename + " is read only, please check it out of source control")
		)
		
		BuildGtxdImage()
	),
	
	--
	-- name: DoGlobalTxdExport
	-- desc:
	--
	fn DoGlobalTxdExport = (
	
		WriteGlobalTxdList()
	)
	
) -- RsMapParentTXD struct

-- pipeline/export/maps/maptxd.ms
