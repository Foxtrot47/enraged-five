--
-- File:: pipeline/export/maps/vector_map.ms
-- Description:: Level vector map export, input for Map Browser application.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 18 June 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: ExportVectorMapDataSpline
--
--
fn ExportVectorMapDataSpline xmldoc xmlroot splineObj = (

	local nsplines = ( numSplines splineObj )
	local xmlelem = xmldoc.CreateElement( "section" )
	xmlelem.SetAttribute "name" splineObj.name
	xmlelem.SetAttribute "spline_count" (nsplines as String)
	
	local xmlelemcolour = xmldoc.CreateElement( "colour" )
	xmlelemcolour.SetAttribute "r" ((splineObj.wireColor.r / 255.0) as String)
	xmlelemcolour.SetAttribute "g" ((splineObj.wireColor.g / 255.0) as String)
	xmlelemcolour.SetAttribute "b" ((splineObj.wireColor.b / 255.0) as String)
	xmlelem.AppendChild( xmlelemcolour )
	
	for s = 1 to nsplines do
	(
		local nknots = ( numKnots splineObj s )
		local xmlelemspline = xmldoc.CreateElement( "spline" )
		xmlelemspline.SetAttribute "point_count" (nknots as String)

		for n = 1 to nknots do
		(
			local p = ( getKnotPoint splineObj s n )
			local xmlelempoint = xmldoc.CreateElement( "point" )
			xmlelempoint.SetAttribute "x" (p.x as String)
			xmlelempoint.SetAttribute "y" (p.y as String)
			xmlelempoint.SetAttribute "z" (p.z as String)
			
			xmlelemspline.AppendChild( xmlelempoint )
		)
		
		xmlelem.AppendChild( xmlelemspline )
	)
	
	xmlelem
)

--
-- name: ExportVectorMapData
-- desc: Export vector map data from current scene to a specified XML file.
--
fn ExportVectorMapData filename = (
	
	local doc = XmlDocument()
	doc.init()
	xmldoc = doc.document
	
	xmlroot = xmldoc.AppendChild( xmldoc.CreateElement( "vector_map" ) )
	for o in rootNode.children do
	(
		if ( undefined != o and Line == classof o ) then
		(
			format "Exporting: %\n" o.name
			xmlroot.AppendChild( ExportVectorMapDataSpline xmldoc xmlroot o )
		)
	)
	
	format "Writing %...\n" filename
	xmldoc.Save( filename )
	true
)

-- pipeline/export/maps/vector_map.ms
