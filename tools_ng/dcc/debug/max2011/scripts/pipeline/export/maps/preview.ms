--
-- File:: pipeline/export/maps/preview.ms
-- Description:: Map exporter preview implementation.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------
global RsMapPreviewEmpty	= 0x00000000
global RsMapPreviewGeometry = 0x00000001
global RsMapPreviewTextures = 0x00000002
global RsMapPreviewIDEIPL	= 0x00000004
global RsMapPreviewAll		= 0x7FFFFFFF	-- Note: SIGNED 32-bit Integer

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- struct: RsMapPreview
-- desc: Map preview namespace struct.
--
struct RsMapPreview
(
	-- PreviewMask determines what is exported using the bit constants
	-- defined above.
	PreviewMask = ( bit.or RsMapPreviewGeometry RsMapPreviewTextures ),

	--
	-- name: Export
	-- desc: Map preview export function, uses RsMapPreviewConfig global for 
	--       configuration.
	--
	fn Export = (

		if ( RsMapPreviewEmpty == PreviewMask ) then
		(
			MessageBox "No pipeline stages set for Map Preview.  Aborting."
			return ( false )
		)

		local exportselobjs = RsMapGetMapContainers()
		for exportsel in exportselobjs do
		(
			if ( not exportsel.is_exportable() ) then
				continue
		
			if ( false == ( RsPreExport exportsel docheck:false previewMask:PreviewMask ) ) then
				return false
			
			if ( ( bit.and PreviewMask RsMapPreviewGeometry ) > 0 ) then
			(
				local mapgeom = RsMapGeometry()
				mapgeom.ExportMap exportsel.objects
			)
			if ( ( bit.and PreviewMask RsMapPreviewTextures ) > 0 ) then
			(
				local maptxd = RsMapTXD()
				maptxd.ExportSelected()
			)
			if ( ( bit.and PreviewMask RsMapPreviewIDEIPL ) > 0 ) then
			(
				RsMapExportIDEIPL()
			)
			RsPostExport exportsel patchExport:true
		)
		
		true
	),
	
	--
	-- name: ClearPreviewFolder
	-- desc: Delete all files in the preview folder.
	--
	fn ClearPreviewFolder = (
		RsDeleteFiles( RsConfigGetPatchDir() + "*.*" )
	)

) -- RsMapPreview struct

-- pipeline/export/maps/preview.ms
