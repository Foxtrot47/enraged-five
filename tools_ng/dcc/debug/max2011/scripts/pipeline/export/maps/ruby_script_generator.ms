--
-- File:: pipeline/export/maps/ruby_script_generator.ms
-- Description:: Map exporter Ruby script generator.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 1 February 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsMapRunScript
-- desc: Run a Ruby script with optional arguments.
--
fn RsMapRunScript rubyFileName args:"" = (

		local commandLine = stringStream ""
		format "ruby \"%\" %" rubyFilename args to:commandLine

		DOSCommand commandLine	
)

--
-- name: RBMapInfoScriptFilename
-- desc: Return the Ruby filename of the map config script
--
fn RBMapInfoScriptFilename mapname = (
	( RsConfigGetScriptDir() + mapname + "/map_config_seek.rb"	)
)

--
-- name: RBMapInfoScriptOutput
-- desc: Return the filename of the map config script output XML
--
fn RBMapInfoScriptOutputFilename mapname = (
	( RsConfigGetScriptDir() + mapname + "/map_config.xml" )
)

--
-- name: RBMapGenerateInfoScript
-- desc: Generate and run the map config seek script to create the XML config file.
--
fn RBMapGenerateInfoScript mapname = (
	local outfilename = ( RBMapInfoScriptOutputFilename mapname )
	local rubyfilename = ( RBMapInfoScriptFilename mapname )
	local projname = RsConfigGetProjectName()
	RsMakeSurePathExists rubyFileName
	
	rubyFile = openfile rubyFileName mode:"w"
	
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/gui/exception_dialog'\n\n" to:rubyFile
	
	format "begin\n" to:rubyFile
	format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "\tproject.load_content(nil, Pipeline::Project::ContentType::MAPS | Pipeline::Project::ContentType::OUTPUT)\n" to:rubyFile
	format "\tputs 'Validating map section'\n" to:rubyFile 
	format "\tcontent_list = project.content.find_by_script( \"content.name == '%' and content.xml_type == 'map'\" )\n\n" mapname to:rubyFile

	format "\tputs 'Writing out map attributes...'\n" to:rubyFile 
	format "\tfilename = \"%\"\n" outfilename to:rubyFile
	format "\tFile.delete(filename) if File.exist?(filename)\n\n" to:rubyFile
	format "\tenv = Pipeline::Environment.new()\n" to:rubyFile
	format "\tproject.fill_env( env, false )\n\n" to:rubyFile
	format "\tdoc = REXML::Document.new()\n" to:rubyFile
	format "\troot_node = doc.add_element(\"%\")\n" mapname to:rubyFile
	format "\tif content_list.size == 1 then\n" to:rubyFile
	format "\t\tcontent = content_list[0]\n" to:rubyFile
	format "\t\tpath = env.subst( content.path )\n\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"path\",path)\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"ide\",content.exportdata.to_s)\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"ipl\",content.exportinstances.to_s)\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"prefix\",content.prefix.to_s)\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"pedantic\", content.pedantic.to_s)\n" to:rubyFile
	format "\t\troot_node.add_attribute(\"level\", content.level)\n" to:rubyFile
	format "\t\n\n" to:rubyFile
	format "\t\tcontent_list = project.content.find_by_script( \"content.name == '%' and content.xml_type == 'maprpf'\" )\n" mapname to:rubyFile
	format "\t\tif ( 1 == content_list.size ) then\n" to:rubyFile
	format "\t\t\tmaprpf_content = content_list[0]\n" to:rubyFile
	format "\t\t\troot_node.add_attribute( 'ide_filename', maprpf_content.children[1].filename ) if ( content.exportdefinitions )\n" to:rubyFile
	format "\t\t\troot_node.add_attribute( 'ipl_filename', maprpf_content.children[2].filename ) if ( content.exportinstances and content.exportdefinitions )\n" to:rubyFile
	format "\t\t\troot_node.add_attribute( 'ipl_filename', maprpf_content.children[1].filename ) if ( content.exportinstances and not content.exportdefinitions )\n" to:rubyFile
	format "\t\tend\n" to:rubyFile
	format "\tend\n\n" to:rubyFile

	format "\tputs 'Writing out all map prefixes...'\n" to:rubyFile 
	format "\tcontent_list = project.content.find_by_script( \"content.xml_type == 'map'\" )\n" to:rubyFile
	format "\tprefix_node = root_node.add_element( 'map_prefixes' )\n" to:rubyFile
	format "\tcontent_list.each do |content|\n" to:rubyFile
	format "\t\tnext if ( content.prefix.empty? )\n\n" to:rubyFile
	format "\t\tmap_node = prefix_node.add_element( 'map' )\n" to:rubyFile
	format "\t\tmap_node.add_attribute( 'name', content.name )\n" to:rubyFile
	format "\t\tmap_node.add_attribute( 'path', env.subst( content.path ) )\n" to:rubyFile
	format "\t\tmap_node.add_attribute( 'prefix', content.prefix )\n" to:rubyFile
	format "\tend\n\n" to:rubyFile

	format "\tFile.open(filename,\"w+\") do |file|\n" to:rubyFile
	format "\t\tfmt = REXML::Formatters::Pretty.new()\n" to:rubyFile
	format "\t\tfmt.write(doc,file)\n" to:rubyFile
	format "\tend\n" to:rubyFile
	format "\tputs 'Complete'\n" to:rubyFile 

	format "rescue Exception => ex\n\n" to:rubyFile
	format "\tPipeline::GUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
	format "end\n\n" to:rubyFile
	
	close rubyFile
	
	( RsMapRunScript rubyfilename )
)

-- pipeline/export/maps/ruby_script_generator.ms
