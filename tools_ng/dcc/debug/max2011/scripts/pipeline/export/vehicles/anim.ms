--
-- File:: pipeline/export/vehicles/anim.ms
-- Description:: Animated vehicle exporter functions.
--
-- Author:: Adam Munson <adam.munson@rockstarnorth.com>
-- Date:: 08 July 2010
--
-- Based off of the export\weapon\anim.ms script
--
-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/util/rexreport.ms"
filein "pipeline/export/models/ruby_script_generator.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------
--
-- name: RsVehicleIsValidBone
-- desc: Determine whether a particular node is a valid vehicle bone.
--
fn RsVehicleIsValidBone rootbone = (

	local retval = true

	if ((classof rootbone != Point) and 
	(classof rootbone != BoneGeometry) and 
	(classof rootbone != Dummy)) do (
		
		retval = false
	)	
	retval
)

--
-- name: RsVehicleIsValidBone
-- desc: Determine whether a particular bone is animated.
--
fn RsVehicleIsBoneAnimated rootbone = (

	local retval = true
	
	if ((rootbone.pos.isAnimated == false) and 
		(rootbone.rotation.isAnimated == false) and
		(rootbone.scale.isAnimated == false)) do (
		
		retval = false
	)	
	retval
)

--
-- name: RsFindVehicleBonesRecursive
-- desc: Recursively finds all bones in the vehicle, also filtered by 
--	 if they are animated (e.g. if saving, only selects animated bones)
--
fn RsFindVehicleBonesRecursive obj boneList animatedFilter:false = (

	if (RsVehicleIsValidBone obj == true) do ( 

		if (animatedFilter == true ) then (
		
			if (RsVehicleIsBoneAnimated obj == true) do (
				
				append boneList obj				
			)
		)
		else (
		
			append boneList obj
		)
	)
	for child in obj.children do (
	
		RsFindVehicleBonesRecursive child boneList animatedFilter:animatedFilter
	)	
)

--
-- name: RsFindVehicleBonesRecursive
-- desc: Finds all bones for the given vehicle and selects them
--
fn RsFindVehicleBones vehicle animatedFilter:false = (

	local boneList = #()
	RsFindVehicleBonesRecursive vehicle boneList animatedFilter:animatedFilter
	select boneList
	boneList
)

--
-- name: RsVehicleAnimSave
-- desc: Save a vehicle animation out to .xaf
--
fn RsVehicleAnimSave animFilename vehicle = (

	-- Only select bones that are animated, to save to .xaf
	bonesList = RsFindVehicleBones vehicle animatedFilter:true
	attrNames = #()
	attrValues = #()
	LoadSaveAnimation.setUpAnimsForSave bonesList animatedTracks:true includeContraints:true keyable:true
	result = LoadSaveAnimation.saveAnimation animFilename bonesList attrNames attrValues animatedTracks:true includeConstraints:true keyableTracks:true saveSegment:true segInterval:animationRange segKeyPerFrame:true

	select vehicle
)

--
-- name: RsVehicleAnimLoad
-- desc: Load a vehicle animation .xaf file
--
fn RsVehicleAnimLoad animFilename vehicle = (
	
	-- Select all bones when loading an anim (animatedFilter not used)
	local bonesList = RsFindVehicleBones vehicle animatedFilter:false
	LoadSaveAnimation.setUpAnimsForLoad bonesList
	LoadSaveAnimation.loadAnimation animFilename bonesList

	select vehicle
)

--
-- name: RsGetVehicleAnimationList
-- desc: Returns a list of any animations the vehicle max file has
--
fn RsGetVehicleAnimationList filename filteredList = (

	animDir = maxFilePath + "anim"
	animFilter = animDir + "\\*.xaf"
	animList = getFiles animFilter
	filename = RsRemoveExtension filename
	pattern = "*\\" + filename + "*xaf"
	
	
	for anim in animList do (
	
		--Check if the animation filename contains the max file name and then any
		--characters after it (the actual animation name)
		if (matchpattern anim pattern:pattern == true) do (
			
			append filteredList anim
		)
	)
)

--
-- name: RsVehicleAnimExport
-- desc: Exports vehicle animation data using RexMax
--
fn RsVehicleAnimExport animFilename vehicleRoot = (

	local vehicleName 	= ( RsRemoveExtension maxfilename )
	local vehicleFile 	= ( RsVehicleSettings.GetScriptDir() + vehicleName )
	local animName 		= ( RsRemovePathAndExtension animFilename )
	local animPath 		= ( RsRemoveFilename animFilename )
	local clipFile 		= ( RsRemoveExtension animFilename )
	local bonevehicleRoot	= vehicleRoot
	
	RsMakeSurePathExists animPath

	rexReset()
	local graphRoot = rexGetRoot()

	-------------------------------------------------------------------------
	-- Rex Export Animation
	-------------------------------------------------------------------------
	local animation = rexAddChild graphRoot animName "Animation"
	rexSetPropertyValue animation "OutputPath" animPath
	rexSetPropertyValue animation "ChannelsToExport" "(translateX;translateY;translateZ;)(rotateX;rotateY;rotateZ;)"
	rexSetPropertyValue animation "AnimMoverTrack" "false"
	rexSetPropertyValue animation "AnimAuthoredOrient" "true"
	rexSetPropertyValue animation "AnimCompressionTolerance" "0.0005"
	rexSetPropertyValue animation "AnimLocalSpace" "true"
	rexSetPropertyValue animation "AnimExportSelected" "true"
	
	clearSelection()
	local selset = RsFindVehicleBones vehicleRoot animatedFilter:true
	select selset

	rexSetNodes animation

	RsRexExportWithReport()
	clearSelection()

	-------------------------------------------------------------------------
	-- Export Clip
	-------------------------------------------------------------------------
	format "Saving clip %\n" clipFile
		
	clipEditInst = clipEditor clipFile
	clipsave clipFile bonevehicleRoot clipEditInst
	
	-- Reselect vehicle dummy again
	select vehicleRoot
	true
)

--
-- name: RsVehicleAnimExportResource
-- desc: Resource the vehicle anim data using RageBuilder.
--
fn RsVehicleAnimExportResource vehicle= 
(
	local vehicleName = ( RsRemoveExtension maxfilename )

	local rbFileName = RsVehicleSettings.GetScriptDir() + vehicleName + "_anim_resourcing.rb"
	RsMakeSurePathExists rbFileName
	
	local rbFile = openfile rbFileName mode:"w+"
	
	RBGenVehicleResourcingScriptPre rbFile vehicle false
	RBGenVehicleResourcingScriptAnim rbFile
	RBGenVehicleResourcingScriptPost rbFile
	
	close rbFile

	-- Run the Ruby resourcing script for the animation data.	
	RsVehicleScriptGenerator.RunScript rbFileName		

	true
)