--
-- File:: pipeline/export/vehicles/ruby_script_generator.ms
-- Description:: Vehicle exporter Ruby script generation functions.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 2 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------




--
-- name: RBGenVehicleResourcingScriptPre
-- desc: Create vehicle resourcing script common start functionality.
--
fn RBGenVehicleResourcingScriptPre rubyFile vehicle load_content = (

	local vehicle_name = ""
	local projname = RsConfigGetProjectName()
	
	format "vehicle % " vehicle
	if ( undefined != vehicle ) do (

		-- Get rid of _skel part if skel was selected by user
		if (matchpattern vehicle.name pattern:"*_skel" == true ) then (
		
			format "pattern matched"
			vehicle_name = substring vehicle.name 1 (vehicle.name.count - 5)
		)
		else (
		
			vehicle_name = vehicle.name
		)
		format "vehicle name % " vehicle_name		
	)
	
	format "require 'pipeline/config/projects'\n" to:rubyFile
	format "require 'pipeline/config/project'\n" to:rubyFile
	format "require 'pipeline/content/content_core'\n" to:rubyFile
	format "require 'pipeline/os/file'\n" to:rubyFile
	format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
	format "require 'pipeline/resourcing/convert'\n" to:rubyFile
	format "require 'pipeline/util/string'\n" to:rubyFile
	format "require 'pipeline/resourcing/util'\n" to:rubyFile
	format "include Pipeline\n\n" to:rubyFile

	format "project = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
	format "project.load_config( )\n" to:rubyFile
	if ( load_content ) then
		format "project.load_content( nil, Project::ContentType::OUTPUT | Project::ContentType::PLATFORM )\n" to:rubyFile
	format "branch = project.branches[project.default_branch]\n\n" to:rubyFile
	
	if ( undefined != vehicle ) then
		format "vehicle_name = '%'\n\n" vehicle_name to:rubyFile
	
	format "stream_dir = '%'\n" (RsVehicleSettings.GetStreamDir()) to:rubyFile
	format "Pipeline::LogSystem::instance().rootlog.info( \"Stream directory: #{stream_dir}.\" )\n" to:rubyFile
	if ( undefined != vehicle ) then
	(
		format "vehicle_stream_dir = '%'\n" (RsVehicleSettings.GetVehicleStreamDir vehicle) to:rubyFile
		format "Pipeline::LogSystem::instance().rootlog.info( \"Vehicle stream directory: #{vehicle_stream_dir}.\" )\n" to:rubyFile
	)
	format "script_dir = Pipeline::OS::Path::combine( project.netstream, 'mapscript', 'vehicles' )\n" to:rubyFile
	format "Pipeline::LogSystem::instance().rootlog.info( \"Script directory: #{script_dir}.\" )\n" to:rubyFile

	format "component_group = Pipeline::Content::Group.new( 'vehicle_content_group' )\n\n" to:rubyFile
)


--
-- name: RBGenVehicleResourcingScriptModel
-- desc: Create vehicle model resourcing Ruby script body.
--
fn RBGenVehicleResourcingScriptModel rubyFile = (

	format "ift = Content::RPF.new( vehicle_name, stream_dir, 'ift', branch.ind_target )\n" to:rubyFile
	format "all_files = OS::FindEx::find_files( OS::Path::combine( vehicle_stream_dir, '*.*' ) )\n" to:rubyFile
	format "rbs_files = OS::FindEx::find_files( OS::Path::combine( stream_dir, '*.rbs' ) )\n" to:rubyFile
	format "(all_files+rbs_files).each do |file|\n" to:rubyFile
	format "\tnext if ( file.ends_with( '.dds' ) )\n\n" to:rubyFile

	format "\tputs \"Adding: #{file}\"\n" to:rubyFile
	format "\tift.inputs << Content::File::from_filename( file )\n" to:rubyFile
	format "end\n" to:rubyFile
	format "itd = Content::RPF.new( vehicle_name, stream_dir, 'itd', branch.ind_target )\n" to:rubyFile
	format "dds_files = OS::FindEx::find_files( OS::Path::combine( vehicle_stream_dir, '*.dds' ) )\n" to:rubyFile
	format "dds_files.each do |file|\n" to:rubyFile
	format "\tputs \"Adding: #{file}\"\n" to:rubyFile
	format "\titd.inputs << Content::File::from_filename( file )\n" to:rubyFile
	format "end\n\n" to:rubyFile
	
	format "component_group.children << ift\n" to:rubyFile
	format "component_group.children << itd\n" to:rubyFile
)


--
-- name: RBGenVehicleResourcingScriptTxd
-- desc: Create vehicle shared txd resourcing Ruby script body.
--
fn RBGenVehicleResourcingScriptTxd rubyFile outname texture_list textures_file = (
	
	format "itd = Content::RPF.new( '%', stream_dir, 'itd', branch.ind_target )\n" outname to:rubyFile
	for texture in texture_list do (
		format "puts \"Adding: #{OS::Path::get_filename( '%' )}\"\n" texture to:rubyFile
		format "itd.inputs << Content::File::from_filename( '%' )\n" texture to:rubyFile
	)
	format "itd.inputs << Content::File::from_filename( '%' )\n" textures_file to:rubyFile
	format "component_group.children << itd\n\n" to:rubyFile
)

--
-- name: RBGenVehicleResourcingScriptAnim
-- desc:
--
fn RBGenVehicleResourcingScriptAnim rubyFile = (

	format "icd = Content::RPF.new( vehicle_name, stream_dir, 'icd', branch.ind_target )\n" to:rubyFile
	format "anim_files = OS::FindEx::find_files( OS::Path::combine( '%', '*.anim' ) )\n" (maxfilepath + "anim") to:rubyFile
	format "anim_files.each do |file|\n" to:rubyFile
	format "\tputs \"Adding Anim: #{file}\"\n" to:rubyFile
	format "\ticd.inputs << Content::File::from_filename( file )\n" to:rubyFile
	format "end\n\n" to:rubyFile
	format "clip_files = OS::FindEx::find_files( OS::Path::combine( '%', '*.clip' ) )\n" (maxfilepath + "anim") to:rubyFile
	format "clip_files.each do |file|\n" to:rubyFile
	format "\tputs \"Adding Clip: #{file}\"\n" to:rubyFile
	format "\ticd.inputs << Content::File::from_filename( file )\n" to:rubyFile
	format "end\n\n" to:rubyFile

	format "component_group.children << icd\n" to:rubyFile
)

--
-- name: RBGenVehicleResourcingScriptPost
-- desc: Builds the packfiles for this vehicle 
--
fn RBGenVehicleResourcingScriptPost rubyFile = (

	format "if component_group.children.size > 0\n" to:rubyFile
	format "\tconvert = Pipeline::ConvertSystem.instance()\n" to:rubyFile
	format "\tconvert.setup( project, nil, true, true, false)\n" to:rubyFile
	
	format "\tcomponent_group.children.each do | content_group |\n" to:rubyFile
	format "\t\tif content_group.methods.include?( 'build' )\n" to:rubyFile
	format "\t\t\tcontent_group.build()\n" to:rubyFile 
	format "\t\tend\n" to:rubyFile
	format "\tend\n" to:rubyFile
	
	format "\tconvert.build(component_group)\n" to:rubyFile 
	
	format "end\n\n" to:rubyFile 
)

--
-- struct: RsVehicleScriptGeneratorStruct
-- desc: Vehicle Ruby script generator container for functions.
--
struct RsVehicleScriptGeneratorStruct
(
	
	--
	-- name: RunScript
	-- desc: Run a Ruby script with optional arguments.
	--
	fn RunScript rubyFileName args:"" = (
	
			local commandLine = stringStream ""
			format "ruby \"%\" %" rubyFilename args to:commandLine
			
			DOSCommand commandLine	
	),

	--
	-- name: GenerateConfigXML
	-- desc: Create and run a Ruby script that generates vehicle configuration
	--       data from the currently selected project's content tree.  This is
	--       much like the map config seek XML file.
	-- refs: RsVehicleSetupGlobals function (pipeline/export/vehicles/globals.ms)
	--
	fn GenerateConfigXML rubyFileName configFileName = (
	
		RsMakeSurePathExists rubyFileName
		RsMakeSurePathExists configFileName
		local projectName = RsConfigGetProjectName()
		local rubyFile = createFile rubyFileName
		
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/os/path'\n" to:rubyFile
		format "include Pipeline\n" to:rubyFile
		format "require 'REXML/document'\n" to:rubyFile
		format "\n" to:rubyFile
		format "config = Pipeline::Config::instance()\n" to:rubyFile
		format "project = config.projects['%']\n" projectName to:rubyFile
		format "project.load_content( nil, Project::ContentType::OUTPUT )\n" to:rubyFile
		format "\n" to:rubyFile
		format "vehicle_rpfs = project.content.find_by_script( \"'vehicles' == content.name and 'vehiclesrpf' == content.xml_type\" )\n" to:rubyFile
		format "filename = '%'\n" configFileName to:rubyFile
		format "File::delete( filename ) if ( File::exists?( filename ) )\n" to:rubyFile
		format "\n"
		format "env = Environment::new()\n" to:rubyFile
		format "project.fill_env( env, false )\n" to:rubyFile
		format "\n"
		format "doc = REXML::Document::new()\n" to:rubyFile
		format "doc << REXML::XMLDecl::new()\n" to:rubyFile
		format "root_node = doc.add_element( 'vehicle_exporter' )\n" to:rubyFile
		format "vehicles_node = root_node.add_element( 'vehicles' )\n" to:rubyFile
		format "vehicle_rpfs.each do |vehicle_rpf|\n" to:rubyFile
		format "\n" to:rubyFile
		format "\trpf_node = vehicles_node.add_element( 'vehicle_rpf' )\n" to:rubyFile
		format "\trpf_node.add_attribute( 'filename', vehicle_rpf.filename )\n" to:rubyFile
		format "\trpf_node.add_attribute( 'sharedtxd', vehicle_rpf.sharedtxd.to_s )\n" to:rubyFile
		format "\trpf_node.add_attribute( 'sharedtxdinput', vehicle_rpf.sharedtxdinput.to_s )\n" to:rubyFile
		format "\trpf_node.add_attribute( 'friendlyname', vehicle_rpf.friendly.to_s )\n" to:rubyFile
		format "end\n" to:rubyFile
		format "\n" to:rubyFile
		format "File::open( filename, 'w' ) do |fp|\n" to:rubyFile
		format "\tfmt = REXML::Formatters::Pretty::new()\n" to:rubyFile
		format "\tfmt.write( doc, fp )\n" to:rubyFile
		format "end\n" to:rubyFile
		
		close rubyFile
		RunScript rubyFileName
	),

	--
	-- name: BuildImage
	-- desc: Create and run the vehicle RPF resourcing Ruby script body.
	--
	fn BuildRPF rubyFileName rebuild:false = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rpf_filename = RsVehicleRPFs[RsVehicleRPFIndex].filename
		local rubyFile = createFile rubyFileName

		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_extract'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\trpf_filename = '%'\n" (RsVehicleRPFs[RsVehicleRPFIndex].filename) to:rubyFile 
		format "\trebuild = %\n" rebuild to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
		format "\tstream_dir = '%'\n" ( RsVehicleSettings.GetStreamDir() ) to:rubyFile
		format "\tscript_dir = '%'\n" ( RsVehicleSettings.GetScriptDir() ) to:rubyFile
		format "\tPipeline::LogSystem::instance().rootlog.info( \"Script directory: #{script_dir}.\" )\n" to:rubyFile
		format "\timage_files = OS::FindEx::find_files( OS::Path::combine( stream_dir, '*.i*' ) )\n" to:rubyFile
		format "\tr = RageUtils.new( project, branch.name )\n" to:rubyFile
		format "\tr.pack.start_uncompressed()\n\n" to:rubyFile
		format "\timage_files.each do |filename|\n" to:rubyFile
		format "\t\tputs \"Adding file: #{filename}\"\n" to:rubyFile
		format "\t\tr.pack.add( filename, OS::Path::get_filename( filename ) )\n" to:rubyFile
		format "\tend\n\n" to:rubyFile
		format "\tr.pack.save( rpf_filename )\n" to:rubyFile
		format "\tr.pack.close( )\n" to:rubyFile

		format "\tProjectUtil::data_convert_file( rpf_filename, rebuild )\n\n" to:rubyFile
		format "rescue Exception => ex\n" to:rubyFile
		format "\tputs \"Unhandled exception: #{ex.message}\"\n" to:rubyFile
		format "\tputs ex.backtrace.join(\"\\n\")\n" to:rubyFile
		format "\tputs 'Press enter or close this window to continue...'\n" to:rubyFile
		format "end\n\n" to:rubyFile
		
		close rubyFile
		RunScript rubyFileName
	),


	--
	-- name: ExtractRPF
	-- desc: Create and run the vehicle RPF extraction script.
	--
	fn ExtractRPF rubyFileName = (

		RsMakeSurePathExists rubyFileName
		local projectName = RsConfigGetProjectName()
		local rpf_filename = RsVehicleRPFs[RsVehicleRPFIndex].filename
		local rubyFile = createFile rubyFileName
		
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_extract'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n" to:rubyFile
		format "\trpf_filename = '%'\n" (RsVehicleRPFs[RsVehicleRPFIndex].filename) to:rubyFile 
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projectName to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n\n" to:rubyFile
		format "\tstream_dir = '%'\n" ( RsVehicleSettings.GetStreamDir() ) to:rubyFile
		format "\tr = RageUtils.new( project, branch.name )\n" to:rubyFile
		format "\tProjectUtil::data_extract_rpf( r, rpf_filename, stream_dir, true ) do |filename|\n" to:rubyFile
		format "\t\tputs \"Extracting: #{filename}\"\n" to:rubyFile
		format "\tend\n" to:rubyFile
		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog.show_dialog( ex )\n" to:rubyFile
		format "end\n" to:rubyFile
		
		close rubyFile
		RunScript rubyFileName
	)

)

-- Create instances of our script generator struct.
global RsVehicleScriptGenerator = RsVehicleScriptGeneratorStruct()

-- pipeline/export/vehicles/ruby_script_generator.ms
