--
-- File:: pipeline/export/vehicles/textures.ms
-- Description:: Texture functions for vehicle exporter
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/util/file.ms"
filein "pipeline/util/rb_script_generator.ms"
filein "pipeline/util/string.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- struct: RsVehicleTextureStruct
-- desc: Texture export functions for vehicle exporter
--
struct RsVehicleTextureStruct
(

	--
	-- name: Export
	-- desc: Export the textures require for specific txd's.
	--
	fn Export texmaplist isbumplist exportDir objectName = (
		-- First check that the textures have valid dimensions
		if (RsCheckTexDimension texmaplist) then (
			local bumpFileName = exportDir + objectName + ".textures"
			local bumpFile = openfile bumpFileName mode:"w+"

			for i = 1 to texmaplist.count do (

				texmap = RsLowercase(texmaplist[i])
				isbump = isbumplist[i]

				if findstring texmap ".dds" != undefined then (

					isbump = false
				)

				stripName = RsStripTexturePath(texmap)
				singleName = (filterstring texmap "+")[1]

				foundFiles = getfiles singleName

				if foundFiles.count > 0 then (

					outputFile = (exportDir + stripName + ".dds")

					texname = RsRemoveSpacesFromString ((filterstring stripName ".")[1])

					if isbump then (

						format "% { Flags 1 }\n" texname to:bumpFile
						rexExportBumpTexture texmap outputFile
					) else (

						format "%\n" texname to:bumpFile
						rexExportTexture texmap outputFile
					)
				)
			)

			close bumpFile
			return true
		)
		else return false
	),

	--
	-- name: ExportShared
	-- desc: Export a shared texture list to a texture dictionary.
	--
	fn ExportShared texlist outname = (

		local stream_dir = ( RsVehicleSettings.GetStreamDir() )
		local script_dir = ( RsVehicleSettings.GetScriptDir() )

		-- Write out .textures file
		local bumpFileName = stream_dir + outname + ".textures"
		RsMakeSurePathExists bumpFileName
		local bumpFile = openfile bumpFileName mode:"w+"
		for texmapname in texlist do (

			texmapname = RsLowercase(RsMakeSafeSlashes texmapname)
			texmapddsname = RsConfigGetTexDir() + RsRemovePathAndExtension(texmapname) + ".dds"
			stripName = RsStripTexturePath(texmapname)				

			if findstring texmapname "_normal" == undefined then (

				format "%\n" ((filterstring stripName ".")[1]) to:bumpFile
				rexExportTexture texmapname texmapddsname 1024
			) else (
				format "% { Flags 1 }\n" ((filterstring stripName ".")[1]) to:bumpFile
				rexExportBumpTexture texmapname texmapddsname 1024
			)
		)
		close bumpFile

		-- Write out resourcing .rb file
		rbFileName = script_dir + outname + "_txd.rb"
		RsMakeSurePathExists rbFileName

		rbFile = openfile rbFileName mode:"w+"
		RBGenVehicleResourcingScriptPre rbFile undefined true
		RBGenVehicleResourcingScriptTxd rbFile outname texlist bumpFileName
		RBGenResourcingScriptPost rbFile
		close rbFile

		-- Run Ruby resource script for texture dictionary.		
		if ( 0 != ( RsVehicleScriptGenerator.RunScript rbFileName ) ) then 
		(
			local ss = stringStream ""
			format "Failed to build shared texture dictionary using script:\n%" rbFileName to:ss
			messagebox (ss as string) title:"Vehicle Exporter Error"
			return false
		)

		true	
	),
	
	--
	-- name: RemoveSharedFromTexMapList
	-- desc: remove any shared items from the vehicles texmap list
	--
	fn RemoveSharedFromTexMapList texmaplist isbumplist newbumplist shared_texture_list = (

		local shared_filenames = ( shared_texture_list.textures )
		newtexlist = #()

		for i = 1 to texmaplist.count do (

			local tex = texmaplist[i]
			local isbump = isbumplist[i]
			local simpleName = RsRemovePathAndExtension tex
			local found = false

			for checkTex in shared_filenames do (	

				if found == false then (

					simpleCheckName = RsRemovePathAndExtension checkTex

					if simpleCheckName == simpleName then (

						found = true
					)
				)
			)

			if found == false then (

				append newtexlist tex
				append newbumplist isbump
			)
		)

		return newtexlist
	)	
)

-- Create instance
global RsVehicleTexture = RsVehicleTextureStruct()

-- pipeline/export/vehicles/textures.ms
