--
-- File:: pipeline/export/vehicles/globals.ms
-- Description:: Vehicle Export Globals
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 3 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "rockstar/export/settings.ms"
filein "pipeline/export/vehicles/settings.ms"
filein "pipeline/util/shared_texture_list.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
global RsVehicle									-- Vehicle exporter functions
global RsVehicleSettings					-- Vehicle exporter settings functions
global RsVehicleScriptGenerator		-- Vehicle exporter script functions

global RsVehicleBatchWarnings
global RsVehicleName
global RsVehicleRPFs							-- Array of ContentVehiclesRpf objects
global RsVehicleRPFIndex					-- Index into above array
global RsCollCount = 0
global RsTruckVal
global RsVehicleAsAscii						-- Export vehicle meshes as ASCII

-- Shared Texture Lists
global RsVehicleTextureListFilename
global RsVehicleTruckTextureListFilename
global RsVehicleTextureVariationFilename
global RsVehicleTextureList
global RsVehicleTruckTextureList
global RsVehicleTextureVariationList

--
global RsVehicleVertexAlpha = #( 
	-- Light Meshes
	"brakelight_l", "brakelight_r", 
	"headlight_l", "headlight_r", 
	"indicator_lf", "indicator_rf", "indicator_lr", "indicator_rr", 
	"reversinglight_l", "reversinglight_r", 
	"taillight_l", "taillight_r",
	
	-- Window Meshes
	"window_lf", "window_lr",
	"window_rf", "window_rr",
	"windscreen_f", "windscreen_r"
)

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsVehicleResetGlobals
-- desc: Reset vehicle exporter globals (called at export start).
--
fn RsVehicleResetGlobals = (

	RsVehicleBatchWarnings = #()
	RsVehicleName = "unknown"
	RsVehicleRPFs = #()
	RsVehicleRPFIndex = 0
	RsCollCount = 0
	RsTruckVal = false
	
	RsVehicleTextureListFilename = ""
	RsVehicleTruckTextureListFilename = ""
	RsVehicleTextureVariationFilename = ""
	RsVehicleTextureList = undefined
	RsVehicleTruckTextureList = undefined
	RsVehicleTextureVariationList = undefined

	OK
)

--
-- name: RsVehicleSetupGlobals
-- desc: Setup vehicle exporter globals
--
fn RsVehicleSetupGlobals = (

	-- Load ContentVehicleRpfs
	RsVehicleRPFs = RsProjectContentFindAll type:"vehiclesrpf"
	RsVehicleRPFIndex = 1
	
	-- Reload shared texture lists.
	local filename = RsVehicleRPFs[RsVehicleRPFIndex].sharedtxdinput
	RsVehicleTextureListFilename = filename
	RsVehicleTruckTextureListFilename = ( RsRemoveExtension filename ) + "_truck.tex"
	
	RsVehicleTextureList = SharedTextureList RsVehicleTextureListFilename
	RsVehicleTruckTextureList = SharedTextureList RsVehicleTruckTextureListFilename
	
	-- Refresh shared texture UI
	if ( undefined != RsVehicleUtility ) then
	(
		format "Refreshing shared texture list UI...\n"
		RsVehicleUtility.rollouts[6].ReloadTexList filename:RsVehicleTextureListFilename
		RsVehicleUtility.rollouts[7].ReloadTexList filename:RsVehicleTruckTextureListFilename
	)
)

-- pipeline/export/vehicles/globals.ms
