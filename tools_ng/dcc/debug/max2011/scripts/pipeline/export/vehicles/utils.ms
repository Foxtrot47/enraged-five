--
-- File:: pipeline/export/vehicles/utils.ms
-- Description:: Vehicle Export Utility Functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Marissa Warner-Wu <marissa.warner-wu@rockstarnorth.com>
-- Date:: 3 October 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "rockstar/util/attribute.ms"
filein "pipeline/util/constraints.ms"
filein "pipeline/util/skinutils.ms"
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- struct: RsVehicleUtilStruct
-- desc: Vehicle export utility functions.
--
struct RsVehicleUtilStruct
(

	--
	-- name: GetCustomSkinnedObjects
	-- desc: Return array of custom skinned objects (those with Skin modifier)
	--
	fn GetCustomSkinnedObjects rootobj = (

		skinnedObjs = #()
		if ( ( Editable_mesh == classof rootobj ) or ( Editable_Poly == classof rootobj ) ) then
		(
			if ( undefined != rootobj.modifiers[#Skin] ) then
				append skinnedObjs rootobj
		)
		-- Recurse through children
		for child in rootobj.children do
		(
			skinnedObjs = skinnedObjs + ( GetCustomSkinnedObjects child )
		)

		skinnedObjs
	),

	--
	-- name: ProcessCustomSkinned
	-- desc: This function updates the custom skinned geometry to use the
	--       newly created skeleton rather than the ones the meshes are
	--       currently skinned to.
	--
	fn ProcessCustomSkinned srcobj customobjs boneList = (
	
		local vehicle = srcobj
		local vehicleFile = RsVehicleSettings.GetVehicleStreamDir vehicle
		local skinDir = vehicleFile + "/custom_skin/"
		
		-- Bake and save out vertex weight skinning information.
		for o in customobjs do
		(
			local skinFile = skinDir + o.name + ".env"	
			RsSkinUtils.BakeAndSaveWeights o skinFile
			collapseStack o
		)
		
		-- Load vertex weights back using new skeleton.
		for o in customobjs do
		(
			local skinFile = skinDir + o.name + ".env"
			RsSkinUtils.SkinAndLoadWeights o skinFile boneList match_by_name:true
		)
	),

	--
	-- name: CreateSkinnedWithCustomPostProcess
	-- desc: 
	--
	fn CreateSkinnedWithCustomPostProcess srcobj origobj customobjs boneList = (
		local children = #()
		local srcChildren = #()
		local destChildren = #()
		RsVehicleUtil.RecGetChildren srcobj children
		
		-- Clone our meshes and collapse into single mesh.
		local cloneRes = maxOps.CloneNodes children actualNodeList:&srcChildren newNodes:&destChildren
		format "Clone Result: %\n" cloneRes
		format "Src: %\n" srcChildren 
		format "New: %\n" destChildren
		
		-- Remove existing Skin modifier on new clones and collapse to a 
		-- single mesh.
		local n = 0
		for o in destChildren do
		(
			deleteModifier o o.modifiers[#Skin]
			n = n + 1
		
			if ( 1 == n ) then 
				continue
			
			meshOp.attach destChildren[1] o condenseMat:false
		)
		destChildren[1].name = srcChildren[1].name
		srcChildren[1].name = srcChildren[1].name + "_components"
		
		-- Setup and apply our Skin_Wrap modifier
		setCommandPanelTaskMode #modify
		wrapMod = Skin_Wrap()
		wrapMod.Distance = 0.012
		wrapMod.threshold = 0.01
		addModifier destChildren[1] wrapMod
		for o in srcChildren do
		(
			format "Appending mesh % to Skin_Wrap" o.name
			append wrapMod.meshList o
		)
		select destChildren[1]
		destChildren[1].modifiers[#Skin_Wrap].Distance = 0.001
		destChildren[1].modifiers[#Skin_Wrap].ConvertToSkin true

		-- Hide the source children components.
		for o in srcChildren do
			hide o

		true
	),

	--
	-- name: RecAddConstraints
	-- desc:
	--
	fn RecAddConstraints rootobj currobj = (

		if currobj.name == "door_dside_f" then (

			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.Axis = RsConstraintAxisZ
			newCons.LimitMin = -72.0
			newCons.LimitMax = 0.0
		)

		if currobj.name == "door_dside_r" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.Axis = RsConstraintAxisZ
			newCons.LimitMin = -72.0
			newCons.LimitMax = 72.0
		)

		if currobj.name == "door_pside_f" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0
		)

		if currobj.name == "door_pside_r" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.rotation = rotateZMatrix 90.0
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0	
		)

		if currobj.name == "bonnet" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = 0.0
			newCons.LimitMax = 72.0	
		)

		if currobj.name == "boot" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -72.0
			newCons.LimitMax = 0.0
		)	

		if currobj.name == "turret_1base" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisZ
			newCons.LimitMin = -180.0
			newCons.LimitMax = 180.0
		)	

		if currobj.name == "turret_1barrel" then (
			
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -30.0
			newCons.LimitMax = 30.0		
		)	

		if ( currobj.name == "gear_f" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisX
			newCons.LimitMin = -90.0
			newCons.LimitMax = 0.0
		)

		if ( currobj.name == "gear_rl" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisY
			newCons.LimitMin = -90.0
			newCons.LimitMax = 0.0
		)
		
		if ( currobj.name == "gear_rr" ) then (
					
			newCons = RsRotationConstraint()
			newCons.parent = currobj
			newCons.pos = currobj.pos
			newCons.axis = RsConstraintAxisY
			newCons.LimitMin = 0.0
			newCons.LimitMax = 90.0
		)

		for obj in currobj.children do (

			RecAddConstraints rootobj obj
		)
	),

	--
	-- name: RecRemoveLimits
	-- desc: Recursively remove constraint helpers from an object.
	--
	fn RecRemoveLimits currobj = (
		
		-- Base case of recursion.
		if ( ( Constraint == classof currobj ) or 
			( RsRotationConstraint == classof currobj ) or
			( RsTranslationConstraint == classof currobj ) ) then
		(
			format "Delete: %\n" currobj.name
			delete currobj
		)
		else if ( undefined != currobj ) then
		(		
			-- Recurse.
			for obj in currobj.children do 
			(
				RecRemoveLimits obj
			)
		)
	),

	--
	-- name: GetBoneListIndex
	-- desc:
	--
	fn GetBoneListIndex boneList boneName = (

		retval = 0

		for i = 1 to boneList.count do (

			if boneList[i].name == boneName then (

				retval = i
			)
		)

		if retval == 0 then (	
			print("couldnt find " + boneName)	
			--messagebox ("couldnt find " + boneName)	
		)

		retval
	),

	--
	-- name: AttachMeshesExistingRec
	-- desc: attach a whole lot of objects and automatically skin
	--
	fn AttachMeshesExistingRec attachobj currentobj parentmat boneList vertexBone = (

		if ( Editable_Mesh == classof currentobj ) then 
		(
			format "AttachMeshesExistingRec: %\n" currentobj.name
		
			if parentmat == undefined then (
				parentmat = currentobj.material 
			) 
			else if parentmat != currentobj.material then (
				local ss = stringStream ""
				format "Error: object % does not use the material %.  All geometry should share the same Multimaterial." currentobj.name parentmat.name to:ss
				messagebox (ss as String)
			)

			local attachNumFaces = attachobj.numfaces
			local attachNumVerts = attachobj.numverts
			local currentNumFaces = currentobj.numfaces
			local currentNumVerts = currentobj.numverts

			setnumfaces attachobj (attachNumFaces + currentNumFaces) true
			setnumverts attachobj (attachNumVerts + currentNumVerts) true		

			for i = 1 to currentNumVerts do (

				local currentVertex = (getVert currentobj i)
				local currentNormal = (getNormal currentobj i)
				local attachIndex = (i + attachNumVerts)

				setVert attachobj attachIndex currentVertex
				setNormal attachobj attachIndex currentNormal

				testName = currentobj.name

				subname = RsLowercase(substring testName (testName.count - 2) 3)

				if subname == "_l0" or subname == "_l1" or subname == "_l2" then (

					testName = substring currentobj.name 1 (currentobj.name.count - 3)
				)

				append vertexBone (GetBoneListIndex boneList testName)
			)

			for i = 1 to currentNumFaces do (

				local currentFace = getFace currentobj i
				local currentFaceMatID = getFaceMatID currentobj i
				local currentSmoothGroup = getFaceSmoothGroup currentobj i
				local attachFace = (i + attachNumFaces)

				currentFace[1] = currentFace[1] + attachNumVerts
				currentFace[2] = currentFace[2] + attachNumVerts
				currentFace[3] = currentFace[3] + attachNumVerts

				setFace attachobj attachFace currentFace
				setFaceMatID attachobj attachFace currentFaceMatID
				setFaceSmoothGroup attachobj attachFace currentSmoothGroup
			)

			local currentNumMaps = meshop.getnummaps currentobj
			local attachNumMaps = meshop.getnummaps attachobj

			if attachNumMaps < currentNumMaps then (

				meshop.setnummaps attachobj currentNumMaps keep:true

				for i = (attachNumMaps + 1) to currentNumMaps do (

					mapChannel = i - 1

					meshop.setmapsupport attachobj mapChannel true
					meshop.setnummapverts attachobj mapChannel 0
				)
			)

			for i = -2 to (currentNumMaps - 1) do (

				local mapChannel = i

				if ( -1 == mapChannel ) then 
					continue
				if ( -2 == mapChannel ) then
				(
					-- Skip if we are processing a particular light mesh,
					-- otherwise we copy the vertex alpha data.
					local lowername = RsLowercase( currentobj.name )
					if ( 0 != ( findItem RsVehicleVertexAlpha lowername ) ) then
					(
						format "Skipping vertex alpha channel for %\n" currentobj.name
						continue
					)
				)
				
				if meshop.getmapsupport currentobj mapChannel then (

					local currentNumVerts = meshop.getnummapverts currentobj mapChannel
					local attachNumVerts = 0

					if meshop.getmapsupport attachobj mapChannel then (

						attachNumVerts = meshop.getnummapverts attachobj mapChannel
					) else (

						meshop.setmapsupport attachobj mapChannel true
					)

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setnummapfaces attachobj mapChannel (attachNumFaces + currentNumFaces) keep:true

					for j = 1 to currentNumVerts do (

						local currentVertex = (meshop.getMapVert currentobj mapChannel j)
						local attachIndex = (j + attachNumVerts)

						meshop.setMapVert attachobj mapChannel attachIndex currentVertex
					)

					for j = 1 to currentNumFaces do (

						local currentFace = meshop.getMapFace currentobj mapChannel j
						local attachFace = (j + attachNumFaces)

						currentFace[1] = currentFace[1] + attachNumVerts
						currentFace[2] = currentFace[2] + attachNumVerts
						currentFace[3] = currentFace[3] + attachNumVerts

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)

				) else (

					local attachNumVerts = 0
					local currentNumVerts = 1

					if meshop.getmapsupport attachobj mapChannel then (

						attachNumVerts = meshop.getnummapverts attachobj mapChannel
					) else (

						meshop.setmapsupport attachobj mapChannel true
					)

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setMapVert attachobj mapChannel (attachNumVerts + currentNumVerts) [1,1,1]

					for j = 1 to currentNumFaces do (

						currentFace = [0,0,0]
						attachFace = (j + attachNumFaces)

						currentFace[1] = (attachNumVerts + currentNumVerts)
						currentFace[2] = (attachNumVerts + currentNumVerts)
						currentFace[3] = (attachNumVerts + currentNumVerts)

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)
				)
			)
		)

		for childobj in currentobj.children do (

			if getattrclass childobj != "Gta Collision" then (

				local newmat = AttachMeshesExistingRec attachobj childobj parentmat boneList vertexBone

				if newmat != undefined then parentmat = newmat
			)
		)

		parentmat
	),


	--
	-- name: AttachMeshesRec
	-- desc: Attach a whole lot of objects and automatically skin
	--
	fn AttachMeshesRec attachobj skelobj currentobj parentmat boneList vertexBone = (
		local isPoly = false
		
		-- If the object is an editable poly, temporarily convert it
		if ( Editable_Poly == classof currentobj ) then
		(
			isPoly = true
			format "Converting % from an editable poly to an editable mesh\n" currentobj.name
			convertToMesh currentobj
		)

		if ( Editable_Mesh == classof currentobj ) then 
		(
			format "AttachMeshesRec: %\n" currentobj.name
		
			if parentmat == undefined then (
				parentmat = currentobj.material 
			) 
			else if parentmat != currentobj.material then (
				local ss = stringStream ""
				format "Error: object % does not use the material %.  All geometry should share the same Multimaterial." currentobj.name parentmat.name to:ss
				messagebox (ss as String)
			)

			local attachNumFaces = attachobj.numfaces
			local attachNumVerts = attachobj.numverts
			local currentNumFaces = currentobj.numfaces
			local currentNumVerts = currentobj.numverts

			setnumfaces attachobj (attachNumFaces + currentNumFaces) true
			setnumverts attachobj (attachNumVerts + currentNumVerts) true		

			for i = 1 to currentNumVerts do (

				local currentVertex = (getVert currentobj i)
				local currentNormal = (getNormal currentobj i)
				local attachIndex = (i + attachNumVerts)

				setVert attachobj attachIndex currentVertex
				setNormal attachobj attachIndex currentNormal

				append vertexBone boneList.count
			)

			for i = 1 to currentNumFaces do (

				local currentFace = getFace currentobj i
				local currentFaceMatID = getFaceMatID currentobj i
				local currentSmoothGroup = getFaceSmoothGroup currentobj i
				local attachFace = (i + attachNumFaces)

				currentFace[1] = currentFace[1] + attachNumVerts
				currentFace[2] = currentFace[2] + attachNumVerts
				currentFace[3] = currentFace[3] + attachNumVerts

				setFace attachobj attachFace currentFace
				setFaceMatID attachobj attachFace currentFaceMatID
				setFaceSmoothGroup attachobj attachFace currentSmoothGroup
			)

			local currentNumMaps = meshop.getnummaps currentobj
			local attachNumMaps = meshop.getnummaps attachobj

			if attachNumMaps < currentNumMaps then (

				meshop.setnummaps attachobj currentNumMaps keep:true

				for i = (attachNumMaps + 1) to currentNumMaps do (

					mapChannel = i - 1

					meshop.setmapsupport attachobj mapChannel true
					meshop.setnummapverts attachobj mapChannel 0
				)
			)

			for i = -2 to (currentNumMaps - 1) do (

				local mapChannel = i

				if ( -1 == mapChannel ) then 
					continue
				if ( -2 == mapChannel ) then
				(				
					-- Skip if we are processing a particular light mesh,
					-- otherwise we copy the vertex alpha data.
					local lowername = RsLowercase( currentobj.name )
					if ( 0 != ( findItem RsVehicleVertexAlpha lowername ) ) then
					(
						format "Skipping vertex alpha channel for %\n" currentobj.name
						continue
					)
				)

				format "From % to %\n" currentobj.name attachobj.name
				if meshop.getmapsupport currentobj mapChannel then (

					if meshop.getmapsupport attachobj mapChannel == false then (

						format "Setting enable channel: %\n" mapChannel
						meshop.setmapsupport attachobj mapChannel true
						meshop.setnummapverts attachobj mapChannel 0
					)

					local attachNumVerts = meshop.getnummapverts attachobj mapChannel
					local currentNumVerts = meshop.getnummapverts currentobj mapChannel
					format "\tCopying channel % [%, %]\n" mapChannel currentNumVerts attachNumVerts

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setnummapfaces attachobj mapChannel (attachNumFaces + currentNumFaces) keep:true

					for j = 1 to currentNumVerts do (

						local currentVertex = (meshop.getMapVert currentobj mapChannel j)
						local attachIndex = (j + attachNumVerts)

						meshop.setMapVert attachobj mapChannel attachIndex currentVertex
					)

					for j = 1 to currentNumFaces do (

						local currentFace = meshop.getMapFace currentobj mapChannel j
						local attachFace = (j + attachNumFaces)

						currentFace[1] = currentFace[1] + attachNumVerts
						currentFace[2] = currentFace[2] + attachNumVerts
						currentFace[3] = currentFace[3] + attachNumVerts

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)

				) else (

					local attachNumVerts = 0
					local currentNumVerts = 1

					if meshop.getmapsupport attachobj mapChannel then (

						attachNumVerts = meshop.getnummapverts attachobj mapChannel
					) else (

						meshop.setmapsupport attachobj mapChannel true
					)

					meshop.setnummapverts attachobj mapChannel (attachNumVerts + currentNumVerts) keep:true
					meshop.setMapVert attachobj mapChannel (attachNumVerts + currentNumVerts) [1,1,1]

					for j = 1 to currentNumFaces do (

						local currentFace = [0,0,0]
						local attachFace = (j + attachNumFaces)

						currentFace[1] = (attachNumVerts + currentNumVerts)
						currentFace[2] = (attachNumVerts + currentNumVerts)
						currentFace[3] = (attachNumVerts + currentNumVerts)

						meshop.setMapFace attachobj mapChannel attachFace currentFace
					)
				)
			)
		)

		for childobj in currentobj.children do (

			if ( Constraint != classof childobj ) and
			   ( RsRotationConstraint != classof childobj ) and 
			   ( RsTranslationConstraint != classof childobj ) then 
			(

				if getattrclass childobj != "Gta Collision" then (

					newskelobj = Dummy()
					newskelobj.name = childobj.name
					newskelobj.transform = childobj.transform
					newskelobj.boxsize = [.2,.2,.2]
					newskelobj.parent = skelobj

					append boneList newskelobj

					newmat = AttachMeshesRec attachobj newskelobj childobj parentmat boneList vertexBone

					if newmat != undefined then parentmat = newmat
				) else (

					col2mesh childobj
					newcollobj = copy childobj
					mesh2col childobj
					mesh2col newcollobj

					newcollobj.name = childobj.name
					newcollobj.parent = skelobj			

					idxCollType = getattrindex "Gta Collision" "Coll Type"
					setattr newcollobj idxCollType (getattr childobj idxCollType)
				)
			)
			else if ( RsRotationConstraint == classof childobj ) then
			(
				newobj = RsRotationConstraint()
				newobj.parent = skelobj
				newobj.transform = childobj.transform
				newobj.Axis = childobj.Axis
				newobj.LimitMin = childobj.LimitMin
				newobj.LimitMax = childobj.LimitMax
			)
			else if ( RsTranslationConstraint == classof childobj ) then
			(
				newobj = RsTranslationConstraint()
				newobj.parent = skelobj
				newobj.transform = childobj.transform
				newobj.Axis = childobj.Axis
				newobj.LimitMin = childobj.LimitMin
				newobj.LimitMax = childobj.LimitMax
			)
		)
		
		-- If this object was originally an editable poly then convert it back
		if isPoly then
		(
			convertTo currentobj (Editable_Poly)
		)

		parentmat
	),


	--
	-- name: DeleteRec
	-- desc: delete a whole hierarchy from the passed in parent down
	--
	fn DeleteRec obj = (

		for childobj in obj.children do (

			DeleteRec childobj
		)

		delete obj
	),


	--
	-- name: RecGetChildren
	-- desc:
	--
	fn RecGetChildren currobj outlist = (

		append outlist currobj

		for obj in currobj.children do (

			RecGetChildren obj outlist
		)
	),

	--
	-- name: AttachRotorToSkeleton
	-- desc: Attach rotor node into vehicle skeleton (attached to chassis node)
	--
	fn AttachRotorToSkeleton skelobj rotorNode = (

		if skelobj.name == "chassis" then (

			if rotorNode != undefined then rotorNode.parent = skelobj
		)

		for obj in skelobj.children do (

			AttachRotorToSkeleton obj rotorNode 
		)		
	),


	--
	-- name: AttachRotor2ToSkeleton
	-- desc: Attach rotor2 node into vehicle skeleton (attached to boot node)
	--
	fn AttachRotor2ToSkeleton skelobj rotorNode = (

		if skelobj.name == "boot" then (

			if rotorNode != undefined then rotorNode.parent = skelobj
		)

		for obj in skelobj.children do (

			AttachRotor2ToSkeleton obj rotorNode 
		)		
	),


	--
	-- name: AttachWheelsToSkeleton
	-- desc:
	--
	fn AttachWheelsToSkeleton skelobj wheelNode wheelNodeL1 wheelNodeR wheelNodeRL1 = (

		if skelobj.name == "wheel_lf" then (

			if wheelNode != undefined then wheelNode.parent = skelobj
			if wheelNodeL1 != undefined then wheelNodeL1.parent = skelobj
		)

		if skelobj.name == "wheel_lr" then (

			if wheelNodeR != undefined then wheelNodeR.parent = skelobj
			if wheelNodeRL1 != undefined then wheelNodeRL1.parent = skelobj
		)	

		for obj in skelobj.children do (

			AttachWheelsToSkeleton obj wheelNode wheelNodeL1 wheelNodeR wheelNodeRL1
		)
	),


	--
	-- name: AddExisting
	-- desc:
	--
	fn AddExisting srcobj savetrans setname boneList = (

		if srcobj != undefined then (

			newobjlod1 = Editable_Mesh()
			newobjlod1.name = setname
			newobjlod1.transform = srcobj.transform

			vertexBone = #()
			copymat = AttachMeshesExistingRec newobjlod1 srcobj undefined boneList vertexBone
			update newobjlod1 

			newobjlod1.material = copy copymat

			select newobjlod1

			setCommandPanelTaskMode #modify
			newSkinLod1 = Skin()
			newSkinLod1.mirrorEnabled = off

			addmodifier newobjlod1 newSkinLod1

			for obj in boneList do (

				skinops.addbone newSkinLod1 obj 1
			)

			if newobjlod1.numverts == vertexBone.count then (

				for i = 1 to vertexBone.count do (	

					skinops.replacevertexweights newSkinLod1 i vertexBone[i] 1.0
				)
			) else (

				messagebox "error in skinning"
			)	

			srcobj.transform = savetrans
		)	
	),


	--
	-- name: TagBones
	-- desc: Tag bones for cutscene animation
	--
	fn TagBones = (
		
		cursel = selection[1]
		
		rootSelect = rexGetSkinRootBone selection[1]

		if rootSelect == undefined then rootSelect = selection[1]
		
		alllist = #()
		RecGetChildren rootSelect alllist
		
		select alllist

		for obj in selection do (
			
			if classof obj == Dummy then (
				-- LPXO: Set the boneid tag to the bone name
				setuserprop obj "tag" obj.name
			)
		) 
		
		select cursel
		
		true
	),


	--
	-- name:
	-- desc: setup the ragdoll version of the model's bounds elements
	--
	fn SetupFragBounds objnode rexroot exportDir wheelobjs:undefined wheelids:undefined = (

		if objnode == undefined then (

			return 0
		)

		if classof objnode == Col_Capsule or classof objnode == Col_Mesh or classof objnode == Col_Box or classof objnode == GtaWheel then (

			if (classof objnode == GtaWheel) and (wheelobjs != undefined) then (

				append wheelobjs objnode
				append wheelids RsCollCount
			)

			idxSurfaceType = getattrindex "Gta Collision" "Coll Type"
			valSurfaceType = RsUppercase(getattr objnode idxSurfaceType)

			if valSurfaceType == "CAR_PANEL" then setattr objnode idxSurfaceType "CAR_METAL"
			if valSurfaceType == "WINDSCREEN_WEAK" then setattr objnode idxSurfaceType "WINDSCREEN_SIDE_WEAK"
			if valSurfaceType == "WINDSCREEN_MED_WEAK" then setattr objnode idxSurfaceType "WINDSCREEN_SIDE_MED"
			if valSurfaceType == "WINDSCREEN_MED_STRONG" then setattr objnode idxSurfaceType "WINDSCREEN_REAR"
			if valSurfaceType == "WINDSCREEN_STRONG" then setattr objnode idxSurfaceType "WINDSCREEN_FRONT"

			select objnode

			surfaceType = RsGetCollSurfaceTypeString objnode 

			bound = rexAddChild rexroot (RsVehicleName + (RsCollCount as string)) "Bound"
			rexSetPropertyValue bound "OutputPath" exportDir
			rexSetPropertyValue bound "ForceExportAsComposite" "false"
			rexSetPropertyValue bound "ExportAsComposite" "false"				
			bvhIdx = getattrindex "Gta Collision" "BVH Bound"
			rexSetPropertyValue bound "ExportAsBvhGeom" ((getattr objnode bvhIdx) as string)
			rexSetPropertyValue bound "BoundExportWorldSpace" "false"
			rexSetPropertyValue bound "SurfaceType" (objnode.name + "=" + surfaceType)
			rexSetNodes bound	

			RsCollCount += 1
		)

		for childobj in objnode.children do (

			SetupFragBounds childobj rexroot exportDir wheelobjs:wheelobjs wheelids:wheelids
		)
	),



	--
	-- name: CreateCustomRagebuilderScripts
	-- desc: Create our custom Ragebuilder scripts to be included in our vehicle 
	--       IFT/IDR files for conversion.
	--
	fn CreateCustomRagebuilderScripts = (

		-- Create Ragebuilder scripts to be included into our independent image.
		luaFileName = RsVehicleSettings.GetStreamDir() + "custom.rbs"
		luaFile = openfile luaFileName mode:"w+"
		format "postprocess_vehicle(1)\n" to:luaFile
		close luaFile

		luaFileName = RsVehicleSettings.GetStreamDir() + "custom_finish.rbs"
		luaFile = openfile luaFileName mode:"w+"
		format "postprocess_vehicle(0)\n" to:luaFile
		close luaFile
		true
	)

) -- RsVehicleUtil utility

-- Global instance
global RsVehicleUtil = RsVehicleUtilStruct()

-- pipeline/export/vehicles/utils.ms
