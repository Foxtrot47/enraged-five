--
-- File:: pipeline/export/vehicles/settings.ms
-- Description:: Vehicle exporter specific settings functions
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 4 November 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/util/file.ms"
filein "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- struct: RsVehicleSettingsStruct
-- desc:
--
struct RsVehicleSettingsStruct
(

	--
	-- name: GetStreamDir
	-- desc: Return vehicle stream directory.
	--
	fn GetStreamDir = 
	(
		local vehicle_rpf = RsVehicleRPFs[RsVehicleRPFIndex]
		( RsConfigGetStreamDir() + "vehicles/" + vehicle_rpf.friendlyname + "/" )
	),

	--
	-- name: GetVehicleStreamDir
	-- desc: Return vehicle-specific stream directory
	--
	fn GetVehicleStreamDir vehicle =
	(
		( GetStreamDir() + vehicle.name + "/" )
	),

	--
	-- name: GetGlobalScriptDir
	-- desc: Return global vehicle script directory (not RPF specific)
	--
	fn GetGlobalScriptDir =
	(
		( RsConfigGetScriptDir() + "vehicles/" )
	),

	--
	-- name: GetScriptDir
	-- desc: Return RPF specific vehicle script directory
	--
	fn GetScriptDir = 
	(
		local vehicle_rpf = RsVehicleRPFs[RsVehicleRPFIndex]
		( GetGlobalScriptDir() + vehicle_rpf.friendlyname + "/" )
	),

	--
	-- name: GetVehicleScriptDir
	-- desc: Return vehicle-specific script directory
	--
	fn GetVehicleScriptDir vehicle =
	(
		( GetScriptDir() + "/" + vehicle_name + "/" )
	)

)

-- Create global instance of VehicleSettingsStruct
global RsVehicleSettings = RsVehicleSettingsStruct()

-- pipeline/export/vehicles/settings.ms
