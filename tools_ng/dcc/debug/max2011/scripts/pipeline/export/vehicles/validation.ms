--
-- validation.ms
-- Weapon Export Validation Functions
--
-- David Muir <david.muir@rockstarnorth.com>
-- 5 August 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsVehicleCheckForValidSkin
-- desc:
--
fn RsVehicleCheckForValidSkin checkNode = (		
	if checkNode == undefined then (
		return true
	)

	select checkNode

	try
	(
		for i = 1 to checkNode.numverts do (

			boneCount = skinOps.GetVertexWeightCount checkNode.modifiers[1] i

			if boneCount > 1 then (

				validBones = 0

				for j = 1 to boneCount do (

					boneWeight = skinOps.GetVertexWeight checkNode.modifiers[1] i j

					if boneWeight > 0.0 then (
						validBones = validBones + 1	
					)
				)

				if validBones > 4 then (
					messagebox (checkNode.name + " has vertices with " + (validBones as string) + " bone influences (" + (i as string) + ")")
					return false
				)
			)
		)
	)
	catch
	(
		ss = stringStream ""
		format "Vehicle skin valid failed: %.  Do you have the right object selected?" ( getCurrentException() ) to:ss
		messagebox ( ss as string )
	)

	return true
)


--
-- name: RsVehicleIsValidSkin
-- desc:
--
fn RsVehicleIsValidSkin = (

	setCommandPanelTaskMode mode:#modify

	cursel = #()
	cursel = cursel + selection

	meshNode = cursel[1]
	meshLodNode1 = getnodebyname (meshNode.name + "_l1_skin") exact:true ignorecase:true
	meshLodNode2 = getnodebyname (meshNode.name + "_l2_skin") exact:true ignorecase:true			

	passedTests = true

	if RsVehicleCheckForValidSkin meshNode == false then (

		passedTests = false
	) else if RsVehicleCheckForValidSkin meshLodNode1 == false then (

		passedTests = false
	) else if RsVehicleCheckForValidSkin meshLodNode2 == false then (

		passedTests = false
	)

	select cursel

	passedTests
)


--
-- name: RsVehicleTestObject
-- desc: Test an individual object before exporting
--
fn RsVehicleTestObject obj = (

	local fEpsilon = 0.01f

	if RsContainsSpaces obj.name then (

		messagebox (obj.name + " has spaces in its name")
		select obj
		return false			
	)

	-- Validate no scaling on meshes.
	if ( Editable_Mesh == classof obj ) or ( Editable_Poly == classof obj ) then 
	(
		in coordsys local (

			if ( ( obj.scale.x < ( 1.0 - fEpsilon ) ) or 
				 ( obj.scale.x > ( 1.0 + fEpsilon ) ) or 
				 ( obj.scale.y < ( 1.0 - fEpsilon ) ) or
				 ( obj.scale.y > ( 1.0 + fEpsilon ) ) or 
				 ( obj.scale.z < ( 1.0 - fEpsilon ) ) or 
				 ( obj.scale.z > ( 1.0 + fEpsilon ) ) ) then (

				local msg = stringStream ""
				format "% has scaling %, cannot export." obj.name obj.scale to:msg
				messagebox (msg as string)
				select obj
				return false
			)
		)
	)
	
	-- Recurse
	for childobj in obj.children do (

		if RsVehicleTestObject childobj == false then return false
	)

	true
)

--
-- name: RsVehicleIsValidForExport
-- desc: check that we have a valid file for export
--
fn RsVehicleIsValidForExport = (

	if selection.count != 1 then (

		messagebox "please select just one object"
		return false			
	)

	if classof(selection[1]) != Dummy then (

		messagebox "object has to be a dummy"
		return false
	)

	if selection[1].parent != undefined then (

		messagebox "has to be a root object"
		return false
	)

	(RsVehicleTestObject selection[1])
)


--
-- name:
-- desc:
--
fn RsVehicleCheckBatchExport batchWarnings = (

	if batchWarnings.count > 0 then (	
		
		global tempArr = #()
		for item in batchWarnings do (
			append tempArr item

		)
		
		rollout RsBatchWarningsRoll "Batch Export Warnings"
		(
			listbox lstMissing1 items:tempArr
			button btnOK "OK" width:100 pos:[150,150]

			
			
			on btnOK pressed do (

				DestroyDialog RsBatchWarningsRoll
				
			)
		)

		CreateDialog RsBatchWarningsRoll width:400 modal:true			
	)
)


--
-- name: RsCheckTexDimension	
-- desc: Checks that the texture has alright dimensions (power of 2)
--
fn RsCheckTexDimension texlist = (
	global RsVehTexErrors = #()
	
	for tex in texlist do (
		-- Only check textures which exist on this computer
		if RsFileExist tex then (
			valid = true
			b = openBitmap tex
			
			-- Check the dimensions
			if ( ( bit.and b.width (b.width - 1) ) != 0 ) then valid = false
			if ( ( bit.and b.height (b.height - 1) ) != 0 ) then valid = false
			
			if not valid then (
				message = tex + " ( " + (b.width as string) + "x" + (b.height as string) + " )"
				append RsVehTexErrors message
			)
		)
	)
	
	-- If we found errors, display them
	if (RsVehTexErrors.count > 0) then (
		rollout RsTexWarningsRoll "Found textures with invalid dimensions!"
		(
			listbox lstBadTex items:RsVehTexErrors
			button btnOK "OK" width:100 

			on btnOK pressed do (
				DestroyDialog RsTexWarningsRoll
			)	
		)
	
		CreateDialog RsTexWarningsRoll width:600 modal:true
		return false
	)
	else (
		return true
	)
)

-- validation.ms
