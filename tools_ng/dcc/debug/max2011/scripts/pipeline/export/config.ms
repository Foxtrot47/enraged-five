--
-- File:: pipeline/export/config.ms
-- Description:: Pipeline Configuration Read-Only Interface
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 5 February 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "pipeline/export/project.ms"
include "pipeline/util/file.ms"
include "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Global Data
-----------------------------------------------------------------------------
RsLocalFileSuffix = "local.xml"
RsMinLogLevel = 1
RsMaxLogLevel = 5
RsSettingsVersion = 12

-----------------------------------------------------------------------------
-- Code
-----------------------------------------------------------------------------

--
-- name: Config
-- desc:
--
struct Config
(
    toolsdrive,		-- Tools drive
	toolsroot,      -- Tools root directory
    toolsbin,       -- Tools bin directory
	toolsconfig,	-- Tools configuration data root directory
	toolslib,		-- Tools lib directory
	toolsproject,	-- Tools project key
	toolsruby,		-- Ruby interpreter absolute path
	
    globalConfig,   -- Global config XML filename
    localConfig,    -- Local config XML filename (for local customisation)
    
    -------------------------------------------------------------------------
    -- Public Configuration
    -------------------------------------------------------------------------
    activeProject = undefined,  -- Currently active project
    
    projects = #(), -- Array of found projects
    version,        -- Integer version number

    username,       -- Username string
    userflags,      -- Userflags mask
    useremailaddress,-- User email address string
    logstdout,      -- True/false logging to stdout
    loglevel,       -- Integer log level
    logtrace,       -- True/false logging trace messages
    currconfig = 1, -- Index of the current project config
    settingsversion = RsSettingsVersion, -- Version of max settings last used.  Set its default value to the current version

    -------------------------------------------------------------------------
    -- Private Data (erm, thats means don't use it!  no really...)
    -------------------------------------------------------------------------
    
    __environment = Environment(),  -- Environment table
    __global,
    __local,
    __xmlGlobalRoot,
    __xmlLocalRoot,

    -------------------------------------------------------------------------
    -- Public Methods
    -------------------------------------------------------------------------

    --
    -- name: print
    -- desc: Print config object out to the listener, useful for debugging.
    --
    fn Dump = (
    
        format "--------------------------------------------------------------------------------\n"
        format " Config\n"
        format "--------------------------------------------------------------------------------\n"
        format "\n"
		format "\ttoolsdrive:   %\n" toolsdrive
        format "\ttoolsroot:    %\n" toolsroot
        format "\ttoolsbin:     %\n" toolsbin
        format "\ttoolsconfig:  %\n" toolsconfig
		format "\ttoolslib:     %\n" toolslib
		format "\ttoolsproject: %\n" toolsproject
		format "\ttoolsruby:    %\n" toolsruby
        format "\n"
        format "\tGlobal File: %\n" globalConfig
        format "\tLocal File: %\n" localConfig
        format "\n"
        format "\tVersion: %\n" version
        format "\t# Projects: %\n" projects.count
        format "\n"
        format "\tUsername: %\n" username
        format "\tUsertype: %\n" usertype
        format "\n"
        format "\tLog Stdout: %\n" logstdout
        format "\tLog Level: %\n" loglevel
        format "\tLog Trace: %\n" logtrace
        format "\n"

        __environment.Print()
        
        for proj in projects do
        (
            proj.Print()
        )
        
        format "--------------------------------------------------------------------------------\n"
        format " End of Config\n"
        format "--------------------------------------------------------------------------------\n"
    ),

    --
    -- name: envsubst
    -- desc: Substitute any environment variables in input string and return 
    --       result
    --
    fn envsubst input = (
    
        ( __environment.subst input )
    ),
    
    --
    -- name: SetActiveProject
    -- desc:
    --
    fn SetActiveProject index = (
    
        activeProject = projects[index]
        RsSettingWrite "RsSettings" "ActiveProject" index
        
        true
    ),
    
    --
    -- name: SetLogLevel
    -- desc: Set the log level and write out the local XML config file
    --
    fn SetLogLevel level = (
    
        if ( ( level < RsMinLogLevel ) or ( level > RsMaxLogLevel ) ) then
        (
            local message = stringStream ""
            format "Error updating log level.  Set between % and % inclusive." RsMinLogLevel RsMaxLogLevel to:message
            
            messageBox message
            return ( false )
        )
                
        -- Find child logging node
        local childNodeList = __xmlLocalRoot.ChildNodes
        for nChild = 0 to ( childNodeList.Count - 1 ) do
        (
        
            local elem = childNodeList.ItemOf( nChild )
            
            -- Client logging settings
            if ( "logging" == elem.name ) then
            (
                local attrLevel = elem.Attributes.ItemOf( "level" )
                attrLevel.Value = ( level as string )
                loglevel = level
                
                __local.save localConfig
                return ( true )
            )
        )
        
        false
    ),

    -------------------------------------------------------------------------
    -- Private Methods
    -------------------------------------------------------------------------   

    fn __parseGlobalConfig = (
    	
        -- Prepopulate our environment with entries from our previously 
        -- configured data.
		__environment.add "toolsdrive" toolsdrive
        __environment.add "toolsroot" toolsroot
        __environment.add "toolsbin" toolsbin
		__environment.add "toolsconfig" toolsconfig
		__environment.add "toolslib" toolslib
		__environment.add "toolsproject" toolsproject
		__environment.add "toolsruby" toolsruby
		__environment.add "filepath" ( RsRemoveFilename globalConfig )
    
        -- Fetch version
        --try
        (
            local attr = __xmlGlobalRoot.Attributes.ItemOf( "version" ) 
            version = ( attr.Value as integer )
        )
        --catch
		(
        --    version = undefined
		)
			
        -- Parse child nodes for additional project config stuff.
        local childNodeList = __xmlGlobalRoot.ChildNodes
        for nChild = 0 to ( childNodeList.Count - 1 ) do
        (

            local elem = childNodeList.ItemOf( nChild )
            if ( "projects" == elem.name ) then
            (

                local projectChildList = elem.ChildNodes
                for nProjChild = 0 to ( projectChildList.Count - 1 ) do
                (
                
                    local projElem = projectChildList.ItemOf( nProjChild )
                    local projAttrName = projElem.Attributes.ItemOf( "name" )
                    local projAttrUIName = projElem.Attributes.ItemOf( "uiname" )
                    local projAttrRoot = projElem.Attributes.ItemOf( "root" )
                    local projAttrConfig = projElem.Attributes.ItemOf( "config" )
                    local projAttrAb = projElem.Attributes.ItemOf( "sc" )
                    
                    local projGlobalFile = __environment.subst( projAttrConfig.Value )
                    local projLocalFile = RsRemoveFile( projGlobalFile ) + RsLocalFileSuffix
                    local projName = projAttrName.Value
                    local projUIName = projAttrUIName.Value
                    local projRoot = projAttrRoot.Value
                    local projAb = projAttrAb.Value
                	
                    --try
                    (
                        -- Ignore configurations that are not there...
                        if not ( doesFileExist projGlobalFile ) then
                            continue
			
                        proj = ( Project projGlobalFile projLocalFile projName projUIName projRoot projAb )
                        if proj != undefined then append projects proj
                        
                        -- Load project (can cause exceptions if files don't exist)
                        projects[projects.count].__load true
                    )
                    --catch 
                    (
                    --    format "Project exception: %\n" ( getCurrentException() )
                    )
                )
            )
            
            -- Currently other 2nd level nodes are ignored
        )
        
    ),
    
    fn __parseLocalConfig = (
    
        -- Parse child nodes for additional local config stuff
        local childNodeList = __xmlLocalRoot.ChildNodes
        for nChild = 0 to ( childNodeList.Count - 1 ) do
        (        
			local elem = childNodeList.ItemOf( nChild )

			-- Get user data
			if ( "user" == elem.name ) then
			(        
				local attrUsername = elem.Attributes.ItemOf( "username" )
				local attrFlags = elem.Attributes.ItemOf( "flags" )
				local attrEmail = elem.Attributes.ItemOf( "emailaddress" )
				username = attrUsername.Value
				userflags = ( attrFlags.Value as Integer )
				useremailaddress = attrEmail.Value
			)
			-- Get project enabled flags
			else if ( "projects" == elem.name ) then
			(
			)
			-- Get client logging settings
			else if ( "logging" == elem.name ) then
			(
				local attrStdout = elem.Attributes.ItemOf( "stdout" )
				local attrLevel = elem.Attributes.ItemOf( "level" )
				local attrTrace = elem.Attributes.ItemOf( "trace" )

				logstdout = ( attrStdout.Value == "true" )
				loglevel = ( attrLevel.Value as integer)
				logtrace = ( attrTrace.Value == "true" )
			)
			else if ( "maxsettings" == elem.name ) then
			(
				local attrCurrConfig = elem.Attributes.ItemOf( "currconfig" )
				local attrSettingsVersion = elem.Attributes.ItemOf( "settingsversion" )

				currconfig = attrCurrConfig.Value as integer
					settingsversion = attrSettingsVersion.Value as integer
			)
            
            -- Currently other 2nd level nodes are ignored
        )
    ),
    
	fn UpdateLocalConfig = (
		
		-- Parse and update nodes
		local childNodeList = __xmlLocalRoot.ChildNodes
		local maxsettingsfound = false
		for nChild = 0 to ( childNodeList.Count - 1 ) do
		(

			local elem = childNodeList.ItemOf( nChild )
			
			if ( "maxsettings" == elem.name ) then
			(
				maxsettingsfound = true
				
				local attrCurrConfig = elem.Attributes.ItemOf( "currconfig" )
				local attrSettingsVersion = elem.Attributes.ItemOf( "settingsversion" )
				
				attrCurrConfig.Value = currconfig as string
				attrSettingsVersion.Value = settingsversion as string
			)
		)
		
		
		-- old style local.xml
		if maxsettingsfound == false then 
		(
			
			local currconfigattr = Attribute "currconfig" currconfig
			local settingsversionattr = Attribute "settingsversion" RsSettingsVersion

			attributearray = #( currconfigattr, settingsversionattr )
			__xmlLocalRoot.AppendChild( RsCreateXmlElement "maxsettings" attributearray __local )
		)
		__local.save localConfig
		
	),
	
    
    fn __load = (
    
        --try
        (
            -- Load plugcfg/pipeline.xml file to determine the toolsbin and
            -- toolsroot directories.
            __pipeline = XmlDocument()
            __pipeline.init()
            __pipeline.load ( getdir #plugcfg + "pipeline.xml" )
            local elem = __pipeline.document.DocumentElement
            if ( "max_pipeline" == elem.name ) then
            (
				local attrToolsDrive = elem.Attributes.ItemOf( "toolsdrive" )
                local attrToolsRoot = elem.Attributes.ItemOf( "toolsroot" )
                local attrToolsBin = elem.Attributes.ItemOf( "toolsbin" )
				local attrToolsConfig = elem.Attributes.ItemOf( "toolsconfig" )
				local attrToolsLib = elem.Attributes.ItemOf( "toolslib" )
				local attrToolsProject = elem.Attributes.ItemOf( "toolsproject" )
				local attrToolsRuby = elem.Attributes.ItemOf( "toolsruby" )
                
                toolsdrive = attrToolsDrive.Value as string
				toolsroot = attrToolsRoot.Value as string
                toolsbin = attrToolsBin.Value as string
				toolsconfig = attrToolsConfig.Value as string
				toolslib = attrToolsLib.Value as string
				toolsproject = attrToolsProject.Value as string
				toolsruby = attrToolsRuby.Value as string
            )
        
            globalConfig = toolsroot + "/config.xml"
            localConfig = toolsroot + "/local.xml"
            __global = XmlDocument()    
            __global.init()
            __global.load globalConfig
            __local = XmlDocument() 
            __local.init()
            __local.load localConfig

            __xmlGlobalRoot = __global.document.DocumentElement
            __xmlLocalRoot = __local.document.DocumentElement
            if ( "config" != __xmlGlobalRoot.name ) then
            (
                messageBox "Error parsing global RSN Pipeline config.  Invalid XML start tag."
                return ( false )
            )
            if ( "local" != __xmlLocalRoot.name ) then
            (
                messageBox "Error parsing local RSN Pipeline config.  Invalid XML start tag."
                return ( false )
            )

            __parseGlobalConfig()
            __parseLocalConfig()
            
            -- Set active project
            local activeProjectIdx = ( RsSettingRead "RsSettings" "ActiveProject" 1 ) as integer        
            SetActiveProject activeProjectIdx
        )
        --catch 
        (
		--	format "Fatal Exception during Config::__load: %\n" ( getCurrentException() )
        )       
    ),
    
    fn __reset = (
    
        --try
        (
			toolsdrive = undefined
            toolsroot = undefined
            toolsbin = undefined
			toolsconfig = undefined
			toolslib = undefined
			toolsruby = undefined
			toolsproject = undefined
			
            activeProject = undefined
            projects = #()
            version = undefined
            
            username = undefined
            usertype = undefined
            logstdout = undefined
            loglevel = undefined
            logtrace = undefined
        )
        --catch
        (
		--	format "Fatal Exception during Config::__reset: %\n" ( getCurrentException() )
        )
    )
)

-- settings is included in many files.  We only need the config to be created once
if RsConfig == undefined then ( 
	
	-- Delcare global instance of our config
	RsConfig = Config()
	--try
	(
	    RsConfig.__reset()
	    RsConfig.__load() 
	)
	--catch
	(
	--    format "Fatal Exception during RsConfig Init: %\n" ( getCurrentException() )
	)
)

-- End of config.ms
