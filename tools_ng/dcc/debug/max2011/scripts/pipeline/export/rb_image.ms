-- Rockstar Image Building
-- Rockstar North
-- 15/7/2008
-- by Luke Openshaw

-- Greg's original image building script updated to use new content system

include "pipeline/util/rb_script_generator.ms"

--------------------------------------------------------------
-- build the independant image and convert it to the platform image
--------------------------------------------------------------
fn RsBuildAndConvertImageUsingScript directory rbscript projname contenttype rubyFileName = (

	-- build script for creating image
	
	RsMakeSurePathExists rubyFileName
	
	RBGenResourcingScript rubyFileName projname rbscript contenttype
	
	cmdline = "ruby" + " "	
	cmdline = cmdline + rubyFileName
	
	if doscommand cmdline != 0 then (

		messagebox ("failed to make image: " + rbscript)
		return false
	)	
	
	return true
)

--------------------------------------------------------------
-- build the independant image and convert it to the platform image
--------------------------------------------------------------
fn RsBuildAndConvertImage directory rbscript projname contenttype dorebuild:false = (

	if dorebuild then (
	
		RsDeleteFiles (directory + "*.c??")
		RsDeleteFiles (directory + "*.w??")
		RsDeleteFiles (directory + "*.x??")		
	)

	
	rubyFileName = RsConfigGetStreamDir() + "/build_and_convert_image.rb"
	
	return (RsBuildAndConvertImageUsingScript directory rbscript projname contenttype rubyFileName)
)