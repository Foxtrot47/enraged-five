--
-- File:: pipeline/export/peds/utils.ms
-- Description:: Ped/player exporter utilities.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 April 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------

--
-- name: RsPedCreateCustomRagebuilderScripts
-- desc: Create our custom Ragebuilder scripts to be included in our streamedpeds 
--       IDD files for conversion.
--
fn RsPedCreateCustomRagebuilderScripts outputDir = (

	format "Writing out custom Ragebuilder scripts...\n"
	RsMakeSurePathExists outputDir

	-- Create Ragebuilder scripts to be included into our independent image.
	luaFileName = RsMakeBackSlashes( outputDir + "custom.rbs" )
	format "\t%\n" luaFileName
	luaFile = openfile luaFileName mode:"w+"
	format "set_auto_texdict( true )\n" to:luaFile
	close luaFile

	luaFileName = RsMakeBackSlashes( outputDir + "custom_finish.rbs" )
	format "\t%\n" luaFileName
	luaFile = openfile luaFileName mode:"w+"
	format "set_auto_texdict( false )\n" to:luaFile
	close luaFile
	true
)

-- pipeline/export/peds/utils.ms
