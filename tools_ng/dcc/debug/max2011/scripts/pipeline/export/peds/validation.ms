-- Rockstar Ped Test
-- Rockstar North
-- 23/11/2005
-- by Greg Smith

filein "pipeline/util/string.ms"
filein "rockstar/util/anim.ms"
filein "pipeline/util/file.ms"

struct pedobjwarning ( object, errmsg )
RsPedWarnings = #()
RsBatchWarnings = #()
RsPedErrors = #() 

--------------------------------------------------------------
-- 
--------------------------------------------------------------		
fn RsCheckPedExportRec obj = (

	if classof obj != Col_Capsule and classof obj != Col_Box then (

		nameCheck = RsLowercase obj.name

--		if obj.name != "Char Footsteps" then (
		if findstring namecheck "footsteps" == undefined then (
		
			val = getuserprop obj "tag"
			
			if val == undefined then (
			
				if obj.name.count > 2 then (
			
					lastbit = RsLowercase(substring obj.name (obj.name.count - 2) 3)
				
					if lastbit != "nub" then (
				
						pedWarning = pedobjwarning obj (obj.name + " doesnt have a tag")
						append RsPedWarnings pedWarning
					)
				)
			)
		)
	)

	for childobj in obj.children do (
	
		RsCheckPedExportRec childobj
	)
)

--------------------------------------------------------------
-- 
--------------------------------------------------------------
fn RsCheckPedExport obj = (

	RsPedWarnings = #()

	RsCheckPedExportRec obj
	
	if RsPedWarnings.count > 0 then (
	
		global pedWarningNames = #()
	
		for obj in RsPedWarnings do (
		
			append pedWarningNames obj.errmsg
		)
	
		rollout RsPedWarningsRoll "Ped Export Warnings"
		(
			listbox lstMissing items:pedWarningNames
			button btnOK "OK" width:100 pos:[150,150]

			on btnOK pressed do (

				DestroyDialog RsPedWarningsRoll
			)	
		)
	
		CreateDialog RsPedWarningsRoll width:400 modal:true
	)
)


fn RsCheckBatchExport = (

	if RsBatchWarnings.count > 0 then (
		
		
		global tempArr = #()
		for item in RsBatchWarnings do (
			append tempArr item

		)
		
		rollout RsBatchWarningsRoll "Batch Export Warnings"
		(
			listbox lstMissing1 items:tempArr
			button btnOK "OK" width:100 pos:[150,150]

			
			
			on btnOK pressed do (

				DestroyDialog RsBatchWarningsRoll
				
			)
			
		)

		CreateDialog RsBatchWarningsRoll width:1000 modal:true			


	)
	RsBatchWarnings = #()

)

fn RsCheckGenericExport = (

	if RsPedErrors.count > 0 then (
		
		
		global tempArr = #()
		for item in RsPedErrors do (
			append tempArr item

		)
		
		rollout RsErrorsRoll "Export Errors"
		(
			listbox lstMissing1 items:tempArr
			button btnOK "OK" width:100 pos:[150,150]

			
			
			on btnOK pressed do (

				DestroyDialog RsErrorsRoll 
				
			)
			
		)

		CreateDialog RsErrorsRoll width:400 modal:true			


	)
	RsPedErrors = #()

)

--------------------------------------------------------------
--  Check that the texture file exists
--------------------------------------------------------------	
fn RsCheckForPedTex testfilename = (
	pedtexFile = substring testfilename 1 (testfilename.count - 3)
	pedtexFile = pedtexFile + "pedtex"
	if openfile pedtexFile != undefined then (
		return true
	) else (
		batchWarning = maxfilename + " has no pedtex file and was not exported."
		append RsBatchWarnings batchWarning	
		return false
		)

)

--------------------------------------------------------------
-- Check that the texture has alright dimensions (power of 2)
--------------------------------------------------------------	
fn RsCheckTexDimension texlist = (
	global RsPedTexErrors = #()
	
	for tex in texlist do (
		if RsFileExist tex then (
			valid = true
			b = openBitmap tex
			
			-- Check the dimensions
			if ( ( bit.and b.width (b.width - 1) ) != 0 ) then valid = false
			if ( ( bit.and b.height (b.height - 1) ) != 0 ) then valid = false
			
			if not valid then (
				message = tex + " ( " + (b.width as string) + "x" + (b.height as string) + " )"
				append RsPedTexErrors message
			)
		)
	)
	
	-- If we found errors, display them
	if (RsPedTexErrors.count > 0) then (
		rollout RsTexWarningsRoll "Found textures with invalid dimensions!"
		(
			listbox lstBadTex items:RsPedTexErrors
			button btnOK "OK" width:100 

			on btnOK pressed do (
				DestroyDialog RsTexWarningsRoll
			)	
		)
	
		CreateDialog RsTexWarningsRoll width:600 modal:true
		return false
	)
	else (
		return true
	)
)

fn RsCheckForCollision = (
	
	select $*
	for sel in selection do (
		if classof(sel) == Col_Capsule then (
			return true
		)
	)
	
	batchWarning = maxfilename + " does not have collisions set up."
	append RsBatchWarnings batchWarning
	return false
)

fn RsCheckFileHasLoaded testfilename = (
	if loadMaxFile testfilename == false then (
		
		batchWarning = testfilename + " failed to load."
		append RsBatchWarnings batchWarning
		return false
	
	)
	return true
)

fn RsCheckForDummy = (
	select $*
	
	for sel in selection do (
		if classof(sel) == Dummy then (
			if RsLowercase(sel.name) + ".max" == RsLowercase(maxfilename) then (
				clearSelection()
				select sel
				return true
			
				
			)
		)
	)
	batchWarning = maxfilename + " failed to export.   Missing Dummy?"
	append RsBatchWarnings batchWarning
	return false
	
)

fn RsAddGenericBatchError batchWarning = (
	print "Adding generic warning"
	append RsBatchWarnings batchWarning
	return true
)

fn RsCheckForRollBonesInCsPedExport = (
	
	boneList = #()
	rollboneList = #()
	boneroot = $Char
	RsGetBoneList  boneroot boneList 
	
	for bone in boneList do (
		
		if matchPattern bone.name pattern:"*Roll*" then (
			append RsPedErrors ("Biped contains roll bone " + bone.name + ".  Invalid for a CS character")
		)
	)
	if RsPedErrors.count > 0 then return false
	else return true
)

	
	