--
-- File:: pipeline/export/peds/ruby_script_generator.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/models/globals.ms"

-----------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------


--
-- struct: RsPedScriptGeneratorStruct
-- desc: Ped Ruby script generator container for functions.
--
struct RsPedScriptGeneratorStruct
(

	--
	-- name: RunScript
	-- desc: Run a Ruby script with optional arguments.
	--
	fn RunScript rubyFileName args:"" = (
	
		local commandLine = stringStream ""
		format "ruby \"%\" %" rubyFilename args to:commandLine

		DOSCommand commandLine
	),


	--
	-- name: GenerateResourceCommonHeader
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonHeader rubyFile = (
		
		local projname = RsConfigGetProjectName()
		format "require 'pipeline/config/projects'\n" to:rubyFile
		format "Pipeline::Config::instance().logtostdout = true\n" to:rubyFile
		format "require 'pipeline/config/project'\n" to:rubyFile
		format "require 'pipeline/content/content_core'\n" to:rubyFile
		format "require 'pipeline/gui/exception_dialog'\n" to:rubyFile
		format "require 'pipeline/os/file'\n" to:rubyFile
		format "require 'pipeline/projectutil/data_convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/convert'\n" to:rubyFile
		format "require 'pipeline/resourcing/util'\n" to:rubyFile
		format "require 'pipeline/util/rage'\n" to:rubyFile
		format "require 'pipeline/util/string'\n" to:rubyFile
		format "include Pipeline\n\n" to:rubyFile

		format "begin\n\n" to:rubyFile
		format "\tproject = Pipeline::Config.instance.projects[\"%\"]\n" projname to:rubyFile
		format "\tproject.load_config( )\n" to:rubyFile
		format "\tbranch = project.branches[project.default_branch]\n" to:rubyFile
		format "\tr = RageUtils::new( project, branch.name )\n\n" to:rubyFile
	),

	--
	-- name: GenerateResourceCommonFooter
	-- desc:
	--
	-- note: Should not be called externally.
	--
	fn GenerateResourceCommonFooter rubyFile = (

		format "rescue Exception => ex\n" to:rubyFile
		format "\tGUI::ExceptionDialog::show_dialog( ex )\n" to:rubyFile
		format "end\n\n" to:rubyFile 	
	),	
	
	--
	-- name: BuildStreamedPedsRpf
	-- desc:
	--
	fn BuildStreamedPedsRpf rubyFileName = (
	
		RsMakeSurePathExists rubyFileName
		local rubyFile = createFile rubyFileName
		local asset_path = ( RsConfigGetStreamDir() + "streamedpeds/" )
	
		GenerateResourceCommonHeader rubyFile
		
		format "\tproject.load_content( nil, Project::ContentType::OUTPUT )\n" to:rubyFile
		format "\tstreamedpeds_rpf = project.content.find_by_script( \"'streamedpeds' == content.name  && 'rpf' == content.xml_type\" )\n" to:rubyFile
		format "\tr.pack.start_uncompressed()\n\n" to:rubyFile
		
		format "\tplayer_dirs = OS::FindEx::find_dirs( '%' )\n" asset_path to:rubyFile 
		format "\tplayer_dirs.each do |player|\n\n" to:rubyFile
		
		format "\t\tplayer_name = OS::Path::get_filename( player )\n\n" to:rubyFile
		
		format "\t\t#Add player fragment\n" to:rubyFile
		format "\t\tift_filename = \"#{player}.ift\"\n" to:rubyFile
		format "\t\tr.pack.add( ift_filename, nil )\n\n" to:rubyFile
		
		format "\t\t#Add player components\n" to:rubyFile
		format "\t\tplayer_files = OS::FindEx::find_files( OS::Path::combine( player, '*.i*' ), true )\n" to:rubyFile
		format "\t\tplayer_files.each do |player_file|\n\n" to:rubyFile
		format "\t\t\tr.pack.add( OS::Path::combine( player, player_file ), OS::Path::combine( player_name, player_file ) )\n" to:rubyFile
		format "\t\tend\n" to:rubyFile
		format "\tend\n\n" to:rubyFile
		
		format "\tr.pack.save( streamedpeds_rpf[0].filename )\n" to:rubyFile
		format "\tr.pack.close( )\n\n" to:rubyFile
		
		format "\tProjectUtil::data_convert_file( streamedpeds_rpf[0].filename )\n\n" to:rubyFile
		
		GenerateResourceCommonFooter rubyFile
		
		close rubyFile
		RunScript rubyFileName		
	)
	
)


-- Create instances of our script generator struct.
global RsPedScriptGenerator = RsPedScriptGeneratorStruct()

-- pipeline/export/peds/ruby_script_generator.ms
