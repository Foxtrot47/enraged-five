--
-- File:: project.ms
-- Description:: Project configuration data.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Author:: Luke Openshaw <luke.openshaw@rockstarnorth.com>
-- Date:: 5 February 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "pipeline/util/environment.ms"
include "pipeline/util/file.ms"
include "pipeline/util/xml.ms"

-----------------------------------------------------------------------------
-- Data
-----------------------------------------------------------------------------
XML_TAG_GROUP = "target"
XML_ATTR_ENABLED = "enabled"
XML_ATTR_PLATFORM = "platform"

-----------------------------------------------------------------------------
-- Code
-----------------------------------------------------------------------------

--
-- name: Target
-- desc:
--
-- note: This structure does not currently maintain a link back to the
--       associated project as the current Ruby equivalent does.
--
struct Target
(
	platform, 		-- Platform string
	target,				-- Target directory string
	enabled,			-- Target enabled boolean flag
	ragebuilder,	-- ragebuilder exe
	rboptions,		-- ragebuilder options
	
	-------------------------------------------------------------------------
	-- Public Methods
	-------------------------------------------------------------------------

	fn Print = (
	
		format "--------------------------------------------------------------------------------\n"
		format " Target\n"
		format "--------------------------------------------------------------------------------\n"
		format "\n"
		format "\tplatform:   %\n" platform
		format "\ttarget:     %\n" target
		format "\tenabled:    %\n" enabled
		format "\n"
		format "--------------------------------------------------------------------------------\n"
		format " End of Target\n"
		format "--------------------------------------------------------------------------------\n"
	)
)

--
-- name: Attribute
-- desc: Struct for passing attributes when building new elements.  
--
struct Attribute (name, value)


--
-- name: Project
-- desc:
--
struct Project
(
	globalConfig,					-- Global config XML filename
	localConfig,					-- Local config XML filename

	-------------------------------------------------------------------------
	-- Public Configuration
	-------------------------------------------------------------------------

	name,								-- Project name
	uiname,							-- Project userfriendly / UI display name	
	root,								-- Project root directory
	abproj,							-- Project Alienbrain project
	
	localdir,						-- Project local output directory (local is keyword!)
	network,						-- Project network output directory
	nettexture,					-- Project network texture resource directory
	netstream,					-- Project stream resource directory
	netgenstream,				-- Project network generic stream resource directory
	netsttexture,				-- Project network speed tree texture resource directory
	build,
	independent,
	common,
	shaders,
	art,							-- Root of the art path
	
	isepisodic,						-- Is project episodic?
	haslevels,						-- Does project have levels?
		
	targets = #(),					-- Array of Target objects
	maps = #(),						-- Array of MapContent objects
	peds = #(),						-- Array of PedContent objects

	mapsource = #(),				-- Array of map source XML files
	vehiclesource = #(),			-- Array of vehicle source XML files
	pedsource = #(),				-- Array of ped source XML files
	loadedConfig = false,			-- Project config loaded flag (false if error)
	showexportlog = false,
	forcetextureexport = false,

	-------------------------------------------------------------------------
	-- Private Data (erm, thats means don't use it!  no really...)
	-------------------------------------------------------------------------
	
	__environment = Environment(),	-- Environment Table
	__global,
	__local,
	__xmlGlobalRoot,
	__xmlLocalRoot,
	

	-------------------------------------------------------------------------
	-- Public Methods
	-------------------------------------------------------------------------

	--
	-- name: print
	-- desc: Print project object out to the listener, useful for debugging.
	--
	fn Dump = (
	
		format "--------------------------------------------------------------------------------\n"
		format " Project\n"
		format "--------------------------------------------------------------------------------\n"
		format "\n"
		format "\tGlobal File:  %\n" globalConfig
		format "\tLocal File:   %\n" localConfig
		format "\n"
		format "\tloadedConfig: %\n" loadedConfig
		format "\n"
		format "\tuame:         %\n" name
		format "\tuiname:       %\n" uiname
		format "\troot:         %\n" root
		format "\tabproj:       %\n" abproj
		format "\tlocal:        %\n" localdir
		format "\tnetwork:      %\n" network
		format "\tart:          %\n" art
		format "\tbuild:        %\n" build
		format "\tindependent:  %\n" independent
		format "\tcommon:       %\n" common
		format "\tshaders:      %\n" shaders
		format "\n"

		__environment.Print()
	
		format "\n"
		format "\t# Targets: %\n" targets.count
		for target in targets do
		(
			target.Print()
		)	
		
		format "\n"
		if ( undefined != vehicleconf ) then
			vehicleconf.Print()
		
		format "--------------------------------------------------------------------------------\n"
		format " End of Project\n"
		format "--------------------------------------------------------------------------------\n"
	),
	
	--
	-- name: GetDefaultBranch
	-- desc: Return default branch key String.
	--
	fn GetDefaultBranch configroot = (
		
		-- Get branch specific content.  Always use the default
		branchesRoot = RsGetXmlElement configroot "branches"

		if branchesRoot != undefined then (
			local defaultBranch = branchesRoot.Attributes.ItemOf( "default" )
			local childNodeList = branchesRoot.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(

				local elem = childNodeList.ItemOf( nChild )
				if elem.Attributes != undefined then (
					local branchNameAtt = elem.Attributes.ItemOf( "name" )

					if ( defaultBranch.Value == branchNameAtt.Value ) then
					(
						return elem
					)
				)
			)
		)
		undefined
	),

	--
	-- name: envsubst
	-- desc: Substitute any environment variables in input string and return 
	--		 result
	--
	fn envsubst input = (
	
		( __environment.subst input )
	),

	--
	-- name: SetTargetEnabled
	-- desc: Set a platform target's enabled flag to true or false
	--
	fn SetTargetEnabled platform enabled = (
	
		if ( BooleanClass != classof enabled ) then
		(
			messageBox "Error updating platform's enabled flag.  Enabled flag must be true or false."
			return ( false )
		)
		
		-- Find target child
		local childNodeList = __xmlLocalRoot.ChildNodes
		for nChild = 0 to ( childNodeList.Count - 1 ) do
		(
		
			local elem = childNodeList.ItemOf( nChild )
			
			-- Client targets settings
			if ( "targets" == elem.name ) then
			(
			
				local targetChildList = elem.ChildNodes
				for nTargetChild = 0 to ( targetChildList.Count - 1 ) do
				(
	
					local targetElem = targetChildList.ItemOf( nTargetChild )
					local attrPlatform = targetElem.Attributes.ItemOf( "platform" )
					local attrEnabled = targetElem.Attributes.ItemOf( "enabled" )
					
					--if ( attrPlatform.Value == platform ) then
					--(
					--	for target in targets do
					--	(
					--	
					--		if ( target.platform == platform ) then
					--		(
					--		
					--			attrEnabled.Value = ( enabled as string )
					--			target.enabled = enabled
					--			
					--			__local.save localConfig
					--			return ( true )
					--		)
					--	)
					--	
					--)
				)	
			)
			
			
		)
		
		false
	),

	-------------------------------------------------------------------------
	-- Private Methods
	-------------------------------------------------------------------------	
	
	--
	-- name: __parseSources
	-- desc: Parse project XML file's sources nodes
	--
	fn __parseSources elem = (
	
		local sourcesChildList = elem.ChildNodes
		for nSource = 0 to ( sourcesChildList.Count - 1 ) do
		(
		
			local sourceElem = sourcesChildList.ItemOf( nSource )
			if ( "source" != sourceElem.name ) then
				continue
						
			-- Assume source node, check its type and pass to specific
			-- source function to parse XML.
			local typeAttr = sourceElem.Attributes.ItemOf( "type" )
			local includeElem = sourceElem.ChildNodes.ItemOf( 0 )
			if ( undefined == includeElem ) then
				continue
			
			local hrefAttr = includeElem.Attributes.ItemOf( "href" )
			local srcpath = ""
			local tgtpath = ""
			--if ( "maps" == typeAttr.Value ) then
			--(
				
			--)
			--else if ( "vehicles" == typeAttr.Value ) then
			--(
			--
			--	local sourceFilename = ( __environment.subst hrefAttr.Value )
			--	vehicleconf = VehicleConfig()
			--	vehicleconf.load sourceFilename __environment
			--)
			-- TODO: add other asset types
		)
	),
	

	--
	-- name: __parseGlobalConfig
	-- desc:
	--	
	fn __parseGlobalConfig = (
		
		format "!!!!!RsConfig::::: %\n" RsConfig
		
		-- Prepopulate our environment with a couple of entries from
		-- our previously configured data.
		__environment.add "root" root
		__environment.add "filepath" (RsRemoveFilename globalConfig)
		__environment.add "toolsbin" RsConfig.toolsbin
		
		-- Fetch top-level attributes
		--try
		(
			local projAttrLocal = __xmlGlobalRoot.Attributes.ItemOf( "local" )
			local projAttrNetwork = __xmlGlobalRoot.Attributes.ItemOf( "network" )
			
			-- Default network dirs
			local projAttrNetTex = __xmlGlobalRoot.Attributes.ItemOf( "nettexture" )
			local projAttrNetStream = __xmlGlobalRoot.Attributes.ItemOf( "netstream" )
			local projAttrNetGenStream = __xmlGlobalRoot.Attributes.ItemOf( "netgenstream" )
			local projAttrNetSTTexture = __xmlGlobalRoot.Attributes.ItemOf( "netsttexture" )
						
			local projAttrHasLevels = __xmlGlobalRoot.Attributes.ItemOf( "has_levels" )
			local projAttrIsEpisodic = __xmlGlobalRoot.Attributes.ItemOf( "is_episodic" )
		
			
			-- Set our local variables, adding to environment and passing to
			-- environment.
			localdir = ( __environment.subst projAttrLocal.Value )
			__environment.add "local" localdir
			
			network = ( __environment.subst projAttrNetwork.Value )
			__environment.add "network" network
			
			nettexture = ( __environment.subst projAttrNetTex.Value )
			__environment.add "nettexture" nettexture
			
			netstream = ( __environment.subst projAttrNetStream.Value )
			__environment.add "netstream" netstream
			
			netgenstream = ( __environment.subst projAttrNetGenStream.Value )
			__environment.add "netgenstream" netgenstream
			
			netsttexture = ( __environment.subst projAttrNetSTTexture.Value )
			__environment.add "netsttexture" netsttexture
						
			-- Optional "levels" attribute.
			if ( undefined != projAttrHasLevels ) then
				haslevels = projAttrHasLevels.Value
			else
				haslevels = false
			
			-- Optional "isepisodic" attribute
			if ( undefined != projAttrIsEpisodic ) then
				isepisodic = projAttrIsEpisodic.Value
			else
				isepisodic = false
			
			--try
			(
				local projAttrBranchName
				local projAttrArt
				local projAttrBuild 
				local projAttrIndependent 
				local projAttrCommon 
				local projAttrShaders
								
				local branchParamsFound = false
				
				branchesRoot = RsGetXmlElement __xmlGlobalRoot "branches"
				if branchesRoot != undefined then (
					local defaultBranch = branchesRoot.Attributes.ItemOf( "default" )
					local childNodeList = branchesRoot.ChildNodes
					for nChild = 0 to ( childNodeList.Count - 1 ) do
					(
						
						local elem = childNodeList.ItemOf( nChild )
						if elem.Attributes != undefined then (
							local branchNameAtt = elem.Attributes.ItemOf( "name" )
							
							if ( defaultBranch.Value == branchNameAtt.Value ) then
							(
								projAttrBranchName = branchNameAtt
								projAttrArt = elem.Attributes.ItemOf( "art" )
								projAttrBuild = elem.Attributes.ItemOf( "build" )
								projAttrIndependent = elem.Attributes.ItemOf( "independent" )
								projAttrCommon = elem.Attributes.ItemOf( "common" )
								projAttrShaders = elem.Attributes.ItemOf( "shaders" )
								
								branchParamsFound = true
							)
						)
					)
				)
				
				if branchParamsFound == true then (
					
					branch = ( __environment.subst projAttrBranchName.Value )
					__environment.add "branch" branch					

					art = ( __environment.subst projAttrArt.Value )
					__environment.add "art" art

					build = ( __environment.subst projAttrBuild.Value )
					__environment.add "build" build

					independent = ( __environment.subst projAttrIndependent.Value )
					__environment.add "independent" independent

					common = ( __environment.subst projAttrCommon.Value )
					__environment.add "common" common

					shaders = ( __environment.subst projAttrShaders.Value )
					__environment.add "shaders" shaders
				)
				else (
					messagebox "No default branch for this project.  Contact tools."
				)
			)
			--catch
			(
			--	ss = stringStream ""
			--	format "Error reading default branch attributes in __parseGlobalConfig: %." ( getCurrentException() ) to:ss
			--	MessageBox ( ss as string )
			)
			
		)
		--catch
		(
		--	ss = stringStream ""
		--	format "Error reading project attributes in __parseGlobalConfig: %." ( getCurrentException() ) to:ss
		--	MessageBox ( ss as string )
		)
		
		local defaultBranchElem = GetDefaultBranch __xmlGlobalRoot
	
		-- Parse child nodes for additional project config stuff.
		local childNodeList = defaultBranchElem.ChildNodes
		for nChild = 0 to ( childNodeList.Count - 1 ) do
		(

			local elem = childNodeList.ItemOf( nChild )
			if ( "targets" == elem.name ) then
			(

				local targetChildList = elem.ChildNodes
				for nTargetChild = 0 to ( targetChildList.Count - 1 ) do
				(
	
					local targetElem = targetChildList.ItemOf( nTargetChild )
					local attrPlatform = targetElem.Attributes.ItemOf( "platform" )
					local attrDirectory = targetElem.Attributes.ItemOf( "path" )
					
					local platform = ( __environment.subst attrPlatform.Value )
					local directory = ( __environment.subst attrDirectory.Value )
										
					append targets ( Target platform directory false )
				)
			)
			else if ( "sources" == elem.name ) then
			(
				__parseSources elem
			)
			else if ( "rage" == elem.name ) then (
				-- The ragebuilder data could really live with the targets in the config.xml since
				-- there is a 1-1 relationship
				local rageChildList = elem.ChildNodes
				for i = 0 to ( rageChildList.count - 1 ) do (
					local rageChildElem = rageChildList.ItemOf( i )
					if ( "ragebuilders" == rageChildElem.name ) then (
				
						local ragebuilderChildlist = rageChildElem.ChildNodes
						for j = 0 to ( ragebuilderChildlist.count - 1 ) do (
							local ragebElem = ragebuilderChildlist.ItemOf( j )
							local attrPlatform = ragebElem.Attributes.ItemOf( "platform" )
							local attrExe = ragebElem.Attributes.ItemOf( "exe" )
							local attrOptions = ragebElem.Attributes.ItemOf( "options" )

							for target in targets do (
								if target.platform == attrPlatform.Value then (
									platrb = ( __environment.subst attrExe.Value )
									target.ragebuilder = platrb
									target.rboptions = attrOptions.Value
								)
								
							)
						)
					)
				)
			)
		)	
	),

	--
	-- name: __parseLocalConfig
	-- desc:
	--	
	fn __parseLocalConfig = (
		
		-- Get root project stream directories
		local projAttrNetTex = __xmlLocalRoot.Attributes.ItemOf( "nettexture" )
		local projAttrNetStream = __xmlLocalRoot.Attributes.ItemOf( "netstream" )
		local projAttrNetGenStream = __xmlLocalRoot.Attributes.ItemOf( "netgenstream" )
		local projAttrNetSTTexture = __xmlLocalRoot.Attributes.ItemOf( "netsttexture" )

		if projAttrNetTex != undefined then nettexture = projAttrNetTex.Value
		if projAttrNetStream != undefined then netstream = projAttrNetStream.Value
		if projAttrNetGenStream != undefined then netgenstream = projAttrNetGenStream.Value
		if projAttrNetSTTexture != undefined then netsttexture = projAttrNetSTTexture.Value
		
		
		-- Get branch specific content.  Always use the default
		local defaultBranchElem = GetDefaultBranch __xmlLocalRoot
		
		
		-- Parse default branch child nodes for additional local config stuff
		if defaultBranchElem == undefined then (
			messagebox "No default branch for current project.  Contact tools"
		)
		else (
			
			local childNodeList = defaultBranchElem.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(
				
				local elem = childNodeList.ItemOf( nChild )

				-- Get project target enabled flags
				if ( "targets" == elem.name ) then
				(

					local targetChildList = elem.ChildNodes
					for nTargetChild = 0 to ( targetChildList.Count - 1 ) do
					(
						
						local targetElem = targetChildList.ItemOf( nTargetChild )
						local attrPlatform = targetElem.Attributes.ItemOf( "platform" )
						local attrEnabled = targetElem.Attributes.ItemOf( "enabled" )

						local platform = attrPlatform.Value
						local enabled = attrEnabled.Value
						
						
						-- Find target and set enabled flag correctly
						for target in targets do
						(

							if ( target.platform == platform ) then
								target.enabled = ( enabled == "true" )
							
						)
					)				
				)
				
				-- Get all setting flags (previously this has bene a global setting across all projects)
				else if ( "settings" == elem.name ) then
				(

					local projAttrShowLog  = elem.Attributes.ItemOf( "showexportlog" )
					local projAttrForceTextureExport  = elem.Attributes.ItemOf( "forcetextureexport" )



					showexportlog = projAttrShowLog.Value
					forcetextureexport = projAttrForceTextureExport.Value

				)


				-- Currently other 2nd level nodes are ignored
			)
		)
		
	),
	
	--
	-- name: UpdateLocalConfig
	-- desc: update local config
	fn SubEnv path = (
		resolvedpath = __environment.subst path
		resolvedpath
	),
	
	--
	-- name: UpdateLocalConfig
	-- desc: update local config
	-- inputs: obj 
	fn UpdateLocalConfig obj = (
		objclass = classof obj
		
		if objclass == Target then (
			local defaultBranchElem = GetDefaultBranch __xmlLocalRoot
			local childNodeList = defaultBranchElem.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(

				local elem = childNodeList.ItemOf( nChild )

				-- Get project target enabled flags
				if ( "targets" == elem.name ) then
				(

					local targetChildList = elem.ChildNodes
					for nTargetChild = 0 to ( targetChildList.Count - 1 ) do
					(
						local targetElem = targetChildList.ItemOf( nTargetChild )
						local attrPlatform = targetElem.Attributes.ItemOf( "platform" )
						local attrEnabled = targetElem.Attributes.ItemOf( "enabled" )
											
						local platform = attrPlatform.Value
						
						-- Update enabled attribute
						if ( platform == (obj.platform as string)) then (
							attrEnabled.Value = obj.enabled as string
						)
						
					)
				)
			)
		)
		-- If undefined class then we are dealing with self
		-- Blanket update of all network project path settings
		else if objclass == UndefinedClass then (
						
			attr = __local.createattribute "nettexture"
			attr.value = nettexture
			__xmlLocalRoot.Attributes.append(attr)
			
			attr = __local.createattribute "netstream"
			attr.value = netstream
			__xmlLocalRoot.Attributes.append(attr)
			
			attr = __local.createattribute "netgenstream"
			attr.value = netgenstream
			__xmlLocalRoot.Attributes.append(attr)
			
			attr = __local.createattribute "netsttexture"
			attr.value = netsttexture
			__xmlLocalRoot.Attributes.append(attr)
			
			
			local projselemfound = false
			local settingsfound = false
			
			local defaultBranchElem = GetDefaultBranch __xmlLocalRoot
			local childNodeList = defaultBranchElem.ChildNodes
			for nChild = 0 to ( childNodeList.Count - 1 ) do
			(

				local elem = childNodeList.ItemOf( nChild )
				/*
				-- Get project target enabled flags
				if ( "projects" == elem.name ) then
				(
					projselemfound = true
					
					local thisprojelemfound = false
					local targetChildList = elem.ChildNodes
					for nTargetChild = 0 to ( targetChildList.Count - 1 ) do
					(
						local targetElem = targetChildList.ItemOf( nTargetChild )
						local attrName = targetElem.Attributes.ItemOf( "name" )
	
						local projname = attrName.Value

						-- Update enabled attribute
						if ( projname == name) then (
							thisprojelemfound = true
							
							local attrNetTexture = targetElem.Attributes.ItemOf( "nettexture" )
							local attrNetStream = targetElem.Attributes.ItemOf( "netstream" )
							local attrNetGenStream = targetElem.Attributes.ItemOf( "netgenstream" )
							local attrNetSTTexture = targetElem.Attributes.ItemOf( "netsttexture" )
							
							-- Setting absolute paths 
							attrNetTexture.Value = nettexture
							attrNetStream.Value = netstream
							attrNetGenStream.Value = netgenstream
							attrNetSTTexture.Value = netsttexture

						)

					)
					-- old format local xml and project entries may need to be added individually
					-- we should be able to remove this before going live
					if thisprojelemfound == false then ( 
						local nameattr = Attribute "name" name
						local nettextureattr = Attribute "nettexture" nettexture
						local netstreamattr = Attribute "netstream" netstream
						local netgenstreamattr = Attribute "netgenstream" netgenstream
						local netsttextureattr = Attribute "netsttexture" netsttexture
						
						attributearray = #(nameattr, nettextureattr, netstreamattr, netgenstreamattr, netsttextureattr)
						
						elem.AppendChild( RsCreateXmlElement "project" attributearray __local )
					)
				)
				*/
				if ( "settings" == elem.name ) then
				(
					settingsfound = true
					
					local projAttrShowLog  = elem.Attributes.ItemOf( "showexportlog" )
					local projAttrForceTextureExport  = elem.Attributes.ItemOf( "forcetextureexport" )
					
					projAttrShowLog.Value = showexportlog as string
					projAttrForceTextureExport.Value = forcetextureexport as string
				)
			)
			
			-- old format local xml
			if settingsfound == false then (
				attributearray = #()
				local showexportlogattr = Attribute "showexportlog" showexportlog
				local forcetextureexportattr = Attribute "forcetextureexport" forcetextureexport
				
				append attributearray showexportlogattr
				append attributearray forcetextureexportattr
				
				defaultBranchElem.AppendChild( RsCreateXmlElement "settings" attributearray __local )
			)
		)
		__local.save localConfig
		
	),
	

	
	--
	-- name: __load
	-- desc:
	--
	fn __load loadlocal = (
		__global = XmlDocument()	
		__global.init()
		__global.load globalConfig
		__local = XmlDocument()	
		__local.init()
		__local.load localConfig
		
		__xmlGlobalRoot = __global.document.DocumentElement
		__xmlLocalRoot = __local.document.DocumentElement
		if ( "project" != __xmlGlobalRoot.name ) then
		(
			messageBox "Error parsing project RSN Pipeline config.  Invalid XML start tag."
			return ( false )
		)
		if ( "local" != __xmlLocalRoot.name ) then
		(
			messageBox "Error parsing local project RSN Pipeline config.  Invalid XML start tag."
			return ( false )
		)
		
		__parseGlobalConfig()
		if true == loadlocal then __parseLocalConfig()
		
		
		loadedConfig = true
	),

	--
	-- name: __save
	-- desc:
	--	
	fn __save = (
		messageBox "Project.save not implemented!"
	)
)

-- End of project.ms
