--
-- File:: pipeline/export/playercoords.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 May 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
include "rockstar/export/settings.ms"
include "pipeline/export/maps/globals.ms"
include "pipeline/export/maps/objects.ms"

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

if ( 0 == selection.count ) then
(
	MessageBox "Select the object whose coordinates you want exported."
)
else
(
	local maps = RsMapGetMapContainers()
	if ( 0 == maps.count ) then
	(
		MessageBox "Invalid map container.  Aborting Player Coordinate export."
	)
	else
	(
		RsMapSetupGlobals maps[1]

		buildDir = ( RsConfigGetProjRootDir() + "build/dev/" )
		RsMakeSurePathExists buildDir
		
		local filess = stringStream ""
		format "%playercoords_%.txt" buildDir RsMapLevel to:filess
		
		local filename = ( filess as string )
		local fp = createFile filename
		if ( undefined != fp ) then
		(
			obj=selection[1]
			x=obj.position.x
			y=obj.position.y
			z=obj.position.z+1.0
			format "% % %" x  y  z to:fp
			close fp
			format "Player coordinates written to: %\n" filename
		)
		else
		(
			local msg = stringStream ""
			format "Failed to create player coordinates file: %." filename
		)
	)
)

-- pipeline/export/playercoords.ms
