--
-- File:: WeaponExporterAnimation.ms
-- Description:: Weapons exporter animation UI rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 29 April 2009
--

-- Weapon-specific includes
filein "pipeline/export/models/weapon.ms"
filein "pipeline/export/weapons/anim.ms"
filein "pipeline/export/weapons/globals.ms"
filein "pipeline/export/weapons/validation.ms"

-- Additional includes
filein "pipeline/export/ui/ExporterErrorDialog.ms"
filein "pipeline/export/ui/SharedTextureList.ms"
filein "rockstar/util/dialog.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsWeaponAnimRoll "Weapon Animation"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	group "Export"
	(
		button btnExportCurrent	"Export Current..."	width:100
		button btnExportAll 	"Export All"		width:100
		button btnTest			"Test Weapon"		width:100
	)
	group "Save/Load Animations:"
	(
		button btnSaveAs		"Save As..." 		width:100
		button btnLoad			"Load..."			width:100
	)
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None	

	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	--
	-- event: btnExportCurrent button click
	-- desc: Export currently weapon animation to a specified anim file.
	--
	on btnExportCurrent pressed do
	(
		local animFilename = ( getSaveFilename caption:"Save Anim As..." types:"Anim File (*.anim)|*.anim" )
		RsWeaponAnimExportCurrent animFilename
		
		if ( RsQueryBoxTimeOut "Resource clip dictionary?" ) then 
		(
			if ( false == RsWeaponAnimExportResource() ) then
				MessageBox "Weapon animation resource failed."
		)
	)

	--
	-- event: btnExportAll button click
	-- desc: Export all current weapon animations.
	--
	on btnExportAll pressed do
	(
		local maxFullFilename = ( maxfilepath + maxfilename )
		local weaponName = ( RsRemoveExtension maxfilename )
		local weaponFile = "weapons/" + weaponName
		local animSourceDir = maxfilepath + "anim\\" + weaponName + "\\"
		local animFiles = ( getFiles ( animSourceDir + "*.xaf" ) )
		
		local weaponName = ( RsRemoveExtension maxfilename )
		local animDestDir = ( RsConfigGetStreamDir() + "weapons/" + weaponName + "/anim/" )
		
		-- Loop through all of our XAF files.
		for animFile in animFiles do
		(
			if ( false == ( RsWeaponAnimLoad animFile ) ) then
			(
				MessageBox ( "Failed to load XAF: " + animFile + ".  Export aborted." )
				return ( false )
			)
			
			local animName = ( RsRemovePathAndExtension animFile )
			local animFullname = ( animDestDir + animName )
			format "Exporting weapon animation: %\n" animName
			RsWeaponAnimExportCurrent animFullname
		)
		
		if ( RsQueryBoxTimeOut "Resource clip dictionary?" ) then 
		(
			if ( false == RsWeaponAnimExportResource() ) then
				MessageBox "Weapon animation resource failed."
		)		
	)

	--
	-- event: btnTest button click
	-- desc: Test the weapon for export.
	--
	on btnTest pressed do
	(
		if ( RsWeaponAnimIsValidForExport() ) then
			MessageBox ( "Animated weapon tests passed." )
	)

	--
	-- event: btnSaveSelected button click
	-- desc: Save the current weapon animation to a XAF file.
	--
	on btnSaveAs pressed do
	(
		local animFilename = ( getSaveFilename caption:"Save Animation As..." types:"XML Animation File (*.xaf)|*.xaf" )
		local animResult = ( RsWeaponAnimSaveAs animFilename )
		if ( not animResult ) then
		(
			local ss = ( stringStream "" )
			format "Loading XAF anim to % failed." animFilename to:ss
			MessageBox (ss as String )
		)
	)
	
	--
	-- event: btnLoadSelected button click
	-- desc: Load a weapon animation onto the current model from a XAF file.
	--
	on btnLoad pressed do
	(
		local animFilename = ( getOpenFilename caption:"Load Animation..." types:"XML Animation File (*.xaf)|*.xaf" )
		local animResult = ( RsWeaponAnimLoad animFilename )
		if ( not animResult ) then
		(
			local ss = ( stringStream "" )
			format "Loading XAF anim to % failed." animFilename to:ss
			MessageBox (ss as String )
		)
	)
	
	--
	-- event: Rollout open event
	-- desc: Attach selection change event handler.
	--
	on RsWeaponAnimRoll open do
	(
		-- Save is currently disabled because they don't seem to load afterwards.
		--btnSaveAs.enabled = false
	)
	
	--
	-- event: Rollout close event
	-- desc: Detach selection change event handler.
	--
	on RsWeaponAnimRoll close do
	(
		RsSettingWrite "rsweaponanim" "rollup" (not RsWeaponAnimRoll.open)
	)
)

-- WeaponExporterAnim.ms
