--
-- File:: pipeline/export/ui/vehicle_exporter_ui.ms
-- Description:: Vehicle exporter UI rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- 
-- Based off of the original by Greg Smith.
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
-- Vehicle-specific includes
filein "pipeline/export/vehicle.ms"
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/export/vehicles/textures.ms"
filein "pipeline/export/vehicles/utils.ms"
filein "pipeline/export/vehicles/validation.ms"
filein "pipeline/export/vehicles/anim.ms"
filein "pipeline/export/ui/vehicle_settings_ui.ms"
filein "pipeline/export/ui/vehicle_exporter_ui_anim.ms"

-- Additional includes
filein "pipeline/export/ui/ExporterErrorDialog.ms"
filein "pipeline/export/ui/shared_texture_list_ui.ms"
filein "pipeline/util/file.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsVehicleRoll "Vehicle Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vehicle_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnAddLimit 		"Add Limits" width:100
	button btnDeleteLimit 	"Delete Limits" width:100
	button btnCreateSkin 	"Create Skinned" width:100
	button btnExportSkinned "Export Skinned" width:100
	button btnPatchExportSkinned "Preview Skinned" width:100
	button btnExportShared 	"Export Shared" width:100
	button btnBatchExportSkinned "Batch Skinned" width:100
	button btnBuildRPF 		"Build RPF" width:100
	button btnRebuildRPF 	"Rebuild RPF" width:100
	button btnExtractRPF 	"Extract RPF" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	--
	-- func: ExportSkinned pressed
	-- desc: Move export body to function so it can be called from by patch export button
	--
	fn ExportSkinned isPatch = (
		if RsVehicleIsValidSkin() == false then 
			return false

		if RsVehicleUtil.TagBones() == false then (
			ss = stringStream ""
			format "Failed to load bone ID XML file.  Make sure you have latest on % and try exporting again." ( RsConfigGetContentDir() ) to:ss
			messagebox ( ss as string )
			return false
		)

		-- Strip off the _skin part of the name for the created packfiles
		saveName = selection[1].name
		vehName = substring selection[1].name 1 (selection[1].name.count - 5)
		selection[1].name = vehName
		
		retval = false
		
		if RsVehicleExportModelSkinned() == true then (
			if RsVehicleResourceModelSkinned isPatch == true then (
				retval = true
			)
		)	

		selection[1].name = saveName
	
		-- If there is no vehicle hierarchy e.g. bison then select the bison_skel hierarchy
		vehicle = getnodebyname vehName
		if (vehicle == undefined) do (
		
			vehicle = getnodebyname (vehName + "_skel")
		)
		
		select vehicle
		
		if (retval != false) do (
		
			animList = #()
			RsGetVehicleAnimationList maxfilename animList

			if (animList.count > 0) do (
			
				for anim in animList do (

					RsVehicleAnimExport anim selection[1]
					RsVehicleAnimExportResource selection[1]		
				)
			)
		)
		
		select (getnodebyname saveName)
				
		retval
	)
	
	-------------------------------------------------------------------------
	-- Events
	-------------------------------------------------------------------------

	
	--
	-- event: btnAddLimit pressed
	-- desc: Add constraint helpers with default angles to the vehicle model.
	--
	on btnAddLimit pressed do (
	
		if selection.count != 1 then (
		
			messagebox "Select the root of the vehicle"
			return 0		
		)	
		vehNode = selection[1]
		RsVehicleUtil.RecAddConstraints vehNode vehNode
	)
	
	--
	-- event: btnDeleteLimits pressed
	-- desc: Remove all of the constraint helpers from the vehicle model.
	--
	on btnDeleteLimit pressed do (
		
		vehNode = selection[1]
		RsVehicleUtil.RecRemoveLimits vehNode
	)
	
	--
	-- event: btnCreateSkin pressed
	-- desc: set a mesh hierarchy up for exporting
	--
	on btnCreateSkin pressed do (
	
		if RsVehicleIsValidForExport() == false then return false
		
		srcobj = selection[1]		
		RsVehicleCreateSkinned srcobj
	)

	--
	-- event: btnExportSkinned pressed
	-- desc: skinned version of export
	--
	on btnExportSkinned pressed do (

		if (classof selection[1] != Editable_mesh) do (
		
			messagebox "Select the skin"
			return false
		)
		
		if ( false == ( ExportSkinned false ) ) then 
			return false
		
		local rubyFilename = ( RsVehicleSettings.GetScriptDir() + "build_rpf.rb" )
		if ( false == ( RsVehicleScriptGenerator.BuildRPF rubyFilename ) ) then
			return false
			
		true
	)
	
	--
	-- event: btnExportSkinned pressed
	-- desc: skinned version of export
	--
	on btnPatchExportSkinned pressed do (
		RsClearPatchDir()
		if ExportSkinned true == false then return false
	)	

	--
	-- event: btnExportShared pressed
	-- desc: Export shared texture dictionary
	--
	on btnExportShared pressed do (
		
		RsVehicleTextureList.reload()
		RsVehicleTruckTextureList.reload()
		
		local shared_textures = RsVehicleTextureList.textures
		local shared_truck_textures = RsVehicleTruckTextureList.textures
		local shared_dictionary = RsVehicleRPFs[RsVehicleRPFIndex].sharedtxd
		local shared_truck_dictionary = RsVehicleRPFs[RsVehicleRPFIndex].sharedtxd + "_truck"
		
		RsVehicleTexture.ExportShared shared_textures shared_dictionary
		RsVehicleTexture.ExportShared shared_truck_textures shared_truck_dictionary
	)
	
	--
	-- event: btnBatchExportSkinned pressed
	-- desc: Batch skinned export
	--
	on btnBatchExportSkinned pressed do (
		
		if ( false != RsVehicleSkinnedBatchExport() ) then 
		(
			local rubyFilename = ( RsVehicleSettings.GetScriptDir() + "build_rpf.rb" )
			if ( false == ( RsVehicleScriptGenerator.BuildRPF rubyFilename ) ) then
				return false
		)
		true
	)	

	--
	-- event: btnBuildRPF pressed
	-- desc: Build the vehicles RPF.
	--
	on btnBuildRPF pressed do (
	
		local rubyFilename = ( RsVehicleSettings.GetScriptDir() + "build_rpf.rb" )
		RsVehicleScriptGenerator.BuildRPF rubyFilename
	)

	--
	-- event: btnReBuildRPF pressed
	-- desc: Rebuild the vehicles RPF.
	--
	on btnReBuildRPF pressed do (
	
		local rubyFilename = ( RsVehicleSettings.GetScriptDir() + "rebuild_rpf.rb" )
		RsVehicleScriptGenerator.BuildRPF rubyFilename rebuild:true
	)
	
	--
	-- event: btnExtractRPF
	-- desc: Extract the vehicles RPF to local stream.
	--
	on btnExtractRPF pressed do (
	
		local rubyFilename = ( RsVehicleSettings.GetScriptDir() + "extract_rpf.rb" )
		RsVehicleScriptGenerator.ExtractRPF rubyFilename
	)
	
	--------------------------------------------------------------
	-- initialisation of rollout
	--------------------------------------------------------------
		
	--
	-- event: RsVehicleRoll close
	-- desc: shutdown of rollout
	--
	on RsVehicleRoll close do (
	
		RsSettingWrite "rsvehicle" "rollup" (not RsVehicleRoll.open)
	)	
)

--
-- name: RsVehicleSkinRoll
-- desc: Additional vehicle exporter rollout for help with custom skinning.
--
rollout RsVehicleSkinRoll "Vehicle Skinning"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Vehicle_Export#Vehicle_Skinning" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnSaveEnv "Save Envelopes" width:100
	button btnLoadEnv "Load Envelopes" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------	
	
	--
	-- name: RsVehicleSkinSelectionChange
	-- desc:
	--
	fn RsVehicleSkinSelectionChange = (
		if ( 1 == selection.count and undefined != selection[1] ) then
		(		
			local topModifier = selection[1].modifiers[#Skin]
			btnSaveEnv.enabled = ( Skin == classof topModifier )
			btnLoadEnv.enabled = ( Skin == classof topModifier )
		)
		else
		(
			btnSaveEnv.enabled = false
			btnLoadEnv.enabled = false
		)
	)
	
	--
	-- name: RsVehicleTexVariationChange
	-- desc: Function called when the loaded file changes so we can load the
	--       correct vehicles texture variation XML file.
	--
	fn RsVehicleTexVariationChange = (
		format "RsVehicleTexVariationChange\n"
		
		if ( ( undefined != maxFileName ) and ( "" != maxFileName ) ) then
		(
			-- Remove "_skinned" from filename.
			local maxname = maxFileName
			local idx = ( findString maxname "_skinned" )
			if ( undefined != idx ) then
				maxname = ( replace maxname idx "_skinned".count "" )
		
			RsVehicleTextureVariationFilename = ( maxFilePath + RsRemoveExtension( maxname ) + ".tex" )
			RsVehicleTextureVariationList = ( SharedTextureList RsVehicleTextureVariationFilename )
			format "Vehicle texture variations: %\n" RsVehicleTextureVariationFilename
						
			if ( 0 == ( getFileSize RsVehicleTextureVariationFilename ) ) then
				RsVehicleTextureVariationList.save()
			else
				RsVehicleTextureVariationList.reload()
		)
		else
		(
			RsVehicleTextureVariationFilename = undefined
			RsVehicleTextureVariationList = undefined
		)
		
		format "RsVehicleTextureVariationFilename: %\n" RsVehicleTextureVariationFilename
	)
		
	
	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------	
	
	--
	-- event: btnSaveEnv button pressed
	-- desc:
	--
	on btnSaveEnv pressed do
	(
		try
		(
			local filename = getSaveFilename caption:"Save Skin Envelopes As..." types:"Envelope ASCII File (*.envASCII)|*.envASCII"
			if ( undefined == filename ) then
				return
			
			format "Selection: %\n" selection[1]
			format "Modifier: %\n" selection[1].modifiers[#Skin]
			local topModifier = selection[1].modifiers[#Skin]
			if ( classof topModifier == Skin ) then
			(
				format "Skin: %\n" topModifier
				skinOps.saveEnvelopeAsASCII topModifier filename
			)
			else
			(
				MessageBox "Error: selected mesh does not have Skin modifier at top of stack."
			)
		)
		catch
		(
			MessageBox "Error: must have mesh selected with Skin modifier on top of stack."
			format "%\n" ( getCurrentException() )
		)
	)
	
	--
	-- event: btnLoadEnv button pressed
	-- desc:
	--
	on btnLoadEnv pressed do
	(
		try
		(
			local filename = getOpenFilename caption:"Load Skin Envelopes from..." types:"Envelope ASCII File (*.envASCII)|*.envASCII|All Files (*.*)|*.*"
			if ( undefined == filename ) then
				return
			
			format "Selection: %\n" selection[1]
			format "Modifier: %\n" selection[1].modifiers[#Skin]
			local topModifier = selection[1].modifiers[#Skin]
			if ( classof topModifier == Skin ) then
			(
				format "Skin: %\n" topModifier
				skinOps.loadEnvelopeAsASCII topModifier filename
			)
			else
			(
				MessageBox "Error: selected mesh does not have Skin modifier at top of stack."
			)
		)
		catch
		(
			MessageBox "Error: must have mesh selected with Skin modifier on top of stack."
			format "%\n" ( getCurrentException() )
		)
	)
	
	--
	-- event: Rollout opened event
	-- desc:
	--
	on RsVehicleSkinRoll open do
	(
		callbacks.addScript #selectionSetChanged "RsVehicleUtility.rollouts[4].RsVehicleSkinSelectionChange()" id:#RsVehicleSkinSelectionChange
		callbacks.addScript #filePostOpen "RsVehicleUtility.rollouts[4].RsVehicleTexVariationChange()" id:#RsVehicleTexVariationChange
		RsVehicleTexVariationChange()
		RsVehicleSkinSelectionChange()
	)
	
	--
	-- event: Rollout closed event
	-- desc:
	--
	on RsVehicleSkinRoll close do
	(
		callbacks.removeScripts #filePostOpen id:#RsVehicleTexVariationChange
		callbacks.removeScripts #selectionSetChanged id:#RsVehicleSkinSelectionChange
		RsSettingWrite "rsvehicleskin" "rollup" (not RsVehicleSkinRoll.open)
	)
)

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

variation_tex_list = RsCreateSharedTextureListRollout "RsVehicleTexVariationList" "Vehicle Variation Textures" RsVehicleTextureVariationFilename
shared_tex_list = RsCreateSharedTextureListRollout "RsVehicleTexList" "Shared Vehicle Textures" RsVehicleTextureListFilename
shared_tex_list_truck = RsCreateSharedTextureListRollout "RsTruckTexList" "Shared Truck Textures" RsVehicleTruckTextureListFilename

try CloseRolloutFloater RsVehicleUtility catch()
RsVehicleUtility = newRolloutFloater "RS Vehicle Exporter" 200 550 50 126
addRollout RsSettingsRoll RsVehicleUtility rolledup:(RsSettingsReadBoolean "rssettings" "rollup" false)
addRollout RsVehicleSettingsRoll RsVehicleUtility rolledup:(RsSettingsReadBoolean "rsvehiclesettings" "rollup" false)
addRollout RsVehicleRoll RsVehicleUtility rolledup:(RsSettingsReadBoolean "rsvehicle" "rollup" false)
addRollout RsVehicleSkinRoll RsVehicleUtility rolledup:(RsSettingsReadBoolean "rsvehicleskin" "rollup" false)
addRollout RsVehicleAnimRoll RsVehicleUtility rolledup:(RsSettingsReadBoolean "rsvehicleanim" "rollup" true)
addRollout variation_tex_list RsVehicleUtility rolledup:true
addRollout shared_tex_list RsVehicleUtility rolledup:true
addRollout shared_tex_list_truck RsVehicleUtility rolledup:true
RsVehicleSetupGlobals()

-- VehicleExporter.ms
