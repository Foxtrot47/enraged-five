--
-- File:: export/ui/MapPreviewExporter.ms
-- Description:: Map Preview Exporter Rollout
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 9 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/preview.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------
rollout RsMapPreviewRoll "Map Preview"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	checkbox chkGeometry 	"Geometry"  triState:1 across:2
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Preview" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	checkbox chkTextures 	"Textures" triState:1
	group "New Game Required:"
	(
		checkbox chkIDEIPL "Export IDE/IPL Files" triState:1
	)
	button btnPreview		"Preview"	width:120
	button btnDelete		"Delete"	width:120
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	fn loadSettings = (
		format "Loading Map Preview settings...\n"
		chkGeometry.checked = ( RsSettingsReadBoolean "rsmappreview" "geometry" true )
		chkTextures.checked = ( RsSettingsReadBoolean "rsmappreview" "textures" true )
		chkIDEIPL.checked = ( RsSettingsReadBoolean "rsmappreview" "ideipl" false )
	)
	
	fn saveSettings = (
		format "Saving Map Preview settings...\n"
		RsSettingWrite "rsmappreview" "geometry" chkGeometry.checked
		RsSettingWrite "rsmappreview" "textures" chkTextures.checked
		RsSettingWrite "rsmappreview" "ideipl" chkIDEIPL.checked
		RsSettingWrite "rsmappreview" "rollup" (not RsMapPreviewRoll.open)
	)
	
	-------------------------------------------------------------------------
	-- UI Events
	-------------------------------------------------------------------------

	--
	-- event: Preview button pressed
	-- desc: Do our preview export (much like Export Selected, less validation)
	--
	on btnPreview pressed do
	(
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected.  Select objects to be previewed and click Preview button."
		)
		else
		(
	
			-- Initialise which preview features we want.
			local RsMapPreviewMask = RsMapPreviewEmpty
			if ( chkGeometry.checked ) then
				RsMapPreviewMask = bit.or RsMapPreviewMask RsMapPreviewGeometry
			if ( chkTextures.checked ) then			
				RsMapPreviewMask = bit.or RsMapPreviewMask RsMapPreviewTextures
			if ( chkIDEIPL.checked ) then			
				RsMapPreviewMask = bit.or RsMapPreviewMask RsMapPreviewIDEIPL

			local preview = RsMapPreview RsMapPreviewMask
			preview.Export()
		)
	)

	--
	-- event: Delete button pressed
	-- desc: 
	--
	on btnDelete pressed do
	(
		local preview = RsMapPreview RsMapPreviewMask
		preview.ClearPreviewFolder()
	)

	--
	-- Rollout open event.
	--
	on RsMapPreviewRoll open do
	(
		loadSettings()
	)
	
	--
	-- Rollout closed event.
	--
	on RsMapPreviewRoll close do
	(
		saveSettings()
	)
)

-- export/ui/MapPreviewExporter.ms
