--
-- File:: pipeline/export/ui/map_geometry_ui.ms
-- Description:: Map Geometry Exporter Rollout Definition
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 10 June 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/maps/mapgeom.ms"

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

rollout RsGeomRoll "Geometry Export"
(
	-------------------------------------------------------------------------
	-- UI
	-------------------------------------------------------------------------
	hyperlink lnkHelp		"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Map_Export#Geometry_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	button btnExportAll 	"Export All Geom" width:100
	button btnExportMissing "Export Missing" width:100
	button btnExportSelected "Export Selected" width:100
	
	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------
	-- None (see pipeline/export/maps/mapgeom.ms)

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------
	
	--------------------------------------------------------------
	-- export all button pressed
	--------------------------------------------------------------	
	on btnExportAll pressed do (

		try
		(
			if ( querybox "Are you sure you want to export all?" title:"sanity check" ) == false then 
				return false

			if RsPreExport() == false then 
				return false
			
			local mapgeom = RsMapGeometry()
			mapgeom.ExportAll()
			RsPostExport()
			
			if (querybox "do you wish to build the map?") == true then (
				RsMapBuildImage()
			)		
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)	
	
	)

	--------------------------------------------------------------
	-- export missing button pressed
	--------------------------------------------------------------
	on btnExportMissing pressed do (

		try
		(
			if RsPreExport() == false then return false
			
			local mapgeom = RsMapGeometry()
			mapgeom.ExportMissing()
			RsPostExport()
			
			if (querybox "do you wish to build the map?") == true then (
				RsMapBuildImage()
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)				
	)

	--------------------------------------------------------------
	-- export selected button pressed
	--------------------------------------------------------------
	on btnExportSelected pressed do (
			
		try
		(
			if RsPreExport() == false then return false
			
			local mapgeom = RsMapGeometry()
			mapgeom.ExportSelected()
			
			RsPostExport()
			
			if (querybox "do you wish to build the map?") == true then (
				RsMapBuildImage()
			)
		)
		catch
		(
			if (getCurrentException()) == "-- Runtime error: progress_cancel" then (
				
				progressEnd()
			) else (
					
				throw()
			)
		)				
	)
	
	--------------------------------------------------------------
	-- shutdown of rollout
	--------------------------------------------------------------
	on RsGeomRoll close do (
	
		RsSettingWrite "rsmapgeom" "rollup" (not RsGeomRoll.open)
	)
)

-- pipeline/export/ui/map_geometry_ui.ms
