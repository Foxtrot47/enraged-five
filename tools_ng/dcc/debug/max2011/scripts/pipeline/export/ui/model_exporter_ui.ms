--
-- File:: model_exporter_ui.ms
-- Description:: Simple model mesh and material exporter rollout.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 14 November 2008
--

-- Model-specific includes
filein "pipeline/export/ui/weapon_exporter_ui.ms"
filein "pipeline/export/model.ms"
filein "pipeline/util/rb_script_generator.ms"

-- Additional includes
filein "pipeline/export/ui/ExporterErrorDialog.ms"

RsAnimResourceQueue = #()
RsCutPropResourceQueue = #()
RsSelSetArrayAsStrings = #()

-----------------------------------------------------------------------------
-- Rollout Definition
-----------------------------------------------------------------------------

--
-- rollout: RsSimpleModelRoll
-- desc: Simple model exporter rollout (IDR/IDD and raw export).
--
rollout RsSimpleModelRoll "Simple Model Export"
(

	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	hyperlink lnkHelp	"Help?" address:"https://devstar.rockstargames.com/wiki/index.php/Model_Export#Simple_Model_Export" align:#right color:(color 0 0 255) hoverColor:(color 0 0 255) visitedColor:(color 0 0 255)
	group "Raw:"
	(
		button btnExportRawSel			"Export Selected" width:120
		button btnExportRawSelAscii "Export Selected (ASCII)" width:120
		button btnExportRawAll			"Export All" width:120
		button btnExportRawAllAscii "Export All (ASCII)" width:120
	)
	group "Drawable (IDR):"
	(
		button btnExportDrawSel "Export Selected..." width:120
	)
	group "Drawable Dictionary (IDD):"
	(
		button btnExportDictSel "Export Selected..." width:120
		button btnExportDictAll "Export All..." width:120
	)
	group "Fragment (IFT):"
	(
		button btnExportFragSel "Export Selected..." width:120
	)
	group "Options:"
	(
		checkbox chkPreview		"Preview Folder"
		label	lblDirectory		"Output Path:"		align:#left
		edittext edtDirectory	text:RsModelOutputDirectory
	)
	
	---------------------------------------------------------------------------
	-- Functions
	---------------------------------------------------------------------------
	
	--
	-- name: GetOutputDirectory
	-- desc: Return output directory based on text entry or preview checkbox.
	--
	fn GetOutputDirectory = (
		
		if ( chkPreview.checked ) then
			RsConfigGetPatchDir()
		else
			edtDirectory.text		
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Raw
	---------------------------------------------------------------------------

	--
	-- event: btnExportRawSel pressed
	-- desc: Invokes raw model exporter on selected objects.
	--
	on btnExportRawSel pressed do (
	
		if ( 0 == selection.count ) then
			MessageBox "No objects selected." title:"Model Export Error"
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected.
		RsModelExportGroup selection asciiMesh:false
	)

	--
	-- event: btnExportRawAllAscii pressed
	-- desc: Invokes raw model exporter on selected objects (ASCII).
	--
	on btnExportRawSelAscii pressed do (
	
		if ( 0 == selection.count ) then
			MessageBox "No objects selected." title:"Model Export Error"
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected.
		RsModelExportGroup $selection asciiMesh:true
	)

	--
	-- event: btnExport pressed
	-- desc: Invokes raw model exporter on all scene objects.
	--
	on btnExportRawAll pressed do (
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
	
		-- Export all.
		RsModelExportGroup $objects asciiMesh:false
	)
	
	--
	-- event: btnExportRawAllAscii pressed
	-- desc: Invokes raw model exporter on all scene objects (ASCII).
	--
	on btnExportRawAllAscii pressed do (
	
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export all.
		RsModelExportGroup $objects asciiMesh:true
	)
	
	---------------------------------------------------------------------------
	-- Drawables
	---------------------------------------------------------------------------
	
	--
	-- name: btnExportDrawSel pressed
	-- desc: Export selected object to a drawable (IDD) file.
	--
	on btnExportDrawSel pressed do (
		
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable (IDR) filename from user.
		local idr_filename = ( getSaveFileName caption:"Save Drawable (IDR) As..." types:"RAGE Drawable (.IDR)|*.idr" )
		if ( undefined == idr_filename ) then
		(			
			MessageBox "No Drawable (IDR) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected geometry.
		local drawableDir = RsModelExportGroup selection asciiMesh:false
		
		-- Construct drawable IDR file.
		local script = RsModelOutputDirectory + "/create_idr.rb"
		if ( 0 != ( RsModelScriptGenerator.BuildDrawable script idr_filename drawableDir[1] ) ) then
			MessageBox "Failed to build drawable file." title:"Model Export Error"
	)

	---------------------------------------------------------------------------
	-- Drawable Dictionary
	---------------------------------------------------------------------------	
	
	--
	-- name: btnExportDictSel pressed
	-- desc: Export selected object(s) and create a drawable dictionary (IDD) file.
	--
	on btnExportDictSel pressed do (
	
		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable Dictionary (IDD) filename from user.
		local idd_filename = ( getSaveFileName caption:"Save Drawable Dictionary (IDD) As..." types:"RAGE Drawable Dictionary (.IDD)|*.idd" )
		if ( undefined == idd_filename ) then
		(
			MessageBox "No Drawable Dictionary (IDD) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export all geometry.
		local drawableDirs = RsModelExportGroup selection asciiMesh:false
		
		-- Constuct drawable dictionary IDD file.
		local script = RsModelOutputDirectory + "/create_idd.rb"
		if ( 0 != ( RsModelScriptGenerator.BuildDrawableDictionary script idd_filename drawableDirs ) ) then
			MessageBox "Failed to build drawable dictionary file." title:"Model Export Error"
	)
	
	--
	-- name: btnExportDictAll pressed
	-- desc: Export all objects and create a drawable dictionary (IDD) file.
	--
	on btnExportDictAll pressed do (
	
		-- Get Drawable Dictionary (IDD) filename from user.
		local idd_filename = ( getSaveFileName caption:"Save Drawable Dictionary (IDD) As..." types:"RAGE Drawable Dictionary (.IDD)|*.idd" )
		if ( undefined == idd_filename ) then
		(
			MessageBox "No Drawable Dictionary (IDD) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export all geometry.
		local drawableDirs = RsModelExportGroup $objects asciiMesh:false
		
		-- Constuct drawable dictionary IDD file.
		local script = RsModelOutputDirectory + "/create_idd.rb"
		if ( 0 != ( RsModelScriptGenerator.BuildDrawableDictionary script idd_filename drawableDirs ) ) then
			MessageBox "Failed to build drawable dictionary file." title:"Model Export Error"
	)
	
	
	---------------------------------------------------------------------------
	-- Drawable Dictionary
	---------------------------------------------------------------------------	
	
	--
	-- name: btnExportFragSel pressed
	-- desc: Export selected object and create a fragment (IFT) file.
	--	
	on btnExportFragSel pressed do (

		if ( 0 == selection.count ) then
		(
			MessageBox "No objects selected." title:"Model Export Error"
			return false
		)
		
		-- Get Drawable (IDR) filename from user.
		local ift_filename = ( getSaveFileName caption:"Save Fragment (IFT) As..." types:"RAGE Fragment (*.ift)|*.ift" )
		if ( undefined == ift_filename ) then
		(			
			MessageBox "No Fragment (IFT) file specified.  Export aborted." title:"Model Export Error"
			return false
		)
		
		-- Save our output directory.
		RsModelSetOutputDirectory (GetOutputDirectory())
		
		-- Export selected geometry.
		local fragmentDir = RsModelExportGroup selection asciiMesh:false
		
		-- Construct drawable IDR file.
		local script = RsModelOutputDirectory + "/create_ift.rb"
		if ( 0 != ( RsModelScriptGenerator.BuildFragment script ift_filename fragmentDir[1] ) ) then
			MessageBox "Failed to build fragment file." title:"Model Export Error"
	)
	

	---------------------------------------------------------------------------
	-- Options
	---------------------------------------------------------------------------	
	
	--
	-- name: chkPreview state changed
	-- desc: Enable/disable the directry entry box.
	--
	on chkPreview changed state  do
	(
		edtDirectory.enabled = ( not state )
	)
)

rollout RsPropRoll "Prop Export"
(
	button btnExport "Export"
	button btnPatchExport "Patch Export"
	button btnRebuildAll "Rebuild All"
	button btnBatchExport "Batch Export"
	
--	button btnTagBones "Tag Bones"
	
	
	--------------------------------------------------------------
	-- export the textures require for specific txd's
	--------------------------------------------------------------
	fn ExportTextures texmaplist isbumplist exportDir = (

		RsMakeSurePathExists exportDir

		for i = 1 to texmaplist.count do (

			texmap = texmaplist[i]
			isbump = isbumplist[i]

			stripName = RsStripTexturePath(texmap)
			singleName = (filterstring texmap "+")[1]

			foundFiles = getfiles singleName

			if foundFiles.count > 0 then (

				outputFile = (exportDir + stripName + ".dds")

				if isbump then (

					rexExportBumpTexture texmap outputFile
				) else (

					rexExportTexture texmap outputFile
				)
			)
		)
	)
	
	fn BuildTexList obj texmaplistexport = (
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxModelGroup = getattrindex "Gta Object" "Model Group"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
		
		model = obj
				
		if getattrclass model == "Gta Object" then (

			if getattr model idxFragment == true then (

				continue
			)

			if classof model == SpeedTree_4 then (

				continue
			)

			if getattr model idxPackTextures == true then (

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObj model texmaplist maxsizelist isbumplist

				

				for texmapname in texmaplist do (

					append texmaplistexport texmapname
				)

				
			) else if RsPackingForSingleTextures == true then (

				objtexmaplist = #()
				objmaxsizelist = #()
				objisbumplist = #()

				RsGetTexMapsFromObj model objtexmaplist objmaxsizelist objisbumplist

				txdname = RsLowercase (getattr model idxTxd)

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()
				usagelist = #()					


				for j = 1 to objtexmaplist.count do (

					texmapname = objtexmaplist[j]
					maxsize = objmaxsizelist[j]

					addToList = false

					idxFound = finditem texmaplist texmapname

					if idxFound != 0 then (

						if usagelist[idxFound] == 1 then (

							addToList = true	
						)
					)

					if addToList then (

						append texmaplistexport texmapname
					)
				)

				
			)
		)
	)
	
	fn CreateModelLists model = (
		
		cutPropRootDir = RsConfigGetStreamDir() + "cutsprops/"
		cutPropDir = cutPropRootDir + selection[1].name
		
		outfilename = RsConfigGetStreamDir() + "mapscript/cutsprops/" + model.name + "/map.xml"
		
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxModelGroup = getattrindex "Gta Object" "Model Group"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
		
		-- init xml doc
		modelXml = XmlDocument()
		modelXml.init()


		modelXmlRoot = modelXml.createelement("mapgeom")
		modelXml.document.AppendChild modelXmlRoot
		geoTexElem = modelXml.createelement("geotex")
		geoTexTexElem = modelXml.createelement("geotextex")
		
		modelXmlRoot.AppendChild(geoTexElem)
		modelXmlRoot.AppendChild(geoTexTexElem)
		
		if getattrclass model == "Gta Object" then (
		
			if getattr model idxFragment == true then (

				continue
			)

			if classof model == SpeedTree_4 then (

				continue
			)

			texFilename = cutPropRootDir + "/" + model.name + "/entity.textures"
			RsMakeSurePathExists texFilename

			RsDeleteFiles texFilename
			
			-- Always pack the textures for cutscene props
			--if getattr model idxPackTextures == true then (

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObjNoStrip model texmaplist maxsizelist isbumplist

				texFile = openfile texFilename mode:"w+"

				for j = 1 to texmaplist.count do (

					texmapname = texmaplist[j]
					maxsize = maxsizelist[j]
					texmapnamestrip = RsStripTexturePath texmapname
					texmapnamestrip = (filterstring texmapnamestrip ".")[1]

					outputFile = RsConfigGetTexDir() + texmapnamestrip + ".dds"			

					if isbumplist[j] then (

						rexExportBumpTexture texmapname outputFile maxsize
					) else (

						rexExportTexture texmapname outputFile maxsize
					)

					format "%\n" texmapnamestrip to:texFile
				)

				close texFile


				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObj model texmaplist maxsizelist isbumplist
				
				objElem = modelXml.CreateElement(model.name)
				geoTexTexElem.AppendChild(objElem)

				for texmapname in texmaplist do (

					texMapElem = modelXml.CreateElement(texmapname)
					objElem.AppendChild(texMapElem)
				)
				
				objElem = modelXml.CreateElement(model.name)
				geoTexElem.AppendChild(objElem)
		
			--)
			
			
		)
		
		RsMakeSurePathExists outfilename
		result = modelXml.save outfilename
		rubyString = "geo_content = Pipeline::Content::CutscenePropGeo.new(\"" + outfilename + "\", project )\n"
		rubyString = rubyString + "component_group.add_child(geo_content)\n\n"
		append RsCutPropResourceQueue rubyString
		result
		
	)
	
	fn BuildCutPropsQueueContent patchExport:false = (
		print "entered BuildCutPropsQueueContent"
		rubyFileName = RsConfigGetScriptDir() + "/cutsprops/batch_resource.rb"
		RsMakeSurePathExists rubyFileName
		deletefile rubyFileName
		rubyFile = createfile rubyFileName

		RBGenCutPropResourcingScriptPre rubyFile
		for scriptstr in RsCutPropResourceQueue do (
			format scriptstr to:rubyFile
		)
		RBGenResourcingScriptPost rubyFile
		if patchExport == true then (
			RBGenResourcingScriptPatch rubyFile
		)

		close rubyFile

		cmdline = "ruby " + rubyFileName
		print("Command line " + cmdline)
		if doscommand cmdline != 0 then (

			messagebox "failed to build model resource"
			return false
		)

		RsCutPropResourceQueue = #()
			
			
	)
	
	
	fn ExportProps patchExport:false = (
		
		infoFileName = RsConfigGetBinDir() + "info.rbs"
		
		infoFile = openfile infoFileName mode:"w+"
		format "set_mipmapcount_max(1)\n" to:infoFile 
		format "set_mipmap_interleaving(0)\n" to:infoFile 
		close infoFile
		
		if RsCreateSetupScript() == false then return false

		if selection.count != 1 then (

			messagebox "select just one object to export"
			return 0
		)

		
		texmaplist = #()
		maxsizelist = #()
		isbumplist = #()
		
		txdList = #()
		
		txdtexmaplist = #()
		txdmaxsizelist = #()
		txdisbumplist = #()
		txdusagelist = #()
		
		
		RsGetSortedTxdList rootnode.children txdlist
		
		
		for txdname in txdlist do (
				
			inputobjlist = #()
			texmaplist = #()
			maxsizelist = #()
			isbumplist = #()
			usagelist = #()
			
			
			RsGetInputObjectList rootnode.children inputobjlist
			RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist isCutsceneObject:true				

			append txdtexmaplist texmaplist
			append txdmaxsizelist maxsizelist
			append txdisbumplist isbumplist
			append txdusagelist usagelist
		)
		
		

		-- export files
		cutPropRootDir = RsConfigGetStreamDir() + "cutsprops/"
		cutPropDir = cutPropRootDir + selection[1].name					

		obj = selection[1]
		objDir = cutPropDir + "/" + (obj.name)

		rexReset()
		graphRoot = rexGetRoot()
		
		
		

		select obj

		entity = rexAddChild graphRoot "entity" "Entity"
		rexSetPropertyValue entity "OutputPath" cutPropDir
--		
		
		select obj
		
		meshobj = rexAddChild entity "mesh" "Mesh"
		rexSetPropertyValue meshobj "OutputPath" cutPropDir
		rexSetPropertyValue meshobj "ExportTextures" "true"
		rexSetPropertyValue meshobj "TextureOutputPath" (RsConfigGetTexDir())
		rexSetPropertyValue meshobj "MeshForSkeleton" "false"
--		rexSetPropertyValue meshobj "MeshAsAscii" "false"
--		rexSetPropertyValue meshobj "MeshSkinOffset" "false"
		rexSetNodes meshobj
		
		RsGetTexMapsFromObj obj texmaplist maxsizelist isbumplist
		
		select obj
		
		skeleton = rexAddChild entity "skeleton" "Skeleton"
		rexSetPropertyValue skeleton "OutputPath" cutPropDir
		rexSetPropertyValue skeleton "ChannelsToExport" "(transX;transY;transZ;)(rotX;rotY;rotZ;)"
		rexSetPropertyValue skeleton "SkeletonWriteLimitAndLockInfo" "true"
		rexSetPropertyValue skeleton "SkeletonRootAtOrigin" "true"
		rexSetPropertyValue skeleton "SkeletonUseBoneIDs" "true"
		rexSetNodes skeleton

			
		select obj
		
		RsSetMapsFlags true true true true
		
		rexExport()
		
			/*
		-- build script file

		cutFile = "cutsprops/" + obj.name
		luaFileName = RsConfigGetScriptDir() + "cutsprops/" + obj.name + ".rbs"
		RsMakeSurePathExists luaFileName

		testlist = #()

		--BuildTexList obj testlist

		luaFile = openfile luaFileName mode:"w+"

		format "require(\"%config.rbs\")\n" (RsConfigGetProjBinDir()) to:luaFile
		format "require(dirBin .. \"script/buildtxd.rbs\")\n" to:luaFile
		format "require(dirBin .. \"script/buildmodeldictionary.rbs\")\n" to:luaFile
		format "require(dirBin .. \"script/buildMapGeometries.rbs\")\n" to:luaFile
		format "require(dirBin .. \"script/convert.rbs\")\n" to:luaFile

		--format "require(dirBin .. \"script/buildMapDynamicBounds.rbs\")\n\n" to:luaFile

		if patchExport == true then (
			patchDir = RsConfigGetPatchDir()
			RsMakeSurePathExists patchDir
			format "dirPatch = \"%\"\n" patchDir to:luaFile
		)
		format "dirCutProps = dirStream .. \"cutsprops/\"\n" cutFile to:luaFile
		
		format "cutPropName = \"%\"\n" selection[1].name to:luaFile
		format "cutPropFile = dirCutProps .. cutPropName\n\n" to:luaFile

		format "objWithTexTexList = {\n" to:luaFile
		
		idxFragment = getattrindex "Gta Object" "Is Fragment"
		idxModelGroup = getattrindex "Gta Object" "Model Group"
		idxTxd = getattrindex "Gta Object" "TXD"
		idxPackTextures = getattrindex "Gta Object" "Pack Textures"
				
		model = obj
		

		if getattrclass model == "Gta Object" then (

			if getattr model idxFragment == true then (

				continue
			)

			if classof model == SpeedTree_4 then (

				continue
			)

			texFilename = cutPropRootDir + "/" + obj.name + "/entity.textures"
			RsMakeSurePathExists texFilename
								
			RsDeleteFiles texFilename

			if getattr model idxPackTextures == true then (
				
				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObjNoStrip model texmaplist maxsizelist isbumplist

				texFile = openfile texFilename mode:"w+"

				for j = 1 to texmaplist.count do (

					texmapname = texmaplist[j]
					maxsize = maxsizelist[j]
					texmapnamestrip = RsStripTexturePath texmapname
					texmapnamestrip = (filterstring texmapnamestrip ".")[1]

					outputFile = RsConfigGetTexDir() + texmapnamestrip + ".dds"			

					if isbumplist[j] then (

						rexExportBumpTexture texmapname outputFile maxsize
					) else (

						rexExportTexture texmapname outputFile maxsize
					)

					format "%\n" texmapnamestrip to:texFile
				)

				close texFile
				
				
				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()

				RsGetTexMapsFromObj model texmaplist maxsizelist isbumplist

				format "\t{" to:luaFile

				for texmapname in texmaplist do (

					format "\"%\"," texmapname to:luaFile
				)

				format "},\n" to:luaFile
			) else if RsPackingForSingleTextures == true then (

				objtexmaplist = #()
				objmaxsizelist = #()
				objisbumplist = #()

				RsGetTexMapsFromObj model objtexmaplist objmaxsizelist objisbumplist

				txdname = RsLowercase (getattr model idxTxd)

				texmaplist = #()
				maxsizelist = #()
				isbumplist = #()
				usagelist = #()					

				TxdNum = finditem txdlist txdname

				if TxdNum != 0 then (

					texmaplist = txdtexmaplist[TxdNum]
					maxsizelist = txdmaxsizelist[TxdNum]
					isbumplist = txdisbumplist[TxdNum]
					usagelist = txdusagelist[TxdNum]
				)

--					RsGetTexMapsByTxdName texmaplist maxsizelist isbumplist usagelist txdname srcobjlist:inputobjlist

				format "\t{" to:luaFile
				
				
				for j = 1 to objtexmaplist.count do (

					texmapname = objtexmaplist[j]
					
					maxsize = objmaxsizelist[j]

					addToList = false

					idxFound = finditem texmaplist texmapname
					
					if idxFound != 0 then (
						
						if usagelist[idxFound] == 1 then (

							addToList = true	
						)
					)

					if addToList then (

						format "\"%\"," texmapname to:luaFile
					)
				)

				format "},\n" to:luaFile
			)
		)

			
		format "}\n\n" to:luaFile
		

		format "modelList = {\n" to:luaFile

		format "\"%\"\n" (obj.name) to:luaFile


		format "}\n\n" to:luaFile

		format "setConfigOverride(dirBin .. \"info.rbs\")\n" to:luaFile

		if patchExport == false then (
			format "buildMapGeometriesWithTex(dirCutProps,modelList,objWithTexTexList)\n" to:luaFile
		)
		else (
			format "buildMapGeometriesWithTexPatch(dirCutProps,dirPatch,modelList,objWithTexTexList)\n" to:luaFile
			format "if configurations.win32.enabled == true then\n" to:luaFile
			format "\tconvert(dirPatch .. cutPropName ..\".idr\",dirPatch .. cutPropName .. \".wdr\",\"win32\")\n" to:luaFile
			format "end\n" to:luaFile
			format "if configurations.xenon.enabled == true then\n" to:luaFile
			format "\tconvert(dirPatch .. cutPropName ..\".idr\",dirPatch .. cutPropName .. \".xdr\",\"xenon\")\n" to:luaFile
			format "end\n" to:luaFile
			format "if configurations.ps3.enabled == true then\n" to:luaFile
			format "convert(dirPatch .. cutPropName ..\".idr\",dirPatch .. cutPropName .. \".cdr\",\"ps3\")\n" to:luaFile
			format "end\n" to:luaFile
		)
		
		

		close luaFile

		cmdline = RsConfigGetBinDir() + RsConfigGetRagebuilder() + " " + luaFileName
		print("Command line " + cmdline)
		if doscommand cmdline != 0 then (

			messagebox "failed to build model resource"
			return false
		)
		*/

		return true
	)
	
	
	fn BatchExportProps projectname = (
		
		-- Load the XML file
		xmlDoc = XmlDocument()	
		xmlDoc.init()
		xmlDoc.load ( RsConfigGetProjBinConfigDir() + "\\anim\\BatchCutscenes.xml" )
	
		gtarootelement = xmlDoc.document.DocumentElement
		rootchildren = gtarootelement.childnodes
		
		-- Find the project node
		for i = 0 to ( rootchildren.Count - 1 ) do (
			child = rootchildren.itemof(i)
			if child.name == projectName then gtarootelement = child
		)
		
		gtaelement = gtarootelement.firstchild
		gtaelement = gtaelement.firstchild
		gtachildren = gtaelement.childnodes
		
		if gtaelement.name != "objects" then (
			messagebox "Error: Could not parse XML, objects node not found"
		)

		SetQuietMode true
		
		-- Loop through every element in the XML group and open and export it.
		for i = 0 to ( gtachildren.Count - 1 ) do (
			elems = gtachildren.itemof(i)
			
			projpath = RsLowercase(RsMakeBackSlashes(RsConfigGetProjRootDir()))
			propfilename =  projpath + projectName + "_art\\anim\\cutscenes\\Models\\props\\" + RsLowercase(elems.innertext)
					
			if RsCheckFileHasLoaded propfilename == true then (
				tokens = filterstring ( RsLowercase(elems.innertext) ) "."
				for obj in rootnode.children do (
					if RsLowercase(obj.name) == RsLowercase(tokens[1]) then select obj
				)
				if selection.count == 1 then (
					if ExportProps() == true then (
						CreateModelLists selection[1]
					)
				)
				else append RsCutsBatchWarnings ("Could not find mesh " + RsLowercase(tokens[1]) " in " + elems.innertext)
			)
			else append RsCutsBatchWarnings ("Could not load " + elems.innertext)
			
		)
		SetQuietMode false
		BuildCutPropsQueueContent()
	)
	
	fn ThrowWarnings batchWarnings = (
		if RsCutsBatchWarnings.count > 0 then (
			global gBatchWarnings = RsCutsBatchWarnings

			rollout RsBatchWarningsRoll "Batch Export Warnings"
			(
				listbox lstMissing items:gBatchWarnings
				button btnOK "OK" width:100 pos:[250,150]

				on btnOK pressed do (

					DestroyDialog RsBatchWarningsRoll
				)	
			)

			CreateDialog RsBatchWarningsRoll width:750 modal:true
		)
	)
	
	
	on btnRebuildAll pressed do (
		rbscript = "content.name == 'cutsprops' and content.xml_type == 'rpf'"
			contenttype = bit.or RS_CONTENT_OUTPUT RS_CONTENT_PLATFORM
			contenttype = bit.or contenttype RS_CONTENT_PEDS
			RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutsprops/") rbscript (RsConfigGetProjectName()) contenttype dorebuild:true
	)
	
	on btnExport pressed do (
		if ExportProps() == true then (
			CreateModelLists selection[1]
		)
		BuildCutPropsQueueContent()
		if querybox "Wanna build the image?" == true then (
			rbscript = "content.name == 'cutsprops' and content.xml_type == 'rpf'"
			contenttype = bit.or RS_CONTENT_OUTPUT RS_CONTENT_PLATFORM
			contenttype = bit.or contenttype RS_CONTENT_PEDS
			RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutsprops/") rbscript (RsConfigGetProjectName()) contenttype
			
		)
	)
	
	on btnPatchExport pressed do (
		RsClearPatchDir()
		if ExportProps patxhExport:true == true then (
			CreateModelLists selection[1]
		)
		BuildCutPropsQueueContent patchExport:true
	)
	
	on btnBatchExport pressed do (
		BatchExportProps RsSettingsRoll.lstConfig.selected
		if querybox "Wanna build the image?" == true then (
			rbscript = "content.name == 'cutsprops' and content.xml_type == 'rpf'"
			contenttype = bit.or RS_CONTENT_OUTPUT RS_CONTENT_PLATFORM
			contenttype = bit.or contenttype RS_CONTENT_PEDS
			RsBuildAndConvertImage (RsConfigGetStreamDir() + "cutsprops/") rbscript (RsConfigGetProjectName()) contenttype
			
		)
	)
	
--	on btnTagBones pressed do (
--		TagPropBones()
--	)

)

-----------------------------------------------------------------------------
-- Entry-Point
-----------------------------------------------------------------------------

shared_tex_list = RsCreateSharedTextureListRollout "RsWeaponTexList" "Shared Textures" RsWeaponsTextureListFilename

try CloseRolloutFloater RsModelUtil catch()
RsModelUtil = newRolloutFloater "RS Model Exporter" 200 500 50 126
addRollout RsSettingsRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rssettings" "rollup" false)
addRollout RsWeaponRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsweapon" "rollup" false)
addRollout RsWeaponAnimRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsweaponanim" "rollup" false)
addRollout RsSimpleModelRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsmodel" "rollup" true)
addRollout RsPropRoll RsModelUtil rolledup:(RsSettingsReadBoolean "rsprop" "rollup" false)

-- model_exporter_ui.ms
