--
-- File:: pipeline/export/water.ms
-- Description:: Water export/import rollout.
--
-- Author:: Greg Smith <greg@rockstarnorth.com>
-- Date:: 30/3/2004
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 31 March 2010
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/water.ms"

-----------------------------------------------------------------------------
-- Rollouts
-----------------------------------------------------------------------------

--
-- rollout: RsWaterExportRoll
-- desc:
--
rollout RsWaterExportRoll "Export Water"
(
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	button btnExport 	"Export" width:100
	button btnImport 	"Import" width:100
	button btnTest		"Test" width:100
	
	group "Attributes:"
	(
		dropdownlist listSelVal items:#("Point1 Flow X","Point1 Flow Y","Point1 Big Wave Strength","Point1 Small Wave Strength","Point2 Flow X","Point2 Flow Y","Point2 Big Wave Strength","Point2 Small Wave Strength","Point3 Flow X","Point3 Flow Y","Point3 Big Wave Strength","Point3 Small Wave Strength","Point4 Flow X","Point4 Flow Y","Point4 Big Wave Strength","Point4 Small Wave Strength")
		spinner spinVal 	"Set Value" scale:0.01 range:[-10,10,0]
		button btnSet 		"Set" width:100
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------
	
	--
	-- event: btnSet pressed
	-- desc: Set water helper attribute value.
	--
	on buttonSetVal pressed do (
	
		for obj in selection do (
		
			if getattrclass obj == "Gta Water" then (
				setattr obj (getattrindex "Gta Water" listSelVal.selected) spinVal.value
			)
		)
	)
	
	--
	-- event: btnExport pressed
	-- desc: Export water DAT file from the 3dsmax scene.
	--
	on btnExport pressed do (
		ExportWaterObjFile (getSaveFilename caption:"Water File" types:"dat file (*.dat)|*.dat")
	)
	
	--
	-- event: btnImport pressed
	-- desc: Import water DAT file into the 3dsmax scene.
	--
	on btnImport pressed do (
		ImportWaterObjFile (getOpenFilename caption:"Water File" types:"dat file (*.dat)|*.dat")
	)
	
	--
	-- name: btnTest pressed
	-- desc: Test water for export issues.
	--
	on btnTest pressed do (
		ExportWaterTest()
	)
)

try CloseRolloutFloater RsWaterExportUtil catch()
RsWaterExportUtil = newRolloutFloater "Export Water" 200 220 1 136
addRollout RsWaterExportRoll RsWaterExportUtil