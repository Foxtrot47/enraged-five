-- 
-- SharedTextureList.ms
-- Shared Texture List UI Rollout Definition
--
-- This file presents a simple UI wrapper around the SharedTextureList struct
-- defined in ../../util/shared_texture_list.ms.
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 21 February 2008
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
fileIn "pipeline/util/file.ms"
fileIn "pipeline/util/shared_texture_list.ms"

-----------------------------------------------------------------------------
-- Globals
-----------------------------------------------------------------------------
RsTextureFilter = "DirectX Texture (*.dds)|*.dds"

-----------------------------------------------------------------------------
-- Implementation
-----------------------------------------------------------------------------

--
-- name: RsCreateSharedTextureListRollout
-- desc: Dynamically creates and returns a shared texture list rollout, it can
--       be added to a rollout floater using the addRollout function.
--
fn RsCreateSharedTextureListRollout rollname caption texturexml = (
	
	rci = rolloutCreator rollname caption
	rci.begin()
		
	-------------------------------------------------------------------------
	-- Locals
	-------------------------------------------------------------------------
	
	-- Jeeebuz MaxScript's rolloutCreator.addLocal() SUCKS!
	--   Trying rci.addLocal "texture_xml" init:texturexml fucks up the
	--   string init value by removing the double quote marks.  No need.
	textureXmlSS = stringStream ""
	format "local texture_xml = @%@" texturexml to:textureXmlSS
	
	rci.addText ( textureXmlSS as string ) filter:on
	rci.addLocal "texturelist" init:undefined

	-------------------------------------------------------------------------
	-- Widgets
	-------------------------------------------------------------------------
	
	rci.addControl #listbox #lstTextures "Textures"
	rci.addControl #button 	#btnAdd "Add" paramStr:"width:78 across:2"
	rci.addControl #button 	#btnRemove "Remove" paramStr:"width:78"
	rci.addControl #button  #btnReload "Reload" paramStr:"width:100"

	-------------------------------------------------------------------------
	-- Functions
	-------------------------------------------------------------------------

	rci.addText "
		--
		-- name: RefreshTexListBox
		-- desc:
		--	
		fn RefreshTexListBox sharedlist = (
	
			items = #()
			for tex in sharedlist.textures do
			(
				append items ( RsRemovePathAndExtension tex )
			)
			lstTextures.items = items
		)" filter:on
	
	rci.addText "
		--
		-- name: ReloadTexList
		-- desc: Reloads shared texture list, configuring UI as required.
		--
		fn ReloadTexList filename:undefined = (
		
			if ( undefined != filename ) then
			(
				format \"Reloading shared texture list from: %\n\" filename
				texture_xml = filename
			)
		
			texturelist = SharedTextureList()
			texturelist.load texture_xml
			
			RefreshTexListBox texturelist
		
			local ro = ( RsIsFileReadOnly texture_xml )
			btnAdd.enabled = ( not ro )
			btnRemove.enabled = ( not ro )
		)	
		" filter:on

	rci.addText "
		--
		-- name: GetNewTextureFilename
		-- desc: Gets a texture filename from the user, checking whether its 
		--       already in the specified SharedTextureList.
		--
		fn GetNewTextureFilename texturelist = (
		
			newtexfile = getOpenFilename caption:@Add a texture file@ types:RsTextureFilter
			if ( undefined == newtexfile ) then
				return undefined
				
			newtexfile = RsMakeSafeSlashes( newtexfile )
			
			-- Ensure item has not already been added
			if ( texturelist.contains newtexfile ) then
			(
				messageBox @This texture has already been added to this shared list.@
				return undefined
			)
			
			newtexfile
		)" filter:on

	-------------------------------------------------------------------------
	-- Event Handlers
	-------------------------------------------------------------------------	
	
	rci.addHandler rollname #open codeStr:"ReloadTexList()" filter:on 
	rci.addHandler rollname #close codeStr:"" filter:off
	
	-- Install handler for Add button to add a new texture, filename
	-- chosen by user using Open File Dialog box.
	rci.addHandler #btnAdd #pressed codeStr:"
			texture = ( GetNewTextureFilename texturelist )
			if ( undefined == texture ) then
				return ( false )
			
			-- Otherwise add it and refresh our list
			texturelist.add texture
			texturelist.save()
			
			RefreshTexListBox texturelist" filter:on
	
	-- Install handler for listbox selection to enable/disable the Remove
	-- button to prevent errors in texture removal.
	rci.addHandler #lstTextures #selected paramStr:"index" codeStr:"
			btnRemove.enabled = ( ( index > 0 ) and ( lstTextures.items.count > 0 ) )
			" filter:off
	
	-- Install handler for Remove button to remove a texture.  File removed
	-- is the one that is selected in the textures listbox.
	rci.addHandler #btnRemove #pressed codeStr:"
			texture = texturelist.textures[lstTextures.selection]
			
			if ( undefined != texture ) then
			(			
				texturelist.remove texture
				texturelist.save()
				
				RefreshTexListBox texturelist
			)
			else
			(
				ss = stringStream @ @
				format @Error removing texture: %.  Contact tools.@ texture
				MessageBox ( ss as string )
			)
			" filter:on
	
	-- Install handler to reload XML list, useful for when a user checks
	-- out the file after opening the exporter's rollout.
	rci.addHandler #btnReload #pressed codeStr:"ReloadTexList()" filter:off
	
	-------------------------------------------------------------------------
	-- Create rollout and return
	-------------------------------------------------------------------------
	
	-- This returns the dynamic rollout definition to the caller.
	-- It can be passed to the addRollout function.
	rci.end()	
)

-- End of SharedTextureList.ms
