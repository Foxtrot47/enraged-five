--
-- File:: pipeline/export/ui/vehicle_settings_ui.ms
-- Description::
--
-- Author:: David Muir <david.muir@rockstarnorth.com>
-- Date:: 27 October 2009
--

-----------------------------------------------------------------------------
-- Uses
-----------------------------------------------------------------------------
filein "pipeline/export/vehicles/globals.ms"
filein "pipeline/export/vehicles/ruby_script_generator.ms"
filein "pipeline/export/vehicles/settings.ms"
filein "pipeline/util/shared_texture_list.ms"
filein "rockstar/export/settings.ms"


-----------------------------------------------------------------------------
-- UI
-----------------------------------------------------------------------------

--
-- rollout: RsVehicleSettingsRoll
-- desc: Vehicle exporter-specific settings
--
rollout RsVehicleSettingsRoll "Vehicle Settings"
(
	---------------------------------------------------------------------------
	-- UI
	---------------------------------------------------------------------------
	dropdownlist 	lstVehicleRPF "Vehicle RPF:"
	group "Debug:"
	(
		checkbox		chkAsAscii 		"Export ASCII Meshes"
	)
	
	---------------------------------------------------------------------------
	-- Events
	---------------------------------------------------------------------------

	--
	-- event: lstVehicleRPF dropdown item selected
	-- desc:
	--
	on lstVehicleRPF selected index do
	(
		RsVehicleRPFIndex = index
		RsVehicleSetupGlobals()
	)
	
	--
	-- event: chkAsAscii checkbox state changed
	-- desc:
	--
	on chkAsAscii changed state do
	(
		RsVehicleAsAscii = state
	)

	--
	-- event: RsVehicleSettingsRoll opened event
	-- desc: On-open event; fetch list of RPFs to display.
	--
	on RsVehicleSettingsRoll open do
	(		
		RsVehicleRPFs = ( RsProjectContentFindAll type:"vehiclesrpf" )
		local rpfs = #()
		for vrpf in RsVehicleRPFs do
			append rpfs vrpf.friendlyname
		lstVehicleRPF.items = rpfs
		
		RsVehicleRPFIndex = ( RsSettingsReadInteger "rsvehicle" "rpfindex" 0 )
		RsVehicleAsAscii = ( RsSettingsReadBoolean "rsvehicle" "asascii" false )
		
		-- Validate the range, otherwise set to 1.
		if ( RsVehicleRPFIndex < 1 or RsVehicleRPFIndex > rpfs.count ) then
			RsVehicleRPFIndex = 1
		lstVehicleRPF.selection = RsVehicleRPFIndex
		chkAsAscii.checked = RsVehicleAsAscii
	)
	
	--
	-- event: RsVehicleSettingsRoll closed event
	-- desc: On-close event; save selected RPF index.
	--
	on RsVehicleSettingsRoll close do
	(
		RsSettingWrite "rsvehicle" "rpfindex" RsVehicleRPFIndex
		RsSettingWrite "rsvehicle" "asascii" RsVehicleAsAscii
	)
)

-- pipeline/export/ui/vehicle_settings_ui.ms
